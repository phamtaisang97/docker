<?php
/**
 * @package		OpenCart
 * @author		Daniel Kerr
 * @copyright	Copyright (c) 2005 - 2017, OpenCart, Ltd. (https://www.opencart.com/)
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://www.opencart.com
*/

/**
* Controller class
*/
abstract class Controller {
	protected $registry;

	public function __construct($registry) {
		$this->registry = $registry;
	}

	public function __get($key) {
		return $this->registry->get($key);
	}

	public function __set($key, $value) {
		$this->registry->set($key, $value);
	}
	public function removeChar($result, $name, $char){
        $name_case = strip_tags(html_entity_decode($result[$name], ENT_QUOTES, 'UTF-8'));
        $arr = explode($char,$name_case);
        $data =end($arr);
        return $data;
    }
    
	public function getValueFromRequest($method, $fieldRequest, $value = null)
	{
	    $method = strtolower($method);
		$data = $this->request->$method;
		if (isset($data[$fieldRequest])) {
			return $data[$fieldRequest];
		}
		if ($value) {
			return $value;
		}
		return '';
	}

    public function responseJson(array $data = [], $status_code = 200)
    {
        http_response_code($status_code);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($data));
	}
}