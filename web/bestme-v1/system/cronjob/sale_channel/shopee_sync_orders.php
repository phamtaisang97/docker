<?php

require_once __DIR__ . './../cronjob_init.php';

require_once __DIR__ . './../../library/redis_util.php';
require_once __DIR__ . './../../library/activity_tracking_util.php';

const CJ_PROCESS_KEY = 'SHOPEE_SYNC_ORDER_PROCESS'; // in seconds
const CJ_POLLING_INTERVAL = 2; // in seconds
const CJ_REDIS_TIMEOUT = 30; // in seconds

// redis job key
const CJ_JOB_KEY_DB_HOSTNAME = 'db_hostname';
const CJ_JOB_KEY_DB_PORT = 'db_port';
const CJ_JOB_KEY_DB_USERNAME = 'db_username';
const CJ_JOB_KEY_DB_PASSWORD = 'db_password';
const CJ_JOB_KEY_DB_DATABASE = 'db_database';
const CJ_JOB_KEY_DB_PREFIX = 'db_prefix';
const CJ_JOB_KEY_DB_DRIVER = 'db_driver';
const CJ_JOB_KEY_SHOP_NAME = 'shop_name';
const CJ_JOB_KEY_SHOPEE_ID = 'shopee_id';
const CJ_JOB_KEY_SYNC_NEXT_TIME = 'sync_next_time'; // timestamp
const CJ_JOB_KEY_SYNC_INTERVAL = 'sync_interval';
const CJ_JOB_KEY_SYNC_TYPE = 'sync_type'; // auto or manual
const CJ_JOB_KEY_TASK = 'task';

// all tasks
const CJ_JOB_TASK_GET_ORDERS = 'GET_ORDERS';

const CJ_UPDATE_JOB_TASK_GET_ORDERS_PREFIX = 'UPDATE_JOB_GET_ORDERS_';
const CJ_UPDATE_JOB_KEY_TYPE = 'type';
const CJ_UPDATE_JOB_TYPE_UPDATE = 'update';
const CJ_UPDATE_JOB_TYPE_REMOVE = 'remove';
const CJ_UPDATE_JOB_KEY_sync_interval = 'sync_interval';

// sync status
const SYNC_STATUS_SYNC_SUCCESS = 1;
const SYNC_STATUS_ERROR_PRODUCT_ORDER_NOT_SYNC = 0;
const SYNC_STATUS_ERROR_ORDER_IS_EDITED = 2;
const SYNC_STATUS_ERROR_PRODUCT_ONLAZADA_REMOVED = 3;
const SYNC_STATUS_ERROR_PRODUCT_OUT_OF_STOCK_ON_ADMIN = 4;
const SYNC_STATUS_ERROR_ORDER_SYNC_TIMEOUT = 5;
const SYNC_STATUS_ORDER_NOT_YET_SYNC = 6;

// global vars
global $redis;

$processing = true;
date_default_timezone_set('Asia/Ho_Chi_Minh');

/* handle kill signal */
// TODO...

/* connect to Redis host */
//println(sprintf("Connecting to Redis host %s at port %s...", MUL_REDIS_HOST, MUL_REDIS_PORT));
$redis = new Redis();
try {
    $redis->connect(MUL_REDIS_HOST, MUL_REDIS_PORT, CJ_REDIS_TIMEOUT);
    $redis->select(MUL_REDIS_DB_SHOPEE);
    //println(sprintf("Connecting to Redis host %s at port %s... connected!", MUL_REDIS_HOST, MUL_REDIS_PORT));
} catch (Exception $e) {
    println(sprintf("Connecting to Redis host %s at port %s... failed! Error: %s", MUL_REDIS_HOST, MUL_REDIS_PORT, $e->getMessage()));
    return;
}

// check max process
if (!checkMaxProcess()) {
    //println('Maximum number of processes is ' . SHOPEE_JOB_MAX_PROCESS);
    return;
}
// end check max process

if ($redis->lLen(SHOPEE_ORDER_JOB_REDIS_QUEUE) == 0) {
    //println('No Shopee job found! Exit.');
    return;
}

// increase running processes number
$redis->incr(CJ_PROCESS_KEY);

$max_job_in_process = getMaxJobInProcess();
$current_job_processed = 0;
/* get job from Redis - forever until killed or reach limited jobs per process */
while ($processing) {
    $current_job_processed++;
    $job_raw = $redis->brPop([SHOPEE_ORDER_JOB_REDIS_QUEUE], 5); // 5 seconds
    /*example $job_raw = [
        0 => 'SHOPEE_ORDER_JOB',
        1=> {data}
    ]*/

    if (!array_key_exists(0, $job_raw) || $job_raw[0] != SHOPEE_ORDER_JOB_REDIS_QUEUE) {
        break;
    }

    if (!array_key_exists(1, $job_raw)) {
        break;
    }

    $job = json_decode($job_raw[1], true);
    // debug
    /*$job = [
        CJ_JOB_KEY_DB_HOSTNAME => DB_HOSTNAME,
        CJ_JOB_KEY_DB_PORT => DB_PORT,
        CJ_JOB_KEY_DB_USERNAME => DB_USERNAME,
        CJ_JOB_KEY_DB_PASSWORD => DB_PASSWORD,
        CJ_JOB_KEY_DB_DATABASE => DB_DATABASE,
        CJ_JOB_KEY_DB_PREFIX => DB_PREFIX,
        CJ_JOB_KEY_DB_DRIVER => DB_DRIVER,
        CJ_JOB_KEY_SHOP_NAME => '',
        CJ_JOB_KEY_SHOPEE_ID => '',
        CJ_JOB_KEY_SYNC_INTERVAL => '',
        CJ_JOB_KEY_SYNC_NEXT_TIME => '',
        CJ_JOB_KEY_SYNC_TYPE => '',
        CJ_JOB_KEY_TASK => CJ_JOB_TASK_GET_ORDER
    ];*/

    if (!is_array($job)) {
        // break for new running if no job!
        //println('No Shopee job found! Exit.');
        $processing = false;
        break;
    }

    try {
        //println('Found new Shopee job! Now run.');
        handleJob($job);
    } catch (Exception $e) {
        println(sprintf('Exception while handling job "%s". Error: %s', $job_raw[1], $e->getMessage()));
    }

    if ($current_job_processed >= $max_job_in_process) {
        // break for new running if no job!
        println('This process has run the maximum number of jobs');
        $processing = false;
        break;
    }

    sleep(CJ_POLLING_INTERVAL);
}

$redis->decr(CJ_PROCESS_KEY);

// TODO: need close?...
$redis->close();

return;

/* ===local functions===*/

/**
 * handle Job
 *
 * @param array $job
 * @return bool
 */
function handleJob(array $job)
{
    global $redis;
    if (!isset($job[CJ_JOB_KEY_DB_HOSTNAME]) ||
        !isset($job[CJ_JOB_KEY_DB_PORT]) ||
        !isset($job[CJ_JOB_KEY_DB_USERNAME]) ||
        !isset($job[CJ_JOB_KEY_DB_PASSWORD]) ||
        !isset($job[CJ_JOB_KEY_DB_DATABASE]) ||
        !isset($job[CJ_JOB_KEY_DB_PREFIX]) ||
        !isset($job[CJ_JOB_KEY_DB_DRIVER]) ||
        !isset($job[CJ_JOB_KEY_SHOP_NAME]) ||
        !isset($job[CJ_JOB_KEY_SHOPEE_ID]) ||
        !isset($job[CJ_JOB_KEY_SYNC_INTERVAL]) ||
        !isset($job[CJ_JOB_KEY_TASK])
    ) {
        println('Missing either hostname or port or username or password or database or prefix or shop_name or shopee_id or task');
        return false;
    }

    /* Read info from job */
    $db_hostname = $job[CJ_JOB_KEY_DB_HOSTNAME];
    $db_port = $job[CJ_JOB_KEY_DB_PORT];
    $db_username = $job[CJ_JOB_KEY_DB_USERNAME];
    $db_password = $job[CJ_JOB_KEY_DB_PASSWORD];
    $db_database = $job[CJ_JOB_KEY_DB_DATABASE];
    $db_prefix = $job[CJ_JOB_KEY_DB_PREFIX];
    $db_driver = $job[CJ_JOB_KEY_DB_DRIVER];
    $shop_name = $job[CJ_JOB_KEY_SHOP_NAME];
    $shopee_id = $job[CJ_JOB_KEY_SHOPEE_ID];
    $sync_interval = $job[CJ_JOB_KEY_SYNC_INTERVAL];
    $sync_next_time = $job[CJ_JOB_KEY_SYNC_NEXT_TIME];
    $sync_type = isset($job[CJ_JOB_KEY_SYNC_TYPE]) ? $job[CJ_JOB_KEY_SYNC_TYPE] : 'auto';
    $task = $job[CJ_JOB_KEY_TASK];

    /* handle task */
    println(sprintf('Handling task "%s"...', $task));
    switch ($task) {
        case CJ_JOB_TASK_GET_ORDERS:
            // check job if disconnected then skip/remove? Or update sync interval
            if ($sync_type == 'auto' && $redis->exists(CJ_UPDATE_JOB_TASK_GET_ORDERS_PREFIX . $shop_name . $shopee_id)) {
                $update_job = $redis->get(CJ_UPDATE_JOB_TASK_GET_ORDERS_PREFIX . $shop_name . $shopee_id);
                $update_job = json_decode($update_job, true);
                /* data example
                 * $update_job = [
                    CJ_UPDATE_JOB_KEY_TYPE => 'update', // update or remove
                    CJ_UPDATE_JOB_KEY_sync_interval => 24,
                ];*/
                $redis->del(CJ_UPDATE_JOB_TASK_GET_ORDERS_PREFIX . $shop_name . $shopee_id);
                if (array_key_exists(CJ_UPDATE_JOB_KEY_TYPE, $update_job) && $update_job[CJ_UPDATE_JOB_KEY_TYPE] == CJ_UPDATE_JOB_TYPE_REMOVE) {
                    println(sprintf("Remove job getting orders from shopee id '%s' for shop '%s'.", $shopee_id, $shop_name));
                    break;
                } else if (array_key_exists(CJ_UPDATE_JOB_KEY_TYPE, $update_job) && $update_job[CJ_UPDATE_JOB_KEY_TYPE] == CJ_UPDATE_JOB_TYPE_UPDATE) {
                    if (array_key_exists(CJ_UPDATE_JOB_KEY_sync_interval, $update_job) && (int)$update_job[CJ_UPDATE_JOB_KEY_sync_interval] != 0) {
                        $job[CJ_JOB_KEY_SYNC_INTERVAL] = (int)$update_job[CJ_UPDATE_JOB_KEY_sync_interval];
                    } else {
                        println(sprintf("Remove job getting orders from shopee id '%s' for shop '%s'.", $shopee_id, $shop_name));
                        break;
                    }
                }
            }

            if ($job[CJ_JOB_KEY_SYNC_NEXT_TIME] != NULL && $job[CJ_JOB_KEY_SYNC_NEXT_TIME] > time()) {
                println(sprintf("Getting orders from shopee id '%s' for shop '%s'.... It's not yet time to run next time", $shopee_id, $shop_name));
                // add new job
                $redis->lPush(SHOPEE_ORDER_JOB_REDIS_QUEUE, json_encode($job));
                break;
            }

            // check if real running
            if (isProcessingStatusRealRunning($shopee_id, $shop_name)) {
                println(sprintf('Handling task "%s"... already running, skip!', $task));
                break;
            }

            // update real running
            updateProcessingStatusRealRunning($shopee_id, $shop_name);
            updateProcessingStatusRunning($shopee_id, $shop_name);

            // add job (cloned) back to queue with new NEXT_TIME
            if ($sync_type == 'auto') {
                if ((int)$job[CJ_JOB_KEY_SYNC_INTERVAL] != 0) {
                    $job[CJ_JOB_KEY_SYNC_NEXT_TIME] = time() + (int)$job[CJ_JOB_KEY_SYNC_INTERVAL] * 60; // by seconds
                    $redis->lPush(SHOPEE_ORDER_JOB_REDIS_QUEUE, json_encode($job));
                }
            }

            // do getting products
            doGetOrders($db_hostname, $db_port, $db_username, $db_password, $db_database, $db_prefix, $db_driver, $shop_name, $shopee_id, $sync_type);
            break;

        // and more case...
    }
    println(sprintf('Handling task "%s"... done!', $task));

    return true;
}

/**
 * do Get Orders
 *
 * @param string $db_hostname
 * @param string $db_port
 * @param string $db_username
 * @param string $db_password
 * @param string $db_database
 * @param string $db_prefix
 * @param string $db_driver
 * @param string $shop_name
 * @param string $shopee_id
 * @param string $sync_type
 */
function doGetOrders($db_hostname, $db_port, $db_username, $db_password, $db_database, $db_prefix, $db_driver, $shop_name, $shopee_id, $sync_type)
{
    println(sprintf('Getting orders from shopee id "%s" for shop "%s"...', $shopee_id, $shop_name));

    /* save all products from Shopee to db */
    $db = null;
    try {
        $db = new DB($db_driver,
            htmlspecialchars_decode($db_hostname),
            htmlspecialchars_decode($db_username),
            htmlspecialchars_decode($db_password),
            htmlspecialchars_decode($db_database),
            htmlspecialchars_decode($db_port)
        );
    } catch (Exception $e) {
        updateProcessingStatusRunning($shopee_id, $shop_name, false);
        updateProcessingStatusRealRunning($shopee_id, $shop_name, false);
        println(sprintf('Getting products from shopee id "%s" for shop "%s"... failed. Error: %s', $shopee_id, $shop_name, $e->getMessage()));
        return;
    }

    /* get products from Shopee */
    $shopee = Sale_Channel::getSaleChannel('shopee', ['shop_id' => (int)$shopee_id]);

    $shop = getShopeeShopInfo($db, $db_prefix, $shopee_id);
    $order_sync_range = isset($shop['order_sync_range']) ? (int)$shop['order_sync_range'] : 15; // 15 day is default

    $list_orders = getListShopeeOrders($shopee_id, $order_sync_range, $shop_name, $shopee, 100, 0);

    $count_saved = 0;
    foreach ($list_orders as $order) {
        if (!array_key_exists('ordersn', $order)) {
            continue;
        }
        $order_detail = $shopee->getOrderDetail(['ordersn_list' => [$order['ordersn']]]);
        $order_detail = json_decode($order_detail, true);
        if (!is_array($order_detail) || !array_key_exists('orders', $order_detail)) {
            continue;
        }
        $order_detail = reset($order_detail['orders']);
        if (!is_array($order_detail) || !array_key_exists('ordersn', $order_detail)) {
            continue;
        }

        // save to db
        try {
            $data = array();
            $data['code'] = $order_detail['ordersn'];
            $data['shopee_shop_id'] = $shopee_id;
            $data['cod'] = $order_detail['cod'] ? 1 : 0;
            $data['currency'] = $order_detail['currency'];
            $data['fullname'] = $order_detail['recipient_address']['name'];
            $data['phone'] = $order_detail['recipient_address']['phone'];
            $data['town'] = $order_detail['recipient_address']['town'];
            $data['district'] = $order_detail['recipient_address']['district'];
            $data['city'] = $order_detail['recipient_address']['city'];
            $data['state'] = $order_detail['recipient_address']['state'];
            $data['country'] = $order_detail['recipient_address']['country'];
            $data['zipcode'] = $order_detail['recipient_address']['zipcode'];
            $data['full_address'] = $order_detail['recipient_address']['full_address'];
            $data['actual_shipping_cost'] = $order_detail['is_actual_shipping_fee_confirmed'] ? $order_detail['actual_shipping_cost'] : $order_detail['estimated_shipping_fee'];
            $data['total_amount'] = $order_detail['total_amount'];
            $data['order_status'] = $order_detail['order_status'];
            $data['payment_status'] = $order_detail['pay_time'] ? 1 : 0;
            $data['shipping_method'] = $order_detail['shipping_carrier'];
            $data['payment_method'] = $order_detail['payment_method'];
            $data['note_for_shop'] = $order_detail['message_to_seller'];
            $data['note'] = $order_detail['note'];
            $data['create_time'] = date('Y-m-d H:i:s', $order_detail['create_time']);
            $data['update_time'] = date('Y-m-d H:i:s', $order_detail['update_time']);

            $data['items'] = $order_detail['items'];

            updateOrder($db, $db_prefix, $data, $sync_type);
        } catch (Exception $e) {
            println(sprintf('Saving shopee order (code %s) from shopee id "%s" for shop "%s"... failed. Error: %s', $order['ordersn'], $shopee_id, $shop_name, $e->getMessage()));
            continue;
        }

        $count_saved++;

        updateProcessingStatusSaved($shopee_id, $shop_name, $count_saved);
    }

    /* mark run completed */
    updateProcessingStatusRunning($shopee_id, $shop_name, false);
    updateProcessingStatusRealRunning($shopee_id, $shop_name, false);
    updateLastTimeSyncOrder($db, $db_prefix, $shopee_id);

    println(sprintf('Getting order from shopee id "%s" for shop "%s"... done. Total: %s, saved: %s', $shopee_id, $shop_name, count($list_orders), $count_saved));
}

function updateOrder(DB $db, $db_prefix, array $data, $sync_type)
{
    if (!array_key_exists('code', $data) || !$data['code']) {
        return;
    }

    $order = $db->query("SELECT * FROM `{$db_prefix}shopee_order` WHERE `code` = '" . $db->escape($data['code']) . "'");
    if (isset($order->row['code'])) {
        if ($data['update_time'] == $order->row['update_time']) {
            return;
        }
        $is_edited = isset($order->row['is_edited']) ? (int)$order->row['is_edited'] : 0;
        editOrder($db, $db_prefix, $data, $sync_type, $is_edited);
    } else {
        addOrder($db, $db_prefix, $data, $sync_type);
    }
}

function addOrder(DB $db, $db_prefix, array $data, $sync_type)
{
    try {
        if (in_array($data['order_status'], ['IN_CANCEL', 'CANCELLED', 'TO_RETURN'])) {
            return;
        }

        $db->query("INSERT INTO `{$db_prefix}shopee_order` 
                        SET `code` = '" . $db->escape($data['code']) . "', 
                            `cod` = " . (int)$data['cod'] . ", 
                            `currency` = '" . $db->escape($data['currency']) . "', 
                            `fullname` = '" . $db->escape($data['fullname']) . "', 
                            `phone` = '" . $db->escape($data['phone']) . "', 
                            `town` = '" . $db->escape($data['town']) . "',
                            `district` = '" . $db->escape($data['district']) . "', 
                            `city` = '" . $db->escape($data['city']) . "',
                            `state` = '" . $db->escape($data['state']) . "',
                            `country` = '" . $db->escape($data['country']) . "', 
                            `zipcode` = '" . $db->escape($data['zipcode']) . "', 
                            `full_address` = '" . $db->escape($data['full_address']) . "', 
                            `actual_shipping_cost` = '" . (float)($data['actual_shipping_cost']) . "',
                            `total_amount` = '" . (float)($data['total_amount']) . "', 
                            `order_status` = '" . $db->escape($data['order_status']) . "', 
                            `shipping_method` = '" . $db->escape($data['shipping_method']) . "', 
                            `payment_method` = '" . $db->escape($data['payment_method']) . "', 
                            `payment_status` = '" . (int)$data['payment_status'] . "', 
                            `note_for_shop` = '" . $db->escape($data['note_for_shop']) . "', 
                            `note` = '', 
                            `create_time` = '" . $data['create_time'] . "', 
                            `update_time` = '" . $data['update_time'] . "', 
                            `shopee_shop_id` = '" . $db->escape($data['shopee_shop_id']) . "', 
                            `sync_status` = " . (int)SYNC_STATUS_ORDER_NOT_YET_SYNC . ", 
                            `sync_time` = NOW()");

        if (array_key_exists('items', $data)) {
            updateOrderProduct($db, $db_prefix, $data['code'], $data['items']);
        }
        if ($sync_type == 'auto') {
            // update to order in admin; report, map address
            syncOrderToAdmin($db, $db_prefix, $data['code']);
        }
    } catch (Exception $e) {
        // do something...
    }

    return isset($data['code']) ? $data['code'] : '';
}

function editOrder(DB $db, $db_prefix, array $data, $sync_type, $is_edited = 0)
{
    try {
        $db->query("UPDATE `{$db_prefix}shopee_order` 
                              SET `cod` = " . (int)$data['cod'] . ", 
                                `currency` = '" . $db->escape($data['currency']) . "', 
                                `fullname` = '" . $db->escape($data['fullname']) . "', 
                                `phone` = '" . $db->escape($data['phone']) . "', 
                                `town` = '" . $db->escape($data['town']) . "',
                                `district` = '" . $db->escape($data['district']) . "', 
                                `city` = '" . $db->escape($data['city']) . "',
                                `state` = '" . $db->escape($data['state']) . "',
                                `country` = '" . $db->escape($data['country']) . "', 
                                `zipcode` = '" . $db->escape($data['zipcode']) . "', 
                                `full_address` = '" . $db->escape($data['full_address']) . "', 
                                `actual_shipping_cost` = '" . (float)($data['actual_shipping_cost']) . "',
                                `total_amount` = '" . (float)($data['total_amount']) . "', 
                                `order_status` = '" . $db->escape($data['order_status']) . "', 
                                `shipping_method` = '" . $db->escape($data['shipping_method']) . "', 
                                `payment_method` = '" . $db->escape($data['payment_method']) . "',  
                                `payment_status` = '" . (int)$data['payment_status'] . "',
                                `note_for_shop` = '" . $db->escape($data['note_for_shop']) . "', 
                                `note` = '', 
                                `create_time` = '" . $data['create_time'] . "', 
                                `update_time` = '" . $data['update_time'] . "', 
                                `shopee_shop_id` = '" . $db->escape($data['shopee_shop_id']) . "', 
                                `sync_time` = NOW() 
                                WHERE `code` = '" . $db->escape($data['code']) . "'");

        if (array_key_exists('items', $data)) {
            updateOrderProduct($db, $db_prefix, $data['code'], $data['items']);
        }

        if ($is_edited) {
            $sync_status = SYNC_STATUS_ERROR_ORDER_IS_EDITED;
            $db->query("UPDATE `{$db_prefix}shopee_order` SET `sync_status` = " . (int)$sync_status . " WHERE `code` = '" . $db->escape($data['code']) . "'");
        } else if ($sync_type == 'auto') {
            // update to order in admin; report, map address
            syncOrderToAdmin($db, $db_prefix, $data['code'], 'edit');
        }
    } catch (Exception $e) {
        // do something...
    }

    return isset($data['code']) ? $data['code'] : '';
}

function updateOrderProduct(DB $db, $db_prefix, $order_code, $items)
{
    try {
        $db->query("DELETE FROM `{$db_prefix}shopee_order_to_product` WHERE `shopee_order_code` = '" . $db->escape($order_code) . "'");
        if (is_array($items)) {
            foreach ($items as $item) {
                if (!array_key_exists('item_id', $item)) {
                    continue;
                }

                $db->query("INSERT INTO `{$db_prefix}shopee_order_to_product` SET
                                              `shopee_order_code` = '" . $db->escape($order_code) . "', 
                                              `item_id` = '" . (int)$item['item_id'] . "', 
                                              `variation_id` = '" . (int)$item['variation_id'] . "',  
                                              `variation_name` = '" . $db->escape($item['variation_name']) . "',  
                                              `quantity` = '" . (int)$item['variation_quantity_purchased'] . "',  
                                              `original_price` = '" . (float)$item['variation_original_price'] . "',  
                                              `price` = '" . (float)$item['variation_discounted_price'] . "'");
            }
        }
    } catch (Exception $e) {
        // do something ...
    }
}

function syncOrderToAdmin(DB $db, $db_prefix, $order_code, $type = 'add')
{
    try {
        // product in order
        $shopee_order_product = $db->query("SELECT * FROM `{$db_prefix}shopee_order_to_product` WHERE `shopee_order_code` = '" . $db->escape($order_code) . "'");
        $admin_order_product = [];
        $sync_status = SYNC_STATUS_SYNC_SUCCESS;
        foreach ($shopee_order_product->rows as $row) {
            if (!isset($row['item_id']) || !isset($row['quantity'])) {
                continue;
            }
            $product = mapProductInOrderToAdmin($db, $db_prefix, $row);
            if (!$product) {
                $sync_status = SYNC_STATUS_ERROR_PRODUCT_ORDER_NOT_SYNC;
                break;
            }

            $admin_order_product[] = $product;
        }
        if ($sync_status != SYNC_STATUS_SYNC_SUCCESS) {
            $db->query("UPDATE `{$db_prefix}shopee_order` SET `sync_status` = " . (int)$sync_status . " WHERE `code` = '" . $db->escape($order_code) . "'");
            return;
        } else {
            // check quantity
            if ($type == 'add') {
                foreach ($admin_order_product as $product) {
                    if (!isset($product['product_id']) || !isset($product['product_version_id']) || !isset($product['quantity']) || !isset($product['price']) || !isset($product['default_store_id']) || !isset($product['sale_on_out_of_stock'])) {
                        continue;
                    }

                    if (!$product['sale_on_out_of_stock'] && !checkQuantity($db, $db_prefix, $product['product_id'], $product['product_version_id'], $product['default_store_id'], $product['quantity'])) {
                        $sync_status = SYNC_STATUS_ERROR_PRODUCT_OUT_OF_STOCK_ON_ADMIN;
                        break;
                    }
                }

                if ($sync_status != SYNC_STATUS_SYNC_SUCCESS) {
                    $db->query("UPDATE `{$db_prefix}shopee_order` SET `sync_status` = " . (int)$sync_status . " WHERE `code` = '" . $db->escape($order_code) . "'");
                    return;
                }
            }
            $result = saveToAdmin($db, $db_prefix, $order_code, $admin_order_product);
            if (!$result) {
                $sync_status = SYNC_STATUS_ORDER_NOT_YET_SYNC;
            }
            $db->query("UPDATE `{$db_prefix}shopee_order` SET `sync_status` = " . (int)$sync_status . " WHERE `code` = '" . $db->escape($order_code) . "'");
        }
    } catch (Exception $e) {
        // do something ...
    }
}

function mapProductInOrderToAdmin(DB $db, $db_prefix, $item)
{
    $product = [
        'price' => $item['original_price'],
        'discount' => (float)$item['original_price'] - (float)$item['price'],
        'quantity' => $item['quantity']
    ];

    if ($item['variation_id']) {
        $product_version_shopee = $db->query("SELECT * FROM `{$db_prefix}shopee_product_version` WHERE `variation_id` = '" . $item['variation_id'] . "'");
        if ($product_version_shopee->num_rows == 0) {
            return false;
        }

        $product_version_shopee = $product_version_shopee->row;
        if (!$product_version_shopee['item_id'] || !$product_version_shopee['bestme_product_id']) {
            return false;
        }

        $product_admin = $db->query("SELECT * FROM `{$db_prefix}product` WHERE `product_id` = '" . $product_version_shopee['bestme_product_id'] . "' AND `deleted` IS NULL");
        if ($product_admin->num_rows == 0) {
            return false;
        }

        $product['default_store_id'] = isset($product_admin->row['default_store_id']) ? (int)$product_admin->row['default_store_id'] : 0;
        $product['product_id'] = $product_version_shopee['bestme_product_id'];
        $product['sale_on_out_of_stock'] = isset($product_admin->row['sale_on_out_of_stock']) ? (int)$product_admin->row['sale_on_out_of_stock'] : 0;
        $product['product_version_id'] = 0;

        if ($product_version_shopee['bestme_product_version_id']) {
            $product_version_admin = $db->query("SELECT * FROM `{$db_prefix}product_version` 
                                                                WHERE `product_id` = '" . $product_version_shopee['bestme_product_id'] . "' 
                                                                AND `product_version_id` = '" . $product_version_shopee['bestme_product_version_id'] . "' 
                                                                AND `deleted` IS NULL");
            if ($product_version_admin->num_rows == 0) {
                return false;
            }

            $arr_variation_name = array_map('trim', explode(',', $item['variation_name']));
            $product_version_id = 0;
            foreach ($product_version_admin->rows as $row) {
                $arr_row_name = array_map('trim', explode(',', $row['version']));
                if (count($arr_variation_name) == count($arr_row_name) && empty(array_diff($arr_variation_name, $arr_row_name)) && empty(array_diff($arr_row_name, $arr_variation_name))) {
                    $product_version_id = $row['product_version_id'];
                    break;
                }
            }
            if ($product_version_id == 0) {
                return false;
            }
            $product['product_version_id'] = $product_version_id;
        }

    } else {
        $product_shopee = $db->query("SELECT * FROM `{$db_prefix}shopee_product` WHERE `item_id` = '" . $item['item_id'] . "'");
        if ($product_shopee->num_rows == 0) {
            return false;
        }

        $product_shopee = $product_shopee->row;
        if (!$product_shopee['bestme_id']) {
            return false;
        }

        $product_admin = $db->query("SELECT * FROM `{$db_prefix}product` WHERE `product_id` = '" . $product_shopee['bestme_id'] . "' AND `deleted` IS NULL");
        if ($product_admin->num_rows == 0) {
            return false;
        }
        $product['default_store_id'] = isset($product_admin->row['default_store_id']) ? (int)$product_admin->row['default_store_id'] : 0;
        $product['product_id'] = $product_shopee['bestme_id'];
        $product['sale_on_out_of_stock'] = isset($product_admin->row['sale_on_out_of_stock']) ? (int)$product_admin->row['sale_on_out_of_stock'] : 0;
        $product['product_version_id'] = 0;

        if ($product_shopee['bestme_product_version_id']) {
            $product_version_admin = $db->query("SELECT * FROM `{$db_prefix}product_version` 
                                                                WHERE `product_id` = '" . $product_shopee['bestme_id'] . "' 
                                                                AND `product_version_id` = '" . $product_shopee['bestme_product_version_id'] . "' 
                                                                AND `deleted` IS NULL");
            if ($product_version_admin->num_rows == 0) {
                return false;
            }

            $arr_variation_name = array_map('trim', explode(',', $item['variation_name']));
            $product_version_id = 0;
            foreach ($product_version_admin->rows as $row) {
                $arr_row_name = array_map('trim', explode(',', $row['version']));
                if (count($arr_variation_name) == count($arr_row_name) && empty(array_diff($arr_variation_name, $arr_row_name)) && empty(array_diff($arr_row_name, $arr_variation_name))) {
                    $product_version_id = $row['product_version_id'];
                    break;
                }
            }
            if ($product_version_id == 0) {
                return false;
            }
            $product['product_version_id'] = $product_version_id;
        }
    }

    return $product;
}

function checkQuantity(DB $db, $db_prefix, $product_id, $product_version_id, $store_id, $quantity)
{
    $product_in_store = $db->query("SELECT * FROM `{$db_prefix}product_to_store` WHERE 
                                                                                `product_id` = " . (int)$product_id . " 
                                                                                AND `product_version_id` = " . (int)$product_version_id . "  
                                                                                AND `store_id` = " . (int)$store_id . "   
                                                                                AND `quantity` >= " . (int)$quantity);

    if ($product_in_store->num_rows > 0) {
        return true;
    }

    return false;
}

function updateQuantity(DB $db, $db_prefix, $product_id, $product_version_id, $quantity, $store_id, $condition = '-')
{
    $db->query("UPDATE `{$db_prefix}product_to_store` 
                SET `quantity` = `quantity` " . $condition . (int)$quantity . " 
                WHERE `product_id` = " . (int)$product_id . " 
                  AND `product_version_id` = " . (int)$product_version_id . " 
                  AND `store_id` = " . (int)$store_id);
}

function saveToAdmin(DB $db, $db_prefix, $order_code, $products)
{
    $shopee_order_info = $db->query("SELECT * FROM `{$db_prefix}shopee_order` WHERE `code` = '" . $db->escape($order_code) . "'");
    if ($shopee_order_info->num_rows == 0) {
        return false;
    }
    $shopee_order_info = $shopee_order_info->row;

    $map_order_satus = [
        'UNPAID' => 7,
        'TO_CONFIRM_RECEIVE' => 8,
        'READY_TO_SHIP' => 8,
        'SHIPPED' => 9,
        'RETRY_SHIP' => 8,
        'COMPLETED' => 9,
        'IN_CANCEL' => 10,
        'CANCELLED' => 10,
        'TO_RETURN' => 10
    ];
    $order_status = isset($map_order_satus[$shopee_order_info['order_status']]) ? (int)$map_order_satus[$shopee_order_info['order_status']] : 0;
    $payment_status = isset($shopee_order_info['payment_status']) ? (int)$shopee_order_info['payment_status'] : 0;
    // TODO use setting config to get user_id, language_id
    $province = getAdministrativeCodeByTextValue($shopee_order_info['state']);
    $district = $province ? getAdministrativeCodeByTextValue($shopee_order_info['city'], $province) : '';
    $ward = $district ? getAdministrativeCodeByTextValue($shopee_order_info['district'], $district) : '';
    $customer_id = getCustomerByTelephone($db, $db_prefix, ['phone' => $shopee_order_info['phone'], 'fullname' => $shopee_order_info['fullname'], 'province' => $province, 'district' => $district, 'ward' => $ward, 'full_address' => $shopee_order_info['full_address']]);
    $customer_info = json_encode(['id' => $customer_id, 'name' => $shopee_order_info['fullname']]);
    if ($order_status == 10) {
        if (isset($shopee_order_info['bestme_order_id']) && $shopee_order_info['bestme_order_id'] > 0) {
            autoRemoveReceiptVoucher($db, $db_prefix, $shopee_order_info['bestme_order_id']);
        }
    } else {
        autoUpdateReceiptVoucher($db, $db_prefix, $shopee_order_info, $customer_info);
    }

    /* tracking activity feature: count shopee order completed */
    if ($order_status == 9) {
        trackingCountShopeeOrderCompleted($db, $db_prefix);
    }
    /* end tracking activity feature: count shopee order completed  */

    if (isset($shopee_order_info['bestme_order_id']) && $shopee_order_info['bestme_order_id'] > 0) {
        $old_order_status_id = $db->query("SELECT `order_status_id` FROM `{$db_prefix}order` WHERE `order_id` = " . (int)$shopee_order_info['bestme_order_id']);
        $old_order_status_id = isset($old_order_status_id->row['order_status_id']) ? $old_order_status_id->row['order_status_id'] : 0;

        $db->query("UPDATE `{$db_prefix}order` 
                                    SET `fullname` = '" . $db->escape($shopee_order_info['fullname']) . "', 
                                    `shipping_province_code` = '" . $province . "', 
                                    `shipping_district_code` = '" . $district . "', 
                                    `shipping_ward_code` = '" . $ward . "', 
                                    `shipping_address_1` = '" . $db->escape($shopee_order_info['full_address']) . "', 
                                    `telephone` = '" . $db->escape($shopee_order_info['phone']) . "', 
                                    `shipping_method` = '" . $db->escape($shopee_order_info['shipping_method']) . "', 
                                    `shipping_method_value` = '-2', 
                                    `shipping_fee` = '" . (float)$shopee_order_info['actual_shipping_cost'] . "', 
                                    `order_status_id` = '" . $order_status . "', 
                                    `previous_order_status_id` = '" . $old_order_status_id . "', 
                                    `order_code` = '" . $db->escape($shopee_order_info['code']) . "', 
                                    `payment_method` = '" . $db->escape($shopee_order_info['payment_method']) . "', 
                                    `discount` = '', 
                                    `total` = '" . (float)$shopee_order_info['total_amount'] . "',
                                    `comment` = '" . $db->escape($shopee_order_info['note_for_shop']) . "',
                                    `user_id` = 1,
                                    `customer_id` = '" . (int)$customer_id . "',
                                    `date_modified` = '" . $shopee_order_info['update_time'] . "',
                                    `payment_status` = '" . (int)$payment_status . "',
                                    `language_id` = 2 WHERE `order_id` = '" . (int)$shopee_order_info['bestme_order_id'] . "'");

        // update quantity product
        if (in_array($old_order_status_id, [7, 8, 9]) && $order_status == 10) {
            foreach ($products as $product) {
                updateQuantity($db, $db_prefix, $product['product_id'], $product['product_version_id'], $product['quantity'], $product['default_store_id'], '+');
            }

        }
        // update to report
        $db->query("UPDATE `{$db_prefix}report_order` SET 
                                                            `customer_id` = ". (int)$customer_id . ", 
                                                            `total_amount` = " . (float)$shopee_order_info['total_amount'] . ", 
                                                            `order_status` = " . (int)$order_status . ",
                                                            `shipping_fee` = '" . (float)$shopee_order_info['actual_shipping_cost'] . "',
                                                            `report_date` = '" . $shopee_order_info['update_time'] . "'
                                                            WHERE `order_id` = " . (int)$shopee_order_info['bestme_order_id']);
    } else {
        $db->query("INSERT INTO `{$db_prefix}order` 
                                    SET `fullname` = '" . $db->escape($shopee_order_info['fullname']) . "', 
                                    `shipping_province_code` = '" . $province . "', 
                                    `shipping_district_code` = '" . $district . "', 
                                    `shipping_ward_code` = '" . $ward . "', 
                                    `shipping_address_1` = '" . $db->escape($shopee_order_info['full_address']) . "', 
                                    `telephone` = '" . $db->escape($shopee_order_info['phone']) . "', 
                                    `shipping_method` = '" . $db->escape($shopee_order_info['shipping_method']) . "', 
                                    `shipping_method_value` = '-2', 
                                    `shipping_fee` = '" . (float)$shopee_order_info['actual_shipping_cost'] . "', 
                                    `order_status_id` = '" . $order_status . "', 
                                    `order_code` = '" . $db->escape($shopee_order_info['code']) . "', 
                                    `payment_method` = '" . $db->escape($shopee_order_info['payment_method']) . "', 
                                    `discount` = '', 
                                    `total` = '" . (float)$shopee_order_info['total_amount'] . "',
                                    `comment` = '" . $db->escape($shopee_order_info['note_for_shop']) . "',
                                    `source` = 'shopee',
                                    `user_id` = 1,
                                    `customer_id` = '" . (int)$customer_id . "',
                                    `date_added` = '" . $shopee_order_info['create_time'] . "',
                                    `date_modified` = '" . $shopee_order_info['update_time'] . "',
                                    `payment_status` = '" . (int)$payment_status . "',
                                    `language_id` = 2");

        $bestme_order_id = $db->getLastId();

        /* tracking activity feature: count shopee order */
        trackingCountShopeeOrder($db, $db_prefix, (float)$shopee_order_info['total_amount']);
        /* end - tracking activity feature: count shopee order */

        $db->query("UPDATE `{$db_prefix}shopee_order` SET `bestme_order_id` = " . (int)$bestme_order_id . " WHERE `code` = '" . $db->escape($order_code) . "'");
        // insert order_product
        $total_discount = 0;
        foreach ($products as $product) {
            $discount = isset($product['discount']) ? $product['discount'] : 0;
            $total_discount += $discount;

            $db->query("INSERT INTO `{$db_prefix}order_product` 
                                                      SET `order_id` = " . (int)$bestme_order_id . ", 
                                                      `product_id` = " . (int)$product['product_id'] . ", 
                                                      `product_version_id` = " . (int)$product['product_version_id'] . ", 
                                                      `quantity` = " . (int)$product['quantity'] . ", 
                                                      `price` = " . (float)$product['price'] . ", 
                                                      `discount` = " . (float)$discount . ", 
                                                      `total` = " . (float)$product['price'] * (int)$product['quantity']);

            // update quantity
            updateQuantity($db, $db_prefix, $product['product_id'], $product['product_version_id'], $product['quantity'], $product['default_store_id']);
        }
        // insert to report
        $report_time = $shopee_order_info['update_time'];
        $date = new DateTime($report_time);
        $report_date = $date->format('Y-m-d');
        $db->query("INSERT INTO `{$db_prefix}report_order` SET 
                                                            `report_time` = '" . $report_time . "', 
                                                            `report_date` = '" . $report_date . "',
                                                            `order_id` = " . (int)$bestme_order_id . ", 
                                                            `customer_id` = " . (int)$customer_id . ",
                                                            `total_amount` = " . (float)$shopee_order_info['total_amount'] . ", 
                                                            `order_status` = " . (int)$order_status . ",
                                                            `discount` = " . $total_discount . ",
                                                            `source` = 'shopee',
                                                            `shipping_fee` = '" . (float)$shopee_order_info['actual_shipping_cost'] . "',
                                                            `store_id` = 0, `user_id` = 1");
        foreach ($products as $product) {
            $discount = isset($product['discount']) ? $product['discount'] : 0;
            $query = $db->query("SELECT cost_price 
                                                FROM `" . $db_prefix . "product_to_store`
                                                WHERE product_id = " . (int)$product['product_id'] . "
                                                    AND product_version_id= " . (int)$product['product_version_id']);
            $cost_price = $query->row['cost_price'];

            // insert report order
            $user_id = 1; // Admin
            $store_id = 0;
            $total_amount = (int)$product['quantity'] * (float)$product['price'];
            $sql = "INSERT INTO " . $db_prefix . "report_product SET report_time = '" . $report_time . "'";
            $sql .= ", report_date = '" . $report_date . "'";
            $sql .= ", order_id = '" . (int)$bestme_order_id . "'";
            $sql .= ", product_id = '" . (int)$product['product_id'] . "'";
            $sql .= ", product_version_id = '" . (int)$product['product_version_id'] . "'";
            $sql .= ", quantity = '" . (int)$product['quantity'] . "'";
            $sql .= ", price = '" . (float)$product['price'] . "'";
            $sql .= ", total_amount = '" . (float)$total_amount . "'";
            $sql .= ", discount = '" . (float)$discount . "'";
            $sql .= ", cost_price = '" . (float)$cost_price . "'";
            $sql .= ", store_id = '" . $store_id . "'";
            $sql .= ", user_id = '" . (int)$user_id . "'";
            $db->query($sql);
        }
    }

    return true;
}

function getCustomerByTelephone(DB $db, $db_prefix, $data)
{
    $customer = $db->query("SELECT DISTINCT * FROM `{$db_prefix}customer` WHERE telephone = '" . $db->escape($data['phone']) . "'");
    if (is_array($customer->row) && isset($customer->row['customer_id'])) {
        return $customer->row['customer_id'];
    }

    return createNewCustomer($db, $db_prefix, $data);
}

function createNewCustomer(DB $db, $db_prefix, $data)
{
    $customer = [
        'firstname' => isset($data['fullname']) ? extract_name($data['fullname'])[0] : '',
        'lastname' => isset($data['fullname']) ? extract_name($data['fullname'])[1] : '',
        'email' => '',
        'telephone' => $data['phone']
    ];
    $db->query("INSERT INTO `{$db_prefix}customer` 
                                            SET  `firstname` = '" . $db->escape($customer['firstname']) . "', 
                                            `lastname` = '" . $db->escape($customer['lastname']) . "', 
                                            `email` = '" . $db->escape($customer['email']) . "', 
                                            `telephone` = '" . $db->escape($customer['telephone']) . "', 
                                            `salt` = '" . $db->escape($salt = 'shopee') . "', 
                                            `password` = '" . $db->escape(sha1($salt . sha1($salt . sha1('shopee')))) . "', 
                                            `status` = 1,  
                                            `date_added` = NOW()");
    $customer_id = $db->getLastId();
    // update customer code
    $prefix = 'KH';
    $code = $prefix . str_pad($customer_id, 6, "0", STR_PAD_LEFT);
    $db->query("UPDATE " . DB_PREFIX . "customer SET `code` = '" . $code . "' WHERE customer_id = '" . (int)$customer_id . "'");

    $address_data = array(
        'customer_id' => $customer_id,
        'firstname' => $customer['firstname'],
        'lastname' => $customer['lastname'],
        'company' => '',
        'city' => $data['province'],
        'district' => $data['district'],
        'wards' => $data['ward'],
        'phone' => $customer['telephone'],
        'postcode' => '',
        'address' => $data['full_address'],
        'default' => true
    );

    $db->query("INSERT INTO `{$db_prefix}address` SET 
                                              customer_id = '" . (int)$customer_id . "', 
                                              firstname = '" . $db->escape($address_data['firstname']) . "', 
                                              lastname = '" . $db->escape($address_data['lastname']) . "', 
                                              company = '" . $db->escape($address_data['company']) . "', 
                                              district = '" . $db->escape($address_data['district']) . "', 
                                              address = '" . $db->escape($address_data['address']) . "', 
                                              phone = '" . $db->escape($address_data['phone']) . "', 
                                              postcode = '" . $db->escape($address_data['postcode']) . "', 
                                              city = '" . $db->escape($address_data['city']) . "'");

    $address_id = $db->getLastId();
    $db->query("UPDATE `{$db_prefix}customer` SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");

    // create Customer Report
    $reportTime = new DateTime();
    $reportTime->setTime($reportTime->format('H'), 0, 0);
    $sql = "INSERT INTO `{$db_prefix}report_customer` SET report_time = '" . $reportTime->format('Y-m-d H:i:s') . "'";
    $sql .= ", report_date = '" . $reportTime->format('Y-m-d') . "'";
    $sql .= ", customer_id = '" . (int)$customer_id . "'";
    $db->query($sql);

    return $customer_id;
}

/**
 * @param string $shopee_id
 * @param int $order_sync_range
 * @param string $shop_name
 * @param \Sale_Channel\Abstract_Sale_Channel $shopee
 * @param int $limit
 * @param int $offset
 * @param array $list_orders
 * @return array
 */
function getListShopeeOrders($shopee_id, $order_sync_range, $shop_name, $shopee, $limit, $offset, $list_orders = [])
{
    $date = new DateTime();
    $timestampTo = $date->getTimestamp();
    $date->modify('-'.$order_sync_range.' days');
    $timestampFrom = $date->getTimestamp();

    $orders = $shopee->getOrders(['create_time_from' => $timestampFrom, 'create_time_to' => $timestampTo, 'pagination_entries_per_page' => $limit, 'pagination_offset' => $offset]);
    $orders = json_decode($orders, true);
    if (is_array($orders) && array_key_exists('orders', $orders) && is_array($orders['orders'])) {
        $list_orders = array_merge($list_orders, $orders['orders']);

        updateProcessingStatusTotal($shopee_id, $shop_name, count($list_orders));
    }

    if (is_array($orders) && array_key_exists('more', $orders) && $orders['more']) {
        $list_orders = getListShopeeOrders($shopee_id, $order_sync_range, $shop_name, $shopee, $limit, $offset + $limit, $list_orders);
    }

    return $list_orders;
}

function checkMaxProcess()
{
    global $redis;
    if (!$redis->exists(CJ_PROCESS_KEY)) {
        $redis->set(CJ_PROCESS_KEY, 0);
        $count_process = 0;
    } else {
        $count_process = $redis->get(CJ_PROCESS_KEY);
        if ($count_process < 0) {
            $redis->set(CJ_PROCESS_KEY, 0);
        }
    }

    if ($count_process >= SHOPEE_JOB_MAX_PROCESS) {
        return false;
    }

    return true;
}

function getMaxJobInProcess()
{
    global $redis;
    $count = $redis->lLen(SHOPEE_ORDER_JOB_REDIS_QUEUE);
    if ($count > (int)SHOPEE_MAX_JOB_IN_A_PROCESS) {
        return (int)SHOPEE_MAX_JOB_IN_A_PROCESS;
    }

    return $count;
}

function updateProcessingStatusSaved($shopee_id, $shop_name, $saved)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'count_order_shopee_saved');
    $redis->set($key, $saved, SHOPEE_PROCESS_INFO_TIMEOUT);
}

function updateProcessingStatusTotal($shopee_id, $shop_name, $total)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'count_order_shopee');
    $redis->set($key, $total, SHOPEE_PROCESS_INFO_TIMEOUT);
}

function updateProcessingStatusRealRunning($shopee_id, $shop_name, $real_running = true)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'load_shopee_order_real_running');
    $redis->set($key, ((bool)$real_running) ? 1 : 0, SHOPEE_PROCESS_INFO_TIMEOUT);
}

function updateProcessingStatusRunning($shopee_id, $shop_name, $running = true)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'load_shopee_order_running');
    $redis->set($key, ((bool)$running) ? 1 : 0, SHOPEE_PROCESS_INFO_TIMEOUT);
}

function isProcessingStatusRealRunning($shopee_id, $shop_name)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'load_shopee_order_real_running');
    $real_running = $redis->get($key);

    return $real_running == 1;
}

/**
 * @param $administrative_text
 * @param null $parent_code null if province, or MUST valid code if district or ward
 * @return mixed|null
 */
function getAdministrativeCodeByTextValue($administrative_text, $parent_code = null)
{
    return (new \Vietnam_Administrative())->getAdministrativeCodeByTextValue($administrative_text, $parent_code);
}

/* Extract fullname to firstname(Tên) and lastname(Họ) */
function extract_name($fullname)
{
    $fullname = sprintf('"%s"', $fullname);
    $fullname = json_decode($fullname);
    $fullname = trim($fullname);
    $delimiter = " ";
    $parts = explode($delimiter, $fullname);
    /* get first element from array */
    $lastname = array_shift($parts);
    if (count($parts) > 0) {
        $firstname = implode($delimiter, $parts);
    } else {
        $firstname = $lastname;
    }
    return array($firstname, $lastname);
}

/* Auto create or update receipt_voucher */

function autoUpdateReceiptVoucher(DB $db, $prefix, $data, $customer_info)
{
    $voucher_class = new AUTO_CREATE_RECEIPT_VOUCHER_UTIL_CLASS($db, $prefix);
    $payment_status = isset($data['payment_status']) ? (int)$data['payment_status'] : 0;
    $dataToSave = [
        'payment_status' => $payment_status,
        'payment_method' => $data['payment_method'],
        'total_pay' => $data['total_amount'],
        'created_at' => isset($data['create_time']) ? $data['create_time'] : null,
        'updated_at' => isset($data['update_time']) ? $data['update_time'] : null,
        'customer_info' => $customer_info ? $customer_info : null
    ];
    try {
        $voucher_class->autoUpdateFromOrder($data['bestme_order_id'], $dataToSave);
        return;
    } catch (Exception $ex) {
        return;
    }

}

function autoRemoveReceiptVoucher(DB $db, $prefix, $order_id)
{
    $voucher_class = new AUTO_CREATE_RECEIPT_VOUCHER_UTIL_CLASS($db, $prefix);
    try {
        $voucher_class->removeReceiptFromOrder($order_id);
        return;
    } catch (Exception $ex) {
        return;
    }
}

function trackingCountShopeeOrder(DB $db, $db_prefix, $total)
{
    try {
        $tracking = new Activity_Tracking_Util();
        $shop_info = $db->query("SELECT * FROM `{$db_prefix}setting` WHERE `key` = '" . $db->escape('shop_name') . "'");
        $shop_name = '';

        if (isset($shop_info->row['value'])) {
            $shop_name = $shop_info->row['value'];
        }

        $packet = $db->query("SELECT * FROM `{$db_prefix}setting` WHERE `key` = '" . $db->escape('config_packet_paid') . "'");
        $domain = 'redis_auto_load_form_shopee';

        $tracking->trackingCountShopeeOrder($domain, $shop_name, $packet->row['value'], $total);
    } catch (Exception $e) {
        // do something...
    }
}

function trackingCountShopeeOrderCompleted(DB $db, $db_prefix, $total = null)
{
    try {
        $tracking = new Activity_Tracking_Util();
        $shop_info = $db->query("SELECT * FROM `{$db_prefix}setting` WHERE `key` = '" . $db->escape('shop_name') . "'");
        $shop_name = '';

        if (isset($shop_info->row['value'])) {
            $shop_name = $shop_info->row['value'];
        }

        $packet = $db->query("SELECT * FROM `{$db_prefix}setting` WHERE `key` = '" . $db->escape('config_packet_paid') . "'");
        $domain = 'redis_auto_load_form_shopee';

        $tracking->trackingCountShopeeOrderCompleted($domain, $shop_name, $packet->row['value']);
    } catch (Exception $e) {
        // do something...
    }
}

function getShopeeShopInfo(DB $db, $db_prefix, $shopee_id)
{
    try {
        $shop = $db->query("SELECT * FROM `{$db_prefix}shopee_shop_config` WHERE `shop_id` = {$shopee_id}");
        return $shop->row;
    } catch (Exception $e) {
        return [];
        // do something...
    }
}

function updateLastTimeSyncOrder(DB $db, $db_prefix, $shopee_id)
{
    try {
        $db->query("UPDATE `{$db_prefix}shopee_shop_config` SET `last_sync_time_order` = NOW() WHERE `shop_id` = {$shopee_id}");
    } catch (Exception $e) {
        // do something...
    }
}