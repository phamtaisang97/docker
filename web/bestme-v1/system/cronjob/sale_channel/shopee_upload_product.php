<?php

require_once __DIR__. './../cronjob_init.php';

const CJ_POLLING_INTERVAL = 2; // in seconds
const CJ_REDIS_TIMEOUT = 30; // in seconds

// redis job key
const CJ_JOB_KEY_DB_HOSTNAME = 'db_hostname';
const CJ_JOB_KEY_DB_PORT = 'db_port';
const CJ_JOB_KEY_DB_USERNAME = 'db_username';
const CJ_JOB_KEY_DB_PASSWORD = 'db_password';
const CJ_JOB_KEY_DB_DATABASE = 'db_database';
const CJ_JOB_KEY_DB_PREFIX = 'db_prefix';
const CJ_JOB_KEY_DB_DRIVER = 'db_driver';
const CJ_JOB_KEY_SHOP_NAME = 'shop_name';
const CJ_JOB_KEY_SHOPEE_ID = 'shopee_id';
const CJ_JOB_KEY_SHOPEE_UPLOAD_PRODUCT_IDS = 'product_ids';
const CJ_JOB_KEY_TASK = 'task';
// all tasks
const CJ_JOB_TASK_UPLOAD_PRODUCTS = 'UPLOAD_PRODUCTS';
const STATUS_PRODUCT_NOT_ENOUGH_INFO = 0;
const STATUS_PRODUCT_READY_FOR_UPLOAD = 1;
const STATUS_PRODUCT_UPLOAD_FAILED = 2;
const STATUS_PRODUCT_UPLOAD_SUCCESS = 3;

// global vars
global $redis;

$processing = true;

/* handle kill signal */
// TODO...

/* connect to Redis host */
//println(sprintf("Connecting to Redis host %s at port %s...", MUL_REDIS_HOST, MUL_REDIS_PORT));
$redis = new Redis();
try {
    $redis->connect(MUL_REDIS_HOST, MUL_REDIS_PORT, CJ_REDIS_TIMEOUT);
    $redis->select(MUL_REDIS_DB_SHOPEE);
} catch (Exception $e) {
    println(sprintf("Connecting to Redis host %s at port %s... failed! Error: %s", MUL_REDIS_HOST, MUL_REDIS_PORT, $e->getMessage()));
    return;
}
//println(sprintf("Connecting to Redis host %s at port %s... connected!", MUL_REDIS_HOST, MUL_REDIS_PORT));

/* get job from Redis - forever until killed */
while ($processing) {
    $job_raw = $redis->rPop(SHOPEE_UPLOAD_JOB_REDIS_QUEUE);
    //println('SHOPEE_UPLOAD_JOB_REDIS_QUEUE: Got raw job: ' . $job_raw);

    $job = json_decode($job_raw, true);

    // debug
    /*$job = [
        CJ_JOB_KEY_DB_HOSTNAME => DB_HOSTNAME,
        CJ_JOB_KEY_DB_PORT => DB_PORT,
        CJ_JOB_KEY_DB_USERNAME => DB_USERNAME,
        CJ_JOB_KEY_DB_PASSWORD => DB_PASSWORD,
        CJ_JOB_KEY_DB_DATABASE => DB_DATABASE,
        CJ_JOB_KEY_DB_PREFIX => DB_PREFIX,
        CJ_JOB_KEY_DB_DRIVER => DB_DRIVER,
        CJ_JOB_KEY_SHOP_NAME => '',
        CJ_JOB_KEY_SHOPEE_ID => '',
        CJ_JOB_KEY_SHOPEE_UPLOAD_PRODUCT_IDS => [],
        CJ_JOB_KEY_TASK => CJ_JOB_TASK_GET_PRODUCTS
    ];*/

    if (!is_array($job)) {
        // break for new running if no job!
        //println('No Shopee upload job found! Exit.');
        $processing = false;
        break;
    }

    try {
        //println('Found new Shopee upload job! Now run.');
        handleJob($job);
    } catch (Exception $e) {
        println(sprintf('Exception while handling job "%s". Error: %s', $job_raw, $e->getMessage()));
    }

    sleep(CJ_POLLING_INTERVAL);
}

// TODO: need close?...
$redis->close();

/**
 * handle Job
 *
 * @param array $job
 * @return bool
 */
function handleJob(array $job)
{
    if (!isset($job[CJ_JOB_KEY_DB_HOSTNAME]) ||
        !isset($job[CJ_JOB_KEY_DB_PORT]) ||
        !isset($job[CJ_JOB_KEY_DB_USERNAME]) ||
        !isset($job[CJ_JOB_KEY_DB_PASSWORD]) ||
        !isset($job[CJ_JOB_KEY_DB_DATABASE]) ||
        !isset($job[CJ_JOB_KEY_DB_PREFIX]) ||
        !isset($job[CJ_JOB_KEY_DB_DRIVER]) ||
        !isset($job[CJ_JOB_KEY_SHOP_NAME]) ||
        !isset($job[CJ_JOB_KEY_SHOPEE_ID]) ||
        !isset($job[CJ_JOB_KEY_SHOPEE_UPLOAD_PRODUCT_IDS]) ||
        !isset($job[CJ_JOB_KEY_TASK])
    ) {
        println('Missing either hostname or port or username or password or database or prefix or shop_name or shopee_id or task');
        return false;
    }

    /* Read info from job */
    $db_hostname = $job[CJ_JOB_KEY_DB_HOSTNAME];
    $db_port = $job[CJ_JOB_KEY_DB_PORT];
    $db_username = $job[CJ_JOB_KEY_DB_USERNAME];
    $db_password = $job[CJ_JOB_KEY_DB_PASSWORD];
    $db_database = $job[CJ_JOB_KEY_DB_DATABASE];
    $db_prefix = $job[CJ_JOB_KEY_DB_PREFIX];
    $db_driver = $job[CJ_JOB_KEY_DB_DRIVER];
    $shop_name = $job[CJ_JOB_KEY_SHOP_NAME];
    $shopee_id = $job[CJ_JOB_KEY_SHOPEE_ID];
    $product_ids = $job[CJ_JOB_KEY_SHOPEE_UPLOAD_PRODUCT_IDS];
    $task = $job[CJ_JOB_KEY_TASK];

    /* handle task */
    println(sprintf('Handling task "%s (data: %s)"...', $task, json_encode($job)));
    switch ($task) {
        case CJ_JOB_TASK_UPLOAD_PRODUCTS:
            // check if real running

            /* not use: have multiple job have same shop_name and shopee_id but product_ids different
             * if (isProcessingStatusRealRunning($shop_name, $shopee_id)) {
                println(sprintf('Handling task "%s"... already running, skip!', $task));
                break;
            }*/

            // update real running
            updateProcessingStatusRealRunning($shop_name, $shopee_id);

            // do getting products
            doUpLoadProducts($db_hostname, $db_port, $db_username, $db_password, $db_database, $db_prefix, $db_driver, $shop_name, $shopee_id, $product_ids);

            break;

        // and more case...
    }
    println(sprintf('Handling task "%s"... done!', $task));

    return true;
}

/**
 * do Get Products
 *
 * @param string $db_hostname
 * @param string $db_port
 * @param string $db_username
 * @param string $db_password
 * @param string $db_database
 * @param string $db_prefix
 * @param string $db_driver
 * @param string $shop_name
 * @param string $shopee_id
 * @param array $product_ids
 */
function doUpLoadProducts($db_hostname, $db_port, $db_username, $db_password, $db_database, $db_prefix, $db_driver, $shop_name, $shopee_id, $product_ids)
{
    println(sprintf('Upload products to shopee id "%s" for shop "%s"...', $shopee_id, $shop_name));

    /* get products from tempory db */
    $db = null;
    try {
        $db = new DB($db_driver,
            htmlspecialchars_decode($db_hostname),
            htmlspecialchars_decode($db_username),
            htmlspecialchars_decode($db_password),
            htmlspecialchars_decode($db_database),
            htmlspecialchars_decode($db_port)
        );
    } catch (Exception $e) {
        /* mark run completed */
        updateAllProductNotInProcess($db, $db_prefix, $product_ids, $shopee_id);
        updateProcessingStatusRunning($shop_name, $shopee_id, false);
        updateProcessingStatusRealRunning($shop_name, $shopee_id, false);
        println(sprintf('Upload products to shopee id "%s" for shop "%s"... failed. Error: %s', $shopee_id, $shop_name, $e->getMessage()));
        return;
    }
    try{
        /* upload products to Shopee to db */
        $shopee = Sale_Channel::getSaleChannel('shopee', ['shop_id' => (int)$shopee_id]);
        $shop_logistic = getLogisticByShop($shopee);
        $list_products = getProductsUpload($db, $db_prefix, $shopee, $shopee_id, $product_ids, $shop_logistic);

        $count_saved = 0;
        foreach ($list_products as $product){
            if ($product['upload_type'] == 'add'){
                //// ADD PRODUCT TO SHOPEE
                addProduct($db, $db_prefix, $shopee_id, $shopee, $product);
            }else if ($product['upload_type'] == 'update') {
                /// UPDATE PRODUCT TO SHOPEE
                updateProduct($db, $db_prefix, $shopee_id, $shopee, $product);
            }else {
                updateProductUploadResult($db, $db_prefix, $product['id'], $shopee_id, NULL, STATUS_PRODUCT_UPLOAD_FAILED, NULL, 'Không phù hợp điều kiện lấy sản phẩm');
            }
            $count_saved ++;
        }

        println(sprintf('Upload products to shopee id "%s" for shop "%s"... done. Total: %s, saved: %s', $shopee_id, $shop_name, count($product_ids), $count_saved));
    }catch (Exception $e){
        println(sprintf('Upload products to shopee id "%s" for shop "%s"... failed. Error: %s', $shopee_id, $shop_name, $e->getMessage()));
    }

    /* mark run completed */
    updateAllProductNotInProcess($db, $db_prefix, $product_ids, $shopee_id);
    updateProcessingStatusRunning($shop_name, $shopee_id, false);
    updateProcessingStatusRealRunning($shop_name, $shopee_id, false);
}




function addProduct($db, $db_prefix, $shopee_id, $shopee, $product)
{
    $flag_update_2_tier_variations = false;
    if (isset($product['type_tier']) && $product['type_tier'] > 0) {
        $flag_update_2_tier_variations = true;
    }
    $result_add = $shopee->uploadProduct($product);
    $result_add = json_decode($result_add, true);
    if (array_key_exists('item_id', $result_add)) {
        $error_code = isset($result_add['error']) ? $result_add['error'] : NULL;
        $error_msg = isset($result_add['warning']) ? $result_add['warning'] : NULL;
        updateProductUploadResult($db, $db_prefix, $product['id'], $shopee_id, $result_add['item_id'], STATUS_PRODUCT_UPLOAD_SUCCESS, $error_code, $error_msg);
        if ($flag_update_2_tier_variations) {
            if (isset($product['variation']) && isset($product['tier_variation']) && !empty($product['variation']) && !empty($product['tier_variation'])) {
                $variations_data = [
                    'item_id' => $result_add['item_id'],
                    'tier_variation' => $product['tier_variation'],
                    'variation' => $product['variation']
                ];
                $reslut_variations = $shopee->uploadInitTierVariation($variations_data);
                $reslut_variations = json_decode($reslut_variations, true);
                /* TODO remove later
                 * updateVariationId($db, $db_prefix, $product['variation'], $reslut_variations);*/
            }
        }
    } else {
        $error_code = isset($result_add['error']) ? $result_add['error'] : NULL;
        $error_msg = isset($result_add['msg']) ? $result_add['msg'] : NULL;
        updateProductUploadResult($db, $db_prefix, $product['id'], $shopee_id, NULL, STATUS_PRODUCT_UPLOAD_FAILED, $error_code, $error_msg);
    }
}

function updateProduct($db, $db_prefix, $shopee_id, $shopee, $product)
{
    // flow update tier variation list : https://open.shopee.com/documents?module=63&type=2&id=54
    $flag_update_2_tier_variations = false;
    if (isset($product['type_tier']) && $product['type_tier'] > 0) {
        $flag_update_2_tier_variations = true;
    }
    $product['item_id'] = $product['update_item_id'];
    $images = $product['images'];
    unset($product['images']);

    /*
     * Cập nhật lại SKU của những biến thể(variation) cũ
     *  update old variation sku*/
    $variations_update = updateOldVariationSKU($product['variation']);
    if (!empty($variations_update)) {
        $product['variations'] = $variations_update;
    }

    // update product
    // $product['days_to_ship'] = 1; // ???


    /*
     * Cập nhật lại thông tin sản phẩm
     */
    $result_update = $shopee->updateProduct($product);
    $result_update = json_decode($result_update, true);
    if (array_key_exists('item_id', $result_update)) {
        $error_code = isset($result_update['error']) ? $result_update['error'] : NULL;
        $error_msg = isset($result_update['warning']) ? $result_update['warning'] : NULL;
        updateProductUploadResult($db, $db_prefix, $product['id'], $shopee_id, $product['item_id'], STATUS_PRODUCT_UPLOAD_SUCCESS, $error_code, $error_msg);

        //
        if (isset($product['delete_variation_list'])){
            deleteOldVariation($shopee, $product['item_id'], $product['delete_variation_list']);
        }

        //
        if ($flag_update_2_tier_variations) {
            if ($product['variation_type'] == 'init') {
                /*
                 * Khởi tạo variation
                 * */
                if (isset($product['variation']) && isset($product['tier_variation']) && !empty($product['variation']) && !empty($product['tier_variation'])) {
                    $variations_data = [
                        'item_id' => $product['item_id'],
                        'tier_variation' => $product['tier_variation'],
                        'variation' => $product['variation']
                    ];
                    $reslut_variations = $shopee->uploadInitTierVariation($variations_data);
                    $reslut_variations = json_decode($reslut_variations, true);
                    $error_msg = updateResultMsg($db, $db_prefix, $reslut_variations, $product['item_id'], $error_msg);

                    /* TODO REMOVE later
                     * updateVariationId($db, $db_prefix, $product['variation'], $reslut_variations);*/
                }
            } else if ($product['variation_type'] == 'update') {

                /*
                 * Cập nhật lại danh sách variation
                 *  update variation list*/
                $variations_data = [
                    'item_id' => $product['item_id'],
                    'tier_variation' => $product['tier_variation']
                ];
                $result_update_tier = $shopee->updateTierVariations($variations_data);
                if (isset($product['variation'])) {
                    /*
                     * Thêm mới variation
                     *  add new variation*/
                    addNewVariation($shopee, $product['item_id'], $product['variation']);
                    /*
                     * Cập nhật lại stock, price cho variation cũ
                     *  update old variation*/
                    updateVariation($shopee, $product['item_id'], $product['variation']);
                }
            }
        } else {
            // update price, stock for product single version
            if (isset($product['price']) && isset($product['stock'])) {
                updatePriceAndStock($shopee, $product['item_id'], $product['price'], $product['stock']);
            }
        }
    } else {
        $error_code = isset($result_update['error']) ? $result_update['error'] : NULL;
        $error_msg = isset($result_update['msg']) ? $result_update['msg'] : NULL;
        updateProductUploadResult($db, $db_prefix, $product['id'], $shopee_id, $product['item_id'], STATUS_PRODUCT_UPLOAD_FAILED, $error_code, $error_msg);
    }


    // update images
    $images = array_map(function ($imag) {
        return $imag['url'];
    }, $images);
    $result_update_images = $shopee->updateImages(['item_id' => $product['item_id'], 'images' => $images]);
    $result_update_images = json_decode($result_update_images, true);
    $error_msg = updateResultMsg($db, $db_prefix, $result_update_images, $product['item_id'], $error_msg);
}

function addNewVariation($shopee, $item_id, $variation)
{
    if (!is_array($variation) || empty($variation)) {
        return;
    }

    $new_variation = [];
    foreach ($variation as $item){
        if (!isset($item['tier_index']) || !isset($item['stock']) || !isset($item['price'])) {
            continue;
        }
        if (isset($item['type']) && $item['type'] == 'add') {
            $new_variation[] = [
                'tier_index' => $item['tier_index'],
                'stock' => $item['stock'],
                'price' => $item['price'],
                'variation_sku' => $item['variation_sku']
            ];
        }
    }

    if (count($new_variation) > 0){
        $shopee->addTierVariation([
            'item_id' => $item_id,
            'variation' => $new_variation
        ]);
    }
}

function updateVariation($shopee, $item_id, $variation)
{
    if (!is_array($variation) || empty($variation)) {
        return;
    }

    $variation_update_price = [];
    $variation_update_stock = [];
    foreach ($variation as $item){
        if (!isset($item['variation_id']) || !isset($item['stock']) || !isset($item['price'])) {
            continue;
        }
        if (isset($item['type']) && $item['type'] == 'update') {
            $variation_update_price[] = [
                'variation_id' => $item['variation_id'],
                'price' => $item['price'],
                'item_id' => $item_id
            ];
            $variation_update_stock[] = [
                'variation_id' => $item['variation_id'],
                'stock' => $item['stock'],
                'item_id' => $item_id
            ];
        }
    }

    if (count($variation_update_price) > 0){
        $shopee->updateVariationPriceBatch([
            'variations' => $variation_update_price
        ]);
        $shopee->updateVariationStockBatch([
            'variations' => $variation_update_stock
        ]);
    }
}

function updateOldVariationSKU($variation)
{
    if (!is_array($variation) || empty($variation)) {
        return [];
    }

    $variation_update = [];
    foreach ($variation as $item) {
        if (!isset($item['variation_id']) || !isset($item['variation_sku']) || trim($item['variation_sku']) == '') {
            continue;
        }

        $variation_update[] = [
            'variation_id' => $item['variation_id'],
            'variation_sku' => $item['variation_sku']
        ];
    }

    return $variation_update;
}

function updatePriceAndStock($shopee, $item_id, $price, $stock)
{
    // update price
    $price_data = [
        'item_id' => $item_id,
        'price' => $price
    ];
    $shopee->updateProductPrice($price_data);

    // update $stock
    $stock_data = [
        'item_id' => $item_id,
        'stock' => $stock
    ];

    $shopee->updateProductStock($stock_data);

    return;
}

function getProductsUpload(DB $db, $db_prefix, $shopee, $shopee_id, $product_ids, $shop_logistic)
{
    if (!is_array($product_ids) || empty($product_ids)) {
        return [];
    }
    $result = [];
    $products = $db->query("SELECT * FROM `{$db_prefix}shopee_upload_product` WHERE `shopee_upload_product_id` IN (" . implode(',', $product_ids) . ")");
    foreach ($products->rows as $product) {
        if (!isset($product['shopee_upload_product_id'])) {
            continue;
        }
        $item = [];
        $item['id'] = $product['shopee_upload_product_id'];
        $item['name'] = $product['name'];
        $item['description'] = $product['description'];
        $item['category_id'] = (int)$product['category_id'];
        $item['price'] = (float)$product['price'];
        $item['stock'] = (int)$product['stock'];
        $item['item_sku'] = $product['sku'];
        if(!$item['item_sku']){
            unset($item['item_sku']);
        }
        $item['weight'] = round((float)$product['weight'] / 1000, 2);
        $item['package_length'] = (int)$product['length'];
        $item['package_width'] = (int)$product['width'];
        $item['package_height'] = (int)$product['height'];
        $item['logistics'] = getLogisticByProductId($db, $db_prefix, $product['shopee_upload_product_id'], $shop_logistic);
        $images = getImagesByProductId($db, $db_prefix, $product['shopee_upload_product_id']);
        if (isset($product['image']) && $product['image']){
            $item['images'] = array_merge([['url' => $product['image']]], $images);
        }else {
            $item['images'] = $images;
        }
        $item['attributes'] = getAttributesByProductId($db, $db_prefix, $product['shopee_upload_product_id']);
        if(empty($item['attributes'])){
            unset($item['attributes']);
        }
        $item['update_item_id'] = getUploadItemIdByProductId($db, $db_prefix, $product['shopee_upload_product_id'], $shopee_id);
        if (!$item['update_item_id']){
            $item['upload_type'] = 'add';
            if (isset($product['bestme_product_id']) && $product['bestme_product_id']) {
                $link_status = checkProductLink($db, $db_prefix, $product['bestme_product_id'], $shopee_id);
                switch ($link_status){
                    case 0: break;
                    case -1: $item['upload_type'] = 'none'; break; // khong tạo mới, cập nhật, trả về trạng thái liên kết không phù hợp
                    default: $item['upload_type'] = 'update'; $item['update_item_id'] = $link_status; break;
                }
            }
        }else{
            $item['upload_type'] = 'update';
        }

        $old_variation = null;
        $delete_variation_list = [];
        if ($item['upload_type'] == 'update') {
            $old_variation = $shopee->GetVariations(['item_id' => $item['update_item_id']]);
            $old_variation = json_decode($old_variation, true);
            if (isset($old_variation['variations']) && !empty($old_variation['variations'])){
                $delete_variation_list = array_map(function ($vari) {
                    return isset($vari['variation_id']) ? $vari['variation_id'] : 0;
                }, $old_variation['variations']);
            }
            /* NOT USE, REMOVE LATER
             * $old_map_name_and_variation_id = [];
            if (isset($old_variation['tier_variation'])) {
                $old_map_name_and_variation_id = mapNameVariationId($old_variation['tier_variation'], $old_variation['variations']);
            }*/
            // TODO [add, delete,'none'], ['delete'], type:init or update
        }

        $item['variation_type'] = 'init';
        $version_attribute = json_decode($product['version_attribute'], true);
        if (!empty($version_attribute)){
            if (count($version_attribute) < 2){
                if (isset($old_variation['tier_variation']) && !empty($old_variation['tier_variation'])){
                    if (count($old_variation['tier_variation']) < 2){
                        $item['variation_type'] = 'update';
                    }
                }
                $item['type_tier'] = 1;
                $item['tier_variation'] = getTierVariation($version_attribute);
                $result1TierVariations = get1TierVariations($db, $db_prefix, $product['shopee_upload_product_id'], $version_attribute, $old_variation);
                $item['variation'] = isset($result1TierVariations['variations']) ? $result1TierVariations['variations'] : [];
                $old_variation_id_update_list = isset($result1TierVariations['old_variation_id_update_list']) ? $result1TierVariations['old_variation_id_update_list'] : [];

            } else {
                if (isset($old_variation['tier_variation']) && !empty($old_variation['tier_variation'])){
                    if (count($old_variation['tier_variation']) >= 2){
                        $item['variation_type'] = 'update';
                    }
                }
                $item['type_tier'] = 2;
                $item['tier_variation'] = getTierVariation($version_attribute);
                $result2TierVariations = get2Variation($db, $db_prefix, $product['shopee_upload_product_id'], $version_attribute, $old_variation);
                $item['variation'] = isset($result2TierVariations['variations']) ? $result2TierVariations['variations'] : [];
                $old_variation_id_update_list = isset($result2TierVariations['old_variation_id_update_list']) ? $result2TierVariations['old_variation_id_update_list'] : [];
            }

            foreach ($delete_variation_list as $k =>$delete_variation) {
                if (in_array($delete_variation, $old_variation_id_update_list)){
                    unset($delete_variation_list[$k]);
                }
            }

            if ($item['price'] == 0){
                $item['price'] = isset($item['variation'][0]['price']) ? $item['variation'][0]['price'] : 10000000;
            }
            if ($item['stock'] == 0){
                $item['stock'] = isset($item['variation'][0]['stock']) ? $item['variation'][0]['stock'] : 1;
            }
        }
        $item['delete_variation_list'] = $delete_variation_list;

        $result[] = $item;
    }

    return $result;
}

function updateResultMsg(DB $db, $db_prefix, $result_update_images, $item_id, $error_msg)
{
    if (!is_array($result_update_images) || !isset($result_update_images['msg']) || $result_update_images['msg'] == ''){
        return '';
    }
    if (isset($result_update_images['msg']) && trim($result_update_images['msg']) == 'No image updated.'){
        return '';
    }
    if ($error_msg){
        $err_msg = $error_msg . ', ' . $result_update_images['msg'];
    }else{
        $err_msg = $result_update_images['msg'];
    }
    $db->query("UPDATE `{$db_prefix}shopee_upload_product_shop_update_result` SET `error_msg` = '" . $db->escape($err_msg) . "' 
                          WHERE `shopee_item_id` = " . (int)$item_id);

    return $err_msg;
}

function getLogisticByProductId(DB $db, $db_prefix, $product_id, $shop_logistic)
{
    $query = $db->query("SELECT logistic.logistic_id as logistic_id, pl.enabled as enabled
                    FROM {$db_prefix}shopee_logistics logistic
                    LEFT JOIN (
                        SELECT *
                        FROM {$db_prefix}shopee_upload_product_logistic 
                        WHERE product_id = " . (int)$product_id . "
                    ) pl ON logistic.logistic_id = pl.logistic_id");

    foreach ($query->rows as $row) {
        if (!isset($row['logistic_id'])) {
            continue;
        }
        $enab = isset($row['enabled']) && $row['enabled'] == 1 ? TRUE : FALSE;
        if (!isset($shop_logistic[(int)$row['logistic_id']]) || !$shop_logistic[(int)$row['logistic_id']]) {
            $enab = FALSE;
        }
        $result[] = [
            'logistic_id' => (int)$row['logistic_id'],
            'enabled' => $enab
        ];
    }

    return $result;
}

function getImagesByProductId(DB $db, $db_prefix, $product_id)
{
    $query = $db->query("SELECT `image` as `url` FROM `{$db_prefix}shopee_upload_product_image` WHERE `product_id` = " . (int)$product_id);

    return $query->rows;
}

function getAttributesByProductId(DB $db, $db_prefix, $product_id)
{
    $query = $db->query("SELECT `attribute_id`, `value` FROM `{$db_prefix}shopee_upload_product_attribute` WHERE `product_id` = " . (int)$product_id);

    $result = [];
    foreach ($query->rows as $row){
        if (!isset($row['attribute_id']) || !isset($row['value']) || trim($row['value']) == '' || trim($row['attribute_id']) == ''){
            continue;
        }
        $result[] = [
            'attributes_id' => (int)$row['attribute_id'],
            'value' => $row['value']
        ];
    }

    return $result;
}

function getUploadItemIdByProductId(DB $db, $db_prefix, $product_id, $shopee_id)
{
    $query = $db->query("SELECT `shopee_item_id` FROM `{$db_prefix}shopee_upload_product_shop_update_result` WHERE `shopee_upload_product_id` = " . (int)$product_id . " AND `shopee_shop_id` = " . (int)$shopee_id);

    if (isset($query->row['shopee_item_id']) && $query->row['shopee_item_id']) {
        return (int)$query->row['shopee_item_id'];
    }

    return NULL;
}

function checkProductLink(DB $db, $db_prefix, $product_id, $shopee_id)
{
    // 0: chưa liên kết, -1: liên kết ko hợp lệ, item_id(vd: 11111) : item_id trên shopee
    $shopee_product = $db->query("SELECT DISTINCT `item_id` FROM `{$db_prefix}shopee_product` 
                                                         WHERE `bestme_id` = " . (int)$product_id . " AND `shopid` = '" . $db->escape($shopee_id) . "'");

    $shopee_product_version = $db->query("SELECT DISTINCT spv.`item_id` FROM `{$db_prefix}shopee_product_version` spv 
                                                                LEFT JOIN {$db_prefix}shopee_product sp ON (sp.`item_id` = spv.`item_id`) 
                                                                WHERE spv.`bestme_product_id` = " . (int)$product_id . " AND sp.`shopid` = '" . $db->escape($shopee_id) . "'");

    if (empty($shopee_product->rows) && empty($shopee_product_version->rows)) {
        return 0;
    }

    if (empty($shopee_product_version->rows)) {
        return isset($shopee_product->row['item_id']) ? (int)$shopee_product->row['item_id'] : 0;
    }

    if ($shopee_product_version->num_rows > 1) {
        return -1;
    }

    return isset($shopee_product_version->row['item_id']) ? (int)$shopee_product_version->row['item_id'] : 0;
}

function get1TierVariations(DB $db, $db_prefix, $product_id, $version_attribute, $old_variation)
{
    if (!is_array($version_attribute) || empty($version_attribute)) {
        return [];
    }
    $type = 'none';
    if (!is_array($old_variation) || !isset($old_variation['variations']) || !is_array($old_variation['variations']) ||
        !isset($old_variation['tier_variation']) || !is_array($old_variation['tier_variation']) || count($old_variation['tier_variation']) != 1) {
        $type = 'add';
    }
    $old_variation_id_update_list = [];

    $tier1 = array_shift($version_attribute)['values'];
    $variations = [];
    foreach ($tier1 as $k1 => $v1) {
        $version_name1 = trim($v1);
        $version_info_raw = $db->query("SELECT * FROM {$db_prefix}shopee_upload_product_version 
                                          WHERE `shopee_upload_product_id` = " . (int)$product_id . " AND 
                                                (`version_name` = '" . $db->escape($version_name1) . "' OR `version_name` = '" . $db->escape($version_name1) . "')");
        $version_info = $version_info_raw->row;
        $tier_index = [$k1];
        $item = array(
            'tier_index' => $tier_index,
            'stock' => isset($version_info['version_stock']) ? (int)$version_info['version_stock'] : 0,
            'price' => isset($version_info['version_price']) ? (float)$version_info['version_price'] : 0,
            'version_id' => isset($version_info['shopee_upload_product_version_id']) ? (int)$version_info['shopee_upload_product_version_id'] : 0
        );
        if (trim($version_info['version_sku'])) {
            $item['variation_sku'] = $version_info['version_sku'];
        }

        // check variation is add or update
        $item['type'] = 'add';
        if ($type == 'add') {
            $item['type'] = 'add';
        } else {
            foreach ($old_variation['variations'] as $o_variation) {
                if (isset($o_variation['variation_id']) && isset($o_variation['tier_index'])) {
                    if ($o_variation['tier_index'] === $tier_index) {
                        $item['type'] = 'update';
                        $item['variation_id'] = $o_variation['variation_id'];
                        $old_variation_id_update_list[] = $o_variation['variation_id'];
                        break;
                    }
                }
            }
        }

        $variations[] = $item;
    }

    return ['variations' => $variations, 'old_variation_id_update_list' => $old_variation_id_update_list];
}

function mapNameVariationId($tier_variation, $variations)
{
    if (!is_array($variations) || empty($variations) || !is_array($tier_variation) || empty($tier_variation)) {
        return [];
    }
    $result = [];
    foreach ($variations as $variation) {
        if (!isset($variation['variation_id'])) {
            continue;
        }
        $item = [
            'variation_id' => $variation['variation_id']
        ];
        if (isset($variation['tier_index']) && is_array($variation['tier_index']) && !empty($variation['tier_index'])) {
            $name = [];
            foreach ($variation['tier_index'] as $k => $index) {
                if (isset($tier_variation[$k]['options'][$index])){
                    $name[] = trim($tier_variation[$k]['options'][$index]);
                }
            }
            $name = implode(',', $name);

            $item['name'] = $name;
        }

        $result[] = $item;
    }

    return $result;
}

function getTierVariation($version_attribute)
{
    if (!is_array($version_attribute) || empty($version_attribute)) {
        return [];
    }

    $result = [];

    foreach ($version_attribute as $attribute) {
        $result[] = array(
            'name' => trim($attribute['name']),
            'options' => $attribute['values']
        );
    }

    return $result;
}

function getVariationList(DB $db, $db_prefix, $item_id)
{
    $query = $db->query("SELECT `variation_result` FROM `{$db_prefix}shopee_upload_product_shop_update_result` WHERE `shopee_item_id` = " . (int)$item_id . " AND `variation_result` IS NOT NULL");

    $result = [];
    if (isset($query->row['variation_result'])) {
        $variations = $query->row['variation_result'];
        $variations = json_decode($variations, true);
        if (is_array($variations)){
            foreach ($variations as $variation){
                if (!isset($variation['variation_id'])){
                    continue;
                }
                $result[] = $variation['variation_id'];
            }
        }
    }

    return $result;
}

function getVariationListFromOldLink(DB $db, $db_prefix, $item_id)
{
    $query = $db->query("SELECT `variation_id` FROM `{$db_prefix}shopee_product_version` WHERE `item_id` = " . (int)$item_id);

    $result = [];
    foreach ($query->rows as $row) {
        if (isset($row['variation_id'])) {
            if (!$row['variation_id'] || in_array($row['variation_id'], $result)) {
                continue;
            }
            $result[] = $row['variation_id'];
        }
    }

    return $result;
}

function get2Variation(DB $db, $db_prefix, $product_id, $version_attribute, $old_variation)
{
    if (!is_array($version_attribute) || empty($version_attribute)) {
        return [];
    }
    $type = 'none';
    if (!is_array($old_variation) || !isset($old_variation['variations']) || !is_array($old_variation['variations']) ||
        !isset($old_variation['tier_variation']) || !is_array($old_variation['tier_variation']) || count($old_variation['tier_variation']) != 2) {
        $type = 'add';
    }
    $old_variation_id_update_list = [];

    $tier1 = array_shift($version_attribute)['values'];
    $tier2 = array_shift($version_attribute)['values'];
    $variations = [];
    foreach ($tier1 as $k1 => $v1) {
        foreach ($tier2 as $k2 => $v2) {
            $version_name1 = trim($v1) . ',' . trim($v2);
            $version_name2 = trim($v2) . ',' . trim($v1);
            $version_info_raw = $db->query("SELECT * FROM {$db_prefix}shopee_upload_product_version 
                                              WHERE `shopee_upload_product_id` = " . (int)$product_id . " AND 
                                                    (`version_name` = '" . $db->escape($version_name1) . "' OR `version_name` = '" . $db->escape($version_name2) . "')");
            $version_info = $version_info_raw->row;
            if (is_array($version_info) && isset($version_info['shopee_upload_product_version_id'])){
                $tier_index = [$k1, $k2];
                $item = array(
                    'tier_index' => $tier_index,
                    'stock' => isset($version_info['version_stock']) ? (int)$version_info['version_stock'] : 0,
                    'price' => isset($version_info['version_price']) ? (float)$version_info['version_price'] : 0,
                    'version_id' => isset($version_info['shopee_upload_product_version_id']) ? (int)$version_info['shopee_upload_product_version_id'] : 0
                );
                if (isset($version_info['version_sku']) && trim($version_info['version_sku'])){
                    $item['variation_sku'] = $version_info['version_sku'];
                }

                // check variation is add or update
                $item['type'] = 'add';
                if ($type == 'add') {
                    $item['type'] = 'add';
                } else {
                    foreach ($old_variation['variations'] as $o_variation) {
                        if (isset($o_variation['variation_id']) && isset($o_variation['tier_index'])) {
                            if ($o_variation['tier_index'] === $tier_index) {
                                $item['type'] = 'update';
                                $item['variation_id'] = $o_variation['variation_id'];
                                $old_variation_id_update_list[] = $o_variation['variation_id'];
                                break;
                            }
                        }
                    }
                }

                $variations[] = $item;
            }
        }
    }

    return ['variations' => $variations, 'old_variation_id_update_list' => $old_variation_id_update_list];
}

function updateVariationId(DB $db, $db_prefix, $tier_variations, $reslut_variations)
{
    if (isset($reslut_variations['item_id']) && isset($reslut_variations['variation_id_list']) && is_array($reslut_variations['variation_id_list'])) {
        // update variation result
        $variation_result = json_encode($reslut_variations['variation_id_list']);
        $db->query("UPDATE `{$db_prefix}shopee_upload_product_shop_update_result` SET `variation_result` = '" . $db->escape($variation_result) . "' 
                                    WHERE `shopee_item_id` = " . (int)$reslut_variations['item_id']);

        // update variation_id for product version
        if (is_array($tier_variations)){
            foreach ($reslut_variations['variation_id_list'] as $item) {
                if (!isset($item['tier_index'])){
                    continue;
                }
                foreach ($tier_variations as $vari) {
                    if (!isset($vari['tier_index'])){
                        continue;
                    }
                    if ($item['tier_index'] === $vari['tier_index']){
                        $variation_id = $item['variation_id'];
                        $version_id = $vari['version_id'];
                        $db->query("UPDATE `{$db_prefix}shopee_upload_product_version` SET `variation_id` = " . (int)$variation_id . " WHERE `shopee_upload_product_version_id` = " . (int)$version_id);
                    }
                }
            }
        }
    }
}

function deleteOldVariation($shopee, $item_id, $variation_list)
{
    if (!is_array($variation_list) || empty($variation_list)){
        return;
    }

    foreach ($variation_list as $variation_id){
        $data = [
            'item_id' => $item_id,
            'variation_id' => (int)$variation_id
        ];
        $shopee->deleteVariation($data);
    }

    return;
}

function getLogisticByShop($shopee)
{
    $logistics = $shopee->getLogistics(['language' => 'en']);
    $logistics = json_decode($logistics, true);
    $result = [];
    if (isset($logistics['logistics']) && is_array($logistics['logistics'])){
        foreach ($logistics['logistics'] as $logistic){
            if (!isset($logistic['logistic_id'])){
                continue;
            }
            $result[$logistic['logistic_id']] = $logistic['enabled'];
        }
    }

    return $result;
}

function updateProductUploadResult(DB $db, $db_prefix, $product_id, $shopee_id, $item_id, $status, $error_code = NULL, $error_msg = NULL)
{
    $query = $db->query("SELECT `id` FROM `{$db_prefix}shopee_upload_product_shop_update_result` WHERE `shopee_upload_product_id` = " . (int)$product_id . " AND `shopee_shop_id` = " . (int)$shopee_id);
    if ($query->num_rows > 0 && isset($query->row['id'])) {
        $db->query("UPDATE `{$db_prefix}shopee_upload_product_shop_update_result` SET 
                                            `shopee_item_id` = '" . $db->escape($item_id) . "', 
                                            `status` = " . (int)$status . ", 
                                            `in_process` = 0, 
                                            `error_code` = '" . $db->escape($error_code) . "', 
                                            `error_msg` = '" . $db->escape($error_msg) . "' 
                                            WHERE `id` = " . (int)$query->row['id']);
    } else {
        $db->query("INSERT INTO `{$db_prefix}shopee_upload_product_shop_update_result` SET 
                                            `shopee_upload_product_id` = " . (int)$product_id . ", 
                                            `shopee_shop_id` = " . (int)$shopee_id . ", 
                                            `shopee_item_id` = '" . $db->escape($item_id) . "', 
                                            `status` = " . (int)$status . ", 
                                            `in_process` = 0, 
                                            `error_code` = '" . $db->escape($error_code) . "', 
                                            `error_msg` = '" . $db->escape($error_msg) . "'");
    }

    // delete data with shopee_id is null
    $db->query("DELETE FROM `{$db_prefix}shopee_upload_product_shop_update_result` WHERE `shopee_upload_product_id` = " . (int)$product_id . " AND `shopee_shop_id` is NULL");

    // update time update product
    $db->query("UPDATE `{$db_prefix}shopee_upload_product` SET `date_modify` = NOW() WHERE `shopee_upload_product_id` = " . (int)$product_id);
}

function updateAllProductNotInProcess(DB $db, $db_prefix, $product_ids, $shopee_id)
{
    if (!$shopee_id || !is_array($product_ids) || empty($product_ids)) {
        return;
    }
    $db->query("UPDATE `{$db_prefix}shopee_upload_product_shop_update_result` SET `in_process` = 0 
                        WHERE `shopee_shop_id` = " . (int)$shopee_id . " AND `shopee_upload_product_id` IN (" . implode(',', $product_ids) . ")");
}

function updateProcessingStatusTotal($shop_name, $shopee_id, $total)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'count_product_upload_shopee');
    $redis->set($key, $total, SHOPEE_PROCESS_INFO_TIMEOUT);
}

function updateProcessingStatusUploaded($shop_name, $shoppe_id, $saved)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shoppe_id, 'count_product_shopee_uploaded');
    $redis->set($key, $saved, SHOPEE_PROCESS_INFO_TIMEOUT);
}

function updateProcessingStatusRunning($shop_name, $shopee_id, $running = true)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'load_shopee_upload_product_running');
    $redis->set($key, ((bool)$running) ? 1 : 0, SHOPEE_PROCESS_INFO_TIMEOUT);
}

function updateProcessingStatusRealRunning($shop_name, $shopee_id, $real_running = true)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'load_shopee_upload_product_real_running');
    $redis->set($key, ((bool)$real_running) ? 1 : 0, SHOPEE_PROCESS_INFO_TIMEOUT);
}

function isProcessingStatusRealRunning($shop_name, $shoppe_id)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shoppe_id, 'load_shopee_upload_product_real_running');
    $real_running = $redis->get($key);

    return $real_running == 1;
}