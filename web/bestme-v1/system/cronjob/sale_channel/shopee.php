<?php

require_once __DIR__. './../cronjob_init.php';

const CJ_POLLING_INTERVAL = 2; // in seconds
const CJ_REDIS_TIMEOUT = 30; // in seconds

// redis job key
const CJ_JOB_KEY_DB_HOSTNAME = 'db_hostname';
const CJ_JOB_KEY_DB_PORT = 'db_port';
const CJ_JOB_KEY_DB_USERNAME = 'db_username';
const CJ_JOB_KEY_DB_PASSWORD = 'db_password';
const CJ_JOB_KEY_DB_DATABASE = 'db_database';
const CJ_JOB_KEY_DB_PREFIX = 'db_prefix';
const CJ_JOB_KEY_DB_DRIVER = 'db_driver';
const CJ_JOB_KEY_SHOP_NAME = 'shop_name';
const CJ_JOB_KEY_SHOPEE_ID = 'shopee_id';
const CJ_JOB_KEY_TASK = 'task';
// all tasks
const CJ_JOB_TASK_GET_PRODUCTS = 'GET_PRODUCTS';

// global vars
global $redis;

$processing = true;

/* handle kill signal */
// TODO...

/* connect to Redis host */
//println(sprintf("Connecting to Redis host %s at port %s...", MUL_REDIS_HOST, MUL_REDIS_PORT));
$redis = new Redis();
try {
    $redis->connect(MUL_REDIS_HOST, MUL_REDIS_PORT, CJ_REDIS_TIMEOUT);
    $redis->select(MUL_REDIS_DB_SHOPEE);
} catch (Exception $e) {
    println(sprintf("Connecting to Redis host %s at port %s... failed! Error: %s", MUL_REDIS_HOST, MUL_REDIS_PORT, $e->getMessage()));
    return;
}
//println(sprintf("Connecting to Redis host %s at port %s... connected!", MUL_REDIS_HOST, MUL_REDIS_PORT));

/* get job from Redis - forever until killed */
while ($processing) {
    $job_raw = $redis->rPop(SHOPEE_JOB_REDIS_QUEUE);
    //println('SHOPEE_JOB_REDIS_QUEUE: Got raw job: ' . $job_raw);

    $job = json_decode($job_raw, true);

    // debug
    /*$job = [
        CJ_JOB_KEY_DB_HOSTNAME => DB_HOSTNAME,
        CJ_JOB_KEY_DB_PORT => DB_PORT,
        CJ_JOB_KEY_DB_USERNAME => DB_USERNAME,
        CJ_JOB_KEY_DB_PASSWORD => DB_PASSWORD,
        CJ_JOB_KEY_DB_DATABASE => DB_DATABASE,
        CJ_JOB_KEY_DB_PREFIX => DB_PREFIX,
        CJ_JOB_KEY_DB_DRIVER => DB_DRIVER,
        CJ_JOB_KEY_SHOP_NAME => '',
        CJ_JOB_KEY_SHOPEE_ID => '',
        CJ_JOB_KEY_TASK => CJ_JOB_TASK_GET_PRODUCTS
    ];*/

    if (!is_array($job)) {
        // break for new running if no job!
        //println('No Shopee job found! Exit.');
        $processing = false;
        break;
    }

    try {
        //println('Found new Shopee job! Now run.');
        handleJob($job);
    } catch (Exception $e) {
        println(sprintf('Exception while handling job "%s". Error: %s', $job_raw, $e->getMessage()));
    }

    sleep(CJ_POLLING_INTERVAL);
}

// TODO: need close?...
$redis->close();

/**
 * handle Job
 *
 * @param array $job
 * @return bool
 */
function handleJob(array $job)
{
    if (!isset($job[CJ_JOB_KEY_DB_HOSTNAME]) ||
        !isset($job[CJ_JOB_KEY_DB_PORT]) ||
        !isset($job[CJ_JOB_KEY_DB_USERNAME]) ||
        !isset($job[CJ_JOB_KEY_DB_PASSWORD]) ||
        !isset($job[CJ_JOB_KEY_DB_DATABASE]) ||
        !isset($job[CJ_JOB_KEY_DB_PREFIX]) ||
        !isset($job[CJ_JOB_KEY_DB_DRIVER]) ||
        !isset($job[CJ_JOB_KEY_SHOP_NAME]) ||
        !isset($job[CJ_JOB_KEY_SHOPEE_ID]) ||
        !isset($job[CJ_JOB_KEY_TASK])
    ) {
        println('Missing either hostname or port or username or password or database or prefix or shop_name or shopee_id or task');
        return false;
    }

    /* Read info from job */
    $db_hostname = $job[CJ_JOB_KEY_DB_HOSTNAME];
    $db_port = $job[CJ_JOB_KEY_DB_PORT];
    $db_username = $job[CJ_JOB_KEY_DB_USERNAME];
    $db_password = $job[CJ_JOB_KEY_DB_PASSWORD];
    $db_database = $job[CJ_JOB_KEY_DB_DATABASE];
    $db_prefix = $job[CJ_JOB_KEY_DB_PREFIX];
    $db_driver = $job[CJ_JOB_KEY_DB_DRIVER];
    $shop_name = $job[CJ_JOB_KEY_SHOP_NAME];
    $shopee_id = $job[CJ_JOB_KEY_SHOPEE_ID];
    $task = $job[CJ_JOB_KEY_TASK];

    /* handle task */
    println(sprintf('Handling task "%s (data: %s)"...', $task, json_encode($job)));
    switch ($task) {
        case CJ_JOB_TASK_GET_PRODUCTS:
            // check if real running
            if (isProcessingStatusRealRunning($shop_name, $shopee_id)) {
                println(sprintf('Handling task "%s"... already running, skip!', $task));
                break;
            }

            // update real running
            updateProcessingStatusRealRunning($shop_name, $shopee_id);

            // do getting products
            doGetProducts($db_hostname, $db_port, $db_username, $db_password, $db_database, $db_prefix, $db_driver, $shop_name, $shopee_id);

            break;

        // and more case...
    }
    println(sprintf('Handling task "%s"... done!', $task));

    return true;
}

/**
 * do Get Products
 *
 * @param string $db_hostname
 * @param string $db_port
 * @param string $db_username
 * @param string $db_password
 * @param string $db_database
 * @param string $db_prefix
 * @param string $db_driver
 * @param string $shop_name
 * @param string $shopee_id
 */
function doGetProducts($db_hostname, $db_port, $db_username, $db_password, $db_database, $db_prefix, $db_driver, $shop_name, $shopee_id)
{
    println(sprintf('Getting products from shopee id "%s" for shop "%s"...', $shopee_id, $shop_name));

    /* get products from Shopee */
    $shopee = Sale_Channel::getSaleChannel('shopee', ['shop_id' => (int)$shopee_id]);

    $list_products = getListShopeeProducts($shop_name, $shopee_id, $shopee, 100, 0);

    /* save all products from Shopee to db */
    $db = null;
    try {
        $db = new DB($db_driver,
            htmlspecialchars_decode($db_hostname),
            htmlspecialchars_decode($db_username),
            htmlspecialchars_decode($db_password),
            htmlspecialchars_decode($db_database),
            htmlspecialchars_decode($db_port)
        );
    } catch (Exception $e) {
        updateProcessingStatusRunning($shop_name, $shopee_id, false);
        updateProcessingStatusRealRunning($shop_name, $shopee_id, false);
        println(sprintf('Getting products from shopee id "%s" for shop "%s"... failed. Error: %s', $shopee_id, $shop_name, $e->getMessage()));
        return;
    }

    $count_saved = 0;
    $list_item_ids = [];
    foreach ($list_products as $product) {
        if (!array_key_exists('item_id', $product)) {
            continue;
        }
        $list_item_ids[] = $product['item_id'];
        $product_detail = $shopee->getProductDetail(['item_id' => $product['item_id']]);
        $product_detail = json_decode($product_detail, true);
        if (!is_array($product_detail) || !array_key_exists('item', $product_detail)) {
            continue;
        }

        // save to db
        try {
            $data = array();
            $data['item_id'] = $product_detail['item']['item_id'];
            $data['shop_id'] = $product_detail['item']['shopid'];
            $data['sku'] = $product_detail['item']['item_sku'];
            $data['stock'] = $product_detail['item']['stock'];
            $data['status'] = $product_detail['item']['status'];
            $data['name'] = $product_detail['item']['name'];
            $data['description'] = $product_detail['item']['description'];
            if($data['description'] == strip_tags($data['description'])) {
                $data['description'] = nl2br($data['description']);
            }
            $data['images'] = json_encode($product_detail['item']['images']);
            $data['currency'] = $product_detail['item']['currency'];
            $data['price'] = $product_detail['item']['price'];
            $data['original_price'] = $product_detail['item']['original_price'];
            $data['weight'] = (float)$product_detail['item']['weight'] * 1000;  // KG * 1000
            $data['category_id'] = $product_detail['item']['category_id'];
            $data['has_variation'] = $product_detail['item']['has_variation'];
            $data['variations'] = json_encode($product_detail['item']['variations']);
            $data['attributes'] = json_encode($product_detail['item']['attributes']);

            updateProduct($db, $db_prefix, $data);

            if ($data['has_variation']) {
                updateProductVersion($db, $db_prefix, $data['item_id'], $data['weight'], $product_detail['item']['variations']);
            }

        } catch (Exception $e) {
            println(sprintf('Saving shopee product (item id %s) from shopee id "%s" for shop "%s"... failed. Error: %s', $data['item_id'], $shopee_id, $shop_name, $e->getMessage()));
            continue;
        }

        $count_saved++;

        updateProcessingStatusSaved($shop_name, $shopee_id, $count_saved);
    }

    // delete product not exist in shopee
    deleteProductNotInList($list_item_ids, $db, $db_prefix, $shopee_id, $shop_name);

    /* mark run completed */
    updateProcessingStatusRunning($shop_name, $shopee_id, false);
    updateProcessingStatusRealRunning($shop_name, $shopee_id, false);
    updateLastTimeSyncProduct($db, $db_prefix, $shopee_id);

    println(sprintf('Getting products from shopee id "%s" for shop "%s"... done. Total: %s, saved: %s', $shopee_id, $shop_name, count($list_products), $count_saved));
}

/**
 * @param string $shop_name
 * @param string $shoppe_id
 * @param \Sale_Channel\Abstract_Sale_Channel $shopee
 * @param int $limit
 * @param int $offset
 * @param array $list_products
 * @return array
 */
function getListShopeeProducts($shop_name, $shoppe_id, $shopee, $limit, $offset, $list_products = [])
{
    $products = $shopee->getProducts(['pagination_entries_per_page' => $limit, 'pagination_offset' => $offset]);
    $products = json_decode($products, true);
    if (is_array($products) && array_key_exists('items', $products) && is_array($products['items'])) {
        $list_products = array_merge($list_products, $products['items']);

        updateProcessingStatusTotal($shop_name, $shoppe_id, count($list_products));
    }

    if (is_array($products) && array_key_exists('more', $products) && $products['more']) {
        $list_products = getListShopeeProducts($shop_name, $shoppe_id, $shopee, $limit, $offset + $limit, $list_products);
    }

    return $list_products;
}

function updateProcessingStatusTotal($shop_name, $shopee_id, $total)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'count_product_shopee');
    $redis->set($key, $total, SHOPEE_PROCESS_INFO_TIMEOUT);
}

function updateProcessingStatusSaved($shop_name, $shoppe_id, $saved)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shoppe_id, 'count_product_shopee_saved');
    $redis->set($key, $saved, SHOPEE_PROCESS_INFO_TIMEOUT);
}

function updateProcessingStatusRunning($shop_name, $shopee_id, $running = true)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'load_shopee_product_running');
    $redis->set($key, ((bool)$running) ? 1 : 0, SHOPEE_PROCESS_INFO_TIMEOUT);
}

function updateProcessingStatusRealRunning($shop_name, $shopee_id, $real_running = true)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'load_shopee_product_real_running');
    $redis->set($key, ((bool)$real_running) ? 1 : 0, SHOPEE_PROCESS_INFO_TIMEOUT);
}

function isProcessingStatusRealRunning($shop_name, $shoppe_id)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shoppe_id, 'load_shopee_product_real_running');
    $real_running = $redis->get($key);

    return $real_running == 1;
}

function updateProduct(DB $db, $db_prefix, array $data)
{
    if (!array_key_exists('item_id', $data) || !$data['item_id']) {
        return;
    }

    $product = $db->query("SELECT `item_id` FROM `{$db_prefix}shopee_product` WHERE `item_id` = " . (int)$data['item_id']);
    if (isset($product->row['item_id'])) {
        editProduct($db, $db_prefix, $data);
    } else {
        addProduct($db, $db_prefix, $data);
    }
}

function updateProductVersion(DB $db, $db_prefix, $item_id, $weight, array $data)
{
    if (!isset($item_id) || !isset($weight)) {
        return;
    }

    $products_version = $db->query("SELECT COUNT(item_id) as total FROM `{$db_prefix}shopee_product_version` WHERE `item_id` = '" . (int)$item_id . "'");
    if (isset($products_version->row['total']) && $products_version->row['total']) {
        editShopeeProductVersion($db, $db_prefix, $item_id, $weight, $data);
    } else {
        addShopeeProductVersion($db, $db_prefix, $item_id, $weight, $data);
    }

}

function addProduct(DB $db, $db_prefix, array $data)
{
    try {
        $db->query("INSERT INTO `{$db_prefix}shopee_product` 
                        SET `item_id` = " . (int)$data['item_id'] . ", 
                            `shopid` = " . (int)$data['shop_id'] . ", 
                            `sku` = '" . $db->escape($data['sku']) . "', 
                            `stock` = " . (int)$data['stock'] . ", 
                            `status` = '" . $db->escape($data['status']) . "', 
                            `name` = '" . $db->escape($data['name']) . "',
                            `description` = '" . $db->escape($data['description']) . "', 
                            `images` = '" . $db->escape($data['images']) . "',
                            `currency` = '" . $db->escape($data['currency']) . "', 
                            `price` = '" . (float)($data['price']) . "',
                            `original_price` = '" . (float)($data['original_price']) . "', 
                            `weight` = '" . (float)($data['weight']) . "', 
                            `category_id` = " . (int)$data['category_id'] . ", 
                            `has_variation` = " . (int)$data['has_variation'] . ", 
                            `variations` = '" . $db->escape($data['variations']) . "', 
                            `attributes` = '" . $db->escape($data['attributes']) . "', 
                            `created_at` = NOW(), 
                            `updated_at` = NOW()");
    } catch (Exception $e) {
        return isset($data['item_id']) ? $data['item_id'] : '';
    }

    return $db->getLastId();
}

function addShopeeProductVersion(DB $db, $db_prefix, $product_item_id, $p_weight, array $data)
{
    if (!isset($data) || !is_array($data)) {
        return;
    }

    foreach ($data as $version) {
        $item_id = $product_item_id;
        $variation_id = isset($version['variation_id']) ? $version['variation_id'] : '';
        if ($variation_id == '') {
            return;
        }

        $version_name = isset($version['name']) ? $version['name'] : '';
        $reserved_stock = isset($version['reserved_stock']) ? $version['reserved_stock'] : 0;
        $product_weight = (float)$p_weight;
        $original_price = isset($version['original_price']) ? (float)$version['original_price'] : 0;
        $price = isset($version['price']) ? (float)$version['price'] : 0;
        $discount_id = isset($version['discount_id']) ? $version['discount_id'] : 0;
        $create_time = isset($version['create_time']) ? $version['create_time'] : '';
        $update_time = isset($version['update_time']) ? $version['update_time'] : '';
        $sku = isset($version['variation_sku']) ? $version['variation_sku'] : '';
        $stock = isset($version['stock']) ? $version['stock'] : 0;
        $is_set_item = (isset($version['is_set_item']) && $version['is_set_item']) ? 1 : 0;
        $status = isset($version['status']) ? $version['status'] : '';

        $db->query("INSERT INTO `{$db_prefix}shopee_product_version` 
                                         SET `item_id` = '" . $item_id . "', 
                                             `variation_id` = '" . $variation_id . "', 
                                             `version_name` = '" . $version_name . "', 
                                             `reserved_stock` = '" . $reserved_stock . "', 
                                             `product_weight` = '" . $product_weight . "', 
                                             `original_price` = '" . $original_price . "', 
                                             `price` = '" . $price . "', 
                                             `discount_id` = '" . $discount_id . "', 
                                             `create_time` = '" . $create_time . "', 
                                             `update_time` = '" . $update_time . "', 
                                             `sku` = '" . $sku . "', 
                                             `stock` = '" . $stock . "', 
                                             `status` = '" . $status . "', 
                                             `is_set_item` = '" . $is_set_item . "' 
                                    ");
    }
}

function editShopeeProductVersion(DB $db, $db_prefix, $product_item_id, $p_weight, array $data)
{
    if (!isset($data) || !is_array($data)) {
        return;
    }

    $products_version = $db->query("SELECT COUNT(item_id) as total FROM `{$db_prefix}shopee_product_version` WHERE `item_id` = '" . (int)$product_item_id . "'");
    if (isset($products_version->row['total']) && $products_version->row['total'] != count($data)) {
        $db->query("DELETE FROM `{$db_prefix}shopee_product_version` WHERE item_id = '" . $product_item_id . "'");
        addShopeeProductVersion($db, $db_prefix, $product_item_id, $p_weight, $data);
        return;
    }

    foreach ($data as $version) {
        $item_id = $product_item_id;
        $variation_id = isset($version['variation_id']) ? $version['variation_id'] : '';
        if ($variation_id == '') {
            return;
        }

        $version_name = isset($version['name']) ? $version['name'] : '';
        $reserved_stock = isset($version['reserved_stock']) ? $version['reserved_stock'] : 0;
        $product_weight = (float)$p_weight;
        $original_price = isset($version['original_price']) ? (float)$version['original_price'] : 0;
        $price = isset($version['price']) ? (float)$version['price'] : 0;
        $discount_id = isset($version['discount_id']) ? $version['discount_id'] : 0;
        $create_time = isset($version['create_time']) ? $version['create_time'] : '';
        $update_time = isset($version['update_time']) ? $version['update_time'] : '';
        $sku = isset($version['variation_sku']) ? $version['variation_sku'] : '';
        $stock = isset($version['stock']) ? $version['stock'] : 0;
        $is_set_item = (isset($version['is_set_item']) && $version['is_set_item']) ? 1 : 0;
        $status = isset($version['status']) ? $version['status'] : '';

        $db->query("UPDATE `{$db_prefix}shopee_product_version` 
                                     SET `version_name` = '" . $version_name . "', 
                                         `reserved_stock` = '" . $reserved_stock . "', 
                                         `product_weight` = '" . $product_weight . "', 
                                         `original_price` = '" . $original_price . "', 
                                         `price` = '" . $price . "', 
                                         `discount_id` = '" . $discount_id . "', 
                                         `create_time` = '" . $create_time . "', 
                                         `update_time` = '" . $update_time . "', 
                                         `sku` = '" . $sku . "', 
                                         `stock` = '" . $stock . "', 
                                         `status` = '" . $status . "', 
                                         `is_set_item` = '" . $is_set_item . "' 
                                         WHERE item_id = '" . $item_id . "' 
                                         AND variation_id = '" . $variation_id . "' 
                                ");

        $product_shopee = $db->query("SELECT `shopid` 
                                    FROM `{$db_prefix}shopee_product` 
                                      WHERE `item_id` = {$item_id}");

        $shopee_shop_id = isset($product_shopee->row['shopid']) ? $product_shopee->row['shopid'] : '';

        if ($shopee_shop_id == '') {
            return;
        }

        $result = $db->query("SELECT `bestme_product_id`, `bestme_product_version_id` 
                          FROM `{$db_prefix}shopee_product_version` 
                            WHERE `item_id` = {$item_id} 
                              AND `variation_id` = {$variation_id}");

        if (isset($result->row['bestme_product_id']) && $result->row['bestme_product_id']) {
            $product_veersion_id = isset($result->row['bestme_product_version_id']) ? $result->row['bestme_product_version_id'] : 0;
            updateProductHasLinkProductShopee($db, $db_prefix, (int)$shopee_shop_id, $result->row['bestme_product_id'], $product_veersion_id, $stock, $price, $original_price);
        }
    }
}

function editProduct(DB $db, $db_prefix, array $data)
{
    try {
        $product = $db->query("SELECT `has_variation`, `variations` FROM `{$db_prefix}shopee_product` WHERE `item_id` = " . (int)$data['item_id']);

        if (isset($product->row['has_variation']) && ((int)$product->row['has_variation'] != (int)$data['has_variation'])) {
            $db->query("UPDATE `{$db_prefix}shopee_product` 
                              SET `bestme_id` = NULL, 
                                  `bestme_product_version_id` = NULL, 
                                  `sync_status` = 0 
                              WHERE `item_id` = " . (int)$data['item_id']);

            // remove if product from multi version to single version
            if ($product->row['has_variation']) {
                $db->query("DELETE FROM `{$db_prefix}shopee_product_version` WHERE `item_id` = '" . (int)$data['item_id'] ."'");
            }
        } else {
            if (isset($product->row['variations']) &&
                is_array(json_decode($product->row['variations'])) &&
                is_array(json_decode($data['variations'])) &&
                (count(json_decode($product->row['variations'])) != count(json_decode($data['variations'])))
            ) {
                $db->query("UPDATE `{$db_prefix}shopee_product` 
                                  SET `bestme_id` = NULL, 
                                      `bestme_product_version_id` = NULL, 
                                      `sync_status` = 0 
                                  WHERE `item_id` = " . (int)$data['item_id']);
            }
        }

        $db->query("UPDATE `{$db_prefix}shopee_product` 
                              SET `shopid` = " . (int)$data['shop_id'] . ", 
                              `sku` = '" . $db->escape($data['sku']) . "', 
                              `stock` = " . (int)$data['stock'] . ", 
                              `status` = '" . $db->escape($data['status']) . "', 
                              `name` = '" . $db->escape($data['name']) . "',
                              `description` = '" . $db->escape($data['description']) . "', 
                              `images` = '" . $db->escape($data['images']) . "',
                              `currency` = '" . $db->escape($data['currency']) . "', 
                              `price` = '" . (float)($data['price']) . "',
                              `original_price` = '" . (float)($data['original_price']) . "', 
                              `weight` = '" . (float)($data['weight']) . "', 
                              `category_id` = " . (int)$data['category_id'] . ", 
                              `has_variation` = " . (int)$data['has_variation'] . ", 
                              `variations` = '" . $db->escape($data['variations']) . "', 
                              `attributes` = '" . $db->escape($data['attributes']) . "', 
                              `updated_at` = NOW() 
                              WHERE `item_id` = " . (int)$data['item_id']);

        $result = $db->query("SELECT `bestme_id`, `bestme_product_version_id` 
                                    FROM `{$db_prefix}shopee_product` 
                                      WHERE `item_id` = {$data['item_id']} 
                                      AND `has_variation` = 0");

        if (isset($result->row['bestme_id']) && $result->row['bestme_id']) {
            $product_veersion_id = isset($result->row['bestme_product_version_id']) ? $result->row['bestme_product_version_id'] : 0;
            updateProductHasLinkProductShopee($db, $db_prefix, (int)$data['shop_id'], $result->row['bestme_id'], $product_veersion_id, (int)$data['stock'], (float)$data['price'], (float)$data['original_price']);
        }

    } catch (Exception $e) {
        // do something...
    }

    return isset($data['item_id']) ? $data['item_id'] : '';
}

function deleteProductNotInList($products_list, DB $db, $db_prefix, $shopee_id, $shop_name)
{
    if (!is_array($products_list) || empty($products_list)) {
        return;
    }

    $products_list = implode(', ', $products_list);

    try {
        $db->query("DELETE FROM `{$db_prefix}shopee_product` WHERE `shopid` = " . (int)$shopee_id . " AND `item_id` NOT IN (" . $products_list . ")");
    } catch (Exception $e) {
        println(sprintf('deleteProductNotInList() from shopee id "%s" for shop "%s"... failed. Error: %s', $shopee_id, $shop_name, $e->getMessage()));
    }
}

function allowUpdateInventory(DB $db, $db_prefix, $shopee_id)
{
    $shop = $db->query("SELECT `allow_update_stock_product` FROM `{$db_prefix}shopee_shop_config` WHERE `shop_id` = {$shopee_id}");

    if (isset($shop->row['allow_update_stock_product']) && $shop->row['allow_update_stock_product']) {
        return true;
    }

    return false;
}

function allowUpdatePrice(DB $db, $db_prefix, $shopee_id)
{
    $shop = $db->query("SELECT `allow_update_price_product` FROM `{$db_prefix}shopee_shop_config` WHERE `shop_id` = {$shopee_id}");

    if (isset($shop->row['allow_update_price_product']) && $shop->row['allow_update_price_product']) {
        return true;
    }

    return false;
}

function updateProductHasLinkProductShopee(DB $db, $db_prefix, $shopee_id, $product_bestme_id, $product_bestme_version_id, $stock, $price, $original_price)
{
    try {
        $product = $db->query("SELECT `default_store_id` FROM `{$db_prefix}product` WHERE `product_id` = {$product_bestme_id}");
        $default_store_id = isset($product->row['default_store_id']) ? $product->row['default_store_id'] : 0;
        if ($original_price == $price) {
            $price = 0;
        }

        if ($product_bestme_version_id) {
            if (allowUpdateInventory($db, $db_prefix, $shopee_id)) {
                $db->query("UPDATE `{$db_prefix}product_to_store` SET `quantity` = {$stock} 
                                  WHERE `product_id` = {$product_bestme_id} 
                                    AND product_version_id = {$product_bestme_version_id} 
                                    AND `store_id` = {$default_store_id}");
            }

            if (allowUpdatePrice($db, $db_prefix, $shopee_id)) {
                $db->query("UPDATE `{$db_prefix}product_version` 
                                  SET `price` = {$price}, 
                                      `compare_price` = {$original_price} 
                                    WHERE `product_id` = {$product_bestme_id} 
                                      AND `product_version_id` = {$product_bestme_version_id}");
            }
        } else {
            if (allowUpdateInventory($db, $db_prefix, $shopee_id)) {
                $db->query("UPDATE `{$db_prefix}product_to_store` 
                                  SET `quantity` = {$stock} 
                                    WHERE `product_id` = {$product_bestme_id} 
                                      AND product_version_id = 0 
                                      AND `store_id` = {$default_store_id}");
            }

            if (allowUpdatePrice($db, $db_prefix, $shopee_id)) {
                $db->query("UPDATE `{$db_prefix}product` 
                                  SET `price` = {$price}, 
                                      `compare_price` = {$original_price} 
                                    WHERE `product_id` = {$product_bestme_id}");
            }
        }
    } catch (Exception $e) {
        // do something
    }
}

function updateLastTimeSyncProduct(DB $db, $db_prefix, $shopee_id)
{
    try {
        $db->query("UPDATE `{$db_prefix}shopee_shop_config` SET `last_sync_time_product` = NOW() WHERE `shop_id` = {$shopee_id}");
    } catch (Exception $e) {
        // do something...
    }
}