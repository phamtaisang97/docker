<?php

require_once __DIR__ . './../cronjob_init.php';

const CJ_PROCESS_KEY = 'LAZADA_SYNC_ORDER_PROCESS'; // in seconds
const CJ_POLLING_INTERVAL = 2; // in seconds
const CJ_REDIS_TIMEOUT = 30; // in seconds

// redis job key
const CJ_JOB_KEY_DB_HOSTNAME = 'db_hostname';
const CJ_JOB_KEY_DB_PORT = 'db_port';
const CJ_JOB_KEY_DB_USERNAME = 'db_username';
const CJ_JOB_KEY_DB_PASSWORD = 'db_password';
const CJ_JOB_KEY_DB_DATABASE = 'db_database';
const CJ_JOB_KEY_DB_PREFIX = 'db_prefix';
const CJ_JOB_KEY_DB_DRIVER = 'db_driver';
const CJ_JOB_KEY_SHOP_NAME = 'shop_name';
const CJ_JOB_KEY_SHOP_ID = 'shop_id';
const CJ_JOB_KEY_ACCESS_TOKEN = 'access_token';
const CJ_JOB_KEY_SYNC_NEXT_TIME = 'sync_next_time'; // timestamp
const CJ_JOB_KEY_SYNC_INTERVAL = 'sync_interval';
const CJ_JOB_KEY_SYNC_TYPE = 'sync_type'; // auto or manual
const CJ_JOB_KEY_TASK = 'task';
// all tasks
const CJ_JOB_TASK_GET_ORDERS = 'GET_ORDERS';
const CJ_UPDATE_JOB_TASK_GET_ORDERS_PREFIX = 'UPDATE_LAZADA_JOB_GET_ORDERS_';
const CJ_UPDATE_JOB_KEY_TYPE = 'type';
const CJ_UPDATE_JOB_TYPE_UPDATE = 'update';
const CJ_UPDATE_JOB_TYPE_REMOVE = 'remove';
const CJ_UPDATE_JOB_KEY_sync_interval = 'sync_interval';

// sync status
const SYNC_STATUS_SYNC_SUCCESS = 1;
const SYNC_STATUS_PRODUCT_NOT_MAPPING = 0;
const SYNC_STATUS_ORDER_IS_EDITED = 2;
const SYNC_STATUS_SHOP_EXPIRED = 3;
const SYNC_STATUS_SYNC_TIME_OUT = 4;
const SYNC_STATUS_PRODUCT_MAPPING_OUT_OF_STOCK = 5;
const SYNC_STATUS_ORDER_NOT_YET_SYNC = 6;


// global vars
global $redis;

$processing = true;
date_default_timezone_set('Asia/Ho_Chi_Minh');

/* handle kill signal */
// TODO...

/* connect to Redis host */
//println(sprintf("Connecting to Redis host %s at port %s...", MUL_REDIS_HOST, MUL_REDIS_PORT));
$redis = new Redis();
try {
    $redis->connect(MUL_REDIS_HOST, MUL_REDIS_PORT, CJ_REDIS_TIMEOUT);
    $redis->select(MUL_REDIS_DB_LAZADA);
} catch (Exception $e) {
    println(sprintf("Connecting to Redis host %s at port %s... failed! Error: %s", MUL_REDIS_HOST, MUL_REDIS_PORT, $e->getMessage()));
    return;
}
//println(sprintf("Connecting to Redis host %s at port %s... connected!", MUL_REDIS_HOST, MUL_REDIS_PORT));

// check max process
if (!checkMaxProcess()) {
    println('Maximum number of processes is ' . LAZADA_JOB_MAX_PROCESS);
    return;
}
$redis->incr(CJ_PROCESS_KEY);
// end check max process

if ($redis->lLen(LAZADA_ORDER_JOB_REDIS_QUEUE) == 0) {
    $redis->decr(CJ_PROCESS_KEY);
    //println('No LAZADA job found! Exit.');
    return;
}

$max_job_in_process = getMaxJobInProcess();
$current_job_processed = 0;
/* get job from Redis - forever until killed */
while ($processing) {
    $current_job_processed++;
    $job_raw = $redis->brPop([LAZADA_ORDER_JOB_REDIS_QUEUE], 5); // 5 seconds
    /*example $job_raw = [
        0 => 'LAZADA_ORDER_JOB',
        1=> {data}
    ]*/
    if (!array_key_exists(0, $job_raw) || $job_raw[0] != LAZADA_ORDER_JOB_REDIS_QUEUE) {
        break;
    }
    if (!array_key_exists(1, $job_raw)) {
        break;
    }
    $job = json_decode($job_raw[1], true);
    // debug
    /*$job = [
        CJ_JOB_KEY_DB_HOSTNAME => DB_HOSTNAME,
        CJ_JOB_KEY_DB_PORT => DB_PORT,
        CJ_JOB_KEY_DB_USERNAME => DB_USERNAME,
        CJ_JOB_KEY_DB_PASSWORD => DB_PASSWORD,
        CJ_JOB_KEY_DB_DATABASE => DB_DATABASE,
        CJ_JOB_KEY_DB_PREFIX => DB_PREFIX,
        CJ_JOB_KEY_DB_DRIVER => DB_DRIVER,
        CJ_JOB_KEY_SHOP_NAME => '',
        CJ_JOB_KEY_SHOP_ID => '',
        CJ_JOB_KEY_ACCESS_TOKEN => '',
        CJ_JOB_KEY_SYNC_INTERVAL => '',
        CJ_JOB_KEY_SYNC_NEXT_TIME => '',
        CJ_JOB_KEY_SYNC_TYPE => '',
        CJ_JOB_KEY_TASK => CJ_JOB_TASK_GET_ORDER
    ];*/

    if (!is_array($job)) {
        // break for new running if no job!
        //println('No Lazada job found! Exit.');
        $processing = false;
        break;
    }

    try {
        //println('Found new Lazada job! Now run.');
        handleJob($job);
    } catch (Exception $e) {
        println(sprintf('Exception while handling job "%s". Error: %s', $job_raw[1], $e->getMessage()));
    }

    if ($current_job_processed >= $max_job_in_process) {
        // break for new running if no job!
        println('This process has run the maximum number of jobs');
        $processing = false;
        break;
    }

    sleep(CJ_POLLING_INTERVAL);
}

$redis->decr(CJ_PROCESS_KEY);
// TODO: need close?...
$redis->close();
return;
/**
 * handle Job
 *
 * @param array $job
 * @return bool
 */
function handleJob(array $job)
{
    global $redis;
    if (!isset($job[CJ_JOB_KEY_DB_HOSTNAME]) ||
        !isset($job[CJ_JOB_KEY_DB_PORT]) ||
        !isset($job[CJ_JOB_KEY_DB_USERNAME]) ||
        !isset($job[CJ_JOB_KEY_DB_PASSWORD]) ||
        !isset($job[CJ_JOB_KEY_DB_DATABASE]) ||
        !isset($job[CJ_JOB_KEY_DB_PREFIX]) ||
        !isset($job[CJ_JOB_KEY_DB_DRIVER]) ||
        !isset($job[CJ_JOB_KEY_SHOP_NAME]) ||
        !isset($job[CJ_JOB_KEY_SHOP_ID]) ||
        !isset($job[CJ_JOB_KEY_ACCESS_TOKEN]) ||
        !isset($job[CJ_JOB_KEY_SYNC_INTERVAL]) ||
        !isset($job[CJ_JOB_KEY_TASK])
    ) {
        println('Missing either hostname or port or username or password or database or prefix or shop_name or access_token or task');
        return false;
    }

    /* Read info from job */
    $db_hostname = $job[CJ_JOB_KEY_DB_HOSTNAME];
    $db_port = $job[CJ_JOB_KEY_DB_PORT];
    $db_username = $job[CJ_JOB_KEY_DB_USERNAME];
    $db_password = $job[CJ_JOB_KEY_DB_PASSWORD];
    $db_database = $job[CJ_JOB_KEY_DB_DATABASE];
    $db_prefix = $job[CJ_JOB_KEY_DB_PREFIX];
    $db_driver = $job[CJ_JOB_KEY_DB_DRIVER];
    $shop_name = $job[CJ_JOB_KEY_SHOP_NAME];
    $shop_id = $job[CJ_JOB_KEY_SHOP_ID];
    $access_token = $job[CJ_JOB_KEY_ACCESS_TOKEN];
    $sync_interval = $job[CJ_JOB_KEY_SYNC_INTERVAL];
    $sync_next_time = $job[CJ_JOB_KEY_SYNC_NEXT_TIME];
    $sync_type = isset($job[CJ_JOB_KEY_SYNC_TYPE]) ? $job[CJ_JOB_KEY_SYNC_TYPE] : 'auto';
    $task = $job[CJ_JOB_KEY_TASK];

    /* handle task */
    println(sprintf('Handling task "%s"...', $task));
    switch ($task) {
        case CJ_JOB_TASK_GET_ORDERS:
            // check job if disconnected then skip/remove? Or update sync interval
            if ($sync_type == 'auto' && $redis->exists(CJ_UPDATE_JOB_TASK_GET_ORDERS_PREFIX . $shop_name . $shop_id)) {
                $update_job = $redis->get(CJ_UPDATE_JOB_TASK_GET_ORDERS_PREFIX . $shop_name . $shop_id);
                $update_job = json_decode($update_job, true);
                /* data example
                 * $update_job = [
                    CJ_UPDATE_JOB_KEY_TYPE => 'update', // update or remove
                    CJ_UPDATE_JOB_KEY_sync_interval => 24,
                ];*/
                $redis->del(CJ_UPDATE_JOB_TASK_GET_ORDERS_PREFIX . $shop_name . $shop_id);
                if (array_key_exists(CJ_UPDATE_JOB_KEY_TYPE, $update_job) && $update_job[CJ_UPDATE_JOB_KEY_TYPE] == CJ_UPDATE_JOB_TYPE_REMOVE) {
                    println(sprintf("Remove job getting orders from lazada config shop id '%s' for shop '%s'.", $shop_id, $shop_name));
                    break;
                } else if (array_key_exists(CJ_UPDATE_JOB_KEY_TYPE, $update_job) && $update_job[CJ_UPDATE_JOB_KEY_TYPE] == CJ_UPDATE_JOB_TYPE_UPDATE) {
                    if (array_key_exists(CJ_UPDATE_JOB_KEY_sync_interval, $update_job) && (int)$update_job[CJ_UPDATE_JOB_KEY_sync_interval] != 0) {
                        $job[CJ_JOB_KEY_SYNC_INTERVAL] = (int)$update_job[CJ_UPDATE_JOB_KEY_sync_interval];
                    } else {
                        println(sprintf("Remove job getting orders from lazada config shop id '%s' for shop '%s'.", $shop_id, $shop_name));
                        break;
                    }
                }
            }

            if ($job[CJ_JOB_KEY_SYNC_NEXT_TIME] != NULL && $job[CJ_JOB_KEY_SYNC_NEXT_TIME] > time()) {
                println(sprintf("Getting orders from lazada config shop id '%s' for shop '%s'.... It's not yet time to run next time", $shop_id, $shop_name));
                // add new job
                $redis->lPush(LAZADA_ORDER_JOB_REDIS_QUEUE, json_encode($job));
                break;
            }

            // check if real running
            if (isProcessingStatusRealRunning($shop_id, $shop_name)) {
                println(sprintf('Handling task "%s"... already running, skip!', $task));
                break;
            }

            // update real running
            updateProcessingStatusRealRunning($shop_id, $shop_name);
            updateProcessingStatusRunning($shop_id, $shop_name);

            // add job (cloned) back to queue with new NEXT_TIME
            if ($sync_type == 'auto') {
                if ((int)$job[CJ_JOB_KEY_SYNC_INTERVAL] != 0) {
                    $job[CJ_JOB_KEY_SYNC_NEXT_TIME] = time() + (int)$job[CJ_JOB_KEY_SYNC_INTERVAL] * 60; // by seconds
                    $redis->lPush(LAZADA_ORDER_JOB_REDIS_QUEUE, json_encode($job));
                }
            }

            // do getting products
            doGetOrders($db_hostname, $db_port, $db_username, $db_password, $db_database, $db_prefix, $db_driver, $shop_name, $shop_id, $access_token, $sync_type);
            break;

        // and more case...
    }
    println(sprintf('Handling task "%s"... done!', $task));

    return true;
}

/**
 * do Get Orders
 *
 * @param string $db_hostname
 * @param string $db_port
 * @param string $db_username
 * @param string $db_password
 * @param string $db_database
 * @param string $db_prefix
 * @param string $db_driver
 * @param string $shop_name
 * @param string $shop_id
 * @param string $access_token
 * @param string $sync_type
 */
function doGetOrders($db_hostname, $db_port, $db_username, $db_password, $db_database, $db_prefix, $db_driver, $shop_name, $shop_id, $access_token, $sync_type)
{
    println(sprintf('Getting orders from lazada config shop id "%s" for shop "%s"...', $shop_id, $shop_name));

    $db = null;
    try {
        $db = new DB($db_driver,
            htmlspecialchars_decode($db_hostname),
            htmlspecialchars_decode($db_username),
            htmlspecialchars_decode($db_password),
            htmlspecialchars_decode($db_database),
            htmlspecialchars_decode($db_port)
        );
    } catch (Exception $e) {
        updateProcessingStatusRunning($shop_name, $shop_id, false);
        updateProcessingStatusRealRunning($shop_name, $shop_id, false);
        println(sprintf('Getting products from lazada config shop id "%s" for shop "%s"... failed. Error: %s', $shop_id, $shop_name, $e->getMessage()));
        return;
    }

    /* get products from Lazada */
    $lazada = Sale_Channel::getSaleChannel('lazada', ['access_token' => $access_token]);

    // get new access_token if expired
    if (checkAccessTokenExpired($lazada)) {
        $refresh_token = getRefreshToken($db, $db_prefix, $access_token);
        if (!$refresh_token) {
            updateProcessingStatusRunning($shop_name, $shop_id, false);
            updateProcessingStatusRealRunning($shop_name, $shop_id, false);
            return;
        }

        $refresh_data = \Sale_Channel\Lazada::refreshAccessToken($refresh_token);
        $refresh_data = json_decode($refresh_data, true);
        if (!is_array($refresh_data) || !array_key_exists('access_token', $refresh_data)) {
            updateProcessingStatusRunning($shop_name, $shop_id, false);
            updateProcessingStatusRealRunning($shop_name, $shop_id, false);
            return;
        }
        updateNewAccessToken($db, $db_prefix, $refresh_data);

        $access_token = $refresh_data['access_token'];
        $lazada = Sale_Channel::getSaleChannel('lazada', ['access_token' => $access_token]);
    }
    // END get new access_token if expired

    /* get orders from Lazada */

    $shop = getLazadaShopInfo($db, $db_prefix, $shop_id);
    $order_sync_range = isset($shop['order_sync_range']) ? (int)$shop['order_sync_range'] : 15; // 15 day is default

    $list_orders = getListLazadaOrders($shop_id, $shop_name, $order_sync_range, $lazada, 100, 0);

    $count_saved = 0;
    foreach ($list_orders as $order) {
        $order_detail = $order;
        if (!is_array($order_detail) || !array_key_exists('order_id', $order_detail)) {
            continue;
        }

        // save to db
        try {
            $data = array();
            $data['items'] = getOrderItems($lazada, $order['order_id']);
            if (empty($data['items'])) {
                continue;
            }

            $data['order_id'] = $order_detail['order_id'];
            $data['lazada_shop_id'] = $shop_id;
            $data['fullname'] = '';
            if ($order_detail['address_shipping']['last_name']) {
                $data['fullname'] = $order_detail['address_shipping']['last_name'] . ' ';
            }
            $data['fullname'] .= $order_detail['address_shipping']['first_name'];
            $data['phone'] = $order_detail['address_shipping']['phone'];
            $data['ward'] = $order_detail['address_shipping']['address5'];
            $data['district'] = $order_detail['address_shipping']['address4'];
            $data['province'] = $order_detail['address_shipping']['address3'];
            $data['full_address'] = $order_detail['address_shipping']['address1'];
            $data['shipping_fee_original'] = $order_detail['shipping_fee_original'];
            $data['shipping_fee'] = $order_detail['shipping_fee'];
            $data['shipping_method'] = '';
            $data['voucher'] = (float)$order_detail['voucher'];
            $data['voucher_seller'] = isset($order_detail['voucher_seller']) ? (float)$order_detail['voucher_seller'] : 0;
            $total_amount = $order_detail['price'];
            $total_amount = str_replace('.00', '', $total_amount);
            $total_amount = str_replace(',', '', $total_amount);
            $data['total_amount'] = (float)$total_amount + (float)$data['shipping_fee'] - (float)$data['voucher'];
            $data['order_status'] = is_array($order_detail['statuses']) ? end($order_detail['statuses']) : (string)$order_detail['statuses'];
            $data['payment_method'] = $order_detail['payment_method'];
            $data['payment_status'] = $data['payment_method'] != 'COD' ? 1 : 0;
            $data['remarks'] = $order_detail['remarks'];

            $data['create_time'] = date('Y-m-d H:i:s', strtotime($order_detail['created_at']));
            $data['update_time'] = date('Y-m-d H:i:s', strtotime($order_detail['updated_at']));

            updateOrder($db, $db_prefix, $data, $sync_type);
        } catch (Exception $e) {
            println(sprintf('Saving lazada order (id: %s) from lazada config id "%s" for shop "%s"... failed. Error: %s', $order['order_id'], $shop_id, $shop_name, $e->getMessage()));
            continue;
        }

        $count_saved++;

        updateProcessingStatusSaved($shop_id, $shop_name, $count_saved);
    }

    /* mark run completed */
    updateProcessingStatusRunning($shop_id, $shop_name, false);
    updateProcessingStatusRealRunning($shop_id, $shop_name, false);
    updateLastTimeSyncOrder($db, $db_prefix, $shop_id);

    println(sprintf('Getting order from lazada config id "%s" for shop "%s"... done. Total: %s, saved: %s', $shop_id, $shop_name, count($list_orders), $count_saved));
}

function updateOrder(DB $db, $db_prefix, array $data, $sync_type)
{
    if (!array_key_exists('order_id', $data) || !$data['order_id']) {
        return;
    }

    $order = $db->query("SELECT * FROM `{$db_prefix}lazada_order` WHERE `order_id` = '" . $db->escape($data['order_id']) . "'");
    if (isset($order->row['order_id'])) {
        if ($data['update_time'] == $order->row['update_time']) {
            return;
        }
        $is_edited = isset($order->row['is_edited']) ? (int)$order->row['is_edited'] : 0;
        editOrder($db, $db_prefix, $data, $sync_type, $is_edited);
    } else {
        addOrder($db, $db_prefix, $data, $sync_type);
    }
}

function addOrder(DB $db, $db_prefix, array $data, $sync_type)
{
    try {
        if (in_array($data['order_status'], ['canceled', 'returned', 'failed'])) {
            return;
        }

        $db->query("INSERT INTO `{$db_prefix}lazada_order` 
                        SET `order_id` = '" . $db->escape($data['order_id']) . "', 
                            `fullname` = '" . $db->escape($data['fullname']) . "', 
                            `phone` = '" . $db->escape($data['phone']) . "', 
                            `ward` = '" . $db->escape($data['ward']) . "',
                            `district` = '" . $db->escape($data['district']) . "', 
                            `province` = '" . $db->escape($data['province']) . "',
                            `full_address` = '" . $db->escape($data['full_address']) . "',
                            `shipping_fee_original` = '" . (float)($data['shipping_fee_original']) . "',
                            `shipping_fee` = '" . (float)($data['shipping_fee']) . "',
                            `shipping_method` = '" . $db->escape($data['shipping_method']) . "',
                            `total_amount` = '" . (float)($data['total_amount']) . "', 
                            `order_status` = '" . $db->escape($data['order_status']) . "', 
                            `payment_method` = '" . $db->escape($data['payment_method']) . "', 
                            `payment_status` = '" . (int)$data['payment_status'] . "', 
                            `voucher` = '" . (float)($data['voucher']) . "', 
                            `voucher_seller` = '" . (float)($data['voucher_seller']) . "', 
                            `remarks` = '" . $db->escape($data['remarks']) . "', 
                            `create_time` = '" . $data['create_time'] . "', 
                            `update_time` = '" . $data['update_time'] . "', 
                            `lazada_shop_id` = '" . $db->escape($data['lazada_shop_id']) . "', 
                            `sync_status` = " . (int)SYNC_STATUS_ORDER_NOT_YET_SYNC . ", 
                            `sync_time` = NOW()");

        if (array_key_exists('items', $data)) {
            updateOrderProduct($db, $db_prefix, $data['order_id'], $data['items']);
        }
        if ($sync_type == 'auto') {
            // update to order in admin; report, map address
            syncOrderToAdmin($db, $db_prefix, $data['order_id']);
        }
    } catch (Exception $e) {
        // do something...
    }

    return isset($data['order_id']) ? $data['order_id'] : '';
}

function editOrder(DB $db, $db_prefix, array $data, $sync_type, $is_edited = 0)
{
    try {
        $db->query("UPDATE `{$db_prefix}lazada_order` 
                              SET `fullname` = '" . $db->escape($data['fullname']) . "', 
                                `phone` = '" . $db->escape($data['phone']) . "', 
                                `ward` = '" . $db->escape($data['ward']) . "',
                                `district` = '" . $db->escape($data['district']) . "', 
                                `province` = '" . $db->escape($data['province']) . "',
                                `full_address` = '" . $db->escape($data['full_address']) . "',
                                `shipping_fee_original` = '" . (float)($data['shipping_fee_original']) . "',
                                `shipping_fee` = '" . (float)($data['shipping_fee']) . "',
                                `shipping_method` = '" . $db->escape($data['shipping_method']) . "',
                                `total_amount` = '" . (float)($data['total_amount']) . "', 
                                `order_status` = '" . $db->escape($data['order_status']) . "', 
                                `payment_method` = '" . $db->escape($data['payment_method']) . "', 
                                `payment_status` = '" . (int)$data['payment_status'] . "', 
                                `voucher` = '" . (float)($data['voucher']) . "', 
                                `voucher_seller` = '" . (float)($data['voucher_seller']) . "', 
                                `remarks` = '" . $db->escape($data['remarks']) . "', 
                                `create_time` = '" . $data['create_time'] . "', 
                                `update_time` = '" . $data['update_time'] . "', 
                                `lazada_shop_id` = '" . $db->escape($data['lazada_shop_id']) . "', 
                                `sync_time` = NOW() 
                                WHERE `order_id` = '" . $db->escape($data['order_id']) . "'");

        if (array_key_exists('items', $data)) {
            updateOrderProduct($db, $db_prefix, $data['order_id'], $data['items']);
        }

        if ($is_edited) {
            $sync_status = SYNC_STATUS_ORDER_IS_EDITED;
            $db->query("UPDATE `{$db_prefix}lazada_order` SET `sync_status` = " . (int)$sync_status . " WHERE `order_id` = '" . $db->escape($data['order_id']) . "'");
        } else if ($sync_type == 'auto') {
            // update to order in admin; report, map address
            syncOrderToAdmin($db, $db_prefix, $data['order_id'], 'edit');
        }
    } catch (Exception $e) {
        // do something...
    }

    return isset($data['order_id']) ? $data['order_id'] : '';
}

function updateOrderProduct(DB $db, $db_prefix, $order_id, $items)
{
    try {
        $db->query("DELETE FROM `{$db_prefix}lazada_order_to_product` WHERE `lazada_order_id` = '" . $db->escape($order_id) . "'");
        if (is_array($items)) {
            foreach ($items as $item) {
                if (!array_key_exists('shop_sku', $item)) {
                    continue;
                }

                $db->query("INSERT INTO `{$db_prefix}lazada_order_to_product` SET
                                              `lazada_order_id` = '" . $db->escape($order_id) . "', 
                                              `sku` = '" . $db->escape($item['sku']) . "', 
                                              `shop_sku` = '" . $db->escape($item['shop_sku']) . "', 
                                              `variation` = '" . $db->escape($item['variation']) . "',  
                                              `name` = '" . $db->escape($item['name']) . "',  
                                              `quantity` = '" . (int)$item['quantity'] . "',  
                                              `item_price` = '" . (float)$item['item_price'] . "',  
                                              `paid_price` = '" . (float)$item['paid_price'] . "'");
            }
        }
    } catch (Exception $e) {
        // do something ...
    }
}

function syncOrderToAdmin(DB $db, $db_prefix, $order_id, $type = 'add')
{
    try {
        // product in order
        $lazada_order_product = $db->query("SELECT * FROM `{$db_prefix}lazada_order_to_product` WHERE `lazada_order_id` = '" . $db->escape($order_id) . "'");
        $admin_order_product = [];
        $sync_status = SYNC_STATUS_SYNC_SUCCESS;
        foreach ($lazada_order_product->rows as $row) {
            if (!isset($row['shop_sku']) || !isset($row['quantity'])) {
                continue;
            }
            $product = mapProductInOrderToAdmin($db, $db_prefix, $row);
            if (!$product) {
                $sync_status = SYNC_STATUS_PRODUCT_NOT_MAPPING;
                break;
            }

            $admin_order_product[] = $product;
        }
        if ($sync_status != SYNC_STATUS_SYNC_SUCCESS) {
            $db->query("UPDATE `{$db_prefix}lazada_order` SET `sync_status` = " . (int)$sync_status . " WHERE `order_id` = '" . $db->escape($order_id) . "'");
            return;
        } else {
            // check quantity
            if ($type == 'add') {
                foreach ($admin_order_product as $product) {
                    if (!isset($product['product_id']) || !isset($product['product_version_id']) || !isset($product['quantity']) || !isset($product['price']) || !isset($product['default_store_id']) || !isset($product['sale_on_out_of_stock'])) {
                        continue;
                    }

                    if (!$product['sale_on_out_of_stock'] && !checkQuantity($db, $db_prefix, $product['product_id'], $product['product_version_id'], $product['default_store_id'], $product['quantity'])) {
                        $sync_status = SYNC_STATUS_PRODUCT_MAPPING_OUT_OF_STOCK;
                        break;
                    }
                }

                if ($sync_status != SYNC_STATUS_SYNC_SUCCESS) {
                    $db->query("UPDATE `{$db_prefix}lazada_order` SET `sync_status` = " . (int)$sync_status . " WHERE `order_id` = '" . $db->escape($order_id) . "'");
                    return;
                }
            }
            $result = saveToAdmin($db, $db_prefix, $order_id, $admin_order_product);
            if (!$result) {
                $sync_status = SYNC_STATUS_ORDER_NOT_YET_SYNC;
            }
            $db->query("UPDATE `{$db_prefix}lazada_order` SET `sync_status` = " . (int)$sync_status . " WHERE `order_id` = '" . $db->escape($order_id) . "'");
        }
    } catch (Exception $e) {
        // do something ...
    }
}

function mapProductInOrderToAdmin(DB $db, $db_prefix, $item)
{
    $product = [
        'price' => $item['item_price'],
        'discount' => (float)$item['item_price'] - (float)$item['paid_price'],
        'quantity' => $item['quantity']
    ];
    $product_lazada = $db->query("SELECT * FROM `{$db_prefix}lazada_product_version` WHERE `shop_sku` = '" . $item['shop_sku'] . "'");
    if ($product_lazada->num_rows == 0) {
        return false;
    }
    $product_lazada = $product_lazada->row;
    if (!$product_lazada['bestme_product_id']) {
        return false;
    }
    $admin_product_id = (int)$product_lazada['bestme_product_id'];
    $product_admin = $db->query("SELECT * FROM `{$db_prefix}product` WHERE `product_id` = '" . $admin_product_id . "' AND `deleted` IS NULL");
    if ($product_admin->num_rows == 0) {
        return false;
    }
    $product['default_store_id'] = isset($product_admin->row['default_store_id']) ? (int)$product_admin->row['default_store_id'] : 0;
    $product['product_id'] = $admin_product_id;
    $product['sale_on_out_of_stock'] = isset($product_admin->row['sale_on_out_of_stock']) ? (int)$product_admin->row['sale_on_out_of_stock'] : 0;
    $product['product_version_id'] = (int)$product_lazada['bestme_product_version_id'];

    if ($product['product_version_id']) {
        $product_version_admin = $db->query("SELECT * FROM `{$db_prefix}product_version` WHERE `product_id` = '" . $admin_product_id . "' AND `product_version_id` = '" . $product['product_version_id'] . "' AND `deleted` IS NULL");
        if ($product_version_admin->num_rows == 0) {
            return false;
        }
    }

    return $product;
}

function checkQuantity(DB $db, $db_prefix, $product_id, $product_version_id, $store_id, $quantity)
{
    $product_in_store = $db->query("SELECT * FROM `{$db_prefix}product_to_store` WHERE 
                                                                                `product_id` = " . (int)$product_id . " 
                                                                                AND `product_version_id` = " . (int)$product_version_id . "  
                                                                                AND `store_id` = " . (int)$store_id . "   
                                                                                AND `quantity` >= " . (int)$quantity);

    if ($product_in_store->num_rows > 0) {
        return true;
    }

    return false;
}

function updateQuantity(DB $db, $db_prefix, $product_id, $product_version_id, $quantity, $store_id, $condition = '-')
{
    $db->query("UPDATE `{$db_prefix}product_to_store` 
                SET `quantity` = `quantity` " . $condition . (int)$quantity . " 
                WHERE `product_id` = " . (int)$product_id . " 
                  AND `product_version_id` = " . (int)$product_version_id . " 
                  AND `store_id` = " . (int)$store_id);
}

function saveToAdmin(DB $db, $db_prefix, $order_id, $products)
{
    $lazada_order_info = $db->query("SELECT * FROM `{$db_prefix}lazada_order` WHERE `order_id` = '" . $db->escape($order_id) . "'");
    if ($lazada_order_info->num_rows == 0) {
        return false;
    }
    $lazada_order_info = $lazada_order_info->row;

    $map_order_satus = [
        'pending' => 7,
        'ready_to_ship' => 8,
        'shipped' => 8,
        'delivered' => 9,
        'canceled' => 10,
        'returned' => 10,
        'failed' => 10
    ];
    $order_status = isset($map_order_satus[$lazada_order_info['order_status']]) ? (int)$map_order_satus[$lazada_order_info['order_status']] : 0;
    $payment_status = isset($lazada_order_info['payment_status']) ? (int)$lazada_order_info['payment_status'] : 0;
    // TODO use setting config to get user_id, language_id
    $province = getAdministrativeCodeByTextValue($lazada_order_info['province']);
    $district = $province ? getAdministrativeCodeByTextValue($lazada_order_info['district'], $province) : '';
    $ward = $district ? getAdministrativeCodeByTextValue($lazada_order_info['ward'], $district) : '';
    $customer_id = getCustomerByTelephone($db, $db_prefix, ['phone' => $lazada_order_info['phone'], 'fullname' => $lazada_order_info['fullname'], 'province' => $province, 'district' => $district, 'ward' => $ward, 'full_address' => $lazada_order_info['full_address']]);
    $customer_info = json_encode(['id' => $customer_id, 'name' => $lazada_order_info['fullname']]);

    if ($order_status == 10) {
        if (isset($lazada_order_info['bestme_order_id']) && $lazada_order_info['bestme_order_id'] > 0) {
            autoRemoveReceiptVoucher($db, $db_prefix, $lazada_order_info['bestme_order_id']);
        }
    } else {
        autoUpdateReceiptVoucher($db, $db_prefix, $lazada_order_info, $customer_info);
    }

    if (isset($lazada_order_info['bestme_order_id']) && $lazada_order_info['bestme_order_id'] > 0) {
        $old_order_status_id = $db->query("SELECT `order_status_id` FROM `{$db_prefix}order` WHERE `order_id` = " . (int)$lazada_order_info['bestme_order_id']);
        $old_order_status_id = isset($old_order_status_id->row['order_status_id']) ? $old_order_status_id->row['order_status_id'] : 0;

        $db->query("UPDATE `{$db_prefix}order` 
                                    SET `fullname` = '" . $db->escape($lazada_order_info['fullname']) . "', 
                                    `shipping_province_code` = '" . $province . "', 
                                    `shipping_district_code` = '" . $district . "', 
                                    `shipping_ward_code` = '" . $ward . "', 
                                    `shipping_address_1` = '" . $db->escape($lazada_order_info['full_address']) . "', 
                                    `telephone` = '" . $db->escape($lazada_order_info['phone']) . "', 
                                    `shipping_method` = '" . $db->escape($lazada_order_info['shipping_method']) . "', 
                                    `shipping_method_value` = '-2', 
                                    `shipping_fee` = '" . (float)$lazada_order_info['shipping_fee'] . "', 
                                    `order_status_id` = '" . $order_status . "', 
                                    `previous_order_status_id` = '" . $old_order_status_id . "', 
                                    `order_code` = '" . $db->escape($lazada_order_info['order_id']) . "', 
                                    `payment_method` = '" . $db->escape($lazada_order_info['payment_method']) . "', 
                                    `discount` = '', 
                                    `total` = '" . (float)$lazada_order_info['total_amount'] . "',
                                    `comment` = '" . $db->escape($lazada_order_info['remarks']) . "',
                                    `user_id` = 1,
                                    `customer_id` = '" . (int)$customer_id . "',
                                    `date_modified` = '" . $lazada_order_info['update_time'] . "',
                                    `payment_status` = '" . (int)$payment_status . "',
                                    `language_id` = 2 WHERE `order_id` = '" . (int)$lazada_order_info['bestme_order_id'] . "'");

        // update quantity product
        if (in_array($old_order_status_id, [7, 8, 9]) && $order_status == 10) {
            foreach ($products as $product) {
                updateQuantity($db, $db_prefix, $product['product_id'], $product['product_version_id'], $product['quantity'], $product['default_store_id'], '+');
            }

        }
        // update to report
        $db->query("UPDATE `{$db_prefix}report_order` SET 
                                                            `customer_id` = " . (int)$customer_id . ",
                                                            `total_amount` = " . (float)$lazada_order_info['total_amount'] . ", 
                                                            `order_status` = " . (int)$order_status . ",
                                                            `shipping_fee` = '" . (float)$lazada_order_info['shipping_fee'] . "',
                                                            `report_date` = '" . $lazada_order_info['update_time'] ."'
                                                            WHERE `order_id` = " . (int)$lazada_order_info['bestme_order_id']);
    } else {
        $db->query("INSERT INTO `{$db_prefix}order` 
                                    SET `fullname` = '" . $db->escape($lazada_order_info['fullname']) . "', 
                                    `shipping_province_code` = '" . $province . "', 
                                    `shipping_district_code` = '" . $district . "', 
                                    `shipping_ward_code` = '" . $ward . "', 
                                    `shipping_address_1` = '" . $db->escape($lazada_order_info['full_address']) . "', 
                                    `telephone` = '" . $db->escape($lazada_order_info['phone']) . "', 
                                    `shipping_method` = '" . $db->escape($lazada_order_info['shipping_method']) . "', 
                                    `shipping_method_value` = '-2', 
                                    `shipping_fee` = '" . (float)$lazada_order_info['shipping_fee'] . "', 
                                    `order_status_id` = '" . $order_status . "', 
                                    `order_code` = '" . $db->escape($lazada_order_info['order_id']) . "', 
                                    `payment_method` = '" . $db->escape($lazada_order_info['payment_method']) . "', 
                                    `discount` = '', 
                                    `total` = '" . (float)$lazada_order_info['total_amount'] . "',
                                    `comment` = '" . $db->escape($lazada_order_info['remarks']) . "',
                                    `source` = 'lazada',
                                    `user_id` = 1,
                                    `customer_id` = '" . (int)$customer_id . "',
                                    `date_added` = '" . $lazada_order_info['create_time'] . "',
                                    `date_modified` = '" . $lazada_order_info['update_time'] . "',
                                    `payment_status` = '" . (int)$payment_status . "',
                                    `language_id` = 2");

        $bestme_order_id = $db->getLastId();
        $db->query("UPDATE `{$db_prefix}lazada_order` SET `bestme_order_id` = " . (int)$bestme_order_id . " WHERE `order_id` = '" . $db->escape($order_id) . "'");
        // insert order_product
        $total_discount = 0;
        foreach ($products as $product) {
            $discount = isset($product['discount']) ? $product['discount'] : 0;
            $total_discount += $discount;

            $db->query("INSERT INTO `{$db_prefix}order_product` 
                                                      SET `order_id` = " . (int)$bestme_order_id . ", 
                                                      `product_id` = " . (int)$product['product_id'] . ", 
                                                      `product_version_id` = " . (int)$product['product_version_id'] . ", 
                                                      `quantity` = " . (int)$product['quantity'] . ", 
                                                      `price` = " . (float)$product['price'] . ", 
                                                      `discount` = " . (float)$discount . ", 
                                                      `total` = " . (float)$product['price'] * (int)$product['quantity']);

            // update quantity
            updateQuantity($db, $db_prefix, $product['product_id'], $product['product_version_id'], $product['quantity'], $product['default_store_id']);
        }
        // insert to report
        $report_time = $lazada_order_info['update_time'];
        $date = new DateTime($report_time);
        $report_date = $date->format('Y-m-d');
        $db->query("INSERT INTO `{$db_prefix}report_order` SET 
                                                            `report_time` = '" . $report_time . "', 
                                                            `report_date` = '" . $report_date . "',
                                                            `order_id` = " . (int)$bestme_order_id . ", 
                                                            `customer_id` = " . (int)$customer_id . ",
                                                            `total_amount` = " . (float)$lazada_order_info['total_amount'] . ", 
                                                            `order_status` = " . (int)$order_status . ",
                                                            `discount` = " . $total_discount . ", 
                                                            `source` = 'lazada',
                                                            `shipping_fee` = '" . (float)$lazada_order_info['shipping_fee'] . "',
                                                            `store_id` = 0, `user_id` = 1");
        foreach ($products as $product) {
            $discount = isset($product['discount']) ? $product['discount'] : 0;
            $query = $db->query("SELECT cost_price 
                                                FROM `" . DB_PREFIX . "product_to_store`
                                                WHERE product_id = " . (int)$product['product_id'] . " 
                                                    AND product_version_id= " . (int)$product['product_version_id']);
            $cost_price = $query->row['cost_price'];

            // insert report order
            $user_id = 1; // Admin
            $store_id = 0;
            $total_amount = (int)$product['quantity'] * (float)$product['price'];
            $sql = "INSERT INTO " . $db_prefix . "report_product SET report_time = '" . $report_time . "'";
            $sql .= ", report_date = '" . $report_date . "'";
            $sql .= ", order_id = '" . (int)$bestme_order_id . "'";
            $sql .= ", product_id = '" . (int)$product['product_id'] . "'";
            $sql .= ", product_version_id = '" . (int)$product['product_version_id'] . "'";
            $sql .= ", quantity = '" . (int)$product['quantity'] . "'";
            $sql .= ", price = '" . (float)$product['price'] . "'";
            $sql .= ", total_amount = '" . (float)$total_amount . "'";
            $sql .= ", discount = '" . (float)$discount . "'";
            $sql .= ", cost_price = '" . (float)$cost_price . "'";
            $sql .= ", store_id = '" . $store_id . "'";
            $sql .= ", user_id = '" . (int)$user_id . "'";
            $db->query($sql);
        }
    }

    return true;
}

function getCustomerByTelephone(DB $db, $db_prefix, $data)
{
    $customer = $db->query("SELECT DISTINCT * FROM `{$db_prefix}customer` WHERE telephone = '" . $db->escape($data['phone']) . "'");
    if (is_array($customer->row) && isset($customer->row['customer_id'])) {
        return $customer->row['customer_id'];
    }

    return createNewCustomer($db, $db_prefix, $data);
}

function createNewCustomer(DB $db, $db_prefix, $data)
{
    $customer = [
        'firstname' => isset($data['fullname']) ? extract_name($data['fullname'])[0] : '',
        'lastname' => isset($data['fullname']) ? extract_name($data['fullname'])[1] : '',
        'email' => '',
        'telephone' => $data['phone']
    ];
    $db->query("INSERT INTO `{$db_prefix}customer` 
                                            SET  `firstname` = '" . $db->escape($customer['firstname']) . "', 
                                            `lastname` = '" . $db->escape($customer['lastname']) . "', 
                                            `email` = '" . $db->escape($customer['email']) . "', 
                                            `telephone` = '" . $db->escape($customer['telephone']) . "', 
                                            `salt` = '" . $db->escape($salt = 'lazada') . "', 
                                            `password` = '" . $db->escape(sha1($salt . sha1($salt . sha1('lazada')))) . "', 
                                            `status` = 1,  
                                            `date_added` = NOW()");
    $customer_id = $db->getLastId();
    // update customer code
    $prefix = 'KH';
    $code = $prefix . str_pad($customer_id, 6, "0", STR_PAD_LEFT);
    $db->query("UPDATE " . DB_PREFIX . "customer SET `code` = '" . $code . "' WHERE customer_id = '" . (int)$customer_id . "'");

    $address_data = array(
        'customer_id' => $customer_id,
        'firstname' => $customer['firstname'],
        'lastname' => $customer['lastname'],
        'company' => '',
        'city' => $data['province'],
        'district' => $data['district'],
        'wards' => $data['ward'],
        'phone' => $customer['telephone'],
        'postcode' => '',
        'address' => $data['full_address'],
        'default' => true
    );

    $db->query("INSERT INTO `{$db_prefix}address` SET 
                                              customer_id = '" . (int)$customer_id . "', 
                                              firstname = '" . $db->escape($address_data['firstname']) . "', 
                                              lastname = '" . $db->escape($address_data['lastname']) . "', 
                                              company = '" . $db->escape($address_data['company']) . "', 
                                              district = '" . $db->escape($address_data['district']) . "', 
                                              address = '" . $db->escape($address_data['address']) . "', 
                                              phone = '" . $db->escape($address_data['phone']) . "', 
                                              postcode = '" . $db->escape($address_data['postcode']) . "', 
                                              city = '" . $db->escape($address_data['city']) . "'");

    $address_id = $db->getLastId();
    $db->query("UPDATE `{$db_prefix}customer` SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");

    // create Customer Report
    $reportTime = new DateTime();
    $reportTime->setTime($reportTime->format('H'), 0, 0);
    $sql = "INSERT INTO `{$db_prefix}report_customer` SET report_time = '" . $reportTime->format('Y-m-d H:i:s') . "'";
    $sql .= ", report_date = '" . $reportTime->format('Y-m-d') . "'";
    $sql .= ", customer_id = '" . (int)$customer_id . "'";
    $db->query($sql);

    return $customer_id;
}

/**
 * @param string $shop_id
 * @param string $shop_name
 * @param int $order_sync_range
 * @param \Sale_Channel\Abstract_Sale_Channel $lazada
 * @param int $limit
 * @param int $offset
 * @param array $list_orders
 * @return array
 */
function getListLazadaOrders($shop_id, $shop_name, $order_sync_range, $lazada, $limit, $offset, $list_orders = [])
{
    try {
        $date = new DateTime();
        $date->modify('-'.$order_sync_range.' days');
        $time_from = $date->format('c'); // 'c' or 'DateTime::ISO8601'

        $orders = $lazada->getOrders(['created_after' => $time_from, 'limit' => $limit, 'offset' => $offset, 'sort_by' => 'created_at', 'sort_direction' => 'DESC']);
        $orders = json_decode($orders, true);
        $count_orders = 0;
        if (is_array($orders) && array_key_exists('data', $orders) && is_array($orders['data'])) {
            $data = $orders['data'];
            if (is_array($data) && array_key_exists('orders', $data) && is_array($data['orders'])) {
                $count_orders = count($data['orders']);
                $list_orders = array_merge($list_orders, $data['orders']);

                updateProcessingStatusTotal($shop_id, $shop_name, count($list_orders));
            }
        }

        if ($count_orders > 0 && $count_orders == $limit) {
            $list_orders = getListLazadaOrders($shop_id, $shop_name, $order_sync_range, $lazada, $limit, $offset + $limit, $list_orders);
        }

        return $list_orders;
    } catch (Exception $e) {
        return [];
    }
}

/**
 * @param \Sale_Channel\Abstract_Sale_Channel $lazada
 * @param int $order_id
 * @return array
 */
function getOrderItems($lazada, $order_id)
{
    try {
        $result = [];
        $order_items = $lazada->getOrderItems(['order_id' => $order_id]);
        $order_items = json_decode($order_items, true);
        $order_items = isset($order_items['data']) ? $order_items['data'] : [];
        if (is_array($order_items)) {
            foreach ($order_items as $item) {
                if (!array_key_exists('shop_sku', $item)) {
                    continue;
                }
                if (array_key_exists($item['shop_sku'], $result)) {
                    $result[$item['shop_sku']]['quantity'] = isset($result[$item['shop_sku']]['quantity']) ? $result[$item['shop_sku']]['quantity'] + 1 : 1;
                    continue;
                }
                $result[$item['shop_sku']] = array(
                    'lazada_order_id' => $order_id,
                    'sku' => $item['sku'],
                    'shop_sku' => $item['shop_sku'],
                    'variation' => $item['variation'],
                    'name' => $item['name'],
                    'quantity' => 1,
                    'item_price' => (float)$item['item_price'],
                    'paid_price' => (float)$item['paid_price']
                );
            }
        }

        return array_values($result);
    } catch (Exception $exception) {
        return [];
    }
}

function checkMaxProcess()
{
    global $redis;
    if (!$redis->exists(CJ_PROCESS_KEY)) {
        $redis->set(CJ_PROCESS_KEY, 0);
        $count_process = 0;
    } else {
        $count_process = $redis->get(CJ_PROCESS_KEY);
        if ($count_process < 0) {
            $redis->set(CJ_PROCESS_KEY, 0);
        }
    }

    if ($count_process >= LAZADA_JOB_MAX_PROCESS) {
        return false;
    }

    return true;
}

function getMaxJobInProcess()
{
    global $redis;
    $count = $redis->lLen(LAZADA_ORDER_JOB_REDIS_QUEUE);
    if ($count > (int)LAZADA_MAX_JOB_IN_A_PROCESS) {
        return (int)LAZADA_MAX_JOB_IN_A_PROCESS;
    }

    return $count;
}

function updateProcessingStatusSaved($shop_id, $shop_name, $saved)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'count_order_lazada_saved');
    $redis->set($key, $saved, LAZADA_PROCESS_INFO_TIMEOUT);
}

function updateProcessingStatusTotal($shop_id, $shop_name, $total)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'count_order_lazada');
    $redis->set($key, $total, LAZADA_PROCESS_INFO_TIMEOUT);
}

function updateProcessingStatusRealRunning($shop_id, $shop_name, $real_running = true)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'load_lazada_order_real_running');
    $redis->set($key, ((bool)$real_running) ? 1 : 0, LAZADA_PROCESS_INFO_TIMEOUT);
}

function updateProcessingStatusRunning($shop_id, $shop_name, $running = true)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'load_lazada_order_running');
    $redis->set($key, ((bool)$running) ? 1 : 0, LAZADA_PROCESS_INFO_TIMEOUT);
}

function isProcessingStatusRealRunning($shop_id, $shop_name)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'load_lazada_order_real_running');
    $real_running = $redis->get($key);

    return $real_running == 1;
}

/**
 * @param $administrative_text
 * @param null $parent_code null if province, or MUST valid code if district or ward
 * @return mixed|null
 */
function getAdministrativeCodeByTextValue($administrative_text, $parent_code = null)
{
    return (new \Vietnam_Administrative())->getAdministrativeCodeByTextValue($administrative_text, $parent_code);
}

/* Extract fullname to firstname(Tên) and lastname(Họ) */
function extract_name($fullname)
{
    $fullname = sprintf('"%s"', $fullname);
    $fullname = json_decode($fullname);
    $fullname = trim($fullname);
    $delimiter = " ";
    $parts = explode($delimiter, $fullname);
    /* get first element from array */
    $lastname = array_shift($parts);
    if (count($parts) > 0) {
        $firstname = implode($delimiter, $parts);
    } else {
        $firstname = $lastname;
    }
    return array($firstname, $lastname);
}

function checkAccessTokenExpired($lazada)
{
    $shoinfo = $lazada->getShopInfo();
    $shoinfo = json_decode($shoinfo, true);
    if (isset($shoinfo['code']) && $shoinfo['code'] == 'IllegalAccessToken') {
        return true;
    }

    return false;
}

function getRefreshToken(DB $db, $db_prefix, $access_token)
{
    $query = $db->query("SELECT `refresh_token` FROM `{$db_prefix}lazada_shop_config` WHERE `access_token` = '" . $db->escape($access_token) . "'");
    if ($query->num_rows > 0) {
        return $query->row['refresh_token'];
    }

    return '';
}

function updateNewAccessToken(DB $db, $db_prefix, $data)
{
    if (!is_array($data) || !array_key_exists('access_token', $data)) {
        return;
    }
    $expires_in = gmdate("Y-m-d H:i:s", (int)$data['expires_in'] + time());
    $refresh_expires_in = gmdate("Y-m-d H:i:s", (int)$data['refresh_expires_in'] + time());
    $db->query("UPDATE `{$db_prefix}lazada_shop_config` SET 
                                        `access_token` = '" . $db->escape($data['access_token']) . "', 
                                        `refresh_token` = '" . $db->escape($data['refresh_token']) . "', 
                                        `expires_in` = '" . $db->escape($expires_in) . "', 
                                        `refresh_expires_in` = '" . $db->escape($refresh_expires_in) . "'");
}

function autoUpdateReceiptVoucher(DB $db, $prefix, $data, $customer_info)
{
    $voucher_class = new AUTO_CREATE_RECEIPT_VOUCHER_UTIL_CLASS($db, $prefix);
    $payment_status = isset($data['payment_status']) ? (int)$data['payment_status'] : 0;
    $dataToSave = [
        'payment_status' => $payment_status,
        'payment_method' => $data['payment_method'],
        'total_pay' => $data['total_amount'],
        'created_at' => isset($data['create_time']) ? $data['create_time'] : null,
        'updated_at' => isset($data['update_time']) ? $data['update_time'] : null,
        'customer_info' => $customer_info ? $customer_info : null
    ];
    try {
        $voucher_class->autoUpdateFromOrder($data['bestme_order_id'], $dataToSave);
        return;
    } catch (Exception $ex) {
        return;
    }

}

function autoRemoveReceiptVoucher(DB $db, $prefix, $order_id)
{
    $voucher_class = new AUTO_CREATE_RECEIPT_VOUCHER_UTIL_CLASS($db, $prefix);
    try {
        $voucher_class->removeReceiptFromOrder($order_id);
        return;
    } catch (Exception $ex) {
        return;
    }
}

function getLazadaShopInfo(DB $db, $db_prefix, $shop_id)
{
    try {
        $shop = $db->query("SELECT * FROM `{$db_prefix}lazada_shop_config` WHERE `id` = {$shop_id}");
        return $shop->row;
    } catch (Exception $e) {
        return [];
        // do something...
    }
}

function updateLastTimeSyncOrder(DB $db, $db_prefix, $shop_id)
{
    try {
        $db->query("UPDATE `{$db_prefix}lazada_shop_config` SET `last_sync_time_order` = NOW() WHERE `id` = {$shop_id}");
    } catch (Exception $e) {
        // do something...
    }
}