<?php

require_once __DIR__. './../cronjob_init.php';

const CJ_POLLING_INTERVAL = 2; // in seconds
const CJ_REDIS_TIMEOUT = 30; // in seconds

// redis job key
const CJ_JOB_KEY_DB_HOSTNAME = 'db_hostname';
const CJ_JOB_KEY_DB_PORT = 'db_port';
const CJ_JOB_KEY_DB_USERNAME = 'db_username';
const CJ_JOB_KEY_DB_PASSWORD = 'db_password';
const CJ_JOB_KEY_DB_DATABASE = 'db_database';
const CJ_JOB_KEY_DB_PREFIX = 'db_prefix';
const CJ_JOB_KEY_DB_DRIVER = 'db_driver';
const CJ_JOB_KEY_SHOP_NAME = 'shop_name';
const CJ_JOB_KEY_SHOP_ID = 'shop_id'; // id in table config
const CJ_JOB_KEY_ACCESS_TOKEN = 'access_token';
const CJ_JOB_KEY_TASK = 'task';
// all tasks
const CJ_JOB_TASK_GET_PRODUCTS = 'GET_PRODUCTS';

// global vars
global $redis;

$processing = true;

/* handle kill signal */
// TODO...

/* connect to Redis host */
//println(sprintf("Connecting to Redis host %s at port %s...", MUL_REDIS_HOST, MUL_REDIS_PORT));
$redis = new Redis();
try {
    $redis->connect(MUL_REDIS_HOST, MUL_REDIS_PORT, CJ_REDIS_TIMEOUT);
    $redis->select(MUL_REDIS_DB_LAZADA);
} catch (Exception $e) {
    println(sprintf("Connecting to Redis host %s at port %s... failed! Error: %s", MUL_REDIS_HOST, MUL_REDIS_PORT, $e->getMessage()));
    return;
}
//println(sprintf("Connecting to Redis host %s at port %s... connected!", MUL_REDIS_HOST, MUL_REDIS_PORT));

/* get job from Redis - forever until killed */
while ($processing) {
    $job_raw = $redis->brPop([LAZADA_PRODUCT_JOB_REDIS_QUEUE], 5); // 5 seconds
    /*example $job_raw = [
        0 => 'LAZADA_PRODUCT_JOB',
        1=> {data}
    ]*/

    if (!array_key_exists(0, $job_raw) || $job_raw[0] != LAZADA_PRODUCT_JOB_REDIS_QUEUE){
        break;
    }
    if (!array_key_exists(1, $job_raw)){
        break;
    }
    $job = json_decode($job_raw[1], true);

    // debug
    /*$job = [
        CJ_JOB_KEY_DB_HOSTNAME => DB_HOSTNAME,
        CJ_JOB_KEY_DB_PORT => DB_PORT,
        CJ_JOB_KEY_DB_USERNAME => DB_USERNAME,
        CJ_JOB_KEY_DB_PASSWORD => DB_PASSWORD,
        CJ_JOB_KEY_DB_DATABASE => DB_DATABASE,
        CJ_JOB_KEY_DB_PREFIX => DB_PREFIX,
        CJ_JOB_KEY_DB_DRIVER => DB_DRIVER,
        CJ_JOB_KEY_SHOP_NAME => '',
        CJ_JOB_KEY_SHOP_Id => '',
        CJ_JOB_KEY_ACCESS_TOKEN => '',
        CJ_JOB_KEY_TASK => CJ_JOB_TASK_GET_PRODUCTS
    ];*/

    if (!is_array($job)) {
        // break for new running if no job!
        //println('No lazada job found! Exit.');
        $processing = false;
        break;
    }

    try {
        //println('Found new Lazada job get products! Now run.');
        handleJob($job);
    } catch (Exception $e) {
        println(sprintf('Exception while handling job "%s". Error: %s', $job_raw, $e->getMessage()));
    }

    sleep(CJ_POLLING_INTERVAL);
}

// TODO: need close?...
$redis->close();
return;

/**
 * handle Job
 *
 * @param array $job
 * @return bool
 */
function handleJob(array $job)
{
    if (!isset($job[CJ_JOB_KEY_DB_HOSTNAME]) ||
        !isset($job[CJ_JOB_KEY_DB_PORT]) ||
        !isset($job[CJ_JOB_KEY_DB_USERNAME]) ||
        !isset($job[CJ_JOB_KEY_DB_PASSWORD]) ||
        !isset($job[CJ_JOB_KEY_DB_DATABASE]) ||
        !isset($job[CJ_JOB_KEY_DB_PREFIX]) ||
        !isset($job[CJ_JOB_KEY_DB_DRIVER]) ||
        !isset($job[CJ_JOB_KEY_SHOP_NAME]) ||
        !isset($job[CJ_JOB_KEY_SHOP_ID]) ||
        !isset($job[CJ_JOB_KEY_ACCESS_TOKEN]) ||
        !isset($job[CJ_JOB_KEY_TASK])
    ) {
        println('Missing either hostname or port or username or password or database or prefix or shop_name or shopee_id or task');
        return false;
    }

    /* Read info from job */
    $db_hostname = $job[CJ_JOB_KEY_DB_HOSTNAME];
    $db_port = $job[CJ_JOB_KEY_DB_PORT];
    $db_username = $job[CJ_JOB_KEY_DB_USERNAME];
    $db_password = $job[CJ_JOB_KEY_DB_PASSWORD];
    $db_database = $job[CJ_JOB_KEY_DB_DATABASE];
    $db_prefix = $job[CJ_JOB_KEY_DB_PREFIX];
    $db_driver = $job[CJ_JOB_KEY_DB_DRIVER];
    $shop_name = $job[CJ_JOB_KEY_SHOP_NAME];
    $shop_id = $job[CJ_JOB_KEY_SHOP_ID];
    $access_token = $job[CJ_JOB_KEY_ACCESS_TOKEN];
    $task = $job[CJ_JOB_KEY_TASK];

    /* handle task */
    //println(sprintf('Handling task "%s (data: %s)"...', $task, json_encode($job)));
    switch ($task) {
        case CJ_JOB_TASK_GET_PRODUCTS:
            // check if real running
            if (isProcessingStatusRealRunning($shop_name, $shop_id)) {
                //println(sprintf('Handling task "%s"... already running, skip!', $task));
                break;
            }

            // update real running
            updateProcessingStatusRealRunning($shop_name, $shop_id);

            // do getting products
            doGetProducts($db_hostname, $db_port, $db_username, $db_password, $db_database, $db_prefix, $db_driver, $shop_name, $shop_id, $access_token);

            break;

        // and more case...
    }
    println(sprintf('Handling task "%s"... done!', $task));

    return true;
}

/**
 * do Get Products
 *
 * @param string $db_hostname
 * @param string $db_port
 * @param string $db_username
 * @param string $db_password
 * @param string $db_database
 * @param string $db_prefix
 * @param string $db_driver
 * @param string $shop_name
 * @param string $shop_id
 * @param string $access_token
 */
function doGetProducts($db_hostname, $db_port, $db_username, $db_password, $db_database, $db_prefix, $db_driver, $shop_name, $shop_id, $access_token)
{
    println(sprintf('Getting products from lazada shop "%s" for shop "%s"...', $shop_id, $shop_name));

    /* save all products from Lazada to db */
    $db = null;
    try {
        $db = new DB($db_driver,
            htmlspecialchars_decode($db_hostname),
            htmlspecialchars_decode($db_username),
            htmlspecialchars_decode($db_password),
            htmlspecialchars_decode($db_database),
            htmlspecialchars_decode($db_port)
        );
    } catch (Exception $e) {
        updateProcessingStatusRunning($shop_name, $shop_id, false);
        updateProcessingStatusRealRunning($shop_name, $shop_id, false);
        println(sprintf('Getting products from lazada shop "%s" for shop "%s"... failed. Error: %s', $shop_id, $shop_name, $e->getMessage()));
        return;
    }

    /* get products from Lazada */
    $lazada = Sale_Channel::getSaleChannel('lazada', ['access_token' => $access_token]);

    // get new access_token if expired
    if (checkAccessTokenExpired($lazada)) {
        $refresh_token = getRefreshToken($db, $db_prefix, $access_token);
        if (!$refresh_token){
            updateProcessingStatusRunning($shop_name, $shop_id, false);
            updateProcessingStatusRealRunning($shop_name, $shop_id, false);
            return;
        }

        $refresh_data = \Sale_Channel\Lazada::refreshAccessToken($refresh_token);
        $refresh_data = json_decode($refresh_data, true);
        if (!is_array($refresh_data) || !array_key_exists('access_token', $refresh_data)){
            updateProcessingStatusRunning($shop_name, $shop_id, false);
            updateProcessingStatusRealRunning($shop_name, $shop_id, false);
            return;
        }
        updateNewAccessToken($db, $db_prefix, $refresh_data);

        $access_token = $refresh_data['access_token'];
        $lazada = Sale_Channel::getSaleChannel('lazada', ['access_token' => $access_token]);
    }
    // END get new access_token if expired

    // todo: limit > 50 -> error: E506: Get product failed
    $list_products = getListLazadaProducts($shop_name, $shop_id, $access_token, $lazada, 50, 0);

    $count_saved = 0;
    $list_item_ids = [];
    $list_category_ids = [];
    foreach ($list_products as $product) {
        if (!array_key_exists('item_id', $product) || !array_key_exists('primary_category', $product)) {
            continue;
        }
        // update categoryAttributes
        if (!in_array($product['primary_category'], $list_category_ids)){
            updateCategoryAttributes($db, $db_prefix, $product['primary_category'], $lazada);
        }
        $list_category_ids[] = $product['primary_category'];
        $list_item_ids[] = $product['item_id'];
        // save to db
        try {
            $data = array();
            $data['shop_id'] = $shop_id;
            $data['item_id'] = $product['item_id'];
            $data['primary_category'] = $product['primary_category'];
            $data['name'] = $product['attributes']['name'];
            $data['short_description'] = isset($product['attributes']['short_description']) ? $product['attributes']['short_description'] : '';
            $data['description'] = isset($product['attributes']['description']) ? $product['attributes']['description'] : $data['short_description'];
            $data['brand'] = isset($product['attributes']['brand']) ? $product['attributes']['brand'] : '';

            updateProduct($db, $db_prefix, $data);
            $skus = isset($product['skus']) ? $product['skus'] : [];
            $sku_ids = [];
            if ($skus && is_array($skus)) {
                foreach ($skus as $sku) {
                    if (!array_key_exists('SkuId', $sku) || !$sku['SkuId']) {
                        continue;
                    }
                    $sku_ids[] = $sku['SkuId'];
                    updateProductVersion($db, $db_prefix, $product['item_id'], $sku, $product['primary_category']);
                }
            }
            deleteProductOldVesion($db, $db_prefix, $product['item_id'], $sku_ids);
        } catch (Exception $e) {
            println(sprintf('Saving lazada product (item id %s) from lazada shop "%s" for shop "%s"... failed. Error: %s', $data['item_id'], $shop_id, $shop_name, $e->getMessage()));
            continue;
        }

        $count_saved++;

        updateProcessingStatusSaved($shop_name, $shop_id, $count_saved);
    }

    // delete product not exist in LAZADA
    deleteProductNotInList($list_item_ids, $db, $db_prefix, $shop_id, $shop_name);

    /* mark run completed */
    updateProcessingStatusRunning($shop_name, $shop_id, false);
    updateProcessingStatusRealRunning($shop_name, $shop_id, false);
    updateLastTimeSyncProduct($db, $db_prefix, $shop_id);

    println(sprintf('Getting products from lazada shop "%s" for shop "%s"... done. Total: %s, saved: %s', $shop_id, $shop_name, count($list_products), $count_saved));
}

/**
 * @param string $shop_name
 * @param string $access_token
 * @param \Sale_Channel\Abstract_Sale_Channel $lazada
 * @param int $limit
 * @param int $offset
 * @param array $list_products
 * @return array
 */
function getListLazadaProducts($shop_name, $shop_id, $access_token, $lazada, $limit, $offset, $list_products = [])
{
    $products = $lazada->getProducts(['filter' => 'all', 'limit' => $limit, 'offset' => $offset]);
    $products = json_decode($products, true);
    $total_product = 0;
    if (is_array($products) && array_key_exists('data', $products) && is_array($products['data'])) {
        $data = $products['data'];
        $total_product = isset($data['total_products']) ? (int)$data['total_products'] : 0;
        if (is_array($data) && array_key_exists('products', $data) && is_array($data['products'])) {
            $list_products = array_merge($list_products, $data['products']);

            updateProcessingStatusTotal($shop_name, $shop_id, count($list_products));
        }
    }

    if ($total_product > 0 && $total_product > ($limit + $offset)) {
        $list_products = getListLazadaProducts($shop_name, $shop_id, $access_token, $lazada, $limit, $offset + $limit, $list_products);
    }

    return $list_products;
}

function updateCategoryAttributes(DB $db, $db_prefix, $category_id, $lazada)
{
    if (!$category_id) {
        return;
    }

    try {
        $data_cateogry = $lazada->getCategoryAttributes(['primary_category_id' => $category_id]);
        $data_cateogry = json_decode($data_cateogry, true);
        if (!isset($data_cateogry['data']) || !is_array($data_cateogry['data'])) {
            return;
        }
        $data = json_encode($data_cateogry['data']);

        $category = $db->query("SELECT * FROM `{$db_prefix}lazada_category_attributes` WHERE `lazada_cateogry_id` = " . (int)$category_id);
        if (isset($category->row['lazada_cateogry_id'])) {
            $db->query("UPDATE `{$db_prefix}lazada_category_attributes` SET `data` = '" . $db->escape($data) . "' WHERE `lazada_cateogry_id` = " . (int)$category_id);
        } else {
            $db->query("INSERT INTO `{$db_prefix}lazada_category_attributes` SET `lazada_cateogry_id` = " . (int)$category_id . ", `data` = '" . $db->escape($data) . "'");
        }
    } catch (Exception $e) {
        println(sprintf('Saving lazada product category (category id %s) from lazada failed. Error: %s', $category_id, $e->getMessage()));
    }
}

function getShopIdFromAccessToken(DB $db, $db_prefix, $access_token)
{
    $shop = $db->query("SELECT * FROM `{$db_prefix}lazada_shop_config` WHERE `access_token` = '" . $db->escape($access_token) . "'");
    if (isset($shop->row['id'])) {
        return (int)$shop->row['id'];
    } else {
        return 0;
    }
}

function updateProcessingStatusTotal($shop_name, $shop_id, $total)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'count_product_lazada');
    $redis->set($key, $total, LAZADA_PROCESS_INFO_TIMEOUT);
}

function updateProcessingStatusSaved($shop_name, $shop_id, $saved)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'count_product_lazada_saved');
    $redis->set($key, $saved, LAZADA_PROCESS_INFO_TIMEOUT);
}

function updateProcessingStatusRunning($shop_name, $shop_id, $running = true)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'load_lazada_product_running');
    $redis->set($key, ((bool)$running) ? 1 : 0, LAZADA_PROCESS_INFO_TIMEOUT);
}

function updateProcessingStatusRealRunning($shop_name, $shop_id, $real_running = true)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'load_lazada_product_real_running');
    $redis->set($key, ((bool)$real_running) ? 1 : 0, LAZADA_PROCESS_INFO_TIMEOUT);
}

function isProcessingStatusRealRunning($shop_name, $shop_id)
{
    global $redis;

    $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'load_lazada_product_real_running');
    $real_running = $redis->get($key);

    return $real_running == 1;
}

function updateProduct(DB $db, $db_prefix, array $data)
{
    if (!array_key_exists('item_id', $data) || !$data['item_id']) {
        return;
    }

    $product = $db->query("SELECT `item_id` FROM `{$db_prefix}lazada_product` WHERE `item_id` = '" . $db->escape($data['item_id']) . "'");
    if (isset($product->row['item_id'])) {
        editProduct($db, $db_prefix, $data);
    } else {
        addProduct($db, $db_prefix, $data);
    }
}

function addProduct(DB $db, $db_prefix, array $data)
{
    try {
        $db->query("INSERT INTO `{$db_prefix}lazada_product` 
                                                  SET `shop_id` = " . (int)$data['shop_id'] . ", 
                                                      `item_id` = '" . $db->escape($data['item_id']) . "', 
                                                      `primary_category` = '" . $db->escape($data['primary_category']) . "', 
                                                      `name` = '" . $db->escape($data['name']) . "', 
                                                      `description` = '" . $db->escape($data['description']) . "', 
                                                      `short_description` = '" . $db->escape($data['short_description']) . "', 
                                                      `brand` = '" . $db->escape($data['brand']) . "', 
                                                      `sync_status` = 0, 
                                                      `created_at` = NOW(), `updated_at` = NOW()");
    } catch (Exception $e) {
        return isset($data['item_id']) ? $data['item_id'] : '';
    }

    return $db->getLastId();
}

function editProduct(DB $db, $db_prefix, array $data)
{
    try {
        $db->query("UPDATE `{$db_prefix}lazada_product` 
                                                  SET `primary_category` = '" . $db->escape($data['primary_category']) . "', 
                                                      `name` = '" . $db->escape($data['name']) . "', 
                                                      `description` = '" . $db->escape($data['description']) . "', 
                                                      `short_description` = '" . $db->escape($data['short_description']) . "', 
                                                      `brand` = '" . $db->escape($data['brand']) . "', 
                                                      `updated_at` = NOW() WHERE `item_id` = '" . $db->escape($data['item_id']) . "'");
    } catch (Exception $e) {
        // do something...
    }

    return isset($data['item_id']) ? $data['item_id'] : '';
}

function updateProductVersion(DB $db, $db_prefix, $item_id, $data, $category_id)
{
    if (!array_key_exists('SkuId', $data) || !$data['SkuId'] || !$item_id) {
        return;
    }

    $productVersion = $db->query("SELECT * FROM `{$db_prefix}lazada_product_version` WHERE `item_id` = '" . $db->escape($item_id) . "' AND `sku_id` = " . (int)$data['SkuId']);
    if (isset($productVersion->row['sku_id'])) {
        editProductVersion($db, $db_prefix, $item_id, $data, $category_id);
    } else {
        addProductVersion($db, $db_prefix, $item_id, $data, $category_id);
    }
}

function addProductVersion(DB $db, $db_prefix, $item_id, $data, $category_id)
{
    try {
        $product_weight = isset($data['product_weight']) ? (float)$data['product_weight'] : (float)$data['package_weight'];
        $images = array_filter($data['Images'], function ($item){
            return trim($item);
        });
        $images = empty($images) ? '' : json_encode($images);
        $version_name = getVersionName($db, $db_prefix, $category_id, $data);
        $available = isset($data['Available']) ? $data['Available'] : 1;

        $db->query("INSERT INTO `{$db_prefix}lazada_product_version` 
                                                  SET `item_id` = '" . $db->escape($item_id) . "', 
                                                      `status` = '" . $db->escape($data['Status']) . "', 
                                                      `version_name` = '" . $db->escape($version_name) . "', 
                                                      `quantity` = '" . (int)$data['quantity'] . "', 
                                                      `product_weight` = '" . (float)$product_weight . "', 
                                                      `images` = '" . $db->escape($images) . "', 
                                                      `seller_sku` = '" . $db->escape($data['SellerSku']) . "', 
                                                      `shop_sku` = '" . $db->escape($data['ShopSku']) . "', 
                                                      `url` = '" . $db->escape($data['Url']) . "', 
                                                      `package_width` = '" . (float)$data['package_width'] . "', 
                                                      `package_height` = '" . (float)$data['package_height'] . "', 
                                                      `package_length` = '" . (float)$data['package_length'] . "', 
                                                      `package_weight` = '" . (float)$data['package_weight'] . "', 
                                                      `price` = '" . (float)$data['price'] . "', 
                                                      `available` = '" . (int)$available . "', 
                                                      `sync_status` = 0, 
                                                      `sku_id` = '" . (int)$data['SkuId'] . "'");
    } catch (Exception $e) {
        // do something...
    }
}

function editProductVersion(DB $db, $db_prefix, $item_id, $data, $category_id)
{
    try {
        $product_weight = isset($data['product_weight']) ? (float)$data['product_weight'] : (float)$data['package_weight'];
        $images = array_filter($data['Images'], function ($item){
            return trim($item);
        });
        $images = empty($images) ? '' : json_encode($images);
        $version_name = getVersionName($db, $db_prefix, $category_id, $data);
        $sku_id = (int)$data['SkuId'];
        $available = isset($data['Available']) ? $data['Available'] : 1;

        $db->query("UPDATE `{$db_prefix}lazada_product_version` 
                                                  SET `status` = '" . $db->escape($data['Status']) . "', 
                                                      `version_name` = '" . $db->escape($version_name) . "',
                                                      `quantity` = '" . (int)$data['quantity'] . "', 
                                                      `product_weight` = '" . (float)$product_weight . "', 
                                                      `images` = '" . $db->escape($images) . "', 
                                                      `seller_sku` = '" . $db->escape($data['SellerSku']) . "', 
                                                      `shop_sku` = '" . $db->escape($data['ShopSku']) . "', 
                                                      `url` = '" . $db->escape($data['Url']) . "', 
                                                      `package_width` = '" . (float)$data['package_width'] . "', 
                                                      `package_height` = '" . (float)$data['package_height'] . "', 
                                                      `package_length` = '" . (float)$data['package_length'] . "', 
                                                      `package_weight` = '" . (float)$data['package_weight'] . "', 
                                                      `price` = '" . (float)$data['price'] . "', 
                                                      `available` = '" . (int)$available . "' 
                                                       WHERE `sku_id` = '" . (int)$data['SkuId'] . "' AND `item_id` = '" . $db->escape($item_id) . "'");

        // get shop_id
        $product_lazada = $db->query("SELECT `shop_id` FROM `{$db_prefix}lazada_product` WHERE `item_id` = {$item_id}");
        $lazada_shop_id = isset($product_lazada->row['shop_id']) ? $product_lazada->row['shop_id'] : '';

        if ($lazada_shop_id == '') {
            return;
        }

        $result = $db->query("SELECT `bestme_product_id`, `bestme_product_version_id` 
                                      FROM `{$db_prefix}lazada_product_version` 
                                        WHERE `item_id` = {$item_id} 
                                          AND `sku_id` = {$sku_id}");

        $stock = (int)$data['quantity'];
        $price = (float)$data['price'];
        $original_price = (float)$data['price'];

        if (isset($result->row['bestme_product_id']) && $result->row['bestme_product_id']) {
            $product_veersion_id = isset($result->row['bestme_product_version_id']) ? $result->row['bestme_product_version_id'] : 0;
            updateProductHasLinkProductLazada($db, $db_prefix, (int)$lazada_shop_id, $result->row['bestme_product_id'], $product_veersion_id, $stock, $price, $original_price);
        }
    } catch (Exception $e) {
        // do something...
    }
}

function getVersionName(DB $db, $db_prefix, $category_id, $data)
{
    $category = $db->query("SELECT * FROM `{$db_prefix}lazada_category_attributes` WHERE `lazada_cateogry_id` = " . (int)$category_id);
    if (!isset($category->row['data'])){
        return '';
    }
    $version_name = [];
    $categoryAttribute = $category->row['data'];
    $categoryAttribute = json_decode($categoryAttribute, true);
    foreach ($categoryAttribute as $attribute){
        if (!array_key_exists('is_sale_prop', $attribute) || !array_key_exists('name', $attribute)){
            continue;
        }

        if ($attribute['is_sale_prop'] == 1) {
            if (isset($data[$attribute['name']]) && $data[$attribute['name']]){
                $version_name[] = $data[$attribute['name']];
            }
        }
    }

    return implode(',', $version_name);
}

function deleteProductNotInList($products_list, DB $db, $db_prefix, $shop_id, $shop_name)
{
    if (!is_array($products_list) || empty($products_list)) {
        return;
    }

    $products_list = implode(', ', $products_list);

    try {
        $delete_list = $db->query("SELECT `item_id` FROM `{$db_prefix}lazada_product` WHERE `shop_id` = " . (int)$shop_id . " AND `item_id` NOT IN (" . $products_list . ")");
        $arr_del_list = [];
        foreach ($delete_list->rows as $row) {
            if (!isset($row['item_id'])) {
                continue;
            }
            $arr_del_list[] = $row['item_id'];
        }
        if (empty($arr_del_list)) {
            return;
        }
        $arr_del_list = implode(', ', $arr_del_list);
        $db->query("DELETE FROM `{$db_prefix}lazada_product` WHERE `item_id` IN (" . $arr_del_list . ")");
        $db->query("DELETE FROM `{$db_prefix}lazada_product_version` WHERE `item_id` IN (" . $arr_del_list . ")");
    } catch (Exception $e) {
        println(sprintf('deleteProductNotInList() from lazada sid "%s" for shop "%s"... failed. Error: %s', $shop_id, $shop_name, $e->getMessage()));
    }
}

function deleteProductOldVesion(DB $db, $db_prefix, $item_id, $sku_ids)
{
    if (!is_array($sku_ids) || empty($sku_ids)) {
        return;
    }

    $sku_ids = implode(', ', $sku_ids);

    try {
        $db->query("DELETE FROM `{$db_prefix}lazada_product_version` WHERE `item_id` = '" . $db->escape($item_id) . "' AND `sku_id` NOT IN (" . $sku_ids . ")");
    } catch (Exception $e) {
        //
    }
}

function checkAccessTokenExpired($lazada)
{
    $shoinfo = $lazada->getShopInfo();
    $shoinfo = json_decode($shoinfo, true);
    if (isset($shoinfo['code']) && $shoinfo['code'] == 'IllegalAccessToken') {
        return true;
    }

    return false;
}

function getRefreshToken(DB $db, $db_prefix, $access_token)
{
    $query = $db->query("SELECT `refresh_token` FROM `{$db_prefix}lazada_shop_config` WHERE `access_token` = '" . $db->escape($access_token) . "'");
    if ($query->num_rows > 0) {
        return $query->row['refresh_token'];
    }

    return '';
}

function updateNewAccessToken(DB $db, $db_prefix, $data)
{
    if (!is_array($data) || !array_key_exists('access_token', $data)) {
        return;
    }
    $expires_in = gmdate("Y-m-d H:i:s", (int)$data['expires_in'] + time());
    $refresh_expires_in = gmdate("Y-m-d H:i:s", (int)$data['refresh_expires_in'] + time());
    $db->query("UPDATE `{$db_prefix}lazada_shop_config` SET 
                                        `access_token` = '" . $db->escape($data['access_token']) . "', 
                                        `refresh_token` = '" . $db->escape($data['refresh_token']) . "', 
                                        `expires_in` = '" . $db->escape($expires_in) . "', 
                                        `refresh_expires_in` = '" . $db->escape($refresh_expires_in) . "'");
}

function allowUpdateInventory(DB $db, $db_prefix, $shop_id)
{
    $shop = $db->query("SELECT `allow_update_stock_product` FROM `{$db_prefix}lazada_shop_config` WHERE `id` = {$shop_id}");

    if (isset($shop->row['allow_update_stock_product']) && $shop->row['allow_update_stock_product']) {
        return true;
    }

    return false;
}

function allowUpdatePrice(DB $db, $db_prefix, $shop_id)
{
    $shop = $db->query("SELECT `allow_update_price_product` FROM `{$db_prefix}lazada_shop_config` WHERE `id` = {$shop_id}");

    if (isset($shop->row['allow_update_price_product']) && $shop->row['allow_update_price_product']) {
        return true;
    }

    return false;
}

function updateProductHasLinkProductLazada(DB $db, $db_prefix, $shop_id, $product_bestme_id, $product_bestme_version_id, $stock, $price, $original_price)
{
    try {
        $product = $db->query("SELECT `default_store_id` FROM `{$db_prefix}product` WHERE `product_id` = {$product_bestme_id}");
        $default_store_id = isset($product->row['default_store_id']) ? $product->row['default_store_id'] : 0;
        if ($original_price == $price) {
            $price = 0;
        }

        if ($product_bestme_version_id) {
            if (allowUpdateInventory($db, $db_prefix, $shop_id)) {
                $db->query("UPDATE `{$db_prefix}product_to_store` SET `quantity` = {$stock} 
                                  WHERE `product_id` = {$product_bestme_id} 
                                    AND product_version_id = {$product_bestme_version_id} 
                                    AND `store_id` = {$default_store_id}");
            }

            if (allowUpdatePrice($db, $db_prefix, $shop_id)) {
                $db->query("UPDATE `{$db_prefix}product_version` 
                                  SET `price` = {$price}, 
                                      `compare_price` = {$original_price} 
                                    WHERE `product_id` = {$product_bestme_id} 
                                      AND `product_version_id` = {$product_bestme_version_id}");
            }
        } else {
            if (allowUpdateInventory($db, $db_prefix, $shop_id)) {
                $db->query("UPDATE `{$db_prefix}product_to_store` 
                                  SET `quantity` = {$stock} 
                                    WHERE `product_id` = {$product_bestme_id} 
                                      AND product_version_id = 0 
                                      AND `store_id` = {$default_store_id}");
            }

            if (allowUpdatePrice($db, $db_prefix, $shop_id)) {
                $db->query("UPDATE `{$db_prefix}product` 
                                  SET `price` = {$price}, 
                                      `compare_price` = {$original_price} 
                                    WHERE `product_id` = {$product_bestme_id}");
            }
        }
    } catch (Exception $e) {
        // do something
    }
}

function updateLastTimeSyncProduct(DB $db, $db_prefix, $shop_id)
{
    try {
        $db->query("UPDATE `{$db_prefix}lazada_shop_config` SET `last_sync_time_product` = NOW() WHERE `id` = {$shop_id}");
    } catch (Exception $e) {
        // do something...
    }
}