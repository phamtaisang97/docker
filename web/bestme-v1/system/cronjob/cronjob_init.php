<?php

require_once __DIR__ . './../../multi.config.php';
require_once __DIR__ . './../../admin/config.php';

// Configuration
if (!defined('DIR_STORAGE')) {
    define('DIR_STORAGE', DIR_SYSTEM . 'storage/');
}

if (is_file(DIR_STORAGE . 'vendor/autoload.php')) {
    require_once(DIR_STORAGE . 'vendor/autoload.php');
}

// Modification Override
function modification($filename)
{
    $file = null;

    if (substr($filename, 0, strlen(DIR_SYSTEM)) == DIR_SYSTEM) {
        $file = DIR_MODIFICATION . 'system/' . substr($filename, strlen(DIR_SYSTEM));
    }

    if (is_file($file)) {
        return $file;
    }

    return $filename;
}

function library($class)
{
    $file = DIR_SYSTEM . 'library/' . str_replace('\\', '/', strtolower($class)) . '.php';

    if (is_file($file)) {
        include_once(modification($file));

        return true;
    } else {
        return false;
    }
}

function cronjob($class)
{
    $file = DIR_SYSTEM . 'cronjob/' . str_replace('\\', '/', strtolower($class)) . '.php';

    if (is_file($file)) {
        include_once(modification($file));

        return true;
    } else {
        return false;
    }
}

spl_autoload_register('library');
spl_autoload_register('cronjob');
spl_autoload_extensions('.php');

function println($str)
{
    $now = (new DateTime())->format('YmdHis');

    echo "[$now]$str\n";
}
