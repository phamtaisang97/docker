<?php

const MIGRATION_VERSION_FILE = DIR_SYSTEM . 'library/migration/migration_version.json';

function doCacheData($data, $migration)
{
    // create folder if not exists
    if (!file_exists(DIR_SYSTEM . 'library/migration/data')) {
        mkdir(DIR_SYSTEM . 'library/migration/data', 0777, true);
    }

    // update cache file
    file_put_contents(DIR_SYSTEM . 'library/migration/data/' . $migration . '.json', $data);

    // update migration version in migration_version.json
    update_file_migration_version($migration);
}

function update_file_migration_version($migration)
{
    $versions = [];
    if (file_exists(MIGRATION_VERSION_FILE)) {
        $versions_json = file_get_contents(MIGRATION_VERSION_FILE);
        $versions = json_decode($versions_json, true);
    }

    if (isset($versions[$migration])) {
        $versions[$migration]['version'] += 1;
    } else {
        $versions[$migration] = ['version' => 1];
    }

    file_put_contents(MIGRATION_VERSION_FILE, json_encode($versions));
}