<?php

require_once __DIR__ . './../cronjob_init.php';
require_once __DIR__ . '/cache_migration_init.php';

$migration = 'migration_20201201_shopee_category_and_logistics';

$volumetric_factor = [
    '50010' => '6000',  // Viettel Post
    '50012' => '6000',  // Giao Hàng Tiết Kiệm
    '50015' => '6000',  // VNPost Nhanh
    '50018' => '6000',  // J&T Express
    '50020' => '6000',  // GrabExpress
    '50023' => '6000',  // Ninja Van
    '50024' => '6000',  // BEST Express
    '50011' => '5000',  // Giao Hàng Nhanh
    '50016' => '4000',  // VNPost Tiết Kiệm
    '50022' => '0',     // NowShip
];

$shop_id = SHOPEE_SHOP_ID;
// Call api to get shopee category
$shopee = Sale_Channel::getSaleChannel('shopee', ['shop_id' => $shop_id]);
$categories_en_json = $shopee->getCategory(['language' => 'en']);
$categories_vn_json = $shopee->getCategory(['language' => 'vi']);

$categories_en = json_decode($categories_en_json, true)['categories'];
$categories_vn = json_decode($categories_vn_json, true)['categories'];

$categories_id = array_column($categories_vn, 'category_id');
$categories = array_combine($categories_id, $categories_vn);

foreach ($categories_en as $category_en) {
    $categories[$category_en['category_id']]['name_en'] = $category_en['category_name'];
    $categories[$category_en['category_id']]['name_vi'] = $categories[$category_en['category_id']]['category_name'];
}

$logistics = $shopee->getLogistics(['language' => 'en']);
$logistics = json_decode($logistics, true)['logistics'];

foreach ($logistics as &$logistic) {
    $logistic['volumetric_factor'] = isset($volumetric_factor[$logistic['logistic_id']]) ? $volumetric_factor[$logistic['logistic_id']] : 0;
}

$data = [
    'categories' => $categories,
    'logistics' => $logistics
];

doCacheData(json_encode($data), $migration);