<?php

require_once __DIR__ . './../cronjob_init.php';

const CJ_POLLING_INTERVAL = 2; // in seconds
const CJ_REDIS_TIMEOUT = 30; // in seconds
const CJ_PROCESS_KEY = 'PULL_IMAGES_FROM_CLOUDINARY_PROCESS'; // in seconds

// redis job key
const CJ_JOB_KEY_DB_HOSTNAME = 'db_hostname';
const CJ_JOB_KEY_DB_PORT = 'db_port';
const CJ_JOB_KEY_DB_USERNAME = 'db_username';
const CJ_JOB_KEY_DB_PASSWORD = 'db_password';
const CJ_JOB_KEY_DB_DATABASE = 'db_database';
const CJ_JOB_KEY_DB_PREFIX = 'db_prefix';
const CJ_JOB_KEY_DB_DRIVER = 'db_driver';
const CJ_JOB_KEY_SHOP_NAME = 'shop_name';
const CJ_JOB_KEY_TASK = 'task';
// all tasks
const CJ_JOB_TASK_PULL_IMAGES = 'PULL_IMAGES_FROM_CLOUDINARY_V2';

// global vars
global $redis;
global $image_pulled; // array('old_url'=>'new_url', ...)

$processing = true;

/* connect to Redis host */
println(sprintf("Connecting to Redis host %s at port %s...", MUL_REDIS_HOST, MUL_REDIS_PORT));
$redis = new Redis();
try {
    $redis->connect(MUL_REDIS_HOST, MUL_REDIS_PORT, CJ_REDIS_TIMEOUT);
    $redis->select(MUL_REDIS_DB_BESTME_IMAGE);
} catch (Exception $e) {
    println(sprintf("Connecting to Redis host %s at port %s... failed! Error: %s", MUL_REDIS_HOST, MUL_REDIS_PORT, $e->getMessage()));
    return;
}
println(sprintf("Connecting to Redis host %s at port %s... connected!", MUL_REDIS_HOST, MUL_REDIS_PORT));

// check max process
if (!checkMaxProcess()) {
    println('Maximum number of pull images form cloudinary processes is ' . BESTME_IMAGE_JOB_MAX_PROCESS);
    return;
}
$redis->incr(CJ_PROCESS_KEY);
// end check max process

/* get job from Redis - forever until killed */
while ($processing) {
    $job_raw = $redis->rPop(BESTME_IMAGE_JOB_REDIS_QUEUE_V2);
    println(BESTME_IMAGE_JOB_REDIS_QUEUE_V2 . ': Got raw job: ' . $job_raw);

    $job = json_decode($job_raw, true);

    // data example
    /*$job = [
        CJ_JOB_KEY_DB_HOSTNAME => DB_HOSTNAME,
        CJ_JOB_KEY_DB_PORT => DB_PORT,
        CJ_JOB_KEY_DB_USERNAME => DB_USERNAME,
        CJ_JOB_KEY_DB_PASSWORD => DB_PASSWORD,
        CJ_JOB_KEY_DB_DATABASE => DB_DATABASE,
        CJ_JOB_KEY_DB_PREFIX => DB_PREFIX,
        CJ_JOB_KEY_DB_DRIVER => DB_DRIVER,
        CJ_JOB_KEY_SHOP_NAME => '',
        CJ_JOB_KEY_TASK => CJ_JOB_TASK_PULL_IMAGES
    ];*/

    if (!is_array($job)) {
        // break for new running if no job!
        println('No pull image job found! Exit.');
        $processing = false;
        break;
    }

    try {
        println('Found new pull image job! Now run.');
        handleJob($job);
    } catch (Exception $e) {
        println(sprintf('Exception while handling job "%s". Error: %s', $job_raw, $e->getMessage()));
    }

    $image_pulled = array();
    sleep(BESTME_IMAGE_CJ_POLLING_INTERVAL);
}

$redis->decr(CJ_PROCESS_KEY);
$redis->close();
return;
/**
 * handle Job
 *
 * @param array $job
 * @return bool
 */
function handleJob(array $job)
{
    if (!isset($job[CJ_JOB_KEY_DB_HOSTNAME]) ||
        !isset($job[CJ_JOB_KEY_DB_PORT]) ||
        !isset($job[CJ_JOB_KEY_DB_USERNAME]) ||
        !isset($job[CJ_JOB_KEY_DB_PASSWORD]) ||
        !isset($job[CJ_JOB_KEY_DB_DATABASE]) ||
        !isset($job[CJ_JOB_KEY_DB_PREFIX]) ||
        !isset($job[CJ_JOB_KEY_DB_DRIVER]) ||
        !isset($job[CJ_JOB_KEY_SHOP_NAME]) ||
        !isset($job[CJ_JOB_KEY_TASK])
    ) {
        println('Missing either hostname or port or username or password or database or prefix or shop_name or task');
        return false;
    }

    /* Read info from job */
    $db_hostname = $job[CJ_JOB_KEY_DB_HOSTNAME];
    $db_port = $job[CJ_JOB_KEY_DB_PORT];
    $db_username = $job[CJ_JOB_KEY_DB_USERNAME];
    $db_password = $job[CJ_JOB_KEY_DB_PASSWORD];
    $db_database = $job[CJ_JOB_KEY_DB_DATABASE];
    $db_prefix = $job[CJ_JOB_KEY_DB_PREFIX];
    $db_driver = $job[CJ_JOB_KEY_DB_DRIVER];
    $shop_name = $job[CJ_JOB_KEY_SHOP_NAME];
    $task = $job[CJ_JOB_KEY_TASK];

    /* handle task */
    switch ($task) {
        case CJ_JOB_TASK_PULL_IMAGES:
            doPullImages($db_hostname, $db_port, $db_username, $db_password, $db_database, $db_prefix, $db_driver, $shop_name);

            break;
        // and more case...
    }
    println(sprintf('Handling task "%s"... done!', $task));

    return true;
}

/**
 * do Pull Images
 *
 * @param string $db_hostname
 * @param string $db_port
 * @param string $db_username
 * @param string $db_password
 * @param string $db_database
 * @param string $db_prefix
 * @param string $db_driver
 * @param string $shop_name
 */
function doPullImages($db_hostname, $db_port, $db_username, $db_password, $db_database, $db_prefix, $db_driver, $shop_name)
{
    println(sprintf('Pull images for shop "%s"...', $shop_name));

    $db = null;
    try {
        $db = new DB($db_driver,
            htmlspecialchars_decode($db_hostname),
            htmlspecialchars_decode($db_username),
            htmlspecialchars_decode($db_password),
            htmlspecialchars_decode($db_database),
            htmlspecialchars_decode($db_port)
        );
    } catch (Exception $e) {
        println(sprintf('Pull images for shop "%s"... failed. Error: %s', $shop_name, $e->getMessage()));
        return;
    }

    global $image_pulled;
    $image_pulled = array();
    pullImageInList($db, $db_prefix, $shop_name);
    println(sprintf('Pull list images for shop "%s" done!', $shop_name));

    pullImageInProductThumb($db, $db_prefix, $shop_name);
    println(sprintf('Pull product thumb images for shop "%s" done!', $shop_name));

    pullImageInProductImages($db, $db_prefix, $shop_name);
    println(sprintf('Pull product images images for shop "%s" done!', $shop_name));

    pullImageProductDescription($db, $db_prefix, $shop_name);
    println(sprintf('Pull product description images for shop "%s" done!', $shop_name));

    pullImageInCollection($db, $db_prefix, $shop_name);
    println(sprintf('Pull collection thumb images for shop "%s" done!', $shop_name));

    pullImageInProductCategory($db, $db_prefix, $shop_name);
    println(sprintf('Pull category thumb images for shop "%s" done!', $shop_name));

    pullImageInUser($db, $db_prefix, $shop_name);
    println(sprintf('Pull user avatar images for shop "%s" done!', $shop_name));

    pullImageInBlog($db, $db_prefix, $shop_name);
    println(sprintf('Pull blog thumb images for shop "%s" done!', $shop_name));

    pullImageInBuilder($db, $db_prefix, $shop_name);
    println(sprintf('Pull builder images for shop "%s" done!', $shop_name));

    println(sprintf('Pull images for shop "%s"... done. Total: %s', $shop_name, count($image_pulled)));
}

function pullImageInList(DB $db, $db_prefix, $shop_name, $limit = 100, $offset = 0)
{
    $list_images = $db->query("SELECT `image_id`, `url` FROM `{$db_prefix}images` WHERE `type` = 'image' AND `url` LIKE '%res.cloudinary.com%' ORDER BY `image_id` LIMIT {$limit} OFFSET {$offset}");

    foreach ($list_images->rows as $image) {
        if (!array_key_exists('image_id', $image) || !array_key_exists('url', $image)) {
            continue;
        }

        $image_query_object = [
            'db' => $db,
            'db_prefix' => $db_prefix,
            'image_id' => $image['image_id']
        ];
        $new_url = uploadToBestmeServer($image['url'], $shop_name, $image_query_object);
        if ($new_url) {
            $db->query("UPDATE `{$db_prefix}images` SET `url` = '$new_url', `source` = 'bestme' WHERE `image_id` = " . $image['image_id']);
        }
    }

    if ($list_images->num_rows >= $limit) {
        pullImageInList($db, $db_prefix, $shop_name, $limit, $offset + $limit);
    }
}

function pullImageInProductThumb(DB $db, $db_prefix, $shop_name, $limit = 100, $offset = 0)
{

    $product_thumbs = $db->query("SELECT `product_id`, `image` FROM `{$db_prefix}product` WHERE `deleted` IS NULL AND `image` LIKE '%res.cloudinary.com%' ORDER BY `product_id` LIMIT {$limit} OFFSET {$offset}");

    foreach ($product_thumbs->rows as $image) {
        if (!array_key_exists('product_id', $image) || !array_key_exists('image', $image)) {
            continue;
        }

        $new_url = uploadToBestmeServer($image['image'], $shop_name);
        if ($new_url) {
            $db->query("UPDATE `{$db_prefix}product` SET `image` = '$new_url' WHERE `product_id` = " . $image['product_id']);
        }
    }

    if ($product_thumbs->num_rows >= $limit) {
        pullImageInProductThumb($db, $db_prefix, $shop_name, $limit, $offset + $limit);
    }
}

function pullImageInProductImages(DB $db, $db_prefix, $shop_name, $limit = 100, $offset = 0)
{

    $product_images = $db->query("SELECT `product_image_id`, `image` FROM `{$db_prefix}product_image` WHERE `image` LIKE '%res.cloudinary.com%' ORDER BY `product_image_id` LIMIT {$limit} OFFSET {$offset}");

    foreach ($product_images->rows as $image) {
        if (!array_key_exists('product_image_id', $image) || !array_key_exists('image', $image)) {
            continue;
        }

        $new_url = uploadToBestmeServer($image['image'], $shop_name);
        if ($new_url) {
            $db->query("UPDATE `{$db_prefix}product_image` SET `image` = '$new_url' WHERE `product_image_id` = " . $image['product_image_id']);
        }
    }

    if ($product_images->num_rows >= $limit) {
        pullImageInProductImages($db, $db_prefix, $shop_name, $limit, $offset + $limit);
    }
}
function pullImageProductDescription(DB $db, $db_prefix, $shop_name, $limit = 100, $offset = 0) {
    $product_images = $db->query("SELECT `product_id`, `description` FROM `{$db_prefix}product_description` ORDER BY `product_id` LIMIT {$limit} OFFSET {$offset}");

    foreach($product_images->rows as $image){
        if (!array_key_exists('product_id', $image) || !array_key_exists('description', $image)) {
            continue;
        }
        $pattern = '(https:(\\\/\\\/|\/\/)res.cloudinary[^\s]+(jpg|jpeg|png|tiff)\b)';
        $content = $image['description'];
        $match = preg_match_all($pattern,$content,$matches);
        if(!empty($matches[0])) {
            foreach($matches[0] as $image_to_change) {
                println(sprintf('Pull image product description "%s" !', $image_to_change));
                $new_url = uploadToBestmeServer($image_to_change, $shop_name);
                if($new_url) {
                    $new_content = str_replace($image_to_change, $new_url, $content);
                    $content = $new_content ;
                }
            }
            $db->query("UPDATE `{$db_prefix}product_description` SET `description` = '$content' WHERE `product_id` = " . $image['product_id']);
        }
    }

    if ($product_images->num_rows >= $limit) {
        pullImageProductDescription($db, $db_prefix, $shop_name, $limit, $offset + $limit);
    }
}

function pullImageInCollection(DB $db, $db_prefix, $shop_name, $limit = 100, $offset = 0)
{
    $collection_icons = $db->query("SELECT `collection_id`, `image` FROM `{$db_prefix}collection_description` WHERE `image` LIKE '%res.cloudinary.com%' ORDER BY `collection_id` LIMIT {$limit} OFFSET {$offset}");

    foreach ($collection_icons->rows as $image) {
        if (!array_key_exists('collection_id', $image) || !array_key_exists('image', $image)) {
            continue;
        }

        $new_url = uploadToBestmeServer($image['image'], $shop_name);
        if ($new_url) {
            $db->query("UPDATE `{$db_prefix}collection_description` SET `image` = '$new_url' WHERE `collection_id` = " . $image['collection_id']);
        }
    }

    if ($collection_icons->num_rows >= $limit) {
        pullImageInCollection($db, $db_prefix, $shop_name, $limit, $offset + $limit);
    }
}

function pullImageInProductCategory(DB $db, $db_prefix, $shop_name, $limit = 100, $offset = 0)
{
    $category_icons = $db->query("SELECT `category_id`, `image` FROM `{$db_prefix}category` WHERE `image` LIKE '%res.cloudinary.com%' ORDER BY `category_id` LIMIT {$limit} OFFSET {$offset}");

    foreach ($category_icons->rows as $image) {
        if (!array_key_exists('category_id', $image) || !array_key_exists('image', $image)) {
            continue;
        }

        $new_url = uploadToBestmeServer($image['image'], $shop_name);
        if ($new_url) {
            $db->query("UPDATE `{$db_prefix}category` SET `image` = '$new_url' WHERE `category_id` = " . $image['category_id']);
        }
    }

    if ($category_icons->num_rows >= $limit) {
        pullImageInProductCategory($db, $db_prefix, $shop_name, $limit, $offset + $limit);
    }
}

function pullImageInUser(DB $db, $db_prefix, $shop_name, $limit = 100, $offset = 0)
{
    $user_icons = $db->query("SELECT `user_id`, `image` FROM `{$db_prefix}user` WHERE `image` LIKE '%res.cloudinary.com%' ORDER BY `user_id` LIMIT {$limit} OFFSET {$offset}");

    foreach ($user_icons->rows as $image) {
        if (!array_key_exists('user_id', $image) || !array_key_exists('image', $image)) {
            continue;
        }

        $new_url = uploadToBestmeServer($image['image'], $shop_name);
        if ($new_url) {
            $db->query("UPDATE `{$db_prefix}user` SET `image` = '$new_url' WHERE `user_id` = " . $image['user_id']);
        }
    }

    if ($user_icons->num_rows >= $limit) {
        pullImageInUser($db, $db_prefix, $shop_name, $limit, $offset + $limit);
    }
}

function pullImageInBlog(DB $db, $db_prefix, $shop_name, $limit = 100, $offset = 0)
{
    $blog_thumbs = $db->query("SELECT `blog_id`, `image` FROM `{$db_prefix}blog_description` WHERE `image` LIKE '%res.cloudinary.com%' ORDER BY `blog_id` LIMIT {$limit} OFFSET {$offset}");

    foreach ($blog_thumbs->rows as $image) {
        if (!array_key_exists('blog_id', $image) || !array_key_exists('image', $image)) {
            continue;
        }

        $new_url = uploadToBestmeServer($image['image'], $shop_name);
        if ($new_url) {
            $db->query("UPDATE `{$db_prefix}blog_description` SET `image` = '$new_url' WHERE `blog_id` = " . $image['blog_id']);
        }
    }

    if ($blog_thumbs->num_rows >= $limit) {
        pullImageInBlog($db, $db_prefix, $shop_name, $limit, $offset + $limit);
    }
}

function pullImageInBuilder(DB $db, $db_prefix, $shop_name)
{
    // header logo
    $header_dataes = $db->query("SELECT `id`, `config` FROM `{$db_prefix}theme_builder_config` WHERE `key` = 'config_section_header'");
    foreach ($header_dataes->rows as $header_data) {
        if (!array_key_exists('id', $header_data) || !array_key_exists('config', $header_data)) {
            continue;
        }
        $data = $header_data['config'];
        $data = json_decode($data, true);
        if (isset($data['logo']['url'])) {
            $new_url = uploadToBestmeServer($data['logo']['url'], $shop_name);
            if ($new_url) {
                $data['logo']['url'] = $new_url;
                $data = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG);
                $data = $db->escape($data);
                $db->query("UPDATE `{$db_prefix}theme_builder_config` SET `config` = '$data' WHERE `id` = " . $header_data['id']);
            }
        }
    }
    unset($data, $new_url, $header_dataes);

    // slide show
    $slide_show_datas = $db->query("SELECT `id`, `config` FROM `{$db_prefix}theme_builder_config` WHERE `key` = 'config_section_slideshow'");
    foreach ($slide_show_datas->rows as $slide_show_data) {
        if (!array_key_exists('id', $slide_show_data) || !array_key_exists('config', $slide_show_data)) {
            continue;
        }
        $data = $slide_show_data['config'];
        $data = json_decode($data, true);
        if (isset($data['display'])) {
            if (is_array($data['display'])) {
                $flag = false;
                foreach ($data['display'] as &$image) {
                    if (isset($image['image-url'])) {
                        $new_url = uploadToBestmeServer($image['image-url'], $shop_name);
                        if ($new_url) {
                            $flag = true;
                            $image['image-url'] = $new_url;
                        }
                    }
                }
                unset($image);
                if ($flag) {
                    $data = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG);
                    $data = $db->escape($data);
                    $db->query("UPDATE `{$db_prefix}theme_builder_config` SET `config` = '$data' WHERE `id` = " . $slide_show_data['id']);
                }
            }
        }
    }
    unset($data, $new_url, $flag, $slide_show_data, $slide_show_datas);

    // banner
    $banner_datas = $db->query("SELECT `id`, `config` FROM `{$db_prefix}theme_builder_config` WHERE `key` = 'config_section_banner'");
    foreach ($banner_datas->rows as $banner_data) {
        if (!array_key_exists('id', $banner_data) || !array_key_exists('config', $banner_data)) {
            continue;
        }
        $data = $banner_data['config'];
        $data = json_decode($data, true);
        if (isset($data['display'])) {
            if (is_array($data['display'])) {
                $flag = false;
                foreach ($data['display'] as &$image) {
                    if (isset($image['image-url'])) {
                        $new_url = uploadToBestmeServer($image['image-url'], $shop_name);
                        if ($new_url) {
                            $flag = true;
                            $image['image-url'] = $new_url;
                        }
                    }
                }
                unset($image);
                if ($flag) {
                    $data = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG);
                    $data = $db->escape($data);
                    $db->query("UPDATE `{$db_prefix}theme_builder_config` SET `config` = '$data' WHERE `id` = " . $banner_data['id']);
                }
            }
        }
    }
    unset($data, $new_url, $flag, $banner_data, $banner_datas);

    // partner
    $partner_datas = $db->query("SELECT `id`, `config` FROM `{$db_prefix}theme_builder_config` WHERE `key` = 'config_section_partner'");
    foreach ($partner_datas->rows as $partner_data) {
        if (!array_key_exists('id', $partner_data) || !array_key_exists('config', $partner_data)) {
            continue;
        }
        $data = $partner_data['config'];
        $data = json_decode($data, true);
        if (isset($data['display'])) {
            if (is_array($data['display'])) {
                $flag = false;
                foreach ($data['display'] as &$image) {
                    if (isset($image['image-url'])) {
                        $new_url = uploadToBestmeServer($image['image-url'], $shop_name);
                        if ($new_url) {
                            $flag = true;
                            $image['image-url'] = $new_url;
                        }
                    }
                }
                unset($image);
                if ($flag) {
                    $data = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG);
                    $data = $db->escape($data);
                    $db->query("UPDATE `{$db_prefix}theme_builder_config` SET `config` = '$data' WHERE `id` = " . $partner_data['id']);
                }
            }
        }
    }
    unset($data, $new_url, $flag, $partner_data, $partner_datas);

    // list product banner
    $list_product_banner_dataes = $db->query("SELECT `id`, `config` FROM `{$db_prefix}theme_builder_config` WHERE `key` = 'config_section_category_banner'");
    foreach ($list_product_banner_dataes->rows as $list_product_banner_data) {
        if (!array_key_exists('id', $list_product_banner_data) || !array_key_exists('config', $list_product_banner_data)) {
            continue;
        }
        $data = $list_product_banner_data['config'];
        $data = json_decode($data, true);
        if (isset($data['display'])) {
            if (is_array($data['display'])) {
                $flag = false;
                foreach ($data['display'] as &$image) {
                    if (isset($image['image-url'])) {
                        $new_url = uploadToBestmeServer($image['image-url'], $shop_name);
                        if ($new_url) {
                            $flag = true;
                            $image['image-url'] = $new_url;
                        }
                    }
                }
                unset($image);
                if ($flag) {
                    $data = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG);
                    $data = $db->escape($data);
                    $db->query("UPDATE `{$db_prefix}theme_builder_config` SET `config` = '$data' WHERE `id` = " . $list_product_banner_data['id']);
                }
            }
        }
    }
    unset($data, $new_url, $flag, $list_product_banner_data, $list_product_banner_dataes);
}

function uploadToBestmeServer($url, $shop_name, $image_query_object = [])
{
    $url = trim($url);
    $shop_name = trim($shop_name);
    if (!$url) {
        return false;
    }

    global $image_pulled;
    if (array_key_exists($url, $image_pulled)) {
        return $image_pulled[$url];
    }

    if (filter_var($url, FILTER_VALIDATE_URL) == FALSE) {
        return false;
    }

    if (!isCloudinaryImage($url)) {
        return false;
    }

    $fields = array(
        "shop_name" => $shop_name,
        "image_url" => $url
    );

    /** @var \Image\Bestme_Image_Manager $image_server */
    $image_server = Image_Server::getImageServer('bestme');
    $result = $image_server->upload_by_url($fields);

    if (array_key_exists('url', $result)) {
        if (trim($result['url'])) {
            $image_pulled[$url] = $result['url'];

            // update image size in images table
            if (!empty($image_query_object)) {
                $image_query_object['db']->query("UPDATE `{$image_query_object['db_prefix']}images` 
                                                  SET `size` = {$result['size']} 
                                                  WHERE `image_id` = " . $image_query_object['image_id']);
            }

            return $result['url'];
        }
    }

    return false;
}

function isCloudinaryImage($url)
{
    if (strpos($url, '//res.cloudinary.com/') !== false) {
        return true;
    }

    return false;
}

function checkMaxProcess()
{
    global $redis;
    if (!$redis->exists(CJ_PROCESS_KEY)) {
        $redis->set(CJ_PROCESS_KEY, 0);
        $count_process = 0;
    } else {
        $count_process = $redis->get(CJ_PROCESS_KEY);
    }

    if ($count_process >= BESTME_IMAGE_JOB_MAX_PROCESS) {
        return false;
    }

    return true;
}