<?php


namespace Async_Callback\Handler;

interface Async_Callback_Handler_Interface
{
    /**
     * @param array $data
     * @return mixed
     */
    public function process(array $data);

    /**
     * @param string $source
     * @return bool
     */
    public function isSupported($source);
}
