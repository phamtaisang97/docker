<?php


namespace Async_Callback\Handler;

use Config;
use Log;

class Chatbot_Novaon_V2_Handler implements Async_Callback_Handler_Interface
{
    public static $SOURCE = 'AC_ChatbotNovaonV2';

    /**
     * @var Log
     */
    protected $log;

    public function __construct()
    {
        $this->initLog();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function process(array $data)
    {
        if (!defined('ASYNC_CALLBACK_CHATBOT_V2_ORDER_NOTIFY_URL')) {
            return false;
        }

        $this->log('Chatbot_Novaon_V2_Handler: callback [POST] ' . ASYNC_CALLBACK_CHATBOT_V2_ORDER_NOTIFY_URL . '...');
        $url = ASYNC_CALLBACK_CHATBOT_V2_ORDER_NOTIFY_URL;
        $curl = curl_init();
        $post_data = json_encode($data);

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $post_data,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($post_data),
            ),
        ));

        $resp = curl_exec($curl);
        curl_close($curl);

        //$response = json_decode($resp, true);
        try {
            $this->log('Chatbot_Novaon_V2_Handler: got response: ' . $resp);
        } catch (\Exception $e) {
            $this->log('Chatbot_Novaon_V2_Handler: could not print response');
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function isSupported($source)
    {
        return $source === self::$SOURCE;
    }

    private function initLog()
    {
        $config = new Config();
        $config->load('default');
        $config->load($application_config = 'admin');
        $this->log = new Log($config->get('error_filename'));
    }

    /**
     * @param string $msg
     * @param bool $also_print
     */
    private function log($msg, $also_print = true)
    {
        $this->log->write($msg);

        if ($also_print) {
            echo "$msg\n";
        }
    }
}
