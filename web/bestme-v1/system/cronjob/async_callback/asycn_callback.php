<?php

use Async_Callback\Handler\Async_Callback_Handler_Interface;

require_once __DIR__ . './../cronjob_init.php';

const CJ_POLLING_INTERVAL = 2; // in seconds
const CJ_REDIS_TIMEOUT = 30; // in seconds

// all tasks
global $CJ_JOB_HANDLERS;
$CJ_JOB_HANDLERS = [
    new \Async_Callback\Handler\Chatbot_Novaon_V2_Handler()
];

// global vars
global $redis;

$processing = true;

/* handle kill signal */
// TODO...

/* connect to Redis host */
$redis = getRedis();
if (!$redis) {
    return;
}

/* get job from Redis - forever until killed */
while ($processing) {
    $job_raw = $redis->rPop(ASYCN_CALLBACK_JOB_REDIS_QUEUE);
    $job = json_decode($job_raw, true);

    // debug
    /*$job = [
        'source' => 'AC_ChatbotNovaonV2',
        'data' => [
            'order_id' => 123,
            'data' => []
        ]
    ];*/

    if (!is_array($job)) {
        // break for new running if no job!
        println('No AsyncCallback job found! Exit.');
        $processing = false;
        break;
    }

    try {
        println("Found new AsyncCallback job! Job data: {$job_raw} . Now run.");
        foreach ($job as $datum) {
            if (!is_array($datum)) {
                continue;
            }

            handleJob($datum);
        }
    } catch (Exception $e) {
        println(sprintf('Exception while handling job "%s". Error: %s', $job_raw, $e->getMessage()));
    }

    sleep(CJ_POLLING_INTERVAL);
}

// TODO: need close?...
$redis->close();

/**
 * @return null|Redis
 */
function getRedis()
{
    $redis = null;
    println(sprintf("Connecting to Redis host %s at port %s...", MUL_REDIS_HOST, MUL_REDIS_PORT));

    try {
        $redis = new Redis();
        $redis->connect(MUL_REDIS_HOST, MUL_REDIS_PORT);
        $redis->select(ASYNC_CALLBACK_DB);
        $redis->setOption(Redis::OPT_READ_TIMEOUT, CJ_REDIS_TIMEOUT);

        println(sprintf("Connecting to Redis host %s at port %s... connected!", MUL_REDIS_HOST, MUL_REDIS_PORT));
    } catch (Exception $e) {
        $redis = null;
        println(sprintf("Connecting to Redis host %s at port %s... Redis error: ", MUL_REDIS_HOST, MUL_REDIS_PORT, $e->getMessage()));
    }

    return $redis;
}

/**
 * handle Job
 *
 * @param array $data
 * @return bool
 */
function handleJob(array $data)
{
    global $CJ_JOB_HANDLERS;
    foreach ($CJ_JOB_HANDLERS as $handler) {
        /** @var Async_Callback_Handler_Interface $handler */
        if (!isset($data['source']) ||
            !$handler->isSupported($data['source']) ||
            !isset($data['data']) || !is_array($data['data'])) {
            continue;
        }

        println(sprintf('Handling task "%s"... done!', $data['source']));
        $handler->process($data['data']);
        println(sprintf('Handling task "%s"... done!', $data['source']));
    }

    return true;
}