<?php

trait Discount_Util_Trait
{
    public static $ERR_CODE_SUCCESS = 0;
    public static $ERR_CODE_INVALID_CONFIG = 100;
    public static $ERR_CODE_SAME_PRODUCT = 101;
    public static $ERR_CODE_SAME_CATEGORY = 102;
    public static $ERR_CODE_SAME_MANUFACTURER = 103;
    public static $ERR_CODE_SAME_ORDER = 104;
    public static $ERR_CODE_UNKNOWN = 999;

    public static $ERR_MSG_MAP = [
        0 => "validate_success",
        100 => "validate_invalid_config",
        101 => "validate_same_product",
        102 => "validate_same_category",
        103 => "validate_same_manufacturer",
        104 => "validate_same_order",
        999 => "validate_unknown_error",
    ];

    /**
     * @param array $data , format as:
     * [
     *     "discount_id": 1 | null,
     *     "discount_type_id": 1,
     *     "discount_status_id": 1,
     *     "config": [ ... ],
     *     "start_at": <Y-m-d H:i:s>,
     *     "end_at": <Y-m-d H:i:s>,
     * ]
     * @return array, format as:
     * [
     *     "code" => ...,
     *     "message" => ...
     * ]
     */
    public function validateDiscount(array $data)
    {
        $this->load->language('discount/discount');
        $this->load->model('discount/discount');
        $this->load->model('discount/discount_type');

        if (empty($data) ||
            !array_key_exists('discount_type_id', $data) ||
            !array_key_exists('start_at', $data) ||
            !array_key_exists('end_at', $data) ||
            !isset($data['config']) || !is_array($data['config'])
        ) {
            return $this->discountConfigEmpty();
        }

        $discount_id = isset($data['discount_id']) ? $data['discount_id'] : 0;
        $config = $data['config'];
        $start_at = $data['start_at'];
        $end_at = $data['end_at'];
        $error = [];

        if ($data['discount_type_id']) {
            switch ($data['discount_type_id']) {
                case ModelDiscountDiscountType::TYPE_PRODUCT:
                    $result = $this->validateDiscountTypeProduct($config, $start_at, $end_at, $discount_id);
                    if (isset($result['code']) && $result['code'] !== self::$ERR_CODE_SUCCESS) {
                        $error = $result;
                    }
                    break;

                case ModelDiscountDiscountType::TYPE_CATEGORY:
                    $result = $this->validateDiscountTypeCategory($config, $start_at, $end_at, $discount_id);
                    if (isset($result['code']) && $result['code'] !== self::$ERR_CODE_SUCCESS) {
                        $error = $result;
                    }
                    break;

                case ModelDiscountDiscountType::TYPE_MANUFACTURER:
                    $result = $this->validateDiscountTypeManufacturer($config, $start_at, $end_at, $discount_id);
                    if (isset($result['code']) && $result['code'] !== self::$ERR_CODE_SUCCESS) {
                        $error = $result;
                    }
                    break;

                case ModelDiscountDiscountType::TYPE_ORDER:
                    $result = $this->validateDiscountTypeOrder($config, $start_at, $end_at, $discount_id);
                    if (isset($result['code']) && $result['code'] !== self::$ERR_CODE_SUCCESS) {
                        $error = $result;
                    }
                    break;
                default:
                    break;
            }
        }

        if (!empty($error)) {
            return $error;
        }

        return $this->discountConfigValid();
    }

    /**
     * @param array $products_info such as
     * [
     *     [$product_id => 10, $product_version_id => 1, $quantity => 1, $total => 10000],
     *     [$product_id => 12, $product_version_id => null, $quantity => 1, $total => 10000],
     *     ...
     * ]
     * @return array
     */
    public function getSatisfiedDiscountsForProduct($products_info)
    {
        if (!is_array($products_info))  {
            return [];
        }

        $result = [];

        // append categories and manufacturers to products_info
        $this->load->model('catalog/product');
        $this->load->model('catalog/manufacturer');
        $this->load->model('discount/discount');
        foreach ($products_info as &$product_info) {
            if (!array_key_exists('product_id', $product_info)) {
                $product_info['categories'] = [];
                continue;
            }

            $categoriesIds = $this->model_catalog_product->getCategoryIdsByProductId($product_info['product_id']);
            $product_info['categories'] = array_map(function ($cat) {
                return isset($cat['category_id']) ? $cat['category_id'] : -1;
            }, $categoriesIds);

            $manufacturerId = $this->model_catalog_product->getManufactureIdByProductId($product_info['product_id']);
            $product_info['manufacturers'] = [$manufacturerId];
        };
        unset($product_info);

        // getting satisfied Discounts for products
        foreach ($products_info as $product_info) {
            if (!array_key_exists('product_id', $product_info) ||
                !array_key_exists('quantity', $product_info)
            ) {
                continue;
            }

            $is_multi_versions = (isset($product_info['product_version_id']) && !empty($product_info['product_version_id']));
            $key = !$is_multi_versions
                ? $product_info['product_id']
                : sprintf('%s-%s', $product_info['product_id'], $product_info['product_version_id']);

            $related_discounts = [];
            if (empty($related_discounts) || !is_array($related_discounts)) {
                continue;
            }

            $satisfied_discounts = [];
            $selected_discounts = [];

            /*
             * format as: [
             *     <discount id> => [ <calc_discount 1>, <calc_discount 2>, ... ]
             * ]
             * e.g:
             * [
             *     12 => [ 12000, 10000, ... ]
             * ]
             */
            $satisfied_discounts_to_conditions = [];

            /* get satisfied discount */
            foreach ($related_discounts as $related_discount) {
                // parse discount array data to discount obj
                $discount_obj = \Model\Discount_Model::fromData($related_discount);
                if (!$discount_obj) {
                    continue; // next discount
                }

                // except not for product
                if (!in_array($discount_obj->getDiscountTypeId(), [
                    ModelDiscountDiscountType::TYPE_PRODUCT,
                    ModelDiscountDiscountType::TYPE_CATEGORY,
                    ModelDiscountDiscountType::TYPE_MANUFACTURER
                ])) {
                    continue; // next discount
                }

                $discount_config = $discount_obj->getConfig();
                $satisfied_discounts_to_conditions[$discount_obj->getDiscountId()] = [];

                // for product
                if ($discount_obj->getDiscountTypeId() == ModelDiscountDiscountType::TYPE_PRODUCT) {
                    $found = false;
                    foreach ($discount_config as $dc) {
                        if (!$dc instanceof \Model\Discount_Config_Product_Model) {
                            continue; // next discount config
                        }

                        if ($dc->getAllProduct() == 0) {
                            if ($dc->getProductId() != $product_info['product_id']) {
                                continue; // next discount config
                            }

                            // if multi version
                            if ($is_multi_versions && !in_array($product_info['product_version_id'], $dc->getProductVersionIds())) {
                                continue; // next discount config
                            }
                        }

                        // check condition
                        $conditions = $dc->getConditions();
                        foreach ($conditions as $condition) {
                            /** @var \Model\Discount_Condition_Quantity_Model $condition */
                            if (extract_number($condition->getMinQuantity()) > $product_info['quantity'] || extract_number($condition->getMaxQuantity()) < $product_info['quantity']) {
                                continue; // next discount condition
                            }

                            $found = true;

                            // calculate discount value
                            $satisfied_discounts_to_conditions[$discount_obj->getDiscountId()][] = $this->calcDiscountDueToConditionForProduct($condition->getType(), $condition->getDiscount(), $product_info['quantity'], $product_info['total']);
                        }
                    }

                    if ($found) {
                        $satisfied_discounts[] = $related_discount;
                    }

                    continue; // break to next discount
                }

                // for category
                if ($discount_obj->getDiscountTypeId() == ModelDiscountDiscountType::TYPE_CATEGORY) {
                    $found = false;
                    $products_in_config = [];

                    foreach ($discount_config as $dc) {
                        if (!$dc instanceof \Model\Discount_Config_Category_Model) {
                            continue; // break to next discount config
                        }

                        if ($dc->getAllCategory() == 1) {
                            $products_in_config = $products_info;
                        } else {
                            foreach ($products_info as $pi) {
                                if (!array_key_exists('categories', $pi) ||
                                    !is_array($pi['categories'])
                                ) {
                                    continue;
                                }

                                // support category multi levels
                                if (!in_array($dc->getCategoryId(), $pi['categories'])) {
                                    $flag = false;
                                    $sub_category_ids = $this->model_catalog_category->getCategoryIdsRecursive($dc->getCategoryId());
                                    foreach ($sub_category_ids as $sub_category_id) {
                                        if (in_array($sub_category_id, $pi['categories'])) {
                                            $flag = true;
                                            break;
                                        }
                                    }

                                    if (!$flag) {
                                        continue;
                                    }
                                }

                                $products_in_config[] = $pi;
                            }
                        }

                        // calc total quantity
                        $total_quantities = 0;
                        foreach ($products_in_config as $pic) {
                            $total_quantities += isset($pic['quantity']) ? $pic['quantity'] : 0;
                        }

                        // check condition
                        $conditions = $dc->getConditions();
                        foreach ($conditions as $condition) {
                            /** @var \Model\Discount_Condition_Quantity_Model $condition */
                            if (extract_number($condition->getMinQuantity()) > $total_quantities || extract_number($condition->getMaxQuantity()) < $total_quantities) {
                                continue;
                            }

                            $found = true;

                            // calculate discount value
                            $satisfied_discounts_to_conditions[$discount_obj->getDiscountId()][] = $this->calcDiscountDueToConditionForProduct($condition->getType(), $condition->getDiscount(), $product_info['quantity'], $product_info['total']);
                        }
                    }

                    if ($found) {
                        $satisfied_discounts[] = $related_discount;
                    }

                    continue;
                }

                // for manufacturer
                if ($discount_obj->getDiscountTypeId() == ModelDiscountDiscountType::TYPE_MANUFACTURER) {
                    $found = false;
                    $products_in_config = [];

                    foreach ($discount_config as $dc) {
                        if (!$dc instanceof \Model\Discount_Config_Manufacturer_Model) {
                            continue;
                        }

                        if ($dc->getAllManufacturer() == 1) {
                            $products_in_config = $products_info;
                        } else {
                            foreach ($products_info as $pi) {
                                if (!array_key_exists('manufacturers', $pi) ||
                                    !is_array($pi['manufacturers']) ||
                                    !in_array($dc->getManufacturerId(), $pi['manufacturers'])
                                ) {
                                    continue;
                                }

                                $products_in_config[] = $pi;
                            }
                        }

                        // calc total quantity
                        $total_quantities = 0;
                        foreach ($products_in_config as $pic) {
                            $total_quantities += isset($pic['quantity']) ? $pic['quantity'] : 0;
                        }

                        // check condition
                        $conditions = $dc->getConditions();
                        foreach ($conditions as $condition) {
                            /** @var \Model\Discount_Condition_Quantity_Model $condition */
                            if (extract_number($condition->getMinQuantity()) > $total_quantities || extract_number($condition->getMaxQuantity()) < $total_quantities) {
                                continue;
                            }

                            $found = true;

                            // calculate discount value
                            $satisfied_discounts_to_conditions[$discount_obj->getDiscountId()][] = $this->calcDiscountDueToConditionForProduct($condition->getType(), $condition->getDiscount(), $product_info['quantity'], $product_info['total']);
                        }
                    }

                    if ($found) {
                        $satisfied_discounts[] = $related_discount;
                        //break; // NO NEED to next discount
                    }

                    continue;
                }
            }

            /* get selected discount due to discount_auto_apply config */
            $this->load->model('setting/setting');
            if ($this->model_setting_setting->getSettingValue('discount_auto_apply') == 1) {
                if ($this->model_setting_setting->getSettingValue('discount_combine') == 1) {
                    $selected_discounts = array_map(function ($sd) {
                        return isset($sd['discount_id']) ? (int)$sd['discount_id'] : null;
                    }, $satisfied_discounts);
                } else {
                    $best_discount_id = null;
                    $best_discount_value = 0;
                    foreach ($satisfied_discounts_to_conditions as $discount_id => $item) {
                        if (!is_array($item) || empty($item)) {
                            continue;
                        }

                        $max_discount_value = max($item);
                        if ($max_discount_value > $best_discount_value) {
                            $best_discount_value = $max_discount_value;
                            $best_discount_id = $discount_id;
                        }
                    }

                    $selected_discounts = $best_discount_id ? [$best_discount_id] : [];
                }
            }

            // build result for each product
            $result[$key] = [
                'discounts' => $satisfied_discounts,
                'selected_discounts' => $selected_discounts
            ];
        }

        return $result;
    }

    /**
     * @param mixed $total
     * @return array
     */
    public function getSatisfiedDiscountsForOrder($total)
    {
        $this->load->model('discount/discount');
        $related_discounts[] = []; // TODO: correct model...
        if (empty($related_discounts) || !is_array($related_discounts)) {
            return [];
        }

        $satisfied_discounts = [];
        $selected_discounts = [];

        foreach ($related_discounts as $related_discount) {
            // parse discount array data to discount obj
            $discount_obj = \Model\Discount_Model::fromData($related_discount);
            if (!$discount_obj) {
                continue; // break to next discount
            }

            // except not for order
            if ($discount_obj->getDiscountTypeId() == 2) {//ModelDiscountDiscountType::TYPE_ORDER) {
                continue; // break to next discount
            }

            $discount_config = $discount_obj->getConfig();
            foreach ($discount_config as $dc) {
                /** @var \Model\Discount_Condition_Value_Model $found_condition */
                $found = false;
                $conditions = $dc->getConditions();
                foreach ($conditions as $condition) {
                    if (!$condition instanceof \Model\Discount_Condition_Value_Model) {
                        continue; // break to next discount condition
                    }

                    if ($total < extract_number($condition->getValue())) {
                        continue; // break to next discount condition
                    }

                    $found = true;
                    break; // break current discount config
                }

                if ($found) {
                    $satisfied_discounts[] = $related_discount;
                    break; // break to next discount
                }
            }
        }

        /* get selected discount due to discount_auto_apply config */
        $this->load->model('setting/setting');
        if ($this->model_setting_setting->getSettingValue('discount_auto_apply') == 1) {
            // return first discount. TODO: Future if support multi discount for order => need do:
            // - check if discount combine
            // - calc and find best discount by discount value
            // see getSatisfiedDiscountsForProduct()
            $selected_discounts = count($satisfied_discounts) > 0
                ? (isset($satisfied_discounts[0]['discount_id']) ? [$satisfied_discounts[0]['discount_id']] : [])
                : [];
        }

        return [
            'discounts' => $satisfied_discounts,
            'selected_discounts' => $selected_discounts
        ];
    }

    /**
     * @param array $products_info such as
     * [
     *     [product_id => 10, product_version_id => 1, quantity => 1, total => 25000, discounts => [1,2]],
     *     [product_id => 12, product_version_id => null, quantity => 3, total => 1500000, discounts => [3]],
     *     ...
     * ]
     * @return array format as:
     * [
     *     "12" => [
     *          "product_id" => 12,
     *          "discount" => 120000
     *     ],
     *     ...
     * ]
     */
    public function calcDiscountForProduct($products_info)
    {
        if (!is_array($products_info))  {
            return [];
        }

        $satisfied_discounts = [];
        if (empty($satisfied_discounts)) {
            return [];
        }

        $total = 0;

        // append categories and manufacturers to products_info
        $this->load->model('catalog/product');
        $this->load->model('catalog/manufacturer');
        foreach ($products_info as &$product_info) {
            if (!array_key_exists('product_id', $product_info)) {
                $product_info['categories'] = [];
                continue;
            }

            if (array_key_exists('total', $product_info)) {
                $total += extract_number($product_info['total']);
            }

            $categoriesIds = $this->model_catalog_product->getCategoryIdsByProductId($product_info['product_id']);
            $product_info['categories'] = array_map(function ($cat) {
                return isset($cat['category_id']) ? $cat['category_id'] : -1;
            }, $categoriesIds);

            $manufacturerId = $this->model_catalog_product->getManufactureIdByProductId($product_info['product_id']);
            $product_info['manufacturers'] = [$manufacturerId];
        };
        unset($product_info);

        $result = [];

        foreach ($products_info as $product_info) {
            if (!array_key_exists('product_id', $product_info)) {
                continue;
            }

            $is_multi_versions = (isset($product_info['product_version_id']) && !empty($product_info['product_version_id']));
            $key = !$is_multi_versions
                ? $product_info['product_id']
                : sprintf('%s-%s', $product_info['product_id'], $product_info['product_version_id']);

            // init result
            $result[$key] = [
                'product_id' => $product_info['product_id'],
                'product_version_id' => !$is_multi_versions ? null : $product_info['product_version_id'],
                'discount' => 0
            ];

            if (!array_key_exists($key, $satisfied_discounts) ||
                !array_key_exists('discounts', $satisfied_discounts[$key])
            ) {
                continue;
            }

            // id to discount
            $satisfied_discounts_ids_to_discounts = [];
            foreach ($satisfied_discounts[$key]['discounts'] as $sd) {
                $satisfied_discounts_ids_to_discounts[$sd['discount_id']] = $sd;
            }

            if (empty($satisfied_discounts_ids_to_discounts)) {
                continue;
            }

            // filter selected discounts from request
            $selected_discounts = [];
            if (!isset($product_info['discounts']) || empty($product_info['discounts'])) {
                continue;
            }

            foreach ($product_info['discounts'] as $selected_discount_id) {
                if (!array_key_exists($selected_discount_id, $satisfied_discounts_ids_to_discounts)) {
                    continue;
                }

                $selected_discounts[] = $satisfied_discounts_ids_to_discounts[$selected_discount_id];
            }

            if (empty($selected_discounts)) {
                continue;
            }

            /* get all conditions to be calculated total discount value*/
            $discount_conditions = [];

            $best_discount_value_for_type_category = 0;
            $best_discount_value_for_type_manufacturer = 0;

            $best_condition_for_type_category = null;
            $best_condition_for_type_manufacturer = null;

            foreach ($selected_discounts as $selected_discount) {
                // parse discount array data to discount obj
                $discount_obj = \Model\Discount_Model::fromData($selected_discount);
                if (!$discount_obj) {
                    continue; // next discount
                }

                // except not for product
                if (!in_array($discount_obj->getDiscountTypeId(), [
                    ModelDiscountDiscountType::TYPE_PRODUCT,
                    ModelDiscountDiscountType::TYPE_CATEGORY,
                    ModelDiscountDiscountType::TYPE_MANUFACTURER
                ])) {
                    continue; // next discount
                }

                $discount_config = $discount_obj->getConfig();

                // for product
                if ($discount_obj->getDiscountTypeId() == ModelDiscountDiscountType::TYPE_PRODUCT) {
                    foreach ($discount_config as $dc) {
                        if (!$dc instanceof \Model\Discount_Config_Product_Model) {
                            continue; // next discount config
                        }

                        if ($dc->getAllProduct() == 0) {
                            if ($dc->getProductId() != $product_info['product_id']) {
                                continue; // next discount config
                            }

                            // if multi version
                            if ($is_multi_versions && !in_array($product_info['product_version_id'], $dc->getProductVersionIds())) {
                                continue; // next discount config
                            }
                        }

                        // check condition
                        $conditions = $dc->getConditions();
                        foreach ($conditions as $condition) {
                            /** @var \Model\Discount_Condition_Quantity_Model $condition */
                            if (extract_number($condition->getMinQuantity()) > $product_info['quantity'] || extract_number($condition->getMaxQuantity()) < $product_info['quantity']) {
                                continue; // next discount condition
                            }

                            $discount_val_i = $this->calcDiscountDueToConditionForProduct($condition->getType(), $condition->getDiscount(), $product_info['quantity'], $product_info['total']);
                            $discount_conditions[] = [
                                'condition' => $condition,
                                'quantity' => $product_info['quantity'],
                                'discount_value' => $discount_val_i
                            ];

                            break; // break current discount condition
                        }
                    }

                    continue; // break to next discount
                }

                // for category
                if ($discount_obj->getDiscountTypeId() == ModelDiscountDiscountType::TYPE_CATEGORY) {
                    $products_in_config = [];

                    foreach ($discount_config as $dc) {
                        if (!$dc instanceof \Model\Discount_Config_Category_Model) {
                            continue; // break to next discount config
                        }

                        if ($dc->getAllCategory() == 1) {
                            $products_in_config = $products_info;
                        } else {
                            foreach ($products_info as $pi) {
                                if (!array_key_exists('categories', $pi) ||
                                    !is_array($pi['categories']) ||
                                    !in_array($dc->getCategoryId(), $pi['categories'])
                                ) {
                                    continue;
                                }

                                $products_in_config[] = $pi;
                            }
                        }

                        // calc total quantity
                        $total_quantities = 0;
                        foreach ($products_in_config as $pic) {
                            $total_quantities += isset($pic['quantity']) ? $pic['quantity'] : 0;
                        }

                        // check condition
                        $conditions = $dc->getConditions();
                        foreach ($conditions as $condition) {
                            /** @var \Model\Discount_Condition_Quantity_Model $condition */
                            if (extract_number($condition->getMinQuantity()) > $total_quantities || extract_number($condition->getMaxQuantity()) < $total_quantities) {
                                continue;
                            }

                            $discount_value_for_type_category_i = $this->calcDiscountDueToConditionForProduct($condition->getType(), $condition->getDiscount(), $product_info['quantity'], $product_info['total']);
                            if ($discount_value_for_type_category_i > $best_discount_value_for_type_category) {
                                // get max
                                $best_discount_value_for_type_category = $discount_value_for_type_category_i;
                                $best_condition_for_type_category = $condition;
                            }
                        }

                        if ($best_condition_for_type_category) {
                            $discount_conditions[] = [
                                'condition' => $best_condition_for_type_category,
                                'quantity' => $product_info['quantity'],
                                'discount_value' => $best_discount_value_for_type_category
                            ];
                        }
                    }

                    continue;
                }

                // for manufacturer
                if ($discount_obj->getDiscountTypeId() == ModelDiscountDiscountType::TYPE_MANUFACTURER) {
                    $products_in_config = [];

                    foreach ($discount_config as $dc) {
                        if (!$dc instanceof \Model\Discount_Config_Manufacturer_Model) {
                            continue;
                        }

                        if ($dc->getAllManufacturer() == 1) {
                            $products_in_config = $products_info;
                        } else {
                            foreach ($products_info as $pi) {
                                if (!array_key_exists('manufacturers', $pi) ||
                                    !is_array($pi['manufacturers']) ||
                                    !in_array($dc->getManufacturerId(), $pi['manufacturers'])
                                ) {
                                    continue;
                                }

                                $products_in_config[] = $pi;
                            }
                        }

                        // calc total quantity
                        $total_quantities = 0;
                        foreach ($products_in_config as $pic) {
                            $total_quantities += isset($pic['quantity']) ? $pic['quantity'] : 0;
                        }

                        // check condition
                        $conditions = $dc->getConditions();
                        foreach ($conditions as $condition) {
                            /** @var \Model\Discount_Condition_Quantity_Model $condition */
                            if (extract_number($condition->getMinQuantity()) > $total_quantities || extract_number($condition->getMaxQuantity()) < $total_quantities) {
                                continue;
                            }

                            $discount_for_type_manufacturer_i = $this->calcDiscountDueToConditionForProduct($condition->getType(), $condition->getDiscount(), $product_info['quantity'], $product_info['total']);
                            if ($discount_for_type_manufacturer_i > $best_discount_value_for_type_manufacturer) {
                                // get max
                                $best_discount_value_for_type_manufacturer = $discount_for_type_manufacturer_i;
                                $best_condition_for_type_manufacturer = $condition;
                            }
                        }

                        if ($best_condition_for_type_manufacturer) {
                            $discount_conditions[] = [
                                'condition' => $best_condition_for_type_manufacturer,
                                'quantity' => $product_info['quantity'],
                                'discount_value' => $best_discount_value_for_type_manufacturer
                            ];
                        }
                    }

                    continue;
                }
            }

            /* sort discount conditions by value */
            usort($discount_conditions, function ($dc1, $dc2) {
                if ($dc1['discount_value'] == $dc2['discount_value']) {
                    return 0;
                }

                return $dc1['discount_value'] > $dc2['discount_value'] ? -1 : 1;
            });

            // calculate total discount for one product
            $total_after_discount = $total;
            foreach ($discount_conditions as $dc) {
                /** @var \Model\Discount_Condition_Quantity_Model $con */
                $con = $dc['condition'];
                $total_after_discount -= $this->calcDiscountDueToConditionForProduct($con->getType(), $con->getDiscount(), $dc['quantity'], $total_after_discount);
            }

            $result[$key]['discount'] = $total - $total_after_discount;
        } // end one product

        return $result;
    }

    /**
     * @param int $total
     * @param array $selected_discount_ids
     * @return array format as:
     * [
     *     "discount" => 100000
     * ]
     */
    public function calcDiscountForOrder($total, $selected_discount_ids = [])
    {
        $satisfied_discounts = [];
        $satisfied_discounts = isset($satisfied_discounts['discounts']) ? $satisfied_discounts['discounts'] : [];
        if (empty($satisfied_discounts)) {
            return [
                'discount' => 0
            ];
        }

        // id to discount
        $satisfied_discounts_ids_to_discounts = [];
        foreach ($satisfied_discounts as $sd) {
            $satisfied_discounts_ids_to_discounts[$sd['discount_id']] = $sd;
        }

        if (empty($satisfied_discounts_ids_to_discounts)) {
            return [
                'discount' => 0
            ];
        }

        // filter selected discounts from request
        $selected_discounts = [];
        foreach ($selected_discount_ids as $selected_discount_id) {
            if (!array_key_exists($selected_discount_id, $satisfied_discounts_ids_to_discounts)) {
                continue;
            }

            $selected_discounts[] = $satisfied_discounts_ids_to_discounts[$selected_discount_id];
        }

        if (empty($selected_discounts)) {
            return [
                'discount' => 0
            ];
        }

        $total_discount = 0;
        foreach ($satisfied_discounts as $related_discount) {
            // parse discount array data to discount obj
            $discount_obj = \Model\Discount_Model::fromData($related_discount);
            if (!$discount_obj) {
                continue; // break to next discount
            }

            // except not for order
            if ($discount_obj->getDiscountTypeId() == 2) {//ModelDiscountDiscountType::TYPE_ORDER) {
                continue; // break to next discount
            }

            $discount_config = $discount_obj->getConfig();
            $total_discount_i = 0;
            foreach ($discount_config as $dc) {
                $conditions = $dc->getConditions();

                $last_matched_value = 0;
                foreach ($conditions as $condition) {
                    if (!$condition instanceof \Model\Discount_Condition_Value_Model) {
                        continue; // break to next discount condition
                    }

                    if ($total < extract_number($condition->getValue())) {
                        continue; // break to next discount condition
                    }

                    // only choose discount with last matched (highest) value
                    if ($last_matched_value <= extract_number($condition->getValue())) {
                        $last_matched_value = extract_number($condition->getValue());
                        $total_discount_i = $this->calcDiscountDueToConditionForOrder($condition->getType(), $condition->getDiscount(), $total);
                    }
                }
            }

            // choose best total discount! (event in current case we have only one discount for order at a time)
            if ($total_discount_i > $total_discount) {
                // get max
                $total_discount = $total_discount_i;
            }
        }

        return [
            'discount' => $total_discount
        ];
    }

    /**
     * @param int $product_id
     * @param int $product_version_id
     * @param null|string $order_source default 'ALL'
     * @return array
     */
    public function getDiscountsRelatedForProduct($product_id, $product_version_id, $order_source = 'ALL')
    {
        if (!$product_id) {
            return [];
        }

        $this->load->model('discount/discount');
        $this->load->model('discount/discount_type');
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');

        $allDiscountForProductActiveNow = $this->model_discount_discount->getDiscountsForProductActiveNow();

        $result = [];
        foreach ($allDiscountForProductActiveNow as $discount) {
            if (!is_array($discount)) {
                continue;
            }

            if (!array_key_exists('discount_id', $discount) || !array_key_exists('code', $discount) || !array_key_exists('config', $discount)
                || !array_key_exists('discount_type_id', $discount)) {
                continue;
            }
            $configs = json_decode($discount['config'], true);
            if (!is_array($configs)) {
                continue;
            }

            // DISCOUNT TYPE PRODUCT
            if ($discount['discount_type_id'] == ModelDiscountDiscountType::TYPE_PRODUCT) {
                $temp_config = [
                    'all_product' => 0,
                    'product_id' => $product_id,
                    'product_version_ids' => empty($product_version_id) ? [] : [$product_version_id]
                ];
                if ($this->checkDuplicate(ModelDiscountDiscountType::TYPE_PRODUCT, [$temp_config], [$discount])) {
                    if (!array_key_exists($discount['discount_id'], $result)) {
                        $result[$discount['discount_id']] = $discount;
                    }
                }

            }
            // DISCOUNT TYPE CATEGORY
            if ($discount['discount_type_id'] == ModelDiscountDiscountType::TYPE_CATEGORY) {
                $product_categories = $this->model_catalog_category->getCategoriesIdByProductId($product_id);
                $flag = false;
                foreach ($configs as $config) {
                    if (!is_array($config)) {
                        continue;
                    }
                    $all_category = isset($config['all_category']) ? $config['all_category'] : 0;
                    if ($all_category) {
                        if (count($product_categories) > 0) { // trường hợp áp dụng cho tất cả loại sản phẩm thì sản phẩm nào ko thuộc loại sản phẩm nào ko được áp dụng
                            $flag = true;
                            break;
                        }
                    } else {
                        $config_category_id = isset($config['category_id']) ? (int)$config['category_id'] : 0;
                        if ($config_category_id && in_array($config_category_id, $product_categories)) {
                            $flag = true;
                            break;
                        }

                        // support category multi levels: if cat parent in discount => all sub cats also in discount
                        $sub_category_ids = $this->model_catalog_category->getCategoryIdsRecursive($config_category_id);
                        foreach ($sub_category_ids as $sub_category_id) {
                            if ($sub_category_id && in_array($sub_category_id, $product_categories)) {
                                $flag = true;
                                break;
                            }
                        }

                        if ($flag) {
                            break;
                        }
                    }
                }

                if ($flag) {
                    if (!array_key_exists($discount['discount_id'], $result)) {
                        $result[$discount['discount_id']] = $discount;
                    }
                }
            }

            // DISCOUNT FOR MANUFACTURE
            if ($discount['discount_type_id'] == ModelDiscountDiscountType::TYPE_MANUFACTURER) {
                $product_manufacture_id = $this->model_catalog_product->getManufactureIdByProductId($product_id);
                $flag = false;
                foreach ($configs as $config) {
                    if (!is_array($config)) {
                        continue;
                    }
                    $all_manufacturer = isset($config['all_manufacturer']) ? $config['all_manufacturer'] : 0;
                    if ($all_manufacturer) {
                        if ($product_manufacture_id) { // trường hợp áp dụng cho tất cả nhà cung cấp thì sản phẩm nào ko thuộc nhà cung cấp nào ko được áp dụng
                            $flag = true;
                            break;
                        }
                    } else {
                        $config_manufacture_id = isset($config['manufacturer_id']) ? (int)$config['manufacturer_id'] : 0;
                        if ($config_manufacture_id && $config_manufacture_id == $product_manufacture_id) {
                            $flag = true;
                            break;
                        }
                    }
                }

                if ($flag) {
                    if (!array_key_exists($discount['discount_id'], $result)) {
                        $result[$discount['discount_id']] = $discount;
                    }
                }
            }
        }

        /* filter by order_source */
        $result = $this->filterDiscountsByOrderSource($result, $order_source);

        return $result;
    }

    public function getDiscountsRelatedForCategory($category_id)
    {
        if (!$category_id) {
            return [];
        }

        $this->load->model('discount/discount');
        $this->load->model('discount/discount_type');
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');

        $allDiscountForCategoryActiveNow = $this->model_discount_discount->getDiscountsForCategoryActiveNow();

        $result = [];
        foreach ($allDiscountForCategoryActiveNow as $discount) {
            if (!is_array($discount)) {
                continue;
            }

            if (!array_key_exists('discount_id', $discount) || !array_key_exists('code', $discount) || !array_key_exists('config', $discount)
                || !array_key_exists('discount_type_id', $discount)) {
                continue;
            }

            $configs = json_decode($discount['config'], true);
            if (!is_array($configs)) {
                continue;
            }

            // DISCOUNT TYPE CATEGORY
            if ($discount['discount_type_id'] != ModelDiscountDiscountType::TYPE_CATEGORY) {
                continue;
            }

            $found = false;
            foreach ($configs as $config) {
                if (!is_array($config)) {
                    continue;
                }

                $all_category = isset($config['all_category']) ? $config['all_category'] : 0;
                if ($all_category) {
                    $found = true;
                    break;
                }

                $config_category_id = isset($config['category_id']) ? (int)$config['category_id'] : 0;
                if ($config_category_id == $category_id) {
                    $found = true;
                    break;
                }
            }

            if ($found) {
                if (!array_key_exists($discount['discount_id'], $result)) {
                    $result[$discount['discount_id']] = $discount;
                }
            }
        }

        /* NO NEED to filter by order_source */

        return $result;
    }

    public function getDiscountsRelatedForManufacturer($manufacturer_id)
    {
        if (!$manufacturer_id) {
            return [];
        }

        $this->load->model('discount/discount');
        $this->load->model('discount/discount_type');
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');

        $allDiscountForManufacturerActiveNow = $this->model_discount_discount->getDiscountsForManufacturerActiveNow();

        $result = [];
        foreach ($allDiscountForManufacturerActiveNow as $discount) {
            if (!is_array($discount)) {
                continue;
            }

            if (!array_key_exists('discount_id', $discount) || !array_key_exists('code', $discount) || !array_key_exists('config', $discount)
                || !array_key_exists('discount_type_id', $discount)) {
                continue;
            }

            $configs = json_decode($discount['config'], true);
            if (!is_array($configs)) {
                continue;
            }

            // DISCOUNT FOR MANUFACTURE
            if ($discount['discount_type_id'] != ModelDiscountDiscountType::TYPE_MANUFACTURER) {
                continue;
            }

            $found = false;
            foreach ($configs as $config) {
                if (!is_array($config)) {
                    continue;
                }
                $all_manufacturer = isset($config['all_manufacturer']) ? $config['all_manufacturer'] : 0;
                if ($all_manufacturer) {
                    $found = true;
                    break;
                }

                $config_manufacture_id = isset($config['manufacturer_id']) ? (int)$config['manufacturer_id'] : 0;
                if ($config_manufacture_id == $manufacturer_id) {
                    $found = true;
                    break;
                }
            }

            if ($found) {
                if (!array_key_exists($discount['discount_id'], $result)) {
                    $result[$discount['discount_id']] = $discount;
                }
            }
        }

        /* NO NEED to filter by order_source */

        return $result;
    }

    public function getProductDiscountsOrderByDiscountValue($discount_order_products, $discounts = [])
    {
        if (!is_array($discount_order_products)) {
            return [];
        }

        $this->load->model('catalog/product');
        $this->load->model('discount/discount');
        $this->load->model('discount/discount_type');

        $count_discount_categories = [];
        /*[
            'category_id' => count, // count = số sản phẩm mua thuộc loại này
            ...
        ]*/
        $count_discount_manufacturer = [];
        /*[
            'manufacturer_id' => count, // count = số sản phẩm mua thuộc nhà cung cấp này
            ...
        ]*/
        $products = [];

        foreach ($discount_order_products as $product) {
            if (!array_key_exists('product_id', $product) || !$product['product_id']
                || !array_key_exists('quantity', $product) || (int)$product['quantity'] < 1) {
                continue;
            }

            $product_info = $this->model_catalog_product->getProduct($product['product_id'], false);
            // get all category and parents cateogry
            $product_info['categories'] = $this->model_catalog_product->getCategoriesAndParents($product['product_id']);
            $price_before_discount = (float)$product_info['compare_price_master'];
            $version_name = '';
            if ($product['product_version_id']) {
                $version_info = $this->model_catalog_product->getProductByVersionId($product['product_id'], $product['product_version_id']);
                $version_name = isset($version_info['version']) ? $version_info['version'] : '';
                if (isset($version_info['compare_price']) && (float)$version_info['compare_price'] > 0) {
                    $price_before_discount = $version_info['compare_price'];
                }
            }
            $full_name = $product_info['name'];
            if ($version_name) {
                $full_name = $full_name . '-' . $version_name;
            }

            if (array_key_exists($product_info['manufacturer_id'], $count_discount_manufacturer)) {
                $count_discount_manufacturer[$product_info['manufacturer_id']] += (int)$product['quantity'];
            } else {
                $count_discount_manufacturer[$product_info['manufacturer_id']] = (int)$product['quantity'];
            }

            if (isset($product_info['categories']) && is_array($product_info['categories'])) {
                foreach ($product_info['categories'] as $category) {
                    if (!is_array($category) || !array_key_exists('id', $category)) {
                        continue;
                    }

                    if (array_key_exists($category['id'], $count_discount_categories)) {
                        $count_discount_categories[$category['id']] += (int)$product['quantity'];;
                    } else {
                        $count_discount_categories[$category['id']] = (int)$product['quantity'];;
                    }
                }
            }

            $products[] = [
                'id' => $product_info['product_id'],
                'fullname' => $full_name,
                'amount' => $product['quantity'],
                'product_version_id' => $product['product_version_id'],
                'manufacturer_id' => $product_info['manufacturer_id'],
                'list_categories' => $product_info['categories'], // TODO get all categories contain parent categories
                'manufacturer' => $product_info['manufacturer'],
                'price_before_discount' => $price_before_discount,
            ];
        }

        if (empty($discounts)) {
            $discounts = $this->model_discount_discount->getDiscountsForProductActiveNow();
        }

        $products_discount_priority = $this->sortDiscountsForEachProduct($products, $discounts, $count_discount_categories, $count_discount_manufacturer);

        return $products_discount_priority;
    }

    private function sortDiscountsForEachProduct($products, $discounts, $count_discount_categories, $count_discount_manufacturer)
    {
        if (!is_array($products) || !is_array($products)) {
            return;
        }

        if (!is_array($discounts) || !is_array($discounts)) {
            return;
        }
        $priority = [];
        foreach ($products as $product) {
            $ppv_id = $product['id'] . '_' . $product['product_version_id'];

            foreach ($discounts as $discount) {
                $order_source = isset($discount['order_source']) ? $discount['order_source'] : '';
                $order_source = json_decode($order_source, true);
                $order_source = is_array($order_source) ? $order_source : [];
                if (!empty($order_source)) {
                    if (!in_array('Website', $order_source) && !in_array('website', $order_source)) {
                        continue;
                    }
                }
                if (!is_array($discount) || empty($discount) || !array_key_exists('config', $discount) || !array_key_exists('discount_type_id', $discount) || !array_key_exists('discount_id', $discount)) {
                    continue;
                }
                $discount_id = $discount['discount_id'];

                $configs = json_decode($discount['config'], true);
                if (!is_array($configs)) {
                    continue;
                }

                $condition_use = [];
                // apply for discount type product
                if ($discount['discount_type_id'] == ModelDiscountDiscountType::TYPE_PRODUCT) {
                    $conditions = [];
                    foreach ($configs as $config) {
                        if (!is_array($config)) {
                            continue;
                        }

                        $all_product = isset($config['all_product']) ? $config['all_product'] : 0;
                        if ($all_product) {
                            $conditions = isset($config['conditions']) ? $config['conditions'] : [];
                            break;
                        } else {
                            if ($product['id'] == $config['product_id'] && (in_array($product['product_version_id'], $config['product_version_ids']) || empty($config['product_version_ids']))) {
                                $conditions = $config['conditions'];
                                break;
                            }
                        }
                    }

                    if (!is_array($conditions)) {
                        continue;
                    }
                    $product_count = isset($product['amount']) ? (int)$product['amount'] : 0;
                    foreach ($conditions as $condition) {
                        if (!is_array($condition) || !array_key_exists('min_quantity', $condition) ||
                            !array_key_exists('max_quantity', $condition) || !array_key_exists('type', $condition)
                            || !array_key_exists('discount', $condition)) {
                            continue;
                        }
                        $min = (int)extract_number($condition['min_quantity']);
                        $max = (int)extract_number($condition['max_quantity']);
                        if ($product_count >= $min && ($product_count <= $max || $max == 0)) {
                            $condition_use = $condition;
                            $condition_use['discount_type_id'] = ModelDiscountDiscountType::TYPE_PRODUCT;
                            $condition_use['min_quantity'] = $min;
                            $condition_use['max_quantity'] = $max;
                            $condition_use['code'] = $discount['code'];
                            break;
                        }

                    }
                }

                // apply for discount type manufacturer
                if ($discount['discount_type_id'] == ModelDiscountDiscountType::TYPE_MANUFACTURER) {
                    $conditions = [];
                    foreach ($configs as $config) {
                        if (!is_array($config)) {
                            continue;
                        }

                        $all_manufacturer = isset($config['all_manufacturer']) ? $config['all_manufacturer'] : 0;
                        if ($all_manufacturer) {
                            $conditions = isset($config['conditions']) ? $config['conditions'] : [];
                            break;
                        } else {
                            if ($product['manufacturer_id'] == $config['manufacturer_id']) {
                                $conditions = $config['conditions'];
                                break;
                            }
                        }
                    }

                    if (!is_array($conditions)) {
                        continue;
                    }
                    $manufacturer_count = isset($count_discount_manufacturer[$product['manufacturer_id']]) ? (int)$count_discount_manufacturer[$product['manufacturer_id']] : 0;
                    foreach ($conditions as $condition) {
                        if (!is_array($condition) || !array_key_exists('min_quantity', $condition) ||
                            !array_key_exists('max_quantity', $condition) || !array_key_exists('type', $condition)
                            || !array_key_exists('discount', $condition)) {
                            continue;
                        }
                        $min = (int)extract_number($condition['min_quantity']);
                        $max = (int)extract_number($condition['max_quantity']);
                        if ($manufacturer_count >= $min && ($manufacturer_count <= $max || $max == 0)) {
                            $condition_use = $condition;
                            $condition_use['discount_type_id'] = ModelDiscountDiscountType::TYPE_MANUFACTURER;
                            $condition_use['min_quantity'] = $min;
                            $condition_use['max_quantity'] = $max;
                            $condition_use['manufacturer'] = $product['manufacturer'];
                            $condition_use['code'] = $discount['code'];
                            break;
                        }

                    }
                }

                // apply for discount type category
                if ($discount['discount_type_id'] == ModelDiscountDiscountType::TYPE_CATEGORY) {
                    $categories = array_map(function ($category) {
                        return $category['id'];
                    }, $product['list_categories']);
                    $arr_conditions = []; // trong trường hợp này vì 1 sản phẩm thuộc nhiều loại sản phẩm nên mảng này là mảng nhiều chiều
                    foreach ($configs as $config) {
                        if (!is_array($config)) {
                            continue;
                        }

                        $all_product = isset($config['all_category']) ? $config['all_category'] : 0;
                        if ($all_product) {
                            $arr_conditions[-1] = isset($config['conditions']) ? $config['conditions'] : [];
                            break;
                        } else {
                            if (in_array($config['category_id'], $categories)) {
                                $arr_conditions[$config['category_id']] = $config['conditions'];
                                break;
                            }
                        }
                    }

                    if (!is_array($arr_conditions)) {
                        continue;
                    }
                    $max_value = 0;
                    foreach ($arr_conditions as $k => $conditions) {
                        if (!is_array($conditions)) {
                            continue;
                        }
                        if ($k == '-1') {
                            foreach ($conditions as $condition) {
                                if (!is_array($condition) || !array_key_exists('min_quantity', $condition) ||
                                    !array_key_exists('max_quantity', $condition) || !array_key_exists('type', $condition)
                                    || !array_key_exists('discount', $condition)) {
                                    continue;
                                }
                                $min = (int)extract_number($condition['min_quantity']);
                                $max = (int)extract_number($condition['max_quantity']);
                                foreach ($categories as $category) {
                                    $category_count = isset($count_discount_categories[$category]) ? (int)$count_discount_categories[$category] : 0;
                                    if ($category_count >= $min && ($category_count <= $max || $max == 0)) {
                                        $discount_value = 0;
                                        if ($condition['type'] == 'price') {
                                            $discount_value = extract_number($condition['discount']) * (int)$product['amount'];
                                        } elseif ($condition['type'] == 'percent') {
                                            $discount_value = extract_number($condition['discount']) * $product['price_before_discount'] * (int)$product['amount'] / 100;
                                        }
                                        if ($discount_value >= $max_value) {
                                            $max_value = $discount_value;
                                            $condition_use = $condition;
                                            $condition_use['discount_type_id'] = ModelDiscountDiscountType::TYPE_CATEGORY;
                                            $condition_use['min_quantity'] = $min;
                                            $condition_use['max_quantity'] = $max;
                                            $condition_use['category_id'] = 'all';
                                            $condition_use['code'] = $discount['code'];
                                        }
                                    }
                                }
                            }
                        } else {
                            foreach ($conditions as $condition) {
                                if (!is_array($condition) || !array_key_exists('min_quantity', $condition) ||
                                    !array_key_exists('max_quantity', $condition) || !array_key_exists('type', $condition)
                                    || !array_key_exists('discount', $condition)) {
                                    continue;
                                }
                                $min = (int)extract_number($condition['min_quantity']);
                                $max = (int)extract_number($condition['max_quantity']);
                                $category_count = isset($count_discount_categories[$k]) ? (int)$count_discount_categories[$k] : 0;
                                if ($category_count >= $min && ($category_count <= $max || $max == 0)) {
                                    $discount_value = 0;
                                    if ($condition['type'] == 'price') {
                                        $discount_value = extract_number($condition['discount']) * (int)$product['amount'];
                                    } elseif ($condition['type'] == 'percent') {
                                        $discount_value = extract_number($condition['discount']) * $product['price_before_discount'] * (int)$product['amount'] / 100;
                                    }
                                    if ($discount_value >= $max_value) {
                                        $max_value = $discount_value;
                                        $condition_use = $condition;
                                        $condition_use['discount_type_id'] = ModelDiscountDiscountType::TYPE_CATEGORY;
                                        $condition_use['min_quantity'] = $min;
                                        $condition_use['max_quantity'] = $max;
                                        $condition_use['category_id'] = $k;
                                        $condition_use['code'] = $discount['code'];
                                    }
                                }
                            }
                        }
                    }
                }

                if (is_array($condition_use) && !empty($condition_use)) {
                    $discount_value = 0;
                    $discount_discount = extract_number($condition_use['discount']);
                    if ($condition_use['type'] == 'price') {
                        $discount_value = (float)$discount_discount * (int)$product['amount'];
                    } elseif ($condition_use['type'] == 'percent') {
                        $discount_value = (float)$discount_discount * $product['price_before_discount'] * (int)$product['amount'] / 100;
                    }
                    $priority[$ppv_id][] = [
                        'discount_id' => $discount['discount_id'],
                        'type' => $condition_use['type'],
                        'discount' => $discount_discount,
                        'discount_value' => $discount_value,
                        'discount_type_id' => $condition_use['discount_type_id'],
                        'min_quantity' => $condition_use['min_quantity'],
                        'max_quantity' => $condition_use['max_quantity'],
                        'manufacturer' => isset($condition_use['manufacturer']) ? $condition_use['manufacturer'] : '',
                        'category_id' => isset($condition_use['category_id']) ? $condition_use['category_id'] : '',
                        'code' => $condition_use['code']
                    ];
                }
            }
        }

        foreach ($priority as &$p) {
            usort($p, function ($a, $b) {
                return $a['discount_value'] <= $b['discount_value'];
            });
        }
        unset($p);

        return $priority;
    }

    public function getDiscountInfoTypeOrder($total_money, $discount = [])
    {
        $discount_for_order = [
            'value_step' =>0,
            'type' => 'price',
            'discount' => 0,
            'value' => 0
        ];
        if ($total_money <= 0) {
            return 0;
        }

        if (!is_array($discount)) {
            return 0;
        }

        if (empty($discount)){
            $discount = [];
        }

        if (!array_key_exists('config', $discount)) {
            return 0;
        }

        $configs = json_decode($discount['config'], true);
        if (!is_array($configs)) {
            return 0;
        }

        $order_source = isset($discount['order_source']) ? $discount['order_source'] : '';
        $order_source = json_decode($order_source, true);
        $order_source = is_array($order_source) ? $order_source : [];
        if (!empty($order_source)){
            if (!in_array('Website', $order_source) && !in_array('website', $order_source)){
                return 0;
            }
        }

        $condition_use = [];
        foreach ($configs as $config) {
            if (!is_array($config)) {
                continue;
            }

            $conditions = is_array($config['conditions']) ? $config['conditions'] : [];
            foreach ($conditions as $condition){
                if (!is_array($condition)){
                    continue;
                }
                if(!array_key_exists('value', $condition) || !array_key_exists('type', $condition) || !array_key_exists('discount', $condition)){
                    continue;
                }
                $condition_value = extract_number($condition['value']);
                if ($total_money >= $condition_value){
                    $condition_use = $condition;
                    $discount_for_order['value_step'] = $condition_value;
                }
            }
        }

        $value = 0;
        if (array_key_exists('type', $condition_use) && array_key_exists('discount', $condition_use)) {
            $discount_for_order['type'] = $condition_use['type'];
            $discount_for_order['discount'] = extract_number($condition_use['discount']);
            if ($condition_use['type'] == 'price'){
                $value = $discount_for_order['discount'];
            }
            if ($condition_use['type'] == 'percent'){
                $value = $total_money * $discount_for_order['discount'] / 100;
            }
        }else{
            return 0;
        }

        $discount_for_order['value'] = $value;

        return $discount_for_order;
    }

    /**
     * @param null|string $order_source default 'ALL'
     * @return array
     */
    public function getDiscountsRelatedForOrder($order_source = 'ALL')
    {
        $this->load->model('discount/discount');
        $discounts = $this->model_discount_discount->getDiscountsForOrderActiveNow();

        /* filter by order_source */
        $discounts = $this->filterDiscountsByOrderSource($discounts, $order_source);

        return reset($discounts); // chỉ lấy phần tử đầu tiên do 1 thời điểm chỉ tồn tại 1 loại discount này
    }

    /**
     * get Detail Discount to show on view detail
     * This may be used by both Admin and Website
     *
     * @param $discount_id
     * @return string
     */
    public function getDetailDiscount($discount_id)
    {
        $this->load->model('discount/discount');
        $this->load->model('discount/discount_type');

        $discount = $this->model_discount_discount->getDiscountById($discount_id);

        if(!is_array($discount) || !array_key_exists('discount_type_id', $discount)){
            return '';
        }

        $configs = array_key_exists('config', $discount) ? $discount['config'] : '';
        $configs = json_decode($configs, true);
        if (!is_array($configs)){
            return '';
        }

        // TODO: multi language...

        $content = '';
        switch ($discount['discount_type_id']) {
            case ModelDiscountDiscountType::TYPE_ORDER:
                foreach ($configs as $config) {
                    $conditions = isset($config['conditions']) ? $config['conditions'] : [];
                    foreach ($conditions as $condition) {
                        $content .= '- Chiết khấu (' . number_format((float)extract_number($condition['discount']), 0, ',', '.') . ($condition['type'] == 'price' ? 'đ' : '%') . ') cho đơn hàng từ (' . $this->currency->format($condition['value'], $this->session->data['currency']) . ').<br>';
                    }
                }
                break;

            case ModelDiscountDiscountType::TYPE_PRODUCT:
                $this->load->model('catalog/product');
                foreach ($configs as $config) {
                    if (array_key_exists('all_product', $config) && $config['all_product'] == 1) {
                        $product_name = 'Tất cả sản phẩm';
                    } else {
                        $product_name = $this->model_catalog_product->getProductVersionsFullName($config['product_id'], $config['product_version_ids']);
                    }
                    $conditions = isset($config['conditions']) ? $config['conditions'] : [];
                    foreach ($conditions as $condition) {
                        if ((int)$condition['max_quantity'] > 0) {
                            $content .= '- Chiết khấu (' . number_format((float)extract_number($condition['discount']), 0, ',', '.') . ($condition['type'] == 'price' ? 'đ' : '%') . ')/sản phẩm nếu đơn hàng mua từ (' . $condition['min_quantity'] . ') đến (' . $condition['max_quantity'] . ') ' . $product_name . '.<br>';
                        } else {
                            $content .= '- Chiết khấu (' . number_format((float)extract_number($condition['discount']), 0, ',', '.') . ($condition['type'] == 'price' ? 'đ' : '%') . ')/sản phẩm nếu đơn hàng mua từ (' . $condition['min_quantity'] . ') ' . $product_name . '.<br>';
                        }
                    }
                }
                break;

            case ModelDiscountDiscountType::TYPE_CATEGORY:
                $this->load->model('catalog/category');
                foreach ($configs as $config) {
                    if (array_key_exists('all_category', $config) && $config['all_category'] == 1) {
                        $category_name = 'Tất cả loại sản phẩm';
                    } else {
                        $category_name = $this->model_catalog_category->getNameRecordById($config['category_id'], 'category');
                    }
                    $conditions = isset($config['conditions']) ? $config['conditions'] : [];
                    foreach ($conditions as $condition) {
                        if ((int)$condition['max_quantity'] > 0) {
                            $content .= '- Chiết khấu (' . number_format((float)extract_number($condition['discount']), 0, ',', '.') . ($condition['type'] == 'price' ? 'đ' : '%') . ')/sản phẩm nếu đơn hàng mua từ (' . $condition['min_quantity'] . ') đến (' . $condition['max_quantity'] . ') sản phẩm thuộc loại (' . $category_name . ').<br>';
                        } else {
                            $content .= '- Chiết khấu (' . number_format((float)extract_number($condition['discount']), 0, ',', '.') . ($condition['type'] == 'price' ? 'đ' : '%') . ')/sản phẩm nếu đơn hàng mua từ (' . $condition['min_quantity'] . ') sản phẩm thuộc loại (' . $category_name . ').<br>';
                        }
                    }
                }
                break;

            case ModelDiscountDiscountType::TYPE_MANUFACTURER:
                $this->load->model('catalog/manufacturer');
                foreach ($configs as $config) {
                    if (array_key_exists('all_manufacturer', $config) && $config['all_manufacturer'] == 1) {
                        $manufacturer_name = 'Tất cả nhà cung cấp';
                    } else {
                        $manufacturer_name = $this->model_catalog_manufacturer->getManufacturerNameById($config['manufacturer_id']);
                    }
                    $conditions = isset($config['conditions']) ? $config['conditions'] : [];
                    foreach ($conditions as $condition) {
                        if ((int)$condition['max_quantity'] > 0) {
                            $content .= '- Chiết khấu (' . number_format((float)extract_number($condition['discount']), 0, ',', '.') . ($condition['type'] == 'price' ? 'đ' : '%') . ')/sản phẩm nếu đơn hàng mua từ (' . $condition['min_quantity'] . ') đến (' . $condition['max_quantity'] . ') sản phẩm thuộc nhà cung cấp (' . $manufacturer_name . ').<br>';
                        } else {
                            $content .= '- Chiết khấu (' . number_format((float)extract_number($condition['discount']), 0, ',', '.') . ($condition['type'] == 'price' ? 'đ' : '%') . ')/sản phẩm nếu đơn hàng mua từ (' . $condition['min_quantity'] . ') sản phẩm thuộc nhà cung cấp (' . $manufacturer_name . ').<br>';
                        }
                    }
                }
                break;

            default:
                break;
        }

        return $content;
    }

    /* private functions */

    private function validateDiscountTypeProduct(array $config, $start_at, $end_at, $discount_id)
    {
        if (empty($config)) {
            return $this->discountConfigEmpty();
        }

        /* validate if existing product in other discounts with same date range */
        // get all discounts with same type
        $allDiscountsForProduct = $this->model_discount_discount->getDiscountsWithTypeAndDate(ModelDiscountDiscountType::TYPE_PRODUCT, $start_at, $end_at, $discount_id);

        // check if product is already in same date range
        $duplicated = false;
        if (is_array($allDiscountsForProduct) && count($allDiscountsForProduct) > 0) {
            $duplicated = $this->checkDuplicate(ModelDiscountDiscountType::TYPE_PRODUCT, $config, $allDiscountsForProduct);
        }

        // if duplicated
        if ($duplicated) {
            return [
                "code" => self::$ERR_CODE_SAME_PRODUCT,
                "message" => $this->language->get(self::$ERR_MSG_MAP[self::$ERR_CODE_SAME_PRODUCT])
            ];
        }

        /* validate more... */

        return $this->discountConfigValid();
    }

    private function validateDiscountTypeCategory(array $config, $start_at, $end_at, $discount_id)
    {
        if (empty($config)) {
            return $this->discountConfigEmpty();
        }

        /* validate if existing category in other discounts with same date range */
        // get all discounts with same type
        $allDiscountsForCategory = $this->model_discount_discount->getDiscountsWithTypeAndDate(ModelDiscountDiscountType::TYPE_CATEGORY, $start_at, $end_at, $discount_id);

        // check if product is already in same date range\
        $duplicated = false;
        if (is_array($allDiscountsForCategory) && count($allDiscountsForCategory) > 0) {
            $duplicated = $this->checkDuplicate(ModelDiscountDiscountType::TYPE_CATEGORY, $config, $allDiscountsForCategory);
        }

        if ($duplicated) {
            return [
                "code" => self::$ERR_CODE_SAME_CATEGORY,
                "message" => $this->language->get(self::$ERR_MSG_MAP[self::$ERR_CODE_SAME_CATEGORY])
            ];
        }

        /* validate more... */

        return $this->discountConfigValid();
    }

    private function validateDiscountTypeManufacturer(array $config, $start_at, $end_at, $discount_id)
    {
        if (empty($config)) {
            return $this->discountConfigEmpty();
        }

        /* validate if existing manufacturer in other discounts with same date range */
        // get all discounts with same type
        $allDiscountsForManufacturer = $this->model_discount_discount->getDiscountsWithTypeAndDate(ModelDiscountDiscountType::TYPE_MANUFACTURER, $start_at, $end_at, $discount_id);

        // check if product is already in same date range
        $duplicated = false;
        if (is_array($allDiscountsForManufacturer) && count($allDiscountsForManufacturer) > 0) {
            $duplicated = $this->checkDuplicate(ModelDiscountDiscountType::TYPE_MANUFACTURER, $config, $allDiscountsForManufacturer);
        }

        // if duplicated
        if ($duplicated) {
            return [
                "code" => self::$ERR_CODE_SAME_MANUFACTURER,
                "message" => $this->language->get(self::$ERR_MSG_MAP[self::$ERR_CODE_SAME_MANUFACTURER])
            ];
        }

        /* validate more... */

        return $this->discountConfigValid();
    }

    private function validateDiscountTypeOrder(array $config, $start_at, $end_at, $discount_id)
    {
        if (empty($config)) {
            return $this->discountConfigEmpty();
        }

        /* validate if existing order in other discounts with same date range */
        // get all discounts with same type
        $allDiscountsForOrder = $this->model_discount_discount->getDiscountsWithTypeAndDate(ModelDiscountDiscountType::TYPE_ORDER, $start_at, $end_at, $discount_id);

        // check if product is already in same date range
        $duplicated = count($allDiscountsForOrder) > 0 ? true : false;

        // if duplicated
        if ($duplicated) {
            return [
                "code" => self::$ERR_CODE_SAME_ORDER,
                "message" => $this->language->get(self::$ERR_MSG_MAP[self::$ERR_CODE_SAME_ORDER])
            ];
        }

        /* validate more... */

        return $this->discountConfigValid();
    }

    private function discountConfigValid()
    {
        return [
            "code" => self::$ERR_CODE_SUCCESS,
            "message" => $this->language->get(self::$ERR_MSG_MAP[self::$ERR_CODE_SUCCESS])
        ];
    }

    private function discountConfigEmpty()
    {
        return [
            "code" => self::$ERR_CODE_INVALID_CONFIG,
            "message" => $this->language->get(self::$ERR_MSG_MAP[self::$ERR_CODE_INVALID_CONFIG])
        ];
    }

    private function checkDuplicate($type_id, $config, $rows)
    {
        $list_config_ids = [];
        $list_rows_ids = [];
        $check_config = false;
        if ($type_id == ModelDiscountDiscountType::TYPE_MANUFACTURER) {
            $check_config = $this->getListManufacturerInConfig($config, $list_config_ids);
        } else if ($type_id == ModelDiscountDiscountType::TYPE_CATEGORY) {
            $check_config = $this->getListCategoryInConfig($config, $list_config_ids);
        } else if ($type_id == ModelDiscountDiscountType::TYPE_PRODUCT) {
            $check_config = $this->getListProductVersionsInConfig($config, $list_config_ids);
        }
        if ($check_config) {
            return true;
        }

        foreach ($rows as $row) {
            if (!array_key_exists('config', $row)) {
                continue;
            }
            $row_configs = json_decode($row['config'], true);
            if (!is_array($row_configs)) {
                continue;
            }
            $check_row = false;
            if ($type_id == ModelDiscountDiscountType::TYPE_MANUFACTURER) {
                $check_row = $this->getListManufacturerInConfig($row_configs, $list_rows_ids);
            } else if ($type_id == ModelDiscountDiscountType::TYPE_CATEGORY) {
                $check_row = $this->getListCategoryInConfig($row_configs, $list_rows_ids);
            } else if ($type_id == ModelDiscountDiscountType::TYPE_PRODUCT) {
                $check_row = $this->getListProductVersionsInConfig($row_configs, $list_rows_ids);
            }
            if ($check_row) {
                return true;
            }
        }
        $result = array_intersect($list_config_ids, $list_rows_ids);
        if (count($result) > 0) {
            return true;
        }

        return false;
    }

    private function getListCategoryInConfig($config, &$list)
    {
        foreach ($config as $item) {
            if (!is_array($item)) {
                continue;
            }

            if (array_key_exists('all_category', $item) && $item['all_category'] == 1) {
                return true;
            }

            if (array_key_exists('category_id', $item)) {
                if ($item['category_id'] != '' && !in_array($item['category_id'], $list)) {
                    $list[] = $item['category_id'];
                }
            }
        }

        return false;
    }

    private function getListManufacturerInConfig($config, &$list)
    {
        foreach ($config as $item) {
            if (!is_array($item)) {
                continue;
            }

            if (array_key_exists('all_manufacturer', $item) && $item['all_manufacturer'] == 1) {
                return true;
            }

            if (array_key_exists('manufacturer_id', $item)) {
                if ($item['manufacturer_id'] != '' && !in_array($item['manufacturer_id'], $list)) {
                    $list[] = $item['manufacturer_id'];
                }
            }
        }

        return false;
    }

    private function getListProductVersionsInConfig($config, &$list)
    {
        foreach ($config as $item) {
            if (!is_array($item)) {
                continue;
            }

            if (array_key_exists('all_product', $item) && $item['all_product'] == 1) {
                return true;
            }

            if (array_key_exists('product_id', $item) && array_key_exists('product_version_ids', $item)) {
                if (!is_array($item['product_version_ids']) || empty($item['product_version_ids'])) {
                    $product_version_id = $item['product_id'];
                    if ($product_version_id != '' && !in_array($product_version_id, $list)) {
                        $list[] = $product_version_id;
                    }
                } else {
                    // add later
                    if ($item['product_id'] != '' && !in_array($item['product_id'], $list)) {
                        $list[] = $item['product_id'];
                    }
                    // end add

                    foreach ($item['product_version_ids'] as $version_id) {
                        $product_version_id = $item['product_id'] . '_' . $version_id;
                        if ($product_version_id != '' && !in_array($product_version_id, $list)) {
                            $list[] = $product_version_id;
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * @param string $type
     * @param int $discount price (100000) or percent (100  (%) )
     * @param int|null $quantity if type price
     * @param int|null $total if type percent
     * @return float|int
     */
    private function calcDiscountDueToConditionForProduct($type, $discount, $quantity, $total)
    {
        if ($type == 'price') {
            return (extract_number($discount) * extract_number($quantity));
        }

        if ($type == 'percent') {
            return (extract_number($discount) * extract_number($total) / 100);
        }

        return 0;
    }

    /**
     * @param string $type
     * @param int $discount price (100000) or percent (100  (%) )
     * @param int|null $total if type percent
     * @return float|int
     */
    private function calcDiscountDueToConditionForOrder($type, $discount, $total)
    {
        if ($type == 'price') {
            return extract_number($discount);
        }

        if ($type == 'percent') {
            return (extract_number($discount) * $total / 100);
        }

        return 0;
    }

    /**
     * @param array $discounts
     * @param null|string $order_source default 'ALL'
     * @return array
     */
    private function filterDiscountsByOrderSource(array $discounts, $order_source = 'ALL')
    {
        if ($order_source === 'ALL') {
            return $discounts;
        }

        $this->load->model('discount/discount');
        $order_source = empty($order_source) ? ModelDiscountDiscount::$SOURCE_ADMIN : $order_source;
        $discounts = array_filter($discounts, function ($discount) use ($order_source) {
            if (!isset($discount['order_source'])) {
                return false;
            }

            if ($discount['order_source'] === 'ALL') {
                return true;
            }

            $order_src = json_decode($discount['order_source'], true);
            $order_src = is_array($order_src) ? $order_src : [];
            if (empty($order_src)) {
                return false;
            }

            return in_array($order_source, $order_src);
        });

        return $discounts;
    }
}