<?php

class Vietnam_Administrative
{
    const JSON_FILE_PROVINCE = DIR_SYSTEM . 'library/vietnam_administrative/province.json';
    const JSON_FILE_DISTRICT = DIR_SYSTEM . 'library/vietnam_administrative/district.json';
    const JSON_FILE_WARD = DIR_SYSTEM . 'library/vietnam_administrative/ward.json';

    protected $cached_provinces_by_code = [];
    protected $cached_provinces_by_name = [];
    protected $cached_districts_by_code = [];
    protected $cached_wards_by_code = [];

    /**
     * @return array
     */
    public function getProvinces()
    {
        // try again from file
        if (empty($this->cached_provinces_by_name)) {
            $this->getProvincesFromFile();
        }

        // then get from cache
        if (!empty($this->cached_provinces_by_name)) {
            $result = $this->cached_provinces_by_name;

            // sort alphabet asc
            uksort($result, function ($k1, $k2) {
                $k1 = $this->escapeVietnamese($k1);
                $k2 = $this->escapeVietnamese($k2);

                if ($k1 == $k2) {
                    return 0;
                }

                return ($k1 < $k2) ? -1 : 1;
            });

            return $result;
        }

        return [];
    }

    /**
     * @param $code
     * @return mixed
     */
    public function getProvinceByCode($code)
    {
        // try again from file
        if (empty($this->cached_provinces_by_code) && !array_key_exists($code, $this->cached_provinces_by_code)) {
            $this->getProvincesFromFile();
        }

        // then get from cache
        if (!empty($this->cached_provinces_by_code) && array_key_exists($code, $this->cached_provinces_by_code)) {
            return $this->cached_provinces_by_code[$code];
        }

        return null;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getProvinceByName($name)
    {
        // try again from file
        if (empty($this->cached_provinces_by_name) && !array_key_exists($name, $this->cached_provinces_by_name)) {
            $this->getProvincesFromFile();
        }

        // then get from cache
        if (!empty($this->cached_provinces_by_name) && array_key_exists($name, $this->cached_provinces_by_name)) {
            return $this->cached_provinces_by_name[$name];
        }

        return null;
    }

    /**
     * @param $provinceCode
     * @return array
     */
    public function getDistrictsByProvinceCode($provinceCode)
    {
        // try again from file
        if (empty($this->cached_districts_by_code)) {
            $this->getDistrictsFromFile();
        }

        // then get from cache
        if (!empty($this->cached_districts_by_code)) {
            // TODO: also cache by province code...
            $districts = [];
            foreach ($this->cached_districts_by_code as $district) {
                if (!is_array($district) ||
                    !array_key_exists('name', $district) ||
                    !array_key_exists('parent_code', $district) ||
                    $district['parent_code'] !== $provinceCode
                ) {
                    continue;
                }

                $districts[$district['name']] = [
                    'code' => $district['code'],
                    'name' => $district['name'],
                    'name_with_type' => $district['name_with_type']
                ];
            }

            // sort alphabet asc
            uksort($districts, function ($k1, $k2) {
                $k1 = $this->escapeVietnamese($k1);
                $k2 = $this->escapeVietnamese($k2);

                if ($k1 == $k2) {
                    return 0;
                }

                return ($k1 < $k2) ? -1 : 1;
            });

            return $districts;
        }

        return [];
    }

    /**
     * @param $code
     * @return mixed
     */
    public function getDistrictByCode($code)
    {
        // try again from file
        if (empty($this->cached_districts_by_code) && !array_key_exists($code, $this->cached_districts_by_code)) {
            $this->getDistrictsFromFile();
        }

        // then get from cache
        if (!empty($this->cached_districts_by_code) && array_key_exists($code, $this->cached_districts_by_code)) {
            $district = $this->cached_districts_by_code[$code];
            if (!is_array($district) ||
                !array_key_exists('code', $district) ||
                !array_key_exists('name', $district)
            ) {
                return null;
            }

            return [
                'code' => $district['code'],
                'name' => $district['name'],
                'name_with_type' => $district['name_with_type']
            ];
        }

        return null;
    }

    /**
     * @param $districtCode
     * @return array
     */
    public function getWardsByDistrictCode($districtCode)
    {
        // try again from file
        if (empty($this->cached_wards_by_code)) {
            $this->getWardsFromFile();
        }

        // then get from cache
        if (!empty($this->cached_wards_by_code)) {
            // TODO: also cache by district code...
            $wards = [];
            foreach ($this->cached_wards_by_code as $ward) {
                if (!is_array($ward) ||
                    !array_key_exists('name', $ward) ||
                    !array_key_exists('parent_code', $ward) ||
                    $ward['parent_code'] !== $districtCode
                ) {
                    continue;
                }

                $wards[$ward['name']] = [
                    'code' => $ward['code'],
                    'name' => $ward['name'],
                    'name_with_type' => $ward['name_with_type']
                ];
            }

            // sort alphabet asc
            uksort($wards, function ($k1, $k2) {
                $k1 = $this->escapeVietnamese($k1);
                $k2 = $this->escapeVietnamese($k2);

                if ($k1 == $k2) {
                    return 0;
                }

                return ($k1 < $k2) ? -1 : 1;
            });

            return $wards;
        }

        return [];
    }

    /**
     * @param $code
     * @return mixed
     */
    public function getWardByCode($code)
    {
        // try again from file
        if (empty($this->cached_wards_by_code) && !array_key_exists($code, $this->cached_wards_by_code)) {
            $this->getWardsFromFile();
        }

        // then get from cache
        if (!empty($this->cached_wards_by_code) && array_key_exists($code, $this->cached_wards_by_code)) {
            $ward = $this->cached_wards_by_code[$code];
            if (!is_array($ward) ||
                !array_key_exists('code', $ward) ||
                !array_key_exists('name', $ward)
            ) {
                return null;
            }

            return [
                'code' => $ward['code'],
                'name' => $ward['name'],
                'name_with_type' => $ward['name_with_type']
            ];
        }

        return null;
    }

    /**
     * @param $administrative_text
     * @param null $parent_code null if province, or MUST valid code if district or ward
     * @return mixed|null
     */
    function getAdministrativeCodeByTextValue($administrative_text, $parent_code = null)
    {
        $province_code = $this->getProvinceCodeByTextValue($administrative_text);
        if (!is_null($province_code)) {
            return $province_code;
        }

        $district_code = $this->getDistrictCodeByTextValue($administrative_text, $parent_code);
        if (!is_null($district_code)) {
            return $district_code;
        }

        $ward_code = $this->getWardCodeByTextValue($administrative_text, $parent_code);
        if (!is_null($ward_code)) {
            return $ward_code;
        }

        return null;
    }

    /**
     * @param $province_text
     * @return mixed|null
     */
    function getProvinceCodeByTextValue($province_text)
    {
        $province_prefixes = [
            "tỉnh ",
            "Tỉnh ",
            "TỈNH ",
            "tinh ",
            "Tinh ",
            "TINH ",
            "thành phố ",
            "Thành phố ",
            "Thành Phố ",
            "THÀNH PHỐ ",
            "tp. ",
            "TP. ",
            "tp ",
            "TP ",
        ];

        $found_prefix = false;
        foreach ($province_prefixes as $prefix) {
            if (strpos($province_text, $prefix) === 0) {
                $province_text = trim(str_replace($prefix, '', $province_text));
                $found_prefix = true;
                break;
            }
        }

        if (!$found_prefix) {
            $province_text = trim($province_text);
        }

        $province_text = $this->escapeVietnamese($province_text);
        $provinces = $this->getProvinces();
        foreach ($provinces as $province) {
            if (!isset($province['name']) || !isset($province['code'])) {
                continue;
            }

            $province_name = strtolower($this->escapeVietnamese(trim($province['name'])));
            if ($province_text == $province_name) {
                return $province['code'];
            }
        }

        return null;
    }

    /**
     * @param $district_text
     * @param $province_code
     * @return mixed|null
     */
    function getDistrictCodeByTextValue($district_text, $province_code)
    {
        $district_prefixes = [
            "quận ",
            "Quận ",
            "QUẬN ",
            "Q ",
            "Q. ",
            "Huyện ",
            "huyện ",
            "HUYỆN ",
            "huyện đảo ",
            "Huyện đảo ",
            "Huyện Đảo ",
            "HUYỆN ĐẢO ",
            "H ",
            "H. ",
            "thành phố ",
            "Thành phố ",
            "Thành Phố ",
            "THÀNH PHỐ ",
            "tp. ",
            "TP. ",
            "tp ",
            "TP ",
            "thị xã ",
            "Thị xã ",
            "Thị Xã ",
            "THỊ XÃ ",
            "tx. ",
            "TX. ",
            "tx ",
            "TX ",
        ];

        foreach ($district_prefixes as $prefix) {
            if (strpos($district_text, $prefix) === 0) {
                $district_text = trim(str_replace($prefix, '', $district_text));
                break;
            }
        }

        $district_text = $this->escapeVietnamese($district_text);
        $districts = $this->getDistrictsByProvinceCode($province_code);
        foreach ($districts as $district) {
            if (!isset($district['name']) || !isset($district['code'])) {
                continue;
            }

            $district_name = strtolower($this->escapeVietnamese(trim($district['name'])));
            if ($district_text == $district_name) {
                return $district['code'];
            }
        }

        return null;
    }

    /**
     * @param $ward_text
     * @param $district_code
     * @return mixed|null
     */
    function getWardCodeByTextValue($ward_text, $district_code)
    {
        $ward_prefixes = [
            "thị trấn ",
            "Thị trấn ",
            "Thị Trấn ",
            "THỊ TRẤN ",
            "tt. ",
            "TT. ",
            "phường ",
            "Phường ",
            "p ",
            "P ",
            "p. ",
            "P. ",
            "xã ",
            "Xã ",
            "X. ",
            "x. ",
        ];

        foreach ($ward_prefixes as $prefix) {
            if (strpos($ward_text, $prefix) === 0) {
                $ward_text = trim(str_replace($prefix, '', $ward_text));
                break;
            }
        }

        $ward_text = $this->escapeVietnamese($ward_text);
        $wards = $this->getWardsByDistrictCode($district_code);
        foreach ($wards as $ward) {
            if (!isset($ward['name']) || !isset($ward['code'])) {
                continue;
            }

            $ward_name = strtolower($this->escapeVietnamese(trim($ward['name'])));
            if ($ward_text == $ward_name) {
                return $ward['code'];
            }
        }

        return null;
    }

    /* === private functions === */

    private function getProvincesFromFile()
    {
        try {
            $provincesRaw = file_get_contents(self::JSON_FILE_PROVINCE);
            $result = json_decode($provincesRaw, true);
            if (json_last_error() === JSON_ERROR_NONE) {
                // update cached by code and name
                foreach ($result as $province) {
                    if (!is_array($province) || !array_key_exists('code', $province) || !array_key_exists('name', $province)) {
                        continue;
                    }

                    $this->cached_provinces_by_code[$province['code']] = [
                        'code' => $province['code'],
                        'name' => $province['name'],
                        'name_with_type' => $province['name_with_type']
                    ];

                    $this->cached_provinces_by_name[$province['name']] = [
                        'code' => $province['code'],
                        'name' => $province['name'],
                        'name_with_type' => $province['name_with_type']
                    ];
                }

                return $result;
            }
        } catch (Exception $e) {
        }

        return [];
    }

    private function getDistrictsFromFile()
    {
        try {
            $districtsRaw = file_get_contents(self::JSON_FILE_DISTRICT);
            $result = json_decode($districtsRaw, true);
            if (json_last_error() === JSON_ERROR_NONE) {
                // update cached by code
                foreach ($result as $district) {
                    if (!is_array($district)) {
                        continue;
                    }

                    if (array_key_exists('code', $district)) {
                        $this->cached_districts_by_code[$district['code']] = $district;
                    }
                }

                return $result;
            }
        } catch (Exception $e) {
        }

        return [];
    }

    private function getWardsFromFile()
    {
        try {
            $wardsRaw = file_get_contents(self::JSON_FILE_WARD);
            $result = json_decode($wardsRaw, true);
            if (json_last_error() === JSON_ERROR_NONE) {
                // update cached by code
                foreach ($result as $ward) {
                    if (!is_array($ward)) {
                        continue;
                    }

                    if (array_key_exists('code', $ward)) {
                        $this->cached_wards_by_code[$ward['code']] = $ward;
                    }
                }

                return $result;
            }
        } catch (Exception $e) {
        }

        return [];
    }

    public function escapeVietnamese($str)
    {
        if (function_exists('escapeVietnamese')) {
            return escapeVietnamese($str);
        }

        // Notice: Duplicate code from system/helper/general->escapeVietnamese()
        // For some where, outside of opencart loader flow, use, e.g cronjob/sale_channel/shopee_sync_orders.php, ...
        // TODO: other ways?...

        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        // $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        // $str = preg_replace('/([\s]+)/', '-', $str);

        return $str;
    }
}