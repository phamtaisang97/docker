<?php
/**
 * @package        Bestme
 * @author        Bestme
 * @copyright    Copyright (c) 2019, Bestme - Novaon, Corp. (https://bestme.vn/)
 * @license        https://opensource.org/licenses/GPL-3.0
 * @link        https://bestme.vn
 */

/**
 * Mail class
 */
class Migration
{
    const MIGRATE_DIRECTION_UP = 'UP';
    const MIGRATE_DIRECTION_DOWN = 'DOWN';
    const JSON_FILE_VERSION = DIR_SYSTEM . '/library/migration/migration_version.json';

    protected $db;
    protected $log;

    /**
     * Constructor
     *
     * @param DB $db
     * @param Log $log
     *
     */
    public function __construct($db, $log)
    {
        $this->db = $db;
        $this->log = $log;
    }


    public function migrate()
    {
        /*
         * NOTICE: hardcode migrate direction to "UP"
         * if need down, set value "DOWN"
         */
        $migrate_direction = self::MIGRATE_DIRECTION_UP;

        $versions = [];
        if(file_exists(self::JSON_FILE_VERSION)) {
            $versions_string = file_get_contents(self::JSON_FILE_VERSION);
            $versions = json_decode($versions_string, true);
        }

        /* migrations to be run */
        $migration_versions = [
            'migration_20190718_default_policy_gg_shopping',
            'migration_20190729_enable_seo',
            'migration_20190729_2_fix_demo_data_sql',
            'migration_20190731_remake_menu',
            'migration_20190810_staff_permission',
            'migration_20190822_product_multi_version',
            'migration_20190828_chatbot',
            'migration_20190828_transport',
            'migration_20190903_sync_staffs_to_welcome',
            'migration_20190904_keep_edited_demo_data',
            'migration_20190930_increase_decimal_order',
            'migration_20191015_new_report_tables_and_events',
            'migration_20191016_new_report_data',
            'migration_20191023_new_report_events',
            'migration_20191024_report_order_from_shop',
            'migration_20191028_add_field_from_domain_order',
            'migration_20191029_default_delivery_method',
            'migration_20191111_blog_permission_and_seo',
            'migration_20191115_order_success_seo_and_text',
            'migration_20191116_add_settings_classify_permission',
            'migration_20191127_sync_onboading_to_welcome',
            'migration_20191209_default_store',
            'migration_20191210_add_columns_for_pos',
            'migration_20191223_save_image_url_to_database',
            'migration_20191225_builder_product_per_row_mobile',
            'migration_20191226_register_success_text',
            'migration_20200103_soft_delete_product',
            'migration_20200117_add_settings_notify_permission',
            'migration_20200212_discount',
            'migration_20200314_chatbot_v2',
            'migration_20200312_store_receipt',
            'migration_20200410_sync_report_order_to_cms',
            'migration_20200420_add_table_order_utm',
            'migration_20200420_store_permission',
            'migration_20200421_fill_product_to_store_with_default_store',
            'migration_20200422_add_column_table_product',
            'migration_20200520_appstore_version',
            'migration_20200626_advance_store_manager_version',
            'migration_20200717_shopee_connection',
            'migration_20200731_add_column_table_shopee_order',
            'migration_20200811_novaonx_social',
            'migration_20200816_add_column_demo_blog_table',
            'migration_20200812_lazada_connection',
            'migration_20200817_move_chatbot_to_app_store',
            'migration_20200818_onfluencer_app',
            'migration_20200821_lazada_sync_order',
            'migration_20200817_add_column_alt_image_blog_table',
            'migration_20200819_add_columns_for_customer',
            'migration_20200904_cash_flow',
            'migration_20200821_lazada_sync_order_again',
            'migration_20200904_activity_log_analytics',
            'migration_20200921_lazada_product_short_description',
            'migration_20200915_shopee_advance_product',
            'migration_20200916_pull_image_from_cloudinary',
            'migration_20200922_transport_status_management',
            'migration_20201007_tracking_pos_order_complete',
            //'migration_20200924_full_text_search_product_name', // temp disable
            'migration_20201002_add_video_to_blog',
            'migration_20201021_pos_return_goods',
            'migration_20201019_bestme_package',
            'migration_20201023_update_image_size',
            'migration_20201103_create_image_folder',
            'migration_20201106_add_columns_for_report',
            'migration_20201103_create_image_folder',
            'migration_20201118_add_columns_keyword_for_blog',
            'migration_20201118_create_attribute_filter_table',
            'migration_20201130_add_permission_customize_layout',
            'migration_20201201_shopee_category_and_logistics',
            'migration_20201214_shopee_upload_add_tables',
            'migration_20210107_tuning_2_11_2',
            'migration_20210302_demo_data',
            'migration_20210310_v3_3_1_advance_permission',
            'migration_20210329_3_3_3_finetune',
            'migration_20210409_add_columns_collection_amount_for_order',
            'migration_20201125_add_column_store_id_for_return_receipt',
            'migration_20210422_add_column_store_id_for_return_receipt_if_not_exists',
            'migration_20210409_add_columns_collection_amount_for_order',
            'migration_20210504_v3_4_2_sync_bestme_social',
            'migration_20210517_add_column_order_id_on_campaign_voucher',
            'migration_20210525_update_migrate_v3_4_2_add_new_table_order_channel',
            'migration_20210616_v3_5_1_open_api',
            'migration_20210812_create_keyword_sync_table',
            'migration_20210817_add_column_source_sync',
            'migration_20210914_add_email_subscribers',
            'migration_20211012_add_new_column_to_table_product',
            'migration_20211201_add_source_author_name_to_blog_description_table',
            'migration_20220415_set_sitemap_detail_lastmod',
            'migration_20220416_add_seo_url_events',
            'migration_20220420_add_new_column_to_table_blog_category',
            'migration_20220519_ingredient',
            'migration_20220520_create_flash_sale_table',
            'migration_20220601_add_column_meta_keyword_table_blog_category',
            'migration_20220611_add_vnpay_to_my_app',
            'migration_20220601_add_column_meta_keyword_table_blog_category',
            'migration_20221013_coupon',
            'migration_20220701_discount_new',
            'migration_20220718_add_column_in_home_for_blog_table',
            'migration_20220701_discount_new',
            'migration_20220728_add_on_deal',
            'migration_20220815_change_column_content_of_blog_des_table',
            'migration_20220818_add_column_approval_status_for_blog_table',
            'migration_20220825_add_column_show_gg_product_feed_for_product_table',
            'migration_20230823_robot_meta_product'
        ];

        /* TODO: check from redis to avoid too many query to db... */

        /* TODO: do migration order by id: up: lower > higher, down: higher > lower... */

        /* filter out already run version */
        $DB_PREFIX = DB_PREFIX;
        $already_run_migrations = [];
        try {
            $query = $this->db->query("SELECT * FROM {$DB_PREFIX}migration");
            if ($query) {
                if (is_array($query->rows)) {
                    foreach ($query->rows as $row) {
                        if (!isset($row['migration']) || !isset($row['run'])) {
                            continue;
                        }

                        // if is for UP direction
                        if ($migrate_direction === self::MIGRATE_DIRECTION_UP && $row['run'] == 0) {
                            continue;
                        }

                        if ($migrate_direction === self::MIGRATE_DIRECTION_UP && isset($versions[$row['migration']]) && $row['version'] < $versions[$row['migration']]['version']) {
                            continue;
                        }

                        // if is for DOWN direction
                        if ($migrate_direction === self::MIGRATE_DIRECTION_DOWN && $row['run'] == 1) {
                            continue;
                        }

                        $already_run_migrations[] = $row['migration'];
                    }
                }

                $migration_versions = array_filter($migration_versions, function ($migration) use ($already_run_migrations) {
                    return !in_array($migration, $already_run_migrations);
                });

                if (empty($migration_versions)) {
                    return;
                }

                /* execute migrations */
                foreach ($migration_versions as $migration_version) {
                    try {
                        $this->log->write('[migration] start run migration version: ' . $migration_version);

                        $class = 'Migration\\' . $migration_version;
                        if (!class_exists($class)) {
                            continue;
                        }

                        /** @var \Migration\migration_abstract $instance */
                        if(isset($versions[$migration_version])) {
                            $instance = new $class($this->db, $this->log, $DB_PREFIX, $migration_version, $versions[$migration_version]['version']);
                        } else {
                            $instance = new $class($this->db, $this->log, $DB_PREFIX, $migration_version, 1);
                        }

                        // if is for UP direction
                        if ($migrate_direction === self::MIGRATE_DIRECTION_UP) {
                            $instance->up();
                        }

                        // if is for DOWN direction
                        if ($migrate_direction === self::MIGRATE_DIRECTION_DOWN) {
                            $instance->down();
                        }

                        $this->log->write('[migration] finished run migration version: ' . $migration_version);
                    } catch (Exception $e) {
                        $this->log->write('[migration] ERROR: failed to run migration version: ' . $migration_version . '. Detail: ' . $e->getMessage());
                    }
                }
            }
        } catch (Exception $e) {
            $this->log->write('[migration] WARNING: failed to get all migration versions, may be because of table not existed before. Continue processing. Detail: ' . $e->getMessage());
        }
    }
}