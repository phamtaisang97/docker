<?php
namespace Cart;
class User {
	private $user_id;
	private $user_group_id;
	private $user_group_name;
	private $username;
    private $type;
    private $access_all;
    private $user_stores;
	private $permission = array();
	private $log;

	public function __construct($registry) {
		$this->db = $registry->get('db');
		$this->request = $registry->get('request');
		$this->session = $registry->get('session');
		$this->log = $registry->get('log');

		if (isset($this->session->data['user_id'])) {
			$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE user_id = '" . (int)$this->session->data['user_id'] . "' AND status = '1'");

			if ($user_query->num_rows) {
				$this->user_id = $user_query->row['user_id'];
				$this->username = $user_query->row['username'];
				$this->user_group_id = $user_query->row['user_group_id'];
                $this->type = isset($user_query->row['type']) ? $user_query->row['type'] : '';

				$this->db->query("UPDATE " . DB_PREFIX . "user SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE user_id = '" . (int)$this->session->data['user_id'] . "'");

				$user_group_query = $this->db->query("SELECT permission, access_all, name FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");
                $this->access_all = $user_group_query->row['access_all'];
                $this->user_group_name = $user_group_query->row['name'];
				$permissions = json_decode($user_group_query->row['permission'], true);

				if (is_array($permissions)) {
					foreach ($permissions as $key => $value) {
						$this->permission[$key] = $value;
					}
				}

				if($this->type === 'Admin') {
                    $user_store_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "store where 1");
                } else {
                    $user_store_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user_store where user_id= '" . (int)$this->session->data['user_id'] . "'");
                }
                $this->user_stores = $user_store_query->rows;
                $this->user_stores = array_column($user_store_query->rows, 'store_id');
			} else {
				$this->logout();
			}
		}
	}

	public function login($username, $password) {
		$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE username = '" . $this->db->escape($username) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1'");

		if ($user_query->num_rows) {
			$this->session->data['user_id'] = $user_query->row['user_id'];

			$this->user_id = $user_query->row['user_id'];
			$this->username = $user_query->row['username'];
			$this->user_group_id = $user_query->row['user_group_id'];
            $this->type = isset($user_query->row['type']) ? $user_query->row['type'] : '';

			$user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");

			$permissions = json_decode($user_group_query->row['permission'], true);

			if (is_array($permissions)) {
				foreach ($permissions as $key => $value) {
					$this->permission[$key] = $value;
				}
			}
			return true;
		} else {
			return false;
		}
	}

    public function loginFromCms($username, $password, $token = null, $email = null, $shop = null) {
        $user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE username = '" . $this->db->escape($username) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1'");

        if ($user_query->num_rows) {
            $this->session->data['user_id'] = $user_query->row['user_id'];

            $this->user_id = $user_query->row['user_id'];
            $this->username = $user_query->row['username'];
            $this->user_group_id = $user_query->row['user_group_id'];
            $this->type = isset($user_query->row['type']) ? $user_query->row['type'] : '';

            $user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");

            $permissions = json_decode($user_group_query->row['permission'], true);

            if (is_array($permissions)) {
                foreach ($permissions as $key => $value) {
                    $this->permission[$key] = $value;
                }
            }
            // save session_id to CMS
            return $this->curlSaveSession($token, $email, $shop);
        } else {
            return false;
        }
    }

    private function curlSaveSession($token, $email, $shop) {
        try{
            $data = array(
                'session_id' => $this->session->getID(),
                'token' => $token,
                'shop' => $shop,
                'email' => $email
            );

            $payload = json_encode($data);

            $url = BESTME_SERVER . 'api/v1/remember-login-shop';


            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $payload,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                    "Content-Length: " . strlen($payload),
                )
            ));

            $response = curl_exec($curl);

            //Check for errors.
            if(curl_errno($curl)){
                $this->log->write('Cannot Curl into CMS. Code: ' . curl_error($curl) );
                return false;
            }

            // Close cURL session handle
            curl_close($curl);

            $response = json_decode($response,true);
            if (!isset($response['code']) || $response['code'] != 200) {
                $this->log->write('Cannot save session_id into CMS. Code: ' . isset($response['code']) ? $response['code'] : 'UNKNOWN');
                return false;
            }
            return true;
        } catch(\Exception $e) {
            $this->log->write('Cannot save session_id into CMS ' . $e->getMessage());
            return false;
        }
    }

	public function loginByEmailWithoutPwd($email, $token, $shop) {
	    try{
            $user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE email = '" . $this->db->escape($email) . "'");

            if ($user_query->num_rows) {
                $this->session->data['user_id'] = $user_query->row['user_id'];

                $this->user_id = $user_query->row['user_id'];
                $this->username = $user_query->row['username'];
                $this->user_group_id = $user_query->row['user_group_id'];
                $this->type = $user_query->row['type'];

                $user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");

                $permissions = json_decode($user_group_query->row['permission'], true);

                if (is_array($permissions)) {
                    foreach ($permissions as $key => $value) {
                        $this->permission[$key] = $value;
                    }
                }

                // save session_id to CMS
                //return $this->curlSaveSession($token, $email, $shop);

            } else {
                $this->log->write('CANNOT LOGIN' .$email);
                return false;
            }
        }catch(\Exception $e){
            $this->log->write('CANNOT LOGIN' .$e->getMessage());
            return false;
        }

	}

	public function logout() {
		unset($this->session->data['user_id']);

		$this->user_id = '';
		$this->username = '';
	}

	public function hasPermission($key, $value) {
		if (isset($this->permission[$key])) {
			return in_array($value, $this->permission[$key]);
		} else {
			return false;
		}
	}

	public function canAccessAll() {
	    return $this->access_all == 1;
    }

	public function isLogged() {
		return $this->user_id;
	}

	public function getId() {
		return $this->user_id;
	}

	public function getUserName() {
		return $this->username;
	}

	public function isUserInStore($store_id) {
        return in_array($store_id, $this->user_stores);
    }

	public function getUserStores() {
	    return $this->user_stores;
    }

    public function isAdmin() {
        return $this->type === 'Admin';
    }

	public function getGroupId() {
		return $this->user_group_id;
	}

    public function isIntern() {
        $user_group_name = $this->user_group_name;
        if ($user_group_name == "TTS") {
            return true;
        }
        return false;
    }
}