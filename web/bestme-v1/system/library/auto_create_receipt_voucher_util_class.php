<?php

class AUTO_CREATE_RECEIPT_VOUCHER_UTIL_CLASS
{
    protected $db;
    protected $prefix;

    public static $RECEIPT_VOUCHER_STATUS_ID_COMPLETE = 1;
    public static $RECEIPT_VOUCHER_TYPE_ID_AUTOMATION = 8;

    public static $IN_BUSINESS_REPORT = 1;
    public static $OBJECT_CUSTOMER = 1;

    public static $CASH_FLOW_METHOD_COD = 3;
    public static $CASH_FLOW_METHOD_TRANSFER = 2;

    public function __construct($db, $prefix)
    {
        $this->db = $db;
        $this->prefix = $prefix;
    }

    public function autoUpdateFromOrder($order_id, $data)
    {
        $payment_status = $data['payment_status'] ? $data['payment_status'] : 0;
        if ((int)$payment_status == 1) {
            $this->autoAddOrUpdateFromOrder($order_id, $data);
        } else {
            $this->removeReceiptFromOrder($order_id);
        }
    }

    // Auto create receipt_voucher when have order paymented
    public function autoAddOrUpdateFromOrder($order_id, $data)
    {
        // Check if exist receipt voucher then update else create new
        $query = $this->db->query("SELECT `receipt_voucher_id` FROM `" . $this->prefix . "receipt_voucher` WHERE `order_id` = " . (int)$order_id . " ");

        if ($query->num_rows > 0 && isset($query->row['receipt_voucher_id'])) {
            $this->autoUpdateVoucherFromOrder($order_id, $data);
        } else {
            $this->autoAddFromOrder($order_id, $data);
        }
    }

    public function autoAddFromOrder($order_id, $data)
    {
        $code = $this->autoGenerationCode();
        $in_business_status = self::$IN_BUSINESS_REPORT;
        $receipt_voucher_type_id = self::$RECEIPT_VOUCHER_TYPE_ID_AUTOMATION;
        $object_info = isset($data['customer_info']) ? $data['customer_info'] : null;
        $store_id = isset($data['store_id']) ? $data['store_id'] : null;
        $date_added = isset($data['created_at']) ? $data['created_at'] : null;
        $date_modified = isset($data['updated_at']) ? $data['updated_at'] : null;
        $method_id = 0;
        if ($data['payment_method'] == '198535319739A' || $data['payment_method'] == 'Cash on Delivery' || $data['payment_method'] == 'COD') {
            $method_id = self::$CASH_FLOW_METHOD_COD;
        } else {
            $method_id = self::$CASH_FLOW_METHOD_TRANSFER;
        }
        try {
            $user_create_id = isset($data['user_create_id']) ? $data['user_create_id'] : 1;
            $query = "INSERT INTO " . $this->prefix . "receipt_voucher 
                              SET receipt_voucher_code = '" . $code . "',
                                  receipt_voucher_type_id = '" . $receipt_voucher_type_id . "', 
                                  order_id = '" . (int)$order_id . "', 
                                  status = '" . self::$RECEIPT_VOUCHER_STATUS_ID_COMPLETE . "', 
                                  in_business_report_status = '" . $in_business_status . "', 
                                  amount = '" . (double)$this->db->escape($data['total_pay']) . "', 
                                  cash_flow_method_id = '" . $method_id . "', 
                                  cash_flow_object_id = '" . self::$OBJECT_CUSTOMER . "', 
                                  object_info = '" . $this->db->escape($object_info) . "', 
                                  note = 'Thêm tự động từ đơn hàng đã thanh toán',
                                  user_create_id = " . $user_create_id . ",";

            if($store_id){
                $query = $query." store_id = '" . $this->db->escape($store_id) . "',";
            }
            if($date_added){
                $query = $query." date_added = '" . $this->db->escape($date_added) . "',";
            }else{
                $query = $query." date_added = NOW(),";
            }
            if($date_modified){
                $query = $query." date_modified = '" . $this->db->escape($date_modified) . "'";
            }else{
                $query = $query." date_modified = NOW()";
            }

            $this->db->query($query);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function autoUpdateVoucherFromOrder($order_id, $data)
    {
        $method_id = 0;
        if ($data['payment_method'] == '198535319739A' || $data['payment_method'] == 'Cash on Delivery') {
            $method_id = self::$CASH_FLOW_METHOD_COD;
        } else {
            $method_id = self::$CASH_FLOW_METHOD_TRANSFER;
        }
        $object_info = isset($data['customer_info']) ? $data['customer_info'] : null;
        try {
            $this->db->query("UPDATE `" . $this->prefix . "receipt_voucher` 
                            SET amount = '" . (double)$this->db->escape($data['total_pay']) . "', 
                                cash_flow_method_id = '" . $method_id . "', 
                                object_info = '" . $this->db->escape($object_info) . "', 
                                date_modified = NOW() 
                                WHERE order_id = " . (int)$order_id);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function removeReceiptFromOrder($order_id)
    {
        try {
            $this->db->query("DELETE FROM " . $this->prefix . "receipt_voucher WHERE order_id = '" . (int)$order_id . "'");
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function autoGenerationCode()
    {
        // max
        $max = $this->getMaxReceiptsId();

        // make code
        $prefix = 'PTT';
        $code = $prefix . str_pad($max + 1, 6, "0", STR_PAD_LEFT);

        return $code;
    }

    private function getMaxReceiptsId()
    {
        $sql = "SELECT MAX(`receipt_voucher_id`) AS max_id FROM `" . $this->prefix . "receipt_voucher`";

        $query = $this->db->query($sql);

        return $query->row['max_id'];
    }

}