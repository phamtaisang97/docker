<?php

/**
 * Theme_Config_Util class
 */
trait Theme_Config_Util
{
    private static $LIST_THEME_SUPPORT_CUSTOMIZE_LAYOUT = [
        'tech_sun_electro',
        'food_grocerie',
        'fashion_flavor',
        'realestate_sbuilding',
        'furniture_sfurniture',
        'tech_egomall',
        'food_farm88',
        'furniture_jup_chili',
        'car_parts_auto',
        'beauty_hani',
        'tech_mechanic_hh',
        'sport_massage_chiha',
        'beauty_vattuyte',
        'food_organic_fruits',
        'other_ketoantrg',
        'other_dothovie',
        'other_petstore',
        'food_dacsanviet',
        'car_carscenr',
        'food_seafood_store',
        'furniture_nova_decor',
        'office_bookstore',
        'tech_kesaths',
        'adg_topal_prima',
        'adg_topal_slima',
        'adg_topal_vp',
        'adg_topal_xfad',
        'adg_topal_xfec',
        'adg_topal_55',
    ];

    private static $LIST_THEME_SUPPORT_CUSTOMIZE_BLOG_GRID = [
        'tech_sun_electro',
        'food_grocerie',
        'fashion_flavor',
        'realestate_sbuilding',
        'furniture_sfurniture',
        'food_dacsanviet',
        'car_carscenr',
        'tech_kesaths',
    ];

    private static $LIST_THEME_SUPPORT_VIDEO_SLIDE = [
        'tech_sun_electro',
        'food_grocerie',
        'fashion_flavor',
        'realestate_sbuilding',
        'furniture_sfurniture',
        'tech_egomall',
        'food_farm88',
        'furniture_jup_chili',
        'car_parts_auto',
        'beauty_hani',
        'tech_mechanic_hh',
        'sport_massage_chiha',
        'beauty_vattuyte',
        'fashion_ap_signme',
        'food_dacsanviet',
        'furniture_nova_decor',
        'food_organic_fruits',
        'office_bookstore',
        'tech_kesaths',
        'food_kafeviet',
        'furniture_furniter',
        'fashion_dhwatch',
        'food_hanafood',
        'beauty_ytehoanghuy'
    ];

    private static $LIST_THEME_SUPPORT_TAB_DESCRIPTION = [
        'tech_sun_electro',
        'food_grocerie',
        'fashion_flavor',
        'realestate_sbuilding',
        'furniture_sfurniture',
        'tech_kesaths',
        'other_dhpet',
        'adg_topal_prima',
        'adg_topal_slima',
        'adg_topal_vp',
        'adg_topal_xfad',
        'adg_topal_xfec',
        'adg_topal_55',
        'furniture_furniter',
    ];

    private static $LIST_THEME_SUPPORT_RATE_FORM = [
        'tech_sun_electro',
        'food_grocerie',
        'fashion_flavor',
        'realestate_sbuilding',
        'furniture_sfurniture',
        'tech_kesaths',
        'food_kafeviet',
        'furniture_furniter',
        'car_parts_auto',
        'beauty_ytehoanghuy'
    ];

    private static $LIST_THEMES_SUPPORT_PRODUCT_VERSION_IMAGE = [
        'food_grocerie',
        'tech_sun_electro',
        'tech_kesaths',
        'food_kafeviet',
        'furniture_furniter',
        'realestate_sbuilding',
        'furniture_sfurniture',
        'fashion_flavor',
        'fashion_dhwatch',
        'food_hanafood',
        'food_farm88',
        'food_dacsanviet'
    ];

    private static $LIST_THEMES_LOAD_CONTACT_IN_HOME = [
        'food_kafeviet'
    ];

    private static $LIST_THEME_LOAD_EMAIL_SUBSCRIBERS = [
        'furniture_lujun_decor',
        'car_parts_auto',
        'tech_scomputer',
        'food_grocerie'
    ];

    private static $CUSTOMIZE_LAYOUT = 'customize_layout';
    private static $CUSTOMIZE_BLOG_GRID = 'customize_blog_grid';
    private static $VIDEO_SLIDE = 'video_slide';
    private static $TAB_DESCRIPTION = 'tab_description';
    private static $RATE_FORM = 'rate_form';
    private static $PRODUCT_VERSION_IMAGE = 'product_version_image';
    private static $CONTACT_IN_HOME = 'contact_in_home';
    private static $EMAIL_SUBSCRIBERS = 'email_subscribers';

    /**
     * check if input theme supports feature or not
     * @param $feature
     * @param null $theme
     * @return bool
     */
    public function isSupportThemeFeature($feature, $theme = null)
    {
        if (empty($theme)) {
            $theme = $this->config->get('config_theme');
        }

        return in_array($theme, $this->getFeaturedThemes($feature));
    }

    /**
     * return an array of themes support feature
     * @param $feature
     * @return array|string[]
     */
    private function getFeaturedThemes($feature)
    {
        switch ($feature) {
            case self::$CUSTOMIZE_LAYOUT:
                return self::$LIST_THEME_SUPPORT_CUSTOMIZE_LAYOUT;
            case self::$CUSTOMIZE_BLOG_GRID:
                return self::$LIST_THEME_SUPPORT_CUSTOMIZE_BLOG_GRID;
            case self::$VIDEO_SLIDE:
                return self::$LIST_THEME_SUPPORT_VIDEO_SLIDE;
            case self::$TAB_DESCRIPTION:
                return self::$LIST_THEME_SUPPORT_TAB_DESCRIPTION;
            case self::$RATE_FORM:
                return self::$LIST_THEME_SUPPORT_RATE_FORM;
            case self::$PRODUCT_VERSION_IMAGE:
                return self::$LIST_THEMES_SUPPORT_PRODUCT_VERSION_IMAGE;
            case self::$CONTACT_IN_HOME:
                return self::$LIST_THEMES_LOAD_CONTACT_IN_HOME;
            case self::$EMAIL_SUBSCRIBERS:
                return self::$LIST_THEME_LOAD_EMAIL_SUBSCRIBERS;
            default:
                return [];
        }
    }

    public function getSelectedPageDueToRoute($route)
    {
        // $this->load->language('theme/common/preview');

        if (empty($route)) {
            return [
                'key' => 'homepage',
                'label' => $this->language->get('menu_text_homepage')
            ];
        }

        if (preg_match('/^section\/.*$/', $route) === 1) {
            return [
                'key' => 'homepage',
                'label' => $this->language->get('menu_text_homepage')
            ];
        }

        if (preg_match('/^section_category\/.*$/', $route) === 1) {
            return [
                'key' => 'category',
                'label' => $this->language->get('menu_text_category')
            ];
        }

        if (preg_match('/^section_product_detail\/.*$/', $route) === 1) {
            return [
                'key' => 'product_detail',
                'label' => $this->language->get('menu_text_product_detail')
            ];
        }

        if (preg_match('/^section_blog\/.*$/', $route) === 1) {
            return [
                'key' => 'blog',
                'label' => $this->language->get('menu_text_blog')
            ];
        }

        if (preg_match('/^section_contact\/.*$/', $route) === 1) {
            return [
                'key' => 'contact',
                'label' => $this->language->get('menu_text_contact')
            ];
        }

        return [
            'key' => 'homepage',
            'label' => $this->language->get('menu_text_homepage')
        ];
    }

    public function getPreviewPageUrlDueToRoute($route)
    {
        $page = $this->getPreviewPageDueToRoute($route);

        // HTTP_CATALOG use front-end for live preview!
        return HTTP_CATALOG. 'index.php?route=' . $page;
    }

    public function getPreviewConfigDueToRoute($route)
    {
        switch ($route) {
            // section ~ page common/home
            case 'section/sections':
                return [];

            case 'section/header':
                return [
                    [
                        'identify' => '.bestme-block-banner-top'
                    ],
                    [
                        'identify' => '.bestme-block-header-menu'
                    ],
                    [
                        'identify' => '.bestme-block-header-logo'
                    ]
                ];

            case 'section/slideshow':
                return [
                    [
                        'identify' => '.bestme-block-slideshow'
                    ]
                ];

            case 'section/list_product':
                return [
                    [
                        'identify' => '.bestme-block-list-product'
                    ]
                ];

            case 'section/hot_product':
                return [
                    [
                        'identify' => '.bestme-block-hot-product'
                    ]
                ];

            case 'section/best_sales_product':
                return [
                    [
                        'identify' => '.bestme-block-best-sales-product'
                    ]
                ];

            case 'section/best_views_product':
                return [
                    [
                        'identify' => '.bestme-block-best-views-product'
                    ]
                ];

            case 'section/new_product':
                return [
                    [
                        'identify' => '.bestme-block-new-product'
                    ]
                ];

            case 'section/sections/product_groups':
                return [
                    [
                        'identify' => '.bestme-block-custom-group'
                    ]
                ];

            case 'section/detail_product':
                return [
                    [
                        'identify' => '.bestme-block-detail-product'
                    ]
                ];

            case 'section/content_customize':
                return [
                    [
                        'identify' => '.bestme-block-content-customize'
                    ]
                ];

            case 'section/banner':
                return [
                    [
                        'identify' => '.bestme-block-banner-1'
                    ],
                    [
                        'identify' => '.bestme-block-banner-2'
                    ],
                    [
                        'identify' => '.bestme-block-banner-3'
                    ],
                    [
                        'identify' => '.bestme-block-banner-custom'
                    ]
                ];

            case 'section/partner':
                return [
                    [
                        'identify' => '.bestme-block-partner'
                    ]
                ];

            case 'section/blog':
                return [
                    [
                        'identify' => '.bestme-block-blog'
                    ]
                ];

            case 'section/footer':
                return [
                    [
                        'identify' => '.bestme-block-footer'
                    ]
                ];

            // theme
            case 'theme/color':
            case 'theme/text':
                return [
                    []
                ];

            case 'theme/social':
                return [
                    [
                        'identify' => '.bestme-block-social'
                    ]
                ];

            case 'theme/section_theme':
                return [
                    [
                        'non-identify' => ''
                    ]
                ];

            // product category
            case 'section_category/product_category':
                return [
                    [
                        'identify' => '.bestme-block-category-product-category'
                    ]
                ];

            case 'section_category/product_list':
                return [
                    [
                        'identify' => '.bestme-block-category-product-list'
                    ]
                ];

            case 'section_category/banner':
                return [
                    [
                        'identify' => '.bestme-block-category-banner'
                    ]
                ];

            case 'section_category/filter':
                return [
                    [
                        'identify' => '.bestme-block-filter-1'
                    ],
                    [
                        'identify' => '.bestme-block-filter-2'
                    ],
                    [
                        'identify' => '.bestme-block-filter-3'
                    ]
                ];

            // product detail
            case 'section_product_detail/related_product':
                return [
                    [
                        'identify' => '.bestme-block-product-related-product'
                    ]
                ];

            // blog
            case 'section_blog/blog_category':
                return [
                    [
                        'identify' => '.bestme-block-blog-category'
                    ]
                ];

            case 'section_blog/blog_list':
                return [
                    [
                        'identify' => '.bestme-block-blog-list'
                    ]
                ];

            case 'section_blog/latest_blog':
                return [
                    [
                        'identify' => '.bestme-block-blog-latest-blog'
                    ]
                ];

            // rate
            case 'section/rate':
                return [
                    [
                        'identify' => '.bestme-block-rate-website'
                    ]
                ];

            // contact
            case 'section_contact/map':
                return [
                    [
                        'identify' => '.bestme-block-contact-map'
                    ]
                ];

            case 'section_contact/contact':
                return [
                    [
                        'identify' => '.bestme-block-contact-contact'
                    ]
                ];

            case 'section_contact/form':
                return [
                    [
                        'identify' => '.bestme-block-contact-form'
                    ]
                ];


            default:
                return [];
        }
    }

    /**
     * getCurrentThemeDir
     *
     * @return string
     */
    public function getCurrentThemeDir()
    {
        $current_theme = $this->config->get('config_theme');
        /*
		 * Support load theme deeper 1 level. This is for themes are under each Theme Partner
		 * e.g
		 * - old theme structure: view/theme/tech_sample_1
		 * - now support additional: view/theme/partner_apollo_group/tech_sample_2
		 */
        $this->load->model('setting/setting');
        $theme_directory = $this->model_setting_setting->getSettingValue('theme_' . $current_theme . '_directory');

        return isset($theme_directory) ? $theme_directory : 'default';
    }

    /* =============== private functions =============== */

    private function getPreviewPageDueToRoute($route)
    {
        if (empty($route)) {
            return 'preview/home';
        }

        if (preg_match('/^section\/.*$/', $route) === 1) {
            return 'preview/home';
        }

        if (preg_match('/^section_category\/.*$/', $route) === 1) {
            return 'preview/shop';
        }

        if (preg_match('/^section_product_detail\/.*$/', $route) === 1) {
            //get product id fist
            $this->load->model('catalog/product');
            $product_id = $this->model_catalog_product->getProductIdLivePreview();
            if ($product_id) {
                return 'preview/product&product_id='.$product_id;
            }
            return 'shop/html';
        }

        if (preg_match('/^section_blog\/.*$/', $route) === 1) {
            return 'preview/blog';
        }

        if (preg_match('/^section_contact\/.*$/', $route) === 1) {
            return 'preview/contact';
        }

        return 'preview/home';
    }
}