<?php

/**
 * Online_Payment class
 */
class Online_Payment
{
    public static function getOnlinePaymentInstance($payment_app_code, $registry, $data = [])
    {
        $paym_file_abstract = sprintf('%scontroller/extension/appstore/paym_abstract.php', DIR_APPLICATION);
        $paym_file = sprintf('%scontroller/extension/appstore/%s.php', DIR_APPLICATION, $payment_app_code);
        if (!file_exists($paym_file_abstract) || !file_exists($paym_file)) {
            return null;
        }

        require_once($paym_file_abstract);
        require_once($paym_file);

        $paym_class = sprintf('ControllerExtensionAppstore%s', str_replace('_', '', $payment_app_code));
        if (!class_exists($paym_class)) {
            return null;
        }

        return new $paym_class($registry, $data);
    }
}