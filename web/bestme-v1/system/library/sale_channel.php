<?php
/**
 * @package     Bestme
 * @author      Bestme
 * @copyright   Copyright (c) 2019, Bestme - Novaon, Corp. (https://bestme.vn/)
 * @license     https://opensource.org/licenses/GPL-3.0
 * @link        https://bestme.vn
 */

/**
 * Sale_Channel class
 */
class Sale_Channel
{
    /**
     * @param string $name
     * @param mixed $credential
     * @return null|\Sale_Channel\Abstract_Sale_Channel
     */
    public static function getSaleChannel($name, $credential)
    {
        $class = 'Sale_Channel\\' . $name;
        if (!class_exists($class)) {
            return null;
        }

        /** @var \Sale_Channel\Abstract_Sale_Channel $instance */
        return new $class($name, $credential);
    }
}