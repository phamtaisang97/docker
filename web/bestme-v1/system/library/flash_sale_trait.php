<?php

/**
 * Product_Util_Trait class
 */
trait Flash_Sale_Trait
{
    use Product_Util_Trait;

    public static $FLASH_SALE_URL = 'flash-sale';

    public function flashSale()
    {
        $this->load->model('catalog/product_flashsale');
        $flashsales = $this->model_catalog_product_flashsale->getFlashsale();

        $collection_id = null;
        $time_now = date("H:i:s");
        $future_time = null;
        foreach($flashsales as $flashsale) {
            if ($flashsale['status'] == 1) {
                if ($time_now >= $flashsale['starting_time'] && $time_now <= $flashsale['ending_time']) {
                    $collection_id = $flashsale['collection_id'];
                    $data['time_flashsale_happening'] = $flashsale['starting_time'];
                    $future_time = $flashsale['ending_date']." ".$flashsale['ending_time'];
                }
                else {
                    if ($flashsale['ending_time'] > $time_now) {
                        $data['time_flashsale_upcoming'][] = [
                            'starting_time' => $flashsale['starting_time']
                        ];
                        if(!$future_time) {
                            $collection_id = $flashsale['collection_id'];
                            foreach ($data['time_flashsale_upcoming'] as $time_flashsale_upcoming) {
                                $future_time = $flashsale['starting_date']." ".$time_flashsale_upcoming['starting_time'];
                            }
                        }
                    }
                }
            }
        }
        $data['future_time'] = $future_time;
        $data['flashsales'] = isset($flashsales) ? $flashsales : [];
        $products = $this->model_catalog_product_flashsale->getProductCollections($collection_id);
        $products_ids = array_map(function ($p) {
            return $p['product_id'];
        }, $products);
        if (!empty($products_ids)) {
            $data['products_flash_sale'] = $this->getProductsByID($products_ids);
        }

        return $data;
    }

    public function getProductsByID($ids) {
        $this->load->model('catalog/product');

        $products = [];
        if (!count($ids)) {
            return $products;
        }

        foreach ($ids as $product_id) {
            if (!$product_id) {
                continue;
            }

            $product = $this->model_catalog_product->getProduct($product_id);
            $product['random'] = rand(1,100);
            if(!isset($product['price_master'])
                && !isset($product['compare_price_master'])
                && !isset($product['tax_class_id'])
                && !isset($product['min_price_version'])
                && !isset($product['product_version_status_max'])
                && !isset($product['product_master_quantity'])
                && !isset($product['sale_on_out_stock'])
            ) {
                $products = [];
            }
            else
            {
                /* convert data product */
                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $price_master = $this->currency->formatCustom($this->tax->calculate($product['price_master'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price_master = false;
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $compare_price_master = $this->currency->formatCustom($this->tax->calculate($product['compare_price_master'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $compare_price_master = false;
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $min_price_version = $this->currency->formatCustom($this->tax->calculate($product['min_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $min_price_version = false;
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $max_price_version = $this->currency->formatCustom($this->tax->calculate($product['max_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $max_price_version = false;
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $min_compare_price_version = $this->currency->formatCustom($this->tax->calculate($product['min_compare_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $min_compare_price_version = false;
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $max_compare_price_version = $this->currency->formatCustom($this->tax->calculate($product['max_compare_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $max_compare_price_version = false;
                }

                if (($this->customer->isLogged() || !$this->config->get('config_customer_price')) && isset($product['compare_price_2'])) {
                    $compare_price_2 = $this->currency->formatCustom($this->tax->calculate($product['compare_price_2'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $compare_price_2 = false;
                }

                if (($this->customer->isLogged() || !$this->config->get('config_customer_price')) && isset($product['price_2'])) {
                    $price_2 = $this->currency->formatCustom($this->tax->calculate($product['price_2'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price_2 = false;
                }

                if (empty($product['product_version_id'])) {
                    $product['price_master_real'] = ((int)$product['price_master'] != 0) ? 1 : 0;
                    $product['max_price_version_real'] = 1;
                }

                if (!empty($product['product_version_id'])) {
                    $product['max_price_version_real'] = ((int)$product['price_version_check_null'] != 0) ? 1 : 0;
                    $product['price_master_real'] = 1;
                }

                $product['percent_sale'] = empty($product['max_price_version']) ? getPercent($product['price_master'], $product['compare_price_master']) : getPercent($product['min_price_version'], $product['min_compare_price_version'], $product['max_percent_pv']);
                $product['percent_sale_value'] = empty($product['max_price_version'])
                    ? getPercent($product['price_master'], $product['compare_price_master'], 0, "")
                    : getPercent($product['min_price_version'], $product['min_compare_price_version'], $product['max_percent_pv'], "");

                if ((float)$product['price_discount'] == 0 && !empty($product['discount_id'])) {
                    $product['percent_sale'] = -100;
                    $product['percent_sale_value'] = '-100%';
                }

                $product['price_master'] = $price_master;
                $product['price_2_text'] = $price_2;
                $product['compare_price_2_text'] = $compare_price_2;
                $product['price_2'] = $product['price_2'];
                $product['compare_price_2'] = $product['compare_price_2'];
                $product['compare_price_master'] = $compare_price_master;
                $product['min_price_version'] = $min_price_version;
                $product['max_price_version'] = empty($product['max_price_version']) ? $product['max_price_version'] : $max_price_version;
                $product['min_compare_price_version'] = $min_compare_price_version;
                $product['max_compare_price_version'] = empty($product['max_compare_price_version']) ? $product['max_compare_price_version'] : $max_compare_price_version;
                $product_version_id = (int)$product['product_version_id'];
                $product['product_is_stock'] = $this->model_catalog_product->isStock($product['product_id'], $product_version_id);
                $product['product_is_stock'] = false;
                if ($product['product_version_status_max'] !== "0" && $product['sale_on_out_stock'] == 1) {
                    $product['product_is_stock'] = true;
                } elseif (empty($product['product_version_id']) && (int)$product['product_master_quantity'] > 0) {
                    $product['product_is_stock'] = true;
                } elseif ($product['product_version_status_max'] !== "0" && !empty($product['product_version_id']) && (int)$product['product_version_quantity'] > 0) {
                    $product['product_is_stock'] = true;
                }
                $product['product_mas_ver_quantity'] = empty($product['product_version_id']) ? (int)$product['product_master_quantity'] : (int)$product['product_version_quantity'];
                $product['sale_on_out_stock_check'] = true;
                if (empty($product['product_version_id']) && $product['product_master_quantity'] < 1 && $product['sale_on_out_stock'] == 0) {
                    $product['sale_on_out_stock_check'] = false;
                }
                $product['multi_versions'] = $product['multi_versions'];
                $product['is_contact_for_price'] = $this->isContactForPrice($product['multi_versions'], $product['compare_price_master'], $product['min_compare_price_version']);
            }
            if(isset($product['product_id'])) {
                $image = $this->resizeImage($product['image']);
                $product['thumb'] = $this->changeUrl($image, true, 'product');
                $product['href'] = $this->url->link('product/product', 'product_id=' . $product['product_id']);
                $products[] = $product;
            }
        }

        return $products;
    }

    private function resizeImage($image, $width = null, $height = null)
    {
        $this->load->model('tool/image');
        if ($image) {
            if ($width == null) {
                $width = $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width');
            }
            if ($height == null) {
                $height = $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height');
            }
            $image = $this->model_tool_image->resize($image, $width, $height);
        } else {
            $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height'));
        }

        return $image;
    }

}