<?php

/**
 * @package        Bestme
 * @author        Bestme
 * @copyright    Copyright (c) 2019, Bestme - Novaon, Corp. (https://bestme.vn/)
 * @license        https://opensource.org/licenses/GPL-3.0
 * @link        https://bestme.vn
 */

/**
 * Mail class
 */
class Expiration
{
    protected $db;
    protected $log;

    /**
     * Constructor
     *
     * @param DB $db
     * @param Log $log
     *
     */
    public function __construct($db, $log)
    {
        $this->db = $db;
        $this->log = $log;
    }

    public function expiration($type)
    {
        try {
            if ($type == "app") {
                // Tìm xem app có status = 1 và expiration_date < now() thì cập nhật status về 0
                $sql = "UPDATE " . DB_PREFIX . "my_app SET status = 0 WHERE 
                    status = 1 AND expiration_date IS NOT NULL 
                    AND DATE(expiration_date) < DATE(NOW()) ";
                $this->db->query($sql);
            }
        } catch (Exception $e) {
        }
    }
}