<?php

namespace Model;

use \Exception;

/**
 * Discount_Config_Order_Model class
 */
class Discount_Config_Order_Model extends Discount_Config_Model
{
    public static function fromData($data)
    {
        try {
            return (new self($data))->parse($data);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @inheritdoc
     */
    public function parse($data)
    {
        if (!is_array($data)) {
            throw new Exception();
        }

        $this->conditions = [];
        $conditions = isset($data['conditions']) ? $data['conditions'] : '';
        if (!is_array($conditions)) {
            throw new Exception();
        }

        foreach ($conditions as $condition) {
            $conditionObj = Discount_Condition_Value_Model::fromData($condition);
            if (!$conditionObj) {
                continue;
            }

            $this->conditions[] = $conditionObj;
        }

        if (!is_array($this->conditions)) {
            throw new Exception();
        }

        return $this;
    }
}