<?php

namespace Model;

use \Exception;

/**
 * Discount_Condition_Value_Model class
 */
class Discount_Condition_Quantity_Model extends Discount_Condition_Model
{
    private $min_quantity;
    private $max_quantity;
    private $type;
    private $discount;

    public static function fromData($data)
    {
        try {
            return (new self($data))->parse($data);
        } catch (Exception $e) {
            return null;
        }
    }

    public function parse($data)
    {
        $this->min_quantity = isset($data['min_quantity']) ? $data['min_quantity'] : '';
        $this->max_quantity = isset($data['max_quantity']) ? $data['max_quantity'] : '';
        $this->type = isset($data['type']) ? $data['type'] : '';
        $this->discount = isset($data['discount']) ? $data['discount'] : '';

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMinQuantity()
    {
        return $this->min_quantity;
    }

    /**
     * @return mixed
     */
    public function getMaxQuantity()
    {
        return $this->max_quantity;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }
}