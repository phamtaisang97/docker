<?php

namespace Model;

use \Exception;

/**
 * Discount_Config_Manufacturer_Model class
 */
class Discount_Config_Manufacturer_Model extends Discount_Config_Model
{
    private $all_manufacturer; // 0 or 1
    private $manufacturer_id;

    public static function fromData($data)
    {
        try {
            return (new self($data))->parse($data);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @inheritdoc
     */
    public function parse($data)
    {
        $this->all_manufacturer = isset($data['all_manufacturer']) ? $data['all_manufacturer'] : '';
        $this->manufacturer_id = isset($data['manufacturer_id']) ? $data['manufacturer_id'] : '';

        $conditions = isset($data['conditions']) ? $data['conditions'] : '';
        if (!is_array($conditions)) {
            throw new Exception();
        }

        $this->conditions = [];
        foreach ($conditions as $condition) {
            $conditionObj = Discount_Condition_Quantity_Model::fromData($condition);
            if (!$conditionObj) {
                continue;
            }

            $this->conditions[] = $conditionObj;
        }

        if (!is_array($this->conditions)) {
            throw new Exception();
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAllManufacturer()
    {
        return $this->all_manufacturer;
    }

    /**
     * @return mixed
     */
    public function getManufacturerId()
    {
        return $this->manufacturer_id;
    }
}