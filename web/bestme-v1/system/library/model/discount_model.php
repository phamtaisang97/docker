<?php

namespace Model;

use \Exception;

/**
 * Discount_Model class
 */
class Discount_Model extends Abstract_Discount_Model
{
    private $discount_id;
    private $code;
    private $discount_type_id;
    private $discount_status_id;
    private $usage_limit;
    private $times_used;

    /** @var array|Discount_Config_Model[] */
    private $config;

    private $order_source;
    private $customer_group;
    private $start_at;
    private $end_at;

    // more...

    public static function fromData($data)
    {
        try {
            return new self($data);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Constructor
     *
     * @param array $data
     * @throws Exception
     */
    public function __construct($data)
    {
        if (!is_array($data)) {
            throw new Exception();
        }

        parent::__construct($data);

        $this->discount_id = isset($data['discount_id']) ? $data['discount_id'] : '';
        $this->code = isset($data['code']) ? $data['code'] : '';

        $this->discount_type_id = isset($data['discount_type_id']) ? $data['discount_type_id'] : '';
        if (!in_array($this->discount_type_id, [1, 2, 3, 4])) { // TODO: use model const!!!
            throw new Exception();
        }

        $this->discount_status_id = isset($data['discount_status_id']) ? $data['discount_status_id'] : '';
        $this->usage_limit = isset($data['usage_limit']) ? $data['usage_limit'] : '';
        $this->times_used = isset($data['times_used']) ? $data['times_used'] : '';

        $config_raw = isset($data['config']) ? json_decode($data['config'], true) : [];
        if (!is_array($data)) {
            throw new Exception();
        }

        $this->config = [];
        foreach ($config_raw as $config) {
            switch ($this->discount_type_id) {
                case 1:
                    $config_obj = Discount_Config_Order_Model::fromData($config);
                    break;

                case 2:
                    $config_obj = Discount_Config_Product_Model::fromData($config);
                    break;

                case 3:
                    $config_obj = Discount_Config_Category_Model::fromData($config);
                    break;

                case 4:
                    $config_obj = Discount_Config_Manufacturer_Model::fromData($config);
                    break;

                default:
                    $config_obj = null;
            }

            if ($config_obj) {
                $this->config[] = $config_obj;
            }
        }

        if (!$this->config) {
            throw new Exception();
        }

        $this->order_source = isset($data['order_source']) ? $data['order_source'] : '';
        $this->customer_group = isset($data['customer_group']) ? $data['customer_group'] : '';
        $this->start_at = isset($data['start_at']) ? $data['start_at'] : '';
        $this->end_at = isset($data['end_at']) ? $data['end_at'] : '';
    }

    /**
     * @return mixed
     */
    public function getDiscountId()
    {
        return $this->discount_id;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return mixed
     */
    public function getDiscountTypeId()
    {
        return $this->discount_type_id;
    }

    /**
     * @return mixed
     */
    public function getDiscountStatusId()
    {
        return $this->discount_status_id;
    }

    /**
     * @return mixed
     */
    public function getUsageLimit()
    {
        return $this->usage_limit;
    }

    /**
     * @return mixed
     */
    public function getTimesUsed()
    {
        return $this->times_used;
    }

    /**
     * @return array|Discount_Config_Model[]
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return mixed
     */
    public function getOrderSource()
    {
        return $this->order_source;
    }

    /**
     * @return mixed
     */
    public function getCustomerGroup()
    {
        return $this->customer_group;
    }

    /**
     * @return mixed
     */
    public function getStartAt()
    {
        return $this->start_at;
    }

    /**
     * @return mixed
     */
    public function getEndAt()
    {
        return $this->end_at;
    }
}