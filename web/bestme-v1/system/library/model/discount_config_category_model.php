<?php

namespace Model;

use \Exception;

/**
 * Discount_Config_Category_Model class
 */
class Discount_Config_Category_Model extends Discount_Config_Model
{
    private $all_category; // 0 or 1
    private $category_id;

    public static function fromData($data)
    {
        try {
            return (new self($data))->parse($data);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @inheritdoc
     */
    public function parse($data)
    {
        $this->all_category = isset($data['all_category']) ? $data['all_category'] : '';
        $this->category_id = isset($data['category_id']) ? $data['category_id'] : '';

        $conditions = isset($data['conditions']) ? $data['conditions'] : '';
        if (!is_array($conditions)) {
            throw new Exception();
        }

        $this->conditions = [];
        foreach ($conditions as $condition) {
            $conditionObj = Discount_Condition_Quantity_Model::fromData($condition);
            if (!$conditionObj) {
                continue;
            }

            $this->conditions[] = $conditionObj;
        }

        if (!is_array($this->conditions)) {
            throw new Exception();
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAllCategory()
    {
        return $this->all_category;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }
}