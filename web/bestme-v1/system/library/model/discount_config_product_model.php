<?php

namespace Model;

use \Exception;

/**
 * Discount_Config_Product_Model class
 */
class Discount_Config_Product_Model extends Discount_Config_Model
{
    private $all_product; // 0 or 1
    private $product_id;
    /** @var array */
    private $product_version_ids;

    public static function fromData($data)
    {
        try {
            return (new self($data))->parse($data);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @inheritdoc
     */
    public function parse($data)
    {
        $this->all_product = isset($data['all_product']) ? $data['all_product'] : '';
        $this->product_id = isset($data['product_id']) ? $data['product_id'] : '';
        $this->product_version_ids = isset($data['product_version_ids']) ? $data['product_version_ids'] : '';

        $conditions = isset($data['conditions']) ? $data['conditions'] : '';
        if (!is_array($conditions)) {
            throw new Exception();
        }

        $this->conditions = [];
        foreach ($conditions as $condition) {
            $conditionObj = Discount_Condition_Quantity_Model::fromData($condition);
            if (!$conditionObj) {
                continue;
            }

            $this->conditions[] = $conditionObj;
        }

        if (!is_array($this->conditions)) {
            throw new Exception();
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAllProduct()
    {
        return $this->all_product;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @return array
     */
    public function getProductVersionIds()
    {
        return $this->product_version_ids;
    }
}