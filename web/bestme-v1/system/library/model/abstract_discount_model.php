<?php

namespace Model;

/**
 * Abstract_Discount_Model class
 */
abstract class Abstract_Discount_Model
{
    public $raw_config;

    /**
     * NOT SUPPORTED for <= PHP5.6. TODO: remove...
     * @param $data
     * @return self|null null on error
     */
    /*public abstract static function fromData($data);*/

    /**
     * Constructor
     *
     * @param array $data
     */
    public function __construct($data)
    {
        $this->raw_config = $data;
    }

    /**
     * @return array
     */
    public function getRawConfig()
    {
        return $this->raw_config;
    }
}