<?php

namespace Model;

use \Exception;

/**
 * Discount_Condition_Value_Model class
 */
class Discount_Condition_Value_Model extends Discount_Condition_Model
{
    private $value;
    private $type;
    private $discount;

    public static function fromData($data)
    {
        try {
            return (new self($data))->parse($data);
        } catch (Exception $e) {
            return null;
        }
    }

    public function parse($data)
    {
        $this->value = isset($data['value']) ? $data['value'] : '';
        $this->type = isset($data['type']) ? $data['type'] : '';
        $this->discount = isset($data['discount']) ? $data['discount'] : '';

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }
}