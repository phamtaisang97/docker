<?php

namespace Model;

use \Exception;

/**
 * Discount_Condition_Model class
 */
abstract class Discount_Condition_Model extends Abstract_Discount_Model
{
    /**
     * Constructor
     *
     * @param array $data
     */
    public function __construct($data)
    {
        parent::__construct($data);
    }

    /**
     * @param $data
     * @return self
     * @throws Exception
     */
    abstract public function parse($data);
}