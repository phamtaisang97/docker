<?php

namespace Model;

use \Exception;

/**
 * Discount_Config_Model class
 */
abstract class Discount_Config_Model extends Abstract_Discount_Model
{
    /** @var array|Discount_Condition_Model[] */
    public $conditions;

    /**
     * Constructor
     *
     * @param array $data
     */
    public function __construct($data)
    {
        parent::__construct($data);
    }

    /**
     * @param $data
     * @return self
     * @throws Exception
     */
    abstract public function parse($data);

    /**
     * @return array|Discount_Condition_Model[]
     */
    public function getConditions()
    {
        return $this->conditions;
    }
}