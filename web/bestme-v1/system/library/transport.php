<?php

use Transport\Abstract_Transport;

/**
 * Transport class
 */
class Transport
{
    const NATIVE_TRANSPORT_CODES = [
        'Ghn',
        'ghn', // ?
        'viettel_post',
        'VIETTEL_POST', // ?
        'vtpost', // ?
    ];

    /**
     * @var Abstract_Transport
     */
    private $adaptor;

    /**
     * Transport constructor.
     * @param string $name
     * @param mixed $credential
     * @param null $registry
     */
    public function __construct($name, $credential, $registry = null)
    {
        try {
            /* native transport by Bestme */
            if (in_array($name, self::NATIVE_TRANSPORT_CODES)) {
                $class = '\Transport\\' . ucfirst($name);
                $this->adaptor = new $class($name, $credential, $registry);

                return;
            }

            /* transport app */
            if (strpos($name, 'trans_') == 0) {
                try {
                    $this->adaptor = new Transport\App_Trans($name, $credential, $registry);
                } catch (Exception $e) {
                    $this->adaptor = null;
                }

                return;
            }
        } catch (Exception $e) {
            $this->adaptor = null;
        } throw new \Exception("Could not create '{$name}' tranport method object");
    }

    /**
     * @return Abstract_Transport
     */
    public function getAdaptor()
    {
        return $this->adaptor;
    }

    /**
     * login
     *
     * @param array $data
     * @return mixed
     */
    public function login(array $data)
    {
        return $this->adaptor->login($data);
    }

    /**
     * getConfiguredPickingWarehouse
     *
     * @param array $data
     * @param bool $type_html
     * @return mixed
     */
    public function getConfiguredPickingWarehouse(array $data, $type_html = false)
    {
        return $this->adaptor->getConfiguredPickingWarehouse($data, $type_html);
    }

    public function setMapConfiguredPickingWarehouse($data = [])
    {
        return $this->adaptor->setMapConfiguredPickingWarehouse($data);
    }

    public function getMapConfiguredPickingWarehouse($data = [])
    {
        return $this->adaptor->getMapConfiguredPickingWarehouse($data);
    }

    /**
     * getListService
     *
     * @param array $data
     * @return string
     */
    public function getListService(array $data)
    {
        return $this->adaptor->getListService($data);
    }

    /**
     * getPrice
     *
     * @param array $data
     * @return mixed
     */
    public function getPrice(array $data)
    {
        return $this->adaptor->getPrice($data);
    }

    /**
     * createOrderDelivery
     *
     * @param array $data
     * @return mixed
     */
    public function createOrderDelivery(array $data)
    {
        return $this->adaptor->createOrderDelivery($data);
    }

    /**
     * getOrderInfo
     *
     * @param array $data
     * @return mixed
     */
    public function getOrderInfo(array $data)
    {
        return $this->adaptor->getOrderInfo($data);
    }

    /**
     * cancelOrder
     *
     * @param array $data
     * @return mixed
     */
    public function cancelOrder(array $data)
    {
        return $this->adaptor->cancelOrder($data);
    }

    public function checkStatusApp() {
        return $this->adaptor->checkStatusDelivery();
    }

    /**
     * @inheritdoc
     * @return mixed
     */
    public function isConnected() {
        return $this->adaptor->isConnected();
    }

    /**
     * @inheritdoc
     * @param array $data
     */
    public function parseWebhookResponse(array $data) {
        return $this->adaptor->parseWebhookResponse($data);
    }
}