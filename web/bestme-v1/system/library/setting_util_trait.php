<?php

/**
 * Device_Util class
 */
trait Setting_Util_Trait
{
    public static $SETTING_KEY_CONFIG_DOMAINS = 'config_domains';
    public static $SETTING_KEY_CONFIG_PACKET_PAID = 'config_packet_paid';

    public static $MODULE_WEBSITE = 'visible_module_website';
    public static $MODULE_POS = 'visible_module_pos';
    public static $MODULE_ECOM_PLATFORM = 'visible_module_ecom_platform';
    public static $MODULE_SOCIAL = 'visible_module_social';
    public static $PACKET_WAIT_FOR_PAY = 'packet_wait_for_pay';

    /**
     * Get shopname for common use
     *
     * @return string
     */
    public function getShopName()
    {
        $this->load->model('setting/setting');

        return $this->model_setting_setting->getShopName();
    }

    /**
     * Get shop url for common use
     *
     * @return string
     */
    public function getShopUrl()
    {
        $this->load->model('setting/setting');

        return $this->model_setting_setting->getShopUrl();
    }

    /**
     * Get shop id for common use
     *
     * @return string
     */
    public function getShopId()
    {
        return defined(SHOP_ID) ? SHOP_ID : '';
    }

    /**
     * Get Main Domain
     *
     * @return string
     */
    public function getMainDomain()
    {
        $this->load->model('setting/setting');
        $config_domains = $this->model_setting_setting->getSettingValue(self::$SETTING_KEY_CONFIG_DOMAINS);
        $config_domains = json_decode($config_domains, true);

        if (!is_array($config_domains) ||
            !isset($config_domains['main_domain'])
        ) {
            return '';
        }

        return $config_domains['main_domain'];
    }

    /**
     * Get Redirect Domain
     *
     * @return string empty if redirect not enabled, or MainDomain if enabled
     */
    public function getRedirectDomain()
    {
        $this->load->model('setting/setting');
        $config_domains = $this->model_setting_setting->getSettingValue(self::$SETTING_KEY_CONFIG_DOMAINS);
        $config_domains = json_decode($config_domains, true);

        if (!is_array($config_domains) ||
            !isset($config_domains['redirect_to_main_domain']) ||
            !$config_domains['redirect_to_main_domain'] ||
            !isset($config_domains['main_domain'])
        ) {
            return '';
        }

        return $config_domains['main_domain'];
    }

    /**
     * check is paid shop
     *
     * @return bool
     */
    public function isPaidShop()
    {
        $this->load->model('setting/setting');

        return $this->model_setting_setting->getSettingValue(self::$SETTING_KEY_CONFIG_PACKET_PAID) == 1;
    }

    public function isEnableModule($module)
    {
        if (!in_array($module, [
            self::$MODULE_WEBSITE,
            self::$MODULE_POS,
            self::$MODULE_ECOM_PLATFORM,
            self::$MODULE_SOCIAL
        ])) {
            return 0;
        }

        $this->load->model('setting/setting');

        return $this->model_setting_setting->getSettingModuleValue($module);
    }

    public function getSettingPacketWaitForPayValue()
    {
        $this->load->model('setting/setting');

        return $this->model_setting_setting->getSettingPacketWaitForPayValue(self::$PACKET_WAIT_FOR_PAY);
    }

    public function replaceTitleAndDescription($text)
    {
        if (!empty($this->request->get['page'])) {
            $text = $text . SEO_TITLE_PAGE_AND_NUMBER . $this->request->get['page'];
        }

        return $text;
    }
}