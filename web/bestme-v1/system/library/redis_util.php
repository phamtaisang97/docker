<?php

/**
 * Redis_Util class
 */
trait Redis_Util
{
    /**
     * redis Connect
     *
     * @param null|string $mul_redis_host
     * @param null|string $mul_redis_port
     * @param null|string $mul_redis_db
     * @return null|Redis
     */
    public function redisConnect($mul_redis_host = null, $mul_redis_port = null, $mul_redis_db = null)
    {
        $redis = null;

        $mul_redis_host = !empty($mul_redis_host) ? $mul_redis_host : MUL_REDIS_HOST;
        $mul_redis_port = is_int($mul_redis_port) ? $mul_redis_port : MUL_REDIS_PORT;
        $mul_redis_db = is_int($mul_redis_db) ? $mul_redis_db : MUL_REDIS_DB;

        try {
            $redis = new \Redis();
            $redis->connect($mul_redis_host, $mul_redis_port);
            $redis->select($mul_redis_db);
        } catch (Exception $e) {
            // could not connect to redis
        }

        return $redis;
    }

    /**
     * redis Close
     *
     * @param Redis $redis
     */
    public function redisClose($redis)
    {
        if (!$redis instanceof \Redis) {
            return;
        }

        try {
            $redis->close();
        } catch (Exception $e) {
            // could not connect to redis
        }
    }
}