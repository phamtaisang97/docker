<?php

/**
 * Online_Transport class
 */
class Online_Transport
{
    public static function getOnlineTransportInstance($transport_app_code, $registry, $data = [])
    {
        $transport_file_abstract = sprintf('%scontroller/extension/appstore/trans_abstract.php', DIR_APPLICATION);
        $transport_file = sprintf('%scontroller/extension/appstore/%s.php', DIR_APPLICATION, $transport_app_code);
        if (!file_exists($transport_file_abstract) || !file_exists($transport_file)) {
            return null;
        }

        require_once($transport_file_abstract);
        require_once($transport_file);

        $trans_class = sprintf('ControllerExtensionAppstore%s', str_replace('_', '', $transport_app_code));
        if (!class_exists($trans_class)) {
            return null;
        }

        return new $trans_class($registry, $data);
    }
}