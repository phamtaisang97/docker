<?php

/**
 * RabbitMq_Trait
 */
/* setup */
// Version
//define('VERSION', '3.0.2.0');

// Configuration
if (is_file(__DIR__ . '/../config.php')) {
    require_once(__DIR__ . '/../config.php');
}

// Autoloader
if (is_file(DIR_STORAGE . 'vendor/autoload.php')) {
    require_once(DIR_STORAGE . 'vendor/autoload.php');
}

/* === sender === */
/* include the library */
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

trait RabbitMq_Trait
{
    /**
     * @param $host
     * @param int $port
     * @param string $user_db
     * @param string $password
     * @param string $queue
     * @param string $exchange
     * @param string $routing_key
     * @param string $message
     */
    function sendMessage($host, $port = 5672, $user_db = 'admin', $password = '123456', $queue, $exchange = '', $routing_key = '', $message = '') {
        try {
            /* create a connection to the server */
//          $connection = new AMQPStreamConnection('localhost', 8072, 'admin', '123456');
            $connection = new AMQPStreamConnection($host, $port, $user_db, $password); // default: 5672, 'guest', 'guest'
            $channel = $connection->channel();

            /* must declare a queue to send to */
            $channel->queue_declare($queue, false, true, false, false);

            /* publish a message to the queue */
            $msg = new AMQPMessage(
                $message,
                array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
            );
            $channel->basic_publish($msg, $exchange, $routing_key);

            //$this->log->write('Sent message ' . $queue . ' success: ' . $message);

            /* Lastly, close the channel and the connection; */
            $channel->close();
            $connection->close();
        } catch (Exception $exception) {
            $this->log->write('Send message error: ' . $exception->getMessage());
        }
    }
}