<?php

namespace Transport;


use Exception;

final class Ghn extends Abstract_Transport
{
    use \Transport_Util_Trait;

    const APP_CODE = 'trans_ons_ghn';

    const JSON_FILE_PROVINCE = DIR_SYSTEM . 'library/localisation/ghn/ghn_province.json';
    const JSON_FILE_DISTRICT = DIR_SYSTEM . 'library/localisation/ghn/ghn_district.json';

    const API_VER_1 = '1';
    const API_VER_2 = '2';

    const SHIPPING_TRANSPORT_STATUES = ['ready_to_pick', 'picking', 'money_collect_picking', 'picked', 'storing', 'transporting', 'sorting', 'delivering', 'money_collect_delivering', 'delivery_fail', 'exception'];
    const COMPLETE_TRANSPORT_STATUES = ['delivered'];
    const CANCELED_TRANSPORT_STATUES = ['cancel', 'waiting_to_return', 'return', 'return_transporting', 'return_sorting', 'returning', 'return_fail', 'returned', 'damage', 'lost'];

    const TRANSPORT_STATUSES = [
        'ORDER_STATUS_ID_COMPLETED' => self::COMPLETE_TRANSPORT_STATUES,
        'ORDER_STATUS_ID_CANCELLED' => self::CANCELED_TRANSPORT_STATUES,
        'ORDER_STATUS_ID_DELIVERING' => self::SHIPPING_TRANSPORT_STATUES
    ];

    const GHN_STATUS_ORDER_CANCEL = 'cancel';

    private static $CURRENT_VERSION = self::API_VER_2;

    /**
     * @inheritDoc
     */
    public function getDisplayName()
    {
        // TODO: language...
        return 'GHN';
    }

    /**
     * get Configured Picking Warehouse from 3rd transport
     *
     * @param string $url
     * @return string
     */

    function get_data($url)
    {
        $ch = curl_init();
        $headers = array(
            'Accept: */*',
            'Cache-Control: no-cache',
            'Cookie: PHPSESSID=471a1b88e43cb169b62984c7e885eb543',// add id
            'Accept-Encoding: gzip, deflate',
            'Referer: ' . $url . '',
            'Connection: keep-alive',
        );
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    /**
     * @inheritDoc
     */
    public function login(array $data)
    {
        switch (self::$CURRENT_VERSION) {
            case self::API_VER_2:
                return $this->loginV2($data);
        }

        return $this->loginV1($data);
    }

    /**
     * @inheritdoc
     */
    public function getConfiguredPickingWarehouse(array $data, $type_html = false)
    {
        switch (self::$CURRENT_VERSION) {
            case self::API_VER_2:
                $response = $this->getConfiguredPickingWarehouseV2($data);
                break;

            default:
                $response = $this->getConfiguredPickingWarehouseV1($data);
        }

        if ($type_html) {
            $html = '';
            $config_hub = $this->registry->config->get('config_GHN_hub');
            if ($response->code == 1) {
                foreach ($response->data as $hub) {
                    if ($hub->HubID == $config_hub) {
                        $html .= '<option selected value="' . $hub->HubID . '">' . $hub->Address . '</option>';
                    } else {
                        $html .= '<option value="' . $hub->HubID . '">' . $hub->Address . '</option>';
                    }
                }
            }

            return $html;
        }

        return json_encode($response);
    }

    /**
     * @inheritDoc
     */
    public function setMapConfiguredPickingWarehouse($data = [])
    {
        // set map
        $this->registry->load->model('setting/setting');
        $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['method']) . '_hub', isset($data['value']) ? $data['value'] : '');

        if (isset($data['ghn_otp']) && !empty($data['ghn_otp'])) {
            // add BESTME as staff of shop
            return $this->addStaffToShopByOtp($data['ghn_otp']);
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function getMapConfiguredPickingWarehouse($data = [])
    {
        // TODO: ...
    }

    /**
     * @param array $data
     * @return string
     */
    public function getListService(array $data)
    {
        switch (self::$CURRENT_VERSION) {
            case self::API_VER_2:
                return $this->getListServiceV2($data);
        }

        return $this->getListServiceV1($data);
    }

    /**
     * @inheritdoc
     */
    public function getPrice(array $data)
    {

    }

    /**
     * @inheritdoc
     */
    public function getTransportFee(array $data)
    {
        // TODO: Implement getTransportFee() method.
    }

    /**
     * @inheritdoc
     */
    public function getOrderInfo(array $data)
    {
        switch (self::$CURRENT_VERSION) {
            case self::API_VER_2:
                return $this->getOrderInfoV2($data);
        }

        return $this->getOrderInfoV1($data);
    }

    /**
     * @inheritdoc
     */
    public function createOrderDelivery(array $data)
    {
        switch (self::$CURRENT_VERSION) {
            case self::API_VER_2:
                return $this->createOrderDeliveryV2($data);
        }

        return $this->createOrderDeliveryV1($data);
    }

    /**
     * @inheritdoc
     */
    public function cancelOrder(array $data)
    {
        switch (self::$CURRENT_VERSION) {
            case self::API_VER_2:
                return $this->cancelOrderV2($data);
        }

        return $this->cancelOrderV1($data);
    }

    /**
     * @inheritdoc
     */
    public function isCancelableOrder($order_status_code)
    {
        return $order_status_code == 'ReadyToPick' ||
        $order_status_code == 'ready_to_pick' ||
        $order_status_code == 'Picking' ||
        $order_status_code == 'picking' ||
        $order_status_code == 'approved' ||
        in_array($order_status_code, self::SHIPPING_TRANSPORT_STATUES) ||
        in_array($order_status_code, self::COMPLETE_TRANSPORT_STATUES);
    }

    /**
     * @inheritdoc
     */
    public function getTransportStatusMessage($status_code)
    {
        $this->registry->load->language('extension/appstore/trans_ons_ghn');
        return $this->registry->language->get('ghn_code_' . $status_code);
    }

    /**
     * @inheritdoc
     */
    public function getProvinceCodeByBestmeProvinceCode($bestme_province_code)
    {
        // TODO: run offline then add to map file instead off online running...

        /* get bestme provice name */
        $vna = new \Vietnam_Administrative();
        $bestme_province = $vna->getProvinceByCode($bestme_province_code);
        $bestme_province_name = mb_strtolower($bestme_province['name']);
        $bestme_province_name_with_type = mb_strtolower($bestme_province['name_with_type']);

        // build set to check. e.g:
        // Hà Nội, Tỉnh Hà Nội, Tinh Hà Nội, TP Hà Nội, TP. Hà Nội, Thành phố Hà nội, ...
        $bestme_province_name_for_check = [
            escapeVietnamese($bestme_province_name),
            escapeVietnamese("Tỉnh $bestme_province_name"),
            escapeVietnamese("Tinh $bestme_province_name"),
            escapeVietnamese("TP $bestme_province_name"),
            escapeVietnamese("TP. $bestme_province_name"),
            escapeVietnamese("Thành phố $bestme_province_name"),
            escapeVietnamese($bestme_province_name_with_type)
        ];

        /* try to get province code from ghn */
        try {
            $provinces_raw = file_get_contents(self::JSON_FILE_PROVINCE);
            $result = json_decode($provinces_raw, true);
            if (json_last_error() === JSON_ERROR_NONE) {
                foreach ($result as $province) {
                    if (!is_array($province) || !array_key_exists('ProvinceID', $province) || !array_key_exists('ProvinceName', $province)) {
                        continue;
                    }

                    // compare
                    $province_name = mb_strtolower($province['ProvinceName']);
                    if (in_array(escapeVietnamese($province_name), $bestme_province_name_for_check)) {
                        return $province['ProvinceID'];
                    }
                }
            }
        } catch (Exception $e) {
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getDistrictCodeByBestmeDistrictCode($bestme_district_code, $province_code)
    {
        // TODO: run offline then add to map file instead off online running...

        /* get bestme provice name */
        $vna = new \Vietnam_Administrative();
        $bestme_district = $vna->getDistrictByCode($bestme_district_code);
        $bestme_district_name = mb_strtolower($bestme_district['name']);
        $bestme_district_name_with_type = mb_strtolower($bestme_district['name_with_type']);
        $bestme_district_name_in_int = (int)$bestme_district_name;

        // build set to check. e.g:
        // Hà Nội, Tỉnh Hà Nội, Tinh Hà Nội, TP Hà Nội, TP. Hà Nội, Thành phố Hà nội, ...
        $bestme_district_name_for_check = [
            escapeVietnamese($bestme_district_name),
            escapeVietnamese("Quận $bestme_district_name"),
            escapeVietnamese("Q $bestme_district_name"),
            escapeVietnamese("Q. $bestme_district_name"),
            escapeVietnamese("Huyện $bestme_district_name"),
            escapeVietnamese("Huyện đảo $bestme_district_name"),
            escapeVietnamese("H $bestme_district_name"),
            escapeVietnamese("H. $bestme_district_name"),
            escapeVietnamese("Thành phố $bestme_district_name"),
            escapeVietnamese("TP. $bestme_district_name"),
            escapeVietnamese("TP $bestme_district_name"),
            escapeVietnamese("Thị xã $bestme_district_name"),
            escapeVietnamese("TX $bestme_district_name"),
            escapeVietnamese("TX. $bestme_district_name"),
            escapeVietnamese($bestme_district_name_with_type),
            // again with $bestme_ward is number. E.g 03 (Quận 03) instead of 3 (Quận 3)
            escapeVietnamese($bestme_district_name_in_int),
            escapeVietnamese("Quận $bestme_district_name_in_int"),
            escapeVietnamese("Q $bestme_district_name_in_int"),
            escapeVietnamese("Q. $bestme_district_name_in_int"),
            escapeVietnamese("Huyện $bestme_district_name_in_int"),
            escapeVietnamese("Huyện đảo $bestme_district_name_in_int"),
            escapeVietnamese("H $bestme_district_name_in_int"),
            escapeVietnamese("H. $bestme_district_name_in_int"),
            escapeVietnamese("Thành phố $bestme_district_name_in_int"),
            escapeVietnamese("TP. $bestme_district_name_in_int"),
            escapeVietnamese("TP $bestme_district_name_in_int"),
            escapeVietnamese("Thị xã $bestme_district_name_in_int"),
            escapeVietnamese("TX $bestme_district_name_in_int"),
            escapeVietnamese("TX. $bestme_district_name_in_int"),
        ];

        /* try to get province code from ghn */
        $districts = self::getDistrictsByProvinceCode($province_code);
        foreach ($districts as $district) {
            if (!is_array($district) || !array_key_exists('DistrictID', $district) || !array_key_exists('DistrictName', $district)) {
                continue;
            }

            // compare
            $district_name = mb_strtolower($district['DistrictName']);
            if (in_array(escapeVietnamese($district_name), $bestme_district_name_for_check)) {
                return $district['DistrictID'];
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getWardCodeByBestmeWardCode($bestmeWardCode, $districtCode, $token)
    {
        // TODO: run offline then add to map file instead off online running...

        /* get bestme provice name */
        $vna = new \Vietnam_Administrative();
        $bestme_ward = $vna->getWardByCode($bestmeWardCode);
        $bestme_ward_name = mb_strtolower($bestme_ward['name']);
        $bestme_ward_name_with_type = mb_strtolower($bestme_ward['name_with_type']);
        $bestme_ward_name_in_int = (int)$bestme_ward_name;

        // build set to check. e.g:
        // Hà Nội, Tỉnh Hà Nội, Tinh Hà Nội, TP Hà Nội, TP. Hà Nội, Thành phố Hà nội, ...
        $bestme_district_name_for_check = [
            escapeVietnamese($bestme_ward_name),
            escapeVietnamese("Quận $bestme_ward_name"),
            escapeVietnamese("Q $bestme_ward_name"),
            escapeVietnamese("Q. $bestme_ward_name"),
            escapeVietnamese("Huyện $bestme_ward_name"),
            escapeVietnamese("Huyện đảo $bestme_ward_name"),
            escapeVietnamese("H $bestme_ward_name"),
            escapeVietnamese("H. $bestme_ward_name"),
            escapeVietnamese("Thành phố $bestme_ward_name"),
            escapeVietnamese("TP. $bestme_ward_name"),
            escapeVietnamese("TP $bestme_ward_name"),
            escapeVietnamese("Thị xã $bestme_ward_name"),
            escapeVietnamese("Thị trấn $bestme_ward_name"),
            escapeVietnamese("Phường $bestme_ward_name"),
            escapeVietnamese("TX $bestme_ward_name"),
            escapeVietnamese("Xã $bestme_ward_name"),
            escapeVietnamese("TX. $bestme_ward_name"),
            escapeVietnamese($bestme_ward_name_with_type),
            // again with $bestme_ward is number. E.g 03 (Phường 03) instead of 3 (Phường 3)
            escapeVietnamese($bestme_ward_name_in_int),
            escapeVietnamese("Quận $bestme_ward_name_in_int"),
            escapeVietnamese("Q $bestme_ward_name_in_int"),
            escapeVietnamese("Q. $bestme_ward_name_in_int"),
            escapeVietnamese("Huyện $bestme_ward_name_in_int"),
            escapeVietnamese("Huyện đảo $bestme_ward_name_in_int"),
            escapeVietnamese("H $bestme_ward_name_in_int"),
            escapeVietnamese("H. $bestme_ward_name_in_int"),
            escapeVietnamese("Thành phố $bestme_ward_name_in_int"),
            escapeVietnamese("TP. $bestme_ward_name_in_int"),
            escapeVietnamese("TP $bestme_ward_name_in_int"),
            escapeVietnamese("Thị xã $bestme_ward_name_in_int"),
            escapeVietnamese("Thị trấn $bestme_ward_name_in_int"),
            escapeVietnamese("Phường $bestme_ward_name_in_int"),
            escapeVietnamese("TX $bestme_ward_name_in_int"),
            escapeVietnamese("Xã $bestme_ward_name_in_int"),
            escapeVietnamese("TX. $bestme_ward_name_in_int"),
        ];

        /* try to get province code from ghn */
        $wards = self::getWardsByDistrictCode($districtCode, $token);
        foreach ($wards as $ward) {
            if (!is_array($ward) || !array_key_exists('WardCode', $ward) || !array_key_exists('WardName', $ward)) {
                continue;
            }

            // compare
            $district_name = mb_strtolower($ward['WardName']);
            if (in_array(escapeVietnamese($district_name), $bestme_district_name_for_check)) {
                return $ward['WardCode'];
            }
        }

        return null;
    }

    /**
     * @param string|int $province_code
     * @return array
     */
    public static function getDistrictsByProvinceCode($province_code)
    {
        $result = [];

        try {
            $district_raw = file_get_contents(self::JSON_FILE_DISTRICT);
            $districts = json_decode($district_raw, true);
            if (json_last_error() === JSON_ERROR_NONE) {
                foreach ($districts as $district) {
                    if (!is_array($district) ||
                        !array_key_exists('ProvinceID', $district) ||
                        !array_key_exists('DistrictID', $district) ||
                        !array_key_exists('DistrictName', $district)
                    ) {
                        continue;
                    }

                    // filter by $province_code
                    if ($district['ProvinceID'] != $province_code) {
                        continue;
                    }

                    $result[$district['DistrictID']] = [
                        'DistrictID' => $district['DistrictID'],
                        'DistrictName' => $district['DistrictName']
                    ];
                }
            }
        } catch (Exception $e) {
        }

        return $result;
    }

    /**
     * @param string $district_code
     * @param string $token
     * @return array
     */
    public static function getWardsByDistrictCode($district_code, $token)
    {
        switch (self::$CURRENT_VERSION) {
            case self::API_VER_2:
                return self::getWardsByDistrictCodeV2($district_code, $token);
        }

        return self::getWardsByDistrictCodeV1($district_code, $token);
    }

    /**
     * get OTP (Affiliate)
     *
     * Current for GHN only. TODO: also add to Abstract_Transport?...
     */
    public function getOtp() {
        // get phone from setting
        $phone = $this->registry->model_setting_setting->getSettingValue('config_GHN_phone');

        // if phone is not set, then trying to use username (if username is phone number),
        // or get phone from ghn api by username
        if (!$phone) {
            $username = $this->registry->model_setting_setting->getSettingValue('config_GHN_username');
            if (preg_match('/^[0-9]*$/', $username)) {
                $phone = $username;
            } else {
                // get user info
                $user_info = $this->getUserInfo($this->registry->model_setting_setting->getSettingValue('config_GHN_token'));
                if (isset($user_info['phone'])) {
                    $phone = $user_info['phone'];
                } else {
                    return false;
                }
            }

            // save phone to setting
            $this->registry->model_setting_setting->editSettingValue('config', 'config_GHN_phone', $phone);
        }

        $body = '{
            "phone": "' . $phone . '"
        }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://online-gateway.ghn.vn/shiip/public-api/v2/shop/affiliateOTP",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($body),
                "Token: " . TRANSPORT_GHN_BESTME_TOKEN
            ),
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);

        return isset($response->code) && 200 == $response->code;
    }

    /* === private functions */
    /**
     * login using token
     *
     * @param array $data
     * @return array
     */
    private function loginV2(array $data) {
        $user_info = $this->getUserInfo($data['token']);

        if (empty($user_info)) {
            return array(
                'status' => false,
                'msg' => 'error'
            );
        } else {
            $delivery_method = 'Ghn';
            if (isset($user_info['phone'])) {
                $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($delivery_method) . '_phone', $user_info['phone']);
                $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($delivery_method) . '_username', $user_info['phone']);
            } elseif (isset($user_info['email'])) {
                $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($delivery_method) . '_username', $user_info['email']);
            }

            $this->registry->load->model('setting/setting');
            $transport_config = is_null($this->registry->config->get('config_transport_active')) ? '' : $this->registry->config->get('config_transport_active');
            if ($transport_config != '') {
                $transport_list = explode(',', $transport_config);
                if (!in_array($delivery_method, $transport_list)) {
                    array_push($transport_list, $delivery_method);
                }
            } else {
                $transport_list = [$delivery_method];
            }

            $this->registry->model_setting_setting->editSettingValue('config', 'config_transport_active', implode(',', $transport_list));
            $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($delivery_method) . '_active', true);
            $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($delivery_method) . '_token', $data['token']);

            return array(
                'status' => true,
                'msg' => 'success'
            );
        }
    }

    /**
     * login v1
     *
     * @param array $data
     * @return array
     */
    private function loginV1(array $data)
    {
        $data_login = '{
            "app_name": "apiv3",
            "phone": "' . $data['username'] . '",
            "password": "' . $data['password'] . '"
        }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://online-gateway.ghn.vn/sso/public-api/v2/client/login",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data_login,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($data_login),
            ),
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);
        if ($response->code == 200) {
            $url = $response->data->url . $response->data->token;
            try {
                $html = $this->get_data($url);
                $doc = new \DOMDocument();
                libxml_use_internal_errors(true);
                $doc->loadHTML($html);
                libxml_clear_errors();
                $data['token'] = $doc->getElementById('APIKey')->getAttribute('value');
            } catch (Exception $e) {
                $data['token'] = '';
            }

            if (empty($data['token'])) {
                return array(
                    'status' => false,
                    'msg' => 'error'
                );
            }

            $this->registry->load->model('setting/setting');
            $transport_config = is_null($this->registry->config->get('config_transport_active')) ? '' : $this->registry->config->get('config_transport_active');
            if ($transport_config != '') {
                $transport_list = explode(',', $transport_config);
                if (!in_array($data['delivery_method'], $transport_list)) {
                    array_push($transport_list, $data['delivery_method']);
                }
            } else {
                $transport_list = [$data['delivery_method']];
            }

            $this->registry->model_setting_setting->editSettingValue('config', 'config_transport_active', implode(',', $transport_list));
            $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_active', true);
            $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_username', $data['username']);
            $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_password', $data['password']);
            $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_token', isset($data['token']) ? $data['token'] : '');

            //save default hubs
            $response = json_decode($this->getConfiguredPickingWarehouse($data, false));
            if (isset($response->code) && $response->code == 1) {
                $configuredPickingWarehouse = isset($response->data) && is_array($response->data) ? $response->data : [];
                foreach ($configuredPickingWarehouse as $hub) {
                    if ($hub->IsMain) {
                        $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_hub', $hub->HubID);
                    }
                }
            }

            return array(
                'status' => true,
                'msg' => 'success'
            );
        }

        return array(
            'status' => false,
            'msg' => 'success'
        );
    }

    /**
     * old login v2
     *
     * @param array $data
     * @return array
     */
    private function loginV2Old(array $data)
    {
        $data_login = '{
            "app_name": "import",
            "phone": "' . $data['username'] . '",
            "password": "' . $data['password'] . '"
        }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://online-gateway.ghn.vn/sso/public-api/v2/client/login",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data_login,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($data_login),
                /* fake user-agent because of GHN policy for website only! */
                "user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/81.0.4044.138 Chrome/81.0.4044.138 Safari/537.36"
            ),
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);
        if ($response->code == 200) {
            // call api login again
            $data_login_again = '{
                "sso_token": "' . $response->data->token . '",
                "device_id": "' . $response->data->token . '"
            }';
            $url = 'https://online-gateway.ghn.vn/shiip/public-api/login';

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $data_login_again,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                    "Content-Length: " . strlen($data_login_again),
                ),
            ));

            $login_again_response = json_decode(curl_exec($curl));
            curl_close($curl);

            $data['token'] = '';
            if (200 == $login_again_response->code) {
                // call api get token API
                $url = 'https://online-gateway.ghn.vn/shiip/public-api/v2/client/getTokenAPI';

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "Content-Type: application/json",
                        "token: " . $login_again_response->data->token,
                    ),
                ));

                $get_token_api_response = json_decode(curl_exec($curl));
                curl_close($curl);

                if (200 == $get_token_api_response->code) {
                    $data['token'] = $get_token_api_response->data->token;

                    // check if username is phone
                    if (preg_match('/^[0-9]*$/', $data['username'])) {
                        $user_phone = $data['username'];
                    } else {
                        // username is email, now do getting user info for getting phone
                        $user_info = $this->getUserInfo($data['token']);
                        if (isset($user_info['phone'])) {
                            $user_phone = $user_info['phone'];
                        }
                    }

                    // save phone to setting
                    if (isset($user_phone)) {
                        $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_phone', $user_phone);
                    }
                }
            }

            if (empty($data['token'])) {
                return array(
                    'status' => false,
                    'msg' => 'error'
                );
            }

            $this->registry->load->model('setting/setting');
            $transport_config = is_null($this->registry->config->get('config_transport_active')) ? '' : $this->registry->config->get('config_transport_active');
            if ($transport_config != '') {
                $transport_list = explode(',', $transport_config);
                if (!in_array($data['delivery_method'], $transport_list)) {
                    array_push($transport_list, $data['delivery_method']);
                }
            } else {
                $transport_list = [$data['delivery_method']];
            }

            $this->registry->model_setting_setting->editSettingValue('config', 'config_transport_active', implode(',', $transport_list));
            $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_active', true);
            $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_username', $data['username']);
            $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_password', $data['password']);
            $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_token', isset($data['token']) ? $data['token'] : '');

            //save default hubs
            $response = json_decode($this->getConfiguredPickingWarehouse($data, false));
            if (isset($response->code) && $response->code == 1) {
                $configuredPickingWarehouse = isset($response->data) && is_array($response->data) ? $response->data : [];
                foreach ($configuredPickingWarehouse as $hub) {
                    if ($hub->IsMain) {
                        $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_hub', $hub->HubID);

                        // add BESTME as staff of shop
                        $this->addStaffToShop();
                    }
                }
            }

            return array(
                'status' => true,
                'msg' => 'success'
            );
        }

        return array(
            'status' => false,
            'msg' => 'success'
        );
    }

    /**
     * get user info from token
     * @param $token
     * @return array
     */
    private function getUserInfo($token) {
        $url = 'https://online-gateway.ghn.vn/shiip/public-api/v2/client';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "token: " . $token,
            ),
        ));

        /**
         * result format
         * {
         *     "code": 200,
         *     "message": "Success",
         *     "data": {
         *         "_id": 1112171,
         *         "name": "Hà Thế Dũng",
         *         "phone": "0977200443",
         *         "email": "dunght@novaon.asia",
         *         "status": 1,
         *         "version_no": "4ab0cdf6-64bc-4dc1-81b1-48d10be12dae",
         *         "main_shop_id": 1073102,
         *         "can_set_order_code": false,
         *         "can_partial_return": false,
         *         "cant_update_cod": false,
         *         "can_full_create": false,
         *         "debt": 0,
         *         "cod": 0,
         *         "num_orders_cod": 0,
         *         "is_debt_allowed": false,
         *         "is_cod_subtracted": false,
         *         "region": 0,
         *         "bd": 0,
         *         "permission": null,
         *         "updated_ip": "118.70.80.151",
         *         "updated_client": 1112171,
         *         "updated_source": "shiip",
         *         "updated_date": "2020-07-31T08:37:40.545Z",
         *         "created_date": "2020-04-18T17:30:08.555Z",
         *         "employee_id": 0,
         *         "is_not_required_otp": false
         *     }
         * }
         */

        $response = json_decode(curl_exec($curl), true);
        curl_close($curl);

        return isset($response['code']) && 200 == $response['code'] ? $response['data'] : [];
    }

    /**
     * getConfiguredPickingWarehouse v1
     *
     * @param array $data
     * @return mixed
     */
    private function getConfiguredPickingWarehouseV1(array $data)
    {
        $token = '{
            "token": "' . $data['token'] . '"
        }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://console.ghn.vn/api/v1/apiv3/GetHubs",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $token,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($token),
            ),
        ));

        /*
         * response, format as:
         *
         {
            "code":1,
            "msg":"Success",
            "data":[
                {
                    "Address":"Ngõ 82 Dịch Vọng Hậu, Dịch Vọng Hậu, Cầu Giấy, Hà Nội, Vietnam",
                    "ClientID":1037248,
                    "ContactName":"Anh Tuyến",
                    "ContactPhone":"0969310911",
                    "DistrictID":1485,
                    "DistrictName":"Quận Cầu Giấy",
                    "Email":"",
                    "FullAddress":"Ngõ 82 Dịch Vọng Hậu, Dịch Vọng Hậu, Cầu Giấy, Hà Nội, Vietnam,Quận Cầu Giấy",
                    "HubID":2465204,
                    "IsEditHub":false,
                    "IsMain":false,
                    "Latitude":21.0297,
                    "Longitude":105.784584,
                    "ProvinceID":201,
                    "ProvinceName":"Hà Nội"
                },
                ...
            ]
        }
         */
        $response = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($response);

        return $response;
    }

    /**
     * getConfiguredPickingWarehouse v1
     *
     * @param array $data
     * @return \stdClass
     */
    private function getConfiguredPickingWarehouseV2(array $data)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://online-gateway.ghn.vn/shiip/public-api/v2/shop/all",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Token: " . $data['token'],
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        /*
         * check response. format as:
         *
         * {
         *      "code": 200,
         *      "message": "Success",
         *      "data": {
         *          "last_offset": 1248517,
         *          "shops": [
         *              {
         *                  "_id": 1073102,
         *                  "name": "Hà Thế Dũng",
         *                  "phone": "0977200443",
         *                  "address": "so 4 ngo 4 Duy Tan Cau Giay HN",
         *                  "ward_code": "1A0602",
         *                  "district_id": 1485,
         *                  "client_id": 1112171,
         *                  "bank_account_id": 0,
         *                  "status": 1,
         *                  "version_no": "f98095ef-7379-4263-92fc-5a44f306c3b7",
         *                  "updated_ip": "14.248.82.102",
         *                  "updated_employee": 0,
         *                  "updated_client": 1112171,
         *                  "updated_source": "shiip",
         *                  "updated_date": "2020-07-07T03:11:41.996Z",
         *                  "created_ip": "",
         *                  "created_employee": 0,
         *                  "created_client": 0,
         *                  "created_source": "",
         *                  "created_date": "2020-04-18T17:33:17.71Z"
         *              },
         *              ...
         *          ]
         *      }
         *  }
         */

        $response = json_decode($response);

        $result = new \stdClass();

        $result->code = 200 == $response->code ? 1 : 0;
        $result->msg = $response->message;
        $result->data = [];

        if ($response->code == 200) {
            foreach ($response->data->shops as $shop) {
                $hub = new \stdClass();
                $hub->Address = $shop->address;
                $hub->IsMain = false;
                $hub->HubID = $shop->_id;
                $hub->DistrictID = $shop->district_id;
                $hub->ContactPhone = $shop->phone;
                $hub->FullAddress = $shop->address;
                $hub->ContactName = $shop->name;

                $result->data[] = $hub;
            }
        }

        return $result;
    }

    /**
     * getListService v1
     *
     * @param array $data
     * @return string
     */
    private function getListServiceV1(array $data)
    {
        $ghnProvinceCode = $this->getProvinceCodeByBestmeProvinceCode($data['transport_shipping_province']);
        $toDistrictID = $this->getDistrictCodeByBestmeDistrictCode($data['transport_shipping_district'], $ghnProvinceCode);
        $FromDistrictID = '';
        $response = json_decode($this->getConfiguredPickingWarehouse($data, false));
        if (isset($response->code) && $response->code == 1) {
            $configuredPickingWarehouse = isset($response->data) && is_array($response->data) ? $response->data : [];
            foreach ($configuredPickingWarehouse as $hub) {
                if ($hub->HubID == $data['from_district_id_ghn']) {
                    $FromDistrictID = $hub->DistrictID;
                }
            }
        }

        $order = '{
            "token": "' . $data['token'] . '",
            "Weight": ' . $data['total_weight'] . ',
            "Length": ' . $data['package_length'] . ',
            "Width": ' . $data['package_width'] . ',
            "Height": ' . $data['package_height'] . ',
            "FromDistrictID": ' . $FromDistrictID . ',
            "ToDistrictID": ' . $toDistrictID . '
        }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://console.ghn.vn/api/v1/apiv3/FindAvailableServices",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $order,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($order),
            ),
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);
        $html = '';
        if (isset($response->code) && $response->code == 1) {
            $html .= '<h5 class="entry-content-title">' . $this->registry->language->get(strtolower($this->name)) . '</h5><ul>';
            $services = isset($response->data) && is_array($response->data) ? $response->data : [];
            foreach ($services as $service) {
                $html .= '<li class="select-shipping" data-service-name="' . $service->Name . '" data-delivery-method="' . $this->name . '" data-service_id="' . $service->ServiceID . '">' . $this->registry->language->get(strtolower($this->name)) . ' - ' . $service->Name . ' <span class="shipping-price">' . number_format($service->ServiceFee, 0, '', ',') . 'đ' . '</span></li>';
            }
            $html .= "</ul>";
        }

        return $html;
    }

    /**
     * getListService v2
     *
     * @param array $data
     * @return string|array
     */
    private function getListServiceV2(array $data)
    {
        $ghnProvinceCode = $this->getProvinceCodeByBestmeProvinceCode($data['transport_shipping_province']);
        $toDistrictID = $this->getDistrictCodeByBestmeDistrictCode($data['transport_shipping_district'], $ghnProvinceCode);
        $FromDistrictID = '';
        $response = json_decode($this->getConfiguredPickingWarehouse($data, false));
        if (isset($response->code) && $response->code == 1) {
            $configuredPickingWarehouse = isset($response->data) && is_array($response->data) ? $response->data : [];
            foreach ($configuredPickingWarehouse as $hub) {
                if ($hub->HubID == $data['from_district_id_ghn']) {
                    $FromDistrictID = $hub->DistrictID;
                }
            }
        }

        $config_hub = $this->registry->config->get('config_GHN_hub');
        $token = $this->registry->config->get('config_GHN_token');

        $order = '{
            "shop_id": ' . $config_hub . ',
            "from_district": ' . $FromDistrictID . ',
            "to_district": ' . $toDistrictID . '
        }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://online-gateway.ghn.vn/shiip/public-api/v2/shipping-order/available-services",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $order,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "token: " . $token,
            ),
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);
        if (empty($data['return_json'])) {
            $html = '';
            if (isset($response->code) && $response->code == 200) {
                $html .= '<h5 class="entry-content-title">' . $this->registry->language->get(strtolower($this->name)) . '</h5><ul>';
                $services = isset($response->data) && is_array($response->data) ? $response->data : [];
                foreach ($services as $service) {
                    // call api to calculate service fee
                    $service_fee_object = $this->calculateFee($data, $service->service_id, $FromDistrictID, $toDistrictID, $config_hub, $token);
                    $service_fee = isset($service_fee_object->service_fee) ? $service_fee_object->service_fee : 0;

                    $html .= '<li class="select-shipping" data-service-name="' . $service->short_name . '" data-delivery-method="' . $this->name . '" data-service_id="' . $service->service_id . '">' . $this->registry->language->get(strtolower($this->name)) . ' - ' . $service->short_name . ' <span class="shipping-price">' . number_format($service_fee, 0, '', ',') . 'đ' . '</span></li>';
                }
                $html .= "</ul>";
            }

            return $html;
        } else {
            $methods_result = [];
            if (isset($response->code) && $response->code == 200) {
                $services = isset($response->data) && is_array($response->data) ? $response->data : [];
                foreach ($services as $service) {
                    // call api to calculate service fee
                    $service_fee_object = $this->calculateFee($data, $service->service_id, $FromDistrictID, $toDistrictID, $config_hub, $token);
                    $service_fee = isset($service_fee_object->service_fee) ? $service_fee_object->service_fee : 0;
                    $methods_result[] = [
                        'service_name' => $service->short_name,
                        'delivery_method' => $this->name,
                        'service_id' => $service->service_id,
                        'method_name' => $this->registry->language->get(strtolower($this->name)) . ' - ' . $service->short_name,
                        'method_fee' => number_format($service_fee, 0, '', ',') . 'đ'
                    ];
                }
            }
            
            return [
                'shipping_unit_name' => $this->registry->language->get(strtolower($this->name)),
                'methods' => $methods_result
            ];
        }
    }

    /**
     * createOrderDelivery v1
     *
     * @param array $data
     * @return array
     */
    private function createOrderDeliveryV1(array $data)
    {
        $this->registry->load->language('sale/order');
        $ghnProvinceCode = $this->getProvinceCodeByBestmeProvinceCode($data['transport_shipping_province']);
        $FromHubID = $data['ghn_district_hub'];
        $FromDistrictID = '';
        $data['ghn_phone_hub'] = '';
        $data['ghn_address_hub'] = '';
        $data['ghn_name_hub'] = '';
        $data['ghn_address_transport_hub'] = '';
        $response = json_decode($this->getConfiguredPickingWarehouse($data, false));
        if (isset($response->code) && $response->code == 1) {
            $configuredPickingWarehouse = isset($response->data) && is_array($response->data) ? $response->data : [];
            foreach ($configuredPickingWarehouse as $hub) {
                if ($hub->HubID == $FromHubID) {
                    $FromDistrictID = $hub->DistrictID;
                    $data['ghn_phone_hub'] = $hub->ContactPhone;
                    $data['ghn_address_hub'] = $hub->FullAddress;
                    $data['ghn_name_hub'] = $hub->ContactName;
                    $data['ghn_address_transport_hub'] = $hub->Address;
                }
            }
        }

        if (empty($data['transport_shipping_district']) || empty($data['transport_shipping_wards']) || empty($data['transport_shipping_province'])) {
            return array(
                "status" => false,
                "msg" => $this->registry->language->get('vtp_transport_error_address')
            );
        }

        $ToDistrictID = $this->getDistrictCodeByBestmeDistrictCode($data['transport_shipping_district'], $ghnProvinceCode);
        $ToWardCode = $this->getWardCodeByBestmeWardCode($data['transport_shipping_wards'], $ToDistrictID, $data['token']);
        $ServiceID = $data['service_id'];
        $comment = $data['transport_note'];
        $allow_viewing = isset($data['allow_viewing']) ? "CHOXEMHANGKHONGTHU" : "KHONGCHOXEMHANG";

        /*
         * Use to declare parcel value. GHN will base on this value for compensation if any unexpected things happen (lost, broken...).
         * Maximum 10.000.000
         */
        $insurance_value = $data['total_pay'] >= 1e7 ? 1e7 : $data['total_pay'];

        // TODO: validate and show error for $data['collection_amount'] > maximum...
        // ghn api:
        // CoDAmount: Amount cash to collect.
        // - Maximum 50.000.000
        // - Default value: 0

        // add shop_name to order_id
        $this->registry->load->model('setting/setting');
        $shop_name = $this->registry->model_setting_setting->getShopName();
        $bestme_order_code = $shop_name . '.' . $data['shipping_bill'];

        $order = '{
            "token": "' . $data['token'] . '",
            "PaymentTypeID": 1,
            "FromDistrictID": ' . $FromDistrictID . ',
            "ToDistrictID": ' . $ToDistrictID . ',
            "ToWardCode": "' . $ToWardCode . '",
            "Note": "' . $comment . '",
            "SealCode": "",
            "ExternalCode": "' . $bestme_order_code . '",
            "ClientContactName": "' . $data['ghn_name_hub'] . '",
            "ClientContactPhone": "' . $data['ghn_phone_hub'] . '",
            "ClientAddress": "' . $data['ghn_address_hub'] . '",
            "CustomerName": "' . $data['transport_shipping_fullname'] . '",
            "CustomerPhone": "' . $data['transport_shipping_phone'] . '",
            "ShippingAddress": "' . $data['transport_shipping_address'] . '",
            "CoDAmount": ' . $data['collection_amount'] . ',
            "NoteCode": "' . $allow_viewing . '",
            "InsuranceFee": ' . $insurance_value . ',
            "ClientHubID": 0,
            "ServiceID": ' . $ServiceID . ',
            "ToLatitude": 0,
            "ToLongitude": 0,
            "FromLat": 0,
            "FromLng": 0,
            "Content": "Test nội dung",
            "CouponCode": "",
            "Weight": ' . $data['total_weight'] . ',
            "Length": ' . $data['package_length'] . ',
            "Width": ' . $data['package_width'] . ',
            "Height": ' . $data['package_height'] . ',
            "CheckMainBankAccount": false,
            "ShippingOrderCosts":
            [
                {
                    "ServiceID": ' . $ServiceID . '
                }
            ],
            "ReturnContactName": "",
            "ReturnContactPhone": "",
            "ReturnAddress": "",
            "ReturnDistrictID": 0,
            "ExternalReturnCode": "",
            "IsCreditCreate": false,
            "AffiliateID": 0
        }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://console.ghn.vn/api/v1/apiv3/CreateOrder",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $order,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($order),
            ),
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);
        if ($response->code == 1) {
            $order_code = $response->data->OrderCode;
            $this->registry->load->model('sale/order');
            $data['transport_order_code'] = $order_code;
            $getOrderInfo = $this->getOrderInfo($data);
            $this->registry->model_sale_order->updateTransportDeliveryApi($data['order_id'], $order_code, $data['transport_method'], $data['service_name'], $data['collection_amount'], '', $getOrderInfo['data']['CurrentStatus'], $data['ghn_address_transport_hub']);

            return array(
                'status' => true,
            );
        }

        return array(
            "status" => false,
            "msg" => $response->msg
        );
    }

    /**
     * createOrderDelivery v2
     *
     * @param array $data
     * @return array
     */
    private function createOrderDeliveryV2(array $data)
    {
        $this->registry->load->language('sale/order');
        $ghnProvinceCode = $this->getProvinceCodeByBestmeProvinceCode($data['transport_shipping_province']);
        $FromHubID = $data['ghn_district_hub'];
        $FromDistrictID = '';
        $data['ghn_phone_hub'] = '';
        $data['ghn_address_hub'] = '';
        $data['ghn_name_hub'] = '';
        $data['ghn_address_transport_hub'] = '';
        $response = json_decode($this->getConfiguredPickingWarehouse($data, false));
        if (isset($response->code) && $response->code == 1) {
            $configuredPickingWarehouse = isset($response->data) && is_array($response->data) ? $response->data : [];
            foreach ($configuredPickingWarehouse as $hub) {
                if ($hub->HubID == $FromHubID) {
                    $FromDistrictID = $hub->DistrictID;
                    $data['ghn_phone_hub'] = $hub->ContactPhone;
                    $data['ghn_address_hub'] = $hub->FullAddress;
                    $data['ghn_name_hub'] = $hub->ContactName;
                    $data['ghn_address_transport_hub'] = $hub->Address;
                }
            }
        }

        if (empty($data['transport_shipping_district']) || empty($data['transport_shipping_wards']) || empty($data['transport_shipping_province'])) {
            return array(
                "status" => false,
                "msg" => $this->registry->language->get('vtp_transport_error_address')
            );
        }

        $ToDistrictID = $this->getDistrictCodeByBestmeDistrictCode($data['transport_shipping_district'], $ghnProvinceCode);
        $ToWardCode = $this->getWardCodeByBestmeWardCode($data['transport_shipping_wards'], $ToDistrictID, $data['token']);
        $ServiceID = $data['service_id'];
        $comment = $data['transport_note'];
        $allow_viewing = isset($data['allow_viewing']) ? "CHOXEMHANGKHONGTHU" : "KHONGCHOXEMHANG";

        /*
         * Use to declare parcel value. GHN will base on this value for compensation if any unexpected things happen (lost, broken...).
         * Maximum 10.000.000
         */
        $insurance_value = $data['total_pay'] >= 1e7 ? 1e7 : $data['total_pay'];

        // TODO: validate and show error for $data['collection_amount'] > maximum...
        // ghn api:
        // CoDAmount: Amount cash to collect.
        // - Maximum 50.000.000
        // - Default value: 0

        // add shop_name to order_id
        $this->registry->load->model('setting/setting');
        $shop_name = $this->registry->model_setting_setting->getShopName();
        $bestme_order_code = $shop_name . '.' . $data['shipping_bill']; // notice: use order_code instead of order_id

        $order = '{
            "payment_type_id": 1,
            "note": "' . $comment . '",
            "required_note": "' . $allow_viewing . '",
            "return_phone": "",
            "return_address": "",
            "return_district_id": ' . $FromDistrictID . ',
            "return_ward_code": "",
            "client_order_code": "' . $bestme_order_code . '",
            "to_name": "' . $data['transport_shipping_fullname'] . '",
            "to_phone": "' . $data['transport_shipping_phone'] . '",
            "to_address": "' . $data['transport_shipping_address'] . '",
            "to_ward_code": "' . $ToWardCode . '",
            "to_district_id": ' . $ToDistrictID . ',
            "cod_amount": ' . $data['collection_amount'] . ',
            "content": "Test nội dung",
            "weight": ' . (int)$data['total_weight'] . ',
            "length": ' . (int)$data['package_length'] . ',
            "width": ' . (int)$data['package_width'] . ',
            "height": ' . (int)$data['package_height'] . ',
            "pick_station_id": 0,
            "insurance_value": ' . $insurance_value . ',
            "coupon": "",
            "service_id": ' . $ServiceID . ',
            "service_type_id": 0
        }';

        $shop_id = $this->registry->model_setting_setting->getSettingValue('config_' . strtoupper($data['transport_method']) . '_hub');

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://online-gateway.ghn.vn/shiip/public-api/v2/shipping-order/create",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $order,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($order),
                "Token: " . TRANSPORT_GHN_BESTME_TOKEN,
                "ShopId: " . $shop_id
            ),
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);
        if ($response->code == 200) {
            $order_code = $response->data->order_code;
            $this->registry->load->model('sale/order');
            $data['transport_order_code'] = $order_code;
            $getOrderInfo = $this->getOrderInfo($data);
            $this->registry->model_sale_order->updateTransportDeliveryApi($data['order_id'], $order_code, $data['transport_method'], $data['service_name'], $data['collection_amount'], '', $getOrderInfo['data']['status'], $data['ghn_address_transport_hub']);

            return array(
                'status' => true,
                'order_code' => $order_code,
                'transport_status' => $getOrderInfo['data']['status'],
                'delivery_address' => $data['ghn_address_transport_hub']
            );
        }

        $message = $response->message;
        if (403 == $response->code) {
            $this->registry->load->language('settings/delivery');
            $message = $this->registry->language->get('text_ghn_error_403');
        }

        return array(
            "status" => false,
            "msg" => $message
        );
    }

    /**
     * getOrderInfo v1
     *
     * @param array $data
     * @return mixed
     */
    private function getOrderInfoV1(array $data)
    {
        $order = '
        {
            "token": "' . $data['token'] . '",
            "OrderCode": "' . $data['transport_order_code'] . '"
        }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://console.ghn.vn/api/v1/apiv3/OrderInfo",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $order,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($order),
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        curl_close($curl);
        if ($response['code'] == 1) {
            $this->registry->load->model('sale/order');
            $this->registry->model_sale_order->updateStatusTransportDeliveryApi($response['data']['OrderCode'], $response['data']['CurrentStatus']);
        }
        return $response;
    }

    /**
     * getOrderInfo v2
     *
     * @param array $data
     * @return mixed
     */
    private function getOrderInfoV2(array $data)
    {
        $order = '
        {
            "order_code": "' . $data['transport_order_code'] . '"
        }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://online-gateway.ghn.vn/shiip/public-api/v2/shipping-order/detail",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $order,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($order),
                "token: " . $data['token']
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        curl_close($curl);
        if ($response['code'] == 200) {
            $this->registry->load->model('sale/order');
            $this->registry->model_sale_order->updateStatusTransportDeliveryApi($response['data']['order_code'], $response['data']['status']);
        }
        return $response;
    }

    /**
     * getWardsByDistrictCode v1
     *
     * @param $district_code
     * @param $token
     * @return array
     */
    private static function getWardsByDistrictCodeV1($district_code, $token)
    {
        $order = '
        {
            "token": "' . $token . '",
            "DistrictID": ' . $district_code . '
        }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://console.ghn.vn/api/v1/apiv3/GetWards",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $order,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($order),
            )
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        if (json_last_error() !== JSON_ERROR_NONE || !isset($response['data']['Wards'])) {
            return [];
        }

        return $response['data']['Wards'];
    }

    /**
     * getWardsByDistrictCode v2
     *
     * @param $district_code
     * @param $token
     * @return array
     */
    private static function getWardsByDistrictCodeV2($district_code, $token)
    {
        $order = '
        {
            "district_id": ' . $district_code . '
        }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://online-gateway.ghn.vn/shiip/public-api/master-data/ward?district_id",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $order,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($order),
                "token: " . $token
            )
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);

        return 200 == $response['code'] ? $response['data'] : [];
    }

    /**
     * cancelOrder v1
     *
     * @param array $data
     * @return array
     */
    private function cancelOrderV1(array $data)
    {
        $order = '{
            "token": "' . $data['token'] . '",
            "OrderCode": "' . $data['transport_order_code'] . '"
        }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://console.ghn.vn/api/v1/apiv3/CancelOrder",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $order,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($order),
            ),
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);
        if ($response->code == 1) {
            $this->registry->load->model('sale/order');
            $this->registry->model_sale_order->updateStatusDeliveryApi($data['data_order_id'], 'Cancel');

            return array(
                'status' => true,
            );
        }

        $this->registry->load->language('common/delivery_api');

        return array(
            "status" => false,
            "msg" => $this->registry->language->get('token_error')
        );
    }

    /**
     * cancelOrder v2
     *
     * @param array $data
     * @return array
     */
    private function cancelOrderV2(array $data)
    {
        $order = '{
            "order_codes": [
                "' . $data['transport_order_code'] . '"
            ]
        }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://online-gateway.ghn.vn/shiip/public-api/v2/switch-status/cancel",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $order,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($order),
                "token: " . $data['token']
            ),
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);
        if ($response->code == 200) {
            $this->registry->load->model('sale/order');
            $this->registry->model_sale_order->updateStatusDeliveryApi($data['data_order_id'], self::GHN_STATUS_ORDER_CANCEL);

            return array(
                'status' => true,
            );
        }

        $this->registry->load->language('common/delivery_api');

        return array(
            "status" => false,
            "msg" => $this->registry->language->get('token_error')
        );
    }

    /**
     * calculateFee v2
     *
     * @param array $data
     * @param $service_id
     * @param $FromDistrictID
     * @param $toDistrictID
     * @param $config_hub
     * @param $token
     * @return null
     */
    private function calculateFee(array $data, $service_id, $FromDistrictID, $toDistrictID, $config_hub, $token)
    {
        $insurance_value = $data['total_pay'] >= 1e7 ? 1e7 : $data['total_pay'];

        $body = '{
            "from_district_id": ' . $FromDistrictID . ',
            "service_id": ' . $service_id . ',
            "service_type_id": null,
            "to_district_id": ' . $toDistrictID . ',
            "to_ward_code": "' . $data['transport_shipping_wards'] . '",
            "height": ' . $data['package_height'] . ',
            "length": ' . $data['package_length'] . ',
            "weight": ' . $data['total_weight'] . ',
            "width": ' . $data['package_width'] . ',
            "insurance_fee": ' . $insurance_value . ',
            "coupon": null
        }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://online-gateway.ghn.vn/shiip/public-api/v2/shipping-order/fee",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "token: " . $token,
            ),
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);

        return isset($response->code) && $response->code == 200 ? $response->data : null;
    }

    /**
     * addStaffToShop
     *
     * @bool
     */
    private function addStaffToShop()
    {
        $token = $this->registry->model_setting_setting->getSettingValue('config_GHN_token');
        $shop_id = $this->registry->model_setting_setting->getSettingValue('config_GHN_hub');

        $body = '{
            "username": "' . TRANSPORT_GHN_BESTME_PHONE . '",
            "shop_id": ' . $shop_id . '
        }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://online-gateway.ghn.vn/shiip/public-api/v2/shop/add-client",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($body),
                "ShopId: " . $shop_id,
                "Token: " . $token
            )
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);

        return $response->code == 200 || (400 == $response->code && 'CLIENT_HAVE_EXISTED' == $response->code_message);
    }

    /**
     * addStaffToShopByOtp
     *
     * @param string $otp
     * @return bool
     */
    private function addStaffToShopByOtp($otp)
    {
        $shop_id = $this->registry->model_setting_setting->getSettingValue('config_GHN_hub');
        $phone = $this->registry->model_setting_setting->getSettingValue('config_GHN_username');

        $body = '{
            "phone": "' . $phone . '",
            "otp": "' . $otp . '",
            "shop_id": ' . $shop_id . '
        }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://online-gateway.ghn.vn/shiip/public-api/v2/shop/affiliateCreateWithShop",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($body),
                //"ShopId: " . $shop_id, // already in body data
                "Token: " . TRANSPORT_GHN_BESTME_TOKEN
            )
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);

        return $response->code == 200 || (400 == $response->code && 'CLIENT_HAVE_EXISTED' == $response->code_message);
    }

    /**
     * @inheritdoc
     * @return mixed|void
     */
    public function isConnected()
    {
        $this->registry->load->model('setting/setting');
        return is_null($this->registry->config->get('config_GHN_active'));
    }

    /**
     * @inheritdoc
     * @param array $data
     */
    public function parseWebhookResponse(array $data) {
        return [
            'order_status_id' => $this->getOrderStatus(self::TRANSPORT_STATUSES, $data['Status']),
            'transport_order_code' => $data['OrderCode'],
            'transport_status' => $data['Status']
        ];
    }
}