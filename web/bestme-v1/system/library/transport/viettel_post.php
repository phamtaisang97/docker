<?php

namespace Transport;


use Exception;

final class Viettel_post extends Abstract_Transport
{
    use \Transport_Util_Trait;

    const APP_CODE = 'trans_ons_vtpost';

    const JSON_FILE_PROVINCE = DIR_SYSTEM . 'library/localisation/viettel_post/viettel_post_province.json';

    const SHIPPING_TRANSPORT_STATUES = [-100, -108, -109, -110, 100, 102, 103, 104, 105, 200, 202, 300, 301, 302, 303, 400, 401, 402, 403, 500, 502, 505, 506, 507, 508, 509, 550];
    const COMPLETE_TRANSPORT_STATUES = [501];
    const CANCELED_TRANSPORT_STATUES = [101, 106, 107, 201, 503, 504, 510, 515];

    const TRANSPORT_STATUSES = [
        'ORDER_STATUS_ID_COMPLETED' => self::COMPLETE_TRANSPORT_STATUES,
        'ORDER_STATUS_ID_CANCELLED' => self::CANCELED_TRANSPORT_STATUES,
        'ORDER_STATUS_ID_DELIVERING' => self::SHIPPING_TRANSPORT_STATUES
    ];

    const VIETTEL_POST_STATUS_ORDER_RECEIVED = -100;
    const VIETTEL_POST_STATUS_ORDER_CANCEL = 107;

    /**
     * @inheritDoc
     */
    public function getDisplayName()
    {
        // TODO: language...
        return 'Viettel Post';
    }

    /**
     * @inheritDoc
     */
    public function login(array $data)
    {
        $input = '{
            "USERNAME":"' . $data['username'] . '",
            "PASSWORD":"' . $data['password'] . '"
        }';

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://partner.viettelpost.vn/v2/user/ownerconnect",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $input,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($input),
                "Token: " . VIETTEL_POST_TOKEN . "",
            ),
        ));
        $response = json_decode(curl_exec($curl));
        curl_close($curl);

        $this->registry->load->language('settings/settings');

        if (isset($response->error) && $response->error === false) {
            //save default hubs
            $data['token'] = isset($response->data->token) ? $response->data->token : '';
            $response_warehouse = ($this->getConfiguredPickingWarehouse($data, false));

            if (isset($response_warehouse['error']) && $response_warehouse['error'] === false) {
                if ($response_warehouse['data'] == null) {
                    return array(
                        "status" => false,
                        "msg" => $this->registry->language->get('vtp_error_address')
                    );
                }

                $hub = array_pop($response_warehouse['data']);
                if (!isset($hub) && !is_array($hub)) {
                    return false;
                }

                $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_cusId', $hub['cusId']);
                $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_groupaddress', $hub['groupaddressId']);
                $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_hub', $hub['districtId']);
                $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_province', $hub['provinceId']);
                $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_wards', $hub['provinceId']);
                $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_name', $hub['name']);
                $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_phone', $hub['phone']);
                $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_address', $hub['address']);
            }

            $this->registry->load->model('setting/setting');
            $transport_config = is_null($this->registry->config->get('config_transport_active')) ? '' : $this->registry->config->get('config_transport_active');
            if ($transport_config != '') {
                $transport_list = explode(',', $transport_config);
                if (!in_array($data['delivery_method'], $transport_list)) {
                    array_push($transport_list, $data['delivery_method']);
                }
            } else {
                $transport_list = [$data['delivery_method']];
            }

            $this->registry->model_setting_setting->editSettingValue('config', 'config_transport_active', implode(',', $transport_list));
            $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_active', true);
            $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_username', $data['username']);
            $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_password', $data['password']);
            $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_token_webhook', isset($data['token_webhook']) ? $data['token_webhook'] : '');
            $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['delivery_method']) . '_token', isset($response->data->token) ? $response->data->token : '');

            return array(
                'status' => true,
            );
        }

        return array(
            "status" => false,
            "msg" => $this->registry->language->get('vtp_error_singin')
        );
    }

    /**
     * @inheritDoc
     */
    public function getListService(array $data, $delivery_name = '')
    {
        $input = '{
            "TYPE": 1
             }
        ';

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://partner.viettelpost.vn/v2/categories/listService",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $input,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($input),
            ),
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);

        $html = '';
        $order_service_viettelpost = array("VCN", "VTK", "VHT", "SCOD");
        if (empty($data['return_json'])) {
            if (isset($response->error) && $response->error === false) {
                $html .= '<h5 class="entry-content-title">' . $this->registry->language->get(strtolower($this->name)) . '</h5><ul>';
                foreach ($response->data as $service) {
                    foreach ($order_service_viettelpost as $service_viettelpost) {
                        if ($service_viettelpost == $service->SERVICE_CODE) {
                            $data['service_code'] = $service->SERVICE_CODE;
                            $ServiceFee_viettelpostObj = $this->getPrice($data);
                            $ServiceFee_viettelpost =
                                $ServiceFee_viettelpostObj['MONEY_TOTAL_FEE'] +
                                $ServiceFee_viettelpostObj['MONEY_FEE'] +
                                $ServiceFee_viettelpostObj['MONEY_COLLECTION_FEE'] +
                                $ServiceFee_viettelpostObj['MONEY_OTHER_FEE'] +
                                $ServiceFee_viettelpostObj['MONEY_VAS'] +
                                $ServiceFee_viettelpostObj['MONEY_VAT'];
                            $html .= '<li class="select-shipping" data-service-name="' . $service->SERVICE_NAME . '" data-delivery-method="viettel_post" data-service_id="' . $service->SERVICE_CODE . '">' . $service->SERVICE_NAME . ' <span class="shipping-price">' . number_format($ServiceFee_viettelpost, 0, '', ',') . 'đ' . '</span></li>';
                        }
                    }
                }
                $html .= "</ul>";
            }

            return $html;
        } else {
            $methods_result = [];

            if (isset($response->error) && $response->error === false) {
                foreach ($response->data as $service) {
                    foreach ($order_service_viettelpost as $service_viettelpost) {
                        if ($service_viettelpost == $service->SERVICE_CODE) {
                            $data['service_code'] = $service->SERVICE_CODE;
                            $ServiceFee_viettelpostObj = $this->getPrice($data);
                            $ServiceFee_viettelpost =
                                $ServiceFee_viettelpostObj['MONEY_TOTAL_FEE'] +
                                $ServiceFee_viettelpostObj['MONEY_FEE'] +
                                $ServiceFee_viettelpostObj['MONEY_COLLECTION_FEE'] +
                                $ServiceFee_viettelpostObj['MONEY_OTHER_FEE'] +
                                $ServiceFee_viettelpostObj['MONEY_VAS'] +
                                $ServiceFee_viettelpostObj['MONEY_VAT'];
                            $methods_result[] = [
                                'service_name' => $service->SERVICE_NAME,
                                'delivery_method' => 'viettel_post',
                                'service_id' => $service->SERVICE_CODE,
                                'method_name' => $service->SERVICE_NAME,
                                'method_fee' => number_format($ServiceFee_viettelpost, 0, '', ',') . 'đ'
                            ];
                        }
                    }
                }
            }

            return [
                'shipping_unit_name' => $this->registry->language->get(strtolower($this->name)),
                'methods' => $methods_result
            ];
        }

    }

    /**
     * @inheritDoc
     */
    public function getPrice(array $data)
    {
        $config_hub = $this->registry->config->get('config_VIETTEL_POST_hub');
        $config_hub_province = $this->registry->config->get('config_VIETTEL_POST_province');

        // TODO: re-use mapped province and district code if has value before...
        $RECEIVER_PROVINCE = $this->getProvinceCodeByBestmeProvinceCode($data['transport_shipping_province']);
        $RECEIVER_DISTRICT = $this->getDistrictCodeByBestmeDistrictCode($data['transport_shipping_district'], $RECEIVER_PROVINCE);

        $input = '{
               "PRODUCT_WEIGHT":' . $data['total_weight'] . ',
               "PRODUCT_PRICE":' . $data['total_pay'] . ',
               "MONEY_COLLECTION":' . $data['total_pay'] . ',
               "ORDER_SERVICE_ADD":"",
               "ORDER_SERVICE":"' . $data['service_code'] . '",
               "SENDER_PROVINCE":"' . $config_hub_province . '",
               "SENDER_DISTRICT":"' . $config_hub . '",
               "RECEIVER_PROVINCE":"' . $RECEIVER_PROVINCE . '",
               "RECEIVER_DISTRICT":"' . $RECEIVER_DISTRICT . '",
               "PRODUCT_TYPE":"HH",
               "NATIONAL_TYPE":1
             }
        ';

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://partner.viettelpost.vn/v2/order/getPrice",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $input,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($input),
                "Token: " . $data['token'] . "",
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($response, true);
        if (json_last_error() !== JSON_ERROR_NONE ||
            !is_array($response) ||
            !array_key_exists('error', $response) ||
            !array_key_exists('status', $response) ||
            !array_key_exists('data', $response) ||
            !is_array($response['data'])
        ) {
            return null;
        }

        if ($response['error'] != false || $response['status'] != 200) {
            return null;
        }

        return $response['data'];
    }

    /**
     * @inheritDoc
     */
    public function createOrderDelivery(array $data)
    {
        $config_hub = $this->registry->config->get('config_VIETTEL_POST_hub');
        if (!isset($config_hub) && empty($config_hub)) {
            return array(
                "status" => false,
            );
        }

        $response_warehouse = ($this->getConfiguredPickingWarehouse($data, false));
        if (isset($response_warehouse['error']) == true && !empty($response_warehouse['error'])) {
            return array(
                "status" => false,
                "msg" => 'Có lỗi'
            );
        }
        $this->registry->load->language('sale/order');

        $GROUPADDRESS_ID = $this->registry->config->get('config_VIETTEL_POST_groupaddress');
        $CUS_ID = $this->registry->config->get('config_VIETTEL_POST_cusId');
        $SENDER_FULLNAME = $this->registry->config->get('config_VIETTEL_POST_name');
        $SENDER_ADDRESS = $this->registry->config->get('config_VIETTEL_POST_address');
        $SENDER_PHONE = $this->registry->config->get('config_VIETTEL_POST_phone');
        $SENDER_WARD = $this->registry->config->get('config_VIETTEL_POST_wards');
        $SENDER_DISTRICT = $this->registry->config->get('config_VIETTEL_POST_hub');
        $SENDER_PROVINCE = $this->registry->config->get('config_VIETTEL_POST_province');
        $RECEIVER_PROVINCE = $this->getProvinceCodeByBestmeProvinceCode($data['transport_shipping_province']);
        $RECEIVER_DISTRICT = $this->getDistrictCodeByBestmeDistrictCode($data['transport_shipping_district'], $RECEIVER_PROVINCE);
        $RECEIVER_WARD = $this->getWardCodeByBestmeWardCode($data['transport_shipping_wards'], $RECEIVER_DISTRICT, $data['token']);

        if (empty($RECEIVER_PROVINCE) || empty($RECEIVER_DISTRICT) || empty($RECEIVER_WARD)) {
            return array(
                "status" => false,
                "msg" => $this->registry->language->get('vtp_transport_error')
            );
        }

        $allow_viewing = isset($data['allow_viewing']) ? "CHO XEM HANG KHONG THU" : "KHONG CHO XEM HANG";
        $PRODUCT_TYPE = 'HH'; // Kiểu hàng hóa: / Product type + TH: Thư/ Envelope + HH: Hàng hóa/ Goods

        // add shop_name to order_id
        $this->registry->load->model('setting/setting');
        $shop_name = $this->registry->model_setting_setting->getShopName();
        $bestme_order_code = $shop_name . '.' . $data['shipping_bill']; // notice: use order_code instead of order_id

        $input = '{
                        "ORDER_NUMBER" : "' . $bestme_order_code . '",
                        "GROUPADDRESS_ID" : ' . $GROUPADDRESS_ID . ',
                        "CUS_ID" : ' . $CUS_ID . ',
                        "DELIVERY_DATE" : "' . date("d/m/Y H:i:s", time()) . '",
                        "SENDER_FULLNAME" : "' . $SENDER_FULLNAME . '",
                        "SENDER_ADDRESS" : "' . $SENDER_ADDRESS . '",
                        "SENDER_PHONE" : "' . $SENDER_PHONE . '",
                        "SENDER_EMAIL" : "",
                        "SENDER_WARD" : ' . $SENDER_WARD . ',
                        "SENDER_DISTRICT" : ' . $SENDER_DISTRICT . ',
                        "SENDER_PROVINCE" : ' . $SENDER_PROVINCE . ',
                        "SENDER_LATITUDE" : 0,
                        "SENDER_LONGITUDE" : 0,
                        "RECEIVER_FULLNAME" : "' . $data['transport_shipping_fullname'] . '",
                        "RECEIVER_ADDRESS" : "' . $data['transport_shipping_address'] . '",
                        "RECEIVER_PHONE" : "' . $data['transport_shipping_phone'] . '",
                        "RECEIVER_EMAIL" : "",
                        "RECEIVER_WARD" : ' . $RECEIVER_WARD . ',
                        "RECEIVER_DISTRICT" : ' . $RECEIVER_DISTRICT . ',
                        "RECEIVER_PROVINCE" : ' . $RECEIVER_PROVINCE . ',
                        "RECEIVER_LATITUDE" : 0,
                        "RECEIVER_LONGITUDE" : 0,
                        "PRODUCT_NAME" : "Bestme",
                        "PRODUCT_DESCRIPTION" : "Đơn hàng từ Bestme",
                        "PRODUCT_QUANTITY" : 1,
                        "PRODUCT_PRICE" : ' . $data['total_pay'] . ',
                        "PRODUCT_WEIGHT" : ' . $data['total_weight'] . ',
                        "PRODUCT_LENGTH" : ' . $data['package_length'] . ',
                        "PRODUCT_WIDTH" : ' . $data['package_width'] . ',
                        "PRODUCT_HEIGHT" : ' . $data['package_height'] . ',
                        "PRODUCT_TYPE" : "' . $PRODUCT_TYPE . '",
                        "ORDER_PAYMENT" : 3, 
                        "ORDER_SERVICE" : "' . $data['service_id'] . '",
                        "ORDER_SERVICE_ADD" : "",
                        "ORDER_VOUCHER" : "",
                        "ORDER_NOTE" : "' . $allow_viewing . '",
                        "MONEY_COLLECTION" : ' . $data['collection_amount'] . ',
                        "MONEY_TOTALFEE" : 0,
                        "MONEY_FEECOD" : 0,
                        "MONEY_FEEVAS" : 0,
                        "MONEY_FEEINSURRANCE" : 0,
                        "MONEY_FEE" : 0,
                        "MONEY_FEEOTHER" : 0,
                        "MONEY_TOTALVAT" : 0,
                        "MONEY_TOTAL" : 0,
                        "LIST_ITEM" : [
                            {
                                "PRODUCT_NAME" : "Bestme",
                                "PRODUCT_PRICE" : ' . $data['total_pay'] . ',
                                "PRODUCT_WEIGHT" : ' . $data['total_weight'] . ',
                                "PRODUCT_QUANTITY" : 1
                            }
                        ]
                    }';

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://partner.viettelpost.vn/v2/order/createOrder",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $input,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($input),
                "Token: " . $data['token'] . "",
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        curl_close($curl);

        if (isset($response['error']) && $response['error'] === false) {
            $order_code = $response['data']['ORDER_NUMBER'];
            $money_total = $response['data']['MONEY_TOTAL'];
            $exchange_weight = $response['data']['EXCHANGE_WEIGHT'];
            $transport_current_warehouse = $SENDER_ADDRESS . ', ' . $this->wardByDistrictAndId($SENDER_DISTRICT, $SENDER_WARD) . ', ' . $this->districtByIdAndProvince($SENDER_PROVINCE, $SENDER_DISTRICT) . ', ' . $this->provinceById($SENDER_PROVINCE);

            $this->registry->load->model('sale/order');
            $this->registry->model_sale_order->updateTransportDeliveryApi($data['order_id'], $order_code, $data['transport_method'], $data['service_name'], $money_total, $exchange_weight, self::VIETTEL_POST_STATUS_ORDER_RECEIVED, $transport_current_warehouse);

            return array(
                'status' => true,
                'order_code' => $order_code,
                'transport_status' => self::VIETTEL_POST_STATUS_ORDER_RECEIVED,
                'delivery_address' => $transport_current_warehouse
            );
        }

        return array(
            "status" => false,
            "msg" => $this->registry->language->get('vtp_transport_error')
        );
    }

    public function getConfiguredPickingWarehouse(array $data, $type_html = false)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://partner.viettelpost.vn/v2/user/listInventory",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Token: " . $data['token'] . "",
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);

        curl_close($curl);
        if ($type_html) {
            $html = '';
            $config_hub = $this->registry->config->get('config_VIETTEL_POST_hub');
            if (isset($response['error']) && $response['error'] === false) {
                foreach ($response['data'] as $hub) {
                    if ($hub['districtId'] == $config_hub) {
                        $html .= '<option data-name="' . $hub['name'] . '" data-phone="' . $hub['phone'] . '" data-address="' . $hub['address'] . '" data-cusId="' . $hub['cusId'] . '" data-groupaddress="' . $hub['groupaddressId'] . '" data-province="' . $hub['provinceId'] . '" data-wards="' . $hub['wardsId'] . '" selected value="' . $hub['districtId'] . '">' . $hub['address'] . ', ' . $this->wardByDistrictAndId($hub['districtId'], $hub['wardsId']) . ', ' . $this->districtByIdAndProvince($hub['provinceId'], $hub['districtId']) . ', ' . $this->provinceById($hub['provinceId']) . '</option>';
                    } else {
                        $html .= '<option data-name="' . $hub['name'] . '" data-phone="' . $hub['phone'] . '" data-address="' . $hub['address'] . '" data-cusId="' . $hub['cusId'] . '" data-groupaddress="' . $hub['groupaddressId'] . '" data-province="' . $hub['provinceId'] . '" data-wards="' . $hub['wardsId'] . '" value="' . $hub['districtId'] . '">' . $hub['address'] . ', ' . $this->wardByDistrictAndId($hub['districtId'], $hub['wardsId']) . ', ' . $this->districtByIdAndProvince($hub['provinceId'], $hub['districtId']) . ', ' . $this->provinceById($hub['provinceId']) . '</option>';
                    }

                }
            }

            return $html;
        }

        return $response;
    }

    public function getOrderInfo(array $data)
    {
        /*
         * notice: VTPost does not have api to get order info
         * solution: get from db and return here...
         */
        $data = array(
            'OrderCode' => isset($data['transport_order_code']) ? $data['transport_order_code'] : '',
            'transport_money_total' => isset($data['transport_money_total']) ? $data['transport_money_total'] : '',
            'transport_weight' => isset($data['transport_weight']) ? $data['transport_weight'] : '',
            'CurrentStatus' => isset($data['transport_status']) ? $data['transport_status'] : '',
            'DeliverWarehouseName' => isset($data['transport_current_warehouse']) ? $data['transport_current_warehouse'] : '',
            // ...
        );

        return array('code' => 1, 'data' => $data);
    }

    public function cancelOrder(array $data)
    {
        $order_number = $data['transport_order_code'];
        $type = 4;
        //Loại trạng thái:
        // 1. Duyệt đơn hàng/ Confirm order
        // 2. Duyệt chuyển hoàn/ Confirm return shipping
        // 3. Phát tiếp/ delivery again
        // 4. Hủy đơn hàng/ delivery again
        // 5. Lấy lại đơn hàng (Gửi lại)/ get back order (re-order)
        // 11. Xóa đơn hàng đã hủy(delete canceled order)
        $input = '
            {
               "TYPE" : ' . $type . ',
               "ORDER_NUMBER" : "' . $order_number . '",
               "NOTE" : "",
               "DATE" : ""
            }
        ';

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://partner.viettelpost.vn/v2/order/UpdateOrder",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $input,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Token: " . $data['token'] . "",
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        curl_close($curl);
        if (isset($response['error']) && $response['error'] === false) {
            $this->registry->load->model('sale/order');
            $this->registry->model_sale_order->updateStatusTransportDeliveryApi($order_number, self::VIETTEL_POST_STATUS_ORDER_CANCEL);
            return array(
                'status' => true,
            );
        }

        if (isset($response['status']) && $response['status'] == 202) {
            $this->registry->load->language('settings/settings');
            return array(
                "status" => false,
                "msg" => $this->registry->language->get('vtp_error_token')
            );
        } else {
            return array(
                "status" => false,
                "msg" => $response['message']
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function isCancelableOrder($order_status_code)
    {
        return $order_status_code == 'ReadyToPick' ||
        $order_status_code == 'ready_to_pick' ||
        $order_status_code == 'Picking' ||
        $order_status_code == 'picking' ||
        $order_status_code == 'approved' ||
        in_array($order_status_code, self::SHIPPING_TRANSPORT_STATUES) ||
        in_array($order_status_code, self::COMPLETE_TRANSPORT_STATUES);
    }

    /**
     * @inheritdoc
     */
    public function getTransportStatusMessage($status_code)
    {
        $this->registry->load->language('extension/appstore/trans_ons_vtpost');
        return $this->registry->language->get('viettel_post_code_' . $status_code);
    }

    /**
     * @inheritdoc
     */
    public function getProvinceCodeByBestmeProvinceCode($bestme_province_code)
    {
        /* get bestme provice name */
        $vna = new \Vietnam_Administrative();
        $bestme_province = $vna->getProvinceByCode($bestme_province_code);
        $bestme_province_name = mb_strtolower($bestme_province['name']);
        $bestme_province_name_with_type = mb_strtolower($bestme_province['name_with_type']);

        // build set to check. e.g:
        // Hà Nội, Tỉnh Hà Nội, Tinh Hà Nội, TP Hà Nội, TP. Hà Nội, Thành phố Hà nội, ...
        $bestme_province_name_for_check = [
            escapeVietnamese($bestme_province_name),
            escapeVietnamese("Tỉnh $bestme_province_name"),
            escapeVietnamese("Tinh $bestme_province_name"),
            escapeVietnamese("TP $bestme_province_name"),
            escapeVietnamese("TP. $bestme_province_name"),
            escapeVietnamese("Thành phố $bestme_province_name"),
            escapeVietnamese($bestme_province_name_with_type)
        ];

        /* try to get province code from viettel_post */
        try {
            $provinces_raw = file_get_contents(self::JSON_FILE_PROVINCE);
            $result = json_decode($provinces_raw, true);
            if (json_last_error() === JSON_ERROR_NONE) {
                foreach ($result as $province) {
                    if (!is_array($province) || !array_key_exists('PROVINCE_ID', $province) || !array_key_exists('PROVINCE_NAME', $province)) {
                        continue;
                    }

                    // compare
                    $province_name = mb_strtolower($province['PROVINCE_NAME']);
                    if (in_array(escapeVietnamese($province_name), $bestme_province_name_for_check)) {
                        return $province['PROVINCE_ID'];
                    }
                }
            }
        } catch (Exception $e) {
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getDistrictCodeByBestmeDistrictCode($bestme_district_code, $province_code)
    {
        // TODO: run offline then add to map file instead off online running...

        /* get bestme district name */
        $vna = new \Vietnam_Administrative();
        $bestme_district = $vna->getDistrictByCode($bestme_district_code);
        $bestme_district_name = mb_strtolower($bestme_district['name']);
        $bestme_district_name_with_type = mb_strtolower($bestme_district['name_with_type']);
        $bestme_district_name_in_int = (int)$bestme_district_name;

        // build set to check. e.g:
        // Hà Nội, Tỉnh Hà Nội, Tinh Hà Nội, TP Hà Nội, TP. Hà Nội, Thành phố Hà nội, ...
        $bestme_district_name_for_check = [
            escapeVietnamese($bestme_district_name),
            escapeVietnamese("Quận $bestme_district_name"),
            escapeVietnamese("Q $bestme_district_name"),
            escapeVietnamese("Q. $bestme_district_name"),
            escapeVietnamese("Huyện $bestme_district_name"),
            escapeVietnamese("H $bestme_district_name"),
            escapeVietnamese("H. $bestme_district_name"),
            escapeVietnamese("Thành phố $bestme_district_name"),
            escapeVietnamese("Thành phố. $bestme_district_name"),
            escapeVietnamese("TP $bestme_district_name"),
            escapeVietnamese("TP. $bestme_district_name"),
            escapeVietnamese("Thị xã $bestme_district_name"),
            escapeVietnamese("Thị xã. $bestme_district_name"),
            escapeVietnamese("TX $bestme_district_name"),
            escapeVietnamese("TX. $bestme_district_name"),
            escapeVietnamese($bestme_district_name_with_type),
            // again with $bestme_ward is number. E.g 03 (Quận 03) instead of 3 (Quận 3)
            escapeVietnamese($bestme_district_name_in_int),
            escapeVietnamese("Quận $bestme_district_name_in_int"),
            escapeVietnamese("Q $bestme_district_name_in_int"),
            escapeVietnamese("Q. $bestme_district_name_in_int"),
            escapeVietnamese("Huyện $bestme_district_name_in_int"),
            escapeVietnamese("Huyện đảo $bestme_district_name_in_int"),
            escapeVietnamese("H $bestme_district_name_in_int"),
            escapeVietnamese("H. $bestme_district_name_in_int"),
            escapeVietnamese("Thành phố $bestme_district_name_in_int"),
            escapeVietnamese("TP. $bestme_district_name_in_int"),
            escapeVietnamese("TP $bestme_district_name_in_int"),
            escapeVietnamese("Thị xã $bestme_district_name_in_int"),
            escapeVietnamese("TX $bestme_district_name_in_int"),
            escapeVietnamese("TX. $bestme_district_name_in_int"),
        ];

        /* try to get province code from ghn */
        $districts = self::getDistrictsByProvinceCode($province_code);
        foreach ($districts as $district) {
            if (!is_array($district) || !array_key_exists('DISTRICT_ID', $district) || !array_key_exists('DISTRICT_NAME', $district)) {
                continue;
            }

            // compare
            $district_name = mb_strtolower($district['DISTRICT_NAME']);
            if (in_array(escapeVietnamese($district_name), $bestme_district_name_for_check)) {
                return $district['DISTRICT_ID'];
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getWardCodeByBestmeWardCode($bestme_ward_code, $district_code, $token)
    {
        // TODO: run offline then add to map file instead off online running...

        /* get bestme district name */
        $vna = new \Vietnam_Administrative();
        $bestme_ward = $vna->getWardByCode($bestme_ward_code);
        $bestme_ward_name = mb_strtolower($bestme_ward['name']);
        $bestme_ward_name_with_type = mb_strtolower($bestme_ward['name_with_type']);
        $bestme_ward_name_in_int = (int)$bestme_ward_name;

        // build set to check. e.g:
        // Hà Nội, Tỉnh Hà Nội, Tinh Hà Nội, TP Hà Nội, TP. Hà Nội, Thành phố Hà nội, ...
        $bestme_district_name_for_check = [
            escapeVietnamese($bestme_ward_name),
            escapeVietnamese("Phường $bestme_ward_name"),
            escapeVietnamese("Ph $bestme_ward_name"),
            escapeVietnamese("Ph. $bestme_ward_name"),
            escapeVietnamese("P $bestme_ward_name"),
            escapeVietnamese("P. $bestme_ward_name"),
            escapeVietnamese("Thị trấn $bestme_ward_name"),
            escapeVietnamese("T.trấn $bestme_ward_name"),
            escapeVietnamese("TT $bestme_ward_name"),
            escapeVietnamese("TT. $bestme_ward_name"),
            escapeVietnamese("Xã $bestme_ward_name"),
            escapeVietnamese("X $bestme_ward_name"),
            escapeVietnamese("X. $bestme_ward_name"),
            escapeVietnamese($bestme_ward_name_with_type),
            // again with $bestme_ward is number. E.g 03 (Phường 03) instead of 3 (Phường 3)
            escapeVietnamese($bestme_ward_name_in_int),
            escapeVietnamese("Quận $bestme_ward_name_in_int"),
            escapeVietnamese("Q $bestme_ward_name_in_int"),
            escapeVietnamese("Q. $bestme_ward_name_in_int"),
            escapeVietnamese("Huyện $bestme_ward_name_in_int"),
            escapeVietnamese("Huyện đảo $bestme_ward_name_in_int"),
            escapeVietnamese("H $bestme_ward_name_in_int"),
            escapeVietnamese("H. $bestme_ward_name_in_int"),
            escapeVietnamese("Thành phố $bestme_ward_name_in_int"),
            escapeVietnamese("TP. $bestme_ward_name_in_int"),
            escapeVietnamese("TP $bestme_ward_name_in_int"),
            escapeVietnamese("Thị xã $bestme_ward_name_in_int"),
            escapeVietnamese("Thị trấn $bestme_ward_name_in_int"),
            escapeVietnamese("Phường $bestme_ward_name_in_int"),
            escapeVietnamese("TX $bestme_ward_name_in_int"),
            escapeVietnamese("Xã $bestme_ward_name_in_int"),
            escapeVietnamese("TX. $bestme_ward_name_in_int"),
        ];

        /* try to get province code from ghn */
        $wards = self::getWardsByDistrictCode($district_code);
        foreach ($wards as $ward) {
            if (!is_array($ward) || !array_key_exists('WARDS_ID', $ward) || !array_key_exists('WARDS_NAME', $ward)) {
                continue;
            }

            // compare
            $district_name = mb_strtolower($ward['WARDS_NAME']);
            if (in_array(escapeVietnamese($district_name), $bestme_district_name_for_check)) {
                return $ward['WARDS_ID'];
            }
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function setMapConfiguredPickingWarehouse($data = [])
    {
        $this->registry->load->model('setting/setting');
        $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['method']) . '_groupaddress', isset($data['groupaddress']) ? $data['groupaddress'] : '');
        $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['method']) . '_cusId', isset($data['cusId']) ? $data['cusId'] : '');
        $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['method']) . '_province', isset($data['data_province']) ? $data['data_province'] : '');
        $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['method']) . '_hub', isset($data['value']) ? $data['value'] : '');
        $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['method']) . '_wards', isset($data['data_wards']) ? $data['data_wards'] : '');
        $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['method']) . '_name', isset($data['data_name']) ? $data['data_name'] : '');
        $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['method']) . '_phone', isset($data['data_phone']) ? $data['data_phone'] : '');
        $this->registry->model_setting_setting->editSettingValue('config', 'config_' . strtoupper($data['method']) . '_address', isset($data['data_address']) ? $data['data_address'] : '');

        return true;
    }

    /**
     * @inheritDoc
     */
    public function getMapConfiguredPickingWarehouse($data = [])
    {
        // TODO: ...
    }

    public static function getDistrictsByProvinceCode($province_code)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://partner.viettelpost.vn/v2/categories/listDistrict?provinceId=" . $province_code . "",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        if (json_last_error() !== JSON_ERROR_NONE || !isset($response['data'])) {
            return [];
        }

        return $response['data'];
    }

    public static function getWardsByDistrictCode($district_code)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://partner.viettelpost.vn/v2/categories/listWards?districtId=" . $district_code . "",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        if (json_last_error() !== JSON_ERROR_NONE || !isset($response['data']) || !is_array($response['data'])) {
            return [];
        }

        return $response['data'];
    }

    /* private functions */
    /**
     * @param $provinceById
     * @return mixed|null
     */
    private function provinceById($provinceById)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://partner.viettelpost.vn/v2/categories/provinceById?provinceById=" . $provinceById . "",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        curl_close($curl);
        if (isset($response['error']) && $response['error'] === false) {
            return $response['data']['PROVINCE_NAME'];
        }
        return null;
    }

    private function districtByIdAndProvince($provinceById, $districtById)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://partner.viettelpost.vn/v2/categories/districtByIdAndProvince?provinceById=" . $provinceById . "&districtId=" . $districtById . "",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        curl_close($curl);
        if (isset($response['error']) && $response['error'] === false) {
            return $response['data']['DISTRICT_NAME'];
        }
        return null;
    }

    private function wardByDistrictAndId($districtId, $wardsId)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://partner.viettelpost.vn/v2/categories/wardByDistrictAndId?districtId=" . $districtId . "&wardsId=" . $wardsId . "",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        curl_close($curl);
        if (isset($response['error']) && $response['error'] === false) {
            return $response['data']['WARDS_NAME'];
        }
        return null;
    }

    /**
     * @inheritdoc
     * @return mixed|void
     */
    public function isConnected()
    {
        $this->registry->load->model('setting/setting');
        return is_null($this->registry->config->get('config_VIETTEL_POST_active'));
    }

    /**
     * @inheritdoc
     * @param array $data
     */
    public function parseWebhookResponse(array $data) {
        return [
            'order_status_id' => $this->getOrderStatus(self::TRANSPORT_STATUSES, $data['ORDER_STATUS']),
            'transport_order_code' => $data['ORDER_NUMBER'],
            'transport_status' => $data['ORDER_STATUS']
        ];
    }
}