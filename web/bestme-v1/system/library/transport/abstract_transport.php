<?php

namespace Transport;

/**
 * Transport class
 */
abstract class Abstract_Transport
{
    /** @var string */
    protected $name;

    /** @var mixed */
    protected $credential;

    /** @var mixed|null */
    protected $registry;

    /**
     * Transport constructor.
     * @param string $name
     * @param mixed|array $credential
     * @param null|mixed $registry
     */
    public function __construct($name, $credential = [], $registry = null)
    {
        $this->name = $name;
        $this->registry = $registry;
        $this->credential = $credential;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public abstract function getDisplayName();

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCredential()
    {
        return $this->credential;
    }

    /**
     * @param mixed $credential
     */
    public function setCredential($credential)
    {
        $this->credential = $credential;
    }

    /**
     * login
     *
     * @param array $data
     * @return mixed
     */
    public abstract function login(array $data);

    /**
     * @param array $data
     * @param bool $type_html
     * @return mixed
     */
    public abstract function getConfiguredPickingWarehouse(array $data, $type_html = false);

    /**
     * @param array $data
     * @return bool
     */
    public abstract function setMapConfiguredPickingWarehouse($data = []);

    /**
     * @param array $data
     * @return mixed
     */
    public abstract function getMapConfiguredPickingWarehouse($data = []);

    /**
     * @param array $data
     * @return mixed
     */
    public abstract function getListService(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public abstract function getPrice(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public abstract function createOrderDelivery(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public abstract function getOrderInfo(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public abstract function cancelOrder(array $data);

    /**
     * @param mixed $order_status_code
     * @return bool
     */
    public abstract function isCancelableOrder($order_status_code);

    /**
     * @param string $status_code
     * @return mixed|string
     */
    public abstract function getTransportStatusMessage($status_code);

    /**
     * get Province Code By Bestme Province Code
     *
     * e.g Bestme Province code = 89 => "Ha Noi" Province => 3rd Province code = 24 ~ "Ha Noi"
     *
     * @param string|int $bestmeProvinceCode
     * @return string|int|null
     */
    public abstract function getProvinceCodeByBestmeProvinceCode($bestmeProvinceCode);

    /**
     * get District Code By Bestme District Code
     *
     * e.g
     * - Bestme District code = 89012, Province code = 89
     * => "Thanh Xuan" Province (find in 3rd Province code = 24 ~ "Ha Noi")
     * => 3rd District code = 1253 ~ "Thanh Xuan"
     *
     * @param string|int $bestmeDistrictCode
     * @param string|int $provinceCode MUST, for finding in small district set due to provinceCode => avoid wrong district name
     * @return string|int|null
     */
    public abstract function getDistrictCodeByBestmeDistrictCode($bestmeDistrictCode, $provinceCode);

    /**
     * get Ward Code By Bestme Ward Code
     *
     * e.g
     * - Bestme Ward code = 890125, District code = 1253
     * - => "Khuong Trung" Province (find in 3rd District code = 1253 ~ "Thanh Xuan")
     * - => 3rd Ward code = 2635 ~ "Khuong Trung"
     *
     * @param $bestmeWardCode
     * @param string|int $districtCode MUST, for finding in small ward set due to districtCode => avoid wrong ward name
     * @param mixed $token
     * @return mixed
     */
    public abstract function getWardCodeByBestmeWardCode($bestmeWardCode, $districtCode, $token);

    /**
     * check if is app connected
     *
     * @return mixed
     */
    public abstract function isConnected();

    /**
     * parse webhook response data
     * @param array $data
     */
    public abstract function parseWebhookResponse(array $data);
}