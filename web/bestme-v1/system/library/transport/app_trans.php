<?php


namespace Transport;

final class App_Trans extends Abstract_Transport
{
    /**
     * @var \ControllerExtensionAppstoreTransAbstract
     */
    private $adapter_controller;

    /**
     * @inheritDoc
     */
    public function __construct($app_code, $credential, $registry = null)
    {
        parent::__construct($app_code, $credential, $registry);

        /* create app transport instance */
        $transport_file_abstract = sprintf('%scontroller/extension/appstore/trans_abstract.php', DIR_APPLICATION);
        $transport_file = sprintf('%scontroller/extension/appstore/%s.php', DIR_APPLICATION, $app_code);
        if (!file_exists($transport_file_abstract) || !file_exists($transport_file)) {
            throw new \Exception('Could not load trans_abstract.php');
        }

        require_once($transport_file_abstract);
        require_once($transport_file);

        $trans_class = sprintf('ControllerExtensionAppstore%s', str_replace('_', '', $app_code));
        if (!class_exists($trans_class)) {
            throw new \Exception("Could not load {$trans_class}.php");
        }

        $this->adapter_controller = new $trans_class($registry, $data = []);
    }

    /**
     * @inheritDoc
     */
    public function getDisplayName()
    {
        return $this->adapter_controller->getDisplayName();
    }

    /**
     * get Configured Picking Warehouse from 3rd transport
     *
     * @param string $url
     * @return string
     */

    function get_data($url)
    {
    }

    public function login(array $data)
    {
        return $this->adapter_controller->login($data);
    }

    /**
     * @inheritdoc
     */
    public function getConfiguredPickingWarehouse(array $data, $type_html = false)
    {
        return $this->adapter_controller->getConfiguredPickingWarehouse($data);
    }

    /**
     * @inheritdoc
     */
    public function setMapConfiguredPickingWarehouse($data = [])
    {
        return $this->adapter_controller->setMapConfiguredPickingWarehouse($data);
    }

    /**
     * @inheritdoc
     */
    public function getMapConfiguredPickingWarehouse($data = [])
    {
        return $this->adapter_controller->getMapConfiguredPickingWarehouse($data);
    }

    /**
     * @inheritDoc
     */
    public function getListService(array $data, $delivery_name = '')
    {
        return $this->adapter_controller->getListService($data);
    }

    public function getPrice(array $data)
    {
        return $this->adapter_controller->getPrice($data);
    }

    /**
     * @inheritdoc
     */
    public function getTransportFee(array $data)
    {
        // TODO: Implement getTransportFee() method.
    }

    /**
     * @inheritdoc
     */
    public function getOrderInfo(array $data)
    {
         return $this->adapter_controller->getOrderInfo($data);
    }

    /**
     * @inheritdoc
     */
    public function createOrderDelivery(array $data)
    {
        return $this->adapter_controller->createOrderDelivery($data);
    }

    /**
     * @inheritdoc
     */
    public function cancelOrder(array $data)
    {
        return $this->adapter_controller->cancelOrder($data);
    }

    /**
     * @inheritdoc
     */
    public function isCancelableOrder($order_status_code)
    {
        return $this->adapter_controller->isCancelableOrder($order_status_code);
    }

    /**
     * @inheritdoc
     */
    public function getTransportStatusMessage($status_code)
    {
        return $this->adapter_controller->getTransportStatusMessage($status_code);
    }

    /**
     * @inheritdoc
     */
    public function getProvinceCodeByBestmeProvinceCode($bestme_province_code)
    {
        return $this->adapter_controller->getProvinceCodeByBestmeProvinceCode($bestme_province_code);
    }

    /**
     * @inheritdoc
     */
    public function getDistrictCodeByBestmeDistrictCode($bestme_district_code, $province_code)
    {
        return $this->adapter_controller->getDistrictCodeByBestmeDistrictCode($bestme_district_code, $province_code);
    }

    /**
     * @inheritdoc
     */
    public function getWardCodeByBestmeWardCode($bestmeWardCode, $districtCode, $token)
    {
        return $this->adapter_controller->getWardCodeByBestmeWardCode($bestmeWardCode, $districtCode);
    }

    public function checkStatusDelivery() {
        return $this->adapter_controller->checkStatusDelivery();
    }

    public function isConnected()
    {
        return $this->adapter_controller->isConnected();
    }

    /**
     * @inheritdoc
     * @param array $data
     */
    public function parseWebhookResponse(array $data) {
        return $this->adapter_controller->parseWebhookResponse($data);
    }
}