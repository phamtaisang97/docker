<?php

/**
 * Device_Util class
 */
trait Device_Util
{
    public function isMobile() {
        if (!isset($_SERVER["HTTP_USER_AGENT"])) {
            return false;
        }

        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]) ? true : false;
    }

    public function isIosDevice(){
        if (!isset($_SERVER["HTTP_USER_AGENT"])) {
            return false;
        }
        $userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
        $iosDevice = array('iphone', 'ipod', 'ipad');
        $isIos = false;

        foreach ($iosDevice as $val) {
            if(stripos($userAgent, $val) !== false){
                $isIos = true;
                break;
            }
        }

        return $isIos;
    }
}