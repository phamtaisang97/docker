<?php
namespace Cache;
class Redis {
    private $expire;
    private $cache;

    /**
     * Redis constructor.
     * @param $expire
     */
    public function __construct($expire) {
        $this->expire = $expire;

        $this->cache = new \Redis();
        $this->cache->connect(MUL_REDIS_HOST_CACHE, MUL_REDIS_PORT, $this->expire);
        $this->cache->select(REDIS_DB_CACHE_QUERY);
    }

    /**
     * get key value
     * @param $key
     * @return mixed
     */
    public function get($key) {
        $value = $this->cache->get($key);
        return json_decode($value, true);
    }

    /**
     * set key using value
     * @param $key
     * @param $value
     * @return bool
     */
    public function set($key, $value) {
        $value = is_array($value) ? json_encode($value) : $value;
        return $this->cache->set($key, $value, ['EX' => 60 * 30]); // expired in 30 minutes
    }

    /**
     * delete multiple keys using key prefix
     * @param $key
     */
    public function delete($key) {
        return $this->cache->del($this->cache->keys($key . ':*'));
    }
}