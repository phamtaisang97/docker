<?php

trait AUTO_CREATE_RECEIPT_VOUCHER_UTIL_TRAIT
{

    public function autoUpdateFromOrder($order_id, $data)
    {
        try {
            (new AUTO_CREATE_RECEIPT_VOUCHER_UTIL_CLASS($this->db, DB_PREFIX))->autoUpdateFromOrder($order_id, $data);
            return;
        } catch (Exception $ex) {
            return;
        }
    }

    public function autoRemoveFromOrder($order_id)
    {
        try {
            (new AUTO_CREATE_RECEIPT_VOUCHER_UTIL_CLASS($this->db, DB_PREFIX))->removeReceiptFromOrder($order_id);
            return;
        } catch (Exception $ex) {
            return;
        }
    }
}