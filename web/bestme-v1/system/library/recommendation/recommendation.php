<?php
/**
 * @package     OpenCart
 * @author      novaon-dunght
 */

namespace Recommendation;


use Queue\Easyrec_Wrapper;
use Queue\Queue_Receiver_Interface;
use Queue\Queue_Sender_Interface;

class Recommendation
{
    const ACTION_VIEW = 'view';
    const ACTION_BUY = 'buy';
    const ACTION_RATE = 'rate';

    const ACTION_RECOMMENDATION = 'recommendationsForUser';

    static $SUPPORTED_ACTIONS = [
        self::ACTION_VIEW,
        self::ACTION_BUY,
        self::ACTION_RATE
    ];

    /**
     * @var Queue_Sender_Interface
     */
    protected $queueSender;

    /**
     * @var Queue_Receiver_Interface
     */
    protected $queueReceiver;

    /**
     * @var Easyrec_Wrapper
     */
    protected $easyRecWrapper;

    /**
     * @var callable
     */
    protected $callback;

    public function __construct(
        Queue_Sender_Interface $queueSender,
        Queue_Receiver_Interface $queueReceiver,
        Easyrec_Wrapper $easyrecWrapper
    )
    {
        $this->queueSender = $queueSender;
        $this->queueReceiver = $queueReceiver;
        $this->easyRecWrapper = $easyrecWrapper;

        $this->callback = function ($data) {
            $this->onReceivedMessage($data);
        };
    }

    /* === business tracking === */

    /**
     * track View
     * @param string $sessionId
     * @param string $itemId
     * @param string $itemDescription
     * @param string $itemUrl
     * @param string|null $itemImageUrl
     * @param string|null $itemType
     * @param string|null $userId
     * @param string|null $actionTime
     * @param string|null $actionInfo
     */
    public function trackView(
        $sessionId,
        $itemId,
        $itemDescription,
        $itemUrl,
        $itemImageUrl = null,
        $itemType = null,
        $userId = null,
        $actionTime = null,
        $actionInfo = null
    )
    {
        $data = [
            'action' => self::ACTION_VIEW,
            'parameters' => [
                $sessionId,
                $itemId,
                $itemDescription,
                $itemUrl,
                $itemImageUrl,
                $itemType,
                $userId,
                $actionTime,
                $actionInfo
            ]
        ];

        $this->enqueue(json_encode($data));
    }

    /**
     * track Buy
     * @param string $sessionId
     * @param string $itemId
     * @param string $itemDescription
     * @param string $itemUrl
     * @param string|null $itemImageUrl
     * @param string|null $itemType
     * @param string|null $userId
     * @param string|null $actionTime
     * @param string|null $actionInfo
     */
    public function trackBuy(
        $sessionId,
        $itemId,
        $itemDescription,
        $itemUrl,
        $itemImageUrl = null,
        $itemType = null,
        $userId = null,
        $actionTime = null,
        $actionInfo = null
    )
    {
        $data = [
            'action' => self::ACTION_BUY,
            'parameters' => [
                $sessionId,
                $itemId,
                $itemDescription,
                $itemUrl,
                $itemImageUrl,
                $itemType,
                $userId,
                $actionTime,
                $actionInfo
            ]
        ];

        $this->enqueue(json_encode($data));
    }

    /**
     * track Rate
     * @param string $sessionId
     * @param string $itemId
     * @param string $itemDescription
     * @param string $itemUrl
     * @param int $ratingValue
     * @param string|null $itemImageUrl
     * @param string|null $itemType
     * @param string|null $userId
     * @param string|null $actionTime
     * @param string|null $actionInfo
     */
    public function trackRate(
        $sessionId,
        $itemId,
        $itemDescription,
        $itemUrl,
        $ratingValue,
        $itemImageUrl = null,
        $itemType = null,
        $userId = null,
        $actionTime = null,
        $actionInfo = null
    )
    {
        $data = [
            'action' => self::ACTION_RATE,
            'parameters' => [
                $sessionId,
                $itemId,
                $itemDescription,
                $itemUrl,
                $ratingValue,
                $itemImageUrl,
                $itemType,
                $userId,
                $actionTime,
                $actionInfo
            ]
        ];

        $this->enqueue(json_encode($data));
    }

    /* === get recommendation === */

    /**
     * get Recommend Data
     * @param string $sessionId
     * @param string $actionType
     * @param string $userId
     * @param string|null $requestedItemType
     * @param string|null $assocType
     * @param string|null $withProfile
     * @return array
     */
    public function getRecommendData($sessionId, $actionType, $userId, $requestedItemType = null, $assocType = null, $withProfile = null)
    {
        $parameters = [
            $sessionId,
            $actionType,
            $userId,
            $requestedItemType,
            $assocType,
            $withProfile
        ];

        return $this->easyRecWrapper->doRecommendations(self::ACTION_RECOMMENDATION, $parameters);
    }

    /* === receiving thread === */

    /**
     * start dequeue thread
     */
    public function startDequeueing()
    {
        $this->queueReceiver->dequeue($this->callback);
    }

    /* === private functions === */
    /**
     * enqueue
     * @param $message
     */
    private function enqueue($message)
    {
        $this->queueSender->enqueue($message);
    }

    /**
     * handle on received message
     * @param $msg
     */
    private function onReceivedMessage($msg)
    {
        $data = $msg->body;
        $data = json_decode($data, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            return;
        }

        // send to easyrec
        $action = $data['action'];
        $parameters = $data['parameters'];
        $this->_sendTracking($action, $parameters);
    }

    /**
     * send Tracking
     * @param string $action
     * @param array $parameters
     * @return bool
     */
    private function _sendTracking($action, array $parameters = [])
    {
        if (!in_array($action, self::$SUPPORTED_ACTIONS)) {
            return false;
        }

        $response = $this->easyRecWrapper->doActions($action, $parameters);

        return ($response['code'] == Easyrec_Wrapper::CODE_SUCCESS);
    }
}