<?php

/**
 * Sync_Bestme_Data_To_Welcome_Util class
 */
trait Sync_Bestme_Data_To_Welcome_Util
{
    use Redis_Util;

    public static $SOURCE_ONBOARDING = 'onboarding';
    public static $SOURCE_SYNC_SHOP_ORDER = 'sync_shop_order';

    /**
     * publish Sync Data
     *
     * @param string $source
     * @param array $data
     * @throws Exception
     */
    public function publishSyncData($source, array $data)
    {
        // support sync multiple data
        $publish_data = [
            [
                'source' => $source,
                'data' => $data
            ]
        ];

        $data_json = json_encode($publish_data);

        $redis = null;
        try {
            $redis = $this->redisConnect();

            $redis->publish(MUL_REDIS_SYNC_BESTME_WEBSITE_DATA_PUBLISH_CHANEL, $data_json);
        } catch (Exception $e) {
            $this->redisClose($redis);

            throw $e;
        }

        $this->redisClose($redis);
    }

    /**
     * publish Sync Data Onboarding
     *
     * @param array $data
     * @throws Exception
     */
    public function publishSyncDataOnboarding(array $data)
    {
        $this->publishSyncData(self::$SOURCE_ONBOARDING, $data);
    }

    /**
     * publish sync shop order
     *
     * @param array $data
     * @throws Exception
     */
    public function publishSyncShopOrder(array $data)
    {
        $this->publishSyncData(self::$SOURCE_SYNC_SHOP_ORDER, $data);
    }
}