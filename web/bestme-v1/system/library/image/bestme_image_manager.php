<?php


namespace Image;

Class Bestme_Image_Manager extends Abstract_Image_Manager
{
    private $api_key;

    /**
     * Bestme_image_manager constructor.
     * @param $name
     * @param $credential
     */
    public function __construct($name, $credential)
    {
        parent::__construct($name, $credential);
        $this->api_key = BESTME_IMAGE_SEVER_API_KEY;
    }

    /**
     * @inheritdoc
     */
    public function upload(array $data)
    {
        if (!array_key_exists('file', $data) || !array_key_exists('shop_name', $data)) {
            return [];
        }
        $file = $data['file'];

        // data fields for POST request
        $fields = ["shop" => $data['shop_name'], "api_key" => $this->api_key];

        $files = [];
        $files['image'] = [
            'name' => $file['name'],
            'content' => file_get_contents($file['tmp_name'])
        ];

        // URL to upload to
        $url = BESTME_IMAGE_SEVER_UPLOAD_URL . '/images/upload';

        // curl
        $curl = curl_init();

        $boundary = uniqid();
        $delimiter = '-------------' . $boundary;

        $post_data = $this->build_data_files($boundary, $fields, $files);

        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $post_data,
            CURLOPT_HTTPHEADER => [
                "Content-Type: multipart/form-data; boundary=" . $delimiter,
                "Content-Length: " . strlen($post_data)

            ],
        ]);

        $response = curl_exec($curl);
        curl_close($curl);

        /* handle response */
        $result = json_decode($response, true);
        $new_image = [];
        if (!is_array($result)) {
            $new_image['error'] = true;
            $new_image['message'] = 'Invalid response';

            return $new_image;
        }

        if (!array_key_exists('code', $result)) {
            $new_image['error'] = true;
            $new_image['message'] = 'Invalid response (missing response code)';

            return $new_image;
        }

        // on error
        if ($result['code'] != 1) {
            $detail_error = isset($result['data']) ? $result['data'] : 'UNKNOWN';
            if (is_array($detail_error)) {
                $detail_error = json_encode($detail_error);
            }

            $new_image['error'] = true;
            $new_image['message'] = sprintf('Upload image failed (Code: %s, Message: %s). Detail: %s',
                $result['code'],
                isset($result['message']) ? $result['message'] : 'UNKNOWN',
                $detail_error
            );

            return $new_image;
        }

        // on success
        if (!array_key_exists('data', $result)) {
            $new_image['error'] = true;
            $new_image['message'] = 'Invalid response (missing response data)';

            return $new_image;
        }

        // return uploaded image info
        $new_image['source_id'] = '';
        $new_image['url'] = $result['data']['url'];;
        $new_image['name'] = $result['data']['filename'];
        $new_image['source'] = 'bestme';
        $new_image['error'] = false;
        $new_image['message'] = 'Success';

        return $new_image;
    }

    /**
     * @param array $data
     * @return array
     */
    public function upload_by_url(array $data)
    {
        if (!array_key_exists('image_url', $data) || !array_key_exists('shop_name', $data)) {
            return [];
        }
        // data fields for POST request
        $fields = [
            "shop" => $data['shop_name'],
            "api_key" => $this->api_key,
            "image_url" => $data['image_url']
        ];

        $files = [];

        // URL to upload to
        $url = BESTME_IMAGE_SEVER_UPLOAD_URL . '/images/upload';

        // curl
        $curl = curl_init();

        $boundary = uniqid();
        $delimiter = '-------------' . $boundary;

        $post_data = $this->build_data_files($boundary, $fields, $files);

        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $post_data,
            CURLOPT_HTTPHEADER => [
                "Content-Type: multipart/form-data; boundary=" . $delimiter,
                "Content-Length: " . strlen($post_data)

            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);
        $result = json_decode($response, true);
        $new_image = [];
        if (is_array($result) && array_key_exists('code', $result) && $result['code'] == 1) {
            if (array_key_exists('data', $result)) {
                $new_image['source_id'] = '';
                $new_image['url'] = $result['data']['url'];
                $new_image['name'] = $result['data']['filename'];
                $new_image['size'] = $result['data']['image_size'];
                $new_image['source'] = 'bestme';
            }
        }

        return $new_image;
    }

    /**
     * @inheritdoc
     */
    public function resizeImage($url, $width, $height)
    {
        $last_slash_pos = strrpos($url, '/');
        if ($last_slash_pos !== false) {
            $url = substr($url, 0, $last_slash_pos + 1) . "w_$width,h_$height/" . substr($url, $last_slash_pos + 1);
        }

        /* other way */
        /*$arr_link = explode('/', $url);
        if (is_array($arr_link) && count($arr_link) > 0) {
            $last = $arr_link[count($arr_link) - 1];
            $arr_link[count($arr_link) - 1] = "w_$width,h_$height";
            $arr_link[] = $last;
            $url = implode('/', $arr_link);
        }*/

        return $url;
    }

    private function build_data_files($boundary, $fields, $files)
    {
        $data = '';
        $eol = "\r\n";

        $delimiter = '-------------' . $boundary;

        foreach ($fields as $name => $content) {
            $data .= "--" . $delimiter . $eol
                . 'Content-Disposition: form-data; name="' . $name . "\"" . $eol . $eol
                . $content . $eol;
        }

        foreach ($files as $name => $content) {
            $data .= "--" . $delimiter . $eol
                . 'Content-Disposition: form-data; name="' . $name . '"; filename="' . $content['name'] . '"' . $eol
                //. 'Content-Type: image/png'.$eol
                . 'Content-Transfer-Encoding: binary' . $eol;

            $data .= $eol;
            $data .= $content['content'] . $eol;
        }
        $data .= "--" . $delimiter . "--" . $eol;

        return $data;
    }

    /**
     * @inheritdoc
     */
    public function deleteImages(array $data)
    {
        foreach ($data as $item) {
            if (!array_key_exists('url', $item) || !$item['url']) {
                continue;
            }

            $this->deleteImage(trim($item['url']));
        }

        return [];
    }

    public function deleteImage($url)
    {
        if (!$url) {
            return [];
        }

        // data fields for POST request
        $fields = array("image_url" => $url, "api_key" => $this->api_key);

        $files = array();

        // URL to upload to
        $url = BESTME_IMAGE_SEVER_UPLOAD_URL . '/images/delete';

        // curl
        $curl = curl_init();

        $boundary = uniqid();
        $delimiter = '-------------' . $boundary;

        $post_data = $this->build_data_files($boundary, $fields, $files);

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $post_data,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: multipart/form-data; boundary=" . $delimiter,
                "Content-Length: " . strlen($post_data)

            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $result = json_decode($response, true);
        $new_image = [];
        if (is_array($result) && array_key_exists('code', $result) && $result['code'] == 1) {
            if (array_key_exists('data', $result)) {
                $new_image['source_id'] = '';
                $new_image['url'] = $result['data']['url'];;
                $new_image['name'] = $result['data']['filename'];
                $new_image['source'] = 'bestme';
            }
        }

        return $new_image;
    }

    public function getImagesSize($data) {
        if (!array_key_exists('shop', $data) || !array_key_exists('images', $data)) {
            return [];
        }
        // data fields for POST request
        $data['api_key'] = $this->api_key;

        // URL to upload to
        $url = BESTME_IMAGE_SEVER_UPLOAD_URL . '/images/get-sizes';

        $boundary = uniqid();
        $delimiter = '-------------' . $boundary;

        $post_data = $this->build_data_files($boundary, $data, []);
        // curl
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $post_data,
            CURLOPT_HTTPHEADER => [
                "Content-Type: multipart/form-data; boundary=" . $delimiter,
                "Content-Length: " . strlen($post_data)
            ]
        ]);

        $response = curl_exec($curl);
        curl_close($curl);

        $result = json_decode($response, true);
        if (is_array($result) && array_key_exists('code', $result) && $result['code'] == 1 && array_key_exists('images_size', $result)) {
            return $result['images_size'];
        }

        return [];
    }
}