<?php


namespace Image;

use DateTime;
use Exception;

class Cloudinary_Manager extends Abstract_Image_Manager
{
    private $cloud_name;
    private $api_key;
    private $api_secret;

    /**
     * Cloudinary_Manager constructor.
     * @param $name
     * @param $credential
     */
    public function __construct($name, $credential)
    {
        parent::__construct($name, $credential);
        $this->cloud_name = CLOUD_NAME;
        $this->api_key = API_KEY;
        $this->api_secret = API_SECRET;

        \Cloudinary::config(array(
            "cloud_name" => CLOUD_NAME,
            "api_key" => API_KEY,
            "api_secret" => API_SECRET
        ));
    }

    /**
     * @inheritdoc
     */
    public function upload(array $data)
    {
        if (!array_key_exists('image', $data) || !array_key_exists('filename', $data) || !array_key_exists('directory', $data)){
            return [];
        }
        $image = $data['image'];
        $filename = $data['filename'];
        $directory = $data['directory'];

        $option = array(
            "folder" => $directory,
            "public_id" => $filename,
            "use_filename" => FALSE,
            "unique_filename" => TRUE,
            "overwrite" => false,
            "tags" => $directory,
            "context" => $filename
        );
        $result = \Cloudinary\Uploader::upload($image, $option);

        if (array_key_exists('existing', $result) && $result['existing']){
            $date = new DateTime();
            $time_stamp =  $date->getTimestamp();
            $filename = $filename . '(' . $time_stamp . ')';
            $option = array(
                "folder" => $directory,
                "public_id" => $filename,
                "use_filename" => FALSE,
                "unique_filename" => TRUE,
                "overwrite" => false,
                "tags" => $directory,
                "context" => $filename
            );
            $result = \Cloudinary\Uploader::upload($image, $option);
        }

        $new_image = [];
        if (array_key_exists('public_id', $result)) {
            $new_image = [];
            $new_image['source_id'] = $result['public_id'];
            $new_image['url'] = $result['secure_url'];
            $imageName = explode('/', $new_image['source_id']);
            $new_image['name'] = is_array($imageName) ? end($imageName) : '';
            $new_image['source'] = 'cloudinary';
        }

        return $new_image;
    }

    /**
     * @inheritdoc
     */
    public function resizeImage($url, $width, $height)
    {
        return $url;

        if (strpos($url, 'http://res.cloudinary.com') === 0 || strpos($url, 'https://res.cloudinary.com') === 0) {
            $arrLink = explode('/', $url);
            /*
             * old with "c_fill_pad,g_auto,b_auto" made image gets truncated, e.g real wide-image 2400x800px
             * $scale = 'c_fill_pad,g_auto,b_auto,w_' . $width . ',h_' . $height;
             *
             * new without above: working perfectly. Testing...
             */
            $scale = 'c_pad,w_' . $width . ',h_' . $height;
            array_splice($arrLink, (int)array_search('upload', $arrLink) + 1, 0, array($scale));
            $url = implode('/', $arrLink);
        }

        return $url;
    }

    /**
     * @param $directory
     * @param int $limit
     * @param string $next_cursor
     * @return mixed
     */
    public function getImages($directory, $next_cursor = null, $limit = 32)
    {
        $api = new \Cloudinary\Api();
        if ($next_cursor != null) {
            $options = array("type" => "upload", "resource_type" => "image", "prefix" => $directory.'/', "max_results" => $limit, "next_cursor" => $next_cursor);
        } else {
            $options = array("type" => "upload", "resource_type" => "image", "prefix" => $directory . '/', "max_results" => $limit - 1);
        }
        try {
            $images = $api->resources($options);
            //$images = $api->resources_by_tag($directory, $options);
        } catch (Exception $e) {
            return array('images' => null);
        }  // suggest: use tag to get images by current folder
        $result = array(
            'images' => $images['resources']
        );
        if (!empty($images) && array_key_exists("next_cursor", $images)) {
            $result['next_cursor'] = $images["next_cursor"];
        }

        return $result;
    }

    public function searchImages($expression, $directory, $next_cursor = null, $limit = 32)
    {
        try{
            $searchApi = new \Cloudinary\Search();
            if ($expression != '') {
                $expression = $expression . ' AND resource_type:image AND folder = ' . $directory . '/*';
            } else {
                $expression = 'resource_type:image AND folder = ' . $directory . '/*';
            }

            $images = $searchApi->expression($expression)->with_field('context')->sort_by('uploaded_at', 'desc')->max_results($limit)->next_cursor($next_cursor)->execute();

            $result = array(
                'images' => $images['resources']
            );
            if (!empty($images) && array_key_exists("next_cursor", $images)) {
                $result['next_cursor'] = $images["next_cursor"];
            }

            return $result;
        }catch (Exception $e){

            return array('images' => null);
        }
    }

    /**
     * @inheritdoc
     */
    public function deleteImages(array $data)
    {
        $public_ids = [];
        foreach ($data as $item) {
            if (!is_array($item)) {
                continue;
            }
            if (!array_key_exists('source_id', $item)) {
                continue;
            }
            $public_ids[] = $item['source_id'];
        }

        $api = new \Cloudinary\Api();  /// add delete thumbnail
        try {
            $result = $api->delete_resources($public_ids);
        } catch (Exception $e) {
            return [];
        }   // delete single images : \Cloudinary\Uploader::destroy('public_id');

        return $result;
    }

    /**
     * @param $directory
     * @return mixed
     */
    public function createEmptyFolder($directory)
    {
        $option = array(
            "folder" => $directory,
            'public_id' => 'profile',
            "use_filename" => FALSE,
            "unique_filename" => TRUE,
            "tags" => $directory
        );

        $image = DIR_IMAGE . '../profile.png';
        $result = \Cloudinary\Uploader::upload($image, $option);

        return $result;
    }

    /**
     * @param $directory
     * @return \Cloudinary\Api\Response
     * @throws \Cloudinary\Api\GeneralError
     */
    public function deleteImagesByFolder($directory)
    {
        $api = new \Cloudinary\Api();
        $result = $api->delete_resources_by_tag($directory); // set tag = directory

        return $result;
    }

    /**
     * @param $public_id
     * @param $version
     * @return string
     */
    public function verifySignatures($public_id, $version)
    {
        return \Cloudinary::api_sign_request(array("public_id" => $public_id, "version" => $version), $this->api_secret);
    }
}