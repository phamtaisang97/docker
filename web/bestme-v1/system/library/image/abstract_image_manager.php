<?php


namespace Image;

/**
 * Image_Manager class
 */
abstract class Abstract_Image_Manager
{
    /** @var string */
    protected $name;

    /** @var mixed */
    protected $credential;

    /**
     * Transport constructor.
     * @param string $name
     * @param mixed $credential
     */
    public function __construct($name, $credential)
    {
        $this->name = $name;
        $this->credential = $credential;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCredential()
    {
        return $this->credential;
    }

    /**
     * @param mixed $credential
     */
    public function setCredential($credential)
    {
        $this->credential = $credential;
    }

    /**
     * upload image to 3rd server
     *
     * @param array $data due to specific transport
     * @return mixed
     */
    public abstract function upload(array $data);

    /**
     * delete image from 3rd server
     *
     * @param array $data due to specific transport
     * @return mixed
     */
    public abstract function deleteImages(array $data);

    /**
     * @param string $url
     * @param $width
     * @param $height
     * @return string
     */
    public abstract function resizeImage($url, $width, $height);
}