<?php

/**
 * Device_Util class
 */
trait Order_Util_Trait
{
    /**
     * @param string $transport_id
     * @param string $city
     * @param string $total_weight
     * @param string $total_order
     * @return array format as
     * [
     *     'fee_order' => [
     *         'name' => 'HA NOI va cac tinh lan can', // $name_transport,
     *         'fee_weight' => 34000, // $fee_price + $value_fee_cod,\
     *         'fee_cod' => 15000, // fee cod
     *         'fee' => 49000, // total fee, = fee_weight + $value_fee_cod
     *         'error' => ''
     *     ]
     * ]
     */
    public function getTransportFee($transport_id = '', $city = '', $total_weight = '', $total_order = '')
    {
        /* get satisfied delivery methods */
        $satisfiedDeliveryMethods = $this->getSatisfiedDeliveryMethods($city, $total_weight, $total_order);
        if (count($satisfiedDeliveryMethods) < 1) {
            /* use default delivery method */
            $this->load->model('setting/setting');
            $delivery_methods_config = $this->model_setting_setting->getSettingValue('delivery_methods');
            $delivery_methods_config = json_decode($delivery_methods_config, true);
            $delivery_methods_config = is_array($delivery_methods_config) ? $delivery_methods_config : [];

            $defaultDeliveryMethod = isset($delivery_methods_config[0]) ? $delivery_methods_config[0] : [];

            return [
                'fee_order' => [
                    'name' => isset($defaultDeliveryMethod['name']) ? $defaultDeliveryMethod['name'] : '',
                    'fee_weight' => isset($defaultDeliveryMethod['fee_region']) ? $defaultDeliveryMethod['fee_region'] : 0,
                    'fee_cod' => 0,
                    'fee' => isset($defaultDeliveryMethod['fee_region']) ? $defaultDeliveryMethod['fee_region'] : 0,
                    'error' => ''
                ]
            ];
        }

        /* get first sastified delivery method */
        $foundDeliveryMethod = false;
        foreach ($satisfiedDeliveryMethods as $satisfiedDeliveryMethod) {
            if (isset($satisfiedDeliveryMethod['id']) && $transport_id == $satisfiedDeliveryMethod['id']) {
                $foundDeliveryMethod = $satisfiedDeliveryMethod;
                break;
            }
        }

        /* build result */
        return [
            'fee_order' => [
                'name' => isset($foundDeliveryMethod['name']) ? $foundDeliveryMethod['name'] : '',
                'fee_weight' => isset($foundDeliveryMethod['fee_weight']) ? $foundDeliveryMethod['fee_weight'] : 0,
                'fee_cod' => isset($foundDeliveryMethod['fee_cod']) ? $foundDeliveryMethod['fee_cod'] : 0,
                'fee' => isset($foundDeliveryMethod['fee']) ? $foundDeliveryMethod['fee'] : 0,
                'error' => isset($foundDeliveryMethod['error']) ? $foundDeliveryMethod['error'] : ''
            ]
        ];
    }

    /**
     * get Satisfied Delivery Methods due to city, weight
     *
     * @param string $city
     * @param string $total_weight
     * @param string $total_order
     * @return array format as
     * [
     *     [
     *         'id' => '1245643122', // transport id,
     *         'name' => 'HA NOI va cac tinh lan can', // $name_transport,
     *         'delivery_method' => [...], // $delivery_method_config,
     *         'fee_region' => [...], // $found_region,
     *         'fee_weight' => 34000, // $fee_price + $value_fee_cod,\
     *         'fee_cod' => 15000, // fee cod
     *         'fee' => 49000, // total fee, = fee_weight + $value_fee_cod
     *         'error' => ''
     *     ],
     *     ...
     * ]
     */
    public function getSatisfiedDeliveryMethods($city = '', $total_weight = '', $total_order = '')
    {
        /* get city name by code */
        $this->load->model('localisation/vietnam_administrative');
        $cityArr = $this->model_localisation_vietnam_administrative->getProvinceByCode($city);
        $city = '';
        if (isset($cityArr['name'])) {
            $city = $cityArr['name'];
        }

        /* get delivery methods setting from db */
        $this->load->model('setting/setting');
        $delivery_methods_config = $this->model_setting_setting->getSettingValue('delivery_methods');
        $delivery_methods_config = json_decode($delivery_methods_config, true);
        $delivery_methods_config = is_array($delivery_methods_config) ? $delivery_methods_config : [];

        /* get delivery fee due to city, total_weight, total_order from each delivery method */
        $value_fee_cod = 0;
        $fee_weight = 0;
        $found_delivery_methods = [];
        foreach ($delivery_methods_config as $key => $delivery_method_config) {
            // TODO: remove...
            //$default_fee_region = isset($delivery_method_config['fee_region']) ? $delivery_method_config['fee_region'] : 0;

            if (!isset($delivery_method_config['fee_regions'])) {
                continue;
            }

            if (!isset($delivery_method_config['status']) || $delivery_method_config['status'] != '1') {
                continue;
            }

            // get delivery fee from each region
            $found_region = false;
            foreach ($delivery_method_config['fee_regions'] as $fee_region_config) {
                // find matched region due to city
                if (!in_array(trim($city), $fee_region_config['provinces'])) {
                    continue;
                }

                if (isset($fee_region_config['steps']) && $fee_region_config['steps'] == 'price') {
                    foreach ($fee_region_config['price_steps'] as $price_step_config) {
                        if (convertWeight($price_step_config['from']) <= $total_order && convertWeight($price_step_config['to']) >= $total_order) {
                            // force remove "," due to json from cb. TODO: DO NOT save value such as 30,000, save 30000 only...
                            $fee_weight = convertWeight($price_step_config['price']); // Gia thuoc tinh thanh
                            $found_region = $fee_region_config;
                            break;
                        }
                    }
                } else {
                    // find matched region due to weight
                    foreach ($fee_region_config['weight_steps'] as $weight_step_config) {
                        if (convertWeight($weight_step_config['from']) <= $total_weight && convertWeight($weight_step_config['to']) >= $total_weight) {
                            // force remove "," due to json from cb. TODO: DO NOT save value such as 30,000, save 30000 only...
                            $fee_weight = convertWeight($weight_step_config['price']); // Gia thuoc tinh thanh
                            $found_region = $fee_region_config;
                            break;
                        }
                    }
                }


                if ($found_region) {
                    break;
                }
            }

            // get delivery fee from each region: if not found, use default!
            // NOT USE due to tuning v1.7.3. TODO: remove when stable...
            /*if (!$found_region) {
                $fee_weight = str_replace(',', '', $default_fee_region);  // Gia mac dinh khi khong thuoc tinh thanh
            }*/
            // end - NOT USE due to tuning v1.7.3. TODO: remove when stable...

            // skip to next delivery_method_config
            if (!$found_region) {
                continue;
            }

            // more fee about COD (fixed or per order total revenue)
            foreach ($delivery_method_config['fee_cod'] as $key_cod => $fee_cod) {
                $arr_fee_status = ($delivery_method_config['fee_cod'][$key_cod]);
                if ($arr_fee_status['status'] == 1) {
                    // force remove "," due to json from cb. TODO: DO NOT save value such as 30,000, save 30000 only...
                    $arr_fee_status = str_replace(',', '', $arr_fee_status['value']);
                    if ($key_cod === 'dynamic') {
                        if ($total_order != 0) {
                            $value_fee_cod = ($arr_fee_status / 100) * $total_order;
                        } else {
                            $value_fee_cod = 0;
                        }
                    } elseif ($key_cod === 'fixed') {
                        $value_fee_cod = $arr_fee_status;
                    }
                }
            }

            // add to list
            $found_delivery_methods[] = [
                'id' => $delivery_method_config['id'],
                'name' => $delivery_method_config['name'],
                'delivery_method' => $delivery_method_config,
                'fee_region' => $found_region,
                'fee_weight' => $fee_weight,
                'fee_cod' => $value_fee_cod,
                'fee' => $fee_weight + $value_fee_cod, // total fee
                'error' => '',
            ];
        }

        return $found_delivery_methods;
    }
}