<?php

trait Activity_Tracking_Util_Trait
{
    /**
     * @param $total
     */
    public function trackingCountShopeeOrder($total)
    {
        try {
            $this->load->model('setting/setting');
            $shop_name = $this->config->get('shop_name');
            $packet_paid = $this->config->get('config_packet_paid');
            $domain = $_SERVER['HTTP_HOST'];
            (new Activity_Tracking_Util())->trackingCountShopeeOrder($domain, $shop_name, $packet_paid, $total);
        } catch (Exception $ex) {
            return;
        }
    }

    /**
     * @param $key
     * @param $data
     */
    public function pushDataToRedis($key, $data)
    {
        try {
            (new Activity_Tracking_Util())->pushDataToRedis($key, $data);
        } catch (Exception $ex) {
            return;
        }
    }

    /**
     * @param $code
     * @param string $data
     */
    public function saveToRedis($code, $data = '')
    {
        try {
            $this->load->model('setting/setting');
            $shop_name = $this->config->get('shop_name');
            $packet_paid = $this->config->get('config_packet_paid');
            $domain = $_SERVER['HTTP_HOST'];
            if(!$packet_paid){
                return;
            }
            (new Activity_Tracking_Util())->saveToRedis($code, $domain, $shop_name, $packet_paid, $data);
        } catch (Exception $ex) {
            return;
        }
    }
}