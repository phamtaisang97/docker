<?php

use Async_Callback\Handler\Chatbot_Novaon_V2_Handler;


/**
 * Async_Callback_Util class
 */
trait Async_Callback_Util
{
    use Redis_Util;

    /**
     * queue Sync Data
     *
     * @param string $source
     * @param array $data
     * @throws Exception
     */
    public function queueAsyncCallbackData($source, array $data)
    {
        // support sync multiple data
        $publish_data = [
            [
                'source' => $source,
                'data' => $data
            ]
        ];

        $data_json = json_encode($publish_data);

        $redis = null;
        try {
            $redis = $this->redisConnect($mul_redis_host = null, $mul_redis_port = null, $mul_redis_db = ASYNC_CALLBACK_DB);

            $redis->lPush(ASYCN_CALLBACK_JOB_REDIS_QUEUE, $data_json);
        } catch (Exception $e) {
            $this->redisClose($redis);

            throw $e;
        }

        $this->redisClose($redis);
    }

    /**
     * queue async chatbot_novaon_v2 order notify
     *
     * @param array $data
     * @throws Exception
     */
    public function queueAsyncChatbotNovaonV2OrderNotify(array $data)
    {
        $this->queueAsyncCallbackData(Chatbot_Novaon_V2_Handler::$SOURCE, $data);
    }
}