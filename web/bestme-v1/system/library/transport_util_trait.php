<?php

/**
 * Transports util trait
 */
trait Transport_Util_Trait
{
    private static $ORDER_STATUS_ID = [
        'ORDER_STATUS_ID_DRAFT' => 6,
        'ORDER_STATUS_ID_PROCESSING' => 7,
        'ORDER_STATUS_ID_DELIVERING' => 8,
        'ORDER_STATUS_ID_COMPLETED' => 9,
        'ORDER_STATUS_ID_CANCELLED' => 10,
        'ORDER_STATUS_ID_DELETED' => -1
    ];

    /**
     * @param $order_code
     * format as shop_name.order_code
     * @return string
     */
    public function getOrderCode($order_code)
    {
        $order_code = explode('.', $order_code);

        return isset($order_code[1]) ? $order_code[1] : '';
    }

    /**
     * @param $delivery_statuses
     * @param $order_delivery_status
     * @return bool
     */
    public function getOrderStatus($delivery_statuses, $order_delivery_status)
    {
        foreach ($delivery_statuses as $key => $delivery_status) {
            $result = in_array($order_delivery_status, $delivery_status);
            if ($result) {
                return self::$ORDER_STATUS_ID[$key];
            }
        }

        return null;
    }
}
