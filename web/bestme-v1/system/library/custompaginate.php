<?php
/**
 * @package        OpenCart
 * @author        Daniel Kerr
 * @copyright    Copyright (c) 2005 - 2017, OpenCart, Ltd. (https://www.opencart.com/)
 * @license        https://opensource.org/licenses/GPL-3.0
 * @link        https://www.opencart.com
 */

/**
 * Pagination class
 */
class CustomPaginate
{
    public $total = 0;
    public $page = 1;
    public $limit = 20;
    public $num_links = 3;
    public $url = '';
    //public $text_first = '<i class="fa fa-angle-double-left"></i>'; // '|&lt;'
    //public $text_last = '<i class="fa fa-angle-double-right"></i>'; //'&gt;|'
    //public $text_next = '<i class="fa fa-angle-right"></i>'; //'&gt;'
    //public $text_prev = '<i class="fa fa-angle-left"></i>'; //'&lt;'
    public $text_first = '<span class="pagination-btn-first">«</span>'; // '|&lt;'
    public $text_last = '<span class="pagination-btn-last">»</span>'; //'&gt;|'
    public $text_next = '<span class="pagination-btn-next">›</span>'; //'&gt;'
    public $text_prev = '<span class="pagination-btn-prev">‹</span>'; //'&lt;'

    /**
     *
     * @param array $options supported keys:
     * [
     *     'text_go_to_page' => 'Go to page',
     * ]
     * @return string
     */
    public function render($options = [])
    {
        $total = $this->total;

        if ($this->page < 1) {
            $page = 1;
        } else {
            $page = $this->page;
        }

        if (!(int)$this->limit) {
            $limit = 10;
        } else {
            $limit = $this->limit;
        }

        $num_links = $this->num_links;
        $num_pages = ceil($total / $limit);

        $this->url = str_replace('%7Bpage%7D', '{page}', $this->url);

        $output = '<ul class="pagination justify-content-center">';
        $output .= '<li class="page-item">';

        // support option "text_go_to_page"
        $text_go_to_page = isset($options['text_go_to_page']) ? $options['text_go_to_page'] : null;
        if (!is_null($text_go_to_page)) {
            $output .= '<li class="page-item"><label>'. $text_go_to_page . '</label><input onchange="gotoPage(this,'. $num_pages . ')" class="enter-page-pagination number-format-4" type="text" /></li>';
        }

        if ($page > 1) {
            $output .= '<li class="page-item"><a class="page-link" data-page="1" href="' . str_replace(array('&amp;page={page}', '?page={page}', '&page={page}'), '', $this->url) . '">' . $this->text_first . '</a></li>';

            if ($page - 1 === 1) {
                $output .= '<li class="page-item"><a class="page-link" data-page="1" href="' . str_replace(array('&amp;page={page}', '?page={page}', '&page={page}'), '', $this->url) . '">' . $this->text_prev . '</a></li>';
            } else {
                $output .= '<li class="page-item"><a class="page-link" data-page="' . ($page - 1) . '" href="' . str_replace('{page}', $page - 1, $this->url) . '">' . $this->text_prev . '</a></li>';
            }
        }

        if ($num_pages > 1) {
            if ($num_pages <= $num_links) {
                $start = 1;
                $end = $num_pages;
            } else {
                $start = $page - floor($num_links / 2);
                $end = $page + floor($num_links / 2);

                if ($start < 1) {
                    $end += abs($start) + 1;
                    $start = 1;
                }

                if ($end > $num_pages) {
                    $start -= ($end - $num_pages);
                    $end = $num_pages;
                }
            }

            for ($i = $start; $i <= $end; $i++) {
                if ($page == $i) {
                    $output .= '<li class="page-item active"><a class="page-link">' . $i . '</a></li>';
                } else {
                    if ($i === 1) {
                        $output .= '<li class="page-item"><a class="page-link" data-page="' . $i . '" href="' . str_replace(array('&amp;page={page}', '?page={page}', '&page={page}'), '', $this->url) . '">' . $i . '</a></li>';
                    } else {
                        $output .= '<li class="page-item"><a class="page-link" data-page="' . $i . '" href="' . str_replace('{page}', $i, $this->url) . '">' . $i . '</a></li>';
                    }
                }
            }
        }

        if ($page < $num_pages) {
            $output .= '<li class="page-item"><a class="page-link" data-page="' . ($page + 1) . '" href="' . str_replace('{page}', $page + 1, $this->url) . '">' . $this->text_next . '</a></li>';
            $output .= '<li class="page-item"><a class="page-link" data-page="' . $num_pages . '" href="' . str_replace('{page}', $num_pages, $this->url) . '">' . $this->text_last . '</a></li>';
        }

        $output .= '</li>';
        $output .= '</ul>';
        if ($num_pages > 1) {
            return $output;
        } else {
            return '';
        }
    }
}