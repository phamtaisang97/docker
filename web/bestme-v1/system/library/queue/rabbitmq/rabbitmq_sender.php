<?php
/**
 * @package     OpenCart
 * @author      novaon-dunght
 */

namespace Queue\RabbitMQ;

use PhpAmqpLib\Message\AMQPMessage;
use Queue\Queue_Sender_Interface;

class RabbitMQ_Sender extends RabbitMQ implements Queue_Sender_Interface
{
    /**
     * @inheritdoc
     */
    public function enqueue($message)
    {
        try {
            /* create a connection to the server */
            $this->openConn();

            /* publish a message to the queue */
            $msg = new AMQPMessage($message);
            $this->channel->basic_publish($msg, '', $this->queue);

            echo " [x] Sent 'Hello World!'\n";
        } catch (\Exception $e) {
            echo " [err] Got error: {$e->getMessage()}\n";
        } finally {
            /* Lastly, close the channel and the connection; */
            $this->closeConn();
        }
    }
}