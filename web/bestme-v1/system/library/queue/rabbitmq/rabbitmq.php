<?php
/**
 * @package     OpenCart
 * @author      novaon-dunght
 */

namespace Queue\RabbitMQ;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;

abstract class RabbitMQ
{
    const DEFAULT_QUEUE = 'opencart-easyrec';
    const DEFAULT_HOST = 'localhost';
    const DEFAULT_PORT = 5672;
    const DEFAULT_USERNAME = 'guest';
    const DEFAULT_PASSWORD = 'guest';

    protected $queue;
    protected $host;
    protected $port;
    protected $username;
    protected $password;

    /**
     * @var AMQPStreamConnection
     */
    protected $conn;

    /**
     * @var AMQPChannel
     */
    protected $channel;

    public function __construct(
        $queue = self::DEFAULT_QUEUE,
        $host = self::DEFAULT_HOST,
        $port = self::DEFAULT_PORT,
        $username = self::DEFAULT_USERNAME,
        $password = self::DEFAULT_PASSWORD
    )
    {
        $this->queue = $queue;
        $this->host = $host;
        $this->port = $port;
        $this->username = $username;
        $this->password = $password;

        $this->conn = null;
        $this->channel = null;
    }

    public function openConn()
    {
        /* create a connection to the server */
        $this->conn = new AMQPStreamConnection($this->host, $this->port, $this->username, $this->password);
        $this->channel = $this->conn->channel();

        /* must declare a queue to send to */
        $this->channel->queue_declare($this->queue, false, false, false, false);
    }

    public function closeConn()
    {
        /* Lastly, close the channel and the connection; */
        if ($this->channel instanceof AMQPChannel) {
            $this->channel->close();
        }

        if ($this->conn instanceof AMQPStreamConnection) {
            $this->conn->close();
        }
    }
}