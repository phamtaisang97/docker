<?php
/**
 * @package     OpenCart
 * @author      novaon-dunght
 */

namespace Queue\RabbitMQ;

use Queue\Queue_Receiver_Interface;

class RabbitMQ_Receiver extends RabbitMQ implements Queue_Receiver_Interface
{
    /**
     * @inheritdoc
     */
    public function dequeue($callback)
    {
        try {
            /* open a connection and a channel, and declare the queue */
            $this->openConn();

            /*
             * define a PHP callable that will receive the messages sent by the server
             * Keep in mind that messages are sent asynchronously from the server to the clients
             */
            echo " [*] Waiting for messages. To exit press CTRL+C\n";

            if (!is_callable($callback)) {
                $callback = function ($msg) {
                    echo '[Default] [x] Received ', $msg->body, "\n";
                };
            }

            $this->channel->basic_consume($this->queue, '', false, true, false, false, $callback);

            /* dequeue */
            while (count($this->channel->callbacks)) {
                $this->channel->wait();
            }

        } catch (\ErrorException $e) {
            echo " [err] Got error: {$e->getMessage()}\n";
        } finally {
            /* Lastly, close the channel and the connection; */
            $this->closeConn();
        }
    }
}