<?php
/**
 * @package     OpenCart
 * @author      novaon-dunght
 */

namespace Queue;


interface Queue_Receiver_Interface
{
    /**
     * @param callable $callback
     * @return mixed
     */
    public function dequeue($callback);
}