<?php
/**
 * @package     OpenCart
 * @author      novaon-dunght
 */

namespace Queue;


interface Queue_Sender_Interface
{
    /**
     * @param string $message
     * @return mixed
     */
    public function enqueue($message);
}