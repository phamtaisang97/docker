<?php
/**
 * @package     OpenCart
 * @author      novaon-dunght
 */

namespace Queue;


use Hafael\Easyrec\Easyrec;

class Easyrec_Wrapper
{
    const DEFAULT_VERSION = '1.1';
    const DEFAULT_BASE_URL = 'http://localhost:8081/easyrec-web/';
    const DEFAULT_API_KEY = '6491d97a8a0c3f7d07b1df5d83a83521';
    const DEFAULT_TENANT_ID = 'EASYREC_DEMO';
    const DEFAULT_API_VERSION = '1.1';
    const DEFAULT_TOKEN = '970217a66b67ea7f0145ab82f2a7ec92';

    const CODE_SUCCESS = 1;
    const CODE_FAILURE = 0;

    /**
     * @var Easyrec
     */
    protected $easyRecApi = null;

    public function __construct(
        $baseUrl = self::DEFAULT_BASE_URL,
        $apiKey = self::DEFAULT_API_KEY,
        $tenantId = self::DEFAULT_TENANT_ID,
        $apiVersion = self::DEFAULT_API_VERSION,
        $token = self::DEFAULT_TOKEN
    )
    {
        $this->easyRecApi = new Easyrec($baseUrl, $apiKey, $tenantId, $apiVersion, $token);
    }

    /**
     * do Actions
     * @param $action
     * @param array $parameters
     * @return array format as
     * [
     *     'code' => CODE_SUCCESS|CODE_FAILURE,
     *     'message' => 'success|failure',
     *     'response' => [...]
     * ];
     */
    public function doActions($action, array $parameters)
    {
        try {
            $api = 'Actions';
            $apiInst = $this->easyRecApi->{$api}();

            return [
                'code' => self::CODE_SUCCESS,
                'message' => 'success',
                'response' => call_user_func_array(array($apiInst, $action), $parameters)
            ];
        } catch (\Exception $e) {
            return [
                'code' => self::CODE_FAILURE,
                'message' => 'failure: ' . $e->getMessage(),
                'response' => []
            ];
        }
    }

    /* doClusters, doImports, doProfiles, doRankings be done later... */

    /**
     * do Recommendations
     * @param $action
     * @param array $parameters
     * @return array
     */
    public function doRecommendations($action, array $parameters)
    {
        try {
            $api = 'Recommendations';
            $apiInst = $this->easyRecApi->{$api}();

            $response = call_user_func_array(array($apiInst, $action), $parameters);
            $response = $this->parseRecommendedData($response['response']);

            return [
                'code' => self::CODE_SUCCESS,
                'message' => 'success',
                'response' => $response
            ];
        } catch (\Exception $e) {
            return [
                'code' => self::CODE_FAILURE,
                'message' => 'failure: ' . $e->getMessage(),
                'response' => []
            ];
        }
    }

    /**
     * @param array $response
     * @return array
     */
    private function parseRecommendedData(array $response)
    {
        // do parsing...

        return $response;
    }
}