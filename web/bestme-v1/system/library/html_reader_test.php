<?php

use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Reader\Html as BaseReaderHtml;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Font;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

/** PhpSpreadsheet root directory */
class Html_Reader_Test extends BaseReaderHtml
{
    /**
     * Spreadsheet from content.
     *
     * @param string $content
     * @param Spreadsheet|null $spreadsheet
     * @return Spreadsheet
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function loadFromString($content, Spreadsheet $spreadsheet = null)
    {
        //    Create a new DOM object
        $dom = new DOMDocument();
        //    Reload the HTML file into the DOM object
        try {
            $loaded = $dom->loadHTML(mb_convert_encoding($this->securityScanner->scan($content), 'HTML-ENTITIES', 'UTF-8'));
        } catch (Throwable $e) {
            $loaded = false;
        }
        if ($loaded === false) {
            throw new Exception('Failed to load content as a DOM Document');
        }

        return $this->loadDocument($dom, $spreadsheet ?? new Spreadsheet());
    }

    /**
     * Loads PhpSpreadsheet from DOMDocument into PhpSpreadsheet instance.
     * @param DOMDocument $document
     * @param Spreadsheet $spreadsheet
     * @return Spreadsheet
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function loadDocument(DOMDocument $document, Spreadsheet $spreadsheet)
    {
        while ($spreadsheet->getSheetCount() <= $this->sheetIndex) {
            $spreadsheet->createSheet();
        }
        $spreadsheet->setActiveSheetIndex($this->sheetIndex);

        // Discard white space
        $document->preserveWhiteSpace = false;

        $row = 0;
        $column = 'A';
        $content = '';
        $this->rowspan = [];
        $this->processDomElement_2($document, $spreadsheet->getActiveSheet(), $row, $column, $content);

        // Return
        return $spreadsheet;
    }

    /**
     * OVERRIDEN parent!
     *
     * @param DOMNode $element
     * @param Worksheet $sheet
     * @param int $row
     * @param string $column
     * @param string $cellContent
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    protected function processDomElement_2(DOMNode $element, Worksheet $sheet, &$row, &$column, &$cellContent)
    {
        foreach ($element->childNodes as $child) {
            if ($child instanceof DOMText) {
                $domText = preg_replace('/\s+/u', ' ', trim($child->nodeValue));
                if (is_string($cellContent)) {
                    //    simply append the text if the cell content is a plain text string
                    $cellContent .= $domText;
                }
                //    but if we have a rich text run instead, we need to append it correctly
                //    TODO
            } elseif ($child instanceof DOMElement) {
                $attributeArray = [];
                foreach ($child->attributes as $attribute) {
                    $attributeArray[$attribute->name] = $attribute->value;
                }

                switch ($child->nodeName) {
                    case 'meta':
                        foreach ($attributeArray as $attributeName => $attributeValue) {
                            switch ($attributeName) {
                                case 'content':
                                    //    TODO
                                    //    Extract character set, so we can convert to UTF-8 if required
                                    break;
                            }
                        }
                        $this->processDomElement_2($child, $sheet, $row, $column, $cellContent);

                        break;
                    case 'title':
                        $this->processDomElement_2($child, $sheet, $row, $column, $cellContent);
                        $sheet->setTitle($cellContent, true, false);
                        $cellContent = '';

                        break;
                    case 'span':
                    case 'div':
                    case 'font':
                    case 'i':
                    case 'em':
                    case 'strong':
                    case 'b':
                        if (isset($attributeArray['class']) && $attributeArray['class'] === 'comment') {
                            $sheet->getComment($column . $row)
                                ->getText()
                                ->createTextRun($child->textContent);

                            break;
                        }

                        if ($cellContent > '') {
                            $cellContent .= ' ';
                        }
                        $this->processDomElement_2($child, $sheet, $row, $column, $cellContent);
                        if ($cellContent > '') {
                            $cellContent .= ' ';
                        }

                        break;
                    case 'hr':
                        $this->flushCell($sheet, $column, $row, $cellContent);
                        ++$row;
                        if (isset($this->formats[$child->nodeName])) {
                            $sheet->getStyle($column . $row)->applyFromArray($this->formats[$child->nodeName]);
                        } else {
                            $cellContent = '----------';
                            $this->flushCell($sheet, $column, $row, $cellContent);
                        }
                        ++$row;
                    // Add a break after a horizontal rule, simply by allowing the code to dropthru
                    // no break
                    case 'br':
                        if ($this->tableLevel > 0) {
                            //    If we're inside a table, replace with a \n
                            $cellContent .= "\n";
                        } else {
                            //    Otherwise flush our existing content and move the row cursor on
                            $this->flushCell($sheet, $column, $row, $cellContent);
                            ++$row;
                        }

                        break;
                    case 'a':
                        foreach ($attributeArray as $attributeName => $attributeValue) {
                            switch ($attributeName) {
                                case 'href':
                                    $sheet->getCell($column . $row)->getHyperlink()->setUrl($attributeValue);
                                    if (isset($this->formats[$child->nodeName])) {
                                        $sheet->getStyle($column . $row)->applyFromArray($this->formats[$child->nodeName]);
                                    }

                                    break;
                                case 'class':
                                    if ($attributeValue === 'comment-indicator') {
                                        break; // Ignore - it's just a red square.
                                    }
                            }
                        }
                        $cellContent .= ' ';
                        $this->processDomElement_2($child, $sheet, $row, $column, $cellContent);

                        break;
                    case 'h1':
                    case 'h2':
                    case 'h3':
                    case 'h4':
                    case 'h5':
                    case 'h6':
                    case 'ol':
                    case 'ul':
                    case 'p':
                        if ($this->tableLevel > 0) {
                            //    If we're inside a table, replace with a \n
                            $cellContent .= "\n";
                            $this->processDomElement_2($child, $sheet, $row, $column, $cellContent);
                        } else {
                            if ($cellContent > '') {
                                $this->flushCell($sheet, $column, $row, $cellContent);
                                ++$row;
                            }
                            $this->processDomElement_2($child, $sheet, $row, $column, $cellContent);
                            $this->flushCell($sheet, $column, $row, $cellContent);

                            if (isset($this->formats[$child->nodeName])) {
                                $sheet->getStyle($column . $row)->applyFromArray($this->formats[$child->nodeName]);
                            }

                            ++$row;
                            $column = 'A';
                        }

                        break;
                    case 'li':
                        if ($this->tableLevel > 0) {
                            //    If we're inside a table, replace with a \n
                            $cellContent .= "\n";
                            $this->processDomElement_2($child, $sheet, $row, $column, $cellContent);
                        } else {
                            if ($cellContent > '') {
                                $this->flushCell($sheet, $column, $row, $cellContent);
                            }
                            ++$row;
                            $this->processDomElement_2($child, $sheet, $row, $column, $cellContent);
                            $this->flushCell($sheet, $column, $row, $cellContent);
                            $column = 'A';
                        }

                        break;
                    case 'table':
                        $this->flushCell($sheet, $column, $row, $cellContent);
                        $column = $this->setTableStartColumn($column);
                        if ($this->tableLevel > 1) {
                            --$row;
                        }
                        $this->processDomElement_2($child, $sheet, $row, $column, $cellContent);
                        $column = $this->releaseTableStartColumn();
                        if ($this->tableLevel > 1) {
                            ++$column;
                        } else {
                            ++$row;
                        }

                        break;
                    case 'thead':
                    case 'tbody':
                        $this->processDomElement_2($child, $sheet, $row, $column, $cellContent);

                        break;
                    case 'tr':
                        $column = $this->getTableStartColumn();
                        $cellContent = '';
                        $this->processDomElement_2($child, $sheet, $row, $column, $cellContent);
                        ++$row;

                        break;
                    case 'th':
                    case 'td':
                        $this->processDomElement_2($child, $sheet, $row, $column, $cellContent);

                        // apply inline style
                        $this->applyInlineStyle_2($sheet, $row, $column, $attributeArray);

                        while (isset($this->rowspan[$column . $row])) {
                            ++$column;
                        }

                        $this->flushCell($sheet, $column, $row, $cellContent);

                        if (isset($attributeArray['rowspan'], $attributeArray['colspan'])) {
                            //create merging rowspan and colspan
                            $columnTo = $column;
                            for ($i = 0; $i < $attributeArray['colspan'] - 1; ++$i) {
                                ++$columnTo;
                            }
                            $range = $column . $row . ':' . $columnTo . ($row + $attributeArray['rowspan'] - 1);
                            foreach (Coordinate::extractAllCellReferencesInRange($range) as $value) {
                                $this->rowspan[$value] = true;
                            }
                            $sheet->mergeCells($range);
                            $column = $columnTo;
                        } elseif (isset($attributeArray['rowspan'])) {
                            //create merging rowspan
                            $range = $column . $row . ':' . $column . ($row + $attributeArray['rowspan'] - 1);
                            foreach (Coordinate::extractAllCellReferencesInRange($range) as $value) {
                                $this->rowspan[$value] = true;
                            }
                            $sheet->mergeCells($range);
                        } elseif (isset($attributeArray['colspan'])) {
                            //create merging colspan
                            $columnTo = $column;
                            for ($i = 0; $i < $attributeArray['colspan'] - 1; ++$i) {
                                ++$columnTo;
                            }
                            $sheet->mergeCells($column . $row . ':' . $columnTo . $row);
                            $column = $columnTo;
                        } elseif (isset($attributeArray['bgcolor'])) {
                            $sheet->getStyle($column . $row)->applyFromArray(
                                [
                                    'fill' => [
                                        'fillType' => Fill::FILL_SOLID,
                                        'color' => ['rgb' => $attributeArray['bgcolor']],
                                    ],
                                ]
                            );
                        }
                        ++$column;

                        break;
                    case 'body':
                        $row = 1;
                        $column = 'A';
                        $cellContent = '';
                        $this->tableLevel = 0;
                        $this->processDomElement_2($child, $sheet, $row, $column, $cellContent);

                        break;
                    default:
                        $this->processDomElement_2($child, $sheet, $row, $column, $cellContent);
                }
            }
        }
    }

    /**
     * OVERRIDEN parent!
     *
     * Apply inline css inline style.
     *
     * NOTES :
     * Currently only intended for td & th element,
     * and only takes 'background-color' and 'color'; property with HEX color
     *
     * TODO :
     * - Implement to other propertie, such as border
     *
     * @param Worksheet $sheet
     * @param int $row
     * @param string $column
     * @param array $attributeArray
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function applyInlineStyle_2(&$sheet, $row, $column, $attributeArray)
    {
        if (!isset($attributeArray['style'])) {
            return;
        }

        $supported_styles = ['background-color', 'color', 'font-weight', 'text-align'];

        // add color styles (background & text) from dom element,currently support : td & th, using ONLY inline css style with RGB color
        $styles = explode(';', $attributeArray['style']);
        foreach ($styles as $st) {
            $value = explode(':', $st);

            if (empty(trim($value[0])) || !in_array(trim($value[0]), $supported_styles)) {
                continue;
            }

            switch (trim($value[0])) {
                case 'background-color':
                    //check if has #, so we can get clean hex
                    if (substr(trim($value[1]), 0, 1) == '#') {
                        $style_color = substr(trim($value[1]), 1);
                    }

                    if (empty($style_color)) {
                        continue;
                    }

                    $sheet->getStyle($column . $row)->applyFromArray(['fill' => ['fillType' => Fill::FILL_SOLID, 'color' => ['rgb' => "{$style_color}"]]]);

                    break;

                case 'color':
                    //check if has #, so we can get clean hex
                    if (substr(trim($value[1]), 0, 1) == '#') {
                        $style_color = substr(trim($value[1]), 1);
                    }

                    if (empty($style_color)) {
                        continue;
                    }

                    $sheet->getStyle($column . $row)->applyFromArray(['font' => ['color' => ['rgb' => "{$style_color}"]]]);

                    break;

                case 'font-weight':
                    $is_font_bold = in_array(trim($value[1]), ['600', '700', 'bold', 'bolder']);
                    $sheet->getStyle($column . $row)->applyFromArray([
                        'font' => [
                            'bold' => $is_font_bold,
                        ],
                    ]);

                    break;

                case 'text-align':
                    $horizontal = Alignment::HORIZONTAL_LEFT;
                    switch (trim($value[1])) {
                        case 'center':
                            $horizontal = Alignment::HORIZONTAL_CENTER ;
                            break;
                        case 'right':
                            $horizontal = Alignment::HORIZONTAL_RIGHT;
                            break;
                        case 'justify':
                            $horizontal = Alignment::HORIZONTAL_JUSTIFY;
                            break;
                    }

                    $sheet->getStyle($column . $row)->applyFromArray([
                        'alignment' => [
                            'horizontal' => $horizontal,
                            //'vertical' => Alignment::VERTICAL_CENTER,
                            //'wrapText' => true,
                        ],
                    ]);

                    break;
            }
        }
    }
}
