<?php
/**
 * @package     Bestme
 * @author      Bestme
 * @copyright   Copyright (c) 2019, Bestme - Novaon, Corp. (https://bestme.vn/)
 * @license     https://opensource.org/licenses/GPL-3.0
 * @link        https://bestme.vn
 */

/**
 * Image_Server class
 */
class Image_Server
{
    CONST BESTME_IMAGE_SERVER = 'bestme';
    CONST CLOUDINARY = 'cloudinary';
    /**
     * @param string $name
     * @param mixed $credential
     * @return null|\Image\Abstract_Image_Manager
     */
    public static function getImageServer($name, $credential = [])
    {
        if ($name == self::CLOUDINARY) {
            $class_name = 'cloudinary_manager';
        } else {
            $class_name = 'bestme_image_manager';
        }

        $class = 'Image\\' . $class_name;
        if (!class_exists($class)) {
            return null;
        }

        /** @var \Image\Abstract_Image_Manager $instance */
        return new $class($class_name, $credential);
    }
}