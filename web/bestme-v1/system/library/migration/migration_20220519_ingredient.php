<?php

namespace Migration;

class migration_20220519_ingredient extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20220519_ingredient';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        // create new StoreReceipt tables
        try {
            $this->migrateNewIngredientTables();
        } catch (\Exception $e) {
            $this->log('migrateNewStoreReceiptTables got error: ' . $e->getMessage());
        }

        // permission for admin user
        try {
            $this->addPermissionForAdminUser([
                'ingredient/ingredient'
            ]);
        } catch (\Exception $e) {
            $this->log('addPermissionForAdminUser (ingredient) got error: ' . $e->getMessage());
        }
    }

    private function migrateNewIngredientTables()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}ingredient` (
                 `ingredient_id` INT(10) NOT NULL AUTO_INCREMENT,
                 `name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
                 `code` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
                 `active_ingredient` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 `unit` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 `basic_description` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 `note` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 `overdose` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 `course_certificates` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 `detail_description` LONGTEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 `us_man_19_30` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 `us_man_31_50` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 `us_man_51_70` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 `us_man_gt_70` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 `us_women_19_30` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 `us_women_31_50` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 `us_women_51_70` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 `us_women_gt_70` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 `vn_man_19_50` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 `vn_man_51_60` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 `vn_man_gt_60` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 `vn_women_19_50` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 `vn_women_51_60` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 `vn_women_gt_60` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 `created_at` datetime NOT NULL,
                 `updated_at` datetime NOT NULL,
                 PRIMARY KEY (`ingredient_id`) USING BTREE
                ) COLLATE='utf8_unicode_ci' ENGINE=MyISAM;";
        $this->db->query($sql);

        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}product_ingredient` (
                 `product_id` INT(10) NOT NULL,
                 `ingredient_id` INT(10) NOT NULL,
                 `quantitative` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                 PRIMARY KEY (`product_id`, `ingredient_id`) USING BTREE
                ) COLLATE='utf8_unicode_ci' ENGINE=MyISAM;";
        $this->db->query($sql);

        try {
            $sql = "ALTER TABLE `{$this->db_prefix}ingredient` ADD UNIQUE INDEX `code` (`code`);";
            $this->db->query($sql);
        } catch (\Exception $e) {
            $this->log("Edit table ingredient got error :" . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back ...');
        // no need
    }
}