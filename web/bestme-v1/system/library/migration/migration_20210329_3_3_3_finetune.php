<?php


namespace Migration;


class migration_20210329_3_3_3_finetune extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20210329_3_3_3_finetune';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /**
     * @inheritDoc
     */
    public function do_up()
    {
        /* homepage rating website (customer feedback) */
        $this->log('Will create ecommerce table');
        try {
            // permission for admin user
            $this->addPermissionForAdminUser([
                'theme/ecommerce'
            ]);
        } catch (\Exception $e) {
            $this->log('Create rate_website table got error: ' . $e->getMessage());
        }
    }

    /* === local functions === */

    /**
     * @inheritDoc
     */
    public function do_down()
    {
        $this->log('Will roll back 3_3_3_finetune... update later...');
    }
}