<?php

namespace Migration;

class migration_20200410_sync_report_order_to_cms extends migration_abstract
{
    use \Sync_Bestme_Data_To_Welcome_Util;

    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200410_sync_report_order_to_cms';
    /**
     * @var string
     */
    private $shop_name = null;

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will sync report order data to welcome db');

        // for admin user
        try {
            $this->syncReportOrderToWelcomeDatabase();
        } catch (\Exception $e) {
            $this->log('syncReportOrderToWelcomeDatabase got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        // no need
    }

    private function syncReportOrderToWelcomeDatabase()
    {
        /* get shop name */
        if (empty($this->shop_name)) {
            $query = $this->db->query("SELECT `value` FROM `{$this->db_prefix}setting` WHERE `key`= 'shop_name'");
            $this->shop_name = $query->row['value'];
        }

        $sql = "SELECT * FROM `{$this->db_prefix}report_order`";
        $result = $this->db->query($sql);
        foreach ($result->rows as $report) {
            if (!is_array($report)) {
                continue;
            }

            if (!array_key_exists('report_time', $report) || !array_key_exists('order_status', $report) || !array_key_exists('total_amount', $report)) {
                continue;
            }

            $dataOrderReport = [
                'shop_name' => $this->shop_name,
                'type' => 'add',
                'new_status' => $report['order_status'],
                'old_status' => null,
                'new_value' => $report['total_amount'],
                'old_value' => null,
                'time' => $report['report_time']
            ];
            $this->publishSyncShopOrder($dataOrderReport);
        }
    }
}