<?php

namespace Migration;

class migration_20200422_add_column_table_product extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200422_add_column_table_product';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate add common_cost_price to product table');

        try {
            $this->db->query("ALTER TABLE `{$this->db_prefix}product` 
                              ADD `common_cost_price` DECIMAL(15,4 ) NULL DEFAULT '0' AFTER `common_price`;");
        } catch (\Exception $e) {
            $this->log('migrate common_cost_price: error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back remove field common_cost_price in product table');

        try {
            $this->db->query("ALTER TABLE `{$this->db_prefix}product` DROP `common_cost_price`;");
        } catch (\Exception $e) {
        }
    }
}