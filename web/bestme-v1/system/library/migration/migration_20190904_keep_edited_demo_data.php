<?php

namespace Migration;

class migration_20190904_keep_edited_demo_data extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20190904_keep_edited_demo_data';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate all edited demo products from demo to real products (set demo=0)');

        $this->migrateEditedDemoDataToRealData();
    }

    protected function migrateEditedDemoDataToRealData()
    {
        $demo_product_ids = [
            // tech
            73,
            74,
            76,
            78,
            79,
            80,
            82,
            100,
            101,
            102,
            // fashion
            83,
            84,
            85,
            86,
            87,
            88,
            97,
            98,
            99,
            103,
            // beauty
            75,
            81,
            89,
            90,
            91,
            92,
            93,
            94,
            95,
            96,
            // furniture: same as tech
        ];

        $demo_product_ids_for_query = implode(', ', $demo_product_ids);

        try {
            /* migrate */
            $sql = "UPDATE `{$this->db_prefix}product` SET `demo` = 0
                    WHERE `date_added` <> `date_modified`
                    AND `product_id` IN ($demo_product_ids_for_query)";

            $this->db->query($sql);
        } catch (\Exception $e) {
            $this->log('[migration] migration_20190904_keep_edited_demo_data up(): got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will force migrate all demo products from both demo/real to demo products (set demo=0)');
        // no need
    }
}