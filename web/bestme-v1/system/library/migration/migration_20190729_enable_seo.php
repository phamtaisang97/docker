<?php

namespace Migration;

class migration_20190729_enable_seo extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20190729_enable_seo';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Enable seo setting');

        if ($this->is_config_seo_enabled()) {
            $this->log('Seo url is already enabled!');
            return;
        }

        /* enable seo setting */
        $sql = "UPDATE `{$this->db_prefix}setting` SET `value` = '1' WHERE `code` = 'config' AND `key` = 'config_seo_url';";
        $this->db->query($sql);

        /* insert some seo url for default pages */
        $sql = "INSERT INTO `{$this->db_prefix}seo_url` (`store_id`, `language_id`, `query`, `keyword`, `table_name`) VALUES
                ('0', '1', 'common/home', 'home', ''),
                ('0', '2', 'common/home', 'trang-chu', ''),
                ('0', '1', 'common/shop', 'products', ''),
                ('0', '2', 'common/shop', 'san-pham', ''),
                ('0', '1', 'contact/contact', 'contact', ''),
                ('0', '2', 'contact/contact', 'lien-he', ''),
                ('0', '1', 'checkout/profile', 'profile', ''),
                ('0', '2', 'checkout/profile', 'tai-khoan', ''),
                ('0', '1', 'account/login', 'login', ''),
                ('0', '2', 'account/login', 'dang-nhap', ''),
                ('0', '1', 'account/register', 'register', ''),
                ('0', '2', 'account/register', 'dang-ky', ''),
                ('0', '1', 'account/logout', 'logout', ''),
                ('0', '2', 'account/logout', 'dang-xuat', ''),
                ('0', '1', 'checkout/setting', 'setting', ''),
                ('0', '2', 'checkout/setting', 'cai-dat', ''),
                ('0', '1', 'checkout/my_orders', 'checkout-cart', ''),
                ('0', '2', 'checkout/my_orders', 'gio-hang', ''),
                ('0', '1', 'checkout/order_preview', 'payment', ''),
                ('0', '2', 'checkout/order_preview', 'thanh-toan', '');";
        $this->db->query($sql);
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Roll back all default policy for google shopping');

        // 99.99% not used. Temp do nothing. // TODO: add code if need...

        /* enable seo setting */
        $sql = "UPDATE `{$this->db_prefix}setting` SET `value` = '0' WHERE `code` = 'config' AND `key` = 'config_seo_url';";
        $this->db->query($sql);

        /* insert some seo url for default pages */
        $sql = "DELETE FROM `{$this->db_prefix}seo_url` WHERE 1 
                  AND `query` IN ('common/home', 'common/shop', 'contact/contact', 'checkout/profile', 'account/login', 'account/register', 'account/logout', 'checkout/setting', 'checkout/my_orders', 'checkout/order_preview') 
                  AND `keyword` IN ('home', 'trang-chu', 'products', 'san-pham', 'contact', 'lien-he', 'profile', 'tai-khoan', 'login', 'dang-nhap', 'register', 'dang-ky', 'logout', 'dang-xuat', 'setting', 'cai-dat', 'checkout-cart', 'gio-hang', 'payment', 'thanh-toan');";
        $this->db->query($sql);
    }

    /* === end - default functions === */

    /**
     * Check record for police tables
     * @return bool
     */
    private function is_config_seo_enabled()
    {
        $query = "SELECT * FROM `{$this->db_prefix}setting` WHERE `code` = 'config' AND `key` = 'config_seo_url' AND `value` = '1';";
        $data = $this->db->query($query);
        // check if enabled seo config existing in db
        if ($data->row) {
            return true;
        }

        return false;
    }
}