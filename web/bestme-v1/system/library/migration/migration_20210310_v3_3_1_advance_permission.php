<?php

namespace Migration;

class migration_20210310_v3_3_1_advance_permission extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20210310_v3_3_1_advance_permission';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration, $version)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION, $version);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        // add tables
        $this->log('add some tables');
        try {
            $this->addTables();
        } catch (\Exception $e) {
            $this->log('add tables got error: ' . $e->getMessage());
        }

        $this->log('add permission for setting/user_group');
        try {
            $this->addPermissionForAdminUser([
                'settings/user_group',
                // hot fix missing permission before...
                'sale_channel/pos_novaon',
            ]);
        } catch (\Exception $e) {
            $this->log('add permission got error: ' . $e->getMessage());
        }

        $this->log('add column access_all in table user_group');
        try {
            $this->addAccessAllColumn();
        } catch (\Exception $e) {
            $this->log('add column got error: ' . $e->getMessage());
        }

        $this->log('add column user_id in multi table');
        try {
            $this->addUserIdColumnForMultiTable();
        } catch (\Exception $e) {
            $this->log('add column user_id in multi table got error: ' . $e->getMessage());
        }
    }

    private function addAccessAllColumn()
    {
        $this->log('Add access_all column in user_group table');
        $sql = "ALTER TABLE `{$this->db_prefix}user_group`
	                ADD COLUMN `access_all` TINYINT NULL DEFAULT '1' AFTER `permission`;";
        $this->db->query($sql);
    }

    private function addUserIdColumnForMultiTable()
    {
        $this->log('Add user_id column in customer table');
        $sql = "ALTER TABLE `{$this->db_prefix}customer`
	                ADD COLUMN `user_create_id` INT(11) NULL DEFAULT '1' AFTER `staff_in_charge`;";
        $this->db->query($sql);

        $this->log('Add user_id column in customer group table');
        $sql = "ALTER TABLE `{$this->db_prefix}customer_group`
	                ADD COLUMN `user_create_id` INT(11) NULL DEFAULT '1' AFTER `customer_group_code`;";
        $this->db->query($sql);

        $this->log('Add user_id column in manufacture table');
        $sql = "ALTER TABLE `{$this->db_prefix}manufacturer`
	                ADD COLUMN `user_create_id` INT(11) NULL DEFAULT '1' AFTER `status`;";
        $this->db->query($sql);

        $this->log('Add user_id column in collection table');
        $sql = "ALTER TABLE `{$this->db_prefix}collection`
	                ADD COLUMN `user_create_id` INT(11) NULL DEFAULT '1' AFTER `type`;";
        $this->db->query($sql);

        $this->log('Add user_id column in product table');
        $sql = "ALTER TABLE `{$this->db_prefix}product`
	                ADD COLUMN `user_create_id` INT(11) NULL DEFAULT '1' AFTER `default_store_id`;";
        $this->db->query($sql);

        $this->log('Add user_id column in oc_return_receipt table');
        $sql = "ALTER TABLE `{$this->db_prefix}return_receipt` 
                    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `note`;";
        $this->db->query($sql);

        $this->log('Add user_id column in oc_payment_voucher table');
        $sql = "ALTER TABLE `{$this->db_prefix}payment_voucher` 
                    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `note`;";
        $this->db->query($sql);

        $this->log('Add user_id column in oc_receipt_voucher table');
        $sql = "ALTER TABLE `{$this->db_prefix}receipt_voucher` 
                    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `note`;";
        $this->db->query($sql);

        $this->log('Add user_id column in oc_store_receipt table');
        $sql = "ALTER TABLE `{$this->db_prefix}store_receipt` 
                    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `status`;";
        $this->db->query($sql);

        $this->log('Add user_id column in oc_store_transfer_receipt table');
        $sql = "ALTER TABLE `{$this->db_prefix}store_transfer_receipt` 
                    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `total`;";
        $this->db->query($sql);

        $this->log('Add user_id column in oc_store_take_receipt table');
        $sql = "ALTER TABLE `{$this->db_prefix}store_take_receipt` 
                    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `difference_amount`;";
        $this->db->query($sql);

        $this->log('Add user_id column in oc_cost_adjustment_receipt table');
        $sql = "ALTER TABLE `{$this->db_prefix}cost_adjustment_receipt` 
                    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `note`;";
        $this->db->query($sql);

        $this->log('Add user_id column in oc_discount table');
        $sql = "ALTER TABLE `{$this->db_prefix}discount` 
                    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `end_at`;";
        $this->db->query($sql);

    }

    private function addTables()
    {
        $this->log('Add table user_store');
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}user_store` ( 
                `id` INT(11) NOT NULL AUTO_INCREMENT , 
                `user_id` INT(11) NOT NULL , 
                `store_id` VARCHAR(255) NULL , 
                PRIMARY KEY (`id`)) ENGINE = MyISAM 
                CHARSET=utf8 COLLATE utf8_general_ci;";
        $this->db->query($sql);
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back create tables in version advance_permission');
    }
}