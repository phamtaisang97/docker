<?php

namespace Migration;

class migration_20210504_v3_4_2_sync_bestme_social extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20210504_v3_4_2_sync_bestme_social';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('add columns is_authenticated,subcriber_id to table customer');
        try {
            $this->addColumnToCustomerTable();
        } catch (\Exception $e) {
            $this->log('add columns is_authenticated to table customer got error: ' . $e->getMessage());
        }

        $this->log('add table oc_order_campaign');
        try {
            $this->addTableTOrderCampaign();
        } catch (\Exception $e) {
            $this->log('add table oc_order_campaign got error: ' . $e->getMessage());
        }

        $this->log('add table oc_campaign_voucher');
        try {
            $this->addTableTCampaignVoucher();
        } catch (\Exception $e) {
            $this->log('add table oc_campaign_voucher got error: ' . $e->getMessage());
        }

        $this->log('add columns campaign_voucher_id to table oc_order_product');
        try {
            $this->addColumnToOrderProductTable();
        } catch (\Exception $e) {
            $this->log('add columns campaign_voucher_id got error: ' . $e->getMessage());
        }

    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back column version');
    }

    protected function addColumnToCustomerTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}customer` 
                ADD `is_authenticated` TINYINT(1) NOT NULL DEFAULT '1' AFTER `customer_source`,
                ADD `subscriber_id` VARCHAR(255) NULL AFTER `user_create_id`;
                ";
        $this->db->query($sql);

        $sql = "ALTER TABLE `{$this->db_prefix}customer` CHANGE `customer_source` `customer_source` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '1: NovaonX Social, 2: Facebook, 3: Bestme';";
        $this->db->query($sql);
    }

    protected function addTableTOrderCampaign()
    {
        $sql = "CREATE TABLE `{$this->db_prefix}order_campaign` ( 
                `order_id` INT(11) NOT NULL , 
                `campaign_id` VARCHAR(255) NOT NULL , 
                PRIMARY KEY (`order_id`, `campaign_id`)) ENGINE = MyISAM;
                ";
        $this->db->query($sql);
    }

    protected function addTableTCampaignVoucher()
    {
        $sql = "CREATE TABLE `{$this->db_prefix}campaign_voucher` ( 
                `campaign_voucher_id` INT(11) NOT NULL AUTO_INCREMENT , 
                `voucher_code` VARCHAR(255) NULL DEFAULT NULL , 
                `voucher_type` INT(11) NULL COMMENT '1: percent. 2: amount' , 
                `amount` DECIMAL(18,4) NULL DEFAULT 0.000 , 
                `user_create_id` INT(11) NULL , 
                `start_at` TIMESTAMP NULL DEFAULT NULL , 
                `end_at` TIMESTAMP NULL DEFAULT NULL , 
                `date_added` TIMESTAMP NULL DEFAULT NULL , 
                `date_modified` TIMESTAMP NULL DEFAULT NULL , 
                PRIMARY KEY (`campaign_voucher_id`)) ENGINE = MyISAM;
                ";
        $this->db->query($sql);
    }

    protected function addColumnToOrderProductTable()
    {
     $sql = "ALTER TABLE `{$this->db_prefix}order_product` 
            ADD `campaign_voucher_id` INT(11) NULL DEFAULT NULL AFTER `tax`;";
     $this->db->query($sql);
    }
}