<?php

namespace Migration;

class migration_20200420_store_permission extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200420_store_permission';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        // permission for admin user
        try {
            $this->addPermissionForAdminUser([
                'catalog/store'
            ]);
        } catch (\Exception $e) {
            $this->log('addPermissionForAdminUser (Catalog/Store) got error: ' . $e->getMessage());
        }

        // permission for staff users
        try {
            $this->addPermissionForStaffUser([
                'catalog/product' => ['catalog/store']
            ]);
        } catch (\Exception $e) {
            $this->log('addPermissionForStaffUser (Catalog/Store) got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back ...');
        // no need
    }
}