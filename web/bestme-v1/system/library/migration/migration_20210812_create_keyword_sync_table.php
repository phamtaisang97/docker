<?php


namespace Migration;


class migration_20210812_create_keyword_sync_table extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20210812_create_keyword_sync_table';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('create keyword_sync table');
        try {
            $this->createKeyword_syncTable();
        } catch (\Exception $e) {
            $this->log('create keyword_sync table got error: ' . $e->getMessage());
        }
    }


    private function createKeyword_syncTable()
    {
        $sql = "CREATE TABLE `{$this->db_prefix}keyword_sync` (
                  `keyword_id` int(11) NOT NULL AUTO_INCREMENT,
                  `keyword` text COLLATE utf8_unicode_ci NOT NULL,
                  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  PRIMARY KEY (keyword_id)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;";
        $this->db->query($sql);
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back create keyword sync table');
    }
}