<?php

namespace Migration;

use Image\Cloudinary_Manager;

class migration_20191223_save_image_url_to_database extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20191223_save_image_url_to_database';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will save images url from cloudinary to database');

        // create table *_images
        $this->db->query("CREATE TABLE IF NOT EXISTS `{$this->db_prefix}images` ( 
                `image_id` INT(11) NOT NULL AUTO_INCREMENT ,
                `type` VARCHAR(56) NOT NULL DEFAULT 'image',
                `name` varchar(256) NULL default NULL ,
                `url` VARCHAR(256) NOT NULL , 
                `directory` VARCHAR(56) NULL DEFAULT NULL , 
                `status` TINYINT(1) NOT NULL DEFAULT '1' , 
                `source` VARCHAR(56) NOT NULL DEFAULT 'Cloudinary' ,
                `source_id` VARCHAR (256) NULL DEFAULT  NULL ,
                PRIMARY KEY (`image_id`)) 
                ENGINE = MyISAM DEFAULT CHARSET=utf8");

        // save image urls from cloudinary to database
        $cloudinary = new Cloudinary_Manager();
        $this->saveImageUrlsFromCloudinary($cloudinary);
    }

    /**
     * @param Cloudinary_Manager $cloudinary
     */
    private function saveImageUrlsFromCloudinary(Cloudinary_Manager $cloudinary, $next_cursor = null)
    {
        $images = $cloudinary->getImages(SHOP_ID, $next_cursor, 100);

        if (!is_array($images)){
            return;
        }

        if (!array_key_exists('images', $images) || !is_array($images['images'])){
            return;
        }

        foreach ($images['images'] as $image){
            if (!isset($image['public_id']) || !isset($image['secure_url'])){
                continue;
            }
            $url = $image['secure_url'];
            $source_id = $image['public_id'];
            $imageName = explode('/', $source_id);
            $imageName = is_array($imageName) ? end($imageName) : '';
            $sql = "INSERT INTO `{$this->db_prefix}images` (`name`, `url`, `source_id`) VALUES ('$imageName','$url', '$source_id');";
            $this->db->query($sql);
        }

        if (array_key_exists('next_cursor', $images) && $images['next_cursor'] != ''){
            $this->saveImageUrlsFromCloudinary($cloudinary, $images['next_cursor']);
        }
        return;
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will drop table *_images');

        $sql = "DROP TABLE IF EXISTS `{$this->db_prefix}images`";
        $this->db->query($sql);
    }
}