<?php

namespace Migration;

class migration_20201007_tracking_pos_order_complete extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20201007_tracking_pos_order_complete';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will insert tracking_pos_order_complete event to event table');

        try {
            $this->migrateInsertEvents();
        } catch (\Exception $e) {
            $this->log('Insert tracking_pos_order_complete event to event table got error: ' . $e->getMessage());
        }
    }

    protected function migrateInsertEvents()
    {
        $sql = "INSERT INTO `{$this->db_prefix}event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES 
                ('tracking_pos_order_complete', 'catalog/model/sale/order/addOrderCommon/after', 'event/activity_tracking/trackingOrderComplete', 1, 0);";
        $this->db->query($sql);
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back remove tracking_pos_order_complete event in event table');
    }
}