<?php

namespace Migration;

class migration_20200922_transport_status_management extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200922_transport_status_management';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate update table for v2.8.2-transport_status_management');
        try {
            $this->migrateUpdateData();
        } catch (\Exception $e) {
            $this->log('Update table for v2.8.2-transport_status_management got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back update table of v2.8.2-transport_status_management');
    }

    protected function migrateUpdateData()
    {
        // Thêm trường manual_update_status vào bảng oc_order
        $sql = "ALTER TABLE `{$this->db_prefix}order` 
                ADD `manual_update_status` TINYINT NOT NULL DEFAULT '0' AFTER `date_modified`";
        $this->db->query($sql);
    }
}