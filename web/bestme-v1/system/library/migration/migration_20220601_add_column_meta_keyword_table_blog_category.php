<?php

namespace Migration;

class migration_20220601_add_column_meta_keyword_table_blog_category extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20220601_add_column_meta_keyword_table_blog_category';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('add_column_meta_keyword_table_blog_category');
        try {
            $this->addColumnMetaKeywordBlogTable();

        } catch (\Exception $e) {
            $this->log('add_column_meta_keyword_table_blog_category got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back column meta_keyword table_blog_category');
    }

    protected function addColumnMetaKeywordBlogTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}blog_category_description` ADD COLUMN `meta_keyword` VARCHAR(255) NULL COLLATE 'utf8_general_ci' AFTER `alias`;";
        $this->db->query($sql);
    }
}