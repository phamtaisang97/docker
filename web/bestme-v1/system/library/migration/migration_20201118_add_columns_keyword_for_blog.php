<?php

namespace Migration;

class migration_20201118_add_columns_keyword_for_blog extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20201118_add_columns_keyword_for_blog';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('add columns to table customer');
        try {
            $this->addColumnToBlogDescriptionTable();
        } catch (\Exception $e) {
            $this->log('add columns to table oc_blog_description got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back column payment_status table oc_blog_description version');
    }

    protected function addColumnToBlogDescriptionTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}blog_description` 
                ADD `seo_keywords` VARCHAR(500) NULL DEFAULT NULL AFTER `meta_description`;";
        $this->db->query($sql);
    }

}