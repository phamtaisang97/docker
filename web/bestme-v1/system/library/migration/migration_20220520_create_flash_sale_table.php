<?php


namespace Migration;


class migration_20220520_create_flash_sale_table extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20220520_create_flash_sale_table';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('create flash_sale table');
        try {
            $this->createFlashSaleTable();
        } catch (\Exception $e) {
            $this->log('create flash_sale table got error: ' . $e->getMessage());
        }
    }


    private function createFlashSaleTable()
    {
        $sql = "CREATE TABLE `{$this->db_prefix}flash_sale` (
                    `flash_sale_id` INT(11) NOT NULL AUTO_INCREMENT,
                    `collection_id` INT(11) NOT NULL,
                    `starting_date` DATE NOT NULL,
                    `ending_date` DATE NOT NULL,
                    `starting_time` TIME NOT NULL,
                    `ending_time` TIME NOT NULL,
	                `status` TINYINT(1) NOT NULL DEFAULT '1',
                    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`flash_sale_id`),
                    INDEX `collection_id` (`collection_id`)
                ) ENGINE=MyISAM COLLATE='utf8mb4_unicode_ci';";
        $this->db->query($sql);
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back create flash_sale table');
    }
}