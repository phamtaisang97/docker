<?php

namespace Migration;

class migration_20200312_store_receipt extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200312_store_receipt';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        // create new StoreReceipt tables
        try {
            $this->migrateNewStoreReceiptTables();
        } catch (\Exception $e) {
            $this->log('migrateNewStoreReceiptTables got error: ' . $e->getMessage());
        }

        // migrate product_to_store table
        try {
            $this->migrateProductToStoreData();
        } catch (\Exception $e) {
            $this->log('migrateNewStoreReceiptTables got error: ' . $e->getMessage());
        }

        // migrate manufacturer_code
        try {
            $this->migrateManufacturerCode();
        } catch (\Exception $e) {
            $this->log('migrateManufacturerCode got error: ' . $e->getMessage());
        }

        // permission for admin user
        try {
            $this->addPermissionForAdminUser([
                'catalog/store_receipt'
            ]);
        } catch (\Exception $e) {
            $this->log('addPermissionForAdminUser (StoreReceipt) got error: ' . $e->getMessage());
        }

        // permission for staff users
        try {
            $this->addPermissionForStaffUser([
                'catalog/product' => ['catalog/store_receipt']
            ]);
        } catch (\Exception $e) {
            $this->log('addPermissionForStaffUser (StoreReceipt) got error: ' . $e->getMessage());
        }
    }

    private function migrateNewStoreReceiptTables()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}store_receipt` ( 
                  `store_receipt_id` INT(11) NOT NULL AUTO_INCREMENT, 
                  `store_id` INT(11) NOT NULL, 
                  `manufacturer_id` INT(11) NOT NULL, 
                  `store_receipt_code` varchar(50) NULL, 
                  `total` decimal(18,4) DEFAULT NULL,
                  `total_to_pay` decimal(18,4) DEFAULT NULL,
                  `total_paid` decimal(18,4) DEFAULT NULL,
                  `discount` decimal(18,4) DEFAULT NULL,
                  `allocation_criteria_type` VARCHAR(50) DEFAULT NULL,
                  `allocation_criteria_total` decimal(18,4) DEFAULT NULL,
                  `allocation_criteria_fees` MEDIUMTEXT NULL DEFAULT NULL,
                  `owed` decimal(18,4) DEFAULT NULL,
                  `comment` TEXT NULL DEFAULT NULL, 
                  `status` INT(4) NULL, 
                  `created_at` DATETIME NULL,
                  `updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
                  PRIMARY KEY (`store_receipt_id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";
        $this->db->query($sql);

        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}store_receipt_to_product` ( 
                  `store_receipt_product_id` INT(11) NOT NULL  AUTO_INCREMENT, 
                  `store_receipt_id` INT(11) NOT NULL, 
                  `product_id` INT(11) NOT NULL, 
                  `product_version_id` INT(11) NULL, 
                  `quantity` INT(11) NULL, 
                  `cost_price` decimal(18,4) DEFAULT NULL, 
                  `fee` decimal(18,4) DEFAULT NULL, 
                  `discount` decimal(18,4) DEFAULT NULL, 
                  `total` decimal(18,4) DEFAULT NULL, 
                  PRIMARY KEY (`store_receipt_product_id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";
        $this->db->query($sql);

        // new column oc_manufacturer
        try {
            $sql = "ALTER TABLE `{$this->db_prefix}manufacturer`
                      ADD `manufacturer_code` varchar(50) NULL AFTER `sort_order`,
                      ADD `telephone` varchar(30) NULL AFTER `manufacturer_code`,
                      ADD `email` varchar(200) NULL AFTER `telephone`,
                      ADD `tax_code` varchar(200) NULL AFTER `email`,
                      ADD `address` text NULL AFTER `tax_code`,
                      ADD `province` varchar(50) NULL AFTER `address`,
                      ADD `district` varchar(50) NULL AFTER `province`,
                      ADD `status` int(4) NOT NULL DEFAULT '1' AFTER `district`,
                      ADD `date_added` datetime NULL AFTER `status`,
                      ADD `date_modified` datetime NULL AFTER `date_added`;";
            $this->db->query($sql);
        } catch (\Exception $e) {
            $this->log('migrateNewStoreReceiptTables got error: new column oc_manufacturer: ' . $e->getMessage());
        }

        // new column oc_product_to_store
        try {
            $sql = "ALTER TABLE `{$this->db_prefix}product_to_store`
                      ADD `product_version_id` int(11) NULL DEFAULT 0 AFTER `store_id`,
                      ADD `quantity` int(11) NULL DEFAULT 0 AFTER `product_version_id`,
                      ADD `cost_price` DECIMAL(15,4 ) NULL DEFAULT '0' AFTER `quantity`;";
            $this->db->query($sql);
        } catch (\Exception $e) {
            $this->log('migrateNewStoreReceiptTables got error: new column oc_product_to_store: ' . $e->getMessage());
        }

        // change primary key oc_product_to_store
        // TODO: test affection old data???
        try {
            $sql = "ALTER TABLE `{$this->db_prefix}product_to_store` 
                      DROP PRIMARY KEY, ADD PRIMARY KEY(`product_id`, `product_version_id`, `store_id`);";
            $this->db->query($sql);
        } catch (\Exception $e) {
            $this->log('migrateNewStoreReceiptTables got error: change primary key oc_product_to_store: ' . $e->getMessage());
        }

        // new column oc_product
        try {
            $sql = "ALTER TABLE `{$this->db_prefix}product` 
                      ADD `default_store_id` INT(11) NULL DEFAULT '0' AFTER `deleted`;";
            $this->db->query($sql);
        } catch (\Exception $e) {
            $this->log('migrateNewStoreReceiptTables got error: new column oc_product: ' . $e->getMessage());
        }
    }

    public function migrateProductToStoreData()
    {
        // get all products with quantity
        $sql = "SELECT p.product_id, 
                       pv.product_version_id, 
                       p.quantity as p_quantity, 
                       pv.quantity as pv_quantity 
                FROM `{$this->db_prefix}product` p 
                LEFT JOIN `{$this->db_prefix}product_version` pv ON (p.product_id = pv.product_id)
                WHERE 1=1 
                  AND (p.`deleted` IS NULL OR p.`deleted` = '') 
                  AND (pv.`deleted` IS NULL OR pv.`deleted` = '')";
        $query = $this->db->query($sql);
        $products = $query->rows;

        // Notice: old data is product_id + store_id only. New data includes product_version_id

        // truncate data if have product_version_id, for re-running migration if force
        $sql = "DELETE FROM `{$this->db_prefix}product_to_store` 
                WHERE 1=1 
                  AND `product_version_id` IS NOT NULL 
                  AND `product_version_id` <> '' 
                  AND `product_version_id` <> '0'";
        $this->db->query($sql);

        // migrate to new table product_to_store.
        $success_count = 0;
        $fail_count = 0;
        foreach ($products as $product) {
            if (!isset($product['product_id'])) {
                $fail_count++;
                continue;
            }

            // for product single version
            $is_multi_versions = (isset($product['product_version_id']) && !empty($product['product_version_id']));
            if (!$is_multi_versions) {
                // migrate quantity
                $quantity = isset($product['p_quantity']) ? $product['p_quantity'] : 0;

                $sql = "UPDATE `{$this->db_prefix}product_to_store` 
                        SET `quantity` = {$quantity} 
                        WHERE 1=1 
                          AND `product_id` = {$product['product_id']} 
                          AND `store_id` = 0";
                $this->db->query($sql);

                $success_count++;

                continue;
            }

            // for product multi versions
            //// delete old data that has product_id only
            $sql = "DELETE FROM `{$this->db_prefix}product_to_store` 
                    WHERE 1=1 
                      AND `product_id` = {$product['product_id']} 
                      AND (`product_version_id` IS NULL OR `product_version_id` = '' OR `product_version_id` = '0')";
            $this->db->query($sql);

            //// skip if empty version
            if (empty($product['product_version_id'])) {
                $fail_count++;

                continue;
            }

            //// do create new with both product_id and product_version_id
            $quantity = isset($product['pv_quantity']) ? $product['pv_quantity'] : 0;
            $sql = "INSERT INTO `{$this->db_prefix}product_to_store` 
                    (`product_id`, `product_version_id`, `store_id`, `quantity`, `cost_price`) 
                    VALUES ({$product['product_id']}, {$product['product_version_id']}, '0', {$quantity}, '0')";
            $this->db->query($sql);

            $success_count++;
        }

        $this->log("migrateProductToStoreData: {$success_count} products success, {$fail_count} products failed");
    }

    private function migrateManufacturerCode()
    {
        $sql = "UPDATE `{$this->db_prefix}manufacturer` 
                SET `manufacturer_code`= CONCAT('NCC', LPAD(`manufacturer_id`, 6, '0')), 
                    `date_added` = NOW(), 
                    `date_modified` = NOW()";
        $this->db->query($sql);
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back ...');
        // no need
    }
}