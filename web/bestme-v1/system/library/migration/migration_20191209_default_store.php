<?php

namespace Migration;

class migration_20191209_default_store extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20191209_default_store';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will init default store');

        /* add default store */
        try {
            $this->up_default_store();
        } catch (\Exception $e) {
            $this->log('up_default_store: error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will rollback the default store');

        /* remove default store */
        try {
            $this->down_default_store();
        } catch (\Exception $e) {
            $this->log('down_default_store: error: ' . $e->getMessage());
        }
    }

    /* === end - default functions === */

    private function up_default_store()
    {
        $this->log('Up default store');

        /* delete if existed */
        $sql = "DELETE FROM `{$this->db_prefix}store` WHERE `store_id` = 0";
        $this->db->query($sql);

        /* then add new */
        $sql = "INSERT INTO `{$this->db_prefix}store` (`store_id`, `name`, `url`, `ssl`) VALUES 
                (0, 'Kho trung tâm', '', '');";
        $this->db->query($sql);

        // VERY IMPORTANT: update default store_id = 0 with default name cause store_id is auto increment start 0
        // this for matching with all old data of old shop with default store_id = 0
        $sql = "UPDATE `{$this->db_prefix}store` SET `store_id` = 0 WHERE `name` = 'Kho trung tâm'";
        $this->db->query($sql);
    }

    private function down_default_store()
    {
        $sql = "DELETE FROM `{$this->db_prefix}store` WHERE `store_id` = 0";
        $this->db->query($sql);
    }
}