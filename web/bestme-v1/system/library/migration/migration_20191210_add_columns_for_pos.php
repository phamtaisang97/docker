<?php

namespace Migration;

class migration_20191210_add_columns_for_pos extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20191210_add_columns_for_pos';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate add store_id, user_id to report_customer, report_product, report_order tables; 
                    add pos_id_ref to order table, and channel to product table');

        // report customer
        try {
            $this->db->query("ALTER TABLE `{$this->db_prefix}report_customer` 
                              ADD `store_id` INT(11) NULL DEFAULT 0 AFTER `customer_id`, 
                              ADD `user_id` INT(11) NULL DEFAULT 1 AFTER `store_id`;");
        } catch (\Exception $e) {
            $this->log('migrate report customer: error: ' . $e->getMessage());
        }

        // report order
        try {
            $this->db->query("ALTER TABLE `{$this->db_prefix}report_order` 
                              ADD `store_id` INT(11) NULL DEFAULT 0 AFTER `order_status`, 
                              ADD `user_id` INT(11) NULL DEFAULT 1 AFTER `store_id`;");

            // TODO: remove?...
            /*if (!$this->checkFieldIsExist($this->db_prefix . 'report_order', 'user_id') && !$this->checkFieldIsExist($this->db_prefix . 'report_order', 'store_id')) {
                // set value for user_id, store_id
                $query = $this->db->query("SELECT rpo.report_time, rpo.order_id, o.user_id, o.store_id FROM `{$this->db_prefix}report_order` rpo LEFT JOIN `{$this->db_prefix}order` o ON (rpo.order_id = o.order_id)");
                foreach ($query->rows as $item) {
                    if (!is_array($item) || !array_key_exists('order_id', $item) || !array_key_exists('report_time', $item)
                        || !array_key_exists('user_id', $item) || !array_key_exists('store_id', $item)) {
                        continue;
                    }
                    $report_time = $item['report_time'];
                    $order_id = $item['order_id'];
                    $store_id = $item['store_id'] === '' || $item['store_id'] === null ? 0 : $item['store_id'];
                    $user_id = $item['user_id'] === '' || $item['user_id'] === null ? 1 : $item['user_id'];
                    // update store_id, user_id
                    $this->db->query("UPDATE `{$this->db_prefix}report_order` SET `store_id` = $store_id, `user_id` = $user_id WHERE `report_time` = '$report_time' AND `order_id` = $order_id");
                }
            }*/
        } catch (\Exception $e) {
            $this->log('migrate report order: error: ' . $e->getMessage());
        }

        // report product
        try {
            $this->db->query("ALTER TABLE `{$this->db_prefix}report_product` 
                              ADD `store_id` INT(11) NULL DEFAULT 0 AFTER `total_amount`, 
                              ADD `user_id` INT NULL DEFAULT 1 AFTER `store_id`;");

            // TODO: remove?...
            /*if (!$this->checkFieldIsExist($this->db_prefix . 'report_product', 'user_id') && !$this->checkFieldIsExist($this->db_prefix . 'report_product', 'store_id')) {
                $this->db->query("ALTER TABLE `{$this->db_prefix}report_product` ADD `store_id` INT(11) NULL DEFAULT NULL AFTER `total_amount`, ADD `user_id` INT NULL DEFAULT NULL AFTER `store_id`;");
                // set value for user_id, store_id
                $query = $this->db->query("SELECT rp.report_time, rp.order_id, rp.product_id, rp.product_version_id, o.store_id, o.user_id FROM `{$this->db_prefix}report_product` rp LEFT JOIN `{$this->db_prefix}order` o ON (o.order_id = rp.order_id)");
                foreach ($query->rows as $item) {
                    if (!is_array($item) || !array_key_exists('order_id', $item) || !array_key_exists('report_time', $item) || !array_key_exists('user_id', $item)
                        || !array_key_exists('store_id', $item) || !array_key_exists('product_id', $item) || !array_key_exists('product_version_id', $item)) {
                        continue;
                    }
                    $product_id = $item['product_id'];
                    $product_version_id = $item['product_version_id'];
                    $report_time = $item['report_time'];
                    $order_id = $item['order_id'];
                    $store_id = $item['store_id'] === '' || $item['store_id'] === null ? 0 : $item['store_id'];
                    $user_id = $item['user_id'] === '' || $item['user_id'] === null ? 1 : $item['user_id'];
                    // update store_id, user_id
                    $this->db->query("UPDATE `{$this->db_prefix}report_product` SET `store_id` = $store_id, `user_id` = $user_id WHERE `report_time` = '$report_time' AND `order_id` = $order_id AND `product_id` = $product_id AND `product_version_id` = $product_version_id");
                }
            }*/
        } catch (\Exception $e) {
            $this->log('migrate report product: error: ' . $e->getMessage());
        }

        // table order
        try {
            $this->db->query("ALTER TABLE `{$this->db_prefix}order` 
                              ADD `pos_id_ref` VARCHAR(50) NULL DEFAULT NULL AFTER `order_code`;");
        } catch (\Exception $e) {
            $this->log('migrate report customer: error: ' . $e->getMessage());
        }

        // table product. channel: null or 0 -> website, 1 -> offline, 2 -> website + offline
        try {
            $this->db->query("ALTER TABLE `{$this->db_prefix}product` 
                              ADD `channel` INT NULL DEFAULT 2 AFTER `status`;");
        } catch (\Exception $e) {
            $this->log('migrate report customer: error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back remove field store_i, user_id in report_customer, report_product, report_order tables');

        // remove store_id, user_id in report_customer
        try {
            $this->db->query("ALTER TABLE `{$this->db_prefix}report_customer` DROP `store_id`, DROP `user_id`;");
        } catch (\Exception $e) {
        }

        // remove store_id, user_id in report_order
        try {
            $this->db->query("ALTER TABLE `{$this->db_prefix}report_order` DROP `store_id`, DROP `user_id`;");
        } catch (\Exception $e) {
        }

        // remove store_id, user_id in report_product
        try {
            $this->db->query("ALTER TABLE `{$this->db_prefix}report_product` DROP `store_id`, DROP `user_id`;");
        } catch (\Exception $e) {
        }
    }

    // TODO: remove?...
    /*private function checkFieldIsExist($table, $field)
    {
        $query = $this->db->query("SHOW COLUMNS FROM `$table` LIKE '$field'");

        return $query->num_rows > 0 ? true : false;
    }*/
}