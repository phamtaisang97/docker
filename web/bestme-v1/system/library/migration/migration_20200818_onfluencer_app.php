<?php

namespace Migration;

class migration_20200818_onfluencer_app extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200818_onfluencer_app';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate add permission mar_ons_onfluencer "extension/appstore/mar_ons_onfluencer" to admin user');

        // permission for admin user
        try {
            $this->addPermissionForAdminUser([
                'extension/appstore/mar_ons_onfluencer'
            ]);
        } catch (\Exception $e) {
            $this->log('addPermissionForAdminUser (mar_ons_onfluencer) got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back permission mar_ons_onfluencer "extension/appstore/mar_ons_onfluencer" from admin user');
        // no need
    }
}