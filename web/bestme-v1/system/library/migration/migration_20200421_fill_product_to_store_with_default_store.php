<?php

namespace Migration;

class migration_20200421_fill_product_to_store_with_default_store extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200421_fill_product_to_store_with_default_store';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        // fill Product To Store With Default Store
        try {
            $this->fillProductToStoreWithDefaultStore();
        } catch (\Exception $e) {
            $this->log('fillProductToStoreWithDefaultStore got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back ...');
        // no need
    }

    private function fillProductToStoreWithDefaultStore()
    {
        $query = $this->db->query("SELECT p.product_id, p.default_store_id, pv.product_version_id 
                                   FROM `{$this->db_prefix}product` p 
                                   LEFT JOIN `{$this->db_prefix}product_version` pv 
                                          ON CASE WHEN p.multi_versions = 1 
                                                  THEN p.product_id 
                                                  ELSE -1 
                                             END = pv.product_id 
                                   WHERE p.`deleted` IS NULL AND pv.`deleted` IS NULL");

        foreach ($query->rows as $pv) {
            $product_id = (int)$pv['product_id'];
            $product_version_id = (int)$pv['product_version_id'];
            $default_store_id = (int)$pv['default_store_id'];
            $pts = $this->db->query("SELECT * FROM `{$this->db_prefix}product_to_store` WHERE `product_id` = " . $product_id
                . " AND `product_version_id` = " . $product_version_id
                . " AND `store_id` = " . $default_store_id);

            if ($pts->num_rows > 0) {
                continue;
            } else {
                $this->db->query("INSERT INTO `{$this->db_prefix}product_to_store` SET `product_id` = " . (int)$product_id
                    . ", `store_id` = " . (int)$default_store_id
                    . ", `product_version_id` = " . (int)$product_version_id
                    . ", `quantity` = 0, `cost_price` = 0");
            }
        }
    }
}