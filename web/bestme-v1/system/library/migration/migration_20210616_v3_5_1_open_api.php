<?php


namespace Migration;


class migration_20210616_v3_5_1_open_api extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20210616_v3_5_1_open_api';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /**
     * @inheritDoc
     */
    public function do_up()
    {
        try {
            // permission for admin user
            $this->addPermissionForAdminUser([
                'app_store/detail'
            ]);
        } catch (\Exception $e) {
            $this->log('addPermissionForAdminUser table got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function do_down()
    {
        $this->log('Will roll back ... update later...');
    }
}