<?php

namespace Migration;

class migration_20200819_add_columns_for_customer extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200819_add_columns_for_customer';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('update Code Customer');
        try {
            $this->updateCodeCustomer();
        } catch (\Exception $e) {
            $this->log('update Code Customer got error: ' . $e->getMessage());
        }

        $this->log('add columns to table customer');
        try {
            $this->addColumnToCustomerTable();
        } catch (\Exception $e) {
            $this->log('add columns to table customer got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back column payment_status table shopee_order version');
    }

    protected function addColumnToCustomerTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}customer` 
            ADD `sex` TINYINT(2) NULL COMMENT '1: Nam, 2: Nữ, 3: Khác' AFTER `date_added`,
            ADD `birthday` DATE NULL AFTER `sex`,
            ADD `full_name` VARCHAR(255) NOT NULL AFTER `birthday`,
            ADD `website` VARCHAR(255) NULL AFTER `full_name`,
            ADD `tax_code` VARCHAR(100) NULL AFTER `website`,
            ADD `staff_in_charge` INT(11) NULL AFTER `tax_code`;";
        $this->db->query($sql);

        $sql = "ALTER TABLE `{$this->db_prefix}customer`
            CHANGE `firstname` `firstname` VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, 
            CHANGE `lastname` `lastname` VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;";
        $this->db->query($sql);

        $sql = "ALTER TABLE `{$this->db_prefix}customer_group` 
          ADD `customer_group_code` VARCHAR(50) NULL AFTER `sort_order`;";
        $this->db->query($sql);
    }

    protected function updateCodeCustomer()
    {
        $sql = "SELECT customer_id FROM `{$this->db_prefix}customer` WHERE code is NULL";
        $customer_ids = $this->db->query($sql)->rows;
        foreach ($customer_ids as $customer_id) {
            $prefix = 'KH';
            $code = $prefix . str_pad($customer_id['customer_id'], 6, "0", STR_PAD_LEFT);
            $sql = "UPDATE `{$this->db_prefix}customer` 
                    SET `code` = '{$code}' 
                    WHERE `customer_id` = {$customer_id['customer_id']}";
            $this->db->query($sql);
        }
    }
}