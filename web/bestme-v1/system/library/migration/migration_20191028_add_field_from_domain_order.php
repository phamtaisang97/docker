<?php

namespace Migration;

class migration_20191028_add_field_from_domain_order extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20191028_add_field_from_domain_order';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Init alter add field from_domain in *_order table');

        try {
            $sql = "ALTER TABLE `{$this->db_prefix}order` ADD `from_domain` VARCHAR(500) NULL DEFAULT NULL AFTER `source`";
            $this->db->query($sql);
        } catch (\Exception $e) {
            $this->log('got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Remove field from_domain in *_order table');

        try {
            $sql = "ALTER TABLE `{$this->db_prefix}order` DROP `from_domain`;";
            $this->db->query($sql);
        } catch (\Exception $e) {
            $this->log('got error: ' . $e->getMessage());
        }
    }
}