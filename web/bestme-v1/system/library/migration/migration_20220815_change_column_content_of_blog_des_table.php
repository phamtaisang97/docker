<?php


namespace Migration;


use Matrix\Exception;

class migration_20220815_change_column_content_of_blog_des_table extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20220815_change_column_content_of_blog_des_table';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('change_column_content_of_blog_des_table');
        try {
            $this->migrateColumn();
        } catch (\Exception $e) {
            $this->log('change_column_content_of_blog_des_table got error: ' . $e->getMessage());
        }


    }

    private function migrateColumn()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}blog_description`
	                CHANGE COLUMN `content` `content` LONGTEXT NULL COLLATE 'utf8_general_ci' AFTER `title`;";
        $this->db->query($sql);

    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back change_column_content_of_blog_des_table');
    }
}