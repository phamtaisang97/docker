<?php

namespace Migration;

class migration_20190731_remake_menu extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20190731_remake_menu';

    static $TYPE_OLD_TO_NEW_MAPPING = [
        1 => 3, // product
        2 => 1, // category
        3 => 6, // blog
        5 => 4, // url
    ];

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate menu from old menu to new menu');

        // khoi tao bang menu moi
        $this->createNewMenuTable();

        // migrate data
        $this->migrateMenu();

        // enable menu permission
        $this->enableMenuPermission();
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back menu');

        $sql = "DROP TABLE IF EXISTS `{$this->db_prefix}menu`, `{$this->db_prefix}menu_description`, `{$this->db_prefix}menu_type`, `{$this->db_prefix}menu_type_description`;";
        $this->db->query("$sql");

        /* HOT: for migrate failed DB PREFIX "oc_" */
        $sql = "DROP TABLE IF EXISTS `oc_menu`, `oc_menu_description`, `oc_menu_type`, `oc_menu_type_description`;";
        $this->db->query("$sql");

        // enable menu permission
        $this->disableMenuPermission();
    }

    /* === end - default functions === */

    private function createNewMenuTable()
    {
        /* check if existed */
        if ($this->is_menu_table_existing()) {
            $this->log('menu already existed');
            return;
        }

        $this->createTable();
    }

    private function migrateMenu()
    {
        if ($this->is_menu_data_existing()) {
            $this->log('menu data already existed');
            return;
        }

        $group_menus = $this->db->query("SElECT * FROM `" . DB_PREFIX . "novaon_group_menu` gm LEFT JOIN `" . DB_PREFIX . "novaon_group_menu_description` gmd ON (`gm`.`id` = `gmd`.`group_menu_id`) WHERE `gmd`.`language_id` = 2"); // tiếng việt
        $group_menus = $group_menus->rows;
        foreach ($group_menus as $group_menu) {
            if (!isset($group_menu['group_menu_id'])) {
                continue;
            }
            if (!isset($group_menu['name'])) {
                continue;
            }
            $this->insertGroupMenu($group_menu['group_menu_id'], $group_menu['name']);
        }

        // tách ra để ưu tiến lấy id trước cho group menu

        foreach ($group_menus as $group_menu) {
            if (!isset($group_menu['group_menu_id']) || !$group_menu['group_menu_id']) {
                continue;
            }
            if (!isset($group_menu['name']) || !$group_menu['name']) {
                continue;
            }
            $this->migrateMenuDependency($group_menu['group_menu_id']);
        }
    }

    private function migrateMenuDependency($id)
    {
        if ($id == null) {
            return;
        }
        // get all menu item of main menu id
        $data = $this->db->query("SELECT * FROM `" . DB_PREFIX . "novaon_relation_table` WHERE `main_name` = 'group_menu' AND `main_id` = " . (int)$id);
        $data = $data->rows;
        // index : 0:id, 1 : main_name, 2: main_id, 3: child_name, 4: child_id, 5: type_id, 6: url, 7: redirect, 8: title, 9: weight_number
        // insert menu item menu table (menu)
        foreach ($data as $item) {
            $parent_id = isset($item['main_id']) ? $item['main_id'] : null;
            $type_id = isset($item['type_id']) ? $item['type_id'] : null;
            $type_id = isset(self::$TYPE_OLD_TO_NEW_MAPPING[$type_id]) ? self::$TYPE_OLD_TO_NEW_MAPPING[$type_id] : 4;
            if ($type_id == 4) {
                $target_value = isset($item['url']) ? $item['url'] : '';  // url
                if ($target_value == '') {
                    /* try again with child... */
                    $target_value = (isset($item['child_id']) && $item['child_id'] != null) ? $this->getTargetValue($item['child_id']) : null;
                }

                if (in_array($target_value, ['?route=common/home', '?route=common/shop', '?route=contact/contact', '?route=account/login', '?route=account/register'])) {
                    $type_id = 5;  // built-in page
                }
            } else {
                $target_value = (isset($item['child_id']) && $item['child_id'] != null) ? $this->getTargetValue($item['child_id']) : null;
            }
            $this->db->query("INSERT INTO `" . DB_PREFIX . "menu` (`parent_id`, `menu_type_id`, `target_value`) VALUES ('$parent_id', '$type_id', '$target_value')");
            $menu_id = $this->db->getLastId();
            // insert menu_description
            $this->insertMenuDescription($menu_id, $item['child_id'], $item['title']);

            // insert child menu_item
            $child_id = $item['child_id'];
            $child_data = $this->db->query("SELECT * FROM `" . DB_PREFIX . "novaon_relation_table` WHERE `main_name` = 'menu_item' AND `main_id` = '$child_id' AND `redirect` = 0");
            $child_data = $child_data->rows;
            // index : 0:id, 1 : main_name, 2: main_id, 3: child_name, 4: child_id, 5: type_id, 6: url, 7: redirect, 8: title, 9: weight_number
            // insert menu item menu table (menu)
            foreach ($child_data as $child) {
                $child_parent_id = $menu_id;
                $type_id = isset($child['type_id']) ? $child['type_id'] : null;
                $type_id = isset($TYPE_OLD_TO_NEW_MAPPING[$type_id]) ? self::$TYPE_OLD_TO_NEW_MAPPING[$type_id] : 4; // 4: url
                if ($type_id == 4) {
                    $target_value = isset($child['url']) ? $child['url'] : '';
                    if ($target_value == '') {
                        /* try again with child... */
                        $target_value = (isset($child['child_id']) && $child['child_id'] != null) ? $this->getTargetValue($child['child_id']) : null;
                    }

                    if (in_array($target_value, ['?route=common/home', '?route=common/shop', '?route=contact/contact', '?route=account/login', '?route=account/register'])) {
                        $type_id = 5;  // built-in page
                    }
                } else {
                    $target_value = isset($child['child_id']) ? $child['child_id'] : null;
                }
                $this->db->query("INSERT INTO `" . DB_PREFIX . "menu` (`parent_id`, `menu_type_id`, `target_value`) VALUES ('$child_parent_id', '$type_id', '$target_value')");
                $child_menu_id = $this->db->getLastId();
                // insert menu_description
                $this->insertMenuDescription($child_menu_id, $child['child_id'], $child['title']);

            }
        }
    }

    private function getTargetValue($child_id)
    {
        $data = $this->db->query("SELECT * FROM `" . DB_PREFIX . "novaon_relation_table` WHERE `main_name` = 'menu_item' AND `main_id` = '$child_id'");
        $data = $data->row;
        // index : 0:id, 1 : main_name, 2: main_id, 3: child_name, 4: child_id, 5: type_id, 6: url, 7: redirect, 8: title, 9: weight_number
        if (isset($data['child_id'])) {
            if ($data ['child_id'] == 0) {
                return $data['url'];
            }
            return $data['child_id'];
        }

        return null;
    }

    private function insertMenuDescription($menu_id, $menu_item_id, $name)
    {
        $data = $this->db->query("SELECT * FROM `" . DB_PREFIX . "novaon_menu_item_description` WHERE `menu_item_id` = '$menu_item_id' AND `language_id` = 2");  // tiếng việt
        $data = $data->row;
        // index : 0:id, 1 : language_id, 2: name, 3: menu_id_id
        if (isset($data['name'])) {
            $name = $data['name'];
        }

        $this->db->query("INSERT INTO  `" . DB_PREFIX . "menu_description` (`menu_id`, `name`, `language_id`) VALUES ('$menu_id', '$name', 1), ('$menu_id', '$name', 2)");
    }

    /**
     * @return bool
     */
    private function is_menu_table_existing()
    {
        $tables = $this->db->query("SHOW TABLES LIKE '" . DB_PREFIX . "menu'");

        return $tables->num_rows > 0;
    }

    private function is_menu_data_existing()
    {
        $data = $this->db->query("SELECT `menu_id` FROM `" . DB_PREFIX . "menu`");
        $rows = $data->rows;
        if (count($rows) > 0) {
            return true;
        }

        return false;
    }

    private function insertGroupMenu($group_menu_id, $name)
    {
        $this->db->query("INSERT INTO `{$this->db_prefix}menu` (`menu_id`, `parent_id`, `menu_type_id`, `target_value`) VALUES
            ('$group_menu_id', Null, Null, Null);");

        $this->db->query("INSERT INTO `{$this->db_prefix}menu_description` (`menu_id`, `name`, `language_id`) VALUES
            ('$group_menu_id', '$name', 1),
            ('$group_menu_id', '$name', 2);");
    }

    private function createTable()
    {
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "menu` (
              `menu_id` INT(11) NOT NULL AUTO_INCREMENT ,
              `parent_id` INT(11) NULL ,
              `menu_type_id` TINYINT(2) NULL ,
              `target_value` VARCHAR(256) NULL ,
              PRIMARY KEY (`menu_id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8");

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "menu_description` (
              `menu_id` INT(11) NOT NULL ,
              `name` VARCHAR(256) NOT NULL ,
              `language_id` INT(11) NOT NULL,
              PRIMARY KEY( `menu_id`, `language_id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8");

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "menu_type` (
              `menu_type_id` INT(11) NOT NULL AUTO_INCREMENT ,
              `value` VARCHAR(128) NOT NULL ,
              PRIMARY KEY (`menu_type_id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8");

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "menu_type_description` (
              `menu_type_id` INT(11) NOT NULL ,
              `language_id` INT(11) NOT NULL ,
              `description` VARCHAR(256) NOT NULL,
              PRIMARY KEY( `menu_type_id`, `language_id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8");

        $this->db->query("INSERT INTO `" . DB_PREFIX . "menu_type` (`menu_type_id`, `value`) VALUES
            ('1', 'category'),
            ('2', 'collection'),
            ('3', 'product'),
            ('4', 'url'),
            ('5', 'built-in page'),
            ('6', 'blog')");

        $this->db->query("INSERT INTO `" . DB_PREFIX . "menu_type_description` (`menu_type_id`, `language_id`, `description`) VALUES
            ('1', '1', 'Category'),
            ('1', '2', 'Loại sản phẩm'),
            ('2', '1', 'Collection'),
            ('2', '2', 'Bộ sưu tập'),
            ('3', '1', 'Product'),
            ('3', '2', 'Sản phẩm'),
            ('4', '1', 'Url'),
            ('4', '2', 'Đường dẫn'),
            ('5', '1', 'Built-in Page'),
            ('5', '2', 'Trang mặc định'),
            ('6', '1', 'Blog'),
            ('6', '2', 'Bài viết');");
    }

    private function enableMenuPermission()
    {
        $sql = "UPDATE `{$this->db_prefix}user_group` SET `permission` = '{\"access\":[\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/collection\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"catalog\\/warehouse\",\"common\\/cloudinary_upload\",\"common\\/column_left\",\"common\\/custom_column_left\",\"common\\/custom_header\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/forgotten_orig\",\"common\\/login_orig\",\"common\\/profile\",\"common\\/reset_orig\",\"common\\/security\",\"custom\\/group_menu\",\"custom\\/menu\",\"custom\\/menu_item\",\"custom\\/preference\",\"custom\\/theme\",\"custom\\/theme_develop\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"design\\/banner\",\"design\\/layout\",\"design\\/seo_url\",\"design\\/theme\",\"design\\/translation\",\"event\\/language\",\"event\\/statistics\",\"event\\/theme\",\"extension\\/analytics\\/google\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/module\\/account\",\"extension\\/module\\/age_restriction\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/customer_welcome\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/ebay_listing\",\"extension\\/module\\/featured\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/hello_world\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/modification_editor\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_braintree_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/recommendation\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/simple_blog\",\"extension\\/module\\/simple_blog\\/article\",\"extension\\/module\\/simple_blog\\/author\",\"extension\\/module\\/simple_blog\\/category\",\"extension\\/module\\/simple_blog\\/comment\",\"extension\\/module\\/simple_blog\\/install\",\"extension\\/module\\/simple_blog\\/report\",\"extension\\/module\\/simple_blog_category\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/so_categories\",\"extension\\/module\\/so_category_slider\",\"extension\\/module\\/so_deals\",\"extension\\/module\\/so_extra_slider\",\"extension\\/module\\/so_facebook_message\",\"extension\\/module\\/so_filter_shop_by\",\"extension\\/module\\/so_home_slider\",\"extension\\/module\\/so_html_content\",\"extension\\/module\\/so_latest_blog\",\"extension\\/module\\/so_listing_tabs\",\"extension\\/module\\/so_megamenu\",\"extension\\/module\\/so_newletter_custom_popup\",\"extension\\/module\\/so_onepagecheckout\",\"extension\\/module\\/so_page_builder\",\"extension\\/module\\/so_quickview\",\"extension\\/module\\/so_searchpro\",\"extension\\/module\\/so_sociallogin\",\"extension\\/module\\/so_tools\",\"extension\\/module\\/soconfig\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/module\\/theme_builder_config\",\"extension\\/openbay\\/amazon\",\"extension\\/openbay\\/amazon_listing\",\"extension\\/openbay\\/amazon_product\",\"extension\\/openbay\\/amazonus\",\"extension\\/openbay\\/amazonus_listing\",\"extension\\/openbay\\/amazonus_product\",\"extension\\/openbay\\/ebay\",\"extension\\/openbay\\/ebay_profile\",\"extension\\/openbay\\/ebay_template\",\"extension\\/openbay\\/etsy\",\"extension\\/openbay\\/etsy_product\",\"extension\\/openbay\\/etsy_shipping\",\"extension\\/openbay\\/etsy_shop\",\"extension\\/openbay\\/fba\",\"extension\\/payment\\/alipay\",\"extension\\/payment\\/alipay_cross\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_braintree\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/squareup\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/wechat_pay\",\"extension\\/payment\\/worldpay\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/citylink\",\"extension\\/shipping\\/ec_ship\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/theme\\/default\",\"extension\\/theme\\/dunght\",\"extension\\/theme\\/novaon\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/forgotten_orig\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"marketplace\\/openbay\",\"online_store\\/contents\",\"online_store\\/domain\",\"online_store\\/google_shopping\",\"report\\/online\",\"report\\/overview\",\"report\\/report\",\"report\\/statistics\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"section\\/banner\",\"section\\/best_sales_product\",\"section\\/best_views_product\",\"section\\/blog\",\"section\\/detail_product\",\"section\\/footer\",\"section\\/header\",\"section\\/hot_product\",\"section\\/list_product\",\"section\\/new_product\",\"section\\/partner\",\"section\\/preview\",\"section\\/sections\",\"section\\/slideshow\",\"section_blog\\/blog_category\",\"section_blog\\/blog_list\",\"section_blog\\/latest_blog\",\"section_blog\\/sections\",\"section_category\\/banner\",\"section_category\\/filter\",\"section_category\\/product_category\",\"section_category\\/product_list\",\"section_category\\/sections\",\"section_contact\\/contact\",\"section_contact\\/form\",\"section_contact\\/map\",\"section_contact\\/sections\",\"section_product_detail\\/related_product\",\"section_product_detail\\/sections\",\"setting\\/setting\",\"setting\\/store\",\"settings\\/account\",\"settings\\/delivery\",\"settings\\/general\",\"settings\\/payment\",\"settings\\/settings\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"super\\/dashboard\",\"theme\\/color\",\"theme\\/favicon\",\"theme\\/section_theme\",\"theme\\/social\",\"theme\\/text\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\"],\"modify\":[\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/collection\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"catalog\\/warehouse\",\"common\\/cloudinary_upload\",\"common\\/column_left\",\"common\\/custom_column_left\",\"common\\/custom_header\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/forgotten_orig\",\"common\\/login_orig\",\"common\\/profile\",\"common\\/reset_orig\",\"common\\/security\",\"custom\\/group_menu\",\"custom\\/menu\",\"custom\\/menu_item\",\"custom\\/preference\",\"custom\\/theme\",\"custom\\/theme_develop\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"design\\/banner\",\"design\\/layout\",\"design\\/seo_url\",\"design\\/theme\",\"design\\/translation\",\"event\\/language\",\"event\\/statistics\",\"event\\/theme\",\"extension\\/analytics\\/google\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/module\\/account\",\"extension\\/module\\/age_restriction\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/customer_welcome\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/ebay_listing\",\"extension\\/module\\/featured\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/hello_world\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/modification_editor\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_braintree_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/recommendation\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/simple_blog\",\"extension\\/module\\/simple_blog\\/article\",\"extension\\/module\\/simple_blog\\/author\",\"extension\\/module\\/simple_blog\\/category\",\"extension\\/module\\/simple_blog\\/comment\",\"extension\\/module\\/simple_blog\\/install\",\"extension\\/module\\/simple_blog\\/report\",\"extension\\/module\\/simple_blog_category\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/so_categories\",\"extension\\/module\\/so_category_slider\",\"extension\\/module\\/so_deals\",\"extension\\/module\\/so_extra_slider\",\"extension\\/module\\/so_facebook_message\",\"extension\\/module\\/so_filter_shop_by\",\"extension\\/module\\/so_home_slider\",\"extension\\/module\\/so_html_content\",\"extension\\/module\\/so_latest_blog\",\"extension\\/module\\/so_listing_tabs\",\"extension\\/module\\/so_megamenu\",\"extension\\/module\\/so_newletter_custom_popup\",\"extension\\/module\\/so_onepagecheckout\",\"extension\\/module\\/so_page_builder\",\"extension\\/module\\/so_quickview\",\"extension\\/module\\/so_searchpro\",\"extension\\/module\\/so_sociallogin\",\"extension\\/module\\/so_tools\",\"extension\\/module\\/soconfig\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/module\\/theme_builder_config\",\"extension\\/openbay\\/amazon\",\"extension\\/openbay\\/amazon_listing\",\"extension\\/openbay\\/amazon_product\",\"extension\\/openbay\\/amazonus\",\"extension\\/openbay\\/amazonus_listing\",\"extension\\/openbay\\/amazonus_product\",\"extension\\/openbay\\/ebay\",\"extension\\/openbay\\/ebay_profile\",\"extension\\/openbay\\/ebay_template\",\"extension\\/openbay\\/etsy\",\"extension\\/openbay\\/etsy_product\",\"extension\\/openbay\\/etsy_shipping\",\"extension\\/openbay\\/etsy_shop\",\"extension\\/openbay\\/fba\",\"extension\\/payment\\/alipay\",\"extension\\/payment\\/alipay_cross\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_braintree\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/squareup\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/wechat_pay\",\"extension\\/payment\\/worldpay\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/citylink\",\"extension\\/shipping\\/ec_ship\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/theme\\/default\",\"extension\\/theme\\/dunght\",\"extension\\/theme\\/novaon\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/forgotten_orig\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"marketplace\\/openbay\",\"online_store\\/contents\",\"online_store\\/domain\",\"online_store\\/google_shopping\",\"report\\/online\",\"report\\/overview\",\"report\\/report\",\"report\\/statistics\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"section\\/banner\",\"section\\/best_sales_product\",\"section\\/best_views_product\",\"section\\/blog\",\"section\\/detail_product\",\"section\\/footer\",\"section\\/header\",\"section\\/hot_product\",\"section\\/list_product\",\"section\\/new_product\",\"section\\/partner\",\"section\\/preview\",\"section\\/sections\",\"section\\/slideshow\",\"section_blog\\/blog_category\",\"section_blog\\/blog_list\",\"section_blog\\/latest_blog\",\"section_blog\\/sections\",\"section_category\\/banner\",\"section_category\\/filter\",\"section_category\\/product_category\",\"section_category\\/product_list\",\"section_category\\/sections\",\"section_contact\\/contact\",\"section_contact\\/form\",\"section_contact\\/map\",\"section_contact\\/sections\",\"section_product_detail\\/related_product\",\"section_product_detail\\/sections\",\"setting\\/setting\",\"setting\\/store\",\"settings\\/account\",\"settings\\/delivery\",\"settings\\/general\",\"settings\\/payment\",\"settings\\/settings\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"super\\/dashboard\",\"theme\\/color\",\"theme\\/favicon\",\"theme\\/section_theme\",\"theme\\/social\",\"theme\\/text\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\"]}' 
                WHERE `user_group_id` = 1 AND `name` = 'Administrator';";
        $this->db->query($sql);
    }

    private function disableMenuPermission()
    {
        $sql = "UPDATE `{$this->db_prefix}user_group` SET `permission` = '{\"access\":[\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/collection\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"catalog\\/warehouse\",\"common\\/cloudinary_upload\",\"common\\/column_left\",\"common\\/custom_column_left\",\"common\\/custom_header\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/forgotten_orig\",\"common\\/login_orig\",\"common\\/profile\",\"common\\/reset_orig\",\"common\\/security\",\"custom\\/group_menu\",\"custom\\/menu_item\",\"custom\\/preference\",\"custom\\/theme\",\"custom\\/theme_develop\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"design\\/banner\",\"design\\/layout\",\"design\\/seo_url\",\"design\\/theme\",\"design\\/translation\",\"event\\/language\",\"event\\/statistics\",\"event\\/theme\",\"extension\\/analytics\\/google\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/module\\/account\",\"extension\\/module\\/age_restriction\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/customer_welcome\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/ebay_listing\",\"extension\\/module\\/featured\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/hello_world\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/modification_editor\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_braintree_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/recommendation\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/simple_blog\",\"extension\\/module\\/simple_blog\\/article\",\"extension\\/module\\/simple_blog\\/author\",\"extension\\/module\\/simple_blog\\/category\",\"extension\\/module\\/simple_blog\\/comment\",\"extension\\/module\\/simple_blog\\/install\",\"extension\\/module\\/simple_blog\\/report\",\"extension\\/module\\/simple_blog_category\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/so_categories\",\"extension\\/module\\/so_category_slider\",\"extension\\/module\\/so_deals\",\"extension\\/module\\/so_extra_slider\",\"extension\\/module\\/so_facebook_message\",\"extension\\/module\\/so_filter_shop_by\",\"extension\\/module\\/so_home_slider\",\"extension\\/module\\/so_html_content\",\"extension\\/module\\/so_latest_blog\",\"extension\\/module\\/so_listing_tabs\",\"extension\\/module\\/so_megamenu\",\"extension\\/module\\/so_newletter_custom_popup\",\"extension\\/module\\/so_onepagecheckout\",\"extension\\/module\\/so_page_builder\",\"extension\\/module\\/so_quickview\",\"extension\\/module\\/so_searchpro\",\"extension\\/module\\/so_sociallogin\",\"extension\\/module\\/so_tools\",\"extension\\/module\\/soconfig\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/module\\/theme_builder_config\",\"extension\\/openbay\\/amazon\",\"extension\\/openbay\\/amazon_listing\",\"extension\\/openbay\\/amazon_product\",\"extension\\/openbay\\/amazonus\",\"extension\\/openbay\\/amazonus_listing\",\"extension\\/openbay\\/amazonus_product\",\"extension\\/openbay\\/ebay\",\"extension\\/openbay\\/ebay_profile\",\"extension\\/openbay\\/ebay_template\",\"extension\\/openbay\\/etsy\",\"extension\\/openbay\\/etsy_product\",\"extension\\/openbay\\/etsy_shipping\",\"extension\\/openbay\\/etsy_shop\",\"extension\\/openbay\\/fba\",\"extension\\/payment\\/alipay\",\"extension\\/payment\\/alipay_cross\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_braintree\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/squareup\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/wechat_pay\",\"extension\\/payment\\/worldpay\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/citylink\",\"extension\\/shipping\\/ec_ship\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/theme\\/default\",\"extension\\/theme\\/dunght\",\"extension\\/theme\\/novaon\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/forgotten_orig\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"marketplace\\/openbay\",\"online_store\\/contents\",\"online_store\\/domain\",\"online_store\\/google_shopping\",\"report\\/online\",\"report\\/overview\",\"report\\/report\",\"report\\/statistics\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"section\\/banner\",\"section\\/best_sales_product\",\"section\\/best_views_product\",\"section\\/blog\",\"section\\/detail_product\",\"section\\/footer\",\"section\\/header\",\"section\\/hot_product\",\"section\\/list_product\",\"section\\/new_product\",\"section\\/partner\",\"section\\/preview\",\"section\\/sections\",\"section\\/slideshow\",\"section_blog\\/blog_category\",\"section_blog\\/blog_list\",\"section_blog\\/latest_blog\",\"section_blog\\/sections\",\"section_category\\/banner\",\"section_category\\/filter\",\"section_category\\/product_category\",\"section_category\\/product_list\",\"section_category\\/sections\",\"section_contact\\/contact\",\"section_contact\\/form\",\"section_contact\\/map\",\"section_contact\\/sections\",\"section_product_detail\\/related_product\",\"section_product_detail\\/sections\",\"setting\\/setting\",\"setting\\/store\",\"settings\\/account\",\"settings\\/delivery\",\"settings\\/general\",\"settings\\/payment\",\"settings\\/settings\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"super\\/dashboard\",\"theme\\/color\",\"theme\\/favicon\",\"theme\\/section_theme\",\"theme\\/social\",\"theme\\/text\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\"],\"modify\":[\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/collection\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"catalog\\/warehouse\",\"common\\/cloudinary_upload\",\"common\\/column_left\",\"common\\/custom_column_left\",\"common\\/custom_header\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/forgotten_orig\",\"common\\/login_orig\",\"common\\/profile\",\"common\\/reset_orig\",\"common\\/security\",\"custom\\/group_menu\",\"custom\\/menu_item\",\"custom\\/preference\",\"custom\\/theme\",\"custom\\/theme_develop\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"design\\/banner\",\"design\\/layout\",\"design\\/seo_url\",\"design\\/theme\",\"design\\/translation\",\"event\\/language\",\"event\\/statistics\",\"event\\/theme\",\"extension\\/analytics\\/google\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/module\\/account\",\"extension\\/module\\/age_restriction\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/customer_welcome\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/ebay_listing\",\"extension\\/module\\/featured\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/hello_world\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/modification_editor\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_braintree_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/recommendation\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/simple_blog\",\"extension\\/module\\/simple_blog\\/article\",\"extension\\/module\\/simple_blog\\/author\",\"extension\\/module\\/simple_blog\\/category\",\"extension\\/module\\/simple_blog\\/comment\",\"extension\\/module\\/simple_blog\\/install\",\"extension\\/module\\/simple_blog\\/report\",\"extension\\/module\\/simple_blog_category\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/so_categories\",\"extension\\/module\\/so_category_slider\",\"extension\\/module\\/so_deals\",\"extension\\/module\\/so_extra_slider\",\"extension\\/module\\/so_facebook_message\",\"extension\\/module\\/so_filter_shop_by\",\"extension\\/module\\/so_home_slider\",\"extension\\/module\\/so_html_content\",\"extension\\/module\\/so_latest_blog\",\"extension\\/module\\/so_listing_tabs\",\"extension\\/module\\/so_megamenu\",\"extension\\/module\\/so_newletter_custom_popup\",\"extension\\/module\\/so_onepagecheckout\",\"extension\\/module\\/so_page_builder\",\"extension\\/module\\/so_quickview\",\"extension\\/module\\/so_searchpro\",\"extension\\/module\\/so_sociallogin\",\"extension\\/module\\/so_tools\",\"extension\\/module\\/soconfig\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/module\\/theme_builder_config\",\"extension\\/openbay\\/amazon\",\"extension\\/openbay\\/amazon_listing\",\"extension\\/openbay\\/amazon_product\",\"extension\\/openbay\\/amazonus\",\"extension\\/openbay\\/amazonus_listing\",\"extension\\/openbay\\/amazonus_product\",\"extension\\/openbay\\/ebay\",\"extension\\/openbay\\/ebay_profile\",\"extension\\/openbay\\/ebay_template\",\"extension\\/openbay\\/etsy\",\"extension\\/openbay\\/etsy_product\",\"extension\\/openbay\\/etsy_shipping\",\"extension\\/openbay\\/etsy_shop\",\"extension\\/openbay\\/fba\",\"extension\\/payment\\/alipay\",\"extension\\/payment\\/alipay_cross\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_braintree\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/squareup\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/wechat_pay\",\"extension\\/payment\\/worldpay\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/citylink\",\"extension\\/shipping\\/ec_ship\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/theme\\/default\",\"extension\\/theme\\/dunght\",\"extension\\/theme\\/novaon\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/forgotten_orig\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"marketplace\\/openbay\",\"online_store\\/contents\",\"online_store\\/domain\",\"online_store\\/google_shopping\",\"report\\/online\",\"report\\/overview\",\"report\\/report\",\"report\\/statistics\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"section\\/banner\",\"section\\/best_sales_product\",\"section\\/best_views_product\",\"section\\/blog\",\"section\\/detail_product\",\"section\\/footer\",\"section\\/header\",\"section\\/hot_product\",\"section\\/list_product\",\"section\\/new_product\",\"section\\/partner\",\"section\\/preview\",\"section\\/sections\",\"section\\/slideshow\",\"section_blog\\/blog_category\",\"section_blog\\/blog_list\",\"section_blog\\/latest_blog\",\"section_blog\\/sections\",\"section_category\\/banner\",\"section_category\\/filter\",\"section_category\\/product_category\",\"section_category\\/product_list\",\"section_category\\/sections\",\"section_contact\\/contact\",\"section_contact\\/form\",\"section_contact\\/map\",\"section_contact\\/sections\",\"section_product_detail\\/related_product\",\"section_product_detail\\/sections\",\"setting\\/setting\",\"setting\\/store\",\"settings\\/account\",\"settings\\/delivery\",\"settings\\/general\",\"settings\\/payment\",\"settings\\/settings\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"super\\/dashboard\",\"theme\\/color\",\"theme\\/favicon\",\"theme\\/section_theme\",\"theme\\/social\",\"theme\\/text\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\"]}' 
                WHERE `user_group_id` = 1 AND `name` = 'Administrator';";
        $this->db->query($sql);
    }
}