<?php

namespace Migration;

class migration_20200816_add_column_demo_blog_table extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200816_add_column_demo_blog_table';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('add column demo to table blog');
        try {
            $this->addColumnDemoToBlogTable();

        } catch (\Exception $e) {
            $this->log('add column demo to table blog got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back column payment_status table shopee_order version');
    }

    protected function addColumnDemoToBlogTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}blog` ADD `demo` TINYINT(1) NOT NULL DEFAULT '0' AFTER `status`;";

        $this->db->query($sql);
    }
}