<?php

namespace Migration;

class migration_20191029_default_delivery_method extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20191029_default_delivery_method';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Init insert default delivery method');

        $delivery_methods = $this->get_delivery_methods();
        /* check if delivery method existed */
        $is_existed = false;
        foreach ($delivery_methods as &$delivery_method) {
            if (isset($delivery_method['id']) && $delivery_method['id'] == -1) {
                $is_existed = true;
                break;
            }
        }

        if (!$is_existed) {
            $default_method['id'] = '-1';
            $default_method['name'] = 'Giao hàng tiêu chuẩn';
            $default_method['status'] = '1';
            $default_method['fee_region'] = '40000';
            array_unshift($delivery_methods, $default_method);
            $this->edit_setting_value('delivery', 'delivery_methods', $delivery_methods);
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Roll back default delivery method');
        $delivery_methods = $this->get_delivery_methods();
        /* check if delivery method existed */
        $is_existed = false;
        foreach ($delivery_methods as $idx => $delivery_method) {
            if (isset($delivery_method['id']) && $delivery_method['id'] == -1) {
                array_splice($delivery_methods, $idx, 1);
                $is_existed = true;
                break;
            }
        }

        if ($is_existed) {
            $this->edit_setting_value('delivery', 'delivery_methods', $delivery_methods);
        }
    }

    /* get all delivery methods */
    private function get_delivery_methods()
    {
        $delivery_methods = $this->get_setting_value('delivery_methods');
        $delivery_methods = json_decode($delivery_methods, true);
        if (!is_array($delivery_methods)) {
            $delivery_methods = [];
        }

        return $delivery_methods;
    }

    /* get setting value */
    private function get_setting_value($key, $store_id = 0)
    {
        $query = $this->db->query("SELECT value FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `key` = '" . $this->db->escape($key) . "'");

        if ($query->num_rows) {
            return $query->row['value'];
        } else {
            return null;
        }
    }

    /* set setting value */
    public function edit_setting_value($code = '', $key = '', $value = '', $store_id = 0)
    {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE store_id = '" . (int)$store_id . "' AND `code` = '" . $this->db->escape($code) . "' AND `key` = '" . $this->db->escape($key) . "'");

        if (!is_array($value)) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
        } else {
            $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(json_encode($value, true)) . "', serialized = '1'");
        }
    }
}