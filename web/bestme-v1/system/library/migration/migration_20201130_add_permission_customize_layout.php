<?php


namespace Migration;


class migration_20201130_add_permission_customize_layout extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20201130_add_permission_customize_layout';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Add permission for section/customize_layout');
        // permission for admin user
        try {
            $this->addPermissionForAdminUser([
                'section/customize_layout'
            ]);
        } catch (\Exception $e) {
            $this->log('addPermissionForAdminUser (section/customize_layout) got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back add permission section/customize_layout');
    }
}