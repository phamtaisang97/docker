<?php

namespace Migration;

class migration_20201002_add_video_to_blog extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20201002_add_video_to_blog';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate alter table blog description');
        try {
            $this->alterColumnBlogDescription();
        } catch (\Exception $e) {
            $this->log('alter table blog description got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back ....');
    }

    protected function alterColumnBlogDescription()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}blog_description` 
                    ADD `type` VARCHAR (255) NOT NULL DEFAULT 'image' AFTER `alt`,
	                ADD `video_url` TEXT NULL AFTER `type`;";

        $this->db->query($sql);
    }
}