<?php

namespace Migration;

class migration_20191225_builder_product_per_row_mobile extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20191225_builder_product_per_row_mobile';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('migrate add Builder Product Per Row Mobile');

        try {
            $this->migrateBuilderProductPerRowMobile();
        } catch (\Exception $e) {
            $this->log('migrateBuilderProductPerRowMobile got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('migrate remove Builder Product Per Row Mobile');

        try {
            $this->removeBuilderProductPerRowMobile();
        } catch (\Exception $e) {
            $this->log('removeBuilderProductPerRowMobile got error: ' . $e->getMessage());
        }
    }

    private function migrateBuilderProductPerRowMobile()
    {
        // config_section_best_sales_product
        $sql = "SELECT tbc.* FROM `{$this->db_prefix}theme_builder_config` tbc 
                WHERE tbc.`key` = 'config_section_best_sales_product'";
        $query = $this->db->query($sql);
        if ($query->rows && is_array($query->rows)) {
            foreach ($query->rows as $config) {
                if (!isset($config['config_theme']) ||
                    !isset($config['config'])
                ) {
                    continue;
                }

                $config_section_best_sales_product = json_decode($config['config'], true);
                if (!isset($config_section_best_sales_product['display']) ||
                    !is_array($config_section_best_sales_product['display']) ||
                    array_key_exists('grid_mobile', $config_section_best_sales_product['display'])
                ) {
                    continue;
                }

                $config_section_best_sales_product['display']['grid_mobile'] = [
                    "quantity" => 2
                ];

                $sqlUpdate = "UPDATE `{$this->db_prefix}theme_builder_config` 
                      SET `config`= '" . json_encode($config_section_best_sales_product, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG) . "' 
                      WHERE `key` = 'config_section_best_sales_product' 
                      AND `config_theme` = '" . $config['config_theme'] . "'";
                $this->db->query($sqlUpdate);
            }
        }

        // config_section_new_product
        $sql = "SELECT tbc.* FROM `{$this->db_prefix}theme_builder_config` tbc 
                WHERE tbc.`key` = 'config_section_new_product'";
        $query = $this->db->query($sql);
        if ($query->rows && is_array($query->rows)) {
            foreach ($query->rows as $config) {
                if (!isset($config['config_theme']) ||
                    !isset($config['config'])
                ) {
                    continue;
                }

                $config_section_new_product = json_decode($config['config'], true);
                if (!isset($config_section_new_product['display']) ||
                    !is_array($config_section_new_product['display']) ||
                    array_key_exists('grid_mobile', $config_section_new_product['display'])
                ) {
                    continue;
                }

                $config_section_new_product['display']['grid_mobile'] = [
                    "quantity" => 2
                ];

                $sqlUpdate = "UPDATE `{$this->db_prefix}theme_builder_config` 
                          SET `config`= '" . json_encode($config_section_new_product, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG) . "' 
                          WHERE `key` = 'config_section_new_product' 
                          AND `config_theme` = '" . $config['config_theme'] . "'";
                $this->db->query($sqlUpdate);
            }
        }

        // config_section_hot_product
        $sql = "SELECT tbc.* FROM `{$this->db_prefix}theme_builder_config` tbc 
                WHERE tbc.`key` = 'config_section_hot_product'";
        $query = $this->db->query($sql);
        if ($query->rows && is_array($query->rows)) {
            foreach ($query->rows as $config) {
                if (!isset($config['config_theme']) ||
                    !isset($config['config'])
                ) {
                    continue;
                }

                $config_section_hot_product = json_decode($config['config'], true);
                if (!isset($config_section_hot_product['display']) ||
                    !is_array($config_section_hot_product['display']) ||
                    array_key_exists('grid_mobile', $config_section_hot_product['display'])
                ) {
                    continue;
                }

                $config_section_hot_product['display']['grid_mobile'] = [
                    "quantity" => 2
                ];

                $sqlUpdate = "UPDATE `{$this->db_prefix}theme_builder_config` 
                          SET `config`= '" . json_encode($config_section_hot_product, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG) . "' 
                          WHERE `key` = 'config_section_hot_product' 
                          AND `config_theme` = '" . $config['config_theme'] . "'";
                $this->db->query($sqlUpdate);
            }
        }

        // for additional product block lists: config_section_product_groups
        $sql = "SELECT tbc.* FROM `{$this->db_prefix}theme_builder_config` tbc 
                WHERE tbc.`key` = 'config_section_product_groups'";
        $query = $this->db->query($sql);
        if ($query->rows && is_array($query->rows)) {
            foreach ($query->rows as $config) {
                if (!isset($config['config_theme']) ||
                    !isset($config['config'])
                ) {
                    continue;
                }

                $config_section_product_groups = json_decode($config['config'], true);
                if (!isset($config_section_product_groups['list']) ||
                    !is_array($config_section_product_groups['list'])
                ) {
                    continue;
                }

                $found = false;
                foreach ($config_section_product_groups['list'] as &$section_product_group) {
                    if (!isset($section_product_group['display']) ||
                        !is_array($section_product_group['display']) ||
                        array_key_exists('grid_mobile', $section_product_group['display'])
                    ) {
                        continue;
                    }

                    $section_product_group['display']['grid_mobile'] = [
                        "quantity" => 2
                    ];

                    $found = true;
                }
                unset($section_product_group);

                if ($found) {
                    $sqlUpdate = "UPDATE `{$this->db_prefix}theme_builder_config` 
                              SET `config`= '" . json_encode($config_section_product_groups, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG) . "' 
                              WHERE `key` = 'config_section_product_groups' 
                              AND `config_theme` = '" . $config['config_theme'] . "'";
                    $this->db->query($sqlUpdate);
                }
            }
        }
    }

    /**
     * removeTextOrderSuccess
     */
    private function removeBuilderProductPerRowMobile()
    {
        // no need
    }
}
