<?php

namespace Migration;

class migration_20191024_report_order_from_shop extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20191024_report_order_from_shop';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('migrate add missing report_order, report_order_product for orders come from shop');

        // *_report_order
        try {
            $this->migrateReportOrderForOrderFromShop();
        } catch (\Exception $e) {
            $this->log('migrateReportOrderForOrderFromShop got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        // NO NEED
    }

    private function migrateReportOrderForOrderFromShop()
    {
        /* update report order */
        $this->log('migrateReportOrderForOrderFromShop: updating report order... ');
        $sql = "SELECT o.*, o.`date_added` as `rp_time` FROM `{$this->db_prefix}order` o 
                WHERE o.`source` = 'shop'";
        $query = $this->db->query($sql);
        $orders = $query->rows;

        $migrated_order = [];
        foreach ($orders as $order) {
            if (!is_array($order)) {
                continue;
            }

            if (!array_key_exists('order_id', $order) || !array_key_exists('rp_time', $order)) {
                continue;
            }

            if (in_array($order['order_id'], $migrated_order)) {
                continue;
            }

            // delete if existing before to avoid duplicate
            $sqlDelete = "DELETE FROM `{$this->db_prefix}report_order` WHERE order_id = '" . $order['order_id'] . "'";
            $this->db->query($sqlDelete);

            // mark as migrated by order id
            $migrated_order[] = $order['order_id'];

            // then insert
            $report_time = date_create_from_format('Y-m-d H:i:s', $order['rp_time']);
            $report_time->setTime($report_time->format('H'), 0, 0);
            $rp_time = $report_time->format('Y-m-d H:i:s');
            $rp_date = $report_time->format('Y-m-d');
            $order_id = $order['order_id'];
            $customer_id = $order['customer_id'];
            $total = $order['total'];
            $order_status_id = $order['order_status_id'];

            $sqlInsert = "INSERT INTO `{$this->db_prefix}report_order` (`report_time`, `report_date`, `order_id`, `customer_id`, `total_amount`, `order_status`)
                          VALUES ('$rp_time', '$rp_date', '$order_id', '$customer_id', '$total', '$order_status_id')";

            $this->db->query($sqlInsert);
        }

        /* update report product */
        $this->log('migrateReportOrderForOrderFromShop: updating report product... ');
        $sql = "SELECT o.*, o.`date_added` as `rp_time`, op.`product_id` as `op_product_id`, 
                op.`quantity` as `op_quantity`, op.`quantity` as `op_quantity`,
                op.`price` as `op_price`, op.`total` as `op_total`,
                op.`product_version_id` as `op_product_version_id` FROM `{$this->db_prefix}order` o 
                LEFT JOIN `{$this->db_prefix}order_product` op ON (o.`order_id` = op.`order_id`) 
                WHERE o.`source` = 'shop'";
        $query = $this->db->query($sql);
        $orders = $query->rows;

        // delete if existing before to avoid duplicate
        foreach ($orders as $order) {
            if (!is_array($order)) {
                continue;
            }

            if (!array_key_exists('order_id', $order) || !array_key_exists('rp_time', $order)) {
                continue;
            }

            $sqlDelete = "DELETE FROM `{$this->db_prefix}report_product` WHERE order_id = '" . $order['order_id'] . "'";
            $this->db->query($sqlDelete);
        }

        // then insert
        foreach ($orders as $order) {
            if (!is_array($order)) {
                continue;
            }

            if (!array_key_exists('order_id', $order) || !array_key_exists('rp_time', $order)) {
                continue;
            }

            $report_time = date_create_from_format('Y-m-d H:i:s', $order['rp_time']);
            $report_time->setTime($report_time->format('H'), 0, 0);
            $rp_time = $report_time->format('Y-m-d H:i:s');
            $rp_date = $report_time->format('Y-m-d');
            $order_id = $order['order_id'];
            $product_id = $order['op_product_id'];
            $product_version_id = $order['op_product_version_id'];
            $price = $order['op_price'];
            $total = $order['op_total'];
            $quantity = $order['op_quantity'];

            $sqlInsert = "INSERT INTO `{$this->db_prefix}report_product` (`report_time`, `report_date`, `order_id`, `product_id`, `product_version_id`, `quantity`, `price`, `total_amount`)
                          VALUES ('$rp_time', '$rp_date', '$order_id', '$product_id', '$product_version_id', '$quantity', '$price', '$total')";

            $this->db->query($sqlInsert);
        }
    }
}
