<?php


namespace Migration;


class migration_20220701_discount_new extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20220701_discount_new';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('remove discount table old');
        try {
            $this->removeDiscountOld();
        } catch (\Exception $e) {
            $this->log('remove discount table old got error: ' . $e->getMessage());
        }

        $this->log('migrate discount table');
        try {
            $this->migrateDiscountTable();
        } catch (\Exception $e) {
            $this->log('migrate discount table got error: ' . $e->getMessage());
        }

        $this->log('migrate discount_history table');
        try {
            $this->migrateDiscountHistoryTable();
        } catch (\Exception $e) {
            $this->log('migrate discount_history table got error: ' . $e->getMessage());
        }

        $this->log('migrate discount_product table');
        try {
            $this->migrateDiscountProductTable();
        } catch (\Exception $e) {
            $this->log('migrate discount_product table got error: ' . $e->getMessage());
        }

        $this->log('migrate alter table order_product');
        try {
            $this->migrateDiscountProductTable();
        } catch (\Exception $e) {
            $this->log('migrate alter table order_product got error: ' . $e->getMessage());
        }

        $this->log('migrate alter table');
        try {
            $this->alterTable();
        } catch (\Exception $e) {
            $this->log('migrate alter table got error: ' . $e->getMessage());
        }
    }

    private function removeDiscountOld()
    {
        $sql = "DROP TABLE `{$this->db_prefix}discount`;";
        $this->db->query($sql);
    }

    private function migrateDiscountTable()
    {
        $sql = "CREATE TABLE `{$this->db_prefix}discount` (
                    `discount_id` INT(11) NOT NULL AUTO_INCREMENT,
                    `name` VARCHAR(255) NULL,
                    `description` TEXT NULL,
                    `date_started` DATETIME NULL DEFAULT NULL,
                    `date_ended` DATETIME NULL DEFAULT NULL,
                    `status` TINYINT(4) NULL DEFAULT '1',
                    `date_modify` DATETIME NULL DEFAULT NULL,
                    PRIMARY KEY (`discount_id`)
                )
                COLLATE='utf8_unicode_ci'
                ;";
        $this->db->query($sql);
    }

    private function migrateDiscountHistoryTable()
    {
        $sql = "CREATE TABLE `{$this->db_prefix}discount_history` (
                    `discount_history_id` INT(11) NOT NULL AUTO_INCREMENT,
                    `discount_id` INT(11) NOT NULL,
                    `user_id` INT(11) NOT NULL,
                    `date_added` DATETIME NOT NULL,
                    `action` VARCHAR(255) NULL DEFAULT NULL,
                    `reson` TEXT NULL DEFAULT NULL,
                    `date_started` DATETIME NOT NULL,
                    `date_ended` DATETIME NOT NULL,
                    PRIMARY KEY (`discount_history_id`)
                )
                COLLATE='utf8_unicode_ci'
                ;";
        $this->db->query($sql);
    }

    private function migrateDiscountProductTable()
    {
        $sql = "CREATE TABLE `{$this->db_prefix}discount_product` (
                    `discount_product_id` INT(11) NOT NULL AUTO_INCREMENT,
                    `product_id` INT(11) NOT NULL,
                    `product_version_id` INT(11) NOT NULL,
                    `discount_id` INT(11) NOT NULL,
                    `discount_type` TINYINT(4) NULL DEFAULT '0' COMMENT '0: gia tien, 1: phan tram',
                    `discount_value` DECIMAL(15,4) NULL DEFAULT NULL,
                    `price_discount` DECIMAL(15,4) NULL DEFAULT NULL,
                    PRIMARY KEY (`discount_product_id`)
                )
                COLLATE='utf8_unicode_ci'
                ;";
        $this->db->query($sql);
    }

    private function alterTable()
    {
        $sql ="ALTER TABLE `{$this->db_prefix}order_product`
	                ADD COLUMN `discount_id` INT(11) NULL DEFAULT NULL AFTER `reward`;";
        $this->db->query($sql);

        $sql = "ALTER TABLE `{$this->db_prefix}discount_product`
                    ADD COLUMN `limit` TINYINT(3) NULL DEFAULT '0' COMMENT '0: khong gioi han, 1: gioi han' AFTER `price_discount`,
                    ADD COLUMN `status` TINYINT(3) NULL DEFAULT '1' COMMENT '0: tat, 1 bat' AFTER `limit`;";
        $this->db->query($sql);

        $sql = "ALTER TABLE `{$this->db_prefix}discount_product`
                    DROP COLUMN `discount_type`,
                    DROP COLUMN `discount_value`;";
        $this->db->query($sql);

        $sql = "ALTER TABLE `{$this->db_prefix}discount`
                    ADD COLUMN `times_used` INT(11) NULL DEFAULT NULL AFTER `date_modify`;";
        $this->db->query($sql);

        $sql = "ALTER TABLE `{$this->db_prefix}order_product`
                    CHANGE COLUMN `tax` `tax` DECIMAL(15,4) NOT NULL DEFAULT '0.0000' AFTER `total`,
                    ADD COLUMN `listed_price` DECIMAL(18,4) NULL DEFAULT '0.0000' AFTER `discount_id`;";
        $this->db->query($sql);

        $sql = "ALTER TABLE `{$this->db_prefix}discount` ADD `deleted` TINYINT NOT NULL DEFAULT '0' AFTER `status`;";
        $this->db->query($sql);
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back migration discount');
    }
}