<?php

namespace Migration;

class migration_20190930_increase_decimal_order extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20190930_increase_decimal_order';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Init alter decimal column total');

        // Alter decimal column total for oc_order table
        $sql = "ALTER TABLE `{$this->db_prefix}order` MODIFY `total` DECIMAL(18,4)";
        $this->db->query($sql);

        // Alter decimal column total for oc_order_product table
        $sql = "ALTER TABLE `{$this->db_prefix}order_product` MODIFY `total` DECIMAL(18,4)";
        $this->db->query($sql);
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Roll back decimal column');

        // Alter default decimal column total for oc_order table
        $sql = "ALTER TABLE `{$this->db_prefix}order` MODIFY `total` DECIMAL(15,4)";
        $this->db->query($sql);

        // Alter default decimal column total for oc_order_product table
        $sql = "ALTER TABLE `{$this->db_prefix}order_product` MODIFY `total` DECIMAL(15,4)";
        $this->db->query($sql);
    }
}