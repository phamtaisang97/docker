<?php

namespace Migration;

class migration_20200420_add_table_order_utm extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200420_add_table_order_utm';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will create table order_utm');

        try {
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}order_utm` (
                      `order_utm_id` int(11) NOT NULL AUTO_INCREMENT,
                      `order_id` int(11) DEFAULT NULL,
                      `code` varchar(256) NOT NULL,
                      `key` varchar(256) NOT NULL,
                      `value` text NOT NULL,
                      `date_added` datetime NOT NULL,
                      `date_modified` datetime NOT NULL,
                       PRIMARY KEY (`order_utm_id`)
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
            $this->db->query($sql);
        } catch (\Exception $e) {
            $this->log('migrate create table order_utm got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will drop table order_utm');

        try {
            $sql = "DROP TABLE IF EXISTS `{$this->db_prefix}order_utm`;";
            $this->db->query($sql);
        } catch (\Exception $e) {
            $this->log('The drop table order_utm got error: ' . $e->getMessage());
        }
    }
}