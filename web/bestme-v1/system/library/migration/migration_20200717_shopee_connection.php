<?php

namespace Migration;

class migration_20200717_shopee_connection extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200717_shopee_connection';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate create table for shopee_connection version');
        try {
            $this->migrateCreateTablesForAdvanceStoreManager();

        } catch (\Exception $e) {
            $this->log('crate table for shopee_connection version got error: ' . $e->getMessage());
        }

        try {
            $this->migrateDataTableShopeeShopConfig();
        } catch (\Exception $e) {
            $this->log('migrate data table shopee_shop_config got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back remove some table of shopee_connection version');
    }

    protected function migrateCreateTablesForAdvanceStoreManager()
    {
        // shopee_order
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}shopee_order` ( 
                  `id` INT(11) NOT NULL AUTO_INCREMENT , 
                  `code` VARCHAR(128) NOT NULL , 
                  `cod` TINYINT(1) NOT NULL, 
                  `currency` VARCHAR(64) NOT NULL, 
                  `fullname` VARCHAR(256) NULL COMMENT 'Tên người nhận' , 
                  `phone` VARCHAR(32) NOT NULL COMMENT 'Số ' , 
                  `town` VARCHAR(256) NULL , 
                  `district` VARCHAR(256) NULL , 
                  `city` VARCHAR(256) NULL , 
                  `state` VARCHAR(256) NULL , 
                  `country` VARCHAR(256) NULL , 
                  `zipcode` VARCHAR(32) NULL , 
                  `full_address` VARCHAR(512) NULL , 
                  `actual_shipping_cost` DECIMAL(18,4) NULL , 
                  `total_amount` DECIMAL(18,4) NOT NULL , 
                  `order_status` VARCHAR(128) NOT NULL , 
                  `shipping_method` VARCHAR(256) NOT NULL COMMENT 'shipping_carrier' , 
                  `payment_method` VARCHAR(256) NOT NULL , 
                  `note_for_shop` TEXT NULL COMMENT 'message_to_seller' , 
                  `note` TEXT NULL , 
                  `create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , 
                  `update_time` DATETIME NULL , 
                  `shopee_shop_id` BIGINT(64) NOT NULL ,
                  `bestme_order_id` INT(11) NULL , 
                  `sync_status` TINYINT(2)  NULL , 
                  `is_edited` TINYINT(1) NOT NULL DEFAULT 0 ,
                  `sync_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , 
                  PRIMARY KEY (`id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);

        // shopee_order_to_product
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}shopee_order_to_product` (
                  `shopee_order_code` VARCHAR(128) NOT NULL COMMENT 'order code on shopee', 
                  `item_id` BIGINT(64) NOT NULL COMMENT '~ product_id ' , 
                  `variation_id` BIGINT(64) NOT NULL COMMENT '~ product_version_id' , 
                  `variation_name` VARCHAR(256) NULL , 
                  `quantity` INT(11) NOT NULL COMMENT 'variation_quantity_purchased' , 
                  `original_price` DECIMAL(18,4) NOT NULL COMMENT 'variation_original_price' ,
                  `price` DECIMAL(18,4) NOT NULL COMMENT 'variation_discounted_price' 
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);

        // shopee_shop_config
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}shopee_shop_config` ( 
                  `shop_id` BIGINT(64) NOT NULL , 
                  `shop_name` VARCHAR(256) NULL , 
                  `sync_interval` INT(11) NOT NULL COMMENT '0: không bao giờ' , 
                  `connected` TINYINT(1) NOT NULL DEFAULT '1' , 
                  PRIMARY KEY (`shop_id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);
    }

    protected function migrateDataTableShopeeShopConfig()
    {
        $setting = $this->db->query("SELECT * FROM `{$this->db_prefix}setting` WHERE `key` LIKE 'shopee_shop_id'");
        $shop_id = isset($setting->row['value']) ? $setting->row['value'] : 0;

        if ($shop_id) {
            $shopee = \Sale_Channel::getSaleChannel('shopee', ['shop_id' => (int)$shop_id]);
            $shop_info = $shopee->getShopInfo();
            $shop_info = json_decode($shop_info, true);

            if (!is_array($shop_info) || !array_key_exists('shop_id', $shop_info) || !array_key_exists('shop_name', $shop_info)) {
                return;
            }

            $this->db->query("INSERT INTO `{$this->db_prefix}shopee_shop_config` 
                                    SET `shop_id` = '" . (int)$shop_info['shop_id'] . "', 
                                        `shop_name` = '" . (string)$shop_info['shop_name'] . "', 
                                        `sync_interval` = " . 0 . ", 
                                        `connected` = " . 1);
        }
    }
}