<?php

namespace Migration;

class migration_20200924_full_text_search_product_name extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200924_full_text_search_product_name';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /*
     * Notice: TEMP NOT USE!!!
     * FULLTEXT by default has a minimum word length, usually 4 chars (see mysql environment variable 'ft_min_word_length')
     * FULLTEXT indexes on MyISAM tables must be rebuilt after changing this variable. Use REPAIR TABLE tbl_name QUICK.
     * See:
     * - https://stackoverflow.com/questions/13130068/mysql-fulltext-search-query-missing-results
     * - https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html#sysvar_ft_min_word_len
     */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate alter table product description');
        try {
            $this->alterColumnProductDescription();
        } catch (\Exception $e) {
            $this->log('alter table product description got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back ....');
    }

    protected function alterColumnProductDescription()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}product_description` ADD FULLTEXT(`name`);";

        $this->db->query($sql);
    }
}