<?php


namespace Migration;


class migration_20201118_create_attribute_filter_table extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20201118_create_attribute_filter_table';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will remove unused attribute data');
        try {
            $this->removeUnusedAttributeData();
        } catch (\Exception $e) {
            $this->log('remove unused attribute data got error: ' . $e->getMessage());
        }

        $this->log('Will create attribute_filter table');
        try {
            $this->createAttributeFilterTable();
        } catch (\Exception $e) {
            $this->log('create attribute_filter table got error: ' . $e->getMessage());
        }

        $this->log('Will create product_attribute_filter table');
        try {
            $this->createProductAttributeFilterTable();
        } catch (\Exception $e) {
            $this->log('create product_attribute_filter table got error: ' . $e->getMessage());
        }

        $this->log('Will insert data into attribute_filter, product_attribute_filter table');
        try {
            $this->insertAttributeFilterData();
        } catch (\Exception $e) {
            $this->log('insert data into attribute_filter, product_attribute_filter table got error: ' . $e->getMessage());
        }
    }

    private function removeUnusedAttributeData()
    {
        /**
         * delete attributes WHERE
         * - not belong to any products
         * - OR belong to deleted products those do not have orders
         */
        // get attribute ids need to delete
        $sql = "SELECT `attribute_id` FROM `{$this->db_prefix}product_attribute` pa
                WHERE pa.`product_id` NOT IN (SELECT `product_id` FROM `{$this->db_prefix}product`) 
                    OR pa.`product_id` IN (SELECT `product_id` FROM `{$this->db_prefix}product` p 
                                            WHERE p.`deleted` IS NOT NULL 
                                            AND p.`product_id` NOT IN (SELECT DISTINCT(`product_id`) FROM `{$this->db_prefix}order_product`))";
        $query = $this->db->query($sql);
        $attribute_id_arr = array_column($query->rows, 'attribute_id');

        if (!empty($attribute_id_arr)) {
            $attribute_ids = implode(',', $attribute_id_arr);

            // delete attribute description
            $sql = "DELETE FROM `{$this->db_prefix}attribute_description` WHERE `attribute_id` IN ({$attribute_ids})";
            $this->db->query($sql);

            // delete attribute
            $sql = "DELETE FROM `{$this->db_prefix}attribute` WHERE `attribute_id` IN ({$attribute_ids})";
            $this->db->query($sql);

            // delete product attribute
            $sql = "DELETE FROM `{$this->db_prefix}product_attribute` WHERE `attribute_id` IN ({$attribute_ids})";
            $this->db->query($sql);
        }
    }

    private function createAttributeFilterTable()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}attribute_filter` (
                  `attribute_filter_id` INT(11) NOT NULL AUTO_INCREMENT,
                  `name` TEXT NOT NULL COLLATE 'utf8_general_ci',
                  `value` TEXT NOT NULL COLLATE 'utf8_general_ci',
                  `status` TINYINT(1) NOT NULL DEFAULT 0,
	              `language_id` INT(11) NOT NULL,
                  PRIMARY KEY (`attribute_filter_id`)
                ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
        $this->db->query($sql);
    }

    private function createProductAttributeFilterTable()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}product_attribute_filter` (
                  `product_id` INT(11) NOT NULL,
                  `attribute_filter_id` INT(11) NOT NULL,
                  `attribute_filter_value` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',
                  PRIMARY KEY (`product_id`, `attribute_filter_id`, `attribute_filter_value`) USING BTREE
                ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
        $this->db->query($sql);
    }

    private function insertAttributeFilterData()
    {
        // get all languages
        $sql = "SELECT `language_id` FROM `{$this->db_prefix}language`";
        $query = $this->db->query($sql);
        $language_ids = array_column($query->rows, 'language_id');

        // get all attributes in each language
        foreach ($language_ids as $language_id) {
            $sql = "SELECT ad.`name` FROM `{$this->db_prefix}attribute` a 
                    INNER JOIN `{$this->db_prefix}attribute_description` ad ON a.attribute_id = ad.attribute_id
                    WHERE ad.`language_id` = {$language_id} GROUP BY ad.`name`";
            $query = $this->db->query($sql);
            $attributes = $query->rows;

            foreach ($attributes as $attribute) {
                // get all attribute values of each lower attribute name (belong to undeleted and unhidden products)
                $lower_attribute_name = mb_strtolower($attribute['name']);
                $sql = "SELECT pa.`text`, pa.`product_id` FROM `{$this->db_prefix}product_attribute` pa
                        INNER JOIN `{$this->db_prefix}product` p ON pa.`product_id` = p.`product_id`
                        WHERE pa.`attribute_id` IN (SELECT a.`attribute_id` FROM `{$this->db_prefix}attribute` a 
                                                INNER JOIN `{$this->db_prefix}attribute_description` ad ON a.attribute_id = ad.attribute_id
                                                WHERE ad.`language_id` = {$language_id} AND LCASE(ad.`name`) = '{$lower_attribute_name}')
                        AND pa.`language_id` = {$language_id}
                        AND p.`status` = 1
                        AND p.`deleted` IS NULL";
                $query = $this->db->query($sql);
                $attribute_values = $query->rows;

                if (!empty($attribute_values)) {
                    // parse attribute values into a array
                    $attribute_values_arr = [];
                    $product_attribute_value_arr = [];
                    foreach ($attribute_values as $attribute_value) {
                        $lower_attribute_value_arr = explode(',', mb_strtolower($attribute_value['text']));
                        $attribute_values_arr = array_merge($attribute_values_arr, $lower_attribute_value_arr);

                        foreach ($lower_attribute_value_arr as $lower_attribute_value) {
                            $product_attribute_value_arr[$attribute_value['product_id']][] = mb_ucwords($lower_attribute_value);
                        }
                    }

                    // remove duplicated attribute values
                    $attribute_values_arr = array_unique($attribute_values_arr);

                    // format attribute values to capitalize
                    $attribute_values_arr = array_map('mb_ucwords', $attribute_values_arr);

                    // save data to attribute_filter table
                    $attribute_filter_name = mb_ucwords($lower_attribute_name);
                    $attribute_filter_value = implode(',', $attribute_values_arr);
                    $sql = "INSERT INTO `{$this->db_prefix}attribute_filter` (`name`, `value`, `language_id`)
                            SELECT '$attribute_filter_name', '$attribute_filter_value', {$language_id} FROM DUAL
                            WHERE NOT EXISTS (SELECT 1 FROM `{$this->db_prefix}attribute_filter` WHERE `name` = '$attribute_filter_name')";
                    $this->db->query($sql);
                    $attribute_filter_id = $this->db->getLastId();

                    // save data to product_attribute_filter table
                    foreach ($product_attribute_value_arr as $product_id => $product_attribute_values) {
                        // remove duplicated attribute values in each product
                        $product_attribute_values = array_unique($product_attribute_values);
                        foreach ($product_attribute_values as $product_attribute_value) {
                            $sql = "INSERT IGNORE INTO `{$this->db_prefix}product_attribute_filter`
                                    SET `product_id` = {$product_id},
                                        `attribute_filter_id` = {$attribute_filter_id},
                                        `attribute_filter_value` = '{$product_attribute_value}'";
                            $this->db->query($sql);
                        }
                    }
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back create attribute filter table');
    }
}