<?php

namespace Migration;

class migration_20190828_transport extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20190828_transport';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate add some columns to order table for transport with 3rd parties');

        $this->addColumnsForTransportWith3rdParties();
    }

    protected function addColumnsForTransportWith3rdParties()
    {
        try {
            /* add columns to order table */
            $sql = "
                ALTER TABLE `{$this->db_prefix}order`
                  ADD `source` VARCHAR(50) NULL AFTER `user_id`,
                  ADD `transport_name` varchar(64) DEFAULT NULL AFTER `telephone`,
                  ADD `transport_order_code` varchar(64) DEFAULT NULL AFTER `telephone`,
                  ADD `transport_service_name` varchar(64) DEFAULT NULL AFTER `telephone`,
                  ADD `transport_money_total` VARCHAR(255) NOT NULL AFTER `transport_name`,
                  ADD `transport_weight` VARCHAR(255) NOT NULL AFTER `transport_money_total`,
                  ADD `transport_status` VARCHAR(255) NOT NULL AFTER `transport_weight`,
                  ADD `transport_current_warehouse` VARCHAR(255) NOT NULL AFTER `transport_status`;";

            $this->db->query($sql);

            /* add setting for viettel post. TODO: use or remove this?... */
            $sql = "
                INSERT INTO `{$this->db_prefix}setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
                (0, 'config', 'config_VIETTEL_POST_token_webhook', '', 0);";

            $this->db->query($sql);
        } catch (\Exception $e) {
            // may columns existed for new shop! Catch to mark migrate done!
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back some columns from order table for removing transport with 3rd parties');
        // no need
    }
}