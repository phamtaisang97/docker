<?php

namespace Migration;

class migration_20220415_set_sitemap_detail_lastmod extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20220415_set_sitemap_detail_lastmod';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Set sitemap detail last modified date');

        try {
            $this->setSitemapDetailLastMod();
        } catch (\Exception $e) {
            $this->log('Could not set sitemap detail last modified date. Got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('This should be rollback the migration and unset sitemap detail last modified date');
    }

    private function setSitemapDetailLastMod()
    {
        $sitemapDetails = ['categories', 'products', 'manufacturers', 'collections'];

        foreach ($sitemapDetails as $sitemapDetail) {
            $sql = "INSERT INTO `{$this->db_prefix}setting` (`store_id`, `code`, `key`,`value`, `serialized`) SELECT 0, 'config', 'sitemap_{$sitemapDetail}_lastmod', '2022-04-15', '0' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM `{$this->db_prefix}setting` WHERE `code` = 'config' AND `key` = 'sitemap_{$sitemapDetail}_lastmod')";
            $this->db->query($sql);
        }
    }
}