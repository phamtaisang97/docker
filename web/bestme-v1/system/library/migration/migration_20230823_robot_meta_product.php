<?php


namespace migration;


class migration_20230823_robot_meta_product extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20230823_robot_meta_product';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('migrate robot_meta product');
        try {
            $this->migrateTable();
        } catch (\Exception $e) {
            $this->log('migrate robot_meta product got error: ' . $e->getMessage());
        }
    }


    private function migrateTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}product_description`
	                ADD COLUMN `noindex` TINYINT NOT NULL DEFAULT '0' AFTER `meta_keyword`,
                    ADD COLUMN `nofollow` TINYINT NOT NULL DEFAULT '0' AFTER `noindex`,
                    ADD COLUMN `noarchive` TINYINT NOT NULL DEFAULT '0' AFTER `nofollow`,
                    ADD COLUMN `noimageindex` TINYINT NOT NULL DEFAULT '0' AFTER `noarchive`,
                    ADD COLUMN `nosnippet` TINYINT NOT NULL DEFAULT '0' AFTER `noimageindex`;";
        $this->db->query($sql);

        $sql = "ALTER TABLE `{$this->db_prefix}category_description`
	                ADD COLUMN `noindex` TINYINT NOT NULL DEFAULT '0' AFTER `meta_keyword`,
                    ADD COLUMN `nofollow` TINYINT NOT NULL DEFAULT '0' AFTER `noindex`,
                    ADD COLUMN `noarchive` TINYINT NOT NULL DEFAULT '0' AFTER `nofollow`,
                    ADD COLUMN `noimageindex` TINYINT NOT NULL DEFAULT '0' AFTER `noarchive`,
                    ADD COLUMN `nosnippet` TINYINT NOT NULL DEFAULT '0' AFTER `noimageindex`;";
        $this->db->query($sql);

        $sql = "ALTER TABLE `{$this->db_prefix}collection_description`
	                ADD COLUMN `noindex` TINYINT NOT NULL DEFAULT '0' AFTER `alias`,
                    ADD COLUMN `nofollow` TINYINT NOT NULL DEFAULT '0' AFTER `noindex`,
                    ADD COLUMN `noarchive` TINYINT NOT NULL DEFAULT '0' AFTER `nofollow`,
                    ADD COLUMN `noimageindex` TINYINT NOT NULL DEFAULT '0' AFTER `noarchive`,
                    ADD COLUMN `nosnippet` TINYINT NOT NULL DEFAULT '0' AFTER `noimageindex`;";
        $this->db->query($sql);
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back robot_meta product');
    }
}