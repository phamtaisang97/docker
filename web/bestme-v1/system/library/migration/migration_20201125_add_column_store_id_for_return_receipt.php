<?php

namespace Migration;

class migration_20201125_add_column_store_id_for_return_receipt extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20201125_add_column_store_id_for_return_receipt';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate add column store_id for return_receipt');
        try {
            $this->migrateColumnStoreId();
        } catch (\Exception $e) {
            $this->log('Add column store_id for return_receipt got error: ' . $e->getMessage());
        }

        try {
            $this->migrateDataDefaultColumnStoreId();
        } catch (\Exception $e) {
            $this->log('Data default store_id for return_receipt got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back remove column store_id of return_receipt');
    }

    protected function migrateColumnStoreId()
    {
        $DB_PREFIX = $this->db_prefix;
        // -- oc_return_receipt
        $sql = "ALTER TABLE `{$DB_PREFIX}return_receipt` ADD `store_id` INT(11) NULL AFTER `note`;";

        $this->db->query($sql);
    }

    protected function migrateDataDefaultColumnStoreId()
    {
        $DB_PREFIX = $this->db_prefix;

        // get return_receipt
        $sql = "SELECT `order_id`, `return_receipt_id` FROM `{$DB_PREFIX}return_receipt`";
        $return_receipts = $this->db->query($sql)->rows;

        foreach ($return_receipts as $item) {
            if (!isset($item['order_id']) || !isset($item['return_receipt_id'])) {
                continue;
            }

            $query = "SELECT `order_id`, `store_id` FROM `{$DB_PREFIX}order` WHERE `order_id` = {$item['order_id']}";
            $order = $this->db->query($query)->row;

            if (!isset($order['store_id'])) {
                continue;
            }

            $this->db->query("UPDATE `{$DB_PREFIX}return_receipt` 
                                      SET `store_id` = {$order['store_id']} WHERE `return_receipt_id` = {$item['return_receipt_id']}");
        }
    }
}