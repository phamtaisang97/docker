<?php


namespace Migration;


class migration_20201103_create_image_folder extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20201103_create_image_folder';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will create table for image folder');

        try {
            $this->createFolderTree();
        } catch (\Exception $e) {
            $this->log('create table folder tree got error: ' . $e->getMessage());
        }

        try {
            $this->addFolderPath();
        } catch (\Exception $e) {
            $this->log('Alter table image add folder path error: ' . $e->getMessage());
        }
    }

    protected function createFolderTree()
    {
        // New table: image_folder
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}image_folder` ( 
                  `id` INT(11) NOT NULL AUTO_INCREMENT, 
                  `parent_id` int(11) NOT NULL DEFAULT '0',
                  `status` tinyint(1) NOT NULL DEFAULT '1',
                  `name` varchar(255) DEFAULT NULL,
                  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  PRIMARY KEY (`id`),
                  UNIQUE INDEX `parent_id_name` (`parent_id`, `name`) USING BTREE
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);
    }

    protected function addFolderPath()
    {
        // Add folder path for image
        $sql = "ALTER TABLE `{$this->db_prefix}images`
                  ADD COLUMN `folder_path` INT(11) NOT NULL DEFAULT 0 AFTER `size`;";

        $this->db->query($sql);
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back create table image directory');
    }
}