<?php

namespace Migration;

class migration_20190718_default_policy_gg_shopping extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20190718_default_policy_gg_shopping';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Init default policy for google shopping');

        /* payment */
        $this->up_payment_method();

        /* delivery */
        $this->up_delivery_method();

        /* policy */
        $this->up_policy_method();
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Roll back all default policy for google shopping');

        // 99.99% not used. Temp do nothing. // TODO: add code if need...

        /* payment */
        $this->down_payment_method();

        /* delivery */
        $this->down_delivery_method();

        /* policy */
        $this->down_policy_method();
    }

    /* === end - default functions === */

    private function up_payment_method()
    {
        $this->log('Up payment methods');

        /* check if existed */
        if ($this->is_method_existing('payment_methods')) {
            $this->log('Payment methods already existed');
            return;
        }

        $sql = "DELETE FROM `{$this->db_prefix}setting` WHERE `code` = 'payment' AND `key` = 'payment_methods';";
        $this->db->query($sql);

        $sql = "INSERT INTO `{$this->db_prefix}setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES 
            (0, 'payment', 'payment_methods', '[{\"payment_id\":\"198535319739A\",\"name\":\"Thanh toán khi nhận hàng (COD)\",\"guide\":\"Bạn sẽ thanh toán toàn bộ giá trị đơn hàng khi nhận hàng\",\"status\":\"1\"}]', 1)";
        $this->db->query($sql);
    }

    private function up_delivery_method()
    {
        $this->log('Up delivery methods');

        /* check if existed */
        if ($this->is_method_existing('delivery_methods')) {
            $this->log('Delivery methods already existed!');
            return;
        }

        $sql = "DELETE FROM `{$this->db_prefix}setting` WHERE `code` = 'delivery' AND `key` = 'delivery_methods';";
        $this->db->query($sql);

        $sql = "INSERT INTO `{$this->db_prefix}setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
            (0, 'delivery', 'delivery_methods', '[{\"id\":0,\"name\":\"Vận chuyển tiêu chuẩn\",\"status\":\"1\",\"fee_region\":\"20000\",\"fee_regions\":[{\"id\":1,\"name\":\"Hà Nội - Miễn phí vận chuyển\",\"provinces\":[\"Hà Nội\"],\"weight_steps\":[{\"type\":\"FROM\",\"from\":\"1\",\"to\":\"101\",\"price\":\"0\"}]}],\"fee_cod\":{\"fixed\":{\"status\":\"1\",\"value\":\"0\"},\"dynamic\":{\"status\":\"0\",\"value\":\"\"}}}]', 1)";
        $this->db->query($sql);
    }

    private function up_policy_method()
    {
        $this->log('Up policy page');

        /* insert policy page default */
        // check if existed
        if ($this->is_record_existed("{$this->db_prefix}page_contents", 'page_id')) {
            $this->log('Default Policy Page already existed!');
            return;
        }

        // insert
        $sql = "INSERT IGNORE INTO `{$this->db_prefix}page_contents` (`page_id`,`title`, `description`, `seo_title`, `seo_description`, `status`, `date_added`) VALUES
            (1000, 'Chính sách và bảo mật', 'I. Chính sách thanh toán - giao hàng
                    <br/>
                    1. GIAO HÀNG – TRẢ TIỀN MẶT (COD)
                    <br/>
                    - Chúng tôi áp dụng hình thức giao hàng COD cho tất cả các vùng miền trên toàn quốc.
                    <br/>
                    2. CHI PHÍ GIAO HÀNG
                    <br/>
                    - Chúng tôi giao hàng miễn phí đối với khách hàng tại nội thành Hà Nội. Các tỉnh thành khác sẽ áp dụng mức chi phí của đối tác vận chuyển khác.
                    <br/>
                    Chính sách đổi trả
                    1. Lý do chấp nhận đổi trả
                    <br/>
                    - Sản phẩm bị lỗi do nhà sản xuất trong 7 ngày đầu sử dụng, đối với khách mua hàng online thì áp dụng từ khi đơn hàng thành công. 
                    <br/>
                    2. Yêu cầu cho sản phẩm đổi trả
                    <br/>
                    - Sản phẩm còn nguyên vẹn, đầy đủ nhãn mác, nguyên đai kiện, niêm phong theo quy cách ban đầu.
                    <br/>
                    - Sản phẩm/dịch vụ còn đầy đủ phụ kiện.
                    <br/>
                    3. Thời gian đổi trả
                    <br/>
                    - Trong vòng 07 ngày kể từ ngày nhận sản phẩm.
                    <br/>
                    <br/>
                    II. CHÍNH SÁCH BẢO MẬT
                    <br/>
                    1. Thu thập thông tin cá nhân
                    <br/>
                    - Chúng tôi sẽ dùng thông tin bạn đã cung cấp để xử lý đơn đặt hàng, cung cấp các dịch vụ và thông tin yêu cầu thông qua website và theo yêu cầu của bạn.
                    <br/>
                    - Chúng tôi có thể chuyển tên và địa chỉ cho bên thứ ba để họ giao hàng cho bạn.
                    <br/>
                    2. Bảo mật
                    <br/>
                    - Chúng tôi có biện pháp thích hợp về kỹ thuật và an ninh để ngăn chặn truy cập trái phép hoặc trái pháp luật hoặc mất mát hoặc hủy hoặc thiệt hại cho thông tin của bạn.', 'Chính sách thanh toán - giao hàng, Chính sách đổi trả, Chính sách bảo mật' , 'Chính sách thanh toán - giao hàng, Chính sách đổi trả, Chính sách bảo mật', 1, NOW())";
        $this->db->query($sql);

        /* insert seo_url for policy page default */
        // check if existed
        if ($this->is_record_existed("{$this->db_prefix}seo_url", 'seo_url_id')) {
            $this->log('Seo url for Default Policy Page already existed!');
            return;
        }

        // insert
        $sql = "INSERT IGNORE INTO `{$this->db_prefix}seo_url` (`seo_url_id`,`store_id`, `language_id`, `query`, `keyword`) VALUES
            (1000, 0, 1, 'page_id=1000' , 'chinh-sach-va-bao-mat'),
            (1001, 0, 2, 'page_id=1000' , 'chinh-sach-va-bao-mat')";
        $this->db->query($sql);

        /* add policy page to menu */
        // check if existed
        if ($this->is_record_existed("{$this->db_prefix}novaon_menu_item", 'id')) {
            $this->log('Policy page already existed in menu!');
            return;
        }

        // insert
        $sql = "INSERT IGNORE INTO `{$this->db_prefix}novaon_menu_item` (`id`, `parent_id`, `group_menu_id`, `status`, `position`, `created_at`, `updated_at`) VALUES
            (1000, 0, 1, 1, 0, NOW(), NOW())";
        $this->db->query($sql);

        /* insert oc_novaon_menu_item_description */
        // check if existed
        if ($this->is_record_existed("{$this->db_prefix}novaon_menu_item_description", 'menu_item_id')) {
            $this->log('Menu description already existed!');
            return;
        }

        // insert
        $sql = "INSERT IGNORE INTO `{$this->db_prefix}novaon_menu_item_description` (`id`, `language_id`, `name`, `menu_item_id`) VALUES
            (1000, 1, 'Chính sách' , 1000),
            (1001, 2, 'Chính sách' , 1000)";
        $this->db->query($sql);

        /* insert to relation table */
        // check if existed
        if ($this->is_record_existed("{$this->db_prefix}novaon_relation_table", 'main_id')) {
            $this->log('Relation already existed in relation table!');
            return;
        }

        // insert
        $sql = "INSERT IGNORE INTO `{$this->db_prefix}novaon_relation_table` (`main_name`, `main_id`, `child_name`, `child_id`, `type_id`, `url`, `redirect`, `weight_number`) VALUES
            ('group_menu', 1, 'menu_item', 1000, 5, '', 0, 0),
            ('menu_item', 1000, '', 0, 5, 'chinh-sach-va-bao-mat', 1, 0)";
        $this->db->query($sql);
    }

    private function down_payment_method()
    {
        $this->down_setting('payment_methods', 'payment_id', "198535319739A");
    }

    private function down_delivery_method()
    {
        $this->down_setting('delivery_methods', 'id', 0);
    }

    private function down_policy_method()
    {
        // TODO: do something...
    }

    private function down_setting($key, $compare_id, $compare)
    {
        $data = $this->db->query("SELECT * FROM {$this->db_prefix}setting WHERE `key` = '$key'");
        $row = $data->row;
        if (!is_array($row) || !isset($row['setting_id']) || !isset($row['value'])) {
            return;
        }

        $methods = $row['value'];
        $setting_id = $row['setting_id'];
        $value = json_decode($methods, true);
        $i = 0;
        $is_existing_setting = false;
        foreach ($value as $item) {
            if (!is_array($item)) continue;
            $id = $item[$compare_id];
            if ($id == $compare) {
                if (count($value) == 1) {
                    $sql = "DELETE FROM {$this->db_prefix}setting WHERE `setting_id` = $setting_id";
                    $this->db->query($sql);
                    $is_existing_setting = false;
                } else {
                    if (array_key_exists($i, $value)) {
                        unset($value[$i]);
                    }
                    $is_existing_setting = true; // TODO: break immediately?
                }
            }
            $i++;
        }

        if (!$is_existing_setting) {
            return;
        }
        $value = array_values($value);
        $value = json_encode($value, JSON_UNESCAPED_UNICODE);
        $sql = "UPDATE {$this->db_prefix}setting SET `value`= '$value' WHERE `setting_id` = $setting_id";
        $this->db->query($sql);
    }

    private function is_method_existing($key = '')
    {
        $data = $this->db->query("SELECT * FROM `{$this->db_prefix}setting` WHERE `key` = '$key'");
        // check payment method in db
        if ($row = $data->row) {
            if (isset($row['value']) && !empty(json_decode($row['value'], true))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check record for police tables
     * @param $table
     * @param $id
     * @return bool
     */
    private function is_record_existed($table, $id)
    {
        $query = "SELECT * FROM `$table` WHERE `$id` = 1000";
        $data = $this->db->query($query);
        // check policy page existing in db
        if ($data->row) {
            return true;
        }

        return false;
    }
}