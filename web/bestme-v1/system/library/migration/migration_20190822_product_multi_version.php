<?php

namespace Migration;

class migration_20190822_product_multi_version extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20190822_product_multi_version';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate add some common columns to product table');

        $this->addCommonColumns();
    }

    protected function addCommonColumns()
    {
        try {
            $sql = "
                ALTER TABLE `{$this->db_prefix}product` 
                ADD `common_price` decimal(15,4) DEFAULT NULL AFTER `sku`,
                ADD `common_compare_price` decimal(15,4) DEFAULT NULL AFTER `sku`,
                ADD `common_sku` varchar(64) DEFAULT NULL AFTER `sku`,
                ADD `common_barcode` varchar(128) DEFAULT NULL AFTER `sku`;";

            $this->db->query($sql);
        } catch (\Exception $e) {
            // may columns existed for new shop! Catch to mark migrate done!
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back some common columns for product table');
        // no need
    }
}