<?php

namespace Migration;

class migration_20201021_pos_return_goods extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20201021_pos_return_goods';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate create table for pos return goods version');
        try {
            $this->migrateCreateTablesForAppstore();
        } catch (\Exception $e) {
            $this->log('crate table for pos return goods version got error: ' . $e->getMessage());
        }

        // permission for admin user
        try {
            $this->addPermissionForAdminUser([
                'sale/return_receipt'
            ]);
        } catch (\Exception $e) {
            $this->log('addPermissionForAdminUser (sale/return_receipt) got error: ' . $e->getMessage());
        }

        // permission for staff users
        try {
            $this->addPermissionForStaffUser([
                'sale/order' => ['sale/return_receipt']
            ]);
        } catch (\Exception $e) {
            $this->log('addPermissionForStaffUser  (sale/return_receipt) got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back remove some table of pos return goods version');
    }

    protected function migrateCreateTablesForAppstore()
    {
        // -- oc_return_receipt
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}return_receipt` (
                    `return_receipt_id` INT(11) NOT NULL AUTO_INCREMENT,
                    `order_id` INT(11) NOT NULL,
                    `return_receipt_code` VARCHAR(50) NOT NULL,
                    `return_fee` DECIMAL(18,4) NULL DEFAULT NULL,
                    `total` DECIMAL(18,4) NULL DEFAULT NULL,
                    `note` VARCHAR(255) NULL DEFAULT NULL,
                    `date_added` DATETIME NOT NULL,
                    PRIMARY KEY (`return_receipt_id`) USING BTREE
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);

        // -- oc_return_product
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}return_product` (
                    `return_product_id` INT(11) NOT NULL AUTO_INCREMENT,
                    `return_receipt_id` INT(11) NOT NULL,
                    `product_id` INT(11) NOT NULL,
                    `product_version_id` INT(11) NULL DEFAULT '0',
                    `price` DECIMAL(18,4) NULL DEFAULT NULL,
                    `quantity` INT(4) NOT NULL,
                    PRIMARY KEY (`return_product_id`) USING BTREE
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);

        $sql = "ALTER TABLE `{$this->db_prefix}payment_voucher`
                  ADD COLUMN `return_receipt_id` INT(11) NULL DEFAULT NULL AFTER `store_receipt_id`;";

        $this->db->query($sql);
    }
}