<?php

namespace Migration;

class migration_20220420_add_new_column_to_table_blog_category extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20220420_add_new_column_to_table_blog_category';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('add column parent_id to table blog category');

        try {
            $this->addColumnParentIdToBlogCategoryTable();
        } catch (\Exception $e) {
            $this->log('add column parent_id to table blog category got error: ' . $e->getMessage());
        }

    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back add column parent_id to table blog category');
    }

    protected function addColumnParentIdToBlogCategoryTable()
    {
        //ALTER TABLE `oc_blog_category` ADD `parent_id` INT NULL DEFAULT '0' AFTER `status`;
        $sql = "ALTER TABLE `{$this->db_prefix}blog_category` 
                ADD `parent_id` INT NULL DEFAULT '0' AFTER `status`;";
        $this->db->query($sql);
    }
}