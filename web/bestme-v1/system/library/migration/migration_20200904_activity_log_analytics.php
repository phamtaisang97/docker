<?php

namespace Migration;

class migration_20200904_activity_log_analytics extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200904_activity_log_analytics';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will insert activity_log_analytics event to event table');

        try {
            $this->migrateInsertEvents();
        } catch (\Exception $e) {
            $this->log('Insert activity_log_analytics event to event table got error: ' . $e->getMessage());
        }
    }

    protected function migrateInsertEvents()
    {
        $sql = "INSERT INTO `{$this->db_prefix}event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES 
                ('tracking_order_complete', 'admin/model/sale/order/updateStatus/after', 'event/activity_tracking/trackingOrderComplete', 1, 0),
                ('tracking_login', 'admin/model/user/user/editLastLoggedIn/after', 'event/activity_tracking/trackingLogin', 1, 0),
                ('tracking_product_category', 'admin/model/catalog/store_take_receipt/addStoreTakeReceipt/after', 'event/activity_tracking/trackingStoreTakeReceipt', 1, 0),
                ('tracking_new_product', 'admin/model/catalog/product/addProduct/after', 'event/activity_tracking/trackingNewProduct', 1, 0),
                ('tracking_transfer_order_status', 'admin/model/sale/order_history/addHistory/after', 'event/activity_tracking/trackingTransferOrderStatus', 1, 0),
                ('tracking_new_product_category', 'admin/model/catalog/category/addCategory/after', 'event/activity_tracking/trackingNewProductCategory', 1, 0),
                ('tracking_new_product_category', 'admin/model/catalog/category/addCategoryFast/after', 'event/activity_tracking/trackingNewProductCategory', 1, 0),
                ('tracking_delete_product_category', 'admin/model/catalog/category/deleteCategory/after', 'event/activity_tracking/trackingDeleteProductCategory', 1, 0),
                ('tracking_product_price', 'admin/model/catalog/product/addProduct/after', 'event/activity_tracking/trackingProductPrice', 1, 0),
                ('tracking_product_price', 'admin/model/catalog/product/editProduct/after', 'event/activity_tracking/trackingProductPrice', 1, 0),
                ('tracking_new_store_transfer_receipt', 'admin/model/catalog/store_transfer_receipt/addStoreTransferReceipt/after', 'event/activity_tracking/trackingStoreTransferReceipt', 1, 0),
                ('tracking_new_store_receipt', 'admin/model/catalog/store_receipt/addStoreReceipt/after', 'event/activity_tracking/trackingStoreReceipt', 1, 0),
                ('tracking_new_cost_adjustment_receipt', 'admin/model/catalog/cost_adjustment_receipt/addReceipt/after', 'event/activity_tracking/trackingCostAdjustmentReceipt', 1, 0),
                ('tracking_new_discount', 'admin/model/discount/discount/addDiscount/after', 'event/activity_tracking/trackingCountDiscount', 1, 0),
                ('tracking_new_order_catalog', 'catalog/model/sale/order/addOrderCommon/after', 'event/activity_tracking/trackingCountOrder', 1, 0),
                ('tracking_new_order_admin', 'admin/model/sale/order/addOrder/after', 'event/activity_tracking/trackingCountOrder', 1, 0),
                ('tracking_new_shopee', 'admin/model/catalog/shopee/addShopeeShop/after', 'event/activity_tracking/trackingCountShopee', 1, 0),
                ('tracking_new_shopee_product', 'admin/model/catalog/product/addProduct/after', 'event/activity_tracking/trackingCountShopeeProduct', 1, 0);";
        $this->db->query($sql);
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back remove activity_log_analytics event in event table');
    }
}