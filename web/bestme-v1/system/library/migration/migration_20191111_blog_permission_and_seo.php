<?php

namespace Migration;

use Exception;

class migration_20191111_blog_permission_and_seo extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20191111_blog_permission_and_seo';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Enable permission and seo setting for blog feature');

        /* add/modify tables for blog feature */
        try {
            $this->addAndModifyTablesForBlogFeature();
        } catch (Exception $e) {
            $this->log('addAndModifyTablesForBlogFeature: error: ' . $e->getMessage());
        }

        /* add permission for blog and builder */
        try {
            $this->addPermissionForBlogAndBuilder();
        } catch (Exception $e) {
            $this->log('addPermissionForBlogAndBuilder: error: ' . $e->getMessage());
        }

        /* insert some seo url for blog */
        try {
            $this->addSeoForBlog();
        } catch (Exception $e) {
            $this->log('addSeoForBlog: error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Roll back permission and seo setting from blog feature');

        /* delete tables from blog feature */
        try {
            // remove `type` from table `collection`
            $sql = "ALTER TABLE `{$this->db_prefix}collection` DROP `type`;";
            $this->db->query($sql);

            // drop tables
            $sql = "DROP TABLE IF EXISTS `{$this->db_prefix}{$this->db_prefix}collection_to_collection`, 
                                         `{$this->db_prefix}{$this->db_prefix}blog`, 
                                         `{$this->db_prefix}{$this->db_prefix}blog_description`, 
                                         `{$this->db_prefix}{$this->db_prefix}blog_category`,
                                         `{$this->db_prefix}{$this->db_prefix}blog_category_description`,
                                         `{$this->db_prefix}{$this->db_prefix}blog_to_blog_category`,
                                         `{$this->db_prefix}{$this->db_prefix}blog_tag`;";
            $this->db->query("$sql");
        } catch (Exception $e) {
            $this->log('deleteTablesFromBlogFeature: error: ' . $e->getMessage());
        }

        /* delete permission for blog and builder */
        // no need

        /* delete seo url for blog */
        try {
            $this->deleteSeoFromBlog();
        } catch (Exception $e) {
            $this->log('deleteSeoForBlog: error: ' . $e->getMessage());
        }
    }

    private function addAndModifyTablesForBlogFeature()
    {
        // create table `collection_to_collection`
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}collection_to_collection` (
                  `collection_to_collection_id` int(11) NOT NULL AUTO_INCREMENT,
                  `collection_id` int(11) NOT NULL,
                  `parent_collection_id` int(11) NOT NULL,
                  `sort_order` int(11) DEFAULT '0',
                  PRIMARY KEY (`collection_to_collection_id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        $this->db->query($sql);

        // add `type` to table `collection`
        try {
            $sql = "ALTER TABLE `{$this->db_prefix}collection` ADD `type` varchar(255) DEFAULT '0';";
            $this->db->query($sql);
        } catch (Exception $e) {
            $this->log('addAndModifyTablesForBlogFeature: warning: ' . $e->getMessage());
        }

        // create table `blog`
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}blog` (
                  `blog_id` int(11) NOT NULL AUTO_INCREMENT,
                  `author` int(11) NOT NULL,
                  `status` tinyint(4) NOT NULL,
                  `date_publish` datetime DEFAULT NULL,
                  `date_added` datetime NOT NULL,
                  `date_modified` datetime NOT NULL,
                  PRIMARY KEY (`blog_id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        $this->db->query($sql);

        // create table `blog`
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}blog_description` (
                  `blog_id` int(11) NOT NULL,
                  `language_id` int(11) NOT NULL,
                  `title` varchar(255) NOT NULL,
                  `content` text,
                  `short_content` text,
                  `image` varchar(255) DEFAULT NULL,
                  `meta_title` varchar(255) DEFAULT NULL,
                  `meta_description` text,
                  `alias` varchar(255) DEFAULT NULL,
                  PRIMARY KEY (`blog_id`,`language_id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        $this->db->query($sql);

        // create table `blog_category`
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}blog_category` (
                  `blog_category_id` int(11) NOT NULL AUTO_INCREMENT,
                  `status` tinyint(1) NOT NULL DEFAULT '0',
                  `date_added` datetime NOT NULL,
                  `date_modified` datetime NOT NULL,
                  PRIMARY KEY (`blog_category_id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        $this->db->query($sql);

        // create table `blog_category_description`
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}blog_category_description` (
                  `blog_category_id` int(11) NOT NULL,
                  `language_id` int(11) NOT NULL,
                  `title` varchar(255) NOT NULL,
                  `meta_title` varchar(255) NOT NULL,
                  `meta_description` text NOT NULL,
                  `alias` varchar(255) NOT NULL,
                  PRIMARY KEY (`blog_category_id`,`language_id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        $this->db->query($sql);

        // create table `blog_to_blog_category`
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}blog_to_blog_category` (
                  `blog_id` int(11) NOT NULL,
                  `blog_category_id` int(11) NOT NULL
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        $this->db->query($sql);

        // create table `blog_tag`
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}blog_tag` (
                  `blog_tag_id` int(11) NOT NULL AUTO_INCREMENT,
                  `blog_id` int(11) NOT NULL,
                  `tag_id` int(11) NOT NULL,
                  PRIMARY KEY (`blog_tag_id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
        $this->db->query($sql);

        // Dumping data for table `blog_category` and `blog_category_description` if not existed
        $blog_category_description = $this->db->query("SELECT * FROM `{$this->db_prefix}blog_category_description` WHERE `title` = 'Mặc định (Uncategorized)'");
        if (!$blog_category_description->rows) {
            // Dumping data for table `blog_category`
            $sql = "INSERT INTO `{$this->db_prefix}blog_category` (`blog_category_id`, `status`, `date_added`, `date_modified`) VALUES
                (1, 1, '2019-11-11 13:37:51', '2019-11-11 19:03:46');";
            $this->db->query($sql);
            $blog_category_id = $this->db->getLastId();

            // Dumping data for table `blog_category_description`
            $sql = "INSERT INTO `{$this->db_prefix}blog_category_description` (`blog_category_id`, `language_id`, `title`, `meta_title`, `meta_description`, `alias`) VALUES
                ({$blog_category_id}, 1, 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'danh-muc-mac-dinh-uncategorized'),
                ({$blog_category_id}, 2, 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'danh-muc-mac-dinh-uncategorized');";
            $this->db->query($sql);
        }
    }

    private function addPermissionForBlogAndBuilder()
    {
        $new_permissions = array(
            'section/content_customize',
            'blog/blog',
            'blog/category'
        );

        try {
            $admin_permissions = $this->db->query("SELECT * FROM `{$this->db_prefix}user_group` WHERE `user_group_id` IN (1, 10)");

            foreach ($admin_permissions->rows as $admin) {
                if (!is_array($admin) || !array_key_exists('user_group_id', $admin) || !array_key_exists('permission', $admin)) {
                    continue;
                }

                $admin_permission = json_decode($admin['permission'], true);

                // for new permission
                foreach ($new_permissions as $permission) {
                    if (is_array($admin_permission) && array_key_exists('access', $admin_permission) && is_array($admin_permission['access'])) {
                        if (!in_array($permission, $admin_permission['access'])) {
                            $admin_permission['access'][] = $permission;
                        }
                    }

                    if (is_array($admin_permission) && array_key_exists('modify', $admin_permission) && is_array($admin_permission['modify'])) {
                        if (!in_array($permission, $admin_permission['modify'])) {
                            $admin_permission['modify'][] = $permission;
                        }
                    }
                }

                $this->db->query("UPDATE `{$this->db_prefix}user_group` SET permission = '" . $this->db->escape(json_encode($admin_permission)) . "' WHERE `user_group_id` = '" . (int)$admin['user_group_id'] . "'");
            }
        } catch (\Exception $e) {
            // may columns existed for new shop! Catch to mark migrate done!
        }
    }

    private function addSeoForBlog()
    {
        // delete seo first if existed
        $this->deleteSeoFromBlog();

        // add seo
        $sql = "INSERT INTO `{$this->db_prefix}seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`, `table_name`) VALUES 
                    (NULL, '0', '1', 'blog/blog', 'blog', ''),
                    (NULL, '0', '2', 'blog/blog', 'bai-viet', '');";
        $this->db->query($sql);
    }

    private function deleteSeoFromBlog()
    {
        $sql = "DELETE FROM `{$this->db_prefix}seo_url` WHERE 1 
                  AND `query` IN ('blog/blog') 
                  AND `keyword` IN ('blog', 'bai-viet');";
        $this->db->query($sql);
    }

    /* === end - default functions === */
}