<?php


namespace Migration;


class migration_20201023_update_image_size extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20201023_update_image_size';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will update image size in images table');

        try {
            $this->resizeImages();
        } catch (\Exception $e) {
            $this->log('update image size got error: ' . $e->getMessage());
        }
    }

    protected function resizeImages()
    {
        /* only resize bestme image size = 0 (kb) */
        // get empty size images
        $sql = "SELECT `name` FROM `{$this->db_prefix}images` WHERE `source` = 'bestme' AND `size` = 0";
        $query = $this->db->query($sql);

        $image_names = [];
        foreach ($query->rows as $image) {
            $image_names[] = $image['name'];
        }

        // get shop name
        $query = $this->db->query("SELECT `value` FROM `{$this->db_prefix}setting` WHERE `key`= 'shop_name'");
        $shop_name = $query->row['value'];

        // call bestme image api to resize
        /** @var \Image\Bestme_Image_Manager $image_server */
        $image_server = \Image_Server::getImageServer('bestme');
        $data = [
            'shop' => $shop_name,
            'images' => json_encode($image_names)
        ];
        $result = $image_server->getImagesSize($data);

        if (!empty($result)) {
            $sql = "UPDATE `{$this->db_prefix}images` SET `size` = CASE ";
            foreach ($result as $name => $size) {
                $sql .= "WHEN `name` = '{$name}' THEN {$size} ";
            }
            $sql .= "END";

            $this->db->query($sql);
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back update image size in images table');
    }
}