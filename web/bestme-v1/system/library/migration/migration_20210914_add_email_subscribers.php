<?php

namespace Migration;

class migration_20210914_add_email_subscribers extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20210914_add_email_subscribers';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('add table email_subscribers');

        try {
            $this->addTableEmailSubscribers();
        } catch (\Exception $e) {
            $this->log('add table email_subscribers got error: ' . $e->getMessage());
        }

    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back add table email_subscribers');
    }

    protected function addTableEmailSubscribers()
    {
        $sql = "CREATE TABLE `{$this->db_prefix}email_subscribers` (
                  `email_id` int(11) NOT NULL AUTO_INCREMENT,
                  `email` text COLLATE utf8_unicode_ci NOT NULL,
                  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  PRIMARY KEY (email_id)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;";
        $this->db->query($sql);
    }
}