<?php

namespace Migration;

class migration_20220416_add_seo_url_events extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20220416_add_seo_url_events';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will add seo url events');

        try {
            $this->addSeoUrlEvents();
        } catch (\Exception $e) {
            $this->log('Add seo url events got error: ' . $e->getMessage());
        }
    }

    protected function addSeoUrlEvents()
    {
        $sql = "INSERT INTO `{$this->db_prefix}event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES 
                ('add_categories_sitemap_lastmod_after', 'admin/model/catalog/category/addCategory/after', 'event/seo_url/afterAddCategory', 1, 0),
                ('add_blog_categories_sitemap_lastmod_after', 'admin/model/blog/category/createBlogCategory/after', 'event/seo_url/afterCreateBlogCategory', 1, 0),
                ('add_collections_sitemap_lastmod_after', 'admin/model/catalog/collection/addCollection/after', 'event/seo_url/afterAddCollection', 1, 0),
                ('add_products_sitemap_lastmod_after', 'admin/model/catalog/product/addProduct/after', 'event/seo_url/afterAddProduct', 1, 0),
                ('add_manufacturers_sitemap_lastmod_after', 'admin/model/catalog/manufacturer/addManufacturer/after', 'event/seo_url/afterAddManufacturer', 1, 0),
                ('add_blogs_sitemap_lastmod_after', 'admin/model/blog/blog/addBlog/after', 'event/seo_url/afterAddBlog', 1, 0),
                
                ('delete_categories_sitemap_lastmod_after', 'admin/model/catalog/category/deleteCategory/after', 'event/seo_url/afterDeleteCategory', 1, 0),
                ('delete_blog_categories_sitemap_lastmod_after', 'admin/model/blog/category/deleteCategory/after', 'event/seo_url/afterDeleteBlogCategory', 1, 0),
                ('delete_collections_sitemap_lastmod_after', 'admin/model/catalog/collection/deleteCollection/after', 'event/seo_url/afterDeleteCollection', 1, 0),
                ('delete_products_sitemap_lastmod_before', 'admin/model/catalog/product/deleteProduct/before', 'event/seo_url/beforeDeleteProduct', 1, 0),
                ('delete_blogs_sitemap_lastmod_before', 'admin/model/blog/blog/deleteBlog/before', 'event/seo_url/beforeDeleteBlog', 1, 0),
                
                ('update_categories_sitemap_lastmod_before', 'admin/model/catalog/category/editCategory/before', 'event/seo_url/beforeEditCategory', 1, 0),
                ('update_blog_categories_sitemap_lastmod_before', 'admin/model/blog/category/editBlogCategory/before', 'event/seo_url/beforeEditBlogCategory', 1, 0),
                ('update_collections_sitemap_lastmod_before', 'admin/model/catalog/collection/editCollection/before', 'event/seo_url/beforeEditCollection', 1, 0),
                ('update_products_sitemap_lastmod_after', 'admin/model/catalog/product/editProduct/after', 'event/seo_url/afterEditProduct', 1, 0),
                ('update_manufacturers_sitemap_lastmod_before', 'admin/model/catalog/manufacturer/editManufacturer/before', 'event/seo_url/beforeEditManufacturer', 1, 0),
                ('update_blogs_sitemap_lastmod_after', 'admin/model/blog/blog/editBlog/after', 'event/seo_url/afterEditBlog', 1, 0);";
        $this->db->query($sql);
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back remove seo url event in event table');
    }
}