<?php

namespace Migration;

class migration_20201106_add_columns_for_report extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20201106_add_columns_for_report';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('add columns to table report ');
        try {
            $this->addColumnToReportTables();
        } catch (\Exception $e) {
            $this->log('add columns to table report got error: ' . $e->getMessage());
        }

        $this->log('update name oc_payment_voucher_type');
        try {
            $this->updateNamePaymentVoucherType();
        } catch (\Exception $e) {
            $this->log('update Name Payment Voucher Type got error: ' . $e->getMessage());
        }

        $this->log('update report data in new column');
        try {
            $this->updateDataReport();
        } catch (\Exception $e) {
            $this->log('update report data in new column error: ' . $e->getMessage());
        }

        $this->log('update data to table report order by return receipt');
        try {
            $this->dataForReportOrder();
        } catch (\Exception $e) {
            $this->log('update data to table report order by return receipt got error: ' . $e->getMessage());
        }

        $this->log('insert event when create return receipt');
        try {
            $this->insertEvent();
        } catch (\Exception $e) {
            $this->log('insert event when create return receipt got error: ' . $e->getMessage());
        }

        // permission for report
        try {
            $this->addPermissionForAdminUser([
                'report/product',
                'report/order',
                //'report/overview', // already
                'report/financial',
                //'report/online', // already
                //'report/report', // already
                'report/staff',
                //'report/statistics', // already
                'report/store'
            ]);
        } catch (\Exception $e) {
            $this->log('addPermissionForAdminUser got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back column payment_status table report version');
    }

    protected function insertEvent()
    {
        $old_event = $this->db->query("SELECT * from `{$this->db_prefix}event` where code='update_report_when_create_return_receipt'");
        if(!$old_event->row) {
            $sql = "INSERT INTO `{$this->db_prefix}event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES 
                ('update_report_when_create_return_receipt', 'admin/model/sale/return_receipt/addReturnReceipt/after', 'event/report_order/afterCreateReturnReceipt', 1, 0);";
            $this->db->query($sql);
        }
    }

    protected function addColumnToReportTables()
    {
        // report order
        $sql = "ALTER TABLE `{$this->db_prefix}report_order`
                  ADD `shipping_fee`DECIMAL(18,4) NOT NULL DEFAULT '0.0000' AFTER `total_amount`,
                  ADD `discount` DECIMAL(18,4) NOT NULL DEFAULT '0.0000' AFTER `shipping_fee`,
                  ADD `source` VARCHAR(50) NULL DEFAULT NULL AFTER `order_status`,
                  ADD `total_return` DECIMAL(18,4) NOT NULL DEFAULT '0.0000' AFTER `total_amount`;";
        $this->db->query($sql);

        // report product
        $sql = "ALTER TABLE `{$this->db_prefix}report_product` 
                  ADD `cost_price` DECIMAL(18,4) NULL DEFAULT '0.0000' AFTER `total_amount`,
                  ADD `discount` DECIMAL(18,4) NOT NULL DEFAULT '0.0000' AFTER `quantity`,
                  ADD `return_amount` DECIMAL(18,4) NULL DEFAULT '0.0000' AFTER `discount`,
                  ADD `quantity_return` INT(11) NULL DEFAULT '0' AFTER `return_amount`;";
        $this->db->query($sql);
    }

    protected function updateNamePaymentVoucherType()
    {
        // migrate update `name` in table oc_payment_voucher_type  for `payment_voucher_type_id`= 7
        // rename value 'Chi phí quản lý cửa hàng'
        $sql = "UPDATE `{$this->db_prefix}payment_voucher_type` 
                   SET `name` = 'Chi phí thuế TNDN' 
                WHERE `payment_voucher_type_id` = 7 
                  AND `language_id` = 2;";
        $this->db->query($sql);

        // rename value 'Store management cost'
        $sql = "UPDATE `{$this->db_prefix}payment_voucher_type` 
                   SET `name` = 'Enterprise income tax expenses' 
                WHERE `payment_voucher_type_id` = 7 
                  AND `language_id` = 1;";
        $this->db->query($sql);
    }

    protected function updateDataReport()
    {
        $DB_PREFIX = $this->db_prefix;

        // get orders
        $sql = "SELECT shipping_fee, discount, source, order_id
                  FROM `{$DB_PREFIX}order`";
        $orders = $this->db->query($sql)->rows;

        // get products in order
        $sql = "SELECT op.*, pts.cost_price AS cost_price
                FROM `{$DB_PREFIX}order_product` op
                    LEFT JOIN {$DB_PREFIX}product p ON p.product_id = op.product_id
                    LEFT JOIN {$DB_PREFIX}product_to_store pts ON p.product_id = pts.product_id AND p.default_store_id = pts.store_id
                WHERE if(p.multi_versions = 1, pts.product_version_id, 1) = if(p.multi_versions = 1, op.product_version_id, 1)";
        $order_products = $this->db->query($sql)->rows;

        // update order_report
        foreach ($orders as $order) {
            if (!is_array($order) ||
                !array_key_exists('order_id', $order) ||
                !array_key_exists('shipping_fee', $order) ||
                !array_key_exists('discount', $order) ||
                !array_key_exists('source', $order)
            ) {
                continue;
            }

            $order_product_in_this_order = array_filter($order_products, function ($order_product) use($order) {
                return $order_product['order_id'] == $order['order_id'];
            });
            $total_discount_for_product = array_sum(array_column($order_product_in_this_order, 'discount'));

            $this->db->query("UPDATE `{$DB_PREFIX}report_order` 
                                 SET `shipping_fee` = " . (double)$order['shipping_fee'] . ", 
                                     `discount` = " . ((double)$order['discount'] + (double)$total_discount_for_product) . ", 
                                     `source` = '" . $this->db->escape($order['source']) . "'
                              WHERE `order_id` = " . $order['order_id']);
        }

        // calculate product discount
        foreach ($order_products as $order_product) {
            $order_key = $this->getOrderKey($order_product['order_id'], $orders);
            if ($order_key === false) {
                continue;
            }

            if (isset($orders[$order_key]['total_before_discount'])) {
                $orders[$order_key]['total_before_discount'] += $order_product['quantity'] * $order_product['price'];
            } else {
                $orders[$order_key]['total_before_discount'] = $order_product['quantity'] * $order_product['price'];
            }
        }

        foreach ($order_products as &$order_product) {
            $order = $orders[$this->getOrderKey($order_product['order_id'], $orders)];
            if ($order === false) {
                continue;
            }

            if ($order['total_before_discount'] == 0) {
                $order_product['discount_with_order_discount_be_divided'] = 0;
            } else {
                $percent_price = $order_product['quantity'] * $order_product['price'] / $order['total_before_discount'];
                $order_product['discount_with_order_discount_be_divided'] = $order_product['discount'] + $percent_price * (int)$order['discount'];
            }
        }

        // update report_order
        foreach ($order_products as $order_product) {
            $product_version_id = isset($order_product['product_version_id']) && !empty($order_product['product_version_id'])
                ? $order_product['product_version_id']
                : 0;

            $this->db->query("UPDATE {$DB_PREFIX}report_product 
                                 SET `cost_price` = " . (double)$order_product['cost_price'] . ",
                                     `discount` = " . (double)$order_product['discount_with_order_discount_be_divided'] . "
                              WHERE `order_id` = " . $order_product['order_id'] . "
                                AND `product_id` = " . $order_product['product_id'] . "
                                AND `product_version_id` = " . $product_version_id);
        }
    }

    private function getOrderKey($order_id, $orders)
    {
        return array_search($order_id, array_column($orders, 'order_id'));
    }

    protected function dataForReportOrder()
    {
        $sql = "SELECT `order_id`, SUM(`total`) as total 
                FROM `{$this->db_prefix}return_receipt` 
                GROUP BY `order_id`";
        $query = $this->db->query($sql);

        foreach ($query->rows as $receipt) {
            $this->updateDataReportOrder($receipt);
        }
    }

    protected function updateDataReportOrder($data)
    {
        if (!isset($data['order_id']) ||
            !isset($data['total'])) {
            return;
        }

        try {
            $sql = "UPDATE `{$this->db_prefix}report_order` 
                      SET `total_return` = {$data['total']} 
                    WHERE `order_id` = {$data['order_id']}";

            $this->db->query($sql);
        } catch (\Exception $e) {
            $this->log('updateDataReportOrder got error: ' . $e->getMessage());
        }
    }
}