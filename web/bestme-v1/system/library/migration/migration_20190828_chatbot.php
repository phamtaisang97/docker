<?php

namespace Migration;

class migration_20190828_chatbot extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20190828_chatbot';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate add permission chatbot "sale_channel/chatbot_novaon" to admin user');

        $this->addPermissionForChatbot();
    }

    protected function addPermissionForChatbot()
    {
        $new_permissions = array(
            'common/delivery_api',
            'sale_channel/chatbot_novaon'
        );

        try {
            $admin_permissions = $this->db->query("SELECT * FROM `{$this->db_prefix}user_group` WHERE `user_group_id` IN (1, 10)");

            foreach ($admin_permissions->rows as $admin) {
                if (!is_array($admin) || !array_key_exists('user_group_id', $admin) || !array_key_exists('permission', $admin)) {
                    continue;
                }

                $admin_permission = json_decode($admin['permission'], true);

                // for new permission
                foreach ($new_permissions as $permission) {
                    if (is_array($admin_permission) && array_key_exists('access', $admin_permission) && is_array($admin_permission['access'])) {
                        if (!in_array($permission, $admin_permission['access'])) {
                            $admin_permission['access'][] = $permission;
                        }
                    }

                    if (is_array($admin_permission) && array_key_exists('modify', $admin_permission) && is_array($admin_permission['modify'])) {
                        if (!in_array($permission, $admin_permission['modify'])) {
                            $admin_permission['modify'][] = $permission;
                        }
                    }
                }

                $this->db->query("UPDATE `{$this->db_prefix}user_group` SET permission = '" . $this->db->escape(json_encode($admin_permission)) . "' WHERE `user_group_id` = '" . (int)$admin['user_group_id'] . "'");
            }
        } catch (\Exception $e) {
            // may columns existed for new shop! Catch to mark migrate done!
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back permission chatbot "sale_channel/chatbot_novaon" from admin user');
        // no need
    }
}