<?php

namespace Migration;

class migration_20200821_lazada_sync_order_again extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200821_lazada_sync_order_again';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate create tables for lazada_sync_order version');
        try {
            $this->migrateCreateTablesForLazadaSyncOrder();

        } catch (\Exception $e) {
            $this->log('Create tables for lazada_sync_order version got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back remove some tables of lazada_sync_order version');
    }

    protected function migrateCreateTablesForLazadaSyncOrder()
    {
        // Lazada_order
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}lazada_order` ( 
                  `id` INT(11) NOT NULL AUTO_INCREMENT , 
                  `order_id` BIGINT(64) NOT NULL , 
                  `fullname` VARCHAR(256) NULL COMMENT 'address_shipping name' , 
                  `phone` VARCHAR(32) NULL COMMENT 'address_shipping phone' , 
                  `ward` VARCHAR(256) NULL COMMENT 'address_billing address5' , 
                  `district` VARCHAR(256) NULL COMMENT 'address_billing address4' , 
                  `province` VARCHAR(256) NULL COMMENT 'address_billing address3' , 
                  `full_address` VARCHAR(256) NULL COMMENT 'address_billing address1' , 
                  `shipping_fee_original` DECIMAL(18,4) NULL , 
                  `shipping_fee` DECIMAL(18,4) NULL , 
                  `shipping_method` VARCHAR(256) NULL COMMENT 'price' , 
                  `total_amount` DECIMAL(18.4) NULL , 
                  `order_status` VARCHAR(64) NULL , 
                  `payment_method` VARCHAR(256) NULL , 
                  `payment_status` TINYINT(2) NULL , 
                  `voucher` DECIMAL(18,4) NULL , 
                  `voucher_seller` DECIMAL(18,4) NULL , 
                  `remarks` VARCHAR(512) NULL , 
                  `create_time` DATETIME NULL , 
                  `update_time` DATETIME NULL , 
                  `lazada_shop_id` INT(11) NOT NULL , 
                  `bestme_order_id` INT(11) NULL , 
                  `sync_status` TINYINT(2) NULL , 
                  `is_edited` TINYINT(2) NULL , 
                  `sync_time` DATETIME NULL , 
                  PRIMARY KEY (`id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);

        // Lazada_order_to_product
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}lazada_order_to_product` (
                  `lazada_order_id` BIGINT(64) NOT NULL , 
                  `sku` VARCHAR(128) NULL , 
                  `shop_sku` VARCHAR(128) NULL , 
                  `variation` VARCHAR(256) NULL ,
                  `name` VARCHAR(256) NULL , 
                  `quantity` INT(11) NOT NULL DEFAULT 0, 
                  `item_price` DECIMAL(18,4) NULL , 
                  `paid_price` DECIMAL(18,4) NULL 
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);
    }
}