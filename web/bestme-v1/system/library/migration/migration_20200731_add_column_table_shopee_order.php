<?php

namespace Migration;

class migration_20200731_add_column_table_shopee_order extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200731_add_column_table_shopee_order';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('add column payment_status table shopee_order');
        try {
            $this->addNewRowInTableShopeeOrder();

        } catch (\Exception $e) {
            $this->log('add column payment_status table shopee_order got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back column payment_status table shopee_order version');
    }

    protected function addNewRowInTableShopeeOrder()
    {
        // shopee_order
        $sql = "ALTER TABLE `{$this->db_prefix}shopee_order` ADD `payment_status` TINYINT(2) NULL DEFAULT '0' AFTER `payment_method`;";

        $this->db->query($sql);
    }
}