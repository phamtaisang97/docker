<?php

namespace Migration;

use DateTime;

abstract class migration_abstract
{
    /** @var \DB */
    protected $db;
    /** @var \Log */
    protected $log;
    protected $db_prefix;
    protected $migration;

    const RUNNING = 2;
    const NOT_RUN = 0;
    const COMPLETE = 1;

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration, $version = 1)
    {
        if (empty($db_prefix) || empty($migration)) {
            throw new \Exception('Required params in constructor: (\DB $db, \Log $log, string: $db_prefix, string: $migration)');
        }

        $this->db = $db;
        $this->log = $log;
        $this->db_prefix = $db_prefix;
        $this->migration = $migration;
        $this->version = $version;
    }

    /* === default functions === */

    /**
     * up version
     */
    public function up()
    {
        $this->log('up...');

        $this->update_running();

        $this->do_up();

        $this->update_already_run();

        $this->log('up... done!');
    }

    /**
     * down version
     */
    public function down()
    {
        $this->log('down...');

        $this->update_running();

        $this->do_down();

        $this->update_not_run();

        $this->log('down... done!');
    }

    /**
     * do up version
     */
    public abstract function do_up();

    /**
     * do down version
     */
    public abstract function do_down();

    /* === end - default functions === */

    private function update_already_run()
    {
        $this->update_version_status(self::COMPLETE);
    }

    private function update_not_run()
    {
        $this->update_version_status(self::NOT_RUN);
    }

    private function update_running()
    {
        $this->update_version_status(self::RUNNING);
    }

    private function update_version_status($run)
    {
        $this->log('Update version status: ' . ($run === self::COMPLETE ? 'run' : ($run === self::NOT_RUN ? 'not run' : 'running')));

        // create migration table if not existed
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}migration` (
              `migration_id` int(11) NOT NULL AUTO_INCREMENT,
              `migration` varchar(255) NOT NULL,
              `run` tinyint(1) NOT NULL DEFAULT 1,
              `date_run` datetime NOT NULL,
              PRIMARY KEY (`migration_id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";

        $query = $this->db->query($sql);
        if (!$query) {
            return false;
        }

        // get existing version
        $sql = "SELECT * FROM `{$this->db_prefix}migration` WHERE `migration` = '$this->migration'";
        $query = $this->db->query($sql);
        if (!$query->row) {
            // create new
            try {
                $sql = "INSERT INTO `{$this->db_prefix}migration` (`migration`, `run`, `date_run`, `version`) VALUES 
                ('$this->migration', $run, NOW(), $this->version);";
                $query = $this->db->query($sql);
            } catch (\Exception $e) {
                try {
                    // trying add column `version`
                    $this->db->query("ALTER TABLE `{$this->db_prefix}migration` ADD version INT(11) NULL DEFAULT 1");
                } catch (\Exception $e2) {
                    // do something...
                }

                $sql = "INSERT INTO `{$this->db_prefix}migration` (`migration`, `run`, `date_run`) VALUES 
                ('$this->migration', $run, NOW());";
                $query = $this->db->query($sql);
            }

            if (!$query) {
                return false;
            }

            return true;
        }

        // else: edit
        $sql = "UPDATE `{$this->db_prefix}migration` SET `run` = $run, `version`= $this->version WHERE `migration` = '$this->migration'";
        $this->db->query($sql);

        return true;
    }

    protected function addPermissionForAdminUser(array $new_permissions)
    {
        $admin_permissions = $this->db->query("SELECT * FROM `{$this->db_prefix}user_group` WHERE `user_group_id` IN (1, 10)");
        if (!isset($admin_permissions->rows) || !is_array($admin_permissions->rows)) {
            return;
        }

        foreach ($admin_permissions->rows as $admin) {
            if (!is_array($admin) || !array_key_exists('user_group_id', $admin) || !array_key_exists('permission', $admin)) {
                continue;
            }

            $admin_permission = json_decode($admin['permission'], true);

            // for new permission
            foreach ($new_permissions as $permission) {
                if (is_array($admin_permission) && array_key_exists('access', $admin_permission) && is_array($admin_permission['access'])) {
                    if (!in_array($permission, $admin_permission['access'])) {
                        $admin_permission['access'][] = $permission;
                    }
                }

                if (is_array($admin_permission) && array_key_exists('modify', $admin_permission) && is_array($admin_permission['modify'])) {
                    if (!in_array($permission, $admin_permission['modify'])) {
                        $admin_permission['modify'][] = $permission;
                    }
                }
            }

            $this->db->query("UPDATE `{$this->db_prefix}user_group` 
                              SET permission = '" . $this->db->escape(json_encode($admin_permission)) . "' 
                              WHERE `user_group_id` = '" . (int)$admin['user_group_id'] . "'");
        }
    }

    /**
     * @param array $depend_permissions format as:
     * [
     *     <condition permission> => [ <new permissions> ]
     * ]
     * e.g:
     * [
     *     'settings/account' => ['settings/classify']
     * ]
     */
    protected function addPermissionForStaffUser(array $depend_permissions)
    {
        $staffs = $this->db->query("SELECT * FROM `{$this->db_prefix}user_group` WHERE `user_group_id` NOT IN (1, 10)");
        if (!isset($staffs->rows) || !is_array($staffs->rows)) {
            return;
        }

        foreach ($staffs->rows as $staff) {
            if (!array_key_exists('user_group_id', $staff) || !array_key_exists('permission', $staff)) {
                continue;
            }

            $staffPermission = json_decode($staff['permission'], true);
            if (!is_array($staffPermission)) {
                continue;
            }

            // for depend permission
            foreach ($depend_permissions as $condition => $new_permissions) {
                if (!is_array($new_permissions)) {
                    continue;
                }

                if (array_key_exists('access', $staffPermission) && in_array($condition, $staffPermission['access'])) {
                    foreach ($new_permissions as $permission) {
                        if (!in_array($permission, $staffPermission['access'])) {
                            $staffPermission['access'][] = $permission;
                        }
                    }
                }

                if (array_key_exists('modify', $staffPermission) && in_array($condition, $staffPermission['modify'])) {
                    foreach ($new_permissions as $permission) {
                        if (!in_array($permission, $staffPermission['modify'])) {
                            $staffPermission['modify'][] = $permission;
                        }
                    }
                }
            }

            $this->db->query("UPDATE `{$this->db_prefix}user_group` 
                              SET permission = '" . $this->db->escape(json_encode($staffPermission)) . "' 
                              WHERE `user_group_id` = '" . (int)$staff['user_group_id'] . "'");
        }
    }

    public function log($message)
    {
        $this->log->write(sprintf("[%s] %s", (new DateTime())->format('YmdHis'), $message));
    }

    protected function getCacheFile()
    {
        $data_json = file_get_contents(DIR_SYSTEM . 'library/migration/data/' . $this->migration . '.json');

        return json_decode($data_json, true);
    }
}