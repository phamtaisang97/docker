<?php

namespace Migration;

use Exception;
use Redis;

class migration_20200916_pull_image_from_cloudinary extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200916_pull_image_from_cloudinary';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will push new job pull image to redis');
        try {
            $this->pushToRedis();
        } catch (\Exception $e) {
            $this->log('push cronjob to redis got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back ....');
    }

    private function pushToRedis()
    {
        $query = $this->db->query("SELECT `value` FROM `{$this->db_prefix}setting` WHERE `key`= 'shop_name'");
        $shop_name = $query->row['value'];

        /* create job */
        $job = [
            'db_hostname' => DB_HOSTNAME,
            'db_port' => DB_PORT,
            'db_username' => DB_USERNAME,
            'db_password' => DB_PASSWORD,
            'db_database' => DB_DATABASE,
            'db_prefix' => DB_PREFIX,
            'db_driver' => DB_DRIVER,
            'shop_name' => $shop_name,
            'task' => 'PULL_IMAGES_FROM_CLOUDINARY'
        ];

        $redis = $this->redisConnect();
        $redis->lPush(BESTME_IMAGE_JOB_REDIS_QUEUE, json_encode($job));
        $this->redisClose($redis);
    }

    /**
     * @return null|Redis
     */
    private function redisConnect()
    {
        $redis = null;

        try {
            $redis = new \Redis();
            $redis->connect(MUL_REDIS_HOST, MUL_REDIS_PORT);
            $redis->select(MUL_REDIS_DB_BESTME_IMAGE);
        } catch (Exception $e) {
            // could not connect to redis
        }

        return $redis;
    }

    /**
     * @param Redis $redis
     */
    private function redisClose($redis)
    {
        if (!$redis instanceof \Redis) {
            return;
        }

        try {
            $redis->close();
        } catch (Exception $e) {
            // could not connect to redis
        }
    }
}