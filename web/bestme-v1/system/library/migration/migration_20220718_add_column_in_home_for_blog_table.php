<?php

namespace Migration;

class migration_20220718_add_column_in_home_for_blog_table extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20220718_add_column_in_home_for_blog_table';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('add column in_home to table blog');

        try {
            $this->addColumnInHomeBlogTable();
        } catch (\Exception $e) {
            $this->log('add column in_home to table blog got error: ' . $e->getMessage());
        }

    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back add column in_home to table blog');
    }

    protected function addColumnInHomeBlogTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}blog` 
                ADD `in_home` TINYINT(4) NOT NULL DEFAULT '0' AFTER `source`;";
        $this->db->query($sql);
    }
}