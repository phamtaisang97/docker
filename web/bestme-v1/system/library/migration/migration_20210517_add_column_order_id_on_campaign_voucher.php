<?php

namespace Migration;

class migration_20210517_add_column_order_id_on_campaign_voucher extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20210517_add_column_order_id_on_campaign_voucher';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('add columns add column order_id on table campaign_voucher');

        try {
            $this->addColumnToCampaignVoucher();
        } catch (\Exception $e) {
            $this->log('add columns add column order_id on table campaign_voucher got error: ' . $e->getMessage());
        }

    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back column version');
    }

    protected function addColumnToCampaignVoucher()
    {
     $sql = "ALTER TABLE `{$this->db_prefix}campaign_voucher` 
            ADD `order_id` INT(11) NULL DEFAULT NULL AFTER `campaign_voucher_id`;";
     $this->db->query($sql);
    }
}