<?php


namespace Migration;


use Matrix\Exception;

class migration_20220728_add_on_deal extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20220728_add_on_deal';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('migrateAddOnDealTable');
        try {
            $this->migrateAddOnDealTable();
        } catch (\Exception $e) {
            $this->log('migrateAddOnDealTable got error: ' . $e->getMessage());
        }

        // permission for admin user
        try {
            $this->addPermissionForAdminUser([
                'discount/add_on_deal'
            ]);
        } catch (\Exception $e) {
            $this->log('addPermissionForAdminUser (add_on_deal) got error: ' . $e->getMessage());
        }
    }

    private function migrateAddOnDealTable()
    {
        $sql = "CREATE TABLE `{$this->db_prefix}add_on_deal` ( 
                `add_on_deal_id` INT(11) NOT NULL AUTO_INCREMENT , 
                `name` VARCHAR(255) NULL DEFAULT NULL , 
                `date_started` DATETIME NULL DEFAULT NULL , 
                `date_ended` DATETIME NULL DEFAULT NULL , 
                `limit_quantity_product` INT(11) NULL DEFAULT NULL , 
                `status` INT(11) NOT NULL DEFAULT '1' , 
                `deleted` TINYINT(4) NULL DEFAULT NULL , 
                `date_modify` DATETIME NULL DEFAULT NULL , 
                PRIMARY KEY (`add_on_deal_id`)) ENGINE = MyISAM;";
        $this->db->query($sql);

        $sql = "CREATE TABLE `{$this->db_prefix}add_on_deal_main_product` ( 
                `main_product_id` INT(11) NOT NULL AUTO_INCREMENT , 
                `add_on_deal_id` INT(11) NOT NULL ,
                `product_id` INT(11) NOT NULL , 
                `product_version_id` INT(11) NULL DEFAULT '0' , 
                `status` INT(11) NULL DEFAULT '1' , 
                PRIMARY KEY (`main_product_id`)) ENGINE = MyISAM;";
        $this->db->query($sql);

        $sql = "CREATE TABLE `{$this->db_prefix}add_on_product` ( 
                `add_on_product_id` INT(11) NOT NULL AUTO_INCREMENT , 
                `add_on_deal_id` INT(11) NOT NULL , 
                `product_id` INT(11) NOT NULL ,
                `product_version_id` INT(11) NOT NULL , 
                `limit_quantity` INT(11) NULL DEFAULT NULL , 
                `discount_price` DECIMAL(15,4) NULL DEFAULT NULL , 
                `status` INT(11) NULL DEFAULT '1' , 
                PRIMARY KEY (`add_on_product_id`)) ENGINE = MyISAM;";
        $this->db->query($sql);

        $sql = "CREATE TABLE `{$this->db_prefix}add_on_deal_history` ( 
                `history_id` INT(11) NOT NULL AUTO_INCREMENT , 
                `add_on_deal_id` INT(11) NOT NULL , 
                `user_id` INT(11) NOT NULL , 
                `date_added` DATETIME NOT NULL , 
                `action` VARCHAR(255) NOT NULL , 
                `reson` VARCHAR(255) NOT NULL , 
                `date_started` DATETIME NOT NULL , 
                `date_ended` DATETIME NOT NULL , 
                PRIMARY KEY (`history_id`)) ENGINE = InnoDB;";
        $this->db->query($sql);

        $sql = "CREATE TABLE `{$this->db_prefix}order_add_on_product` (
                    `order_add_on_product_id` INT(11) NOT NULL AUTO_INCREMENT,
                    `order_id` INT(11) NOT NULL,
                    `main_product_id` INT(11) NOT NULL,
                    `main_product_version_id` INT(11) NOT NULL DEFAULT '0',
                    `product_id` INT(11) NOT NULL,
                    `product_version_id` INT(11) NOT NULL DEFAULT '0',
                    PRIMARY KEY (`order_add_on_product_id`)
                ) ENGINE = MyISAM
                COLLATE='utf8mb4_unicode_ci'
                ;";
        $this->db->query($sql);

        $sql = "ALTER TABLE `{$this->db_prefix}order_product` ADD `add_on_deal_id` INT(11) NULL DEFAULT NULL AFTER `discount`;";
        $this->db->query($sql);

        $sql = "ALTER TABLE `{$this->db_prefix}add_on_product`
	                ADD COLUMN `current_quantity` INT(10) NULL DEFAULT NULL AFTER `limit_quantity`;";
        $this->db->query($sql);
        
        $sql = "ALTER TABLE `{$this->db_prefix}add_on_deal`
	                ADD COLUMN `date_modify` DATETIME NULL DEFAULT NULL AFTER `deleted`;";
        $this->db->query($sql);
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back migration AddOnDealTable');
    }
}