<?php


namespace Migration;


class migration_20210302_demo_data extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20210302_demo_data';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /**
     * @inheritDoc
     */
    public function do_up()
    {
        try {
            // permission for admin user
            $this->addPermissionForAdminUser([
                'demo_data/demo_data',
            ]);
        } catch (\Exception $e) {
            $this->log('Create permission for user admin v3.1.3 demo_data/demo_data  got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function do_down()
    {
        $this->log('Will roll back v3.1.3_demo_data... update later...');
    }
}