<?php

namespace Migration;

class migration_20220611_add_vnpay_to_my_app extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20220611_add_vnpay_to_my_app';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('insert vnpay to my_app table');
        try {
            $this->addColumnMetaKeywordBlogTable();

        } catch (\Exception $e) {
            $this->log('insert vnpay to my_app table got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back insert vnpay to my_app table');
    }

    protected function addColumnMetaKeywordBlogTable()
    {
        $sql = "INSERT INTO `{$this->db_prefix}my_app` (`app_code`, `status`, `expiration_date`, `app_name`, `path_logo`, `path_asset`, `trial`, `installed`, `date_added`, `date_modified`) VALUES ('paym_ons_vnpay', 1, NULL, 'VNPay', 'view/image/appstore/paym_ons_vnpay.png', NULL, 0, 1, NOW(), NOW());";
        $this->db->query($sql);
    }
}