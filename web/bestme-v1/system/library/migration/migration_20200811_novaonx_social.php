<?php

namespace Migration;

class migration_20200811_novaonx_social extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200811_novaonx_social';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate add permission novaonx_social "sale_channel/novaonx_social" to admin user');

        // permission for admin user
        try {
            $this->addPermissionForAdminUser([
                'sale_channel/novaonx_social'
            ]);
        } catch (\Exception $e) {
            $this->log('addPermissionForAdminUser (novaonx_social) got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back permission novaonx_social "sale_channel/novaonx_social" from admin user');
        // no need
    }
}