<?php

namespace Migration;

class migration_20200212_discount extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200212_discount';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate add permission discount "discount/discount" to admin user');

        $this->addPermissionForDiscount();

        // create new discount tables
        try {
            $this->migrateCreatingNewDiscountTables();
        } catch (\Exception $e) {
            $this->log('migrateCreatingNewDiscountTables got error: ' . $e->getMessage());
        }

        // create data tables discount type and status
        try {
            $this->dataTableDiscountStatusAndDiscountType();
        } catch (\Exception $e) {
            $this->log('dataTableDiscountStatusAndDiscountType got error: ' . $e->getMessage());
        }
    }

    protected function addPermissionForDiscount()
    {
        $new_permissions = array(
            'discount/discount',
            'discount/coupon',
            'discount/setting'
        );

        try {
            $admin_permissions = $this->db->query("SELECT * FROM `{$this->db_prefix}user_group` WHERE `user_group_id` IN (1, 10)");

            foreach ($admin_permissions->rows as $admin) {
                if (!is_array($admin) || !array_key_exists('user_group_id', $admin) || !array_key_exists('permission', $admin)) {
                    continue;
                }

                $admin_permission = json_decode($admin['permission'], true);

                // for new permission
                foreach ($new_permissions as $permission) {
                    if (is_array($admin_permission) && array_key_exists('access', $admin_permission) && is_array($admin_permission['access'])) {
                        if (!in_array($permission, $admin_permission['access'])) {
                            $admin_permission['access'][] = $permission;
                        }
                    }

                    if (is_array($admin_permission) && array_key_exists('modify', $admin_permission) && is_array($admin_permission['modify'])) {
                        if (!in_array($permission, $admin_permission['modify'])) {
                            $admin_permission['modify'][] = $permission;
                        }
                    }
                }

                $this->db->query("UPDATE `{$this->db_prefix}user_group` SET permission = '" . $this->db->escape(json_encode($admin_permission)) . "' WHERE `user_group_id` = '" . (int)$admin['user_group_id'] . "'");
            }
        } catch (\Exception $e) {
            // may columns existed for new shop! Catch to mark migrate done!
        }
    }

    private function migrateCreatingNewDiscountTables()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}discount` ( 
                  `discount_id` INT(11) NOT NULL AUTO_INCREMENT , 
                  `code` VARCHAR(50) NOT NULL , 
                  `discount_type_id` INT(11) NOT NULL , 
                  `discount_status_id` INT(11) NOT NULL , 
                  `usage_limit` INT(11) NOT NULL DEFAULT '0' , 
                  `times_used` INT(11) NOT NULL DEFAULT '0' ,
                  `config` MEDIUMTEXT NULL DEFAULT NULL , 
                  `order_source` VARCHAR(128) NULL DEFAULT NULL , 
                  `customer_group` VARCHAR(128) NULL DEFAULT 'All' , 
                  `start_at` DATETIME NOT NULL , 
                  `end_at` DATETIME NULL , 
                  `pause_reason` VARCHAR(256) NULL,
                  `paused_at` DATETIME NULL,
                  `delete_reason` VARCHAR(256) NULL,
                  `deleted_at` DATETIME NULL,
                  `created_at` DATETIME NULL , 
                  `updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP , 
                  PRIMARY KEY (`discount_id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";
        $this->db->query($sql);

        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}discount_description` (
                  `discount_id` INT(11) NOT NULL , 
                  `language_id` INT(11) NOT NULL ,
                  `name` VARCHAR(256) NOT NULL ,
                  `description` VARCHAR(256) NOT NULL ,
                  PRIMARY KEY (discount_id, `language_id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";
        $this->db->query($sql);

        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}discount_to_store` (
                  `discount_id` INT(11) NOT NULL , 
                  `store_id` INT(11) NOT NULL , 
                  PRIMARY KEY (`discount_id`, `store_id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";
        $this->db->query($sql);

        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}discount_type` ( 
                  `discount_type_id` INT(11) NOT NULL , 
                  `language_id` INT(11) NOT NULL , 
                  `name` VARCHAR(128) NOT NULL , 
                  `description` VARCHAR(128) NULL , 
                  PRIMARY KEY (`discount_type_id`, `language_id` )
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";
        $this->db->query($sql);

        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}discount_status` ( 
                  `discount_status_id` INT(11) NOT NULL , 
                  `language_id` INT(11) NOT NULL , 
                  `name` VARCHAR(128) NOT NULL , 
                  `description` VARCHAR(128) NULL , 
                  PRIMARY KEY (`discount_status_id`, `language_id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";
        $this->db->query($sql);

        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}order_to_discount` (
                  `order_id` INT(11) NOT NULL ,
                  `discount_id` INT(11) NOT NULL ,
                  PRIMARY KEY (`order_id`, `discount_id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";
        $this->db->query($sql);

        // add discount to table order_product
        try {
            $sql = "ALTER TABLE `{$this->db_prefix}order_product` 
                    ADD `discount` DECIMAL(18,4) NOT NULL DEFAULT '0.0000' AFTER `price`;";
            $this->db->query($sql);
        } catch (\Exception $e) {
            $this->log('migrateCreatingNewDiscountTables got error: add discount to table order_product: ' . $e->getMessage());
        }

        // add discount to table order
        try {
            $sql = "ALTER TABLE `{$this->db_prefix}order` 
                    ADD `discount` DECIMAL(18,4) NOT NULL DEFAULT '0.0000' AFTER `order_cancel_comment`;";
            $this->db->query($sql);
        } catch (\Exception $e) {
            $this->log('migrateCreatingNewDiscountTables got error: add discount to table order: ' . $e->getMessage());
        }
    }

    private function dataTableDiscountStatusAndDiscountType()
    {
        $sql = "DELETE FROM `{$this->db_prefix}discount_status` WHERE `discount_status_id` IN (1,2,3,4)";
        $this->db->query($sql);

        $sql = "DELETE FROM `{$this->db_prefix}discount_type` WHERE `discount_type_id` IN (1,2,3,4)";
        $this->db->query($sql);

        $sql = "INSERT INTO `{$this->db_prefix}discount_status` (`discount_status_id`, `language_id`, `name`, `description`) VALUES 
                ('1', '2', 'Đã lập lịch', NULL), 
                ('2', '2', 'Đang hoạt động', NULL), 
                ('3', '2', 'Hết hạn', NULL), 
                ('4', '2', 'Đã xóa', NULL), 
                ('1', '1', 'Scheduled', NULL), 
                ('2', '1', 'Activated', NULL), 
                ('3', '1', 'Paused', NULL), 
                ('4', '1', 'Deleted', NULL);";
        $this->db->query($sql);

        $sql = "INSERT INTO `{$this->db_prefix}discount_type` (`discount_type_id`, `language_id`, `name`, `description`) VALUES 
                ('1', '2', 'Chiết khấu theo tổng giá trị đơn hàng', NULL), 
                ('2', '2', 'Chiết khấu theo từng sản phẩm', NULL), 
                ('3', '2', 'Chiết khấu theo loại sản phẩm', NULL), 
                ('4', '2', 'Chiết khấu theo nhà cung cấp', NULL), 
                ('1', '1', 'Discount by total order value', NULL), 
                ('2', '1', 'Discount by each product', NULL), 
                ('3', '1', 'Discount by product type', NULL), 
                ('4', '1', 'Discount by supplier', NULL);";
        $this->db->query($sql);
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back permission discount "discount/discount" from admin user');
        // no need
    }
}