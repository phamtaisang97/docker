<?php

namespace Migration;

class migration_20210525_update_migrate_v3_4_2_add_new_table_order_channel extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20210525_update_migrate_v3_4_2_add_new_table_order_channel';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('add table order channel');

        try {
            $this->addTableOrderChannel();
        } catch (\Exception $e) {
            $this->log('add table order channel got error: ' . $e->getMessage());
        }

    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back add table order channel');
    }

    protected function addTableOrderChannel()
    {
        $sql = "CREATE TABLE `{$this->db_prefix}order_channel` ( 
                    `order_id` INT(11) NOT NULL , 
                    `channel_id` VARCHAR(255) NOT NULL , 
                PRIMARY KEY (`order_id`, `channel_id`)) ENGINE = MyISAM;";

        $this->db->query($sql);
    }
}