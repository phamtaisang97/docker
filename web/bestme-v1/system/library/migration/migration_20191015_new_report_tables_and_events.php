<?php

namespace Migration;

class migration_20191015_new_report_tables_and_events extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20191015_new_report_tables_and_events';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('migrate creating new report tables');

        // create new report tables
        try {
            $this->migrateCreatingNewReportTables();
        } catch (\Exception $e) {
            $this->log('migrateCreatingNewReportTables got error: ' . $e->getMessage());
        }

        // create new event on order, product, customer changing
        try {
            $this->migrateCreatingNewReportEvents();
        } catch (\Exception $e) {
            $this->log('migrateCreatingNewReportEvents got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Migrate removing new report tables report_order, report_customer, report_product, report_web_traffic');

        // DROP TABLES
        $sql = "DROP TABLE IF EXISTS `{$this->db_prefix}report_order`, `{$this->db_prefix}report_customer`, `{$this->db_prefix}report_product`, `{$this->db_prefix}report_web_traffic`;";
        $this->db->query("$sql");
    }

    private function migrateCreatingNewReportTables()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}report_order` (
                  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `report_date` date NOT NULL,
                  `order_id` int(11) NOT NULL,
                  `customer_id` int(11) DEFAULT 0,
                  `total_amount` decimal(18,4) DEFAULT NULL,
                  `order_status` int(11) DEFAULT 0
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        $this->db->query($sql);

        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}report_product` (
                  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `report_date` date NOT NULL,
                  `order_id` int(11) NOT NULL,
                  `product_id` int(11) NOT NULL,
                  `product_version_id` int(11) DEFAULT NULL,
                  `quantity` int(11) NOT NULL,
                  `price` decimal(18,4) DEFAULT NULL,
                  `total_amount` decimal(18,4) DEFAULT NULL
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        $this->db->query($sql);

        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}report_customer` (
                  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `report_date` date NOT NULL,
                  `customer_id` int(11) DEFAULT 0
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        $this->db->query($sql);

        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}report_web_traffic` (
                  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `report_date` date NOT NULL,
                  `value` varchar(500) NOT NULL,
                  `last_updated` timestamp NULL DEFAULT NULL
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        $this->db->query($sql);
    }

    private function migrateCreatingNewReportEvents()
    {
        $sql = "DELETE FROM `{$this->db_prefix}event` WHERE `trigger` = 'admin/model/sale/order/addOrder/after' AND `action` = 'event/report_order/afterCreateOrder'";
        $this->db->query($sql);

        $sql = "DELETE FROM `{$this->db_prefix}event` WHERE `trigger` = 'admin/model/sale/order/updateColumnTotalOrder/after' AND `action` = 'event/report_order/editOrderInOrderList'";
        $this->db->query($sql);

        $sql = "DELETE FROM `{$this->db_prefix}event` WHERE `trigger` = 'admin/model/sale/order/updateStatus/after' AND `action` = 'event/report_order/updateOrderStatusInOrderList'";
        $this->db->query($sql);

        $sql = "DELETE FROM `{$this->db_prefix}event` WHERE `trigger` = 'admin/model/sale/order/editOrder/after' AND `action` = 'event/report_order/editOrderInOrderDetail'";
        $this->db->query($sql);

        $sql = "DELETE FROM `{$this->db_prefix}event` WHERE `trigger` = 'admin/model/sale/order/addOrder/after' AND `action` = 'event/report_product/afterCreateOrder'";
        $this->db->query($sql);

        $sql = "DELETE FROM `{$this->db_prefix}event` WHERE `trigger` = 'admin/model/sale/order/updateProductOrder/after' AND `action` = 'event/report_product/editOrderInOrderList'";
        $this->db->query($sql);

        $sql = "DELETE FROM `{$this->db_prefix}event` WHERE `trigger` = 'admin/model/sale/order/updateStatus/after' AND `action` = 'event/report_product/updateOrderStatusInOrderList'";
        $this->db->query($sql);

        $sql = "DELETE FROM `{$this->db_prefix}event` WHERE `trigger` = 'admin/model/sale/order/editOrder/after' AND `action` = 'event/report_product/editOrderInOrderDetail'";
        $this->db->query($sql);

        $sql = "DELETE FROM `{$this->db_prefix}event` WHERE `trigger` = 'admin/model/customer/customer/addCustomer/after' AND `action` = 'event/report_customer/createCustomer'";
        $this->db->query($sql);

        $sql = "DELETE FROM `{$this->db_prefix}event` WHERE `trigger` = 'admin/model/customer/customer/deleteCustomer/after' AND `action` = 'event/report_customer/deleteCustomer'";
        $this->db->query($sql);

        $sql = "INSERT INTO `{$this->db_prefix}event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES
                ('create new order', 'admin/model/sale/order/addOrder/after', 'event/report_order/afterCreateOrder', 1, 0),
                ('edit order in list', 'admin/model/sale/order/updateColumnTotalOrder/after', 'event/report_order/editOrderInOrderList', 1, 0),
                ('change status order in order list', 'admin/model/sale/order/updateStatus/after', 'event/report_order/updateOrderStatusInOrderList', 1, 0),
                ('edit order in order detail', 'admin/model/sale/order/editOrder/after', 'event/report_order/editOrderInOrderDetail', 1, 0),
                ('create new order', 'admin/model/sale/order/addOrder/after', 'event/report_product/afterCreateOrder', 1, 0),
                ('edit order in list', 'admin/model/sale/order/updateProductOrder/after', 'event/report_product/editOrderInOrderList', 1, 0),
                ('change status order in order list', 'admin/model/sale/order/updateStatus/after', 'event/report_product/updateOrderStatusInOrderList', 1, 0),
                ('edit order in order detail', 'admin/model/sale/order/editOrder/after', 'event/report_product/editOrderInOrderDetail', 1, 0),
                ('create customer', 'admin/model/customer/customer/addCustomer/after', 'event/report_customer/createCustomer', 1, 0),
                ('delete customer', 'admin/model/customer/customer/deleteCustomer/after', 'event/report_customer/deleteCustomer', 1, 0);";
        $this->db->query($sql);
    }
}
