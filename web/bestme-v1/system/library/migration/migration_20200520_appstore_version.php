<?php

namespace Migration;

class migration_20200520_appstore_version extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200520_appstore_version';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate create table my_app for Appstore version');
        try {
            $this->migrateCreateTablesForAppstore();
            $this->migrateInsertApp();
        } catch (\Exception $e) {
            $this->log('crate table for appstore version got error: ' . $e->getMessage());
        }

        // permission for admin user
        try {
            $this->addPermissionForAdminUser([
                'app_store/my_app',
                'extension/extension/appstore',
                'common/app_config'
            ]);
        } catch (\Exception $e) {
            $this->log('addPermissionForAdminUser (app_store/my_app) got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back remove some table of Appstore version');
    }

    protected function migrateCreateTablesForAppstore()
    {
        // my_app : Lưu danh sách các app được cài đặt

        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}my_app` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `app_code` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
                    `status` int(1)  NULL,
                    `expiration_date` datetime  DEFAULT NULL,
                    `app_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                    `path_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                    `path_asset` text COLLATE utf8_unicode_ci  NULL,
                    `trial` int(2)  NULL,
                    `installed` int(11) NOT NULL DEFAULT '1',
                    `date_added` datetime NOT NULL,
                    `date_modified` datetime NOT NULL,
                    PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
        $this->db->query($sql);

        // oc_appstore_setting: Lưu các cài đặt của app
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}appstore_setting` (
                  `module_id` int(11) NOT NULL AUTO_INCREMENT,
                  `name` varchar(64) CHARACTER SET utf8 NOT NULL,
                  `code` varchar(64) CHARACTER SET utf8 NOT NULL,
                  `setting` text CHARACTER SET utf8 NOT NULL,
                  PRIMARY KEY (`module_id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
        $this->db->query($sql);

        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}app_config_theme` (
                  `id` int(255) NOT NULL AUTO_INCREMENT,
                  `module_id` int(255) NOT NULL,
                  `theme_value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                  `page` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                  `position` varchar(255) COLLATE utf8_unicode_ci NULL,
                  `sort` int(10)  NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
        $this->db->query($sql);
    }

    protected function migrateInsertApp()
    {
        // TODO: check existing data to avoid duplicate... @Huudt

        $sql = "INSERT INTO `{$this->db_prefix}my_app` (`app_code`, `status`, `expiration_date`, `app_name`, `path_logo`, `path_asset`, `trial`, `installed`,`date_added`,`date_modified`) VALUES
                ('mar_ons_chatbot', 1, NULL, 'NovaonX ChatBot', 'view/image/appstore/mar_ons_chatbot.jpg', '', 0, 1, NOW(), NOW()),
                ('mar_ons_maxlead', 1, NULL, 'AutoAds Maxlead', 'view/image/appstore/mar_ons_maxlead.jpg', '', 0, 1, NOW(), NOW()),
                ('trans_ons_vtpost', 1, NULL, 'VietelPost', 'view/image/appstore/trans_ons_vtpost.jpg', '', 0, 1, NOW(), NOW()),
                ('trans_ons_ghn', 1, NULL, 'Giao Hàng Nhanh', 'view/image/appstore/trans_ons_ghn.jpg', '', 0, 1, NOW(), NOW());";
        $this->db->query($sql);
    }
}