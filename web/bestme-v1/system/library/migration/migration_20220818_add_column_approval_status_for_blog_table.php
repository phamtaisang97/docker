<?php

namespace Migration;

class migration_20220818_add_column_approval_status_for_blog_table extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20220818_add_column_approval_status_for_blog_table';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('add column approval_status to table blog');

        try {
            $this->addColumnApprovalStatusBlogTable();
        } catch (\Exception $e) {
            $this->log('add column approval_status to table blog got error: ' . $e->getMessage());
        }

    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back add column approval_status to table blog');
    }

    protected function addColumnApprovalStatusBlogTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}blog` 
                ADD `approval_status` VARCHAR(255) NULL DEFAULT NULL AFTER `in_home`;";
        $this->db->query($sql);
    }
}