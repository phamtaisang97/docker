<?php

namespace Migration;

class migration_20191226_register_success_text extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20191226_register_success_text';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('migrate add config register text success');

        try {
            $this->migrateRegisterTextSuccess();
        } catch (\Exception $e) {
            $this->log('migrateRegisterTextSuccess got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('migrate remove config register text success');

        try {
            $this->removeRegisterTextSuccess();
        } catch (\Exception $e) {
            $this->log('removeRegisterTextSuccess got error: ' . $e->getMessage());
        }
    }

    private function migrateRegisterTextSuccess()
    {
        $sql = "SELECT st.* FROM `{$this->db_prefix}setting` st 
                WHERE st.`key` = 'config_register_success_text'";
        $query = $this->db->query($sql);

        // insert if not existing
        $setting_config_order_success = $query->rows;
        if (!$setting_config_order_success) {
            $sqlInsert = "INSERT INTO `{$this->db_prefix}setting` (`store_id`, `code`, `key`, `value`, `serialized`)
                          VALUES ('0', 'config', 'config_register_success_text', '<p>Chúc mừng! Tài khoản mới của bạn đã được tạo thành công!</p>\n\n<p>Bây giờ bạn có thể tận hưởng lợi ích thành viên khi tham gia trải nghiệm mua sắm cùng chúng tôi.</p>\n\n<p>Nếu bạn có câu hỏi gì về hoạt động của cửa hàng, vui lòng liên hệ với chủ cửa hàng.</p>\n\n<p>Một email xác nhận đã được gửi tới hòm thư của bạn. Nếu bạn không nhận được email này trong vòng 1 giờ, vui lòng liên hệ chúng tôi.</p>', '0')";
            $this->db->query($sqlInsert);
        }
    }

    /**
     * removeTextOrderSuccess
     */
    private function removeRegisterTextSuccess()
    {
        $sql = "DELETE FROM `{$this->db_prefix}setting` WHERE 1 
                AND `key` = 'config_register_success_text'";
        $this->db->query($sql);
    }
}
