<?php

namespace Migration;

class migration_20200314_chatbot_v2 extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200314_chatbot_v2';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate add column "customer_ref_id" to table order');

        try {
            $sql = "ALTER TABLE `{$this->db_prefix}order` ADD `customer_ref_id` VARCHAR(128) NULL DEFAULT NULL AFTER `pos_id_ref`;";
            $this->db->query($sql);
        } catch (\Exception $e) {
            $this->log('migrate add column "customer_ref_id" to table order got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Remove column "customer_ref_id" from table order');

        try {
            $sql = "ALTER TABLE `{$this->db_prefix}order` DROP `customer_ref_id`;";
            $this->db->query($sql);
        } catch (\Exception $e) {
            $this->log('migrate remove column "customer_ref_id" from table order got error: ' . $e->getMessage());
        }
    }
}