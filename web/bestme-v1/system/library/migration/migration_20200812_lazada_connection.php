<?php

namespace Migration;

class migration_20200812_lazada_connection extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200812_lazada_connection';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate create table for lazada_connection version');
        try {
            $this->migrateCreateTablesForLazadaConnection();

        } catch (\Exception $e) {
            $this->log('crate table for lazada_connection version got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back remove some table of lazada_connection version');
    }

    protected function migrateCreateTablesForLazadaConnection()
    {
        // lazada_category_attributes
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}lazada_category_attributes` (
                  `lazada_cateogry_id` BIGINT(32) NOT NULL ,
                  `data` LONGTEXT NOT NULL ,
                  PRIMARY KEY (`lazada_cateogry_id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);

        // lazada_shop_config
        // sync_interval: use later for Lazada order sync
        // refresh_expires_in: connection expired date
        // use seller api, get access token api to get shop info
        // - seller api: https://open.lazada.com/doc/api.htm?spm=a2o9m.11193531.0.0.1d086bbeFhH4yb#/api?cid=2&path=/seller/get
        // - get access token: https://open.lazada.com/doc/doc.htm?spm=a2o9m.11193535.0.0.330b38e4EEjk0W#?nodeId=10434&docId=108260
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}lazada_shop_config` (
                  `id` INT(11) NOT NULL AUTO_INCREMENT ,
                  `seller_id` BIGINT(32) NOT NULL ,
                  `country` VARCHAR(16) NOT NULL ,
                  `shop_name` VARCHAR(256) NOT NULL ,
                  `access_token` VARCHAR(256) NOT NULL ,
                  `expires_in` DATETIME NOT NULL ,
                  `refresh_token` VARCHAR(256) NOT NULL ,
                  `refresh_expires_in` DATETIME NOT NULL ,
                  `sync_interval` INT(11) NOT NULL DEFAULT '0' ,
                  `connected` TINYINT(2) NOT NULL DEFAULT '1' ,
                  PRIMARY KEY (`id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);

        // lazada_product
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}lazada_product` (
                  `id` INT(11) NOT NULL AUTO_INCREMENT ,
                  `shop_id` INT(11) NOT NULL ,
                  `item_id` VARCHAR(64) NOT NULL ,
                  `primary_category` VARCHAR(64) NOT NULL ,
                  `name` VARCHAR(256) NOT NULL ,
                  `description` VARCHAR(512) NULL ,
                  `short_description` VARCHAR(256) NULL ,
                  `brand` VARCHAR(128) NULL ,
                  `bestme_product_id` INT(11) NULL ,
                  `sync_status` INT(11) NULL ,
                  `created_at` DATETIME NULL ,
                  `updated_at` DATETIME NULL ,
                  PRIMARY KEY (`id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);

        // lazada_product_version
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}lazada_product_version` (
                  `id` INT(11) NOT NULL AUTO_INCREMENT ,
                  `item_id` VARCHAR(64) NOT NULL ,
                  `version_name` VARCHAR(128) NULL ,
                  `status` VARCHAR(26) NULL ,
                  `quantity` INT(11) NULL ,
                  `product_weight` DECIMAL(15,4) NULL ,
                  `images` MEDIUMTEXT NULL COMMENT 'list image' ,
                  `seller_sku` VARCHAR(128) NOT NULL ,
                  `shop_sku` VARCHAR(128) NULL ,
                  `url` VARCHAR(256) NULL ,
                  `package_width` DECIMAL(10,2) NULL COMMENT 'centimet' ,
                  `package_height` DECIMAL(10,2) NULL COMMENT 'centimet' ,
                  `package_length` DECIMAL(10,2) NULL COMMENT 'centimet',
                  `package_weight` DECIMAL(10,4) NULL COMMENT 'kilogam' ,
                  `price` DECIMAL(15,4) NULL ,
                  `available` INT(11) NULL ,
                  `sku_id` BIGINT(32) NULL ,
                  `bestme_product_id` INT(11) NULL ,
                  `bestme_product_version_id` INT(11) NULL ,
                  `sync_status` INT(11) NULL ,
                  PRIMARY KEY (`id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);

        // lazada_category_tree
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}lazada_category_tree` ( 
                  `id` INT(11) NOT NULL , 
                  `parent_id` INT(11) NOT NULL DEFAULT '0' , 
                  `name` VARCHAR(256) NOT NULL , 
                  PRIMARY KEY (`id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);

        // lazada_category_description
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}lazada_category_description` (
                  `category_id` INT(11) NOT NULL , 
                  `name` VARCHAR(256) NOT NULL 
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);
    }
}