<?php

namespace Migration;

class migration_20211012_add_new_column_to_table_product extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20211012_add_new_column_to_table_product';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('add column brand_shop_name to table product');

        try {
            $this->addColumnBrandShopNameToProductTable();
        } catch (\Exception $e) {
            $this->log('add column brand_shop_name to table product got error: ' . $e->getMessage());
        }

    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back add column brand_shop_name to table product');
    }

    protected function addColumnBrandShopNameToProductTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}product` 
                ADD `brand_shop_name` VARCHAR(255) NULL DEFAULT NULL AFTER `source`;";
        $this->db->query($sql);
    }
}