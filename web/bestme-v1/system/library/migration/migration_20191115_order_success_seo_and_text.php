<?php

namespace Migration;

class migration_20191115_order_success_seo_and_text extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20191115_order_success_seo_and_text';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('migrate add seo and config order text success');

        try {
            $this->addOrderSuccessSeo();
        } catch (\Exception $e) {
            $this->log('addOrderSuccessSeo got error: ' . $e->getMessage());
        }

        try {
            $this->migrateTextOrderSuccess();
        } catch (\Exception $e) {
            $this->log('migrateTextOrderSuccess got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {

        $this->log('migrate remove seo and config order text success');

        try {
            $this->removeOrderSuccessSeo();
        } catch (\Exception $e) {
            $this->log('removeOrderSuccessSeo got error: ' . $e->getMessage());
        }

        try {
            $this->removeTextOrderSuccess();
        } catch (\Exception $e) {
            $this->log('removeTextOrderSuccess got error: ' . $e->getMessage());
        }
    }

    /**
     * addOrderSuccessSeo
     */
    private function addOrderSuccessSeo()
    {
        // remove first if existing
        $this->removeOrderSuccessSeo();

        // add
        $sql = "INSERT INTO `{$this->db_prefix}seo_url` (`store_id`, `language_id`, `query`, `keyword`, `table_name`) VALUES
                ('0', '1', 'checkout/order_preview/orderSuccess', 'payment-success', ''),
                ('0', '2', 'checkout/order_preview/orderSuccess', 'dat-hang-thanh-cong', '')";
        $this->db->query($sql);
    }

    private function migrateTextOrderSuccess()
    {
        $sql = "SELECT st.* FROM `{$this->db_prefix}setting` st 
                WHERE st.`key` = 'config_order_success_text'";
        $query = $this->db->query($sql);

        // insert if not existing
        $setting_config_order_success = $query->rows;
        if (!$setting_config_order_success) {
            $sqlInsert = "INSERT INTO `{$this->db_prefix}setting` (`store_id`, `code`, `key`, `value`, `serialized`)
                          VALUES ('0', 'config', 'config_order_success_text', 'MUA HÀNG THÀNH CÔNG!', '0')";
            $this->db->query($sqlInsert);
        }
    }

    /**
     * removeOrderSuccessSeo
     */
    private function removeOrderSuccessSeo()
    {
        $sql = "DELETE FROM `{$this->db_prefix}seo_url` WHERE 1 
                AND `query` IN ('checkout/order_preview/orderSuccess') 
                AND `keyword` IN ('payment-success', 'dat-hang-thanh-cong');";
        $this->db->query($sql);
    }

    /**
     * removeTextOrderSuccess
     */
    private function removeTextOrderSuccess()
    {
        $sql = "DELETE FROM `{$this->db_prefix}setting` WHERE 1 
                AND `key` = 'config_order_success_text'";
        $this->db->query($sql);
    }
}
