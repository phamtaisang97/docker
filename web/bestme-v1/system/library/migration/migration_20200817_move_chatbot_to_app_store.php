<?php

namespace Migration;

class migration_20200817_move_chatbot_to_app_store extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200817_move_chatbot_to_app_store';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate add permission mar_ons_chatbot "extension/appstore/mar_ons_chatbot" to admin user');

        // permission for admin user
        try {
            $this->addPermissionForAdminUser([
                'extension/appstore/mar_ons_chatbot'
            ]);
        } catch (\Exception $e) {
            $this->log('addPermissionForAdminUser (extension/appstore/mar_ons_chatbot) got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back permission mar_ons_chatbot "extension/appstore/mar_ons_chatbot" from admin user');
        // no need
    }
}