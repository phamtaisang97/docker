<?php

namespace Migration;

class migration_20191016_new_report_data extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20191016_new_report_data';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('migrate old report data');

        // *_report_order
        try {
            $this->migrateOrderData();
        } catch (\Exception $e) {
            $this->log('migrateOrderData got error: ' . $e->getMessage());
        }

        // *_report_product
        try {
            $this->migrateProductData();
        } catch (\Exception $e) {
            $this->log('migrateProductData got error: ' . $e->getMessage());
        }

        // *_report_customer
        try {
            $this->migrateCustomer();
        } catch (\Exception $e) {
            $this->log('migrateCustomer got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('TRUNCATE TABLE *_report_order, *_report_customer, *_report_product');

        // TRUNCATE TABLE *_report_order
        $sql = "TRUNCATE TABLE `{$this->db_prefix}report_order`";
        $this->db->query($sql);

        // TRUNCATE TABLE *_report_customer
        $sql = "TRUNCATE TABLE `{$this->db_prefix}report_customer`";
        $this->db->query($sql);

        // TRUNCATE TABLE *_report_product
        $sql = "TRUNCATE TABLE `{$this->db_prefix}report_product`";
        $this->db->query($sql);
    }

    private function migrateOrderData()
    {
        $sql = "SELECT o.*, oh.`date_added` as `rp_time` FROM `{$this->db_prefix}order` o 
                LEFT JOIN `{$this->db_prefix}order_history` oh ON (o.`order_id` = oh.`order_id`) 
                WHERE oh.`order_status_id` = 7 AND oh.`status` IN (1,3)";
        $query = $this->db->query($sql);
        $orders = $query->rows;

        $migrated_order = [];

        foreach ($orders as $order) {
            if (!is_array($order)) {
                continue;
            }
            if (!array_key_exists('order_id', $order) || !array_key_exists('rp_time', $order)) {
                continue;
            }

            if (in_array($order['order_id'], $migrated_order)) {
                continue;
            }

            $migrated_order[] = $order['order_id'];

            $report_time = date_create_from_format('Y-m-d H:i:s', $order['rp_time']);
            $report_time->setTime($report_time->format('H'), 0, 0);
            $rp_time = $report_time->format('Y-m-d H:i:s');
            $rp_date = $report_time->format('Y-m-d');
            $order_id = $order['order_id'];
            $customer_id = $order['customer_id'];
            $total = $order['total'];
            $order_status_id = $order['order_status_id'];

            $sqlInsert = "INSERT INTO `{$this->db_prefix}report_order` (`report_time`, `report_date`, `order_id`, `customer_id`, `total_amount`, `order_status`) 
                          VALUES ('$rp_time', '$rp_date', '$order_id', '$customer_id', '$total', '$order_status_id')";

            $this->db->query($sqlInsert);
        }
    }

    private function migrateProductData()
    {
        $sql = "SELECT op.*, oh.`date_added` as `rp_time` FROM `{$this->db_prefix}order_product` op 
                LEFT JOIN `{$this->db_prefix}order_history` oh ON (op.`order_id` = oh.`order_id`) 
                WHERE oh.`order_status_id` = 7 AND oh.`status` IN (1,3)";
        $query = $this->db->query($sql);
        $ops = $query->rows;

        $migrated = [];

        foreach ($ops as $op) {
            if (!is_array($op)) {
                continue;
            }
            if (!array_key_exists('order_id', $op) || !array_key_exists('product_id', $op) || !array_key_exists('rp_time', $op)) {
                continue;
            }

            $migrated_key = sprintf('%s--%s--%s', $op['order_id'], $op['product_id'], $op['product_version_id']);
            if (in_array($migrated_key, $migrated)) {
                continue;
            }

            $migrated[] = $migrated_key;

            $report_time = date_create_from_format('Y-m-d H:i:s', $op['rp_time']);
            $report_time->setTime($report_time->format('H'), 0, 0);
            $rp_time = $report_time->format('Y-m-d H:i:s');
            $rp_date = $report_time->format('Y-m-d');
            $order_id = $op['order_id'];
            $product_id = $op['product_id'];
            $product_version_id = $op['product_version_id'];
            $quantity = $op['quantity'];
            $price = $op['price'];
            $total = $op['total'];

            $sqlInsert = "INSERT INTO `{$this->db_prefix}report_product` (`report_time`, `report_date`, `order_id`, `product_id`, `product_version_id`, `quantity`, `price`, `total_amount`) VALUES ('$rp_time', '$rp_date', '$order_id', '$product_id', '$product_version_id', '$quantity', '$price', '$total')";

            $this->db->query($sqlInsert);
        }
    }

    private function migrateCustomer()
    {
        $sql = "SELECT * FROM `{$this->db_prefix}customer`";
        $query = $this->db->query($sql);
        $customers = $query->rows;

        foreach ($customers as $customer) {
            if (!is_array($customer)) {
                continue;
            }
            if (!array_key_exists('customer_id', $customer) || !array_key_exists('date_added', $customer)) {
                continue;
            }

            $report_time = date_create_from_format('Y-m-d H:i:s', $customer['date_added']);
            $report_time->setTime($report_time->format('H'), 0, 0);
            $rp_time = $report_time->format('Y-m-d H:i:s');
            $rp_date = $report_time->format('Y-m-d');
            $customer_id = $customer['customer_id'];

            $sqlInsert = "INSERT INTO `{$this->db_prefix}report_customer` (`report_time`, `report_date`, `customer_id`) VALUES ('$rp_time', '$rp_date', '$customer_id')";

            $this->db->query($sqlInsert);
        }
    }
}
