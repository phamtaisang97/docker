<?php

namespace Migration;

class migration_20200904_cash_flow extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200904_cash_flow';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate create table for v2.7.4_cash_flow');
        try {
            $this->migrateCreateTablesForCashFolow();
            $this->migrateInsertData();
        } catch (\Exception $e) {
            $this->log('create table for v2.7.4_cash_flow got error: ' . $e->getMessage());
        }

        // permission for admin user
        try {
            $this->addPermissionForAdminUser([
                'cash_flow/cash_flow',
                'cash_flow/receipt_voucher',
                'cash_flow/payment_voucher'
            ]);
        } catch (\Exception $e) {
            $this->log('addPermissionForAdminUser (v2.7.4_cash_flow) got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back remove some table of v2.7.4_cash_flow');
    }

    protected function migrateCreateTablesForCashFolow()
    {
        // Bảng lưu phiếu thu và phiếu chi
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}receipt_voucher` (
            `receipt_voucher_id` INT(11) NOT NULL AUTO_INCREMENT,
            `receipt_voucher_code` VARCHAR(50) NOT NULL,
            `receipt_voucher_type_id` INT(11) NOT NULL,
            `order_id` INT(11) NULL DEFAULT NULL,
            `status` INT(11) NOT NULL,
            `in_business_report_status` TINYINT(4) DEFAULT 1,
            `amount` decimal(18,4) NOT NULL DEFAULT '0.0000',
            `store_id` INT(11) NOT NULL,
            `cash_flow_method_id` INT(11) NOT NULL,
            `cash_flow_object_id` INT(11) NULL DEFAULT NULL,
            `object_info` TEXT NULL DEFAULT NULL,
            `receipt_type_other` VARCHAR (255) NULL DEFAULT NULL,
            `note` TEXT NULL DEFAULT NULL,
            `date_added` DATETIME NOT NULL,
            `date_modified` DATETIME NOT NULL, 
            PRIMARY KEY (`receipt_voucher_id`) USING BTREE
        ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";
        $this->db->query($sql);

        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}payment_voucher` (
            `payment_voucher_id` INT(11) NOT NULL AUTO_INCREMENT,
            `payment_voucher_type_id` INT(11) NOT NULL,
            `payment_voucher_code` VARCHAR(50) NOT NULL,
            `store_receipt_id` INT(11) NULL DEFAULT NULL,
            `status` INT(11) NOT NULL,
            `in_business_report_status` TINYINT(4) DEFAULT 1,
            `amount` decimal(18,4) NOT NULL DEFAULT '0.0000',
            `store_id` INT(11) NOT NULL,
            `cash_flow_method_id` INT(11) NOT NULL,
            `cash_flow_object_id` INT(11) NULL DEFAULT NULL,
            `object_info` TEXT NULL DEFAULT NULL,
            `payment_type_other` VARCHAR (255) NULL DEFAULT NULL,
            `note` TEXT NULL DEFAULT NULL,
            `date_added` DATETIME NOT NULL,
            `date_modified` DATETIME NOT NULL, 
            PRIMARY KEY (`payment_voucher_id`) USING BTREE
        ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);

        // Bảng lưu loại phiếu thu, loại phiếu chi
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}receipt_voucher_type` (
            `receipt_voucher_type_id` INT(11) NOT NULL,
            `language_id` TINYINT(4) NOT NULL,
            `name` VARCHAR(255) NOT NULL,
            `description` VARCHAR(255) NULL DEFAULT NULL,
            PRIMARY KEY (`receipt_voucher_type_id`, `language_id`) USING BTREE
        ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);

        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}payment_voucher_type` (
            `payment_voucher_type_id` INT(11) NOT NULL,
            `language_id` TINYINT(4) NOT NULL,
            `name` VARCHAR(255) NOT NULL,
            `description` VARCHAR(255) NULL DEFAULT NULL,
            PRIMARY KEY (`payment_voucher_type_id`, `language_id`) USING BTREE
        ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);

        // Bảng lưu phương thức thanh toán của phiếu thu và phiếu chi

        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}cash_flow_method` (
            `cash_flow_method_id` INT(11) NOT NULL,
            `language_id` TINYINT(4) NOT NULL,
            `name` VARCHAR (255) NOT NULL,
            `description` VARCHAR(255) NULL DEFAULT NULL,
            PRIMARY KEY (`cash_flow_method_id`, `language_id`) USING BTREE
        ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);

        // Bảng lưu đối tượng của phiếu thu phiếu chi

        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}cash_flow_object` (
            `cash_flow_object_id` INT(11) NOT NULL,
            `language_id` TINYINT(4) NOT NULL,
            `name` VARCHAR (255) NOT NULL,
            `description` VARCHAR(255) NULL DEFAULT NULL,
            PRIMARY KEY (`cash_flow_object_id`, `language_id`) USING BTREE
        ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);
    }

    protected function migrateInsertData()
    {
        // Thêm dữ liệu cho các bảng: oc_cash_flow_method, oc_cash_flow_object, oc_receipt_voucher_type, oc_payment_voucher_type

        $sql = "INSERT IGNORE INTO `{$this->db_prefix}cash_flow_method` (`cash_flow_method_id`, `language_id`, `name`, `description`) VALUES
            (1, 2, 'Tiền mặt', NULL),
            (1, 1, 'Cash', NULL),
            (2, 2, 'Chuyển khoản', NULL),
            (2, 1, 'Transfer', NULL),
            (3, 2, 'COD', NULL),
            (3, 1, 'COD', NULL),
            (4, 2, 'Quẹt thẻ', NULL),
            (4, 1, 'Card', NULL);";
        $this->db->query($sql);

        $sql = "INSERT IGNORE INTO `{$this->db_prefix}cash_flow_object` (`cash_flow_object_id`, `language_id`, `name`, `description`) VALUES
            (1, 2, 'Khách hàng', NULL),
            (1, 1, 'Customer', NULL),
            (2, 2, 'Nhân viên', NULL),
            (2, 1, 'Staff', NULL),
            (3, 2, 'Nhà cung cấp', NULL),
            (3, 1, 'Supplier', NULL),
            (4, 2, 'Đối tác vận chuyển', NULL),
            (4, 1, 'Shipping partner', NULL),
            (5, 2, 'Khác', NULL),
            (5, 1, 'Other', NULL);";

        $this->db->query($sql);

        $sql = "INSERT IGNORE INTO `{$this->db_prefix}receipt_voucher_type` (`receipt_voucher_type_id`, `language_id`, `name`, `description`) VALUES
            (1, 2, 'Đối tác vận chuyển đặt cọc', NULL),
            (1, 1, 'Shipping partner makes a deposit', NULL),
            (2, 2, 'Thu nợ đối tác vận chuyển', NULL),
            (2, 1, 'Collecting debt from shipping partner', NULL),
            (3, 2, 'Thu nhập khác', NULL),
            (3, 1, 'Other income', NULL),
            (4, 2, 'Tiền thưởng', NULL),
            (4, 1, 'Bonus/Reward', NULL),
            (5, 2, 'Tiền bồi thường', NULL),
            (5, 1, 'Compensation', NULL),
            (6, 2, 'Cho thuê, thanh lý tài sản', NULL),
            (6, 1, 'Rent and liquidation property', NULL),
            (7, 2, 'Thu nợ khách hàng', NULL),
            (7, 1, 'Collecting debt from customer', NULL),
            (8, 2, 'Thu tự động', NULL),
            (8, 1, 'Automation receipt', NULL),
            (9, 2, 'Khác', NULL),
            (9, 1, 'Other', NULL);";

        $this->db->query($sql);

        $sql = "INSERT IGNORE INTO `{$this->db_prefix}payment_voucher_type` (`payment_voucher_type_id`, `language_id`, `name`, `description`) VALUES
            (1, 2, 'Trả nợ đối tác vận chuyển', NULL),
            (1, 1, 'Paying debt for shipping partner', NULL),
            (2, 2, 'Chi phí sản xuất', NULL),
            (2, 1, 'Production cost', NULL),
            (3, 2, 'Chi phí nguyên - vật liệu', NULL),
            (3, 1, 'Raw materials cost', NULL),
            (4, 2, 'Chi phí sinh hoạt', NULL),
            (4, 1, 'Living cost', NULL),
            (5, 2, 'Chi phí nhân công', NULL),
            (5, 1, 'Labor cost', NULL),
            (6, 2, 'Chi phí bán hàng', NULL),
            (6, 1, 'Selling cost', NULL),
            (7, 2, 'Chi phí quản lý cửa hàng', NULL),
            (7, 1, 'Store management cost', NULL),
            (8, 2, 'Chi tự động', NULL),
            (8, 1, 'Automation Payment', NULL),
            (9, 2, 'Chi phí khác', NULL),
            (9, 1, 'Other cost', NULL);";

        $this->db->query($sql);
    }
}