<?php

namespace Migration;

class migration_20201214_shopee_upload_add_tables extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20201214_shopee_upload_add_tables';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration, $version)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION, $version);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        // add tables
        $this->log('add some tables');
        try {
            $this->addTables();
        } catch (\Exception $e) {
            $this->log('add tables got error: ' . $e->getMessage());
        }

        // add Permission
        $this->addPermission();
    }

    private function addPermission()
    {
        $this->log('addPermissionForAdminUser');
        try {
            $this->addPermissionForAdminUser([
                'sale_channel/shopee_upload'
            ]);
        } catch (\Exception $e) {
            $this->log('addPermissionForAdminUser got error: ' . $e->getMessage());
        }

        // permission for staff users
        $this->log('addPermissionForStaffUser');
        try {
            $this->addPermissionForStaffUser([
                'sale_channel/shopee' => ['sale_channel/shopee_upload']
            ]);
        } catch (\Exception $e) {
            $this->log('addPermissionForStaffUser got error: ' . $e->getMessage());
        }
    }

    private function addTables()
    {
        $this->log('add table shopee_upload_product');
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}shopee_upload_product` (
                 `shopee_upload_product_id` INT(11) NOT NULL AUTO_INCREMENT , 
                 `bestme_product_id` INT(11) NULL , 
                `name` VARCHAR(255) NOT NULL , 
                `description` TEXT NOT NULL , 
                `category_id` BIGINT(64) NULL , 
                `version_attribute` TEXT NULL, 
                `image` VARCHAR(255) NULL , 
                `price` DECIMAL(18,4) NOT NULL , 
                `stock` INT(11) NOT NULL , 
                `sku` VARCHAR(255) NULL , 
                `common_price` DECIMAL(18,4)  NULL , 
                `common_stock` INT(11) NULL , 
                `common_sku` VARCHAR(255)  NULL , 
                `weight` INT(11) NULL , 
                `length` INT(11) NULL , 
                `width` INT(11) NULL , 
                `height` INT(11) NULL , 
                `date_added` TIMESTAMP NULL , 
                `date_modify` TIMESTAMP NULL , 
                PRIMARY KEY (`shopee_upload_product_id`)) ENGINE = MyISAM 
                CHARSET=utf8 COLLATE utf8_general_ci;";
        $this->db->query($sql);

        $this->log('add table shopee_upload_product_version');
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}shopee_upload_product_version` ( 
                `shopee_upload_product_version_id` INT(11) NOT NULL AUTO_INCREMENT , 
                `shopee_upload_product_id` INT(11) NOT NULL , 
                `version_name` VARCHAR(255) NOT NULL , 
                `version_price` DECIMAL(18,4) NOT NULL, 
                `version_stock` INT(11) NOT NULL , 
                `version_sku` VARCHAR(255) NULL , 
                `variation_id` BIGINT(32) NULL , 
                PRIMARY KEY (`shopee_upload_product_version_id`)) ENGINE = MyISAM 
                CHARSET=utf8 COLLATE utf8_general_ci;";
        $this->db->query($sql);

        $this->log('add table shopee_upload_product_image');
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}shopee_upload_product_image` ( 
                `shopee_upload_product_image_id` INT(11) NOT NULL AUTO_INCREMENT , 
                `product_id` INT(11) NOT NULL , 
                `image` VARCHAR(255) NULL , 
                `sort_order` INT(11) NULL , 
                PRIMARY KEY (`shopee_upload_product_image_id`)) ENGINE = MyISAM 
                CHARSET=utf8 COLLATE utf8_general_ci;";
        $this->db->query($sql);

        $this->log('add table shopee_upload_product_attribute');
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}shopee_upload_product_attribute` ( 
                `shopee_upload_product_attribute_id` INT(11) NOT NULL AUTO_INCREMENT , 
                `product_id` INT(11) NOT NULL , 
                `attribute_id` INT(11) NOT NULL , 
                `value` VARCHAR(255) NULL , 
                PRIMARY KEY (`shopee_upload_product_attribute_id`)) ENGINE = MyISAM 
                CHARSET=utf8 COLLATE utf8_general_ci;";
        $this->db->query($sql);

        $this->log('add table shopee_upload_product_logistic');
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}shopee_upload_product_logistic` ( 
                `shopee_upload_product_logistic_id` INT(11) NOT NULL AUTO_INCREMENT , 
                `logistic_id` INT(11) NOT NULL , 
                `product_id` INT(11) NOT NULL , 
                `enabled` BOOLEAN NOT NULL , 
                PRIMARY KEY (`shopee_upload_product_logistic_id`)) ENGINE = MyISAM 
                CHARSET=utf8 COLLATE utf8_general_ci;";
        $this->db->query($sql);

        $this->log('add table shopee_upload_product_shop_update_result');
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}shopee_upload_product_shop_update_result` ( 
                `id` INT(11) NOT NULL AUTO_INCREMENT , 
                `shopee_upload_product_id` INT(11) NULL , 
                `shopee_shop_id` BIGINT(64) NULL , 
                `shopee_item_id` BIGINT(64) NULL , 
                `status` TINYINT(4) NULL , 
                `in_process` TINYINT(2) NULL DEFAULT '0' , 
                `error_code` VARCHAR(255) NULL , 
                `error_msg` VARCHAR(255) NULL , 
                `variation_result` TEXT NULL , 
                PRIMARY KEY (`id`)) ENGINE = MyISAM 
                CHARSET=utf8 COLLATE utf8_general_ci;";
        $this->db->query($sql);
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back create tables in version shopee_upload');
    }
}