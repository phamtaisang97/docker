<?php

namespace Migration;

class migration_20190729_2_fix_demo_data_sql extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20190729_2_fix_demo_data_sql';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Enable seo setting');

        // product_to_store
        if ($this->is_product_to_store_existing()) {
            $this->log('Product #79 is already existing in product_to_store!');
        } else {
            $sql = "INSERT INTO `{$this->db_prefix}product_to_store` (`product_id`, `store_id`) VALUES 
                    (79, 0);";
            $this->db->query($sql);
        }

        // warehouse
        if ($this->is_warehouse_existing()) {
            $this->log('Product #79 is already existing in warehouse!');
        } else {
            $sql = "INSERT INTO `{$this->db_prefix}warehouse` (`warehouse_id`, `product_id`, `product_version_id`) VALUES 
                    (1051, 79, NULL);";
            $this->db->query($sql);
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Roll back all default policy for google shopping');

        // 99.99% not used. Temp do nothing. // TODO: add code if need...

        // NO DOWN!
    }

    /* === end - default functions === */

    /**
     * Check record for police tables
     * @return bool
     */
    private function is_product_to_store_existing()
    {
        $query = "SELECT * FROM `{$this->db_prefix}product_to_store` WHERE `product_id` = 79 AND `store_id` = 0;";
        $data = $this->db->query($query);
        // check if product_to_store existing in db
        if ($data->row) {
            return true;
        }

        return false;
    }

    /**
     * Check record for police tables
     * @return bool
     */
    private function is_warehouse_existing()
    {
        $query = "SELECT * FROM `{$this->db_prefix}warehouse` WHERE `warehouse_id` = 1051 AND `product_id` = 79 AND `product_version_id` IS NULL;";
        $data = $this->db->query($query);
        // check if warehouse existing in db
        if ($data->row) {
            return true;
        }

        return false;
    }
}