<?php

namespace Migration;

class migration_20210409_add_columns_collection_amount_for_order extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20210409_add_columns_collection_amount_for_order';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('add columns collection_amount to table order');
        try {
            $this->addColumnToOrderTable();
        } catch (\Exception $e) {
            $this->log('add columns collection_amount to table order got error: ' . $e->getMessage());
        }

        $this->log('set value columns collection_amount to table order');
        try {
            $this->setValueColumnToOrderTable();
        } catch (\Exception $e) {
            $this->log('set value columns collection_amount to table order got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back column collection_amount table order version');
    }

    protected function addColumnToOrderTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}order` ADD `collection_amount` DECIMAL(18,4) NULL DEFAULT NULL AFTER `total`;";
        $this->db->query($sql);
    }

    protected function setValueColumnToOrderTable()
    {
        $sql = "UPDATE `{$this->db_prefix}order` SET `collection_amount` = `total` WHERE `collection_amount` IS NULL";
        $this->db->query($sql);
    }

}