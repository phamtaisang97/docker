<?php


namespace Migration;


class migration_20221013_coupon extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20221013_coupon';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('migrate coupon table');
        try {
            $this->migrateCouponTable();
        } catch (\Exception $e) {
            $this->log('migrate coupon table got error: ' . $e->getMessage());
        }

        $this->log('migrate coupon_product table');
        try {
            $this->migrateCouponProductTable();
        } catch (\Exception $e) {
            $this->log('migrate coupon_product table got error: ' . $e->getMessage());
        }

        $this->log('migrate order_voucher table');
        try {
            $this->migrateOrderVoucherTable();
        } catch (\Exception $e) {
            $this->log('migrate order_voucher table got error: ' . $e->getMessage());
        }

        $this->log('Will add seo url events');

        try {
            $this->addSeoUrlEvents();
        } catch (\Exception $e) {
            $this->log('Add seo url events got error: ' . $e->getMessage());
        }
    }


    private function migrateCouponTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}coupon`
                    CHANGE COLUMN `date_start` `date_start` DATETIME NOT NULL AFTER `total`,
                    CHANGE COLUMN `date_end` `date_end` DATETIME NOT NULL AFTER `date_start`,
                    ADD COLUMN `allow_with_discount` INT(11) NULL DEFAULT NULL COMMENT 'Cho phép sử dụng chung với chương trình khuyến mãi' COLLATE 'utf8_general_ci' AFTER `date_added`,
                    ADD COLUMN `limit_times` INT(11) NULL DEFAULT NULL COMMENT 'Số lần sử dụng' COLLATE 'utf8_general_ci' AFTER `allow_with_discount`,
                    ADD COLUMN `max_type_value` DECIMAL(15,4) NULL DEFAULT NULL COMMENT 'Giá cao nhất' COLLATE 'utf8_general_ci' AFTER `limit_times`,
                    ADD COLUMN `apply_for` INT(11) NULL DEFAULT NULL COMMENT 'Áp dụng cho' COLLATE 'utf8_general_ci' AFTER `max_type_value`,
                    ADD COLUMN `applicable_type` INT(11) NULL DEFAULT NULL COMMENT 'Loại áp dụng' COLLATE 'utf8_general_ci' AFTER `apply_for`,
                    ADD COLUMN `applicable_type_value` DECIMAL(15,4) NULL DEFAULT NULL COMMENT 'Số lượng sản phẩm áp dụng hoặc Tổng giá trị sản phẩm' COLLATE 'utf8_general_ci' AFTER `applicable_type`,
                    ADD COLUMN `sale_channel` INT(11) NULL DEFAULT NULL COMMENT 'Kênh áp dụng' COLLATE 'utf8_general_ci' AFTER `applicable_type_value`,
                    ADD COLUMN `order_value_from` DECIMAL(15,4) NULL DEFAULT NULL COMMENT 'Trị giá đơn hàng từ (số tiền)' COLLATE 'utf8_general_ci' AFTER `sale_channel`,
                    ADD COLUMN `how_to_apply` INT(11) NULL DEFAULT NULL COMMENT 'Cách áp dụng' COLLATE 'utf8_general_ci' AFTER `order_value_from`,
                    ADD COLUMN `subjects_of_application` INT(11) NULL DEFAULT NULL COMMENT 'Đối tượng áp dụng' COLLATE 'utf8_general_ci' AFTER `how_to_apply`;";
        $this->db->query($sql);

        $sql = "ALTER TABLE `{$this->db_prefix}coupon`
                    CHANGE COLUMN `date_start` `date_start` DATETIME NOT NULL AFTER `total`,
                    ADD COLUMN `never_expired` INT(10) NULL DEFAULT NULL AFTER `subjects_of_application`,
                    ADD COLUMN `allow_coupon_with_discount` INT(10) NULL DEFAULT NULL AFTER `never_expired`;";
        $this->db->query($sql);

        $sql = "ALTER TABLE `{$this->db_prefix}coupon`
                    DROP COLUMN `allow_coupon_with_discount`;";
        $this->db->query($sql);

        $sql = "ALTER TABLE `{$this->db_prefix}coupon`
	                ADD COLUMN `init_limit_times` INT(10) NULL DEFAULT NULL AFTER `limit_times`;";
        $this->db->query($sql);


    }

    private function migrateCouponProductTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}coupon_product`
                    ADD COLUMN `product_version_id` INT(11) NULL DEFAULT NULL AFTER `product_id`,
                    DROP PRIMARY KEY,
                    ADD PRIMARY KEY (`coupon_product_id`) USING BTREE,
                    ADD INDEX `coupon_id` (`coupon_id`),
                    ADD INDEX `product_id` (`product_id`),
                    ADD INDEX `product_version_id` (`product_version_id`);";
        $this->db->query($sql);
    }

    private function migrateOrderVoucherTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}order_voucher`
                    ADD COLUMN `status` INT(10) NULL DEFAULT 1 AFTER `amount`,
                    ADD COLUMN `date_added` DATETIME NULL AFTER `status`;";
        $this->db->query($sql);
    }

    private function addSeoUrlEvents()
    {
        $sql = "INSERT INTO `{$this->db_prefix}event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES 
                ('update_limit_times_coupon', 'catalog/model/sale/order/addOrderVoucher/after', 'event/activity_tracking/afterAddOrderVoucher', 1, 0);";
        $this->db->query($sql);
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back migration coupon');
    }
}