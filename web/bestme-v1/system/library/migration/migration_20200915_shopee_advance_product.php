<?php

namespace Migration;

class migration_20200915_shopee_advance_product extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200915_shopee_advance_product';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate create table for v2.8.1-shopee-advance-product');

        try {
            $this->migrateCreateTablesForShopeeAdvanceProduct();
        } catch (\Exception $exception) {
            $this->log('migrateCreateTablesForShopeeAdvanceProduct error: '.$exception->getMessage());
        }

        try {
            $this->migrateDataTablesForShopeeAdvanceProduct(0, 100);
        } catch (\Exception $exception) {
            $this->log('migrateDataTablesForShopeeAdvanceProduct error: '.$exception->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back remove some table of v2.8.1-shopee-advance-product');
    }

    protected function migrateCreateTablesForShopeeAdvanceProduct()
    {
        // New table: shopee_product_version
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}shopee_product_version` ( 
                  `id` INT(11) NOT NULL AUTO_INCREMENT, 
                  `item_id` VARCHAR(64) NOT NULL , 
                  `variation_id` BIGINT(64) NOT NULL , 
                  `version_name` VARCHAR(128) NULL , 
                  `reserved_stock` INT(11) NULL DEFAULT '0' , 
                  `product_weight` DECIMAL(15,4) NULL , 
                  `original_price` DECIMAL(15,4) NULL , 
                  `price` DECIMAL(15,4) NULL , 
                  `discount_id` INT NULL DEFAULT '0' , 
                  `create_time` VARCHAR(64) NULL DEFAULT NULL , 
                  `update_time` VARCHAR(64) NULL DEFAULT NULL , 
                  `sku` VARCHAR(64) NULL , 
                  `stock` INT(11) NULL DEFAULT '0' , 
                  `is_set_item` BOOLEAN NULL , 
                  `bestme_product_id` INT(11) NULL , 
                  `bestme_product_version_id` INT(11) NULL , 
                  `sync_status` INT(11) NULL , 
                  `status` VARCHAR(32) NULL ,
                  PRIMARY KEY (`id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);

        // IMPORTANT: new table: shopee_product for below sql "ALTER TABLE `{$this->db_prefix}shopee_product`..."
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}shopee_product` (
                  `id` INT(11) NOT NULL AUTO_INCREMENT ,
                  `item_id` BIGINT(64) NOT NULL ,
                  `shopid` BIGINT(64) NOT NULL ,
                  `sku` VARCHAR(64) NULL ,
                  `stock` INT(32) NULL ,
                  `status` VARCHAR(32) NULL ,
                  `name` VARCHAR(255) CHARACTER SET utf8 NOT NULL ,
                  `description` TEXT CHARACTER SET utf8 NOT NULL ,
                  `images` TEXT NOT NULL ,
                  `currency` VARCHAR(32) NOT NULL ,
                  `price` FLOAT(15,4) NOT NULL ,
                  `original_price` FLOAT(15,4) NOT NULL ,
                  `weight` FLOAT(15,4) NOT NULL ,
                  `category_id` BIGINT(64) NOT NULL ,
                  `has_variation` BOOLEAN NOT NULL DEFAULT FALSE ,
                  `variations` TEXT CHARACTER SET utf8 NULL ,
                  `attributes` TEXT CHARACTER SET utf8 NULL ,
                  `bestme_id` INT(11) NULL ,
                  `created_at` datetime NOT NULL,
                  `updated_at` datetime NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);

        // Alter table: shopee_product
        $sql= "ALTER TABLE `{$this->db_prefix}shopee_product` 
                  ADD `sync_status` INT(11) NULL AFTER `updated_at`, 
                  ADD `bestme_product_version_id` INT(11) NULL AFTER `sync_status`;";

        $this->db->query($sql);
    }

    protected function migrateDataTablesForShopeeAdvanceProduct($offset, $limit, $totalProduct = 0)
    {
        if (!$totalProduct) {
            // DELETE ALL
            $this->db->query("DELETE FROM `{$this->db_prefix}shopee_product_version` WHERE 1");
        }

        $sql = "SELECT * FROM `{$this->db_prefix}shopee_product` LIMIT {$limit} OFFSET {$offset} ";
        $query = $this->db->query($sql);

        if (count($query->rows) <= 0) {
            return;
        }

        foreach ($query->rows as $product) {
            if (isset($product['has_variation']) && $product['has_variation'] && isset($product['variations'])) {
                $versions = json_decode($product['variations'], true);
                $this->addDataTableShopeeProductVersion($product, $versions);
            }

            if (isset($product['bestme_id']) && $product['bestme_id'] != null) {
                $this->linkProductBestmeAndShopee($product['bestme_id'], $product);
            }
        }

        $offset += $limit;
        if (!$totalProduct){
            $totalProduct = $this->countTotalProductShopee();
        }

        if ($offset >= $totalProduct) {
            return;
        }

        $this->migrateDataTablesForShopeeAdvanceProduct($offset, $limit, $totalProduct);
    }

    protected function countTotalProductShopee()
    {
        $sql = "SELECT COUNT(id) as `total` FROM `{$this->db_prefix}shopee_product` WHERE `has_variation` = 1";
        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    protected function addDataTableShopeeProductVersion($product, $versions = array())
    {
        if (!isset($product['item_id']) &&
            !isset($product['weight'])) {
            return;
        }

        foreach ($versions as $version) {
            $item_id = $product['item_id'];
            $variation_id = isset($version['variation_id']) ? $version['variation_id'] : '';
            if ($variation_id == '') {
                return;
            }

            $version_name = isset($version['name']) ? $version['name'] : '';
            $reserved_stock = isset($version['reserved_stock']) ? $version['reserved_stock'] : 0;
            $product_weight = (float)$product['weight'];
            $original_price = isset($version['original_price']) ? (float)$version['original_price'] : 0;
            $price = isset($version['price']) ? (float)$version['price'] : 0;
            $discount_id = isset($version['discount_id']) ? $version['discount_id'] : 0;
            $create_time = isset($version['create_time']) ? $version['create_time'] : '';
            $update_time = isset($version['update_time']) ? $version['update_time'] : '';
            $sku = isset($version['variation_sku']) ? $version['variation_sku'] : '';
            $stock = isset($version['stock']) ? $version['stock'] : 0;
            $is_set_item = (isset($version['is_set_item']) && $version['is_set_item']) ? 1 : 0;
            $status = isset($version['status']) ? $version['status'] : '';

            $this->db->query("INSERT INTO `{$this->db_prefix}shopee_product_version` 
                                         SET `item_id` = '" . $item_id . "', 
                                             `variation_id` = '" . $variation_id . "', 
                                             `version_name` = '" . $version_name . "', 
                                             `reserved_stock` = '" . $reserved_stock . "', 
                                             `product_weight` = '" . $product_weight . "', 
                                             `original_price` = '" . $original_price . "', 
                                             `price` = '" . $price . "', 
                                             `discount_id` = '" . $discount_id . "', 
                                             `create_time` = '" . $create_time . "', 
                                             `update_time` = '" . $update_time . "', 
                                             `sku` = '" . $sku . "', 
                                             `stock` = '" . $stock . "', 
                                             `status` = '" . $status . "', 
                                             `is_set_item` = '" . $is_set_item . "' 
                                    ");
        }
    }

    protected function linkProductBestmeAndShopee($product_id, $product)
    {
        if (!isset($product['item_id'])) {
            return;
        }

        //  set sync_status if `bestme_id` not null
        $this->db->query("UPDATE `{$this->db_prefix}shopee_product` 
                                            SET `sync_status` = 1 
                                                WHERE `bestme_id` = {$product_id} 
                                            ");

        if (isset($product['has_variation']) && $product['has_variation'] == 0) {
            return;
        }

        // get product version
        $productVersionBestme = $this->db->query("SELECT * FROM `{$this->db_prefix}product_version` WHERE product_id = '" . (int)$product_id . "' AND deleted IS NULL");

        // get product shopee version
        $productVersionShopee = $this->db->query("SELECT * FROM `{$this->db_prefix}shopee_product_version` WHERE item_id = '" . (int)$product['item_id'] . "'");

        foreach ($productVersionBestme->rows as $p_bestme) {
            foreach ($productVersionShopee->rows as $p_shopee) {
                if ($p_bestme['version'] == $p_shopee['version_name']) {
                    $this->db->query("UPDATE `{$this->db_prefix}shopee_product_version` 
                                            SET `bestme_product_id` = {$product_id}, 
                                                `bestme_product_version_id` = {$p_bestme['product_version_id']}, 
                                                `sync_status` = 1 
                                                WHERE `id` = {$p_shopee['id']} 
                                            ");
                }
            }
        }
    }
}