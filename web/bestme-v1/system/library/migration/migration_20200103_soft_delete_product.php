<?php

namespace Migration;

class migration_20200103_soft_delete_product extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200103_soft_delete_product';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate add deleted to product and product_version tables');

        // product
        try {
            $this->db->query("ALTER TABLE `{$this->db_prefix}product` 
                              ADD `deleted` VARCHAR(50) DEFAULT NULL AFTER `demo`;");
        } catch (\Exception $e) {
            $this->log('migrate soft_delete_product: error: ' . $e->getMessage());
        }

        // product version
        try {
            $this->db->query("ALTER TABLE `{$this->db_prefix}product_version` 
                              ADD `deleted` VARCHAR(50) NULL AFTER `status`;");
        } catch (\Exception $e) {
            $this->log('migrate soft_delete_product: error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back remove field deleted in product and product_version table');

        // product
        try {
            $this->db->query("ALTER TABLE `{$this->db_prefix}product` DROP `deleted`;");
        } catch (\Exception $e) {
        }

        // product version
        try {
            $this->db->query("ALTER TABLE `{$this->db_prefix}product_version` DROP `deleted`;");
        } catch (\Exception $e) {
        }
    }
}