<?php

namespace Migration;

class migration_20200921_lazada_product_short_description extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200921_lazada_product_short_description';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate change type some fields on table oc_lazada_product');
        try {
            $this->migrateChangeTypeFields();
        } catch (\Exception $e) {
            $this->log('change type fields on table oc_lazada_product got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back ....');
    }

    protected function migrateChangeTypeFields()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}lazada_product` CHANGE 
                              `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, 
                              CHANGE `short_description` `short_description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;";

        $this->db->query($sql);
    }
}