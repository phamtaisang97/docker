<?php


namespace Migration;


class migration_20210817_add_column_source_sync extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20210817_add_column_source_sync';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('add column source for table oc_product, oc_manufacturer, oc_category, oc_blog_category, oc_blog, oc_page_contents, oc_discount');
        try {
            $this->addColumnSourceProductTable();
        } catch (\Exception $e) {
            $this->log('add column source product table got error: ' . $e->getMessage());
        }
        try {
            $this->addColumnSourceManufacturerTable();
        } catch (\Exception $e) {
            $this->log('add column source manufacturer table got error: ' . $e->getMessage());
        }
        try {
            $this->addColumnSourceBlogTable();
        } catch (\Exception $e) {
            $this->log('add column source blog table got error: ' . $e->getMessage());
        }
        try {
            $this->addColumnSourceBlogCategoryTable();
        } catch (\Exception $e) {
            $this->log('add column source blog category table got error: ' . $e->getMessage());
        }
        try {
            $this->addColumnSourcePageContentsTable();
        } catch (\Exception $e) {
            $this->log('add column source page contents table got error: ' . $e->getMessage());
        }
        try {
            $this->addColumnSourceDiscountTable();
        } catch (\Exception $e) {
            $this->log('add column source page contents table got error: ' . $e->getMessage());
        }

        try {
            $this->addColumnSourceCategoryTable();
        } catch (\Exception $e) {
            $this->log('add column source category table got error: ' . $e->getMessage());
        }
    }


    private function addColumnSourceProductTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}product` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `user_create_id`";
        $this->db->query($sql);
    }

    private function addColumnSourceManufacturerTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}manufacturer` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `user_create_id`";
        $this->db->query($sql);
    }

    private function addColumnSourceBlogTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}blog` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `demo`";
        $this->db->query($sql);
    }

    private function addColumnSourceBlogCategoryTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}blog_category` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `status`";
        $this->db->query($sql);
    }

    private function addColumnSourcePageContentsTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}page_contents` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `seo_description`";
        $this->db->query($sql);
    }

    private function addColumnSourceDiscountTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}discount` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `updated_at`";
        $this->db->query($sql);
    }

    private function addColumnSourceCategoryTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}category` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `status`;";
        $this->db->query($sql);
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back create sync table');
    }
}