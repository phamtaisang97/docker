<?php

namespace Migration;

class migration_20201019_bestme_package extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20201019_bestme_package';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate insertPacketConfig in setting table');

        try {
            $this->insertPacketConfig();
            $this->addColumnSizeImage();
        } catch (\Exception $e) {
            $this->log('insertPacketConfig setting got error: ' . $e->getMessage());
        }
    }

    protected function insertPacketConfig()
    {
        // Insert config: các shop cũ giới hạn dung lượng 10GB
        // Lưu ý: Các shop mới chạy opencart.sql thì các chỉ số đều không giới hạn

        $sql = "INSERT INTO `{$this->db_prefix}setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES 
                    ('0','config','config_packet_capacity','10',0),
                    ('0','config','config_packet_number_of_stores','',0),
                    ('0','config','config_packet_number_of_staffs','',0),
                    ('0','config','config_packet_unlimited_capacity','0',0),
                    ('0','config','config_packet_unlimited_stores','1',0),
                    ('0','config','config_packet_unlimited_staffs','1',0);";
        $this->db->query($sql);
    }

    private function addColumnSizeImage()
    {
        // Thêm cột size cho bảng image
        $sql = "ALTER TABLE `{$this->db_prefix}images` 
                    ADD `size` FLOAT (5)  NOT NULL DEFAULT 0 AFTER `source_id`;";
        $this->db->query($sql);
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back ....');
    }
}