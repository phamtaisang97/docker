<?php

namespace Migration;

class migration_20201201_shopee_category_and_logistics extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20201201_shopee_category_and_logistics';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration, $version)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION, $version);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->addColumnVersionToTableMigragion();

        $this->createShopeeCategoryTable();

        $this->createShopeeLogisticTable();

        $data = $this->getCacheFile();

        $this->log('add columns to table shopee_category ');
        try {
            $this->updateCategory($data['categories']);
        } catch (\Exception $e) {
            $this->log('add columns to table report got error: ' . $e->getMessage());
        }

        $this->log('add columns to table shopee_logistic ');
        try {
            $this->updateLogistics($data['logistics']);
        } catch (\Exception $e) {
            $this->log('add columns to table report got error: ' . $e->getMessage());
        }
    }

    private function addColumnVersionToTableMigragion()
    {
        try {
            $sql = "ALTER TABLE `{$this->db_prefix}migration`
                  ADD `version` INT(11) DEFAULT 1 NULL AFTER `date_run`";
            $this->db->query($sql);
        } catch (\Exception $e) {
            $this->log('add columns to table migration got error: ' . $e->getMessage());
        }
    }

    private function createShopeeCategoryTable()
    {
        try {
            $this->log('add table shopee_category');
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}shopee_category` ( 
                `shopee_category_id` BIGINT(64) NOT NULL AUTO_INCREMENT , 
                `parent_id` BIGINT(64) NOT NULL , 
                `name_vi` VARCHAR(255) NULL , 
                `name_en` VARCHAR(255) NULL , 
                `has_children` BOOLEAN NOT NULL , 
                PRIMARY KEY (`shopee_category_id`)) ENGINE = MyISAM 
                CHARSET=utf8 COLLATE utf8_general_ci;";
            $this->db->query($sql);
        } catch (\Exception $e) {
            $this->log('add table migration got error: ' . $e->getMessage());
        }
    }

    private function createShopeeLogisticTable()
    {
        try {
            $this->log('add table shopee_logistics');
            $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}shopee_logistics` ( 
                `logistic_id` INT(11) NOT NULL AUTO_INCREMENT , 
                `logistic_name` VARCHAR(255) NULL , 
                `enabled` BOOLEAN NULL , 
                `fee_type` VARCHAR(255) NULL , 
                `sizes` TEXT NULL , 
                `item_max_weight` DECIMAL(15,8) NULL , 
                `item_min_weight` DECIMAL(15,8) NULL , 
                `height` DECIMAL(15,8) NULL , 
                `width` DECIMAL(15,8) NULL , 
                `length` DECIMAL(15,8) NULL , 
                `volumetric_factor` INT(11) NULL , 
                PRIMARY KEY (`logistic_id`)) ENGINE = MyISAM 
                CHARSET=utf8 COLLATE utf8_general_ci;";
            $this->db->query($sql);
        } catch (\Exception $e) {
            $this->log('add table migration got error: ' . $e->getMessage());
        }
    }

    private function updateCategory($categories)
    {
        $this->db->query("DELETE FROM `{$this->db_prefix}shopee_category` where 1");

        $sql = "INSERT INTO `{$this->db_prefix}shopee_category` (`shopee_category_id`, `parent_id`, `name_vi`, `name_en`, `has_children`) VALUES";
        $insert_data = [];

        foreach ($categories as $category) {
            $has_children = $category['has_children'] ? 1 : 0;
            $insert_data[] = "(
                {$category['category_id']},
                {$category['parent_id']},
                '{$this->db->escape($category['name_vi'])}',
                '{$this->db->escape($category['name_en'])}',
                $has_children
            )";
        }

        $sql .= implode(', ', $insert_data);
        $this->db->query($sql);
    }

    private function updateLogistics($logistics)
    {
        $this->db->query("DELETE FROM `{$this->db_prefix}shopee_logistics` where 1");

        $sql = "INSERT INTO `{$this->db_prefix}shopee_logistics` (`logistic_id`, `logistic_name`, `sizes`, `item_max_weight`, `item_min_weight`, `height`, `width`, `length`, `volumetric_factor`) VALUES";
        $insert_data = [];

        foreach ($logistics as $logistic) {
            $size = json_encode($logistic['sizes']);
            $height = isset($logistic['item_max_dimension']['height']) ? $logistic['item_max_dimension']['height'] : 'NULL';
            $width = isset($logistic['item_max_dimension']['width']) ? $logistic['item_max_dimension']['width'] : 'NULL';
            $length = isset($logistic['item_max_dimension']['length']) ? $logistic['item_max_dimension']['length'] : 'NULL';
            $volumetric_factor = isset($logistic['volumetric_factor']) ? $logistic['volumetric_factor'] : 0;

            $insert_data[] = "(
                {$logistic['logistic_id']},
                '{$this->db->escape($logistic['logistic_name'])}',
                '{$this->db->escape($size)}',
                '{$logistic['weight_limits']['item_max_weight']}',
                '{$logistic['weight_limits']['item_min_weight']}',
                $height,
                $width,
                $length,
                $volumetric_factor
            )";
        }

        $sql .= implode(', ', $insert_data);
        $this->db->query($sql);
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back column payment_status table report version');
    }
}