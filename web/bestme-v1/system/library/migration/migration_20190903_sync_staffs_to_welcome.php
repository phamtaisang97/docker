<?php

namespace Migration;

use Exception;
use Redis;

class migration_20190903_sync_staffs_to_welcome extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20190903_sync_staffs_to_welcome';

    /**
     * @var string
     */
    private $shop_name = null;

    /**
     * @var null|Redis
     */
    private $redis = null;

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will push all staffs information to welcome code base to sync customer-staff info for login');

        $this->syncStaffsToWelcomeDatabase();
    }

    protected function syncStaffsToWelcomeDatabase()
    {
        try {
            /* get all staffs */
            $sql = "SELECT * FROM `{$this->db_prefix}user` WHERE `type` = 'Staff'";

            $query = $this->db->query($sql);
            $all_staffs = $query->rows;

            /* get shop name */
            if (empty($this->shop_name)) {
                $query = $this->db->query("SELECT `value` FROM `{$this->db_prefix}setting` WHERE `key`= 'shop_name'");
                $this->shop_name = $query->row['value'];
            }

            /* get redis instance */
            if (!$this->redis instanceof Redis) {
                $this->redis = $this->redisConnect();
            }

            /* push to sync */
            foreach ($all_staffs as $staff) {
                if (!is_array($staff)) {
                    continue;
                }

                $this->publishCustomerStaff($staff);
            }
        } catch (\Exception $e) {
            // may columns existed for new shop! Catch to mark migrate done!
        }

        $this->redisClose($this->redis);
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back all staffs information from welcome code base to remove sync customer-staff info for login');
        // no need
    }

    /**
     * @param array $user
     * @param string $action
     */
    private function publishCustomerStaff(array $user, $action = 'add')
    {
        if (!isset($user['email']) ||
            !isset($user['telephone'])
        ) {
            return;
        }

        $data['email'] = $user['email'];
        $data['firstname'] = isset($user['firstname']) ? $user['firstname'] : '';
        $data['lastname'] = isset($user['lastname']) ? $user['lastname'] : '';
        $data['shopname'] = $this->shop_name;
        $data['phone'] = $user['telephone'];
        $data['action'] = $action;
        $data_json = json_encode($data);

        $this->redis->publish(MUL_REDIS_CUSTOMER_STAFF_PUBLISH_CHANEL, $data_json);
    }

    /**
     * @return null|Redis
     */
    private function redisConnect()
    {
        $redis = null;

        try {
            $redis = new \Redis();
            $redis->connect(MUL_REDIS_HOST, MUL_REDIS_PORT);
            $redis->select(MUL_REDIS_DB);
        } catch (Exception $e) {
            // could not connect to redis
        }

        return $redis;
    }

    /**
     * @param Redis $redis
     */
    private function redisClose($redis)
    {
        if (!$redis instanceof \Redis) {
            return;
        }

        try {
            $redis->close();
        } catch (Exception $e) {
            // could not connect to redis
        }
    }
}