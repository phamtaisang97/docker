<?php

namespace Migration;

class migration_20220825_add_column_show_gg_product_feed_for_product_table extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20220825_add_column_show_gg_product_feed_for_product_table';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('add column show_gg_product_feed to table product');

        try {
            $this->addColumnShowGgProductFeedProductTable();
        } catch (\Exception $e) {
            $this->log('add column show_gg_product_feed to table product got error: ' . $e->getMessage());
        }

    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back add column show_gg_product_feed to table product');
    }

    protected function addColumnShowGgProductFeedProductTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}product` 
                ADD `show_gg_product_feed` TINYINT(1) NOT NULL DEFAULT '1' AFTER `date_modified`;";
        $this->db->query($sql);
    }
}