<?php


namespace Migration;


class migration_20210107_tuning_2_11_2 extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20210107_tuning_2_11_2';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /**
     * @inheritDoc
     */
    public function do_up()
    {
        /* image for each version of product multi versions */
        $this->log('Will add image column to product_version table');
        try {
            $this->addImageColumnToProductVersionTable();
        } catch (\Exception $e) {
            $this->log('add image column to product_version table got error: ' . $e->getMessage());
        }

        /* more description tab for product */
        $this->log('Will create product_description_tab table');
        try {
            $this->createProductDescriptionTabTable();
        } catch (\Exception $e) {
            $this->log('create product_description_tab table got error: ' . $e->getMessage());
        }

        /* homepage rating website (customer feedback) */
        $this->log('Will create rate_website table');
        try {
            $this->createTableRateWebsite();
            // permission for admin user
            $this->addPermissionForAdminUser([
                'section/rate',
                'rate/rate'
            ]);
        } catch (\Exception $e) {
            $this->log('Create rate_website table got error: ' . $e->getMessage());
        }

        /* shopee/lazada config override product info... */
        $this->addColumnShopeeAndLazadaConfig();

        /* config homepage block displaying */
        $this->log('addPermissionForAdminUser (section/blog)');
        // permission for admin user
        try {
            $this->addPermissionForAdminUser([
                'section/blog'
            ]);
        } catch (\Exception $e) {
            $this->log('addPermissionForAdminUser (section/blog) got error: ' . $e->getMessage());
        }
    }

    /* === local functions === */

    private function addImageColumnToProductVersionTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}product_version`
	            ADD COLUMN `image` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci' AFTER `deleted`,
	            ADD COLUMN `image_alt` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci' AFTER `image`;";
        $this->db->query($sql);
    }

    private function createProductDescriptionTabTable()
    {
        $sql = "CREATE TABLE `{$this->db_prefix}product_description_tab` (
                `product_description_tab_id` int(11) NOT NULL AUTO_INCREMENT,  
                `product_id` int(11) NOT NULL,
                `title` varchar(255) COLLATE utf8_general_ci NOT NULL,
                `description` text COLLATE utf8_general_ci NOT NULL,
                PRIMARY KEY (`product_description_tab_id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;";
        $this->db->query($sql);
    }

    private function createTableRateWebsite()
    {
        // create table `rate_website`
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}rate_website` (
                  `rate_id` int(11) NOT NULL AUTO_INCREMENT,
                  `status` tinyint(4) NOT NULL,
                  `customer_name` varchar(255) NOT NULL,
                  `sub_info` text,
                  `content` text,
                  `image` varchar(255) DEFAULT NULL,
                  `alt` varchar(255) DEFAULT NULL,
                  `date_added` datetime NOT NULL,
                  `date_modified` datetime NOT NULL,
                  PRIMARY KEY (`rate_id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        $this->db->query($sql);
    }

    private function addColumnShopeeAndLazadaConfig()
    {
        $this->log('Will add column to shopee_config table');
        try {
            $sql = "ALTER TABLE `{$this->db_prefix}shopee_shop_config` 
                ADD `order_sync_range` INT(11) NULL DEFAULT '15' AFTER `connected`, 
                ADD `allow_update_stock_product` TINYINT(2) NULL DEFAULT '0' AFTER `order_sync_range`, 
                ADD `allow_update_price_product` TINYINT(2) NULL DEFAULT '0' AFTER `allow_update_stock_product`, 
                ADD `last_sync_time_product` TIMESTAMP NULL AFTER `allow_update_price_product`, 
                ADD `last_sync_time_order` TIMESTAMP NULL AFTER `last_sync_time_product`;";

            $this->db->query($sql);
        } catch (\Exception $e) {
            $this->log('add column to shopee_config table got error: ' . $e->getMessage());
        }

        $this->log('Will add column to lazada_config table');
        try {
            $sql = "ALTER TABLE `{$this->db_prefix}lazada_shop_config` 
                ADD `order_sync_range` INT(11) NULL DEFAULT '15' AFTER `connected`, 
                ADD `allow_update_stock_product` TINYINT(2) NULL DEFAULT '0' AFTER `order_sync_range`, 
                ADD `allow_update_price_product` TINYINT(2) NULL DEFAULT '0' AFTER `allow_update_stock_product`, 
                ADD `last_sync_time_product` TIMESTAMP NULL AFTER `allow_update_price_product`, 
                ADD `last_sync_time_order` TIMESTAMP NULL AFTER `last_sync_time_product`;";

            $this->db->query($sql);
        } catch (\Exception $e) {
            $this->log('add column to lazada_config table got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function do_down()
    {
        $this->log('Will roll back tuning_2_11_2... update later...');
    }
}