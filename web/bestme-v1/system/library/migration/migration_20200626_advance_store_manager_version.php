<?php

namespace Migration;

class migration_20200626_advance_store_manager_version extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20200626_advance_store_manager_version';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate create table for advance_store_manager version');
        try {
            $this->migrateCreateTablesForAdvanceStoreManager();

        } catch (\Exception $e) {
            $this->log('crate table for advance_store_manager version got error: ' . $e->getMessage());
        }

        // permission for admin user
        try {
            $this->addPermissionForAdminUser([
                'catalog/cost_adjustment_receipt',
                'catalog/store_take_receipt',
                'catalog/store_transfer_receipt'
            ]);
        } catch (\Exception $e) {
            $this->log('addPermissionForAdminUser (advance_store_manager) got error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back remove some table of advance_store_manager version');
    }

    protected function migrateCreateTablesForAdvanceStoreManager()
    {
        // store_transfer_receipt : Table to save transfer receipt of store
        $sql = "CREATE TABLE IF NOT EXISTS  `{$this->db_prefix}store_transfer_receipt` ( 
                  `store_transfer_receipt_id` INT(11) NOT NULL AUTO_INCREMENT,
                  `code` VARCHAR(128) NOT NULL, 
                  `export_store_id` INT(11) NOT NULL, 
                  `import_store_id` INT(11) NOT NULL, 
                  `common_quantity_transfer` INT(16) NULL,
                  `total` decimal(18,4) NOT NULL, 
                  `status` TINYINT(2) NOT NULL DEFAULT '0' COMMENT '0: lưu nháp, 1: đã chuyển, 2: đã nhận, 9: đã hủy', 
                  `date_exported` DATE NOT NULL, 
                  `date_imported` DATE NULL, 
                  `date_added` DATETIME NOT NULL, 
                  `date_modified` DATETIME NOT NULL,
                  PRIMARY KEY (`store_transfer_receipt_id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);

        // store_transfer_receipt_to_product : relationship transfer receipt with product
        $sql = "CREATE TABLE IF NOT EXISTS  `{$this->db_prefix}store_transfer_receipt_to_product` ( 
                  `store_transfer_receipt_id` INT(11) NOT NULL,
                  `product_id` INT(11) NOT NULL, 
                  `product_version_id` INT(11) NOT NULL DEFAULT '0', 
                  `quantity` INT(11) NOT NULL, 
                  `current_quantity` INT(11) NULL, 
                  `price` decimal(18,4) NOT NULL, 
                  PRIMARY KEY (`store_transfer_receipt_id`, `product_id`, `product_version_id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);

        // store_take_receipt : Table to save take receipt of store
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}store_take_receipt` ( 
                  `store_take_receipt_id` INT(11) NOT NULL AUTO_INCREMENT,
                  `code` VARCHAR(128) NOT NULL,
                  `store_id` INT(11) NOT NULL, 
                  `difference_amount` decimal(18,4) NOT NULL, 
                  `status` TINYINT(2) NOT NULL DEFAULT '0',
                  `date_taked` DATE NOT NULL,  
                  `date_added` DATETIME NOT NULL, 
                  `date_modified` DATETIME NOT NULL,
                  PRIMARY KEY (`store_take_receipt_id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);

        //store_take_receipt_to_product : Relationship for take receipt with product
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}store_take_receipt_to_product` ( 
                  `store_take_receipt_id` INT(11) NOT NULL,
                  `product_id` INT(11) NOT NULL, 
                  `product_version_id` INT(11) NOT NULL DEFAULT '0',
                  `inventory_quantity` INT(11) NOT NULL, 
                  `actual_quantity` INT(11) NOT NULL, 
                  `difference_amount` decimal(18,4) NOT NULL, 
                  `reason`  VARCHAR(256),
                  PRIMARY KEY (`store_take_receipt_id`, `product_id`, `product_version_id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);

        // cost_adjustment_receipt: table to save cost adjustment receipt
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}cost_adjustment_receipt` ( 
                  `cost_adjustment_receipt_id` INT NOT NULL AUTO_INCREMENT, 
                  `cost_adjustment_receipt_code` VARCHAR(255) NULL,
                  `store_id` INT(255) NOT NULL, 
                  `note` TEXT NULL, 
                  `status` tinyint(2) NULL DEFAULT 0,
                  `date_added` datetime NOT NULL,
                  `date_modified` datetime NOT NULL,
                  PRIMARY KEY (`cost_adjustment_receipt_id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);

        // cost_adjustment_receipt_to_product: Relationship of cost adjustment receipt with product
        $sql = "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}cost_adjustment_receipt_to_product` ( 
                  `cost_adjustment_receipt_id` INT NOT NULL, 
                  `product_id` INT(255) NOT NULL, 
                  `product_version_id` INT(255) NOT NULL DEFAULT '0', 
                  `adjustment_cost_price` DECIMAL(18,4) NULL, 
                  `current_cost_price` DECIMAL(18,4) NULL, 
                  `note` VARCHAR(255) NULL,
                  PRIMARY KEY (`cost_adjustment_receipt_id`, `product_id`, `product_version_id`)
                ) ENGINE = MyISAM DEFAULT CHARSET=utf8;";

        $this->db->query($sql);
    }
}