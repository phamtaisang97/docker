<?php

namespace Migration;

class migration_20191127_sync_onboading_to_welcome extends migration_abstract
{
    use \Sync_Bestme_Data_To_Welcome_Util;

    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20191127_sync_onboading_to_welcome';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will sync onboading data to welcome db');

        // for admin user
        try {
            $this->syncOnboardingToWelcomeDatabase();
        } catch (\Exception $e) {
            $this->log('syncOnboardingToWelcomeDatabase got error: ' . $e->getMessage());
        }
    }

    protected function syncOnboardingToWelcomeDatabase()
    {
        /* get current onboarding step */
        $current_onBoarding_steps_count = 0;
        $last_onBoarding_steps = 0;

        $sql = "SELECT * FROM `{$this->db_prefix}setting` WHERE `key` = 'config_onboarding_complete'";
        $query = $this->db->query($sql);
        $is_onBoarding_complete = !isset($query->row['value']) || ($query->row['value'] !== 0 && $query->row['value'] !== '0'); // complete: 1 or null or empty

        // get current onboarding steps count
        $sql = "SELECT * FROM `{$this->db_prefix}setting` WHERE `key` = 'config_onboarding_step_active'";
        $query = $this->db->query($sql);
        $onboarding_steps = isset($query->row['value']) ? $query->row['value'] : ''; // 1, 2, 3
        if (!empty($onboarding_steps)) {
            $onboarding_steps = explode(',', $onboarding_steps);
            $last_onBoarding_steps = intval(end($onboarding_steps));

            if ($is_onBoarding_complete) {
                $current_onBoarding_steps_count = 99;
            } else {
                $current_onBoarding_steps_count = count($onboarding_steps);
            }
        }

        /* get shop name */
        $shop_name = '';
        if (empty($this->shop_name)) {
            $query = $this->db->query("SELECT `value` FROM `{$this->db_prefix}setting` WHERE `key`= 'shop_name'");
            $shop_name = $query->row['value'];
        }

        /* sync */
        $data = [
            'shop_domain' => $shop_name,
            'last_step' => $last_onBoarding_steps,
            'steps' => $current_onBoarding_steps_count
        ];

        $this->publishSyncDataOnboarding($data);
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        // no need
    }
}