<?php

namespace Migration;

class migration_20190810_staff_permission extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20190810_staff_permission';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate add permission common/filemanager, custom/menu (by custom/group_men)  for staff');

        $this->addPermission();
    }

    protected function addPermission()
    {
        $new_permissions = array(
            'common/filemanager',
        );

        // condition permission => new permissions to be added depend on condition
        $depend_permissions = array(
            // for menu permission only
            'custom/group_menu' => ['custom/menu'],
            // for product permission only
            'catalog/product' => ['catalog/collection', 'catalog/manufacturer', 'catalog/warehouse'],
            // for theme builder permission only
            'custom/theme' => [
                'extension/module/theme_builder_config',
                'section/new_product',
                // theme (general) setting in theme builder
                'theme/color',
                'theme/favicon',
                'theme/preview',
                'theme/section_theme',
                'theme/social',
                'theme/text',
            ],
        );
        $staffs = $this->db->query("SELECT * FROM `{$this->db_prefix}user_group` WHERE `user_group_id` NOT IN (1, 10)");

        foreach ($staffs->rows as $staff) {
            if (!array_key_exists('user_group_id', $staff) || !array_key_exists('permission', $staff)) {
                continue;
            }

            $staffPermission = json_decode($staff['permission'], true);

            // for new permission
            foreach ($new_permissions as $permission) {
                if (array_key_exists('access', $staffPermission)) {
                    if (!in_array($permission, $staffPermission['access'])) {
                        $staffPermission['access'][] = $permission;
                    }
                }

                if (array_key_exists('modify', $staffPermission)) {
                    if (!in_array($permission, $staffPermission['modify'])) {
                        $staffPermission['modify'][] = $permission;
                    }
                }
            }

            // for depend permission
            foreach ($depend_permissions as $condition => $new_permissions) {
                if (array_key_exists('access', $staffPermission) && in_array($condition, $staffPermission['access'])) {
                    foreach ($new_permissions as $permission) {
                        if (!in_array($permission, $staffPermission['access'])) {
                            $staffPermission['access'][] = $permission;
                        }
                    }
                }

                if (array_key_exists('modify', $staffPermission) && in_array($condition, $staffPermission['modify'])) {
                    foreach ($new_permissions as $permission) {
                        if (!in_array($permission, $staffPermission['modify'])) {
                            $staffPermission['modify'][] = $permission;
                        }
                    }
                }
            }

            $this->db->query("UPDATE `{$this->db_prefix}user_group` SET permission = '" . $this->db->escape(json_encode($staffPermission)) . "' WHERE `user_group_id` = '" . (int)$staff['user_group_id'] . "'");
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back permission staff');
        // no need
    }
}