<?php

namespace Migration;

class migration_20211201_add_source_author_name_to_blog_description_table extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20211201_add_source_author_name_to_blog_description_table';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Add source_author_name column to blog_description table');

        try {
            $this->addSourceAuthorNameColumnToBlogDescriptionTable();
        } catch (\Exception $e) {
            $this->log('Could not add source_author_name column to blog_description table. Got error: ' . $e->getMessage());
        }

    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('This should be rollback the migration and delete source_author_name column from blog_description table ...!');
    }

    protected function addSourceAuthorNameColumnToBlogDescriptionTable()
    {
        $sql = "ALTER TABLE `{$this->db_prefix}blog_description` 
                ADD `source_author_name` VARCHAR(255) NULL DEFAULT NULL AFTER `video_url`;";
        $this->db->query($sql);
    }
}