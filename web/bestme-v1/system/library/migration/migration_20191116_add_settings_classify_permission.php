<?php

namespace Migration;

class migration_20191116_add_settings_classify_permission extends migration_abstract
{
    /* NOTICE: same name with current class! */
    static $MIGRATION = 'migration_20191116_add_settings_classify_permission';

    public function __construct(\DB $db, \Log $log, $db_prefix, $migration)
    {
        parent::__construct($db, $log, $db_prefix, self::$MIGRATION);
    }

    /* === default functions === */

    /**
     * @inheritdoc
     */
    public function do_up()
    {
        $this->log('Will migrate add permission settings classify to admin and staff user');

        // for admin user
        try {
            $this->addPermissionForSettingsClassifyForAdminUser();
        } catch (\Exception $e) {
            $this->log('addPermissionForSettingsClassifyForAdminUser got error: ' . $e->getMessage());
        }

        // for staff users
        try {
            $this->addPermissionForSettingsClassifyForStaffUser();
        } catch (\Exception $e) {
            $this->log('addPermissionForSettingsClassifyForStaffUser got error: ' . $e->getMessage());
        }
    }

    protected function addPermissionForSettingsClassifyForAdminUser()
    {
        $new_permissions = array(
            'settings/classify'
        );

        $admin_permissions = $this->db->query("SELECT * FROM `{$this->db_prefix}user_group` WHERE `user_group_id` IN (1, 10)");

        foreach ($admin_permissions->rows as $admin) {
            if (!is_array($admin) || !array_key_exists('user_group_id', $admin) || !array_key_exists('permission', $admin)) {
                continue;
            }

            $admin_permission = json_decode($admin['permission'], true);

            // for new permission
            foreach ($new_permissions as $permission) {
                if (is_array($admin_permission) && array_key_exists('access', $admin_permission) && is_array($admin_permission['access'])) {
                    if (!in_array($permission, $admin_permission['access'])) {
                        $admin_permission['access'][] = $permission;
                    }
                }

                if (is_array($admin_permission) && array_key_exists('modify', $admin_permission) && is_array($admin_permission['modify'])) {
                    if (!in_array($permission, $admin_permission['modify'])) {
                        $admin_permission['modify'][] = $permission;
                    }
                }
            }

            $this->db->query("UPDATE `{$this->db_prefix}user_group` 
                              SET permission = '" . $this->db->escape(json_encode($admin_permission)) . "' 
                              WHERE `user_group_id` = '" . (int)$admin['user_group_id'] . "'");
        }
    }

    protected function addPermissionForSettingsClassifyForStaffUser()
    {
        // condition permission => new permissions to be added depend on condition
        $depend_permissions = array(
            // for setting permission only
            'settings/account' => ['settings/classify']
        );
        $staffs = $this->db->query("SELECT * FROM `{$this->db_prefix}user_group` WHERE `user_group_id` NOT IN (1, 10)");

        foreach ($staffs->rows as $staff) {
            if (!array_key_exists('user_group_id', $staff) || !array_key_exists('permission', $staff)) {
                continue;
            }

            $staffPermission = json_decode($staff['permission'], true);

            // for depend permission
            foreach ($depend_permissions as $condition => $new_permissions) {
                if (array_key_exists('access', $staffPermission) && in_array($condition, $staffPermission['access'])) {
                    foreach ($new_permissions as $permission) {
                        if (!in_array($permission, $staffPermission['access'])) {
                            $staffPermission['access'][] = $permission;
                        }
                    }
                }

                if (array_key_exists('modify', $staffPermission) && in_array($condition, $staffPermission['modify'])) {
                    foreach ($new_permissions as $permission) {
                        if (!in_array($permission, $staffPermission['modify'])) {
                            $staffPermission['modify'][] = $permission;
                        }
                    }
                }
            }

            $this->db->query("UPDATE `{$this->db_prefix}user_group` 
                              SET permission = '" . $this->db->escape(json_encode($staffPermission)) . "' 
                              WHERE `user_group_id` = '" . (int)$staff['user_group_id'] . "'");
        }
    }

    /**
     * @inheritdoc
     */
    public function do_down()
    {
        $this->log('Will roll back permission settings/classify from admin user and staff');
        // no need
    }
}