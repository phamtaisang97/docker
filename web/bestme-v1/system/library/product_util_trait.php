<?php

/**
 * Product_Util_Trait class
 */
trait Product_Util_Trait
{
    use Device_Util;
    /* avoid get all from DB! */
    public static $OPTIMIZED_LIMIT = 1e4;

    public static $PRODUCT_BLOCK_RESOURCE_TYPE_COLLECTION = 1;
    public static $PRODUCT_BLOCK_RESOURCE_TYPE_CATEGORY = 2;

    public static $PRODUCT_BLOCK_DISPLAY_NUMBER_OF_ROW_DEFAULT = 1;

    public static $PRODUCT_BLOCK_AUTOPLAY_DEFAULT = 1;
    public static $PRODUCT_BLOCK_AUTOPLAY_TIME_DEFAULT = 5;
    public static $PRODUCT_BLOCK_LOOP_DEFAULT = 1;

    /* for caching between functions call */
    // this conflict with parent class call this trait, the error:
    // Unknown: ControllerCommonHome and Product_Util_Trait define the same property ($hot_products) in the composition of ControllerCommonHome.
    // This might be incompatible, to improve maintainability consider using accessor methods in traits instead.
    // Class was composed in /var/www/html/x2_projects/bestme/novaon_x2/catalog/controller/common/home.php on line
    //
    private $hot_products = [];
    private $hot_products_with_config = [];

    private $best_sale_products = [];
    private $best_sale_products_with_config = [];

    private $new_products = [];
    private $new_products_with_config = [];

    private $IS_DEBUG_RUNNING_TIME = false;

    /**
     * @param $multi_versions
     * @param $compare_price_master
     * @param $min_compare_price_version
     * @return bool
     */
    public function isContactForPrice($multi_versions, $compare_price_master, $min_compare_price_version)
    {
        $is_multi_versions = isset($multi_versions) && $multi_versions == 1;
        if (!$is_multi_versions) {
            if ((int)($compare_price_master) == 0) {
                return true;
            }
        } else {
            if ((int)($min_compare_price_version) == 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * getBestSalesProducts
     *
     * @param int $visible
     * @param int|null $limit default null for all
     * @return array
     */
    public function getBestSalesProducts($visible = 1, $limit = null)
    {
        $data = [];

        $this->load->model('extension/module/theme_builder_config');
        $best_sales_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_BEST_SALES_PRODUCT);

        $best_sales_config = json_decode($best_sales_config, true);

        $best_sales_config['visible'] = $visible;

        if ($best_sales_config['visible'] == 0) {
            $data['best_sales_product_visible'] = false;
            return $data;
        }

        $data['best_sales_product_title'] = '';
        if (isset($best_sales_config['setting']['title'])) {
            $data['best_sales_product_title'] = $best_sales_config['setting']['title'];
        }
        $data['best_sales_sub_title'] = '';
        if (isset($best_sales_config['setting']['sub_title'])) {
            $data['best_sales_sub_title'] = $best_sales_config['setting']['sub_title']; // backward compatibility
            $data['best_sales_product_sub_title'] = $best_sales_config['setting']['sub_title']; // new key
        }

        /* optimized limit */
        $limit = empty($limit) || (int)$limit < 0 ? self::$OPTIMIZED_LIMIT : (int)$limit;
        $best_sales_product = [];
        $collections = [];

        $this->load->model('catalog/product');
        $this->load->model('catalog/collection');
        $this->load->model('tool/image');

        /* get products, sub-collections due to config */
        // check if get from special collection in config
        if ($best_sales_config['setting']['auto_retrieve_data'] == 0) {
            $resource_type = (isset($best_sales_config['setting']['resource_type'])) ? $best_sales_config['setting']['resource_type'] : self::$PRODUCT_BLOCK_RESOURCE_TYPE_COLLECTION; // default collection for backward compatibility!
            // if resource type = collection
            if ($resource_type == self::$PRODUCT_BLOCK_RESOURCE_TYPE_COLLECTION) {
                $filter['filter_collection_id'] = $best_sales_config['setting']['collection_id'];
                $filter['limit'] = $limit;

                $current_collection = $this->model_catalog_collection->getCollection($filter);
                // skip collection if not available (not existed or disabled)
                if (!isset($current_collection['collection_id']) || empty($current_collection['collection_id'])) {
                    // do something...
                } else {
                    // check if collection contains product only (type not set or type = 0)
                    if (!isset($current_collection['type']) || $current_collection['type'] != 1) {
                        $best_sales_product = $this->model_catalog_product->getProductsByColection($filter);
                    } else {
                        $collections = $this->model_catalog_collection->getCollections($filter);

                        // resize collections images
                        foreach ($collections as &$collection) {
                            if ($collection['image']) {
                                // no need resize
                            } else {
                                $collection['image'] = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
                            }
                            $collection['url'] = $this->url->link('common/shop', 'collection=' . $collection['collection_id']);
                        }
                        unset($collection);
                    }

                    $data['current_collection_image'] = isset($current_collection['image']) ? $this->changeUrl($current_collection['image']) : '';
                }
            } elseif ($resource_type == self::$PRODUCT_BLOCK_RESOURCE_TYPE_CATEGORY) { // if resource type = category
                $filter['filter_category_id'] = isset($best_sales_config['setting']['category_id']) ? $best_sales_config['setting']['category_id'] : '';
                $filter['limit'] = $limit;

                if ($filter['filter_category_id'] != '') {
                    $best_sales_product = $this->model_catalog_product->getProductsByCategoryId($filter['filter_category_id']);
                }
            } else {
                // do something...
            }
        } else {
            $best_sales_product = $this->model_catalog_product->getBestSellerProducts($limit);
            $best_sales_product = is_array($best_sales_product) ? $best_sales_product : [];
        }

        // use this {if} to speed up
        $new_products = [];
        if (!empty($best_sales_product)) {
            /*$new_products = $this->model_catalog_product->getProductNews($limit);*/
            if (empty($this->new_products)) {
                $new_products = $this->new_products = $this->model_catalog_product->getProductNews($limit);
            } else {
                $new_products = $this->new_products;
            }
        }

        $best_sales_product_data = $this->buildProductData($best_sales_product, $bs_ids = [], $new_products, $option = [
            'is_best_sale_product' => true
        ]);

        /* build final hot product data */
        $data['best_sales_product'] = $best_sales_product_data;
        $data['collections_sale'] = $collections;
        $data['best_sales_product_display_grid_quantity'] = isset($best_sales_config['display']['grid']['quantity']) ? $best_sales_config['display']['grid']['quantity'] : 4;
        $data['best_sales_product_display_grid_mobile_quantity'] = isset($best_sales_config['display']['grid_mobile']['quantity']) ? $best_sales_config['display']['grid_mobile']['quantity'] : 1;
        $data['best_sales_product_display_grid_row'] = isset($best_sales_config['display']['grid']['row']) ? $best_sales_config['display']['grid']['row'] : self::$PRODUCT_BLOCK_DISPLAY_NUMBER_OF_ROW_DEFAULT;
        $data['best_sales_product_display_grid_mobile_row'] = isset($best_sales_config['display']['grid_mobile']['row']) ? $best_sales_config['display']['grid_mobile']['row'] : self::$PRODUCT_BLOCK_DISPLAY_NUMBER_OF_ROW_DEFAULT;
        $data['best_sales_product_autoplay'] = isset($best_sales_config['setting']['autoplay']) ? $best_sales_config['setting']['autoplay'] : self::$PRODUCT_BLOCK_AUTOPLAY_DEFAULT;
        $data['best_sales_product_autoplay_time'] = isset($best_sales_config['setting']['autoplay_time']) ? $best_sales_config['setting']['autoplay_time'] : self::$PRODUCT_BLOCK_AUTOPLAY_TIME_DEFAULT;
        $data['best_sales_product_loop'] = isset($best_sales_config['setting']['loop']) ? $best_sales_config['setting']['loop'] : self::$PRODUCT_BLOCK_LOOP_DEFAULT;
        $data['view_more'] = $this->url->link('common/shop');

        if ($best_sales_config['setting']['auto_retrieve_data'] == 0) {
            $resource_type = (isset($best_sales_config['setting']['resource_type'])) ? $best_sales_config['setting']['resource_type'] : self::$PRODUCT_BLOCK_RESOURCE_TYPE_COLLECTION; // default collection for backward compatibility!
            if ($resource_type == self::$PRODUCT_BLOCK_RESOURCE_TYPE_COLLECTION) {
                $data['view_more'] = $this->url->link('common/shop', 'collection=' . $best_sales_config['setting']['collection_id']);
            } elseif ($resource_type == self::$PRODUCT_BLOCK_RESOURCE_TYPE_CATEGORY) { // if resource type = category
                if(isset($best_sales_config['setting']['category_id']) && $best_sales_config['setting']['category_id'] != '') {
                    $data['view_more'] = $this->url->link('common/shop', 'path=' . $best_sales_config['setting']['category_id']);
                }
            }
        }

        if (count($best_sales_product_data) > 0 || $collections) {
            $data['best_sales_product_visible'] = true;
        } else {
            $data['best_sales_product_visible'] = false;
        }

        return $data;
    }

    /**
     * get New Products
     *
     * @param int $visible
     * @param int|null $limit default null for all
     * @return array
     */
    public function getNewProducts($visible = 1, $limit = null)
    {
        $data = [];

        $this->load->model('extension/module/theme_builder_config');
        $new_product_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_NEW_PRODUCT);
        $new_product_config = json_decode($new_product_config, true);

        $new_product_config['visible'] = $visible;

        if ($new_product_config['visible'] == 0) {
            $data['new_product_visible'] = false;
            return $data;
        }

        $data['new_product_title'] = '';
        if (isset($new_product_config['setting']['title'])) {
            $data['new_product_title'] = $new_product_config['setting']['title'];
        }
        $data['new_sub_title'] = '';
        if (isset($new_product_config['setting']['sub_title'])) {
            $data['new_sub_title'] = $new_product_config['setting']['sub_title']; // backward compatibility
            $data['new_product_sub_title'] = $new_product_config['setting']['sub_title']; // new key
        }

        /* optimized limit */
        $limit = empty($limit) || (int)$limit < 0 ? self::$OPTIMIZED_LIMIT : (int)$limit;

        $new_products = [];
        $collections = [];

        $this->load->model('catalog/product');
        $this->load->model('catalog/collection');
        $this->load->model('tool/image');

        /* get products, sub-collections due to config */
        // check if get from special collection in config
        if ($new_product_config['setting']['auto_retrieve_data'] == 0) {
            $resource_type = (isset($new_product_config['setting']['resource_type'])) ? $new_product_config['setting']['resource_type'] : self::$PRODUCT_BLOCK_RESOURCE_TYPE_COLLECTION; // default collection for backward compatibility!
            if ($resource_type == self::$PRODUCT_BLOCK_RESOURCE_TYPE_COLLECTION) {
                $filter['filter_collection_id'] = $new_product_config['setting']['collection_id'];
                $filter['limit'] = $limit;

                $current_collection = $this->model_catalog_collection->getCollection($filter);
                // skip collection if not available (not existed or disabled)
                if (!isset($current_collection['collection_id']) || empty($current_collection['collection_id'])) {
                    // do something...
                } else {
                    // check if collection contains product only (type not set or type = 0)
                    if (!isset($current_collection['type']) || $current_collection['type'] != 1) {
                        $new_products = $this->model_catalog_product->getProductsByColection($filter);
                    } else {
                        $collections = $this->model_catalog_collection->getCollections($filter);

                        // resize collections images
                        foreach ($collections as &$collection) {
                            if ($collection['image']) {
                                // no need resize
                            } else {
                                $collection['image'] = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
                            }
                            $collection['url'] = $this->url->link('common/shop', 'collection=' . $collection['collection_id']);
                        }
                        unset($collection);
                    }

                    $data['current_collection_image'] = isset($current_collection['image']) ? $this->changeUrl($current_collection['image']) : '';
                }
            } elseif ($resource_type == self::$PRODUCT_BLOCK_RESOURCE_TYPE_CATEGORY) {
                $filter['filter_category_id'] = $new_product_config['setting']['category_id'];
                $filter['limit'] = $limit;
                $new_products = $this->model_catalog_product->getProductsByCategoryId($filter['filter_category_id']);
            } else {
                // do something...
            }
        } else {
            $new_products = $this->model_catalog_product->getLatestProducts($limit);
            $new_products = is_array($new_products) ? $new_products : [];
        }

        // use this {if} to speed up
        $best_sales_products = [];
        if (!empty($new_products)) {
            // $best_sale_product_ids for checking if each product is best sale product
            /*$best_sales_products = $this->model_catalog_product->getProductBestSales($limit);*/
            if (empty($this->best_sale_products)) {
                $this->best_sale_products = $best_sales_products = $this->model_catalog_product->getProductBestSales($limit);
            } else {
                $best_sales_products = $this->best_sale_products;
            }
        }

        $new_product_data = $this->buildProductData($new_products, $best_sales_products, $np_ids = [], $option = [
            'is_new_product_ids' => true,
        ]);

        $data['new_products'] = $new_product_data;
        $data['collections_new'] = $collections;
        $data['new_products_display_grid_quantity'] = isset($new_product_config['display']['grid']['quantity']) ? $new_product_config['display']['grid']['quantity'] : 4;
        $data['new_products_display_grid_mobile_quantity'] = isset($new_product_config['display']['grid_mobile']['quantity']) ? $new_product_config['display']['grid_mobile']['quantity'] : 1;
        $data['new_products_display_grid_row'] = isset($new_product_config['display']['grid']['row']) ? $new_product_config['display']['grid']['row'] : self::$PRODUCT_BLOCK_DISPLAY_NUMBER_OF_ROW_DEFAULT;
        $data['new_products_display_grid_mobile_row'] = isset($new_product_config['display']['grid_mobile']['row']) ? $new_product_config['display']['grid_mobile']['row'] : self::$PRODUCT_BLOCK_DISPLAY_NUMBER_OF_ROW_DEFAULT;
        $data['new_products_autoplay'] = isset($new_product_config['setting']['autoplay']) ? $new_product_config['setting']['autoplay'] : self::$PRODUCT_BLOCK_AUTOPLAY_DEFAULT;
        $data['new_products_autoplay_time'] = isset($new_product_config['setting']['autoplay_time']) ? $new_product_config['setting']['autoplay_time'] : self::$PRODUCT_BLOCK_AUTOPLAY_TIME_DEFAULT;
        $data['new_products_loop'] = isset($new_product_config['setting']['loop']) ? $new_product_config['setting']['loop'] : self::$PRODUCT_BLOCK_LOOP_DEFAULT;
        $data['view_more'] = $this->url->link('common/shop');

        if ($new_product_config['setting']['auto_retrieve_data'] == 0) {
            $resource_type = (isset($new_product_config['setting']['resource_type'])) ? $new_product_config['setting']['resource_type'] : self::$PRODUCT_BLOCK_RESOURCE_TYPE_COLLECTION; // default collection for backward compatibility!
            if ($resource_type == self::$PRODUCT_BLOCK_RESOURCE_TYPE_COLLECTION) {
                $data['view_more'] = $this->url->link('common/shop', 'collection=' . $new_product_config['setting']['collection_id']);
            } elseif ($resource_type == self::$PRODUCT_BLOCK_RESOURCE_TYPE_CATEGORY) { // if resource type = category
                $data['view_more'] = $this->url->link('common/shop', 'path=' . $new_product_config['setting']['category_id']);
            }
        }

        if (count($new_product_data) > 0 || $collections) {
            $data['new_product_visible'] = true;
        } else {
            $data['new_product_visible'] = false;
        }

        return $data;
    }

    /**
     * get Hot Products
     *
     * @param int $visible
     * @param int|null $limit default null for all
     * @return array
     */
    public function getHotProduct($visible = 1, $limit = null)
    {
        $data = [];

        $this->load->model('extension/module/theme_builder_config');
        $hot_product_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HOT_PRODUCT);
        $hot_product_config = json_decode($hot_product_config, true);

        $hot_product_config['visible'] = $visible;

        if ($hot_product_config['visible'] == 0) {
            $data['hot_product_visible'] = false;
            return $data;
        }

        $this->load->model('catalog/product');
        $this->load->model('catalog/collection');
        $this->load->model('tool/image');

        $data['hot_product_title'] = '';
        if (isset($hot_product_config['setting']['title'])) {
            $data['hot_product_title'] = $hot_product_config['setting']['title'];
        }
        $data['hot_sub_title'] = '';
        if (isset($hot_product_config['setting']['sub_title'])) {
            $data['hot_sub_title'] = $hot_product_config['setting']['sub_title']; // backward compatibility
            $data['hot_product_sub_title'] = $hot_product_config['setting']['sub_title']; // new key
        }

        /* optimized limit */
        $limit = empty($limit) || (int)$limit < 0 ? self::$OPTIMIZED_LIMIT : (int)$limit;

        $hot_products = [];
        $collections = [];

        /* get products, sub-collections due to config */
        // check if get from special collection in config
        if ($hot_product_config['setting']['auto_retrieve_data'] == 0) {
            $resource_type = (isset($hot_product_config['setting']['resource_type'])) ? $hot_product_config['setting']['resource_type'] : self::$PRODUCT_BLOCK_RESOURCE_TYPE_COLLECTION; // default collection for backward compatibility!
            if ($resource_type == self::$PRODUCT_BLOCK_RESOURCE_TYPE_COLLECTION) {
                $filter['filter_collection_id'] = $hot_product_config['setting']['collection_id'];
                $filter['limit'] = $limit;

                $current_collection = $this->model_catalog_collection->getCollection($filter);
                // skip collection if not available (not existed or disabled)
                if (!isset($current_collection['collection_id']) || empty($current_collection['collection_id'])) {
                    // do something...
                } else {
                    // check if collection contains product only (type not set or type = 0)
                    if (!isset($current_collection['type']) || $current_collection['type'] != 1) {
                        $hot_products = $this->model_catalog_product->getProductsByColection($filter);
                    } else {
                        $collections = $this->model_catalog_collection->getCollections($filter);

                        // resize collections images
                        foreach ($collections as &$collection) {
                            if ($collection['image']) {
                                // no need resize
                            } else {
                                $collection['image'] = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
                            }
                            $collection['url'] = $this->url->link('common/shop', 'collection=' . $collection['collection_id']);
                        }
                        unset($collection);
                    }

                    $data['current_collection_image'] = isset($current_collection['image']) ? $this->changeUrl($current_collection['image']) : '';
                }
            } elseif ($resource_type == self::$PRODUCT_BLOCK_RESOURCE_TYPE_CATEGORY) {
                $filter['filter_category_id'] = $hot_product_config['setting']['category_id'];
                $filter['limit'] = $limit;
                $hot_products = $this->model_catalog_product->getProductsByCategoryId($filter['filter_category_id']);
            } else {
                // do something...
            }
        } else { // get auto best sale products
            $hot_products = $this->model_catalog_product->getHotProducts($limit);
            $hot_products = is_array($hot_products) ? $hot_products : [];
        }

        // use this {if} to speed up
        $best_sales_products = [];
        $new_products = [];
        if (!empty($hot_products)) {
            // $best_sale_product_ids for checking if each product is best sale product
            /*$best_sales_products = $this->model_catalog_product->getProductBestSales($limit);*/
            if (empty($this->best_sale_products)) {
                $this->best_sale_products = $best_sales_products = $this->model_catalog_product->getProductBestSales($limit);
            } else {
                $best_sales_products = $this->best_sale_products;
            }

            // $new_product_ids for checking if each product is new product
            /*$new_products = $this->model_catalog_product->getProductNews($limit);*/
            if (empty($this->new_products)) {
                $new_products = $this->new_products = $this->model_catalog_product->getProductNews($limit);
            } else {
                $new_products = $this->new_products;
            }
        }

        $hot_product_data = $this->buildProductData($hot_products, $best_sales_products, $new_products, $option = []);

        /* build final hot product data */
        $data['hot_products'] = $hot_product_data;
        $data['collections_hot'] = $collections;
        $data['hot_products_display_grid_quantity'] = isset($hot_product_config['display']['grid']['quantity']) ? $hot_product_config['display']['grid']['quantity'] : 4;
        $data['hot_products_display_grid_mobile_quantity'] = isset($hot_product_config['display']['grid_mobile']['quantity']) ? $hot_product_config['display']['grid_mobile']['quantity'] : 1;
        $data['hot_products_display_grid_row'] = isset($hot_product_config['display']['grid']['row']) ? $hot_product_config['display']['grid']['row'] : self::$PRODUCT_BLOCK_DISPLAY_NUMBER_OF_ROW_DEFAULT;
        $data['hot_products_display_grid_mobile_row'] = isset($hot_product_config['display']['grid_mobile']['row']) ? $hot_product_config['display']['grid_mobile']['row'] : self::$PRODUCT_BLOCK_DISPLAY_NUMBER_OF_ROW_DEFAULT;
        $data['hot_products_autoplay'] = isset($hot_product_config['setting']['autoplay']) ? $hot_product_config['setting']['autoplay'] : self::$PRODUCT_BLOCK_AUTOPLAY_DEFAULT;
        $data['hot_products_autoplay_time'] = isset($hot_product_config['setting']['autoplay_time']) ? $hot_product_config['setting']['autoplay_time'] : self::$PRODUCT_BLOCK_AUTOPLAY_TIME_DEFAULT;
        $data['hot_products_loop'] = isset($hot_product_config['setting']['loop']) ? $hot_product_config['setting']['loop'] : self::$PRODUCT_BLOCK_LOOP_DEFAULT;
        $data['view_more'] = $this->url->link('common/shop');

        if ($hot_product_config['setting']['auto_retrieve_data'] == 0) {
            $resource_type = (isset($hot_product_config['setting']['resource_type'])) ? $hot_product_config['setting']['resource_type'] : self::$PRODUCT_BLOCK_RESOURCE_TYPE_COLLECTION; // default collection for backward compatibility!
            if ($resource_type == self::$PRODUCT_BLOCK_RESOURCE_TYPE_COLLECTION) {
                $data['view_more'] = $this->url->link('common/shop', 'collection=' . $hot_product_config['setting']['collection_id']);
            } elseif ($resource_type == self::$PRODUCT_BLOCK_RESOURCE_TYPE_CATEGORY) { // if resource type = category
                $data['view_more'] = $this->url->link('common/shop', 'path=' . $hot_product_config['setting']['category_id']);
            }
        }

        if (count($hot_product_data) > 0 || $collections) {
            $data['hot_product_visible'] = true;
        } else {
            $data['hot_product_visible'] = false;
        }

        return $data;
    }

    public function getBestviewsProducts($visible = 1)
    {
        $data = [];

        $this->load->model('extension/module/theme_builder_config');
        $best_views_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_BEST_VIEWS_PRODUCT);
        $best_views_config = json_decode($best_views_config, true);

        $data['best_views_product_title'] = '';
        if (isset($best_views_config['setting']['title'])) {
            $data['best_views_product_title'] = $best_views_config['setting']['title'];
        }

        $limit = 10;
        /*if (isset($best_views_config['display']['grid']['quantity'])) {
            $limit = $best_views_config['display']['grid']['quantity'];
        }*/
        $this->load->model('catalog/product');
        $this->load->model('catalog/collection');
        $this->load->model('tool/image');
        $best_views_product = $this->model_catalog_product->getPopularProducts($limit);
        $best_views_product = is_array($best_views_product) ? $best_views_product : [];
        $best_views_product_data = array();
        foreach ($best_views_product as $product) {
            if ($product['image']) {
                //$image = $this->model_tool_image->resize($product['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
                $image = $this->model_tool_image->resize($product['image'], 500, 500);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $price = false;
            }

            if ((float)$product['special']) {
                $special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $special = false;
            }

            if ($this->config->get('config_tax')) {
                $tax = $this->currency->format((float)$product['special'] ? $product['special'] : $product['price'], $this->session->data['currency']);
            } else {
                $tax = false;
            }

            if ($this->config->get('config_review_status')) {
                $rating = (int)$product['rating'];
            } else {
                $rating = false;
            }

            $category_name = $this->model_catalog_product->getCategoriesCustom($product['product_id']);
            $name_cate = (!empty($category_name[0]['name']) ? $category_name[0]['name'] : '');

            $collection_ids = $this->model_catalog_product->getCollectionByProductId($product['product_id']);
            $collection_ids = is_array($collection_ids) ? array_map(function ($_c) {
                return isset($_c['id']) ? $_c['id'] : null;
            }, $collection_ids) : [];

            $best_views_product_data[] = array(
                'product_id' => $product['product_id'],
                'thumb' => $image,
                'name' => $product['name'],
                'description' => utf8_substr(trim(strip_tags(html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                'price' => $price,
                'special' => $special,
                'tax' => $tax,
                'minimum' => $product['minimum'] > 0 ? $product['minimum'] : 1,
                'rating' => $rating,
                'href' => $this->url->link('product/product', 'product_id=' . $product['product_id']),
                'nameCategory' => (!empty($name_cate) ? $name_cate : ''),
                'collections' => $collection_ids,
            );
        }
        $data['best_views_product'] = $best_views_product_data;
        $data['best_views_product_display_grid_quantity'] = isset($best_views_config['display']['grid']['quantity']) ? $best_views_config['display']['grid']['quantity'] : 4;

        if (count($best_views_product_data) > 0) {
            $data['best_views_product_visible'] = true;
        } else {
            $data['best_views_product_visible'] = false;
        }

        return $data;
    }

    /**
     * getRelatedProducts
     *
     * @param $product_id
     * @param int|null $limit default null for all
     * @return array
     */
    public function getRelatedProducts($product_id, $limit = null)
    {
        $data = [];

        $this->load->model('extension/module/theme_builder_config');
        $related_product_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_PRODUCT_DETAIL_RELATED_PRODUCT);
        $related_product_config = json_decode($related_product_config, true);

        if ($related_product_config['visible'] == 0) {
            $data['related_product_visible'] = false;
            return $data;
        }

        $data['related_product_title'] = '';
        if (isset($related_product_config['setting']['title'])) {
            $data['related_product_title'] = $related_product_config['setting']['title'];
        }

        /* optimized limit */
        $limit = empty($limit) || (int)$limit < 0 ? self::$OPTIMIZED_LIMIT : (int)$limit;

        $this->load->model('catalog/product');

        /* get related products due to config */
        $resource_type = (isset($related_product_config['setting']['resource_type'])) ?
            $related_product_config['setting']['resource_type'] :
            self::$PRODUCT_BLOCK_RESOURCE_TYPE_COLLECTION; // default collection for backward compatibility!
        if ($related_product_config['setting']['auto_retrieve_data'] == 0 && $resource_type == self::$PRODUCT_BLOCK_RESOURCE_TYPE_COLLECTION) {
            $filter['filter_collection_id'] = $related_product_config['setting']['collection_id'];
            $filter['start'] = 0;
            $filter['limit'] = $limit;
            $filter['filter_product_id'] = $product_id;
            $related_products = $this->model_catalog_product->getProductsByColection($filter);
        } else if ($related_product_config['setting']['auto_retrieve_data'] == 0 && $resource_type == self::$PRODUCT_BLOCK_RESOURCE_TYPE_CATEGORY) {
            $filter['filter_category_id'] = $related_product_config['setting']['category_id'];
            $filter['limit'] = $limit;
            $related_products = $this->model_catalog_product->getProductsByCategoryId($filter['filter_category_id']);
        } else {
            $filter['filter_product_id'] = $product_id;
            $filter['start'] = 0;
            $filter['limit'] = $limit;
            $related_products = $this->model_catalog_product->getProductsByIdsSameCategory($filter);
        }

        $this->load->model('tool/image');
        $this->load->model('catalog/product');

        // use this {if} to speed up
        $best_sales_products = [];
        $new_products = [];
        if (!empty($related_products)) {
            // $best_sale_product_ids for checking if each product is best sale product
            /*$best_sales_products = $this->model_catalog_product->getProductBestSales($limit);*/
            if (empty($this->best_sale_products)) {
                $this->best_sale_products = $best_sales_products = $this->model_catalog_product->getProductBestSales($limit);
            } else {
                $best_sales_products = $this->best_sale_products;
            }

            // $new_product_ids for checking if each product is new product
            /*$new_products = $this->model_catalog_product->getProductNews($limit);*/
            if (empty($this->new_products)) {
                $new_products = $this->new_products = $this->model_catalog_product->getProductNews($limit);
            } else {
                $new_products = $this->new_products;
            }
        }

        $related_products_data = $this->buildProductData($related_products, $best_sales_products, $new_products, $option = []);

        if (count($related_products_data) == 0) {
            $data['related_product_visible'] = false;
        }
        else {
            $data['related_product_visible'] = true;
        }
        $data['related_products'] = $related_products_data;
        $data['related_products_display_grid_quantity'] = isset($related_product_config['display']['grid']['quantity']) ? $related_product_config['display']['grid']['quantity'] : 4;
        $data['related_products_display_grid_mobile_quantity'] = isset($related_product_config['display']['grid_mobile']['quantity']) ? $related_product_config['display']['grid_mobile']['quantity'] : 1;
        $data['grid'] = $this->isMobile() ? $data['related_products_display_grid_mobile_quantity'] : $data['related_products_display_grid_quantity'];
        $data['related_products_autoplay'] = isset($related_product_config['setting']['autoplay']) ? $related_product_config['setting']['autoplay'] : self::$PRODUCT_BLOCK_AUTOPLAY_DEFAULT;
        $data['related_products_autoplay_time'] = isset($related_product_config['setting']['autoplay_time']) ? $related_product_config['setting']['autoplay_time'] : self::$PRODUCT_BLOCK_AUTOPLAY_TIME_DEFAULT;
        $data['related_products_loop'] = isset($related_product_config['setting']['loop']) ? $related_product_config['setting']['loop'] : self::$PRODUCT_BLOCK_LOOP_DEFAULT;

        return $data;
    }

    public function getProductBlocksList($blocks, $sections_config)
    {
        $data_block = [];

        /* sort order */
        $this->load->model('extension/module/theme_builder_config');
        $sections_config_raw = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_SECTIONS);
        $sections_config_raw = json_decode($sections_config_raw, true);
        $sections_config_raw = is_array($sections_config_raw) ? $sections_config_raw : [];

        /* hot product */
        if (array_key_exists('hot-deals', $sections_config)) {
            $hot_products = $this->getHotProductWithCache($sections_config['hot-deals']);
            if ($hot_products['hot_product_visible']) {
                $data_block[] = [
                    'collections' => $hot_products['collections_hot'],
                    'products' => $hot_products['hot_products'],
                    'title' => $hot_products['hot_product_title'],
                    'block_type' => 'hot_product',
                    'bestme_block_preview_class' => 'bestme-block-hot-product',
                    'grid' => $this->isMobile() ? $hot_products['hot_products_display_grid_mobile_quantity'] : $hot_products['hot_products_display_grid_quantity'],
                    'max_element' => $hot_products['hot_products_display_grid_quantity'],
                    'visible' => $hot_products['hot_product_visible'],
                    'sub_title' => $hot_products['hot_sub_title'],
                    'current_collection_image' => isset($hot_products['current_collection_image']) ? $this->changeUrl($hot_products['current_collection_image']) : '',
                    'view_more' => $hot_products['view_more'],
                    'sort_order' => getValueByKey($sections_config_raw['section']['hot-deals'], 'sort_order'),
                    'row' => (int)($this->isMobile() ? $hot_products['hot_products_display_grid_mobile_row'] : $hot_products['hot_products_display_grid_row']),
                    'autoplay' => (int)$hot_products['hot_products_autoplay'],
                    'autoplay_time' => (int)$hot_products['hot_products_autoplay_time'],
                    'loop' => (int)$hot_products['hot_products_loop']
                ];
            }
        }

        /* best sale product */
        if (array_key_exists('feature-products', $sections_config)) {
            $best_sale_products = $this->getBestSalesProductsWithCache($sections_config['feature-products']);
            if ($best_sale_products['best_sales_product_visible']) {
                $data_block[] = [
                    'collections' => $best_sale_products['collections_sale'],
                    'products' => $best_sale_products['best_sales_product'],
                    'title' => $best_sale_products['best_sales_product_title'],
                    'block_type' => 'best_sales_product',
                    'bestme_block_preview_class' => 'bestme-block-best-sales-product',
                    'grid' => $this->isMobile() ? $best_sale_products['best_sales_product_display_grid_mobile_quantity'] : $best_sale_products['best_sales_product_display_grid_quantity'],
                    'max_element' => $best_sale_products['best_sales_product_display_grid_quantity'],
                    'visible' => $best_sale_products['best_sales_product_visible'],
                    'sub_title' => $best_sale_products['best_sales_sub_title'],
                    'current_collection_image' => isset($best_sale_products['current_collection_image']) ? $this->changeUrl($best_sale_products['current_collection_image']) : '',
                    'view_more' => $best_sale_products['view_more'],
                    'sort_order' => getValueByKey($sections_config_raw['section']['feature-products'], 'sort_order'),
                    'row' => (int)($this->isMobile() ? $best_sale_products['best_sales_product_display_grid_mobile_row'] : $best_sale_products['best_sales_product_display_grid_row']),
                    'autoplay' => (int)$best_sale_products['best_sales_product_autoplay'],
                    'autoplay_time' => (int)$best_sale_products['best_sales_product_autoplay_time'],
                    'loop' => (int)$best_sale_products['best_sales_product_loop']
                ];
            }
        }

        /* best new product */
        if (array_key_exists('new-products', $sections_config)) {
            $new_products = $this->getNewProductsWithCache($sections_config['new-products']);
            if ($new_products['new_product_visible']) {
                $data_block[] = [
                    'collections' => $new_products['collections_new'],
                    'products' => $new_products['new_products'],
                    'title' => $new_products['new_product_title'],
                    'block_type' => 'new_product',
                    'bestme_block_preview_class' => 'bestme-block-new-product',
                    'grid' => $this->isMobile() ? $new_products['new_products_display_grid_mobile_quantity'] : $new_products['new_products_display_grid_quantity'],
                    'max_element' => $new_products['new_products_display_grid_quantity'],
                    'visible' => $new_products['new_product_visible'],
                    'sub_title' => $new_products['new_sub_title'],
                    'current_collection_image' => isset($new_products['current_collection_image']) ? $this->changeUrl($new_products['current_collection_image']) : '',
                    'view_more' => $new_products['view_more'],
                    'sort_order' => getValueByKey($sections_config_raw['section']['new-products'], 'sort_order'),
                    'row' => (int)($this->isMobile() ? $new_products['new_products_display_grid_mobile_row'] : $new_products['new_products_display_grid_row']),
                    'autoplay' => (int)$new_products['new_products_autoplay'],
                    'autoplay_time' => (int)$new_products['new_products_autoplay_time'],
                    'loop' => (int)$new_products['new_products_loop']
                ];
            }
        }

        $this->_logRunningTime('getProductBlocksList: after default blocks');

        /* custom product list */
        // TODO: use fn getGroupBlock()...

        $limit = $this->getLimitProductInBlock();

        $this->load->model('catalog/product');
        $this->load->model('catalog/collection');
        foreach ($blocks as $key => $block) {
            $products = [];
            $product_data = [];
            $collections = [];

            // TODO: use fn getGroupBlock()...
            $resource_type = (isset($block['setting']['resource_type'])) ? $block['setting']['resource_type'] : Product_Util_Trait::$PRODUCT_BLOCK_RESOURCE_TYPE_COLLECTION; // default collection for backward compatibility!
            // if resource type = collection
            if ($resource_type == Product_Util_Trait::$PRODUCT_BLOCK_RESOURCE_TYPE_COLLECTION) {
                $filter['filter_collection_id'] = $block['setting']['collection_id'];
                $filter['limit'] = $limit;
                $current_collection = $this->model_catalog_collection->getCollection($filter);

                // skip collection if not available (not existed or disabled)
                if (!isset($current_collection['collection_id']) || empty($current_collection['collection_id'])) {
                    continue;
                }

                // check if collection contains product only (type not set or type = 0)
                if (!isset($current_collection['type']) || $current_collection['type'] != 1) {
                    $products = $this->model_catalog_product->getProductsByColection($filter);
                    $products = is_array($products) ? $products : [];

                    $this->_logRunningTime('getProductBlocksList: after getProductsByColection ' . $filter['filter_collection_id']);
                } else { // collection contains sub collections only
                    $collections = $this->model_catalog_collection->getCollections($filter);

                    // resize collections images
                    foreach ($collections as &$collection) {
                        if ($collection['image']) {
                            // no need resize
                        } else {
                            $collection['image'] = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
                        }
                        $collection['url'] = $this->url->link('common/shop', 'collection=' . $collection['collection_id']);
                    }
                    unset($collection);

                    $this->_logRunningTime('getProductBlocksList: after getCollections');
                }
            }

            // if resource type = category
            $is_category_block = false;
            if ($resource_type == 2) {
                $filter['filter_collection_id'] = null;
                $filter['filter_category_id'] = $block['setting']['category_id'];
                $products = $this->model_catalog_product->getProductsByCategoryId($filter['filter_category_id']);
                $is_category_block = true;
            }

            // $best_sale_product_ids for checking if each product is best sale product
            $best_sale_product_ids = [];
            if (isset($this->best_sale_products_with_config['best_sales_product']) && is_array($this->best_sale_products_with_config['best_sales_product'])) {
                $best_sale_product_ids = array_map(function ($bsp) {
                    return isset($bsp['product_id']) ? $bsp['product_id'] : null;
                }, $this->best_sale_products_with_config['best_sales_product']);
            }

            // $new_product_ids for checking if each product is new product
            $new_product_ids = [];
            if (isset($this->new_products_with_config['new_products']) && is_array($this->new_products_with_config['new_products'])) {
                $new_product_ids = array_map(function ($np) {
                    return isset($np['product_id']) ? $np['product_id'] : null;
                }, $this->new_products_with_config['new_products']);
            }

            // build product data
            $product_data = $this->buildProductData($products, $best_sale_product_ids, $new_product_ids, $option = []);

            $this->_logRunningTime('getProductBlocksList: after custom block');

            // update product category if block product is category
            if ($is_category_block) {
                    foreach ($product_data as &$p) {
                        foreach ($p['categories'] as $p_category) {
                            if ($filter['filter_category_id'] == $p_category['id']) {
                                $p['nameCategory'] = $p_category['name'];
                                break;
                            }
                        }
                    }
                    unset($p);
            }

            // sub title
            $sub_title = '';
            if (isset($block['setting']['sub_title'])) {
                $sub_title = $block['setting']['sub_title'];
            }

            // grid on mobile
            $grid_mobile = isset($block['display']['grid_mobile']['quantity']) ? $block['display']['grid_mobile']['quantity'] : 1;

            // view more url
            $data['view_more'] = $this->url->link('common/shop');
            if ($resource_type == self::$PRODUCT_BLOCK_RESOURCE_TYPE_COLLECTION) {
                if (isset($block['setting']['collection_id'])) {
                    $data['view_more'] = $this->url->link('common/shop', 'collection=' . $block['setting']['collection_id']);
                }
            } elseif ($resource_type == self::$PRODUCT_BLOCK_RESOURCE_TYPE_CATEGORY) { // if resource type = category
                if (isset($block['setting']['category_id'])) {
                    $data['view_more'] = $this->url->link('common/shop', 'path=' . $block['setting']['category_id']);
                }
            }

            // number of row
            $grid_row = isset($block['display']['grid']['row']) ? $block['display']['grid']['row'] : Product_Util_Trait::$PRODUCT_BLOCK_DISPLAY_NUMBER_OF_ROW_DEFAULT;
            $grid_row_on_mobile = isset($block['display']['grid_mobile']['row']) ? $block['display']['grid_mobile']['row'] : Product_Util_Trait::$PRODUCT_BLOCK_DISPLAY_NUMBER_OF_ROW_DEFAULT;

            // autoplay
            $autoplay = isset($block['setting']['autoplay']) ? $block['setting']['autoplay'] : Product_Util_Trait::$PRODUCT_BLOCK_AUTOPLAY_DEFAULT;
            $autoplay_time = isset($block['setting']['autoplay_time']) ? $block['setting']['autoplay_time'] : Product_Util_Trait::$PRODUCT_BLOCK_AUTOPLAY_TIME_DEFAULT;
            $loop = isset($block['setting']['loop']) ? $block['setting']['loop'] : Product_Util_Trait::$PRODUCT_BLOCK_LOOP_DEFAULT;

            // final: build product data block
            $data_block[] = [
                'collections' => $collections,
                'products' => $product_data,
                'title' => $block['setting']['title'],
                'block_type' => 'custom_product_group',
                'bestme_block_preview_class' => 'bestme-block-custom-group',
                'grid' => $this->isMobile() ? $grid_mobile : $block['display']['grid']['quantity'],
                'max_element' => $block['display']['grid']['quantity'],
                'sub_title' => $sub_title,
                'visible' => (count($product_data) > 0 || $collections) ? true : false,
                'current_collection_image' => isset($current_collection['image']) ? $this->changeUrl($current_collection['image']) : '',
                'view_more' => $data['view_more'],
                'sort_order' => getValueByKey($block, 'sort_order'),
                'row' => (int)($this->isMobile() ? $grid_row_on_mobile : $grid_row),
                'autoplay' => (int)$autoplay,
                'autoplay_time' => (int)$autoplay_time,
                'loop' => (int)$loop
            ];
        }

        $this->_logRunningTime('getProductBlocksList: after all custom blocks');

        /* sort order */
        $data_block_with_sort = array_filter($data_block, function ($db) {
            return !is_null($db['sort_order']) && $db['sort_order'] != '';
        });

        $data_block_without_sort = array_filter($data_block, function ($db) {
            return is_null($db['sort_order']) || $db['sort_order'] == '';
        });

        if (!empty($data_block_with_sort)) {
            usort($data_block_with_sort, function ($a, $b) {
                if ((int)$a['sort_order'] === (int)$b['sort_order']) {
                    return 0;
                }

                return ((int)$a['sort_order'] < (int)$b['sort_order']) ? -1 : 1;
            });
        }

        $data_block = array_merge($data_block_with_sort, $data_block_without_sort);

        return $data_block;
    }
    
    public function getNewProductsWithCache($visible = 1)
    {
        $limit = $this->getLimitProductInBlock();

        // enable self cache
        if (empty($this->new_products_with_config)) {
            $result_new_product = $this->new_products_with_config = $this->getNewProducts($visible, $limit);
        } else {
            $result_new_product = $this->new_products_with_config;
        }

        return $result_new_product;
    }

    public function getBestSalesProductsWithCache($visible = 1)
    {
        $limit = $this->getLimitProductInBlock();

        // enable self cache
        if (empty($this->best_sale_products_with_config)) {
            $result_best_sale = $this->best_sale_products_with_config = $this->getBestSalesProducts($visible, $limit);
        } else {
            $result_best_sale = $this->best_sale_products_with_config;
        }

        return $result_best_sale;
    }

    public function getHotProductWithCache($visible = 1)
    {
        $limit = $this->getLimitProductInBlock();

        // enable self cache
        if (empty($this->hot_products_with_config)) {
            $result_hot_product = $this->hot_products_with_config = $this->getHotProduct($visible, $limit);
        } else {
            $result_hot_product = $this->hot_products_with_config;
        }

        return $result_hot_product;
    }

    public function generateCategories() {
        $this->load->model('catalog/category');
        $categories = $this->model_catalog_category->getFullCategoriesForColumnLeft(['filter_status' => 1]);
        return $categories;
    }

    public function getFilterProduct()
    {
        $this->load->language('common/shop');
        $this->load->model('extension/module/theme_builder_config');
        $this->load->model('catalog/manufacturer');
        $this->load->model('catalog/category');
        $this->load->model('catalog/collection');
        $this->load->model('catalog/tag');
        $this->load->model('catalog/product');
        $this->load->model('catalog/attribute_filter');

        $result = array();
        $related_filter_product_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CATEGORY_FILTER);
        $related_filter_product_config = json_decode($related_filter_product_config, true);
        $result['visible_supplier'] = $related_filter_product_config['display']['supplier']; // nha cung cap
        $result['visible_product_type'] = $related_filter_product_config['display']['product-type']; // loai san pham
        $result['visible_collection'] = $related_filter_product_config['display']['collection'];  // bo suu tap
        $result['visible_product_price'] = $related_filter_product_config['display']['product-price'];  // giá sản phẩm
        $result['visible_tag'] = isset($related_filter_product_config['display']['tag']) ? $related_filter_product_config['display']['tag'] : ['visible' => 1]; // tag
        $result['visible_attribute'] = isset($related_filter_product_config['display']['attribute']) ? $related_filter_product_config['display']['attribute'] : ['visible' => 1]; // attribute filter
        $url = '';
        $result['urlserach'] = $this->url->link('common/shop', modQuery($url, 'search') . '', true);

        if (isset($this->request->get['valueoptiton'])) {
            $url .= '&valueoptiton=' . $this->request->get['valueoptiton'];
        }

        if (isset($this->request->get['search'])) {
            $url .= '&search=' . trim($this->request->get['search']);
        }

        if (isset($this->request->get['path']) && isset($result['visible_product_type']['visible']) && $result['visible_product_type']['visible'] == 1) {
            $url .= '&path=' . $this->request->get['path'];
        }

        if (isset($this->request->get['manufacture']) && isset($result['visible_supplier']['visible']) && $result['visible_supplier']['visible'] == 1) {
            $url .= '&manufacture=' . $this->request->get['manufacture'];
        }

        if (isset($this->request->get['collection']) && isset($result['visible_collection']['visible']) && $result['visible_collection']['visible'] == 1) {
            $url .= '&collection=' . $this->request->get['collection'];
        }

        if (isset($this->request->get['nametag'])) {
            $url .= '&nametag=' . $this->request->get['nametag'];
        }

        if (isset($this->request->get['min']) && isset($result['visible_product_price']['visible']) && $result['visible_product_price']['visible'] == 1) {
            $url .= '&min=' . $this->request->get['min'];
        }

        if (isset($this->request->get['max']) && isset($result['visible_product_price']['visible']) && $result['visible_product_price']['visible'] == 1) {
            $url .= '&max=' . $this->request->get['max'];
        }

        if (isset($this->request->get['attribute_name']) && isset($this->request->get['attribute_value'])) {
            $url .= '&attribute_name=' . $this->request->get['attribute_name'] . '&attribute_value=' . $this->request->get['attribute_value'];
        }

        $result['product_price_url'] = $this->url->link('common/shop', modQuery($url, 'min'), true);
        $result['product_price_url'] = modQuery($result['product_price_url'], 'max');

        if (isset($result['visible_tag']['visible']) && $result['visible_tag']['visible'] == 1) {
            // todo: load more tags
            $tags = []; // $this->model_catalog_tag->getAllTag();
            foreach ($tags as $tag) {
                if (isset($this->request->get['nametag']) && $this->request->get['nametag'] == $tag['tag_id']) {
                    $tag_url = $this->url->link('common/shop', modQuery($url, 'nametag'), true);
                    $result['tag_selected_name'] = $tag['value'];
                } else {
                    $tag_url = $this->url->link('common/shop', modQuery($url, 'nametag') . '&nametag=' . $tag['tag_id'], true);
                }
                $result['tag'][] = array(
                    'tag_id' => $tag['tag_id'],
                    'name' => $tag['value'],
                    'url' => $tag_url,
                );
            }

            // option: tất cả tag
            $result['tag_all'] = array(
                'tag_id' => '-1',
                'name' => 'Tất cả',
                'url' => $this->url->link('common/shop', modQuery($url, 'nametag'), true),
            );
        }

        if (isset($result['visible_supplier']['visible']) && $result['visible_supplier']['visible'] == 1) {
            // $related_manufacturer = $this->model_catalog_product->getManufacturer(); // TODO: remove...
            // $related_manufacturer = $this->model_catalog_manufacturer->getManufacturersHadProduct(); // TODO: use this instead of all manufacturers?...
            $related_manufacturer = $this->model_catalog_manufacturer->getManufacturers(false, false);
            foreach ($related_manufacturer as $manufacturer) {
                if (isset($this->request->get['manufacture']) && $this->request->get['manufacture'] == $manufacturer['manufacturer_id']) {
                    $manufacturer_url = $this->url->link('common/shop', modQuery($url, 'manufacture'), true);
                    $result['manufacturer_selected_name'] = $manufacturer['name'];
                } else {
                    $manufacturer_url = $this->url->link('common/shop', modQuery($url, 'manufacture') . '&manufacture=' . $manufacturer['manufacturer_id'], true);
                }
//                TODO: remove...
//                $filter_data_manufacturer = array(
//                    'filter_manufacturer_id' => $manufacturer['manufacturer_id']
//                );
//                $count_products = $this->model_catalog_product->getTotalProductsCustom($filter_data_manufacturer);
                $result['manufacturer'][] = array(
                    'manufacturer_id' => $manufacturer['manufacturer_id'],
                    'name' => $manufacturer['name'],
                    'url' => $manufacturer_url,
//                    'count_product' => $count_products,
                );
            }
            // option: tất cả nhà cung cấp
            $result['manufacturer_all'] = array(
                'manufacturer_id' => '-1',
                'name' => $this->language->get('text_all_supplier'),
                'url' => $this->url->link('common/shop', modQuery($url, 'manufacture'), true),
            );
        }

        if (isset($result['visible_product_type']['visible']) && $result['visible_product_type']['visible'] == 1) {
            $related_category = $this->model_catalog_category->getCategoriesHadProduct();
            foreach ($related_category as $category) {
                if (isset($this->request->get['path']) && $this->request->get['path'] == $category['category_id']) {
                    $category_url = $this->url->link('common/shop', modQuery($url, 'path'), true);
                    $result['category_selected_name'] = $category['name'];
                    $result['category_selected_meta_title'] = isset($category['meta_title']) ? html_entity_decode($category['meta_title']) : '';
                    $result['category_selected_meta_keyword'] =  isset($category['meta_keyword']) ? html_entity_decode($category['meta_keyword']) : '';
                    $result['category_selected_meta_description'] = isset($category['meta_description']) ? html_entity_decode($category['meta_description']) : '';
                } else {
                    $category_url = $this->url->link('common/shop', modQuery($url, 'path') . '&path=' . $category['category_id'], true);
                }
//                TODO: remove
//                $filter_data_category = array(
//                    'filter_category_id' => $category['category_id']
//                );
//                $count_products = $this->model_catalog_product->getTotalProductsCustom($filter_data_category);
                $result['category'][] = array(
                    'id' => $category['category_id'],
                    'category_id' => $category['category_id'],
                    'name' => $category['name'],
                    'image' => $category['image'],
                    'url' => $category_url,
                    'parent_id' => $category['parent_id'],
//                    'count_product' => $count_products,
                );
            }

            // option: tất cả loại sản phẩm
            $result['category_all'] = array(
                'category_id' => '-1',
                'name' => $this->language->get('text_all_category'),
                'url' => $this->url->link('common/shop', modQuery($url, 'path'), true),
            );
        }

        if (isset($result['visible_collection']['visible']) && $result['visible_collection']['visible'] == 1) {
            $related_collection = $this->model_catalog_collection->getAllcollection();
            foreach ($related_collection as $collection) {
                if (isset($this->request->get['collection']) && $this->request->get['collection'] == $collection['collection_id']) {
                    $collection_url = $this->url->link('common/shop', modQuery($url, 'collection'), true);
                    $result['collection_selected_name'] = $collection['title'];

                    $result['collection_selected_meta_title'] = isset($collection['meta_title']) ? html_entity_decode($collection['meta_title']) : '';
                    $result['collection_selected_meta_description'] = isset($collection['meta_description']) ? html_entity_decode($collection['meta_description']) : '';
                } else {
                    $collection_url = $this->url->link('common/shop', modQuery($url, 'collection') . '&collection=' . $collection['collection_id'], true);
                }
//                TODO: remove
//                $filter_data_collection = array(
//                    'filter_collection_id' => $collection['collection_id']
//                );
//                $count_products = $this->model_catalog_product->getTotalProductsCustom($filter_data_collection);
//                $count_collections = '1' == $collection['type'] ? $this->model_catalog_collection->getCountCollections($filter_data_collection) : 0;
                $result['collection'][] = array(
                    'collection_id' => $collection['collection_id'],
                    'name' => $collection['title'],
                    'url' => $collection_url,
                    'image' => $collection['image'],
                    'description' => $collection['description'],
//                    'count_product' => $count_products,
//                    'count_collection' => $count_collections,
                    'type' => $collection['type']
                );
            }

            // option: tất cả bộ sưu tập
            $result['collection_all'] = array(
                'collection_id' => '-1',
                'name' => $this->language->get('text_all_collection'),
                'url' => $this->url->link('common/shop', modQuery($url, 'collection'), true),
            );
        }

        if (isset($result['visible_attribute']['visible']) && $result['visible_attribute']['visible'] == 1) {
            $attribute_filters = $this->model_catalog_attribute_filter->getAttributeFilters();
            foreach ($attribute_filters as $attribute_filter) {
                $attribute_filter_values = [];
                foreach ($attribute_filter['value'] as $value) {
                    $is_active_value = false;
                    if (isset($this->request->get['attribute_name']) && isset($this->request->get['attribute_value']) && $this->request->get['attribute_name'] == $attribute_filter['name'] && $this->request->get['attribute_value'] == $value) {
                        $url_params = modQuery($url, 'attribute_name');
                        $url_params = modQuery($url_params, 'attribute_value');
                        $attribute_url = $this->url->link('common/shop', $url_params, true);

                        $result['attribute_selected_name'] = $this->request->get['attribute_name'];
                        $result['attribute_selected_value'] = $this->request->get['attribute_value'];

                        $is_active_value = true;
                    } else {
                        $url_params = modQuery($url, 'attribute_name') . '&attribute_name=' . $attribute_filter['name'];
                        $url_params = modQuery($url_params, 'attribute_value') . '&attribute_value=' . urlencode($value);
                        $attribute_url = $this->url->link('common/shop', $url_params, true);
                    }

                    $attribute_filter_values[] = [
                        'value' =>  $value,
                        'url' => $attribute_url,
                        'is_active' => $is_active_value
                    ];
                }

                $result['attribute'][] = array(
                    'attribute_id' => $attribute_filter['attribute_filter_id'],
                    'name' => $attribute_filter['name'],
                    'value' => $attribute_filter_values
                );
            }

            // option: all attribute filter
            $result['attribute_all'] = array(
                'attribute_id' => '-1',
                'name' => $this->language->get('text_all_attribute'),
                'url' => $this->url->link('common/shop', modQuery($url, 'attribute_name'), true)
            );
        }

        return $result;
    }

    /**
     * @param array $filter format
     * [
     *     "valueoptiton" => "1|2|3|4", // 1=A-Z, 2=Z-A, 3=price desc, 4=price asc
     *     "search" => "abc",
     *     "filter_category_id" => "11",
     *     "filter_sub_category" => true,
     *     "filter_manufacturer_id" => "9",
     *     "filter_collection_id" => "7",
     *     "filter_nametag" => "tag1",
     *     "price_min" => "1500000",
     *     "price_max" => "9000000",
     *     "start" => "1",
     *     "limit" => "15",
     *     "page" => "1",
     * ]
     * @return array
     */
    public function getProductsByFilter(array $filter)
    {
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('catalog/collection');
        $this->load->model('catalog/manufacturer');
        $this->load->model('tool/image');

        /* get products */
        $products = $this->model_catalog_product->getProductsCustomOptimize($filter);

        /* build product data */
        $best_sales_products = $this->model_catalog_product->getProductBestSales();
        $new_products = $this->model_catalog_product->getProductNews();

        $result = $this->buildProductData($products, $best_sales_products, $new_products, $option = []);

        return $result;
    }

    /* == private functions == */

    /**
     * @param $product_id
     * @param $image_default
     * @return mixed
     */
    private function getOtherHoverImage($product_id, $image_default)
    {
        /** Get other image */
        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        $product_images = $this->model_catalog_product->getProductImages($product_id);
        $product_images = empty($product_images) || !is_array($product_images) ? [] : $product_images;
        foreach ($product_images as &$product_image) {
            $product_image['image'] = $this->resizeImage($product_image['image'], 2000, 2000); // Avoid breaking when zooming in, TODO ...
        }
        unset($product_image);

        // add image default
        $product_image_default = [
            'image' => $image_default
        ];

        array_unshift($product_images, $product_image_default);
        $product_image_empty = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height'));
        /* fix if product has only image */
        $alternative_product_images = (isset($product_images[0]['image']) ? $product_images[0]['image'] : $product_image_empty);
        $other_image_product = (isset($product_images[1]['image']) ? $product_images[1]['image'] : $alternative_product_images);

        return $other_image_product;
    }

    /**
     * @param array $products
     * @param array $best_sale_product_ids
     * @param array $new_product_ids
     * @param array $option contains keys:
     * - bool is_best_sale_product, default: false
     * - bool is_new_product_ids, default: false
     * - bool include_category, default: true
     * - bool include_collection, default: true
     * - bool include_second_image, default: true
     * @return array
     */
    private function buildProductData(array $products,
                                      array $best_sale_product_ids = [],
                                      array $new_product_ids = [],
                                      array $option = [])
    {
        if (empty($products)) {
            return [];
        }

        $isNeedSpeedUpMore = $this->isNeedSpeedUpMore();

        $is_best_sale_product = getValueByKey($option, 'is_best_sale_product', false);
        $is_new_product = getValueByKey($option, 'is_new_product_ids', false);
        $include_category = !$isNeedSpeedUpMore && getValueByKey($option, 'include_category', true);
        $include_collection = !$isNeedSpeedUpMore && getValueByKey($option, 'include_collection', true);
        $include_second_image = !$isNeedSpeedUpMore && getValueByKey($option, 'include_second_image', true);

        $product_data = [];

        foreach ($products as $product) {
            if (!isset($product['product_id'])) {
                continue;
            }

            $p_image = '';
            if ($product['multi_versions']) {
                if(!empty($product['pv_image']))  {
                    $p_image = $product['pv_image'];
                } else {
                    $p_image = $product['image'];
                }
            } else {
                $p_image = $product['image'];
            }

            if ($p_image) {
                //$image = $this->model_tool_image->resize($product['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
                $image = $this->model_tool_image->resize($p_image, 500, 500);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $price = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $price_master = $this->currency->formatCustom($this->tax->calculate($product['price_master'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $price_master = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $compare_price_master = $this->currency->formatCustom($this->tax->calculate($product['compare_price_master'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $compare_price_master = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $min_price_version = $this->currency->formatCustom($this->tax->calculate($product['min_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $min_price_version = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $max_price_version = $this->currency->formatCustom($this->tax->calculate($product['max_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $max_price_version = false;
            }

            if (($this->customer->isLogged() || !$this->config->get('config_customer_price')) && isset($product['compare_price_2'])) {
                $compare_price_2 = $this->currency->formatCustom($this->tax->calculate($product['compare_price_2'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $compare_price_2 = false;
            }

            if (($this->customer->isLogged() || !$this->config->get('config_customer_price')) && isset($product['price_2'])) {
                $price_2 = $this->currency->formatCustom($this->tax->calculate($product['price_2'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $price_2 = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $min_compare_price_version = $this->currency->formatCustom($this->tax->calculate($product['min_compare_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $min_compare_price_version = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $max_compare_price_version = $this->currency->formatCustom($this->tax->calculate($product['max_compare_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $max_compare_price_version = false;
            }

            if ((float)$product['special']) {
                $special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $special = false;
            }

            if ($this->config->get('config_tax')) {
                $tax = $this->currency->format((float)$product['special'] ? $product['special'] : $product['price'], $this->session->data['currency']);
            } else {
                $tax = false;
            }

            if ($this->config->get('config_review_status')) {
                $rating = (int)$product['rating'];
            } else {
                $rating = false;
            }

            if (empty($product['product_version_id'])) {
                $product['price_master_real'] = ((int)$product['price_master'] != 0) ? 1 : 0;
                $product['max_price_version_real'] = 1;
            }

            if (!empty($product['product_version_id'])) {
                $product['max_price_version_real'] = ((int)$product['price_version_check_null'] != 0) ? 1 : 0;
                $product['price_master_real'] = 1;
            }

            if (!is_null($product['product_id'])) {
                $product_version_id = (int)$product['product_version_id'];
                $product_id = $product['product_id'];

                $name_cate = '';
                if ($include_category) {
                    $category_name = $this->model_catalog_product->getCategoriesCustom($product['product_id']);
                    $name_cate = (!empty($category_name[0]['name']) ? $category_name[0]['name'] : '');
                }

                $collection_ids = [];
                if ($include_collection) {
                    $collection_ids = $this->model_catalog_product->getCollectionByProductId($product['product_id']);
                    $collection_ids = is_array($collection_ids) ? array_map(function ($_c) {
                        return isset($_c['id']) ? $_c['id'] : null;
                    }, $collection_ids) : [];
                }

                $description = utf8_substr(trim(strip_tags(html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..';

                $in_stock = $this->model_catalog_product->isStock($product_id); // $product_version_id null for all versions

                $percent_sale = empty($product['max_price_version'])
                    ? getPercent($product['price_master'], $product['compare_price_master'])
                    : getPercent($product['min_price_version'], $product['min_compare_price_version'], $product['max_percent_pv']);
                $percent_sale_value = empty($product['max_price_version'])
                    ? getPercent($product['price_master'], $product['compare_price_master'], 0, "")
                    : getPercent($product['min_price_version'], $product['min_compare_price_version'], $product['max_percent_pv'], "");


                if ((float)$product['price_discount'] == 0 && !empty($product['discount_id'])) {
                    $percent_sale = -100;
                    $percent_sale_value = '-100%';
                }

                if ($product['multi_versions'] == 1 &&
                    isset($product['pv_price_discount']) &&
                    isset($product['pv_discount_id']) &&
                    (float)$product['pv_price_discount'] == 0 &&
                    !empty($product['pv_discount_id'])) {
                    $percent_sale = '-100%';
                    $percent_sale_value = -100;
                }

                if ($product['multi_versions'] == 0 &&
                    isset($product['p_price_discount']) &&
                    isset($product['p_discount_id']) &&
                    (float)$product['p_price_discount'] == 0 &&
                    !empty($product['p_discount_id'])) {
                    $percent_sale = '-100%';
                    $percent_sale_value = -100;
                }

                $second_image = empty($product['second_image']) ? '' : $product['second_image'];  //$include_second_image ? $this->getOtherHoverImage($product['product_id'], $image) : '';

                $is_contact_for_price = $this->isContactForPrice($product['multi_versions'], $compare_price_master, $min_compare_price_version);

                // Add-on Deal
                $this->load->model('discount/add_on_deal');
                $hasDeal = false;
                $dealName = $this->model_discount_add_on_deal->getAddOnDealNameByProductId($product['product_id']);

                if ($dealName) {
                    $hasDeal = true;
                }

                $pv_id = '';
                if (!empty($product['product_version_id'])) {
                    $pv_id = $product['product_version_id'];
                }

                if (!empty($product['product_version_id_2'])) {
                    $pv_id = $product['product_version_id_2'];
                }

                $param_text = !empty($pv_id) ? "&variant={$pv_id}" : '';

                $product_data[] = [
                    'product_id' => $product['product_id'],
                    'is_new_product' => $is_new_product || in_array($product['product_id'], $new_product_ids),
                    'is_best_sale_product' => $is_best_sale_product || in_array($product['product_id'], $best_sale_product_ids),
                    'total_sold' => getValueByKey($product, 'total', 0),
                    'thumb' => $this->changeUrl($image, true, 'product'),
                    'name' => $product['name'],
                    'multi_versions' => $product['multi_versions'],
                    'description' => $description,
                    'sub_description' => $product['sub_description'],
                    'price' => $price,
                    'special' => $special,
                    'tax' => $tax,
                    'minimum' => $product['minimum'] > 0 ? $product['minimum'] : 1,
                    'rating' => $rating,
                    'href' => $this->url->link('product/product', 'product_id=' . $product['product_id'] . $param_text),
                    'price_master_real' => $product['price_master_real'],
                    'max_price_version_real' => $product['max_price_version_real'],
                    'price_master' => $price_master,
                    'compare_price_master' => $compare_price_master,
                    'min_price_version' => $min_price_version,
                    'max_price_version' => empty($product['max_price_version']) ? $product['max_price_version'] : $max_price_version,
                    'min_compare_price_version' => $min_compare_price_version,
                    'max_compare_price_version' => empty($product['max_compare_price_version']) ? $product['max_compare_price_version'] : $max_compare_price_version,
                    'product_is_stock' => $in_stock,
                    'Percent_protduct' => $percent_sale,
                    // maintain key for percent product
                    'percent_protduct' => $percent_sale,
                    'percent_product' => $percent_sale,
                    'percent_sale' => $percent_sale,
                    'percent_sale_value' => $percent_sale_value,
                    // end - maintain key for percent product
                    'categories' => getValueByKey($product, 'categories', []),
                    'nameCategory' => (!empty($name_cate) ? $name_cate : ''),
                    'manufacturer' => $product['manufacturer'],
                    'product_images' => $this->changeUrl($second_image),
                    'collections' => $collection_ids,
                    'is_contact_for_price' => $is_contact_for_price,
                    // for common/shop
                    ////'quantity' => ...,
                    'price_compare_zero' => $product['price'],
                    ////'price' => ...,
                    'compare_price' => $compare_price_master,
                    'price_version_check_null' => (int)$product['price_version_check_null'],
                    'price_version_min' => $min_price_version,
                    'price_version_max' => empty($product['max_price_version']) ? $product['max_price_version'] : $max_price_version,
                    'compare_version_min' => $min_compare_price_version,
                    'compare_version_max' => empty($product['max_compare_price_version']) ? $product['max_compare_price_version'] : $max_compare_price_version,
                    'compare_price_2_text' => $compare_price_2,
                    'price_2_text' => $price_2,
                    'compare_price_2' => $product['compare_price_2'],
                    'price_2' => $product['price_2'],
                    'discount_id' => $product['multi_versions'] ? (isset($product['pv_discount_id']) ? $product['pv_discount_id'] : $product['discount_id']) : (isset($product['p_discount_id']) ? $product['p_discount_id'] : $product['discount_id']),
                    'has_deal' => $hasDeal,
                    'deal_name' => $dealName ?? '',
                ];
            }
        }

        return $product_data;
    }

    private function getLimitProductInBlock()
    {
        // if isNeedSpeedUpMore
        if (defined('LIMIT_PRODUCT_IN_BLOCK_MIN') &&
            (int)LIMIT_PRODUCT_IN_BLOCK_MIN > 0 &&
            $this->isNeedSpeedUpMore()
        ) {
            return (int)LIMIT_PRODUCT_IN_BLOCK_MIN;
        }

        // return config
        return defined('LIMIT_PRODUCT_IN_BLOCK') && (int)LIMIT_PRODUCT_IN_BLOCK > 0
            ? LIMIT_PRODUCT_IN_BLOCK
            : self::LIMIT_PRODUCT_IN_BLOCK;
    }

    /**
     * check if is Need Speed Up More
     *
     * @return bool
     */
    private function isNeedSpeedUpMore()
    {
        if (defined('SHOPS_USE_LIMIT_PRODUCT_IN_BLOCK_MIN') &&
            !empty(SHOPS_USE_LIMIT_PRODUCT_IN_BLOCK_MIN)
        ) {
            $shop_names = explode(',', SHOPS_USE_LIMIT_PRODUCT_IN_BLOCK_MIN);
            $shop_names = array_map(function ($sn) {
                return trim($sn);
            }, $shop_names);

            $this->load->model('setting/setting');
            $shop_name = $this->model_setting_setting->getShopName();

            return in_array($shop_name, $shop_names);
        }

        return false;
    }

    private function _logRunningTime($message, $prev_running = null, $end = false, $show_sum = false)
    {
        if (!$this->IS_DEBUG_RUNNING_TIME) {
            return;
        }

        $prev_running = empty($prev_running) ? $this->prev_running : $prev_running;
        $pick_runing = round(microtime(true) * 1000);
        $execute_time = $show_sum ? ($pick_runing - $this->start_runing_first) : ($pick_runing - $prev_running);
        echo "$message: " . $execute_time . 'ms' . '<br>';

        $this->prev_running = $pick_runing;

        if ($end) {
            die;
        }
    }


    /**
     * @param $current_url
     * @param $config_link boolean
     * @param $image_of string
     *
     * @return string $new_url
     */
    public function changeUrl($current_url, $config_link = false, $image_of = '')
    {
        $new_url = $current_url;

        if (defined('BESTME_CURRENT_IMAGE_SERVER_SERVE_URL') &&
            defined('BESTME_NEW_IMAGE_SERVER_SERVE_URL')){
            $new_url = str_replace(BESTME_CURRENT_IMAGE_SERVER_SERVE_URL, BESTME_NEW_IMAGE_SERVER_SERVE_URL, $current_url);
        }

        $is_mobile = $this->isMobile();
        $resize_images = $is_mobile ? BESTME_RESIZE_IMAGES['mobile'] : BESTME_RESIZE_IMAGES['desktop'];

        if ($config_link && !empty($image_of)) {
            $url_server_image = URL_IMAGE_SERVER;
            if (strpos($new_url, $url_server_image)) {
                $arrUrl = explode('/', $new_url);
                $length = count($arrUrl);
                if ($arrUrl && $length) {
                    switch ($image_of) {
                        case 'product':
                            $arrUrl[$length -1] = $resize_images['product'] . $arrUrl[$length -1];
                            $new_url = implode('/', $arrUrl);
                            break;
                        case 'product_detail':
                            $arrUrl[$length -1] = $resize_images['product_detail'] . $arrUrl[$length -1];
                            $new_url = implode('/', $arrUrl);
                            break;
                        case 'product_detail_thump':
                            $arrUrl[$length -1] = $resize_images['product_detail_thump'] . $arrUrl[$length -1];
                            $new_url = implode('/', $arrUrl);
                            break;
                        case 'product_cart':
                            $arrUrl[$length -1] = $resize_images['product_cart'] . $arrUrl[$length -1];
                            $new_url = implode('/', $arrUrl);
                            break;
                        case 'product_cart_header':
                            $arrUrl[$length -1] = $resize_images['product_cart_header'] . $arrUrl[$length -1];
                            $new_url = implode('/', $arrUrl);
                            break;
                        case 'product_account_detail_order':
                            $arrUrl[$length -1] = $resize_images['product_account_detail_order'] . $arrUrl[$length -1];
                            $new_url = implode('/', $arrUrl);
                            break;
                        case 'blog_home':
                            $arrUrl[$length -1] = $resize_images['blog_home'] . $arrUrl[$length -1];
                            $new_url = implode('/', $arrUrl);
                            break;
                        case 'blog_list_first':
                            $arrUrl[$length -1] = $resize_images['blog_list_first'] . $arrUrl[$length -1];
                            $new_url = implode('/', $arrUrl);
                            break;
                        case 'blog_list':
                            $arrUrl[$length -1] = $resize_images['blog_list'] . $arrUrl[$length -1];
                            $new_url = implode('/', $arrUrl);
                            break;
                    }
                }
            }
        }

        return $new_url;
    }

    public function schemaOrganization()
    {
        $schemaOrganization = [
            "@context" => "https://schema.org",
            "@type" => "Organization",
            "@id" => HTTPS_SERVER . "#Organization",
            "name" => PRODUCTION_BRAND_FULL_NAME,
            "url" => $this->url->link('common/shop'),
            "logo" => [
                "@type" => "ImageObject",
                "url" => BESTME_ORGANIZATION_LOGO
            ],
            "contactPoint" => [
                [
                    "@type" => "ContactPoint",
                    "telephone" => BESTME_ORGANIZATION_SALE_PHONE,
                    "contactType" => "sales",
                    "areaServed" => "VN",
                    "availableLanguage" => ["EN", "VN"]
                ],
                [
                    "@type" => "ContactPoint",
                    "telephone" => BESTME_ORGANIZATION_TECHNICAL_SP_PHONE,
                    "contactType" => "technical support",
                    "areaServed" => "VN",
                    "availableLanguage" => ["EN", "VN"]
                ],
                [
                    "@type" => "ContactPoint",
                    "telephone" => BESTME_ORGANIZATION_CUSTOMER_SP_PHONE,
                    "contactType" => "customer support",
                    "areaServed" => "VN",
                    "availableLanguage" => ["EN", "VN"]
                ]
            ],
            "sameAs" => BESTME_ORGANIZATION_SOCIAL_NETWORK
        ];

        return json_encode($schemaOrganization, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param $url_link
     * @param $url_item_name
     * @param $name
     * @return false|string
     */
    public function schemaBreadcrumbList($url_link, $url_item_name, $arr_url_item_name = [],  $name = '')
    {
        $count = 1;
        $schemaBreadcrumbList = [
            "@type" => "BreadcrumbList",
            "@context" => "https://schema.org",
            "itemListElement" => [
                [
                    "@type" => "ListItem",
                    "position" => $count,
                    "item" => [
                        "@id" => HTTPS_SERVER,
                        "name" => PRODUCTION_BRAND_FULL_NAME,
                    ]
                ],
                [
                    "@type" => "ListItem",
                    "position" => ++$count,
                    "item" => [
                        "@id" => $url_link,
                        "name" => $url_item_name
                    ]
                ]
            ]
        ];

        if (!empty($arr_url_item_name)) {
            foreach ($arr_url_item_name as $item) {
                $item_tem = [
                    "@type" => "ListItem",
                    "position" => ++$count,
                    "item" => [
                        "@id" => $item['href'],
                        "name" => $item['name']
                    ]
                ];

                $schemaBreadcrumbList['itemListElement'][] = $item_tem;
            }
        }

        if(!empty($name)) {
            $item = [
                "@type" => "ListItem",
                "position" => ++$count,
                "item" => [
                    "@id" => HTTPS_DOMAIN_SERVER . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH),
                    "name" => $name
                ]
            ];

            $schemaBreadcrumbList['itemListElement'][] = $item;
        }

        return json_encode($schemaBreadcrumbList, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }

    public function schemaItemOfCategory($category = [], $products = [])
    {
        if (!isset($category[0])) {
            return [];
        }

        $category = $category[0];
        $length = 0;
        $lowPrice = 0;
        $highPrice = 0;

        $brand = [
            "@type" => "Brand",
            "name" => []
        ];

        if (!empty($category) && !empty($category['children'])) {
            $brand['name'] = array_map(function ($item) {
                return $item['text'];
            }, $category['children']);
        }

        if (!empty($products)) {
            $length = count($products);
            $lowPrice = (isset($products[$length - 1]) && $products[$length - 1]['multi_versions'] == 0) ? (float)$products[$length - 1]['compare_price'] : (float)$products[$length - 1]['max_compare_price_version'];
            $highPrice = (isset($products[0]) && $products[0]['multi_versions'] == 0) ? (float)$products[0]['compare_price'] : (float)$products[0]['max_compare_price_version'];
        }

        $schema = [
            "@context" => "https://schema.org",
            "@type" => "Product",
            "name" => $category['text'],
            "image" => [$category['image']],
            "description" => $category['meta_description'],
            "brand" => $brand,
            "offers" => [
                "@type" => "AggregateOffer",
                "priceCurrency" => "VND",
                "seller" => [
                    "@id" => HTTPS_DOMAIN_SERVER . "#Organization"
                ],
                "offerCount" => $length,
                "lowPrice" => $lowPrice,
                "highPrice" => $highPrice
            ]
        ];

        return json_encode($schema, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param $p_v_ids array
     * @return array
     */
    public function dataAddOnProducts($p_v_ids = [])
    {
        $products = [];
        if (empty($p_v_ids)) {
            return $products;
        }

        $this->load->model('catalog/product');
        $this->load->model('tool/image');

        foreach ($p_v_ids as $id) {
            $product_id = $id;
            $product_version_id = 0;
            if (strpos($id, '-')) {
                $pvId = explode('-', $id);
                $product_id = $pvId[0];
                $product_version_id = !empty($pvId[1]) ? $pvId[1] : 0;
            }

            $item = $this->model_catalog_product->getDataAddOnDealProductById($product_id, $product_version_id);

            if (empty($item)) {
                continue;
            }

            $is_stock = true;
            if ($item['current_quantity'] < 1 || $item['pts_quantity'] < 1) {
                $is_stock = false;
            }

            if ($item['image'] != '') {
                $image = $this->model_tool_image->resize($item['image'], 100, 100);
            } else {
                $image = $this->model_tool_image->resize('no_image.png', 100, 100);
            }

            $version_name = implode(' • ', explode(',', $item['version']));
            $version_name = $version_name != '' ? $version_name : '';

            $sale_price = (float)$item['discount_price'];
            $original_price = empty($item['multi_versions']) ? (float)$item['p_compare_price'] : (float)$item['pv_compare_price'];
            $sku = empty($item['multi_versions']) ? $item['p_sku'] : $item['pv_sku'];

            $percent = round(($original_price - $sale_price) / $original_price * 100);

            if ($sale_price == 0) {
                $percent = 100;
            }

            $products[] = [
                "id" => $id,
                "url" => $this->url->link('product/product', 'product_id=' . $item['product_id'], true),
                "product_id" => $item['product_id'],
                "product_version_id" => $item['product_version_id'],
                "version" => $version_name,
                "product_name" => $item['name'],
                'name' => strlen($item['name']) <= 30 ? $item['name'] : mb_substr($item['name'], 0, 30, 'UTF-8') . ' ...',
                "image" => $image,
                "sale_price" => $sale_price,
                "original_price" => $original_price,
                "sale_price_format" => $this->currency->formatCustom($this->tax->calculate($sale_price, $item['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
                "original_price_format" => $this->currency->formatCustom($this->tax->calculate($original_price, $item['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
                "quantity" => (float)$item['pts_quantity'],
                "sku" => $sku,
                "weight" => (float)$item['weight'],
                "percent_reduce" => $percent,
                "in_stock" => $is_stock
            ];
        }

        return $products;
    }

    /**
     * @param $product
     * @param $need_format
     * @return array
     */
    public function calculateProductPrice($product) {
        $price = null;
        $old_price = null;
        if (!isset($product['is_contact_for_price'])) {
            $product['is_contact_for_price'] = $this->isContactForPrice($product['multi_versions'], $product['compare_price_master'], $product['min_compare_price_version']);
        }
        if ($product['is_contact_for_price']) {
            $this->load->language('product/product_detail');
            $price = $this->language->get('text_success');
        } else {
            if (1 == $product['multi_versions']) {
                if (0 != $product['price_version_check_null']) {
                    if (is_null($product['price_2']) && is_null($product['compare_price_2'])) {
                        $price =  $product['price_2'];
                    } else {
                        if (0 == $product['price_2'] && is_null($product['discount_id'])) {
                            $price = $product['compare_price_2'];
                        } else {
                            $price = $product['price_2'];
                            $old_price = $product['compare_price_2'];
                        }
                    }
                } else {
                    if (is_null($product['discount_id'])) {
                        $price = $product['min_compare_price_version'];
                    } else {
                        $price = $product['price_2'];
                        $old_price = $product['compare_price_2'];
                    }
                }
            } else {
                if (0 == $product['price'] && is_null($product['discount_id'])) {
                    $price =  $product['compare_price_master'];
                } else {
                    $price = $product['price_master'];
                    $old_price = $product['compare_price_master'];
                }
            }
        }

        return [
            'price' => $price,
            'old_price' => $old_price
        ];
    }
}