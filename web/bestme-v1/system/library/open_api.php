<?php
trait Open_Api
{
    protected $SCOPE_READ = "read";
    protected $SCOPE_MODIFY = "modify";

    protected $CRUD_INDEX = "index";
    protected $CRUD_CREATE = "create";
    protected $CRUD_STORE = "store";
    protected $CRUD_SHOW = "show";
    protected $CRUD_UPDATE = "update";
    protected $CRUD_DELETE = "delete";
    protected $CONFIG_CHATBOT_NOVAON_API_KEY = 'config_open_api_bestme';

    private $STATUS_CODE = [
        'bad_request' => 400,
        'forbidden' => 403,
        'method_not_allowed' => 405,
        'internal_server_error' => 500
    ];

    public function call_api($url, $body = null, $method = "GET") {
        $ARR_HTTPHEADER = array(
            "Content-Type: application/json",
            "Accept: application/json",
        );

        if (!empty($body)) {
            array_push($ARR_HTTPHEADER,"Content-Length: " . strlen($body));
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => $ARR_HTTPHEADER
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($response, true);
        return $response;
    }

    //token api from CMS
    public function getTokenApiKeyByShopName($shop_name) {
        try {
            $this->load->model('setting/setting');
            /* model data */
            $api_key = $this->model_setting_setting->getSettingValue($this->CONFIG_CHATBOT_NOVAON_API_KEY);
            if (empty($api_key)) {
                // auto create api key if not has
                $api_key = token(32);
                $this->model_setting_setting->editSettingValue($code = 'config', $this->CONFIG_CHATBOT_NOVAON_API_KEY, $api_key);
            }
            $token_string = isset($shop_name) ? $shop_name : '';
            if (empty($token_string)) {
                return null;
            }

            $token_string .= $api_key;
            $decryptedData = md5($token_string);
        } catch (\Exception $e) {
            $decryptedData = '';
        }

        return $decryptedData;
    }

    /**
     * get access token from header
     *
     * @return string|null
     */
    function getAccessToken() {
        $access_token = null;
        if (isset($_SERVER['HTTP_BESTME_API_ACCESSTOKEN'])) {
            $access_token = trim($_SERVER["HTTP_BESTME_API_ACCESSTOKEN"]);
        }
        else if (isset($_SERVER['HTTP_BESTME_API_ACCESSTOKEN'])) { //Nginx or fast CGI
            $access_token = trim($_SERVER["HTTP_BESTME_API_ACCESSTOKEN"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            if (isset($requestHeaders['Bestme-Api-AccessToken'])) {
                $access_token = trim($requestHeaders['Bestme-Api-AccessToken']);
            }
        }

        return $access_token;
    }

    public function mapScope($function, $scope = '')
    {
        switch ($function) {
            case $this->CRUD_INDEX:
                $scope = $this->SCOPE_READ;
                break;
            case $this->CRUD_CREATE:
                $scope = $this->SCOPE_MODIFY;
                break;
            case $this->CRUD_STORE:
                $scope = $this->SCOPE_MODIFY;
                break;
            case $this->CRUD_SHOW:
                $scope = $this->SCOPE_READ;
                break;
            case $this->CRUD_UPDATE:
                $scope = $this->SCOPE_MODIFY;
                break;
            case $this->CRUD_DELETE:
                $scope = $this->SCOPE_MODIFY;
                break;
        }

        return $scope;
    }

    /**
     * @param $function
     * @return bool
     */
    protected function validate($function, $scope = '')
    {
        try {
            $bestme_api_access_token = $this->getAccessToken();
            if (empty($bestme_api_access_token)) {
                return false;
            }

            $this->load->model('setting/setting');
            $app_settings = $this->model_setting_setting->getSettingValueByCodeAndKey('public_app', $bestme_api_access_token);

            // validate app expire date
            if ($app_settings['end_date'] != null && $app_settings['end_date'] < date("Y-m-d")) {
                return false;
            }

            // validate app permissions
            if (empty($app_settings['permissions'][self::PERMISSION]) || !in_array($this->mapScope($function, $scope), $app_settings['permissions'][self::PERMISSION])) {
                return false;
            }

            return true;
        } catch (Exception $exception) {
            $this->log->write('Validate open API error: '.$exception->getMessage());
            return false;
        }
    }

    protected function validateAccessToken(){
        try {
            $bestme_api_access_token = $this->getAccessToken();
            if (empty($bestme_api_access_token)) {
                return false;
            }

            $this->load->model('setting/setting');
            $app_settings = $this->model_setting_setting->getSettingValueByCodeAndKey('public_app', $bestme_api_access_token);

            // validate app expire date
            if ($app_settings['end_date'] != null && $app_settings['end_date'] < date("Y-m-d")) {
                return false;
            }

            return true;
        } catch (Exception $exception) {
            $this->log->write('Validate open API error: '.$exception->getMessage());
            return false;
        }
    }
}