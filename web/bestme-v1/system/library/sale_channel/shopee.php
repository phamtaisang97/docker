<?php

namespace Sale_Channel;


use DateTime;

class Shopee extends Abstract_Sale_Channel
{
    /** @var  int */
    protected $partner_id;

    /** @var  string */
    protected $partner_key;

    /** @var string */
    protected $shop_id;

    /** @var string */
    protected $prefix_url;

    /**
     * Shoppee constructor.
     * @param string $name
     * @param mixed $credential
     */
    public function __construct($name, $credential)
    {
        parent::__construct($name, $credential);
        $this->partner_id = SHOPEE_PARTNER_ID;
        $this->partner_key = SHOPEE_PARTNER_KEY;
        $this->prefix_url = SHOPEE_API_URL_PREFIX;
        $this->shop_id = $credential['shop_id'];
    }

    public static function getAuthLink($redirect)
    {
        $partner_id = defined('SHOPEE_PARTNER_ID') ? SHOPEE_PARTNER_ID : '';
        $partner_key = defined('SHOPEE_PARTNER_KEY') ? SHOPEE_PARTNER_KEY : '';
        if (!$partner_id || !$partner_key) {
            echo 'Thiếu config shopee';
            return;
        }
        $token = hash('sha256', $partner_key . $redirect);
        $url_prefix = defined('SHOPEE_API_URL_PREFIX') ? SHOPEE_API_URL_PREFIX : "https://partner.shopeemobile.com/";
        $auth_url = $url_prefix . "api/v1/shop/auth_partner"; // test env
        $redirect_url = urlencode($redirect);

        $auth_url = $auth_url . "?id=" . $partner_id . "&token=" . $token . "&redirect=" . $redirect_url;

        return $auth_url;
    }

    public static function disconnect($redirect)
    {
        $partner_id = defined('SHOPEE_PARTNER_ID') ? SHOPEE_PARTNER_ID : '';
        $partner_key = defined('SHOPEE_PARTNER_KEY') ? SHOPEE_PARTNER_KEY : '';
        if (!$partner_id || !$partner_key) {
            echo 'Thiếu config shopee';
            return;
        }
        $token = hash('sha256', $partner_key . $redirect);
        $url_prefix = defined('SHOPEE_API_URL_PREFIX') ? SHOPEE_API_URL_PREFIX : "https://partner.shopeemobile.com/";
        $auth_url = $url_prefix . "api/v1/shop/cancel_auth_partner"; // test env
        $redirect_url = urlencode($redirect);

        $auth_url = $auth_url . "?id=" . $partner_id . "&token=" . $token . "&redirect=" . $redirect_url;

        return $auth_url;
    }

    public function getShopInfo()
    {
        $url = "api/v1/shop/get";

        return $this->call_api($url, [], 'POST');
    }

    public function getCategory($data)
    {
        $url = "api/v1/item/categories/get";
        return $this->call_api($url, $data, 'POST');
    }

    public function getAttribute($data)
    {
        $url = "api/v1/item/attributes/get";
        return $this->call_api($url, $data, 'POST');
    }

    public function getLogistics($data)
    {
        $url = "api/v1/logistics/channel/get";
        return $this->call_api($url, $data, 'POST');
    }

    /**
     * @inheritdoc
     */
    public function getProducts(array $data)
    {
        /*$data = [
            'pagination_offset' => $offset,
            'pagination_entries_per_page' => $limit,
        ];*/

        $url = "api/v1/items/get";  // test url

        return $this->call_api($url, $data, 'POST');
    }

    /**
     * @inheritdoc
     */
    public function getProductDetail(array $data)
    {
        /*$data = [
            'item_id' => $item_id,
        ];*/

        $url = "api/v1/item/get";  // test url

        return $this->call_api($url, $data, 'POST');
    }

    /**
     * @inheritdoc
     */
    public function getCategories(array $data)
    {
        /*$data = [
            'language' => 'vi',
        ];*/

        $url = "api/v1/item/categories/get";  // test url

        return $this->call_api($url, $data, 'POST');
    }

    public function GetVariations($data)
    {
        /*$data = [
            'item_id' => $item_id
        ];*/

        $url = "api/v1/item/tier_var/get";  // test url

        return $this->call_api($url, $data, 'POST');
    }

    /**
     * @inheritdoc
     */
    public function getOrders(array $data)
    {
        /*$data = [
            'create_time_from' => $timestampFrom,
            'create_time_to' => $timestampTo,'
            'pagination_entries_per_page' => 100,
            'pagination_offset' => 0
        ];*/

        $url = "api/v1/orders/basics";

        return $this->call_api($url, $data, 'POST');
    }

    public function getOrderDetail(array $data)
    {
        /*$data['ordersn_list'] = [
            '200715DYWET543'
        ];*/

        $url = "api/v1/orders/detail";

        return $this->call_api($url, $data, 'POST');
    }

    /**
     * @inheritdoc
     */
    public function uploadProduct(array $data)
    {
        $url = "api/v1/item/add";

        return $this->call_api($url, $data, 'POST');
    }

    /**
     * @inheritdoc
     */
    public function updateProduct(array $data)
    {
        $url = "api/v1/item/update";

        return $this->call_api($url, $data, 'POST');
    }

    public function uploadInitTierVariation(array $data) // init TierVariations
    {
        $url = "api/v1/item/tier_var/init";

        return $this->call_api($url, $data, 'POST');
    }

    public function updateTierVariations(array $data) // update TierVariations
    {
        $url = "api/v1/item/tier_var/update_list";

        return $this->call_api($url, $data, 'POST');
    }

    public function updateVariationStockBatch(array $data)
    {
        $url = "api/v1/items/update/vars_stock";

        return $this->call_api($url, $data, 'POST');
    }

    public function updateVariationPriceBatch(array $data)
    {
        $url = "api/v1/items/update/vars_price";

        return $this->call_api($url, $data, 'POST');
    }

    public function addTierVariation(array $data) // add TierVariation
    {
        $url = "api/v1/item/tier_var/add";

        return $this->call_api($url, $data, 'POST');
    }

    public function deleteVariation(array $data)
    { // delete TierVaration
        $url = 'api/v1/item/delete_variation';

        return $this->call_api($url, $data, 'POST');
    }

    public function updateImages(array $data)
    {
        $url = 'api/v1/item/img/update';

        return $this->call_api($url, $data, 'POST');
    }

    public function updateProductStock(array $data) // update stock of product single version
    {
        $url = 'api/v1/items/update_stock';

        return $this->call_api($url, $data, 'POST');
    }

    public function updateProductPrice(array $data) // update price of product single version
    {
        $url = 'api/v1/items/update_price';

        return $this->call_api($url, $data, 'POST');
    }

    private function call_api($url, $data, $method = 'POST')
    {
        $date = new DateTime();
        $timestamp = $date->getTimestamp();

        $data['partner_id'] = $this->partner_id;
        $data['shopid'] = $this->shop_id;
        $data['timestamp'] = $timestamp;

        $url = $this->prefix_url . $url;
        if ($method == 'GET'){
            $url .= '?' . http_build_query($data);
        }

        $data = json_encode($data);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($data),
                "Authorization:" . $this->signature($url, $data)
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        return $response;
    }

    private function signature($url, $data)
    {
        $auth = $url . '|' . $data;

        return hash_hmac('sha256', $auth, $this->partner_key);
    }
}