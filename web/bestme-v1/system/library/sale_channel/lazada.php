<?php

namespace Sale_Channel;


use DateTime;

class Lazada extends Abstract_Sale_Channel
{
    /** @var  string */
    protected $app_key;

    /** @var  string */
    protected $secret_key;

    /** @var string */
    protected $access_token;

    /** @var string */
    protected $prefix_url;

    /** @var string */
    protected $sign_method = 'sha256';

    /**
     * Shoppee constructor.
     * @param string $name
     * @param mixed $credential
     */
    public function __construct($name, $credential)
    {
        parent::__construct($name, $credential);
        $this->app_key = LAZADA_APP_KEY;
        $this->secret_key = LAZADA_APP_SECRET_KEY;
        $this->prefix_url = LAZADA_API_URL_PREFIX;
        $this->access_token = $credential['access_token'];
    }

    public static function getAuthLink($redirect)
    {
        $app_key = defined('LAZADA_APP_KEY') ? LAZADA_APP_KEY : '';
        if (!$app_key) {
            echo 'Thiếu config lazada';
            return '';
        }
        $redirect_url = urlencode($redirect);
        $auth_url = "https://auth.lazada.com/oauth/authorize?response_type=code&force_auth=true&redirect_uri=$redirect_url&client_id=$app_key";

        return $auth_url;
    }

    public static function getAccessToken($code)
    {
        $app_key = defined('LAZADA_APP_KEY') ? LAZADA_APP_KEY : '';
        $app_secret = defined('LAZADA_APP_SECRET_KEY') ? LAZADA_APP_SECRET_KEY : '';
        if (!$app_key || !$app_secret) {
            echo 'Thiếu config lazada';
            return '';
        }

        $data['code'] = $code;
        $url = "https://auth.lazada.com/rest/auth/token/create";

        list($msec, $sec) = explode(' ', microtime());
        $timestamp = $sec . '000';

        $data['app_key'] = $app_key;
        $data['timestamp'] = $timestamp;
        $data['sign_method'] = 'sha256';

        // get sign
        $params = $data;
        ksort($params);
        $stringToBeSigned = '';
        $stringToBeSigned .= '/auth/token/create';
        foreach ($params as $k => $v) {
            $stringToBeSigned .= "$k$v";
        }
        unset($k, $v);
        $sign = hash_hmac('sha256', $stringToBeSigned, $app_secret);
        $sign = strtoupper($sign);
        // end get sign

        $data['sign'] = $sign;
        $data = json_encode($data);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($data)
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        return $response;
    }

    public static function refreshAccessToken($refresh_token)
    {
        $app_key = defined('LAZADA_APP_KEY') ? LAZADA_APP_KEY : '';
        $app_secret = defined('LAZADA_APP_SECRET_KEY') ? LAZADA_APP_SECRET_KEY : '';
        if (!$app_key || !$app_secret) {
            echo 'Thiếu config lazada';
            return '';
        }

        $data['refresh_token'] = $refresh_token;
        $url = "https://auth.lazada.com/rest/auth/token/refresh";

        list($msec, $sec) = explode(' ', microtime());
        $timestamp = $sec . '000';

        $data['app_key'] = $app_key;
        $data['timestamp'] = $timestamp;
        $data['sign_method'] = 'sha256';

        // get sign
        $params = $data;
        ksort($params);
        $stringToBeSigned = '';
        $stringToBeSigned .= '/auth/token/refresh';
        foreach ($params as $k => $v) {
            $stringToBeSigned .= "$k$v";
        }
        unset($k, $v);
        $sign = hash_hmac('sha256', $stringToBeSigned, $app_secret);
        $sign = strtoupper($sign);
        // end get sign

        $data['sign'] = $sign;
        $data = json_encode($data);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($data)
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        return $response;
    }

    public static function disconnect($redirect)
    {
        // TODO
    }

    public function getShopInfo()
    {
        $url = '/seller/get';

        return $this->call_api($url, [], 'GET');
    }

    public function getCategoryTree()
    {
        $url = '/category/tree/get';

        return $this->call_api($url, [], 'GET');
    }

    /**
     * @inheritdoc
     */
    public function getProducts(array $data)
    {
        /* Example data
         * $data = [
            'filter' => 'all', // all, live, inactive, deleted, image-missing, pending, rejected, sold-out
            'limit' => 10,
            'offset' => 0
        ];*/

        $url = '/products/get';

        return $this->call_api($url, $data, 'GET');
    }

    /**
     * @inheritdoc
     */
    public function getProductDetail(array $data)
    {
        /* Example data
         * $data = [
            'item_id' => '',
            'seller_sku' => ''
        ];*/

        $url = '/product/item/get';

        return $this->call_api($url, $data, 'GET');
    }

    /**
     * @inheritdoc
     */
    public function getCategories(array $data)
    {
        // TODO
    }

    public function getCategoryAttributes(array $data)
    {
        /* Example data
         * $data['primary_category_id'] = '123';*/

        $url = '/category/attributes/get';

        return $this->call_api($url, $data, 'GET');
    }


    /**
     * @inheritdoc
     */
    public function getOrders(array $data)
    {
        /* data example
         * $data = ['created_after' => '2020-08-08T09:00:00+08:00', 'limit' => 10, 'offset' => 0, 'sort_by' => 'created_at', 'sort_direction' => 'DESC'];*/
        $url = '/orders/get';
        return $this->call_api($url, $data, 'GET');
    }

    public function getOrderDetail(array $data)
    {
        /* data example
         * $data = ['order_id' => '111999'];*/

        $url = '/order/get';

        return $this->call_api($url, $data, 'GET');
    }

    public function getOrderItems(array $data)
    {
        /* data example
         * $data = ['order_id' => '111999'];*/

        $url = '/order/items/get';

        return $this->call_api($url, $data, 'GET');
    }

    /**
     * @inheritdoc
     */
    public function uploadProduct(array $data)
    {
        // TODO
    }

    /**
     * @inheritdoc
     */
    public function updateProduct(array $data)
    {
        // TODO
    }

    private function call_api($url, $data, $method = 'POST')
    {
        list($msec, $sec) = explode(' ', microtime());
        $timestamp = $sec . '000';

        $data['app_key'] = $this->app_key;
        $data['access_token'] = $this->access_token;
        $data['timestamp'] = $timestamp;
        $data['sign_method'] = $this->sign_method;
        $data['sign'] = $this->generateSign($url, $data);

        $url = $this->prefix_url . $url;
        if ($method == 'GET'){
            $url .= '?' . http_build_query($data);
        }
        $data = json_encode($data);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Content-Length: " . strlen($data)
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        return $response;
    }

    private function generateSign($apiName, $params)
    {
        ksort($params);

        $stringToBeSigned = '';
        $stringToBeSigned .= $apiName;
        foreach ($params as $k => $v) {
            $stringToBeSigned .= "$k$v";
        }

        unset($k, $v);

        return strtoupper($this->hmac_sha256($stringToBeSigned, $this->secret_key));
    }

    private function hmac_sha256($data, $key)
    {
        return hash_hmac('sha256', $data, $key);
    }
}