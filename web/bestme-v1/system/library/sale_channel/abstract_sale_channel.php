<?php
/**
 * @package     Bestme
 * @author      Bestme
 * @copyright   Copyright (c) 2019, Bestme - Novaon, Corp. (https://bestme.vn/)
 * @license     https://opensource.org/licenses/GPL-3.0
 * @link        https://bestme.vn
 */

namespace Sale_Channel;

/**
 * Sale_Channel class
 */
abstract class Abstract_Sale_Channel
{
    /** @var string */
    protected $name;

    /** @var mixed */
    protected $credential;

    /**
     * Transport constructor.
     * @param string $name
     * @param mixed $credential
     */
    public function __construct($name, $credential)
    {
        $this->name = $name;
        $this->credential = $credential;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCredential()
    {
        return $this->credential;
    }

    /**
     * @param mixed $credential
     */
    public function setCredential($credential)
    {
        $this->credential = $credential;
    }

    /**
     * get products from 3rd sale channel
     *
     * @param array $data due to specific transport
     * @return mixed
     */
    public abstract function getProducts(array $data);

    /**
     * get product detail from 3rd sale channel
     *
     * @param array $data due to specific transport
     * @return mixed
     */
    public abstract function getProductDetail(array $data);

    /**
     * get categories from 3rd sale channel
     *
     * @param array $data due to specific transport
     * @return array
     */
    public abstract function getCategories(array $data);

    /**
     * get orders from 3rd sale channel
     *
     * @param array $data due to specific transport
     * @return array
     */
    public abstract function getOrders(array $data);

    /**
     * upload product to 3rd sale channel
     *
     * @param array $data due to specific transport
     * @return array
     */
    public abstract function uploadProduct(array $data);

    /**
     * upload product to 3rd sale channel
     *
     * @param array $data due to specific transport
     * @return array
     */
    public abstract function updateProduct(array $data);
}