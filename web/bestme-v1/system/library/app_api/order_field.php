<?php

namespace App_Api;

/**
 * Class OrderFields
 */
class Order_Field
{
    const NAME = 1;
    const PRICE = 2;
    const IMAGE = 3;
    const OTHER_IMAGES = 4;
    const STORE = 5;
}