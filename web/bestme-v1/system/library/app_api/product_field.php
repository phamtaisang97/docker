<?php

namespace App_Api;


/**
 * Class ProductFields
 */
class Product_Field
{
    const PRODUCT_ID = 1001;
    const PRODUCT_NAME = 1002;
    const PRODUCT_PRICE = 1003;
    const PRODUCT_IMAGE = 1004;
    const PRODUCT_OTHER_IMAGES = 1005;
    const STORE_ID = 5001;
    const STORE_NAME = 5002;
    const CATEGORY_ID = 6001;
    const COLLECTION_ID = 7001;
    const MANUFACTURER_ID = 8001;
    const MANUFACTURER_NAME = 8002;
    const TAG_ID = 9001;
    const TAG_NAME = 9002;

    const SORT_ASC = 1;
    const SORT_DESC = 2;

    public static $SUPPORTED_FIELDS = [
        self::PRODUCT_ID,
    ];
}