<?php


namespace App_Api;


use Exception;

class Query_App_Builder
{
    const CODE_SUCCESS = 200;
    const CODE_NOT_SUPPORTED_TABLE = 400;

    protected static $CODE_MAP = [
        self::CODE_SUCCESS => 'SUCCESS',
        self::CODE_NOT_SUPPORTED_TABLE => 'NOT_SUPPORTED_TABLE',
    ];

    public function __construct($registry)
    {
        $this->config = $registry->get('config');
        $this->db = $registry->get('db');
    }

    public function getData($table, $fields = null, $conditions = null, $group_by = null, $sort_by = null, $sort_direction = 'DESC', $start = null, $limit = null)
    {
        /* validate */
        $validate_result = $this->validate($table, $fields, $conditions, $group_by, $sort_by, $start, $limit, $sort_direction);
        if (isset($validate_result['code']) && $validate_result['code'] !== self::CODE_SUCCESS) {
            // do somethings...

            return $validate_result;
        }

        /* build query */
        $sql = $this->buildQuery($table, $fields, $conditions, $group_by, $sort_by, $sort_direction, $start, $limit);
        /* execute query */
        $result = [];
        try {
            $result = $this->db->query($sql)->rows;
        } catch (Exception $e) {
        }

        return $result;
    }

    private function validate($table, $fields = null, $conditions = null, $group_by = null, $sort_by = null, $sort_direction = 'DESC', $start = null, $limit = null)
    {
        /* validate table */
        if (!in_array($table, Table_Name::$SUPPORTED_TABLES)) {
            return [
                'code' => self::CODE_NOT_SUPPORTED_TABLE,
                'message' => self::$CODE_MAP[self::CODE_NOT_SUPPORTED_TABLE]
            ];
        }

        /* validate fields */


        /* validate conditions */


        /* validate group_by */


        /* validate sort */
    }

    private function buildQuery($table, $fields = null, $conditions = null, $group_by = null, $sort_by = null, $sort_direction = 'DESC', $start = null, $limit = null)
    {
        switch ($table) {
            case Table_Name::PRODUCT:
                return $this->buildQueryProduct($fields, $conditions, $group_by, $sort_by, $sort_direction = 'DESC', $start, $limit);

            default:
                return '';
        }
    }

    private function buildQueryProduct($fields = null, $conditions = null, $group_by = null, $sort_by = null, $sort_direction = 'DESC', $start = null, $limit = null)
    {
        $DB_PREFIX = DB_PREFIX;

        /* build sql select */
        $sqlSelect = "SELECT p.product_id, pd.name as product_name";

        // Add field to SELECT
        if (in_array(Product_Field::PRODUCT_IMAGE, $fields)) {
            $sqlSelect .= ", p.image as product_image ";
        }

        if (in_array(Product_Field::PRODUCT_PRICE, $fields)) {
            $sqlSelect .= ", p.price ";
        }

        /* build sql join */
        $sqlJoin = " LEFT JOIN {$DB_PREFIX}product_description pd ON (pd.product_id = p.product_id)";
        $sqlWhere = " WHERE pd.language_id = " . (int)$this->config->get('config_language_id') . " ";

        /* Create Conditions */
        if (isset($conditions[Product_Field::PRODUCT_ID])) {
            $sqlWhere .= ' AND p.product_id = ' . $conditions[Product_Field::PRODUCT_ID];
        }

        if (isset($conditions[Product_Field::MANUFACTURER_ID])) {
            $sqlJoin .= " LEFT JOIN {$DB_PREFIX}product_to_category c ON (p.product_id = c.product_id)";
            $sqlWhere .= ' AND m.manufacturer_id IN (' . $conditions[Product_Field::MANUFACTURER_ID] . ')';
        }

        if (isset($conditions[Product_Field::CATEGORY_ID])) {
            $sqlJoin .= " LEFT JOIN {$DB_PREFIX}product_to_category c ON (p.product_id = c.product_id)";
            $sqlWhere .= ' AND c.category_id IN (' . $conditions[Product_Field::CATEGORY_ID] . ')';
        }

        if (isset($conditions[Product_Field::COLLECTION_ID])) {
            $sqlJoin .= " LEFT JOIN {$DB_PREFIX}product_collection pc ON (p.product_id = pc.product_id)";
            $sqlWhere .= ' AND pc.collection_id IN (' . $conditions[Product_Field::COLLECTION_ID] . ')';
        }

        if (isset($conditions[Product_Field::TAG_NAME])) {
            $sqlJoin .= " LEFT JOIN {$DB_PREFIX}product_tag pt ON (pt.product_id = p.product_id)";
            $sqlJoin .= " LEFT JOIN {$DB_PREFIX}tag t ON (t.tag_id = pt.tag_id)";
            $sqlWhere .= " AND t.value RLIKE '" . $conditions[Product_Field::TAG_NAME] . "'";
        }

        /* search by product name */
        /*if (isset($conditions['filter_name']) && $conditions['filter_name'] !== '') {
            $sqlWhere .= " AND (pd.`name` LIKE '%" . $this->db->escape(trim($conditions['filter_name'])) . "%' ";
            $sqlWhere .= " OR t.`value` LIKE '%" . $this->db->escape(trim($conditions['filter_name'])) . "%' ) ";
        }*/

        $sqlWhere .= " AND p.`deleted` IS NULL ";

        /* build sql group */
        $sql_group = '';
        if (!$group_by) {
            $sql_group = " GROUP BY p.product_id ";
        } else {
            switch ($group_by) {
                case Product_Field::PRODUCT_ID:
                    $sql_group = " GROUP BY p.product_id ";
                    break;

                case Product_Field::MANUFACTURER_ID:
                    $sql_group = " GROUP BY m.manufacturer_id ";
                    break;
            }
        }

        /* build sql order */
        $sql_order_by = '';
        if (!$sort_by) {
            $sql_order_by = " ORDER BY p.date_modified DESC";
        } else {
            $has_sort_by = true;
            switch ($sort_by) {
                case Product_Field::PRODUCT_ID:
                    $sql_order_by = " ORDER BY p.product_id ";
                    break;

                case Product_Field::MANUFACTURER_ID:
                    $sql_order_by = " ORDER BY m.manufacturer_id ";
                    break;

                default:
                    $has_sort_by = false;
            }

            if ($has_sort_by) {
                if ($sort_direction) {
                    $sql_order_by .= " DESC ";
                } else {
                    switch ($sort_by) {
                        case Product_Field::SORT_ASC:
                            $sql_order_by = " ASC ";
                            break;

                        default:
                            $sql_order_by = " DESC ";
                    }
                }
            }
        }

        /* pagination */
        $limit = "";
        if (isset($start) || isset($limit)) {
            if ($start < 0) {
                $start = 0;
            }

            if ($limit < 1) {
                $limit = 20;
            }

            $limit = " LIMIT " . (int)$start . "," . (int)$limit;
        }

        /* build final sql */
        $sql = $sqlSelect . " FROM {$DB_PREFIX}product p " . $sqlJoin . $sqlWhere . $group_by . $sql_order_by . $limit;

        return $sql;
    }
}