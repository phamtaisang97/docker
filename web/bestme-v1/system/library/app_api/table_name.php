<?php

namespace App_Api;


/**
 * Class ProductFields
 */
final class Table_Name
{
    const PRODUCT = 1;
    const ORDER = 2;

    public static $SUPPORTED_TABLES = [
        self::PRODUCT,
        self::ORDER,
    ];
}