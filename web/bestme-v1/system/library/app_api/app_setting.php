<?php


namespace App_Api;


class App_Setting
{
    public function __construct($registry)
    {
        $this->config = $registry->get('config');
        $this->db = $registry->get('db');
    }
    public function addModule($code, $data) {
        $this->db->query("INSERT INTO `" . DB_PREFIX . "appstore_setting` SET `name` = '" . $this->db->escape($data['name']) . "', `code` = '" . $this->db->escape($code) . "', `setting` = '" . $this->db->escape(json_encode($data)) . "'");
    }

    public function editModule($module_id, $data) {
        $this->db->query("UPDATE `" . DB_PREFIX . "appstore_setting` SET `name` = '" . $this->db->escape($data['name']) . "', `setting` = '" . $this->db->escape(json_encode($data)) . "' WHERE `module_id` = '" . (int)$module_id . "'");
    }

    public function deleteModule($module_id) {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "appstore_setting` WHERE `module_id` = '" . (int)$module_id . "'");

        $this->db->query("DELETE FROM `" . DB_PREFIX . "app_config_theme` WHERE `module_id` = '" . (int)$module_id . "'");

        //$this->db->query("DELETE FROM `" . DB_PREFIX . "layout_appstore` WHERE `code` LIKE '%." . (int)$module_id . "'");
    }

    public function getModule($module_id) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "appstore_setting` WHERE `module_id` = '" . (int)$module_id . "'");

        if ($query->row) {
            return json_decode($query->row['setting'], true);
        } else {
            return array();
        }
    }

    public function getModules() {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "appstore_setting` ORDER BY `code`");

        return $query->rows;
    }

    public function getModulesByCode($code) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "appstore_setting` WHERE `code` = '" . $this->db->escape($code) . "' ORDER BY `name`");

        return $query->rows;
    }

    public function deleteModulesByCode($code) {
        /* Xóa các module và cài đặt vị trí các module của app */
        $sqlModules = "SELECT module_id FROM " . DB_PREFIX . "appstore_setting  
                WHERE `code` = '" . $this->db->escape($code) . "'";
        $modules = $this->db->query($sqlModules)->rows;
        foreach($modules as $module){
            $sqlDeleteModule = "DELETE FROM " . DB_PREFIX . "appstore_setting
                            WHERE `module_id` = '" .$module['module_id'] ."' ";

            $sqlDeleteConfig = "DELETE FROM " . DB_PREFIX . "app_config_theme
                            WHERE `module_id` = '" .$module['module_id'] ."' ";

            $this->db->query($sqlDeleteModule);
            $this->db->query($sqlDeleteConfig);
        }
        //$this->db->query("DELETE FROM `" . DB_PREFIX . "appstore_setting` WHERE `code` = '" . $this->db->escape($code) . "'");
        //$this->db->query("DELETE FROM `" . DB_PREFIX . "layout_appstore` WHERE `code` LIKE '" . $this->db->escape($code) . "' OR `code` LIKE '" . $this->db->escape($code . '.%') . "'");
    }
    public function updateInfo($app_code,$data){
        if(isset($app_code) && isset($data['path_logo']) && isset($data['app_name'])){
           $sql =  "UPDATE " . DB_PREFIX . "my_app SET 
                `path_logo` = '" . $this->db->escape($data['path_logo']) . "',
                `app_name` = '" . $this->db->escape($data['app_name']) . "' 
                 WHERE app_code = '" . $this->db->escape($app_code) . "' ";
           $this->db->query($sql);
        }
    }
    public function addAsset($app_code,$asset){
        if(isset($app_code) && !empty($asset)){
            $path_asset = json_encode($asset);
            $sql =  "UPDATE " . DB_PREFIX . "my_app SET 
                `path_asset` = '" . $this->db->escape($path_asset) . "'
                 WHERE app_code = '" . $this->db->escape($app_code) . "' ";
            $this->db->query($sql);
        }
    }
    public function getAsset(){
        $assets = null;
        $sql = "SELECT * FROM " . DB_PREFIX . "my_app WHERE status = 1 AND path_asset IS NOT NULL";

        $query = $this->db->query($sql);

        $assets = array();
        foreach ($query->rows as $result) {
            if($this->checkModuleEnable($result['app_code'])){
                $assets[] = json_decode($result['path_asset']);
            }
        }
        return $assets;
    }
    protected function checkModuleEnable($app_code){
        $modules = $this->getModulesByCode($app_code);
        $check = false;
        foreach ($modules as $module){
            $setting = $this->getModule($module['module_id']);
            if(isset($setting['status']) && $setting['status'] == 1){
                $check = true;
                break;
            }
        }
        return $check;
    }

}