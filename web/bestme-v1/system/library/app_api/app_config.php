<?php


namespace App_Api;


class App_Config
{
    public function __construct($registry)
    {
        $this->config = $registry->get('config');
        $this->db = $registry->get('db');
    }


    public function addConfigTheme($module_id,$data){
        /*Check xem co cai dat chua
        TH1: Neu co thi Update thong tin
        TH2: Chua co thi tao moi */
        $sqlCheck = "SELECT COUNT(config.module_id) AS total FROM " . DB_PREFIX . "app_config_theme config 
                WHERE config.module_id = " . (int)$module_id . " 
                  AND config.theme_value = '". $this->db->escape($data['theme_value']). "'";
        $queryCheck = $this->db->query($sqlCheck);
        // Update db
        if($queryCheck->row['total'] > 0){
            $sql = "UPDATE `" . DB_PREFIX . "app_config_theme` SET `position` = '" . $this->db->escape($data['position']) . "',
                                                            `page` = '" . $this->db->escape($data['page']) . "', 
                                                            `sort` = " . (int)$data['sort'] . "                                                          
                                                                WHERE `module_id` = " . (int)$module_id . " 
                                                                AND `theme_value` = '" . $this->db->escape($data['theme_value']) . "'";

            $this->db->query($sql);
        }else{
            $sql = "INSERT INTO `" . DB_PREFIX . "app_config_theme` 
                SET `module_id` = '" . (int)$module_id . "', 
                    `theme_value` = '" . $this->db->escape($data['theme_value']). "', 
                    `position` = '" . $this->db->escape($data['position']). "', 
                    `page` = '" . $this->db->escape($data['page']) . "', 
                    `sort` = '" . (int)$data['sort'] . "'";

            $this->db->query($sql);
        }
    }
    public  function registerApp($data){
        $sql = "INSERT INTO `" . DB_PREFIX . "appstore` 
                SET `name` = '" . $this->db->escape($data['name']). "', 
                    `code` = '" . $this->db->escape($data['code']). "', 
                    `sort_description` = '" . $this->db->escape($data['sort_description']). "', 
                    `description` = '" . $this->db->escape($data['description']) . "', 
                    `price` = '" . (float)$data['price'] . "',
                    `path_image` = '" .  $this->db->escape($data['path_image']). "'";
        $this->db->query($sql);
    }
    public function unRegisterApp($code){
        /* Chuyển status của app trong my_app về 0*/
        $sql = "UPDATE `" . DB_PREFIX . "my_app` SET `status` = 0 
                WHERE `app_code` = '" . $this->db->escape($code). "'";
        $this->db->query($sql);
    }
}