<?php

Class Activity_Tracking_Util
{
    use Redis_Util;

    /**
     * @param $shop_name
     * @param $packet
     * @param $total
     */
    public function trackingCountShopeeOrder($domain, $shop_name, $packet, $total)
    {
        $this->saveToRedis('total_amount_shopee', $domain, $shop_name, $packet, $total);
    }

    public function trackingCountShopeeOrderCompleted($domain, $shop_name, $packet)
    {
        $this->saveToRedis('count_order_complete', $domain, $shop_name, $packet);
    }
    /**
     * @param $key
     * @param $data
     */
    public function pushDataToRedis($key, $data)
    {
        try {
            $redis = $this->redisConnect();

            $redis->lPush($key, json_encode($data));
            $this->redisClose($redis);
        } catch (Exception $e) {
            $this->redisClose($redis);
        }
    }

    /**
     * @param $code
     * @param string $data
     */
    public function saveToRedis($code, $domain , $shop_name, $packet, $data = '')
    {

        $result = [
            'key' => $code,
            'data' => $data,
            'shop_name' => $shop_name,
            'domain' => $domain,
            'time' => date("Y-m-d H:i:s"),
            'packet_paid' => $packet
        ];
        $this->pushDataToRedis('INDICATOR_TRACKING', $result);
    }
}