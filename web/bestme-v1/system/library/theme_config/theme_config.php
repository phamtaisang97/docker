<?php

namespace theme_config;


use theme_config\validators\Config_Validator_Interface;
use theme_config\validators\Theme_Validator;

class Theme_Config
{
    /**
     * @var \Config
     */
    private $config;

    /**
     * @var Config_Validator_Interface[]
     */
    private $configValidators;

    /**
     * @var \DB
     */
    private $db;

    public function __construct(\Registry $registry)
    {
        $this->config = $registry->get('config');
        $this->db = $registry->get('db');

        $this->configValidators = [
            'color' => new Theme_Validator(realpath(__DIR__ . '/schema/theme/color.json')),
            'text' => new Theme_Validator(realpath(__DIR__ . '/schema/theme/text.json')),
            'favicon' => new Theme_Validator(realpath(__DIR__ . '/schema/theme/favicon.json')),
            'social' => new Theme_Validator(realpath(__DIR__ . '/schema/theme/social.json')),
            'checkout-page' => new Theme_Validator(realpath(__DIR__ . '/schema/theme/checkout-page.json')),
        ];
    }

    /* === theme === */

    /**
     * @return array
     * @throws \Exception
     */
    public function buildThemeColorConfig()
    {
        $currentDir = __DIR__;
        $config = json_decode(file_get_contents(realpath($currentDir . '/default_config/theme/color.json')), true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Could not load default Theme Color Config');
        }

        return $config;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function buildThemeTextConfig()
    {
        $config = json_decode(file_get_contents(realpath(__DIR__ . '/default_config/theme/text.json')), true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Could not load default Theme Text Config');
        }

        return $config;
    }

    /**
     * @param array $config
     */
    public function validateColorConfig(array $config)
    {
        $this->configValidators['color']->validate($config);
    }

    /**
     * @param array $config
     */
    public function validateTextConfig(array $config)
    {
        $this->configValidators['text']->validate($config);
    }

    /**
     * @param array $config
     */
    public function validateSectionSlideShowConfig(array $config)
    {
        return true;
        $this->configValidators['slideshow']->validate($config);
    }

    public function applyThemeColorConfig(array $color, &$css_file_content)
    {
        $this->validateColorConfig($color);

        $main_color = $color['main']['main']['hex'];
        $button_color = $color['main']['button']['hex'];

        /* notice: add "#" before color if not has */
        if (!empty($main_color) && strpos($main_color, '#') !== 0) {
            $main_color = sprintf('#%s', $main_color);
        }

        if (!empty($button_color) && strpos($button_color, '#') !== 0) {
            $button_color = sprintf('#%s', $button_color);
        }

        $css_file_content = str_replace('$MAIN_COLOR$', $main_color, $css_file_content);
        $css_file_content = str_replace('$BUTTON_COLOR$', $button_color, $css_file_content);
    }

    public function applyThemeTextConfig($text, &$cssFileContent)
    {
        $this->validateTextConfig($text);

        $all_font = $text['all']['font'];

        $cssFileContent = str_replace('$ALL_FONT$', $all_font, $cssFileContent);
    }

    /* === end - theme === */

    /* === section === */
    public function applySectionSlideShowConfig($slideshow, &$css_file_content)
    {
        $this->validateTextConfig($slideshow);

        $transition_time = $slideshow['setting']['transition-time'] . 's';

        $css_file_content = str_replace('$SECTION_SLIDESHOW_TRANSITION_TIME$', $transition_time, $css_file_content);
    }
    /* === end - section === */
}
