<?php

namespace theme_config\validators;


interface Config_Validator_Interface
{
    /**
     * @param array $config
     * @throws \InvalidArgumentException
     */
    public function validate(array $config);

    /**
     * @return array
     */
    public function getErrors();

    /**
     * @return string
     */
    public function getErrorsString();
}