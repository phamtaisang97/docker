<?php

namespace theme_config\validators;

use JsonSchema\Validator;

class Theme_Validator implements Config_Validator_Interface
{
    /**
     * @var string file path
     */
    protected $schema;

    /**
     * @var Validator
     */
    protected $validator;

    public function __construct($schema)
    {
        if (!file_exists($schema) || !is_file($schema) || !is_readable($schema)) {
            throw new \InvalidArgumentException('Invalid given schema, please check file exist and is readable');
        }

        $this->schema = $schema;
        $this->validator = new Validator();
    }

    /**
     * @inheritdoc
     */
    public function validate(array $config)
    {
        // make stdClass, not array!
        $_config = json_decode(json_encode($config));
        $this->validator->validate($_config, (object)['$ref' => 'file://' . $this->schema]);

        return true; // temp not validate. TODO: remove...

        if (!$this->validator->isValid()) {
            throw new \InvalidArgumentException(sprintf('Invalid given config: ', $this->getErrorsString()));
        }
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->validator->getErrors();
    }

    /**
     * @return string
     */
    public function getErrorsString()
    {
        $errors = [];

        foreach ($this->getErrors() as $error) {
            $errors[] = sprintf("[%s: %s]", $error['property'], $error['message']);
        }

        return implode(', ', $errors);
    }
}