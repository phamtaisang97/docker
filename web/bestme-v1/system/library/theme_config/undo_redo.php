<?php

namespace theme_config;

/**
 * Class Undo_Redo
 * @package theme_config
 *
 * work cases:
 *
 * give data: 1 2 3 4 5, limit = 5, current index = latest = 5 (with *)
 * data: 1  2  3  4  5*
 * undo: 1  2  3  4* 5
 * undo: 1  2  3* 4  5
 * redo: 1  2  3  4* 5
 * redo: 1  2  3  4  5*
 * undo: 1  2  3  4*
 * add : 1  2  3  4  6*  => removed: 5
 * undo: 1  2  3  4* 6
 * redo: 1  2  3  4  6*
 * add : 2  3  4  6  7*  => truncate: 1
 * add : 3  4  6  7  8*  => truncate: 2
 * undo: 3  4  6  7* 8
 * redo: 3  4  6  7  8*
 * ===
 * data: 3 4 6 7 8
 */
class Undo_Redo
{
    private $data;
    private $index;
    private $limit;

    /**
     * Undo_Redo constructor.
     * @param array $data
     * @param int $index force -1 if data empty
     * @param int $limit set -1 known no limit
     */
    public function __construct($data = [], $index = -1, $limit = 5)
    {
        $this->data = $data;
        $this->index = empty($data) ? -1 : $index;
        $this->limit = is_int($limit) && $limit > 0 ? $limit : -1;
    }

    /**
     * @return int
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @return array
     */
    public function allData()
    {
        return $this->data;
    }

    /**
     * undo
     */
    public function undo()
    {
        $this->index--;
        $this->sync();
    }

    /**
     * redo
     */
    public function redo()
    {
        $this->index++;
        $this->sync();
    }

    /**
     * add
     * @param $data
     */
    public function add($data)
    {
        // add to right
        $currentData = $this->allManagedData();
        $currentData[] = $data;

        // remove from left
        if ($this->limit > 0 && count($currentData) > $this->limit) {
            $currentData = array_slice($currentData, count($currentData) - $this->limit);
            $this->index = count($currentData) - 2; // then +1 after this!
        }

        // set new data
        $this->data = $currentData;

        // maintain index
        $this->index++;
        $this->sync();
    }

    /**
     * current data
     * @return array
     */
    public function allManagedData()
    {
        if (empty($this->data) || $this->index <= 0 || $this->index >= count($this->data) - 1) {
            return $this->data;
        }

        return array_slice($this->data, 0, $this->index + 1);
    }

    /**
     * current data
     * @return mixed
     */
    public function currentData()
    {
        return (empty($this->data) || !isset($this->data[$this->index])) ? null : $this->data[$this->index];
    }

    /**
     * sync
     */
    private function sync()
    {
        /* empty data */
        if (empty($this->data)) {
            $this->index = -1;
            return;
        }

        /* index at first of data */
        if ($this->index < 0) {
            $this->index = -1;
        }

        /* index over data length */
        if ($this->index >= count($this->data)) {
            $this->index = count($this->data) - 1;
        }
    }
}