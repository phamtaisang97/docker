    # How to define theme demo data

Welcome! This guides you how to define theme demo data.

### If not yet defined special theme

use theme group by default:

- beauty.sql
- fashion.sql
- furniture.sql
- tech.sql
 
or create new theme group... such as:

- sport.sql
- pet.sql
- ...

### To define special theme demo data

following rules:

1. naming

   add dir and sql files like this "realestate_realestates":
   
   ```
   library/theme_config/demo_data/special_theme
   |
    \_ realestate_realestates
       |
        \_ blog.sql
       |
        \_ category.sql
       |
        \_ collection.sql
       |
        \_ product.sql
       |
        \_ seo_url.sql
       |
        \_ theme_builder_config.sql
       |
        \_ demo_data_config.json
   ```

1. content

   fill content in sql exported from demo shop. e.g
   
   ```
   -- category.sql
   -- oc_category PRIMARY KEY (`category_id`)
   INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
   (21, NULL, 0, 0, 0, 0, 1, '2020-06-23 20:20:36', '2020-06-23 20:20:36'),
   (22, NULL, 0, 0, 0, 0, 1, '2020-06-23 20:20:36', '2020-06-23 20:20:36');
   
   
   -- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
   INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
   (22, 2, 'Biệt thự cao cấp', 'Biệt thự cao cấp', '', '', ''),
   (22, 1, 'Biệt thự cao cấp', 'Biệt thự cao cấp', '', '', ''),
   (21, 1, 'Biệt thự nhà vườn', 'Biệt thự nhà vườn', '', '', ''),
   (21, 2, 'Biệt thự nhà vườn', 'Biệt thự nhà vườn', '', '', '');
   
   ...
   ```
   
1. some notes

   1. blog.sql
   
      - set `demo` = 1
      
   1. product.sql
   
      - remove product has `deleted` = 1
      
      - keep product has `demo` = 0 only (product create/edit by hand)
      
      - then set product `demo` = 1
      
      - use `INSERT IGNORE INTO ...` to avoid sql exception
      
      - notice: `xxx_product_description` may occur multi times due to long sql, remove it! And also set `;` before to `,`
          
      - copy product ids array to `products` array in file `demo_data_config.json` 
      
   1. seo_url.sql
   
      - keep seo url associated with blog, category, collection, product above 
      
      - EXCLUDE seo url from default. List:
      
        - chinh-sach-va-bao-mat
        - common/home
        - common/shop
        - contact/contact
        - checkout/profile
        - account/login
        - account/register
        - account/logout
        - checkout/setting
        - checkout/my_orders
        - checkout/order_preview
        - checkout/order_preview/orderSuccess
        - blog/blog
      
   1. theme_builder_config.sql
   
      - remove theme_builder_config id
      
   1. /asset/resources.json
   
      - remove if theme not use this file (see /common/header.twig load css, js directly...)
      