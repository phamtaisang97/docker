--
-- create new demo data
--
CREATE TABLE IF NOT EXISTS `oc_demo_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_id` int(11) NOT NULL,
  `table_name` varchar(255) DEFAULT '',
  `theme_group` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- create new demo data
--

INSERT IGNORE INTO `oc_demo_data` (`data_id`, `table_name`, `theme_group`) VALUES
-- product table
(73, 'product', 'tech'),
(74, 'product', 'tech'),
(76, 'product', 'tech'),
(78, 'product', 'tech'),
(79, 'product', 'tech'),
(80, 'product', 'tech'),
(82, 'product', 'tech'),
(100, 'product', 'tech'),
(101, 'product', 'tech'),
(102, 'product', 'tech'),
-- category table
(1, 'category', 'tech'),
(2, 'category', 'tech'),
(3, 'category', 'tech'),
(4, 'category', 'tech'),
(5, 'category', 'tech'),
(6, 'category', 'tech'),
(7, 'category', 'tech'),
(8, 'category', 'tech'),
(9, 'category', 'tech'),
-- collection table
(4, 'collection', 'tech'),
(5, 'collection', 'tech'),
(6, 'collection', 'tech'),
(7, 'collection', 'tech');
-- seo_url table: auto deleted when deleting product, category, collection

-- category
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(1, NULL, 0, 0, 0, 0, 1, '2019-01-09 11:29:42', '2019-01-09 11:29:42'),
(2, NULL, 0, 0, 0, 0, 1, '2019-01-09 15:36:36', '2019-01-09 15:36:36'),
(3, NULL, 0, 0, 0, 0, 1, '2019-01-09 15:36:50', '2019-01-09 15:36:50'),
(4, NULL, 0, 0, 0, 0, 1, '2019-01-09 15:37:11', '2019-01-09 15:37:11'),
(5, NULL, 0, 0, 0, 0, 1, '2019-01-09 15:37:34', '2019-01-09 15:37:34'),
(6, NULL, 0, 0, 0, 0, 1, '2019-01-09 16:02:06', '2019-01-09 16:02:06'),
(7, NULL, 0, 0, 0, 0, 1, '2019-01-09 16:02:24', '2019-01-09 16:02:24'),
(8, NULL, 0, 0, 0, 0, 1, '2019-04-22 09:58:25', '2019-04-22 09:58:25'),
(9, NULL, 0, 0, 0, 0, 1, '2019-05-10 16:14:17', '2019-05-10 16:14:17');

-- oc_category_description
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, 2, 'Điện thoại', 'Điện thoại chất lương tốt nhất thị trường', '', '', ''),
(1, 1, 'Điện thoại', 'Điện thoại chất lương tốt nhất thị trường', '', '', ''),
(2, 2, 'Máy tính bảng', 'Máy tính bảng tốt nhất thị trường', '', '', ''),
(2, 1, 'Máy tính bảng', 'Máy tính bảng tốt nhất thị trường', '', '', ''),
(3, 2, 'Máy tính xách tay', 'Máy tính xách tay tốt nhất thị trường', '', '', ''),
(3, 1, 'Máy tính xách tay', 'Máy tính xách tay tốt nhất thị trường', '', '', ''),
(4, 2, 'Máy tính để bàn', 'Máy tính để bàn tốt nhất thị trường', '', '', ''),
(4, 1, 'Máy tính để bàn', 'Máy tính để bàn tốt nhất thị trường', '', '', ''),
(5, 2, 'Phụ kiện công nghệ', 'Phụ kiện công nghệ hấp dẫn nhất hiện nay', '', '', ''),
(5, 1, 'Phụ kiện công nghệ', 'Phụ kiện công nghệ hấp dẫn nhất hiện nay', '', '', ''),
(6, 2, 'Tai nghe', 'Tai nghe chất lượng cao', '', '', ''),
(6, 1, 'Tai nghe', 'Tai nghe chất lượng cao', '', '', ''),
(7, 2, 'Sạc dự phòng', 'Sạc dự phòng chất lượng cao', '', '', ''),
(7, 1, 'Sạc dự phòng', 'Sạc dự phòng chất lượng cao', '', '', ''),
(8, 2, 'Đồng hồ', 'Đồng hồ', '', '', ''),
(8, 1, 'Đồng hồ', 'Đồng hồ', '', '', ''),
(9, 2, 'Awei', 'Awei', '', '', ''),
(9, 1, 'Awei', 'Awei', '', '', '');

-- oc_category_path
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(1, 1, 0),
(2, 2, 0),
(3, 3, 0),
(4, 4, 0),
(5, 5, 0),
(6, 5, 0),
(6, 6, 1),
(7, 5, 0),
(7, 7, 1),
(8, 8, 0),
(9, 9, 0);

-- oc_category_to_store
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(5, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0);

-- oc_collection
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`) VALUES
(5, 'Sản phẩm hot', 0, 1, ''),
(6, 'Sản phẩm mới', 0, 1, ''),
(4, 'Sản phẩm khuyến mại', 0, 1, ''),
(7, 'Hàng ngày', 0, 1, NULL);

-- oc_collection_description
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(4, 'https://res.cloudinary.com/novaon-x2/image/upload/v1555900767/x2-8787.bestme.test/Capture28.png', '&lt;p&gt;sản phẩm khuyến mại&lt;/p&gt;', '', '', ''),
(5, 'https://res.cloudinary.com/novaon-x2/image/upload/v1555900690/x2-8787.bestme.test/dell-vostro-5568-077m52-vangdong-450x300-450x300-600x600.png', '&lt;p&gt;sản phẩm hot&lt;/p&gt;', '', '', ''),
(6, '', '&lt;p&gt;nhũng sản phẩm mới nhất c&amp;oacute; mặt tại cửa h&amp;agrave;ng&lt;/p&gt;', '', '', ''),
(7, 'https://res.cloudinary.com/novaon-x2/image/upload/v1558345596/x2-8888.bestme.test/8ts19s033-fw125-l_2.jpg', '&lt;p&gt;Bộ sưu tập h&amp;agrave;ng ng&amp;agrave;y&lt;/p&gt;', '', '', '');

-- oc_product
-- 73
-- 74
-- 76
-- 78
-- 79
-- 80
-- 82
-- 100
-- 101
-- 102
UPDATE `oc_product` SET `deleted` = NULL WHERE `product_id` IN (73, 74, 76, 78, 79, 80, 82, 100, 101, 102) AND `demo` = 1;

INSERT IGNORE INTO `oc_product` (`product_id`, `model`, `sku`, `barcode`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `sale_on_out_of_stock`, `stock_status_id`, `image`, `image_alt`, `multi_versions`, `manufacturer_id`, `shipping`, `price`, `price_currency_id`, `compare_price`, `c_price_currency_id`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `viewed`, `date_added`, `date_modified`, `demo`) VALUES
(73, '', '', '', '', '', '', '', '', '', 350, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558663712/x2-8888.bestme.test/iphone-xs-max-gray-400x460.png', '', 0, 1, 1, '29990000.0000', 1, '35000000.0000', 1, 0, 0, '0000-00-00', '208.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-20 16:03:08', '2019-05-20 16:03:08', 1),
(74, '', '', '', '', '', '', '', '', '', 320, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558663876/x2-8888.bestme.test/samsung-galaxy-note8-black-400x460.png', '', 0, 6, 1, '11000000.0000', 1, '14990000.0000', 1, 0, 0, '0000-00-00', '208.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-20 16:07:02', '2019-05-20 16:07:02', 1),
(76, '', 'HUKW3943JJJ', '', '', '', '', '', '', '', 316, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558666180/x2-8888.bestme.test/ipad-6th-wifi-128-gb-1-400x460.png', '', 0, 1, 1, '10500000.0000', 1, '10500000.0000', 1, 0, 0, '0000-00-00', '325.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-20 16:12:44', '2019-05-20 16:12:44', 1),
(78, '', 'HUKW3943888', '', '', '', '', '', '', '', 400, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558666241/x2-8888.bestme.test/ipad-mini-79-inch-wifi-cellular-64gb-2019-gray-400x460.png', '', 0, 1, 1, '9500000.0000', 1, '10500000.0000', 1, 0, 0, '0000-00-00', '325.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-20 16:22:09', '2019-05-20 16:22:09', 1),
(79, '', 'MREE2SA', '', '', '', '', '', '', '', 350, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558666115/x2-8888.bestme.test/apple-macbook-air-mree2sa-a-i5-8gb-128gb-133-gold-400x400.jpg', '', 0, 1, 1, '30699000.0000', 1, '31000000.0000', 1, 0, 0, '0000-00-00', '2500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-20 16:35:06', '2019-05-20 16:35:06', 1),
(80, '', 'S510UN', '', '', '', '', '', '', '', 345, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558666050/x2-8888.bestme.test/asus-s510un-i5-8250u-4gb-1tb-mx150-win10-bq276t-33397-thumb-400x400.jpg', '', 0, 8, 1, '16700000.0000', 1, '16700000.0000', 1, 0, 0, '0000-00-00', '2200.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-20 16:38:08', '2019-05-20 16:38:08', 1),
(82, '', '14IKB', '', '', '', '', '', '', '', 325, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558666485/x2-8888.bestme.test/lenovo-ideapad-530s-14ikb-i7-8550u-8gb-256gb-win10-33397-thumb-400x400.jpg', '', 0, 7, 1, '28990000.0000', 1, '29990000.0000', 1, 0, 0, '0000-00-00', '2100.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-20 16:40:29', '2019-05-20 16:40:29', 1),
(100, '', 'SKU215', '', '', '', '', '', '', '', 420, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558665200/x2-8888.bestme.test/32.png', '', 0, 6, 1, '2150000.0000', 1, '3850000.0000', 1, 0, 0, '0000-00-00', '750.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-21 15:57:29', '2019-05-21 15:57:29', 1),
(101, '', 'SKU1999', '', '', '', '', '', '', '', 350, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558665277/x2-8888.bestme.test/34.png', '', 0, 2, 1, '1999000.0000', 1, '2500000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-21 16:03:41', '2019-05-21 16:03:41', 1),
(102, '', 'SKU2991', '', '', '', '', '', '', '', 400, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558665508/x2-8888.bestme.test/38.png', '', 0, 25, 1, '250000.0000', 1, '299000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-21 16:13:09', '2019-05-21 16:13:09', 1);

-- oc_product_collection
INSERT IGNORE INTO `oc_product_collection` (`product_collection_id`, `product_id`, `collection_id`, `sort_order`) VALUES
(73, 73, 4, 0),
(74, 74, 6, 0),
(131, 76, 5, 0),
(112, 100, 5, 0),
(113, 100, 4, 0),
(114, 100, 6, 0),
(115, 101, 5, 0),
(116, 101, 4, 0),
(117, 101, 6, 0),
(126, 102, 6, 0),
(125, 102, 4, 0),
(124, 102, 5, 0);

-- oc_product_descriptions
INSERT IGNORE INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `sub_description`, `seo_title`, `seo_description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(73, 2, 'Điện thoại iPhone Xs Max 64GB', '&lt;h3&gt;Ho&amp;agrave;n to&amp;agrave;n xứng đ&amp;aacute;ng với những g&amp;igrave; được mong chờ, phi&amp;ecirc;n bản cao cấp nhất IPhone Xs Max&amp;nbsp;của Apple năm nay nổi bật với chip A12 Bionic mạnh mẽ, m&amp;agrave;n h&amp;igrave;nh rộng đến 6.5 inch, c&amp;ugrave;ng camera k&amp;eacute;p tr&amp;iacute; tuệ nh&amp;acirc;n tạo v&amp;agrave; Face ID được n&amp;acirc;ng cấp.&lt;/h3&gt;', '', '', '', '', '', '', ''),
(74, 2, 'Điện thoại Samsung Galaxy Note 8', '&lt;h2&gt;Galaxy Note 8 l&amp;agrave; niềm hy vọng vực lại d&amp;ograve;ng Note danh tiếng của điện thoại SamSung&amp;nbsp;với diện mạo nam t&amp;iacute;nh, sang trọng. M&amp;aacute;y trang bị camera k&amp;eacute;p x&amp;oacute;a ph&amp;ocirc;ng thời thượng, m&amp;agrave;n h&amp;igrave;nh c&amp;ocirc; cực&amp;nbsp;như tr&amp;ecirc;n S8 Plus, b&amp;uacute;t S Pen c&amp;ugrave;ng nhiều t&amp;iacute;nh năng mới v&amp;agrave; nhiều c&amp;ocirc;ng nghệ được ưu &amp;aacute;i.&lt;/h2&gt;', '', '', '', '', '', '', ''),
(76, 2, 'Máy tính bảng iPad Wifi 128 GB (2018)', '&lt;h2&gt;&amp;nbsp;IPad Wifi 128 GB l&amp;agrave; chiếc iPad mới được Apple ra mắt với mức gi&amp;aacute; phải chăng hơn hứa hẹn sẽ mang c&amp;aacute;c thiết bị iPad đến được với đ&amp;ocirc;ng đảo người d&amp;ugrave;ng hơn.&lt;/h2&gt;', '', 'Máy tính bảng iPad Wifi 128 GB (2018)', 'Máy tính bảng iPad Wifi 128 GB (2018)', '', '', '', ''),
(78, 2, 'Máy tính bảng iPad Mini Wifi Cellular 64GB (2019)', '&lt;h2&gt;M&amp;aacute;y t&amp;iacute;nh bảng iPad Mini Wifi Cellular 64GB (2019) đ&amp;aacute;nh dấu sự trở lại mạnh mẽ của Apple trong ph&amp;acirc;n kh&amp;uacute;c m&amp;aacute;y t&amp;iacute;nh bảng nhỏ gọn, c&amp;oacute; thể dễ d&amp;agrave;ng mang theo b&amp;ecirc;n m&amp;igrave;nh.&lt;/h2&gt;', '', 'Máy tính bảng iPad Mini Wifi Cellular 64GB (2019)', 'Máy tính bảng iPad Mini Wifi Cellular 64GB (2019)', '', '', '', ''),
(79, 2, 'Laptop Apple Macbook Air 2018 128GB (MREE2SA)', '&lt;h1&gt;Laptop Apple Macbook Air 2018 i5/8GB/128GB (MREE2SA/A)&lt;/h1&gt;', '', 'Laptop Apple Macbook Air 2018 128GB (MREE2SA)', 'Laptop Apple Macbook Air 2018 128GB (MREE2SA)', '', '', '', ''),
(80, 2, 'Laptop Asus VivoBook S510UN i5 8250U', '&lt;h1&gt;Laptop Asus VivoBook S510UN i5 8250U&lt;/h1&gt;', '', 'Laptop Asus VivoBook S510UN i5 8250U', 'Laptop Asus VivoBook S510UN i5 8250U', '', '', '', ''),
(82, 2, 'Laptop Lenovo Ideapad 530S 14IKB i7 8550U', '&lt;h1&gt;Laptop Lenovo Ideapad 530S 14IKB i7 8550U&lt;/h1&gt;', '', 'Laptop Lenovo Ideapad 530S 14IKB i7 8550U', 'Laptop Lenovo Ideapad 530S 14IKB i7 8550U', '', '', '', ''),
(100, 2, 'ĐIỆN THOẠI SAMSUNG A9 XÁCH TAY', '&lt;p&gt;Th&amp;ocirc;ng số kỹ thuật&lt;/p&gt;\r\n\r\n&lt;p&gt;M&amp;agrave;n h&amp;igrave;nh: Super AMOLED, 6.3&amp;quot;, Full HD+&lt;/p&gt;\r\n\r\n&lt;p&gt;Hệ điều h&amp;agrave;nh: Android 8.0 (Oreo)&lt;/p&gt;\r\n\r\n&lt;p&gt;Camera sau: 24 MP, 10 MP, 8 MP v&amp;agrave; 5 MP (4 camera)&lt;/p&gt;\r\n\r\n&lt;p&gt;Camera trước: 24 MP&lt;/p&gt;\r\n\r\n&lt;p&gt;CPU: Qualcomm Snapdragon 660 8 nh&amp;acirc;n&lt;/p&gt;\r\n\r\n&lt;p&gt;RAM: 6 GB&lt;/p&gt;\r\n\r\n&lt;p&gt;Bộ nhớ trong: 64 GB&lt;/p&gt;\r\n\r\n&lt;p&gt;Thẻ nhớ: MicroSD, hỗ trợ tối đa 512 GB&lt;/p&gt;', '', '', '', '', '', '', ''),
(101, 2, 'ĐIỆN THOẠI OPPO F9 XÁCH TAY CAO CẤP GIÁ RẺ 64GB', '&lt;p&gt;1. Thiết kế gọn g&amp;agrave;ng v&amp;agrave; &amp;ocirc;m tay, viền m&amp;agrave;n h&amp;igrave;nh tr&amp;agrave;n cạnh&lt;/p&gt;\r\n\r\n&lt;p&gt;2. M&amp;agrave;n h&amp;igrave;nh tr&amp;agrave;n viền, 6.3&amp;#39; inch, độ ph&amp;acirc;n giải Full HD&lt;/p&gt;\r\n\r\n&lt;p&gt;3. Cấu h&amp;igrave;nh OPPO F9 vừa đủ d&amp;ugrave;ng zalo,Facebook,youtobe&lt;/p&gt;\r\n\r\n&lt;p&gt;4. Camera selfie ấn tượng : Ch&amp;iacute;nh: 16.0MP; Phụ: 25.0MP 5. Android 8.1 c&amp;ugrave;ng ColorOS 2.5D mới mẻ, nhận diện khu&amp;ocirc;n mặt ....&lt;/p&gt;\r\n\r\n&lt;p&gt;Th&amp;ocirc;ng số cơ bản..&lt;/p&gt;', '', '', '', '', '', '', ''),
(102, 2, 'Tai nghe Gaming G-Net H7S Có Rung, Led 7 màu', '&lt;p&gt;-Tai nghe G-Net H7S l&amp;agrave; phi&amp;ecirc;n bản n&amp;acirc;ng cấp từ d&amp;ograve;ng tai nghe G-Net H7 l&amp;agrave; sản phẩm tai nghe nhận được nhiều sự y&amp;ecirc;u th&amp;iacute;ch từ c&amp;aacute;c game thủ.&lt;/p&gt;\r\n\r\n&lt;p&gt;Đặc điểm nổi bật của Tai nghe G-Net H7S -T&amp;iacute;nh năng ưu việt Tai nghe Gnet H7S Rung Led với thiết kế &amp;acirc;m thanh đỉnh cao c&amp;ugrave;ng chế độ rung theo bass cho bạn kh&amp;ocirc;ng gian nghe nhạc sống động v&amp;agrave; ch&amp;acirc;n thực hơn bao giờ hết&lt;/p&gt;\r\n\r\n&lt;p&gt;Đối với c&amp;aacute;c bản nhạc lớn tai nghe cho ra &amp;acirc;m thanh bass c&amp;ugrave;ng chế độ rung nghe cực đ&amp;atilde; tai. C&amp;aacute;c bạn cần cắm cổng usb v&amp;agrave; chạm v&amp;agrave;o n&amp;uacute;t cảm ứng b&amp;ecirc;n dưới micro để bật chế độ rung mạnh.&lt;/p&gt;\r\n\r\n&lt;p&gt;-Thiết kế độc đ&amp;aacute;o Tai nghe G-Net H7S được thiết kế c&amp;ugrave;ng kiều d&amp;aacute;ng v&amp;agrave; m&amp;agrave;u sắc c&amp;aacute; t&amp;iacute;nh đem lại sự hiện đại, mạnh mẽ rất được c&amp;aacute;c game thủ ưa chuộng Thiết kế hiện đại nhưng sử dụng v&amp;ocirc; c&amp;ugrave;ng thoải m&amp;aacute;i v&amp;agrave; đơn giản.&lt;/p&gt;\r\n\r\n&lt;p&gt;Phần ốp v&amp;agrave; gọng tai nghe đều được ốp kim loại c&amp;oacute; khả năng chống va đập rơi vỡ Thiết kế ho&amp;agrave;n hảo n&amp;agrave;y gi&amp;uacute;p bạn thoải m&amp;aacute;i sử dụng m&amp;agrave; kh&amp;ocirc;ng cần qu&amp;aacute; lo lắng khi bị va chạm.&lt;/p&gt;\r\n\r\n&lt;p&gt;Phần ốp được thiết kế bản to, &amp;ocirc;m trọn tai tạo sự thoải m&amp;aacute;i cho người đeo Chất liệu ốp tai bằng da chất lượng cao rất &amp;ecirc;m kh&amp;ocirc;ng g&amp;acirc;y đau nhức hay b&amp;iacute; tai.&lt;/p&gt;\r\n\r\n&lt;p&gt;Tai nghe G-net H7S c&amp;oacute; Micro được thiết kế nhỏ gọn chắc chắn thuận tiện cho người sử dụng.&lt;/p&gt;\r\n\r\n&lt;p&gt;Tai nghe được thiết kế n&amp;uacute;t điều chỉnh &amp;acirc;m lượng ngay ph&amp;iacute;a sau tai cho người sử dụng c&amp;oacute; thể dễ d&amp;agrave;ng kiểm so&amp;aacute;t &amp;acirc;m thanh theo &amp;yacute; muốn.&lt;/p&gt;\r\n\r\n&lt;p&gt;-Kết nối đơn giản Tai nghe G-Net H7S c&amp;oacute; thể sử dụng đa dạng tr&amp;ecirc;n c&amp;aacute;c thiết bị như M&amp;aacute;y t&amp;iacute;nh, Laptop, Điện thoại..&lt;/p&gt;', '', '', '', '', '', '', '');

-- oc_product_image
INSERT IGNORE INTO `oc_product_image` (`product_image_id`, `product_id`, `image`, `image_alt`, `sort_order`) VALUES
(140, 82, 'https://res.cloudinary.com/novaonx2/image/upload/v1558666481/x2-8888.bestme.test/lenovo-ideapad-330s-14ikbr-i5-8250u-4gb-1tb-win10-33397-thumb-400x400.jpg', '', 0),
(142, 82, 'https://res.cloudinary.com/novaonx2/image/upload/v1558666473/x2-8888.bestme.test/asus-x407ua-i5-8250u-4gb-16gb-1tb-win10-bv485t-thumb33397-400x400.jpg', '', 2),
(141, 82, 'https://res.cloudinary.com/novaonx2/image/upload/v1558666477/x2-8888.bestme.test/asus-x507uf-i5-8250u-4gb-1tb-2gb-mx130-ej121t-thumb-1-400x400.jpg', '', 1),
(120, 100, 'https://res.cloudinary.com/novaonx2/image/upload/v1558665195/x2-8888.bestme.test/31.png', '', 0),
(121, 100, 'https://res.cloudinary.com/novaonx2/image/upload/v1558665189/x2-8888.bestme.test/30.png', '', 1),
(122, 101, 'https://res.cloudinary.com/novaonx2/image/upload/v1558665272/x2-8888.bestme.test/33.png', '', 0),
(130, 102, 'https://res.cloudinary.com/novaonx2/image/upload/v1558665494/x2-8888.bestme.test/35.png', '', 2),
(129, 102, 'https://res.cloudinary.com/novaonx2/image/upload/v1558665497/x2-8888.bestme.test/36.png', '', 1),
(128, 102, 'https://res.cloudinary.com/novaonx2/image/upload/v1558665502/x2-8888.bestme.test/37.png', '', 0);

-- oc_product_to_category
INSERT IGNORE INTO `oc_product_to_category` (`product_id`, `category_id`) VALUES
(73, 1),
(74, 1),
(76, 2),
(78, 2),
(80, 3),
(82, 3),
(100, 1),
(101, 1),
(102, 3),
(102, 5),
(102, 6);

-- oc_product_to_store
INSERT IGNORE INTO `oc_product_to_store` (`product_id`, `store_id`) VALUES
(73, 0),
(74, 0),
(76, 0),
(78, 0),
(79, 0),
(80, 0),
(82, 0),
(100, 0),
(101, 0),
(102, 0);

INSERT IGNORE INTO `oc_warehouse` (`warehouse_id`, `product_id`, `product_version_id`) VALUES
(1043, 73, NULL),
(1044, 74, NULL),
(1046, 76, NULL),
(1050, 78, NULL),
(1051, 79, NULL),
(1052, 80, NULL),
(1054, 82, NULL),
(1101, 100, NULL),
(1102, 101, NULL),
(1103, 102, NULL);

-- oc_seo_url
INSERT IGNORE INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`, `table_name`) VALUES
(2001, 0, 1, 'product_id=73', 'product-73', 'product'),
(2002, 0, 2, 'product_id=73', 'product-73', 'product'),
(2003, 0, 1, 'product_id=74', 'product-74', 'product'),
(2004, 0, 2, 'product_id=74', 'product-74', 'product'),
(2005, 0, 1, 'product_id=76', 'product-76', 'product'),
(2006, 0, 2, 'product_id=76', 'product-76', 'product'),
(2007, 0, 1, 'product_id=78', 'product-78', 'product'),
(2008, 0, 2, 'product_id=78', 'product-78', 'product'),
(2009, 0, 1, 'product_id=79', 'product-79', 'product'),
(2010, 0, 2, 'product_id=79', 'product-79', 'product'),
(2011, 0, 1, 'product_id=80', 'product-80', 'product'),
(2012, 0, 2, 'product_id=80', 'product-80', 'product'),
(2013, 0, 1, 'product_id=82', 'product-82', 'product'),
(2014, 0, 2, 'product_id=82', 'product-82', 'product'),
(2015, 0, 1, 'product_id=100', 'product-100', 'product'),
(2016, 0, 2, 'product_id=100', 'product-100', 'product'),
(2017, 0, 1, 'product_id=101', 'product-101', 'product'),
(2018, 0, 2, 'product_id=101', 'product-101', 'product'),
(2019, 0, 1, 'product_id=102', 'product-102', 'product'),
(2020, 0, 2, 'product_id=102', 'product-102', 'product'),
-- category table
(2021, 0, 2, 'category_id=1', 'category-1', 'category'),
(2022, 0, 2, 'category_id=2', 'category-2', 'category'),
(2023, 0, 2, 'category_id=3', 'category-3', 'category'),
(2024, 0, 2, 'category_id=4', 'category-4', 'category'),
(2025, 0, 2, 'category_id=5', 'category-5', 'category'),
(2026, 0, 2, 'category_id=6', 'category-6', 'category'),
(2027, 0, 2, 'category_id=7', 'category-7', 'category'),
(2028, 0, 2, 'category_id=8', 'category-8', 'category'),
(2029, 0, 2, 'category_id=9', 'category-9', 'category'),
-- collection table
(2030, 0, 2, 'collection=4', 'collection-4', 'category'),
(2031, 0, 2, 'collection=5', 'collection-5', 'category'),
(2032, 0, 2, 'collection=6', 'collection-6', 'category'),
(2033, 0, 2, 'collection=7', 'collection-7', 'category');
