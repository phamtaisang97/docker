-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(21, NULL, 0, 0, 0, 0, 1, '2020-06-22 16:54:40', '2020-06-22 16:54:40'),
(22, NULL, 0, 0, 0, 0, 1, '2020-06-22 16:54:40', '2020-06-22 16:54:40'),
(23, NULL, 0, 0, 0, 0, 1, '2020-06-22 16:54:40', '2020-06-22 16:54:40'),
(24, NULL, 0, 0, 0, 0, 1, '2020-06-22 16:54:40', '2020-06-22 16:54:40'),
(25, NULL, 0, 0, 0, 0, 1, '2020-06-22 16:54:40', '2020-06-22 16:54:40'),
(26, NULL, 0, 0, 0, 0, 1, '2020-06-22 16:54:40', '2020-06-22 16:54:40');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(21, 2, 'Yoga giảm cân', 'Yoga giảm cân', '', '', ''),
(21, 1, 'Yoga giảm cân', 'Yoga giảm cân', '', '', ''),
(22, 2, 'Yoga trẻ hóa', 'Yoga trẻ hóa', '', '', ''),
(22, 1, 'Yoga trẻ hóa', 'Yoga trẻ hóa', '', '', ''),
(23, 2, 'Yoga nâng cao', 'Yoga nâng cao', '', '', ''),
(23, 1, 'Yoga nâng cao', 'Yoga nâng cao', '', '', ''),
(24, 2, 'Yoga trị liệu', 'Yoga trị liệu', '', '', ''),
(24, 1, 'Yoga trị liệu', 'Yoga trị liệu', '', '', ''),
(25, 2, 'Yoga hồi phục', 'Yoga hồi phục', '', '', ''),
(25, 1, 'Yoga hồi phục', 'Yoga hồi phục', '', '', ''),
(26, 2, 'Yoga cơ bản', 'Yoga cơ bản', '', '', ''),
(26, 1, 'Yoga cơ bản', 'Yoga cơ bản', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(21, 21, 0),
(22, 22, 0),
(23, 23, 0),
(24, 24, 0),
(25, 25, 0),
(26, 26, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(21, 0),
(22, 0),
(23, 0),
(24, 0),
(25, 0),
(26, 0);