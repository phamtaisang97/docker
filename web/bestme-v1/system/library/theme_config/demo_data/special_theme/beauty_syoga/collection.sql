-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(10, '28 Ngày Phép Màu Yoga', 0, 1, '', '0'),
(9, 'Bí Mật Thiền Ứng Dụng', 0, 1, '', '0'),
(8, 'Tuyệt Chiêu Yoga Tăng Tự Tin', 0, 1, '', '0'),
(11, 'Bí Mật - Yoga Sinh Lý Nữ', 0, 1, '', '0'),
(12, 'Dinh Dưỡng Trẻ Hóa Làn Da', 0, 1, '', '0'),
(13, 'Yoga chữa bệnh dạ dày', 0, 1, '', '0'),
(14, 'Yoga Bà Bầu, Mẹ Khỏe Con Thông Minh', 0, 1, '', '0'),
(15, 'Tuyệt Chiêu Yoga Tăng Tự Tin', 0, 1, '', '0'),
(16, 'Yoga Trị Liệu - Hồi Phục Tự Nhiên', 0, 1, '', '0'),
(17, 'Nhập Môn Yoga Cổ Điển', 0, 1, '', '0'),
(18, 'Thiền Và Quản Trị Cảm Xúc', 0, 1, '', '0'),
(19, 'Dinh Dưỡng Sức Khỏe', 0, 1, '', '0'),
(20, 'TH8', 0, 1, '', '1'),
(21, 'TH4', 0, 1, '', '1'),
(22, 'TH', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(10, 'https://cdn.bestme.asia/images/x2/bst6-min.png', '', '', '', ''),
(9, 'https://cdn.bestme.asia/images/x2/bst7-min.png', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/x2/bst5-min.png', '', '', '', ''),
(12, 'https://cdn.bestme.asia/images/x2/bst3-min.png', '', '', '', ''),
(13, 'https://cdn.bestme.asia/images/x2/bst2-min.png', '', '', '', ''),
(14, 'https://cdn.bestme.asia/images/x2/bst1-min.png', '', '', '', ''),
(8, 'https://cdn.bestme.asia/images/x2/bst8-min.png', '', '', '', ''),
(15, 'https://cdn.bestme.asia/images/x2/cac-tu-the-yoga-song-ao-cung-ban-than-dep-nha-20.jpg', '', '', '', ''),
(16, 'https://cdn.bestme.asia/images/x2/unnamed-1.jpg', '', '', '', ''),
(17, 'https://cdn.bestme.asia/images/x2/bst4.png', '', '', '', ''),
(18, 'https://cdn.bestme.asia/images/x2/5-ly-do-ru-ban-than-di-tap-yoga-728159282582229.jpg', '', '', '', ''),
(19, 'https://cdn.bestme.asia/images/x2/yogabeyond628159282583029.jpg', '', '', '', ''),
(20, '', '', '', '', ''),
(21, '', '', '', '', ''),
(22, '', '', '', '', '');