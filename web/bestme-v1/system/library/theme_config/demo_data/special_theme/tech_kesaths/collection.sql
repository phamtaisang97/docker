-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(27, 'Kệ siêu thị', 0, 1, '', '0'),
(28, 'Kệ siêu thị đơn', 0, 1, '', '0'),
(29, 'Kệ siêu thị đôi', 0, 1, '', '0'),
(30, 'Kệ kho hàng', 0, 1, '', '0'),
(31, 'Kệ công nghiệp', 0, 1, '', '0'),
(32, 'Kệ hàng khối lượng lớn', 0, 1, '', '1'),
(33, 'Kệ trưng bày bán lẻ', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(27, '', '', '', '', ''),
(28, '', '', '', '', ''),
(29, '', '', '', '', ''),
(30, '', '', '', '', ''),
(31, '', '', '', '', ''),
(32, '', '', '', '', ''),
(33, '', '', '', '', '');