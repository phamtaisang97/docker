-- oc_blog PRIMARY KEY (`blog_id`)
INSERT IGNORE INTO `oc_blog` (`blog_id`, `author`, `status`, `demo`, `date_publish`, `date_added`, `date_modified`) VALUES
(1, 1, 1, 1, '2020-08-07 11:21:51', '2020-08-07 11:21:51', '2020-08-07 18:37:48'),
(2, 1, 1, 1, '2020-08-07 11:22:23', '2020-08-07 11:22:23', '2020-08-07 18:37:32'),
(3, 1, 1, 1, '2020-08-07 11:22:42', '2020-08-07 11:22:42', '2020-08-07 18:37:08');

-- oc_blog_category PRIMARY KEY (`blog_category_id`)
INSERT IGNORE INTO `oc_blog_category` (`blog_category_id`, `status`, `date_added`, `date_modified`) VALUES
(1, 1, '2019-11-11 13:37:51', '2019-11-11 19:03:46');

-- oc_blog_category_description PRIMARY KEY (`blog_category_id`,`language_id`)
INSERT IGNORE INTO `oc_blog_category_description` (`blog_category_id`, `language_id`, `title`, `meta_title`, `meta_description`, `alias`) VALUES
(1, 1, 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'danh-muc-mac-dinh-uncategorized'),
(1, 2, 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'danh-muc-mac-dinh-uncategorized');

-- oc_blog_description PRIMARY KEY (`blog_id`,`language_id`)
INSERT IGNORE INTO `oc_blog_description` (`blog_id`, `language_id`, `title`, `content`, `short_content`, `image`, `meta_title`, `meta_description`, `seo_keywords`, `alias`, `alt`, `type`, `video_url`) VALUES
(1, 2, 'a', '&lt;p&gt;a&lt;/p&gt;', '', 'https://cdn.bestme.asia/images/x2/1-3.png', '', '', NULL, 'a', NULL, 'image', NULL),
(2, 2, 'aa', '', '', 'https://cdn.bestme.asia/images/x2/3-3.png', '', '', NULL, 'aa', NULL, 'image', NULL),
(3, 2, 'aaa', '', '', 'https://cdn.bestme.asia/images/x2/2-3.png', '', '', NULL, 'aaa', NULL, 'image', NULL);

-- oc_blog_to_blog_category NO PRIMARY KEY
-- remove first
DELETE FROM `oc_blog_to_blog_category`
WHERE `blog_id` IN (1, 2, 3) AND `blog_category_id` = 1;

-- insert
INSERT IGNORE INTO `oc_blog_to_blog_category` (`blog_id`, `blog_category_id`) VALUES
(1, 1),
(2, 1),
(3, 1);