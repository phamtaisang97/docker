-- oc_product PRIMARY KEY (`product_id`)
-- delete first
INSERT IGNORE INTO `oc_product` (`product_id`, `model`, `sku`, `common_barcode`, `common_sku`, `common_compare_price`, `common_price`, `common_cost_price`, `barcode`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `sale_on_out_of_stock`, `stock_status_id`, `image`, `image_alt`, `multi_versions`, `manufacturer_id`, `shipping`, `price`, `price_currency_id`, `compare_price`, `c_price_currency_id`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `channel`, `viewed`, `demo`, `deleted`, `default_store_id`, `user_create_id`, `date_added`, `date_modified`) VALUES
(112, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://cdn.bestme.asia/images/x2/3-2.png', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '100.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2020-08-07 10:05:00', '2020-08-19 12:01:34'),
(113, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://cdn.bestme.asia/images/x2/2-2.png', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '100.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2020-08-07 10:05:00', '2020-08-19 12:01:18'),
(114, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://cdn.bestme.asia/images/x2/1-128159781321529.png', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '100.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2020-08-07 10:05:00', '2020-08-19 12:01:04'),
(115, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://cdn.bestme.asia/images/x2/4_kLCncdl.png', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '100.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2020-08-07 10:05:00', '2020-08-19 12:01:47');

-- restore soft-deleted products if demo
UPDATE `oc_product` SET `deleted` = NULL
WHERE `demo` = 1
  AND `product_id` IN (
    112,
    113,
    114,
    115
  );

-- oc_product_collection PRIMARY KEY (`product_collection_id`)
INSERT IGNORE INTO `oc_product_collection` (`product_collection_id`, `product_id`, `collection_id`, `sort_order`) VALUES
(152, 115, 8, 0),
(149, 114, 8, 0),
(150, 113, 8, 0),
(151, 112, 8, 0);

-- oc_product_description PRIMARY KEY (`product_id`,`language_id`)
INSERT IGNORE INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `sub_description`, `seo_title`, `seo_description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(112, 2, 'Fun Factory Wave Dildo', '&lt;p&gt;Good sex can help you express your desires and feelings. And we are ready to surprise you how many ways of the very expression may be. We propose detailed information about every our product, as well as, the feedback base where people like you share their opinions and sex toy experience.&lt;/p&gt;', '&lt;p&gt;Good sex can help you express your desires and feelings. And we are ready to surprise you how many ways of the very expression may be. We propose detailed information about every our product, as well as, the feedback base where people like you share their opinions and sex toy experience.&lt;/p&gt;', '', '', '', '', '', ''),
(113, 2, 'B Swish bcute classic', '&lt;p&gt;Good sex can help you express your desires and feelings. And we are ready to surprise you how many ways of the very expression may be. We propose detailed information about every our product, as well as, the feedback base where people like you share their opinions and sex toy experience.&lt;/p&gt;', '&lt;p&gt;Good sex can help you express your desires and feelings. And we are ready to surprise you how many ways of the very expression may be. We propose detailed information about every our product, as well as, the feedback base where people like you share their opinions and sex toy experience.&lt;/p&gt;', '', '', '', '', '', ''),
(114, 2, 'Celeste Silicone Kegel Balls with Pull Cord', '&lt;p&gt;Good sex can help you express your desires and feelings. And we are ready to surprise you how many ways of the very expression may be. We propose detailed information about every our product, as well as, the feedback base where people like you share their opinions and sex toy experience.&lt;/p&gt;', '&lt;p&gt;Good sex can help you express your desires and feelings. And we are ready to surprise you how many ways of the very expression may be. We propose detailed information about every our product, as well as, the feedback base where people like you share their opinions and sex toy experience.&lt;/p&gt;', '', '', '', '', '', ''),
(115, 2, 'Crazycity New Multi Speed 01', '&lt;p&gt;Good sex can help you express your desires and feelings. And we are ready to surprise you how many ways of the very expression may be. We propose detailed information about every our product, as well as, the feedback base where people like you share their opinions and sex toy experience.&lt;/p&gt;', '&lt;p&gt;Good sex can help you express your desires and feelings. And we are ready to surprise you how many ways of the very expression may be. We propose detailed information about every our product, as well as, the feedback base where people like you share their opinions and sex toy experience.&lt;/p&gt;', '', '', '', '', '', '');


-- oc_product_image PRIMARY KEY (`product_image_id`)
-- INSERT IGNORE INTO `oc_product_image` (`product_image_id`, `product_id`, `image`, `image_alt`, `sort_order`) VALUES
-- nothing

-- oc_product_to_category PRIMARY KEY (`product_id`,`category_id`)
-- INSERT IGNORE INTO `oc_product_to_category` (`product_id`, `category_id`) VALUES
-- nothing

-- oc_product_to_store PRIMARY KEY (`product_id`,`store_id`)
INSERT IGNORE INTO `oc_product_to_store` (`product_id`, `store_id`, `product_version_id`, `quantity`, `cost_price`) VALUES
(113, 0, 0, 30, '0.0000'),
(114, 0, 0, 30, '0.0000'),
(112, 0, 0, 30, '0.0000'),
(115, 0, 0, 30, '0.0000');