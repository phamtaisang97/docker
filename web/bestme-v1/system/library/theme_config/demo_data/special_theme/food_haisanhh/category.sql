-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(39, '', 0, 0, 0, 0, 1, '2020-12-21 17:29:46', '2020-12-21 17:29:46'),
(38, '', 0, 0, 0, 0, 1, '2020-12-21 17:04:05', '2020-12-21 17:04:05'),
(37, '', 0, 0, 0, 0, 1, '2020-12-21 17:03:56', '2020-12-21 17:03:56'),
(35, '', 0, 0, 0, 0, 1, '2020-12-21 17:03:18', '2020-12-21 17:03:18'),
(34, '', 0, 0, 0, 0, 1, '2020-12-21 17:03:04', '2020-12-21 17:03:04'),
(36, '', 0, 0, 0, 0, 1, '2020-12-21 17:03:32', '2020-12-21 17:03:32'),
(32, '', 0, 0, 0, 0, 1, '2020-12-21 17:02:33', '2020-12-21 17:03:45'),
(33, '', 0, 0, 0, 0, 1, '2020-12-21 17:02:40', '2020-12-21 17:02:40');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(39, 2, 'Hàu - Bào ngư', 'Hàu - Bào ngư', '', '', ''),
(39, 1, 'Hàu - Bào ngư', 'Hàu - Bào ngư', '', '', ''),
(38, 1, 'Hải sản nhập khẩu', 'Hải sản nhập khẩu', '', '', ''),
(38, 2, 'Hải sản nhập khẩu', 'Hải sản nhập khẩu', '', '', ''),
(32, 1, 'Tôm các loại', 'Tôm các loại', '', '', ''),
(33, 2, 'Cua - Ghẹ', 'Cua - Ghẹ', '', '', ''),
(33, 1, 'Cua - Ghẹ', 'Cua - Ghẹ', '', '', ''),
(34, 2, 'Nghêu - Sò - Ốc', 'Nghêu - Sò - Ốc', '', '', ''),
(34, 1, 'Nghêu - Sò - Ốc', 'Nghêu - Sò - Ốc', '', '', ''),
(35, 2, 'Bạch Tuộc - Mực', 'Bạch Tuộc - Mực', '', '', ''),
(35, 1, 'Bạch Tuộc - Mực', 'Bạch Tuộc - Mực', '', '', ''),
(36, 2, 'Cá các loại', 'Cá các loại', '', '', ''),
(36, 1, 'Cá các loại', 'Cá các loại', '', '', ''),
(37, 2, 'Hải sản nội địa', 'Hải sản nội địa', '', '', ''),
(37, 1, 'Hải sản nội địa', 'Hải sản nội địa', '', '', ''),
(32, 2, 'Tôm các loại', 'Tôm các loại', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(34, 34, 0),
(33, 33, 0),
(36, 36, 0),
(35, 35, 0),
(32, 32, 0),
(37, 37, 0),
(39, 39, 0),
(38, 38, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(39, 0);