-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(12, 'Tôm các loại', 0, 1, '', '0'),
(11, 'Cua - Ghẹ', 0, 1, '', '0'),
(10, 'Hàu - Bào ngư', 0, 1, '', '0'),
(9, 'Nghêu - Sò - Ốc', 0, 1, '', '0'),
(13, 'Đồ biển khô', 0, 1, '', '0'),
(14, 'Các loại cá', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, '', '', '', '', ''),
(10, '', '', '', '', ''),
(11, '', '', '', '', ''),
(12, '', '', '', '', ''),
(13, '', '', '', '', ''),
(14, '', '', '', '', '');