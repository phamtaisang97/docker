--
-- create new demo data
--
CREATE TABLE IF NOT EXISTS `oc_demo_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_id` int(11) NOT NULL,
  `table_name` varchar(255) DEFAULT '',
  `theme_group` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- create new demo data
--

INSERT IGNORE INTO `oc_demo_data` (`data_id`, `table_name`, `theme_group`) VALUES
-- product table
(83, 'product', 'realestate_sbuilding'),
(84, 'product', 'realestate_sbuilding'),
(85, 'product', 'realestate_sbuilding'),
(86, 'product', 'realestate_sbuilding'),
(87, 'product', 'realestate_sbuilding'),
(88, 'product', 'realestate_sbuilding'),
(97, 'product', 'realestate_sbuilding'),
(98, 'product', 'realestate_sbuilding'),
(99, 'product', 'realestate_sbuilding'),
(103, 'product', 'realestate_sbuilding'),
(104, 'product', 'realestate_sbuilding'),
(105, 'product', 'realestate_sbuilding'),
(106, 'product', 'realestate_sbuilding'),
(107, 'product', 'realestate_sbuilding'),
(108, 'product', 'realestate_sbuilding'),
-- category table
(20, 'category', 'realestate_sbuilding'),
(19, 'category', 'realestate_sbuilding'),
(18, 'category', 'realestate_sbuilding'),
(17, 'category', 'realestate_sbuilding'),
(14, 'category', 'realestate_sbuilding'),
(12, 'category', 'realestate_sbuilding'),
(21, 'category', 'realestate_sbuilding'),
(22, 'category', 'realestate_sbuilding'),
-- collection table
(10, 'collection', 'realestate_sbuilding'),
(8, 'collection', 'realestate_sbuilding'),
(9, 'collection', 'realestate_sbuilding'),
(11, 'collection', 'realestate_sbuilding'),
(12, 'collection', 'realestate_sbuilding'),
(13, 'collection', 'realestate_sbuilding'),
(14, 'collection', 'realestate_sbuilding'),
(15, 'collection', 'realestate_sbuilding'),
(16, 'collection', 'realestate_sbuilding');
-- seo_url table: auto deleted when deleting product, category, collection

-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(20, NULL, 0, 0, 0, 0, 1, '2019-05-21 16:20:38', '2019-05-21 16:20:38'),
(19, NULL, 0, 0, 0, 0, 1, '2019-05-21 15:47:12', '2019-05-21 15:47:12'),
(18, NULL, 0, 0, 0, 0, 1, '2019-05-21 15:44:23', '2019-05-21 15:44:23'),
(17, NULL, 0, 0, 0, 0, 1, '2019-05-21 15:36:21', '2019-05-21 15:36:21'),
(14, NULL, 0, 0, 0, 0, 1, '2019-05-20 17:45:50', '2019-05-20 17:45:50'),
(12, NULL, 0, 0, 0, 0, 1, '2019-05-20 16:48:18', '2019-05-20 16:48:18'),
(21, NULL, 0, 0, 0, 0, 1, '2020-05-22 14:17:29', '2020-05-22 14:17:29'),
(22, NULL, 0, 0, 0, 0, 1, '2020-05-22 16:45:11', '2020-05-22 16:45:11');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(18, 2, 'Giày sandal', 'Giày sandal', '', '', ''),
(18, 1, 'Giày sandal', 'Giày sandal', '', '', ''),
(17, 2, 'Giày valen', 'Giày valen', '', '', ''),
(17, 1, 'Giày valen', 'Giày valen', '', '', ''),
(14, 1, 'Giày cao gót', 'Giày cao gót', '', '', ''),
(14, 2, 'Giày cao gót', 'Giày cao gót', '', '', ''),
(12, 1, 'Giày cổ điển', 'Giày cổ điển', '', '', ''),
(12, 2, 'Giày cổ điển', 'Giày cổ điển', '', '', ''),
(19, 2, 'Dép sandal', 'Dép sandal', '', '', ''),
(19, 1, 'Dép sandal', 'Dép sandal', '', '', ''),
(20, 2, 'Giày đúp', 'Giày đúp', '', '', ''),
(20, 1, 'Giày đúp', 'Giày đúp', '', '', ''),
(21, 2, 'Dự Án Nổi Bật', 'Dự Án Nổi Bật', '', '', ''),
(21, 1, 'Dự Án Nổi Bật', 'Dự Án Nổi Bật', '', '', ''),
(22, 2, 'Tin Rao Dành Cho Bạn', 'Tin Rao Dành Cho Bạn', '', '', ''),
(22, 1, 'Tin Rao Dành Cho Bạn', 'Tin Rao Dành Cho Bạn', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(20, 20, 0),
(19, 19, 0),
(18, 18, 0),
(17, 17, 0),
(14, 14, 0),
(12, 12, 0),
(21, 21, 0),
(22, 22, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(12, 0),
(14, 0),
(17, 0),
(18, 0),
(19, 0),
(20, 0),
(21, 0),
(22, 0);

-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(10, 'Bất Động Sản Nổi Bật Cần Bán', 0, 1, '', '0'),
(8, 'Dự Án Nối Bật', 0, 1, '', '0'),
(9, 'Tin Rao Bât Động Sản', 0, 1, '', '0'),
(11, 'Căn hộ S1', 0, 1, '', '0'),
(12, 'Căn hộ S2', 0, 1, '', '0'),
(13, 'Căn hộ S3', 0, 1, '', '0'),
(14, 'Căn hộ S4', 0, 1, '', '0'),
(15, 'x1', 0, 1, '', '1'),
(16, 'x2', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(13, 'https://res.cloudinary.com/novaonx2/image/upload/v1590381063/21639/bst3.png', '&lt;p&gt;2&lt;/p&gt;', '', '', ''),
(14, 'https://res.cloudinary.com/novaonx2/image/upload/v1590381066/21639/bst4.png', '&lt;p&gt;d&lt;/p&gt;', '', '', ''),
(10, '', '', '', '', ''),
(11, 'https://res.cloudinary.com/novaonx2/image/upload/v1590381058/21639/bst1.png', '&lt;p&gt;s1&lt;/p&gt;', '', '', ''),
(12, 'https://res.cloudinary.com/novaonx2/image/upload/v1590381060/21639/bst2.png', '&lt;p&gt;s&lt;/p&gt;', '', '', ''),
(8, 'https://res.cloudinary.com/novaonx2/image/upload/v1590131660/21639/tai-xuong-7.jpg', '', '', '', ''),
(9, 'https://res.cloudinary.com/novaonx2/image/upload/v1590131658/21639/tai-xuong-6.jpg', '', '', '', ''),
(15, '', '', '', '', ''),
(16, '', '', '', '', '');

-- oc_product PRIMARY KEY (`product_id`)
INSERT IGNORE INTO `oc_product` (`product_id`, `model`, `sku`, `common_barcode`, `common_sku`, `common_compare_price`, `common_price`, `common_cost_price`, `barcode`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `sale_on_out_of_stock`, `stock_status_id`, `image`, `image_alt`, `multi_versions`, `manufacturer_id`, `shipping`, `price`, `price_currency_id`, `compare_price`, `c_price_currency_id`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `channel`, `viewed`, `demo`, `deleted`, `default_store_id`, `date_added`, `date_modified`) VALUES
(83, '', '', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 350, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/83/thumb.jpg', '', 0, 1, 1, '0.0000', 1, '204000.0000', 1, 0, 0, '0000-00-00', '208.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, '1', 0, '2019-05-20 16:03:08', '2019-05-20 16:03:08'),
(84, '', '', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 320, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/84/thumb.jpg', '', 0, 6, 1, '0.0000', 1, '200000.0000', 1, 0, 0, '0000-00-00', '208.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, '1', 0, '2019-05-20 16:07:02', '2019-05-20 16:07:02'),
(85, '', 'HUKW3943JJJ', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 316, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/85/thumb.jpg', '', 0, 1, 1, '210000.0000', 1, '360000.0000', 1, 0, 0, '0000-00-00', '325.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, '1', 0, '2019-05-20 16:12:44', '2019-05-20 16:12:44'),
(86, '', 'HUKW3943888', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 400, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/86/thumb.jpg', '', 0, 1, 1, '150000.0000', 1, '200000.0000', 1, 0, 0, '0000-00-00', '325.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, '1', 0, '2019-05-20 16:22:09', '2019-05-20 16:22:09'),
(87, '', 'MREE2SA', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 350, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/87/thumb.jpg', '', 0, 1, 1, '130000.0000', 1, '220000.0000', 1, 0, 0, '0000-00-00', '2500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, '1', 0, '2019-05-20 16:35:06', '2019-05-20 16:35:06'),
(88, '', 'S510UN', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 345, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/88/thumb.jpg', '', 0, 8, 1, '200000.0000', 1, '320000.0000', 1, 0, 0, '0000-00-00', '2200.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, '1', 0, '2019-05-20 16:38:08', '2019-05-20 16:38:08'),
(97, '', '14IKB', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 325, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/97/thumb.jpg', '', 0, 7, 1, '199000.0000', 1, '360000.0000', 1, 0, 0, '0000-00-00', '2100.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, '1', 0, '2019-05-20 16:40:29', '2019-05-20 16:40:29'),
(98, '', 'SKU215', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 420, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/98/thumb.jpg', '', 0, 6, 1, '230000.0000', 1, '390000.0000', 1, 0, 0, '0000-00-00', '750.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, '1', 0, '2019-05-21 15:57:29', '2019-05-21 15:57:29'),
(99, '', 'SKU1999', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 350, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/99/thumb.jpg', '', 0, 2, 1, '190000.0000', 1, '340000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, '1', 0, '2019-05-21 16:03:41', '2019-05-21 16:03:41'),
(103, '', 'SKU2991', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 400, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/103/thumb.jpg', '', 0, 25, 1, '0.0000', 1, '170000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, '1', 0, '2019-05-21 16:13:09', '2019-05-21 16:13:09'),
(104, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1590131661/21639/tai-xuong.jpg', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '1.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 0, NULL, 0, '2020-05-22 14:17:29', '2020-05-22 14:17:29'),
(105, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1590131656/21639/tai-xuong-4.jpg', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '1.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 0, NULL, 0, '2020-05-22 16:35:39', '2020-05-22 16:35:39'),
(106, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1590131653/21639/tai-xuong-2.jpg', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '10.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 0, NULL, 0, '2020-05-22 16:36:37', '2020-05-22 16:36:37'),
(107, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1590131658/21639/tai-xuong-6.jpg', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '1.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 0, NULL, 0, '2020-05-22 16:37:34', '2020-05-22 16:37:34'),
(108, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1590140378/21639/20190507115455-76ecwm.jpg', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '1.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 0, NULL, 0, '2020-05-22 16:45:11', '2020-05-22 16:45:11');

-- restore soft-deleted products if demo
UPDATE `oc_product` SET `deleted` = NULL
WHERE `demo` = 1
  AND `product_id` IN (
      83,
      84,
      85,
      86,
      87,
      88,
      97,
      98,
      99,
      103,
      104,
      105,
      106,
      107,
      108
  );

-- oc_product_collection PRIMARY KEY (`product_collection_id`)
INSERT IGNORE INTO `oc_product_collection` (`product_collection_id`, `product_id`, `collection_id`, `sort_order`) VALUES
(150, 106, 14, 0),
(149, 106, 13, 0),
(148, 106, 12, 0),
(147, 108, 11, 0),
(146, 107, 10, 0),
(145, 105, 10, 0),
(144, 104, 10, 0),
(143, 106, 10, 0),
(142, 104, 9, 0),
(141, 105, 9, 0),
(140, 106, 9, 0),
(139, 108, 9, 0),
(138, 107, 8, 0),
(137, 105, 8, 0),
(136, 104, 8, 0),
(135, 106, 8, 0);

-- oc_product_description PRIMARY KEY (`product_id`,`language_id`)
INSERT IGNORE INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `sub_description`, `seo_title`, `seo_description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(83, 2, 'Giày cổ điển sang trọng màu hồng', 'Giày cổ điển sang trọng màu hồng sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(84, 2, 'Giày cao gót gót kim sa xù độc lạ', '- Cao 8cm - Màu đen và nâu cafe sang trọng - Là kiểu giày mang phong cách cổ điển sang trọng, dễ phối đồ. Đi tiệc hay đi làm đều rất sang. - Size 35-39', '', '', '', '', '', '', ''),
(85, 2, 'Giày cao gót quai trong phối đinh', 'Giày cao gót quai trong phối đinh sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(86, 2, 'Giày cao gót dép đen nơ', 'Giày cao gót dép đen nơ sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(87, 2, 'Giày valen viền đinh size 36 37 38', 'Giày valen viền đinh size 36 37 38 sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(88, 2, 'Giày sandal cao gót quai trong gót kim sa size 34 đến 40', 'Giày sandal cao gót quai trong gót kim sa size 34 đến 40 sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(97, 2, 'Giày sandal quai ngang ánh 7 màu', 'Giày sandal quai ngang ánh 7 màu sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(98, 2, 'Dép sandal xoắn cổ chân', 'Dép sandal xoắn cổ chân sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(99, 2, 'Giày sandal chiến binh dây kéo 1 bên', 'Giày sandal chiến binh dây kéo 1 bên sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(103, 2, 'Giày đúp cao 11cm', 'Giày đúp cao 11cm sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(104, 2, 'Maris city', '&lt;p&gt;Tên dự án&lt;/p&gt;\r\n\r\n&lt;p&gt;Maris City&lt;/p&gt;\r\n\r\n&lt;p&gt;Địa chỉ&lt;/p&gt;\r\n\r\n&lt;p&gt;Đường Nguyễn Tri Phương, Phường Chánh Lộ, Quảng Ngãi, Quảng Ngãi&lt;/p&gt;\r\n\r\n&lt;p&gt;Tổng diện tích&lt;/p&gt;\r\n\r\n&lt;p&gt;96.000 m²&lt;/p&gt;\r\n\r\n&lt;p&gt;Loại hình phát triển&lt;/p&gt;\r\n\r\n&lt;p&gt;Dự án khác&lt;/p&gt;', '', '', '', '', '', '', ''),
(105, 2, 'MỸ KHÊ ANGKORA PARK', '&lt;p&gt;Tên dự án&lt;/p&gt;\r\n\r\n&lt;p&gt;Mỹ Khê Angkora Park&lt;/p&gt;\r\n\r\n&lt;p&gt;Địa chỉ&lt;/p&gt;\r\n\r\n&lt;p&gt;Đường Mỹ Trà Mỹ Khê, Xã Tịnh Khê, Quảng Ngãi, Quảng Ngãi&lt;/p&gt;\r\n\r\n&lt;p&gt;Tổng diện tích&lt;/p&gt;\r\n\r\n&lt;p&gt;122.000 m²&lt;/p&gt;\r\n\r\n&lt;p&gt;Loại hình phát triển&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu đô thị mới&lt;/p&gt;', '&lt;p&gt;Tên dự án&lt;/p&gt;\r\n\r\n&lt;p&gt;Mỹ Khê Angkora Park&lt;/p&gt;\r\n\r\n&lt;p&gt;Địa chỉ&lt;/p&gt;\r\n\r\n&lt;p&gt;Đường Mỹ Trà Mỹ Khê, Xã Tịnh Khê, Quảng Ngãi, Quảng Ngãi&lt;/p&gt;\r\n\r\n&lt;p&gt;Tổng diện tích&lt;/p&gt;\r\n\r\n&lt;p&gt;122.000 m²&lt;/p&gt;\r\n\r\n&lt;p&gt;Loại hình phát triển&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu đô thị mới&lt;/p&gt;', '', '', '', '', '', ''),
(106, 2, 'KHU ĐÔ THỊ MỚI PHÚ CƯỜNG', '&lt;p&gt;Tên dự án&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu Đô Thị Mới Phú Cường&lt;/p&gt;\r\n\r\n&lt;p&gt;Chủ đầu tư&lt;/p&gt;\r\n\r\n&lt;p&gt;CÔNG TY CPĐT PHÚ CƯỜNG KIÊN GIANG&lt;/p&gt;\r\n\r\n&lt;p&gt;Địa chỉ&lt;/p&gt;\r\n\r\n&lt;p&gt;TP.Rạch Giá, Kiên Giang&lt;/p&gt;\r\n\r\n&lt;p&gt;Tổng diện tích&lt;/p&gt;\r\n\r\n&lt;p&gt;1.660.000 m²&lt;/p&gt;\r\n\r\n&lt;p&gt;Loại hình phát triển&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu đô thị mới&lt;/p&gt;', '', '', '', '', '', '', ''),
(107, 2, 'Phú Điền Residences', '&lt;p&gt;Tên dự án&lt;/p&gt;\r\n\r\n&lt;p&gt;Phú Điền Residences&lt;/p&gt;\r\n\r\n&lt;p&gt;Địa chỉ&lt;/p&gt;\r\n\r\n&lt;p&gt;ĐT624, Xã Nghĩa Điền, Tư Nghĩa, Quảng Ngãi&lt;/p&gt;\r\n\r\n&lt;p&gt;Tổng diện tích&lt;/p&gt;\r\n\r\n&lt;p&gt;122.000 m²&lt;/p&gt;\r\n\r\n&lt;p&gt;Loại hình phát triển&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu đô thị mới&lt;/p&gt;\r\n\r\n&lt;p&gt;Quy mô dự án&lt;/p&gt;\r\n\r\n&lt;p&gt;409 lô biệt thự, liền kề&lt;/p&gt;', '', '', '', '', '', '', ''),
(108, 2, 'Chuyển nhượng gấp 9 căn thuộc chung cư An Bình', '&lt;p&gt;Tháp A8:&lt;br /&gt;\r\n- Căn 5 tháp A8, loại căn hộ A1, diện tích 74,7m2, căn 2 phòng ngủ đều có ánh sáng và gió tự nhiên, cửa hướng Nam, ban công hướng Bắc view khu biệt thự và hồ điều hòa.&lt;br /&gt;\r\nGiá bán: 2,4 tỷ/căn.&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Căn 11 tháp A8, loại căn hộ B5, diện tích 86,5m2, cửa chính Đông ban công chính Tây và chính Nam view công viên và hồ điều hòa 16ha, thiết kế thông minh tất cả các phòng đều có ánh sáng và gió tự nhiên.&lt;br /&gt;\r\nGiá bán: Tổng giá trị căn hộ 2,7tỷ.&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Căn 01 tháp A8, loại căn hộ C1, diện tích 114,5m2, cửa vào chính Nam, ban công chính Bắc và chính Tây, view hồ điều hòa và khu biệt thự tuyệt đẹp.&lt;br /&gt;\r\nGiá bán: Căn hộ 3 tỷ.&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Căn 03 tháp A8, loại căn hộ B1, diện tích 90,6m2, cửa vào chính Nam, ban công chính Bắc, view hồ, khu biệt thự và chung cư Green Stars.&lt;br /&gt;\r\nGiá bán: 2,85 tỷ/căn.&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Căn 10 tháp A8, loại căn hộ B4, diện tích 83,7m2, ban công chính Đông và Nam, cửa vào chính Bắc, view quảng trường và bể bơi rất đẹp.&lt;br /&gt;\r\nGiá bán: 2,9 tỷ.&lt;br /&gt;\r\n&lt;br /&gt;\r\nTháp A7:&lt;br /&gt;\r\n- Căn 5 tháp A7, loại căn hộ A1, diện tích 74,7m2, căn 2 phòng ngủ đều có ánh sáng và gió tự nhiên, cửa hướng Bắc, ban công chính Nam view quảng trường tuyệt đẹp.&lt;br /&gt;\r\nGiá bán: 2.5 tỷ/căn.&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Căn 02 tháp A7, loại căn hộ C2, diện tích 114,5m2, căn góc, cửa vào chính Nam, ban công chính Tây và Bắc, view hồ và công viên cây xanh đẹp.&lt;br /&gt;\r\nGiá bán: 3 tỷ.&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Căn 09 tháp A7, loại căn hộ B4, diện tích 83,7m2, căn góc, ban công chính Đông và Nam, cửa vào chính Bắc, view 2 phía quảng trường và bể bơi.&lt;br /&gt;\r\nGiá bán: 2,75 tỷ.&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Căn 04 tháp A7, loại căn hộ B2, diện tích 90,6m2, ban công chính Bắc, cửa vào chính Nam, vị trí view vườn hoa và hồ điều hòa.&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Căn 12 tháp A7, loại căn hộ B6, diện tích 86,5m2, ban công chính Đông và Bắc, cửa vào chính Tây, view bể bơi và quảng trường.&lt;br /&gt;\r\nNgoài ra tôi còn nhiều căn khác với giá bán hợp lý.&lt;br /&gt;\r\n&lt;br /&gt;\r\n2. Tiến độ thanh toán:&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Đợt 1: 95% nhận ngay nhà mới.&lt;br /&gt;\r\n- Đợt 2: 5% khi cấp giấy chứng nhận quyền sử dụng đất.&lt;br /&gt;\r\n&lt;br /&gt;\r\n3. Điểm nổi bật dự án:&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Thiết kế căn hộ thông minh, diện tích hợp lý tất cả các phòng đều có ánh sáng và đón gió tự nhiên.&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Cư dân sống nơi đây sẽ được sử dụng một quần thể rộng với kiến trúc hiện đại theo đó là không gian sống trong lành.&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Dịch vụ hoàn hảo có bể bơi bốn mùa, khu tập gym, spa... Và trung tâm thương mại đảm bảo cho cư dân nơi đây có một cuộc sống hiện đại.&lt;br /&gt;\r\n &lt;/p&gt;', '', '', '', '', '', '', '');

-- oc_product_image PRIMARY KEY (`product_image_id`)
INSERT IGNORE INTO `oc_product_image` (`product_image_id`, `product_id`, `image`, `image_alt`, `sort_order`) VALUES
(139, 83, '/catalog/view/theme/default/image/fashion_shoeszone/products/83/image1.jpg', '', 1),
(138, 84, '/catalog/view/theme/default/image/fashion_shoeszone/products/84/thumb.jpg', '', 0),
(133, 85, '/catalog/view/theme/default/image/fashion_shoeszone/products/85/image1.jpg', '', 0),
(132, 86, '/catalog/view/theme/default/image/fashion_shoeszone/products/86/thumb.jpg', '', 1),
(131, 87, '/catalog/view/theme/default/image/fashion_shoeszone/products/87/image1.jpg', '', 0),
(117, 88, '/catalog/view/theme/default/image/fashion_shoeszone/products/88/image1.jpg', '', 0),
(118, 97, '/catalog/view/theme/default/image/fashion_shoeszone/products/97/thumb.jpg', '', 0),
(119, 98, '/catalog/view/theme/default/image/fashion_shoeszone/products/98/image2.jpg', '', 1),
(126, 99, '/catalog/view/theme/default/image/fashion_shoeszone/products/99/image1.jpg', '', 0),
(127, 103, '/catalog/view/theme/default/image/fashion_shoeszone/products/103/image1.jpg', '', 1);

-- oc_product_to_category PRIMARY KEY (`product_id`,`category_id`)
INSERT IGNORE INTO `oc_product_to_category` (`product_id`, `category_id`) VALUES
(104, 21),
(105, 21),
(106, 21),
(107, 21),
(108, 22);

-- oc_product_to_store PRIMARY KEY (`product_id`,`store_id`)
INSERT IGNORE INTO `oc_product_to_store` (`product_id`, `store_id`, `product_version_id`, `quantity`, `cost_price`) VALUES
(108, 0, 0, 0, '0.0000'),
(107, 0, 0, 0, '0.0000'),
(106, 0, 0, 0, '0.0000'),
(105, 0, 0, 0, '0.0000'),
(104, 0, 0, 0, '0.0000');


-- oc_seo_url PRIMARY KEY (`seo_url_id`)
INSERT IGNORE INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`, `table_name`) VALUES
(2083, 0, 2, 'category_id=12', 'd-cat-giay-co-dien', 'category'),
(2142, 0, 2, 'collection=16', 'x2', 'collection'),
(2140, 0, 2, 'collection=14', 'can-ho-s4', 'collection'),
(2141, 0, 2, 'collection=15', 'x1', 'collection'),
(2139, 0, 2, 'collection=13', 'can-ho-s3', 'collection'),
(2138, 0, 2, 'collection=12', 'can-ho-s2', 'collection'),
(2137, 0, 2, 'collection=11', 'can-ho-s1', 'collection'),
(2133, 0, 1, 'product_id=108', 'chuyen-nhuong-gap-9-can-thuoc-chung-cu-an-binh', 'product'),
(2131, 0, 1, 'category_id=22', 'tin-rao-danh-cho-ban', 'category'),
(2132, 0, 2, 'product_id=108', 'chuyen-nhuong-gap-9-can-thuoc-chung-cu-an-binh', 'product'),
(2130, 0, 2, 'category_id=22', 'tin-rao-danh-cho-ban', 'category'),
(2128, 0, 2, 'product_id=107', 'phu-dien-residences', 'product'),
(2129, 0, 1, 'product_id=107', 'phu-dien-residences', 'product'),
(2127, 0, 1, 'product_id=106', 'khu-do-thi-moi-phu-cuong', 'product'),
(2126, 0, 2, 'product_id=106', 'khu-do-thi-moi-phu-cuong', 'product'),
(2125, 0, 1, 'product_id=105', 'my-khe-angkora-park', 'product'),
(2123, 0, 1, 'product_id=104', 'maris-city', 'product'),
(2124, 0, 2, 'product_id=105', 'my-khe-angkora-park', 'product'),
(2121, 0, 1, 'category_id=21', 'du-an-noi-bat', 'category'),
(2122, 0, 2, 'product_id=104', 'maris-city', 'product'),
(2120, 0, 2, 'category_id=21', 'du-an-noi-bat', 'category'),
(2119, 0, 2, 'blog=3', 'su-kien-duoc-quan-tam-nhat-bat-dong-san-viet-nam', 'blog'),
(2117, 0, 2, 'blog=1', 'bao-chi-noi-ve-bat-dong-san', 'blog'),
(2118, 0, 2, 'blog=2', 'bat-dong-san-va-nhung-cai-tien-cong-nghe', 'blog'),
(1000, 0, 1, 'page_id=1000', 'chinh-sach-va-bao-mat', ''),
(1001, 0, 2, 'page_id=1000', 'chinh-sach-va-bao-mat', ''),
(2093, 0, 1, 'common/home', 'home', ''),
(2094, 0, 2, 'common/home', 'trang-chu', ''),
(2095, 0, 1, 'common/shop', 'products', ''),
(2096, 0, 2, 'common/shop', 'san-pham', ''),
(2097, 0, 1, 'contact/contact', 'contact', ''),
(2098, 0, 2, 'contact/contact', 'lien-he', ''),
(2099, 0, 1, 'checkout/profile', 'profile', ''),
(2100, 0, 2, 'checkout/profile', 'tai-khoan', ''),
(2101, 0, 1, 'account/login', 'login', ''),
(2102, 0, 2, 'account/login', 'dang-nhap', ''),
(2103, 0, 1, 'account/register', 'register', ''),
(2104, 0, 2, 'account/register', 'dang-ky', ''),
(2105, 0, 1, 'account/logout', 'logout', ''),
(2106, 0, 2, 'account/logout', 'dang-xuat', ''),
(2107, 0, 1, 'checkout/setting', 'setting', ''),
(2108, 0, 2, 'checkout/setting', 'cai-dat', ''),
(2109, 0, 1, 'checkout/my_orders', 'checkout-cart', ''),
(2110, 0, 2, 'checkout/my_orders', 'gio-hang', ''),
(2111, 0, 1, 'checkout/order_preview', 'payment', ''),
(2112, 0, 2, 'checkout/order_preview', 'thanh-toan', ''),
(2113, 0, 1, 'checkout/order_preview/orderSuccess', 'payment-success', ''),
(2114, 0, 2, 'checkout/order_preview/orderSuccess', 'dat-hang-thanh-cong', ''),
(2115, 0, 1, 'blog/blog', 'blog', ''),
(2116, 0, 2, 'blog/blog', 'bai-viet', ''),
(2084, 0, 2, 'category_id=14', 'd-cat-giay-cao-got', 'category'),
(2085, 0, 2, 'category_id=17', 'd-cat-giay-valen', 'category'),
(2086, 0, 2, 'category_id=18', 'd-cat-giay-sandal', 'category'),
(2087, 0, 2, 'category_id=19', 'd-cat-dep-sandal', 'category'),
(2088, 0, 2, 'category_id=20', 'd-cat-giay-dup', 'category'),
(2134, 0, 2, 'collection=8', 'du-an-noi-bat-1', 'collection'),
(2135, 0, 2, 'collection=9', 'tin-rao-bat-dong-san', 'collection'),
(2136, 0, 2, 'collection=10', 'bat-dong-san-noi-bat-can-ban', 'collection');

-- oc_theme_builder_config NO PRIMARY KEY
-- remove first
DELETE FROM `oc_theme_builder_config`
WHERE `store_id` = '0'
  AND `config_theme` = 'realestate_sbuilding'
  AND `code` = 'config';

-- insert
INSERT IGNORE INTO `oc_theme_builder_config` (`store_id`, `config_theme`, `code`, `key`, `config`, `date_added`, `date_modified`) VALUES
(0, 'realestate_sbuilding', 'config', 'config_theme_color', '{\n    \"main\": {\n        \"main\": {\n            \"hex\": \"1766B0\",\n            \"visible\": \"1\"\n        },\n        \"button\": {\n            \"hex\": \"#1766b0\",\n            \"visible\": \"1\"\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-26 11:29:44'),
(0, 'realestate_sbuilding', 'config', 'config_theme_text', '{\n    \"all\": {\n        \"title\": \"Font Tahoma\",\n        \"font\": \"Roboto\",\n        \"supported-fonts\": [\n            \"Open Sans\",\n            \"IBM Plex Sans\",\n            \"Courier New\",\n            \"Roboto\",\n            \"Nunito\",\n            \"Arial\",\n            \"DejaVu Sans\",\n            \"Tahoma\",\n            \"Time News Roman\"\n        ]\n    },\n    \"heading\": {\n        \"font\": \"Tahoma\",\n        \"supported-fonts\": [\n            \"Open Sans\",\n            \"IBM Plex Sans\",\n            \"Courier New\",\n            \"Roboto\",\n            \"Nunito\",\n            \"Arial\",\n            \"DejaVu Sans\",\n            \"Tahoma\",\n            \"Time News Roman\"\n        ],\n        \"font-type\": \"regular\",\n        \"supported-font-types\": [\n            \"regular\",\n            \"bold\",\n            \"italic\"\n        ],\n        \"font-size\": \"32\"\n    },\n    \"body\": {\n        \"font\": \"Aria\",\n        \"supported-fonts\": [\n            \"Open Sans\",\n            \"IBM Plex Sans\",\n            \"Courier New\",\n            \"Roboto\",\n            \"Nunito\",\n            \"Arial\",\n            \"DejaVu Sans\",\n            \"Tahoma\",\n            \"Time News Roman\"\n        ],\n        \"font-type\": \"regular\",\n        \"supported-font-types\": [\n            \"regular\",\n            \"bold\",\n            \"italic\"\n        ],\n        \"font-size\": \"14\"\n    }\n}', '2020-05-20 15:38:57', '2020-05-26 11:29:44'),
(0, 'realestate_sbuilding', 'config', 'config_theme_favicon', '{\n    \"image\": {\n        \"url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1590381232\\/21639\\/logo-01-min-2.png\"\n    }\n}', '2020-05-20 15:38:57', '2020-05-26 11:29:44'),
(0, 'realestate_sbuilding', 'config', 'config_theme_social', '[\n    {\n        \"name\": \"facebook\",\n        \"text\": \"Facebook\",\n        \"url\": \"https:\\/\\/www.facebook.com\"\n    },\n    {\n        \"name\": \"twitter\",\n        \"text\": \"Twitter\",\n        \"url\": \"https:\\/\\/www.twitter.com\"\n    },\n    {\n        \"name\": \"instagram\",\n        \"text\": \"Instagram\",\n        \"url\": \"https:\\/\\/www.instagram.com\"\n    },\n    {\n        \"name\": \"tumblr\",\n        \"text\": \"Tumblr\",\n        \"url\": \"https:\\/\\/www.tumblr.com\"\n    },\n    {\n        \"name\": \"youtube\",\n        \"text\": \"Youtube\",\n        \"url\": \"https:\\/\\/www.youtube.com\"\n    },\n    {\n        \"name\": \"googleplus\",\n        \"text\": \"Google Plus\",\n        \"url\": \"https:\\/\\/www.plus.google.com\"\n    }\n]', '2020-05-20 15:38:57', '2020-05-26 11:29:45'),
(0, 'realestate_sbuilding', 'config', 'config_theme_override_css', '{\n    \"css\": \".block-header .block-header-menu .collapse\\u003Eul.nav \\u003E li \\u003E a {\\n    color: #000;\\n    font-size: 16px;\\n}\\n.block-header .block-header-logo \\u003E a {\\n    padding: 5px 0;\\n}\\n.block-banner-ads {\\n    padding-top: 5px;\\n}\\n.block_service_home .container .block_content {\\n    padding: 20px 10px;\\n}\\n.pt-3, .py-3 {\\n    padding-top: 0.5rem!important;\\n}\\n.mb-4, .my-4 {\\n    margin-bottom: 0.5rem!important;\\n}\\n.product-item {\\n    transition: all 1s ease;\\n    margin-top: 1px;\\n    margin-bottom: 1px;\\n    padding: 2px 2px;\\n}\\n.header-font {\\n    font-size: 25px;\\n    font-weight: 600;\\n}\\n.pt-5, .py-5 {\\n    padding-top: 0.5rem!important;\\n}\\n.owl-product .owl-item {\\n    padding: 0 6px 3px;\\n}\\n.block_service_home {\\n    padding-top: 0px;\\n}\\n.bg-footer {\\n    background-color: #333;\\n    background-image: url(https:\\/\\/2.pik.vn\\/202024fc62da-3199-4c4d-8818-02d570efb0f2.png);\\n}\\n.block-header .block-header-menu ul.nav li:hover \\u003Ea, .block-header .block-header-menu ul.nav li a:hover, .block-header .block-header-menu ul.nav a.active {\\n    color: #1E90FF!important;\\n}\\n.mt-5, .my-5 {\\n    margin-top: 0.5rem!important;\\n}\\n.mb-5, .my-5 {\\n    margin-bottom: 0.5rem!important;\\n}\\n.block-footer a:hover, .block-footer a.active {\\n    color: white;\\n}\\n\"\n}', '2020-05-20 15:38:57', '2020-05-26 11:29:45'),
(0, 'realestate_sbuilding', 'config', 'config_theme_checkout_page', '{\n}', '2020-05-20 15:38:57', '2020-05-20 15:38:57'),
(0, 'realestate_sbuilding', 'config', 'config_section_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"header\": {\n            \"name\": \"header\",\n            \"text\": \"Header\",\n            \"visible\": \"1\"\n        },\n        \"slide-show\": {\n            \"name\": \"slide-show\",\n            \"text\": \"Slideshow\",\n            \"visible\": \"1\"\n        },\n        \"categories\": {\n            \"name\": \"categories\",\n            \"text\": \"Danh mục sản phẩm\",\n            \"visible\": \"1\"\n        },\n        \"hot-deals\": {\n            \"name\": \"hot-deals\",\n            \"text\": \"Sản phẩm khuyến mại\",\n            \"visible\": \"1\"\n        },\n        \"feature-products\": {\n            \"name\": \"feature-products\",\n            \"text\": \"Sản phẩm bán chạy\",\n            \"visible\": \"1\"\n        },\n        \"related-products\": {\n            \"name\": \"related-products\",\n            \"text\": \"Sản phẩm xem nhiều\",\n            \"visible\": \"1\"\n        },\n        \"new-products\": {\n            \"name\": \"new-products\",\n            \"text\": \"Sản phẩm mới\",\n            \"visible\": \"1\"\n        },\n        \"product-detail\": {\n            \"name\": \"product-details\",\n            \"text\": \"Chi tiết sản phẩm\",\n            \"visible\": \"1\"\n        },\n        \"banner\": {\n            \"name\": \"banner\",\n            \"text\": \"Banner\",\n            \"visible\": \"1\"\n        },\n        \"content_customize\": {\n            \"name\": \"content-customize\",\n            \"text\": \"Nội dung tùy chỉnh\",\n            \"visible\": \"1\"\n        },\n        \"partners\": {\n            \"name\": \"partners\",\n            \"text\": \"Đối tác\",\n            \"visible\": \"1\"\n        },\n        \"blog\": {\n            \"name\": \"blog\",\n            \"text\": \"Blog\",\n            \"visible\": \"1\"\n        },\n        \"footer\": {\n            \"name\": \"footer\",\n            \"text\": \"Footer\",\n            \"visible\": \"1\"\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-25 13:23:48'),
(0, 'realestate_sbuilding', 'config', 'config_section_banner', '{\n    \"name\": \"banner\",\n    \"text\": \"Banner\",\n    \"visible\": \"1\",\n    \"display\": [\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1589964223\\/21639\\/banner1.png\",\n            \"url\": \"https:\\/\\/s-building.bestme.asia\\/san-pham\",\n            \"description\": \"Banner 1\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1590391069\\/21639\\/banner22.png\",\n            \"url\": \"https:\\/\\/s-building.bestme.asia\\/san-pham\",\n            \"description\": \"Banner 2\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1590391072\\/21639\\/banner33.png\",\n            \"url\": \"https:\\/\\/s-building.bestme.asia\\/san-pham\",\n            \"description\": \"Banner 3\",\n            \"visible\": \"1\"\n        }\n    ]\n}', '2020-05-20 15:38:57', '2020-05-25 14:18:17'),
(0, 'realestate_sbuilding', 'config', 'config_section_best_sales_product', '{\n    \"name\": \"feature-product\",\n    \"text\": \"Sản phẩm bán chạy\",\n    \"visible\": \"1\",\n    \"setting\": {\n        \"title\": \"Bất Động Sản Nổi Bật Cần Bán\",\n        \"sub_title\": \"Mô tả tiêu đề\",\n        \"auto_retrieve_data\": \"0\",\n        \"collection_id\": \"9\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": \"4\"\n        },\n        \"grid_mobile\": {\n            \"quantity\": \"2\"\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-25 13:24:33'),
(0, 'realestate_sbuilding', 'config', 'config_section_new_product', '{\n    \"name\": \"new-product\",\n    \"text\": \"Sản phẩm mới\",\n    \"visible\": \"1\",\n    \"setting\": {\n        \"title\": \"Tin Rao Bât Động Sản\",\n        \"sub_title\": \"Mô tả tiêu đề\",\n        \"auto_retrieve_data\": \"0\",\n        \"collection_id\": \"16\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": \"2\"\n        },\n        \"grid_mobile\": {\n            \"quantity\": \"2\"\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-25 13:25:28'),
(0, 'realestate_sbuilding', 'config', 'config_section_best_views_product', '{\n    \"name\": \"related-product\",\n    \"text\": \"Sản phẩm xem nhiều\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Sản phẩm xem nhiều\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 10\n        },\n        \"grid_mobile\": {\n            \"quantity\": 2\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-20 15:38:57'),
(0, 'realestate_sbuilding', 'config', 'config_section_blog', '{\n    \"name\": \"blog\",\n    \"text\": \"Blog\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Blog\"\n    },\n    \"display\": {\n        \"menu\": [\n            {\n                \"type\": \"existing\",\n                \"menu\": {\n                    \"id\": 12,\n                    \"name\": \"Danh sách bài viết 1\"\n                }\n            },\n            {\n                \"type\": \"manual\",\n                \"entries\": [\n                    {\n                        \"id\": 10,\n                        \"name\": \"Entry 10\"\n                    },\n                    {\n                        \"id\": 11,\n                        \"name\": \"Entry 11\"\n                    }\n                ]\n            }\n        ],\n        \"grid\": {\n            \"quantity\": 10\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-20 15:38:57'),
(0, 'realestate_sbuilding', 'config', 'config_section_detail_product', '{\n    \"name\": \"product-detail\",\n    \"text\": \"Chi tiết sản phẩm\",\n    \"visible\": 1,\n    \"display\": {\n        \"name\": true,\n        \"description\": false,\n        \"price\": true,\n        \"price-compare\": false,\n        \"status\": false,\n        \"sale\": false,\n        \"rate\": false\n    }\n}', '2020-05-20 15:38:57', '2020-05-20 15:38:57'),
(0, 'realestate_sbuilding', 'config', 'config_section_footer', '{\n    \"name\": \"footer\",\n    \"text\": \"Footer\",\n    \"visible\": \"1\",\n    \"contact\": {\n        \"title\": \"Liên hệ chúng tôi\",\n        \"address\": \"Cau Giay - Ha Noi\",\n        \"phone-number\": \"(+84)987654321\",\n        \"email\": \"dungbt@novaon.vn\",\n        \"visible\": \"1\"\n    },\n    \"collection\": {\n        \"title\": \"Bộ sưu tập\",\n        \"menu_id\": \"1\",\n        \"visible\": \"0\"\n    },\n    \"quick-links\": {\n        \"title\": \"Liên kết nhanh\",\n        \"menu_id\": \"1\",\n        \"visible\": \"1\"\n    },\n    \"subscribe\": {\n        \"title\": \"Đăng ký theo dõi\",\n        \"social_network\": \"1\",\n        \"visible\": \"1\",\n        \"youtube_visible\": \"1\",\n        \"facebook_visible\": \"1\",\n        \"instagram_visible\": \"1\"\n    }\n}', '2020-05-20 15:38:57', '2020-05-25 11:34:50'),
(0, 'realestate_sbuilding', 'config', 'config_section_header', '{\n    \"name\": \"header\",\n    \"text\": \"Header\",\n    \"visible\": \"1\",\n    \"notify-bar\": {\n        \"name\": \"Thanh thông báo\",\n        \"visible\": \"1\",\n        \"content\": \"S-Building - Tận hưởng đẳng cấp thượng lưu\",\n        \"url\": \"https:\\/\\/s-building.bestme.asia\\/san-pham\"\n    },\n    \"logo\": {\n        \"name\": \"Logo\",\n        \"url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1589964230\\/21639\\/logo-01.png\",\n        \"height\": \"\"\n    },\n    \"menu\": {\n        \"name\": \"Menu\",\n        \"display-list\": {\n            \"name\": \"Menu chính\",\n            \"id\": \"1\"\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-25 13:53:04'),
(0, 'realestate_sbuilding', 'config', 'config_section_hot_product', '{\n    \"name\": \"hot-deals\",\n    \"text\": \"Sản phẩm khuyến mại\",\n    \"visible\": \"1\",\n    \"setting\": {\n        \"title\": \"Dự Án Nối Bật\",\n        \"sub_title\": \"Mô tả tiêu đề\",\n        \"auto_retrieve_data\": \"0\",\n        \"collection_id\": \"15\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": \"2\"\n        },\n        \"grid_mobile\": {\n            \"quantity\": \"2\"\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-25 11:33:17'),
(0, 'realestate_sbuilding', 'config', 'config_section_list_product', '{\n    \"name\": \"categories\",\n    \"text\": \"Danh mục sản phẩm\",\n    \"visible\": 0,\n    \"setting\": {\n        \"title\": \"Danh mục sản phẩm\"\n    },\n    \"display\": {\n        \"menu\": {\n            \"id\": 1,\n            \"name\": \"Danh mục sản phẩm 1\"\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-20 15:38:57'),
(0, 'realestate_sbuilding', 'config', 'config_section_partner', '{\n    \"name\": \"partners\",\n    \"text\": \"Đối tác\",\n    \"visible\": \"1\",\n    \"limit-per-line\": \"4\",\n    \"display\": [\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1590381232\\/21639\\/logo-01-min-2.png\",\n            \"url\": \"#\",\n            \"description\": \"Partner 1\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1590381234\\/21639\\/logo-01-min-3.png\",\n            \"url\": \"#\",\n            \"description\": \"Partner 2\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1590381236\\/21639\\/logo-01-min.png\",\n            \"url\": \"#\",\n            \"description\": \"Partner 3\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1590381237\\/21639\\/s.png\",\n            \"url\": \"#\",\n            \"description\": \"Partner 4\",\n            \"visible\": \"1\"\n        }\n    ]\n}', '2020-05-20 15:38:57', '2020-05-25 11:34:27'),
(0, 'realestate_sbuilding', 'config', 'config_section_slideshow', '{\n    \"name\": \"slide-show\",\n    \"text\": \"Slideshow\",\n    \"visible\": \"1\",\n    \"setting\": {\n        \"transition-time\": \"5\"\n    },\n    \"display\": [\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1589965027\\/21639\\/slide1-minpng3.png\",\n            \"url\": \"#\",\n            \"description\": \"slide_1\",\n            \"id\": \"88102120\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1590401299\\/21639\\/slide2-min.png\",\n            \"url\": \"#\",\n            \"description\": \"slide_2\",\n            \"id\": \"1795102\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1589965033\\/21639\\/slide3-minpng1.png\",\n            \"url\": \"#\",\n            \"description\": \"slide_3\",\n            \"id\": \"6262832\"\n        }\n    ]\n}', '2020-05-20 15:38:57', '2020-05-25 17:08:26'),
(0, 'realestate_sbuilding', 'config', 'config_section_product_groups', '{\n    \"name\": \"group products\",\n    \"text\": \"nhóm sản phẩm\",\n    \"visible\": \"1\",\n    \"list\": [\n        {\n            \"text\": \"Danh sách sản phẩm 1\",\n            \"visible\": \"1\",\n            \"setting\": {\n                \"title\": \"Dự Án Nổi Bật\",\n                \"collection_id\": \"8\",\n                \"sub_title\": \"Danh sách sản phẩm 1\"\n            },\n            \"display\": {\n                \"grid\": {\n                    \"quantity\": \"4\"\n                },\n                \"grid_mobile\": {\n                    \"quantity\": \"2\"\n                }\n            }\n        },\n        {\n            \"text\": \"Danh sách sản phẩm\",\n            \"visible\": \"1\",\n            \"setting\": {\n                \"title\": \"Siêu Phẩm 2020\",\n                \"collection_id\": \"15\"\n            },\n            \"display\": {\n                \"grid\": {\n                    \"quantity\": \"4\"\n                },\n                \"grid_mobile\": {\n                    \"quantity\": \"1\"\n                }\n            }\n        },\n        {\n            \"text\": \"Danh sách sản phẩm\",\n            \"visible\": \"1\",\n            \"setting\": {\n                \"title\": \"Căn Hộ Cao Cấp\",\n                \"collection_id\": \"16\"\n            },\n            \"display\": {\n                \"grid\": {\n                    \"quantity\": \"2\"\n                },\n                \"grid_mobile\": {\n                    \"quantity\": \"1\"\n                }\n            }\n        }\n    ]\n}', '2020-05-20 15:38:57', '2020-05-25 15:18:27'),
(0, 'realestate_sbuilding', 'config', 'config_section_content_customize', '{\n    \"name\": \"content-customize\",\n    \"title\": \"Nội dung tuỳ chỉnh\",\n    \"display\": [\n        {\n            \"name\": \"content-1\",\n            \"title\": \"Bảo đảm chất lượng\",\n            \"type\": \"fixed\",\n            \"content\": {\n                \"icon\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1590379957\\/21639\\/quality.png\",\n                \"title\": \"Bảo đảm chất lượng\",\n                \"description\": \"Sản phẩm đảm bảo chất lượng\",\n                \"html\": \"\"\n            }\n        },\n        {\n            \"name\": \"ho-tro-tra-gop-uu-dai-1\",\n            \"title\": \"Hỗ trợ trả góp ưu đãi\",\n            \"type\": \"fixed\",\n            \"content\": {\n                \"icon\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1590379953\\/21639\\/money.png\",\n                \"title\": \"Hỗ trợ trả góp ưu đãi\",\n                \"description\": \"Cho khách hàng cá nhân\",\n                \"html\": \"\"\n            }\n        },\n        {\n            \"name\": \"mo-ban-online-247-2\",\n            \"title\": \"Mở bán online 24\\/7\",\n            \"type\": \"fixed\",\n            \"content\": {\n                \"icon\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1590379955\\/21639\\/time.png\",\n                \"title\": \"Mở bán online 24\\/7\",\n                \"description\": \"Hotline 012.345.678\",\n                \"html\": \"\"\n            }\n        }\n    ]\n}', '2020-05-20 15:38:57', '2020-05-25 11:13:55'),
(0, 'realestate_sbuilding', 'config', 'config_section_category_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"banner\": {\n            \"name\": \"banner\",\n            \"text\": \"Banner\",\n            \"visible\": 1\n        },\n        \"filter\": {\n            \"name\": \"filter\",\n            \"text\": \"Filter\",\n            \"visible\": 1\n        },\n        \"product_category\": {\n            \"name\": \"product_category\",\n            \"text\": \"Product Category\",\n            \"visible\": 1\n        },\n        \"product_list\": {\n            \"name\": \"product_list\",\n            \"text\": \"Product List\",\n            \"visible\": 1\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-20 15:38:57'),
(0, 'realestate_sbuilding', 'config', 'config_section_category_banner', '{\n    \"name\": \"banner\",\n    \"text\": \"Banner\",\n    \"visible\": \"1\",\n    \"display\": [\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1589965033\\/21639\\/slide3-minpng1.png\",\n            \"url\": \"https:\\/\\/s-building.bestme.asia\\/san-pham\",\n            \"description\": \"Banner 1\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"\\/catalog\\/view\\/theme\\/default\\/image\\/banner\\/img-banner-02.png\",\n            \"url\": \"#\",\n            \"description\": \"Banner 2\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"\\/catalog\\/view\\/theme\\/default\\/image\\/banner\\/img-banner-03.png\",\n            \"url\": \"#\",\n            \"description\": \"Banner 3\",\n            \"visible\": \"1\"\n        }\n    ]\n}', '2020-05-20 15:38:57', '2020-05-25 13:53:33'),
(0, 'realestate_sbuilding', 'config', 'config_section_category_filter', '{\n    \"name\": \"filter\",\n    \"text\": \"Bộ lọc\",\n    \"visible\": \"1\",\n    \"setting\": {\n        \"title\": \"Bộ lọc\"\n    },\n    \"display\": {\n        \"supplier\": {\n            \"title\": \"Nhà cung cấp\",\n            \"visible\": \"1\"\n        },\n        \"product-type\": {\n            \"title\": \"Loại sản phẩm\",\n            \"visible\": \"1\"\n        },\n        \"collection\": {\n            \"title\": \"Bộ sưu tập\",\n            \"visible\": \"0\"\n        },\n        \"property\": {\n            \"title\": \"Lọc theo tt - k dung\",\n            \"visible\": \"1\",\n            \"prop\": [\n                \"all\",\n                \"color\",\n                \"weight\",\n                \"size\"\n            ]\n        },\n        \"product-price\": {\n            \"title\": \"Giá sản phẩm\",\n            \"visible\": \"1\",\n            \"range\": {\n                \"from\": \"0\",\n                \"to\": \"100000000\"\n            }\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-25 13:53:53'),
(0, 'realestate_sbuilding', 'config', 'config_section_category_product_category', '{\n    \"name\": \"product-category\",\n    \"text\": \"Danh mục sản phẩm\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Danh mục sản phẩm\"\n    },\n    \"display\": {\n        \"menu\": {\n            \"id\": 1,\n            \"name\": \"Danh mục sản phẩm 1\"\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-20 15:38:57'),
(0, 'realestate_sbuilding', 'config', 'config_section_category_product_list', '{\n    \"name\": \"product-list\",\n    \"text\": \"Danh sách sản phẩm\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Danh sách sản phẩm\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 10\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-20 15:38:57'),
(0, 'realestate_sbuilding', 'config', 'config_section_product_detail_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"related_product\": {\n            \"name\": \"related_product\",\n            \"text\": \"Related Product\",\n            \"visible\": 1\n        },\n        \"template\": {\n            \"name\": \"template\",\n            \"text\": \"Template\",\n            \"visible\": 1\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-20 15:38:57'),
(0, 'realestate_sbuilding', 'config', 'config_section_product_detail_related_product', '{\n    \"name\": \"related-product\",\n    \"text\": \"Sản phẩm liên quan\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Sản phẩm liên quan\",\n        \"auto_retrieve_data\": 0,\n        \"collection_id\": 1\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 4\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-20 15:38:57'),
(0, 'realestate_sbuilding', 'config', 'config_section_product_detail_template', '{\n    \"name\": \"template\",\n    \"text\": \"Giao diện\",\n    \"visible\": 1,\n    \"display\": {\n        \"template\": {\n            \"id\": 10\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-20 15:38:57'),
(0, 'realestate_sbuilding', 'config', 'config_section_blog_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"blog_category\": {\n            \"name\": \"blog_category\",\n            \"text\": \"Blog Category\",\n            \"visible\": 1\n        },\n        \"blog_list\": {\n            \"name\": \"blog_list\",\n            \"text\": \"Blog List\",\n            \"visible\": 1\n        },\n        \"latest_blog\": {\n            \"name\": \"latest_blog\",\n            \"text\": \"Latest Blog\",\n            \"visible\": 1\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-20 15:38:57'),
(0, 'realestate_sbuilding', 'config', 'config_section_blog_blog_category', '{\n    \"name\": \"blog-category\",\n    \"text\": \"Danh mục bài viết\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Danh mục bài viết\"\n    },\n    \"display\": {\n        \"menu\": {\n            \"id\": 1,\n            \"name\": \"Danh mục bài viết 1\"\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-20 15:38:57'),
(0, 'realestate_sbuilding', 'config', 'config_section_blog_blog_list', '{\n    \"name\": \"blog-list\",\n    \"text\": \"Danh sách bài viết\",\n    \"visible\": 1,\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 20\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-20 15:38:57'),
(0, 'realestate_sbuilding', 'config', 'config_section_blog_latest_blog', '{\n    \"name\": \"latest-blog\",\n    \"text\": \"Bài viết mới nhất\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Bài viết mới nhất\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 10\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-20 15:38:57'),
(0, 'realestate_sbuilding', 'config', 'config_section_contact_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"map\": {\n            \"name\": \"map\",\n            \"text\": \"Bản đồ\",\n            \"address\": \"<iframe src=\\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\\" width=\\\"600\\\" height=\\\"450\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\" allowfullscreen></iframe>\",\n            \"visible\": 1\n        },\n        \"info\": {\n            \"name\": \"info\",\n            \"text\": \"Thông tin cửa hàng\",\n            \"email\": \"contact-us@novaon.asia\",\n            \"phone\": \"0000000000\",\n            \"address\": \"address\",\n            \"visible\": 1\n        },\n        \"form\": {\n            \"name\": \"form\",\n            \"title\": \"Form liên hệ\",\n            \"email\": \"email@mail.com\",\n            \"visible\": 1\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-20 15:38:57'),
(0, 'realestate_sbuilding', 'config', 'config_section_contact_map', '{   //////// CÁI NÀY KHÔNG DÙNG NỮA\n    \"name\": \"map\",\n    \"text\": \"Bản đồ\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Bản đồ\"\n    },\n    \"display\": {\n        \"address\": \"<iframe src=\\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\\" width=\\\"600\\\" height=\\\"450\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\" allowfullscreen></iframe>\"\n    }\n}', '2020-05-20 15:38:57', '2020-05-20 15:38:57'),
(0, 'realestate_sbuilding', 'config', 'config_section_contact_contact', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\n    \"name\": \"contact\",\n    \"text\": \"Liên hệ\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Vị trí của chúng tôi\"\n    },\n    \"display\": {\n        \"store\": {\n            \"title\": \"Gian hàng\",\n            \"content\": \"Số 12 Dịch Vọng Hậu, Cầu Giấy, Hà Nội\",\n            \"visible\": 1\n        },\n        \"telephone\": {\n            \"title\": \"Điện thoại\",\n            \"content\": \"(+84) 987 654 321\",\n            \"visible\": 1\n        },\n        \"social_follow\": {\n            \"socials\": [\n                {\n                    \"name\": \"facebook\",\n                    \"text\": \"Facebook\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"twitter\",\n                    \"text\": \"Twitter\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"instagram\",\n                    \"text\": \"Instagram\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"tumblr\",\n                    \"text\": \"Tumblr\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"youtube\",\n                    \"text\": \"Youtube\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"googleplus\",\n                    \"text\": \"Google Plus\",\n                    \"visible\": 1\n                }\n            ],\n            \"visible\": 1\n        }\n    }\n}', '2020-05-20 15:38:57', '2020-05-20 15:38:57'),
(0, 'realestate_sbuilding', 'config', 'config_section_contact_form', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\n    \"name\": \"form\",\n    \"text\": \"Biểu mẫu\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Liên hệ với chúng tôi\"\n    },\n    \"data\": {\n        \"email\": \"sale.247@xshop.com\"\n    }\n}', '2020-05-20 15:38:57', '2020-05-20 15:38:57');

-- oc_blog PRIMARY KEY (`blog_id`)
-- nothing

-- oc_blog_category PRIMARY KEY (`blog_category_id`)
-- nothing

-- oc_blog_category_description PRIMARY KEY (`blog_category_id`,`language_id`)
-- nothing

-- oc_blog_description PRIMARY KEY (`blog_id`,`language_id`)
-- nothing

-- oc_blog_to_blog_category NO PRIMARY KEY
-- nothing
