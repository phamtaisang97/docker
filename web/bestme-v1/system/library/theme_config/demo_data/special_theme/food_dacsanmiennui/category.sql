-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(32, NULL, 0, 0, 0, 0, 1, '2020-12-21 14:58:12', '2020-12-21 14:58:12'),
(33, NULL, 0, 0, 0, 0, 1, '2020-12-21 14:58:12', '2020-12-21 14:58:12'),
(34, NULL, 0, 0, 0, 0, 1, '2020-12-21 14:58:12', '2020-12-21 15:40:26'),
(35, NULL, 0, 0, 0, 0, 1, '2020-12-21 14:58:12', '2020-12-21 14:58:12'),
(36, NULL, 0, 0, 0, 0, 1, '2020-12-21 14:58:12', '2020-12-21 14:58:12'),
(37, '', 34, 0, 0, 0, 1, '2020-12-21 15:40:26', '2020-12-21 15:40:26'),
(38, '', 34, 0, 0, 0, 1, '2020-12-21 15:40:27', '2020-12-21 15:40:27');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(32, 2, 'Gia vị Tây Bắc', 'Gia vị Tây Bắc', '', '', ''),
(32, 1, 'Gia vị Tây Bắc', 'Gia vị Tây Bắc', '', '', ''),
(33, 2, 'Đồ khô Tây Bắc', 'Đồ khô Tây Bắc', '', '', ''),
(33, 1, 'Đồ khô Tây Bắc', 'Đồ khô Tây Bắc', '', '', ''),
(34, 2, 'Rượu quý Tây Bắc', 'Rượu quý Tây Bắc', '', '', ''),
(34, 1, 'Rượu quý Tây Bắc', 'Rượu quý Tây Bắc', '', '', ''),
(35, 2, 'Thịt gác bếp Tây Bắc', 'Thịt gác bếp Tây Bắc', '', '', ''),
(35, 1, 'Thịt gác bếp Tây Bắc', 'Thịt gác bếp Tây Bắc', '', '', ''),
(36, 2, 'Thuốc quý Tây Bắc', 'Thuốc quý Tây Bắc', '', '', ''),
(36, 1, 'Thuốc quý Tây Bắc', 'Thuốc quý Tây Bắc', '', '', ''),
(37, 2, 'Rượu ngâm', 'Rượu ngâm', '', '', ''),
(37, 1, 'Rượu ngâm', 'Rượu ngâm', '', '', ''),
(38, 2, 'Rượu nấu', 'Rượu nấu', '', '', ''),
(38, 1, 'Rượu nấu', 'Rượu nấu', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(38, 34, 0),
(38, 38, 1),
(33, 33, 0),
(35, 35, 0),
(37, 37, 1),
(32, 32, 0),
(36, 36, 0),
(37, 34, 0),
(34, 34, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0);