-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(14, 'Đặc sản đồ nhắm Tây Bắc', 0, 1, '', '1'),
(6, 'Sản phẩm nổi bật', 0, 1, '', '0'),
(15, 'Đặc sản rừng núi Tây Bắc', 0, 1, '', '1'),
(9, 'Gia vị Tây Bắc', 0, 1, '', '0'),
(10, 'Đồ khô Tây Bắc', 0, 1, NULL, '0'),
(11, 'Rượu quý Tây Bắc', 0, 1, '', '0'),
(12, 'Thịt gác bếp Tây Bắc', 0, 1, '', '0'),
(13, 'Thuốc quý Tây Bắc', 0, 1, '', '0'),
(16, 'Thịt gác bếp', 0, 1, '', '0'),
(17, 'Rượu Tây bắc', 0, 1, '', '0'),
(18, 'Gia vị miền núi', 0, 1, '', '0'),
(19, 'Thuốc quý', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(16, 'https://cdn.bestme.asia/images/dacsanmiennui/2_illfmPa.png', '', '', '', ''),
(14, '', '', '', '', ''),
(18, 'https://cdn.bestme.asia/images/dacsanmiennui/4_SDxPtpL.png', '', '', '', ''),
(17, 'https://cdn.bestme.asia/images/dacsanmiennui/3_XCIRJbU.png', '', '', '', ''),
(15, '', '', '', '', ''),
(6, '', '&lt;p&gt;nhũng sản phẩm mới nhất có mặt tại cửa hàng&lt;/p&gt;', '', '', ''),
(9, '', '', '', '', ''),
(10, '', '', '', '', ''),
(11, '', '', '', '', ''),
(12, '', '', '', '', ''),
(13, '', '', '', '', ''),
(19, 'https://cdn.bestme.asia/images/dacsanmiennui/1_p5PD4Su.png', '', '', '', '');