-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(9, 'Hot sale', 0, 1, '', '0'),
(10, 'Máy câu và cần câu', 0, 1, '', '0'),
(11, 'Dây câu và mồi câu', 0, 1, '', '0'),
(12, 'Phụ kiện cho dân câu', 0, 1, '', '0'),
(13, 'Đồ câu cao cấp', 0, 1, '', '0'),
(26, 'Dây Câu Cước', 0, 1, '', '0'),
(27, 'Cần Câu Tay', 0, 1, '', '0'),
(28, 'Máy câu cao cấp', 0, 1, '', '1'),
(29, 'Cần câu tay cao cấp', 0, 1, '', '1'),
(22, 'Mồi Câu', 0, 1, '', '0'),
(23, 'Máy Câu Đứng', 0, 1, '', '0'),
(24, 'Máy Câu Ngang', 0, 1, '', '0'),
(25, 'Lưỡi Câu Đơn', 0, 1, '', '0'),
(30, 'Lưỡi câu, mồi câu', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, '', '', '', '', ''),
(10, '', '', '', '', ''),
(11, '', '', '', '', ''),
(12, '', '', '', '', ''),
(13, '', '', '', '', ''),
(22, 'https://cdn.bestme.asia/images/hs-fishing/bst6-free.jpg', '', '', '', ''),
(23, 'https://cdn.bestme.asia/images/hs-fishing/bst4-free.jpg', '', '', '', ''),
(24, 'https://cdn.bestme.asia/images/hs-fishing/bst5-free.jpg', '', '', '', ''),
(25, 'https://cdn.bestme.asia/images/hs-fishing/bst3-free.jpg', '', '', '', ''),
(26, 'https://cdn.bestme.asia/images/hs-fishing/bst2-free.jpg', '', '', '', ''),
(27, 'https://cdn.bestme.asia/images/hs-fishing/bst1-free.jpg', '', '', '', ''),
(28, '', '', '', '', ''),
(29, '', '', '', '', ''),
(30, '', '', '', '', '');