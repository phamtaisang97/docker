-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(40, NULL, 0, 0, 0, 0, 1, '2020-12-21 16:29:27', '2020-12-21 16:29:27'),
(39, NULL, 0, 0, 0, 0, 1, '2020-12-21 16:29:27', '2020-12-21 16:29:27'),
(38, NULL, 0, 0, 0, 0, 1, '2020-12-21 16:29:27', '2020-12-21 16:29:27'),
(37, NULL, 0, 0, 0, 0, 1, '2020-12-21 16:29:27', '2020-12-21 16:29:27'),
(36, NULL, 0, 0, 0, 0, 1, '2020-12-21 16:29:27', '2020-12-21 16:29:27'),
(35, NULL, 0, 0, 0, 0, 1, '2020-12-21 16:29:27', '2020-12-21 16:29:27'),
(34, NULL, 0, 0, 0, 0, 1, '2020-12-21 16:29:27', '2020-12-21 16:29:27'),
(33, NULL, 0, 0, 0, 0, 1, '2020-12-21 16:29:27', '2020-12-21 16:29:27'),
(19, NULL, 0, 0, 0, 0, 1, '2019-05-21 15:47:12', '2019-05-21 15:47:12'),
(17, NULL, 0, 0, 0, 0, 1, '2019-05-21 15:36:21', '2019-05-21 15:36:21'),
(14, NULL, 0, 0, 0, 0, 1, '2019-05-20 17:45:50', '2019-05-20 17:45:50'),
(18, NULL, 0, 0, 0, 0, 1, '2019-05-21 15:44:23', '2019-05-21 15:44:23'),
(12, NULL, 0, 0, 0, 0, 1, '2019-05-20 16:48:18', '2019-05-20 16:48:18'),
(32, NULL, 0, 0, 0, 0, 1, '2020-12-21 16:29:26', '2020-12-21 16:29:26'),
(41, NULL, 0, 0, 0, 0, 1, '2020-12-21 16:29:27', '2020-12-21 16:29:27'),
(20, NULL, 0, 0, 0, 0, 1, '2019-05-21 16:20:38', '2019-05-21 16:20:38');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(39, 2, 'Máy câu ngang', 'Máy câu ngang', '', '', ''),
(38, 1, 'Máy câu đứng', 'Máy câu đứng', '', '', ''),
(38, 2, 'Máy câu đứng', 'Máy câu đứng', '', '', ''),
(37, 1, 'Cần câu tay', 'Cần câu tay', '', '', ''),
(37, 2, 'Cần câu tay', 'Cần câu tay', '', '', ''),
(36, 2, 'Cần câu lục', 'Cần câu lục', '', '', ''),
(36, 1, 'Cần câu lục', 'Cần câu lục', '', '', ''),
(35, 2, 'Mồi câu', 'Mồi câu', '', '', ''),
(35, 1, 'Mồi câu', 'Mồi câu', '', '', ''),
(34, 1, 'Lưỡi câu Lancer', 'Lưỡi câu Lancer', '', '', ''),
(33, 1, 'Lưỡi câu đơn', 'Lưỡi câu đơn', '', '', ''),
(34, 2, 'Lưỡi câu Lancer', 'Lưỡi câu Lancer', '', '', ''),
(32, 1, 'Phụ kiện cho dân câu', 'Phụ kiện cho dân câu', '', '', ''),
(33, 2, 'Lưỡi câu đơn', 'Lưỡi câu đơn', '', '', ''),
(12, 1, 'Giày cổ điển', 'Giày cổ điển', '', '', ''),
(32, 2, 'Phụ kiện cho dân câu', 'Phụ kiện cho dân câu', '', '', ''),
(19, 2, 'Dép sandal', 'Dép sandal', '', '', ''),
(19, 1, 'Dép sandal', 'Dép sandal', '', '', ''),
(20, 2, 'Giày đúp', 'Giày đúp', '', '', ''),
(20, 1, 'Giày đúp', 'Giày đúp', '', '', ''),
(14, 2, 'Giày cao gót', 'Giày cao gót', '', '', ''),
(14, 1, 'Giày cao gót', 'Giày cao gót', '', '', ''),
(17, 2, 'Giày valen', 'Giày valen', '', '', ''),
(17, 1, 'Giày valen', 'Giày valen', '', '', ''),
(18, 2, 'Giày sandal', 'Giày sandal', '', '', ''),
(18, 1, 'Giày sandal', 'Giày sandal', '', '', ''),
(12, 2, 'Giày cổ điển', 'Giày cổ điển', '', '', ''),
(39, 1, 'Máy câu ngang', 'Máy câu ngang', '', '', ''),
(40, 2, 'Dây câu cước', 'Dây câu cước', '', '', ''),
(40, 1, 'Dây câu cước', 'Dây câu cước', '', '', ''),
(41, 2, 'Dây câu Leader', 'Dây câu Leader', '', '', ''),
(41, 1, 'Dây câu Leader', 'Dây câu Leader', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(41, 41, 0),
(40, 40, 0),
(17, 17, 0),
(14, 14, 0),
(19, 19, 0),
(18, 18, 0),
(39, 39, 0),
(38, 38, 0),
(12, 12, 0),
(20, 20, 0),
(37, 37, 0),
(36, 36, 0),
(35, 35, 0),
(34, 34, 0),
(33, 33, 0),
(32, 32, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(12, 0),
(14, 0),
(17, 0),
(18, 0),
(19, 0),
(20, 0),
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(39, 0),
(40, 0),
(41, 0);
