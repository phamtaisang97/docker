-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(11, 'Đầu đốt', 0, 1, '', '0'),
(10, 'Thân máy và phụ kiện', 0, 1, '', '0'),
(9, 'Hot Sale', 0, 1, '', '0'),
(12, 'Tinh dầu lan tỏa', 0, 1, '', '0'),
(13, 'Pod Mod', 0, 1, '', '0'),
(14, 'BST 1', 0, 1, '', '1'),
(15, 'BST 2', 0, 1, '', '1');


-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, '', '', '', '', ''),
(10, 'https://cdn.bestme.asia/images/vapevip/bst1-free.jpg', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/vapevip/bst2-free.jpg', '', '', '', ''),
(12, 'https://cdn.bestme.asia/images/vapevip/bst3-free.jpg', '', '', '', ''),
(13, 'https://cdn.bestme.asia/images/vapevip/bst4-free.jpg', '', '', '', ''),
(14, '', '', '', '', ''),
(15, '', '', '', '', '');
