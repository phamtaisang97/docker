-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(9, 'Siêu phẩm 2020', 0, 1, '', '0'),
(10, 'TOYOTA INNOVA', 0, 1, '', '0'),
(8, 'BST tổng hợp', 0, 1, '', '0'),
(11, 'BST tổng hợp1', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(10, 'https://cdn.bestme.asia/images/x2/banner-2_6XVySoE.png', '', '', '', ''),
(11, '', '', '', '', ''),
(8, '', '', '', '', ''),
(9, 'https://cdn.bestme.asia/images/x2/banner-1_doS7eQk.png', '', '', '', '');