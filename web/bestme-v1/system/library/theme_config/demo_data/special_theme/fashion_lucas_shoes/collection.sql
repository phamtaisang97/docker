-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(11, 'Giày búp bê đẹp', 0, 1, '', '0'),
(10, 'Sandals HOT', 0, 1, '', '0'),
(9, 'Giày Tây công sở', 0, 1, '', '0'),
(12, 'Giày gót vuông', 0, 1, '', '0'),
(13, 'Sandals cho bé', 0, 1, '', '0'),
(14, 'Giày búp bê cho bé gái', 0, 1, '', '0'),
(15, 'Boot nữ HOT 2020', 0, 1, '', '0'),
(23, 'Dành cho phái nữ', 0, 1, '', '0'),
(24, 'Danh cho nam', 0, 1, '', '0'),
(25, 'Hot sale', 0, 1, '', '0'),
(22, 'Bộ sưu tập', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, 'https://cdn.bestme.asia/images/lucas-shoes/bst1-100.jpg', '', '', '', ''),
(10, 'https://cdn.bestme.asia/images/lucas-shoes/bst4-100.jpg', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/lucas-shoes/bst6-100.jpg', '', '', '', ''),
(12, 'https://cdn.bestme.asia/images/lucas-shoes/bst2-100.jpg', '', '', '', ''),
(13, 'https://cdn.bestme.asia/images/lucas-shoes/bst5-100.jpg', '', '', '', ''),
(14, '', '', '', '', ''),
(15, 'https://cdn.bestme.asia/images/lucas-shoes/bst3-100.jpg', '', '', '', ''),
(22, '', '', '', '', ''),
(23, '', '', '', '', ''),
(24, '', '', '', '', ''),
(25, '', '', '', '', '');