-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(34, '', 0, 0, 0, 3, 1, '2020-12-08 11:00:34', '2020-12-08 11:00:56'),
(35, '', 0, 0, 0, 4, 1, '2020-12-08 11:00:50', '2020-12-08 11:01:01'),
(33, NULL, 0, 0, 0, 1, 1, '2020-12-08 10:47:33', '2020-12-08 10:59:30'),
(32, NULL, 0, 0, 0, 2, 1, '2020-12-08 10:47:33', '2020-12-08 10:59:31');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(34, 2, 'Máy Tập Thể Dục', 'Máy Tập Thể Dục', '', '', ''),
(34, 1, 'Máy Tập Thể Dục', 'Máy Tập Thể Dục', '', '', ''),
(35, 2, 'Ghế Massage Văn Phòng', 'Ghế Massage Văn Phòng', '', '', ''),
(35, 1, 'Ghế Massage Văn Phòng', 'Ghế Massage Văn Phòng', '', '', ''),
(32, 2, 'Dụng Cụ Massage', 'Dụng Cụ Massage', '', '', ''),
(32, 1, 'Dụng Cụ Massage', 'Dụng Cụ Massage', '', '', ''),
(33, 2, 'Ghế Massage', 'Ghế Massage', '', '', ''),
(33, 1, 'Ghế Massage', 'Ghế Massage', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(33, 33, 0),
(32, 32, 0),
(34, 34, 0),
(35, 35, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0);