-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(9, 'Xua tan stress cho dân công sở', 0, 1, '', '0'),
(10, 'Thiết bị cao cấp', 0, 1, '', '0'),
(11, 'Thư giãn, giải tỏa căng thẳng', 0, 1, '', '0'),
(12, 'Hỗ trợ, hồi phục cơ bắp', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, '', '', '', '', ''),
(10, '', '', '', '', ''),
(11, '', '', '', '', ''),
(12, '', '', '', '', '');