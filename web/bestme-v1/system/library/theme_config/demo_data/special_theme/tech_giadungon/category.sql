-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(32, '', 0, 0, 0, 0, 1, '2020-12-12 15:36:10', '2020-12-12 15:36:10'),
(33, '', 0, 0, 0, 0, 1, '2020-12-12 15:47:47', '2020-12-12 15:47:58'),
(34, '', 0, 0, 0, 0, 1, '2020-12-12 15:48:10', '2020-12-12 15:48:10'),
(35, '', 0, 0, 0, 0, 1, '2020-12-12 17:16:46', '2020-12-12 17:16:46'),
(36, '', 0, 0, 0, 0, 1, '2020-12-12 17:17:03', '2020-12-12 17:20:29');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(32, 2, 'Tủ lạnh, tủ mát', 'Tủ lạnh, tủ mát', '', '', ''),
(32, 1, 'Tủ lạnh, tủ mát', 'Tủ lạnh, tủ mát', '', '', ''),
(33, 2, 'Lò nướng, lò vi sóng', 'Lò nướng, lò vi sóng', '', '', ''),
(33, 1, 'Lò nướng, lò vi sóng', 'Lò nướng, lò vi sóng', '', '', ''),
(34, 2, 'Máy ép, máy xay', 'Máy ép, máy xay', '', '', ''),
(34, 1, 'Máy ép, máy xay', 'Máy ép, máy xay', '', '', ''),
(35, 2, 'Bếp điện', 'Bếp điện', '', '', ''),
(35, 1, 'Bếp điện', 'Bếp điện', '', '', ''),
(36, 2, 'Thiết bị, dụng cụ nhà bếp', 'Thiết bị, dụng cụ nhà bếp', '', '', ''),
(36, 1, 'Thiết bị, dụng cụ nhà bếp', 'Thiết bị, dụng cụ nhà bếp', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(33, 33, 0),
(35, 35, 0),
(32, 32, 0),
(36, 36, 0),
(34, 34, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0);
