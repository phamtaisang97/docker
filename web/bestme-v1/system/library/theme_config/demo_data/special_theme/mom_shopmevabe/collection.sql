-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(9, 'Bottles', 0, 1, NULL, '0'),
(10, 'BST mới', 0, 1, '', '0'),
(11, 'BST bán chạy', 0, 1, '', '0'),
(12, 'Baby kit', 0, 1, NULL, '0'),
(13, 'Bath tub', 0, 1, NULL, '0'),
(14, 'BST khuyến mãi', 0, 1, NULL, '0'),
(15, 'Milk bottles', 0, 1, NULL, '0'),
(16, 'Giặt Xả, Tắm Gội', 0, 1, NULL, '0'),
(17, 'Deuter', 0, 1, NULL, '0'),
(18, 'Baby Towel', 0, 1, NULL, '0');


-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, '', '', '', '', ''),
(10, '', '', '', '', ''),
(11, '', '', '', '', ''),
(12, '', '', '', '', ''),
(13, '', '', '', '', ''),
(14, '', '', '', '', ''),
(15, '', '', '', '', ''),
(16, '', '', '', '', ''),
(17, '', '', '', '', ''),
(18, '', '', '', '', '');
