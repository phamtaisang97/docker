-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(23, 'Máy cắt - Máy cưa', 0, 1, '', '0'),
(24, 'Máy cưa', 0, 1, '', '0'),
(18, 'Sản phẩm mới', 0, 1, '', '0'),
(19, 'Sản phẩm bán chạy', 0, 1, '', '0'),
(20, 'Sản phẩm khuyến mại', 0, 1, '', '0'),
(21, 'Máy mài', 0, 1, '', '0'),
(25, 'Dụng cụ dùng điện', 0, 1, '', '1'),
(26, 'Phụ kiện các loại', 0, 1, '', '0'),
(27, 'Dụng cụ dùng pin', 0, 1, '', '0'),
(28, 'Máy nén khí', 0, 1, '', '0'),
(29, 'Máy rút đinh', 0, 1, '', '0'),
(30, 'Máy bắn đinh', 0, 1, '', '0'),
(31, 'Dụng cụ dùng hơi', 0, 1, '', '1'),
(32, 'Dụng cụ cắt - bấm', 0, 1, '', '0'),
(33, 'Dụng cụ cầm tay', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(31, '', '', '', '', ''),
(23, '', '', '', '', ''),
(24, '', '', '', '', ''),
(28, 'https://cdn.bestme.asia/images/cokhihoanghuy/may-nen-khi.png', '&lt;p&gt;Máy nén khí&lt;/p&gt;', '', '', ''),
(29, '', '', '', '', ''),
(30, '', '&lt;p&gt;Máy bắn đinh&lt;/p&gt;', '', '', ''),
(18, '', '', '', '', ''),
(19, '', '', '', '', ''),
(20, '', '', '', '', ''),
(21, '', '', '', '', ''),
(25, '', '', '', '', ''),
(26, 'https://cdn.bestme.asia/images/cokhihoanghuy/phu-kien.png', '&lt;p&gt;Phụ kiện các loại&lt;/p&gt;', '', '', ''),
(27, 'https://cdn.bestme.asia/images/cokhihoanghuy/su-dung-pin.png', '&lt;p&gt;Dụng cụ dùng pin&lt;/p&gt;', '', '', ''),
(32, '', '', '', '', ''),
(33, '', '', '', '', '');