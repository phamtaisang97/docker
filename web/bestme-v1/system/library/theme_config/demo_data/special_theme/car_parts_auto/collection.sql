-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(11, 'Bộ Phận Động Cơ', 0, 1, '', '0'),
(8, 'Bộ Dụng Cụ', 0, 1, '', '0'),
(10, 'Huyền Phù', 0, 1, '', '0'),
(9, 'Đĩa Và Miếng Phanh', 0, 1, '', '0'),
(12, 'Dầu Và Chất Bôi Trơn', 0, 1, '', '0'),
(13, 'Lốp Và Bánh Xe', 0, 1, '', '0'),
(14, 'Hệ Thống Lái', 0, 1, '', '0'),
(15, 'Pin', 0, 1, '', '0'),
(7, 'Hàng ngày', 0, 1, NULL, '0'),
(6, 'Sản phẩm mới', 0, 1, '', '0'),
(16, 'BST lớn', 0, 1, '', '1'),
(17, 'Tháng Khuyến Mãi', 0, 1, '', '0'),
(18, 'Siêu Ưu Đãi', 0, 1, '', '0'),
(19, 'th2', 0, 1, '', '1'),
(4, 'Sản phẩm khuyến mại', 0, 1, '', '0'),
(5, 'Sản phẩm hot', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, 'https://cdn.bestme.asia/images/x2/3.jpg', '', '', '', ''),
(10, 'https://cdn.bestme.asia/images/x2/2.jpg', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/x2/4.jpg', '', '', '', ''),
(12, 'https://cdn.bestme.asia/images/x2/5.jpg', '', '', '', ''),
(13, 'https://cdn.bestme.asia/images/x2/6.jpg', '', '', '', ''),
(14, 'https://cdn.bestme.asia/images/x2/7.jpg', '', '', '', ''),
(15, 'https://cdn.bestme.asia/images/x2/8.jpg', '', '', '', ''),
(8, 'https://cdn.bestme.asia/images/x2/1.jpg', '', '', '', ''),
(16, '', '&lt;p&gt;12&lt;/p&gt;', '', '', ''),
(17, 'https://cdn.bestme.asia/images/x2/banner-them-1.jpg', '', '', '', ''),
(18, 'https://cdn.bestme.asia/images/x2/banner-them-2.jpg', '', '', '', ''),
(19, '', '', '', '', ''),
(6, '', '&lt;p&gt;nhũng sản phẩm mới nhất c&amp;oacute; mặt tại cửa h&amp;agrave;ng&lt;/p&gt;', '', '', ''),
(7, 'https://cdn.bestme.asia/images/x2/placeholder.png', '&lt;p&gt;Bộ sưu tập h&amp;agrave;ng ng&amp;agrave;y&lt;/p&gt;', '', '', ''),
(4, 'https://cdn.bestme.asia/images/x2/placeholder.png', '&lt;p&gt;sản phẩm khuyến mại&lt;/p&gt;', '', '', ''),
(5, 'https://cdn.bestme.asia/images/x2/placeholder.png', '&lt;p&gt;sản phẩm hot&lt;/p&gt;', '', '', '');