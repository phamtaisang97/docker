-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(7, 'Sushi', 0, 1, '', '0'),
(6, 'Sashimi', 0, 1, '', '0'),
(5, 'KimBap', 0, 1, '', '0'),
(12, 'Ramen', 0, 1, '', '0'),
(4, 'Takikomigohan', 0, 1, '', '0'),
(9, 'Set Combo', 0, 1, '', '0'),
(10, 'Menu Sushi', 0, 1, '', '1'),
(11, 'Hot Menu', 0, 1, '', '0'),
(13, 'Sản phẩm khuyến mại', 0, 1, '', '0'),
(14, 'Main dishes', 0, 1, '', '1'),
(15, 'Dorayaki', 0, 1, '', '0'),
(16, 'Ice Cream', 0, 1, '', '0'),
(17, 'Desserts', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(4, 'https://cdn.bestme.asia/images/x2/taki-gi-gi-do-100.jpg', '&lt;p&gt;sản phẩm khuyến mại&lt;/p&gt;', '', '', ''),
(5, 'https://cdn.bestme.asia/images/x2/kimbap-100.jpg', '&lt;p&gt;sản phẩm hot&lt;/p&gt;', '', '', ''),
(6, 'https://cdn.bestme.asia/images/x2/sashimi-100.jpg', '&lt;p&gt;nhũng sản phẩm mới nhất có mặt tại cửa hàng&lt;/p&gt;', '', '', ''),
(7, 'https://cdn.bestme.asia/images/x2/sushi-100.jpg', '', '', '', ''),
(9, 'https://cdn.bestme.asia/images/x2/combo-100.jpg', '', '', '', ''),
(10, '', '', '', '', ''),
(11, '', '', '', '', ''),
(12, 'https://cdn.bestme.asia/images/x2/ramen-100.jpg', '', '', '', ''),
(13, '', '', '', '', ''),
(14, '', '', '', '', ''),
(15, 'https://cdn.bestme.asia/images/x2/dorayaki-100.jpg', '', '', '', ''),
(16, 'https://cdn.bestme.asia/images/x2/kem-da-bao-100.jpg', '', '', '', ''),
(17, '', '', '', '', '');