-- oc_seo_url PRIMARY KEY (`seo_url_id`)
INSERT IGNORE INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`, `table_name`) VALUES
(2248, 0, 2, 'product_id=141', 'sushi-10', 'product'),
(2377, 0, 1, 'product_id=140', 'sushi-09', 'product'),
(2376, 0, 2, 'product_id=140', 'sushi-09', 'product'),
(2371, 0, 1, 'product_id=139', 'sushi-08', 'product'),
(2370, 0, 2, 'product_id=139', 'sushi-08', 'product'),
(2333, 0, 1, 'product_id=138', 'sushi-07', 'product'),
(2332, 0, 2, 'product_id=138', 'sushi-07', 'product'),
(2359, 0, 1, 'product_id=137', 'sushi-06', 'product'),
(2312, 0, 2, 'product_id=136', 'sushi-05', 'product'),
(2358, 0, 2, 'product_id=137', 'sushi-06', 'product'),
(2313, 0, 1, 'product_id=136', 'sushi-05', 'product'),
(2379, 0, 1, 'product_id=135', 'sushi-04', 'product'),
(2378, 0, 2, 'product_id=135', 'sushi-04', 'product'),
(2335, 0, 1, 'product_id=134', 'sushi-03', 'product'),
(2334, 0, 2, 'product_id=134', 'sushi-03', 'product'),
(2373, 0, 1, 'product_id=133', 'sushi-02', 'product'),
(2372, 0, 2, 'product_id=133', 'sushi-02', 'product'),
(2383, 0, 1, 'product_id=132', 'sushi-01', 'product'),
(2382, 0, 2, 'product_id=132', 'sushi-01', 'product'),
(2229, 0, 1, 'manufacturer_id=26', '-1', 'manufacturer'),
(2228, 0, 2, 'manufacturer_id=26', '-1', 'manufacturer'),
(2193, 0, 1, 'menu_id=6', 'menu-bo-suu-tap', ''),
(2192, 0, 2, 'menu_id=6', 'menu-bo-suu-tap', ''),
(2311, 0, 1, 'category_id=35', 'mi', 'category'),
(2117, 0, 2, 'manufacturer_id=26', '', 'manufacturer'),
(2118, 0, 1, 'manufacturer_id=26', '', 'manufacturer'),
(2392, 0, 2, 'collection=17', 'desserts', 'collection'),
(2388, 0, 2, 'collection=13', 'san-pham-khuyen-mai', 'collection'),
(2389, 0, 2, 'collection=14', 'main-dishes', 'collection'),
(2390, 0, 2, 'collection=15', 'dorayaki', 'collection'),
(2391, 0, 2, 'collection=16', 'ice-cream', 'collection'),
(2387, 0, 2, 'collection=12', 'ramen', 'collection'),
(2365, 0, 1, 'category_id=44', 'banh-chien', 'category'),
(2384, 0, 2, 'collection=9', 'set-combo', 'collection'),
(2385, 0, 2, 'collection=10', 'menu-sushi', 'collection'),
(2386, 0, 2, 'collection=11', 'hot-menu', 'collection'),
(2314, 0, 2, 'category_id=36', 'com', 'category'),
(2315, 0, 1, 'category_id=36', 'com', 'category'),
(2089, 0, 2, 'collection=4', 'd-col-san-pham-khuyen-mai', 'category'),
(2090, 0, 2, 'collection=5', 'd-col-san-pham-hot', 'category'),
(2091, 0, 2, 'collection=6', 'd-col-san-pham-moi', 'category'),
(2092, 0, 2, 'collection=7', 'd-col-hang-ngay', 'category'),
(2249, 0, 1, 'product_id=141', 'sushi-10', 'product'),
(2250, 0, 2, 'product_id=142', 'sushi-11', 'product'),
(2251, 0, 1, 'product_id=142', 'sushi-11', 'product'),
(2325, 0, 1, 'product_id=143', 'sushi-12', 'product'),
(2324, 0, 2, 'product_id=143', 'sushi-12', 'product'),
(2369, 0, 1, 'product_id=144', 'sushi-13', 'product'),
(2368, 0, 2, 'product_id=144', 'sushi-13', 'product'),
(2375, 0, 1, 'product_id=145', 'sushi-14', 'product'),
(2374, 0, 2, 'product_id=145', 'sushi-14', 'product'),
(2357, 0, 1, 'product_id=146', 'sushi-15', 'product'),
(2356, 0, 2, 'product_id=146', 'sushi-15', 'product'),
(2361, 0, 1, 'product_id=147', 'sushi-16', 'product'),
(2360, 0, 2, 'product_id=147', 'sushi-16', 'product'),
(2317, 0, 1, 'product_id=148', 'sushi-17', 'product'),
(2316, 0, 2, 'product_id=148', 'sushi-17', 'product'),
(2349, 0, 1, 'product_id=149', 'sushi-18', 'product'),
(2348, 0, 2, 'product_id=149', 'sushi-18', 'product'),
(2319, 0, 1, 'product_id=150', 'sushi-19', 'product'),
(2318, 0, 2, 'product_id=150', 'sushi-19', 'product'),
(2363, 0, 1, 'product_id=151', 'sushi-20', 'product'),
(2362, 0, 2, 'product_id=151', 'sushi-20', 'product'),
(2270, 0, 2, 'product_id=152', 'sushi-21', 'product'),
(2271, 0, 1, 'product_id=152', 'sushi-21', 'product'),
(2329, 0, 1, 'product_id=153', 'sushi-22', 'product'),
(2328, 0, 2, 'product_id=153', 'sushi-22', 'product'),
(2274, 0, 2, 'product_id=154', 'sushi-23', 'product'),
(2275, 0, 1, 'product_id=154', 'sushi-23', 'product'),
(2276, 0, 2, 'product_id=155', 'sushi-24', 'product'),
(2277, 0, 1, 'product_id=155', 'sushi-24', 'product'),
(2323, 0, 1, 'product_id=156', 'sushi-25', 'product'),
(2322, 0, 2, 'product_id=156', 'sushi-25', 'product'),
(2280, 0, 2, 'product_id=157', 'sushi-26', 'product'),
(2281, 0, 1, 'product_id=157', 'sushi-26', 'product'),
(2341, 0, 1, 'product_id=158', 'sushi-27', 'product'),
(2340, 0, 2, 'product_id=158', 'sushi-27', 'product'),
(2337, 0, 1, 'product_id=159', 'sushi-28', 'product'),
(2336, 0, 2, 'product_id=159', 'sushi-28', 'product'),
(2381, 0, 1, 'product_id=160', 'sushi-29', 'product'),
(2380, 0, 2, 'product_id=160', 'sushi-29', 'product'),
(2345, 0, 1, 'product_id=161', 'sushi-30', 'product'),
(2344, 0, 2, 'product_id=161', 'sushi-30', 'product'),
(2290, 0, 2, 'product_id=162', 'sushi-31', 'product'),
(2291, 0, 1, 'product_id=162', 'sushi-31', 'product'),
(2353, 0, 1, 'product_id=163', 'sushi-32', 'product'),
(2352, 0, 2, 'product_id=163', 'sushi-32', 'product'),
(2367, 0, 1, 'product_id=164', 'sushi-33', 'product'),
(2366, 0, 2, 'product_id=164', 'sushi-33', 'product'),
(2296, 0, 2, 'product_id=165', 'sushi-34', 'product'),
(2297, 0, 1, 'product_id=165', 'sushi-34', 'product'),
(2355, 0, 1, 'product_id=166', 'sushi-35', 'product'),
(2354, 0, 2, 'product_id=166', 'sushi-35', 'product'),
(2320, 0, 2, 'category_id=37', 'temaki-mono', 'category'),
(2321, 0, 1, 'category_id=37', 'temaki-mono', 'category'),
(2326, 0, 2, 'category_id=38', 'sashimi', 'category'),
(2327, 0, 1, 'category_id=38', 'sashimi', 'category'),
(2330, 0, 2, 'category_id=39', 'banh-mochi', 'category'),
(2331, 0, 1, 'category_id=39', 'banh-mochi', 'category'),
(2338, 0, 2, 'category_id=40', 'banh-dorayaki', 'category'),
(2339, 0, 1, 'category_id=40', 'banh-dorayaki', 'category'),
(2342, 0, 2, 'category_id=41', 'kem', 'category'),
(2343, 0, 1, 'category_id=41', 'kem', 'category'),
(2346, 0, 2, 'category_id=42', 'caramen', 'category'),
(2347, 0, 1, 'category_id=42', 'caramen', 'category'),
(2350, 0, 2, 'category_id=43', 'banh-takoyaki', 'category'),
(2351, 0, 1, 'category_id=43', 'banh-takoyaki', 'category'),
(2364, 0, 2, 'category_id=44', 'banh-chien', 'category'),
(2310, 0, 2, 'category_id=35', 'mi', 'category'),
(2307, 0, 1, 'category_id=34', 'nirigi-sushi', 'category'),
(2306, 0, 2, 'category_id=34', 'nirigi-sushi', 'category'),
(2302, 0, 2, 'category_id=32', 'maki-mono', 'category'),
(2303, 0, 1, 'category_id=32', 'maki-mono', 'category'),
(2304, 0, 2, 'category_id=33', 'gunkan-sushi', 'category'),
(2305, 0, 1, 'category_id=33', 'gunkan-sushi', 'category'),
(2202, 0, 2, 'blog=1', '7-dang-vay-maxi-hop-nhat-cho-cac-nang-chan-ngan', 'blog'),
(2203, 0, 2, 'blog=2', '7-phong-cach-thoi-trang-co-ban-nhat', 'blog');