-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(44, NULL, 0, 0, 0, 0, 1, '2020-10-09 15:26:02', '2020-10-09 15:26:02'),
(43, NULL, 0, 0, 0, 0, 1, '2020-10-09 15:22:14', '2020-10-09 15:22:14'),
(42, NULL, 0, 0, 0, 0, 1, '2020-10-09 15:21:32', '2020-10-09 15:21:32'),
(41, NULL, 0, 0, 0, 0, 1, '2020-10-09 15:21:06', '2020-10-09 15:21:06'),
(40, NULL, 0, 0, 0, 0, 1, '2020-10-09 15:19:41', '2020-10-09 15:19:41'),
(39, NULL, 0, 0, 0, 0, 1, '2020-10-09 15:18:14', '2020-10-09 15:18:14'),
(38, NULL, 0, 0, 0, 0, 1, '2020-10-09 15:17:54', '2020-10-09 15:17:54'),
(37, NULL, 0, 0, 0, 0, 1, '2020-10-09 15:17:16', '2020-10-09 15:17:16'),
(36, NULL, 0, 0, 0, 0, 1, '2020-10-09 15:16:45', '2020-10-09 15:16:45'),
(35, NULL, 0, 0, 0, 0, 1, '2020-10-09 15:16:38', '2020-10-09 15:16:38'),
(32, '', 0, 0, 0, 0, 1, '2020-10-09 15:13:28', '2020-10-09 15:13:28'),
(33, '', 0, 0, 0, 0, 1, '2020-10-09 15:15:19', '2020-10-09 15:15:19'),
(34, '', 0, 0, 0, 0, 1, '2020-10-09 15:15:34', '2020-10-09 15:15:34');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(44, 1, 'Bánh chiên', 'Bánh chiên', '', '', ''),
(43, 1, 'Bánh Takoyaki', 'Bánh Takoyaki', '', '', ''),
(44, 2, 'Bánh chiên', 'Bánh chiên', '', '', ''),
(42, 1, 'Caramen', 'Caramen', '', '', ''),
(43, 2, 'Bánh Takoyaki', 'Bánh Takoyaki', '', '', ''),
(41, 1, 'Kem', 'Kem', '', '', ''),
(42, 2, 'Caramen', 'Caramen', '', '', ''),
(40, 2, 'Bánh dorayaki', 'Bánh dorayaki', '', '', ''),
(40, 1, 'Bánh dorayaki', 'Bánh dorayaki', '', '', ''),
(41, 2, 'Kem', 'Kem', '', '', ''),
(38, 1, 'Sashimi', 'Sashimi', '', '', ''),
(39, 2, 'Bánh mochi', 'Bánh mochi', '', '', ''),
(39, 1, 'Bánh mochi', 'Bánh mochi', '', '', ''),
(32, 2, 'Maki Mono', 'Maki Mono', '', '', ''),
(32, 1, 'Maki Mono', 'Maki Mono', '', '', ''),
(33, 2, 'Gunkan Sushi', 'Gunkan Sushi', '', '', ''),
(33, 1, 'Gunkan Sushi', 'Gunkan Sushi', '', '', ''),
(34, 2, 'Nirigi sushi', 'Nirigi sushi', '', '', ''),
(34, 1, 'Nirigi sushi', 'Nirigi sushi', '', '', ''),
(35, 2, 'Mì', 'Mì', '', '', ''),
(35, 1, 'Mì', 'Mì', '', '', ''),
(36, 2, 'Cơm', 'Cơm', '', '', ''),
(36, 1, 'Cơm', 'Cơm', '', '', ''),
(37, 2, 'Temaki Mono', 'Temaki Mono', '', '', ''),
(37, 1, 'Temaki Mono', 'Temaki Mono', '', '', ''),
(38, 2, 'Sashimi', 'Sashimi', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(35, 35, 0),
(34, 34, 0),
(33, 33, 0),
(32, 32, 0),
(44, 44, 0),
(37, 37, 0),
(36, 36, 0),
(43, 43, 0),
(42, 42, 0),
(41, 41, 0),
(40, 40, 0),
(39, 39, 0),
(38, 38, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(39, 0),
(40, 0),
(41, 0),
(42, 0),
(43, 0),
(44, 0);