-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(20, 'TH', 0, 1, '', '0'),
(19, 'Sản phẩm nổi bật', 0, 1, '', '0'),
(18, 'Sản phẩm bán chạy', 0, 1, '', '0'),
(9, 'Cà chua', 0, 1, '', '0'),
(10, 'Cam ngọt', 0, 1, '', '0'),
(11, 'Chuối', 0, 1, '', '0'),
(12, 'Ớt chuông', 0, 1, '', '0'),
(13, 'Dưa chuột', 0, 1, '', '0'),
(14, 'Khoai tây', 0, 1, '', '0'),
(15, 'Sản phẩm', 0, 1, '', '1'),
(16, 'Bí ngô', 0, 1, '', '0'),
(17, 'Berry', 0, 1, '', '0'),
(21, 'Hoa quả nhập khẩu', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(18, '', '', '', '', ''),
(19, '', '', '', '', ''),
(20, '', '', '', '', ''),
(21, '', '', '', '', ''),
(9, 'https://cdn.bestme.asia/images/x2/bst1_8UekgKS.jpg', '&lt;p&gt;Cà chua&lt;/p&gt;', '', '', ''),
(10, 'https://cdn.bestme.asia/images/x2/bst2_1LObSt5.jpg', '&lt;p&gt;Cam ngọt&lt;/p&gt;', '', '', ''),
(11, 'https://cdn.bestme.asia/images/x2/bst3_eUweDoi.jpg', '&lt;p&gt;Chuối&lt;/p&gt;', '', '', ''),
(12, 'https://cdn.bestme.asia/images/x2/bst4_LWSQ5Ru.jpg', '&lt;p&gt;Ớt chuông&lt;/p&gt;', '', '', ''),
(13, 'https://cdn.bestme.asia/images/x2/bst5.jpg', '&lt;p&gt;Dưa chuột&lt;/p&gt;', '', '', ''),
(14, 'https://cdn.bestme.asia/images/x2/bst6.jpg', '&lt;p&gt;Khoai tây&lt;/p&gt;', '', '', ''),
(15, '', '&lt;p&gt;Sản phẩm&lt;/p&gt;', '', '', ''),
(16, 'https://cdn.bestme.asia/images/x2/bst7.jpg', '&lt;p&gt;Bí ngô&lt;/p&gt;', '', '', ''),
(17, 'https://cdn.bestme.asia/images/x2/bst8.jpg', '&lt;p&gt;Berry&lt;/p&gt;', '', '', '');