-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(38, NULL, 0, 0, 0, 0, 1, '2020-10-08 14:37:55', '2020-10-08 14:37:55'),
(37, NULL, 0, 0, 0, 0, 1, '2020-10-08 14:37:55', '2020-10-08 14:37:55'),
(36, NULL, 0, 0, 0, 0, 1, '2020-10-08 14:37:55', '2020-10-08 14:37:55'),
(35, NULL, 0, 0, 0, 0, 1, '2020-10-08 14:37:55', '2020-10-08 14:37:55'),
(34, NULL, 0, 0, 0, 0, 1, '2020-10-08 14:37:55', '2020-10-08 14:37:55'),
(33, NULL, 0, 0, 0, 0, 1, '2020-10-08 14:37:55', '2020-10-08 14:37:55'),
(39, NULL, 0, 0, 0, 0, 1, '2020-10-13 11:54:07', '2020-10-13 11:54:07');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(38, 1, 'Trái Cây Hàn Quốc', 'Trái Cây Hàn Quốc', '', '', ''),
(38, 2, 'Trái Cây Hàn Quốc', 'Trái Cây Hàn Quốc', '', '', ''),
(37, 1, 'Trái Cây Nhật', 'Trái Cây Nhật', '', '', ''),
(37, 2, 'Trái Cây Nhật', 'Trái Cây Nhật', '', '', ''),
(36, 2, 'Trái Cây Mỹ', 'Trái Cây Mỹ', '', '', ''),
(36, 1, 'Trái Cây Mỹ', 'Trái Cây Mỹ', '', '', ''),
(35, 1, 'Trái Cây New Zealand', 'Trái Cây New Zealand', '', '', ''),
(35, 2, 'Trái Cây New Zealand', 'Trái Cây New Zealand', '', '', ''),
(34, 2, 'Trái Cây Úc', 'Trái Cây Úc', '', '', ''),
(34, 1, 'Trái Cây Úc', 'Trái Cây Úc', '', '', ''),
(33, 2, 'Trái Cây Việt Nam', 'Trái Cây Việt Nam', '', '', ''),
(33, 1, 'Trái Cây Việt Nam', 'Trái Cây Việt Nam', '', '', ''),
(39, 2, 'Trái Cây Ai Cập', 'Trái Cây Ai Cập', '', '', ''),
(39, 1, 'Trái Cây Ai Cập', 'Trái Cây Ai Cập', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(38, 38, 0),
(37, 37, 0),
(36, 36, 0),
(35, 35, 0),
(34, 34, 0),
(33, 33, 0),
(39, 39, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(39, 0);