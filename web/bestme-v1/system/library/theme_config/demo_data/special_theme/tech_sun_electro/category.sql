-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(19, '', 0, 0, 0, 0, 1, '2019-05-21 15:47:12', '2020-08-07 10:54:41'),
(17, '', 0, 0, 0, 0, 1, '2019-05-21 15:36:21', '2020-08-07 10:54:47'),
(14, '', 0, 0, 0, 0, 1, '2019-05-20 17:45:50', '2020-08-07 10:54:56'),
(26, '', 0, 0, 0, 0, 1, '2020-06-09 17:50:59', '2020-08-07 10:54:34');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(17, 2, 'Phụ kiện', 'Phụ kiện', '', '', ''),
(17, 1, 'Phụ kiện', 'Phụ kiện', '', '', ''),
(14, 1, 'Máy ảnh', 'Máy ảnh', '', '', ''),
(14, 2, 'Máy ảnh', 'Máy ảnh', '', '', ''),
(26, 2, 'Laptop', 'Laptop', '', '', ''),
(26, 1, 'Laptop', 'Laptop', '', '', ''),
(19, 2, 'Iphone', 'Iphone', '', '', ''),
(19, 1, 'Iphone', 'Iphone', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(19, 19, 0),
(17, 17, 0),
(14, 14, 0),
(26, 26, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(14, 0),
(17, 0),
(19, 0),
(26, 0);