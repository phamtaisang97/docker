-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(11, 'Camera', 0, 1, '', '0'),
(8, 'Camera Collection', 0, 1, '', '0'),
(10, 'Accessories Collection', 0, 1, '', '0'),
(9, 'Laptop Collection', 0, 1, '', '0'),
(12, 'Điện thoại', 0, 1, '', '0'),
(13, 'Laptop Asus', 0, 1, '', '0'),
(14, 'Laptop MSI', 0, 1, '', '0'),
(15, 'Macbook', 0, 1, '', '0'),
(16, 'Sản phẩm khuyến mãi', 0, 1, '', '1'),
(17, 'Sản phẩm bán chạy', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, 'https://cdn.bestme.asia/images/x2/elecanh-bst-2.png', '', '', '', ''),
(10, 'https://cdn.bestme.asia/images/x2/elecanh-san-pham-6.png', '', '', '', ''),
(11, '', '', '', '', ''),
(12, '', '', '', '', ''),
(13, '', '', '', '', ''),
(14, '', '', '', '', ''),
(15, '', '', '', '', ''),
(16, '', '', '', '', ''),
(17, '', '', '', '', ''),
(8, 'https://cdn.bestme.asia/images/x2/elecanh-bst-1.png', '', '', '', '');