-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(11, 'Rượu quý Tây Bắc', 0, 1, NULL, '0'),
(10, 'Đồ khô Tây Bắc', 0, 1, NULL, '0'),
(9, 'Gia vị Tây Bắc', 0, 1, NULL, '0'),
(12, 'Thịt gác bếp Tây Bắc', 0, 1, NULL, '0'),
(13, 'Thuốc quý Tây Bắc', 0, 1, NULL, '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, '', '', '', '', ''),
(10, '', '', '', '', ''),
(11, '', '', '', '', ''),
(12, '', '', '', '', ''),
(13, '', '', '', '', '');