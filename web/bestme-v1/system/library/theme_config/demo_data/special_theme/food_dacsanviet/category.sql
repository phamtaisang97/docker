-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(37, 'https://cdn.bestme.asia/images/dacsanviet/whiskey.png', 0, 0, 0, 0, 1, '2020-12-24 15:19:25', '2020-12-24 15:50:17'),
(38, 'https://cdn.bestme.asia/images/dacsanviet/rice.png', 0, 0, 0, 0, 1, '2020-12-24 15:19:35', '2020-12-24 15:48:40'),
(39, 'https://cdn.bestme.asia/images/dacsanviet/honey.png', 0, 0, 0, 0, 1, '2020-12-24 15:19:52', '2020-12-24 15:49:37'),
(36, NULL, 0, 0, 0, 0, 1, '2020-12-21 17:26:24', '2020-12-21 17:26:24'),
(35, 'https://cdn.bestme.asia/images/dacsanviet/pork.png', 0, 0, 0, 0, 1, '2020-12-21 17:26:24', '2020-12-24 15:51:56'),
(34, 'https://cdn.bestme.asia/images/dacsanviet/beer-bottle.png', 0, 0, 0, 0, 1, '2020-12-21 17:26:24', '2020-12-24 15:49:53'),
(33, 'https://cdn.bestme.asia/images/dacsanviet/nachos.png', 0, 0, 0, 0, 1, '2020-12-21 17:26:24', '2020-12-24 15:51:15'),
(32, 'https://cdn.bestme.asia/images/dacsanviet/seasoning.png', 0, 0, 0, 0, 1, '2020-12-21 17:26:24', '2020-12-24 15:48:57'),
(41, 'https://cdn.bestme.asia/images/dacsanviet/dried-fruits.png', 0, 0, 0, 0, 1, '2020-12-24 15:22:07', '2020-12-24 15:49:17'),
(40, 'https://cdn.bestme.asia/images/dacsanviet/lemongrass.png', 0, 0, 0, 0, 1, '2020-12-24 15:20:10', '2020-12-24 15:51:40');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(41, 2, 'Hoa quả khô', 'Hoa quả khô', '', '', ''),
(41, 1, 'Hoa quả khô', 'Hoa quả khô', '', '', ''),
(40, 1, 'Đồ ngâm rượu', 'Đồ ngâm rượu', '', '', ''),
(40, 2, 'Đồ ngâm rượu', 'Đồ ngâm rượu', '', '', ''),
(37, 2, 'Rượu đặc sản', 'Rượu đặc sản', '', '', ''),
(37, 1, 'Rượu đặc sản', 'Rượu đặc sản', '', '', ''),
(38, 2, 'Gạo Tây Bắc', 'Gạo Tây Bắc', '', '', ''),
(38, 1, 'Gạo Tây Bắc', 'Gạo Tây Bắc', '', '', ''),
(39, 2, 'Mật ong rừng', 'Mật ong rừng', '', '', ''),
(39, 1, 'Mật ong rừng', 'Mật ong rừng', '', '', ''),
(36, 2, 'Thuốc quý Tây Bắc', 'Thuốc quý Tây Bắc', '', '', ''),
(36, 1, 'Thuốc quý Tây Bắc', 'Thuốc quý Tây Bắc', '', '', ''),
(32, 2, 'Gia vị Tây Bắc', 'Gia vị Tây Bắc', '', '', ''),
(32, 1, 'Gia vị Tây Bắc', 'Gia vị Tây Bắc', '', '', ''),
(33, 2, 'Đồ khô Tây Bắc', 'Đồ khô Tây Bắc', '', '', ''),
(33, 1, 'Đồ khô Tây Bắc', 'Đồ khô Tây Bắc', '', '', ''),
(34, 2, 'Rượu quý Tây Bắc', 'Rượu quý Tây Bắc', '', '', ''),
(34, 1, 'Rượu quý Tây Bắc', 'Rượu quý Tây Bắc', '', '', ''),
(35, 2, 'Thịt gác bếp Tây Bắc', 'Thịt gác bếp Tây Bắc', '', '', ''),
(35, 1, 'Thịt gác bếp Tây Bắc', 'Thịt gác bếp Tây Bắc', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(35, 35, 0),
(34, 34, 0),
(33, 33, 0),
(32, 32, 0),
(37, 37, 0),
(36, 36, 0),
(38, 38, 0),
(39, 39, 0),
(40, 40, 0),
(41, 41, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(39, 0),
(40, 0),
(41, 0);