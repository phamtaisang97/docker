-- soft-deleted products if demo
-- UPDATE `oc_product` SET `deleted` = 1
-- WHERE `demo` = 1
--   AND `product_id` IN (
--       104,
--       105,
--       106,
--       107,
--       108,
--       109,
--       110,
--       111,
--       112,
--       113,
--       114
--   );

-- TODO: delete categories, collections, manufactures due to products

-- oc_blog_description PRIMARY KEY (`blog_id`,`language_id`)
DELETE FROM `oc_blog_description`
WHERE `language_id` = 2
    AND `blog_id` IN (1, 2, 3) AND `blog_id` IN (SELECT `blog_id` FROM `oc_blog` WHERE `demo` = 1);

-- oc_blog_to_blog_category NO PRIMARY KEY
DELETE FROM `oc_blog_to_blog_category`
WHERE `blog_id` IN (1, 2, 3) AND `blog_id` IN (SELECT `blog_id` FROM `oc_blog` WHERE `demo` = 1);

-- oc_blog PRIMARY KEY (`blog_id`)
DELETE FROM `oc_blog`
WHERE `demo` = 1
  AND `blog_id` IN (
      1,
      2,
      3
  );