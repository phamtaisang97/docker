-- oc_product PRIMARY KEY (`product_id`)
-- delete first
INSERT IGNORE INTO `oc_product` (`product_id`, `model`, `sku`, `common_barcode`, `common_sku`, `common_compare_price`, `common_price`, `common_cost_price`, `barcode`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `sale_on_out_of_stock`, `stock_status_id`, `image`, `image_alt`, `multi_versions`, `manufacturer_id`, `shipping`, `price`, `price_currency_id`, `compare_price`, `c_price_currency_id`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `channel`, `viewed`, `demo`, `deleted`, `default_store_id`, `date_added`, `date_modified`) VALUES
(132, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020224960f0-0cff-4260-9832-45fed1b416b6.png', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '100.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-08-06 12:25:40', '2020-08-06 12:25:40'),
(133, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020b99e9a84-2d63-4417-a0f6-137d3c2a2eee.png', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '100.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-08-06 12:25:40', '2020-08-06 12:25:40'),
(134, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/20202a13bac8-3260-4529-b7d5-f4cab6211337.png', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '100.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-08-06 12:25:40', '2020-08-06 12:25:40'),
(135, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020393a8c23-a5b8-4ac1-b482-fc6c64f516b5.png', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '100.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-08-06 12:25:40', '2020-08-06 12:25:40');

-- restore soft-deleted products if demo
UPDATE `oc_product` SET `deleted` = NULL
WHERE `demo` = 1
  AND `product_id` IN (
   132,
   133,
   134,
   135
  );

-- oc_product_collection PRIMARY KEY (`product_collection_id`)
INSERT IGNORE INTO `oc_product_collection` (`product_collection_id`, `product_id`, `collection_id`, `sort_order`) VALUES
(135, 131, 8, 0),
(136, 130, 8, 0),
(137, 129, 8, 0),
(155, 135, 10, 0),
(154, 134, 10, 0),
(153, 133, 9, 2),
(152, 132, 9, 1);

-- oc_product_description PRIMARY KEY (`product_id`,`language_id`)
INSERT IGNORE INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `sub_description`, `seo_title`, `seo_description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(132, 2, 'Súng 01', '', '', '', '', '', '', '', ''),
(133, 2, 'Súng 02', '', '', '', '', '', '', '', ''),
(134, 2, 'Súng 03', '', '', '', '', '', '', '', ''),
(135, 2, 'Súng 04', '', '', '', '', '', '', '', '');

-- oc_product_image PRIMARY KEY (`product_image_id`)
-- INSERT IGNORE INTO `oc_product_image` (`product_image_id`, `product_id`, `image`, `image_alt`, `sort_order`) VALUES
-- nothing

-- oc_product_to_category PRIMARY KEY (`product_id`,`category_id`)
-- INSERT IGNORE INTO `oc_product_to_category` (`product_id`, `category_id`) VALUES
-- nothing

-- oc_product_to_store PRIMARY KEY (`product_id`,`store_id`)
INSERT IGNORE INTO `oc_product_to_store` (`product_id`, `store_id`, `product_version_id`, `quantity`, `cost_price`) VALUES
(132, 0, 0, 30, '0.0000'),
(133, 0, 0, 30, '0.0000'),
(134, 0, 0, 30, '0.0000'),
(135, 0, 0, 30, '0.0000');