-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(35, NULL, 0, 0, 0, 0, 1, '2021-03-22 17:17:18', '2021-03-22 17:17:18'),
(34, NULL, 0, 0, 0, 0, 1, '2021-03-22 17:03:39', '2021-03-22 17:03:39'),
(33, NULL, 0, 0, 0, 0, 1, '2021-03-22 16:48:34', '2021-03-22 16:48:34'),
(32, NULL, 0, 0, 0, 0, 1, '2021-03-22 16:05:53', '2021-03-22 16:05:53');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(32, 2, 'Cà phê', 'Cà phê', '', '', ''),
(32, 1, 'Cà phê', 'Cà phê', '', '', ''),
(33, 2, 'Trà', 'Trà', '', '', ''),
(33, 1, 'Trà', 'Trà', '', '', ''),
(34, 2, 'Nước hoa quả', 'Nước hoa quả', '', '', ''),
(34, 1, 'Nước hoa quả', 'Nước hoa quả', '', '', ''),
(35, 2, 'Nước uống healthy', 'Nước uống healthy', '', '', ''),
(35, 1, 'Nước uống healthy', 'Nước uống healthy', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(35, 35, 0),
(34, 34, 0),
(33, 33, 0),
(32, 32, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0);