-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(21, 'Đồ Thờ Bằng Gốm', 0, 1, '', '0'),
(22, 'Đồ Thờ Bằng Đồng', 0, 1, '', '0'),
(23, 'Đồ Thờ Gia Tiên', 0, 1, '', '0'),
(20, 'Đồ Đồng Phong Thuỷ', 0, 1, '', '1'),
(19, 'Các Con Vật Phong Thuỷ Khác', 0, 1, '', '0'),
(18, '12 Con Giáp', 0, 1, '', '0'),
(24, 'Trống Đồng', 0, 1, '', '0'),
(25, 'Quà Tặng bằng Đồng', 0, 1, '', '0'),
(26, 'Tượng Đồng', 0, 1, '', '0'),
(27, 'Đồ Đồng Cao Cấp', 0, 1, '', '0'),
(28, 'Chuông Đồng', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(24, '', '', '', '', ''),
(25, '', '', '', '', ''),
(26, '', '', '', '', ''),
(27, '', '', '', '', ''),
(18, '', '', '', '', ''),
(19, '', '', '', '', ''),
(20, '', '', '', '', ''),
(21, 'https://cdn.bestme.asia/images/x2/bst1_G2uUacY.jpg', '', '', '', ''),
(22, 'https://cdn.bestme.asia/images/x2/bst3_tArH1nz.jpg', '', '', '', ''),
(23, 'https://cdn.bestme.asia/images/x2/bst-2.jpg', '', '', '', ''),
(28, '', '', '', '', '');