-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(41, '', 0, 0, 0, 0, 1, '2020-10-06 09:36:45', '2020-10-06 09:36:45'),
(40, '', 0, 0, 0, 0, 1, '2020-10-06 09:36:21', '2020-10-06 09:36:21'),
(39, '', 0, 0, 0, 0, 1, '2020-10-06 09:36:02', '2020-10-06 09:36:02'),
(36, '', 0, 0, 0, 0, 1, '2020-10-06 09:35:14', '2020-10-06 09:35:14'),
(35, '', 0, 0, 0, 0, 1, '2020-10-06 09:34:32', '2020-10-06 09:34:32'),
(34, '', 0, 0, 0, 0, 1, '2020-10-06 09:34:18', '2020-10-06 09:34:18'),
(33, '', 0, 0, 0, 0, 1, '2020-10-06 09:34:02', '2020-10-06 09:34:02'),
(32, '', 0, 0, 0, 0, 1, '2020-10-06 09:33:50', '2020-10-06 09:33:50'),
(37, '', 0, 0, 0, 0, 1, '2020-10-06 09:35:28', '2020-10-06 09:35:28'),
(38, '', 0, 0, 0, 0, 1, '2020-10-06 09:35:47', '2020-10-06 09:35:47');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(41, 1, 'Trái Cây Nhật', 'Trái Cây Nhật', '', '', ''),
(41, 2, 'Trái Cây Nhật', 'Trái Cây Nhật', '', '', ''),
(40, 2, 'Trái Cây Pháp', 'Trái Cây Pháp', '', '', ''),
(40, 1, 'Trái Cây Pháp', 'Trái Cây Pháp', '', '', ''),
(39, 1, 'Trái Cây Việt Nam', 'Trái Cây Việt Nam', '', '', ''),
(39, 2, 'Trái Cây Việt Nam', 'Trái Cây Việt Nam', '', '', ''),
(38, 2, 'Trái Cây Nam Phi', 'Trái Cây Nam Phi', '', '', ''),
(38, 1, 'Trái Cây Nam Phi', 'Trái Cây Nam Phi', '', '', ''),
(37, 2, 'Trái Cây Chile', 'Trái Cây Chile', '', '', ''),
(37, 1, 'Trái Cây Chile', 'Trái Cây Chile', '', '', ''),
(32, 2, 'Trái Cây Mỹ', 'Trái Cây Mỹ', '', '', ''),
(32, 1, 'Trái Cây Mỹ', 'Trái Cây Mỹ', '', '', ''),
(33, 2, 'Trái Cây Hàn Quốc', 'Trái Cây Hàn Quốc', '', '', ''),
(33, 1, 'Trái Cây Hàn Quốc', 'Trái Cây Hàn Quốc', '', '', ''),
(34, 2, 'Trái Cây Úc', 'Trái Cây Úc', '', '', ''),
(34, 1, 'Trái Cây Úc', 'Trái Cây Úc', '', '', ''),
(35, 2, 'Trái Cây Canada', 'Trái Cây Canada', '', '', ''),
(35, 1, 'Trái Cây Canada', 'Trái Cây Canada', '', '', ''),
(36, 2, 'Trái Cây New Zealand', 'Trái Cây New Zealand', '', '', ''),
(36, 1, 'Trái Cây New Zealand', 'Trái Cây New Zealand', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(35, 35, 0),
(34, 34, 0),
(33, 33, 0),
(32, 32, 0),
(37, 37, 0),
(36, 36, 0),
(41, 41, 0),
(40, 40, 0),
(39, 39, 0),
(38, 38, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(39, 0),
(40, 0),
(41, 0);