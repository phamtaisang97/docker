-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(11, 'Sản Phẩm Bán Chạy', 0, 1, '', '0'),
(10, 'Sản Phẩm Khuyến Mại', 0, 1, '', '0'),
(9, 'Sản Phẩm Mới', 0, 1, '', '0'),
(12, '1', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, 'https://cdn.bestme.asia/images/x2/thiet-ke-khong-ten-7.png', '', '', '', ''),
(10, 'https://cdn.bestme.asia/images/x2/thiet-ke-khong-ten-5.png', '', '', '', ''),
(11, '', '', '', '', ''),
(12, '', '', '', '', '');