-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(11, 'Kinh Tế', 0, 1, '', '0'),
(10, 'Hồi Kí - Tùy Bút', 0, 1, '', '0'),
(9, 'Kiếm Hiệp - Võ Hiệp', 0, 1, '', '0'),
(12, 'Truyện Trinh Thám', 0, 1, '', '0'),
(13, 'Lịch Sử', 0, 1, '', '0'),
(14, 'Triết Học', 0, 1, NULL, '0'),
(15, 'Văn Học Việt Nam', 0, 1, NULL, '0'),
(16, 'Văn Hóa - Xã Hội', 0, 1, '', '0'),
(17, 'Y Học - Sức Khỏe', 0, 1, '', '0'),
(18, 'Truyện Thiếu Nhi', 0, 1, '', '0'),
(19, 'Truyện Cười', 0, 1, '', '0'),
(8, 'Sản phẩm bán chạy', 0, 1, '', '0'),
(21, 'Sách Truyện Các Loại', 0, 1, '', '1'),
(20, 'Sách về các lĩnh vực', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, 'https://cdn.bestme.asia/images/bookstore/truyen-kiem-hiep-100.jpg', '', '', '', ''),
(10, '', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/bookstore/sach-kinh-te-100.jpg', '', '', '', ''),
(12, 'https://cdn.bestme.asia/images/bookstore/truyen-trinh-tham-100.jpg', '', '', '', ''),
(13, 'https://cdn.bestme.asia/images/bookstore/sach-lich-su-100.jpg', '', '', '', ''),
(14, '', '', '', '', ''),
(15, '', '', '', '', ''),
(16, 'https://cdn.bestme.asia/images/bookstore/sach-van-hoa-xa-hoi-100.jpg', '', '', '', ''),
(17, 'https://cdn.bestme.asia/images/bookstore/sach-y-hoc-100.jpg', '', '', '', ''),
(18, 'https://cdn.bestme.asia/images/bookstore/truyen-thieu-nhi-100.jpg', '', '', '', ''),
(19, 'https://cdn.bestme.asia/images/bookstore/truyen-cuoi-100.jpg', '', '', '', ''),
(8, 'https://cdn.bestme.asia/images/x2/1_Qqu1ktY.jpg', '', '', '', ''),
(21, '', '', '', '', ''),
(20, '', '', '', '', '');