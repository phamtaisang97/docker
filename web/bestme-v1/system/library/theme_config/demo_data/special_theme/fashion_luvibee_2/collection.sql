-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(4, 'Sản phẩm khuyến mại', 0, 1, '', '0'),
(8, 'BST tổng hợp', 0, 1, '', '0'),
(5, 'Sản phẩm hot', 0, 1, '', '0'),
(6, 'Sản phẩm mới', 0, 1, '', '0'),
(7, 'Hàng ngày', 0, 1, NULL, '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(7, '/catalog/view/theme/default/image/fashion_shoeszone/products/86/thumb.jpg', '&lt;p&gt;Bộ sưu tập h&amp;agrave;ng ng&amp;agrave;y&lt;/p&gt;', '', '', ''),
(6, '/catalog/view/theme/default/image/fashion_shoeszone/products/85/thumb.jpg', '&lt;p&gt;nhũng sản phẩm mới nhất c&amp;oacute; mặt tại cửa h&amp;agrave;ng&lt;/p&gt;', '', '', ''),
(5, '/catalog/view/theme/default/image/fashion_shoeszone/products/84/thumb.jpg', '&lt;p&gt;sản phẩm hot&lt;/p&gt;', '', '', ''),
(8, '', '', '', '', ''),
(4, '/catalog/view/theme/default/image/fashion_shoeszone/products/83/thumb.jpg', '&lt;p&gt;sản phẩm khuyến mại&lt;/p&gt;', '', '', '');