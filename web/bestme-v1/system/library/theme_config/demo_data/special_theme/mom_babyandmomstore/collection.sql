-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(13, 'Giặt xả - Tắm gội', 0, 1, '', '0'),
(12, 'Đồ cho bé trai', 0, 1, '', '0'),
(11, 'Đồ cho bé gái', 0, 1, '', '0'),
(10, 'Đồ dùng ăn uống', 0, 1, '', '0'),
(9, 'Xe - Đai - Địu', 0, 1, '', '0'),
(25, 'Trẻ sơ sinh', 0, 1, '', '0'),
(16, 'Mẹ bầu và sau sinh', 0, 1, '', '0'),
(17, 'Nôi - Giường - Cũi', 0, 1, '', '0'),
(26, 'Thời trang cho bé', 0, 1, '', '1'),
(24, 'Sữa và đồ dùng cho bé', 0, 1, '', '0'),
(27, 'Bé an toàn', 0, 1, '', '1'),
(28, 'Dành cho bé', 0, 1, '', '1'),
(29, 'Mẹ và bé', 0, 1, '', '1'),
(30, 'Sản phẩm bán chạy', 0, 1, '', '0');


-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, 'https://cdn.bestme.asia/images/babyandmomstore/bst1100_kkVHLBK.jpg', '', '', '', ''),
(10, 'https://cdn.bestme.asia/images/babyandmomstore/bst8100_USTA45O.jpg', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/babyandmomstore/bst4100_RFSTCWK.jpg', '', '', '', ''),
(12, 'https://cdn.bestme.asia/images/babyandmomstore/bst3100_snPKRrc.jpg', '', '', '', ''),
(13, 'https://cdn.bestme.asia/images/babyandmomstore/bst7100_dY3Imok.jpg', '', '', '', ''),
(25, '', '', '', '', ''),
(16, 'https://cdn.bestme.asia/images/babyandmomstore/bst5100_QH2d2HF.jpg', '', '', '', ''),
(17, 'https://cdn.bestme.asia/images/babyandmomstore/bst6100_9hDDuHm.jpg', '', '', '', ''),
(24, 'https://cdn.bestme.asia/images/babyandmomstore/bst2100_Gzln0ia.jpg', '', '', '', ''),
(26, '', '', '', '', ''),
(27, '', '', '', '', ''),
(28, '', '', '', '', ''),
(29, '', '', '', '', ''),
(30, '', '', '', '', '');
