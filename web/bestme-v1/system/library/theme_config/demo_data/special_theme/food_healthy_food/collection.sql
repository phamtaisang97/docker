-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(45, 'Ăn sạch sống khỏe', 0, 1, '', '1'),
(46, 'Sản phẩm nổi bật', 0, 1, '', '0'),
(44, 'Thực phẩm sạch', 0, 1, '', '1'),
(42, 'Dành cho Eat Clean', 0, 1, '', '0'),
(43, 'Thực phẩm không chất bảo quản', 0, 1, '', '0'),
(36, 'Trái cây tươi', 0, 1, '', '0'),
(37, 'Gia vị, phụ liệu', 0, 1, '', '0'),
(38, 'Phô mai cao cấp', 0, 1, '', '0'),
(39, 'Rau sạch', 0, 1, '', '0'),
(40, 'Rau, củ làm phụ liệu', 0, 1, '', '0'),
(41, 'Dành cho người ăn chay', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(42, 'https://cdn.bestme.asia/images/healthy-food/bst1.jpg', '', '', '', ''),
(43, 'https://cdn.bestme.asia/images/healthy-food/bst6.jpg', '', '', '', ''),
(44, '', '', '', '', ''),
(45, '', '', '', '', ''),
(46, '', '', '', '', ''),
(40, 'https://cdn.bestme.asia/images/healthy-food/bst8.jpg', '', '', '', ''),
(41, 'https://cdn.bestme.asia/images/healthy-food/bst7.jpg', '', '', '', ''),
(36, 'https://cdn.bestme.asia/images/healthy-food/bst5.jpg', '', '', '', ''),
(37, 'https://cdn.bestme.asia/images/healthy-food/bst3.jpg', '', '', '', ''),
(38, 'https://cdn.bestme.asia/images/healthy-food/bst4.jpg', '', '', '', ''),
(39, 'https://cdn.bestme.asia/images/healthy-food/bst2.jpg', '', '', '', '');