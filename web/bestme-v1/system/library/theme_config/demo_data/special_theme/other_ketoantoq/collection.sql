-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(21, 'Dịch vụ chữ ký số', 0, 1, '', '0'),
(22, 'Hóa đơn điện tử', 0, 1, '', '0'),
(23, 'Giải pháp kế toán cho doanh nghiệp', 0, 1, '', '1'),
(20, 'Đào tạo kế toán', 0, 1, '', '0'),
(19, 'Dịch vụ kế toán', 0, 1, '', '0'),
(18, 'Thành lập doanh nghiệp', 0, 1, '', '0'),
(24, 'TH', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(22, 'https://cdn.bestme.asia/images/ketoantoanquoc/bst5_1hTfFU4.jpg', '', '', '', ''),
(23, '', '', '', '', ''),
(24, '', '', '', '', ''),
(18, 'https://cdn.bestme.asia/images/ketoantoanquoc/bst1_X874sSg.jpg', '', '', '', ''),
(19, 'https://cdn.bestme.asia/images/ketoantoanquoc/bst2_GxwTwSj.jpg', '', '', '', ''),
(20, 'https://cdn.bestme.asia/images/ketoantoanquoc/bst3_tKlRkBJ.jpg', '', '', '', ''),
(21, 'https://cdn.bestme.asia/images/ketoantoanquoc/bst4_uVMcR20.jpg', '', '', '', '');