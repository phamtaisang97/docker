-- oc_product PRIMARY KEY (`product_id`)
-- delete first
INSERT IGNORE INTO `oc_product` (`product_id`, `model`, `sku`, `common_barcode`, `common_sku`, `common_compare_price`, `common_price`, `common_cost_price`, `barcode`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `sale_on_out_of_stock`, `stock_status_id`, `image`, `image_alt`, `multi_versions`, `manufacturer_id`, `shipping`, `price`, `price_currency_id`, `compare_price`, `c_price_currency_id`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `channel`, `viewed`, `demo`, `deleted`, `default_store_id`, `date_added`, `date_modified`) VALUES
(149, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/ketoantoanquoc/co-bat-buoc-phai-co-ke-toan-truong_2009143203.jpg', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 1, NULL, 0, '2020-12-11 14:49:07', '2020-12-11 14:49:07'),
(148, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/ketoantoanquoc/accounting-task255b1255d-1582099498465576533045.jpg', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 1, NULL, 0, '2020-12-11 14:48:22', '2020-12-11 14:48:22'),
(147, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/ketoantoanquoc/co-bat-buoc-phai-co-ke-toan-truong_2009143203.jpg', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 1, NULL, 0, '2020-12-11 14:47:24', '2020-12-11 14:47:24'),
(146, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/ketoantoanquoc/unnamed-1.jpg', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 1, NULL, 0, '2020-12-10 16:22:52', '2020-12-11 14:44:54'),
(145, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/ketoantoanquoc/unnamed.jpg', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 1, NULL, 0, '2020-12-10 16:20:08', '2020-12-11 14:44:22'),
(143, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/ketoantoanquoc/unnamed.png', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 1, NULL, 0, '2020-12-10 16:14:36', '2020-12-11 14:44:04'),
(144, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/ketoantoanquoc/financial-accounting-theory.jpeg', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 1, NULL, 0, '2020-12-10 16:17:04', '2020-12-11 14:45:09'),
(142, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/ketoantoanquoc/1.jpg', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 1, NULL, 0, '2020-12-10 16:13:26', '2020-12-11 14:44:31');

-- restore soft-deleted products if demo
UPDATE `oc_product` SET `deleted` = NULL
WHERE `demo` = 1
  AND `product_id` IN (
       149,
       148,
       147,
       146,
       145,
       143,
       144,
       142
  );

-- oc_product_collection PRIMARY KEY (`product_collection_id`)
INSERT IGNORE INTO `oc_product_collection` (`product_collection_id`, `product_id`, `collection_id`, `sort_order`) VALUES
(285, 149, 20, 8),
(284, 148, 20, 7),
(283, 147, 20, 6),
(282, 144, 20, 5),
(281, 146, 20, 4),
(301, 149, 18, 8),
(300, 148, 18, 7),
(299, 147, 18, 6),
(269, 149, 22, 8),
(298, 144, 18, 5),
(297, 146, 18, 4),
(268, 148, 22, 7),
(267, 147, 22, 6),
(266, 144, 22, 5),
(293, 149, 19, 8),
(292, 148, 19, 7),
(265, 146, 22, 4),
(291, 147, 19, 6),
(290, 144, 19, 5),
(289, 146, 19, 4),
(277, 149, 21, 8),
(276, 148, 21, 7),
(275, 147, 21, 6),
(274, 144, 21, 5),
(273, 146, 21, 4),
(272, 142, 21, 3),
(288, 142, 19, 3),
(264, 142, 22, 3),
(296, 142, 18, 3),
(280, 142, 20, 3),
(271, 145, 21, 2),
(287, 145, 19, 2),
(263, 145, 22, 2),
(295, 145, 18, 2),
(279, 145, 20, 2),
(270, 143, 21, 1),
(286, 143, 19, 1),
(262, 143, 22, 1),
(294, 143, 18, 1),
(278, 143, 20, 1);

-- oc_product_description PRIMARY KEY (`product_id`,`language_id`)
INSERT IGNORE INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `sub_description`, `seo_title`, `seo_description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(142, 2, 'Phần mềm kế toán doanh nghiệp SME.NET 2021', '&lt;h2&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;strong&gt;Cung cấp mọi góc nhìn về tình hình tài chính doanh nghiệp&lt;/strong&gt;&lt;/span&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;Nhập liệu tự động và kiểm soát tính hợp lệ từ các giao dịch ngân hàng, hóa đơn, mã số thuế…&lt;br /&gt;\r\nLà một phần mềm kế toán đơn giản, thông minh và đôi lúc thật kỳ diệu.&lt;/p&gt;\r\n\r\n&lt;h3&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;strong&gt;Nâng cao năng suất&lt;/strong&gt;&lt;/span&gt;&lt;/h3&gt;\r\n\r\n&lt;p&gt;Tự động hạch toán từ: Hóa đơn đầu vào, Báo cáo ngân hàng.&lt;/p&gt;\r\n\r\n&lt;p&gt;Tự động tổng hợp báo cáo thuế, BCTC tiết kiệm 80% thời gian nhập liệu.&lt;/p&gt;\r\n\r\n&lt;h3&gt;&lt;strong&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;Kết nối thông minh&lt;/span&gt;&lt;/strong&gt;&lt;/h3&gt;\r\n\r\n&lt;p&gt;Kết nối Tổng cục thuế, Ngân hàng, Hóa đơn điện tử, Chữ ký số, Phần mềm bán hàng… tạo thành hệ sinh thái xử lý dữ liệu nhanh, tiện.&lt;/p&gt;\r\n\r\n&lt;h3&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;strong&gt;Quản trị tài chính tức thời&lt;/strong&gt;&lt;/span&gt;&lt;/h3&gt;\r\n\r\n&lt;p&gt;Giám đốc luôn nắm được tình hình tài chính: Doanh thu, Lợi nhuận, Chi phí, Công nợ,… mọi lúc, mọi nơi kịp thời ra quyết định điều hành.&lt;/p&gt;', '', '', '', '', '', '', ''),
(143, 2, 'Hóa đơn điện tử  meInvoice', '&lt;h2&gt; &lt;/h2&gt;\r\n\r\n&lt;p&gt;Thay đổi hoàn toàn cách thức phát hành, quản lý, báo cáo hóa đơn của Doanh nghiệp bạn!&lt;/p&gt;\r\n\r\n&lt;p&gt;- Đáp ứng đầy đủ nghiệp vụ hóa đơn theo đúng Nghị định 119/2018/NĐ-CP, Thông tư 32/2011/TT-BTC, Thông tư 39/2014/TT-BTC… và sẵn sàng đáp ứng Thông tư 68/2019/TT-BTC&lt;/p&gt;\r\n\r\n&lt;p&gt;- Người mua tức thời nhận được hóa đơn điện tử qua email, SMS và thực hiện phát hành, quản lý, báo cáo hóa đơn mọi lúc, mọi nơi qua mobile, website, desktop&lt;/p&gt;\r\n\r\n&lt;p&gt;- Phần mềm hóa đơn điện tử duy nhất tại Việt Nam ứng dụng công nghệ Blockchain đảm bảo an toàn, bảo mật và chống làm giả hóa đơn&lt;/p&gt;', '', '', '', '', '', '', ''),
(144, 2, 'Dịch vụ thuế điện tử', '&lt;p&gt;&lt;strong&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;Dịch vụ kê khai, nộp thuế điện tử ngay trên phần mềm kế toán&lt;/span&gt;&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;Đã được Tổng Cục Thuế cấp phép&lt;/li&gt;\r\n	&lt;li&gt;Đáp ứng đầy đủ mọi nghiệp vụ kê khai, nộp thuế cho Doanh nghiệp và Cá nhân (doanh nghiệp kê khai hộ)&lt;/li&gt;\r\n	&lt;li&gt;Tích hợp ngay trên phần mềm kế toán MISA tạo thành hệ sinh thái khép kín giúp kê khai, nộp thuế thuận tiện, nhanh chóng hơn&lt;/li&gt;\r\n&lt;/ul&gt;', '', '', '', '', '', '', ''),
(145, 2, 'Dịch vụ chữ ký số eSign', '&lt;h2&gt;ĐÁP ỨNG ĐẦY ĐỦ NGHIỆP VỤ KÝ KẾT VĂN BẢN/HỢP ĐỒNG/GIAO DỊCH ĐIỆN TỬ&lt;/h2&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;Điện tử hóa mọi việc ký kết văn bản, hợp đồng&lt;/li&gt;\r\n	&lt;li&gt;Hệ sinh thái quản trị hiện đại và an toàn&lt;/li&gt;\r\n	&lt;li&gt;Sẵn sàng tích hợp với các hệ thống khác&lt;/li&gt;\r\n	&lt;li&gt;Công nghệ vượt trội – An toàn tuyệt đối&lt;/li&gt;\r\n&lt;/ul&gt;', '', '', '', '', '', '', ''),
(146, 2, 'Dịch vụ ngân hàng điện tử Bankhub', '&lt;p&gt;BANKHUB Phần mềm cho phép kế toán ngồi tại văn phòng cũng có thể thực hiện đầy đủ giao dịch với ngân hàng ngay trên dịch vụ kế toán.&lt;/p&gt;\r\n\r\n&lt;p&gt;CHUYỂN TIỀN TRỰC TUYẾN NGAY TRÊN PHẦN MỀM&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;Cho phép kế toán viên lập, kế toán trưởng phê duyệt lệnh chuyển tiền ngay trên phần mềm&lt;/li&gt;\r\n	&lt;li&gt;Thực hiện được các loại Ủy nhiệm chi cho nhà cung cấp, thông thường hoặc chuyển nội bộ&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p&gt;TRA CỨU SỐ DƯ TÀI KHOẢN NHANH CHÓNG VÀ CHÍNH XÁC&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;Kế toán dễ dàng tra cứu số dư tài khoản thanh toán ngay trên phần mềm&lt;/li&gt;\r\n	&lt;li&gt;Tiết kiệm thời gian và thao tác kiểm tra, xác minh tài khoản&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p&gt;DỄ DÀNG TRA CỨU LỊCH SỬ GIAO DỊCH&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;Tra cứu nhanh lịch sử giao dịch của tài khoản trong một khoảng thời gian&lt;/li&gt;\r\n	&lt;li&gt;Tiết kiệm thời gian tra cứu tại ngân hàng&lt;/li&gt;\r\n	&lt;li&gt;Lập nhanh UNC, Chứng từ thu tiền gửi từ những giao dịch thu, chi tiền không thực hiện trên phần mềm  SME.NET 2020&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p&gt;THUẬN TIỆN QUẢN LÝ LỆNH CHUYỂN TIỀN&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;Dễ dàng xem thông tin lệnh chuyển tiền&lt;/li&gt;\r\n	&lt;li&gt;Nhanh chóng phê duyệt lệnh chuyển tiền&lt;/li&gt;\r\n	&lt;li&gt;Tức thời thu hồi lệnh chuyển tiền&lt;/li&gt;\r\n	&lt;li&gt;Nắm bắt nhật ký giao dịch lệnh chuyển tiền&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p&gt;THANH TOÁN LƯƠNG TRỰC TUYẾN&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;Chuyển tiền lương cho nhân viên&lt;/li&gt;\r\n	&lt;li&gt;Phê duyệt lương từ kế toán trưởng và giám đốc ngay trên phần mềm&lt;/li&gt;\r\n	&lt;li&gt;Không phải mất công nhập liệu 2 lần&lt;/li&gt;\r\n	&lt;li&gt;Tự động hạch toán dễ dàng ngay trên phần mềm&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p&gt; &lt;/p&gt;', '', '', '', '', '', '', ''),
(147, 2, 'Dịch vụ làm báo cáo tài chính tại Dương Kinh Hải Phòng', '', '', '', '', '', '', '', ''),
(148, 2, 'Dịch vụ kế toán trọn gói tại Bắc Ninh', '', '', '', '', '', '', '', ''),
(149, 2, 'Dịch vụ kế toán trọn gói tại Bắc Giang', '', '', '', '', '', '', '', '');

-- oc_product_image PRIMARY KEY (`product_image_id`)
INSERT IGNORE INTO `oc_product_image` (`product_image_id`, `product_id`, `image`, `image_alt`, `sort_order`) VALUES
(144, 142, 'https://cdn.bestme.asia/images/ketoantoanquoc/unnamed.png', '', 0);

-- oc_product_to_category PRIMARY KEY (`product_id`,`category_id`)
-- nothing

-- oc_product_to_store PRIMARY KEY (`product_id`,`store_id`)
INSERT IGNORE INTO `oc_product_to_store` (`product_id`, `store_id`, `product_version_id`, `quantity`, `cost_price`) VALUES
(149, 0, 0, 0, '0.0000'),
(148, 0, 0, 0, '0.0000'),
(145, 0, 0, 0, '0.0000'),
(147, 0, 0, 0, '0.0000'),
(146, 0, 0, 0, '0.0000'),
(144, 0, 0, 0, '0.0000'),
(142, 0, 0, 0, '0.0000'),
(143, 0, 0, 0, '0.0000');
