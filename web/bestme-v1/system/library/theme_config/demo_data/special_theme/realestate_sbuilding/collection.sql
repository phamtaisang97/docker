-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(10, 'Bất Động Sản Nổi Bật Cần Bán', 0, 1, '', '0'),
(8, 'Dự Án Nối Bật', 0, 1, '', '0'),
(9, 'Tin Rao Bât Động Sản', 0, 1, '', '0'),
(11, 'Căn hộ S1', 0, 1, '', '0'),
(12, 'Căn hộ S2', 0, 1, '', '0'),
(13, 'Căn hộ S3', 0, 1, '', '0'),
(14, 'Căn hộ S4', 0, 1, '', '0'),
(15, 'x1', 0, 1, '', '1'),
(16, 'x2', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(13, 'https://cdn.bestme.asia/images/x2/bst3.png', '&lt;p&gt;2&lt;/p&gt;', '', '', ''),
(14, 'https://cdn.bestme.asia/images/x2/bst4_dHJJnFD.png', '&lt;p&gt;d&lt;/p&gt;', '', '', ''),
(10, '', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/x2/bst1.png', '&lt;p&gt;s1&lt;/p&gt;', '', '', ''),
(12, 'https://cdn.bestme.asia/images/x2/bst2.png', '&lt;p&gt;s&lt;/p&gt;', '', '', ''),
(8, 'https://cdn.bestme.asia/images/x2/tai-xuong-7.jpg', '', '', '', ''),
(9, 'https://cdn.bestme.asia/images/x2/tai-xuong-6.jpg', '', '', '', ''),
(15, '', '', '', '', ''),
(16, '', '', '', '', '');