-- oc_seo_url PRIMARY KEY (`seo_url_id`)
INSERT IGNORE INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`, `table_name`) VALUES
(2083, 0, 2, 'category_id=12', 'd-cat-giay-co-dien', 'category'),
(2142, 0, 2, 'collection=16', 'x2', 'collection'),
(2140, 0, 2, 'collection=14', 'can-ho-s4', 'collection'),
(2141, 0, 2, 'collection=15', 'x1', 'collection'),
(2139, 0, 2, 'collection=13', 'can-ho-s3', 'collection'),
(2138, 0, 2, 'collection=12', 'can-ho-s2', 'collection'),
(2137, 0, 2, 'collection=11', 'can-ho-s1', 'collection'),
(2133, 0, 1, 'product_id=108', 'chuyen-nhuong-gap-9-can-thuoc-chung-cu-an-binh', 'product'),
(2131, 0, 1, 'category_id=22', 'tin-rao-danh-cho-ban', 'category'),
(2132, 0, 2, 'product_id=108', 'chuyen-nhuong-gap-9-can-thuoc-chung-cu-an-binh', 'product'),
(2130, 0, 2, 'category_id=22', 'tin-rao-danh-cho-ban', 'category'),
(2128, 0, 2, 'product_id=107', 'phu-dien-residences', 'product'),
(2129, 0, 1, 'product_id=107', 'phu-dien-residences', 'product'),
(2127, 0, 1, 'product_id=106', 'khu-do-thi-moi-phu-cuong', 'product'),
(2126, 0, 2, 'product_id=106', 'khu-do-thi-moi-phu-cuong', 'product'),
(2125, 0, 1, 'product_id=105', 'my-khe-angkora-park', 'product'),
(2123, 0, 1, 'product_id=104', 'maris-city', 'product'),
(2124, 0, 2, 'product_id=105', 'my-khe-angkora-park', 'product'),
(2121, 0, 1, 'category_id=21', 'du-an-noi-bat', 'category'),
(2122, 0, 2, 'product_id=104', 'maris-city', 'product'),
(2120, 0, 2, 'category_id=21', 'du-an-noi-bat', 'category'),
(2119, 0, 2, 'blog=3', 'su-kien-duoc-quan-tam-nhat-bat-dong-san-viet-nam', 'blog'),
(2117, 0, 2, 'blog=1', 'bao-chi-noi-ve-bat-dong-san', 'blog'),
(2118, 0, 2, 'blog=2', 'bat-dong-san-va-nhung-cai-tien-cong-nghe', 'blog'),
-- (1000, 0, 1, 'page_id=1000', 'chinh-sach-va-bao-mat', ''),
-- (1001, 0, 2, 'page_id=1000', 'chinh-sach-va-bao-mat', ''),
-- (2093, 0, 1, 'common/home', 'home', ''),
-- (2094, 0, 2, 'common/home', 'trang-chu', ''),
-- (2095, 0, 1, 'common/shop', 'products', ''),
-- (2096, 0, 2, 'common/shop', 'san-pham', ''),
-- (2097, 0, 1, 'contact/contact', 'contact', ''),
-- (2098, 0, 2, 'contact/contact', 'lien-he', ''),
-- (2099, 0, 1, 'checkout/profile', 'profile', ''),
-- (2100, 0, 2, 'checkout/profile', 'tai-khoan', ''),
-- (2101, 0, 1, 'account/login', 'login', ''),
-- (2102, 0, 2, 'account/login', 'dang-nhap', ''),
-- (2103, 0, 1, 'account/register', 'register', ''),
-- (2104, 0, 2, 'account/register', 'dang-ky', ''),
-- (2105, 0, 1, 'account/logout', 'logout', ''),
-- (2106, 0, 2, 'account/logout', 'dang-xuat', ''),
-- (2107, 0, 1, 'checkout/setting', 'setting', ''),
-- (2108, 0, 2, 'checkout/setting', 'cai-dat', ''),
-- (2109, 0, 1, 'checkout/my_orders', 'checkout-cart', ''),
-- (2110, 0, 2, 'checkout/my_orders', 'gio-hang', ''),
-- (2111, 0, 1, 'checkout/order_preview', 'payment', ''),
-- (2112, 0, 2, 'checkout/order_preview', 'thanh-toan', ''),
-- (2113, 0, 1, 'checkout/order_preview/orderSuccess', 'payment-success', ''),
-- (2114, 0, 2, 'checkout/order_preview/orderSuccess', 'dat-hang-thanh-cong', ''),
-- (2115, 0, 1, 'blog/blog', 'blog', ''),
-- (2116, 0, 2, 'blog/blog', 'bai-viet', ''),
(2084, 0, 2, 'category_id=14', 'd-cat-giay-cao-got', 'category'),
(2085, 0, 2, 'category_id=17', 'd-cat-giay-valen', 'category'),
(2086, 0, 2, 'category_id=18', 'd-cat-giay-sandal', 'category'),
(2087, 0, 2, 'category_id=19', 'd-cat-dep-sandal', 'category'),
(2088, 0, 2, 'category_id=20', 'd-cat-giay-dup', 'category'),
(2134, 0, 2, 'collection=8', 'du-an-noi-bat-1', 'collection'),
(2135, 0, 2, 'collection=9', 'tin-rao-bat-dong-san', 'collection'),
(2136, 0, 2, 'collection=10', 'bat-dong-san-noi-bat-can-ban', 'collection');