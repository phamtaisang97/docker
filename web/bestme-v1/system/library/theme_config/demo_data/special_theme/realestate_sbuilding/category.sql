-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(20, NULL, 0, 0, 0, 0, 1, '2019-05-21 16:20:38', '2019-05-21 16:20:38'),
(19, NULL, 0, 0, 0, 0, 1, '2019-05-21 15:47:12', '2019-05-21 15:47:12'),
(18, NULL, 0, 0, 0, 0, 1, '2019-05-21 15:44:23', '2019-05-21 15:44:23'),
(17, NULL, 0, 0, 0, 0, 1, '2019-05-21 15:36:21', '2019-05-21 15:36:21'),
(14, NULL, 0, 0, 0, 0, 1, '2019-05-20 17:45:50', '2019-05-20 17:45:50'),
(12, NULL, 0, 0, 0, 0, 1, '2019-05-20 16:48:18', '2019-05-20 16:48:18'),
(21, NULL, 0, 0, 0, 0, 1, '2020-05-22 14:17:29', '2020-05-22 14:17:29'),
(22, NULL, 0, 0, 0, 0, 1, '2020-05-22 16:45:11', '2020-05-22 16:45:11');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(18, 2, 'Giày sandal', 'Giày sandal', '', '', ''),
(18, 1, 'Giày sandal', 'Giày sandal', '', '', ''),
(17, 2, 'Giày valen', 'Giày valen', '', '', ''),
(17, 1, 'Giày valen', 'Giày valen', '', '', ''),
(14, 1, 'Giày cao gót', 'Giày cao gót', '', '', ''),
(14, 2, 'Giày cao gót', 'Giày cao gót', '', '', ''),
(12, 1, 'Giày cổ điển', 'Giày cổ điển', '', '', ''),
(12, 2, 'Giày cổ điển', 'Giày cổ điển', '', '', ''),
(19, 2, 'Dép sandal', 'Dép sandal', '', '', ''),
(19, 1, 'Dép sandal', 'Dép sandal', '', '', ''),
(20, 2, 'Giày đúp', 'Giày đúp', '', '', ''),
(20, 1, 'Giày đúp', 'Giày đúp', '', '', ''),
(21, 2, 'Dự Án Nổi Bật', 'Dự Án Nổi Bật', '', '', ''),
(21, 1, 'Dự Án Nổi Bật', 'Dự Án Nổi Bật', '', '', ''),
(22, 2, 'Tin Rao Dành Cho Bạn', 'Tin Rao Dành Cho Bạn', '', '', ''),
(22, 1, 'Tin Rao Dành Cho Bạn', 'Tin Rao Dành Cho Bạn', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(20, 20, 0),
(19, 19, 0),
(18, 18, 0),
(17, 17, 0),
(14, 14, 0),
(12, 12, 0),
(21, 21, 0),
(22, 22, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(12, 0),
(14, 0),
(17, 0),
(18, 0),
(19, 0),
(20, 0),
(21, 0),
(22, 0);