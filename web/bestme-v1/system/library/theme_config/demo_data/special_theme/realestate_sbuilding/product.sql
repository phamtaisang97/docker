-- oc_product PRIMARY KEY (`product_id`)
INSERT IGNORE INTO `oc_product` (`product_id`, `model`, `sku`, `common_barcode`, `common_sku`, `common_compare_price`, `common_price`, `common_cost_price`, `barcode`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `sale_on_out_of_stock`, `stock_status_id`, `image`, `image_alt`, `multi_versions`, `manufacturer_id`, `shipping`, `price`, `price_currency_id`, `compare_price`, `c_price_currency_id`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `channel`, `viewed`, `demo`, `deleted`, `default_store_id`, `user_create_id`, `date_added`, `date_modified`) VALUES
(83, '', '', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 350, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/83/thumb.jpg', '', 0, 1, 1, '0.0000', 1, '204000.0000', 1, 0, 0, '0000-00-00', '208.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2019-05-20 16:03:08', '2019-05-20 16:03:08'),
(84, '', '', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 320, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/84/thumb.jpg', '', 0, 6, 1, '0.0000', 1, '200000.0000', 1, 0, 0, '0000-00-00', '208.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2019-05-20 16:07:02', '2019-05-20 16:07:02'),
(85, '', 'HUKW3943JJJ', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 316, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/85/thumb.jpg', '', 0, 1, 1, '210000.0000', 1, '360000.0000', 1, 0, 0, '0000-00-00', '325.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2019-05-20 16:12:44', '2019-05-20 16:12:44'),
(86, '', 'HUKW3943888', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 400, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/86/thumb.jpg', '', 0, 1, 1, '150000.0000', 1, '200000.0000', 1, 0, 0, '0000-00-00', '325.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2019-05-20 16:22:09', '2019-05-20 16:22:09'),
(87, '', 'MREE2SA', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 350, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/87/thumb.jpg', '', 0, 1, 1, '130000.0000', 1, '220000.0000', 1, 0, 0, '0000-00-00', '2500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2019-05-20 16:35:06', '2019-05-20 16:35:06'),
(88, '', 'S510UN', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 345, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/88/thumb.jpg', '', 0, 8, 1, '200000.0000', 1, '320000.0000', 1, 0, 0, '0000-00-00', '2200.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2019-05-20 16:38:08', '2019-05-20 16:38:08'),
(97, '', '14IKB', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 325, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/97/thumb.jpg', '', 0, 7, 1, '199000.0000', 1, '360000.0000', 1, 0, 0, '0000-00-00', '2100.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2019-05-20 16:40:29', '2019-05-20 16:40:29'),
(98, '', 'SKU215', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 420, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/98/thumb.jpg', '', 0, 6, 1, '230000.0000', 1, '390000.0000', 1, 0, 0, '0000-00-00', '750.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2019-05-21 15:57:29', '2019-05-21 15:57:29'),
(99, '', 'SKU1999', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 350, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/99/thumb.jpg', '', 0, 2, 1, '190000.0000', 1, '340000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2019-05-21 16:03:41', '2019-05-21 16:03:41'),
(103, '', 'SKU2991', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 400, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/103/thumb.jpg', '', 0, 25, 1, '0.0000', 1, '170000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2019-05-21 16:13:09', '2019-05-21 16:13:09'),
(104, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/x2/tai-xuong_4Wsnc3Y.jpg', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '1.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 0, NULL, 0, 1, '2020-05-22 14:17:29', '2020-05-22 14:17:29'),
(105, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/x2/tai-xuong-4.jpg', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '1.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 0, NULL, 0, 1, '2020-05-22 16:35:39', '2020-05-22 16:35:39'),
(106, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/x2/tai-xuong-2_YOlECRW.jpg', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '10.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 0, NULL, 0, 1, '2020-05-22 16:36:37', '2020-05-22 16:36:37'),
(107, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/x2/tai-xuong-6.jpg', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '1.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 0, NULL, 0, 1, '2020-05-22 16:37:34', '2020-05-22 16:37:34'),
(108, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/x2/20190507115455-76ecwm.jpg', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '1.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 0, NULL, 0, 1, '2020-05-22 16:45:11', '2020-05-22 16:45:11');

-- restore soft-deleted products if demo
UPDATE `oc_product` SET `deleted` = NULL
WHERE `demo` = 1
  AND `product_id` IN (
      83,
      84,
      85,
      86,
      87,
      88,
      97,
      98,
      99,
      103,
      104,
      105,
      106,
      107,
      108
  );

-- oc_product_collection PRIMARY KEY (`product_collection_id`)
INSERT IGNORE INTO `oc_product_collection` (`product_collection_id`, `product_id`, `collection_id`, `sort_order`) VALUES
(150, 106, 14, 0),
(149, 106, 13, 0),
(148, 106, 12, 0),
(147, 108, 11, 0),
(146, 107, 10, 0),
(145, 105, 10, 0),
(144, 104, 10, 0),
(143, 106, 10, 0),
(142, 104, 9, 0),
(141, 105, 9, 0),
(140, 106, 9, 0),
(139, 108, 9, 0),
(138, 107, 8, 0),
(137, 105, 8, 0),
(136, 104, 8, 0),
(135, 106, 8, 0);

-- oc_product_description PRIMARY KEY (`product_id`,`language_id`)
INSERT IGNORE INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `sub_description`, `seo_title`, `seo_description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(83, 2, 'Giày cổ điển sang trọng màu hồng', 'Giày cổ điển sang trọng màu hồng sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(84, 2, 'Giày cao gót gót kim sa xù độc lạ', '- Cao 8cm - Màu đen và nâu cafe sang trọng - Là kiểu giày mang phong cách cổ điển sang trọng, dễ phối đồ. Đi tiệc hay đi làm đều rất sang. - Size 35-39', '', '', '', '', '', '', ''),
(85, 2, 'Giày cao gót quai trong phối đinh', 'Giày cao gót quai trong phối đinh sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(86, 2, 'Giày cao gót dép đen nơ', 'Giày cao gót dép đen nơ sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(87, 2, 'Giày valen viền đinh size 36 37 38', 'Giày valen viền đinh size 36 37 38 sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(88, 2, 'Giày sandal cao gót quai trong gót kim sa size 34 đến 40', 'Giày sandal cao gót quai trong gót kim sa size 34 đến 40 sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(97, 2, 'Giày sandal quai ngang ánh 7 màu', 'Giày sandal quai ngang ánh 7 màu sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(98, 2, 'Dép sandal xoắn cổ chân', 'Dép sandal xoắn cổ chân sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(99, 2, 'Giày sandal chiến binh dây kéo 1 bên', 'Giày sandal chiến binh dây kéo 1 bên sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(103, 2, 'Giày đúp cao 11cm', 'Giày đúp cao 11cm sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(104, 2, 'Maris city', '&lt;p&gt;Tên dự án&lt;/p&gt;\r\n\r\n&lt;p&gt;Maris City&lt;/p&gt;\r\n\r\n&lt;p&gt;Địa chỉ&lt;/p&gt;\r\n\r\n&lt;p&gt;Đường Nguyễn Tri Phương, Phường Chánh Lộ, Quảng Ngãi, Quảng Ngãi&lt;/p&gt;\r\n\r\n&lt;p&gt;Tổng diện tích&lt;/p&gt;\r\n\r\n&lt;p&gt;96.000 m²&lt;/p&gt;\r\n\r\n&lt;p&gt;Loại hình phát triển&lt;/p&gt;\r\n\r\n&lt;p&gt;Dự án khác&lt;/p&gt;', '', '', '', '', '', '', ''),
(105, 2, 'MỸ KHÊ ANGKORA PARK', '&lt;p&gt;Tên dự án&lt;/p&gt;\r\n\r\n&lt;p&gt;Mỹ Khê Angkora Park&lt;/p&gt;\r\n\r\n&lt;p&gt;Địa chỉ&lt;/p&gt;\r\n\r\n&lt;p&gt;Đường Mỹ Trà Mỹ Khê, Xã Tịnh Khê, Quảng Ngãi, Quảng Ngãi&lt;/p&gt;\r\n\r\n&lt;p&gt;Tổng diện tích&lt;/p&gt;\r\n\r\n&lt;p&gt;122.000 m²&lt;/p&gt;\r\n\r\n&lt;p&gt;Loại hình phát triển&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu đô thị mới&lt;/p&gt;', '&lt;p&gt;Tên dự án&lt;/p&gt;\r\n\r\n&lt;p&gt;Mỹ Khê Angkora Park&lt;/p&gt;\r\n\r\n&lt;p&gt;Địa chỉ&lt;/p&gt;\r\n\r\n&lt;p&gt;Đường Mỹ Trà Mỹ Khê, Xã Tịnh Khê, Quảng Ngãi, Quảng Ngãi&lt;/p&gt;\r\n\r\n&lt;p&gt;Tổng diện tích&lt;/p&gt;\r\n\r\n&lt;p&gt;122.000 m²&lt;/p&gt;\r\n\r\n&lt;p&gt;Loại hình phát triển&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu đô thị mới&lt;/p&gt;', '', '', '', '', '', ''),
(106, 2, 'KHU ĐÔ THỊ MỚI PHÚ CƯỜNG', '&lt;p&gt;Tên dự án&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu Đô Thị Mới Phú Cường&lt;/p&gt;\r\n\r\n&lt;p&gt;Chủ đầu tư&lt;/p&gt;\r\n\r\n&lt;p&gt;CÔNG TY CPĐT PHÚ CƯỜNG KIÊN GIANG&lt;/p&gt;\r\n\r\n&lt;p&gt;Địa chỉ&lt;/p&gt;\r\n\r\n&lt;p&gt;TP.Rạch Giá, Kiên Giang&lt;/p&gt;\r\n\r\n&lt;p&gt;Tổng diện tích&lt;/p&gt;\r\n\r\n&lt;p&gt;1.660.000 m²&lt;/p&gt;\r\n\r\n&lt;p&gt;Loại hình phát triển&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu đô thị mới&lt;/p&gt;', '', '', '', '', '', '', ''),
(107, 2, 'Phú Điền Residences', '&lt;p&gt;Tên dự án&lt;/p&gt;\r\n\r\n&lt;p&gt;Phú Điền Residences&lt;/p&gt;\r\n\r\n&lt;p&gt;Địa chỉ&lt;/p&gt;\r\n\r\n&lt;p&gt;ĐT624, Xã Nghĩa Điền, Tư Nghĩa, Quảng Ngãi&lt;/p&gt;\r\n\r\n&lt;p&gt;Tổng diện tích&lt;/p&gt;\r\n\r\n&lt;p&gt;122.000 m²&lt;/p&gt;\r\n\r\n&lt;p&gt;Loại hình phát triển&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu đô thị mới&lt;/p&gt;\r\n\r\n&lt;p&gt;Quy mô dự án&lt;/p&gt;\r\n\r\n&lt;p&gt;409 lô biệt thự, liền kề&lt;/p&gt;', '', '', '', '', '', '', ''),
(108, 2, 'Chuyển nhượng gấp 9 căn thuộc chung cư An Bình', '&lt;p&gt;Tháp A8:&lt;br /&gt;\r\n- Căn 5 tháp A8, loại căn hộ A1, diện tích 74,7m2, căn 2 phòng ngủ đều có ánh sáng và gió tự nhiên, cửa hướng Nam, ban công hướng Bắc view khu biệt thự và hồ điều hòa.&lt;br /&gt;\r\nGiá bán: 2,4 tỷ/căn.&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Căn 11 tháp A8, loại căn hộ B5, diện tích 86,5m2, cửa chính Đông ban công chính Tây và chính Nam view công viên và hồ điều hòa 16ha, thiết kế thông minh tất cả các phòng đều có ánh sáng và gió tự nhiên.&lt;br /&gt;\r\nGiá bán: Tổng giá trị căn hộ 2,7tỷ.&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Căn 01 tháp A8, loại căn hộ C1, diện tích 114,5m2, cửa vào chính Nam, ban công chính Bắc và chính Tây, view hồ điều hòa và khu biệt thự tuyệt đẹp.&lt;br /&gt;\r\nGiá bán: Căn hộ 3 tỷ.&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Căn 03 tháp A8, loại căn hộ B1, diện tích 90,6m2, cửa vào chính Nam, ban công chính Bắc, view hồ, khu biệt thự và chung cư Green Stars.&lt;br /&gt;\r\nGiá bán: 2,85 tỷ/căn.&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Căn 10 tháp A8, loại căn hộ B4, diện tích 83,7m2, ban công chính Đông và Nam, cửa vào chính Bắc, view quảng trường và bể bơi rất đẹp.&lt;br /&gt;\r\nGiá bán: 2,9 tỷ.&lt;br /&gt;\r\n&lt;br /&gt;\r\nTháp A7:&lt;br /&gt;\r\n- Căn 5 tháp A7, loại căn hộ A1, diện tích 74,7m2, căn 2 phòng ngủ đều có ánh sáng và gió tự nhiên, cửa hướng Bắc, ban công chính Nam view quảng trường tuyệt đẹp.&lt;br /&gt;\r\nGiá bán: 2.5 tỷ/căn.&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Căn 02 tháp A7, loại căn hộ C2, diện tích 114,5m2, căn góc, cửa vào chính Nam, ban công chính Tây và Bắc, view hồ và công viên cây xanh đẹp.&lt;br /&gt;\r\nGiá bán: 3 tỷ.&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Căn 09 tháp A7, loại căn hộ B4, diện tích 83,7m2, căn góc, ban công chính Đông và Nam, cửa vào chính Bắc, view 2 phía quảng trường và bể bơi.&lt;br /&gt;\r\nGiá bán: 2,75 tỷ.&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Căn 04 tháp A7, loại căn hộ B2, diện tích 90,6m2, ban công chính Bắc, cửa vào chính Nam, vị trí view vườn hoa và hồ điều hòa.&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Căn 12 tháp A7, loại căn hộ B6, diện tích 86,5m2, ban công chính Đông và Bắc, cửa vào chính Tây, view bể bơi và quảng trường.&lt;br /&gt;\r\nNgoài ra tôi còn nhiều căn khác với giá bán hợp lý.&lt;br /&gt;\r\n&lt;br /&gt;\r\n2. Tiến độ thanh toán:&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Đợt 1: 95% nhận ngay nhà mới.&lt;br /&gt;\r\n- Đợt 2: 5% khi cấp giấy chứng nhận quyền sử dụng đất.&lt;br /&gt;\r\n&lt;br /&gt;\r\n3. Điểm nổi bật dự án:&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Thiết kế căn hộ thông minh, diện tích hợp lý tất cả các phòng đều có ánh sáng và đón gió tự nhiên.&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Cư dân sống nơi đây sẽ được sử dụng một quần thể rộng với kiến trúc hiện đại theo đó là không gian sống trong lành.&lt;br /&gt;\r\n&lt;br /&gt;\r\n- Dịch vụ hoàn hảo có bể bơi bốn mùa, khu tập gym, spa... Và trung tâm thương mại đảm bảo cho cư dân nơi đây có một cuộc sống hiện đại.&lt;br /&gt;\r\n &lt;/p&gt;', '', '', '', '', '', '', '');

-- oc_product_image PRIMARY KEY (`product_image_id`)
INSERT IGNORE INTO `oc_product_image` (`product_image_id`, `product_id`, `image`, `image_alt`, `sort_order`) VALUES
(139, 83, '/catalog/view/theme/default/image/fashion_shoeszone/products/83/image1.jpg', '', 1),
(138, 84, '/catalog/view/theme/default/image/fashion_shoeszone/products/84/thumb.jpg', '', 0),
(133, 85, '/catalog/view/theme/default/image/fashion_shoeszone/products/85/image1.jpg', '', 0),
(132, 86, '/catalog/view/theme/default/image/fashion_shoeszone/products/86/thumb.jpg', '', 1),
(131, 87, '/catalog/view/theme/default/image/fashion_shoeszone/products/87/image1.jpg', '', 0),
(117, 88, '/catalog/view/theme/default/image/fashion_shoeszone/products/88/image1.jpg', '', 0),
(118, 97, '/catalog/view/theme/default/image/fashion_shoeszone/products/97/thumb.jpg', '', 0),
(119, 98, '/catalog/view/theme/default/image/fashion_shoeszone/products/98/image2.jpg', '', 1),
(126, 99, '/catalog/view/theme/default/image/fashion_shoeszone/products/99/image1.jpg', '', 0),
(127, 103, '/catalog/view/theme/default/image/fashion_shoeszone/products/103/image1.jpg', '', 1);

-- oc_product_to_category PRIMARY KEY (`product_id`,`category_id`)
INSERT IGNORE INTO `oc_product_to_category` (`product_id`, `category_id`) VALUES
(104, 21),
(105, 21),
(106, 21),
(107, 21),
(108, 22);

-- oc_product_to_store PRIMARY KEY (`product_id`,`store_id`)
INSERT IGNORE INTO `oc_product_to_store` (`product_id`, `store_id`, `product_version_id`, `quantity`, `cost_price`) VALUES
(108, 0, 0, 0, '0.0000'),
(107, 0, 0, 0, '0.0000'),
(106, 0, 0, 0, '0.0000'),
(105, 0, 0, 0, '0.0000'),
(104, 0, 0, 0, '0.0000');