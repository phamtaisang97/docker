-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(12, 'Máy Massage Giảm Cân', 0, 1, '', '0'),
(9, 'Máy Tập Thể Hình', 0, 1, '', '0'),
(10, 'Whey Protein', 0, 1, '', '0'),
(11, 'Phụ Kiện', 0, 1, '', '0'),
(14, 'Sản phẩm mới', 0, 1, '', '0'),
(15, 'Sản phẩm bán chạy', 0, 1, '', '0'),
(16, 'BST Tổng Hợp', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(14, '', '&lt;p&gt;Tạo mẫu&lt;/p&gt;', '', '', ''),
(12, 'https://cdn.bestme.asia/images/x2/11-100.jpg', '&lt;p&gt;Tạo mẫu&lt;/p&gt;', '', '', ''),
(11, 'https://cdn.bestme.asia/images/x2/13-10028159783191829.jpg', '&lt;p&gt;Tạo mẫu&lt;/p&gt;', '', '', ''),
(9, 'https://cdn.bestme.asia/images/x2/1-100_tINxOfB.jpg', '&lt;p&gt;Chuyên cung cấp các loại máy chính hãng&lt;/p&gt;', '', '', ''),
(10, 'https://cdn.bestme.asia/images/x2/12-10028159783191429.jpg', '&lt;p&gt;Tạo mẫu&lt;/p&gt;', '', '', ''),
(15, '', '&lt;p&gt;Tạo mẫu&lt;/p&gt;', '', '', ''),
(16, '', '', '', '', '');