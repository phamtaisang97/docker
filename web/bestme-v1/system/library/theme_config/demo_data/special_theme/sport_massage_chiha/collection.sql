-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(9, 'Hỗ trợ, hồi phục cơ bắp', 0, 1, '', '0'),
(10, 'Thư giãn, giải tỏa căng thẳng', 0, 1, '', '0'),
(5, 'Sản phẩm hot', 0, 1, '', '0'),
(11, 'Thiết bị cao cấp', 0, 1, '', '0'),
(12, 'Xua tan stress cho dân công sở', 0, 1, '', '0'),
(13, 'Ưu đãi Black Friday', 0, 1, '', '0'),
(16, 'SIÊU KHUYẾN MÃI', 0, 1, '', '0'),
(17, 'BLACK FRIDAY', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, '', '', '', '', ''),
(10, '', '', '', '', ''),
(11, '', '', '', '', ''),
(12, '', '', '', '', ''),
(17, 'https://cdn.bestme.asia/images/ghemassagechinhhang/slide1.jpg', '', '', '', ''),
(5, 'https://res.cloudinary.com/novaon-x2/image/upload/v1555900690/x2-8787.bestme.test/dell-vostro-5568-077m52-vangdong-450x300-450x300-600x600.png', '&lt;p&gt;sản phẩm hot&lt;/p&gt;', '', '', ''),
(16, 'https://cdn.bestme.asia/images/ghemassagechinhhang/slide2.jpg', '', '', '', ''),
(13, 'https://cdn.bestme.asia/images/ghemassagechinhhang/bst22.jpg', '', '', '', '');