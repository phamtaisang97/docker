-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(33, '', 0, 0, 0, 2, 1, '2020-12-08 10:12:22', '2020-12-08 10:43:49'),
(32, '', 0, 0, 0, 1, 1, '2020-12-08 10:12:08', '2020-12-08 10:41:33');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(32, 2, 'Ghế Massage', 'Ghế Massage', '', '', ''),
(32, 1, 'Ghế Massage', 'Ghế Massage', '', '', ''),
(33, 2, 'Dụng Cụ Massage', 'Dụng Cụ Massage', '', '', ''),
(33, 1, 'Dụng Cụ Massage', 'Dụng Cụ Massage', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(33, 33, 0),
(32, 32, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0);