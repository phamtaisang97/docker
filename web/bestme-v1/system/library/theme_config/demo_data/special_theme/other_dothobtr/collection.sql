-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(11, 'Đỉnh Đồng - Lư Hương', 0, 1, '', '0'),
(10, 'Bàn Thờ', 0, 1, '', '0'),
(9, 'Bộ Đồ Thờ', 0, 1, '', '0'),
(12, 'Đồ Thờ', 0, 1, '', '0'),
(13, 'Đồ Vật Phong Thủy', 0, 1, '', '0'),
(14, 'Tượng Thờ', 0, 1, '', '0'),
(15, 'Đồ Thờ Bát Tràng', 0, 1, '', '1'),
(16, 'Đồ Thờ Bán Chạy', 0, 1, '', '1'),
(17, 'Đồ Thờ Phong Thủy', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, 'https://cdn.bestme.asia/images/x2/bst1_si9SyGI.jpg', '', '', '', ''),
(10, 'https://cdn.bestme.asia/images/x2/bst6_OKsC4YM.jpg', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/x2/bst2_S7ykWrS.jpg', '', '', '', ''),
(12, 'https://cdn.bestme.asia/images/x2/bst3_Wzco9rX.jpg', '', '', '', ''),
(13, 'https://cdn.bestme.asia/images/x2/bst4_Lq8i40H.jpg', '', '', '', ''),
(14, 'https://cdn.bestme.asia/images/x2/bst5_NfAtwge.jpg', '', '', '', ''),
(15, '', '', '', '', ''),
(16, '', '', '', '', ''),
(17, '', '', '', '', '');