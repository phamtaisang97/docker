-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(9, 'Giàn tạ đa năng', 0, 1, '', '0'),
(10, 'Xe đạp tập', 0, 1, '', '0'),
(11, 'Máy tập nhóm cơ', 0, 1, '', '0'),
(12, 'Tạ tập phòng GYM', 0, 1, '', '0'),
(18, 'BST tạ', 0, 1, '', '0'),
(14, 'Sản phẩm mới', 0, 1, '', '0'),
(15, 'Sản phẩm khuyến mại', 0, 1, '', '0'),
(17, 'Thiết bị tập GYM', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, 'https://cdn.bestme.asia/images/queensport/gian-ta-a-nang-100.jpg', '', '', '', ''),
(10, 'https://cdn.bestme.asia/images/queensport/xe-ap-tap-100.jpg', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/queensport/may-tap-co-100.jpg', '', '', '', ''),
(12, 'https://cdn.bestme.asia/images/queensport/ta-tap-gym-100-1.jpg', '', '', '', ''),
(18, '', '', '', '', ''),
(14, '', '', '', '', ''),
(15, '', '', '', '', ''),
(17, '', '', '', '', '');