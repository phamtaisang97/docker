-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(30, 'TH1', 0, 1, '', '1'),
(29, 'Bộ sưu tập 2', 0, 1, '', '0'),
(28, 'Bộ sưu tập 1', 0, 1, '', '0'),
(27, 'Trang sức phong thủy', 0, 1, '', '0'),
(26, 'Sản phẩm bán chạy', 0, 1, '', '0'),
(25, 'Sản phẩm khuyến mại', 0, 1, '', '0'),
(24, 'Sản phẩm mới', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(30, '', '', '', '', ''),
(29, 'https://cdn.bestme.asia/images/phongthuyviet/banner1100.jpg', '', '', '', ''),
(28, 'https://cdn.bestme.asia/images/phongthuyviet/banner2100.jpg', '', '', '', ''),
(27, '', '', '', '', ''),
(26, '', '', '', '', ''),
(25, '', '', '', '', ''),
(24, '', '', '', '', '');