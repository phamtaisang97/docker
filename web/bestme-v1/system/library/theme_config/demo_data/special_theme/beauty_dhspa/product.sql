-- oc_product PRIMARY KEY (`product_id`)
-- delete first
INSERT IGNORE INTO `oc_product` (`product_id`, `model`, `sku`, `common_barcode`, `common_sku`, `common_compare_price`, `common_price`, `common_cost_price`, `barcode`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `sale_on_out_of_stock`, `stock_status_id`, `image`, `image_alt`, `multi_versions`, `manufacturer_id`, `shipping`, `price`, `price_currency_id`, `compare_price`, `c_price_currency_id`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `channel`, `viewed`, `demo`, `deleted`, `default_store_id`, `user_create_id`, `date_added`, `date_modified`) VALUES
(132, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/x2/thiet-ke-khong-ten-29.png', '', 1, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 1, NULL, 0, 1, '2020-08-10 14:43:44', '2020-08-10 14:43:44'),
(133, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/x2/thiet-ke-khong-ten-28.png', '', 1, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 1, NULL, 0, 1, '2020-08-10 14:45:44', '2020-08-10 14:45:44'),
(134, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/x2/thiet-ke-khong-ten-27.png', '', 1, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 1, NULL, 0, 1, '2020-08-10 14:49:48', '2020-08-10 14:49:48'),
(135, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/x2/z19873074740210b3d1228a7450fbc5da64ea888ebc197.jpg', '', 1, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 1, NULL, 0, 1, '2020-08-10 14:53:23', '2020-08-10 14:53:23'),
(136, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/x2/thiet-ke-khong-ten-25.png', '', 1, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 1, NULL, 0, 1, '2020-08-10 14:55:18', '2020-08-10 14:55:18'),
(137, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/x2/thiet-ke-khong-ten-26.png', '', 0, 0, 1, '0.0000', 1, '480000.0000', 1, 0, 0, '0000-00-00', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 1, NULL, 0, 1, '2020-08-10 14:56:45', '2020-08-10 14:56:45'),
(138, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/x2/thiet-ke-khong-ten-24.png', '', 1, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2020-08-10 14:59:14', '2020-08-10 14:59:14'),
(139, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/x2/thiet-ke-khong-ten-30.png', '', 1, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2020-08-10 15:01:33', '2020-08-10 15:01:33');

-- restore soft-deleted products if demo
UPDATE `oc_product` SET `deleted` = NULL
WHERE `demo` = 1
  AND `product_id` IN (
      132,
      133,
      134,
      135,
      136,
      137,
      138,
      139
  );

-- oc_product_collection PRIMARY KEY (`product_collection_id`)
INSERT IGNORE INTO `oc_product_collection` (`product_collection_id`, `product_id`, `collection_id`, `sort_order`) VALUES
(159, 135, 18, 0),
(158, 132, 18, 0),
(157, 137, 18, 0),
(156, 138, 18, 0),
(155, 136, 16, 0),
(154, 137, 15, 0),
(153, 132, 14, 0),
(152, 138, 13, 0),
(151, 134, 12, 0),
(150, 139, 11, 0),
(149, 133, 10, 0),
(148, 135, 9, 0),
(176, 139, 20, 0),
(175, 135, 20, 0),
(174, 132, 20, 0),
(173, 137, 20, 0),
(172, 138, 20, 0),
(171, 136, 19, 0),
(170, 133, 19, 0),
(169, 134, 19, 0),
(168, 139, 19, 0),
(167, 135, 19, 0),
(166, 132, 19, 0),
(165, 137, 19, 0),
(164, 138, 19, 0),
(163, 136, 18, 0),
(162, 133, 18, 0),
(161, 134, 18, 0),
(160, 139, 18, 0),
(177, 134, 20, 0),
(178, 133, 20, 0),
(179, 136, 20, 0);

-- oc_product_description PRIMARY KEY (`product_id`,`language_id`)
INSERT IGNORE INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `sub_description`, `seo_title`, `seo_description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(132, 2, 'Massage Ấn Huyệt Toàn Thân (Shiatsu Kiểu Nhật)', '', '', '', '', '', '', '', ''),
(133, 2, 'Massage Toàn Thân Với Tinh Dầu', '', '', '', '', '', '', '', ''),
(134, 2, 'Massage Toàn Thân Bằng Đá Nóng', '', '', '', '', '', '', '', ''),
(135, 2, 'Massage Lưng, Vai, Đầu, Cổ', '', '', '', '', '', '', '', ''),
(136, 2, 'Trẻ Hóa Da Với  Sữa Ong Chúa', '', '', '', '', '', '', '', ''),
(137, 2, 'Gội, Massage Đầu Và Ngửi Tinh Dầu Thư Giãn', '', '', '', '', '', '', '', ''),
(138, 2, 'Chăm Sóc Và Massage Chân Với Thảo Dược, Muối Khoáng Và Đá Nóng', '', '', '', '', '', '', '', ''),
(139, 2, 'Massage Thái', '', '', '', '', '', '', '', '');

-- oc_product_image PRIMARY KEY (`product_image_id`)
-- INSERT IGNORE INTO `oc_product_image` (`product_image_id`, `product_id`, `image`, `image_alt`, `sort_order`) VALUES
-- nothing

-- oc_product_to_category PRIMARY KEY (`product_id`,`category_id`)
INSERT IGNORE INTO `oc_product_to_category` (`product_id`, `category_id`) VALUES
(132, 54),
(133, 52),
(134, 53),
(135, 50),
(136, 63),
(137, 49),
(138, 37),
(139, 55);

-- oc_product_to_store PRIMARY KEY (`product_id`,`store_id`)
INSERT IGNORE INTO `oc_product_to_store` (`product_id`, `store_id`, `product_version_id`, `quantity`, `cost_price`) VALUES
(135, 0, 10, 0, '0.0000'),
(135, 0, 9, 0, '0.0000'),
(134, 0, 8, 0, '0.0000'),
(134, 0, 7, 0, '0.0000'),
(134, 0, 6, 0, '0.0000'),
(133, 0, 5, 0, '0.0000'),
(133, 0, 4, 0, '0.0000'),
(133, 0, 3, 0, '0.0000'),
(132, 0, 2, 0, '0.0000'),
(132, 0, 1, 0, '0.0000'),
(136, 0, 11, 0, '0.0000'),
(136, 0, 12, 0, '0.0000'),
(137, 0, 0, 0, '0.0000'),
(138, 0, 13, 0, '0.0000'),
(138, 0, 14, 0, '0.0000'),
(139, 0, 15, 0, '0.0000'),
(139, 0, 16, 0, '0.0000');