-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(11, 'Massage Thái', 0, 1, '', '0'),
(9, 'Massage Toàn Thân', 0, 1, '', '0'),
(10, 'Massage Tinh Dầu', 0, 1, '', '0'),
(12, 'Massage Đá Nóng', 0, 1, '', '0'),
(13, 'Massage Chân', 0, 1, '', '0'),
(14, 'Massage Ấn Huyệt', 0, 1, '', '0'),
(15, 'Gội đầu thư  giãn', 0, 1, '', '0'),
(16, 'Chăm sóc da mặt', 0, 1, '', '0'),
(17, 'Danh mục dịch vụ', 0, 1, '', '1'),
(18, 'Dịch vụ bán chạy', 0, 1, '', '0'),
(19, 'Dịch vụ khuyến mãi', 0, 1, '', '0'),
(20, 'Dịch vụ mới', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(11, 'https://cdn.bestme.asia/images/x2/massage-thai-100.jpg', '', '', '', ''),
(12, 'https://cdn.bestme.asia/images/x2/massage-da-nong-100.jpg', '', '', '', ''),
(13, 'https://cdn.bestme.asia/images/x2/massage-chan-100.jpg', '', '', '', ''),
(9, 'https://cdn.bestme.asia/images/x2/massage-toan-than-100.jpg', '', '', '', ''),
(10, 'https://cdn.bestme.asia/images/x2/massage-tinh-dau-100.jpg', '', '', '', ''),
(14, 'https://cdn.bestme.asia/images/x2/massage-an-huyet-100.jpg', '', '', '', ''),
(15, 'https://cdn.bestme.asia/images/x2/massage-goi-dau-thu-gian-100.jpg', '', '', '', ''),
(16, 'https://cdn.bestme.asia/images/x2/cham-soc-da-mat-10028159703292929.jpg', '', '', '', ''),
(17, '', '', '', '', ''),
(18, '', '', '', '', ''),
(19, '', '', '', '', ''),
(20, '', '', '', '', '');