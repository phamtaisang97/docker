-- oc_seo_url PRIMARY KEY (`seo_url_id`)
INSERT IGNORE INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`, `table_name`) VALUES
-- (1000, 0, 1, 'page_id=1000', 'chinh-sach-va-bao-mat', ''),
-- (1001, 0, 2, 'page_id=1000', 'chinh-sach-va-bao-mat', ''),
-- (2204, 0, 1, 'common/home', 'home', ''),
-- (2205, 0, 2, 'common/home', 'trang-chu', ''),
-- (2206, 0, 1, 'common/shop', 'products', ''),
-- (2207, 0, 2, 'common/shop', 'san-pham', ''),
-- (2208, 0, 1, 'contact/contact', 'contact', ''),
-- (2209, 0, 2, 'contact/contact', 'lien-he', ''),
-- (2210, 0, 1, 'checkout/profile', 'profile', ''),
-- (2211, 0, 2, 'checkout/profile', 'tai-khoan', ''),
-- (2212, 0, 1, 'account/login', 'login', ''),
-- (2213, 0, 2, 'account/login', 'dang-nhap', ''),
-- (2214, 0, 1, 'account/register', 'register', ''),
-- (2215, 0, 2, 'account/register', 'dang-ky', ''),
-- (2216, 0, 1, 'account/logout', 'logout', ''),
-- (2217, 0, 2, 'account/logout', 'dang-xuat', ''),
-- (2218, 0, 1, 'checkout/setting', 'setting', ''),
-- (2219, 0, 2, 'checkout/setting', 'cai-dat', ''),
-- (2220, 0, 1, 'checkout/my_orders', 'checkout-cart', ''),
-- (2221, 0, 2, 'checkout/my_orders', 'gio-hang', ''),
-- (2222, 0, 1, 'checkout/order_preview', 'payment', ''),
-- (2223, 0, 2, 'checkout/order_preview', 'thanh-toan', ''),
-- (2224, 0, 1, 'checkout/order_preview/orderSuccess', 'payment-success', ''),
-- (2225, 0, 2, 'checkout/order_preview/orderSuccess', 'dat-hang-thanh-cong', ''),
-- (2226, 0, 1, 'blog/blog', 'blog', ''),
-- (2227, 0, 2, 'blog/blog', 'bai-viet', ''),
(2253, 0, 1, 'manufacturer_id=26', 'bazas', 'manufacturer'),
(2468, 0, 1, 'product_id=132', 'giay-the-thao-nam-bazas', 'product'),
(2467, 0, 2, 'product_id=132', 'giay-the-thao-nam-bazas', 'product'),
(2256, 0, 2, 'manufacturer_id=27', 'going-link', 'manufacturer'),
(2257, 0, 1, 'manufacturer_id=27', 'going-link', 'manufacturer'),
(2460, 0, 1, 'product_id=133', 'giay-chay-bo-goinglink-dream-1-running', 'product'),
(2459, 0, 2, 'product_id=133', 'giay-chay-bo-goinglink-dream-1-running', 'product'),
(2260, 0, 2, 'manufacturer_id=28', 'anta', 'manufacturer'),
(2261, 0, 1, 'manufacturer_id=28', 'anta', 'manufacturer'),
(2466, 0, 1, 'product_id=134', 'giay-chay-bo-nam-anta', 'product'),
(2465, 0, 2, 'product_id=134', 'giay-chay-bo-nam-anta', 'product'),
(2456, 0, 1, 'product_id=135', 'giay-bong-ro-nam-anta', 'product'),
(2455, 0, 2, 'product_id=135', 'giay-bong-ro-nam-anta', 'product'),
(2266, 0, 2, 'manufacturer_id=29', 'nike', 'manufacturer'),
(2267, 0, 1, 'manufacturer_id=29', 'nike', 'manufacturer'),
(2458, 0, 1, 'product_id=136', 'giay-bong-ro-nam-nike-pg-3', 'product'),
(2457, 0, 2, 'product_id=136', 'giay-bong-ro-nam-nike-pg-3', 'product'),
(2454, 0, 1, 'product_id=137', 'giay-bong-ro-nam-nike-hyperdunk-x-low', 'product'),
(2453, 0, 2, 'product_id=137', 'giay-bong-ro-nam-nike-hyperdunk-x-low', 'product'),
(2272, 0, 2, 'manufacturer_id=30', 'adidas', 'manufacturer'),
(2273, 0, 1, 'manufacturer_id=30', 'adidas', 'manufacturer'),
(2193, 0, 1, 'menu_id=6', 'menu-bo-suu-tap', ''),
(2192, 0, 2, 'menu_id=6', 'menu-bo-suu-tap', ''),
(2354, 0, 2, 'collection=9', 'giay-chay-bo-1', 'collection'),
(2093, 0, 1, 'common/home', 'home', ''),
(2094, 0, 2, 'common/home', 'trang-chu', ''),
(2095, 0, 1, 'common/shop', 'products', ''),
(2096, 0, 2, 'common/shop', 'san-pham', ''),
(2097, 0, 1, 'contact/contact', 'contact', ''),
(2098, 0, 2, 'contact/contact', 'lien-he', ''),
(2099, 0, 1, 'checkout/profile', 'profile', ''),
(2100, 0, 2, 'checkout/profile', 'tai-khoan', ''),
(2101, 0, 1, 'account/login', 'login', ''),
(2102, 0, 2, 'account/login', 'dang-nhap', ''),
(2103, 0, 1, 'account/register', 'register', ''),
(2104, 0, 2, 'account/register', 'dang-ky', ''),
(2105, 0, 1, 'account/logout', 'logout', ''),
(2106, 0, 2, 'account/logout', 'dang-xuat', ''),
(2107, 0, 1, 'checkout/setting', 'setting', ''),
(2108, 0, 2, 'checkout/setting', 'cai-dat', ''),
(2109, 0, 1, 'checkout/my_orders', 'checkout-cart', ''),
(2110, 0, 2, 'checkout/my_orders', 'gio-hang', ''),
(2111, 0, 1, 'checkout/order_preview', 'payment', ''),
(2112, 0, 2, 'checkout/order_preview', 'thanh-toan', ''),
(2113, 0, 1, 'checkout/order_preview/orderSuccess', 'payment-success', ''),
(2114, 0, 2, 'checkout/order_preview/orderSuccess', 'dat-hang-thanh-cong', ''),
(2115, 0, 1, 'blog/blog', 'blog', ''),
(2116, 0, 2, 'blog/blog', 'bai-viet', ''),
(2117, 0, 2, 'manufacturer_id=26', '', 'manufacturer'),
(2118, 0, 1, 'manufacturer_id=26', '', 'manufacturer'),
(2239, 0, 1, 'category_id=37', 'ta', 'category'),
(2238, 0, 2, 'category_id=37', 'ta', 'category'),
(2237, 0, 1, 'category_id=36', 'may-chay-bo', 'category'),
(2236, 0, 2, 'category_id=36', 'may-chay-bo', 'category'),
(2242, 0, 2, 'category_id=39', 'gian-ta-da-nang', 'category'),
(2235, 0, 1, 'category_id=35', 'giay-bong-da', 'category'),
(2234, 0, 2, 'category_id=35', 'giay-bong-da', 'category'),
(2233, 0, 1, 'category_id=34', 'giay-tap-gym', 'category'),
(2232, 0, 2, 'category_id=34', 'giay-tap-gym', 'category'),
(2231, 0, 1, 'category_id=33', 'giay-bong-ro', 'category'),
(2230, 0, 2, 'category_id=33', 'giay-bong-ro', 'category'),
(2229, 0, 1, 'category_id=32', 'giay-chay-bo', 'category'),
(2228, 0, 2, 'category_id=32', 'giay-chay-bo', 'category'),
(2252, 0, 2, 'manufacturer_id=26', 'bazas', 'manufacturer'),
(2251, 0, 1, 'category_id=43', 'bong-da', 'category'),
(2250, 0, 2, 'category_id=43', 'bong-da', 'category'),
(2249, 0, 1, 'category_id=42', 'cau-long', 'category'),
(2248, 0, 2, 'category_id=42', 'cau-long', 'category'),
(2246, 0, 2, 'category_id=41', 'bong-ro', 'category'),
(2245, 0, 1, 'category_id=40', 'tennis', 'category'),
(2243, 0, 1, 'category_id=39', 'gian-ta-da-nang', 'category'),
(2244, 0, 2, 'category_id=40', 'tennis', 'category'),
(2240, 0, 2, 'category_id=38', 'xe-dap-tap', 'category'),
(2241, 0, 1, 'category_id=38', 'xe-dap-tap', 'category'),
(2369, 0, 2, 'blog=4', 'hiit-la-gi-giam-can-hieu-qua-tuc-thi-voi-hiit', 'blog'),
(2370, 0, 2, 'blog=5', '10-nguon-thuc-pham-chua-protein-thuc-vat-tot-nhat', 'blog'),
(2247, 0, 1, 'category_id=41', 'bong-ro', 'category'),
(2448, 0, 1, 'product_id=138', 'giay-bong-da-tq-adidas', 'product'),
(2447, 0, 2, 'product_id=138', 'giay-bong-da-tq-adidas', 'product'),
(2450, 0, 1, 'product_id=139', 'giay-bong-da-san-co-nhan-tao-tf-adidas', 'product'),
(2449, 0, 2, 'product_id=139', 'giay-bong-da-san-co-nhan-tao-tf-adidas', 'product'),
(2278, 0, 2, 'manufacturer_id=31', '-1', 'manufacturer'),
(2279, 0, 1, 'manufacturer_id=31', '-1', 'manufacturer'),
(2446, 0, 1, 'product_id=140', 'giaybong-da-adidas-nitrocharge-30-trx-ag', 'product'),
(2445, 0, 2, 'product_id=140', 'giaybong-da-adidas-nitrocharge-30-trx-ag', 'product'),
(2282, 0, 2, 'manufacturer_id=32', 'thol', 'manufacturer'),
(2283, 0, 1, 'manufacturer_id=32', 'thol', 'manufacturer'),
(2490, 0, 1, 'product_id=141', 'giay-tap-gym-thol-t-731-thoi-trang-chat-luong', 'product'),
(2489, 0, 2, 'product_id=141', 'giay-tap-gym-thol-t-731-thoi-trang-chat-luong', 'product'),
(2484, 0, 1, 'product_id=142', 'giay-cardio-thol', 'product'),
(2483, 0, 2, 'product_id=142', 'giay-cardio-thol', 'product'),
(2486, 0, 1, 'product_id=143', 'giay-the-thao-zoom-train-action-nam', 'product'),
(2485, 0, 2, 'product_id=143', 'giay-the-thao-zoom-train-action-nam', 'product'),
(2290, 0, 2, 'manufacturer_id=33', 'kingsport', 'manufacturer'),
(2291, 0, 1, 'manufacturer_id=33', 'kingsport', 'manufacturer'),
(2433, 0, 1, 'product_id=144', 'ta-tay-phong-gym-kingsport', 'product'),
(2432, 0, 2, 'product_id=144', 'ta-tay-phong-gym-kingsport', 'product'),
(2498, 0, 1, 'product_id=145', 'ta-sat-boc-nhua-ziva', 'product'),
(2497, 0, 2, 'product_id=145', 'ta-sat-boc-nhua-ziva', 'product'),
(2500, 0, 1, 'product_id=146', 'ta-tay-phong-gym-10kg', 'product'),
(2499, 0, 2, 'product_id=146', 'ta-tay-phong-gym-10kg', 'product'),
(2427, 0, 1, 'product_id=147', 'may-chay-bo-kingsport-ks-2041', 'product'),
(2426, 0, 2, 'product_id=147', 'may-chay-bo-kingsport-ks-2041', 'product'),
(2478, 0, 1, 'product_id=148', 'may-chay-bo-kingsport-bk-2034', 'product'),
(2477, 0, 2, 'product_id=148', 'may-chay-bo-kingsport-bk-2034', 'product'),
(2476, 0, 1, 'product_id=149', 'may-chay-bo-kingsport-bk-5000new', 'product'),
(2475, 0, 2, 'product_id=149', 'may-chay-bo-kingsport-bk-5000new', 'product'),
(2419, 0, 1, 'product_id=150', 'xe-dap-tap-eliptical-bike', 'product'),
(2418, 0, 2, 'product_id=150', 'xe-dap-tap-eliptical-bike', 'product'),
(2415, 0, 1, 'product_id=151', 'xe-dap-tap-xbike', 'product'),
(2414, 0, 2, 'product_id=151', 'xe-dap-tap-xbike', 'product'),
(2494, 0, 1, 'product_id=152', 'xe-dap-the-thao-kingsport-xd12', 'product'),
(2493, 0, 2, 'product_id=152', 'xe-dap-the-thao-kingsport-xd12', 'product'),
(2413, 0, 1, 'product_id=153', 'gian-ta-da-nang-kingsport-bk-1998-new', 'product'),
(2412, 0, 2, 'product_id=153', 'gian-ta-da-nang-kingsport-bk-1998-new', 'product'),
(2407, 0, 1, 'product_id=154', 'gian-ta-da-nang-kingsport-bk-399-pro', 'product'),
(2406, 0, 2, 'product_id=154', 'gian-ta-da-nang-kingsport-bk-399-pro', 'product'),
(2409, 0, 1, 'product_id=155', 'ghe-ta-da-nang-kingsport-bk-785-pro', 'product'),
(2408, 0, 2, 'product_id=155', 'ghe-ta-da-nang-kingsport-bk-785-pro', 'product'),
(2316, 0, 2, 'manufacturer_id=34', 'sportland', 'manufacturer'),
(2317, 0, 1, 'manufacturer_id=34', 'sportland', 'manufacturer'),
(2320, 0, 2, 'manufacturer_id=35', 'wolves', 'manufacturer'),
(2321, 0, 1, 'manufacturer_id=35', 'wolves', 'manufacturer'),
(2401, 0, 1, 'product_id=157', 'vot-tennis-wolves', 'product'),
(2400, 0, 2, 'product_id=157', 'vot-tennis-wolves', 'product'),
(2324, 0, 2, 'manufacturer_id=36', 'lamersport', 'manufacturer'),
(2325, 0, 1, 'manufacturer_id=36', 'lamersport', 'manufacturer'),
(2550, 0, 1, 'product_id=178', 'qua-bong-da-telstar', 'product'),
(2545, 0, 2, 'product_id=177', 'qua-bong-da-ebete', 'product'),
(2539, 0, 2, 'product_id=176', 'qua-bong-da-svds', 'product'),
(2540, 0, 1, 'product_id=176', 'qua-bong-da-svds', 'product'),
(2546, 0, 1, 'product_id=177', 'qua-bong-da-ebete', 'product'),
(2334, 0, 2, 'manufacturer_id=37', 'kawasaki', 'manufacturer'),
(2335, 0, 1, 'manufacturer_id=37', 'kawasaki', 'manufacturer'),
(2387, 0, 1, 'product_id=162', 'vot-cau-long-kawasaki-250', 'product'),
(2386, 0, 2, 'product_id=162', 'vot-cau-long-kawasaki-250', 'product'),
(2338, 0, 2, 'manufacturer_id=38', 'yonex', 'manufacturer'),
(2339, 0, 1, 'manufacturer_id=38', 'yonex', 'manufacturer'),
(2342, 0, 2, 'manufacturer_id=39', 'aaa', 'manufacturer'),
(2343, 0, 1, 'manufacturer_id=39', 'aaa', 'manufacturer'),
(2522, 0, 1, 'product_id=164', 'vot-cau-long-aaa-18', 'product'),
(2521, 0, 2, 'product_id=164', 'vot-cau-long-aaa-18', 'product'),
(2554, 0, 1, 'product_id=179', 'vot-tennis-babolat', 'product'),
(2553, 0, 2, 'product_id=179', 'vot-tennis-babolat', 'product'),
(2348, 0, 2, 'manufacturer_id=40', 'yono', 'manufacturer'),
(2349, 0, 1, 'manufacturer_id=40', 'yono', 'manufacturer'),
(2552, 0, 1, 'manufacturer_id=51', 'babolat', 'manufacturer'),
(2551, 0, 2, 'manufacturer_id=51', 'babolat', 'manufacturer'),
(2379, 0, 1, 'product_id=167', 'qua-bong-da-pelada', 'product'),
(2378, 0, 2, 'product_id=167', 'qua-bong-da-pelada', 'product'),
(2355, 0, 2, 'collection=10', 'giay-tap-gym-1', 'collection'),
(2356, 0, 2, 'collection=11', 'giay-bong-ro-1', 'collection'),
(2357, 0, 2, 'collection=12', 'giay-bong-da-1', 'collection'),
(2358, 0, 2, 'collection=13', 'may-chay-bo-1', 'collection'),
(2359, 0, 2, 'collection=14', 'xe-dap-tap-1', 'collection'),
(2360, 0, 2, 'collection=15', 'ta-1', 'collection'),
(2361, 0, 2, 'collection=16', 'gian-ta-da-nang-1', 'collection'),
(2362, 0, 2, 'collection=17', 'tennis-1', 'collection'),
(2363, 0, 2, 'collection=18', 'bong-ro-1', 'collection'),
(2364, 0, 2, 'collection=19', 'cau-long-1', 'collection'),
(2365, 0, 2, 'collection=20', 'bong-da-1', 'collection'),
(2366, 0, 2, 'collection=21', 'bst-giay', 'collection'),
(2367, 0, 2, 'collection=22', 'bst-the-hinh', 'collection'),
(2368, 0, 2, 'collection=23', 'bst-phu-kien-the-thao', 'collection'),
(2371, 0, 2, 'blog=6', 'bmr-la-gi-xac-dinh-bmr-giup-giam-can-hieu-qua', 'blog'),
(2372, 0, 2, 'menu_id=7', 'menu-chan-trang', ''),
(2373, 0, 1, 'menu_id=7', 'menu-chan-trang', ''),
(2438, 0, 2, 'collection=24', 'san-pham-moi', 'collection'),
(2439, 0, 2, 'collection=25', 'san-pham-ban-chay', 'collection'),
(2440, 0, 2, 'collection=26', 'san-pham-khuyen-mai', 'collection'),
(2469, 0, 2, 'product_id=168', 'giay-bong-da-nike-mercurial', 'product'),
(2470, 0, 1, 'product_id=168', 'giay-bong-da-nike-mercurial', 'product'),
(2473, 0, 2, 'product_id=169', 'may-chay-bo-sunny-health-amp-fitness', 'product'),
(2474, 0, 1, 'product_id=169', 'may-chay-bo-sunny-health-amp-fitness', 'product'),
(2487, 0, 2, 'manufacturer_id=41', 'puma', 'manufacturer'),
(2488, 0, 1, 'manufacturer_id=41', 'puma', 'manufacturer'),
(2491, 0, 2, 'manufacturer_id=42', 'sunny', 'manufacturer'),
(2492, 0, 1, 'manufacturer_id=42', 'sunny', 'manufacturer'),
(2501, 0, 2, 'manufacturer_id=43', 'cavina', 'manufacturer'),
(2502, 0, 1, 'manufacturer_id=43', 'cavina', 'manufacturer'),
(2518, 0, 1, 'product_id=170', 'vot-cau-long-cavina', 'product'),
(2517, 0, 2, 'product_id=170', 'vot-cau-long-cavina', 'product'),
(2507, 0, 2, 'manufacturer_id=44', 'bokai', 'manufacturer'),
(2508, 0, 1, 'manufacturer_id=44', 'bokai', 'manufacturer'),
(2516, 0, 1, 'product_id=171', 'vot-cau-long-bokai', 'product'),
(2515, 0, 2, 'product_id=171', 'vot-cau-long-bokai', 'product'),
(2513, 0, 2, 'manufacturer_id=45', 'wilson', 'manufacturer'),
(2514, 0, 1, 'manufacturer_id=45', 'wilson', 'manufacturer'),
(2519, 0, 2, 'manufacturer_id=46', 'victor', 'manufacturer'),
(2520, 0, 1, 'manufacturer_id=46', 'victor', 'manufacturer'),
(2523, 0, 2, 'manufacturer_id=47', 'jatan', 'manufacturer'),
(2524, 0, 1, 'manufacturer_id=47', 'jatan', 'manufacturer'),
(2525, 0, 2, 'product_id=172', 'qua-bong-ro-jatan', 'product'),
(2526, 0, 1, 'product_id=172', 'qua-bong-ro-jatan', 'product'),
(2527, 0, 2, 'manufacturer_id=48', 'prostar', 'manufacturer'),
(2528, 0, 1, 'manufacturer_id=48', 'prostar', 'manufacturer'),
(2529, 0, 2, 'product_id=173', 'qua-bong-ro-prostar', 'product'),
(2530, 0, 1, 'product_id=173', 'qua-bong-ro-prostar', 'product'),
(2531, 0, 2, 'product_id=174', 'qua-bong-ro-spalding', 'product'),
(2532, 0, 1, 'product_id=174', 'qua-bong-ro-spalding', 'product'),
(2533, 0, 2, 'manufacturer_id=49', 'geru-star', 'manufacturer'),
(2534, 0, 1, 'manufacturer_id=49', 'geru-star', 'manufacturer'),
(2535, 0, 2, 'product_id=175', 'qua-bong-ro-geru-star', 'product'),
(2536, 0, 1, 'product_id=175', 'qua-bong-ro-geru-star', 'product'),
(2537, 0, 2, 'manufacturer_id=50', 'spalding', 'manufacturer'),
(2538, 0, 1, 'manufacturer_id=50', 'spalding', 'manufacturer'),
(2549, 0, 2, 'product_id=178', 'qua-bong-da-telstar', 'product'),
(2555, 0, 2, 'product_id=180', 'vot-tennis-wilson', 'product'),
(2556, 0, 1, 'product_id=180', 'vot-tennis-wilson', 'product'),
(2557, 0, 2, 'product_id=181', 'vot-tennis-head-graphene-360-speed', 'product'),
(2558, 0, 1, 'product_id=181', 'vot-tennis-head-graphene-360-speed', 'product');