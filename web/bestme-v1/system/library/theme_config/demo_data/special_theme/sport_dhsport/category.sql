-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(40, '', 0, 0, 0, 0, 1, '2020-08-13 08:55:22', '2020-08-13 08:55:22'),
(41, '', 0, 0, 0, 0, 1, '2020-08-13 08:55:30', '2020-08-13 08:55:30'),
(42, '', 0, 0, 0, 0, 1, '2020-08-13 08:55:39', '2020-08-13 08:55:39'),
(43, '', 0, 0, 0, 0, 1, '2020-08-13 08:58:36', '2020-08-13 08:58:36'),
(37, '', 0, 0, 0, 0, 1, '2020-08-13 08:54:50', '2020-08-13 08:54:50'),
(36, '', 0, 0, 0, 0, 1, '2020-08-13 08:54:44', '2020-08-13 08:54:44'),
(35, '', 0, 0, 0, 0, 1, '2020-08-13 08:54:28', '2020-08-13 08:54:28'),
(34, '', 0, 0, 0, 0, 1, '2020-08-13 08:54:05', '2020-08-13 08:54:05'),
(33, '', 0, 0, 0, 0, 1, '2020-08-13 08:53:53', '2020-08-13 08:53:53'),
(32, '', 0, 0, 0, 0, 1, '2020-08-13 08:51:47', '2020-08-13 08:51:47'),
(38, '', 0, 0, 0, 0, 1, '2020-08-13 08:55:02', '2020-08-13 08:55:02'),
(39, '', 0, 0, 0, 0, 1, '2020-08-13 08:55:14', '2020-08-13 08:55:14');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(43, 1, 'Bóng Đá', 'Bóng Đá', '', '', ''),
(42, 1, 'Cầu Lông', 'Cầu Lông', '', '', ''),
(43, 2, 'Bóng Đá', 'Bóng Đá', '', '', ''),
(40, 1, 'Tennis', 'Tennis', '', '', ''),
(41, 2, 'Bóng Rổ', 'Bóng Rổ', '', '', ''),
(41, 1, 'Bóng Rổ', 'Bóng Rổ', '', '', ''),
(42, 2, 'Cầu Lông', 'Cầu Lông', '', '', ''),
(39, 1, 'Giàn Tạ Đa Năng', 'Giàn Tạ Đa Năng', '', '', ''),
(40, 2, 'Tennis', 'Tennis', '', '', ''),
(38, 1, 'Xe Đạp Tập', 'Xe Đạp Tập', '', '', ''),
(39, 2, 'Giàn Tạ Đa Năng', 'Giàn Tạ Đa Năng', '', '', ''),
(37, 2, 'Tạ', 'Tạ', '', '', ''),
(37, 1, 'Tạ', 'Tạ', '', '', ''),
(38, 2, 'Xe Đạp Tập', 'Xe Đạp Tập', '', '', ''),
(32, 2, 'Giày Chạy Bộ', 'Giày Chạy Bộ', '', '', ''),
(32, 1, 'Giày Chạy Bộ', 'Giày Chạy Bộ', '', '', ''),
(33, 2, 'Giày Bóng Rổ', 'Giày Bóng Rổ', '', '', ''),
(33, 1, 'Giày Bóng Rổ', 'Giày Bóng Rổ', '', '', ''),
(34, 2, 'Giày Tập Gym', 'Giày Tập Gym', '', '', ''),
(34, 1, 'Giày Tập Gym', 'Giày Tập Gym', '', '', ''),
(35, 2, 'Giày Bóng Đá', 'Giày Bóng Đá', '', '', ''),
(35, 1, 'Giày Bóng Đá', 'Giày Bóng Đá', '', '', ''),
(36, 2, 'Máy Chạy Bộ', 'Máy Chạy Bộ', '', '', ''),
(36, 1, 'Máy Chạy Bộ', 'Máy Chạy Bộ', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(37, 37, 0),
(36, 36, 0),
(35, 35, 0),
(34, 34, 0),
(33, 33, 0),
(32, 32, 0),
(39, 39, 0),
(38, 38, 0),
(43, 43, 0),
(42, 42, 0),
(41, 41, 0),
(40, 40, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(39, 0),
(40, 0),
(41, 0),
(42, 0),
(43, 0);