-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(11, 'Kinh Tế', 0, 1, NULL, '0'),
(10, 'Hồi Kí - Tùy Bút', 0, 1, NULL, '0'),
(9, 'Kiếm Hiệp - Võ Hiệp', 0, 1, NULL, '0'),
(12, 'Truyện Trinh Thám', 0, 1, NULL, '0'),
(13, 'Lịch Sử', 0, 1, NULL, '0'),
(14, 'Triết Học', 0, 1, NULL, '0'),
(15, 'Văn Học Việt Nam', 0, 1, NULL, '0'),
(16, 'Văn Hóa - Xã Hội', 0, 1, NULL, '0'),
(17, 'Y Học - Sức Khỏe', 0, 1, NULL, '0'),
(18, 'Truyện Thiếu Nhi', 0, 1, NULL, '0'),
(19, 'Truyện Cười', 0, 1, NULL, '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, '', '', '', '', ''),
(10, '', '', '', '', ''),
(11, '', '', '', '', ''),
(12, '', '', '', '', ''),
(13, '', '', '', '', ''),
(14, '', '', '', '', ''),
(15, '', '', '', '', ''),
(16, '', '', '', '', ''),
(17, '', '', '', '', ''),
(18, '', '', '', '', ''),
(19, '', '', '', '', '');