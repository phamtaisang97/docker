-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `source`, `date_added`, `date_modified`) VALUES
    (34, '', 0, 0, 0, 0, 1, NULL, '2021-11-25 14:25:49', '2021-11-25 14:25:49'),
	(32, 'view/image/theme/upload-placeholder2.svg', 0, 0, 0, 0, 1, NULL, '2021-10-06 13:09:26', '2021-11-25 14:25:13'),
	(33, '', 0, 0, 0, 0, 1, NULL, '2021-11-25 14:25:38', '2021-11-25 14:25:38');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
    (32, 2, 'Cửa đi mở quay', 'Cửa đi mở quay', '', '', ''),
    (32, 1, 'Cửa đi mở quay', 'Cửa đi mở quay', '', '', ''),
    (33, 2, 'Cửa gấp trượt prima', 'Cửa gấp trượt prima', '', '', ''),
    (33, 1, 'Cửa gấp trượt prima', 'Cửa gấp trượt prima', '', '', ''),
    (34, 2, 'Cửa lùa', 'Cửa lùa', '', '', ''),
    (34, 1, 'Cửa lùa', 'Cửa lùa', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
    (34, 34, 0),
    (33, 33, 0),
    (32, 32, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
    (32, 0),
    (33, 0),
    (34, 0);