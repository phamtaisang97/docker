-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(16, 'Trang trí không gian Coffee Shop', 0, 1, '', '0'),
(17, 'Không gian nhà bếp tiện lợi, xinh xắn', 0, 1, '', '0'),
(18, 'Phòng khách ấm cúng, cả nhà quây quần', 0, 1, '', '0'),
(14, 'Trang trí không gian phòng ngủ', 0, 1, '', '0'),
(15, 'Kệ đựng đồ xinh xắn', 0, 1, '', '0'),
(19, 'TH', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(16, '', '', '', '', ''),
(17, '', '', '', '', ''),
(18, '', '', '', '', ''),
(19, '', '', '', '', ''),
(14, '', '', '', '', ''),
(15, '', '', '', '', '');
