-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(32, NULL, 0, 0, 0, 0, 1, '2020-12-10 11:31:28', '2020-12-10 11:31:28'),
(33, NULL, 0, 0, 0, 0, 1, '2020-12-10 11:31:28', '2020-12-10 11:31:28'),
(34, NULL, 0, 0, 0, 0, 1, '2020-12-10 11:31:28', '2020-12-10 11:31:28'),
(35, NULL, 0, 0, 0, 0, 1, '2020-12-10 11:31:28', '2020-12-10 11:31:28');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(32, 2, 'Kệ sắt phòng khách', 'Kệ sắt phòng khách', '', '', ''),
(32, 1, 'Kệ sắt phòng khách', 'Kệ sắt phòng khách', '', '', ''),
(33, 2, 'Kệ sắt phòng bếp', 'Kệ sắt phòng bếp', '', '', ''),
(33, 1, 'Kệ sắt phòng bếp', 'Kệ sắt phòng bếp', '', '', ''),
(34, 2, 'Kệ sắt decor quán cà phê', 'Kệ sắt decor quán cà phê', '', '', ''),
(34, 1, 'Kệ sắt decor quán cà phê', 'Kệ sắt decor quán cà phê', '', '', ''),
(35, 2, 'Kệ khung Decor', 'Kệ khung Decor', '', '', ''),
(35, 1, 'Kệ khung Decor', 'Kệ khung Decor', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(33, 33, 0),
(35, 35, 0),
(32, 32, 0),
(34, 34, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0);