-- oc_blog PRIMARY KEY (`blog_id`)
-- nothing

-- oc_blog_category PRIMARY KEY (`blog_category_id`)
INSERT IGNORE INTO `oc_blog_category` (`blog_category_id`, `status`, `date_added`, `date_modified`) VALUES
(1, 1, '2019-11-11 13:37:51', '2019-11-11 19:03:46');

-- oc_blog_category_description PRIMARY KEY (`blog_category_id`,`language_id`)
INSERT IGNORE INTO `oc_blog_category_description` (`blog_category_id`, `language_id`, `title`, `meta_title`, `meta_description`, `alias`) VALUES
(1, 1, 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'danh-muc-mac-dinh-uncategorized'),
(1, 2, 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'danh-muc-mac-dinh-uncategorized');

-- oc_blog_description PRIMARY KEY (`blog_id`,`language_id`)
-- nothing

-- oc_blog_to_blog_category NO PRIMARY KEY
-- nothing