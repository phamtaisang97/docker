-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(9, 'THiệp Cưới', 0, 1, '', '0'),
(10, 'Váy Cưới Bó Sát', 0, 1, '', '0'),
(11, 'Váy Cưới Màu Sắc', 0, 1, '', '0'),
(8, 'Hoa Cưới', 0, 1, '', '0'),
(12, 'Váy Cưới Dáng Xoè', 0, 1, '', '0'),
(13, 'Váy Cưới Bà Bầu', 0, 1, '', '0'),
(14, 'TH Váy Cưới', 0, 1, '', '1'),
(15, 'Vest Cưới', 0, 1, '', '0'),
(16, 'Dịch vụ', 0, 1, '', '1'),
(17, 'Váy đẹp', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(10, 'https://cdn.bestme.asia/images/x2/vay-bo-100.jpg', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/x2/vay-mau-100.jpg', '', '', '', ''),
(12, 'https://cdn.bestme.asia/images/x2/vay-100.jpg', '', '', '', ''),
(13, 'https://cdn.bestme.asia/images/x2/vay-bau-100-1.jpg', '', '', '', ''),
(14, 'https://cdn.bestme.asia/images/x2/vay-cuoi-100.jpg', '', '', '', ''),
(15, 'https://cdn.bestme.asia/images/x2/vest-cuoi-100.jpg', '', '', '', ''),
(8, 'https://cdn.bestme.asia/images/x2/hoa-cuoi-100.jpg', '', '', '', ''),
(9, 'https://cdn.bestme.asia/images/x2/thiep-cuoi-100.jpg', '', '', '', ''),
(16, '', '', '', '', ''),
(17, '', '', '', '', '');