-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(21, NULL, 0, 0, 0, 0, 1, '2020-08-06 09:48:48', '2020-08-06 09:48:48'),
(22, NULL, 0, 0, 0, 0, 1, '2020-08-06 09:48:48', '2020-08-06 09:48:48'),
(23, NULL, 0, 0, 0, 0, 1, '2020-08-06 09:48:48', '2020-08-06 09:48:48'),
(24, NULL, 0, 0, 0, 0, 1, '2020-08-06 09:48:48', '2020-08-06 09:48:48'),
(25, NULL, 0, 0, 0, 0, 1, '2020-08-06 09:48:49', '2020-08-06 09:48:49'),
(26, NULL, 0, 0, 0, 0, 1, '2020-08-06 09:48:49', '2020-08-06 09:48:49');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(22, 2, 'Váy cưới bó sát', 'Váy cưới bó sát', '', '', ''),
(22, 1, 'Váy cưới bó sát', 'Váy cưới bó sát', '', '', ''),
(23, 2, 'Váy cưới dáng xòa', 'Váy cưới dáng xòa', '', '', ''),
(23, 1, 'Váy cưới dáng xòa', 'Váy cưới dáng xòa', '', '', ''),
(24, 2, 'Váy cưới màu sắc', 'Váy cưới màu sắc', '', '', ''),
(24, 1, 'Váy cưới màu sắc', 'Váy cưới màu sắc', '', '', ''),
(25, 2, 'Hoa cưới', 'Hoa cưới', '', '', ''),
(21, 1, 'Váy cưới bà bầu', 'Váy cưới bà bầu', '', '', ''),
(21, 2, 'Váy cưới bà bầu', 'Váy cưới bà bầu', '', '', ''),
(25, 1, 'Hoa cưới', 'Hoa cưới', '', '', ''),
(26, 2, 'Thiệp cưới', 'Thiệp cưới', '', '', ''),
(26, 1, 'Thiệp cưới', 'Thiệp cưới', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(21, 21, 0),
(22, 22, 0),
(23, 23, 0),
(24, 24, 0),
(25, 25, 0),
(26, 26, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(21, 0),
(22, 0),
(23, 0),
(24, 0),
(25, 0),
(26, 0);