-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(9, 'Sơ sinh', 0, 1, '', '0'),
(10, 'Bé gái', 0, 1, '', '0'),
(11, 'Bé trai', 0, 1, '', '0'),
(12, 'Tất cho bé', 0, 1, '', '0'),
(13, 'Khăn mũ', 0, 1, '', '0'),
(14, 'Balo-Vali-Túi sách', 0, 1, '', '0'),
(15, 'Giày dép cho bé', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, '', '', '', '', ''),
(10, '', '', '', '', ''),
(11, '', '', '', '', ''),
(12, '', '', '', '', ''),
(13, '', '', '', '', ''),
(14, '', '', '', '', ''),
(15, '', '', '', '', '');