-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `source`, `date_added`, `date_modified`) VALUES
(33, NULL, 0, 0, 0, 0, 1, NULL, '2021-11-09 16:17:59', '2021-11-09 16:17:59'),
(32, NULL, 0, 0, 0, 0, 1, NULL, '2021-11-08 15:39:56', '2021-11-08 15:39:56');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(32, 2, 'demo', 'demo', '', '', ''),
(32, 1, 'demo', 'demo', '', '', ''),
(33, 2, 'loại 1', 'loại 1', '', '', ''),
(33, 1, 'loại 1', 'loại 1', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(33, 33, 0),
(32, 32, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0);