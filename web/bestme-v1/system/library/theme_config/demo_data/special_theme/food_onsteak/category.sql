-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(36, '', 0, 0, 0, 0, 1, '2021-01-11 14:10:22', '2021-01-11 14:10:22'),
(35, '', 0, 0, 0, 0, 1, '2021-01-11 14:10:11', '2021-01-11 14:10:11'),
(34, '', 0, 0, 0, 0, 1, '2021-01-11 14:09:53', '2021-01-11 14:09:53'),
(33, '', 0, 0, 0, 0, 1, '2021-01-11 14:09:30', '2021-01-11 14:09:30'),
(32, '', 0, 0, 0, 0, 1, '2021-01-11 14:08:42', '2021-01-19 17:29:27'),
(37, '', 0, 0, 0, 0, 1, '2021-01-19 17:29:38', '2021-01-19 17:29:38'),
(38, NULL, 0, 0, 0, 0, 1, '2021-01-20 09:21:18', '2021-01-20 09:21:18');


-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(38, 2, 'Pasta', 'Pasta', '', '', ''),
(38, 1, 'Pasta', 'Pasta', '', '', ''),
(32, 2, 'Appetizers and Soups', 'Appetizers and Soups', '', '', ''),
(33, 2, 'Main Courses', 'Main Courses', '', '', ''),
(33, 1, 'Main Courses', 'Main Courses', '', '', ''),
(34, 2, 'Desserts', 'Desserts', '', '', ''),
(34, 1, 'Desserts', 'Desserts', '', '', ''),
(35, 2, 'Drinks', 'Drinks', '', '', ''),
(35, 1, 'Drinks', 'Drinks', '', '', ''),
(36, 2, 'Wine', 'Wine', '', '', ''),
(36, 1, 'Wine', 'Wine', '', '', ''),
(32, 1, 'Appetizers and Soups', 'Appetizers and Soups', '', '', ''),
(37, 2, 'Salads', 'Salads', '', '', ''),
(37, 1, 'Salads', 'Salads', '', '', '');


-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(35, 35, 0),
(34, 34, 0),
(33, 33, 0),
(32, 32, 0),
(37, 37, 0),
(36, 36, 0),
(38, 38, 0);


-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0);
