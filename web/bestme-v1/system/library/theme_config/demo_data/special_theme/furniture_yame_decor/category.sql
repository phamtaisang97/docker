-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(45, '', 0, 0, 0, 0, 1, '2020-08-27 14:18:50', '2020-08-27 14:18:50'),
(44, '', 0, 0, 0, 0, 1, '2020-08-27 14:18:42', '2020-08-27 14:18:42'),
(43, '', 0, 0, 0, 0, 1, '2020-08-27 14:17:42', '2020-08-27 14:17:42'),
(38, '', 0, 0, 0, 0, 1, '2020-08-26 10:41:42', '2020-08-26 10:41:42'),
(42, NULL, 0, 0, 0, 0, 1, '2020-08-27 14:09:03', '2020-08-27 14:09:03'),
(41, '', 0, 0, 0, 0, 1, '2020-08-26 13:40:08', '2020-08-26 13:40:08'),
(35, '', 0, 0, 0, 0, 1, '2020-08-26 10:41:15', '2020-08-26 10:41:15'),
(32, '', 0, 0, 0, 0, 1, '2020-08-26 10:41:07', '2020-08-26 10:41:07'),
(47, '', 0, 0, 0, 0, 1, '2020-08-27 14:20:03', '2020-08-27 14:20:03');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(45, 1, 'Tủ nhựa đồ trang điểm', 'Tủ nhựa đồ trang điểm', '', '', ''),
(45, 2, 'Tủ nhựa đồ trang điểm', 'Tủ nhựa đồ trang điểm', '', '', ''),
(44, 2, 'Tủ nhựa đựng giày', 'Tủ nhựa đựng giày', '', '', ''),
(42, 2, 'Tủ Nhựa Song Long.', 'Tủ Nhựa Song Long.', '', '', ''),
(43, 2, 'Tủ nhựa 3D', 'Tủ nhựa 3D', '', '', ''),
(43, 1, 'Tủ nhựa 3D', 'Tủ nhựa 3D', '', '', ''),
(44, 1, 'Tủ nhựa đựng giày', 'Tủ nhựa đựng giày', '', '', ''),
(38, 2, 'Tủ nhựa Việt Nhật', 'Tủ nhựa Việt Nhật', '', '', ''),
(38, 1, 'Tủ nhựa Việt Nhật', 'Tủ nhựa Việt Nhật', '', '', ''),
(32, 2, 'Tủ nhựa Đài Loan', 'Tủ nhựa Đài Loan', '', '', ''),
(32, 1, 'Tủ nhựa Đài Loan', 'Tủ nhựa Đài Loan', '', '', ''),
(47, 2, 'Tủ Nhựa Người Lớn SHplastic', 'Tủ Nhựa Người Lớn SHplastic', '', '', ''),
(47, 1, 'Tủ Nhựa Người Lớn SHplastic', 'Tủ Nhựa Người Lớn SHplastic', '', '', ''),
(35, 2, 'Tủ nhựa Đại Đồng Tiến', 'Tủ nhựa Đại Đồng Tiến', '', '', ''),
(35, 1, 'Tủ nhựa Đại Đồng Tiến', 'Tủ nhựa Đại Đồng Tiến', '', '', ''),
(41, 2, 'Tủ nhựa Duy Tân', 'Tủ nhựa Duy Tân', '', '', ''),
(41, 1, 'Tủ nhựa Duy Tân', 'Tủ nhựa Duy Tân', '', '', ''),
(42, 1, 'Tủ Nhựa Song Long.', 'Tủ Nhựa Song Long.', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(32, 32, 0),
(41, 41, 0),
(47, 47, 0),
(45, 45, 0),
(43, 43, 0),
(44, 44, 0),
(42, 42, 0),
(38, 38, 0),
(35, 35, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(35, 0),
(38, 0),
(41, 0),
(42, 0),
(43, 0),
(44, 0),
(45, 0),
(47, 0);