-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(5, 'Sản phẩm hot', 0, 1, '', '0'),
(6, 'Sản phẩm mới', 0, 1, '', '0'),
(4, 'Sản phẩm khuyến mại', 0, 1, '', '0'),
(7, 'Hàng ngày', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(7, 'https://cdn.bestme.asia/images/x2/cs882.png', '&lt;p&gt;Bộ sưu tập hàng ngày&lt;/p&gt;', '', '', ''),
(6, '', '&lt;p&gt;nhũng sản phẩm mới nhất có mặt tại cửa hàng&lt;/p&gt;', '', '', ''),
(4, '', '&lt;p&gt;sản phẩm khuyến mại&lt;/p&gt;', '', '', ''),
(5, 'https://cdn.bestme.asia/images/x2/sliderindex61_hQejGKb.jpg', '&lt;p&gt;sản phẩm hot&lt;/p&gt;', '', '', '');