-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(41, '', 40, 0, 0, 0, 1, '2020-12-14 16:45:52', '2020-12-14 16:45:52'),
(40, '', 0, 0, 0, 0, 1, '2020-12-14 16:45:52', '2020-12-14 16:45:52'),
(39, '', 32, 0, 0, 0, 1, '2020-12-14 16:44:16', '2020-12-14 16:44:16'),
(38, '', 32, 0, 0, 0, 1, '2020-12-14 16:44:16', '2020-12-14 16:44:16'),
(37, '', 32, 0, 0, 0, 1, '2020-12-14 16:44:16', '2020-12-14 16:44:16'),
(36, '', 32, 0, 0, 0, 1, '2020-12-14 16:44:16', '2020-12-14 16:44:16'),
(32, '', 0, 0, 0, 0, 1, '2020-12-14 16:44:16', '2020-12-14 16:44:16'),
(33, '', 32, 0, 0, 0, 1, '2020-12-14 16:44:16', '2020-12-14 16:44:16'),
(34, '', 32, 0, 0, 0, 1, '2020-12-14 16:44:16', '2020-12-14 16:44:16'),
(35, '', 32, 0, 0, 0, 1, '2020-12-14 16:44:16', '2020-12-14 16:44:16'),
(42, '', 40, 0, 0, 0, 1, '2020-12-14 16:45:52', '2020-12-14 16:45:52'),
(43, '', 40, 0, 0, 0, 1, '2020-12-14 16:45:52', '2020-12-14 16:45:52'),
(44, '', 40, 0, 0, 0, 1, '2020-12-14 16:45:52', '2020-12-14 16:45:52'),
(45, '', 40, 0, 0, 0, 1, '2020-12-14 16:45:52', '2020-12-14 16:45:52'),
(46, '', 0, 0, 0, 0, 1, '2020-12-14 16:46:52', '2020-12-14 16:46:52'),
(47, '', 46, 0, 0, 0, 1, '2020-12-14 16:46:52', '2020-12-14 16:46:52'),
(48, '', 46, 0, 0, 0, 1, '2020-12-14 16:46:52', '2020-12-14 16:46:52'),
(49, '', 46, 0, 0, 0, 1, '2020-12-14 16:46:52', '2020-12-14 16:46:52'),
(50, '', 46, 0, 0, 0, 1, '2020-12-14 16:46:52', '2020-12-14 16:46:52'),
(51, '', 46, 0, 0, 0, 1, '2020-12-14 16:46:52', '2020-12-14 16:46:52'),
(52, '', 0, 0, 0, 0, 1, '2020-12-14 16:47:48', '2020-12-14 16:47:48'),
(53, '', 52, 0, 0, 0, 1, '2020-12-14 16:47:48', '2020-12-14 16:47:48'),
(54, '', 52, 0, 0, 0, 1, '2020-12-14 16:47:48', '2020-12-14 16:47:48'),
(55, '', 52, 0, 0, 0, 1, '2020-12-14 16:47:48', '2020-12-14 16:47:48'),
(56, '', 0, 0, 0, 0, 1, '2020-12-14 16:48:27', '2020-12-14 16:48:27'),
(57, '', 56, 0, 0, 0, 1, '2020-12-14 16:48:27', '2020-12-14 16:48:27'),
(58, '', 56, 0, 0, 0, 1, '2020-12-14 16:48:27', '2020-12-14 16:48:27'),
(59, '', 56, 0, 0, 0, 1, '2020-12-14 16:48:27', '2020-12-14 16:48:27');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(45, 1, 'Sandals nam', 'Sandals nam', '', '', ''),
(46, 2, 'Giày unisex', 'Giày unisex', '', '', ''),
(45, 2, 'Sandals nam', 'Sandals nam', '', '', ''),
(44, 1, 'Boot nam', 'Boot nam', '', '', ''),
(44, 2, 'Boot nam', 'Boot nam', '', '', ''),
(43, 1, 'Giày Tây công sở', 'Giày Tây công sở', '', '', ''),
(43, 2, 'Giày Tây công sở', 'Giày Tây công sở', '', '', ''),
(42, 2, 'Slip on nam', 'Slip on nam', '', '', ''),
(42, 1, 'Slip on nam', 'Slip on nam', '', '', ''),
(41, 2, 'Sneaker nam', 'Sneaker nam', '', '', ''),
(41, 1, 'Sneaker nam', 'Sneaker nam', '', '', ''),
(40, 1, 'Giày nam', 'Giày nam', '', '', ''),
(39, 2, 'Giày búp bê', 'Giày búp bê', '', '', ''),
(39, 1, 'Giày búp bê', 'Giày búp bê', '', '', ''),
(40, 2, 'Giày nam', 'Giày nam', '', '', ''),
(38, 2, 'Dép nữ', 'Dép nữ', '', '', ''),
(38, 1, 'Dép nữ', 'Dép nữ', '', '', ''),
(36, 1, 'Boot nữ', 'Boot nữ', '', '', ''),
(37, 2, 'Sneaker nữ', 'Sneaker nữ', '', '', ''),
(37, 1, 'Sneaker nữ', 'Sneaker nữ', '', '', ''),
(32, 2, 'Giày nữ', 'Giày nữ', '', '', ''),
(32, 1, 'Giày nữ', 'Giày nữ', '', '', ''),
(33, 2, 'Giày cao gót', 'Giày cao gót', '', '', ''),
(33, 1, 'Giày cao gót', 'Giày cao gót', '', '', ''),
(34, 2, 'Giày sandals nữ', 'Giày sandals nữ', '', '', ''),
(34, 1, 'Giày sandals nữ', 'Giày sandals nữ', '', '', ''),
(35, 2, 'Slip on nữ', 'Slip on nữ', '', '', ''),
(35, 1, 'Slip on nữ', 'Slip on nữ', '', '', ''),
(36, 2, 'Boot nữ', 'Boot nữ', '', '', ''),
(46, 1, 'Giày unisex', 'Giày unisex', '', '', ''),
(47, 2, 'Slip on', 'Slip on', '', '', ''),
(47, 1, 'Slip on', 'Slip on', '', '', ''),
(48, 2, 'Sandals', 'Sandals', '', '', ''),
(48, 1, 'Sandals', 'Sandals', '', '', ''),
(49, 2, 'Sneaker', 'Sneaker', '', '', ''),
(49, 1, 'Sneaker', 'Sneaker', '', '', ''),
(50, 2, 'Boot', 'Boot', '', '', ''),
(50, 1, 'Boot', 'Boot', '', '', ''),
(51, 2, 'Dép unisex', 'Dép unisex', '', '', ''),
(51, 1, 'Dép unisex', 'Dép unisex', '', '', ''),
(52, 2, 'Phụ kiện giày dép', 'Phụ kiện giày dép', '', '', ''),
(52, 1, 'Phụ kiện giày dép', 'Phụ kiện giày dép', '', '', ''),
(53, 2, 'Dây giày', 'Dây giày', '', '', ''),
(53, 1, 'Dây giày', 'Dây giày', '', '', ''),
(54, 2, 'Dung dịch làm sạch vết bẩn', 'Dung dịch làm sạch vết bẩn', '', '', ''),
(54, 1, 'Dung dịch làm sạch vết bẩn', 'Dung dịch làm sạch vết bẩn', '', '', ''),
(55, 2, 'Miếng lót giày', 'Miếng lót giày', '', '', ''),
(55, 1, 'Miếng lót giày', 'Miếng lót giày', '', '', ''),
(56, 2, 'Giày trẻ em', 'Giày trẻ em', '', '', ''),
(56, 1, 'Giày trẻ em', 'Giày trẻ em', '', '', ''),
(57, 2, 'Sneaker trẻ em', 'Sneaker trẻ em', '', '', ''),
(57, 1, 'Sneaker trẻ em', 'Sneaker trẻ em', '', '', ''),
(58, 2, 'Boot trẻ em', 'Boot trẻ em', '', '', ''),
(58, 1, 'Boot trẻ em', 'Boot trẻ em', '', '', ''),
(59, 2, 'Sandals trẻ em', 'Sandals trẻ em', '', '', ''),
(59, 1, 'Sandals trẻ em', 'Sandals trẻ em', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(44, 40, 0),
(43, 43, 1),
(43, 40, 0),
(42, 42, 1),
(42, 40, 0),
(41, 41, 1),
(41, 40, 0),
(40, 40, 0),
(39, 39, 1),
(33, 33, 1),
(34, 32, 0),
(32, 32, 0),
(33, 32, 0),
(39, 32, 0),
(38, 38, 1),
(35, 32, 0),
(34, 34, 1),
(38, 32, 0),
(37, 37, 1),
(37, 32, 0),
(36, 36, 1),
(36, 32, 0),
(35, 35, 1),
(44, 44, 1),
(45, 40, 0),
(45, 45, 1),
(46, 46, 0),
(47, 46, 0),
(47, 47, 1),
(48, 46, 0),
(48, 48, 1),
(49, 46, 0),
(49, 49, 1),
(50, 46, 0),
(50, 50, 1),
(51, 46, 0),
(51, 51, 1),
(52, 52, 0),
(53, 52, 0),
(53, 53, 1),
(54, 52, 0),
(54, 54, 1),
(55, 52, 0),
(55, 55, 1),
(56, 56, 0),
(57, 56, 0),
(57, 57, 1),
(58, 56, 0),
(58, 58, 1),
(59, 56, 0),
(59, 59, 1);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(39, 0),
(40, 0),
(41, 0),
(42, 0),
(43, 0),
(44, 0),
(45, 0),
(46, 0),
(47, 0),
(48, 0),
(49, 0),
(50, 0),
(51, 0),
(52, 0),
(53, 0),
(54, 0),
(55, 0),
(56, 0),
(57, 0),
(58, 0),
(59, 0);