-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(22, NULL, 0, 0, 0, 0, 1, '2019-12-13 15:31:34', '2019-12-13 15:31:34'),
(21, NULL, 0, 0, 0, 0, 1, '2019-12-13 15:31:34', '2019-12-13 15:31:34');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(22, 1, 'Đồng hồ nam', 'Đồng hồ nam', '', '', ''),
(22, 2, 'Đồng hồ nam', 'Đồng hồ nam', '', '', ''),
(21, 1, 'Nước hoa nữ', 'Nước hoa nữ', '', '', ''),
(21, 2, 'Nước hoa nữ', 'Nước hoa nữ', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(22, 22, 0),
(21, 21, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(21, 0),
(22, 0);