-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(11, 'Mồi, dây câu', 0, 1, '', '0'),
(10, 'Cần câu và máy câu', 0, 1, '', '0'),
(9, 'Phụ kiện cho dân câu', 0, 1, '', '0'),
(12, 'Hot sale', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, '', '', '', '', ''),
(10, '', '', '', '', ''),
(11, '', '', '', '', ''),
(12, '', '', '', '', '');