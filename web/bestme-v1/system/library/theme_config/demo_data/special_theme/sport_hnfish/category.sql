-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(41, '', 0, 0, 0, 0, 1, '2020-12-21 15:58:38', '2020-12-21 15:58:38'),
(40, '', 0, 0, 0, 0, 1, '2020-12-21 15:54:23', '2020-12-21 15:54:23'),
(39, '', 0, 0, 0, 0, 1, '2020-12-21 15:54:14', '2020-12-21 15:54:14'),
(38, '', 0, 0, 0, 0, 1, '2020-12-21 15:48:42', '2020-12-21 15:48:42'),
(37, '', 0, 0, 0, 0, 1, '2020-12-21 15:32:48', '2020-12-21 15:32:48'),
(35, '', 0, 0, 0, 0, 1, '2020-12-21 15:32:07', '2020-12-21 15:32:07'),
(34, '', 0, 0, 0, 0, 1, '2020-12-21 15:32:00', '2020-12-21 15:32:00'),
(36, '', 0, 0, 0, 0, 1, '2020-12-21 15:32:40', '2020-12-21 15:32:40'),
(32, '', 0, 0, 0, 0, 1, '2020-12-21 15:31:34', '2020-12-21 15:31:34'),
(33, '', 0, 0, 0, 0, 1, '2020-12-21 15:31:41', '2020-12-21 15:31:41');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(41, 1, 'Phụ kiện cho dân câu', 'Phụ kiện cho dân câu', '', '', ''),
(41, 2, 'Phụ kiện cho dân câu', 'Phụ kiện cho dân câu', '', '', ''),
(40, 2, 'Dây câu Leader', 'Dây câu Leader', '', '', ''),
(40, 1, 'Dây câu Leader', 'Dây câu Leader', '', '', ''),
(39, 1, 'Dây câu cước', 'Dây câu cước', '', '', ''),
(38, 2, 'Mồi câu', 'Mồi câu', '', '', ''),
(38, 1, 'Mồi câu', 'Mồi câu', '', '', ''),
(39, 2, 'Dây câu cước', 'Dây câu cước', '', '', ''),
(32, 1, 'Máy câu đứng', 'Máy câu đứng', '', '', ''),
(33, 2, 'Máy câu ngang', 'Máy câu ngang', '', '', ''),
(33, 1, 'Máy câu ngang', 'Máy câu ngang', '', '', ''),
(34, 2, 'Cần câu lục', 'Cần câu lục', '', '', ''),
(34, 1, 'Cần câu lục', 'Cần câu lục', '', '', ''),
(35, 2, 'Cần câu tay', 'Cần câu tay', '', '', ''),
(35, 1, 'Cần câu tay', 'Cần câu tay', '', '', ''),
(36, 2, 'Lưỡi câu đơn', 'Lưỡi câu đơn', '', '', ''),
(36, 1, 'Lưỡi câu đơn', 'Lưỡi câu đơn', '', '', ''),
(37, 2, 'Lưỡi câu Lancer', 'Lưỡi câu Lancer', '', '', ''),
(37, 1, 'Lưỡi câu Lancer', 'Lưỡi câu Lancer', '', '', ''),
(32, 2, 'Máy câu đứng', 'Máy câu đứng', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(34, 34, 0),
(33, 33, 0),
(36, 36, 0),
(35, 35, 0),
(32, 32, 0),
(37, 37, 0),
(41, 41, 0),
(40, 40, 0),
(39, 39, 0),
(38, 38, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(39, 0),
(40, 0),
(41, 0);