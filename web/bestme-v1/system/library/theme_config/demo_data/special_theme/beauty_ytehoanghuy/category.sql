-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(42, '', 0, 0, 0, 0, 1, '2020-12-10 17:32:08', '2020-12-10 17:32:08'),
(41, '', 0, 0, 0, 0, 1, '2020-12-10 17:31:55', '2020-12-10 17:31:55'),
(40, '', 0, 0, 0, 0, 1, '2020-12-10 17:31:31', '2020-12-10 17:31:31'),
(39, '', 0, 0, 0, 0, 1, '2020-12-10 17:31:14', '2020-12-10 17:31:14'),
(38, '', 0, 0, 0, 0, 1, '2020-12-10 17:30:51', '2020-12-10 17:30:51'),
(37, '', 33, 0, 0, 0, 1, '2020-12-10 17:29:32', '2020-12-10 17:29:32'),
(36, '', 33, 0, 0, 0, 1, '2020-12-10 17:29:32', '2020-12-10 17:29:32'),
(35, '', 33, 0, 0, 0, 1, '2020-12-10 17:29:32', '2020-12-10 17:29:32'),
(34, '', 33, 0, 0, 0, 1, '2020-12-10 17:29:32', '2020-12-10 17:29:32'),
(32, '', 0, 0, 0, 0, 1, '2020-12-10 17:28:20', '2020-12-10 17:28:20'),
(33, '', 0, 0, 0, 0, 1, '2020-12-10 17:29:32', '2020-12-10 17:29:32'),
(43, '', 0, 0, 0, 0, 1, '2020-12-10 17:32:30', '2020-12-10 17:32:30');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(41, 1, 'Thiết bị dụng cụ y tế gia đình', 'Thiết bị dụng cụ y tế gia đình', '', '', ''),
(41, 2, 'Thiết bị dụng cụ y tế gia đình', 'Thiết bị dụng cụ y tế gia đình', '', '', ''),
(40, 1, 'Ống nghiệm, kim lấy mẫu', 'Ống nghiệm, kim lấy mẫu', '', '', ''),
(40, 2, 'Ống nghiệm, kim lấy mẫu', 'Ống nghiệm, kim lấy mẫu', '', '', ''),
(39, 1, 'Nhi, sản phụ khoa', 'Nhi, sản phụ khoa', '', '', ''),
(39, 2, 'Nhi, sản phụ khoa', 'Nhi, sản phụ khoa', '', '', ''),
(38, 1, 'Khử khuẩn, tiệt trùng', 'Khử khuẩn, tiệt trùng', '', '', ''),
(38, 2, 'Khử khuẩn, tiệt trùng', 'Khử khuẩn, tiệt trùng', '', '', ''),
(37, 2, 'Dao kéo phẫu thuật', 'Dao kéo phẫu thuật', '', '', ''),
(37, 1, 'Dao kéo phẫu thuật', 'Dao kéo phẫu thuật', '', '', ''),
(32, 1, 'Chẩn đoán hình ảnh', 'Chẩn đoán hình ảnh', '', '', ''),
(33, 2, 'Dụng cụ phẫu thuật', 'Dụng cụ phẫu thuật', '', '', ''),
(33, 1, 'Dụng cụ phẫu thuật', 'Dụng cụ phẫu thuật', '', '', ''),
(34, 2, 'Chỉ phẫu thuật', 'Chỉ phẫu thuật', '', '', ''),
(34, 1, 'Chỉ phẫu thuật', 'Chỉ phẫu thuật', '', '', ''),
(35, 2, 'Băng gạc phẫu thuật', 'Băng gạc phẫu thuật', '', '', ''),
(35, 1, 'Băng gạc phẫu thuật', 'Băng gạc phẫu thuật', '', '', ''),
(36, 2, 'Kim luồn, kim gây tê', 'Kim luồn, kim gây tê', '', '', ''),
(36, 1, 'Kim luồn, kim gây tê', 'Kim luồn, kim gây tê', '', '', ''),
(32, 2, 'Chẩn đoán hình ảnh', 'Chẩn đoán hình ảnh', '', '', ''),
(42, 2, 'Tiêm, truyền dịch', 'Tiêm, truyền dịch', '', '', ''),
(42, 1, 'Tiêm, truyền dịch', 'Tiêm, truyền dịch', '', '', ''),
(43, 2, 'Hô hấp', 'Hô hấp', '', '', ''),
(43, 1, 'Hô hấp', 'Hô hấp', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(43, 43, 0),
(42, 42, 0),
(34, 33, 0),
(33, 33, 0),
(35, 33, 0),
(34, 34, 1),
(41, 41, 0),
(40, 40, 0),
(32, 32, 0),
(35, 35, 1),
(39, 39, 0),
(38, 38, 0),
(37, 37, 1),
(37, 33, 0),
(36, 36, 1),
(36, 33, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(39, 0),
(40, 0),
(41, 0),
(42, 0),
(43, 0);