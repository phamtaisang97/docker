-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(38, NULL, 0, 0, 0, 0, 1, '2020-12-17 11:21:19', '2020-12-17 11:21:19'),
(37, NULL, 0, 0, 0, 0, 1, '2020-12-17 11:21:19', '2020-12-17 11:21:19'),
(36, NULL, 0, 0, 0, 0, 1, '2020-12-17 11:21:19', '2020-12-17 11:21:19'),
(35, NULL, 0, 0, 0, 0, 1, '2020-12-17 11:21:19', '2020-12-17 11:21:19'),
(34, NULL, 0, 0, 0, 0, 1, '2020-12-17 11:21:19', '2020-12-17 11:21:19'),
(33, NULL, 0, 0, 0, 0, 1, '2020-12-17 11:21:19', '2020-12-17 11:21:19'),
(39, NULL, 0, 0, 0, 0, 1, '2020-12-17 11:24:25', '2020-12-17 11:24:25');


-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(37, 1, 'Thiệp cưới', 'Thiệp cưới', '', '', ''),
(37, 2, 'Thiệp cưới', 'Thiệp cưới', '', '', ''),
(36, 1, 'Váy cưới màu sắc', 'Váy cưới màu sắc', '', '', ''),
(36, 2, 'Váy cưới màu sắc', 'Váy cưới màu sắc', '', '', ''),
(35, 1, 'Váy cưới dáng xòa', 'Váy cưới dáng xòa', '', '', ''),
(35, 2, 'Váy cưới dáng xòa', 'Váy cưới dáng xòa', '', '', ''),
(34, 1, 'Váy cưới bó sát', 'Váy cưới bó sát', '', '', ''),
(33, 1, 'Váy cưới bà bầu', 'Váy cưới bà bầu', '', '', ''),
(34, 2, 'Váy cưới bó sát', 'Váy cưới bó sát', '', '', ''),
(33, 2, 'Váy cưới bà bầu', 'Váy cưới bà bầu', '', '', ''),
(39, 1, 'Vest Cưới', 'Vest Cưới', '', '', ''),
(39, 2, 'Vest Cưới', 'Vest Cưới', '', '', ''),
(38, 2, 'Hoa cưới', 'Hoa cưới', '', '', ''),
(38, 1, 'Hoa cưới', 'Hoa cưới', '', '', '');


-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(38, 38, 0),
(39, 39, 0),
(37, 37, 0),
(36, 36, 0),
(35, 35, 0),
(34, 34, 0),
(33, 33, 0);


-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(39, 0);
