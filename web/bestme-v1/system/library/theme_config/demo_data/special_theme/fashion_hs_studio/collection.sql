-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(26, 'Thiệp cưới đẹp mắt', 0, 1, '', '0'),
(25, 'Hoa cưới lộng lẫy', 0, 1, '', '0'),
(24, 'Váy cưới tôn dáng', 0, 1, '', '0'),
(18, 'Váy cưới sang trọng', 0, 1, '', '0'),
(19, 'Các loại hoa cưới', 0, 1, '', '0'),
(20, 'Các mẫu thiệp cưới', 0, 1, '', '0'),
(21, 'Váy cưới lộng lẫy', 0, 1, '', '0'),
(22, 'Váy cưới sang trọng', 0, 1, '', '0'),
(23, 'Váy cưới quý phái', 0, 1, '', '0'),
(16, 'Vest Cưới Sang Trọng', 0, 1, NULL, '0'),
(27, 'Hiển thị 2 bst', 0, 0, '', '1'),
(28, 'Hiển thị 2 bst 2', 0, 0, '', '1'),
(29, 'Bộ sưu tập váy cưới', 0, 1, '', '1'),
(30, 'Đồ trang hoàng ngày cưới', 0, 1, '', '1');


-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(18, '', '', '', '', ''),
(19, '', '', '', '', ''),
(20, '', '', '', '', ''),
(21, 'https://cdn.bestme.asia/images/hs-studio/bst1.jpg', '&lt;p&gt;Wedding #1&lt;/p&gt;', '', '', ''),
(22, 'https://cdn.bestme.asia/images/hs-studio/bst2.jpg', '&lt;p&gt;Wedding #2&lt;/p&gt;', '', '', ''),
(23, 'https://cdn.bestme.asia/images/hs-studio/bst3.jpg', '&lt;p&gt;Wedding #3&lt;/p&gt;', '', '', ''),
(24, 'https://cdn.bestme.asia/images/hs-studio/bst4.jpg', '&lt;p&gt;Wedding #4&lt;/p&gt;', '', '', ''),
(25, 'https://cdn.bestme.asia/images/hs-studio/bst5.jpg', '&lt;p&gt;Wedding #5&lt;/p&gt;', '', '', ''),
(26, 'https://cdn.bestme.asia/images/hs-studio/bst6.jpg', '&lt;p&gt;Wedding #6&lt;/p&gt;', '', '', ''),
(27, '', '&lt;p&gt;Hiển thị 2 bst&lt;/p&gt;', '', '', ''),
(16, '', '', '', '', ''),
(28, '', '&lt;p&gt;Hiển thị 2 bst 2&lt;/p&gt;', '', '', ''),
(29, '', '&lt;p&gt;Hiển thị 4 bst &lt;/p&gt;', '', '', ''),
(30, '', '&lt;p&gt;Hiển thị 4 bst 2&lt;/p&gt;', '', '', '');
