-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(9, 'Vest Cưới Sang Trọng', 0, 1, '', '0'),
(8, 'Váy Cưới Cho Mẹ Bầu Xinh Đẹp', 0, 1, '', '0'),
(10, 'Váy Cưới Tôn Dáng', 0, 1, '', '0'),
(11, 'Váy Cưới Màu Sắc', 0, 1, '', '0'),
(12, 'Hoa Cưới', 0, 1, '', '0'),
(13, 'Xe Cưới', 0, 1, '', '0'),
(14, 'Ảnh Cưới', 0, 1, '', '0'),
(15, 'Thiệp cưới', 0, 1, '', '0'),
(16, 'Váy', 0, 1, '', '1'),
(17, 'Dịch vụ', 0, 1, '', '1'),
(18, 'VÁY TRẮNG', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(16, '', '', '', '', ''),
(13, '', '', '', '', ''),
(14, '', '', '', '', ''),
(15, 'https://cdn.bestme.asia/images/x2/thiep-cuoi-100_enTRZPw.jpg', '', '', '', ''),
(8, '', '', '', '', ''),
(9, 'https://cdn.bestme.asia/images/x2/vest-cuoi-100_JiTPrDu.jpg', '', '', '', ''),
(10, '', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/x2/vay-cuoi-100_ObwCL6M.jpg', '', '', '', ''),
(12, 'https://cdn.bestme.asia/images/x2/hoa-cuoi-100_nChH1gf.jpg', '', '', '', ''),
(17, '', '', '', '', ''),
(18, '', '', '', '', '');