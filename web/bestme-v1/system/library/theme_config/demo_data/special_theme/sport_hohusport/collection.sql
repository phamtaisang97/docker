-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(9, 'Sản phẩm mới', 0, 1, '', '0'),
(10, 'Sản phẩm bán chạy', 0, 1, '', '0'),
(11, 'Sản phẩm nổi bật', 0, 1, '', '0'),
(12, 'Tạ tập phòng GYM', 0, 1, '', '0'),
(13, 'BST TH', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, '', '', '', '', ''),
(10, 'https://cdn.bestme.asia/images/thethaohoanghuy/bst2100.jpg', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/thethaohoanghuy/bst1100.jpg', '', '', '', ''),
(12, '', '', '', '', ''),
(13, '', '', '', '', '');