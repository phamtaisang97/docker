-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(42, '', 0, 0, 0, 0, 1, '2020-12-08 10:07:27', '2020-12-08 10:07:27'),
(41, '', 40, 0, 0, 0, 1, '2020-12-08 10:07:09', '2020-12-08 10:07:09'),
(40, '', 0, 0, 0, 0, 1, '2020-12-08 10:07:09', '2020-12-08 10:07:09'),
(39, '', 0, 0, 0, 0, 1, '2020-12-08 10:05:03', '2020-12-08 10:05:03'),
(36, NULL, 40, 0, 0, 0, 1, '2020-12-08 10:00:23', '2020-12-08 10:07:09'),
(35, NULL, 40, 0, 0, 0, 1, '2020-12-08 10:00:23', '2020-12-08 10:07:09'),
(34, NULL, 0, 0, 0, 0, 1, '2020-12-08 10:00:23', '2020-12-08 10:00:23'),
(33, NULL, 0, 0, 0, 0, 1, '2020-12-08 10:00:23', '2020-12-08 10:03:39'),
(32, NULL, 0, 0, 0, 0, 1, '2020-12-08 10:00:22', '2020-12-08 10:00:22'),
(37, '', 33, 0, 0, 0, 1, '2020-12-08 10:03:39', '2020-12-08 10:03:39'),
(38, '', 33, 0, 0, 0, 1, '2020-12-08 10:03:39', '2020-12-08 10:03:39');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(42, 2, 'Ghế massage', 'Ghế massage', '', '', ''),
(41, 1, 'Máy tập cardio', 'Máy tập cardio', '', '', ''),
(41, 2, 'Máy tập cardio', 'Máy tập cardio', '', '', ''),
(40, 1, 'Thiết bị phòng GYM', 'Thiết bị phòng GYM', '', '', ''),
(40, 2, 'Thiết bị phòng GYM', 'Thiết bị phòng GYM', '', '', ''),
(39, 1, 'Máy rung giảm cân', 'Máy rung giảm cân', '', '', ''),
(39, 2, 'Máy rung giảm cân', 'Máy rung giảm cân', '', '', ''),
(38, 1, 'Xe đạp tập ngoài trời', 'Xe đạp tập ngoài trời', '', '', ''),
(37, 1, 'Xe đạp tập trong nhà', 'Xe đạp tập trong nhà', '', '', ''),
(38, 2, 'Xe đạp tập ngoài trời', 'Xe đạp tập ngoài trời', '', '', ''),
(36, 1, 'Phụ kiện tập GYM', 'Phụ kiện tập GYM', '', '', ''),
(37, 2, 'Xe đạp tập trong nhà', 'Xe đạp tập trong nhà', '', '', ''),
(32, 2, 'Máy chạy bộ', 'Máy chạy bộ', '', '', ''),
(32, 1, 'Máy chạy bộ', 'Máy chạy bộ', '', '', ''),
(33, 2, 'Xe đạp thể thao', 'Xe đạp thể thao', '', '', ''),
(33, 1, 'Xe đạp thể thao', 'Xe đạp thể thao', '', '', ''),
(34, 2, 'Giàn tạ đa năng', 'Giàn tạ đa năng', '', '', ''),
(34, 1, 'Giàn tạ đa năng', 'Giàn tạ đa năng', '', '', ''),
(35, 2, 'Máy tập nhóm cơ', 'Máy tập nhóm cơ', '', '', ''),
(35, 1, 'Máy tập nhóm cơ', 'Máy tập nhóm cơ', '', '', ''),
(36, 2, 'Phụ kiện tập GYM', 'Phụ kiện tập GYM', '', '', ''),
(42, 1, 'Ghế massage', 'Ghế massage', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(35, 35, 0),
(34, 34, 0),
(33, 33, 0),
(32, 32, 0),
(42, 42, 0),
(41, 41, 1),
(37, 33, 0),
(36, 36, 0),
(41, 40, 0),
(40, 40, 0),
(39, 39, 0),
(38, 38, 1),
(38, 33, 0),
(37, 37, 1);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(39, 0),
(40, 0),
(41, 0),
(42, 0);