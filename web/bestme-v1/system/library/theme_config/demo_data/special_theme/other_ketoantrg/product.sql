-- oc_product PRIMARY KEY (`product_id`)
-- delete first
INSERT IGNORE INTO `oc_product` (`product_id`, `model`, `sku`, `common_barcode`, `common_sku`, `common_compare_price`, `common_price`, `common_cost_price`, `barcode`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `sale_on_out_of_stock`, `stock_status_id`, `image`, `image_alt`, `multi_versions`, `manufacturer_id`, `shipping`, `price`, `price_currency_id`, `compare_price`, `c_price_currency_id`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `channel`, `viewed`, `demo`, `deleted`, `default_store_id`, `date_added`, `date_modified`) VALUES
(184, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/ketoantrongoi/sp4.png', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 1, NULL, 0, '2020-12-11 13:46:53', '2020-12-11 14:48:12'),
(185, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/ketoantrongoi/dfrsfdgr.png', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 1, NULL, 0, '2020-12-11 13:48:01', '2020-12-11 13:48:01'),
(186, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/ketoantrongoi/sp2.png', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 1, NULL, 0, '2020-12-11 13:48:33', '2020-12-11 14:46:45'),
(187, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/ketoantrongoi/sp1.png', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 1, NULL, 0, '2020-12-11 13:49:05', '2020-12-11 14:46:31'),
(188, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/ketoantrongoi/sp3.png', '', 0, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, 0, 1, NULL, 0, '2020-12-11 13:49:39', '2020-12-11 14:47:54');

-- restore soft-deleted products if demo
UPDATE `oc_product` SET `deleted` = NULL
WHERE `demo` = 1
  AND `product_id` IN (
                       184,
                       185,
                       186,
                       187,
                       188
  );

-- oc_product_collection PRIMARY KEY (`product_collection_id`)
INSERT IGNORE INTO `oc_product_collection` (`product_collection_id`, `product_id`, `collection_id`, `sort_order`) VALUES
(405, 188, 19, 0),
(399, 187, 20, 0),
(404, 188, 20, 0),
(403, 188, 21, 0),
(402, 186, 19, 0),
(408, 184, 19, 0),
(401, 186, 20, 0),
(400, 186, 21, 0),
(407, 184, 20, 0),
(406, 184, 21, 0),
(387, 185, 21, 1),
(395, 185, 19, 1);

-- oc_product_description PRIMARY KEY (`product_id`,`language_id`)
INSERT IGNORE INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `sub_description`, `seo_title`, `seo_description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(184, 2, 'Dịch vụ ngân hàng điện tử Bankhub', '&lt;p&gt;BANKHUB Phần mềm cho phép kế toán ngồi tại văn phòng cũng có thể thực hiện đầy đủ giao dịch với ngân hàng ngay trên dịch vụ kế toán.&lt;/p&gt;\r\n\r\n&lt;p&gt;CHUYỂN TIỀN TRỰC TUYẾN NGAY TRÊN PHẦN MỀM&lt;/p&gt;\r\n\r\n&lt;p&gt;Cho phép kế toán viên lập, kế toán trưởng phê duyệt lệnh chuyển tiền ngay trên phần mềm&lt;br /&gt;\r\nThực hiện được các loại Ủy nhiệm chi cho nhà cung cấp, thông thường hoặc chuyển nội bộ&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;br /&gt;\r\nTRA CỨU SỐ DƯ TÀI KHOẢN NHANH CHÓNG VÀ CHÍNH XÁC&lt;/p&gt;\r\n\r\n&lt;p&gt;Kế toán dễ dàng tra cứu số dư tài khoản thanh toán ngay trên phần mềm&lt;br /&gt;\r\nTiết kiệm thời gian và thao tác kiểm tra, xác minh tài khoản&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;br /&gt;\r\nDỄ DÀNG TRA CỨU LỊCH SỬ GIAO DỊCH&lt;/p&gt;\r\n\r\n&lt;p&gt;Tra cứu nhanh lịch sử giao dịch của tài khoản trong một khoảng thời gian&lt;br /&gt;\r\nTiết kiệm thời gian tra cứu tại ngân hàng&lt;br /&gt;\r\nLập nhanh UNC, Chứng từ thu tiền gửi từ những giao dịch thu, chi tiền không thực hiện trên phần mềm  SME.NET 2020&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;br /&gt;\r\nTHUẬN TIỆN QUẢN LÝ LỆNH CHUYỂN TIỀN&lt;/p&gt;\r\n\r\n&lt;p&gt;Dễ dàng xem thông tin lệnh chuyển tiền&lt;br /&gt;\r\nNhanh chóng phê duyệt lệnh chuyển tiền&lt;br /&gt;\r\nTức thời thu hồi lệnh chuyển tiền&lt;br /&gt;\r\nNắm bắt nhật ký giao dịch lệnh chuyển tiền&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;br /&gt;\r\nTHANH TOÁN LƯƠNG TRỰC TUYẾN&lt;/p&gt;\r\n\r\n&lt;p&gt;Chuyển tiền lương cho nhân viên&lt;br /&gt;\r\nPhê duyệt lương từ kế toán trưởng và giám đốc ngay trên phần mềm&lt;br /&gt;\r\nKhông phải mất công nhập liệu 2 lần&lt;br /&gt;\r\nTự động hạch toán dễ dàng ngay trên phần mềm&lt;/p&gt;', '', '', '', '', '', '', ''),
(185, 2, 'Dịch vụ chữ ký số eSign', '&lt;p&gt;ĐÁP ỨNG ĐẦY ĐỦ NGHIỆP VỤ KÝ KẾT VĂN BẢN/HỢP ĐỒNG/GIAO DỊCH ĐIỆN TỬ&lt;br /&gt;\r\nĐiện tử hóa mọi việc ký kết văn bản, hợp đồng&lt;br /&gt;\r\nHệ sinh thái quản trị hiện đại và an toàn&lt;br /&gt;\r\nSẵn sàng tích hợp với các hệ thống khác&lt;br /&gt;\r\nCông nghệ vượt trội – An toàn tuyệt đối&lt;/p&gt;', '', '', '', '', '', '', ''),
(186, 2, 'Dịch vụ thuế điện tử', '&lt;p&gt;Dịch vụ kê khai, nộp thuế điện tử ngay trên phần mềm kế toán&lt;/p&gt;\r\n\r\n&lt;p&gt;Đã được Tổng Cục Thuế cấp phép&lt;br /&gt;\r\nĐáp ứng đầy đủ mọi nghiệp vụ kê khai, nộp thuế cho Doanh nghiệp và Cá nhân (doanh nghiệp kê khai hộ)&lt;br /&gt;\r\nTích hợp ngay trên phần mềm kế toán MISA tạo thành hệ sinh thái khép kín giúp kê khai, nộp thuế thuận tiện, nhanh chóng hơn&lt;/p&gt;', '', '', '', '', '', '', ''),
(187, 2, 'Hóa đơn điện tử  meInvoice', '&lt;p&gt;Thay đổi hoàn toàn cách thức phát hành, quản lý, báo cáo hóa đơn của Doanh nghiệp bạn!&lt;/p&gt;\r\n\r\n&lt;p&gt;- Đáp ứng đầy đủ nghiệp vụ hóa đơn theo đúng Nghị định 119/2018/NĐ-CP, Thông tư 32/2011/TT-BTC, Thông tư 39/2014/TT-BTC… và sẵn sàng đáp ứng Thông tư 68/2019/TT-BTC&lt;/p&gt;\r\n\r\n&lt;p&gt;- Người mua tức thời nhận được hóa đơn điện tử qua email, SMS và thực hiện phát hành, quản lý, báo cáo hóa đơn mọi lúc, mọi nơi qua mobile, website, desktop&lt;/p&gt;\r\n\r\n&lt;p&gt;- Phần mềm hóa đơn điện tử duy nhất tại Việt Nam ứng dụng công nghệ Blockchain đảm bảo an toàn, bảo mật và chống làm giả hóa đơn&lt;/p&gt;', '', '', '', '', '', '', ''),
(188, 2, 'Phần mềm kế toán doanh nghiệp SME.NET 2021', '&lt;p&gt;Cung cấp mọi góc nhìn về tình hình tài chính doanh nghiệp&lt;br /&gt;\r\nNhập liệu tự động và kiểm soát tính hợp lệ từ các giao dịch ngân hàng, hóa đơn, mã số thuế…&lt;br /&gt;\r\nLà một phần mềm kế toán đơn giản, thông minh và đôi lúc thật kỳ diệu.&lt;/p&gt;\r\n\r\n&lt;p&gt;Nâng cao năng suất&lt;br /&gt;\r\nTự động hạch toán từ: Hóa đơn đầu vào, Báo cáo ngân hàng.&lt;/p&gt;\r\n\r\n&lt;p&gt;Tự động tổng hợp báo cáo thuế, BCTC tiết kiệm 80% thời gian nhập liệu.&lt;/p&gt;\r\n\r\n&lt;p&gt;Kết nối thông minh&lt;br /&gt;\r\nKết nối Tổng cục thuế, Ngân hàng, Hóa đơn điện tử, Chữ ký số, Phần mềm bán hàng… tạo thành hệ sinh thái xử lý dữ liệu nhanh, tiện.&lt;/p&gt;\r\n\r\n&lt;p&gt;Quản trị tài chính tức thời&lt;br /&gt;\r\nGiám đốc luôn nắm được tình hình tài chính: Doanh thu, Lợi nhuận, Chi phí, Công nợ,… mọi lúc, mọi nơi kịp thời ra quyết định điều hành.&lt;/p&gt;', '', '', '', '', '', '', '');

-- oc_product_image PRIMARY KEY (`product_image_id`)
-- nothing

-- oc_product_to_category PRIMARY KEY (`product_id`,`category_id`)
-- nothing

-- oc_product_to_store PRIMARY KEY (`product_id`,`store_id`)
INSERT IGNORE INTO `oc_product_to_store` (`product_id`, `store_id`, `product_version_id`, `quantity`, `cost_price`) VALUES
(186, 0, 0, 0, '0.0000'),
(188, 0, 0, 0, '0.0000'),
(187, 0, 0, 0, '0.0000'),
(185, 0, 0, 0, '0.0000'),
(184, 0, 0, 0, '0.0000');