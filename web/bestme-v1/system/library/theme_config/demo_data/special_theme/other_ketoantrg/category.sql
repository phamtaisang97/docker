-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(49, '', 0, 0, 0, 0, 1, '2020-12-11 14:31:29', '2020-12-11 14:31:29'),
(50, '', 0, 0, 0, 0, 1, '2020-12-11 14:31:41', '2020-12-11 14:31:41'),
(51, '', 0, 0, 0, 0, 1, '2020-12-11 14:31:59', '2020-12-11 14:31:59'),
(52, '', 0, 0, 0, 0, 1, '2020-12-11 14:32:14', '2020-12-11 14:32:14'),
(53, '', 0, 0, 0, 0, 1, '2020-12-11 14:32:40', '2020-12-11 14:32:40');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(49, 2, 'Phần mềm tạo báo cáo', 'Phần mềm tạo báo cáo', '', '', ''),
(49, 1, 'Phần mềm tạo báo cáo', 'Phần mềm tạo báo cáo', '', '', ''),
(50, 2, 'Phần mềm quản lý tồn kho', 'Phần mềm quản lý tồn kho', '', '', ''),
(50, 1, 'Phần mềm quản lý tồn kho', 'Phần mềm quản lý tồn kho', '', '', ''),
(51, 2, 'Phần mềm tính lương', 'Phần mềm tính lương', '', '', ''),
(51, 1, 'Phần mềm tính lương', 'Phần mềm tính lương', '', '', ''),
(52, 2, 'Phần mềm tính P&L', 'Phần mềm tính P&L', '', '', ''),
(52, 1, 'Phần mềm tính P&L', 'Phần mềm tính P&L', '', '', ''),
(53, 2, 'Phần mềm kiểm soát nội bộ', 'Phần mềm kiểm soát nội bộ', '', '', ''),
(53, 1, 'Phần mềm kiểm soát nội bộ', 'Phần mềm kiểm soát nội bộ', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(51, 51, 0),
(50, 50, 0),
(49, 49, 0),
(52, 52, 0),
(53, 53, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(49, 0),
(50, 0),
(51, 0),
(52, 0),
(53, 0);