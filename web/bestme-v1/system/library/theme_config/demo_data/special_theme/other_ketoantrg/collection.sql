-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(19, 'Kế toán quản trị doanh nghiệp', 0, 1, '', '0'),
(20, 'Giải pháp cho doanh nghiệp nhỏ', 0, 1, '', '0'),
(21, 'Dịch vụ kế toán', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(21, 'https://cdn.bestme.asia/images/ketoantrongoi/bst3-tinhphi.jpg', '', '', '', ''),
(19, 'https://cdn.bestme.asia/images/ketoantrongoi/bst1-tinhphi.jpg', '', '', '', ''),
(20, 'https://cdn.bestme.asia/images/ketoantrongoi/bst2-tinhphi.jpg', '', '', '', '');