-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(12, 'Chair collection', 0, 1, '', '0'),
(11, 'BST Bán chạy', 0, 1, '', '0'),
(10, 'BST Mới', 0, 1, '', '0'),
(9, 'BST Khuyến mãi', 0, 1, '', '0'),
(13, 'Lamp collection', 0, 1, '', '0'),
(14, 'Sofa collection', 0, 1, '', '0'),
(15, 'Sale up to 70% off', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, '', '&lt;p&gt;BST Khuyến mãi&lt;/p&gt;', '', '', ''),
(10, '', '&lt;p&gt;BST Mới&lt;/p&gt;', '', '', ''),
(11, '', '&lt;p&gt;BST Bán chạy&lt;/p&gt;\r\n\r\n&lt;p&gt;BST Bán chạy&lt;/p&gt;\r\n\r\n&lt;p&gt;BST Bán chạy&lt;/p&gt;\r\n\r\n&lt;p&gt;BST Bán chạy&lt;/p&gt;\r\n\r\n&lt;p&gt;BST Bán chạy&lt;/p&gt;\r\n\r\n&lt;p&gt;BST Bán chạy&lt;/p&gt;\r\n\r\n&lt;p&gt;BST Bán chạy&lt;/p&gt;\r\n\r\n&lt;p&gt;BST Bán chạy&lt;/p&gt;', '', '', ''),
(12, 'https://cdn.bestme.asia/images/furniturer-furniter-demo/group-857.png', '&lt;p&gt;Chair collection&lt;/p&gt;', '', '', ''),
(13, 'https://cdn.bestme.asia/images/furniturer-furniter-demo/group-860.png', '&lt;p&gt;Lamp collection&lt;/p&gt;', '', '', ''),
(14, 'https://cdn.bestme.asia/images/furniturer-furniter-demo/group-889.png', '&lt;p&gt;Sofa collection&lt;/p&gt;', '', '', ''),
(15, 'https://cdn.bestme.asia/images/furniturer-furniter-demo/group-890.png', '', '', '', '');