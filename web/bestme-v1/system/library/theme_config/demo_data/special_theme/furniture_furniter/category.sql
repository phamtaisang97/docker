-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(36, NULL, 0, 0, 0, 0, 1, '2021-03-23 11:54:58', '2021-03-23 11:54:58'),
(35, NULL, 0, 0, 0, 0, 1, '2021-03-23 11:54:33', '2021-03-23 11:54:33'),
(34, NULL, 0, 0, 0, 0, 1, '2021-03-23 11:43:22', '2021-03-23 11:43:22'),
(33, NULL, 0, 0, 0, 0, 1, '2021-03-23 11:43:22', '2021-03-23 11:43:22'),
(32, NULL, 0, 0, 0, 0, 1, '2021-03-23 11:43:06', '2021-03-23 11:43:06'),
(37, NULL, 0, 0, 0, 0, 1, '2021-03-23 11:55:09', '2021-03-23 11:55:09');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(32, 2, 'Bàn', 'Bàn', '', '', ''),
(32, 1, 'Bàn', 'Bàn', '', '', ''),
(33, 2, 'Ghế', 'Ghế', '', '', ''),
(33, 1, 'Ghế', 'Ghế', '', '', ''),
(34, 2, 'Sofa', 'Sofa', '', '', ''),
(34, 1, 'Sofa', 'Sofa', '', '', ''),
(35, 2, 'Đồ trang trí', 'Đồ trang trí', '', '', ''),
(35, 1, 'Đồ trang trí', 'Đồ trang trí', '', '', ''),
(36, 2, 'Kệ sách', 'Kệ sách', '', '', ''),
(36, 1, 'Kệ sách', 'Kệ sách', '', '', ''),
(37, 2, 'Tủ quần áo', 'Tủ quần áo', '', '', ''),
(37, 1, 'Tủ quần áo', 'Tủ quần áo', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(35, 35, 0),
(34, 34, 0),
(33, 33, 0),
(32, 32, 0),
(37, 37, 0),
(36, 36, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0);