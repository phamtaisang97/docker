-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(36, '', 0, 0, 0, 0, 1, '2020-09-28 11:17:33', '2020-09-28 11:17:33'),
(22, '', 36, 0, 0, 0, 1, '2020-06-15 18:06:09', '2020-09-28 11:19:33'),
(24, NULL, 36, 0, 0, 0, 1, '2020-06-15 18:06:09', '2020-09-28 11:17:33'),
(27, NULL, 0, 0, 0, 0, 1, '2020-06-15 18:06:09', '2020-06-15 18:06:09'),
(38, '', 22, 0, 0, 0, 1, '2020-09-28 11:19:33', '2020-09-28 11:19:33'),
(30, NULL, 0, 0, 0, 0, 1, '2020-06-15 18:06:09', '2020-06-15 18:06:09'),
(31, NULL, 0, 0, 0, 0, 1, '2020-06-15 18:06:09', '2020-06-15 18:06:09'),
(32, NULL, 0, 0, 0, 0, 1, '2020-06-15 18:06:09', '2020-06-15 18:06:09'),
(33, NULL, 0, 0, 0, 0, 1, '2020-06-15 18:06:09', '2020-06-15 18:06:09'),
(34, NULL, 0, 0, 0, 0, 1, '2020-06-15 18:06:09', '2020-06-15 18:06:09'),
(35, NULL, 0, 0, 0, 0, 1, '2020-06-15 18:06:09', '2020-06-15 18:06:09'),
(37, '', 22, 0, 0, 0, 1, '2020-09-28 11:19:33', '2020-09-28 11:19:33');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(36, 1, 'Đồng Hồ Cao Cấp', 'Đồng Hồ Cao Cấp', '', '', ''),
(36, 2, 'Đồng Hồ Cao Cấp', 'Đồng Hồ Cao Cấp', '', '', ''),
(22, 2, 'Đồng Hồ Nam', 'Đồng Hồ Nam', '', '', ''),
(22, 1, 'Đồng Hồ Nam', 'Đồng Hồ Nam', '', '', ''),
(37, 2, 'Đồng Hồ Nam Đính Đá', 'Đồng Hồ Nam Đính Đá', '', '', ''),
(37, 1, 'Đồng Hồ Nam Đính Đá', 'Đồng Hồ Nam Đính Đá', '', '', ''),
(24, 2, 'Đồng Hồ Nữ', 'Đồng Hồ Nữ', '', '', ''),
(24, 1, 'Đồng Hồ Nữ', 'Đồng Hồ Nữ', '', '', ''),
(38, 2, 'Đồ Hồ Nan Dây Da', 'Đồ Hồ Nan Dây Da', '', '', ''),
(38, 1, 'Đồ Hồ Nan Dây Da', 'Đồ Hồ Nan Dây Da', '', '', ''),
(27, 2, 'Đồng Hồ Đôi', 'Đồng Hồ Đôi', '', '', ''),
(27, 1, 'Đồng Hồ Đôi', 'Đồng Hồ Đôi', '', '', ''),
(32, 2, 'Đồng Hồ Cổ', 'Đồng Hồ Cổ', '', '', ''),
(31, 2, 'Đồng Hồ Cổ', 'Đồng Hồ Cổ', '', '', ''),
(30, 2, 'Đồng Hồ Cổ', 'Đồng Hồ Cổ', '', '', ''),
(32, 1, 'Đồng Hồ Cổ', 'Đồng Hồ Cổ', '', '', ''),
(31, 1, 'Đồng Hồ Cổ', 'Đồng Hồ Cổ', '', '', ''),
(30, 1, 'Đồng Hồ Cổ', 'Đồng Hồ Cổ', '', '', ''),
(34, 2, 'Đồng Hồ Thời Trang', 'Đồng Hồ Thời Trang', '', '', ''),
(35, 2, 'Đồng Hồ Thời Trang', 'Đồng Hồ Thời Trang', '', '', ''),
(33, 2, 'Đồng Hồ Thời Trang', 'Đồng Hồ Thời Trang', '', '', ''),
(34, 1, 'Đồng Hồ Thời Trang', 'Đồng Hồ Thời Trang', '', '', ''),
(35, 1, 'Đồng Hồ Thời Trang', 'Đồng Hồ Thời Trang', '', '', ''),
(33, 1, 'Đồng Hồ Thời Trang', 'Đồng Hồ Thời Trang', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(36, 36, 0),
(38, 38, 2),
(38, 36, 0),
(22, 22, 1),
(38, 22, 1),
(24, 24, 0),
(37, 37, 2),
(22, 36, 0),
(37, 22, 1),
(27, 27, 0),
(37, 36, 0),
(32, 32, 0),
(31, 31, 0),
(30, 30, 0),
(34, 34, 0),
(33, 33, 0),
(35, 35, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(22, 0),
(24, 0),
(27, 0),
(30, 0),
(31, 0),
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0);