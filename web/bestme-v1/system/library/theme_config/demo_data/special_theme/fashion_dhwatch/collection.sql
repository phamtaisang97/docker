-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(11, 'Đồng Hồ Đôi', 0, 1, '', '0'),
(12, 'Đồng Hồ Rolex', 0, 1, '', '0'),
(9, 'Đồng Hồ Nữ', 0, 1, '', '0'),
(10, 'Đồng Hồ Nam', 0, 1, '', '0'),
(13, 'TH2', 0, 1, '', '1'),
(14, 'KM', 0, 1, '', '0'),
(15, 'Đồng Hồ Hublot', 0, 1, '', '0'),
(16, 'Đồng Hồ Thời Trang', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(12, 'https://cdn.bestme.asia/images/dhwatch/bst-rolex-2-100.jpg', '', '', '', ''),
(13, '', '', '', '', ''),
(14, '', '', '', '', ''),
(15, 'https://cdn.bestme.asia/images/dhwatch/bst-hublot-2-100.jpg', '', '', '', ''),
(16, 'https://cdn.bestme.asia/images/dhwatch/bst-g-shock-2-100.jpg', '', '', '', ''),
(9, '', '', '', '', ''),
(11, '', '', '', '', ''),
(10, 'https://cdn.bestme.asia/images/dhwatch/bst-g-shock-1-100.jpg', '', '', '', '');