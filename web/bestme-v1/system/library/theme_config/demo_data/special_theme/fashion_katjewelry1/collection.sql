-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(11, 'Sang trọng, quyến rũ', 0, 1, '', '0'),
(10, 'Nhẫn quý phái', 0, 1, '', '0'),
(9, 'Dành cho phái đẹp', 0, 1, '', '0'),
(12, 'Trang sức đeo tay', 0, 1, '', '0'),
(13, 'Bộ sưu tập cho phái đẹp', 0, 1, '', '1'),
(14, 'Nhẫn', 0, 1, '', '0'),
(16, 'Dây chuyền', 0, 1, '', '0'),
(17, 'Bông tai', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, 'https://cdn.bestme.asia/images/katjewelry1/bst2.png', '', '', '', ''),
(10, '', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/katjewelry1/bst1_a5LVPZM.png', '', '', '', ''),
(12, '', '', '', '', ''),
(13, '', '', '', '', ''),
(14, '', '', '', '', ''),
(16, '', '', '', '', ''),
(17, '', '', '', '', '');