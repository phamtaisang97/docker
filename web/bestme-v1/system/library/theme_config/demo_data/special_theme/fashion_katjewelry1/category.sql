-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(45, '', 0, 0, 0, 2, 1, '2020-12-09 09:44:16', '2020-12-09 09:44:19'),
(44, '', 36, 0, 0, 0, 1, '2020-12-09 09:43:47', '2020-12-09 09:43:47'),
(43, '', 36, 0, 0, 0, 1, '2020-12-09 09:43:47', '2020-12-09 09:43:47'),
(42, '', 36, 0, 0, 0, 1, '2020-12-09 09:43:47', '2020-12-09 09:43:47'),
(41, '', 36, 0, 0, 0, 1, '2020-12-09 09:43:47', '2020-12-09 09:43:47'),
(36, '', 0, 0, 0, 1, 1, '2020-12-09 09:43:47', '2020-12-09 09:43:53'),
(37, '', 36, 0, 0, 0, 1, '2020-12-09 09:43:47', '2020-12-09 09:43:47'),
(38, '', 36, 0, 0, 0, 1, '2020-12-09 09:43:47', '2020-12-09 09:43:47'),
(39, '', 36, 0, 0, 0, 1, '2020-12-09 09:43:47', '2020-12-09 09:43:47'),
(40, '', 36, 0, 0, 0, 1, '2020-12-09 09:43:47', '2020-12-09 09:43:47'),
(46, '', 45, 0, 0, 0, 1, '2020-12-09 09:44:16', '2020-12-09 09:44:16'),
(47, '', 45, 0, 0, 0, 1, '2020-12-09 09:44:16', '2020-12-09 09:44:16'),
(48, '', 45, 0, 0, 0, 1, '2020-12-09 09:44:16', '2020-12-09 09:44:16'),
(49, '', 0, 0, 0, 3, 1, '2020-12-09 09:45:26', '2020-12-09 09:45:48'),
(50, '', 49, 0, 0, 0, 1, '2020-12-09 09:45:26', '2020-12-09 09:45:44'),
(51, '', 49, 0, 0, 0, 1, '2020-12-09 09:45:44', '2020-12-09 09:45:44'),
(52, '', 49, 0, 0, 0, 1, '2020-12-09 09:45:44', '2020-12-09 09:45:44'),
(53, '', 49, 0, 0, 0, 1, '2020-12-09 09:45:44', '2020-12-09 09:45:44'),
(54, '', 0, 0, 0, 0, 1, '2020-12-09 09:46:20', '2020-12-09 09:46:20'),
(55, '', 54, 0, 0, 0, 1, '2020-12-09 09:46:20', '2020-12-09 09:46:20'),
(56, '', 54, 0, 0, 0, 1, '2020-12-09 09:46:20', '2020-12-09 09:46:20'),
(57, '', 54, 0, 0, 0, 1, '2020-12-09 09:46:20', '2020-12-09 09:46:20'),
(58, '', 54, 0, 0, 0, 1, '2020-12-09 09:46:20', '2020-12-09 09:46:20'),
(62, '', 0, 0, 0, 0, 1, '2020-12-10 17:08:40', '2020-12-10 17:08:40'),
(59, '', 0, 0, 0, 0, 1, '2020-12-10 17:07:58', '2020-12-10 17:07:58'),
(61, '', 0, 0, 0, 0, 1, '2020-12-10 17:08:23', '2020-12-10 17:08:23'),
(60, '', 0, 0, 0, 0, 1, '2020-12-10 17:08:07', '2020-12-10 17:08:07'),
(63, '', 0, 0, 0, 0, 1, '2020-12-10 17:11:28', '2020-12-10 17:11:28'),
(64, NULL, 0, 0, 0, 0, 1, '2020-12-10 17:13:27', '2020-12-10 17:13:27'),
(65, NULL, 0, 0, 0, 0, 1, '2020-12-10 17:13:28', '2020-12-10 17:13:28'),
(66, NULL, 0, 0, 0, 0, 1, '2020-12-10 17:13:28', '2020-12-10 17:13:28');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(46, 1, 'Dây chuyền cặp đôi bạc', 'Dây chuyền cặp đôi bạc', '', '', ''),
(46, 2, 'Dây chuyền cặp đôi bạc', 'Dây chuyền cặp đôi bạc', '', '', ''),
(45, 1, 'Trang sức đôi', 'Trang sức đôi', '', '', ''),
(45, 2, 'Trang sức đôi', 'Trang sức đôi', '', '', ''),
(44, 2, 'Nhẫn bạc nữ', 'Nhẫn bạc nữ', '', '', ''),
(44, 1, 'Nhẫn bạc nữ', 'Nhẫn bạc nữ', '', '', ''),
(43, 1, 'Lắc chân bạc nữ', 'Lắc chân bạc nữ', '', '', ''),
(43, 2, 'Lắc chân bạc nữ', 'Lắc chân bạc nữ', '', '', ''),
(42, 2, 'Lắc tay bạc nữ', 'Lắc tay bạc nữ', '', '', ''),
(42, 1, 'Lắc tay bạc nữ', 'Lắc tay bạc nữ', '', '', ''),
(41, 1, 'Dây chuyền mặt chữ', 'Dây chuyền mặt chữ', '', '', ''),
(39, 2, 'Dây chuyền bạc nữ', 'Dây chuyền bạc nữ', '', '', ''),
(39, 1, 'Dây chuyền bạc nữ', 'Dây chuyền bạc nữ', '', '', ''),
(40, 2, 'Dây chuyền cung hoàng đạo', 'Dây chuyền cung hoàng đạo', '', '', ''),
(40, 1, 'Dây chuyền cung hoàng đạo', 'Dây chuyền cung hoàng đạo', '', '', ''),
(41, 2, 'Dây chuyền mặt chữ', 'Dây chuyền mặt chữ', '', '', ''),
(36, 2, 'Trang sức nữ', 'Trang sức nữ', '', '', ''),
(36, 1, 'Trang sức nữ', 'Trang sức nữ', '', '', ''),
(37, 2, 'Bộ trang sức', 'Bộ trang sức', '', '', ''),
(37, 1, 'Bộ trang sức', 'Bộ trang sức', '', '', ''),
(38, 2, 'Bông tai bạc nữ', 'Bông tai bạc nữ', '', '', ''),
(38, 1, 'Bông tai bạc nữ', 'Bông tai bạc nữ', '', '', ''),
(47, 2, 'Nhẫn đôi bạc nhẫn cặp bạc', 'Nhẫn đôi bạc nhẫn cặp bạc', '', '', ''),
(47, 1, 'Nhẫn đôi bạc nhẫn cặp bạc', 'Nhẫn đôi bạc nhẫn cặp bạc', '', '', ''),
(48, 2, 'Vòng tay đôi bạc vòng tay cặp bạc', 'Vòng tay đôi bạc vòng tay cặp bạc', '', '', ''),
(48, 1, 'Vòng tay đôi bạc vòng tay cặp bạc', 'Vòng tay đôi bạc vòng tay cặp bạc', '', '', ''),
(49, 2, 'Trang sức nam', 'Trang sức nam', '', '', ''),
(49, 1, 'Trang sức nam', 'Trang sức nam', '', '', ''),
(50, 2, 'Dây chuyền bạc nam', 'Dây chuyền bạc nam', '', '', ''),
(50, 1, 'Dây chuyền bạc nam', 'Dây chuyền bạc nam', '', '', ''),
(51, 2, 'Lắc tay bạc nam', 'Lắc tay bạc nam', '', '', ''),
(51, 1, 'Lắc tay bạc nam', 'Lắc tay bạc nam', '', '', ''),
(52, 2, 'Khuyên tai bạc nam', 'Khuyên tai bạc nam', '', '', ''),
(52, 1, 'Khuyên tai bạc nam', 'Khuyên tai bạc nam', '', '', ''),
(53, 2, 'Nhẫn bạc nam', 'Nhẫn bạc nam', '', '', ''),
(53, 1, 'Nhẫn bạc nam', 'Nhẫn bạc nam', '', '', ''),
(54, 2, 'Trang sức cho bé', 'Trang sức cho bé', '', '', ''),
(54, 1, 'Trang sức cho bé', 'Trang sức cho bé', '', '', ''),
(55, 2, 'Bông tai bạc cho bé', 'Bông tai bạc cho bé', '', '', ''),
(55, 1, 'Bông tai bạc cho bé', 'Bông tai bạc cho bé', '', '', ''),
(56, 2, 'Dây chuyền bạc cho bé gái', 'Dây chuyền bạc cho bé gái', '', '', ''),
(56, 1, 'Dây chuyền bạc cho bé gái', 'Dây chuyền bạc cho bé gái', '', '', ''),
(57, 2, 'Dây chuyền bạc cho bé trai', 'Dây chuyền bạc cho bé trai', '', '', ''),
(57, 1, 'Dây chuyền bạc cho bé trai', 'Dây chuyền bạc cho bé trai', '', '', ''),
(58, 2, 'Lắc bạc cho bé', 'Lắc bạc cho bé', '', '', ''),
(58, 1, 'Lắc bạc cho bé', 'Lắc bạc cho bé', '', '', ''),
(61, 1, 'Trang sức phong thủy', 'Trang sức phong thủy', '', '', ''),
(62, 2, 'Trang sức kim cương', 'Trang sức kim cương', '', '', ''),
(59, 2, 'Siêu phẩm cho nàng', 'Siêu phẩm cho nàng', '', '', ''),
(59, 1, 'Siêu phẩm cho nàng', 'Siêu phẩm cho nàng', '', '', ''),
(60, 2, 'Nhẫn đôi', 'Nhẫn đôi', '', '', ''),
(60, 1, 'Nhẫn đôi', 'Nhẫn đôi', '', '', ''),
(61, 2, 'Trang sức phong thủy', 'Trang sức phong thủy', '', '', ''),
(62, 1, 'Trang sức kim cương', 'Trang sức kim cương', '', '', ''),
(63, 2, 'Trang sức ngọc trai', 'Trang sức ngọc trai', '', '', ''),
(63, 1, 'Trang sức ngọc trai', 'Trang sức ngọc trai', '', '', ''),
(64, 2, 'Nhẫn', 'Nhẫn', '', '', ''),
(64, 1, 'Nhẫn', 'Nhẫn', '', '', ''),
(65, 2, 'Dây chuyền', 'Dây chuyền', '', '', ''),
(65, 1, 'Dây chuyền', 'Dây chuyền', '', '', ''),
(66, 2, 'Bông tai', 'Bông tai', '', '', ''),
(66, 1, 'Bông tai', 'Bông tai', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(48, 45, 0),
(47, 47, 1),
(47, 45, 0),
(46, 46, 1),
(46, 45, 0),
(45, 45, 0),
(44, 44, 1),
(44, 36, 0),
(43, 43, 1),
(38, 36, 0),
(37, 37, 1),
(38, 38, 1),
(36, 36, 0),
(43, 36, 0),
(42, 42, 1),
(37, 36, 0),
(39, 36, 0),
(42, 36, 0),
(41, 41, 1),
(41, 36, 0),
(40, 40, 1),
(40, 36, 0),
(39, 39, 1),
(48, 48, 1),
(49, 49, 0),
(50, 49, 0),
(50, 50, 1),
(51, 49, 0),
(51, 51, 1),
(52, 49, 0),
(52, 52, 1),
(53, 49, 0),
(53, 53, 1),
(54, 54, 0),
(55, 54, 0),
(55, 55, 1),
(56, 54, 0),
(56, 56, 1),
(57, 54, 0),
(57, 57, 1),
(58, 54, 0),
(58, 58, 1),
(63, 63, 0),
(64, 64, 0),
(59, 59, 0),
(62, 62, 0),
(61, 61, 0),
(60, 60, 0),
(65, 65, 0),
(66, 66, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(36, 0),
(37, 0),
(38, 0),
(39, 0),
(40, 0),
(41, 0),
(42, 0),
(43, 0),
(44, 0),
(45, 0),
(46, 0),
(47, 0),
(48, 0),
(49, 0),
(50, 0),
(51, 0),
(52, 0),
(53, 0),
(54, 0),
(55, 0),
(56, 0),
(57, 0),
(58, 0),
(59, 0),
(60, 0),
(61, 0),
(62, 0),
(63, 0),
(64, 0),
(65, 0),
(66, 0);