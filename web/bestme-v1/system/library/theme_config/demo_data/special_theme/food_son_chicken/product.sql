-- oc_product PRIMARY KEY (`product_id`)
-- delete first
INSERT IGNORE INTO `oc_product` (`product_id`, `model`, `sku`, `common_barcode`, `common_sku`, `common_compare_price`, `common_price`, `common_cost_price`, `barcode`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `sale_on_out_of_stock`, `stock_status_id`, `image`, `image_alt`, `multi_versions`, `manufacturer_id`, `shipping`, `price`, `price_currency_id`, `compare_price`, `c_price_currency_id`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `channel`, `viewed`, `demo`, `deleted`, `default_store_id`, `user_create_id`, `date_added`, `date_modified`) VALUES
(104, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/x2/chan-ga-chien-mam.jpg', '', 1, 0, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2020-07-13 10:52:16', '2020-07-13 10:52:16'),
(111, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/202028f0796a-5ed8-43be-9b9b-74a5080c9bd7.png', '', 1, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2020-07-13 11:09:52', '2020-07-13 11:10:47'),
(112, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020a62011de-1120-4b71-a4ab-0162ebfbf0e8.png', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2020-07-13 11:09:52', '2020-07-13 11:10:47'),
(113, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020193dca04-856d-41e1-93fc-37072b14c835.png', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2020-07-13 11:09:52', '2020-07-13 11:10:47'),
(114, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020c50f57e6-538c-4415-8fbc-9338c6a1b643.png', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2020-07-13 11:09:52', '2020-07-13 11:10:47'),
(115, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020e52204b7-2229-48c2-9cfe-1dc0f5756af4.png', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2020-07-13 11:09:52', '2020-07-13 11:10:47'),
(116, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/202028f0796a-5ed8-43be-9b9b-74a5080c9bd7.png', '', 1, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2020-07-13 11:11:02', '2020-07-24 19:13:25'),
(117, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020a62011de-1120-4b71-a4ab-0162ebfbf0e8.png', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2020-07-13 11:11:02', '2020-07-13 11:11:02');
-- restore soft-deleted products if demo
UPDATE `oc_product` SET `deleted` = NULL
WHERE `demo` = 1
  AND `product_id` IN (
   104,
   111,
   112,
   113,
   114,
   115,
   116,
   117
  );

-- oc_product_collection PRIMARY KEY (`product_collection_id`)
INSERT IGNORE INTO `oc_product_collection` (`product_collection_id`, `product_id`, `collection_id`, `sort_order`) VALUES
(137, 114, 8, 0),
(136, 117, 8, 0),
(135, 112, 8, 0),
(175, 116, 8, 0),
(139, 111, 8, 0),
(138, 104, 8, 0),
(143, 112, 9, 0),
(142, 115, 8, 0),
(141, 113, 8, 0),
(146, 104, 9, 0),
(145, 114, 9, 0),
(144, 117, 9, 0),
(147, 112, 10, 0),
(148, 117, 10, 0),
(149, 114, 10, 0),
(150, 104, 10, 0),
(151, 112, 11, 0),
(152, 117, 11, 0),
(153, 114, 11, 0),
(154, 104, 11, 0),
(155, 112, 12, 0),
(156, 117, 12, 0),
(157, 114, 12, 0),
(158, 104, 12, 0),
(159, 112, 14, 0),
(160, 117, 14, 0),
(161, 114, 14, 0),
(162, 104, 14, 0),
(163, 112, 15, 0),
(164, 117, 15, 0),
(165, 114, 15, 0),
(166, 104, 15, 0),
(167, 112, 16, 0),
(168, 117, 16, 0),
(169, 114, 16, 0),
(170, 104, 16, 0),
(171, 117, 17, 0),
(172, 114, 17, 0),
(173, 104, 17, 0),
(174, 111, 17, 0);

-- oc_product_description PRIMARY KEY (`product_id`,`language_id`)
INSERT IGNORE INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `sub_description`, `seo_title`, `seo_description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(104, 2, 'Chân gà chiên mắm siêu ngon', '&lt;p&gt;24 Ngõ 33 Phan Văn Trường, Quận Cầu Giấy, Hà Nội&lt;/p&gt;\r\n\r\n&lt;p&gt;10:00 - 22:00&lt;/p&gt;\r\n\r\n&lt;p&gt;10,000 - 130,000&lt;/p&gt;', '&lt;p&gt;24 Ngõ 33 Phan Văn Trường, Quận Cầu Giấy, Hà Nội&lt;/p&gt;\r\n\r\n&lt;p&gt;10:00 - 22:00&lt;/p&gt;\r\n\r\n&lt;p&gt;10,000 - 130,000&lt;/p&gt;', 'Chân gà chiên mắm siêu ngon', 'Chân gà chiên mắm siêu ngon', '', '', '', ''),
(111, 2, 'Chân gà chiên mắm siêu siêu ngon', '  - 24 Ngõ 33 Phan Văn Trường, Quận Cầu Giấy, Hà Nội\n- Giảm 10% đặt món trước khi đến cửa hàng', '', '', '', '', '', '', ''),
(112, 2, 'Cánh gà chiên mắm', '  - 24 Ngõ 33 Phan Văn Trường, Quận Cầu Giấy, Hà Nội\n- Giảm 10% đặt món trước khi đến cửa hàng', '', '', '', '', '', '', ''),
(113, 2, 'Chân gà rang muối', '  - 24 Ngõ 33 Phan Văn Trường, Quận Cầu Giấy, Hà Nội\n- Giảm 10% đặt món trước khi đến cửa hàng', '', '', '', '', '', '', ''),
(114, 2, 'Cánh gà rang muối', '  - 24 Ngõ 33 Phan Văn Trường, Quận Cầu Giấy, Hà Nội\n- Giảm 10% đặt món trước khi đến cửa hàng', '', '', '', '', '', '', ''),
(115, 2, 'Đùi gà chiên mắm', '  - 24 Ngõ 33 Phan Văn Trường, Quận Cầu Giấy, Hà Nội\n- Giảm 10% đặt món trước khi đến cửa hàng', '', '', '', '', '', '', ''),
(116, 2, 'Chân gà chiên mắm Tươi ngon tuyệt hảo', '&lt;p&gt;- Nguyên liệu:&lt;/p&gt;\r\n\r\n&lt;p&gt;- Đặc biệt: &lt;/p&gt;\r\n\r\n&lt;p&gt;- Độ tươi ngon&lt;/p&gt;\r\n\r\n&lt;p&gt;- Ảnh sp:&lt;/p&gt;\r\n\r\n&lt;p&gt;- Hướng dẫn mua hàng - đặt hàng&lt;/p&gt;\r\n\r\n&lt;p&gt;- Ưu đãi, chương trình khuyến mại&lt;/p&gt;\r\n\r\n&lt;p&gt;- Giới thiệu &lt;a href=&quot;https://son-chicken.bestme.asia/san-pham&quot;&gt;sản phẩm&lt;/a&gt; khác&lt;/p&gt;', '&lt;p&gt;- Hướng dẫn mua hàng - đặt hàng&lt;/p&gt;\r\n\r\n&lt;p&gt;- Ưu đãi, chương trình khuyến mại&lt;/p&gt;', 'Chân gà chiên mắm Tươi ngon tuyệt hảo', 'Chân gà chiên mắm tươi ngon giòn rụm', '', '', '', ''),
(117, 2, 'Cánh gà chiên mắm - 0982263268', '  - 24 Ngõ 33 Phan Văn Trường, Quận Cầu Giấy, Hà Nội\n- Giảm 10% đặt món trước khi đến cửa hàng', '', '', '', '', '', '', '');

-- oc_product_image PRIMARY KEY (`product_image_id`)
INSERT IGNORE INTO `oc_product_image` (`product_image_id`, `product_id`, `image`, `image_alt`, `sort_order`) VALUES
(172, 111, 'https://2.pik.vn/20208ab957a9-ae15-404f-aefa-4bf579c2a260.png', '', 4),
(171, 111, 'https://2.pik.vn/2020e52204b7-2229-48c2-9cfe-1dc0f5756af4.png', '', 3),
(170, 111, 'https://2.pik.vn/2020c50f57e6-538c-4415-8fbc-9338c6a1b643.png', '', 2),
(169, 111, 'https://2.pik.vn/2020193dca04-856d-41e1-93fc-37072b14c835.png', '', 1),
(168, 111, 'https://2.pik.vn/2020a62011de-1120-4b71-a4ab-0162ebfbf0e8.png', '', 0),
(177, 112, 'https://2.pik.vn/202061549398-7488-4cd6-8e34-5f19437d2853.png', '', 4),
(176, 112, 'https://2.pik.vn/20208ab957a9-ae15-404f-aefa-4bf579c2a260.png', '', 3),
(175, 112, 'https://2.pik.vn/2020e52204b7-2229-48c2-9cfe-1dc0f5756af4.png', '', 2),
(174, 112, 'https://2.pik.vn/2020c50f57e6-538c-4415-8fbc-9338c6a1b643.png', '', 1),
(173, 112, 'https://2.pik.vn/2020193dca04-856d-41e1-93fc-37072b14c835.png', '', 0),
(182, 113, 'https://2.pik.vn/202061549398-7488-4cd6-8e34-5f19437d2853.png', '', 4),
(181, 113, 'https://2.pik.vn/20208ab957a9-ae15-404f-aefa-4bf579c2a260.png', '', 3),
(180, 113, 'https://2.pik.vn/2020e52204b7-2229-48c2-9cfe-1dc0f5756af4.png', '', 2),
(179, 113, 'https://2.pik.vn/2020c50f57e6-538c-4415-8fbc-9338c6a1b643.png', '', 1),
(178, 113, 'https://2.pik.vn/2020a62011de-1120-4b71-a4ab-0162ebfbf0e8.png', '', 0),
(187, 114, 'https://2.pik.vn/202061549398-7488-4cd6-8e34-5f19437d2853.png', '', 4),
(186, 114, 'https://2.pik.vn/20208ab957a9-ae15-404f-aefa-4bf579c2a260.png', '', 3),
(185, 114, 'https://2.pik.vn/2020e52204b7-2229-48c2-9cfe-1dc0f5756af4.png', '', 2),
(184, 114, 'https://2.pik.vn/2020193dca04-856d-41e1-93fc-37072b14c835.png', '', 1),
(183, 114, 'https://2.pik.vn/2020a62011de-1120-4b71-a4ab-0162ebfbf0e8.png', '', 0),
(192, 115, 'https://2.pik.vn/202061549398-7488-4cd6-8e34-5f19437d2853.png', '', 4),
(191, 115, 'https://2.pik.vn/20208ab957a9-ae15-404f-aefa-4bf579c2a260.png', '', 3),
(190, 115, 'https://2.pik.vn/2020c50f57e6-538c-4415-8fbc-9338c6a1b643.png', '', 2),
(189, 115, 'https://2.pik.vn/2020193dca04-856d-41e1-93fc-37072b14c835.png', '', 1),
(188, 115, 'https://2.pik.vn/2020a62011de-1120-4b71-a4ab-0162ebfbf0e8.png', '', 0),
(207, 116, 'https://2.pik.vn/20208ab957a9-ae15-404f-aefa-4bf579c2a260.png', '', 4),
(206, 116, 'https://2.pik.vn/2020e52204b7-2229-48c2-9cfe-1dc0f5756af4.png', '', 3),
(205, 116, 'https://2.pik.vn/2020c50f57e6-538c-4415-8fbc-9338c6a1b643.png', '', 2),
(204, 116, 'https://2.pik.vn/2020193dca04-856d-41e1-93fc-37072b14c835.png', '', 1),
(203, 116, 'https://2.pik.vn/2020a62011de-1120-4b71-a4ab-0162ebfbf0e8.png', '', 0),
(198, 117, 'https://2.pik.vn/2020193dca04-856d-41e1-93fc-37072b14c835.png', '', 0),
(199, 117, 'https://2.pik.vn/2020c50f57e6-538c-4415-8fbc-9338c6a1b643.png', '', 1),
(200, 117, 'https://2.pik.vn/2020e52204b7-2229-48c2-9cfe-1dc0f5756af4.png', '', 2),
(201, 117, 'https://2.pik.vn/20208ab957a9-ae15-404f-aefa-4bf579c2a260.png', '', 3),
(202, 117, 'https://2.pik.vn/202061549398-7488-4cd6-8e34-5f19437d2853.png', '', 4);

-- oc_product_to_category PRIMARY KEY (`product_id`,`category_id`)
INSERT IGNORE INTO `oc_product_to_category` (`product_id`, `category_id`) VALUES
(104, 21),
(104, 22),
(111, 21),
(112, 22),
(113, 21),
(114, 22),
(115, 23),
(116, 21),
(117, 22);

-- oc_product_to_store PRIMARY KEY (`product_id`,`store_id`)
INSERT IGNORE INTO `oc_product_to_store` (`product_id`, `store_id`, `product_version_id`, `quantity`, `cost_price`) VALUES
(104, 0, 1, 98, '7000.0000'),
(104, 0, 2, 100, '0.0000'),
(104, 0, 3, 100, '0.0000'),
(113, 0, 0, 30, '0.0000'),
(112, 0, 0, 30, '0.0000'),
(116, 0, 12, 30, '0.0000'),
(116, 0, 13, 30, '0.0000'),
(111, 0, 11, 30, '0.0000'),
(115, 0, 0, 30, '0.0000'),
(114, 0, 0, 30, '0.0000'),
(111, 0, 10, 30, '0.0000'),
(117, 0, 0, 30, '0.0000');