-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(21, NULL, 0, 0, 0, 0, 1, '2020-07-13 10:52:16', '2020-07-13 10:52:16'),
(22, NULL, 0, 0, 0, 0, 1, '2020-07-13 10:52:16', '2020-07-13 10:52:16'),
(23, NULL, 0, 0, 0, 0, 1, '2020-07-13 11:07:31', '2020-07-13 11:07:31');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(22, 2, 'Cánh gà', 'Cánh gà', '', '', ''),
(22, 1, 'Cánh gà', 'Cánh gà', '', '', ''),
(23, 2, 'Đùi Gà', 'Đùi Gà', '', '', ''),
(23, 1, 'Đùi Gà', 'Đùi Gà', '', '', ''),
(21, 1, 'Chân gà', 'Chân gà', '', '', ''),
(21, 2, 'Chân gà', 'Chân gà', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(21, 21, 0),
(22, 22, 0),
(23, 23, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(21, 0),
(22, 0),
(23, 0);