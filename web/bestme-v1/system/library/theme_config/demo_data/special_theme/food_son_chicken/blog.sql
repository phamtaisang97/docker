-- oc_blog PRIMARY KEY (`blog_id`)
INSERT IGNORE INTO `oc_blog` (`blog_id`, `author`, `status`, `demo`, `date_publish`, `date_added`, `date_modified`) VALUES
(1, 1, 1, 1, '2020-07-13 11:36:29', '2020-07-13 11:36:29', '2020-07-24 19:17:35'),
(2, 1, 1, 1, '2020-07-13 11:38:30', '2020-07-13 11:38:30', '2020-07-13 11:38:30'),
(3, 1, 1, 1, '2020-07-13 11:38:58', '2020-07-13 11:38:58', '2020-07-13 11:38:58');

-- oc_blog_category PRIMARY KEY (`blog_category_id`)
INSERT IGNORE INTO `oc_blog_category` (`blog_category_id`, `status`, `date_added`, `date_modified`) VALUES
(1, 1, '2019-11-11 13:37:51', '2020-07-13 11:34:57'),
(2, 1, '2020-07-13 11:35:17', '2020-07-13 11:35:17'),
(3, 1, '2020-07-13 11:35:33', '2020-07-13 11:35:33');

-- oc_blog_category_description PRIMARY KEY (`blog_category_id`,`language_id`)
INSERT IGNORE INTO `oc_blog_category_description` (`blog_category_id`, `language_id`, `title`, `meta_title`, `meta_description`, `alias`) VALUES
(1, 1, 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'danh-muc-mac-dinh-uncategorized'),
(1, 2, 'Khuyến mãi', 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'danh-muc-mac-dinh-uncategorized'),
(2, 2, 'Giới thiệu', '', '', 'gioi-thieu'),
(3, 2, 'Chia sẻ', '', '', 'chia-se');

-- oc_blog_description PRIMARY KEY (`blog_id`,`language_id`)
INSERT IGNORE INTO `oc_blog_description` (`blog_id`, `language_id`, `title`, `content`, `short_content`, `image`, `meta_title`, `meta_description`, `alias`) VALUES
(1, 2, 'Chân gà chiên mắm mua 1 tặng 1 - Siêu khuyễn mãi tháng 7', '&lt;p&gt;- Nội dung chương trình&lt;/p&gt;\r\n\r\n&lt;p&gt;- Thời gian áp dụng&lt;/p&gt;\r\n\r\n&lt;p&gt;- Điều kiện áp dụng&lt;/p&gt;\r\n\r\n&lt;p&gt; &lt;/p&gt;', '&lt;p&gt;- Giảm 10% khi ăn tại quán&lt;/p&gt;\r\n\r\n&lt;p&gt;- Giảm 15% cho lần mua thứ 2&lt;/p&gt;', 'https://cdn.bestme.asia/images/x2/bst1-min_KKhhByZ.png', 'Chân gà chiên mắm mua 1 tặng 1 Siêu khuyễn mãi tháng 7', 'Mua 1 tặng 1 - siêu ưu đãi........', NULL, 'mua-1-tang-1', NULL, 'image', NULL),
(2, 2, 'Những mẹo vặt kinh điển', '&lt;p&gt;Những mẹo vặt kinh điển&lt;/p&gt;', '&lt;p&gt;Những mẹo vặt kinh điển&lt;/p&gt;', 'https://cdn.bestme.asia/images/x2/bst2-min_cdGR06K.png', '', '', NULL, 'nhung-meo-vat-kinh-dien', NULL, 'image', NULL),
(3, 2, 'Giới thiệu cửa hàng', '&lt;p&gt;Giới thiệu cửa hàng&lt;/p&gt;', '&lt;p&gt;Giới thiệu cửa hàng&lt;/p&gt;', 'https://cdn.bestme.asia/images/x2/bst3-min_yYkTiAB.png', '', '', NULL, 'gioi-thieu-cua-hang', NULL, 'image', NULL);
-- oc_blog_to_blog_category NO PRIMARY KEY
-- remove first
DELETE FROM `oc_blog_to_blog_category`
WHERE (`blog_id` = 1 AND `blog_category_id` = 1)
   OR (`blog_id` = 2 AND `blog_category_id` = 3)
   OR (`blog_id` = 3 AND `blog_category_id` = 2);

-- insert
INSERT IGNORE INTO `oc_blog_to_blog_category` (`blog_id`, `blog_category_id`) VALUES
(1, 1),
(2, 3),
(3, 2);