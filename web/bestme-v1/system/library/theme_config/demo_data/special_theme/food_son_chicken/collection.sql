-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(9, 'Món ngon mỗi ngày', 0, 1, '', '0'),
(10, 'Đồ ăn nhanh', 0, 1, '', '0'),
(11, 'Sản phẩm cho sức khỏe', 0, 1, '', '0'),
(8, 'All SP', 0, 1, '', '0'),
(12, 'Samosa', 0, 1, '', '0'),
(13, 'TH1', 0, 1, '', '1'),
(14, 'Khoai tây chiên', 0, 1, '', '0'),
(15, 'Đồ ăn  nhanh', 0, 1, '', '0'),
(16, 'Chim cút quay', 0, 1, '', '0'),
(17, 'Chân gà xả ớt', 0, 1, '', '0'),
(18, 'TH2', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(15, 'https://cdn.bestme.asia/images/x2/bstvuong3-01-min.png', '', '', '', ''),
(10, 'https://cdn.bestme.asia/images/x2/bst1-min_KKhhByZ.png', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/x2/bst3-min_yYkTiAB.png', '', '', '', ''),
(12, 'https://cdn.bestme.asia/images/x2/bst2-min_cdGR06K.png', '', '', '', ''),
(13, '', '', '', '', ''),
(14, 'https://cdn.bestme.asia/images/x2/bstvuong4-01-min.png', '', '', '', ''),
(8, '', '', '', '', ''),
(9, 'https://cdn.bestme.asia/images/x2/bst4-min.png', '', '', '', ''),
(16, 'https://cdn.bestme.asia/images/x2/bstvuong2-01-min.png', '', '', '', ''),
(17, 'https://cdn.bestme.asia/images/x2/bstvuong1-01-min.png', '', '', '', ''),
(18, '', '', '', '', '');