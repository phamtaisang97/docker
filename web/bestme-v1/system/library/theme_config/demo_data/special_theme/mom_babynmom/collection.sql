-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(12, 'Đồ chơi , quà tặng', 0, 1, '', '0'),
(11, 'Sơ sinh , trẻ nhỏ', 0, 1, '', '0'),
(9, 'Sản phẩm cho mẹ', 0, 1, '', '0'),
(10, 'Sản phẩm cho bé', 0, 1, '', '0'),
(29, 'Máy hút sữa', 0, 1, '', '0'),
(30, 'Đồ ăn dặm', 0, 1, '', '0'),
(31, '1', 0, 1, '', '1'),
(32, 'Xe đẩy cho bé', 0, 1, '', '0'),
(33, 'Vitamin cho mẹ bầu', 0, 1, '', '0'),
(34, 'Thực Phẩm Chức Năng', 0, 1, '', '0'),
(27, 'Mỹ phẩm cho mẹ', 0, 1, '', '0'),
(28, 'BST Nous Xuân Hè', 0, 1, '', '0'),
(22, 'Kids', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(30, 'https://cdn.bestme.asia/images/x2/17apr9eb9cd58b9ea5e04c890326b5c1f471f.png', '', '', '', ''),
(31, '', '', '', '', ''),
(32, 'https://cdn.bestme.asia/images/x2/17aprfea9aa88a2b6c3803f40c5323343a21c.png', '', '', '', ''),
(28, 'https://cdn.bestme.asia/images/x2/17apr602e8f042f463dc47ebfdf6a94ed5a6d.png', '', '', '', ''),
(29, 'https://cdn.bestme.asia/images/x2/17aprf19c9085129709ee14d013be869df69b.png', '', '', '', ''),
(11, '', '', '', '', ''),
(12, '', '', '', '', ''),
(9, '', '', '', '', ''),
(10, '', '', '', '', ''),
(33, 'https://cdn.bestme.asia/images/x2/27jun1672eea6e4e7bd3854faf2df69940737.png', '', '', '', ''),
(34, 'https://cdn.bestme.asia/images/x2/27jun1672eea6e4e7bd3854faf2df69940737.png', '', '', '', ''),
(22, '', '', '', '', ''),
(27, 'https://cdn.bestme.asia/images/x2/17apr10fb15c77258a991b0028080a64fb42d.png', '', '', '', '');