-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(11, 'Sang trọng, quyến rũ', 0, 1, '', '0'),
(10, 'Dành cho phái đẹp', 0, 1, '', '0'),
(9, 'Nhẫn quý phái', 0, 1, '', '0'),
(13, 'Trang sức đeo tay', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, 'https://cdn.bestme.asia/images/katjewelry/bst2-tinhphi_hjc63jq.jpg', '', '', '', ''),
(10, 'https://cdn.bestme.asia/images/katjewelry/bst3-tinhphi_iJVlZyc.jpg', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/katjewelry/bst1-tinhphi.jpg', '', '', '', ''),
(13, 'https://cdn.bestme.asia/images/katjewelry/bst4-tinhphi.jpg', '', '', '', '');