--
-- create new demo data
--
CREATE TABLE IF NOT EXISTS `oc_demo_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_id` int(11) NOT NULL,
  `table_name` varchar(255) DEFAULT '',
  `theme_group` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- create new demo data
--

INSERT IGNORE INTO `oc_demo_data` (`data_id`, `table_name`, `theme_group`) VALUES
-- product table
(104, 'product', 'car'),
(105, 'product', 'car'),
(106, 'product', 'car'),
(107, 'product', 'car'),
(108, 'product', 'car'),
(109, 'product', 'car'),
(110, 'product', 'car'),
(111, 'product', 'car'),
(112, 'product', 'car'),
(113, 'product', 'car'),
(114, 'product', 'car'),
(115, 'product', 'car'),
(116, 'product', 'car'),
(117, 'product', 'car'),
(118, 'product', 'car'),
(119, 'product', 'car'),
(120, 'product', 'car'),
(121, 'product', 'car'),
(122, 'product', 'car'),
-- category table
-- collection table
(9, 'collection', 'car'),
(10, 'collection', 'car'),
(8, 'collection', 'car'),
(11, 'collection', 'car');
-- seo_url table: auto deleted when deleting product, category, collection

-- oc_category PRIMARY KEY (`category_id`)
-- nothing

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
-- nothing

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
-- nothing

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
-- nothing

-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(9, 'Siêu phẩm 2020', 0, 1, '', '0'),
(10, 'TOYOTA INNOVA', 0, 1, '', '0'),
(8, 'BST tổng hợp', 0, 1, '', '0'),
(11, 'BST tổng hợp1', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(10, 'https://res.cloudinary.com/novaonx2/image/upload/v1587472611/19449/banner-2.png', '', '', '', ''),
(11, '', '', '', '', ''),
(8, '', '', '', '', ''),
(9, 'https://res.cloudinary.com/novaonx2/image/upload/v1587472607/19449/banner-1.png', '', '', '', '');

-- oc_product PRIMARY KEY (`product_id`)
INSERT IGNORE INTO `oc_product` (`product_id`, `model`, `sku`, `common_barcode`, `common_sku`, `common_compare_price`, `common_price`, `barcode`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `sale_on_out_of_stock`, `stock_status_id`, `image`, `image_alt`, `multi_versions`, `manufacturer_id`, `shipping`, `price`, `price_currency_id`, `compare_price`, `c_price_currency_id`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `channel`, `viewed`, `demo`, `deleted`, `default_store_id`, `date_added`, `date_modified`) VALUES
(104, '', '', '', '', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/202033ff0c99-3125-45d8-bcfb-7f39fa486f88.jpg', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50000.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-04-21 19:50:20', '2020-04-21 19:50:20'),
(105, '', '', '', '', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020faf13eeb-0345-4b96-8041-fda5eee7848d.jpg', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50000.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-04-21 19:50:20', '2020-04-21 19:50:20'),
(106, '', '', '', '', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/202034f3fabb-e949-4dd3-9351-e37069ed4aee.jpg', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50000.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-04-21 19:50:20', '2020-04-21 19:50:20'),
(107, '', '', '', '', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020ee0e1b49-3c91-4d4b-aa35-4e670937b856.jpg', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50000.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-04-21 19:50:20', '2020-04-21 19:50:20'),
(108, '', '', '', '', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/202063d46acb-a0f8-45b8-99e5-21bcc444de3c.jpg', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50000.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-04-21 19:50:20', '2020-04-21 19:50:20'),
(109, '', '', '', '', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020dc3dfd5f-12e0-48e8-a387-4d2f0adad93f.jpg', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50000.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-04-21 19:50:20', '2020-04-21 19:50:20'),
(110, '', '', '', '', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020349246ab-974b-4d7b-8c7c-a28d742a454f.jpg', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50000.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-04-21 19:50:20', '2020-04-21 19:50:20'),
(111, '', '', '', '', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020c557a26e-5256-457b-9bc5-dc0413d07fd7.jpg', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50000.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-04-21 19:50:20', '2020-04-21 19:50:20'),
(112, '', '', '', '', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020e2fdfac9-e8ff-4a7c-ae52-cb45331b348f.jpg', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50000.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-04-21 19:50:20', '2020-04-21 19:50:20'),
(113, '', '', '', '', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/202006d4477b-a392-40da-931a-ea8aa7c2d399.jpg', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50000.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-04-21 19:50:20', '2020-04-21 19:50:20'),
(114, '', '', '', '', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/20207a8c022c-e01a-4309-985c-7ac63d5d7439.jpg', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50000.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-04-21 19:50:20', '2020-04-21 19:50:20'),
(115, '', '', '', '', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020cdb40eea-1e9a-4fa8-8cae-d6fcaa872d2c.jpg', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50000.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-04-21 19:50:20', '2020-04-21 19:50:20'),
(116, '', '', '', '', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020d15ec6bf-b403-4eca-9171-bb5eedbeed4b.jpg', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50000.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-04-21 19:50:20', '2020-04-21 19:50:20'),
(117, '', '', '', '', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/20205ae283e7-8148-4f54-babc-70bc3de6b7f4.jpg', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50000.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-04-21 19:50:20', '2020-04-21 19:50:20'),
(118, '', '', '', '', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020775d4cc6-46bb-4505-a075-1331278a924e.jpg', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50000.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-04-21 19:50:20', '2020-04-21 19:50:20'),
(119, '', '', '', '', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/202069469af2-5e09-4d61-8009-deed3e43c0c4.jpg', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50000.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-04-21 19:50:20', '2020-04-21 19:50:20'),
(120, '', '', '', '', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/202024fb663e-ebf3-47d7-9163-54722ac41067.jpg', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50000.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-04-21 19:50:20', '2020-04-21 19:50:20'),
(121, '', '', '', '', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/202013ad3e8e-0d7a-4239-a562-1ad874796216.jpg', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50000.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-04-21 19:50:20', '2020-04-21 19:50:20'),
(122, '', '', '', '', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020c4c5561d-1440-4db2-9dff-90533c8ee1f5.jpg', '', 0, 26, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '50000.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-04-21 19:50:20', '2020-04-21 19:50:20');

-- restore soft-deleted products if demo
UPDATE `oc_product` SET `deleted` = NULL
WHERE `demo` = 1
  AND `product_id` IN (
      104,
      105,
      106,
      107,
      108,
      109,
      110,
      111,
      112,
      113,
      114,
      115,
      116,
      117,
      118,
      119,
      120,
      121,
      122
  );

-- oc_product_collection PRIMARY KEY (`product_collection_id`)
INSERT IGNORE INTO `oc_product_collection` (`product_collection_id`, `product_id`, `collection_id`, `sort_order`) VALUES
(137, 114, 8, 0),
(136, 113, 8, 0),
(135, 104, 8, 0),
(140, 117, 8, 0),
(139, 116, 8, 0),
(138, 115, 8, 0),
(143, 121, 8, 0),
(142, 120, 8, 0),
(141, 119, 8, 0),
(146, 109, 8, 0),
(145, 108, 8, 0),
(144, 122, 8, 0),
(147, 110, 8, 0),
(148, 111, 8, 0),
(149, 105, 8, 0),
(150, 106, 8, 0),
(151, 104, 9, 0),
(152, 113, 9, 0),
(153, 114, 9, 0),
(154, 115, 9, 0),
(155, 116, 9, 0),
(156, 117, 9, 0),
(157, 118, 9, 0),
(158, 119, 9, 0),
(159, 104, 10, 0),
(160, 113, 10, 0),
(161, 114, 10, 0),
(162, 115, 10, 0),
(163, 116, 10, 0),
(164, 117, 10, 0),
(165, 118, 10, 0),
(166, 119, 10, 0),
(167, 120, 10, 0);

-- oc_product_description PRIMARY KEY (`product_id`,`language_id`)
INSERT IGNORE INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `sub_description`, `seo_title`, `seo_description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(104, 2, 'TOYOTA INNOVA 2.0E1', ' - Số chỗ ngồi : 8 chỗ \n- Kiểu dáng : Đa dụng \n- Nhiên liệu : Xăng \n- Xuất xứ : Xe trong nước \n\nThông tin khác: \n- Số tự động 5 cấp\n- Động cơ xăng dung tích 1.998 cm3', '', '', '', '', '', '', ''),
(105, 2, 'TOYOTA INNOVA 2.0E2', ' - Số chỗ ngồi : 8 chỗ \n- Kiểu dáng : Đa dụng \n- Nhiên liệu : Xăng \n- Xuất xứ : Xe trong nước \n\nThông tin khác: \n- Số tự động 5 cấp\n- Động cơ xăng dung tích 1.998 cm3', '', '', '', '', '', '', ''),
(106, 2, 'TOYOTA INNOVA 2.0E3', ' - Số chỗ ngồi : 8 chỗ \n- Kiểu dáng : Đa dụng \n- Nhiên liệu : Xăng \n- Xuất xứ : Xe trong nước \n\nThông tin khác: \n- Số tự động 5 cấp\n- Động cơ xăng dung tích 1.998 cm3', '', '', '', '', '', '', ''),
(107, 2, 'TOYOTA INNOVA 2.0E4', ' - Số chỗ ngồi : 8 chỗ \n- Kiểu dáng : Đa dụng \n- Nhiên liệu : Xăng \n- Xuất xứ : Xe trong nước \n\nThông tin khác: \n- Số tự động 5 cấp\n- Động cơ xăng dung tích 1.998 cm3', '', '', '', '', '', '', ''),
(108, 2, 'TOYOTA INNOVA 2.0E5', ' - Số chỗ ngồi : 8 chỗ \n- Kiểu dáng : Đa dụng \n- Nhiên liệu : Xăng \n- Xuất xứ : Xe trong nước \n\nThông tin khác: \n- Số tự động 5 cấp\n- Động cơ xăng dung tích 1.998 cm3', '', '', '', '', '', '', ''),
(109, 2, 'TOYOTA INNOVA 2.0E6', ' - Số chỗ ngồi : 8 chỗ \n- Kiểu dáng : Đa dụng \n- Nhiên liệu : Xăng \n- Xuất xứ : Xe trong nước \n\nThông tin khác: \n- Số tự động 5 cấp\n- Động cơ xăng dung tích 1.998 cm3', '', '', '', '', '', '', ''),
(110, 2, 'TOYOTA INNOVA 2.0E7', ' - Số chỗ ngồi : 8 chỗ \n- Kiểu dáng : Đa dụng \n- Nhiên liệu : Xăng \n- Xuất xứ : Xe trong nước \n\nThông tin khác: \n- Số tự động 5 cấp\n- Động cơ xăng dung tích 1.998 cm3', '', '', '', '', '', '', ''),
(111, 2, 'TOYOTA INNOVA 2.0E8', ' - Số chỗ ngồi : 8 chỗ \n- Kiểu dáng : Đa dụng \n- Nhiên liệu : Xăng \n- Xuất xứ : Xe trong nước \n\nThông tin khác: \n- Số tự động 5 cấp\n- Động cơ xăng dung tích 1.998 cm3', '', '', '', '', '', '', ''),
(112, 2, 'TOYOTA INNOVA 2.0E9', ' - Số chỗ ngồi : 8 chỗ \n- Kiểu dáng : Đa dụng \n- Nhiên liệu : Xăng \n- Xuất xứ : Xe trong nước \n\nThông tin khác: \n- Số tự động 5 cấp\n- Động cơ xăng dung tích 1.998 cm3', '', '', '', '', '', '', ''),
(113, 2, 'TOYOTA INNOVA 2.0E10', ' - Số chỗ ngồi : 8 chỗ \n- Kiểu dáng : Đa dụng \n- Nhiên liệu : Xăng \n- Xuất xứ : Xe trong nước \n\nThông tin khác: \n- Số tự động 5 cấp\n- Động cơ xăng dung tích 1.998 cm3', '', '', '', '', '', '', ''),
(114, 2, 'TOYOTA INNOVA 2.0E11', ' - Số chỗ ngồi : 8 chỗ \n- Kiểu dáng : Đa dụng \n- Nhiên liệu : Xăng \n- Xuất xứ : Xe trong nước \n\nThông tin khác: \n- Số tự động 5 cấp\n- Động cơ xăng dung tích 1.998 cm3', '', '', '', '', '', '', ''),
(115, 2, 'TOYOTA INNOVA 2.0E12', ' - Số chỗ ngồi : 8 chỗ \n- Kiểu dáng : Đa dụng \n- Nhiên liệu : Xăng \n- Xuất xứ : Xe trong nước \n\nThông tin khác: \n- Số tự động 5 cấp\n- Động cơ xăng dung tích 1.998 cm3', '', '', '', '', '', '', ''),
(116, 2, 'TOYOTA INNOVA 2.0E13', ' - Số chỗ ngồi : 8 chỗ \n- Kiểu dáng : Đa dụng \n- Nhiên liệu : Xăng \n- Xuất xứ : Xe trong nước \n\nThông tin khác: \n- Số tự động 5 cấp\n- Động cơ xăng dung tích 1.998 cm3', '', '', '', '', '', '', ''),
(117, 2, 'TOYOTA INNOVA 2.0E14', ' - Số chỗ ngồi : 8 chỗ \n- Kiểu dáng : Đa dụng \n- Nhiên liệu : Xăng \n- Xuất xứ : Xe trong nước \n\nThông tin khác: \n- Số tự động 5 cấp\n- Động cơ xăng dung tích 1.998 cm3', '', '', '', '', '', '', ''),
(118, 2, 'TOYOTA INNOVA 2.0E15', ' - Số chỗ ngồi : 8 chỗ \n- Kiểu dáng : Đa dụng \n- Nhiên liệu : Xăng \n- Xuất xứ : Xe trong nước \n\nThông tin khác: \n- Số tự động 5 cấp\n- Động cơ xăng dung tích 1.998 cm3', '', '', '', '', '', '', ''),
(119, 2, 'TOYOTA INNOVA 2.0E16', ' - Số chỗ ngồi : 8 chỗ \n- Kiểu dáng : Đa dụng \n- Nhiên liệu : Xăng \n- Xuất xứ : Xe trong nước \n\nThông tin khác: \n- Số tự động 5 cấp\n- Động cơ xăng dung tích 1.998 cm3', '', '', '', '', '', '', ''),
(120, 2, 'TOYOTA INNOVA 2.0E17', ' - Số chỗ ngồi : 8 chỗ \n- Kiểu dáng : Đa dụng \n- Nhiên liệu : Xăng \n- Xuất xứ : Xe trong nước \n\nThông tin khác: \n- Số tự động 5 cấp\n- Động cơ xăng dung tích 1.998 cm3', '', '', '', '', '', '', ''),
(121, 2, 'TOYOTA INNOVA 2.0E18', ' - Số chỗ ngồi : 8 chỗ \n- Kiểu dáng : Đa dụng \n- Nhiên liệu : Xăng \n- Xuất xứ : Xe trong nước \n\nThông tin khác: \n- Số tự động 5 cấp\n- Động cơ xăng dung tích 1.998 cm3', '', '', '', '', '', '', ''),
(122, 2, 'TOYOTA INNOVA 2.0E19', ' - Số chỗ ngồi : 8 chỗ \n- Kiểu dáng : Đa dụng \n- Nhiên liệu : Xăng \n- Xuất xứ : Xe trong nước \n\nThông tin khác: \n- Số tự động 5 cấp\n- Động cơ xăng dung tích 1.998 cm3', '', '', '', '', '', '', '');

-- oc_product_image PRIMARY KEY (`product_image_id`)
-- nothing

-- oc_product_to_category PRIMARY KEY (`product_id`,`category_id`)
-- nothing

-- oc_product_to_store PRIMARY KEY (`product_id`,`store_id`)
INSERT IGNORE INTO `oc_product_to_store` (`product_id`, `store_id`, `product_version_id`, `quantity`, `cost_price`) VALUES
(104, 0, 0, 10, '0.0000'),
(105, 0, 0, 10, '0.0000'),
(106, 0, 0, 10, '0.0000'),
(107, 0, 0, 10, '0.0000'),
(108, 0, 0, 10, '0.0000'),
(109, 0, 0, 10, '0.0000'),
(110, 0, 0, 10, '0.0000'),
(111, 0, 0, 10, '0.0000'),
(112, 0, 0, 10, '0.0000'),
(113, 0, 0, 10, '0.0000'),
(114, 0, 0, 10, '0.0000'),
(115, 0, 0, 10, '0.0000'),
(116, 0, 0, 10, '0.0000'),
(117, 0, 0, 10, '0.0000'),
(118, 0, 0, 10, '0.0000'),
(119, 0, 0, 10, '0.0000'),
(120, 0, 0, 10, '0.0000'),
(121, 0, 0, 10, '0.0000'),
(122, 0, 0, 10, '0.0000');

-- oc_seo_url PRIMARY KEY (`seo_url_id`)
INSERT IGNORE INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`, `table_name`) VALUES
(1000, 0, 1, 'page_id=1000', 'chinh-sach-va-bao-mat', ''),
(1001, 0, 2, 'page_id=1000', 'chinh-sach-va-bao-mat', ''),
(2093, 0, 1, 'common/home', 'home', ''),
(2094, 0, 2, 'common/home', 'trang-chu', ''),
(2095, 0, 1, 'common/shop', 'products', ''),
(2096, 0, 2, 'common/shop', 'san-pham', ''),
(2097, 0, 1, 'contact/contact', 'contact', ''),
(2098, 0, 2, 'contact/contact', 'lien-he', ''),
(2099, 0, 1, 'checkout/profile', 'profile', ''),
(2100, 0, 2, 'checkout/profile', 'tai-khoan', ''),
(2101, 0, 1, 'account/login', 'login', ''),
(2102, 0, 2, 'account/login', 'dang-nhap', ''),
(2103, 0, 1, 'account/register', 'register', ''),
(2104, 0, 2, 'account/register', 'dang-ky', ''),
(2105, 0, 1, 'account/logout', 'logout', ''),
(2106, 0, 2, 'account/logout', 'dang-xuat', ''),
(2107, 0, 1, 'checkout/setting', 'setting', ''),
(2108, 0, 2, 'checkout/setting', 'cai-dat', ''),
(2109, 0, 1, 'checkout/my_orders', 'checkout-cart', ''),
(2110, 0, 2, 'checkout/my_orders', 'gio-hang', ''),
(2111, 0, 1, 'checkout/order_preview', 'payment', ''),
(2112, 0, 2, 'checkout/order_preview', 'thanh-toan', ''),
(2113, 0, 1, 'checkout/order_preview/orderSuccess', 'payment-success', ''),
(2114, 0, 2, 'checkout/order_preview/orderSuccess', 'dat-hang-thanh-cong', ''),
(2115, 0, 1, 'blog/blog', 'blog', ''),
(2116, 0, 2, 'blog/blog', 'bai-viet', ''),
(2162, 0, 1, 'menu_id=6', 'menu-chan-trang', ''),
(2161, 0, 2, 'menu_id=6', 'menu-chan-trang', ''),
(2160, 0, 2, 'collection=11', 'bst-tong-hop1', 'collection'),
(2159, 0, 2, 'collection=10', 'toyota-innova', 'collection'),
(2158, 0, 2, 'collection=9', 'sieu-pham-2020', 'collection'),
(2157, 0, 2, 'collection=8', 'bst-tong-hop', 'collection'),
(2117, 0, 2, 'manufacturer_id=26', '', 'manufacturer'),
(2118, 0, 1, 'manufacturer_id=26', '', 'manufacturer'),
(2119, 0, 2, 'product_id=104', 'toyota-innova-20e1', 'product'),
(2120, 0, 1, 'product_id=104', 'toyota-innova-20e1', 'product'),
(2121, 0, 2, 'product_id=105', 'toyota-innova-20e2', 'product'),
(2122, 0, 1, 'product_id=105', 'toyota-innova-20e2', 'product'),
(2123, 0, 2, 'product_id=106', 'toyota-innova-20e3', 'product'),
(2124, 0, 1, 'product_id=106', 'toyota-innova-20e3', 'product'),
(2125, 0, 2, 'product_id=107', 'toyota-innova-20e4', 'product'),
(2126, 0, 1, 'product_id=107', 'toyota-innova-20e4', 'product'),
(2127, 0, 2, 'product_id=108', 'toyota-innova-20e5', 'product'),
(2128, 0, 1, 'product_id=108', 'toyota-innova-20e5', 'product'),
(2129, 0, 2, 'product_id=109', 'toyota-innova-20e6', 'product'),
(2130, 0, 1, 'product_id=109', 'toyota-innova-20e6', 'product'),
(2131, 0, 2, 'product_id=110', 'toyota-innova-20e7', 'product'),
(2132, 0, 1, 'product_id=110', 'toyota-innova-20e7', 'product'),
(2133, 0, 2, 'product_id=111', 'toyota-innova-20e8', 'product'),
(2134, 0, 1, 'product_id=111', 'toyota-innova-20e8', 'product'),
(2135, 0, 2, 'product_id=112', 'toyota-innova-20e9', 'product'),
(2136, 0, 1, 'product_id=112', 'toyota-innova-20e9', 'product'),
(2137, 0, 2, 'product_id=113', 'toyota-innova-20e10', 'product'),
(2138, 0, 1, 'product_id=113', 'toyota-innova-20e10', 'product'),
(2139, 0, 2, 'product_id=114', 'toyota-innova-20e11', 'product'),
(2140, 0, 1, 'product_id=114', 'toyota-innova-20e11', 'product'),
(2141, 0, 2, 'product_id=115', 'toyota-innova-20e12', 'product'),
(2142, 0, 1, 'product_id=115', 'toyota-innova-20e12', 'product'),
(2143, 0, 2, 'product_id=116', 'toyota-innova-20e13', 'product'),
(2144, 0, 1, 'product_id=116', 'toyota-innova-20e13', 'product'),
(2145, 0, 2, 'product_id=117', 'toyota-innova-20e14', 'product'),
(2146, 0, 1, 'product_id=117', 'toyota-innova-20e14', 'product'),
(2147, 0, 2, 'product_id=118', 'toyota-innova-20e15', 'product'),
(2148, 0, 1, 'product_id=118', 'toyota-innova-20e15', 'product'),
(2149, 0, 2, 'product_id=119', 'toyota-innova-20e16', 'product'),
(2150, 0, 1, 'product_id=119', 'toyota-innova-20e16', 'product'),
(2151, 0, 2, 'product_id=120', 'toyota-innova-20e17', 'product'),
(2152, 0, 1, 'product_id=120', 'toyota-innova-20e17', 'product'),
(2153, 0, 2, 'product_id=121', 'toyota-innova-20e18', 'product'),
(2154, 0, 1, 'product_id=121', 'toyota-innova-20e18', 'product'),
(2155, 0, 2, 'product_id=122', 'toyota-innova-20e19', 'product'),
(2156, 0, 1, 'product_id=122', 'toyota-innova-20e19', 'product');

-- oc_theme_builder_config NO PRIMARY KEY
-- remove first
DELETE FROM `oc_theme_builder_config`
WHERE `store_id` = '0'
  AND `config_theme` = 'car_carcenter'
  AND `code` = 'config';

-- insert
INSERT IGNORE INTO `oc_theme_builder_config` (`store_id`, `config_theme`, `code`, `key`, `config`, `date_added`, `date_modified`) VALUES
(0, 'car_carcenter', 'config', 'config_theme_color', '{\n    \"main\": {\n        \"main\": {\n            \"hex\": \"D2691E\",\n            \"visible\": \"1\"\n        },\n        \"button\": {\n            \"hex\": \"DE370D\",\n            \"visible\": \"1\"\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-23 09:04:55'),
(0, 'car_carcenter', 'config', 'config_theme_text', '{\n    \"all\": {\n        \"title\": \"Font Tahoma\",\n        \"font\": \"Tahoma\",\n        \"supported-fonts\": [\n            \"Open Sans\",\n            \"IBM Plex Sans\",\n            \"Courier New\",\n            \"Roboto\",\n            \"Nunito\",\n            \"Arial\",\n            \"DejaVu Sans\",\n            \"Tahoma\",\n            \"Time News Roman\"\n        ]\n    },\n    \"heading\": {\n        \"font\": \"Tahoma\",\n        \"supported-fonts\": [\n            \"Open Sans\",\n            \"IBM Plex Sans\",\n            \"Courier New\",\n            \"Roboto\",\n            \"Nunito\",\n            \"Arial\",\n            \"DejaVu Sans\",\n            \"Tahoma\",\n            \"Time News Roman\"\n        ],\n        \"font-type\": \"regular\",\n        \"supported-font-types\": [\n            \"regular\",\n            \"bold\",\n            \"italic\"\n        ],\n        \"font-size\": 32\n    },\n    \"body\": {\n        \"font\": \"Aria\",\n        \"supported-fonts\": [\n            \"Open Sans\",\n            \"IBM Plex Sans\",\n            \"Courier New\",\n            \"Roboto\",\n            \"Nunito\",\n            \"Arial\",\n            \"DejaVu Sans\",\n            \"Tahoma\",\n            \"Time News Roman\"\n        ],\n        \"font-type\": \"regular\",\n        \"supported-font-types\": [\n            \"regular\",\n            \"bold\",\n            \"italic\"\n        ],\n        \"font-size\": 14\n    }\n}', '2020-04-21 19:05:35', '2020-04-23 09:04:55'),
(0, 'car_carcenter', 'config', 'config_theme_favicon', '{\n    \"image\": {\n        \"url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1587472799\\/19449\\/logo%281587472798%29.png\"\n    }\n}', '2020-04-21 19:05:35', '2020-04-23 09:04:55'),
(0, 'car_carcenter', 'config', 'config_theme_social', '[\n    {\n        \"name\": \"facebook\",\n        \"text\": \"Facebook\",\n        \"url\": \"https:\\/\\/www.facebook.com\"\n    },\n    {\n        \"name\": \"twitter\",\n        \"text\": \"Twitter\",\n        \"url\": \"https:\\/\\/www.twitter.com\"\n    },\n    {\n        \"name\": \"instagram\",\n        \"text\": \"Instagram\",\n        \"url\": \"https:\\/\\/www.instagram.com\"\n    },\n    {\n        \"name\": \"tumblr\",\n        \"text\": \"Tumblr\",\n        \"url\": \"https:\\/\\/www.tumblr.com\"\n    },\n    {\n        \"name\": \"youtube\",\n        \"text\": \"Youtube\",\n        \"url\": \"https:\\/\\/www.youtube.com\"\n    },\n    {\n        \"name\": \"googleplus\",\n        \"text\": \"Google Plus\",\n        \"url\": \"https:\\/\\/www.plus.google.com\"\n    }\n]', '2020-04-21 19:05:35', '2020-04-23 09:04:55'),
(0, 'car_carcenter', 'config', 'config_theme_override_css', '{\n    \"css\": \".content-font {\\n    color: black !important;\\n    font-weight: 600 !important;\\n}\\n.main-color {\\n    background-color: white !important;\\n}\\n.block-header-menu .main-menu ul li a {\\n    font-size: 16px;\\n    color: black;\\n    font-family: \'Open Sans\', sans-serif;\\n    font-weight: 700;\\n}\\n.block-product {\\n  background-color: #4E4848;\\n  padding-top: 30px;\\n}\\n.block-product .block-head {\\n    margin-bottom: 0px;\\n}\\n.owl-carousel .owl-stage-outer .owl-stage {\\n    padding: 20px 0 10px 0;\\n}\\n.col-md-6.col-6.collection {\\n\\tpadding-top: 20px;\\n}\\n.content-font {\\n\\tcolor: #D2691E !important;\\n}\\n.block-footer .widget-footer span {\\n\\tcolor: white;\\n}\\n.block-footer .widget-footer .contact-item a {\\n\\tcolor: white;\\n}\\n.block-footer .widget-footer ul li a {\\n\\tcolor: white;\\n}\\n.widget-footer .social ul li:first-child a svg path {\\n\\tfill: #0a0b73;\\n}\\n.widget-footer .social ul li:nth-last-child(2) a svg path {\\n\\tfill: #8B0000;\\n}\\n.widget-footer .social ul li:last-child a svg path {\\n\\tfill: #FF0000;\\n}\\n.text-danger {\\n    color: white!important;\\n}\\nbody {\\n    font-family: \'Montserrat\', sans-serif;\\n}\\n.product-item .product-content {\\n    color: red;\\n    font-family: \'Open Sans\', sans-serif;\\n    font-weight: 600;\\n}\\n.block-footer {\\n    background-image: url(https:\\/\\/2.pik.vn\\/2020435d25b5-7c8d-4392-a1ef-aab8cd20db41.png);\\n}\\n.block-footer .widget-footer h5 {\\n    color: white;\\n}\\n.block-header {\\n    padding-top: 1px;\\n    padding-bottom: 10px;\\n}\\n.main-color {\\n    background-color: #C0C0C0 !important;\\n}\"\n}', '2020-04-21 19:05:35', '2020-04-23 09:04:55'),
(0, 'car_carcenter', 'config', 'config_theme_checkout_page', '{\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"header\": {\n            \"name\": \"header\",\n            \"text\": \"Header\",\n            \"visible\": 1\n        },\n        \"slide-show\": {\n            \"name\": \"slide-show\",\n            \"text\": \"Slideshow\",\n            \"visible\": 1\n        },\n        \"categories\": {\n            \"name\": \"categories\",\n            \"text\": \"Danh mục sản phẩm\",\n            \"visible\": 1\n        },\n        \"hot-deals\": {\n            \"name\": \"hot-deals\",\n            \"text\": \"Sản phẩm khuyến mại\",\n            \"visible\": 1\n        },\n        \"feature-products\": {\n            \"name\": \"feature-products\",\n            \"text\": \"Sản phẩm bán chạy\",\n            \"visible\": 1\n        },\n        \"related-products\": {\n            \"name\": \"related-products\",\n            \"text\": \"Sản phẩm xem nhiều\",\n            \"visible\": 1\n        },\n        \"new-products\": {\n            \"name\": \"new-products\",\n            \"text\": \"Sản phẩm mới\",\n            \"visible\": 1\n        },\n        \"product-detail\": {\n            \"name\": \"product-details\",\n            \"text\": \"Chi tiết sản phẩm\",\n            \"visible\": 1\n        },\n        \"banner\": {\n            \"name\": \"banner\",\n            \"text\": \"Banner\",\n            \"visible\": 1\n        },\n        \"content_customize\" : {\n            \"name\" : \"content-customize\",\n            \"text\" : \"Nội dung tùy chỉnh\",\n            \"visible\" : 1\n        },\n        \"partners\": {\n            \"name\": \"partners\",\n            \"text\": \"Đối tác\",\n            \"visible\": 1\n        },\n        \"blog\": {\n            \"name\": \"blog\",\n            \"text\": \"Blog\",\n            \"visible\": 1\n        },\n        \"footer\": {\n            \"name\": \"footer\",\n            \"text\": \"Footer\",\n            \"visible\": 1\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_banner', '{\n    \"name\": \"banner\",\n    \"text\": \"Banner\",\n    \"visible\": \"1\",\n    \"display\": [\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1587472611\\/19449\\/banner-2.png\",\n            \"url\": \"https:\\/\\/oto-tuankhaiauto.bestme.asia\\/toyota-innova-20e1\",\n            \"description\": \"Banner 1\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1587472607\\/19449\\/banner-1.png\",\n            \"url\": \"https:\\/\\/oto-tuankhaiauto.bestme.asia\\/toyota-innova-20e5\",\n            \"description\": \"Banner 2\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1587472714\\/19449\\/banner-3.png\",\n            \"url\": \"https:\\/\\/oto-tuankhaiauto.bestme.asia\\/toyota-innova-20e4\",\n            \"description\": \"Banner 3\",\n            \"visible\": \"1\"\n        }\n    ]\n}', '2020-04-21 19:05:35', '2020-04-22 11:06:37'),
(0, 'car_carcenter', 'config', 'config_section_best_sales_product', '{\n    \"name\": \"feature-product\",\n    \"text\": \"Sản phẩm bán chạy\",\n    \"visible\": \"1\",\n    \"setting\": {\n        \"title\": \"Sản phẩm bán chạy\",\n        \"sub_title\": \"Mô tả tiêu đề\",\n        \"auto_retrieve_data\": \"0\",\n        \"collection_id\": \"8\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": \"4\"\n        },\n        \"grid_mobile\": {\n            \"quantity\": \"2\"\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-22 10:04:04'),
(0, 'car_carcenter', 'config', 'config_section_new_product', '{\n    \"name\": \"new-product\",\n    \"text\": \"Sản phẩm mới\",\n    \"visible\": \"1\",\n    \"setting\": {\n        \"title\": \"Sản phẩm mới\",\n        \"sub_title\": \"Mô tả tiêu đề\",\n        \"auto_retrieve_data\": \"0\",\n        \"collection_id\": \"11\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": \"2\"\n        },\n        \"grid_mobile\": {\n            \"quantity\": \"2\"\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:52:54'),
(0, 'car_carcenter', 'config', 'config_section_best_views_product', '{\n    \"name\": \"related-product\",\n    \"text\": \"Sản phẩm xem nhiều\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Sản phẩm xem nhiều\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 10\n        },\n        \"grid_mobile\": {\n            \"quantity\": 2\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_blog', '{\n    \"name\": \"blog\",\n    \"text\": \"Blog\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Blog\"\n    },\n    \"display\": {\n        \"menu\": [\n            {\n                \"type\": \"existing\",\n                \"menu\": {\n                    \"id\": 12,\n                    \"name\": \"Danh sách bài viết 1\"\n                }\n            },\n            {\n                \"type\": \"manual\",\n                \"entries\": [\n                    {\n                        \"id\": 10,\n                        \"name\": \"Entry 10\"\n                    },\n                    {\n                        \"id\": 11,\n                        \"name\": \"Entry 11\"\n                    }\n                ]\n            }\n        ],\n        \"grid\": {\n            \"quantity\": 10\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_detail_product', '{\n    \"name\": \"product-detail\",\n    \"text\": \"Chi tiết sản phẩm\",\n    \"visible\": 1,\n    \"display\": {\n        \"name\": true,\n        \"description\": false,\n        \"price\": true,\n        \"price-compare\": false,\n        \"status\": false,\n        \"sale\": false,\n        \"rate\": false\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_footer', '{\n    \"name\": \"footer\",\n    \"text\": \"Footer\",\n    \"visible\": 1,\n    \"contact\": {\n        \"title\": \"Liên hệ chúng tôi\",\n        \"address\": \"Cau Giay - Ha Noi\",\n        \"phone-number\": \"(+84)987654321\",\n        \"email\": \"dungbt@novaon.vn\",\n        \"visible\": 1\n    },\n    \"collection\": {\n        \"title\": \"Bộ sưu tập\",\n        \"menu_id\": 6,\n        \"visible\": 1\n    },\n    \"quick-links\": {\n        \"title\": \"Liên kết nhanh\",\n        \"menu_id\": 1,\n        \"visible\": 1\n    },\n    \"subscribe\": {\n        \"title\": \"Đăng ký theo dõi\",\n        \"social_network\": 1,\n        \"visible\": 1,\n        \"youtube_visible\": 1,\n        \"facebook_visible\": 1,\n        \"instagram_visible\": 1\n    }\n}', '2020-04-21 19:05:35', '2020-04-22 11:08:38'),
(0, 'car_carcenter', 'config', 'config_section_header', '{\n    \"name\": \"header\",\n    \"text\": \"Header\",\n    \"visible\": \"1\",\n    \"notify-bar\": {\n        \"name\": \"Thanh thông báo\",\n        \"visible\": \"1\",\n        \"content\": \"Ô tô Tuấn Khải - Sang trọng - Tinh tế - Đẳng cấp                                        - Hotline: 0982263268\",\n        \"url\": \"https:\\/\\/oto-tuankhaiauto.bestme.asia\\/san-pham\"\n    },\n    \"logo\": {\n        \"name\": \"Logo\",\n        \"url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1587472252\\/19449\\/logo.png\",\n        \"height\": \"84\"\n    },\n    \"menu\": {\n        \"name\": \"Menu\",\n        \"display-list\": {\n            \"name\": \"Menu chính\",\n            \"id\": \"1\"\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-22 09:50:19'),
(0, 'car_carcenter', 'config', 'config_section_hot_product', '{\n    \"name\": \"hot-deals\",\n    \"text\": \"Sản phẩm khuyến mại\",\n    \"visible\": \"1\",\n    \"setting\": {\n        \"title\": \"Sản phẩm khuyến mại\",\n        \"sub_title\": \"Mô tả tiêu đề\",\n        \"auto_retrieve_data\": \"0\",\n        \"collection_id\": \"8\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": \"4\"\n        },\n        \"grid_mobile\": {\n            \"quantity\": \"2\"\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:52:34'),
(0, 'car_carcenter', 'config', 'config_section_list_product', '{\n    \"name\": \"categories\",\n    \"text\": \"Danh mục sản phẩm\",\n    \"visible\": 0,\n    \"setting\": {\n        \"title\": \"Danh mục sản phẩm\"\n    },\n    \"display\": {\n        \"menu\": {\n            \"id\": 1,\n            \"name\": \"Danh mục sản phẩm 1\"\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_partner', '{\n    \"name\": \"partners\",\n    \"text\": \"Đối tác\",\n    \"visible\": \"1\",\n    \"limit-per-line\": \"4\",\n    \"display\": [\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1587472805\\/19449\\/unnamed.png\",\n            \"url\": \"#\",\n            \"description\": \"Partner 1\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1587472803\\/19449\\/unnamed-1.jpg\",\n            \"url\": \"#\",\n            \"description\": \"Partner 2\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1587472801\\/19449\\/mazda-car-logo.jpg\",\n            \"url\": \"#\",\n            \"description\": \"Partner 3\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1587472891\\/19449\\/images.png\",\n            \"url\": \"#\",\n            \"description\": \"Partner 4\",\n            \"visible\": \"1\"\n        }\n    ]\n}', '2020-04-21 19:05:35', '2020-04-21 19:41:37'),
(0, 'car_carcenter', 'config', 'config_section_slideshow', '{\n    \"name\": \"slide-show\",\n    \"text\": \"Slideshow\",\n    \"visible\": \"1\",\n    \"setting\": {\n        \"transition-time\": \"3\"\n    },\n    \"display\": [\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1587471674\\/19449\\/slider.png\",\n            \"url\": \"https:\\/\\/oto-tuankhaiauto.bestme.asia\\/san-pham\\/bst-tong-hop\",\n            \"description\": \"slide_1\",\n            \"id\": \"51534105\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1587471863\\/19449\\/slider1.png\",\n            \"description\": \"\",\n            \"id\": \"51737638\",\n            \"url\": \"https:\\/\\/oto-tuankhaiauto.bestme.asia\\/san-pham\\/sieu-pham-2020\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1587471866\\/19449\\/slider2.png\",\n            \"description\": \"\",\n            \"id\": \"71803511\",\n            \"url\": \"https:\\/\\/oto-tuankhaiauto.bestme.asia\\/toyota-innova-20e2\"\n        }\n    ]\n}', '2020-04-21 19:05:35', '2020-04-22 11:05:37'),
(0, 'car_carcenter', 'config', 'config_section_product_groups', '{\n    \"name\": \"group products\",\n    \"text\": \"nhóm sản phẩm\",\n    \"visible\": \"1\",\n    \"list\": [\n        {\n            \"text\": \"Danh sách sản phẩm 1\",\n            \"visible\": \"1\",\n            \"setting\": {\n                \"title\": \"Siêu phẩm 2020\",\n                \"collection_id\": \"8\",\n                \"sub_title\": \"Danh sách sản phẩm 1\"\n            },\n            \"display\": {\n                \"grid\": {\n                    \"quantity\": \"4\"\n                },\n                \"grid_mobile\": {\n                    \"quantity\": \"2\"\n                }\n            }\n        },\n        {\n            \"text\": \"Danh sách sản phẩm\",\n            \"visible\": \"1\",\n            \"setting\": {\n                \"title\": \"TOYOTA INNOVA\",\n                \"collection_id\": \"10\"\n            },\n            \"display\": {\n                \"grid\": {\n                    \"quantity\": \"4\"\n                },\n                \"grid_mobile\": {\n                    \"quantity\": \"1\"\n                }\n            }\n        }\n    ]\n}', '2020-04-21 19:05:35', '2020-04-21 19:53:34'),
(0, 'car_carcenter', 'config', 'config_section_content_customize', '{\n    \"name\": \"content-customize\",\n    \"title\": \"Nội dung tuỳ chỉnh\",\n    \"display\": [\n        {\n            \"name\": \"content-1\",\n            \"title\": \"Bảo đảm chất lượng\",\n            \"type\": \"fixed\",\n            \"content\": {\n                \"icon\": \"/catalog/view/theme/default/image/favicon.ico\",\n                \"title\": \"Bảo đảm chất lượng\",\n                \"description\": \"Sản phẩm đảm bảo chất lượng\",\n                \"html\": \"\"\n            }\n        },\n        {\n            \"name\": \"content-2\",\n            \"title\": \"Miễn phí giao hàng\",\n            \"type\": \"fixed\",\n            \"content\": {\n                \"icon\": \"/catalog/view/theme/default/image/favicon.ico\",\n                \"title\": \"Miễn phí giao hàng\",\n                \"description\": \"Cho đơn hàng từ 2 triệu\",\n                \"html\": \"\"\n            }\n        },\n        {\n            \"name\": \"content-3\",\n            \"title\": \"Hỗ trợ 24/7\",\n            \"type\": \"fixed\",\n            \"content\": {\n                \"icon\": \"/catalog/view/theme/default/image/favicon.ico\",\n                \"title\": \"Hỗ trợ 24/7\",\n                \"description\": \"Hotline 012.345.678\",\n                \"html\": \"\"\n            }\n        },\n        {\n            \"name\": \"content-4\",\n            \"title\": \"Đổi trả hàng\",\n            \"type\": \"fixed\",\n            \"content\": {\n                \"icon\": \"/catalog/view/theme/default/image/favicon.ico\",\n                \"title\": \"Đổi trả hàng\",\n                \"description\": \"Trong vòng 7 ngày\",\n                \"html\": \"\"\n            }\n        }\n    ]\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_category_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"banner\": {\n            \"name\": \"banner\",\n            \"text\": \"Banner\",\n            \"visible\": 1\n        },\n        \"filter\": {\n            \"name\": \"filter\",\n            \"text\": \"Filter\",\n            \"visible\": 1\n        },\n        \"product_category\": {\n            \"name\": \"product_category\",\n            \"text\": \"Product Category\",\n            \"visible\": 1\n        },\n        \"product_list\": {\n            \"name\": \"product_list\",\n            \"text\": \"Product List\",\n            \"visible\": 1\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_category_banner', '{\n    \"name\": \"banner\",\n    \"text\": \"Banner\",\n    \"visible\": 1,\n    \"display\": [\n        {\n            \"image-url\": \"/catalog/view/theme/default/image/banner/img-banner-06.jpg\",\n            \"url\": \"#\",\n            \"description\": \"Banner 1\",\n            \"visible\": 1\n        },\n        {\n            \"image-url\": \"/catalog/view/theme/default/image/banner/img-banner-02.png\",\n            \"url\": \"#\",\n            \"description\": \"Banner 2\",\n            \"visible\": 1\n        },\n        {\n            \"image-url\": \"/catalog/view/theme/default/image/banner/img-banner-03.png\",\n            \"url\": \"#\",\n            \"description\": \"Banner 3\",\n            \"visible\": 1\n        }\n    ]\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_category_filter', '{\n    \"name\": \"filter\",\n    \"text\": \"Bộ lọc\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Bộ lọc\"\n    },\n    \"display\": {\n        \"supplier\": {\n            \"title\": \"Nhà cung cấp\",\n            \"visible\": 1\n        },\n        \"product-type\": {\n            \"title\": \"Loại sản phẩm\",\n            \"visible\": 1\n        },\n        \"collection\": {\n            \"title\": \"Bộ sưu tập\",\n            \"visible\": 1\n        },\n        \"property\": {\n            \"title\": \"Lọc theo tt - k dung\",\n            \"visible\": 1,\n            \"prop\": [\n                \"all\",\n                \"color\",\n                \"weight\",\n                \"size\"\n            ]\n        },\n        \"product-price\": {\n            \"title\": \"Giá sản phẩm\",\n            \"visible\": 1,\n            \"range\": {\n                \"from\": 0,\n                \"to\": 100000000\n            }\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_category_product_category', '{\n    \"name\": \"product-category\",\n    \"text\": \"Danh mục sản phẩm\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Danh mục sản phẩm\"\n    },\n    \"display\": {\n        \"menu\": {\n            \"id\": 1,\n            \"name\": \"Danh mục sản phẩm 1\"\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_category_product_list', '{\n    \"name\": \"product-list\",\n    \"text\": \"Danh sách sản phẩm\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Danh sách sản phẩm\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 10\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_product_detail_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"related_product\": {\n            \"name\": \"related_product\",\n            \"text\": \"Related Product\",\n            \"visible\": 1\n        },\n        \"template\": {\n            \"name\": \"template\",\n            \"text\": \"Template\",\n            \"visible\": 1\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_product_detail_related_product', '{\n    \"name\": \"related-product\",\n    \"text\": \"Sản phẩm liên quan\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Sản phẩm liên quan\",\n        \"auto_retrieve_data\": 0,\n        \"collection_id\": 1\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 4\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_product_detail_template', '{\n    \"name\": \"template\",\n    \"text\": \"Giao diện\",\n    \"visible\": 1,\n    \"display\": {\n        \"template\": {\n            \"id\": 10\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_blog_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"blog_category\": {\n            \"name\": \"blog_category\",\n            \"text\": \"Blog Category\",\n            \"visible\": 1\n        },\n        \"blog_list\": {\n            \"name\": \"blog_list\",\n            \"text\": \"Blog List\",\n            \"visible\": 1\n        },\n        \"latest_blog\": {\n            \"name\": \"latest_blog\",\n            \"text\": \"Latest Blog\",\n            \"visible\": 1\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_blog_blog_category', '{\n    \"name\": \"blog-category\",\n    \"text\": \"Danh mục bài viết\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Danh mục bài viết\"\n    },\n    \"display\": {\n        \"menu\": {\n            \"id\": 1,\n            \"name\": \"Danh mục bài viết 1\"\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_blog_blog_list', '{\n    \"name\": \"blog-list\",\n    \"text\": \"Danh sách bài viết\",\n    \"visible\": 1,\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 20\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_blog_latest_blog', '{\n    \"name\": \"latest-blog\",\n    \"text\": \"Bài viết mới nhất\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Bài viết mới nhất\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 10\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_contact_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"map\": {\n            \"name\": \"map\",\n            \"text\": \"Bản đồ\",\n            \"address\": \"<iframe src=\\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\\" width=\\\"600\\\" height=\\\"450\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\" allowfullscreen></iframe>\",\n            \"visible\": 1\n        },\n        \"info\": {\n            \"name\": \"info\",\n            \"text\": \"Thông tin cửa hàng\",\n            \"email\": \"contact-us@novaon.asia\",\n            \"phone\": \"0000000000\",\n            \"address\": \"address\",\n            \"visible\": 1\n        },\n        \"form\": {\n            \"name\": \"form\",\n            \"title\": \"Form liên hệ\",\n            \"email\": \"email@mail.com\",\n            \"visible\": 1\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_contact_map', '{   //////// CÁI NÀY KHÔNG DÙNG NỮA\n    \"name\": \"map\",\n    \"text\": \"Bản đồ\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Bản đồ\"\n    },\n    \"display\": {\n        \"address\": \"<iframe src=\\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\\" width=\\\"600\\\" height=\\\"450\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\" allowfullscreen></iframe>\"\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_contact_contact', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\n    \"name\": \"contact\",\n    \"text\": \"Liên hệ\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Vị trí của chúng tôi\"\n    },\n    \"display\": {\n        \"store\": {\n            \"title\": \"Gian hàng\",\n            \"content\": \"Số 12 Dịch Vọng Hậu, Cầu Giấy, Hà Nội\",\n            \"visible\": 1\n        },\n        \"telephone\": {\n            \"title\": \"Điện thoại\",\n            \"content\": \"(+84) 987 654 321\",\n            \"visible\": 1\n        },\n        \"social_follow\": {\n            \"socials\": [\n                {\n                    \"name\": \"facebook\",\n                    \"text\": \"Facebook\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"twitter\",\n                    \"text\": \"Twitter\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"instagram\",\n                    \"text\": \"Instagram\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"tumblr\",\n                    \"text\": \"Tumblr\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"youtube\",\n                    \"text\": \"Youtube\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"googleplus\",\n                    \"text\": \"Google Plus\",\n                    \"visible\": 1\n                }\n            ],\n            \"visible\": 1\n        }\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35'),
(0, 'car_carcenter', 'config', 'config_section_contact_form', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\n    \"name\": \"form\",\n    \"text\": \"Biểu mẫu\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Liên hệ với chúng tôi\"\n    },\n    \"data\": {\n        \"email\": \"sale.247@xshop.com\"\n    }\n}', '2020-04-21 19:05:35', '2020-04-21 19:05:35');

-- oc_blog PRIMARY KEY (`blog_id`)
-- nothing

-- oc_blog_category PRIMARY KEY (`blog_category_id`)
-- nothing

-- oc_blog_category_description PRIMARY KEY (`blog_category_id`,`language_id`)
-- nothing

-- oc_blog_description PRIMARY KEY (`blog_id`,`language_id`)
-- nothing

-- oc_blog_to_blog_category NO PRIMARY KEY
-- nothing
