-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(11, 'Tiện nghi phòng bếp', 0, 1, '', '0'),
(10, 'Thiết bị tiện nghi', 0, 1, '', '0'),
(9, 'Thiết bị cao cấp', 0, 1, '', '0'),
(12, 'Sản phẩm hiện đại, nhiều tính năng', 0, 1, '', '0'),
(13, 'Hot sale', 0, 1, '', '0'),
(14, 'Sản phẩm phục vụ nội trợ', 0, 1, '', '1'),
(15, 'Tiện nghi cuộc sống', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, 'https://cdn.bestme.asia/images/giadungs/bst3.jpg', '', '', '', ''),
(10, 'https://cdn.bestme.asia/images/giadungs/bst4.jpg', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/giadungs/bst2.jpg', '', '', '', ''),
(12, 'https://cdn.bestme.asia/images/giadungs/bst1.jpg', '', '', '', ''),
(13, '', '', '', '', ''),
(14, '', '', '', '', ''),
(15, '', '', '', '', '');