-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(38, '', 0, 0, 0, 0, 1, '2020-12-14 14:54:56', '2020-12-14 14:54:56'),
(37, '', 0, 0, 0, 0, 1, '2020-12-14 14:54:47', '2020-12-14 14:54:47'),
(35, '', 0, 0, 0, 0, 1, '2020-12-14 14:53:48', '2020-12-14 14:53:48'),
(34, '', 0, 0, 0, 0, 1, '2020-12-14 14:53:37', '2020-12-14 14:53:37'),
(36, '', 0, 0, 0, 0, 1, '2020-12-14 14:54:18', '2020-12-14 14:54:18'),
(32, '', 0, 0, 0, 0, 1, '2020-12-14 14:53:12', '2020-12-14 14:53:12'),
(33, '', 0, 0, 0, 0, 1, '2020-12-14 14:53:23', '2020-12-14 14:53:23');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(38, 1, 'Tivi', 'Tivi', '', '', ''),
(38, 2, 'Tivi', 'Tivi', '', '', ''),
(37, 1, 'Máy lạnh, máy lọc không khí', 'Máy lạnh, máy lọc không khí', '', '', ''),
(32, 1, 'Tủ lạnh, tủ mát', 'Tủ lạnh, tủ mát', '', '', ''),
(33, 2, 'Lò nướng, lò vi sóng', 'Lò nướng, lò vi sóng', '', '', ''),
(33, 1, 'Lò nướng, lò vi sóng', 'Lò nướng, lò vi sóng', '', '', ''),
(34, 2, 'Máy ép, máy xay', 'Máy ép, máy xay', '', '', ''),
(34, 1, 'Máy ép, máy xay', 'Máy ép, máy xay', '', '', ''),
(35, 2, 'Bếp điện', 'Bếp điện', '', '', ''),
(35, 1, 'Bếp điện', 'Bếp điện', '', '', ''),
(36, 2, 'Thiết bị, dụng cụ nhà bếp', 'Thiết bị, dụng cụ nhà bếp', '', '', ''),
(36, 1, 'Thiết bị, dụng cụ nhà bếp', 'Thiết bị, dụng cụ nhà bếp', '', '', ''),
(37, 2, 'Máy lạnh, máy lọc không khí', 'Máy lạnh, máy lọc không khí', '', '', ''),
(32, 2, 'Tủ lạnh, tủ mát', 'Tủ lạnh, tủ mát', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(34, 34, 0),
(33, 33, 0),
(36, 36, 0),
(35, 35, 0),
(32, 32, 0),
(37, 37, 0),
(38, 38, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0);