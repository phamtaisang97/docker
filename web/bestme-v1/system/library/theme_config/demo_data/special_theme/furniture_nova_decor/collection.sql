-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(9, 'Sản phẩm bán chạy', 0, 1, '', '0'),
(10, 'Tân Phú', 0, 1, '', '0'),
(11, 'Đại Đồng Tiến', 0, 1, '', '0'),
(12, 'Song Long', 0, 1, '', '0'),
(13, 'Việt Nhật', 0, 1, '', '0'),
(14, 'Duy Tân', 0, 1, '', '0'),
(15, 'Việt Thanh', 0, 1, '', '0'),
(16, 'Đối tác tiêu biểu', 0, 1, '', '1'),
(17, 'Tủ quần áo', 0, 1, '', '0'),
(18, 'Tủ cho bé', 0, 1, '', '0'),
(19, 'Tủ trang điểm', 0, 1, '', '0'),
(20, 'Tủ đựng giày', 0, 1, '', '0'),
(21, 'Tủ đựng sách', 0, 1, '', '0'),
(22, 'Kệ trang trí', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, '', '', '', '', ''),
(10, 'https://cdn.bestme.asia/images/x2/328160005269729.png', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/x2/3-100_otZGYUQ.jpg', '', '', '', ''),
(12, 'https://cdn.bestme.asia/images/x2/song-long-100_URykYmR.jpg', '', '', '', ''),
(13, 'https://cdn.bestme.asia/images/x2/2-100_MAwY4Wr.jpg', '', '', '', ''),
(14, 'https://cdn.bestme.asia/images/x2/1-100_eauqYFY.jpg', '', '', '', ''),
(15, 'https://cdn.bestme.asia/images/x2/viet-thanh-100_LmXqna5.jpg', '', '', '', ''),
(16, '', '', '', '', ''),
(17, '', '', '', '', ''),
(18, '', '', '', '', ''),
(19, '', '', '', '', ''),
(20, '', '', '', '', ''),
(21, '', '', '', '', ''),
(22, '', '', '', '', '');