-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(9, 'Vest Cưới Sang Trọng', 0, 1, NULL, '0'),
(10, 'Hoa Cưới', 0, 1, NULL, '0'),
(11, 'Thiệp cưới', 0, 1, NULL, '0'),
(12, 'VÁY TRẮNG', 0, 1, NULL, '0'),
(13, 'Váy Cưới Màu Sắc', 0, 1, NULL, '0'),
(14, 'Ảnh Cưới', 0, 1, NULL, '0'),
(15, 'Xe Cưới', 0, 1, NULL, '0'),
(16, 'Váy Cưới Cho Mẹ Bầu Xinh Đẹp', 0, 1, NULL, '0'),
(17, 'Váy Cưới Tôn Dáng', 0, 1, NULL, '0');


-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, '', '', '', '', ''),
(10, '', '', '', '', ''),
(11, '', '', '', '', ''),
(12, '', '', '', '', ''),
(13, '', '', '', '', ''),
(14, '', '', '', '', ''),
(15, '', '', '', '', ''),
(16, '', '', '', '', ''),
(17, '', '', '', '', '');
