-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(38, NULL, 0, 0, 0, 1, 1, '2020-12-17 11:36:18', '2020-12-17 11:38:13'),
(37, NULL, 0, 0, 0, 3, 1, '2020-12-17 11:36:18', '2020-12-17 11:38:24'),
(35, NULL, 0, 0, 0, 4, 1, '2020-12-17 11:36:18', '2020-12-17 11:38:32'),
(34, NULL, 0, 0, 0, 6, 1, '2020-12-17 11:36:18', '2020-12-17 11:38:42'),
(36, NULL, 0, 0, 0, 2, 1, '2020-12-17 11:36:18', '2020-12-17 11:38:18'),
(32, NULL, 0, 0, 0, 5, 1, '2020-12-17 11:36:18', '2020-12-17 11:38:35'),
(33, NULL, 0, 0, 0, 7, 1, '2020-12-17 11:36:18', '2020-12-17 11:38:44');


-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(38, 2, 'Váy cưới màu sắc', 'Váy cưới màu sắc', '', '', ''),
(38, 1, 'Váy cưới màu sắc', 'Váy cưới màu sắc', '', '', ''),
(32, 1, 'Vest cưới', 'Vest cưới', '', '', ''),
(33, 2, 'Hoa cưới', 'Hoa cưới', '', '', ''),
(33, 1, 'Hoa cưới', 'Hoa cưới', '', '', ''),
(34, 2, 'Thiệp cưới', 'Thiệp cưới', '', '', ''),
(34, 1, 'Thiệp cưới', 'Thiệp cưới', '', '', ''),
(35, 2, 'Váy cưới bà bầu', 'Váy cưới bà bầu', '', '', ''),
(35, 1, 'Váy cưới bà bầu', 'Váy cưới bà bầu', '', '', ''),
(36, 2, 'Váy cưới bó sát', 'Váy cưới bó sát', '', '', ''),
(36, 1, 'Váy cưới bó sát', 'Váy cưới bó sát', '', '', ''),
(37, 2, 'Váy cưới dáng xòa', 'Váy cưới dáng xòa', '', '', ''),
(37, 1, 'Váy cưới dáng xòa', 'Váy cưới dáng xòa', '', '', ''),
(32, 2, 'Vest cưới', 'Vest cưới', '', '', '');


-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(34, 34, 0),
(33, 33, 0),
(36, 36, 0),
(35, 35, 0),
(32, 32, 0),
(37, 37, 0),
(38, 38, 0);


-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0);
