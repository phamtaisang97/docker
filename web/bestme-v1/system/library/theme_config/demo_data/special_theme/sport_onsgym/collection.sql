-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(13, 'Giày Tập', 0, 1, '', '0'),
(12, 'Quần Áo Tập', 0, 1, '', '0'),
(11, 'Thiết bị tập', 0, 1, '', '0'),
(10, 'Các Dịch Vụ', 0, 1, '', '0'),
(14, 'Phụ Kiện Tập Gym', 0, 1, '', '0'),
(15, 'Phụ Kiện Tập Yoga', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(10, '', '', '', '', ''),
(11, '', '', '', '', ''),
(12, '', '', '', '', ''),
(13, '', '', '', '', ''),
(14, '', '', '', '', ''),
(15, '', '', '', '', '');