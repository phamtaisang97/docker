-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(9, 'Sản phẩm mới', 0, 1, '', '0'),
(10, 'Sản phẩm khuyến mại', 0, 1, '', '0'),
(11, 'Sản phẩm bán chạy', 0, 1, '', '0'),
(12, 'Kho vật tư y tế', 0, 1, '', '1'),
(13, 'Nhiệt kế', 0, 1, '', '0'),
(14, 'Dụng cụ y tế chuyên dụng', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, '', '', '', '', ''),
(10, '', '', '', '', ''),
(11, '', '', '', '', ''),
(12, '', '', '', '', ''),
(13, 'https://cdn.bestme.asia/images/khovattuyte/bst1100.jpg', '', '', '', ''),
(14, 'https://cdn.bestme.asia/images/khovattuyte/bst2100.jpg', '', '', '', '');