-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(43, '', 0, 0, 0, 0, 1, '2020-12-10 20:50:21', '2020-12-10 20:50:21'),
(42, '', 0, 0, 0, 0, 1, '2020-12-10 20:50:10', '2020-12-10 20:50:10'),
(41, '', 0, 0, 0, 0, 1, '2020-12-10 20:49:56', '2020-12-10 20:49:56'),
(40, '', 0, 0, 0, 0, 1, '2020-12-10 20:49:39', '2020-12-10 20:49:39'),
(39, '', 0, 0, 0, 0, 1, '2020-12-10 20:49:28', '2020-12-10 20:49:28'),
(38, '', 0, 0, 0, 0, 1, '2020-12-10 20:49:20', '2020-12-10 20:49:20'),
(32, '', 0, 0, 0, 0, 1, '2020-12-10 20:47:54', '2020-12-10 20:47:54'),
(33, '', 0, 0, 0, 0, 1, '2020-12-10 20:49:05', '2020-12-10 20:49:05'),
(34, '', 33, 0, 0, 0, 1, '2020-12-10 20:49:05', '2020-12-10 20:49:05'),
(35, '', 33, 0, 0, 0, 1, '2020-12-10 20:49:05', '2020-12-10 20:49:05'),
(36, '', 33, 0, 0, 0, 1, '2020-12-10 20:49:05', '2020-12-10 20:49:05'),
(37, '', 33, 0, 0, 0, 1, '2020-12-10 20:49:05', '2020-12-10 20:49:05');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(42, 2, 'Thiết bị dụng cụ y tế gia đình', 'Thiết bị dụng cụ y tế gia đình', '', '', ''),
(41, 1, 'Ống nghiệm, kim lấy mẫu', 'Ống nghiệm, kim lấy mẫu', '', '', ''),
(32, 1, 'Chẩn đoán hình ảnh', 'Chẩn đoán hình ảnh', '', '', ''),
(32, 2, 'Chẩn đoán hình ảnh', 'Chẩn đoán hình ảnh', '', '', ''),
(41, 2, 'Ống nghiệm, kim lấy mẫu', 'Ống nghiệm, kim lấy mẫu', '', '', ''),
(40, 1, 'Nhi, sản phụ khoa', 'Nhi, sản phụ khoa', '', '', ''),
(37, 2, 'Kim luồn, kim gây tê', 'Kim luồn, kim gây tê', '', '', ''),
(37, 1, 'Kim luồn, kim gây tê', 'Kim luồn, kim gây tê', '', '', ''),
(38, 2, 'Hô hấp', 'Hô hấp', '', '', ''),
(38, 1, 'Hô hấp', 'Hô hấp', '', '', ''),
(39, 2, 'Khử khuẩn, tiệt trùng', 'Khử khuẩn, tiệt trùng', '', '', ''),
(39, 1, 'Khử khuẩn, tiệt trùng', 'Khử khuẩn, tiệt trùng', '', '', ''),
(40, 2, 'Nhi, sản phụ khoa', 'Nhi, sản phụ khoa', '', '', ''),
(33, 2, 'Dụng cụ phẫu thuật', 'Dụng cụ phẫu thuật', '', '', ''),
(33, 1, 'Dụng cụ phẫu thuật', 'Dụng cụ phẫu thuật', '', '', ''),
(34, 2, 'Băng gạc phẫu thuật', 'Băng gạc phẫu thuật', '', '', ''),
(34, 1, 'Băng gạc phẫu thuật', 'Băng gạc phẫu thuật', '', '', ''),
(35, 2, 'Chỉ phẫu thuật', 'Chỉ phẫu thuật', '', '', ''),
(35, 1, 'Chỉ phẫu thuật', 'Chỉ phẫu thuật', '', '', ''),
(36, 2, 'Dao kéo phẫu thuật', 'Dao kéo phẫu thuật', '', '', ''),
(36, 1, 'Dao kéo phẫu thuật', 'Dao kéo phẫu thuật', '', '', ''),
(42, 1, 'Thiết bị dụng cụ y tế gia đình', 'Thiết bị dụng cụ y tế gia đình', '', '', ''),
(43, 2, 'Tiêm, truyền dịch', 'Tiêm, truyền dịch', '', '', ''),
(43, 1, 'Tiêm, truyền dịch', 'Tiêm, truyền dịch', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(43, 43, 0),
(42, 42, 0),
(36, 36, 1),
(37, 33, 0),
(37, 37, 1),
(33, 33, 0),
(41, 41, 0),
(40, 40, 0),
(34, 34, 1),
(36, 33, 0),
(39, 39, 0),
(38, 38, 0),
(32, 32, 0),
(35, 33, 0),
(35, 35, 1),
(34, 33, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(39, 0),
(40, 0),
(41, 0),
(42, 0),
(43, 0);