-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(35, 'Đồ phong thủy', 0, 1, '', '1'),
(34, 'Tượng Phật', 0, 1, '', '0'),
(33, 'Sản phẩm bán chạy', 0, 1, '', '0'),
(32, 'Vật phẩm phong thủy', 0, 1, '', '0'),
(31, 'Sản phẩm nổi bật', 0, 1, '', '0'),
(30, 'Vòng phong thủy', 0, 1, '', '0'),
(29, 'Linh vật phong thủy', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(32, '', '', '', '', ''),
(33, 'https://cdn.bestme.asia/images/thegioiphongthuy/banner5100.jpg', '', '', '', ''),
(34, '', '', '', '', ''),
(35, '', '', '', '', ''),
(29, '', '', '', '', ''),
(30, 'https://cdn.bestme.asia/images/thegioiphongthuy/banner4-100.jpg', '', '', '', ''),
(31, 'https://cdn.bestme.asia/images/thegioiphongthuy/banner3-100.jpg', '', '', '', '');