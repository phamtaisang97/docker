-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(22, '', 0, 0, 0, 0, 1, '2020-07-22 11:39:57', '2020-07-22 11:39:57'),
(21, '', 0, 0, 0, 0, 1, '2020-07-22 11:39:48', '2020-07-22 11:39:48'),
(4, '', 0, 0, 0, 0, 1, '2019-01-09 15:37:11', '2020-07-22 11:29:13'),
(3, '', 0, 0, 0, 0, 1, '2019-01-09 15:36:50', '2020-07-22 11:39:28'),
(2, '', 0, 0, 0, 0, 1, '2019-01-09 15:36:36', '2020-07-22 11:29:35'),
(1, '', 0, 0, 0, 0, 1, '2019-01-09 11:29:42', '2020-07-22 11:39:38'),
(23, '', 0, 0, 0, 0, 1, '2020-07-22 11:40:06', '2020-07-22 11:40:06');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(22, 2, 'Đồ uống - Giải khát', 'Đồ uống - Giải khát', '', '', ''),
(2, 2, 'Rau - Củ - Quả', 'Rau - Củ - Quả', '', '', ''),
(2, 1, 'Rau - Củ - Quả', 'Rau - Củ - Quả', '', '', ''),
(21, 1, 'Thực phẩm khô', 'Thực phẩm khô', '', '', ''),
(1, 2, 'Gia vị', 'Gia vị', '', '', ''),
(1, 1, 'Gia vị', 'Gia vị', '', '', ''),
(21, 2, 'Thực phẩm khô', 'Thực phẩm khô', '', '', ''),
(3, 2, 'Thịt - Cá - Trứng', 'Thịt - Cá - Trứng', '', '', ''),
(3, 1, 'Thịt - Cá - Trứng', 'Thịt - Cá - Trứng', '', '', ''),
(4, 2, 'Đồ đông lạnh', 'Đồ đông lạnh', '', '', ''),
(4, 1, 'Đồ đông lạnh', 'Đồ đông lạnh', '', '', ''),
(22, 1, 'Đồ uống - Giải khát', 'Đồ uống - Giải khát', '', '', ''),
(23, 2, 'Sữa', 'Sữa', '', '', ''),
(23, 1, 'Sữa', 'Sữa', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(23, 23, 0),
(21, 21, 0),
(4, 4, 0),
(3, 3, 0),
(2, 2, 0),
(1, 1, 0),
(22, 22, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(21, 0),
(22, 0),
(23, 0);