-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(26, NULL, 0, 0, 0, 0, 1, '2019-12-13 15:31:34', '2019-12-13 15:31:34'),
(25, NULL, 0, 0, 0, 0, 1, '2019-12-13 15:31:34', '2019-12-13 15:31:34'),
(24, NULL, 0, 0, 0, 0, 1, '2019-12-13 15:31:34', '2019-12-13 15:31:34'),
(23, NULL, 0, 0, 0, 0, 1, '2019-12-13 15:31:34', '2019-12-13 15:31:34'),
(22, NULL, 0, 0, 0, 0, 1, '2019-12-13 15:31:34', '2019-12-13 15:31:34'),
(21, NULL, 0, 0, 0, 0, 1, '2019-12-13 15:31:34', '2019-12-13 15:31:34'),
(27, NULL, 0, 0, 0, 0, 1, '2019-12-13 15:31:34', '2019-12-13 15:31:34'),
(28, NULL, 0, 0, 0, 0, 1, '2019-12-13 15:31:34', '2019-12-13 15:31:34'),
(29, NULL, 0, 0, 0, 0, 1, '2019-12-13 15:31:34', '2019-12-13 15:31:34'),
(30, NULL, 0, 0, 0, 0, 1, '2019-12-13 15:31:34', '2019-12-13 15:31:34'),
(31, NULL, 0, 0, 0, 0, 1, '2019-12-13 15:31:35', '2019-12-13 15:31:35'),
(32, NULL, 0, 0, 0, 0, 1, '2019-12-13 15:31:35', '2019-12-13 15:31:35'),
(33, NULL, 0, 0, 0, 0, 1, '2019-12-13 15:31:35', '2019-12-13 15:31:35'),
(12, NULL, 0, 0, 0, 0, 1, '2019-05-20 16:48:18', '2019-05-20 16:48:18'),
(14, NULL, 0, 0, 0, 0, 1, '2019-05-20 17:45:50', '2019-05-20 17:45:50'),
(17, NULL, 0, 0, 0, 0, 1, '2019-05-21 15:36:21', '2019-05-21 15:36:21'),
(18, NULL, 0, 0, 0, 0, 1, '2019-05-21 15:44:23', '2019-05-21 15:44:23'),
(19, NULL, 0, 0, 0, 0, 1, '2019-05-21 15:47:12', '2019-05-21 15:47:12'),
(20, NULL, 0, 0, 0, 0, 1, '2019-05-21 16:20:38', '2019-05-21 16:20:38');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(24, 2, 'Mẹ và bé', 'Mẹ và bé', '', '', ''),
(23, 2, 'Balo cho bé', 'Balo cho bé', '', '', ''),
(23, 1, 'Balo cho bé', 'Balo cho bé', '', '', ''),
(22, 1, 'Đồng hồ nam', 'Đồng hồ nam', '', '', ''),
(22, 2, 'Đồng hồ nam', 'Đồng hồ nam', '', '', ''),
(21, 1, 'Nước hoa nữ', 'Nước hoa nữ', '', '', ''),
(21, 2, 'Nước hoa nữ', 'Nước hoa nữ', '', '', ''),
(24, 1, 'Mẹ và bé', 'Mẹ và bé', '', '', ''),
(25, 2, 'Thời trang trẻ em', 'Thời trang trẻ em', '', '', ''),
(25, 1, 'Thời trang trẻ em', 'Thời trang trẻ em', '', '', ''),
(26, 2, 'Balo mẫu giáo', 'Balo mẫu giáo', '', '', ''),
(26, 1, 'Balo mẫu giáo', 'Balo mẫu giáo', '', '', ''),
(27, 2, 'Balo tiểu học', 'Balo tiểu học', '', '', ''),
(27, 1, 'Balo tiểu học', 'Balo tiểu học', '', '', ''),
(28, 2, 'Áo croptop', 'Áo croptop', '', '', ''),
(28, 1, 'Áo croptop', 'Áo croptop', '', '', ''),
(29, 2, 'Áo sơ mi nữ', 'Áo sơ mi nữ', '', '', ''),
(29, 1, 'Áo sơ mi nữ', 'Áo sơ mi nữ', '', '', ''),
(30, 2, 'Trang sức', 'Trang sức', '', '', ''),
(30, 1, 'Trang sức', 'Trang sức', '', '', ''),
(31, 2, 'Balo nam', 'Balo nam', '', '', ''),
(31, 1, 'Balo nam', 'Balo nam', '', '', ''),
(32, 2, 'Túi xách nữ', 'Túi xách nữ', '', '', ''),
(32, 1, 'Túi xách nữ', 'Túi xách nữ', '', '', ''),
(33, 2, 'Balo da', 'Balo da', '', '', ''),
(33, 1, 'Balo da', 'Balo da', '', '', ''),
(12, 2, 'Giày cổ điển', 'Giày cổ điển', '', '', ''),
(12, 1, 'Giày cổ điển', 'Giày cổ điển', '', '', ''),
(14, 2, 'Giày cao gót', 'Giày cao gót', '', '', ''),
(14, 1, 'Giày cao gót', 'Giày cao gót', '', '', ''),
(17, 2, 'Giày valen', 'Giày valen', '', '', ''),
(17, 1, 'Giày valen', 'Giày valen', '', '', ''),
(18, 2, 'Giày sandal', 'Giày sandal', '', '', ''),
(18, 1, 'Giày sandal', 'Giày sandal', '', '', ''),
(19, 2, 'Dép sandal', 'Dép sandal', '', '', ''),
(19, 1, 'Dép sandal', 'Dép sandal', '', '', ''),
(20, 2, 'Giày đúp', 'Giày đúp', '', '', ''),
(20, 1, 'Giày đúp', 'Giày đúp', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(26, 26, 0),
(25, 25, 0),
(24, 24, 0),
(23, 23, 0),
(22, 22, 0),
(21, 21, 0),
(27, 27, 0),
(28, 28, 0),
(29, 29, 0),
(30, 30, 0),
(31, 31, 0),
(32, 32, 0),
(33, 33, 0),
(12, 12, 0),
(14, 14, 0),
(17, 17, 0),
(18, 18, 0),
(19, 19, 0),
(20, 20, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(12, 0),
(14, 0),
(17, 0),
(18, 0),
(19, 0),
(20, 0),
(21, 0),
(22, 0),
(23, 0),
(24, 0),
(25, 0),
(26, 0),
(27, 0),
(28, 0),
(29, 0),
(30, 0),
(31, 0),
(32, 0),
(33, 0);