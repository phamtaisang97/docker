-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(11, 'Tranh Đá Quý Thác Nước', 0, 1, '', '0'),
(10, 'Tranh Đá Quý Làng Quê', 0, 1, '', '0'),
(9, 'Tranh Đá Quý Phong Cảnh', 0, 1, '', '0'),
(12, 'Tranh Đá Quý Động Vật', 0, 1, '', '0'),
(13, 'Tranh Đá Quý Cao Cấp', 0, 1, '', '1'),
(14, 'Tranh Đá Tứ Quý', 0, 1, '', '0'),
(15, 'Tranh Đính Đá Đồng Hồ', 0, 1, '', '0'),
(16, 'Tranh Đính Đá Hoa', 0, 1, '', '0'),
(17, 'Tranh Đá Quý Phố Cổ', 0, 1, '', '0'),
(18, 'Tranh Đá Quý Cao Cấp Bán Chạy', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, 'https://cdn.bestme.asia/images/x2/bst1_EVUPjPy.jpg', '', '', '', ''),
(10, 'https://cdn.bestme.asia/images/x2/bst3_6a0y7Wj.jpg', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/x2/bst4_PdBf8XQ.jpg', '', '', '', ''),
(12, 'https://cdn.bestme.asia/images/x2/bst2_LnSMWUP.jpg', '', '', '', ''),
(13, '', '', '', '', ''),
(14, '', '', '', '', ''),
(15, '', '', '', '', ''),
(16, '', '', '', '', ''),
(17, '', '', '', '', ''),
(18, '', '', '', '', '');