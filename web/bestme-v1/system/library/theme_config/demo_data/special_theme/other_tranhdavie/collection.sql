-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(9, 'Tranh đính đá phong cảnh', 0, 1, '', '0'),
(10, 'Tranh đính đá tôn giáo', 0, 1, '', '0'),
(11, 'Tranh đính đá gia đình', 0, 1, '', '0'),
(12, 'Tranh đính đá phong thủy', 0, 1, '', '0'),
(13, 'Tranh đính đá tứ quý-tranh bộ', 0, 1, '', '0'),
(14, 'Tranh đính đá thư pháp', 0, 1, '', '0'),
(15, 'Phong cảnh', 0, 1, '', '0'),
(16, 'Tôn giáo', 0, 1, '', '0'),
(17, 'Gia đình', 0, 1, '', '0'),
(18, 'Phong thủy', 0, 1, '', '0'),
(19, 'Tứ quý-tranh bộ', 0, 1, '', '0'),
(20, 'Thư pháp', 0, 1, '', '0'),
(21, 'Bộ sưu tập', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(18, 'https://cdn.bestme.asia/images/x2/phong-thuy.jpg', '', '', '', ''),
(17, 'https://cdn.bestme.asia/images/x2/gia-dinh.jpg', '', '', '', ''),
(16, 'https://cdn.bestme.asia/images/x2/ton-giao.jpg', '', '', '', ''),
(13, '', '', '', '', ''),
(14, '', '', '', '', ''),
(15, 'https://cdn.bestme.asia/images/x2/phong-canh.jpg', '', '', '', ''),
(10, '', '', '', '', ''),
(11, '', '', '', '', ''),
(12, '', '', '', '', ''),
(9, '', '', '', '', ''),
(19, 'https://cdn.bestme.asia/images/x2/tu-quy.jpg', '', '', '', ''),
(20, 'https://cdn.bestme.asia/images/x2/thu-phap.jpg', '', '', '', ''),
(21, '', '', '', '', '');