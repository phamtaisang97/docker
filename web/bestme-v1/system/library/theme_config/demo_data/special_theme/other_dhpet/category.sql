-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(21, NULL, 0, 0, 0, 0, 1, '2020-07-31 16:56:26', '2020-07-31 16:56:26'),
(22, NULL, 0, 0, 0, 0, 1, '2020-07-31 16:56:26', '2020-07-31 16:56:26'),
(23, NULL, 0, 0, 0, 0, 1, '2020-07-31 16:56:26', '2020-07-31 16:56:26'),
(24, NULL, 0, 0, 0, 0, 1, '2020-07-31 16:56:26', '2020-07-31 16:56:26'),
(25, NULL, 0, 0, 0, 0, 1, '2020-07-31 16:56:26', '2020-07-31 16:56:26'),
(26, NULL, 0, 0, 0, 0, 1, '2020-07-31 16:56:26', '2020-07-31 16:56:26');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(22, 2, 'Thức ăn cho mèo', 'Thức ăn cho mèo', '', '', ''),
(22, 1, 'Thức ăn cho mèo', 'Thức ăn cho mèo', '', '', ''),
(23, 2, 'Chó cảnh', 'Chó cảnh', '', '', ''),
(23, 1, 'Chó cảnh', 'Chó cảnh', '', '', ''),
(24, 2, 'Mèo cảnh', 'Mèo cảnh', '', '', ''),
(24, 1, 'Mèo cảnh', 'Mèo cảnh', '', '', ''),
(25, 2, 'Quần áo thú cưng', 'Quần áo thú cưng', '', '', ''),
(25, 1, 'Quần áo thú cưng', 'Quần áo thú cưng', '', '', ''),
(21, 1, 'Thức ăn cho chó', 'Thức ăn cho chó', '', '', ''),
(21, 2, 'Thức ăn cho chó', 'Thức ăn cho chó', '', '', ''),
(26, 2, 'Phụ kiện thú cưng', 'Phụ kiện thú cưng', '', '', ''),
(26, 1, 'Phụ kiện thú cưng', 'Phụ kiện thú cưng', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(21, 21, 0),
(22, 22, 0),
(23, 23, 0),
(24, 24, 0),
(25, 25, 0),
(26, 26, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(21, 0),
(22, 0),
(23, 0),
(24, 0),
(25, 0),
(26, 0);