-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(38, '', 0, 0, 0, 0, 1, '2020-08-24 17:36:10', '2020-08-24 17:36:10'),
(37, '', 0, 0, 0, 0, 1, '2020-08-24 17:35:58', '2020-08-24 17:35:58'),
(36, '', 0, 0, 0, 0, 1, '2020-08-24 17:34:42', '2020-08-24 17:35:08'),
(35, '', 0, 0, 0, 0, 1, '2020-08-24 17:34:27', '2020-08-24 17:35:14'),
(32, '', 0, 0, 0, 0, 1, '2020-08-24 17:26:12', '2020-08-24 17:34:53'),
(33, '', 0, 0, 0, 0, 1, '2020-08-24 17:32:21', '2020-08-24 17:35:25'),
(34, '', 0, 0, 0, 0, 1, '2020-08-24 17:34:04', '2020-08-24 17:35:20');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(38, 1, 'Dịch Vụ Khác', 'Dịch Vụ Khác', '', '', ''),
(37, 1, 'Thuốc Cho Thú Cưng', 'Thuốc Cho Thú Cưng', '', '', ''),
(38, 2, 'Dịch Vụ Khác', 'Dịch Vụ Khác', '', '', ''),
(37, 2, 'Thuốc Cho Thú Cưng', 'Thuốc Cho Thú Cưng', '', '', ''),
(36, 1, 'Phụ Kiện Cho Thú Cưng', 'Phụ Kiện Cho Thú Cưng', '', '', ''),
(32, 2, 'Thức Ăn Cho Thú Cưng', 'Thức Ăn Cho Thú Cưng', '', '', ''),
(32, 1, 'Thức Ăn Cho Thú Cưng', 'Thức Ăn Cho Thú Cưng', '', '', ''),
(33, 2, 'Thời Trang Cho Thú Cưng', 'Thời Trang Cho Thú Cưng', '', '', ''),
(33, 1, 'Thời Trang Cho Thú Cưng', 'Thời Trang Cho Thú Cưng', '', '', ''),
(34, 2, 'Chuồng Cho Thú Cưng', 'Chuồng Cho Thú Cưng', '', '', ''),
(34, 1, 'Chuồng Cho Thú Cưng', 'Chuồng Cho Thú Cưng', '', '', ''),
(35, 2, 'Đồ Chơi Cho Thú Cưng', 'Đồ Chơi Cho Thú Cưng', '', '', ''),
(35, 1, 'Đồ Chơi Cho Thú Cưng', 'Đồ Chơi Cho Thú Cưng', '', '', ''),
(36, 2, 'Phụ Kiện Cho Thú Cưng', 'Phụ Kiện Cho Thú Cưng', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(35, 35, 0),
(34, 34, 0),
(33, 33, 0),
(32, 32, 0),
(37, 37, 0),
(36, 36, 0),
(38, 38, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0);