-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(33, '1', 0, 1, '', '0'),
(24, 'Chuồng Cho Thú Cưng', 0, 1, '', '0'),
(23, 'Phụ Kiện Cho Thú Cưng', 0, 1, '', '0'),
(22, 'Thuốc Cho Thú Cưng', 0, 1, '', '0'),
(21, 'Đồ Chơi Cho Thú Cưng', 0, 1, '', '0'),
(20, 'Thức Ăn Cho Thú Cưng', 0, 1, '', '0'),
(19, 'Quần Áo Cho Thú Cưng', 0, 1, '', '0'),
(18, 'Dịch Vụ Cho Thú Cưng', 0, 1, '', '0'),
(34, '2', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(18, '', '', '', '', ''),
(19, '', '', '', '', ''),
(20, '', '', '', '', ''),
(21, '', '', '', '', ''),
(22, 'https://cdn.bestme.asia/images/x2/thiet-ke-khong-ten-7-removebg-preview.png', '', '', '', ''),
(23, 'https://cdn.bestme.asia/images/x2/thiet-ke-khong-ten-3-removebg-preview.png', '', '', '', ''),
(24, 'https://cdn.bestme.asia/images/x2/free-delivery-luxury-pet-house-spot-double-top-puppy-dog-cat-warm-kennel-sleep-bed-room-mat-shop-now-wdx1-xhj028159840979629.png', '', '', '', ''),
(33, '', '', '', '', ''),
(34, '', '', '', '', '');