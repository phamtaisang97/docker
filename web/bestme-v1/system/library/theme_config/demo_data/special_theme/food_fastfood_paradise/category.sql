-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(42, '', 0, 0, 0, 0, 1, '2021-01-07 09:22:10', '2021-01-07 10:34:28'),
(41, '', 0, 0, 0, 0, 1, '2021-01-07 09:21:29', '2021-01-07 10:34:59'),
(40, '', 0, 0, 0, 0, 1, '2021-01-07 09:20:58', '2021-01-07 09:21:22'),
(39, '', 34, 0, 0, 0, 1, '2021-01-07 09:06:58', '2021-01-07 09:06:58'),
(38, '', 34, 0, 0, 0, 1, '2021-01-07 09:06:58', '2021-01-07 09:06:58'),
(37, '', 34, 0, 0, 0, 1, '2021-01-07 09:06:58', '2021-01-07 09:06:58'),
(35, '', 0, 0, 0, 0, 1, '2021-01-06 16:15:43', '2021-01-06 16:15:43'),
(34, '', 0, 0, 0, 0, 1, '2021-01-06 16:12:02', '2021-01-07 09:06:58'),
(36, '', 0, 0, 0, 0, 1, '2021-01-06 16:17:21', '2021-01-07 09:20:32'),
(32, '', 0, 0, 0, 0, 1, '2021-01-06 16:09:32', '2021-01-07 09:19:34'),
(33, '', 0, 0, 0, 0, 1, '2021-01-06 16:10:31', '2021-01-06 16:10:31'),
(43, '', 0, 0, 0, 0, 1, '2021-01-07 11:23:22', '2021-01-07 11:23:22');


-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(43, 1, 'Desserts', 'Desserts', '', '', ''),
(43, 2, 'Desserts', 'Desserts', '', '', ''),
(42, 1, 'Milk and Cookies', 'Milk and Cookies', '', '', ''),
(41, 1, 'Drinks and Ice Blends', 'Drinks and Ice Blends', '', '', ''),
(42, 2, 'Milk and Cookies', 'Milk and Cookies', '', '', ''),
(40, 1, 'Kid\'s Meals', 'Kid\'s Meals', '', '', ''),
(41, 2, 'Drinks and Ice Blends', 'Drinks and Ice Blends', '', '', ''),
(39, 1, 'Spicy Crispy Chicken', 'Spicy Crispy Chicken', '', '', ''),
(40, 2, 'Kid\'s Meals', 'Kid\'s Meals', '', '', ''),
(38, 1, 'Crispy Chicken', 'Crispy Chicken', '', '', ''),
(39, 2, 'Spicy Crispy Chicken', 'Spicy Crispy Chicken', '', '', ''),
(32, 1, 'Happy Combo', 'Happy Combo', '', '', ''),
(33, 2, 'Salads', 'Salads', '', '', ''),
(33, 1, 'Salads', 'Salads', '', '', ''),
(34, 2, 'Fried Chicken', 'Fried Chicken', '', '', ''),
(34, 1, 'Fried Chicken', 'Fried Chicken', '', '', ''),
(35, 2, 'Breakfast', 'Breakfast', '', '', ''),
(35, 1, 'Breakfast', 'Breakfast', '', '', ''),
(36, 2, 'Entrees', 'Entrees', '', '', ''),
(36, 1, 'Entrees', 'Entrees', '', '', ''),
(37, 2, 'Fried Chicken With Sauce', 'Fried Chicken With Sauce', '', '', ''),
(37, 1, 'Fried Chicken With Sauce', 'Fried Chicken With Sauce', '', '', ''),
(38, 2, 'Crispy Chicken', 'Crispy Chicken', '', '', ''),
(32, 2, 'Happy Combo', 'Happy Combo', '', '', '');


-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(43, 43, 0),
(34, 34, 0),
(33, 33, 0),
(36, 36, 0),
(35, 35, 0),
(42, 42, 0),
(41, 41, 0),
(32, 32, 0),
(37, 34, 0),
(40, 40, 0),
(39, 39, 1),
(39, 34, 0),
(38, 38, 1),
(38, 34, 0),
(37, 37, 1);


-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(39, 0),
(40, 0),
(41, 0),
(42, 0),
(43, 0);
