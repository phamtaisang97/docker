-- oc_seo_url PRIMARY KEY (`seo_url_id`)
INSERT IGNORE INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`, `table_name`) VALUES
(2260, 0, 2, 'product_id=145', '2-mieng-ga-gion-sot-cay-khoai-tay-nuoc', 'product'),
(2269, 0, 1, 'product_id=144', '2-mieng-ga-gion-vui-ve', 'product'),
(2268, 0, 2, 'product_id=144', '2-mieng-ga-gion-vui-ve', 'product'),
(2257, 0, 1, 'product_id=143', 'grilled-chicken-sandwich', 'product'),
(2256, 0, 2, 'product_id=143', 'grilled-chicken-sandwich', 'product'),
(2255, 0, 1, 'product_id=142', 'spicy-deluxe-sandwich', 'product'),
(2254, 0, 2, 'product_id=142', 'spicy-deluxe-sandwich', 'product'),
(2253, 0, 1, 'product_id=141', 'spicy-chicken-sandwich', 'product'),
(2252, 0, 2, 'product_id=141', 'spicy-chicken-sandwich', 'product'),
(2251, 0, 1, 'product_id=140', 'deluxe-sandwich', 'product'),
(2250, 0, 2, 'product_id=140', 'deluxe-sandwich', 'product'),
(2245, 0, 1, 'product_id=139', 'english-muffin', 'product'),
(2244, 0, 2, 'product_id=139', 'english-muffin', 'product'),
(2243, 0, 1, 'product_id=138', 'hash-browns', 'product'),
(2242, 0, 2, 'product_id=138', 'hash-browns', 'product'),
(2241, 0, 1, 'product_id=137', 'sunflower-multigrain-bagel', 'product'),
(2240, 0, 2, 'product_id=137', 'sunflower-multigrain-bagel', 'product'),
(2239, 0, 1, 'product_id=136', 'buttered-biscuit', 'product'),
(2238, 0, 2, 'product_id=136', 'buttered-biscuit', 'product'),
(2384, 0, 1, 'menu_id=6', 'menu-bo-suu-tap', ''),
(2383, 0, 2, 'menu_id=6', 'menu-bo-suu-tap', ''),
(2117, 0, 2, 'manufacturer_id=26', '', 'manufacturer'),
(2118, 0, 1, 'manufacturer_id=26', '', 'manufacturer'),
(2386, 0, 1, 'menu_id=22', 'menu-dau-trang', ''),
(2380, 0, 2, 'menu_id=7', 'menu-chan-trang', ''),
(2381, 0, 1, 'menu_id=7', 'menu-chan-trang', ''),
(2382, 0, 2, 'blog_category_id=2', 'food-recipes', 'blog_category'),
(2385, 0, 2, 'menu_id=22', 'menu-dau-trang', ''),
(2377, 0, 1, 'category_id=43', 'desserts', 'category'),
(2378, 0, 2, 'blog=5', 'smoothies-la-gi-cac-cong-thuc-smoothies-giam-can-dep-da-don-gian', 'blog'),
(2379, 0, 2, 'blog=6', 'milkshake-la-gi-tong-hop-cac-loai-milkshake-ngon-giu-chan-khach', 'blog'),
(2376, 0, 2, 'category_id=43', 'desserts', 'category'),
(2373, 0, 2, 'collection=9', 'san-pham-moi', 'collection'),
(2374, 0, 2, 'collection=10', 'san-pham-ban-chay', 'collection'),
(2375, 0, 2, 'collection=11', 'san-pham-khuyen-mai', 'collection'),
(2354, 0, 1, 'product_id=165', 'peppermint-chip-milkshake', 'product'),
(2237, 0, 1, 'product_id=135', 'chicken-egg-amp-cheese-bagel', 'product'),
(2235, 0, 1, 'product_id=134', 'bacon-egg-amp-cheese-biscuit', 'product'),
(2362, 0, 1, 'product_id=162', 'frosted-coffee', 'product'),
(2361, 0, 2, 'product_id=162', 'frosted-coffee', 'product'),
(2366, 0, 1, 'product_id=163', 'freshly-brewed-iced-tea-sweetened', 'product'),
(2261, 0, 1, 'product_id=145', '2-mieng-ga-gion-sot-cay-khoai-tay-nuoc', 'product'),
(2314, 0, 1, 'category_id=32', 'happy-combo', 'category'),
(2313, 0, 2, 'category_id=32', 'happy-combo', 'category'),
(2271, 0, 1, 'product_id=147', '2-mieng-ga-sot-cay', 'product'),
(2270, 0, 2, 'product_id=147', '2-mieng-ga-sot-cay', 'product'),
(2266, 0, 2, 'product_id=148', 'ga-chien-vang', 'product'),
(2267, 0, 1, 'product_id=148', 'ga-chien-vang', 'product'),
(2272, 0, 2, 'product_id=149', 'ga-rut-xuong-chien-gion', 'product'),
(2273, 0, 1, 'product_id=149', 'ga-rut-xuong-chien-gion', 'product'),
(2370, 0, 1, 'product_id=150', 'com-ga-gion-cay-sup-ga-nuoc-ngot', 'product'),
(2369, 0, 2, 'product_id=150', 'com-ga-gion-cay-sup-ga-nuoc-ngot', 'product'),
(2372, 0, 1, 'product_id=151', 'com-ga-ran-nuoc-ngot', 'product'),
(2371, 0, 2, 'product_id=151', 'com-ga-ran-nuoc-ngot', 'product'),
(2278, 0, 2, 'product_id=152', '1-mieng-ga-sot-cay-com-sup-nuoc', 'product'),
(2279, 0, 1, 'product_id=152', '1-mieng-ga-sot-cay-com-sup-nuoc', 'product'),
(2282, 0, 2, 'product_id=153', 'spicy-southwest-salad', 'product'),
(2283, 0, 1, 'product_id=153', 'spicy-southwest-salad', 'product'),
(2284, 0, 2, 'product_id=154', 'cobb-salad', 'product'),
(2285, 0, 1, 'product_id=154', 'cobb-salad', 'product'),
(2286, 0, 2, 'product_id=155', 'market-salad', 'product'),
(2287, 0, 1, 'product_id=155', 'market-salad', 'product'),
(2288, 0, 2, 'product_id=156', 'side-salad', 'product'),
(2289, 0, 1, 'product_id=156', 'side-salad', 'product'),
(2292, 0, 2, 'category_id=33', 'salads', 'category'),
(2293, 0, 1, 'category_id=33', 'salads', 'category'),
(2353, 0, 2, 'product_id=165', 'peppermint-chip-milkshake', 'product'),
(2312, 0, 1, 'category_id=34', 'fried-chicken', 'category'),
(2311, 0, 2, 'category_id=34', 'fried-chicken', 'category'),
(2298, 0, 2, 'category_id=35', 'breakfast', 'category'),
(2299, 0, 1, 'category_id=35', 'breakfast', 'category'),
(2316, 0, 1, 'category_id=36', 'entrees', 'category'),
(2315, 0, 2, 'category_id=36', 'entrees', 'category'),
(2306, 0, 1, 'category_id=37', 'fried-chicken-with-sauce', 'category'),
(2307, 0, 2, 'category_id=38', 'crispy-chicken', 'category'),
(2308, 0, 1, 'category_id=38', 'crispy-chicken', 'category'),
(2309, 0, 2, 'category_id=39', 'spicy-crispy-chicken', 'category'),
(2310, 0, 1, 'category_id=39', 'spicy-crispy-chicken', 'category'),
(2356, 0, 1, 'product_id=164', 'strawberry-milkshake', 'product'),
(2319, 0, 2, 'category_id=40', 'kids-meals', 'category'),
(2320, 0, 1, 'category_id=40', 'kids-meals', 'category'),
(2343, 0, 2, 'category_id=41', 'drinks-and-ice-blends', 'category'),
(2344, 0, 1, 'category_id=41', 'drinks-and-ice-blends', 'category'),
(2342, 0, 1, 'category_id=42', 'milk-and-cookies', 'category'),
(2341, 0, 2, 'category_id=42', 'milk-and-cookies', 'category'),
(2332, 0, 1, 'product_id=157', 'hash-brown-scramble-bowl', 'product'),
(2331, 0, 2, 'product_id=157', 'hash-brown-scramble-bowl', 'product'),
(2329, 0, 2, 'product_id=158', 'hash-brown-scramble-burrito', 'product'),
(2330, 0, 1, 'product_id=158', 'hash-brown-scramble-burrito', 'product'),
(2333, 0, 2, 'product_id=159', 'chick-n-minis', 'product'),
(2334, 0, 1, 'product_id=159', 'chick-n-minis', 'product'),
(2355, 0, 2, 'product_id=164', 'strawberry-milkshake', 'product'),
(2337, 0, 2, 'product_id=160', 'grilled-nuggets-kids-meal', 'product'),
(2338, 0, 1, 'product_id=160', 'grilled-nuggets-kids-meal', 'product'),
(2339, 0, 2, 'product_id=161', 'chicken-noodle-soup', 'product'),
(2340, 0, 1, 'product_id=161', 'chicken-noodle-soup', 'product'),
(2365, 0, 2, 'product_id=163', 'freshly-brewed-iced-tea-sweetened', 'product'),
(2236, 0, 2, 'product_id=135', 'chicken-egg-amp-cheese-bagel', 'product'),
(2230, 0, 2, 'product_id=132', 'banh-quy-ga', 'product'),
(2231, 0, 1, 'product_id=132', 'banh-quy-ga', 'product'),
(2232, 0, 2, 'product_id=133', 'egg-white-grill', 'product'),
(2233, 0, 1, 'product_id=133', 'egg-white-grill', 'product'),
(2234, 0, 2, 'product_id=134', 'bacon-egg-amp-cheese-biscuit', 'product'),
(2304, 0, 2, 'blog=4', 'canh-ga-chien-9-cong-thuc-don-gian-ma-ngon-cuc-ky', 'blog'),
(2305, 0, 2, 'category_id=37', 'fried-chicken-with-sauce', 'category');
