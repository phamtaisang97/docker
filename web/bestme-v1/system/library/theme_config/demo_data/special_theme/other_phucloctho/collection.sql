-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(12, 'Quà Tặng Bằng Đồng', 0, 1, '', '0'),
(11, 'Tranh Đồng', 0, 1, '', '0'),
(10, 'Trống Đồng', 0, 1, '', '0'),
(9, 'Tượng Đồng', 0, 1, '', '0'),
(13, 'Đồ Thờ Cúng', 0, 1, '', '0'),
(14, 'Đồ Đồng Phong Thuỷ', 0, 1, '', '0'),
(15, 'Đồ Đồng Cao Cấp', 0, 1, '', '0'),
(16, 'Chuông Đồng', 0, 1, '', '0'),
(17, '1', 0, 1, '', '1'),
(18, '2', 0, 1, '', '1'),
(19, '3', 0, 1, '', '1'),
(20, '4', 0, 1, '', '1'),
(21, 'Sản Phẩm Mới', 0, 1, '', '0'),
(22, 'Sản Phẩm Bán Chạy', 0, 1, '', '0'),
(23, 'Sản Phẩm Khuyến Mại', 0, 1, '', '0');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, 'https://cdn.bestme.asia/images/x2/tuong-dong-100.jpg', '', '', '', ''),
(10, 'https://cdn.bestme.asia/images/x2/trong-dong-100.jpg', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/x2/tranh-dong-100.jpg', '', '', '', ''),
(12, 'https://cdn.bestme.asia/images/x2/qua-tang-bang-dong-100.jpg', '', '', '', ''),
(13, 'https://cdn.bestme.asia/images/x2/do-tho-100.jpg', '', '', '', ''),
(14, 'https://cdn.bestme.asia/images/x2/do-phong-thuy-100.jpg', '', '', '', ''),
(15, 'https://cdn.bestme.asia/images/x2/do-cao-cap-100.jpg', '', '', '', ''),
(16, 'https://cdn.bestme.asia/images/x2/chuong-dong-100.jpg', '', '', '', ''),
(17, '', '', '', '', ''),
(18, '', '', '', '', ''),
(19, '', '', '', '', ''),
(20, '', '', '', '', ''),
(21, '', '', '', '', ''),
(22, '', '', '', '', ''),
(23, '', '', '', '', '');