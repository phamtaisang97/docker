-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(11, 'Tranh đính đá tôn giáo', 0, 1, '', '0'),
(10, 'Tranh đính đá thư pháp', 0, 1, '', '0'),
(9, 'Tranh đính đá phong cảnh', 0, 1, '', '0'),
(12, 'Tranh đính đá gia đình', 0, 1, '', '0'),
(13, 'Tranh đính đá phong thủy', 0, 1, '', '0'),
(14, 'Tranh đính đá tứ quý-tranh bộ', 0, 1, '', '0'),
(15, 'Phong cảnh', 0, 1, '', '0'),
(16, 'Tôn giáo', 0, 1, '', '0'),
(17, 'Gia đình', 0, 1, '', '0'),
(18, 'Phong thủy', 0, 1, '', '0'),
(19, 'Tứ quý', 0, 1, '', '0'),
(20, 'Thư pháp', 0, 1, '', '0'),
(21, 'Bộ sưu tập', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(9, '', '', '', '', ''),
(10, '', '', '', '', ''),
(11, '', '', '', '', ''),
(12, '', '', '', '', ''),
(13, '', '', '', '', ''),
(14, '', '', '', '', ''),
(15, 'https://cdn.bestme.asia/images/x2/1-100_QwMbR5R.jpg', '', '', '', ''),
(16, 'https://cdn.bestme.asia/images/x2/4-100_gX279ye.jpg', '', '', '', ''),
(17, 'https://cdn.bestme.asia/images/x2/5-100.jpg', '', '', '', ''),
(18, 'https://cdn.bestme.asia/images/x2/2-100_em1SjIz.jpg', '', '', '', ''),
(19, 'https://cdn.bestme.asia/images/x2/3-100_SXMAM3a.jpg', '', '', '', ''),
(20, 'https://cdn.bestme.asia/images/x2/6-100.jpg', '', '', '', ''),
(21, '', '', '', '', '');