-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(20, '', 0, 0, 0, 0, 1, '2019-05-21 16:20:38', '2020-06-09 20:28:48'),
(19, NULL, 0, 0, 0, 0, 1, '2019-05-21 15:47:12', '2019-05-21 15:47:12'),
(18, NULL, 0, 0, 0, 0, 1, '2019-05-21 15:44:23', '2019-05-21 15:44:23'),
(17, NULL, 0, 0, 0, 0, 1, '2019-05-21 15:36:21', '2019-05-21 15:36:21'),
(14, NULL, 0, 0, 0, 0, 1, '2019-05-20 17:45:50', '2019-05-20 17:45:50'),
(12, NULL, 0, 0, 0, 0, 1, '2019-05-20 16:48:18', '2019-05-20 16:48:18'),
(21, NULL, 0, 0, 0, 0, 1, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(22, NULL, 0, 0, 0, 0, 1, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(23, '', 0, 0, 0, 0, 1, '2020-06-09 17:50:59', '2020-06-09 20:29:17'),
(24, NULL, 0, 0, 0, 0, 1, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(25, NULL, 0, 0, 0, 0, 1, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(26, NULL, 0, 0, 0, 0, 1, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(27, NULL, 0, 0, 0, 0, 1, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(28, '', 23, 0, 0, 0, 1, '2020-06-09 20:29:17', '2020-06-09 20:29:57'),
(29, '', 23, 0, 0, 0, 1, '2020-06-09 20:29:17', '2020-06-09 20:29:17'),
(30, '', 28, 0, 0, 0, 1, '2020-06-09 20:29:57', '2020-06-09 20:29:57'),
(31, '', 28, 0, 0, 0, 1, '2020-06-09 20:29:57', '2020-06-09 20:29:57');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(18, 2, 'Giày sandal', 'Giày sandal', '', '', ''),
(18, 1, 'Giày sandal', 'Giày sandal', '', '', ''),
(17, 2, 'Giày valen', 'Giày valen', '', '', ''),
(17, 1, 'Giày valen', 'Giày valen', '', '', ''),
(14, 1, 'Giày cao gót', 'Giày cao gót', '', '', ''),
(14, 2, 'Giày cao gót', 'Giày cao gót', '', '', ''),
(12, 1, 'Giày cổ điển', 'Giày cổ điển', '', '', ''),
(12, 2, 'Giày cổ điển', 'Giày cổ điển', '', '', ''),
(21, 2, 'Váy dự tiệc', 'Váy dự tiệc', '', '', ''),
(21, 1, 'Váy dự tiệc', 'Váy dự tiệc', '', '', ''),
(22, 2, 'Váy đầm xòe', 'Váy đầm xòe', '', '', ''),
(22, 1, 'Váy đầm xòe', 'Váy đầm xòe', '', '', ''),
(23, 2, 'Váy dáng suông', 'Váy dáng suông', '', '', ''),
(23, 1, 'Váy dáng suông', 'Váy dáng suông', '', '', ''),
(24, 2, 'Váy đầm sơ mi', 'Váy đầm sơ mi', '', '', ''),
(24, 1, 'Váy đầm sơ mi', 'Váy đầm sơ mi', '', '', ''),
(25, 2, 'Váy đầm xòe họa tiết', 'Váy đầm xòe họa tiết', '', '', ''),
(25, 1, 'Váy đầm xòe họa tiết', 'Váy đầm xòe họa tiết', '', '', ''),
(26, 2, 'Váy đầm lụa đuôi cá', 'Váy đầm lụa đuôi cá', '', '', ''),
(26, 1, 'Váy đầm lụa đuôi cá', 'Váy đầm lụa đuôi cá', '', '', ''),
(27, 2, 'Váy đầm công sở', 'Váy đầm công sở', '', '', ''),
(27, 1, 'Váy đầm công sở', 'Váy đầm công sở', '', '', ''),
(19, 2, 'Dép sandal', 'Dép sandal', '', '', ''),
(19, 1, 'Dép sandal', 'Dép sandal', '', '', ''),
(20, 2, 'Giày búp bê', 'Giày búp bê', '', '', ''),
(20, 1, 'Giày búp bê', 'Giày búp bê', '', '', ''),
(28, 2, 'Váy công sở', 'Váy công sở', '', '', ''),
(28, 1, 'Váy công sở', 'Váy công sở', '', '', ''),
(29, 2, 'Váy dạ tiệc', 'Váy dạ tiệc', '', '', ''),
(29, 1, 'Váy dạ tiệc', 'Váy dạ tiệc', '', '', ''),
(30, 2, 'Váy ngắn', 'Váy ngắn', '', '', ''),
(30, 1, 'Váy ngắn', 'Váy ngắn', '', '', ''),
(31, 2, 'Váy dài', 'Váy dài', '', '', ''),
(31, 1, 'Váy dài', 'Váy dài', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(20, 20, 0),
(19, 19, 0),
(18, 18, 0),
(17, 17, 0),
(14, 14, 0),
(12, 12, 0),
(21, 21, 0),
(22, 22, 0),
(23, 23, 0),
(24, 24, 0),
(25, 25, 0),
(26, 26, 0),
(27, 27, 0),
(28, 23, 0),
(28, 28, 1),
(29, 23, 0),
(29, 29, 1),
(30, 23, 0),
(30, 28, 1),
(30, 30, 2),
(31, 23, 0),
(31, 28, 1),
(31, 31, 2);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(12, 0),
(14, 0),
(17, 0),
(18, 0),
(19, 0),
(20, 0),
(21, 0),
(22, 0),
(23, 0),
(24, 0),
(25, 0),
(26, 0),
(27, 0),
(28, 0),
(29, 0),
(30, 0),
(31, 0);