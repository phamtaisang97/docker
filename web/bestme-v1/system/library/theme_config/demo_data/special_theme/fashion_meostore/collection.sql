-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(13, 'Sleepwear &amp; Lounge', 0, 1, '', '0'),
(9, 'TH', 0, 1, '', '0'),
(10, 'BRAS', 0, 1, '', '0'),
(11, 'Bralettes', 0, 1, '', '0'),
(12, 'Underwear', 0, 1, '', '0'),
(14, 'TH1', 0, 1, '', '1'),
(15, 'TH2', 0, 1, '', '1');

-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(15, '', '', '', '', ''),
(13, 'https://cdn.bestme.asia/images/x2/bst3.jpg', '', '', '', ''),
(14, '', '', '', '', ''),
(12, 'https://cdn.bestme.asia/images/x2/bst2.jpg', '', '', '', ''),
(9, '', '', '', '', ''),
(10, 'https://cdn.bestme.asia/images/x2/bst4.jpg', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/x2/bst1.jpg', '', '', '', '');