-- oc_blog PRIMARY KEY (`blog_id`)
INSERT IGNORE INTO `oc_blog` (`blog_id`, `author`, `status`, `demo`, `date_publish`, `date_added`, `date_modified`) VALUES
(4, 1, 1, 1, '2020-08-18 17:07:29', '2020-08-18 17:07:29', '2020-08-18 17:27:06'),
(5, 1, 1, 1, '2020-08-18 17:07:55', '2020-08-18 17:07:55', '2020-08-18 17:27:31'),
(6, 1, 1, 1, '2020-08-18 17:08:22', '2020-08-18 17:08:22', '2020-08-18 17:27:20');

-- oc_blog_category PRIMARY KEY (`blog_category_id`)
INSERT IGNORE INTO `oc_blog_category` (`blog_category_id`, `status`, `date_added`, `date_modified`) VALUES
(1, 1, '2019-11-11 13:37:51', '2020-08-18 17:08:30');

-- oc_blog_category_description PRIMARY KEY (`blog_category_id`,`language_id`)
INSERT IGNORE INTO `oc_blog_category_description` (`blog_category_id`, `language_id`, `title`, `meta_title`, `meta_description`, `alias`) VALUES
(1, 1, 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'danh-muc-mac-dinh-uncategorized'),
(1, 2, 'Blog', 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'danh-muc-mac-dinh-uncategorized');

-- oc_blog_description PRIMARY KEY (`blog_id`,`language_id`)
INSERT IGNORE INTO `oc_blog_description` (`blog_id`, `language_id`, `title`, `content`, `short_content`, `image`, `meta_title`, `meta_description`, `seo_keywords`, `alias`, `alt`, `type`, `video_url`) VALUES
(4, 2, 'We are so lucky living in such a wonderful time', '&lt;p&gt;We are so lucky living in such a wonderful time Our society doesn\'t have such strict foundations as there were hundred years ago. It is so good that we have an opportunity to make a choice. Lingerie wasn\'t always so stylish and erotic. Actually the concept of lingerie being visually appealing was a development of the late 19th century.Nowadays the lingerie industry is one of the most successful business spheres. The magic of the lingerie is an undoubted fact because...&lt;/p&gt;', '&lt;p&gt;We are so lucky living in such a wonderful time Our society doesn\'t have such strict foundations as there were hundred years ago. It is so good that we have an opportunity to make a choice. Lingerie wasn\'t always so stylish and erotic. Actually the concept of lingerie being visually appealing was a development of the late 19th century.Nowadays the lingerie industry is one of the most successful business spheres. The magic of the lingerie is an undoubted fact because...&lt;/p&gt;', 'https://cdn.bestme.asia/images/x2/3_4UyfZh6.png', '', '', NULL, 'we-are-so-lucky-living-in-such-a-wonderful-time', NULL, 'image', NULL),
(5, 2, 'Lingerie wasn’t always so stylish and erotic', '&lt;p&gt;Lingerie wasn’t always so stylish and erotic We care about our clients and never let them down. We often provide different promos and you can get a good benefit. If you have some questions you can address our superb 24/7 online support system. We are so lucky living in such a wonderful time. Our almost unlimited abilities give us a real freedom. Our society doesn\'t have such strict foundations as there were hundred years ago. Nowadays the lingerie industry is...&lt;/p&gt;', '&lt;p&gt;Lingerie wasn’t always so stylish and erotic We care about our clients and never let them down. We often provide different promos and you can get a good benefit. If you have some questions you can address our superb 24/7 online support system. We are so lucky living in such a wonderful time. Our almost unlimited abilities give us a real freedom. Our society doesn\'t have such strict foundations as there were hundred years ago. Nowadays the lingerie industry is...&lt;/p&gt;', 'https://cdn.bestme.asia/images/x2/2_oED5rwl.png', '', '', NULL, 'lingerie-wasnt-always-so-stylish-and-erotic', NULL, 'image', NULL),
(6, 2, 'Sexy lingerie', '&lt;p&gt;Sexy lingerie We always stay in touch with the latest fashion tendencies - that is why our goods are so popular and we have a great number of faithful customers all over the country. The magic of the lingerie is an undoubted fact because it is a way of self expression and it gives a pleasure and satisfaction. We often provide different promos and you can get a good benefit. Panties  Bras  Sexy lingerie  Collection We are so lucky living...&lt;/p&gt;', '&lt;p&gt;Sexy lingerie We always stay in touch with the latest fashion tendencies - that is why our goods are so popular and we have a great number of faithful customers all over the country. The magic of the lingerie is an undoubted fact because it is a way of self expression and it gives a pleasure and satisfaction. We often provide different promos and you can get a good benefit. Panties  Bras  Sexy lingerie  Collection We are so lucky living...&lt;/p&gt;', 'https://cdn.bestme.asia/images/x2/1.png', '', '', NULL, 'sexy-lingerie', NULL, 'image', NULL);

-- oc_blog_to_blog_category NO PRIMARY KEY
-- remove first
DELETE FROM `oc_blog_to_blog_category`
WHERE `blog_id` IN (4, 5, 6) AND `blog_category_id` = 1;

-- insert
INSERT IGNORE INTO `oc_blog_to_blog_category` (`blog_id`, `blog_category_id`) VALUES
(6, 1),
(5, 1),
(4, 1);