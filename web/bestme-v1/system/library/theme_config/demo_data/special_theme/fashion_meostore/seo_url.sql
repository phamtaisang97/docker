-- oc_seo_url PRIMARY KEY (`seo_url_id`)
INSERT IGNORE INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`, `table_name`) VALUES
-- (1000, 0, 1, 'page_id=1000', 'chinh-sach-va-bao-mat', ''),
-- (1001, 0, 2, 'page_id=1000', 'chinh-sach-va-bao-mat', ''),
-- (2204, 0, 1, 'common/home', 'home', ''),
-- (2205, 0, 2, 'common/home', 'trang-chu', ''),
-- (2206, 0, 1, 'common/shop', 'products', ''),
-- (2207, 0, 2, 'common/shop', 'san-pham', ''),
-- (2208, 0, 1, 'contact/contact', 'contact', ''),
-- (2209, 0, 2, 'contact/contact', 'lien-he', ''),
-- (2210, 0, 1, 'checkout/profile', 'profile', ''),
-- (2211, 0, 2, 'checkout/profile', 'tai-khoan', ''),
-- (2212, 0, 1, 'account/login', 'login', ''),
-- (2213, 0, 2, 'account/login', 'dang-nhap', ''),
-- (2214, 0, 1, 'account/register', 'register', ''),
-- (2215, 0, 2, 'account/register', 'dang-ky', ''),
-- (2216, 0, 1, 'account/logout', 'logout', ''),
-- (2217, 0, 2, 'account/logout', 'dang-xuat', ''),
-- (2218, 0, 1, 'checkout/setting', 'setting', ''),
-- (2219, 0, 2, 'checkout/setting', 'cai-dat', ''),
-- (2220, 0, 1, 'checkout/my_orders', 'checkout-cart', ''),
-- (2221, 0, 2, 'checkout/my_orders', 'gio-hang', ''),
-- (2222, 0, 1, 'checkout/order_preview', 'payment', ''),
-- (2223, 0, 2, 'checkout/order_preview', 'thanh-toan', ''),
-- (2224, 0, 1, 'checkout/order_preview/orderSuccess', 'payment-success', ''),
-- (2225, 0, 2, 'checkout/order_preview/orderSuccess', 'dat-hang-thanh-cong', ''),
-- (2226, 0, 1, 'blog/blog', 'blog', ''),
-- (2227, 0, 2, 'blog/blog', 'bai-viet', ''),
(2083, 0, 2, 'category_id=12', 'd-cat-giay-co-dien', 'category'),
(2193, 0, 1, 'menu_id=6', 'menu-bo-suu-tap', ''),
(2192, 0, 2, 'menu_id=6', 'menu-bo-suu-tap', ''),
(2250, 0, 2, 'collection=9', 'th', 'collection'),
-- (2093, 0, 1, 'common/home', 'home', ''),
-- (2094, 0, 2, 'common/home', 'trang-chu', ''),
-- (2095, 0, 1, 'common/shop', 'products', ''),
-- (2096, 0, 2, 'common/shop', 'san-pham', ''),
-- (2097, 0, 1, 'contact/contact', 'contact', ''),
-- (2098, 0, 2, 'contact/contact', 'lien-he', ''),
-- (2099, 0, 1, 'checkout/profile', 'profile', ''),
-- (2100, 0, 2, 'checkout/profile', 'tai-khoan', ''),
-- (2101, 0, 1, 'account/login', 'login', ''),
-- (2102, 0, 2, 'account/login', 'dang-nhap', ''),
-- (2103, 0, 1, 'account/register', 'register', ''),
-- (2104, 0, 2, 'account/register', 'dang-ky', ''),
-- (2105, 0, 1, 'account/logout', 'logout', ''),
-- (2106, 0, 2, 'account/logout', 'dang-xuat', ''),
-- (2107, 0, 1, 'checkout/setting', 'setting', ''),
-- (2108, 0, 2, 'checkout/setting', 'cai-dat', ''),
-- (2109, 0, 1, 'checkout/my_orders', 'checkout-cart', ''),
-- (2110, 0, 2, 'checkout/my_orders', 'gio-hang', ''),
-- (2111, 0, 1, 'checkout/order_preview', 'payment', ''),
-- (2112, 0, 2, 'checkout/order_preview', 'thanh-toan', ''),
-- (2113, 0, 1, 'checkout/order_preview/orderSuccess', 'payment-success', ''),
-- (2114, 0, 2, 'checkout/order_preview/orderSuccess', 'dat-hang-thanh-cong', ''),
-- (2115, 0, 1, 'blog/blog', 'blog', ''),
-- (2116, 0, 2, 'blog/blog', 'bai-viet', ''),
(2117, 0, 2, 'manufacturer_id=26', '', 'manufacturer'),
(2118, 0, 1, 'manufacturer_id=26', '', 'manufacturer'),
(2119, 0, 2, 'category_id=21', 'vay-du-tiec', 'category'),
(2120, 0, 1, 'category_id=21', 'vay-du-tiec', 'category'),
(2123, 0, 2, 'category_id=22', 'vay-dam-xoe', 'category'),
(2124, 0, 1, 'category_id=22', 'vay-dam-xoe', 'category'),
(2196, 0, 2, 'category_id=23', 'vay-dang-suong-1', 'category'),
(2131, 0, 2, 'category_id=24', 'vay-dam-so-mi', 'category'),
(2132, 0, 1, 'category_id=24', 'vay-dam-so-mi', 'category'),
(2135, 0, 2, 'category_id=25', 'vay-dam-xoe-hoa-tiet', 'category'),
(2136, 0, 1, 'category_id=25', 'vay-dam-xoe-hoa-tiet', 'category'),
(2139, 0, 2, 'category_id=26', 'vay-dam-lua-duoi-ca', 'category'),
(2140, 0, 1, 'category_id=26', 'vay-dam-lua-duoi-ca', 'category'),
(2143, 0, 2, 'category_id=27', 'vay-dam-cong-so', 'category'),
(2144, 0, 1, 'category_id=27', 'vay-dam-cong-so', 'category'),
(2084, 0, 2, 'category_id=14', 'd-cat-giay-cao-got', 'category'),
(2085, 0, 2, 'category_id=17', 'd-cat-giay-valen', 'category'),
(2086, 0, 2, 'category_id=18', 'd-cat-giay-sandal', 'category'),
(2087, 0, 2, 'category_id=19', 'd-cat-dep-sandal', 'category'),
(2194, 0, 2, 'category_id=20', 'giay-bup-be', 'category'),
(2195, 0, 1, 'category_id=20', 'giay-bup-be', 'category'),
(2197, 0, 1, 'category_id=23', 'vay-dang-suong-1', 'category'),
(2198, 0, 2, 'category_id=28', 'vay-cong-so', 'category'),
(2199, 0, 1, 'category_id=28', 'vay-cong-so', 'category'),
(2289, 0, 2, 'blog=4', 'we-are-so-lucky-living-in-such-a-wonderful-time', 'blog'),
(2290, 0, 2, 'blog=5', 'lingerie-wasnt-always-so-stylish-and-erotic', 'blog'),
(2241, 0, 1, 'product_id=137', 'vibrating-sex-toy-adult-toy-for-men-s06', 'product'),
(2240, 0, 2, 'product_id=137', 'vibrating-sex-toy-adult-toy-for-men-s06', 'product'),
(2239, 0, 1, 'product_id=136', 'vibrating-sex-toy-adult-toy-for-men-s05', 'product'),
(2238, 0, 2, 'product_id=136', 'vibrating-sex-toy-adult-toy-for-men-s05', 'product'),
(2237, 0, 1, 'product_id=135', 'vibrating-sex-toy-adult-toy-for-men-s04', 'product'),
(2236, 0, 2, 'product_id=135', 'vibrating-sex-toy-adult-toy-for-men-s04', 'product'),
(2235, 0, 1, 'product_id=134', 'vibrating-sex-toy-adult-toy-for-men-s03', 'product'),
(2234, 0, 2, 'product_id=134', 'vibrating-sex-toy-adult-toy-for-men-s03', 'product'),
(2233, 0, 1, 'product_id=133', 'vibrating-sex-toy-adult-toy-for-men-s02', 'product'),
(2232, 0, 2, 'product_id=133', 'vibrating-sex-toy-adult-toy-for-men-s02', 'product'),
(2231, 0, 1, 'product_id=132', 'vibrating-sex-toy-adult-toy-for-men-s01', 'product'),
(2230, 0, 2, 'product_id=132', 'vibrating-sex-toy-adult-toy-for-men-s01', 'product'),
(2229, 0, 1, 'manufacturer_id=26', '-1', 'manufacturer'),
(2228, 0, 2, 'manufacturer_id=26', '-1', 'manufacturer'),
(2297, 0, 1, 'product_id=143', 'asos-madison-lace-underwire-bra-a02', 'product'),
(2296, 0, 2, 'product_id=143', 'asos-madison-lace-underwire-bra-a02', 'product'),
(2295, 0, 1, 'product_id=142', 'asos-madison-lace-underwire-bra-a01', 'product'),
(2294, 0, 2, 'product_id=142', 'asos-madison-lace-underwire-bra-a01', 'product'),
(2242, 0, 2, 'product_id=138', 'vibrating-sex-toy-adult-toy-for-men-s07', 'product'),
(2243, 0, 1, 'product_id=138', 'vibrating-sex-toy-adult-toy-for-men-s07', 'product'),
(2244, 0, 2, 'product_id=139', 'vibrating-sex-toy-adult-toy-for-men-s08', 'product'),
(2245, 0, 1, 'product_id=139', 'vibrating-sex-toy-adult-toy-for-men-s08', 'product'),
(2246, 0, 2, 'product_id=140', 'vibrating-sex-toy-adult-toy-for-men-s09', 'product'),
(2247, 0, 1, 'product_id=140', 'vibrating-sex-toy-adult-toy-for-men-s09', 'product'),
(2248, 0, 2, 'product_id=141', 'vibrating-sex-toy-adult-toy-for-men-s10', 'product'),
(2249, 0, 1, 'product_id=141', 'vibrating-sex-toy-adult-toy-for-men-s10', 'product'),
(2299, 0, 1, 'product_id=144', 'asos-madison-lace-underwire-bra-a03', 'product'),
(2298, 0, 2, 'product_id=144', 'asos-madison-lace-underwire-bra-a03', 'product'),
(2301, 0, 1, 'product_id=145', 'asos-madison-lace-underwire-bra-a04', 'product'),
(2300, 0, 2, 'product_id=145', 'asos-madison-lace-underwire-bra-a04', 'product'),
(2259, 0, 2, 'product_id=146', 'asos-madison-lace-underwire-bra-a05', 'product'),
(2260, 0, 1, 'product_id=146', 'asos-madison-lace-underwire-bra-a05', 'product'),
(2261, 0, 2, 'product_id=147', 'asos-madison-lace-underwire-bra-a06', 'product'),
(2262, 0, 1, 'product_id=147', 'asos-madison-lace-underwire-bra-a06', 'product'),
(2263, 0, 2, 'product_id=148', 'asos-madison-lace-underwire-bra-a07', 'product'),
(2264, 0, 1, 'product_id=148', 'asos-madison-lace-underwire-bra-a07', 'product'),
(2265, 0, 2, 'product_id=149', 'asos-madison-lace-underwire-bra-a08', 'product'),
(2266, 0, 1, 'product_id=149', 'asos-madison-lace-underwire-bra-a08', 'product'),
(2267, 0, 2, 'collection=10', 'bras', 'collection'),
(2268, 0, 2, 'collection=11', 'bralettes', 'collection'),
(2269, 0, 2, 'product_id=150', 'marie-meili-wisdom-super-push-p01', 'product'),
(2270, 0, 1, 'product_id=150', 'marie-meili-wisdom-super-push-p01', 'product'),
(2271, 0, 2, 'product_id=151', 'marie-meili-wisdom-super-push-p02', 'product'),
(2272, 0, 1, 'product_id=151', 'marie-meili-wisdom-super-push-p02', 'product'),
(2273, 0, 2, 'product_id=152', 'marie-meili-wisdom-super-push-p03', 'product'),
(2274, 0, 1, 'product_id=152', 'marie-meili-wisdom-super-push-p03', 'product'),
(2275, 0, 2, 'product_id=153', 'marie-meili-wisdom-super-push-p04', 'product'),
(2276, 0, 1, 'product_id=153', 'marie-meili-wisdom-super-push-p04', 'product'),
(2277, 0, 2, 'collection=12', 'underwear', 'collection'),
(2278, 0, 2, 'product_id=154', 'marie-meili-wisdom-super-push-p05', 'product'),
(2279, 0, 1, 'product_id=154', 'marie-meili-wisdom-super-push-p05', 'product'),
(2280, 0, 2, 'product_id=155', 'marie-meili-wisdom-super-push-p06', 'product'),
(2281, 0, 1, 'product_id=155', 'marie-meili-wisdom-super-push-p06', 'product'),
(2282, 0, 2, 'product_id=156', 'marie-meili-wisdom-super-push-p07', 'product'),
(2283, 0, 1, 'product_id=156', 'marie-meili-wisdom-super-push-p07', 'product'),
(2284, 0, 2, 'product_id=157', 'marie-meili-wisdom-super-push-p08', 'product'),
(2285, 0, 1, 'product_id=157', 'marie-meili-wisdom-super-push-p08', 'product'),
(2286, 0, 2, 'collection=13', 'sleepwear-amp-lounge', 'collection'),
(2287, 0, 2, 'collection=14', 'th1', 'collection'),
(2288, 0, 2, 'collection=15', 'th2', 'collection'),
(2291, 0, 2, 'blog=6', 'sexy-lingerie', 'blog'),
(2292, 0, 2, 'menu_id=9', 'menu-bst', ''),
(2293, 0, 1, 'menu_id=9', 'menu-bst', '');