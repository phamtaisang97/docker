-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(21, NULL, 0, 0, 0, 0, 1, '2020-06-23 20:20:36', '2020-06-23 20:20:36'),
(22, NULL, 0, 0, 0, 0, 1, '2020-06-23 20:20:36', '2020-06-23 20:20:36');


-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(22, 2, 'Biệt thự cao cấp', 'Biệt thự cao cấp', '', '', ''),
(22, 1, 'Biệt thự cao cấp', 'Biệt thự cao cấp', '', '', ''),
(21, 1, 'Biệt thự nhà vườn', 'Biệt thự nhà vườn', '', '', ''),
(21, 2, 'Biệt thự nhà vườn', 'Biệt thự nhà vườn', '', '', '');


-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(21, 21, 0),
(22, 22, 0);


-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(21, 0),
(22, 0);
