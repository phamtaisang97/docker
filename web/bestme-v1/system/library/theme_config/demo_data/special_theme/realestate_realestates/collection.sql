-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(9, 'Sản phẩm chất lượng cao', 0, 1, '', '0'),
(10, 'Dịch vụ chuyên nghiệp', 0, 1, '', '0'),
(11, 'Môi trường xanh sạch', 0, 1, '', '0'),
(12, 'Cộng đồng nhân văn', 0, 1, '', '0'),
(13, 'An ninh đảm bảo', 0, 1, '', '0'),
(14, 'Tiện ích đồng bộ', 0, 1, '', '0'),
(15, 'Nơi hạnh phúc ngập tràn', 0, 1, '', '1'),
(16, 'Cuộc sống xanh', 0, 1, '', '0'),
(17, 'Tiện ích', 0, 1, '', '0'),
(4, 'Sản phẩm khuyến mại', 0, 1, '', '0'),
(5, 'Sản phẩm hot', 0, 1, '', '0'),
(6, 'Sản phẩm mới', 0, 1, '', '0'),
(7, 'Hàng ngày', 0, 1, NULL, '0');


-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(10, 'https://cdn.bestme.asia/images/x2/gallery2.jpg', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/x2/450x338-gallery31.jpg', '', '', '', ''),
(12, 'https://cdn.bestme.asia/images/x2/gallery4.jpg', '', '', '', ''),
(13, 'https://cdn.bestme.asia/images/x2/gallery5.jpg', '', '', '', ''),
(14, 'https://cdn.bestme.asia/images/x2/gallery6.jpg', '', '', '', ''),
(9, 'https://cdn.bestme.asia/images/x2/450x338-gallery311.jpg', '', '', '', ''),
(15, '', '', '', '', ''),
(16, 'https://cdn.bestme.asia/images/x2/cs-img.jpg', '', '', '', ''),
(17, 'https://cdn.bestme.asia/images/x2/34.jpg', '', '', '', ''),
(4, '/catalog/view/theme/default/image/fashion_shoeszone/products/83/thumb.jpg', '&lt;p&gt;sản phẩm khuyến mại&lt;/p&gt;', '', '', ''),
(5, '/catalog/view/theme/default/image/fashion_shoeszone/products/84/thumb.jpg', '&lt;p&gt;sản phẩm hot&lt;/p&gt;', '', '', ''),
(6, '/catalog/view/theme/default/image/fashion_shoeszone/products/85/thumb.jpg', '&lt;p&gt;nhũng sản phẩm mới nhất c&amp;oacute; mặt tại cửa h&amp;agrave;ng&lt;/p&gt;', '', '', ''),
(7, '/catalog/view/theme/default/image/fashion_shoeszone/products/86/thumb.jpg', '&lt;p&gt;Bộ sưu tập h&amp;agrave;ng ng&amp;agrave;y&lt;/p&gt;', '', '', '');