-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(43, '', 42, 0, 0, 0, 1, '2020-12-23 09:49:53', '2020-12-23 09:49:53'),
(42, '', 37, 0, 0, 0, 1, '2020-12-23 09:48:48', '2020-12-23 09:49:53'),
(41, '', 37, 0, 0, 0, 1, '2020-12-23 09:48:48', '2020-12-23 09:48:48'),
(40, '', 0, 0, 0, 0, 1, '2020-12-22 11:44:47', '2020-12-22 11:44:47'),
(39, '', 0, 0, 0, 0, 1, '2020-12-22 11:26:33', '2020-12-22 11:26:33'),
(32, NULL, 0, 0, 0, 0, 1, '2020-12-21 17:29:08', '2020-12-21 17:29:08'),
(33, NULL, 0, 0, 0, 0, 1, '2020-12-21 17:29:08', '2020-12-21 17:29:08'),
(34, NULL, 0, 0, 0, 0, 1, '2020-12-21 17:29:08', '2020-12-21 17:29:08'),
(35, NULL, 0, 0, 0, 0, 1, '2020-12-21 17:29:08', '2020-12-21 17:29:08'),
(36, NULL, 0, 0, 0, 0, 1, '2020-12-21 17:29:08', '2020-12-21 17:29:08'),
(37, '', 0, 0, 0, 0, 1, '2020-12-22 11:24:16', '2020-12-23 09:48:48'),
(38, '', 0, 0, 0, 0, 1, '2020-12-22 11:24:30', '2020-12-22 11:24:30'),
(44, '', 42, 0, 0, 0, 1, '2020-12-23 09:49:53', '2020-12-23 09:49:53');


-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(43, 1, 'Bạch tuộc loại 1', 'Bạch tuộc loại 1', '', '', ''),
(43, 2, 'Bạch tuộc loại 1', 'Bạch tuộc loại 1', '', '', ''),
(42, 1, 'Bạch tuộc 1 nắng', 'Bạch tuộc 1 nắng', '', '', ''),
(32, 2, 'Cá các loại', 'Cá các loại', '', '', ''),
(32, 1, 'Cá các loại', 'Cá các loại', '', '', ''),
(33, 2, 'Nghêu - Sò - Ốc', 'Nghêu - Sò - Ốc', '', '', ''),
(33, 1, 'Nghêu - Sò - Ốc', 'Nghêu - Sò - Ốc', '', '', ''),
(34, 2, 'Cua - Ghẹ', 'Cua - Ghẹ', '', '', ''),
(34, 1, 'Cua - Ghẹ', 'Cua - Ghẹ', '', '', ''),
(35, 2, 'Hải sản nhập khẩu', 'Hải sản nhập khẩu', '', '', ''),
(35, 1, 'Hải sản nhập khẩu', 'Hải sản nhập khẩu', '', '', ''),
(36, 2, 'Tôm các loại', 'Tôm các loại', '', '', ''),
(36, 1, 'Tôm các loại', 'Tôm các loại', '', '', ''),
(37, 2, 'Bạch Tuộc - Mực', 'Bạch Tuộc - Mực', '', '', ''),
(37, 1, 'Bạch Tuộc - Mực', 'Bạch Tuộc - Mực', '', '', ''),
(38, 2, 'Hải sản nội địa', 'Hải sản nội địa', '', '', ''),
(38, 1, 'Hải sản nội địa', 'Hải sản nội địa', '', '', ''),
(39, 2, 'Hàu - Bào ngư', 'Hàu - Bào ngư', '', '', ''),
(39, 1, 'Hàu - Bào ngư', 'Hàu - Bào ngư', '', '', ''),
(40, 2, 'Đồ biển khô', 'Đồ biển khô', '', '', ''),
(40, 1, 'Đồ biển khô', 'Đồ biển khô', '', '', ''),
(41, 2, 'Bạch tuộc khô', 'Bạch tuộc khô', '', '', ''),
(41, 1, 'Bạch tuộc khô', 'Bạch tuộc khô', '', '', ''),
(42, 2, 'Bạch tuộc 1 nắng', 'Bạch tuộc 1 nắng', '', '', ''),
(44, 2, 'Bạch tuộc loại 2', 'Bạch tuộc loại 2', '', '', ''),
(44, 1, 'Bạch tuộc loại 2', 'Bạch tuộc loại 2', '', '', '');


-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(44, 44, 2),
(44, 42, 1),
(44, 37, 0),
(43, 43, 2),
(43, 42, 1),
(39, 39, 0),
(40, 40, 0),
(41, 37, 0),
(33, 33, 0),
(43, 37, 0),
(42, 42, 1),
(35, 35, 0),
(38, 38, 0),
(42, 37, 0),
(41, 41, 1),
(32, 32, 0),
(36, 36, 0),
(37, 37, 0),
(34, 34, 0);


-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(39, 0),
(40, 0),
(41, 0),
(42, 0),
(43, 0),
(44, 0);
