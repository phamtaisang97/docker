-- oc_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(15, 'Sản phẩm mới', 0, 1, '', '0'),
(16, 'Thế Giới Hải Sản', 0, 1, '', '1'),
(17, 'Bán Chạy Nhất', 0, 1, '', '1'),
(18, 'Hải sản nhập khẩu', 0, 1, '', '0'),
(9, 'Đồ biển khô', 0, 1, '', '0'),
(10, 'Các loại cá', 0, 1, '', '0'),
(11, 'Nghêu - Sò - Ốc', 0, 1, '', '0'),
(12, 'Hàu - Bào ngư', 0, 1, '', '0'),
(13, 'Cua - Ghẹ', 0, 1, '', '0'),
(14, 'Tôm các loại', 0, 1, '', '0'),
(19, 'Hải sản nội địa', 0, 1, '', '0');


-- oc_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(15, '', '', '', '', ''),
(16, '', '', '', '', ''),
(17, '', '', '', '', ''),
(18, '', '', '', '', ''),
(19, '', '', '', '', ''),
(9, 'https://cdn.bestme.asia/images/seafoodstore/o-bien-kho100.jpg', '', '', '', ''),
(10, 'https://cdn.bestme.asia/images/seafoodstore/cac-loai-ca100.jpg', '', '', '', ''),
(11, 'https://cdn.bestme.asia/images/seafoodstore/nghieu-so-oc100.jpg', '', '', '', ''),
(12, 'https://cdn.bestme.asia/images/seafoodstore/hau-bao-ngu100.jpg', '', '', '', ''),
(13, 'https://cdn.bestme.asia/images/seafoodstore/cua-ghe100.jpg', '', '', '', ''),
(14, 'https://cdn.bestme.asia/images/seafoodstore/tom-cac-loai100.jpg', '', '', '', '');
