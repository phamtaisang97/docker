DO NOT RENAME THIS DIR AND FILES!

This is for load/remove demo data for each theme group.

Supported theme groups:

- beauty (e.g: beauty_xoiza)

- fashion (e.g: fashion_zorka)

- tech (e.g: tech_allegorithmic)

- furniture (e.g: furniture_rango)

- mom (e.g: mom_babie)

- ...<update later>...