--
-- create new demo data
--
CREATE TABLE IF NOT EXISTS `oc_demo_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_id` int(11) NOT NULL,
  `table_name` varchar(255) DEFAULT '',
  `theme_group` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- create new demo data
--

INSERT IGNORE INTO `oc_demo_data` (`data_id`, `table_name`, `theme_group`) VALUES
-- product table
(75, 'product', 'beauty'),
(81, 'product', 'beauty'),
(89, 'product', 'beauty'),
(90, 'product', 'beauty'),
(91, 'product', 'beauty'),
(92, 'product', 'beauty'),
(93, 'product', 'beauty'),
(94, 'product', 'beauty'),
(95, 'product', 'beauty'),
(96, 'product', 'beauty'),
-- category table
(10, 'category', 'beauty'),
(11, 'category', 'beauty'),
(13, 'category', 'beauty'),
(15, 'category', 'beauty'),
(16, 'category', 'beauty'),
-- collection table
(4, 'collection', 'beauty'),
(5, 'collection', 'beauty'),
(6, 'collection', 'beauty'),
(7, 'collection', 'beauty');
-- seo_url table: auto deleted when deleting product, category, collection

-- category
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(10, NULL, 0, 0, 0, 0, 1, '2019-05-20 16:05:16', '2019-05-20 16:05:16'),
(11, NULL, 0, 0, 0, 0, 1, '2019-05-20 16:37:28', '2019-05-20 16:37:28'),
(13, NULL, 0, 0, 0, 0, 1, '2019-05-20 16:54:28', '2019-05-20 16:54:28'),
(15, NULL, 0, 0, 0, 0, 1, '2019-05-21 13:49:04', '2019-05-21 13:49:04'),
(16, NULL, 0, 0, 0, 0, 1, '2019-05-21 14:28:02', '2019-05-21 14:28:02');

-- oc_category_description
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(10, 2, 'Sữa dưỡng thể', 'Sữa dưỡng thể', '', '', ''),
(10, 1, 'Sữa dưỡng thể', 'Sữa dưỡng thể', '', '', ''),
(11, 2, 'Sữa rửa mặt', 'Sữa rửa mặt', '', '', ''),
(11, 1, 'Sữa rửa mặt', 'Sữa rửa mặt', '', '', ''),
(13, 2, 'Kem chống nắng', 'Kem chống nắng', '', '', ''),
(13, 1, 'Kem chống nắng', 'Kem chống nắng', '', '', ''),
(15, 2, 'Kem dưỡng da', 'Kem dưỡng da', '', '', ''),
(15, 1, 'Kem dưỡng da', 'Kem dưỡng da', '', '', ''),
(16, 2, 'Dầu gội', 'Dầu gội', '', '', ''),
(16, 1, 'Dầu gội', 'Dầu gội', '', '', '');

-- oc_category_path
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(10, 10, 0),
(11, 11, 0),
(13, 13, 0),
(15, 15, 0),
(16, 16, 0);

-- oc_category_to_store
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(10, 0),
(11, 0),
(13, 0),
(15, 0),
(16, 0);

-- oc_collection
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`) VALUES
(4, 'Sản phẩm khuyến mại', 0, 1, ''),
(5, 'Sản phẩm hot', 0, 1, ''),
(6, 'Sản phẩm mới', 0, 1, ''),
(7, 'Hàng ngày', 0, 1, NULL);

-- oc_collection_description
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(4, 'https://res.cloudinary.com/novaon-x2/image/upload/v1555900767/x2-8787.bestme.test/Capture28.png', '&lt;p&gt;sản phẩm khuyến mại&lt;/p&gt;', '', '', ''),
(5, 'https://res.cloudinary.com/novaon-x2/image/upload/v1555900690/x2-8787.bestme.test/dell-vostro-5568-077m52-vangdong-450x300-450x300-600x600.png', '&lt;p&gt;sản phẩm hot&lt;/p&gt;', '', '', ''),
(6, '', '&lt;p&gt;nhũng sản phẩm mới nhất c&amp;oacute; mặt tại cửa h&amp;agrave;ng&lt;/p&gt;', '', '', ''),
(7, 'https://res.cloudinary.com/novaon-x2/image/upload/v1558345596/x2-8888.bestme.test/8ts19s033-fw125-l_2.jpg', '&lt;p&gt;Bộ sưu tập h&amp;agrave;ng ng&amp;agrave;y&lt;/p&gt;', '', '', '');

-- oc_product
-- 75
-- 81
-- 89
-- 90
-- 91
-- 92
-- 93
-- 94
-- 95
-- 96
UPDATE `oc_product` SET `deleted` = NULL WHERE `product_id` IN (75, 81, 89, 90, 91, 92, 93, 94, 95, 96) AND `demo` = 1;

INSERT IGNORE INTO `oc_product` (`product_id`, `model`, `sku`, `barcode`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `sale_on_out_of_stock`, `stock_status_id`, `image`, `image_alt`, `multi_versions`, `manufacturer_id`, `shipping`, `price`, `price_currency_id`, `compare_price`, `c_price_currency_id`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `viewed`, `date_added`, `date_modified`, `demo`) VALUES
(75, '', 'SKU123', '', '', '', '', '', '', '', 400, 0, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558663987/x2-8888.bestme.test/2.png', '', 0, 14, 1, '240000.0000', 1, '350000.0000', 1, 0, 0, '0000-00-00', '250.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-20 16:08:44', '2019-05-20 16:08:44', 1),
(81, '', 'SKU250', '', '', '', '', '', '', '', 350, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664073/x2-8888.bestme.test/4.png', '', 0, 15, 1, '140000.0000', 1, '250000.0000', 1, 0, 0, '0000-00-00', '200.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-20 16:40:20', '2019-05-20 16:40:20', 1),
(89, '', 'SKU435', '', '', '', '', '', '', '', 360, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664151/x2-8888.bestme.test/6.png', '', 0, 17, 1, '350000.0000', 1, '430000.0000', 1, 0, 0, '0000-00-00', '200.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-20 17:51:54', '2019-05-20 17:51:54', 1),
(90, '', 'SKU255', '', '', '', '', '', '', '', 370, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664248/x2-8888.bestme.test/9.png', '', 0, 18, 1, '260000.0000', 1, '335000.0000', 1, 0, 0, '0000-00-00', '200.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-21 13:42:58', '2019-05-21 13:42:58', 1),
(91, '', 'SKU199', '', '', '', '', '', '', '', 420, 0, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664345/x2-8888.bestme.test/11.png', '', 0, 19, 1, '199000.0000', 1, '250000.0000', 1, 0, 0, '0000-00-00', '200.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-21 13:49:13', '2019-05-21 13:49:13', 1),
(92, '', '', '', '', '', '', '', '', '', 1, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664478/x2-8888.bestme.test/15.png', '', 0, 20, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-21 14:16:14', '2019-05-21 14:16:14', 1),
(93, '', 'SKU360', '', '', '', '', '', '', '', 450, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664581/x2-8888.bestme.test/18.png', '', 0, 21, 1, '280000.0000', 1, '360000.0000', 1, 0, 0, '0000-00-00', '200.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-21 14:28:48', '2019-05-21 14:28:48', 1),
(94, '', '', '', '', '', '', '', '', '', 1, 0, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664663/x2-8888.bestme.test/600x600/20.png', '', 0, 22, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '1500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-21 14:43:39', '2019-05-21 14:43:39', 1),
(95, '', 'SKU850', '', '', '', '', '', '', '', 510, 0, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664811/x2-8888.bestme.test/22.png', '', 0, 23, 1, '850000.0000', 1, '1000000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-21 15:01:39', '2019-05-21 15:01:39', 1),
(96, '', 'SKU150', '', '', '', '', '', '', '', 390, 0, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664954/x2-8888.bestme.test/25.png', '', 0, 23, 1, '215000.0000', 1, '280000.0000', 1, 0, 0, '0000-00-00', '300.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-21 15:12:38', '2019-05-21 15:12:38', 1);

-- oc_product_collection
INSERT IGNORE INTO `oc_product_collection` (`product_collection_id`, `product_id`, `collection_id`, `sort_order`) VALUES
(75, 75, 4, 0),
(78, 81, 4, 0),
(85, 89, 6, 0),
(87, 90, 5, 0),
(88, 92, 6, 0),
(89, 92, 4, 0),
(90, 93, 4, 0),
(91, 93, 6, 0),
(92, 94, 5, 0),
(93, 94, 4, 0),
(94, 94, 6, 0),
(95, 95, 5, 0),
(96, 95, 6, 0),
(97, 96, 4, 0),
(98, 96, 5, 0),
(99, 96, 6, 0);

-- oc_product_descriptions
INSERT IGNORE INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `sub_description`, `seo_title`, `seo_description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(75, 2, 'Sữa dưỡng thể trắng da chống nắng Hatomugi SPF Nhật Bản', '&lt;p&gt;*** C&amp;ocirc;ng dụng: &amp;bull; Đ&amp;acirc;y l&amp;agrave; si&amp;ecirc;u phẩm cho m&amp;ugrave;a h&amp;egrave; m&amp;aacute;t lạnh với t&amp;aacute;c dụng vừa chống nắng vừa l&amp;agrave;m trắng da &amp;bull; Gi&amp;uacute;p tr&amp;aacute;nh được t&amp;igrave;nh trạng ch&amp;aacute;y nắng đỏ r&amp;aacute;t khi ra ngo&amp;agrave;i trời m&amp;ugrave;a h&amp;egrave;. &amp;bull; Dễ thẩm thấu ngay v&amp;agrave;o da khi b&amp;ocirc;i kh&amp;ocirc;ng h&amp;acirc;y t&amp;igrave;nh trạng bết d&amp;iacute;nh kh&amp;oacute; chịu. &amp;bull; Gi&amp;uacute;p dưỡng ẩm s&amp;acirc;u, l&amp;agrave;m mềm da. &amp;bull; Gi&amp;uacute;p dưỡng da s&amp;aacute;ng hơn, trắng hơn, x&amp;oacute;a mờ th&amp;acirc;m từng ng&amp;agrave;y. &amp;bull; Gi&amp;uacute;p l&amp;agrave;m dịu da, chống vi&amp;ecirc;m, cải thiện t&amp;igrave;nh trạng mụn, trứng c&amp;aacute;. &amp;bull; L&amp;agrave;m mịn da, giảm sự th&amp;ocirc; r&amp;aacute;p tr&amp;ecirc;n bề mặt da. &amp;bull; C&amp;oacute; t&amp;aacute;c dụng se kh&amp;iacute;t lỗ ch&amp;acirc;n l&amp;ocirc;ng. &amp;bull; L&amp;agrave;m săn chắc da, chống l&amp;atilde;o h&amp;oacute;a, cải thiện dấu hiệu của tuổi t&amp;aacute;c. &amp;bull; B&amp;igrave;nh thường h&amp;oacute;a chu kỳ sừng h&amp;oacute;a của tế b&amp;agrave;o da. &amp;bull; Sữa dưỡng thể l&amp;agrave;m trắng, chống nắng Hatomugi SPF31 PA+++ kh&amp;ocirc;ng g&amp;acirc;y hại cho da v&amp;igrave; th&amp;agrave;nh phần từ hạt &amp;yacute; dĩ &amp;ndash; một dược liệu qu&amp;yacute; v&amp;agrave; c&amp;oacute; &amp;iacute;ch với sức khỏe v&amp;agrave; c&amp;ocirc;ng dụng thần kỳ trong việc l&amp;agrave;m đẹp l&amp;agrave;m trắng da phi thường của người Nhật *** C&amp;aacute;ch d&amp;ugrave;ng: &amp;bull; Thoa đều l&amp;ecirc;n da body v&amp;agrave;o mỗi buổi s&amp;aacute;ng l&amp;agrave; đủ cho cả ng&amp;agrave;y d&amp;agrave;i nắng n&amp;oacute;ng &amp;bull; Chất lotion lỏng nhẹ được thoa l&amp;ecirc;n da, tan v&amp;agrave; thấm ngay v&amp;agrave;o da chỉ sau 2-3s.&lt;/p&gt;', '', '', '', '', '', '', ''),
(81, 2, 'Sữa Rửa Mặt Kose Softymo 190g Chính Hãng Nhật Bản', '&lt;p&gt;✅Sữa rửa mặt Kose Softymo Hyaluronic Acid (tu&amp;yacute;p m&amp;agrave;u hồng): dưỡng ẩm d&amp;agrave;nh cho da kh&amp;ocirc; Tu&amp;yacute;p m&amp;agrave;u hồng: cung cấp th&amp;ecirc;m độ ẩm cho da kh&amp;ocirc;. Sữa rửa mặt Kose Softymo Hyaluronic Acid Cleansing Wash nhẹ nh&amp;agrave;ng đ&amp;aacute;nh bật v&amp;agrave; loại bỏ c&amp;aacute;c vết bẩn v&amp;agrave; tẩy trang. C&amp;aacute;c hạt liti trong Sữa rửa mặt thấm s&amp;acirc;u v&amp;agrave;o trong c&amp;aacute;c khe hở mang đi c&amp;aacute;c chất dầu tr&amp;ecirc;n mặt.&lt;/p&gt;\r\n\r\n&lt;p&gt;✅Sữa rửa mặt Kose Softymo Collagen (tu&amp;yacute;p m&amp;agrave;u xanh): d&amp;agrave;nh cho da thường Tu&amp;yacute;p m&amp;agrave;u xanh: sữa mặt Collagen gi&amp;uacute;p l&amp;agrave;m trẻ h&amp;oacute;a, căng mịn l&amp;agrave;n da, chứa c&amp;aacute;c th&amp;agrave;nh phần tự nhi&amp;ecirc;n v&amp;agrave; tinh chất dược th&amp;acirc;m nhập s&amp;acirc;u v&amp;agrave;o c&amp;aacute;c khe hở lấy đi c&amp;aacute;c chất bẩn v&amp;agrave; dầu tr&amp;ecirc;n da mặt, mang lại l&amp;agrave;n da khỏe khoắn v&amp;agrave; sạch đẹp.&lt;/p&gt;\r\n\r\n&lt;p&gt;✅Sữa rửa mặt Kose Softymo White (tu&amp;yacute;p m&amp;agrave;u trắng): th&amp;iacute;ch hợp cho da mụn. Tu&amp;yacute;p m&amp;agrave;u trắng: Sữa rửa mặt Kose Softymo White gi&amp;uacute;p l&amp;agrave;m sạch s&amp;acirc;u, đồng thời chứa tinh chất l&amp;agrave;m trắng, l&amp;agrave;m mờ c&amp;aacute;c vết th&amp;acirc;m, đốm, cho da bạn s&amp;aacute;ng bừng l&amp;ecirc;n mỗi ng&amp;agrave;y Shop HAT - &amp;quot;C&amp;ugrave;ng bạn Xinh Đẹp - Hạnh Ph&amp;uacute;c&amp;quot;&lt;/p&gt;', '', '', '', '', '', '', ''),
(89, 2, 'Kem chống nắng Laroche-Posay Gel Cream Dry Touch SPF 50', '&lt;p&gt;✴️✴️✴️Kem chống nắng Laroche-Posay Gel Cream Dry Touch SPF 50+ &amp;ndash; ', '', '', '', '', '', '', ''),
(90, 2, 'Kem Chống Nắng Ziajka SPF50 Bảo Vệ Làn Da Cho Bé - Balan', '&lt;p&gt;T&amp;ecirc;n sản phẩm : Kem Chống Nắng Ziajka SPF 50+&lt;/p&gt;\r\n\r\n&lt;p&gt;Xuất xứ : Ba lan&lt;/p&gt;\r\n\r\n&lt;p&gt;Thương Hiệu : Ziajka&lt;/p&gt;\r\n\r\n&lt;p&gt;Dung t&amp;iacute;ch : 125ml Hạn sử dụng : 2/2022&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kem Chống Nắng Ziajka SPF 50+ Dạng sữa kh&amp;ocirc;ng thấm nước c&amp;oacute; t&amp;aacute;c dụng chống nắng gi&amp;uacute;p bảo vệ l&amp;agrave;n da của b&amp;eacute; khi vui chơi, giữ ẩm da b&amp;eacute; m&amp;agrave; kh&amp;ocirc;ng g&amp;acirc;y nhờn d&amp;iacute;nh.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kem Chống Nắng Ziajka SPF 50+ Hấp thụ tia s&amp;aacute;ng tốt, tr&amp;aacute;nh hao hụt vitamin D.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kem Chống Nắng Ziajka SPF 50+ L&amp;agrave;m &amp;ecirc;m dịu da, chống lại tia cực t&amp;iacute;m v&amp;agrave; sự g&amp;acirc;y hại từ c&amp;aacute;c t&amp;aacute;c nh&amp;acirc;n b&amp;ecirc;n ngo&amp;agrave;i.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kem Chống Nắng Ziajka SPF 50+ Gi&amp;uacute;p chăm s&amp;oacute;c da b&amp;eacute; lu&amp;ocirc;n mềm mại, mượt m&amp;agrave;.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kem Chống Nắng Ziajka SPF 50+ Sản phẩm kh&amp;ocirc;ng m&amp;agrave;u, kh&amp;ocirc;ng m&amp;ugrave;i, kh&amp;ocirc;ng g&amp;acirc;y dị ứng rất an to&amp;agrave;n cho b&amp;eacute;.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kem Chống Nắng Ziajka SPF 50+ Dạng sữa n&amp;ecirc;n kh&amp;ocirc;ng hề b&amp;oacute;ng nhờn, kh&amp;ocirc; ngay tại chỗ (dạng sữa thường kh&amp;ocirc; nhanh v&amp;agrave; &amp;iacute;t b&amp;oacute;ng nhờn hơn dạng kem)&lt;/p&gt;\r\n\r\n&lt;p&gt;C&amp;aacute;ch d&amp;ugrave;ng - Lắc đều, b&amp;ocirc;i một lượt kem mỏng l&amp;ecirc;n v&amp;ugrave;ng da.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Tr&amp;aacute;nh v&amp;agrave;o mắt. Kh&amp;ocirc;ng b&amp;ocirc;i l&amp;ecirc;n vết thương hở.&lt;/p&gt;', '', '', '', '', '', '', ''),
(91, 2, 'Kem dưỡng da trắng mượt đều màu ban đêm LOreal Paris White Perfect 50ml', '&lt;p&gt;C&amp;ocirc;ng dụng&lt;/p&gt;\r\n\r\n&lt;p&gt;- Dưỡng trắng đều m&amp;agrave;u: với vitamin Cg, gi&amp;uacute;p giảm v&amp;agrave; c&amp;acirc;n bằng hắc sắc tố ở mức thấp nhất.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Dưỡng da mịn mượt: với Glycerin, tạo v&amp;agrave; lưu giữ độ ẩm tr&amp;ecirc;n da.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Rạng rỡ l&amp;agrave;n da: với tinh thể đ&amp;aacute; qu&amp;yacute; Tourmaline, gi&amp;uacute;p th&amp;uacute;c đẩy qu&amp;aacute; tr&amp;igrave;nh tuần ho&amp;agrave;n dưới da.&lt;/p&gt;\r\n\r\n&lt;p&gt;Hiệu quả&lt;/p&gt;\r\n\r\n&lt;p&gt;- Ngay lập tức, l&amp;agrave;n da được dưỡng ẩm mềm mịn v&amp;agrave; tươi s&amp;aacute;ng r&amp;otilde; rệt.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Sau 1 tuần, l&amp;agrave;n da dần đều m&amp;agrave;u &amp;amp; được cải thiện r&amp;otilde; rệt.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Sau 1 th&amp;aacute;ng, l&amp;agrave;n da trở n&amp;ecirc;n trắng mượt, đều m&amp;agrave;u thấy r&amp;otilde;. Hướng dẫn sử dụng - Sử dụng v&amp;agrave;o mỗi tối, trước khi ngủ.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Sau bước l&amp;agrave;m sạch da, sử dụng một lượng kem vừa đủ (khoảng 1 hạt bắp) v&amp;agrave; chia đều tr&amp;ecirc;n năm điểm: tr&amp;aacute;n, mũi, cằm, hai b&amp;ecirc;n m&amp;aacute;.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Sau đ&amp;oacute;, thoa đều nhẹ nh&amp;agrave;ng hướng từ dưới l&amp;ecirc;n tr&amp;ecirc;n v&amp;agrave; vỗ nhẹ.&lt;/p&gt;\r\n\r\n&lt;p&gt;- N&amp;ecirc;n d&amp;ugrave;ng th&amp;ecirc;m cho v&amp;ugrave;ng da ở cổ. Tr&amp;aacute;nh v&amp;ugrave;ng da quanh mắt. Hướng dẫn bảo quản - Bảo quản nơi kh&amp;ocirc; r&amp;aacute;o, tho&amp;aacute;ng m&amp;aacute;t.&lt;/p&gt;', '', '', '', '', '', '', ''),
(92, 2, 'KEM DƯỠNG DA V7 TONING LIGHT DR JART 50ML', '&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Thương hiệu: Dr.Jart+&lt;/p&gt;\r\n\r\n&lt;p&gt;- Trọng lượng: 50ml&lt;/p&gt;\r\n\r\n&lt;p&gt;1. D&amp;ugrave;ng cho mọi loại da, đặc biệt l&amp;agrave; da tối m&amp;agrave;u hoặc th&amp;acirc;m do mụn.&lt;/p&gt;\r\n\r\n&lt;p&gt;2. L&amp;agrave; sản phẩm trị th&amp;acirc;m v&amp;agrave; l&amp;agrave;m đều m&amp;agrave;u da nhanh nhất, được nghi&amp;ecirc;n cứu v&amp;agrave; sản xuất dưới sự hợp t&amp;aacute;c giữa Mỹ v&amp;agrave; H&amp;agrave;n Quốc; l&amp;agrave; sản phẩm nổi tiếng v&amp;agrave; được b&amp;aacute;n tại Sephora c&amp;ugrave;ng c&amp;aacute;c cửa h&amp;agrave;ng lớn tr&amp;ecirc;n thế giới.&lt;/p&gt;\r\n\r\n&lt;p&gt;3. L&amp;agrave; kem dưỡng trắng, l&amp;agrave;m s&amp;aacute;ng tone da ngay lập tức; l&amp;agrave;m mờ t&amp;agrave;n nhang, th&amp;acirc;m, n&amp;aacute;m v&amp;agrave; đốm n&amp;acirc;u, gi&amp;uacute;p da s&amp;aacute;ng đều m&amp;agrave;u hơn; gi&amp;uacute;p t&amp;aacute;i tạo, phục hồi da; giảm nhờn b&amp;oacute;ng, se kh&amp;iacute;t lỗ ch&amp;acirc;n l&amp;ocirc;ng; ngăn ngừa sự h&amp;igrave;nh th&amp;agrave;nh, ph&amp;aacute;t triển của mụn.&lt;/p&gt;\r\n\r\n&lt;p&gt;4. Chứa Niacinamide, 7 loại vitamin c&amp;ugrave;ng c&amp;aacute;c th&amp;agrave;nh phần Korean Jade Water, White Jade v&amp;agrave; Licorice extract c&amp;oacute; t&amp;aacute;c dụng l&amp;agrave;m da trắng s&amp;aacute;ng, n&amp;acirc;ng tone da. Dr.Jart+ V7 Toning Light l&amp;agrave; Sản phẩm dưỡng trắng mới đang cực hot b&amp;ecirc;n H&amp;agrave;n, đặc biệt được feedback l&amp;agrave; sản phẩm trị th&amp;acirc;m v&amp;agrave; l&amp;agrave;m đều m&amp;agrave;u da nhanh hơn tất cả c&amp;aacute;c loại kem kh&amp;aacute;c.&lt;/p&gt;\r\n\r\n&lt;p&gt;Đ&amp;acirc;y ch&amp;iacute;nh l&amp;agrave; cứu c&amp;aacute;nh cho những bạn c&amp;oacute; l&amp;agrave;n da tối m&amp;agrave;u v&amp;agrave; l&amp;agrave;n da th&amp;acirc;m do mụn. Dr.Jart+ V7 Toning Light được nghi&amp;ecirc;n cứu v&amp;agrave; sản xuất dưới sự hợp t&amp;aacute;c giữa Mỹ v&amp;agrave; H&amp;agrave;n Quốc, l&amp;agrave; sản phẩm nổi tiếng v&amp;agrave; được b&amp;aacute;n tại Sephora c&amp;ugrave;ng c&amp;aacute;c cửa h&amp;agrave;ng lớn tr&amp;ecirc;n thế giới.&lt;/p&gt;', '', '', '', '', '', '', ''),
(93, 2, 'Dầu gội suôn mượt mềm mại Tsubaki Botanical 450ml', '&lt;p&gt;&amp;bull; Dầu gội ngăn ngừa xơ rối gi&amp;uacute;p duy tr&amp;igrave; m&amp;aacute;i t&amp;oacute;c su&amp;ocirc;n mượt, m&amp;ecirc;̀m mại.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với phức hợp giàu dưỡng ch&amp;acirc;́t thiết yếu bổ sung dưỡng chất từ ch&amp;acirc;n t&amp;oacute;c, gi&amp;uacute;p thẳng mượt, &amp;oacute;ng ả.&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;bull; M&amp;ugrave;i hương tinh t&amp;ecirc;́ và gợi cảm được chiết xuất từ t&amp;ocirc;̉ hợp c&amp;aacute;c loại hoa v&amp;agrave; quả mọng.&lt;/p&gt;\r\n\r\n&lt;p&gt;3 bước chăm s&amp;oacute;c t&amp;oacute;c cơ bản:&lt;/p&gt;\r\n\r\n&lt;p&gt;1. Dầu gội: Dầu gội su&amp;ocirc;n mượt mềm mại (Tsubaki smooth shampoo)&lt;/p&gt;\r\n\r\n&lt;p&gt;2. Dầu xả: Dầu xả su&amp;ocirc;n mượt mềm mại (Tsubaki smooth conditioner)&lt;/p&gt;\r\n\r\n&lt;p&gt;3. Xịt dưỡng t&amp;oacute;c: Xịt dưỡng t&amp;oacute;c su&amp;ocirc;n mượt mềm mại (Tsubaki smooth hair water)&lt;/p&gt;\r\n\r\n&lt;p&gt;3 bước chăm s&amp;oacute;c t&amp;oacute;c phục hồi:&lt;/p&gt;\r\n\r\n&lt;p&gt;1. Dầu gội: Dầu gội su&amp;ocirc;n mượt mềm mại (Tsubaki smooth shampoo)&lt;/p&gt;\r\n\r\n&lt;p&gt;2. Kem xả phục hồi: Kem xả phục hồi chuy&amp;ecirc;n s&amp;acirc;u su&amp;ocirc;n mượt mềm mại (Tsubaki smooth treatment)&lt;/p&gt;\r\n\r\n&lt;p&gt;3. Xịt dưỡng t&amp;oacute;c: Xịt dưỡng t&amp;oacute;c su&amp;ocirc;n mượt mềm mại (Tsubaki smooth hair water)&lt;/p&gt;\r\n\r\n&lt;p&gt;HDSD: Giữ nắp chai v&amp;agrave; xoay v&amp;ograve;i ngược chiều kim đồng hồ, lấy một lượng vừa đủ dầu gội l&amp;ecirc;n tay, d&amp;ugrave;ng đầu ng&amp;oacute;n tay xoa đều dầu gội l&amp;ecirc;n t&amp;oacute;c từ gốc đến ngọn v&amp;agrave; m&amp;aacute;t-xa nhẹ nh&amp;agrave;ng da đầu. Sau đ&amp;oacute; xả sạch t&amp;oacute;c với nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Lưu &amp;yacute;: Để xa tầm tay trẻ em. Tr&amp;aacute;nh tiếp x&amp;uacute;c với mắt, trong trường hợp tiếp x&amp;uacute;c với mắt, kh&amp;ocirc;ng được ch&amp;agrave; x&amp;aacute;t, phải rửa ngay với nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Sau 2-3 lần bổ sung dầu gội cần thay vỏ chai mới để tr&amp;aacute;nh vỏ chai bị nhiễm khuẩn. Ngưng sử dụng ngay khi c&amp;oacute; c&amp;aacute;c biểu hiện k&amp;iacute;ch ứng.&lt;/p&gt;\r\n\r\n&lt;p&gt;Xuất xứ: Nhật Bản&lt;/p&gt;', '', '', '', '', '', '', ''),
(94, 2, 'Dầu Gội Pantene chai 900G', '&lt;p&gt;Gi&amp;uacute;p ngăn ngừa 10 dấu hiệu t&amp;oacute;c hư tổn*** ***10 dấu hiệu hư tổn bao gồm:&lt;/p&gt;\r\n\r\n&lt;p&gt;t&amp;oacute;c rối, t&amp;oacute;c xỉn m&amp;agrave;u, dễ g&amp;atilde;y, t&amp;oacute;c chẻ ngọn, t&amp;oacute;c g&amp;atilde;y rụng, t&amp;oacute;c kh&amp;ocirc;, nh&amp;aacute;m, yếu, x&amp;ugrave;,kh&amp;ocirc;ng v&amp;agrave;o nếp.&lt;/p&gt;\r\n\r\n&lt;p&gt;C&amp;ocirc;ng thức Pro-V tăng cường th&amp;ecirc;m dưỡng chất gi&amp;uacute;p ngăn ngừa hư tổn lớp Keratin&lt;/p&gt;\r\n\r\n&lt;p&gt;** &amp;bull; Gi&amp;uacute;p cải thiện sự duy tr&amp;igrave; li&amp;ecirc;n kết protein tự nhi&amp;ecirc;n để ngăn chặn qu&amp;aacute; tr&amp;igrave;nh hư tổn &amp;bull; Gi&amp;uacute;p ngừa t&amp;oacute;c chẻ ngọn mới ph&amp;aacute;t sinh ngay cả sau 3 th&amp;aacute;ng****&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;bull; Kh&amp;ocirc;ng chất m&amp;agrave;u, kh&amp;ocirc;ng Paraben Tr&amp;aacute;nh để d&amp;iacute;nh v&amp;agrave;o mắt. Nếu dầu gội d&amp;iacute;nh v&amp;agrave;o mắt, rửa sạch với nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Bảo quản sản phẩm nơi tho&amp;aacute;ng m&amp;aacute;t, tr&amp;aacute;nh &amp;aacute;nh nắng trực tiếp. Hướng dẫn sử dụng: Thoa dầu gội, m&amp;aacute;t xa nhẹ nh&amp;agrave;ng tr&amp;ecirc;n t&amp;oacute;c ướt/ da đầu.&lt;/p&gt;\r\n\r\n&lt;p&gt;Sau đ&amp;oacute;, gội sạch bằng nước.&lt;/p&gt;', '', '', '', '', '', '', ''),
(95, 2, 'Kem Dưỡng Da Trắng Hồng Vichy Ideal Lumiere Day Cream 50ml', '&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;Loại sản phẩm - Kem dưỡng da trắng hồng căng mọng ban ng&amp;agrave;y.&lt;/p&gt;\r\n\r\n&lt;p&gt;Loại da ph&amp;ugrave; hợp - Ph&amp;ugrave; hợp với mọi loại da.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kh&amp;aacute;ch h&amp;agrave;ng muốn c&amp;oacute; l&amp;agrave;n da trắng hồng căng mọng.&lt;/p&gt;\r\n\r\n&lt;p&gt;Độ an to&amp;agrave;n - Kh&amp;ocirc;ng Paraben.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kh&amp;ocirc;ng dị ứng. - Kiểm nghiệm tr&amp;ecirc;n l&amp;agrave;n da Ch&amp;acirc;u &amp;Aacute; dưới sự gi&amp;aacute;m s&amp;aacute;t của B&amp;aacute;c sĩ da liễu.&lt;/p&gt;\r\n\r\n&lt;p&gt;Th&amp;agrave;nh phần&lt;/p&gt;\r\n\r\n&lt;p&gt;C&amp;ocirc;ng nghệ dưỡng da trắng hồng ti&amp;ecirc;n tiến:&lt;/p&gt;\r\n\r\n&lt;p&gt;1. CHIẾT XUẤT RỄ HOA MẪU ĐƠN V&amp;Agrave; VITAMIN C: Hỗ trợ tăng cường tuần ho&amp;agrave;n m&amp;aacute;u, ngăn ngừa qu&amp;aacute; tr&amp;igrave;nh oxi h&amp;oacute;a, dưỡng trắng tăng cường, cải thiện độ xỉn m&amp;agrave;u v&amp;agrave; tăng sắc hồng cho l&amp;agrave;n da, gi&amp;uacute;p da trắng hồng rạng rỡ.&lt;/p&gt;\r\n\r\n&lt;p&gt;2. HYALURONIC ACID: Với khả năng cấp nước v&amp;agrave; giữ nước gấp ng&amp;agrave;n lần khối lượng, gi&amp;uacute;p dưỡng ẩm tăng cường cho l&amp;agrave;n da.&lt;/p&gt;\r\n\r\n&lt;p&gt;3. NƯỚC KHO&amp;Aacute;NG VICHY CHỨA 15 LOẠI KHO&amp;Aacute;NG CHẤT: L&amp;agrave;m dịu, tăng cường sức đề kh&amp;aacute;ng cho l&amp;agrave;n da, gi&amp;uacute;p da chống lại c&amp;aacute;c t&amp;aacute;c nh&amp;acirc;n từ m&amp;ocirc;i trường.&lt;/p&gt;', '', '', '', '', '', '', ''),
(96, 2, 'Mặt Nạ Ngủ Dưỡng Ẩm Giúp Làm Sáng Da Vichy 15ml', '&lt;p&gt;TH&amp;Ocirc;NG TIN SẢN PHẨM&lt;/p&gt;\r\n\r\n&lt;p&gt;Loại sản phẩm&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kem dưỡng kết hợp mặt nạ ngủ cung cấp nước tức th&amp;igrave; v&amp;agrave; phục hồi chuy&amp;ecirc;n s&amp;acirc;u.&lt;/p&gt;\r\n\r\n&lt;p&gt;Loại da ph&amp;ugrave; hợp&lt;/p&gt;\r\n\r\n&lt;p&gt;- Mọi loại da, kể cả da nhạy cảm.&lt;/p&gt;\r\n\r\n&lt;p&gt;Độ an to&amp;agrave;n&lt;/p&gt;\r\n\r\n&lt;p&gt;- An to&amp;agrave;n với da nhạy cảm.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kh&amp;ocirc;ng Paraben.&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kiểm nghiệm 100% tr&amp;ecirc;n l&amp;agrave;n da Ch&amp;acirc;u &amp;Aacute; nhạy cảm.&lt;/p&gt;\r\n\r\n&lt;p&gt;Th&amp;agrave;nh phần - 50% nước kho&amp;aacute;ng Vichy Mineralizing Water: gi&amp;uacute;p phục hồi v&amp;agrave; củng cố h&amp;agrave;ng r&amp;agrave;o bảo vệ da.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Chiết xuất bạch quả Ginkgo Extract: cung cấp oxy cho từng tế b&amp;agrave;o, gi&amp;uacute;p da trở n&amp;ecirc;n s&amp;aacute;ng mịn hơn.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Hoạt chất Hyaluronic Acid v&amp;agrave; Aquabioryl: lưu giữ độ ẩm tr&amp;ecirc;n bề mặt da, cho da căng mịn.&lt;/p&gt;', '', '', '', '', '', '', '');

-- oc_product_image
INSERT IGNORE INTO `oc_product_image` (`product_image_id`, `product_id`, `image`, `image_alt`, `sort_order`) VALUES
(94, 75, 'https://res.cloudinary.com/novaonx2/image/upload/v1558663981/x2-8888.bestme.test/1.png', '', 0),
(95, 81, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664067/x2-8888.bestme.test/3.png', '', 0),
(106, 89, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664146/x2-8888.bestme.test/5.png', '', 0),
(107, 90, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664238/x2-8888.bestme.test/7.png', '', 0),
(108, 90, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664243/x2-8888.bestme.test/8.png', '', 1),
(109, 91, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664343/x2-8888.bestme.test/10.png', '', 0),
(110, 92, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664461/x2-8888.bestme.test/12.png', '', 0),
(111, 92, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664467/x2-8888.bestme.test/13.png', '', 1),
(112, 92, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664473/x2-8888.bestme.test/14.png', '', 2),
(113, 93, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664574/x2-8888.bestme.test/16.png', '', 0),
(114, 93, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664579/x2-8888.bestme.test/17.png', '', 1),
(115, 94, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664661/x2-8888.bestme.test/19.png', '', 0),
(116, 95, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664809/x2-8888.bestme.test/21.png', '', 0);

-- oc_product_to_category
INSERT IGNORE INTO `oc_product_to_category` (`product_id`, `category_id`) VALUES
(75, 10),
(81, 11),
(89, 13),
(90, 13),
(91, 15),
(92, 15),
(93, 16),
(94, 16),
(95, 15),
(96, 15);

-- oc_product_to_store
INSERT IGNORE INTO `oc_product_to_store` (`product_id`, `store_id`) VALUES
(75, 0),
(81, 0),
(89, 0),
(90, 0),
(91, 0),
(92, 0),
(93, 0),
(94, 0),
(95, 0),
(96, 0);

INSERT IGNORE INTO `oc_warehouse` (`warehouse_id`, `product_id`, `product_version_id`) VALUES
(1045, 75, NULL),
(1053, 81, NULL),
(1085, 89, NULL),
(1086, 90, NULL),
(1087, 91, NULL),
(1088, 92, NULL),
(1091, 93, NULL),
(1092, 94, NULL),
(1096, 95, NULL),
(1097, 96, NULL);

-- oc_seo_url
INSERT IGNORE INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`, `table_name`) VALUES
(2034, 0, 1, 'product_id=75', 'product-75', 'product'),
(2035, 0, 2, 'product_id=75', 'product-75', 'product'),
(2036, 0, 1, 'product_id=81', 'product-81', 'product'),
(2037, 0, 2, 'product_id=81', 'product-81', 'product'),
(2038, 0, 1, 'product_id=89', 'product-89', 'product'),
(2039, 0, 2, 'product_id=89', 'product-89', 'product'),
(2040, 0, 1, 'product_id=90', 'product-90', 'product'),
(2041, 0, 2, 'product_id=90', 'product-90', 'product'),
(2042, 0, 1, 'product_id=91', 'product-91', 'product'),
(2043, 0, 2, 'product_id=91', 'product-91', 'product'),
(2044, 0, 1, 'product_id=92', 'product-92', 'product'),
(2045, 0, 2, 'product_id=92', 'product-92', 'product'),
(2046, 0, 1, 'product_id=93', 'product-93', 'product'),
(2047, 0, 2, 'product_id=93', 'product-93', 'product'),
(2048, 0, 1, 'product_id=94', 'product-94', 'product'),
(2049, 0, 2, 'product_id=94', 'product-94', 'product'),
(2050, 0, 1, 'product_id=95', 'product-95', 'product'),
(2051, 0, 2, 'product_id=95', 'product-95', 'product'),
(2052, 0, 1, 'product_id=96', 'product-96', 'product'),
(2053, 0, 2, 'product_id=96', 'product-96', 'product'),
-- category table
(2054, 0, 2, 'category_id=10', 'category-10', 'category'),
(2055, 0, 2, 'category_id=11', 'category-11', 'category'),
(2056, 0, 2, 'category_id=13', 'category-13', 'category'),
(2057, 0, 2, 'category_id=15', 'category-15', 'category'),
(2058, 0, 2, 'category_id=16', 'category-16', 'category'),
-- collection table
(2059, 0, 2, 'collection=4', 'collection-4', 'category'),
(2060, 0, 2, 'collection=5', 'collection-5', 'category'),
(2061, 0, 2, 'collection=6', 'collection-6', 'category'),
(2062, 0, 2, 'collection=7', 'collection-7', 'category');
