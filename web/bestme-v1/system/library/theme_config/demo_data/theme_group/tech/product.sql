-- oc_product
-- 73
-- 74
-- 76
-- 78
-- 79
-- 80
-- 82
-- 100
-- 101
-- 102

INSERT IGNORE INTO `oc_product` (`product_id`, `model`, `sku`, `common_barcode`, `common_sku`, `common_compare_price`, `common_price`, `common_cost_price`, `barcode`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `sale_on_out_of_stock`, `stock_status_id`, `image`, `image_alt`, `multi_versions`, `manufacturer_id`, `shipping`, `price`, `price_currency_id`, `compare_price`, `c_price_currency_id`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `channel`, `viewed`, `demo`, `deleted`, `default_store_id`, `user_create_id`, `date_added`, `date_modified`) VALUES
(73, '', '', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 350, 1, 0, 'https://cdn.bestme.asia/images/x2/iphone-xs-max-gray-400x460_J5PqCXv.png', '', 0, 1, 1, '29990000.0000', 1, '35000000.0000', 1, 0, 0, '0000-00-00', '208.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2019-05-20 16:03:08', '2019-05-20 16:03:08'),
(74, '', '', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 320, 1, 0, 'https://cdn.bestme.asia/images/x2/samsung-galaxy-note8-black-400x460_kGLlrGB.png', '', 0, 6, 1, '11000000.0000', 1, '14990000.0000', 1, 0, 0, '0000-00-00', '208.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2019-05-20 16:07:02', '2019-05-20 16:07:02'),
(76, '', 'HUKW3943JJJ', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 316, 1, 0, 'https://cdn.bestme.asia/images/x2/ipad-6th-wifi-128-gb-1-400x460_9ul78Id.png', '', 0, 1, 1, '10500000.0000', 1, '10500000.0000', 1, 0, 0, '0000-00-00', '325.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2019-05-20 16:12:44', '2019-05-20 16:12:44'),
(78, '', 'HUKW3943888', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 400, 1, 0, 'https://cdn.bestme.asia/images/x2/ipad-mini-79-inch-wifi-cellular-64gb-2019-gray-400x460_Ue2C3e2.png', '', 0, 1, 1, '9500000.0000', 1, '10500000.0000', 1, 0, 0, '0000-00-00', '325.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2019-05-20 16:22:09', '2019-05-20 16:22:09'),
(79, '', 'MREE2SA', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 350, 1, 0, 'https://cdn.bestme.asia/images/x2/apple-macbook-air-mree2sa-a-i5-8gb-128gb-133-gold-400x400_kfIWI2J.jpg', '', 0, 1, 1, '30699000.0000', 1, '31000000.0000', 1, 0, 0, '0000-00-00', '2500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2019-05-20 16:35:06', '2019-05-20 16:35:06'),
(80, '', 'S510UN', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 345, 1, 0, 'https://cdn.bestme.asia/images/x2/asus-s510un-i5-8250u-4gb-1tb-mx150-win10-bq276t-33397-thumb-400x400_HpIbgG2.jpg', '', 0, 8, 1, '16700000.0000', 1, '16700000.0000', 1, 0, 0, '0000-00-00', '2200.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2019-05-20 16:38:08', '2019-05-20 16:38:08'),
(82, '', '14IKB', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 325, 1, 0, 'https://cdn.bestme.asia/images/x2/lenovo-ideapad-530s-14ikb-i7-8550u-8gb-256gb-win10-33397-thumb-400x400_CnHkpE3.jpg', '', 0, 7, 1, '28990000.0000', 1, '29990000.0000', 1, 0, 0, '0000-00-00', '2100.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2019-05-20 16:40:29', '2019-05-20 16:40:29'),
(100, '', 'SKU215', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 420, 1, 0, 'https://cdn.bestme.asia/images/x2/32_y76raJF.png', '', 0, 6, 1, '2150000.0000', 1, '3850000.0000', 1, 0, 0, '0000-00-00', '750.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2019-05-21 15:57:29', '2019-05-21 15:57:29'),
(101, '', 'SKU1999', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 350, 1, 0, 'https://cdn.bestme.asia/images/x2/34_g2abBJv.png', '', 0, 2, 1, '1999000.0000', 1, '2500000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2019-05-21 16:03:41', '2019-05-21 16:03:41'),
(102, '', 'SKU2991', NULL, NULL, NULL, NULL, '0.0000', '', '', '', '', '', '', '', 400, 1, 0, 'https://cdn.bestme.asia/images/x2/38_1GW9Naf.png', '', 0, 25, 1, '250000.0000', 1, '299000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, '2019-05-21 16:13:09', '2019-05-21 16:13:09');

UPDATE `oc_product` SET `deleted` = NULL WHERE `product_id` IN (73, 74, 76, 78, 79, 80, 82, 100, 101, 102) AND `demo` = 1;

-- oc_product_collection
INSERT IGNORE INTO `oc_product_collection` (`product_collection_id`, `product_id`, `collection_id`, `sort_order`) VALUES
(73, 73, 4, 0),
(74, 74, 6, 0),
(131, 76, 5, 0),
(112, 100, 5, 0),
(113, 100, 4, 0),
(114, 100, 6, 0),
(115, 101, 5, 0),
(116, 101, 4, 0),
(117, 101, 6, 0),
(126, 102, 6, 0),
(125, 102, 4, 0),
(124, 102, 5, 0);

-- oc_product_descriptions
INSERT IGNORE INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `sub_description`, `seo_title`, `seo_description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(73, 2, 'Điện thoại iPhone Xs Max 64GB', '&lt;h3&gt;Ho&amp;agrave;n to&amp;agrave;n xứng đ&amp;aacute;ng với những g&amp;igrave; được mong chờ, phi&amp;ecirc;n bản cao cấp nhất IPhone Xs Max&amp;nbsp;của Apple năm nay nổi bật với chip A12 Bionic mạnh mẽ, m&amp;agrave;n h&amp;igrave;nh rộng đến 6.5 inch, c&amp;ugrave;ng camera k&amp;eacute;p tr&amp;iacute; tuệ nh&amp;acirc;n tạo v&amp;agrave; Face ID được n&amp;acirc;ng cấp.&lt;/h3&gt;', '', '', '', '', '', '', ''),
(74, 2, 'Điện thoại Samsung Galaxy Note 8', '&lt;h2&gt;Galaxy Note 8 l&amp;agrave; niềm hy vọng vực lại d&amp;ograve;ng Note danh tiếng của điện thoại SamSung&amp;nbsp;với diện mạo nam t&amp;iacute;nh, sang trọng. M&amp;aacute;y trang bị camera k&amp;eacute;p x&amp;oacute;a ph&amp;ocirc;ng thời thượng, m&amp;agrave;n h&amp;igrave;nh c&amp;ocirc; cực&amp;nbsp;như tr&amp;ecirc;n S8 Plus, b&amp;uacute;t S Pen c&amp;ugrave;ng nhiều t&amp;iacute;nh năng mới v&amp;agrave; nhiều c&amp;ocirc;ng nghệ được ưu &amp;aacute;i.&lt;/h2&gt;', '', '', '', '', '', '', ''),
(76, 2, 'Máy tính bảng iPad Wifi 128 GB (2018)', '&lt;h2&gt;&amp;nbsp;IPad Wifi 128 GB l&amp;agrave; chiếc iPad mới được Apple ra mắt với mức gi&amp;aacute; phải chăng hơn hứa hẹn sẽ mang c&amp;aacute;c thiết bị iPad đến được với đ&amp;ocirc;ng đảo người d&amp;ugrave;ng hơn.&lt;/h2&gt;', '', 'Máy tính bảng iPad Wifi 128 GB (2018)', 'Máy tính bảng iPad Wifi 128 GB (2018)', '', '', '', ''),
(78, 2, 'Máy tính bảng iPad Mini Wifi Cellular 64GB (2019)', '&lt;h2&gt;M&amp;aacute;y t&amp;iacute;nh bảng iPad Mini Wifi Cellular 64GB (2019) đ&amp;aacute;nh dấu sự trở lại mạnh mẽ của Apple trong ph&amp;acirc;n kh&amp;uacute;c m&amp;aacute;y t&amp;iacute;nh bảng nhỏ gọn, c&amp;oacute; thể dễ d&amp;agrave;ng mang theo b&amp;ecirc;n m&amp;igrave;nh.&lt;/h2&gt;', '', 'Máy tính bảng iPad Mini Wifi Cellular 64GB (2019)', 'Máy tính bảng iPad Mini Wifi Cellular 64GB (2019)', '', '', '', ''),
(79, 2, 'Laptop Apple Macbook Air 2018 128GB (MREE2SA)', '&lt;h1&gt;Laptop Apple Macbook Air 2018 i5/8GB/128GB (MREE2SA/A)&lt;/h1&gt;', '', 'Laptop Apple Macbook Air 2018 128GB (MREE2SA)', 'Laptop Apple Macbook Air 2018 128GB (MREE2SA)', '', '', '', ''),
(80, 2, 'Laptop Asus VivoBook S510UN i5 8250U', '&lt;h1&gt;Laptop Asus VivoBook S510UN i5 8250U&lt;/h1&gt;', '', 'Laptop Asus VivoBook S510UN i5 8250U', 'Laptop Asus VivoBook S510UN i5 8250U', '', '', '', ''),
(82, 2, 'Laptop Lenovo Ideapad 530S 14IKB i7 8550U', '&lt;h1&gt;Laptop Lenovo Ideapad 530S 14IKB i7 8550U&lt;/h1&gt;', '', 'Laptop Lenovo Ideapad 530S 14IKB i7 8550U', 'Laptop Lenovo Ideapad 530S 14IKB i7 8550U', '', '', '', ''),
(100, 2, 'ĐIỆN THOẠI SAMSUNG A9 XÁCH TAY', '&lt;p&gt;Th&amp;ocirc;ng số kỹ thuật&lt;/p&gt;\r\n\r\n&lt;p&gt;M&amp;agrave;n h&amp;igrave;nh: Super AMOLED, 6.3&amp;quot;, Full HD+&lt;/p&gt;\r\n\r\n&lt;p&gt;Hệ điều h&amp;agrave;nh: Android 8.0 (Oreo)&lt;/p&gt;\r\n\r\n&lt;p&gt;Camera sau: 24 MP, 10 MP, 8 MP v&amp;agrave; 5 MP (4 camera)&lt;/p&gt;\r\n\r\n&lt;p&gt;Camera trước: 24 MP&lt;/p&gt;\r\n\r\n&lt;p&gt;CPU: Qualcomm Snapdragon 660 8 nh&amp;acirc;n&lt;/p&gt;\r\n\r\n&lt;p&gt;RAM: 6 GB&lt;/p&gt;\r\n\r\n&lt;p&gt;Bộ nhớ trong: 64 GB&lt;/p&gt;\r\n\r\n&lt;p&gt;Thẻ nhớ: MicroSD, hỗ trợ tối đa 512 GB&lt;/p&gt;', '', '', '', '', '', '', ''),
(101, 2, 'ĐIỆN THOẠI OPPO F9 XÁCH TAY CAO CẤP GIÁ RẺ 64GB', '&lt;p&gt;1. Thiết kế gọn g&amp;agrave;ng v&amp;agrave; &amp;ocirc;m tay, viền m&amp;agrave;n h&amp;igrave;nh tr&amp;agrave;n cạnh&lt;/p&gt;\r\n\r\n&lt;p&gt;2. M&amp;agrave;n h&amp;igrave;nh tr&amp;agrave;n viền, 6.3&amp;#39; inch, độ ph&amp;acirc;n giải Full HD&lt;/p&gt;\r\n\r\n&lt;p&gt;3. Cấu h&amp;igrave;nh OPPO F9 vừa đủ d&amp;ugrave;ng zalo,Facebook,youtobe&lt;/p&gt;\r\n\r\n&lt;p&gt;4. Camera selfie ấn tượng : Ch&amp;iacute;nh: 16.0MP; Phụ: 25.0MP 5. Android 8.1 c&amp;ugrave;ng ColorOS 2.5D mới mẻ, nhận diện khu&amp;ocirc;n mặt ....&lt;/p&gt;\r\n\r\n&lt;p&gt;Th&amp;ocirc;ng số cơ bản..&lt;/p&gt;', '', '', '', '', '', '', ''),
(102, 2, 'Tai nghe Gaming G-Net H7S Có Rung, Led 7 màu', '&lt;p&gt;-Tai nghe G-Net H7S l&amp;agrave; phi&amp;ecirc;n bản n&amp;acirc;ng cấp từ d&amp;ograve;ng tai nghe G-Net H7 l&amp;agrave; sản phẩm tai nghe nhận được nhiều sự y&amp;ecirc;u th&amp;iacute;ch từ c&amp;aacute;c game thủ.&lt;/p&gt;\r\n\r\n&lt;p&gt;Đặc điểm nổi bật của Tai nghe G-Net H7S -T&amp;iacute;nh năng ưu việt Tai nghe Gnet H7S Rung Led với thiết kế &amp;acirc;m thanh đỉnh cao c&amp;ugrave;ng chế độ rung theo bass cho bạn kh&amp;ocirc;ng gian nghe nhạc sống động v&amp;agrave; ch&amp;acirc;n thực hơn bao giờ hết&lt;/p&gt;\r\n\r\n&lt;p&gt;Đối với c&amp;aacute;c bản nhạc lớn tai nghe cho ra &amp;acirc;m thanh bass c&amp;ugrave;ng chế độ rung nghe cực đ&amp;atilde; tai. C&amp;aacute;c bạn cần cắm cổng usb v&amp;agrave; chạm v&amp;agrave;o n&amp;uacute;t cảm ứng b&amp;ecirc;n dưới micro để bật chế độ rung mạnh.&lt;/p&gt;\r\n\r\n&lt;p&gt;-Thiết kế độc đ&amp;aacute;o Tai nghe G-Net H7S được thiết kế c&amp;ugrave;ng kiều d&amp;aacute;ng v&amp;agrave; m&amp;agrave;u sắc c&amp;aacute; t&amp;iacute;nh đem lại sự hiện đại, mạnh mẽ rất được c&amp;aacute;c game thủ ưa chuộng Thiết kế hiện đại nhưng sử dụng v&amp;ocirc; c&amp;ugrave;ng thoải m&amp;aacute;i v&amp;agrave; đơn giản.&lt;/p&gt;\r\n\r\n&lt;p&gt;Phần ốp v&amp;agrave; gọng tai nghe đều được ốp kim loại c&amp;oacute; khả năng chống va đập rơi vỡ Thiết kế ho&amp;agrave;n hảo n&amp;agrave;y gi&amp;uacute;p bạn thoải m&amp;aacute;i sử dụng m&amp;agrave; kh&amp;ocirc;ng cần qu&amp;aacute; lo lắng khi bị va chạm.&lt;/p&gt;\r\n\r\n&lt;p&gt;Phần ốp được thiết kế bản to, &amp;ocirc;m trọn tai tạo sự thoải m&amp;aacute;i cho người đeo Chất liệu ốp tai bằng da chất lượng cao rất &amp;ecirc;m kh&amp;ocirc;ng g&amp;acirc;y đau nhức hay b&amp;iacute; tai.&lt;/p&gt;\r\n\r\n&lt;p&gt;Tai nghe G-net H7S c&amp;oacute; Micro được thiết kế nhỏ gọn chắc chắn thuận tiện cho người sử dụng.&lt;/p&gt;\r\n\r\n&lt;p&gt;Tai nghe được thiết kế n&amp;uacute;t điều chỉnh &amp;acirc;m lượng ngay ph&amp;iacute;a sau tai cho người sử dụng c&amp;oacute; thể dễ d&amp;agrave;ng kiểm so&amp;aacute;t &amp;acirc;m thanh theo &amp;yacute; muốn.&lt;/p&gt;\r\n\r\n&lt;p&gt;-Kết nối đơn giản Tai nghe G-Net H7S c&amp;oacute; thể sử dụng đa dạng tr&amp;ecirc;n c&amp;aacute;c thiết bị như M&amp;aacute;y t&amp;iacute;nh, Laptop, Điện thoại..&lt;/p&gt;', '', '', '', '', '', '', '');

-- oc_product_image
INSERT IGNORE INTO `oc_product_image` (`product_image_id`, `product_id`, `image`, `image_alt`, `sort_order`) VALUES
(140, 82, 'https://cdn.bestme.asia/images/x2/lenovo-ideapad-330s-14ikbr-i5-8250u-4gb-1tb-win10-33397-thumb-400x400_LYdC10T.jpg', '', 0),
(142, 82, 'https://cdn.bestme.asia/images/x2/asus-x407ua-i5-8250u-4gb-16gb-1tb-win10-bv485t-thumb33397-400x400_7Cd62PR.jpg', '', 2),
(141, 82, 'https://cdn.bestme.asia/images/x2/asus-x507uf-i5-8250u-4gb-1tb-2gb-mx130-ej121t-thumb-1-400x400_ELBf6Ql.jpg', '', 1),
(120, 100, 'https://cdn.bestme.asia/images/x2/31_HUhDeTb.png', '', 0),
(121, 100, 'https://cdn.bestme.asia/images/x2/30_XKqK0B3.png', '', 1),
(122, 101, 'https://cdn.bestme.asia/images/x2/33_hxbjD7o.png', '', 0),
(130, 102, 'https://cdn.bestme.asia/images/x2/35_4IRx7y1.png', '', 2),
(129, 102, 'https://cdn.bestme.asia/images/x2/36_7WFRSRM.png', '', 1),
(128, 102, 'https://cdn.bestme.asia/images/x2/37_MeojvtY.png', '', 0);

-- oc_product_to_category
INSERT IGNORE INTO `oc_product_to_category` (`product_id`, `category_id`) VALUES
(73, 1),
(74, 1),
(76, 2),
(78, 2),
(80, 3),
(82, 3),
(100, 1),
(101, 1),
(102, 3),
(102, 5),
(102, 6);

-- oc_product_to_store
INSERT IGNORE INTO `oc_product_to_store` (`product_id`, `store_id`) VALUES
(73, 0),
(74, 0),
(76, 0),
(78, 0),
(79, 0),
(80, 0),
(82, 0),
(100, 0),
(101, 0),
(102, 0);

INSERT IGNORE INTO `oc_warehouse` (`warehouse_id`, `product_id`, `product_version_id`) VALUES
(1043, 73, NULL),
(1044, 74, NULL),
(1046, 76, NULL),
(1050, 78, NULL),
(1051, 79, NULL),
(1052, 80, NULL),
(1054, 82, NULL),
(1101, 100, NULL),
(1102, 101, NULL),
(1103, 102, NULL);