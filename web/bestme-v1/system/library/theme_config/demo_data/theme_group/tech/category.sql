-- category
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(1, NULL, 0, 0, 0, 0, 1, '2019-01-09 11:29:42', '2019-01-09 11:29:42'),
(2, NULL, 0, 0, 0, 0, 1, '2019-01-09 15:36:36', '2019-01-09 15:36:36'),
(3, NULL, 0, 0, 0, 0, 1, '2019-01-09 15:36:50', '2019-01-09 15:36:50'),
(4, NULL, 0, 0, 0, 0, 1, '2019-01-09 15:37:11', '2019-01-09 15:37:11'),
(5, NULL, 0, 0, 0, 0, 1, '2019-01-09 15:37:34', '2019-01-09 15:37:34'),
(6, NULL, 0, 0, 0, 0, 1, '2019-01-09 16:02:06', '2019-01-09 16:02:06'),
(7, NULL, 0, 0, 0, 0, 1, '2019-01-09 16:02:24', '2019-01-09 16:02:24'),
(8, NULL, 0, 0, 0, 0, 1, '2019-04-22 09:58:25', '2019-04-22 09:58:25'),
(9, NULL, 0, 0, 0, 0, 1, '2019-05-10 16:14:17', '2019-05-10 16:14:17');

-- oc_category_description
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, 2, 'Điện thoại', 'Điện thoại chất lương tốt nhất thị trường', '', '', ''),
(1, 1, 'Điện thoại', 'Điện thoại chất lương tốt nhất thị trường', '', '', ''),
(2, 2, 'Máy tính bảng', 'Máy tính bảng tốt nhất thị trường', '', '', ''),
(2, 1, 'Máy tính bảng', 'Máy tính bảng tốt nhất thị trường', '', '', ''),
(3, 2, 'Máy tính xách tay', 'Máy tính xách tay tốt nhất thị trường', '', '', ''),
(3, 1, 'Máy tính xách tay', 'Máy tính xách tay tốt nhất thị trường', '', '', ''),
(4, 2, 'Máy tính để bàn', 'Máy tính để bàn tốt nhất thị trường', '', '', ''),
(4, 1, 'Máy tính để bàn', 'Máy tính để bàn tốt nhất thị trường', '', '', ''),
(5, 2, 'Phụ kiện công nghệ', 'Phụ kiện công nghệ hấp dẫn nhất hiện nay', '', '', ''),
(5, 1, 'Phụ kiện công nghệ', 'Phụ kiện công nghệ hấp dẫn nhất hiện nay', '', '', ''),
(6, 2, 'Tai nghe', 'Tai nghe chất lượng cao', '', '', ''),
(6, 1, 'Tai nghe', 'Tai nghe chất lượng cao', '', '', ''),
(7, 2, 'Sạc dự phòng', 'Sạc dự phòng chất lượng cao', '', '', ''),
(7, 1, 'Sạc dự phòng', 'Sạc dự phòng chất lượng cao', '', '', ''),
(8, 2, 'Đồng hồ', 'Đồng hồ', '', '', ''),
(8, 1, 'Đồng hồ', 'Đồng hồ', '', '', ''),
(9, 2, 'Awei', 'Awei', '', '', ''),
(9, 1, 'Awei', 'Awei', '', '', '');

-- oc_category_path
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(1, 1, 0),
(2, 2, 0),
(3, 3, 0),
(4, 4, 0),
(5, 5, 0),
(6, 5, 0),
(6, 6, 1),
(7, 5, 0),
(7, 7, 1),
(8, 8, 0),
(9, 9, 0);

-- oc_category_to_store
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(5, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0);