-- category
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(10, NULL, 0, 0, 0, 0, 1, '2019-05-20 16:05:16', '2019-05-20 16:05:16'),
(11, NULL, 0, 0, 0, 0, 1, '2019-05-20 16:37:28', '2019-05-20 16:37:28'),
(13, NULL, 0, 0, 0, 0, 1, '2019-05-20 16:54:28', '2019-05-20 16:54:28'),
(15, NULL, 0, 0, 0, 0, 1, '2019-05-21 13:49:04', '2019-05-21 13:49:04'),
(16, NULL, 0, 0, 0, 0, 1, '2019-05-21 14:28:02', '2019-05-21 14:28:02');

-- oc_category_description
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(10, 2, 'Sữa dưỡng thể', 'Sữa dưỡng thể', '', '', ''),
(10, 1, 'Sữa dưỡng thể', 'Sữa dưỡng thể', '', '', ''),
(11, 2, 'Sữa rửa mặt', 'Sữa rửa mặt', '', '', ''),
(11, 1, 'Sữa rửa mặt', 'Sữa rửa mặt', '', '', ''),
(13, 2, 'Kem chống nắng', 'Kem chống nắng', '', '', ''),
(13, 1, 'Kem chống nắng', 'Kem chống nắng', '', '', ''),
(15, 2, 'Kem dưỡng da', 'Kem dưỡng da', '', '', ''),
(15, 1, 'Kem dưỡng da', 'Kem dưỡng da', '', '', ''),
(16, 2, 'Dầu gội', 'Dầu gội', '', '', ''),
(16, 1, 'Dầu gội', 'Dầu gội', '', '', '');

-- oc_category_path
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(10, 10, 0),
(11, 11, 0),
(13, 13, 0),
(15, 15, 0),
(16, 16, 0);

-- oc_category_to_store
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(10, 0),
(11, 0),
(13, 0),
(15, 0),
(16, 0);