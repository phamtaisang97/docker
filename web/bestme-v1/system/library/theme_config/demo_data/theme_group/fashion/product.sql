-- oc_product
-- 83
-- 84
-- 85
-- 86
-- 87
-- 88
-- 97
-- 98
-- 99
-- 103

INSERT IGNORE INTO `oc_product` (`product_id`, `model`, `sku`, `barcode`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `sale_on_out_of_stock`, `stock_status_id`, `image`, `image_alt`, `multi_versions`, `manufacturer_id`, `shipping`, `price`, `price_currency_id`, `compare_price`, `c_price_currency_id`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `viewed`, `date_added`, `date_modified`, `demo`) VALUES
(83, '', '', '', '', '', '', '', '', '', 350, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/83/thumb.jpg', '', 0, 1, 1, '', 1, '204000.0000', 1, 0, 0, '0000-00-00', '208.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-20 16:03:08', '2019-05-20 16:03:08', 1),
(84, '', '', '', '', '', '', '', '', '', 320, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/84/thumb.jpg', '', 0, 6, 1, '', 1, '200000.0000', 1, 0, 0, '0000-00-00', '208.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-20 16:07:02', '2019-05-20 16:07:02', 1),
(85, '', 'HUKW3943JJJ', '', '', '', '', '', '', '', 316, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/85/thumb.jpg', '', 0, 1, 1, '210000.0000', 1, '360000.0000', 1, 0, 0, '0000-00-00', '325.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-20 16:12:44', '2019-05-20 16:12:44', 1),
(86, '', 'HUKW3943888', '', '', '', '', '', '', '', 400, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/86/thumb.jpg', '', 0, 1, 1, '150000.0000', 1, '200000.0000', 1, 0, 0, '0000-00-00', '325.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-20 16:22:09', '2019-05-20 16:22:09', 1),
(87, '', 'MREE2SA', '', '', '', '', '', '', '', 350, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/87/thumb.jpg', '', 0, 1, 1, '130000.0000', 1, '220000.0000', 1, 0, 0, '0000-00-00', '2500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-20 16:35:06', '2019-05-20 16:35:06', 1),
(88, '', 'S510UN', '', '', '', '', '', '', '', 345, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/88/thumb.jpg', '', 0, 8, 1, '200000.0000', 1, '320000.0000', 1, 0, 0, '0000-00-00', '2200.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-20 16:38:08', '2019-05-20 16:38:08', 1),
(97, '', '14IKB', '', '', '', '', '', '', '', 325, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/97/thumb.jpg', '', 0, 7, 1, '199000.0000', 1, '360000.0000', 1, 0, 0, '0000-00-00', '2100.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-20 16:40:29', '2019-05-20 16:40:29', 1),
(98, '', 'SKU215', '', '', '', '', '', '', '', 420, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/98/thumb.jpg', '', 0, 6, 1, '230000.0000', 1, '390000.0000', 1, 0, 0, '0000-00-00', '750.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-21 15:57:29', '2019-05-21 15:57:29', 1),
(99, '', 'SKU1999', '', '', '', '', '', '', '', 350, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/99/thumb.jpg', '', 0, 2, 1, '190000.0000', 1, '340000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-21 16:03:41', '2019-05-21 16:03:41', 1),
(103, '', 'SKU2991', '', '', '', '', '', '', '', 400, 1, 0, '/catalog/view/theme/default/image/fashion_shoeszone/products/103/thumb.jpg', '', 0, 25, 1, '', 1, '170000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 0, '2019-05-21 16:13:09', '2019-05-21 16:13:09', 1);

UPDATE `oc_product` SET `deleted` = NULL WHERE `product_id` IN (83, 84, 85, 86, 87, 88, 97, 98, 99, 103) AND `demo` = 1;

-- oc_product_collection
INSERT IGNORE INTO `oc_product_collection` (`product_collection_id`, `product_id`, `collection_id`, `sort_order`) VALUES
(134, 83, 7, 0),
(127, 84, 7, 0),
(130, 85, 7, 0),
(129, 86, 7, 0),
(128, 87, 7, 0),
(100, 97, 5, 0),
(101, 97, 4, 0),
(102, 97, 6, 0),
(103, 98, 5, 0),
(104, 98, 4, 0),
(105, 98, 6, 0),
(111, 99, 6, 0),
(110, 99, 4, 0),
(109, 99, 5, 0),
(121, 103, 5, 0),
(122, 103, 4, 0),
(123, 103, 6, 0);

-- oc_product_descriptions
INSERT IGNORE INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `sub_description`, `seo_title`, `seo_description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(83, 2, 'Giày cổ điển sang trọng màu hồng', 'Giày cổ điển sang trọng màu hồng sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(84, 2, 'Giày cao gót gót kim sa xù độc lạ', '- Cao 8cm - Màu đen và nâu cafe sang trọng - Là kiểu giày mang phong cách cổ điển sang trọng, dễ phối đồ. Đi tiệc hay đi làm đều rất sang. - Size 35-39', '', '', '', '', '', '', ''),
(85, 2, 'Giày cao gót quai trong phối đinh', 'Giày cao gót quai trong phối đinh sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(86, 2, 'Giày cao gót dép đen nơ', 'Giày cao gót dép đen nơ sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(87, 2, 'Giày valen viền đinh size 36 37 38', 'Giày valen viền đinh size 36 37 38 sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(88, 2, 'Giày sandal cao gót quai trong gót kim sa size 34 đến 40', 'Giày sandal cao gót quai trong gót kim sa size 34 đến 40 sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(97, 2, 'Giày sandal quai ngang ánh 7 màu', 'Giày sandal quai ngang ánh 7 màu sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(98, 2, 'Dép sandal xoắn cổ chân', 'Dép sandal xoắn cổ chân sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(99, 2, 'Giày sandal chiến binh dây kéo 1 bên', 'Giày sandal chiến binh dây kéo 1 bên sang trọng, dễ phối đồ', '', '', '', '', '', '', ''),
(103, 2, 'Giày đúp cao 11cm', 'Giày đúp cao 11cm sang trọng, dễ phối đồ', '', '', '', '', '', '', '');

-- oc_product_image
INSERT IGNORE INTO `oc_product_image` (`product_image_id`, `product_id`, `image`, `image_alt`, `sort_order`) VALUES
(139, 83, '/catalog/view/theme/default/image/fashion_shoeszone/products/83/image1.jpg', '', 1),
(138, 84, '/catalog/view/theme/default/image/fashion_shoeszone/products/84/thumb.jpg', '', 0),
(133, 85, '/catalog/view/theme/default/image/fashion_shoeszone/products/85/image1.jpg', '', 0),
(132, 86, '/catalog/view/theme/default/image/fashion_shoeszone/products/86/thumb.jpg', '', 1),
(131, 87, '/catalog/view/theme/default/image/fashion_shoeszone/products/87/image1.jpg', '', 0),
(117, 88, '/catalog/view/theme/default/image/fashion_shoeszone/products/88/image1.jpg', '', 0),
(118, 97, '/catalog/view/theme/default/image/fashion_shoeszone/products/97/thumb.jpg', '', 0),
(119, 98, '/catalog/view/theme/default/image/fashion_shoeszone/products/98/image2.jpg', '', 1),
(126, 99, '/catalog/view/theme/default/image/fashion_shoeszone/products/99/image1.jpg', '', 0),
(127, 103, '/catalog/view/theme/default/image/fashion_shoeszone/products/103/image1.jpg', '', 1);

-- oc_product_to_category
INSERT IGNORE INTO `oc_product_to_category` (`product_id`, `category_id`) VALUES
(83, 12),
(84, 14),
(85, 14),
(86, 14),
(87, 17),
(88, 18),
(97, 18),
(98, 19),
(99, 18),
(99, 19),
(103, 20);

-- oc_product_to_store
INSERT IGNORE INTO `oc_product_to_store` (`product_id`, `store_id`) VALUES
(83, 0),
(84, 0),
(85, 0),
(86, 0),
(87, 0),
(88, 0),
(97, 0),
(98, 0),
(99, 0),
(103, 0);

INSERT IGNORE INTO `oc_warehouse` (`warehouse_id`, `product_id`, `product_version_id`) VALUES
(1055, 83, NULL),
(1074, 84, NULL),
(1078, 85, NULL),
(1079, 86, NULL),
(1080, 87, NULL),
(1081, 88, NULL),
(1098, 97, NULL),
(1099, 98, NULL),
(1100, 99, NULL),
(1104, 103, NULL);