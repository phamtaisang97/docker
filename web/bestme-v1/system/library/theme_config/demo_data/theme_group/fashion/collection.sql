-- oc_collection
INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`) VALUES
(4, 'Sản phẩm khuyến mại', 0, 1, ''),
(5, 'Sản phẩm hot', 0, 1, ''),
(6, 'Sản phẩm mới', 0, 1, ''),
(7, 'Hàng ngày', 0, 1, NULL);

-- oc_collection_description
INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(4, '/catalog/view/theme/default/image/fashion_shoeszone/products/83/thumb.jpg', '&lt;p&gt;sản phẩm khuyến mại&lt;/p&gt;', '', '', ''),
(5, '/catalog/view/theme/default/image/fashion_shoeszone/products/84/thumb.jpg', '&lt;p&gt;sản phẩm hot&lt;/p&gt;', '', '', ''),
(6, '/catalog/view/theme/default/image/fashion_shoeszone/products/85/thumb.jpg', '&lt;p&gt;nhũng sản phẩm mới nhất c&amp;oacute; mặt tại cửa h&amp;agrave;ng&lt;/p&gt;', '', '', ''),
(7, '/catalog/view/theme/default/image/fashion_shoeszone/products/86/thumb.jpg', '&lt;p&gt;Bộ sưu tập h&amp;agrave;ng ng&amp;agrave;y&lt;/p&gt;', '', '', '');