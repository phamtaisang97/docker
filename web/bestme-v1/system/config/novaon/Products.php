<?php
/* products */
class novaon_dimensions{
	public $length = 0;
	public $width = 0;
	public $height = 0;
	public function __construct($agrs = []){
		$agrs = (array)$agrs;
		if(is_array($agrs) && count($agrs)){
			if(isset($agrs['length']))
				$this->length = ($agrs['length']);
			if(isset($agrs['width']))
				$this->width = ($agrs['width']);
			if(isset($agrs['height']))
				$this->height = ($agrs['height']);
		}
	}
}

class novaon_categories{
	public $id = 0;
	public $name = '';
	public $language_id = 0;
	public function __construct($agrs = []){
		$agrs = (array)$agrs;
		if(is_array($agrs) && count($agrs)){
			if(isset($agrs['id']))
				$this->id = intval($agrs['id']);
			if(isset($agrs['name']))
				$this->name = ($agrs['name']);
			if(isset($agrs['language_id']))
				$this->language_id = intval($agrs['language_id']);
		}
	}
}
/* novaon_option get from table option */
class novaon_option{
	public $product_option_id = 0;
	public $product_option_value = [];
	public $option_id = 0;
	public $name = 0;
	public $type = 0;
	public $value = 0;
	public $required = 0;
	
	public function __construct($agrs = []){
		$agrs = (array)$agrs;
		if(is_array($agrs) && count($agrs)){
			if(isset($agrs['product_option_id']))
				$this->product_option_id = intval($agrs['product_option_id']);
			if(isset($agrs['product_option_value']))
				$this->product_option_value = ($agrs['product_option_value']);
			if(isset($agrs['option_id']))
				$this->option_id = intval($agrs['option_id']);
			if(isset($agrs['name']))
				$this->name = intval($agrs['name']);
			if(isset($agrs['type']))
				$this->type = intval($agrs['type']);
			if(isset($agrs['value']))
				$this->value = intval($agrs['value']);
			if(isset($agrs['required']))
				$this->required = intval($agrs['required']);
		}
	}
}
class novaon_attributes{
	public $id = 0;
	public $name = '';
	public $position = 0;
	public $visible = 0;
	public $options = [];
	public $language_id = 0;
	public function __construct($agrs = []){
		$agrs = (array)$agrs;
		if(is_array($agrs) && count($agrs)){
			if(isset($agrs['id']))
				$this->id = intval($agrs['id']);
			if(isset($agrs['name']))
				$this->name = ($agrs['name']);
			if(isset($agrs['position']))
				$this->position = intval($agrs['position']);
			if(isset($agrs['visible']))
				$this->visible = intval($agrs['visible']);
			if(isset($agrs['language_id']))
				$this->language_id = intval($agrs['language_id']);
			if(isset($agrs['options']))
				$this->options = ($agrs['options']);
		}
	}
	public function pushOption($array = []){
		$array = (array)$array;
		array_push( $this->options, new novaon_option($array) );
	}
}
/* novaon_meta_dt get from table attribute */
class novaon_meta_data{
	public $id = 0;
	public $key = '';
	public $value = '';
	public $language_id = 0;
	public function __construct($agrs = []){
		$agrs = (array)$agrs;
		if(is_array($agrs) && count($agrs)){
			if(isset($agrs['id']))
				$this->id = intval($agrs['id']);
			if(isset($agrs['key']))
				$this->key = ($agrs['key']);
			else if(isset($agrs['attribute_key']))
				$this->key = ($agrs['attribute_key']);
			if(isset($agrs['value']))
				$this->value = ($agrs['value']);
			if(isset($agrs['language_id']))
				$this->language_id = intval($agrs['language_id']);
		}
	}
}

class novaon_Product{
	public $id = 0;
	public $name = '';
	public $link;
	public $image_Link;
	public $additional_image_link;
	public $date_created;
	public $date_created_gmt;
	public $date_modified;
	public $date_modified_gmt;
	public $availability_date_gmt;
	public $status;
	public $short_description;
	public $sku;
	public $manufacturer;
	public $upc;
	public $gtin;
	public $parent_id = 0;
	public $sale_price;
	public $price;
	public $date_on_sale_from;
	public $date_on_sale_from_gmt;
	public $date_on_sale_to;
	public $date_on_sale_to_gmt;
	public $availability_date;
	public $stock_quantity = 0;
	public $in_stock = true;
	public $weight;
	public $dimensions;
	public $categories = [];
	public $attributes = [];
	public $meta_data = [];
	public $language_id = 0;
	
	public function __construct($agrs = []){
		$agrs = (array)$agrs;
		if( isset($agrs['id']) )
			$this->id = intval($agrs['id']);
			
		if( isset($agrs['name']) )
			$this->name = ($agrs['name']);
			
		if( isset($agrs['link']) )
			$this->link = ($agrs['link']);
			
		if( isset($agrs['image_Link']) )
			$this->image_Link = ($agrs['image_Link']);
			
		if( isset($agrs['additional_image_link']) )
			$this->additional_image_link = ($agrs['additional_image_link']);
			
		if( isset($agrs['date_created']) )
			$this->date_created = date("Y/m/d H:i:s", strtotime($agrs['date_created']));
	
		if( isset($agrs['date_created']) )
			$this->date_created_gmt = gmdate("D, Y/m/d H:i:s", strtotime($agrs['date_created']))." GMT";
			
		if( isset($agrs['date_modified']) )
			$this->date_modified = date("Y/m/d H:i:s", strtotime($agrs['date_modified']));

        if( isset($agrs['date_modified']) )
            $this->date_modified = date("Y/m/d H:i:s", strtotime($agrs['date_modified']));
			
		if( isset($agrs['availability_date']) )
			$this->date_modified_gmt = gmdate("D, Y/m/j H:i:s", strtotime($agrs['availability_date']))." GMT";
		
		if( isset($agrs['status']) )
			$this->status = ($agrs['status']);
		
		if( isset($agrs['short_description']) )
			$this->short_description = ($agrs['short_description']);
		
		if( isset($agrs['sku']) )
			$this->sku = ($agrs['sku']);
		
		if( isset($agrs['manufacturer']) )
			$this->manufacturer = ($agrs['manufacturer']);
		
		if( isset($agrs['gtin']) )
			$this->gtin = ($agrs['gtin']);
		
		if( isset($agrs['parent_id']) )
			$this->parent_id = intval($agrs['parent_id']);
		
		if( isset($agrs['sale_price']) )
			$this->sale_price = ($agrs['sale_price']);
		
		if( isset($agrs['price']) )
			$this->price = ($agrs['price']);
		
		if( isset($agrs['date_on_sale_from']) )
			$this->date_on_sale_from = ($agrs['date_on_sale_from']);
		
		if( isset($agrs['date_on_sale_from_gmt']) )
			$this->date_on_sale_from_gmt = ($agrs['date_on_sale_from_gmt']);
		
		if( isset($agrs['date_on_sale_to']) )
			$this->date_on_sale_to = ($agrs['date_on_sale_to']);
		
		if( isset($agrs['date_on_sale_to_gmt']) )
			$this->date_on_sale_to_gmt = ($agrs['date_on_sale_to_gmt']);
		
		if( isset($agrs['availability_date']) )
			$this->availability_date = ($agrs['availability_date']);
		
		if( isset($agrs['stock_quantity']) )
			$this->stock_quantity = intval($agrs['stock_quantity']);
		
		if( isset($agrs['in_stock']) )
			$this->in_stock = ($agrs['in_stock']);
		
		if( isset($agrs['weight']) )
			$this->weight = ($agrs['weight']);
		
		
		if( isset($agrs['categories']) )
			$this->categories = ($agrs['categories']);
		
		if( isset($agrs['attributes']) )
			$this->attributes = ($agrs['attributes']);
		
		if( isset($agrs['meta_data']) )
			$this->meta_data = ($agrs['meta_data']);
		
		if( isset($agrs['language_id']) )
			$this->language_id = intval($agrs['language_id']);
	}
	
	public function set($field, $name=null){
		if($name != null)
			$this->$field = htmlspecialchars_decode($name);
	}
	public function setInt($field, $name=null){
		if($name != 0)
			$this->$field = intval($name);
	}
	public function setString($field, $name=null){
		if($name!=null && !empty($name))
			$this->$field = $name;
	}
	public function setDate($field, $name=null){
		$fieldgmt = $field.'_gmt';
		if($name != null && strtotime($name) > strtotime('0000-00-00 00:00:00')){
			$this->$field = date('Y-m-d H:i:s', strtotime($name));
			$this->$fieldgmt = gmdate('D, Y-m-j H:i:s', strtotime($name))." GMT";
		}else{
			$this->$field = null;
			$this->$fieldgmt;
		}
	}
	public function setObject($field, $name=null){
		//print_r($name);
		$obj = 'novaon_'.$field;
		if(is_array($name) && count($name))
			$this->$field = new $obj($name);
		else if(is_object($name))
			$this->$field = $name;
	}
	public function setArray($field, $name=null){
		if(is_array($name) && count($name))
			$this->$field = $name;
	}
	public function pushArray($field, $name = []){
		$obj = 'novaon_'.$field;
		if(class_exists($obj)){
			$name = new $obj($name);
		}
		if( is_array($this->$field) )
			array_push( $this->$field, $name );
	}
}

class novaon_Products{
	public $products = [];
	public function push(novaon_Product $obj){
		if(is_object($obj))
			array_push( $this->products, $obj );
	}
}

/* Attributes */
class novaon_LAttribute{
	public $id = 0;
	public $name = '';
	public $language_id = 0;
	public function __construct($agrs = []){
		$agrs = (array)$agrs;
		if(is_array($agrs) && count($agrs)){
			if(isset($agrs['id']))
				$this->id = intval($agrs['id']);
			if(isset($agrs['name']))
				$this->name = htmlspecialchars_decode($agrs['name']);
			if(isset($agrs['language_id']))
				$this->language_id = intval($agrs['language_id']);
		}
	}
}

class novaon_LAttributes{
	public $Attributes = [];
	public function __construct($agrs = []){
		if(is_object($agrs))
			$this->Attributes = [$agrs];
	}
	
	public function push($agrs = []){
		if(is_object($agrs))
			array_push($this->Attributes, $agrs);
	}
}

/* Categories */

class novaon_LCategory{
	public $id = 0;
	public $name = '';
	public $parentid = 0;
	public $date_created;
	public $date_created_gmt;
	public $date_modified;
	public $date_modified_gmt;
	public $language_id = 0;
	public function __construct($agrs = []){
		$agrs = (array)$agrs;
		if(is_array($agrs) && count($agrs)){
			if(isset($agrs['id']))
				$this->id = intval($agrs['id']);
			else
				$this->id = intval($agrs['category_id']);
			if(isset($agrs['name']))
				$this->name = htmlspecialchars_decode($agrs['name']);
			if(isset($agrs['date_created']))
				$this->date_created = date("Y-m-d H:i:s", strtotime($agrs['date_created']));
			else
				$this->date_created = date("Y-m-d H:i:s", strtotime($agrs['date_added']));
			if(isset($agrs['date_created']))
				$this->date_created_gmt = gmdate("D, Y-m-j H:i:s", strtotime($agrs['date_created']))." GMT";
			else
				$this->date_created_gmt = gmdate("D, Y-m-j H:i:s", strtotime($agrs['date_added']))." GMT";
			if(isset($agrs['date_modified']))
				$this->date_modified = date("Y-m-d H:i:s", strtotime($agrs['date_modified']));
			if(isset($agrs['date_modified']))
				$this->date_modified_gmt = gmdate("D, Y-m-j H:i:s", strtotime($agrs['date_modified']))." GMT";
			if(isset($agrs['language_id']))
				$this->language_id = intval($agrs['language_id']);
			// if(isset($agrs['parent_id']))
				// $this->parent_id = intval($agrs['parent_id']);
			if(isset($agrs['parent_id']))
				$this->parentid = intval($agrs['parent_id']);
		}
	}
}

class novaon_LCategories{
	public $categories = [];
	public function __construct($agrs = []){
		if(is_object($agrs))
			$this->categories = [$agrs];
	}
	
	public function push($agrs = []){
		if(is_object($agrs))
			array_push($this->categories, $agrs);
	}
}

/* Languages */

class novaon_LLanguage{
	public $language_id = 0;
	public $name = '';
	public $code = 0;
	public $status = 0;
	public function __construct($agrs = []){
		$agrs = (array)$agrs;
		if(is_array($agrs) && count($agrs)){
			if(isset($agrs['language_id']))
				$this->language_id = intval($agrs['language_id']);
			if(isset($agrs['name']))
				$this->name = htmlspecialchars_decode($agrs['name']);
			if(isset($agrs['code']))
				$this->code = intval($agrs['code']);
			if(isset($agrs['status']))
				$this->status = intval($agrs['status']);
		}
	}
}

class novaon_LLanguages{
	public $languages = [];
	public function __construct($agrs = []){
		if(is_object($agrs))
			$this->languages = [$agrs];
	}
	
	public function push($agrs = []){
		if(is_object($agrs))
			array_push($this->languages, $agrs);
	}
}

/* ProductAttributes */

class novaon_LProductAttribute{
	public $id = 0;
	public $name = '';
	public $description = 0;
	public $language_id = 0;
	public function __construct($agrs = []){
		$agrs = (array)$agrs;
		if(is_array($agrs) && count($agrs)){
			if(isset($agrs['id']))
				$this->id = intval($agrs['id']);
			if(isset($agrs['name']))
				$this->name = htmlspecialchars_decode($agrs['name']);
			if(isset($agrs['description']))
				$this->description = htmlspecialchars_decode($agrs['description']);
			if(isset($agrs['language_id']))
				$this->language_id = intval($agrs['language_id']);
		}
	}
}

class novaon_LProductAttributes{
	public $ProductAttribute = [];
	public function __construct($agrs = []){
		if(is_object($agrs))
			$this->ProductAttribute = [$agrs];
	}
	
	public function push($agrs = []){
		if(is_object($agrs))
			array_push($this->ProductAttribute, $agrs);
	}
}

?>