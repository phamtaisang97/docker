<?php
function token($length = 32) {
	// Create random token
	$string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

	$max = strlen($string) - 1;

	$token = '';

	for ($i = 0; $i < $length; $i++) {
		$token .= $string[mt_rand(0, $max)];
	}

	return $token;
}

function cloudinary_change_http_to_https($url)
{
    if (strpos($url, 'https') !== 0 && strpos($url, 'http://res.cloudinary.com') === 0) {
        $url = preg_replace('/http/', 'https', $url, 1);
    }

    return $url;
}


/**
 * Backwards support for timing safe hash string comparisons
 *
 * http://php.net/manual/en/function.hash-equals.php
 */

if(!function_exists('hash_equals')) {
	function hash_equals($known_string, $user_string) {
		$known_string = (string)$known_string;
		$user_string = (string)$user_string;

		if(strlen($known_string) != strlen($user_string)) {
			return false;
		} else {
			$res = $known_string ^ $user_string;
			$ret = 0;

			for($i = strlen($res) - 1; $i >= 0; $i--) $ret |= ord($res[$i]);

			return !$ret;
		}
	}
}

if (!function_exists('number_format')) {
    function number_format($number, $decimal_place, $decimal_point, $thousand_point) {
        return number_format($number, (int)$decimal_place, $decimal_point, $thousand_point);
    }
}

if (!function_exists('extract_number')) {
    function extract_number($string, $decimal_point = '.') {
        return preg_replace("/[^0-9{$decimal_point}]/", '', $string);
    }
}

if (!function_exists('convertDate')) {
    function convertDate($date) {
        return date('d/m/Y H:i:s',strtotime($date));
    }
}

if (!function_exists('convertAddressCustomer')) {
    function convertAddressCustomer($address, $province, $district, $ward, $key = '-') {
        return $address.$key.$ward.$key.$district.$key.$province;
    }
}

if (!function_exists('convertAddressOrderList')) {
    function convertAddressOrderList($address, $province, $district, $ward, $key = ', ') {
        $address = (!empty($address) ? $address : '');
        $province = (!empty($province) ? $key.$province : '');
        $district = (!empty($district) ? $key.$district : '');
        $ward = (!empty($ward) ? $key.$ward : '');
        return $address.$ward.$district.$province;
    }
}

if (!function_exists('convertAddressPrint')) {
    function convertAddressPrint($address) {
        return  str_replace(",",", ",$address);
    }
}

if (!function_exists('convertWeight')) {
    function convertWeight($mass) {
        return  str_replace(",","",$mass);
    }
}

if (!function_exists('getPercent')) {
    function getPercent($price, $compare_price, $max_percent_pv = 0, $percent_sign = "%") {
        if ((int)$max_percent_pv != 0) {
            return round($max_percent_pv) . $percent_sign;
        }

        if ((int)$price == 0) {
            return '';
        }

        if ($compare_price  == 0 ) {
            return '';
        }

        if ($price == $compare_price) {
            return '';
        }

        $percent = ($price - $compare_price) / $compare_price * 100;

        return round($percent) . $percent_sign;
    }
}

if (!function_exists('modQuery')) {
    function modQuery($url, $key) {
        $url = preg_replace('/(?:&|(\?))' .$key .'=[^&]*(?(1)&|)?/i',"$1",$url);
        return $url;
    }
}

if(!function_exists('executeSqlFileDueToShopDBPrefix')) {
    /**
     * execute Sql Due To Shop DBPrefix. I.e replace oc_ with DB_PREFIX value. Notice: clone code from install/
     *
     * @param string $file sql file path
     * @param DB $db DB instance
     */
    function executeSqlFileDueToShopDBPrefix($file, $db)
    {
        if (!file_exists($file)) {
            exit('Could not load sql file: ' . $file);
        }

        $lines = file($file);

        if ($lines) {
            $sql = '';

            foreach ($lines as $line) {
                if ($line && (substr($line, 0, 2) != '--') && (substr($line, 0, 1) != '#')) {
                    $sql .= $line;

                    if (preg_match('/;\s*$/', $line)) {
                        $sql = str_replace("DROP TABLE IF EXISTS `oc_", "DROP TABLE IF EXISTS `" . DB_PREFIX, $sql);
                        $sql = str_replace("CREATE TABLE `oc_", "CREATE TABLE `" . DB_PREFIX, $sql);
                        $sql = str_replace("INSERT INTO `oc_", "INSERT INTO `" . DB_PREFIX, $sql);
                        // more add for ALTER:
                        $sql = str_replace("CREATE TABLE IF NOT EXISTS `oc_", "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX, $sql);
                        $sql = str_replace("ALTER TABLE `oc_", "ALTER TABLE `" . DB_PREFIX, $sql);
                        $sql = str_replace("INSERT IGNORE INTO `oc_", "INSERT IGNORE INTO `" . DB_PREFIX, $sql);
                        $sql = str_replace("TRUNCATE TABLE `oc_", "TRUNCATE TABLE `" . DB_PREFIX, $sql);
                        $sql = str_replace("UPDATE `oc_", "UPDATE `" . DB_PREFIX, $sql);
                        $sql = str_replace("DELETE FROM `oc_", "DELETE FROM `" . DB_PREFIX, $sql);
                        $sql = str_replace("DELETE FROM oc_", "DELETE FROM " . DB_PREFIX, $sql);

                        // for sub query
                        $sql = str_replace(" FROM `oc_", " FROM `" . DB_PREFIX, $sql);
                        $sql = str_replace(" FROM oc_", " FROM " . DB_PREFIX, $sql);

                        $db->query($sql);
                        $sql = '';
                    }
                }
            }
        }
    }
}

if(!function_exists('loadDemoDataForTheme')) {
    /**
     * load Demo Data For Theme. Notice: clone code from install/
     *
     * @param string $file sql file path
     * @param DB $db DB instance
     */
    function loadDemoDataForTheme($file, $db)
    {
        if (!file_exists($file)) {
            // try default file
            $file = DIR_SYSTEM . 'library/theme_config/demo_data/tech.sql';
            if (!file_exists($file)) {
                exit('Could not load sql file: ' . $file);
            }
        }

        executeSqlFileDueToShopDBPrefix($file, $db);
    }
}

if(!function_exists('deleteDemoDataForTheme')) {
    /**
     * delete Demo Data For Theme. Notice: clone code from install/
     *
     * @param string $file sql file path
     * @param DB $db DB instance
     */
    function deleteDemoDataForTheme($file, $db)
    {
        executeSqlFileDueToShopDBPrefix($file, $db);
    }
}

function random_bytes_php56($length)
{
    if (function_exists('random_bytes')) {
        // Notice: random_bytes or random_int in php7 only!
        return substr(bin2hex(random_bytes($length)), 0, $length);
    }

    if (function_exists('openssl_random_pseudo_bytes')) {
        // Notice: since php 5.3.0
        return substr(bin2hex(openssl_random_pseudo_bytes($length)), 0, $length);
    }

    // TODO: other function to use $length param above?...
    return sprintf("%s", rand(0, 9999999999));
}

/* Extract fullname to firstname(Tên) and lastname(Họ) */
function extract_name($fullname)
{
    $fullname = sprintf('"%s"', $fullname);
    $fullname = json_decode($fullname);
    $fullname = trim($fullname);
    $delimiter = " ";
    $parts = explode($delimiter, $fullname);
    /* get first element from array */
    $lastname = array_shift($parts);
    if (count($parts) > 0) {
        $firstname = implode($delimiter, $parts);
    } else {
        $firstname = $lastname;
    }
    return array($firstname, $lastname);
}

/* Combine firstname(Tên) and lastname(Họ) to fullname*/
function combine_name($firstname, $lastname)
{
    $firstname = sprintf('"%s"', $firstname);
    $firstname = json_decode($firstname);
    $firstname = trim($firstname);
    $lastname = sprintf('"%s"', $lastname);
    $lastname = json_decode($lastname);
    $lastname = trim($lastname);
    $delimiter = " ";
    $fullname = implode($delimiter, array($lastname, $firstname));
    return $fullname;
}

/*
 * NKT: show error page on un-couched system exception
 */
function get_err_content(Exception $exception, $http_err_code)
{
    $content = '';
    if (defined('MUL_ERR_FILEPATH')) {
        $file = MUL_ERR_FILEPATH . $http_err_code . '.html';
        if (file_exists($file)) {
            $content = file_get_contents($file);

            // more detail to log to error page
            $console_debug_message = sprintf('Detail error: \n - %s \n - %s \n - %s \n - %s \n',
                'Code: ' . $exception->getCode(),
                'File: ' . $exception->getFile(),
                'Line: ' . $exception->getLine(),
                'Message: ' . json_encode($exception->getMessage(), JSON_HEX_APOS));
            $content = str_replace('$$CONSOLE_DEBUG$$', $console_debug_message, $content);
        }
    }

    return $content;
}

function show_http_error(Exception $exception, $http_err_code)
{
    $err_content = get_err_content($exception, $http_err_code);
    if ($err_content) {
        echo $err_content;
        die;
    }
}

function os_log($log)
{
    if (defined('MUL_PRODUCTION_LOG') && MUL_PRODUCTION_LOG == TRUE) {
        $time = time();
        $log = date('Y-m-d H:i:s: ', $time) . $log;
        try {
            if (defined('MUL_PRODUCTION_LOG_FILE')) {
                file_put_contents(MUL_PRODUCTION_LOG_FILE, $log . PHP_EOL, FILE_APPEND | LOCK_EX);
            }
        } catch (Exception $e) {
            // don't care...
        }
    }
}

if (defined('MUL_PRODUCTION_LOG') && MUL_PRODUCTION_LOG == TRUE) {
    function os_exception_handler($exception)
    {
        os_log($exception);
        show_http_error($exception, 500);
    }

    set_exception_handler('os_exception_handler');
}

if(!function_exists('pickChildrenFromFlatArray')) {
    /**
     * pickChildrenFromFlatArray
     *
     * @param array $flat_array
     * @param string $parent_id
     * @param int $deep
     * @param string $ITEM_KEY
     * @param string $PARENT_KEY
     * @param string $CHILDREN_KEY
     * @return array
     */
    function pickChildrenFromFlatArray(array $flat_array, $parent_id, $deep = 1, $ITEM_KEY = 'id', $PARENT_KEY = 'parent_id', $CHILDREN_KEY = 'children')
    {
        $children = [];
        foreach ($flat_array as $el) {
            if (!isset($el[$ITEM_KEY]) || !isset($el[$PARENT_KEY])) {
                continue;
            }

            if ($parent_id == $el[$PARENT_KEY]) {
                if (!isset($el[$CHILDREN_KEY])) {
                    $el[$CHILDREN_KEY] = [];
                }

                if ($deep > 1) {
                    $el[$CHILDREN_KEY] = pickChildrenFromFlatArray($flat_array, $el[$ITEM_KEY], $deep - 1, $ITEM_KEY, $PARENT_KEY, $CHILDREN_KEY);
                }

                $children[] = $el;
            }
        }

        return $children;
    }
}

if(!function_exists('buildNestedArrayWithRelationShipFromFlatArray')) {
    /**
     * buildNestedArrayWithRelationShipFromFlatArray
     *
     * @param array $flat_array
     * @param int $deep
     * @param string $ITEM_KEY
     * @param string $PARENT_KEY
     * @param string $CHILDREN_KEY
     * @return array
     */
    function buildNestedArrayWithRelationShipFromFlatArray(array $flat_array, $deep = 3, $ITEM_KEY = 'id', $PARENT_KEY = 'parent_id', $CHILDREN_KEY = 'children')
    {
        /* pick root lv */
        $tree = [];
        foreach ($flat_array as $el) {
            if (!isset($el[$ITEM_KEY]) || !isset($el[$PARENT_KEY])) {
                continue;
            }

            if (empty($el[$PARENT_KEY])) {
                $el[$CHILDREN_KEY] = [];
                $tree[] = $el;
            }
        }

        /* pick sub (recursive) to root */
        foreach ($tree as &$item) {
            $item[$CHILDREN_KEY] = pickChildrenFromFlatArray($flat_array, $item[$ITEM_KEY], $deep - 1, $ITEM_KEY, $PARENT_KEY, $CHILDREN_KEY);
        }
        unset($item);

        return $tree;
    }
}

if(!function_exists('getValueByKey')) {
    /**
     * getValueByKey
     *
     * @param mixed|array $data
     * @param mixed $key
     * @param null|mixed $default_value
     * @return mixed|null
     */
    function getValueByKey($data, $key, $default_value = null)
    {
        if (is_array($data)) {
            return array_key_exists($key, $data) ? $data[$key] : $default_value;
        }

        return isset($data[$key]) ? $data[$key] : $default_value;
    }
}

if (!function_exists('escapeVietnamese')) {
    function escapeVietnamese($str)
    {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        // $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        // $str = preg_replace('/([\s]+)/', '-', $str);

        return $str;
    }
}

if (!function_exists('modifyUrlParameters')) {
    /**
     * modify url parameters
     *
     * @param $modify_parameters
     * @param false $url
     * @return string
     */
    function modifyUrlParameters($modify_parameters, $url = false)
    {
        // If $url wasn't passed in, use the current url
        if ($url == false) {
            $scheme = $_SERVER['SERVER_PORT'] == 80 ? 'http' : 'https';
            $url = $scheme . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        }

        // Parse the url into pieces
        $url_array = parse_url($url);

        if (empty($url_array['query'])) {
            // The original URL didn't have a query string, add it.
            $query_string = $modify_parameters;
        } else {
            // The original URL had a query string, modify it.
            parse_str($url_array['query'], $query_array);
            foreach ($query_array as $key => $value) {
                if (!empty($value)) {
                    $query_array[$key] = $value;
                }
            }

            $query_string = http_build_query($query_array);
        }

        $port = empty($url_array['port']) ? '' : $url_array['port'];
        $path = empty($url_array['path']) ? '' : $url_array['path'];

        return $url_array['scheme'] . '://' . $url_array['host'] . ':' . $port . ($path) . '?' . $query_string;
    }
}

if (!function_exists('minifyCss')) {
    /**
     * Minify css string content
     *
     * @param $input
     * @return array|mixed|string|string[]|null
     */
    function minifyCss($input)
    {
        if (trim($input) === "") return $input;
        return preg_replace(
            array(
                // Remove comment(s)
                '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')|\/\*(?!\!)(?>.*?\*\/)|^\s*|\s*$#s',
                // Remove unused white-space(s)
                '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/))|\s*+;\s*+(})\s*+|\s*+([*$~^|]?+=|[{};,>~]|\s(?![0-9\.])|!important\b)\s*+|([[(:])\s++|\s++([])])|\s++(:)\s*+(?!(?>[^{}"\']++|"(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')*+{)|^\s++|\s++\z|(\s)\s+#si',
                // Replace `0(cm|em|ex|in|mm|pc|pt|px|vh|vw|%)` with `0`
                '#(?<=[\s:])(0)(cm|em|ex|in|mm|pc|pt|px|vh|vw|%)#si',
                // Replace `:0 0 0 0` with `:0`
                '#:(0\s+0|0\s+0\s+0\s+0)(?=[;\}]|\!important)#i',
                // Replace `background-position:0` with `background-position:0 0`
                '#(background-position):0(?=[;\}])#si',
                // Replace `0.6` with `.6`, but only when preceded by `:`, `,`, `-` or a white-space
                '#(?<=[\s:,\-])0+\.(\d+)#s',
                // Minify string value
                '#(\/\*(?>.*?\*\/))|(?<!content\:)([\'"])([a-z_][a-z0-9\-_]*?)\2(?=[\s\{\}\];,])#si',
                '#(\/\*(?>.*?\*\/))|(\burl\()([\'"])([^\s]+?)\3(\))#si',
                // Minify HEX color code
                '#(?<=[\s:,\-]\#)([a-f0-6]+)\1([a-f0-6]+)\2([a-f0-6]+)\3#i',
                // Replace `(border|outline):none` with `(border|outline):0`
                '#(?<=[\{;])(border|outline):none(?=[;\}\!])#',
                // Remove empty selector(s)
                '#(\/\*(?>.*?\*\/))|(^|[\{\}])(?:[^\s\{\}]+)\{\}#s'
            ),
            array(
                '$1',
                '$1$2$3$4$5$6$7',
                '$1',
                ':0',
                '$1:0 0',
                '.$1',
                '$1$3',
                '$1$2$4$5',
                '$1$2$3',
                '$1:0',
                '$1$2'
            ),
            $input);
    }
}

if (!function_exists('minifyJs')) {
    /**
     * Minify js string content
     *
     * @param $input
     * @return array|string|string[]|null
     */
    function minifyJs($input)
    {
        return \JShrink\Minifier::minify($input);
    }
}