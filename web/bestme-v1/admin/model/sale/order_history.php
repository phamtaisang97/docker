<?php

class ModelSaleOrderHistory extends Model
{
    /* be careful: following values is const only, not is value in lang vi-vn!!! */
    const COMMENT_ADD_ORDER = 'tạo';
    const COMMENT_UPDATE_ORDER = 'cập nhật';
    const COMMENT_CHANGE_STATUS = 'trạng thái';
    const COMMENT_LANG_DRAFT = 'Nháp';
    const COMMENT_LANG_PROCESSING = 'Đang xử lý';
    const COMMENT_LANG_DELIVERING = 'Đang giao hàng';
    const COMMENT_LANG_COMPLETE = 'Hoàn thành';
    const COMMENT_LANG_CANCELED = 'Đã hủy';

    public function addHistory($data)
    {
        $sql = "INSERT INTO " . DB_PREFIX . "order_history SET order_id='" . (int)$data['order_id'] . "', order_status_id='" . (int)$data['order_status_id'] . "',";
        $sql .= " notify='" . (int)$data['notify'] . "', user_name='" . $data['user_name'] . "', user_id='" . (int)$data['user_id'] . "', comment='" . $data['comment'] . "', status='" . (int)$data['status'] . "', date_added=NOW()";
        $this->db->query($sql);
    }

    public function getHistory($order_id)
    {
        $sql = "SELECT oh.status, oh.date_added, CONCAT(u.lastname, ' ', u.firstname) as full_name, os.name as name_status FROM " . DB_PREFIX . "order_history as oh LEFT JOIN " . DB_PREFIX . "order_status as os ON oh.order_status_id = os.order_status_id ";
        $sql .= "LEFT JOIN " . DB_PREFIX . "user as u ON u.user_id = oh.user_id ";
        $sql .= "WHERE oh.order_id = '" . (int)$order_id . "' ORDER BY oh.date_added DESC";
        $query = $this->db->query($sql);
        $order_histories = $query->rows;

        return $this->translateLanguage($order_histories);
    }

    /**
     * translate. This is hard code. TODO: find better way...
     *
     * @param array $order_histories
     * @return array
     */
    private function translateLanguage(array $order_histories)
    {
        $current_lang_code = $this->config->get('config_language');
        $this->load->language('sale/order');

        foreach ($order_histories as &$order_history) {
            if ($current_lang_code === 'vi-vn') {
                (trim($order_history['name_status']) === self::COMMENT_LANG_DRAFT ? $order_history['name_status'] = $this->language->get('lang_text_draft') : '') ;
                (trim($order_history['name_status']) === self::COMMENT_LANG_PROCESSING ? $order_history['name_status'] = $this->language->get('lang_text_processing') : '') ;
                (trim($order_history['name_status']) === self::COMMENT_LANG_DELIVERING ? $order_history['name_status'] = $this->language->get('lang_text_delivering') : '') ;
                (trim($order_history['name_status']) === self::COMMENT_LANG_COMPLETE ? $order_history['name_status'] = $this->language->get('lang_text_complete') : '') ;
                (trim($order_history['name_status']) === self::COMMENT_LANG_CANCELED ? $order_history['name_status'] = $this->language->get('lang_text_canceled') : '') ;
            }

            if ($current_lang_code === 'en-gb') {
                (trim($order_history['name_status']) === self::COMMENT_LANG_DRAFT ? $order_history['name_status'] = $this->language->get('lang_text_draft') : '') ;
                (trim($order_history['name_status']) === self::COMMENT_LANG_PROCESSING ? $order_history['name_status'] = $this->language->get('lang_text_processing') : '') ;
                (trim($order_history['name_status']) === self::COMMENT_LANG_DELIVERING ? $order_history['name_status'] = $this->language->get('lang_text_delivering') : '') ;
                (trim($order_history['name_status']) === self::COMMENT_LANG_COMPLETE ? $order_history['name_status'] = $this->language->get('lang_text_complete') : '') ;
                (trim($order_history['name_status']) === self::COMMENT_LANG_CANCELED ? $order_history['name_status'] = $this->language->get('lang_text_canceled') : '') ;
            }
        }

        unset($order_history);

        return $order_histories;
    }
}