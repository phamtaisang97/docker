<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ModelSaleOrder extends Model
{
    const ORDER_STATUS_ID_DRAFT = 6;
    const ORDER_STATUS_ID_PROCESSING = 7;
    const ORDER_STATUS_ID_DELIVERING = 8;
    const ORDER_STATUS_ID_COMPLETED = 9;
    const ORDER_STATUS_ID_CANCELLED = 10;
    const ORDER_STATUS_ID_DELETED = -1;

    const ORDER_SHIPPING_VALUE_FREE = -1;
    const ORDER_SHIPPING_VALUE_OTHER = -2;

    const ORDER_TAG_NEW_PREFIX = 'NewTag_';

    const ORDER_PAYMENT_METHOD_NEXT = 'Thanh toán trước';
    const APP_CODE_PAYMENT_VNPAY = 'paym_ons_vnpay';

    // voucher
    const VOUCHER_APPLY_FOR_ALL_ORDER = 1;
    const VOUCHER_APPLY_FOR_VALUE_ORDER = 2;
    const VOUCHER_APPLY_FOR_PRODUCT = 3;

    const COUPON_APPLY_FOR = [
        self::VOUCHER_APPLY_FOR_ALL_ORDER => 'Tất cả đơn hàng',
        self::VOUCHER_APPLY_FOR_VALUE_ORDER => 'Giá trị đơn hàng',
        self::VOUCHER_APPLY_FOR_PRODUCT => 'Sản phẩm'
    ];

    use RabbitMq_Trait;

    public function addOrder($data)
    {
        // customer
        if (!isset($data['customer']) || $data['customer'] == '') {
            // if ($data['customer_phone'] == '' || $data['customer_email'] == '') { todo: not check email because email allows empty
            if ($data['customer_phone'] == '') {
                $customer_id = '';
            } else {
                if (isset($data['customer_full_name'])) {
                    $name = extract_name($data['customer_full_name']);
                    $data['customer_firstname'] = $name[0];
                    $data['customer_lastname'] = $name[1];
                }

                $data['customer_district'] = array_key_exists('customer_district', $data) ? $data['customer_district'] : '';
                $data['customer_wards'] = array_key_exists('customer_wards', $data) ? $data['customer_wards'] : '';

                $this->db->query("INSERT INTO " . DB_PREFIX . "customer 
                                    SET firstname = '" . $this->db->escape($data['customer_firstname']) . "', 
                                        lastname = '" . $this->db->escape($data['customer_lastname']) . "', 
                                        email = '" . $this->db->escape($data['customer_email']) . "', 
                                        telephone = '" . $this->db->escape($data['customer_phone']) . "', 
                                        customer_group_id = '" . $this->db->escape($data['customer_group_id']) . "', 
                                        salt = '" . $this->db->escape($salt = token(9)) . "', 
                                        password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1(token(9))))) . "', 
                                        user_create_id = '" . $this->user->getId() . "', 
                                        status = 1,  
                                        date_added = NOW()");

                $customer_id = $this->db->getLastId();

                // update customer code
                $prefix = 'KH';
                $code = $prefix . str_pad($customer_id, 6, "0", STR_PAD_LEFT);

                $this->db->query("UPDATE " . DB_PREFIX . "customer 
                          SET `code` = '" . $code . "' 
                          WHERE customer_id = '" . (int)$customer_id . "'");

                $this->load->model('report/customer');
                $this->model_report_customer->createCustomerReport($customer_id);

                $this->db->query("INSERT INTO " . DB_PREFIX . "address 
                                    SET customer_id = '" . (int)$customer_id . "', 
                                        firstname = '" . $this->db->escape($data['customer_firstname']) . "', 
                                        lastname = '" . $this->db->escape($data['customer_lastname']) . "', 
                                        address = '" . $this->db->escape($data['customer_address']) . "', 
                                        city = '" . $this->db->escape($data['customer_province']) . "', 
                                        district = '" . $this->db->escape($data['customer_district']) . "', 
                                        wards = '" . $this->db->escape($data['customer_wards']) . "', 
                                        phone = '" . $this->db->escape($data['customer_phone']) . "'");

                $address_id = $this->db->getLastId();
                $this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
            }
        } else {
            $customer_id = (int)$data['customer'];
        }

        $data['shipping_fullname'] = trim($data['shipping_fullname']) != '' ? $data['shipping_fullname'] : $data['customer_full_name'];

        $order_status_id = isset($data['submit_action']) && $data['submit_action'] == 'apply' ? self::ORDER_STATUS_ID_PROCESSING : self::ORDER_STATUS_ID_DRAFT;  // 7: đang xử lý, 6: nháp
        $payment_status = isset($data['payment_status']) ? $data['payment_status'] : '0';
        $this->db->query("INSERT INTO `" . DB_PREFIX . "order` 
                          SET fullname = '" . $this->db->escape($data['shipping_fullname']) . "', 
                              shipping_province_code = '" . $this->db->escape($data['shipping_province']) . "', 
                              shipping_district_code = '" . $this->db->escape($data['shipping_district']) . "', 
                              shipping_ward_code = '" . $this->db->escape($data['shipping_wards']) . "', 
                              shipping_address_1 = '" . $this->db->escape($data['shipping_address']) . "', 
                              telephone = '" . $this->db->escape($data['shipping_phone']) . "', 
                              shipping_method = '" . $this->db->escape($data['shipping_method']) . "', 
                              shipping_method_value = '" . $this->db->escape($data['delivery']) . "', 
                              shipping_fee = '" . $this->db->escape($data['shipping_fee']) . "', 
                              order_code = '" . $this->db->escape('xxx') . "', 
                              order_status_id = " . (int)$order_status_id . ", 
                              customer_id = " . (int)$customer_id . ", 
                              payment_method = '" . (array_key_exists('payment_method', $data) ? $this->db->escape($data['payment_method']) : '') . "', 
                              discount = '" . (isset($data['order-discount']) ? $this->db->escape(extract_number($data['order-discount'])) : 0) . "', 
                              total = '" . $this->db->escape($data['total_pay']) . "', 
                              comment = '" . $this->db->escape($data['note']) . "', 
                              user_id = " . (int)$data['user_id'] . ", 
                              date_added = NOW(), 
                              date_modified = NOW()" . ", 
                              payment_status = '" . $payment_status . "', 
                              language_id = '" . (int)$this->config->get('config_language_id') . "'");
        $order_id = $this->db->getLastId();
        //$date = new DateTime();
        //$date = $date->format('Ymd');
        //$order_code = sprintf('%011d', $order_id);
        //$order_code = $date . $order_code;
        // confix Prefix, suffix in order_code
        $this->load->model('setting/setting');

        $config_id_prefix = $this->model_setting_setting->getSettingByKey('config_id_prefix');
        $config_id_prefix = isset($config_id_prefix['value']) ? $config_id_prefix['value'] : '';

        $config_id_suffix = $this->model_setting_setting->getSettingByKey('config_id_suffix');
        $config_id_suffix = isset($config_id_suffix['value']) ? $config_id_suffix['value'] : '';

        $max_order_id = 99999999999;
        $value_order = $max_order_id - ($max_order_id - $order_id);
        $strlen_value = strlen($value_order);
        $number_for = 11 - $strlen_value;
        $number_zero = '0';
        $result = '';
        for ($i = 1; $i <= $number_for; $i++) {
            $result .= $number_zero;
        }
        $order_code = $config_id_prefix . $result . $order_id . $config_id_suffix;
        if(isset($data['order_source']) && $data['order_source']) {
            $this->db->query("UPDATE " . DB_PREFIX . "order SET order_code = '" . $this->db->escape($order_code) . "', source = '".$this->db->escape($data['order_source'])."' WHERE order_id = " . (int)$order_id);
        } else {
            $this->db->query("UPDATE " . DB_PREFIX . "order SET order_code = '" . $this->db->escape($order_code) . "', source = 'admin' WHERE order_id = " . (int)$order_id);
        }

        //tag
        if (isset($data['tag_list'])) {
            $tags = is_array($data['tag_list']) ? $data['tag_list'] : [];
            foreach ($tags as $tag) {
                if (strpos($tag, self::ORDER_TAG_NEW_PREFIX) !== false) {
                    $newTag = str_replace(self::ORDER_TAG_NEW_PREFIX, '', $tag);
                    $sql_update = "INSERT INTO " . DB_PREFIX . "tag(value) VALUES ('" . $this->db->escape($newTag) . "')";
                    $this->db->query($sql_update);
                    $tag_id = $this->db->getLastId();
                    $this->db->query("INSERT INTO " . DB_PREFIX . "order_tag SET order_id = " . (int)$order_id . ", tag_id = " . (int)$tag_id);
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "order_tag SET order_id = " . (int)$order_id . ", tag_id = " . (int)$tag);
                }
            }
        }

        // product_order
        if (isset($data['pids'])) {
            $product_ids = is_array($data['pids']) ? $data['pids'] : [];
            foreach ($product_ids as $key => $ids) {
                $ids = explode('-', $ids);
                $product_id = array_shift($ids);
                $product_version_id = array_shift($ids);
                $price = isset($data['prices'][$key]) ? $data['prices'][$key] : 0;
                $price = str_replace(',', '', $price);
                $quantity = isset($data['quantities'][$key]) ? (int)$data['quantities'][$key] : 0;
                $quantity = str_replace(',', '', $quantity);
                $discount = isset($data['product-discount'][$key]) ? (int)(extract_number($data['product-discount'][$key])) : 0;
                $total = ($price * $quantity) - $discount;
                $this->db->query("INSERT INTO " . DB_PREFIX . "order_product 
                                  SET order_id = " . (int)$order_id . ", 
                                      product_id = " . (int)$product_id . ", 
                                      product_version_id =" . (int)$product_version_id . ", 
                                      quantity = " . $quantity . ", 
                                      price = " . $price . ", 
                                      discount = '" . $discount . "', 
                                      total = '" . $total . "'");
            }
        }

        $this->deleteProductCaches();

        try {
            $this->sendToRabbitMq($order_id);
        } catch (Exception $exception) {
            $this->log->write('Send message order error: ' . $exception->getMessage());
        }

        return $order_id;
    }

    public function editOrder($order_id, $data)
    {
        // TODO move to event: event before edit
        // put back order quantity to stock
        if (isset($data['old_order_status_id'])) {
            $old_order_status_id = $data['old_order_status_id'];
        } else {
            $old_order_status_id = $this->getOrderStatusIdByOrderId($order_id);
        }
        if ($old_order_status_id == self::ORDER_STATUS_ID_PROCESSING) {
            $this->updateProductOrderChangeStatus($order_id, false);
        }

        // customer
        if (!isset($data['customer']) || $data['customer'] == '') {
            // if ($data['customer_phone'] == '' || $data['customer_email'] == '') { todo: not check email because email allows empty
            if ($data['customer_phone'] == '') {
                $customer_id = '';
            } else {
                if (isset($data['customer_full_name'])) {
                    $name = extract_name($data['customer_full_name']);
                    $data['customer_firstname'] = $name[0];
                    $data['customer_lastname'] = $name[1];
                }

                $this->db->query("INSERT INTO " . DB_PREFIX . "customer 
                                    SET firstname = '" . $this->db->escape($data['customer_firstname']) . "', 
                                        lastname = '" . $this->db->escape($data['customer_lastname']) . "', 
                                        email = '" . $this->db->escape($data['customer_email']) . "', 
                                        telephone = '" . $this->db->escape($data['customer_phone']) . "', 
                                        customer_group_id = '" . $this->db->escape($data['customer_group_id']) . "', 
                                        salt = '" . $this->db->escape($salt = token(9)) . "', 
                                        password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1(token(9))))) . "', 
                                        status = 1,  
                                        date_added = NOW()");
                $customer_id = $this->db->getLastId();

                // update customer code
                $prefix = 'KH';
                $code = $prefix . str_pad($customer_id, 6, "0", STR_PAD_LEFT);

                $this->db->query("UPDATE " . DB_PREFIX . "customer 
                          SET `code` = '" . $code . "' 
                          WHERE customer_id = '" . (int)$customer_id . "'");

                $this->db->query("INSERT INTO " . DB_PREFIX . "address 
                                    SET customer_id = '" . (int)$customer_id . "', 
                                        firstname = '" . $this->db->escape($data['customer_firstname']) . "', 
                                        lastname = '" . $this->db->escape($data['customer_lastname']) . "', 
                                        address = '" . $this->db->escape($data['customer_address']) . "', 
                                        city = '" . $this->db->escape($data['customer_province']) . "', 
                                        district = '" . $this->db->escape($data['customer_district']) . "', 
                                        wards = '" . $this->db->escape($data['customer_wards']) . "', 
                                        phone = '" . $this->db->escape($data['customer_phone']) . "'");

                $address_id = $this->db->getLastId();
                $this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
            }
        } else {
            $customer_id = (int)$data['customer'];
        }
        $data['shipping_fullname'] = trim($data['shipping_fullname']) != '' ? $data['shipping_fullname'] : $data['customer_full_name'];

        $order_status_id = (int)$data['order_status'];
        if ($data['payment_status'] == 4 && $data['payment_method'] != 1) {// payment_method =1: thoanh toans quas vnpay, payment_status=4: thanh toán lỗi
            $data['payment_status'] = 0;
        }
        $payment_status = $data['payment_status'];
        $this->db->query("UPDATE `" . DB_PREFIX . "order` 
                          SET fullname = '" . $this->db->escape($data['shipping_fullname']) . "', 
                          shipping_province_code = '" . $this->db->escape($data['shipping_province']) . "', 
                          shipping_district_code = '" . $this->db->escape($data['shipping_district']) . "', 
                          shipping_ward_code = '" . $this->db->escape($data['shipping_wards']) . "', 
                          shipping_address_1 = '" . $this->db->escape($data['shipping_address']) . "', 
                          telephone = '" . $this->db->escape($data['shipping_phone']) . "', 
                          shipping_method = '" . $this->db->escape($data['shipping_method']) . "', 
                          shipping_method_value = '" . (isset($data['delivery']) ? $this->db->escape($data['delivery']) : $this->db->escape("0")) . "', 
                          shipping_fee = '" . $this->db->escape($data['shipping_fee']) . "', 
                          order_status_id = " . (int)$order_status_id . ", 
                          customer_id = " . (int)$customer_id . ", 
                          payment_method = '" . (array_key_exists('payment_method', $data) ? $this->db->escape($data['payment_method']) : '') . "', 
                          discount = '" . (isset($data['order-discount']) ? $this->db->escape(extract_number($data['order-discount'])) : 0) . "', 
                          total = '" . $this->db->escape($data['total_pay']) . "', 
                          customer_ref_id = " . (isset($data['customer_ref_id']) ? ("'" . $this->db->escape($data['customer_ref_id']) . "'") : '`customer_ref_id`') . ", 
                          comment = '" . $this->db->escape($data['note']) . "', 
                          user_id = " . (int)$data['user_id'] . ",
                          date_modified = NOW()" . ",  
                          payment_status = '" . $payment_status . "' WHERE order_id = " . (int)$order_id);

        // notice for above sql: keep old value customer_ref_id (for chatbot) if not set when edit order

        //tag
        $this->db->query("DELETE FROM " . DB_PREFIX . "order_tag WHERE order_id = '" . (int)$order_id . "'");
        if (isset($data['tag_list'])) {
            $tags = is_array($data['tag_list']) ? $data['tag_list'] : [];
            foreach ($tags as $tag) {
                if (strpos($tag, self::ORDER_TAG_NEW_PREFIX) !== false) {
                    $newTag = str_replace(self::ORDER_TAG_NEW_PREFIX, '', $tag);
                    $sql_update = "INSERT INTO " . DB_PREFIX . "tag(value) VALUES ('" . $this->db->escape($newTag) . "')";
                    $this->db->query($sql_update);
                    $tag_id = $this->db->getLastId();
                    $this->db->query("INSERT INTO " . DB_PREFIX . "order_tag SET order_id = " . (int)$order_id . ", tag_id = " . (int)$tag_id);
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "order_tag SET order_id = " . (int)$order_id . ", tag_id = " . (int)$tag);
                }
            }
        }

        // product_order
        $this->db->query("DELETE FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
        if (isset($data['pids'])) {
            $product_ids = is_array($data['pids']) ? $data['pids'] : [];
            foreach ($product_ids as $key => $ids) {
                $ids = explode('-', $ids);
                $product_id = array_shift($ids);
                $product_version_id = array_shift($ids);
                $price = isset($data['prices'][$key]) ? $data['prices'][$key] : 0;
                $price = str_replace(',', '', $price);
                $quantity = isset($data['quantities'][$key]) ? (int)$data['quantities'][$key] : 0;
                $quantity = str_replace(',', '', $quantity);
                $discount = isset($data['product-discount'][$key]) ? (int)(extract_number($data['product-discount'][$key])) : 0;
                $total = $price * $quantity - $discount;
                $this->db->query("INSERT INTO " . DB_PREFIX . "order_product 
                                  SET order_id = " . (int)$order_id . ", 
                                  product_id = " . (int)$product_id . ", 
                                  product_version_id =" . (int)$product_version_id . ", 
                                  quantity = " . $quantity . ", 
                                  price = " . $price . ", 
                                  discount = '" . $discount . "', 
                                  total = '" . $total . "'");
            }
        }

        // update order date_modified
        $sql = "UPDATE " . DB_PREFIX . "order SET date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'";
        $this->db->query($sql);

        // update manual update status
        if (!(isset($data['is_create_order_delivery']) && $data['is_create_order_delivery']) && $order_status_id != $old_order_status_id) {
            $this->updateManualUpdateStatus($order_id);
        }

        if (isset($data['order_source']) && $data['order_source'] == 'shopee') {
            $this->updateOrderShopee($order_id);
        }

        if (isset($data['order_source']) && $data['order_source'] == 'lazada') {
            $this->updateOrderLazada($order_id);
        }

        $this->deleteProductCaches();

        try {
            $this->sendToRabbitMq($order_id);
        } catch (Exception $exception) {
            $this->log->write('Send message order error: ' . $exception->getMessage());
        }

        return $order_id;
    }

    /**
     * @param $order_id
     * @param int $manual_update_status
     */
    public function updateManualUpdateStatus($order_id, $manual_update_status = 1)
    {
        // only update when not integrated with transport service yet
        $sql = "UPDATE " . DB_PREFIX . "order SET `manual_update_status` = $manual_update_status WHERE `order_id` = '" . (int)$order_id . "' AND `transport_order_code` IS NOT NULL";
        $this->db->query($sql);
    }

    public function updateOrderShopee($order_id)
    {
        // 1: is edited, 2 order is update by bestme
        $sql = "UPDATE " . DB_PREFIX . "shopee_order 
                    SET `is_edited` = 1, 
                        `sync_status` = 2 
                    WHERE `bestme_order_id` = '" . (int)$order_id . "'";
        $this->db->query($sql);
    }

    public function updateOrderLazada($order_id)
    {
        // 1: is edited, 2 order is update by bestme
        $sql = "UPDATE " . DB_PREFIX . "lazada_order 
                    SET `is_edited` = 1, 
                        `sync_status` = 2 
                    WHERE `bestme_order_id` = '" . (int)$order_id . "'";
        $this->db->query($sql);
    }

    public function getOrder_print($order_id)
    {
        $order_query = $this->db->query("SELECT *, (SELECT CONCAT(c.customer_id,c.firstname, ' ', c.lastname) FROM " . DB_PREFIX . "customer c WHERE c.customer_id = o.customer_id) AS customer, (SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int)$this->config->get('config_language_id') . "') AS order_status FROM `" . DB_PREFIX . "order` o WHERE o.order_id IN (" . $order_id . ")");

        if ($order_query->num_rows) {
            $all_orders = array();
            foreach ($order_query->rows as $key => $order_query_rows) {
                $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query_rows['payment_country_id'] . "'");
                if ($country_query->num_rows) {
                    $payment_iso_code_2 = $country_query->row['iso_code_2'];
                    $payment_iso_code_3 = $country_query->row['iso_code_3'];
                } else {
                    $payment_iso_code_2 = '';
                    $payment_iso_code_3 = '';
                }

                $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query_rows['payment_zone_id'] . "'");

                if ($zone_query->num_rows) {
                    $payment_zone_code = $zone_query->row['code'];
                } else {
                    $payment_zone_code = '';
                }

                $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query_rows['shipping_country_id'] . "'");

                if ($country_query->num_rows) {
                    $shipping_iso_code_2 = $country_query->row['iso_code_2'];
                    $shipping_iso_code_3 = $country_query->row['iso_code_3'];
                } else {
                    $shipping_iso_code_2 = '';
                    $shipping_iso_code_3 = '';
                }

                $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query_rows['shipping_zone_id'] . "'");

                if ($zone_query->num_rows) {
                    $shipping_zone_code = $zone_query->row['code'];
                } else {
                    $shipping_zone_code = '';
                }

                $reward = 0;

                $order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

                foreach ($order_product_query->rows as $product) {
                    $reward += $product['reward'];
                }

                $this->load->model('customer/customer');

                $affiliate_info = $this->model_customer_customer->getCustomer($order_query_rows['affiliate_id']);

                if ($affiliate_info) {
                    $affiliate_firstname = $affiliate_info['firstname'];
                    $affiliate_lastname = $affiliate_info['lastname'];
                } else {
                    $affiliate_firstname = '';
                    $affiliate_lastname = '';
                }

                $this->load->model('localisation/language');

                $language_info = $this->model_localisation_language->getLanguage($order_query_rows['language_id']);

                if ($language_info) {
                    $language_code = $language_info['code'];
                } else {
                    $language_code = $this->config->get('config_language');
                }

                $this->load->model('localisation/vietnam_administrative');
                $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($order_query_rows['shipping_province_code']);
                $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($order_query_rows['shipping_district_code']);
                $ward = $this->model_localisation_vietnam_administrative->getWardByCode($order_query_rows['shipping_ward_code']);


                $all_orders[] = array(
                    'order_id' => $order_query_rows['order_id'],
                    'invoice_no' => $order_query_rows['invoice_no'],
                    'invoice_prefix' => $order_query_rows['invoice_prefix'],
                    'store_id' => $order_query_rows['store_id'],
                    'store_name' => $order_query_rows['store_name'],
                    'store_url' => $order_query_rows['store_url'],
                    'customer_id' => $order_query_rows['customer_id'],
                    'customer' => $order_query_rows['customer'],
                    'customer_group_id' => $order_query_rows['customer_group_id'],
                    'firstname' => $order_query_rows['firstname'],
                    'fullname' => $order_query_rows['fullname'],
                    'lastname' => $order_query_rows['lastname'],
                    'email' => $order_query_rows['email'],
                    'telephone' => $order_query_rows['telephone'],
                    'custom_field' => json_decode($order_query_rows['custom_field'], true),
                    'payment_firstname' => $order_query_rows['payment_firstname'],
                    'payment_lastname' => $order_query_rows['payment_lastname'],
                    'payment_company' => $order_query_rows['payment_company'],
                    'payment_address_1' => $order_query_rows['payment_address_1'],
                    'payment_address_2' => $order_query_rows['payment_address_2'],
                    'payment_postcode' => $order_query_rows['payment_postcode'],
                    'payment_city' => $order_query_rows['payment_city'],
                    'payment_zone_id' => $order_query_rows['payment_zone_id'],
                    'payment_zone' => $order_query_rows['payment_zone'],
                    'payment_zone_code' => $payment_zone_code,
                    'payment_country_id' => $order_query_rows['payment_country_id'],
                    'payment_country' => $order_query_rows['payment_country'],
                    'payment_iso_code_2' => $payment_iso_code_2,
                    'payment_iso_code_3' => $payment_iso_code_3,
                    'payment_address_format' => $order_query_rows['payment_address_format'],
                    'payment_custom_field' => json_decode($order_query_rows['payment_custom_field'], true),
                    'payment_method' => $order_query_rows['payment_method'],
                    'payment_code' => $order_query_rows['payment_code'],
                    'shipping_firstname' => $order_query_rows['shipping_firstname'],
                    'shipping_lastname' => $order_query_rows['shipping_lastname'],
                    'shipping_company' => $order_query_rows['shipping_company'],
                    'shipping_address_1' => $order_query_rows['shipping_address_1'],
                    'shipping_address_2' => $order_query_rows['shipping_address_2'],
                    'shipping_postcode' => $order_query_rows['shipping_postcode'],
                    'shipping_city' => $order_query_rows['shipping_city'],
                    'shipping_zone_id' => $order_query_rows['shipping_zone_id'],
                    'shipping_zone' => $order_query_rows['shipping_zone'],
                    'shipping_zone_code' => $shipping_zone_code,
                    'shipping_country_id' => $order_query_rows['shipping_country_id'],
                    'shipping_country' => $order_query_rows['shipping_country'],
                    'shipping_iso_code_2' => $shipping_iso_code_2,
                    'shipping_iso_code_3' => $shipping_iso_code_3,
                    'shipping_address_format' => $order_query_rows['shipping_address_format'],
                    'shipping_custom_field' => json_decode($order_query_rows['shipping_custom_field'], true),
                    'shipping_method' => $order_query_rows['shipping_method'],
                    'shipping_code' => $order_query_rows['shipping_code'],
                    'comment' => $order_query_rows['comment'],
                    'total' => $order_query_rows['total'],
                    'reward' => $reward,
                    'order_status_id' => $order_query_rows['order_status_id'],
                    'order_status' => $order_query_rows['order_status_id'],
                    'affiliate_id' => $order_query_rows['affiliate_id'],
                    'affiliate_firstname' => $affiliate_firstname,
                    'affiliate_lastname' => $affiliate_lastname,
                    'commission' => $order_query_rows['commission'],
                    'language_id' => $order_query_rows['language_id'],
                    'language_code' => $language_code,
                    'currency_id' => $order_query_rows['currency_id'],
                    'currency_code' => $order_query_rows['currency_code'],
                    'currency_value' => $order_query_rows['currency_value'],
                    'ip' => $order_query_rows['ip'],
                    'forwarded_ip' => $order_query_rows['forwarded_ip'],
                    'user_agent' => $order_query_rows['user_agent'],
                    'accept_language' => $order_query_rows['accept_language'],
                    'date_added' => $order_query_rows['date_added'],
                    'date_modified' => $order_query_rows['date_modified'],
                    'shipping_ward_code' => $order_query_rows['shipping_ward_code'],
                    'shipping_district_code' => $order_query_rows['shipping_district_code'],
                    'shipping_province_code' => $order_query_rows['shipping_province_code'],
                    'order_code' => $order_query_rows['order_code'],
                    'address_shipping_total' => convertAddressOrderList(isset($result['shipping_address_1']) ? $result['shipping_address_1'] : '',
                        isset($province['name']) ? $province['name'] : '',
                        isset($district['name']) ? $district['name'] : '',
                        isset($ward['name']) ? $ward['name'] : ''),
                    'shipping_fee' => $order_query_rows['shipping_fee'],
                    'user_id' => $order_query_rows['user_id'],
                    'from_domain' => isset($order_query_rows['from_domain']) ? $order_query_rows['from_domain'] : '',
                    'transport_name' => $order_query_rows['transport_name'],
                    'transport_order_code' => $order_query_rows['transport_order_code'],
                    'source' => $order_query->row['source'],
                    'collection_amount' => $order_query_rows['collection_amount']
                );
            }
            return $all_orders;
        } else {
            return [];
        }
    }

    public function getManualUpdateStatusById($order_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT `manual_update_status`, `transport_order_code` FROM `{$DB_PREFIX}order` 
                WHERE `order_id` = " . (int)$order_id;

        $query = $this->db->query($sql);

        return $query->row;

    }

    public function getOrderCodeById($order_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT `order_code` FROM `{$DB_PREFIX}order` 
                WHERE order_id = " . (int)$order_id;

        $query = $this->db->query($sql);

        return $query->row['order_code'];
    }

    public function getOrder($order_id)
    {
        $order_query = $this->db->query("SELECT *, (SELECT CONCAT(c.customer_id,c.firstname, ' ', c.lastname) FROM " . DB_PREFIX . "customer c WHERE c.customer_id = o.customer_id) AS customer, (SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int)$this->config->get('config_language_id') . "') AS order_status FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int)$order_id . "'"); // comment AND o.`payment_status` != '3' => show in admin
        //echo "SELECT *, (SELECT CONCAT(c.customer_id,c.firstname, ' ', c.lastname) FROM " . DB_PREFIX . "customer c WHERE c.customer_id = o.customer_id) AS customer, (SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int)$this->config->get('config_language_id') . "') AS order_status FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int)$order_id . "'";
        //echo "<pre>";
        //print_r ($order_query);
        //echo "</pre>";

        if ($order_query->num_rows) {
            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");

            if ($country_query->num_rows) {
                $payment_iso_code_2 = $country_query->row['iso_code_2'];
                $payment_iso_code_3 = $country_query->row['iso_code_3'];
            } else {
                $payment_iso_code_2 = '';
                $payment_iso_code_3 = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");

            if ($zone_query->num_rows) {
                $payment_zone_code = $zone_query->row['code'];
            } else {
                $payment_zone_code = '';
            }

            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

            if ($country_query->num_rows) {
                $shipping_iso_code_2 = $country_query->row['iso_code_2'];
                $shipping_iso_code_3 = $country_query->row['iso_code_3'];
            } else {
                $shipping_iso_code_2 = '';
                $shipping_iso_code_3 = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

            if ($zone_query->num_rows) {
                $shipping_zone_code = $zone_query->row['code'];
            } else {
                $shipping_zone_code = '';
            }

            $reward = 0;

            $order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

            foreach ($order_product_query->rows as $product) {
                $reward += $product['reward'];
            }

            $this->load->model('customer/customer');

            $affiliate_info = $this->model_customer_customer->getCustomer($order_query->row['affiliate_id']);

            if ($affiliate_info) {
                $affiliate_firstname = $affiliate_info['firstname'];
                $affiliate_lastname = $affiliate_info['lastname'];
            } else {
                $affiliate_firstname = '';
                $affiliate_lastname = '';
            }

            $this->load->model('localisation/language');

            $language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);

            if ($language_info) {
                $language_code = $language_info['code'];
            } else {
                $language_code = $this->config->get('config_language');
            }

            $this->load->model('localisation/vietnam_administrative');
            $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($order_query->row['shipping_province_code']);
            $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($order_query->row['shipping_district_code']);
            $ward = $this->model_localisation_vietnam_administrative->getWardByCode($order_query->row['shipping_ward_code']);

            return array(
                'order_id' => $order_query->row['order_id'],
                'invoice_no' => $order_query->row['invoice_no'],
                'invoice_prefix' => $order_query->row['invoice_prefix'],
                'store_id' => $order_query->row['store_id'],
                'store_name' => $order_query->row['store_name'],
                'store_url' => $order_query->row['store_url'],
                'customer_id' => $order_query->row['customer_id'],
                'customer' => $order_query->row['customer'],
                'customer_group_id' => $order_query->row['customer_group_id'],
                'firstname' => $order_query->row['firstname'],
                'fullname' => $order_query->row['fullname'],
                'lastname' => $order_query->row['lastname'],
                'email' => $order_query->row['email'],
                'telephone' => $order_query->row['telephone'],
                'custom_field' => json_decode($order_query->row['custom_field'], true),
                'payment_firstname' => $order_query->row['payment_firstname'],
                'payment_lastname' => $order_query->row['payment_lastname'],
                'payment_company' => $order_query->row['payment_company'],
                'payment_address_1' => $order_query->row['payment_address_1'],
                'payment_address_2' => $order_query->row['payment_address_2'],
                'payment_postcode' => $order_query->row['payment_postcode'],
                'payment_city' => $order_query->row['payment_city'],
                'payment_zone_id' => $order_query->row['payment_zone_id'],
                'payment_zone' => $order_query->row['payment_zone'],
                'payment_zone_code' => $payment_zone_code,
                'payment_country_id' => $order_query->row['payment_country_id'],
                'payment_country' => $order_query->row['payment_country'],
                'payment_iso_code_2' => $payment_iso_code_2,
                'payment_iso_code_3' => $payment_iso_code_3,
                'payment_address_format' => $order_query->row['payment_address_format'],
                'payment_custom_field' => json_decode($order_query->row['payment_custom_field'], true),
                'payment_method' => $order_query->row['payment_method'],
                'payment_code' => $order_query->row['payment_code'],
                'payment_status' => $order_query->row['payment_status'],
                'shipping_firstname' => $order_query->row['shipping_firstname'],
                'shipping_lastname' => $order_query->row['shipping_lastname'],
                'shipping_company' => $order_query->row['shipping_company'],
                'shipping_address_1' => $order_query->row['shipping_address_1'],
                'shipping_address_2' => $order_query->row['shipping_address_2'],
                'shipping_postcode' => $order_query->row['shipping_postcode'],
                'shipping_city' => $order_query->row['shipping_city'],
                'shipping_zone_id' => $order_query->row['shipping_zone_id'],
                'shipping_zone' => $order_query->row['shipping_zone'],
                'shipping_zone_code' => $shipping_zone_code,
                'shipping_country_id' => $order_query->row['shipping_country_id'],
                'shipping_country' => $order_query->row['shipping_country'],
                'shipping_iso_code_2' => $shipping_iso_code_2,
                'shipping_iso_code_3' => $shipping_iso_code_3,
                'shipping_address_format' => $order_query->row['shipping_address_format'],
                'shipping_custom_field' => json_decode($order_query->row['shipping_custom_field'], true),
                'shipping_method' => $order_query->row['shipping_method'],
                'shipping_code' => $order_query->row['shipping_code'],
                'comment' => $order_query->row['comment'],
                'discount' => isset($order_query->row['discount']) ? $order_query->row['discount'] : 0,
                'total' => $order_query->row['total'],
                'collection_amount' => isset($order_query->row['collection_amount']) ? $order_query->row['collection_amount'] : $order_query->row['total'],
                'reward' => $reward,
                'order_status_id' => $order_query->row['order_status_id'],
                'order_status' => $order_query->row['order_status_id'],
                'affiliate_id' => $order_query->row['affiliate_id'],
                'affiliate_firstname' => $affiliate_firstname,
                'affiliate_lastname' => $affiliate_lastname,
                'commission' => $order_query->row['commission'],
                'language_id' => $order_query->row['language_id'],
                'language_code' => $language_code,
                'currency_id' => $order_query->row['currency_id'],
                'currency_code' => $order_query->row['currency_code'],
                'currency_value' => $order_query->row['currency_value'],
                'ip' => $order_query->row['ip'],
                'forwarded_ip' => $order_query->row['forwarded_ip'],
                'user_agent' => $order_query->row['user_agent'],
                'accept_language' => $order_query->row['accept_language'],
                'date_added' => $order_query->row['date_added'],
                'date_modified' => $order_query->row['date_modified'],
                'shipping_ward_code' => $order_query->row['shipping_ward_code'],
                'shipping_district_code' => $order_query->row['shipping_district_code'],
                'shipping_province_code' => $order_query->row['shipping_province_code'],
                'order_code' => $order_query->row['order_code'],
                'address_shipping_total' => convertAddressOrderList(isset($result['shipping_address_1']) ? $result['shipping_address_1'] : '',
                    isset($province['name']) ? $province['name'] : '',
                    isset($district['name']) ? $district['name'] : '',
                    isset($ward['name']) ? $ward['name'] : ''),
                'shipping_fee' => $order_query->row['shipping_fee'],
                'shipping_method_value' => $order_query->row['shipping_method_value'],
                'user_id' => $order_query->row['user_id'],
                'previous_order_status_id' => $order_query->row['previous_order_status_id'],
                'transport_name' => $order_query->row['transport_name'],
                'transport_order_code' => $order_query->row['transport_order_code'],
                'transport_service_name' => $order_query->row['transport_service_name'],
                'source' => $order_query->row['source'],
                'transport_money_total' => $order_query->row['transport_money_total'],
                'transport_weight' => $order_query->row['transport_weight'],
                'transport_status' => $order_query->row['transport_status'],
                'transport_current_warehouse' => $order_query->row['transport_current_warehouse'],
                'manual_update_status' => $order_query->row['manual_update_status']
            );
        } else {
            return;
        }
    }

    /**
     * conflicted with getOrderStatus below (that function is INVALID name, TODO: rename that function name...)
     * getOrderStatus below returns name only of all order_statuses. Very confused...
     *
     * @param $order_id
     * @return mixed
     */
    public function getOrderStatusForOrder($order_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT o.`order_status_id` FROM `{$DB_PREFIX}order` 
                WHERE o.order_id = " . (int)$order_id;

        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getOrders($data = array())
    {
        $sql = "SELECT *,o.date_added as date_added_order, o.order_id as order_id_result, o.telephone as telephone_order, CONCAT(customer.lastname, ' ', customer.firstname) as customer_full_name, o.fullname as order_full_name FROM `" . DB_PREFIX . "order` as o";

        $sql .= " LEFT JOIN `" . DB_PREFIX . "address` as address ON o.`customer_id` = address.`customer_id` ";
        $sql .= " LEFT JOIN `" . DB_PREFIX . "customer` as customer ON o.`customer_id` = customer.`customer_id` ";

        // join for filtering tag
        if (!empty($data['order_tag'])) {
            $sql .= " LEFT JOIN `" . DB_PREFIX . "order_tag` ot ON (o.`order_id` = ot.`order_id`) ";
            $sql .= " LEFT JOIN `" . DB_PREFIX . "tag` t ON (ot.`tag_id` = t.`tag_id`) ";
        }

        if (!empty($data['order_utm'])) {
            $sql .= " LEFT JOIN `" . DB_PREFIX . "order_utm` ou ON (o.`order_id` = ou.`order_id`) ";
        }

        // join for filtering discount
        if (!empty($data['order_discount'])) {
            $sql .= " LEFT JOIN `" . DB_PREFIX . "order_to_discount` otd ON (o.`order_id` = otd.`order_id`) ";
        }

        // filter order_status backward compatibility
        $statusArray = array_keys($this->getListStatus());
        $array = implode(",", $statusArray);
        if (!empty($data['order_status_id'])) {
            $sql .= "WHERE o.`order_status_id` = '" . (int)$data['order_status_id'] . "' ";
        } else {
            $sql .= "WHERE o.`order_status_id` IN (" . $array . ") ";
        }

        // not show order with payment -status = 3
        // $sql .= " AND o.`payment_status` != '3'"; // => show in admin
        //$sql .= " AND !(o.`order_status_id` = 6 AND o.`payment_status` = 3)";

        // filter order_status new
        if (!empty($data['order_status'])) {
            $order_status_array = implode(",", $data['order_status']);
            if (!empty($order_status_array)) {
                $sql .= " AND o.`order_status_id` IN (" . $order_status_array . ") ";
            }
        }

        // filter:  order have product created by users in `filter_staff`
        if (isset($data['filter_staff']) && $data['filter_staff'] !== '') {
            $sql .= " AND o.order_id IN (
                                            SELECT order_id FROM " . DB_PREFIX . "order_product sop LEFT JOIN " . DB_PREFIX . "product sp ON sp.product_id = sop.product_id 
                                            WHERE sp.`user_create_id` IN (" . $data['filter_staff'] . ") GROUP BY sop.order_id
                                         )";

        }

        // filter order_payment status
        if (!empty($data['order_payment'])) {
            if (in_array("2", $data['order_payment'])) {
                $sql .= "AND o.payment_status = 1 AND o.order_status_id ='" . (int)ModelSaleOrder::ORDER_STATUS_ID_CANCELLED . "' ";
                $remain = array_diff($data['order_payment'], ['2']);
                if (!empty($remain)) {
                    $payment_status_array = implode(",", $remain);
                    $sql .= " OR o.`payment_status` IN (" . $payment_status_array . ") ";
                    $sql .= " AND NOT (o.previous_order_status_id = '" . (int)ModelSaleOrder::ORDER_STATUS_ID_COMPLETED . "' AND o.order_status_id ='" . (int)ModelSaleOrder::ORDER_STATUS_ID_CANCELLED . "')";
                }
            } else {
                $payment_status_array = implode(",", $data['order_payment']);
                $sql .= " AND o.`payment_status` IN (" . $payment_status_array . ") ";
                $sql .= " AND NOT (o.previous_order_status_id = '" . (int)ModelSaleOrder::ORDER_STATUS_ID_COMPLETED . "' AND o.order_status_id ='" . (int)ModelSaleOrder::ORDER_STATUS_ID_CANCELLED . "')";
            }
        }

        // filter order_tag
        // filter order_tag
        if (isset($data['order_tag']) && is_array($data['order_tag'])) {
            $count = 0;
            $sql_order_tag = " AND (";
            foreach ($data['order_tag'] as $tag) {
                if (!empty($tag)) {
                    if ($count == 0) {
                        $sql_order_tag .= " t.`value` LIKE '%" . $tag . "%' ";
                    } else {
                        $sql_order_tag .= " OR t.`value` LIKE '%" . $tag . "%' ";
                    }
                    $count++;
                }
            }
            $sql_order_tag .= " )";
            if ($count > 0) {
                $sql .= $sql_order_tag;
            }
        }

        // filter order_utm
        if (isset($data['order_utm']) && is_array($data['order_utm'])) {
            $count = 0;
            $sql_order_utm = " AND (";
            foreach ($data['order_utm'] as $utm) {
                if (!empty($utm)) {
                    if ($count == 0) {
                        $sql_order_utm .= " ou.`value` LIKE '%" . $utm . "%' ";
                    } else {
                        $sql_order_utm .= " OR ou.`value` LIKE '%" . $utm . "%' ";
                    }
                    $count++;
                }
            }
            $sql_order_utm .= " )";
            if ($count > 0) {
                $sql .= $sql_order_utm;
            }
        }

        // filter order_date_modified
        if (isset($data['order_updated_time_type']) && is_array($data['order_updated_time_type'])) {
            $sql_order = 'AND ( ';
            foreach ($data['order_updated_time_type'] as $idx => $order_updated_time_type) {
                $compare = explode('_', $order_updated_time_type);
                $compare_type = html_entity_decode($compare[0]);

                if (isset($compare[1])) {
                    $compare_value = $compare[1];
                } else {
                    break;
                }

                if (!in_array($compare_type, ['>=', '<=', '>=<='])) {
                    continue;
                }

                try {
                    $order_date_modified = date_create_from_format('d/m/Y', $compare_value);
                } catch (Exception $e) {
                    continue;
                }
                switch ($compare_type) {
                    case '>=':
                        $order_date_modified = $order_date_modified->format('Y-m-d 00:00:00');
                        break;

                    case '<=':
                        $order_date_modified = $order_date_modified->format('Y-m-d 23:59:59');
                        break;
                }
                if ($idx == 0) {
                    $sql_order .= "o.`date_modified` " . $compare_type . "'" . $order_date_modified . "'";
                } else {
                    $sql_order .= " OR o.`date_modified` " . $compare_type . " '" . $order_date_modified . "'";
                }
            }
            $sql = $sql . $sql_order . ' )';
        }

        // filter order_total
        if (isset($data['order_total_type']) && is_array($data['order_total_type'])) {
            /*
             * Nhỏ hơn (NH), Lớn hơn (LH)
             * Nếu NH < LH => query: (o.`total` < NH OR o.`total` > NH). Ví dụ: (o.`total` < 10.000 OR o.`total` > 20.000)
             * Nếu NH > LH => query: (o.`total` > LH AND o.`total` < NH). Ví dụ: (o.`total` < 25.000 AND o.`total` > 15.000)
             * Nếu có điều kiện Bằng ('=', B) => luôn thêm OR. Ví dụ:
             *   - Nếu NH < LH, B => query: ((o.`total` < NH OR o.`total` > NH) OR B). Ví dụ: ((o.`total` < 10.000 OR o.`total` > 20.000) OR o.`total` = 15000 OR o.`total` = 16000)
             *   - Nếu NH > LH => query: ((o.`total` > LH AND o.`total` < NH) OR B). Ví dụ: ((o.`total` < 25.000 AND o.`total` > 15.000) OR o.`total` = 35000)
             */
            $compare_lt = '';  // <
            $compare_gt = '';  // >
            $compare_eqs = []; // =
            foreach ($data['order_total_type'] as $idx => $order_total_type) {
                $compare = explode('_', $order_total_type);
                $compare_type = html_entity_decode($compare[0]);
                $compare_total = isset($compare[1]) ? $compare[1] : 0;

                if ($compare_type == '<' && ($compare_lt === '' || $compare_lt > $compare_total)) {
                    $compare_lt = $compare_total; // get min
                }

                if ($compare_type == '>' && ($compare_gt === '' || $compare_gt < $compare_total)) {
                    $compare_gt = $compare_total; // get max
                }

                if ($compare_type == '=') {
                    $compare_eqs[] = $compare_total; // allow multi values
                }
            }
            // build expression
            $sql_total = '';
            if ($compare_lt <= $compare_gt && $compare_gt !== '' && $compare_lt !== '') {
                $sql_total = "(o.`total` < $compare_lt OR o.`total` > $compare_gt)";
            } elseif ($compare_lt >= $compare_gt && $compare_gt !== '' && $compare_lt !== '') {
                $sql_total = "(o.`total` < $compare_lt AND o.`total` > $compare_gt)";
            } elseif ($compare_lt !== '') {
                $sql_total = "(o.`total` < $compare_lt)";
            } elseif ($compare_gt !== '') {
                $sql_total = "(o.`total` > $compare_gt)";
            }

            foreach ($compare_eqs as $idx => $compare_eq) {
                if (!empty($sql_total)) {
                    $sql_total .= " OR o.`total` = $compare_eq";
                } else {
                    if ($idx === 0) {
                        $sql_total .= "o.`total` = $compare_eq";
                    } else {
                        $sql_total .= " OR o.`total` = $compare_eq";
                    }
                }
            }

            $sql .= " AND ($sql_total)";
        }

        // filter order_code
        if (!empty($data['order_code'])) {
            $sql .= " AND o.`order_code` LIKE '%" . $this->db->escape($data['order_code']) . "%' ";
        }

        // TODO: remove...
        if (!empty($data['order_transfer'])) {
            $sql .= " AND o.`order_transfer` = '" . (int)$data['order_transfer'] . "' ";
        }

        if (!empty($data['order_search_text'])) {
            $sql .= " AND o.`fullname` LIKE '%" . $this->db->escape($data['order_search_text']) . "%' ";
        }

        // filter order_phone
        if (isset($data['order_phone']) && $data['order_phone'] !== ''){
            $sql .= " AND o.telephone = '" . $this->db->escape($data['order_phone']) . "' ";
        }
        // filter order_source
        // filter order_tag
        if (!empty($data['order_source']) && is_array($data['order_source'])) {
            $count = 0;
            $sql_order_source = " AND (";
            foreach ($data['order_source'] as $source) {
                if (!empty($source)) {
                    if ($count == 0) {
                        $sql_order_source .= " o.`source`  = '" . $this->db->escape($source) . "' ";
                    } else {
                        $sql_order_source .= " OR o.`source` = '" . $this->db->escape($source) . "' ";
                    }
                    $count++;
                }
            }
            $sql_order_source .= " )";
            if ($count > 0) {
                $sql .= $sql_order_source;
            }
        }

        // filter order_discount
        if (!empty($data['order_discount']) && is_array($data['order_discount'])) {
            $count = 0;
            $sql_order_discount = " AND (";
            foreach ($data['order_discount'] as $discount) {
                if (!empty($discount)) {
                    if ($count == 0) {
                        $sql_order_discount .= " otd.`discount_id` = '" . (int)$discount . "' ";
                    } else {
                        $sql_order_discount .= " OR otd.`discount_id` = '" . (int)$discount . "' ";
                    }
                    $count++;
                }
            }
            $sql_order_discount .= " )";
            if ($count > 0) {
                $sql .= $sql_order_discount;
            }
        }

        $sql .= "GROUP BY o.`order_id`";

        $sql .= " ORDER BY o.`date_added`";
        if (isset($data['order']) && ($data['order'] == 'ASC')) {
            $sql .= " ASC";
        } else {
            $sql .= " DESC";
        }
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getAddress($customer_id)
    {
        $query = $this->db->query("SELECT ad.*, cu.email, cu.customer_id FROM " . DB_PREFIX . "customer cu LEFT JOIN " . DB_PREFIX . "address ad ON cu.address_id = ad.address_id WHERE cu.customer_id = " . (int)$customer_id);

        return $query->row;
    }

    public function getLastestOrders($data = ['start' => 0, 'limit' => 10])
    {
        $sql = "SELECT `order_id`, `order_code`, `fullname`, `order_status_id`, `total`, `date_added` FROM `" . DB_PREFIX . "order` ";
        $sql .= "ORDER BY date_added DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getLastestOrdersBydate($data)
    {
        $data['filter_date_added_end'] = date("Y-m-d", strtotime($data['filter_date_added_end'] . ' + 1 days')); /// compare dateTime < date
        $sql = "SELECT `date_added`, `total` FROM `" . DB_PREFIX . "order` WHERE `date_added` >= DATE('" . $this->db->escape($data['filter_date_added_start']) . "') AND `date_added` < DATE('" . $this->db->escape($data['filter_date_added_end']) . "')";

        if (!empty($data['order_status'])) {
            $sql .= "AND order_status_id = '" . (int)$data['order_status'] . "' ";
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getOrderProducts($order_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

        return $query->rows;
    }

    public function getRelatedOrderProductsByProductId($product_id, $order_statuses = [])
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT op.* FROM `{$DB_PREFIX}order_product` op
                                   JOIN `{$DB_PREFIX}order` o ON (op.order_id = o.order_id)
                                   WHERE op.product_id = '" . (int)$product_id . "' AND o.`source` NOT IN ('shopee', 'lazada')";

        if (!empty($order_statuses)) {
            $order_statuses = array_map(function ($os) {
                return trim($os);
            }, $order_statuses);

            $sql .= " AND o.order_status_id IN (" . implode(',', $order_statuses) . ")";
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getInfoOrderProducts($order_id)
    {
        $query = $this->db->query("SELECT 
            CASE 
                WHEN op.product_version_id = 0 THEN p.status 
                WHEN op.product_version_id IS NULL THEN p.status 
                ELSE pv.status 
            END AS ppv_status,
            CASE 
                WHEN op.product_version_id = 0 THEN p.quantity 
                WHEN op.product_version_id IS NULL THEN p.quantity 
                ELSE pv.quantity 
                END AS ppv_quantity, 
            p.status as p_status, 
            p.compare_price as p_compare_price, 
            pv.compare_price as pv_compare_price, 
            p.sale_on_out_of_stock as p_sale_on_out_of_stock, p.multi_versions,
            p.product_id, p.weight, pd.name, p.image, pv.product_version_id, pv.version, op.quantity, op.price, op.discount, op.total, op.add_on_deal_id, 
            (CASE WHEN op.`product_version_id` = 0 OR op.`product_version_id` IS NULL THEN p.`sku` ELSE pv.`sku` END) order_product_sku 
            FROM " . DB_PREFIX . "order_product op 
            LEFT JOIN " . DB_PREFIX . "product p ON op.product_id = p.product_id 
            LEFT JOIN " . DB_PREFIX . "product_description pd ON op.product_id = pd.product_id 
            LEFT JOIN " . DB_PREFIX . "product_version pv ON op.product_version_id = pv.product_version_id 
            WHERE op.order_id = " . (int)$order_id . " 
                AND pd.language_id = " . (int)$this->config->get('config_language_id'));

        $results = [];
        $this->load->model('tool/image');
        foreach ($query->rows as $k => $row) {

            $flag = 'true';
            // TODO REMOVE check in validateForm after submit
            /*if ($row['ppv_status'] == 0 || $row['p_status'] == 0) {
                $flag = 'false';
            } elseif ($row['p_sale_on_out_of_stock'] == 0 && $row['ppv_quantity'] < 1) {
                $flag = 'false';
            }else if (!$row['product_version_id'] && $row['multi_versions'] == 1){
                $flag = 'false';
            }*/

            if ($row['image'] != '') {
                $image = $this->model_tool_image->resize($row['image'], 100, 100);
            } else {
                $image = $this->model_tool_image->resize('no_image.png', 100, 100);
            }
            $results[$k] = $row;
            $results[$k]['flag'] = $flag;
            $results[$k]['compare_price'] = $row['multi_versions'] ? $row['pv_compare_price'] : $row['p_compare_price'];
            $results[$k]['weight'] = (int)$row['weight'];
            $results[$k]['price'] = (int)$row['price'];
            $results[$k]['discount'] = (float)$row['discount'];
            $results[$k]['total'] = (float)$row['total'];
            $results[$k]['pv_id'] = $row['product_id'];
            $results[$k]['image'] = $image;
            $results[$k]['sku'] = $row['order_product_sku'];
            if ($row['product_version_id'] != '') {
                $results[$k]['pv_id'] .= '-' . $row['product_version_id'];
            }
            $results[$k]['add_on_deal_id'] = $row['add_on_deal_id'];
            $results[$k]['link_add_on_deal_detail'] =$this->url->link('discount/add_on_deal/edit', 'user_token=' . $this->session->data['user_token'] . '&add_on_deal_id=' . $row['add_on_deal_id'], true);
        }

        return $results;
    }

    public function getOrderProductsTotal($order_id)
    {
        $query = $this->db->query("SELECT SUM(total) as total FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

        return $query->row['total'];
    }

    public function getOrderProductsDetail($order_id)
    {
        $sql = "SELECT DISTINCT(t.order_product_id) as odi, t.* FROM
                    (SELECT o.order_id, o.order_code, o.customer_id, 
                            od.order_product_id, od.quantity, od.price, od.total, 
                            p.product_id, p.default_store_id, p.image, pd.name, od.product_version_id 
                    FROM `" . DB_PREFIX . "order_product` od 
                    JOIN `" . DB_PREFIX . "order` o ON o.order_id = od.order_id 
                    JOIN `" . DB_PREFIX . "product` p ON od.product_id = p.product_id 
                    JOIN `" . DB_PREFIX . "product_description` pd ON p.product_id = pd.product_id 
                    WHERE od.order_id = " . (int)$order_id . ") t";

        $query = $this->db->query($sql);
        $product_orders = [];
        foreach ($query->rows as $row) {
            $product_orders[] = [
                'order_id' => $row['order_id'],
                'order_code' => $row['order_code'],
                'customer_id' => $row['customer_id'],
                'product_id' => $row['product_id'],
                'name' => $row['name'],
                'quantity' => $row['quantity'],
                'price' => $row['price'],
                'total' => $row['total'],
                'image' => $row['image'],
                'product_version_id' => $row['product_version_id'],
                'default_store_id' => $row['default_store_id'],
            ];
        }

        return $product_orders;
    }

    public function getOrderOptions($order_id, $order_product_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product_id . "'");

        return $query->rows;
    }

    public function getOrderVouchers($order_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");

        return $query->rows;
    }

    public function getOrderUtm($order_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_utm WHERE order_id = '" . (int)$order_id . "'");

        return $query->rows;
    }

    public function getOrderVoucherByVoucherId($voucher_id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_voucher` WHERE voucher_id = '" . (int)$voucher_id . "'");

        return $query->row;
    }

    public function getOrderTotals($order_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order");

        return $query->rows;
    }

    public function getTotalOrders($data = array(), $exceptDraft = false)
    {
        $sql = "SELECT COUNT(DISTINCT o.order_id ) AS total FROM `" . DB_PREFIX . "order` as o ";

        // join for filtering tag
        if (!empty($data['order_tag'])) {
            $sql .= " LEFT JOIN `" . DB_PREFIX . "order_tag` ot ON (o.`order_id` = ot.`order_id`) ";
            $sql .= " LEFT JOIN `" . DB_PREFIX . "tag` t ON (ot.`tag_id` = t.`tag_id`) ";
        }

        if (!empty($data['order_utm'])) {
            $sql .= " LEFT JOIN `" . DB_PREFIX . "order_utm` ou ON (o.`order_id` = ou.`order_id`) ";
        }

        // join for filtering discount
        if (!empty($data['order_discount'])) {
            $sql .= " LEFT JOIN `" . DB_PREFIX . "order_to_discount` otd ON (o.`order_id` = otd.`order_id`) ";
        }

        // filter order_status backward compatibility
        if ($exceptDraft) {
            $statusArray = array_keys($this->getListStatusExceptDraft());
        } else {
            $statusArray = array_keys($this->getListStatus());
        }
        $array = implode(",", $statusArray);
        if (!empty($data['order_status_id'])) {
            $sql .= "WHERE o.`order_status_id` = '" . (int)$data['order_status_id'] . "' ";
        } else {
            $sql .= "WHERE o.`order_status_id` IN (" . $array . ") ";
        }

        // not show order with payment -status = 3
//        $sql .= " AND o.`payment_status` != '3'"; // => show in admin

        // filter store
        if (isset($data['store_id']) && $data['store_id'] !== '') {
            $sql .= " AND o.store_id = '" . (int)$data['store_id'] . "' ";
        }

        // filter  user
        if (isset($data['user_id']) && $data['user_id'] !== '') {
            $sql .= " AND o.user_id = '" . (int)$data['user_id'] . "' ";
        }

        // filter order_status new
        if (!empty($data['order_status'])) {
            $order_status_array = implode(",", $data['order_status']);
            if (!empty($order_status_array)) {
                $sql .= " AND o.`order_status_id` IN (" . $order_status_array . ") ";
            }
        }

        // filter:  order have product created by users in `filter_staff`
        if (isset($data['filter_staff']) && $data['filter_staff'] !== '') {
            $sql .= " AND o.order_id IN (
                                            SELECT order_id FROM " . DB_PREFIX . "order_product sop LEFT JOIN " . DB_PREFIX . "product sp ON sp.product_id = sop.product_id 
                                            WHERE sp.`user_create_id` IN (" . $data['filter_staff'] . ") GROUP BY sop.order_id
                                         )";

        }

        // filter order_payment status
        if (!empty($data['order_payment'])) {
            if (in_array("2", $data['order_payment'])) {
                $sql .= "AND o.previous_order_status_id = '" . (int)ModelSaleOrder::ORDER_STATUS_ID_COMPLETED . "' AND o.order_status_id ='" . (int)ModelSaleOrder::ORDER_STATUS_ID_CANCELLED . "' ";
                $remain = array_diff($data['order_payment'], ['2']);
                if (!empty($remain)) {
                    $payment_status_array = implode(",", $remain);
                    $sql .= " OR o.`payment_status` IN (" . $payment_status_array . ") ";
                    $sql .= " AND NOT (o.previous_order_status_id = '" . (int)ModelSaleOrder::ORDER_STATUS_ID_COMPLETED . "' AND o.order_status_id ='" . (int)ModelSaleOrder::ORDER_STATUS_ID_CANCELLED . "')";
                }
            } else {
                $payment_status_array = implode(",", $data['order_payment']);
                $sql .= " AND o.`payment_status` IN (" . $payment_status_array . ") ";
                $sql .= " AND NOT (o.previous_order_status_id = '" . (int)ModelSaleOrder::ORDER_STATUS_ID_COMPLETED . "' AND o.order_status_id ='" . (int)ModelSaleOrder::ORDER_STATUS_ID_CANCELLED . "')";
            }
        }

        // filter order_tag
        if (isset($data['order_tag']) && is_array($data['order_tag'])) {
            $count = 0;
            $sql_order_tag = " AND (";
            foreach ($data['order_tag'] as $tag) {
                if (!empty($tag)) {
                    if ($count == 0) {
                        $sql_order_tag .= " t.`value` LIKE '%" . $tag . "%' ";
                    } else {
                        $sql_order_tag .= " OR t.`value` LIKE '%" . $tag . "%' ";
                    }
                    $count++;
                }
            }
            $sql_order_tag .= " )";
            if ($count > 0) {
                $sql .= $sql_order_tag;
            }
        }

        // filter order_utm
        if (isset($data['order_utm']) && is_array($data['order_utm'])) {
            $count = 0;
            $sql_order_utm = " AND (";
            foreach ($data['order_utm'] as $utm) {
                if (!empty($utm)) {
                    if ($count == 0) {
                        $sql_order_utm .= " ou.`value` LIKE '%" . $utm . "%' ";
                    } else {
                        $sql_order_utm .= " OR ou.`value` LIKE '%" . $utm . "%' ";
                    }
                    $count++;
                }
            }
            $sql_order_utm .= " )";
            if ($count > 0) {
                $sql .= $sql_order_utm;
            }
        }

        // filter order_date_modified
        if (isset($data['order_updated_time_type']) && is_array($data['order_updated_time_type'])) {
            $sql_order = 'AND ( ';
            foreach ($data['order_updated_time_type'] as $idx => $order_updated_time_type) {
                $compare = explode('_', $order_updated_time_type);
                $compare_type = html_entity_decode($compare[0]);

                if (isset($compare[1])) {
                    $compare_value = $compare[1];
                } else {
                    break;
                }

                if (!in_array($compare_type, ['>=', '<='])) {
                    continue;
                }

                try {
                    $order_date_modified = date_create_from_format('d/m/Y', $compare_value);
                } catch (Exception $e) {
                    continue;
                }
                switch ($compare_type) {
                    case '>=':
                        $order_date_modified = $order_date_modified->format('Y-m-d 00:00:00');
                        break;

                    case '<=':
                        $order_date_modified = $order_date_modified->format('Y-m-d 23:59:59');
                        break;
                }
                if ($idx == 0) {
                    $sql_order .= "o.`date_modified` " . $compare_type . "'" . $order_date_modified . "'";
                } else {
                    $sql_order .= " AND o.`date_modified` " . $compare_type . "'" . $order_date_modified . "'";
                }
            }
            $sql = $sql . $sql_order . ' )';
        }

        // filter order_total
        if (isset($data['order_total_type']) && is_array($data['order_total_type'])) {
            /*
             * Nhỏ hơn (NH), Lớn hơn (LH)
             * Nếu NH < LH => query: (o.`total` < NH OR o.`total` > NH). Ví dụ: (o.`total` < 10.000 OR o.`total` > 20.000)
             * Nếu NH > LH => query: (o.`total` > LH AND o.`total` < NH). Ví dụ: (o.`total` < 25.000 AND o.`total` > 15.000)
             * Nếu có điều kiện Bằng ('=', B) => luôn thêm OR. Ví dụ:
             *   - Nếu NH < LH, B => query: ((o.`total` < NH OR o.`total` > NH) OR B). Ví dụ: ((o.`total` < 10.000 OR o.`total` > 20.000) OR o.`total` = 15000 OR o.`total` = 16000)
             *   - Nếu NH > LH => query: ((o.`total` > LH AND o.`total` < NH) OR B). Ví dụ: ((o.`total` < 25.000 AND o.`total` > 15.000) OR o.`total` = 35000)
             */
            $compare_lt = '';  // <
            $compare_gt = '';  // >
            $compare_eqs = []; // =
            foreach ($data['order_total_type'] as $idx => $order_total_type) {
                $compare = explode('_', $order_total_type);
                $compare_type = html_entity_decode($compare[0]);
                $compare_total = isset($compare[1]) ? $compare[1] : 0;

                if ($compare_type == '<' && ($compare_lt === '' || $compare_lt > $compare_total)) {
                    $compare_lt = $compare_total; // get min
                }

                if ($compare_type == '>' && ($compare_gt === '' || $compare_gt < $compare_total)) {
                    $compare_gt = $compare_total; // get max
                }

                if ($compare_type == '=') {
                    $compare_eqs[] = $compare_total; // allow multi values
                }
            }
            // build expression
            $sql_total = '';
            if ($compare_lt <= $compare_gt && $compare_gt !== '' && $compare_lt !== '') {
                $sql_total = "(o.`total` < $compare_lt OR o.`total` > $compare_gt)";
            } elseif ($compare_lt >= $compare_gt && $compare_gt !== '' && $compare_lt !== '') {
                $sql_total = "(o.`total` < $compare_lt AND o.`total` > $compare_gt)";
            } elseif ($compare_lt !== '') {
                $sql_total = "(o.`total` < $compare_lt)";
            } elseif ($compare_gt !== '') {
                $sql_total = "(o.`total` > $compare_gt)";
            }

            foreach ($compare_eqs as $idx => $compare_eq) {
                if (!empty($sql_total)) {
                    $sql_total .= " OR o.`total` = $compare_eq";
                } else {
                    if ($idx === 0) {
                        $sql_total .= "o.`total` = $compare_eq";
                    } else {
                        $sql_total .= " OR o.`total` = $compare_eq";
                    }
                }
            }

            $sql .= " AND ($sql_total)";
        }

        // filter order_code
        if (!empty($data['order_code'])) {
            $sql .= " AND o.`order_code` LIKE '%" . $this->db->escape($data['order_code']) . "%' ";
        }

        // TODO: remove...
        if (!empty($data['order_transfer'])) {
            $sql .= " AND o.`order_transfer` = '" . (int)$data['order_transfer'] . "' ";
        }

        if (!empty($data['order_search_text'])) {
            $sql .= " AND o.`fullname` LIKE '%" . $this->db->escape($data['order_search_text']) . "%' ";
        }

        // filter order_phone
        if (isset($data['order_phone']) && $data['order_phone'] !== ''){
            $sql .= " AND o.telephone = '" . $this->db->escape($data['order_phone']) . "' ";
        }
        // filter order_source
        // filter order_tag
        if (!empty($data['order_source']) && is_array($data['order_source'])) {
            $count = 0;
            $sql_order_source = " AND (";
            foreach ($data['order_source'] as $source) {
                if (!empty($source)) {
                    if ($count == 0) {
                        $sql_order_source .= " o.`source`  = '" . $this->db->escape($source) . "' ";
                    } else {
                        $sql_order_source .= " OR o.`source` = '" . $this->db->escape($source) . "' ";
                    }
                    $count++;
                }
            }
            $sql_order_source .= " )";
            if ($count > 0) {
                $sql .= $sql_order_source;
            }
        }

        // filter order_discount
        if (!empty($data['order_discount']) && is_array($data['order_discount'])) {
            $count = 0;
            $sql_order_discount = " AND (";
            foreach ($data['order_discount'] as $discount) {
                if (!empty($discount)) {
                    if ($count == 0) {
                        $sql_order_discount .= " otd.`discount_id` = '" . (int)$discount . "' ";
                    } else {
                        $sql_order_discount .= " OR otd.`discount_id` = '" . (int)$discount . "' ";
                    }
                    $count++;
                }
            }
            $sql_order_discount .= " )";
            if ($count > 0) {
                $sql .= $sql_order_discount;
            }
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalOrdersByStoreId($store_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE store_id = '" . (int)$store_id . "'");

        return $query->row['total'];
    }

    public function getTotalOrdersByOrderStatusId($order_status_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE order_status_id = '" . (int)$order_status_id . "' AND order_status_id > '0'");

        return $query->row['total'];
    }

    public function getTotalOrdersByProcessingStatus()
    {
        $implode = array();

        $order_statuses = $this->config->get('config_processing_status');

        foreach ($order_statuses as $order_status_id) {
            $implode[] = "order_status_id = '" . (int)$order_status_id . "'";
        }

        if ($implode) {
            $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE " . implode(" OR ", $implode));

            return $query->row['total'];
        } else {
            return 0;
        }
    }

    public function getTotalOrdersByCompleteStatus()
    {
        $implode = array();

        $order_statuses = $this->config->get('config_complete_status');

        foreach ($order_statuses as $order_status_id) {
            $implode[] = "order_status_id = '" . (int)$order_status_id . "'";
        }

        if ($implode) {
            $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE " . implode(" OR ", $implode) . "");

            return $query->row['total'];
        } else {
            return 0;
        }
    }

    public function getTotalOrdersByLanguageId($language_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE language_id = '" . (int)$language_id . "' AND order_status_id > '0'");

        return $query->row['total'];
    }

    public function getTotalOrdersByCurrencyId($currency_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE currency_id = '" . (int)$currency_id . "' AND order_status_id > '0'");

        return $query->row['total'];
    }

    public function getTotalSales($data = array())
    {
        $sql = "SELECT SUM(total) AS total FROM `" . DB_PREFIX . "order`";

        if (!empty($data['filter_order_status'])) {
            $implode = array();

            $order_statuses = explode(',', $data['filter_order_status']);

            foreach ($order_statuses as $order_status_id) {
                $implode[] = "order_status_id = '" . (int)$order_status_id . "'";
            }

            if ($implode) {
                $sql .= " WHERE (" . implode(" OR ", $implode) . ")";
            }
        } elseif (isset($data['filter_order_status_id']) && $data['filter_order_status_id'] !== '') {
            $sql .= " WHERE order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
        } else {
            $sql .= " WHERE order_status_id > '0'";
        }

        if (!empty($data['filter_order_id'])) {
            $sql .= " AND order_id = '" . (int)$data['filter_order_id'] . "'";
        }

        if (!empty($data['filter_customer'])) {
            $sql .= " AND CONCAT(firstname, ' ', o.lastname) LIKE '%" . $this->db->escape($data['filter_customer']) . "%'";
        }

        if (!empty($data['filter_date_added'])) {
            $sql .= " AND DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if (!empty($data['filter_date_modified'])) {
            $sql .= " AND DATE(date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')";
        }

        if (!empty($data['filter_total'])) {
            $sql .= " AND total = '" . (float)$data['filter_total'] . "'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function createInvoiceNo($order_id)
    {
        $order_info = $this->getOrder($order_id);

        if ($order_info && !$order_info['invoice_no']) {
            $query = $this->db->query("SELECT MAX(invoice_no) AS invoice_no FROM `" . DB_PREFIX . "order` WHERE invoice_prefix = '" . $this->db->escape($order_info['invoice_prefix']) . "'");

            if ($query->row['invoice_no']) {
                $invoice_no = $query->row['invoice_no'] + 1;
            } else {
                $invoice_no = 1;
            }

            $this->db->query("UPDATE `" . DB_PREFIX . "order` SET invoice_no = '" . (int)$invoice_no . "', invoice_prefix = '" . $this->db->escape($order_info['invoice_prefix']) . "' WHERE order_id = '" . (int)$order_id . "'");

            return $order_info['invoice_prefix'] . $invoice_no;
        }
    }

    public function getOrderHistories($order_id, $start = 0, $limit = 10)
    {
        if ($start < 0) {
            $start = 0;
        }

        if ($limit < 1) {
            $limit = 10;
        }

        $query = $this->db->query("SELECT oh.date_added, os.name AS status, oh.comment, oh.notify FROM " . DB_PREFIX . "order_history oh LEFT JOIN " . DB_PREFIX . "order_status os ON oh.order_status_id = os.order_status_id WHERE oh.order_id = '" . (int)$order_id . "' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY oh.date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

        return $query->rows;
    }

    public function getTotalOrderHistories($order_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_history WHERE order_id = '" . (int)$order_id . "'");

        return $query->row['total'];
    }

    public function getTotalOrderHistoriesByOrderStatusId($order_status_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_history WHERE order_status_id = '" . (int)$order_status_id . "'");

        return $query->row['total'];
    }

    public function getEmailsByProductsOrdered($products, $start, $end)
    {
        $implode = array();

        foreach ($products as $product_id) {
            $implode[] = "op.product_id = '" . (int)$product_id . "'";
        }

        $query = $this->db->query("SELECT DISTINCT email FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) WHERE (" . implode(" OR ", $implode) . ") AND o.order_status_id <> '0' LIMIT " . (int)$start . "," . (int)$end);

        return $query->rows;
    }

    public function getTotalEmailsByProductsOrdered($products)
    {
        $implode = array();

        foreach ($products as $product_id) {
            $implode[] = "op.product_id = '" . (int)$product_id . "'";
        }

        $query = $this->db->query("SELECT COUNT(DISTINCT email) AS total FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) WHERE (" . implode(" OR ", $implode) . ") AND o.order_status_id <> '0'");

        return $query->row['total'];
    }

    public function getListStatus()
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_status` WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'");
        $listStatus = [];
        foreach ($query->rows as $row) {
            $listStatus[$row['order_status_id']] = $row['name'];
        }

        return $listStatus;
    }

    public function getListStatusExceptDraft()
    {
        $listStatus = $this->getListStatus();
        if (isset($listStatus[self::ORDER_STATUS_ID_DRAFT])) {
            unset($listStatus[self::ORDER_STATUS_ID_DRAFT]);
        }

        return $listStatus;
    }

    public function getNameStatusOrder($status = null)
    {
        if (isset($status)) {
            $data = $this->getListStatus();
            if (isset($data[$status])) {
                return $data[$status];
            }
            return null;
        }

        return null;
    }

    public function getListPayment()
    {
        $this->load->language('sale/order');
        $data = array(
            1 => $this->language->get('text_success_payment'),
            2 => $this->language->get('text_success_payment_cod'),
        );

        return $data;
    }

    public function getListShipment()
    {
        $this->load->language('sale/order');
        $data = array(
            1 => $this->language->get('order_status_delivering_wait'),
            2 => $this->language->get('order_status_delivering'),
            3 => $this->language->get('order_status_comback'),
        );

        return $data;
    }

    public function getCustomerIdByEmail($data)
    {
        $email = $data['email'];
        $sql = "SELECT * FROM `" . DB_PREFIX . "customer` WHERE email = '" . $email . "'";
        $query = $this->db->query($sql);
        if (count($query->row) > 0) {
            return $query->row['customer_id'];
        }

        $customerData['firstname'] = $data['firstname'];
        $customerData['lastname'] = $data['lastname'];
        $customerData['email'] = $data['email'];
        $customerData['telephone'] = $data['telephone'];
        $customerData['customer_source'] = '';
        $customerData['customer_group_id'] = '';
        $customerData['note'] = '';

        $this->load->model('customer/customer');
        $customer_id = $this->model_customer_customer->addCustomer($customerData);

        return $customer_id;
    }

    public function checkCustomerEmailExist($email)
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "customer` WHERE email = '" . $email . "'";
        $query = $this->db->query($sql);
        if (count($query->row) > 0) {
            return true;
        }

        return false;
    }

    public function addOrderCommon($data, $customerId = null)
    {
        $this->db->query("INSERT INTO `" . DB_PREFIX . "order` 
                          SET  firstname = '" . $this->db->escape($data['firstname']) . "', 
                          lastname = '" . $this->db->escape($data['lastname']) . "', 
                          email = '" . $this->db->escape($data['email']) . "', 
                          fullname = '" . $this->db->escape($data['fullname']) . "', 
                          telephone = '" . $data['telephone'] . "', 
                          order_status_id='" . $data['order_status'] . "', 
                          order_payment='" . $data['order_payment'] . "', 
                          order_transfer = '" . $data['order_transfer'] . "', 
                          comment = '" . $data['order_note'] . "', 
                          customer_id = '" . $customerId . "', 
                          discount = '" . (isset($data['order-discount']) ? $this->db->escape(extract_number($data['order-discount'])) : 0) . "', 
                          total = '" . $data['order_total'] . "', 
                          customer_ref_id = '" . (isset($data['customer_ref_id']) ? $this->db->escape($data['customer_ref_id']) : '') . "', 
                          date_added = NOW(), 
                          date_modified = NOW()");
        $order_id = $this->db->getLastId();

        return $order_id;
    }

    public function addOrderCustom($data)
    {
        $order['fullname'] = $customer['fullname'] = $data['fullname'];
        $order['firstname'] = $customer['firstname'] = $data['firstname'];
        $order['lastname'] = $customer['lastname'] = $data['lastname'];
        $order['email'] = $customer['email'] = $data['email'];
        $order['telephone'] = $customer['telephone'] = $data['telephone'];
        $customerId = $this->getCustomerIdByEmail($data);
        $orderId = $this->addOrderCommon($data, $customerId);

        return $orderId;
    }

    public function addOrderProduct($data, $orderId)
    {
        if (isset($data['product-choose'])) {
            $productChooses = $data['product-choose'];
            foreach ($productChooses as $key => $productId) {
                //insert order_product table
                $discount = isset($data['product-discount'][$key]) ? (float)(extract_number($data['product-discount'][$key])) : 0;
                $orderProductTotal = $data['product_quantity'][$key] * $data['product_price'][$key];
                $sql = "
					INSERT INTO " . DB_PREFIX . "order_product 
					SET  order_id = '" . (int)$orderId . "', 
					product_id = '" . (int)$productId . "', 
					quantity = '" . (int)$data['product_quantity'][$key] . "', 
					price = '" . $data['product_price'][$key] . "', 
					discount = '" . $discount . "', 
					total = '" . $orderProductTotal . "'";
                $this->db->query($sql);
                //reduce quantity product in product table
                $sqlProduct = "
					UPDATE " . DB_PREFIX . "product SET quantity = quantity - '" . (int)$data['product_quantity'][$key] . "', date_modified = NOW() WHERE product_id = '" . (int)$productId . "'
				";
                $this->db->query($sqlProduct);
            }

            return true;
        }

        return false;
    }

    public function putBackQuantityByProductId($product_id, $quantity)
    {
        $sqlProduct = "
			UPDATE " . DB_PREFIX . "product SET quantity = quantity + '" . (int)$quantity . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'
		";
        $this->db->query($sqlProduct);
    }

    public function deleteOrderById($orderId)
    {
        /*//put back quantity product
        $sql = "
			SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$orderId . "'
		";
        $data = $this->db->query($sql)->rows;
        foreach ($data as $key => $value) {
            $this->putBackQuantityByProductId($value['product_id'], $value['quantity']);
        }*/

        //delete order_product where order_id in order_product table
        $sqlOrderProduct = "
			DELETE FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$orderId . "'
		";
        $this->db->query($sqlOrderProduct);

        //delete order_tag where order_id in order_tag table
        $sqlOrderTag = "
			DELETE FROM " . DB_PREFIX . "order_tag WHERE order_id = '" . (int)$orderId . "'
		";
        $this->db->query($sqlOrderTag);

        //delete order in order table
        $sqlOrder = "
			DELETE FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$orderId . "'
		";
        $this->db->query($sqlOrder);

        return true;
    }

    public function getOrderById($orderId)
    {
        $sqlOrder = "
			SELECT * FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$orderId . "'
		";
        $order = $this->db->query($sqlOrder)->row;
        return $order;
    }

    public function getListProductByArrayId($listId = array())
    {
        $listId = implode(',', $listId);

        if (count($listId) == 0) {
            return null;
        }
        $sql = "
			SELECT DISTINCT * FROM " . DB_PREFIX . "product p JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id IN (" . $listId . ") AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'
		";

        $data = $this->db->query($sql)->rows;
    }

    public function getOrderDetailById($orderId)
    {
        $sql = "SELECT o.order_id AS order_id, o.fullname AS fullname, o.firstname AS firstname, o.lastname AS lastname, o.customer_id AS customer_id, o.order_transfer AS order_transfer, o.order_payment AS order_payment, o.order_status AS order_status, o.total AS total,p.product_id AS product_id, p.image AS image, p.quantity AS max_quantity, pd.name AS name, op.price AS price, op.quantity AS quantity, op.total AS total_product FROM " . DB_PREFIX . "order o JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) JOIN " . DB_PREFIX . "product p ON (op.product_id = p.product_id) JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
			WHERE o.order_id = '" . (int)$orderId . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
        // var_dump($sql);die;
        $result = $this->db->query($sql)->rows;
        return $result;
    }

    public function getTopProducts($data = ['start' => 0, 'limit' => 10])
    {
        $sql = "SELECT op.name as name, op.product_id as product_id, p.quantity as quantity, p.image as image, SUM(op.total) as total FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX . "product p ON (op.product_id = p .product_id) GROUP BY op.product_id ORDER BY total DESC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 10;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * NOTICE: keep $old_order_status_id for event listener
     *
     * @param array $data
     * @param int $old_order_status_id
     */
    public function updateStatus($data, $old_order_status_id)
    {
        if (isset($data['order_cancel_comment'])) {
            $sql = sprintf("UPDATE %s 
                            SET order_status_id = '%d', 
                                order_cancel_comment = '%s', 
                                comment = '%s', 
                                date_modified = NOW() 
                            WHERE order_id = '%d'",
                DB_PREFIX . "order",
                (int)$data['order_status_id'],
                $data['order_cancel_comment'],
                $data['order_cancel_comment'],
                (int)$data['order_id']
            );
        } else {
            $sql = sprintf("UPDATE %s 
                            SET order_status_id = '%d', 
                                date_modified = NOW() 
                            WHERE order_id = '%d'",
                DB_PREFIX . "order",
                (int)$data['order_status_id'],
                (int)$data['order_id']
            );
        }

        $this->db->query($sql);

        if (isset($data['order_source']) && $data['order_source'] == 'shopee') {
            $this->updateOrderShopee($data['order_id']);
        }
        if (isset($data['order_source']) && $data['order_source'] == 'lazada') {
            $this->updateOrderLazada($data['order_id']);
        }

        // update manual update status
        if ($data['order_status_id'] != $old_order_status_id) {
            $this->updateManualUpdateStatus($data['order_id']);
        }

        if ($data['order_status_id'] === self::ORDER_STATUS_ID_CANCELLED) {
            $DB_PREFIX = DB_PREFIX;
            // revert limit time coupon
            $sql = "SELECT voucher_id FROM {$DB_PREFIX}order_voucher WHERE order_id = '" . (int)$data['order_id'] . "' LIMIT 1";
            $query = $this->db->query($sql);

            if (isset($query->row['voucher_id'])) {
                $this->db->query("UPDATE `{$DB_PREFIX}coupon` 
                                SET `limit_times` = `limit_times` + 1 
                                WHERE `coupon_id` = '" . (int)$query->row['voucher_id'] . "'");
            }

            // set status for order_voucher
            $this->db->query("UPDATE `{$DB_PREFIX}order_voucher` 
                                SET `status` = 0 
                                WHERE `order_id` = '" . (int)$data['order_id'] . "'");
        }

        try {
            $this->sendToRabbitMq($data['order_id']);
        } catch (Exception $exception) {
            $this->log->write('Send message order error: ' . $exception->getMessage());
        }
    }

    public function updateOrderForStatusNotDraft($data, $order_id)
    {
        $sql = sprintf("UPDATE %s 
                        SET order_status_id = '%d', 
                            order_payment = '%d', 
                            order_transfer = '%d', 
                            comment = '%s', 
                            date_modified = NOW() 
                        WHERE order_id = '%d'",
            DB_PREFIX . "order",
            (int)$data['order_status_id'],
            (int)$data['order_payment'],
            (int)$data['order_transfer'],
            $data['order_note'],
            (int)$order_id
        );
        $this->db->query($sql);
    }

    public function updateOrderForStatusDraft($data, $order_id, $customerId)
    {
        $sql = sprintf("UPDATE %s   
                            SET  firstname = '%s', 
                                 lastname = '%s', 
                                 email = '%s', 
                                 fullname = '%s', 
                                 telephone = '%s', 
                                 order_status_id='%d', 
                                 order_payment='%s', 
                                 order_transfer = '%s', 
                                 comment = '%s',
                                 customer_id = '%d', 
                                 total = '%s', 
                                 date_added = NOW(), 
                                 date_modified = NOW() 
                            WHERE order_id = '%s'",
            DB_PREFIX . "order",
            $this->db->escape($data['firstname']),
            $this->db->escape($data['lastname']),
            $this->db->escape($data['email']),
            $this->db->escape($data['fullname']),
            $data['telephone'],
            $data['order_status_id'],
            $data['order_payment'],
            $data['order_transfer'],
            $data['order_note'],
            $customerId,
            $data['order_total'],
            $order_id
        );
        $this->db->query($sql);
    }

    public function deleteProductOrder($order_id)
    {
        $sql = sprintf("DELETE FROM %s 
                        WHERE order_id = '%d'",
            DB_PREFIX . "order_product",
            $order_id
        );
        $this->db->query($sql);
    }

    public function editOrderCustom($data, $order_id)
    {
        $order = $this->getOrderById($order_id);

        if (in_array($order['order_status_id'], [4, 5])) {
            $this->updateOrderForStatusNotDraft($data, $order_id);
        }

        if (in_array($order['order_status_id'], [2, 3])) {
            if (in_array($data['order_status_id'], [4, 5])) {
                foreach ($data['product-choose'] as $key => $value) {
                    $product_id = $value;
                    $quantity = $data['product_order_quantity'][$key];
                    $this->putBackQuantityByProductId($product_id, $quantity);
                }
                $this->updateOrderForStatusNotDraft($data, $order_id);
            } else {
                $this->updateOrderForStatusNotDraft($data, $order_id);
            }
        }

        // if order_status_id = 6: draft => allow edit all fields
        if ($order['order_status_id'] == self::ORDER_STATUS_ID_DRAFT) {
            $order_products = $this->getOrderProducts($order_id);
            // put back order quantity to stock
            foreach ($order_products as $order_product) {
                $product_id = $order_product['product_id'];
                $quantity = $order_product['quantity'];
                $this->putBackQuantityByProductId($product_id, $quantity);
            }

            // delete all order product records
            $this->deleteProductOrder($order_id);

            // create new order product records
            $this->addOrderProduct($data, $order_id);

            // update order
            $customerId = $this->getCustomerIdByEmail($data);
            $this->updateOrderForStatusDraft($data, $order_id, $customerId);
        }
    }

    public function copy($order_id)
    {
        //duplicate trong bang order
        $sqlCopy = "
    		INSERT INTO " . DB_PREFIX . "order (firstname, fullname, lastname, email, telephone, order_status_id, order_payment, order_transfer, comment, customer_id, total, date_added, date_modified) SELECT firstname, fullname, lastname, email, telephone, order_status_id, order_payment, order_transfer, comment, customer_id, total, date_added, date_modified FROM " . DB_PREFIX . "order WHERE order_id = '" . $order_id . "'
    	";
        $this->db->query($sqlCopy);
        $order_id_copy = $this->db->getLastId();

        //duplicate trong order_product
        $sqlOrderProduct = "
			SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'
		";
        $data = $this->db->query($sqlOrderProduct)->rows;

        foreach ($data as $key => $value) {
            //create new record order_product
            $sql = "
				INSERT INTO " . DB_PREFIX . "order_product SET  order_id = '" . (int)$order_id_copy . "', product_id = '" . (int)$value['product_id'] . "', quantity = '" . (int)$value['quantity'] . "', price = '" . $value['price'] . "', total = '" . $value['total'] . "'
			";
            $this->db->query($sql);

            //kiem tra quantity sp trong kho so voi quantity copy
            $quantity_store = $this->db->query("SELECT quantity FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$value['product_id'] . "'")->row['quantity'];
            if (!array_key_exists('quantity', $value)) {
                continue;
            }

            if ($quantity_store < $value['quantity']) {
                //xoa order_id_copy
                $this->db->query("DELETE FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$order_id_copy . "'");
                return false;
            }

            //reduce quantity product in product table
            $sqlProduct = "
				UPDATE " . DB_PREFIX . "product SET quantity = quantity - '" . (int)$value['quantity'] . "', date_modified = NOW() WHERE product_id = '" . (int)$value['product_id'] . "'
			";
            $this->db->query($sqlProduct);
        }

        return true;
    }

    public function getProductPaginator($data = array())
    {
        $sql = "SELECT p.status as p_status, p.sale_on_out_of_stock as p_sale_on_out_of_stock, p.product_id as pid, p.multi_versions, p.price as o_price, p.compare_price as o_compare_price, p.weight as weight, p.quantity as o_quantity, dp.name,p.image as p_image, pv.*,
                (CASE WHEN p.`multi_versions` = 1 THEN pv.`sku` ELSE p.`sku` END) final_sku 
                from " . DB_PREFIX . "product p 
                LEFT JOIN " . DB_PREFIX . "product_description as dp ON (p.product_id = dp.product_id) 
                LEFT JOIN " . DB_PREFIX . "product_version as pv ON CASE 
                                                                            WHEN p.multi_versions = 1 THEN p.product_id 
                                                                            ELSE -1
                                                                            END = pv.product_id 
                WHERE 1=1 and dp.language_id = " . (int)$this->config->get('config_language_id');
        if (!empty($data['filter_name'])) {
            $sql .= " AND dp.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (isset($data['current_user_id']) && !empty($data['current_user_id'])) {
            $sql .= " AND p.user_create_id = '" . (int)$data['current_user_id'] . "'";
        }

        $sql .= " AND p.`deleted` IS NULL ";
        $sql .= " AND pv.`deleted` IS NULL ";
        $sql .= " ORDER BY dp.name ASC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);
        $result = array();
        foreach ($query->rows as $k => $item) {
            $result[$k] = array(
                'product_id' => $item['pid'],
                'name' => mb_strlen($item['name']) > 90 ? mb_substr($item['name'], 0, 90) . "..." : $item['name'],
                'product_version_id' => $item['product_version_id'],
                'product_name' => $item['name'],
                'weight' => $item['weight'],
                'image' => $item['p_image'],
                'version' => $item['version'],
                'sale_on_out_of_stock' => $item['p_sale_on_out_of_stock'],
                'multi_versions' => $item['multi_versions'],
                'sku' => $item['final_sku']
            );

            if ($item['product_version_id'] != null) {
                $result[$k]['id'] = $item['pid'] . '-' . $item['product_version_id'];
                $result[$k]['quantity'] = $item['quantity'];
//                $result[$k]['price'] = $item['price'] != 0 ? $item['price'] : $item['compare_price'];
                $result[$k]['compare_price'] = $item['compare_price'];
                $result[$k]['price'] = $item['price'];
                $result[$k]['status'] = $item['status'];
                $result[$k]['p_status'] = $item['p_status'];
            } else {
                $result[$k]['id'] = $item['pid'];
                $result[$k]['quantity'] = $item['o_quantity'];
//                $result[$k]['price'] = $item['o_price'] != 0 ? $item['o_price'] : $item['o_compare_price'];
                $result[$k]['compare_price'] = $item['o_compare_price'];
                $result[$k]['price'] = $item['o_price'];
                $result[$k]['status'] = $item['p_status'];
                $result[$k]['p_status'] = $item['p_status'];
            }
        }

        return $result;
    }


    public function countProductAll($data = array())
    {
        $language_id = $this->config->get('config_language_id');
        $sql = "SELECT p.product_id,pd.name from " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description as pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_version as pv ON (p.product_id = pv.product_id) WHERE 1=1";
        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (isset($data['current_user_id']) && !empty($data['current_user_id'])) {
            $sql .= " AND p.user_create_id = '" . (int)$data['current_user_id'] . "'";
        }


        $sql .= " AND p.`deleted` IS NULL ";
        $sql .= " AND pv.`deleted` IS NULL ";
        $sql .= " AND pd.language_id = " . $language_id . "";
        $sql .= " ORDER BY p.product_id ASC";
        $query = $this->db->query($sql);

        return count($query->rows);
    }

    public function getProductImages($product_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "' ORDER BY sort_order ASC");
        return $query->rows;
    }

    public function getOrderStatus($order_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_id . "' and language_id = '" . (int)$this->config->get('config_language_id') . "'");
        return $query->row['name'];
    }

    public function getOrderStatusIdByOrderId($order_id)
    {
        $query = $this->db->query("SELECT `order_status_id` FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$order_id . "'");
        if (isset($query->row['order_status_id'])) {
            return (int)$query->row['order_status_id'];
        } else {
            return 0;
        }
    }

    public function getOrderStatusName($order_status_id)
    {
        $query = $this->db->query("SELECT name FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "'");
        if (isset($query->row['name'])) {
            return $query->row['name'];
        } else {
            return '';
        }
    }

    public function getAllOrders($data = array())
    {
        $sql = "SELECT o.order_code,o.fullname,o.telephone,o.email,o.shipping_address_1,o.comment,
                p.product_id,pd.name,op.price,op.quantity,o.order_status_id,op.total,o.date_modified,
                o.payment_status,o.payment_method,o.shipping_method,o.shipping_fee,o.shipping_province_code,
                o.shipping_district_code,o.shipping_ward_code
                FROM " . DB_PREFIX . "order o LEFT JOIN ";
        $sql .= "" . DB_PREFIX . "order_product op ON (o.order_id = op.order_id)";
        $sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (op.product_id = p.product_id)";
        $sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";
        $sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
        }

        $sort_data = array(
            'op.name',
            'o.fullname',
            'o.email',
            'o.total',
            'o.order_status_id',
            'o.date_added',
            'o.date_modified'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY o.date_modified";
        }

        if (isset($data['order']) && ($data['order'] == 'ASC')) {
            $sql .= " ASC";
        } else {
            $sql .= " DESC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getAllOrders_export($data = array())
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "order  ";
        //$sql .= "" . DB_PREFIX . "order_product op ON (o.order_id = op.order_id)";
        //$sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (op.product_id = p.product_id)";
        //$sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";
        //$sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        // filter:  order have product created by users in `filter_staff`
        if (!$this->user->canAccessAll()){
            $sql .= " WHERE order_id IN (
                                            SELECT order_id FROM " . DB_PREFIX . "order_product sop LEFT JOIN " . DB_PREFIX . "product sp ON sp.product_id = sop.product_id 
                                            WHERE sp.`user_create_id` = " . (int)$this->user->getId() . " GROUP BY sop.order_id
                                         )";
        }
        $sql .= " ORDER BY date_modified";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function order_product($order_id = '')
    {
        $sql = "SELECT *, op.`quantity` as op_quantity FROM " . DB_PREFIX . "order_product op";
        //$sql .= "" . DB_PREFIX . "order_product op ON (o.order_id = op.order_id)";
        //$sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (op.product_id = p.product_id)";
        $sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (op.product_id = pd.product_id)";
        $sql .= " WHERE  op.order_id = " . $order_id . " AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
        //$sql .= " ORDER BY date_modified";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function export()
    {
        $this->load->language('sale/order');

        /* get all products */
        $orders = $this->getAllOrders_export();

        // support multi_versions:
        $all_orders = [];

        $CHANGEABLE_ORDER_STATUS = [
            self::ORDER_STATUS_ID_DRAFT => $this->language->get('order_status_draft'),
            self::ORDER_STATUS_ID_PROCESSING => $this->language->get('order_status_processing'),
            self::ORDER_STATUS_ID_DELIVERING => $this->language->get('order_status_delivering'),
            self::ORDER_STATUS_ID_COMPLETED => $this->language->get('order_status_complete'),
            self::ORDER_STATUS_ID_CANCELLED => $this->language->get('order_status_canceled'),
        ];

        // refactor data
        $this->load->model('setting/setting');
        $payment_methods = $this->model_setting_setting->getSettingValue('payment_methods');
        $payment_methods = json_decode($payment_methods, true);
        $payment_methods = is_array($payment_methods) ? $payment_methods : [];

        foreach ($orders as $order) {

            $order['order_status_id'] = $CHANGEABLE_ORDER_STATUS[$order['order_status_id']];
            // Trang thai thanh toan
            $order['payment_status'] = ($order['payment_status'] == 1 ? $this->language->get('payment_status_success') : $this->language->get('payment_status_fail'));

            $this->load->model('localisation/vietnam_administrative');

            $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($order['shipping_province_code']);
            $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($order['shipping_district_code']);
            $ward = $this->model_localisation_vietnam_administrative->getWardByCode($order['shipping_ward_code']);

            $province['name'] = isset($province['name']) ? $province['name'] : '';
            $district['name'] = isset($district['name']) ? $district['name'] : '';
            $ward['name'] = isset($ward['name']) ? $ward['name'] : '';

            $order['shipping_address_1'] = convertAddressCustomer($order['shipping_address_1'], $province['name'], $district['name'], $ward['name']);
            $order['order_code'] = "'" . $order['order_code'];
            $name_product = $this->order_product($order['order_id']);
            $namepro = '';
            foreach ($name_product as $vl) {
                $namepro .= $vl['name'] . "\n";
            }
            $order['name'] = $namepro;
            $pricepro = '';
            foreach ($name_product as $vl) {
                $pricepro .= (float)$vl['price'] . "\n";
            }
            $order['price'] = $pricepro;

            $order['name'] = $namepro;
            $quantitypro = '';
            foreach ($name_product as $vl) {
                $quantitypro .= (float)$vl['op_quantity'] . "\n";
            }
            $order['quantity'] = $quantitypro;
            $order['telephone'] = "'" . $order['telephone'];

            $order['payment_method'] = $order['payment_method'] == self::APP_CODE_PAYMENT_VNPAY ? $this->language->get('txt_vnppay') : $order['payment_method'];

            array_filter($payment_methods, function ($value) use (&$order) {
                if ($value['payment_id'] == $order['payment_method']) {
                    $order['payment_method'] = $value['name'];
                }
            });
            $all_orders[] = $order;
        }

        /* create xlsx and bind product data to it */
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $column = 'A';
        $row = 1;

        // map fields from language
        $EXPORT_EXCEL_COLUMN_MAPPING = [
            'order_code' => $this->language->get('export_order_code'),
            'fullname' => $this->language->get('export_customer_name'),
            'telephone' => $this->language->get('export_customer_phone'),
            'email' => $this->language->get('export_customer_email'),
            'shipping_address_1' => $this->language->get('export_customer_address'),
            'comment' => $this->language->get('export_customer_note'),
            'name' => $this->language->get('export_product_name'),
            'price' => $this->language->get('export_product_price'),
            'quantity' => $this->language->get('export_product_amount'),
            'order_status_id' => $this->language->get('export_order_status'),
            'total' => $this->language->get('export_order_total'),
            'payment_status' => $this->language->get('export_payment_status'),
            'payment_method' => $this->language->get('export_payment_note'),
            'payment_code' => $this->language->get('export_payment_code'),
            'shipping_method' => $this->language->get('export_delivery'),
            'shipping_fee' => $this->language->get('export_delivery_fee'),
            'date_modified' => $this->language->get('export_order_update')
        ];

        // write header
        foreach ($EXPORT_EXCEL_COLUMN_MAPPING as $headerName) {
            $sheet->setCellValue($column . $row, $headerName);
            $column++;
        }

        // write rows
        $row++;
        foreach ($all_orders as $order) {
            $p = array_intersect_key($order, $EXPORT_EXCEL_COLUMN_MAPPING);
            $p = array_merge(array_flip(array_keys($EXPORT_EXCEL_COLUMN_MAPPING)), $p);
            $column = 'A';
            $name = '';
            foreach ($p as $col => $value) {
                $name .= $p['name'];
                $sheet->setCellValue($column . $row, $value);
                $sheet->getStyle('A:A')->getAlignment()->setHorizontal('top');
                $column++;
            }
            $spreadsheet->getActiveSheet()->getStyle('A')->getNumberFormat()->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
            $spreadsheet->getActiveSheet()->getStyle('C')->getNumberFormat()->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
            $spreadsheet->getActiveSheet()->getCell("G" . $row . "")->setValue($p['name']);
            $spreadsheet->getActiveSheet()->getStyle("G" . $row . "")->getAlignment()->applyFromArray(['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, 'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP, 'textRotation' => 0, 'wrapText' => TRUE]);
            $spreadsheet->getActiveSheet()->getCell("H" . $row . "")->setValue($p['price']);
            $spreadsheet->getActiveSheet()->getStyle("H" . $row . "")->getAlignment()->applyFromArray(['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, 'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP, 'textRotation' => 0, 'wrapText' => TRUE]);
            $spreadsheet->getActiveSheet()->getStyle('C')->getNumberFormat()->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);
            $spreadsheet->getActiveSheet()->getCell("I" . $row . "")->setValue($p['quantity']);
            $spreadsheet->getActiveSheet()->getStyle("I" . $row . "")->getAlignment()->applyFromArray(['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, 'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP, 'textRotation' => 0, 'wrapText' => TRUE]);

            $row++;
        }
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(70);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        /*
         * write output for downloading...
         * https://stackoverflow.com/questions/27701981/phpexcel-download-using-ajax-call
         */
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        ob_start();
        $writer->save("php://output"); // for test: DIR_APPLICATION . "orders.xlsx"
        $xlsxData = ob_get_contents();
        ob_end_clean();

        return 'data:application/vnd.ms-excel;base64,' . base64_encode($xlsxData);
    }


    public function getProductPrice($product_id, $product_version_id)
    {
        if ($product_version_id) {
            $sql = "SELECT price, compare_price
                FROM ".DB_PREFIX."product_version
                WHERE product_id=".$product_id." AND product_version_id=".$product_version_id;
        } else {
            $sql = "SELECT price, compare_price
                FROM ".DB_PREFIX."product
                WHERE product_id=".(int)$product_id;
        }
        $query = $this->db->query($sql);
        return $query->rows[0];
    }


    public function getProductsDetailByOrder($order_id, $source = '')
    {
        $DB_PREFIX = DB_PREFIX;
        $this->load->model('tool/image');
        $image_null = $this->model_tool_image->resize('no_image.png', 100, 100);
        $sql = "SELECT p.sale_on_out_of_stock as p_sale_on_out_of_stock, 
                       p.status as p_status, pv.status as pv_status, 
                       p.quantity as p_quantity, pv.quantity as pv_quantity, 
                       op.price as order_product_price, 
                       CASE WHEN p.image IS NULL OR p.image='' 
                            THEN '{$image_null}' 
                            ELSE p.image 
                       END AS image_check, 
                       CASE WHEN op.product_version_id IS NULL 
                            THEN op.product_id 
                            ELSE CONCAT(op.product_id, '-', op.product_version_id) 
                       END AS id_version, 
                       op.product_version_id, op.product_id, op.discount, op.total, p.image, 
                       CASE WHEN op.product_version_id IS NULL OR op.product_version_id = 0 
                            THEN p.price 
                            ELSE pv.price 
                       END as p_price, 
                       CASE WHEN op.product_version_id IS NULL OR op.product_version_id = 0 
                            THEN p.compare_price 
                            ELSE pv.compare_price 
                       END as p_compare_price, 
                       pd.name, op.quantity, pv.version 
                       FROM {$DB_PREFIX}order_product as op 
                       LEFT JOIN {$DB_PREFIX}product_description as pd ON op.product_id=pd.product_id 
                       LEFT JOIN {$DB_PREFIX}product p ON (op.product_id = p.product_id) 
                       LEFT JOIN {$DB_PREFIX}product_version pv ON (op.product_version_id = pv.product_version_id) 
                       WHERE order_id = '" . (int)$order_id . "' 
                         AND pd.language_id='" . (int)$this->config->get('config_language_id') . "'";

        $query = $this->db->query($sql);
        $product_list = $query->rows;

        foreach ($product_list as $key => $product) {
            $flag = true;
            if (empty($product['product_version_id'])) {
                if ($product['p_status'] == 0) {
                    $flag = false;
                }
                if ($product['p_sale_on_out_of_stock'] == 0 && $product['p_quantity'] < 1) {
                    $flag = false;
                }
            } else {
                if ($product['pv_status'] == 0 || $product['p_status'] == 0) {
                    $flag = false;
                }
                if ($product['p_sale_on_out_of_stock'] == 0 && $product['pv_quantity'] < 1) {
                    $flag = false;
                }
            }
            if ($product['p_price'] == 0) {
                $product_list[$key]['p_price'] = $product_list[$key]['p_compare_price'];
            }

            // for pos
            if ($source == 'pos') {
                $product_list[$key]['p_price'] = $product['order_product_price'];
            }

            $product_list[$key]['flag'] = $flag;
        }
        return $product_list;
    }

    public function updateInfoCustomer($data)
    {
        $sql = "UPDATE " . DB_PREFIX . "order SET date_modified = NOW(), fullname = '" . $this->db->escape(isset($data['fullname']) ? $data['fullname'] : '') . "',telephone = '" . $this->db->escape($data['telephone']) . "', ";
        $sql .= "shipping_ward_code = '" . $this->db->escape(isset($data['shipping_ward_code']) ? $data['shipping_ward_code'] : '') . "', shipping_district_code = '" . $this->db->escape(isset($data['shipping_district_code']) ? $data['shipping_district_code'] : '') . "' ,shipping_province_code = '" . $this->db->escape(isset($data['shipping_province_code']) ? $data['shipping_province_code'] : '') . "', ";
        $sql .= "shipping_address_1 = '" . $this->db->escape($data['shipping_address']) . "' WHERE order_id='" . (int)$data['order_id'] . "'";

        $this->db->query($sql);
    }

    public function updateProductOrder($products, $order_id)
    {
        $order = $this->getOrder($order_id);
        $order_products = $this->getOrderProducts($order_id);
        $order_status = isset($order['order_status_id']) ? $order['order_status_id'] : '';

        // put back order quantity to stock
        if ($order_status != self::ORDER_STATUS_ID_DRAFT && $order_status != self::ORDER_STATUS_ID_CANCELLED) {
            $this->updateProductOrderChangeStatus($order_id);
        }

        // todo: not use because param from view result one element
//        $this->deleteProductOrder($order_id);
//        if ($products) {
//            foreach ($products as $product) {
//                $productId = explode('-', $product['product_id'])[0];
//                $sql = "INSERT INTO " . DB_PREFIX . "order_product SET  order_id = '" . (int)$order_id . "', product_id = '" . (int)$productId . "', quantity = '" . (int)$product['quantity'] . "', price = '" . $product['price'] . "', total = '" . (int)$product['quantity'] * (int)$product['price'] . "'";
//
//                if ($product['product_version_id'] != 0) {
//                    $sql = "INSERT INTO " . DB_PREFIX . "order_product SET  order_id = '" . (int)$order_id . "', product_id = '" . (int)$productId . "', quantity = '" . (int)$product['quantity'] . "', price = '" . $product['price'] . "', total = '" . (int)$product['quantity'] * $product['price'] . "', product_version_id='" . (int)$product['product_version_id'] . "'";
//                }
//
//                $this->db->query($sql);
//            }
//        }

        //reduce quantity product in product table
        if ($order_status != self::ORDER_STATUS_ID_DRAFT && $order_status != self::ORDER_STATUS_ID_CANCELLED) {
            $this->updateProductOrderChangeStatus($order_id, false);
        }

    }

    public function updateProductOrderChangeStatus($order_id, $flag = true)
    {
        $condition = $flag ? '-' : '+';
        $products = $this->getOrderProductsDetail($order_id);
        foreach ($products as $product) {
            $default_store_id = isset($product['default_store_id']) ? (int)$product['default_store_id'] : 0;

            $sqlProduct = "UPDATE " . DB_PREFIX . "product_to_store 
					       SET quantity = quantity " . $condition . " '" . (int)$product['quantity'] . "' 
                           WHERE product_id = '" . (int)$product['product_id'] . "' 
                             AND product_version_id = '" . (int)$product['product_version_id'] . "' 
                             AND store_id = '" . (int)$default_store_id . "'";

            $result = $this->db->query($sqlProduct);

            // TODO: review this new code...
            if (!$result){
                $sqlProduct = "INSERT INTO " . DB_PREFIX . "product_to_store 
                               SET quantity = 0 " . $condition . " '" . (int)$product['quantity'] . "', 
                                   product_id = '" . (int)$product['product_id'] . "', 
                                   product_version_id = '" . (int)$product['product_version_id'] . "',
                                   store_id = '" . (int)$default_store_id . "'";

                $this->db->query($sqlProduct);
            }
        }
    }

    public function updateOrderComment($order_id, $comment)
    {
        $sql = "UPDATE " . DB_PREFIX . "order SET comment='" . $this->db->escape($comment) . "',  date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'";
        $this->db->query($sql);
    }

    public function updateColumnTotalOrder($order_id)
    {
        //TODO update total 
        $total = $this->getOrderProductsTotal($order_id);
        $products = $this->getOrderProductShipment($order_id);
        $shipping_fee = 0;
        foreach ($products as $product) {
            if ($product['shipping_method_value'] == -1) {
                $shipping_fee = 0;
                break;
            }
            if ($product['shipping_method_value'] == -2) {
                $shipping_fee = $product['shipping_fee'];
                break;
            }
            $shipping = $this->getTransportFee($product['shipping_method_value'], $product['shipping_province_code'], $product['weight'], $product['total']);
            $shipping_fee += $shipping['fee_order']['fee'];
        }
        $total += $shipping_fee;
        $sql = "UPDATE " . DB_PREFIX . "order SET total='" . (int)$total . "', shipping_fee = '" . (int)$shipping_fee . "' ,  date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'";
        $this->db->query($sql);
    }

    public function getOrderProductVersion($order_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT pd.name, 
                       op.price, 
                       op.quantity, 
                       op.total, 
                       op.order_product_id, 
                       pv.version, 
                       op.tax, 
                       op.discount, 
                       CASE WHEN op.product_version_id IS NULL OR op.product_version_id = 0 THEN p.sku ELSE pv.sku END AS sku_version 
                FROM {$DB_PREFIX}order_product as op LEFT JOIN {$DB_PREFIX}product as p ON p.product_id = op.product_id 
                LEFT JOIN {$DB_PREFIX}product_version as pv ON pv.product_version_id = op.product_version_id 
                LEFT JOIN {$DB_PREFIX}product_description as pd ON pd.product_id = op.product_id 
                WHERE pd.language_id ='" . (int)$this->config->get('config_language_id') . "' 
                  AND op.order_id='" . (int)$order_id . "' 
                GROUP BY op.`order_product_id`";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getOrderProductShipment($order_id)
    {
        $sql = "SELECT o.shipping_method, o.shipping_province_code, op.total, p.weight, o.shipping_method_value, o.shipping_fee  FROM " . DB_PREFIX . 'order as o ';
        $sql .= "LEFT JOIN " . DB_PREFIX . "order_product as op ON op.order_id = o.order_id ";
        $sql .= "LEFT JOIN " . DB_PREFIX . "product as p ON p.product_id = op.product_id ";
        $sql .= "WHERE op.order_id='" . (int)$order_id . "'";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTransportFee($transport = '', $city = '', $mass = '', $total_order = '')
    {

        $this->load->model('localisation/vietnam_administrative');
        $transport = $transport; // ID Phuong thuc van chuyen
        $city_code = $city;  // Van chuyen den thanh pho (VD: An Giang)
        $cityArr = $this->model_localisation_vietnam_administrative->getProvinceByCode($city_code);
        $city = '';
        if (isset($cityArr['name'])) {
            $city = $cityArr['name'];
        }
        $mass = $mass; // Khoi luong san pham
        $total_order = $total_order; // Tong gia tri don hang

        $this->load->model('sale/order');
        $this->load->model('setting/setting');
        $this->load->language('sale/order');
        $delivery_methods = $this->model_setting_setting->getSettingValue('delivery_methods');
        $delivery_methods = json_decode($delivery_methods, true);
        $delivery_methods = is_array($delivery_methods) ? $delivery_methods : [];

        foreach ($delivery_methods as $key => $vl) {
            if ($transport == $vl['id']) {
                $name_transport = $vl['name'];
                foreach ($vl['fee_regions'] as $value) {
                    if (in_array(trim($city), $value['provinces'])) {
                        foreach ($value['weight_steps'] as $vl_weight) {
                            if ($vl_weight['from'] <= $mass && $vl_weight['to'] >= $mass) {
                                $fee_price = str_replace(',', '', $vl_weight['price']); // Gia thuoc tinh thanh
                                break;
                            } else {
                                $fee_price = str_replace(',', '', $vl['fee_region']);  // Gia mac dinh khi khong thuoc tinh thanh
                            }
                        }
                    } else {
                        $fee_price = str_replace(',', '', $vl['fee_region']);  // Gia mac dinh khi khong thuoc tinh thanh
                    }
                }
                foreach ($vl['fee_cod'] as $key_cod => $fee_cod) {
                    $arr_fee_status = ($vl['fee_cod'][$key_cod]);
                    if ($arr_fee_status['status'] == 1) {
                        $arr_fee_status = str_replace(',', '', $arr_fee_status['value']);
                        if ($key_cod === 'dynamic') {
                            if ($total_order != 0) {
                                $value_fee_cod = ($arr_fee_status / 100) * $total_order;
                            } else {
                                $value_fee_cod = 0;
                            }
                        } elseif ($key_cod === 'fixed') {
                            $value_fee_cod = $arr_fee_status;
                        }
                    }
                }
            }
        }
        if (!empty($fee_price) && !empty($value_fee_cod)) {
            $json['fee_order'] = [
                'name' => $name_transport,
                'fee' => $fee_price + $value_fee_cod,
                'error' => '',
            ];
        } else {
            $json['fee_order'] = [
                'name' => (isset($name_transport) ? $name_transport : ''),
                'fee' => '',
                'error' => '',
            ];
        }
        return $json;
    }

    public function updateOrderPaymentStatus($order_id, $status)
    {
        $sql = "UPDATE " . DB_PREFIX . "order SET payment_status = '" . (int)$status . "' WHERE order_id = '" . (int)$order_id . "'";
        $this->db->query($sql);
    }

    public function updatePreviousOrderStatusId($order_id, $previous_order_status_id)
    {
        $sql = "UPDATE " . DB_PREFIX . "order SET previous_order_status_id = '" . (int)$previous_order_status_id . "' WHERE order_id = '" . (int)$order_id . "'";
        $this->db->query($sql);
    }

    public function updateTransportDeliveryApi($order_id, $order_code, $transport_method, $service_name, $money_total, $exchange_weight, $transport_status, $transport_current_warehouse)
    {
        $sql = "UPDATE " . DB_PREFIX . "order SET 
        transport_order_code  = '" . $this->db->escape($order_code) . "', 
        transport_name = '" . $this->db->escape($transport_method) . "', 
        transport_service_name = '" . $this->db->escape($service_name) . "', 
        transport_money_total = '" . $this->db->escape($money_total) . "', 
        transport_weight = '" . $this->db->escape($exchange_weight) . "',
        transport_status = '" . $this->db->escape($transport_status) . "',
        transport_current_warehouse = '" . $this->db->escape($transport_current_warehouse) . "'
        WHERE order_id = '" . (int)$order_id . "'";
        $this->db->query($sql);
    }

    public function updateStatusTransportDeliveryApi($transport_order_code, $transport_status)
    {
        $sql = "UPDATE " . DB_PREFIX . "order SET transport_status = '" . $transport_status . "' WHERE transport_order_code  = '" . $transport_order_code . "'";
        $this->db->query($sql);
    }

    public function updateStatusDeliveryApi($order_id, $transport_status)
    {
        $sql = "UPDATE " . DB_PREFIX . "order SET transport_status = '" . $transport_status . "' WHERE order_id  = '" . (int)$order_id . "'";
        $this->db->query($sql);
    }

    public function isStock($product_id, $product_version_id)
    {
        $sql = "SELECT p.sale_on_out_of_stock, 
                       pts.quantity 
                FROM " . DB_PREFIX . "product_to_store pts 
                LEFT JOIN `" . DB_PREFIX . "product` p ON p.`product_id` = pts.`product_id` 
                WHERE pts.`product_id` = " . (int)$product_id . " 
                AND pts.`product_version_id` = " . (int)$product_version_id . " 
                AND pts.`store_id` = p.`default_store_id`";
        $query = $this->db->query($sql);

        /* notice: if product not select any store for quantity, known as quantity = 0, then check sale_on_out_of_stock only! */
        if (!isset($query->row['quantity'])) {
            $sql = "SELECT p.sale_on_out_of_stock 
                    FROM " . DB_PREFIX . "product p 
                    WHERE p.`product_id` = " . (int)$product_id;
            $query = $this->db->query($sql);

            return isset($query->row['sale_on_out_of_stock']) && $query->row['sale_on_out_of_stock'] == 1;
        }

        return ($query->row['quantity'] > 0) || (isset($query->row['sale_on_out_of_stock']) && $query->row['sale_on_out_of_stock'] == 1);
    }

    /**
     * update Related Order Products due to product_id (price change, ...)
     *
     * @param $product_id
     */
    public function updateRelatedOrderProducts($product_id)
    {
        if (empty($product_id)) {
            return;
        }

        try {
            $this->load->model('catalog/product');

            $latest_product = $this->model_catalog_product->getProduct($product_id);
            $latest_product_versions = $this->model_catalog_product->getProductVersions($product_id);
            // remap product_version_id as key
            foreach ($latest_product_versions as $key => $lpv) {
                if (!isset($lpv['product_version_id'])) {
                    continue;
                }

                $latest_product_versions[$lpv['product_version_id']] = $lpv;
                unset($latest_product_versions[$key]);
            }

            // only for order if in some statuses
            $order_statuses = [
                ModelSaleOrder::ORDER_STATUS_ID_DRAFT
            ];
            $related_order_products = $this->model_sale_order->getRelatedOrderProductsByProductId($product_id, $order_statuses);

            foreach ($related_order_products as $related_order_product) {
                // single version
                $related_pv_id = $related_order_product['product_version_id'];
                if (!$related_pv_id) {
                    if (!array_key_exists('price', $latest_product) || !array_key_exists('compare_price', $latest_product)) {
                        continue;
                    }

                    $price_for_sale = !empty($latest_product['price']) && $latest_product['price'] > 0 ? $latest_product['price'] : $latest_product['compare_price'];
                } else { // multi versions
                    $latest_pv = isset($latest_product_versions[$related_pv_id]) ? $latest_product_versions[$related_pv_id] : [];
                    if (!array_key_exists('price', $latest_pv) || !array_key_exists('compare_price', $latest_pv)) {
                        continue;
                    }

                    $price_for_sale = !empty($latest_pv['price']) && $latest_pv['price'] > 0 ? $latest_pv['price'] : $latest_pv['compare_price'];
                }

                $related_order_product['price'] = $price_for_sale;
                //$related_order_product['discount'] = ?;
                $related_order_product['total'] = $price_for_sale * $related_order_product['quantity'];

                // product_order
                $DB_PREFIX = DB_PREFIX;
                $this->db->query("DELETE FROM {$DB_PREFIX}order_product 
                              WHERE order_id = '" . (int)$related_order_product['order_id'] . "' 
                              AND product_id = '" . (int)$related_order_product['product_id'] . "' 
                              AND product_version_id = '" . (int)$related_order_product['product_version_id'] . "'");

                $this->db->query("INSERT INTO {$DB_PREFIX}order_product 
                                  SET order_id = " . (int)$related_order_product['order_id'] . ", 
                                      product_id = " . (int)$related_order_product['product_id'] . ", 
                                      product_version_id =" . (int)$related_order_product['product_version_id'] . ", 
                                      quantity = " . (int)$related_order_product['quantity'] . ", 
                                      price = '{$related_order_product['price']}', 
                                      discount = '{$related_order_product['discount']}', 
                                      total = '{$related_order_product['total']}'");

                // update order info such as: total, discount, transport fee, ...
                $this->reCalculateOrderInfo($related_order_product['order_id']);
            }
        } catch (Exception $e) {
            // do something...
        }
    }

    /**
     * refresh order info such as total, discount, transport fee, ...
     *
     * @param int $order_id
     */
    public function reCalculateOrderInfo($order_id)
    {
        if (empty($order_id)) {
            return;
        }

        $order = $this->getOrder($order_id);
        if (empty($order)) {
            return;
        }

        $order_product = $this->getOrderProducts($order_id);
        if (empty($order_product)) {
            return;
        }

        $total_amount = 0;
        foreach ($order_product as $item) {
            $total_amount += $item['total'];
        }

        $DB_PREFIX = DB_PREFIX;
        $shipping_fee = $order['shipping_fee']; // TODO: re-calc shipping_fee
        $discount_order = $order['discount']; // TODO: re-calc discount
        $total_pay = (float)$total_amount - (float)$discount_order + (float)$shipping_fee;

        $this->db->query("UPDATE `{$DB_PREFIX}order` 
                          SET shipping_fee = {$shipping_fee}, 
                          discount = {$discount_order}, 
                          total = '" . $this->db->escape($total_pay) . "', 
                          date_modified = NOW() 
                          WHERE order_id = " . (int)$order_id);
    }

    public function getShopeeShopName($order_code)
    {
        if (!$order_code){
            return '';
        }

        $query = $this->db->query("SELECT ssc.`shop_name` FROM `" . DB_PREFIX . "shopee_order` so LEFT JOIN `" . DB_PREFIX . "shopee_shop_config` ssc ON (so.`shopee_shop_id` = ssc.`shop_id`) WHERE so.`code` = '" . $this->db->escape($order_code) . "'");

        if (isset($query->row['shop_name'])){
            return $query->row['shop_name'];
        }

        return '';
    }

    public function getLazadaShopName($order_code)
    {
        if (!$order_code){
            return '';
        }

        $query = $this->db->query("SELECT lsc.`shop_name` FROM `" . DB_PREFIX . "lazada_order` lo 
                                    LEFT JOIN `" . DB_PREFIX . "lazada_shop_config` lsc ON (lo.`lazada_shop_id` = lsc.`id`) 
                                        WHERE lo.`order_id` = '" . $this->db->escape($order_code) . "'");

        if (isset($query->row['shop_name'])){
            return $query->row['shop_name'];
        }

        return '';
    }


    // USE for return receipt
    public function getOrdersListData(array $data)
    {
        $sql = "SELECT o.total, o.date_added, o.source, o.order_code, o.`payment_status`, o.`order_status_id`, o.`date_modified`, 
                        o.order_id, 
                        o.telephone as telephone_order, 
                        CONCAT(customer.lastname, ' ', customer.firstname) as customer_full_name, 
                        o.fullname as order_full_name FROM `" . DB_PREFIX . "order` as o";

        $sql .= " LEFT JOIN `" . DB_PREFIX . "customer` as customer ON o.`customer_id` = customer.`customer_id` ";
        $sql .= " WHERE 1";
        // not show order with payment -status = 3
        $sql .= " AND o.`payment_status` != '3'";
        $sql .= " AND o.`order_status_id` = '9'";
        $sql .= " AND o.`source` NOT IN('shopee', 'lazada', 'tiki')";

        // filter order_code
        if (!empty($data['order_code'])) {
            $sql .= " AND o.`order_code` LIKE '%" . $this->db->escape($data['order_code']) . "%' ";
        }

        // filter order_source
        if (!empty($data['order_source'])) {
            $sql .= " AND o.`source` LIKE '" . $this->db->escape($data['order_source']) . "' ";
        }

        if (!empty($data['time_from']) && !empty($data['time_to'])) {
            $sql .= " AND o.date_added BETWEEN '" . $data['time_from'] . "' AND '" . $data['time_to'] . "'";
        }

        if (isset($data['current_user_id']) && $data['current_user_id'] !== '') {
            $sql .= " AND o.order_id IN (
                                            SELECT order_id FROM " . DB_PREFIX . "order_product sop LEFT JOIN " . DB_PREFIX . "product sp ON sp.product_id = sop.product_id 
                                            WHERE sp.`user_create_id` = " . $data['current_user_id'] . " GROUP BY sop.order_id
                                         )";

        }

        $sql .= " GROUP BY o.`order_id`";
        $sql .= " ORDER BY o.`date_modified` DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getOrdersListTotal(array $data)
    {
        $sql = "SELECT COUNT(DISTINCT o.order_id ) AS total FROM `" . DB_PREFIX . "order` as o ";

        $sql .= " LEFT JOIN `" . DB_PREFIX . "customer` as customer ON o.`customer_id` = customer.`customer_id` ";
        $sql .= " WHERE 1";
        // not show order with payment -status = 3
        $sql .= " AND o.`payment_status` != '3'";
        $sql .= " AND o.`order_status_id` = '9'";
        $sql .= " AND o.`source` NOT IN('shopee', 'lazada', 'tiki')";

        // filter order_code
        if (!empty($data['order_code'])) {
            $sql .= " AND o.`order_code` LIKE '%" . $this->db->escape($data['order_code']) . "%' ";
        }

        // filter order_source
        if (!empty($data['order_source'])) {
            $sql .= " AND o.`source` LIKE '" . $this->db->escape($data['order_source']) . "' ";
        }

        if (!empty($data['time_from']) && !empty($data['time_to'])) {
            $sql .= " AND o.date_added BETWEEN '" . $data['time_from'] . "' AND '" . $data['time_to'] . "'";
        }

        if (isset($data['current_user_id']) && $data['current_user_id'] !== '') {
            $sql .= " AND o.order_id IN (
                                            SELECT order_id FROM " . DB_PREFIX . "order_product sop LEFT JOIN " . DB_PREFIX . "product sp ON sp.product_id = sop.product_id 
                                            WHERE sp.`user_create_id` = " . $data['current_user_id'] . " GROUP BY sop.order_id
                                         )";

        }

        $sql .= " ORDER BY o.`date_modified` DESC";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function checkOrderPermission($order_id)
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "order_product op 
                        LEFT JOIN " . DB_PREFIX . "product p ON p.product_id = op.product_id 
                            WHERE op.order_id = " . (int)$order_id . " AND p.user_create_id = " . (int)$this->user->getId();

        $query = $this->db->query($sql);

        return $query->num_rows > 0;
    }

    public function updateCollectionAmount($order_id, $collection_amount)
    {
        $DB_PREFIX = DB_PREFIX;
        $this->db->query("UPDATE `{$DB_PREFIX}order` 
                          SET collection_amount = " . (float)$collection_amount . " 
                          WHERE order_id = " . (int)$order_id);
    }

    public function getOrderByOrderId($order_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM `{$DB_PREFIX}order` WHERE order_id = " . (int)$order_id;
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getOrderProductByOrderId($order_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $products = [];
        $sql = "SELECT * FROM `{$DB_PREFIX}order_product` WHERE order_id = " . (int)$order_id;

        $query = $this->db->query($sql);

        foreach ($query->rows as $item) {
            if (empty($item['product_id'])) {
                continue;
            }

            $this->load->model('catalog/product');
            $product = $this->model_catalog_product->getProduct($item['product_id']);
            $product_version = $this->getProductVersionInfo($item['product_id'], $item['product_version_id']);
            $product_attributes = $this->model_catalog_product->getProductAttributes($item['product_id']);
            $products[] = [
                'name' => $product['name'],
                'sku' => $product['sku'],
                'common_sku' => $product['common_sku'],
                'source' => $product['source'],
                'brand_shop_name' => isset($product['brand_shop_name']) ? $product['brand_shop_name'] : null,
                'version' => isset($product_version['version']) ? $product_version['version'] : '',
                'version_sku' => isset($product_version['sku']) ? $product_version['sku'] : '',
                'quantity' => $item['quantity'],
                'price' => (float)$item['price'],
                'discount' => (float)$item['discount'],
                'total' => (float)$item['total'],
                'tax' => (float)$item['tax'],
                'weight' => (float)$product['weight'],
                'description' => $product['description'],
                'sub_description' => $product['sub_description'],
                'sale_on_out_of_stock' => $product['sale_on_out_of_stock'],
                'multi_versions' => $product['multi_versions'],
                'original_price' => $product['compare_price'],
                'promotion_price' => $product['price'],
                'version_original_price' => isset($product_version['compare_price']) ? $product_version['compare_price'] : '',
                'version_promotion_price' => isset($product_version['price']) ? $product_version['price'] : '',
                'product_attributes' => json_encode($product_attributes),
                'barcode' => $product['barcode'],
                'version_barcode' => isset($product_version['barcode']) ? $product_version['barcode'] : '',
            ];
        }

        return $products;
    }

    public function getProductVersionInfo($product_id, $version_id)
    {
        if (empty($product_id) || empty($version_id)) {
            return  [];
        }

        $sql = "SELECT version, sku, price, compare_price, barcode from " . DB_PREFIX . "product_version as pv WHERE pv.product_id = '" . (int)$product_id . "' AND pv.product_version_id ='" . (int)$version_id . "'";
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function sendToRabbitMq($order_id)
    {
        try {
            if (!empty($order_id)) {
                $message = [
                    'shop_name' => $this->config->get('shop_name'),
                    'order' => []
                ];
                $order = $this->getOrderByOrderId($order_id);
                if (isset($order) && !empty($order) && !empty($order['order_status_id']) && in_array($order['order_status_id'], [7,8,9,10])) {
                    $message['order'] = [
                        'order_code' => $order['order_code'],
                        'order_status' => $order['order_status_id'],
                        'total' => (float)$order['total'],
                        'order_product' => $this->getOrderProductByOrderId($order_id),
                        'customer' => $this->dataCustomer($order)
                    ];

                    $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'order', '', 'order', json_encode($message));
                }
            }
        } catch (Exception $exception) {
            $this->log->write('Send message order error: ' . $exception->getMessage());
        }
    }

    public function dataCustomer($order_info)
    {
        try {
            $this->load->model('customer/customer');

            $customer = $this->model_customer_customer->getCustomer($order_info['customer_id']);
            $customer_address = $this->model_customer_customer->getAddressByCustomerId($order_info['customer_id']);

            $this->load->model('localisation/vietnam_administrative');

            $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($customer_address['city']);
            $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($customer_address['district']);
            $ward = $this->model_localisation_vietnam_administrative->getWardByCode($customer_address['wards']);

            return [
                'customer_full_name' => (isset($customer['lastname']) ? $customer['lastname'] : '') . ' ' . (isset($customer['firstname']) ? $customer['firstname'] : ''),
                'customer_email' => (isset($customer['email']) ? $customer['email'] : ''),
                'customer_telephone' => (isset($customer['telephone']) ? $customer['telephone'] : ''),
                'customer_address' => $customer_address['address'] . (!empty($ward['name']) ? ', ' . $ward['name'] : '') . (!empty($district['name']) ? ', ' . $district['name'] : '') . (!empty($province['name']) ? ', ' . $province['name'] : ''),
            ];
        } catch (Exception $exception) {
            $this->log->write('SendToRabbitMq dataCustomer for order got error : ' . $exception->getMessage());

            return [];
        }
    }

    public function getOrderVoucherByOrderId($order_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM `{$DB_PREFIX}order_voucher` WHERE `order_id` = {$order_id}";

        $query = $this->db->query($sql);
        return $query->row;
    }

    public function getVoucherById($voucher_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM `{$DB_PREFIX}coupon` WHERE `coupon_id` = {$voucher_id}";

        $query = $this->db->query($sql);
        return $query->row;
    }

    /**
     * delete product caches
     */
    private function deleteProductCaches() {
        $product_key_redis =  DB_PREFIX . 'product';
        $this->cache->delete($product_key_redis);
    }
}
