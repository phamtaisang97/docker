<?php

class ModelSaleOrderOld extends Model
{
    public function getOrder($order_id)
    {
        $order_query = $this->db->query("SELECT *, (SELECT CONCAT(c.firstname, ' ', c.lastname) FROM " . DB_PREFIX . "customer c WHERE c.customer_id = o.customer_id) AS customer, (SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int)$this->config->get('config_language_id') . "') AS order_status FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int)$order_id . "'");

        if ($order_query->num_rows) {
            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");

            if ($country_query->num_rows) {
                $payment_iso_code_2 = $country_query->row['iso_code_2'];
                $payment_iso_code_3 = $country_query->row['iso_code_3'];
            } else {
                $payment_iso_code_2 = '';
                $payment_iso_code_3 = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");

            if ($zone_query->num_rows) {
                $payment_zone_code = $zone_query->row['code'];
            } else {
                $payment_zone_code = '';
            }

            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

            if ($country_query->num_rows) {
                $shipping_iso_code_2 = $country_query->row['iso_code_2'];
                $shipping_iso_code_3 = $country_query->row['iso_code_3'];
            } else {
                $shipping_iso_code_2 = '';
                $shipping_iso_code_3 = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

            if ($zone_query->num_rows) {
                $shipping_zone_code = $zone_query->row['code'];
            } else {
                $shipping_zone_code = '';
            }

            $reward = 0;

            $order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

            foreach ($order_product_query->rows as $product) {
                $reward += $product['reward'];
            }

            $this->load->model('customer/customer');

            $affiliate_info = $this->model_customer_customer->getCustomer($order_query->row['affiliate_id']);

            if ($affiliate_info) {
                $affiliate_firstname = $affiliate_info['firstname'];
                $affiliate_lastname = $affiliate_info['lastname'];
            } else {
                $affiliate_firstname = '';
                $affiliate_lastname = '';
            }

            $this->load->model('localisation/language');

            $language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);

            if ($language_info) {
                $language_code = $language_info['code'];
            } else {
                $language_code = $this->config->get('config_language');
            }

            return array(
                'order_id' => $order_query->row['order_id'],
                'invoice_no' => $order_query->row['invoice_no'],
                'invoice_prefix' => $order_query->row['invoice_prefix'],
                'store_id' => $order_query->row['store_id'],
                'store_name' => $order_query->row['store_name'],
                'store_url' => $order_query->row['store_url'],
                'customer_id' => $order_query->row['customer_id'],
                'customer' => $order_query->row['customer'],
                'customer_group_id' => $order_query->row['customer_group_id'],
                'firstname' => $order_query->row['firstname'],
                'lastname' => $order_query->row['lastname'],
                'email' => $order_query->row['email'],
                'telephone' => $order_query->row['telephone'],
                'custom_field' => json_decode($order_query->row['custom_field'], true),
                'payment_firstname' => $order_query->row['payment_firstname'],
                'payment_lastname' => $order_query->row['payment_lastname'],
                'payment_company' => $order_query->row['payment_company'],
                'payment_address_1' => $order_query->row['payment_address_1'],
                'payment_address_2' => $order_query->row['payment_address_2'],
                'payment_postcode' => $order_query->row['payment_postcode'],
                'payment_city' => $order_query->row['payment_city'],
                'payment_zone_id' => $order_query->row['payment_zone_id'],
                'payment_zone' => $order_query->row['payment_zone'],
                'payment_zone_code' => $payment_zone_code,
                'payment_country_id' => $order_query->row['payment_country_id'],
                'payment_country' => $order_query->row['payment_country'],
                'payment_iso_code_2' => $payment_iso_code_2,
                'payment_iso_code_3' => $payment_iso_code_3,
                'payment_address_format' => $order_query->row['payment_address_format'],
                'payment_custom_field' => json_decode($order_query->row['payment_custom_field'], true),
                'payment_method' => $order_query->row['payment_method'],
                'payment_code' => $order_query->row['payment_code'],
                'shipping_firstname' => $order_query->row['shipping_firstname'],
                'shipping_lastname' => $order_query->row['shipping_lastname'],
                'shipping_company' => $order_query->row['shipping_company'],
                'shipping_address_1' => $order_query->row['shipping_address_1'],
                'shipping_address_2' => $order_query->row['shipping_address_2'],
                'shipping_postcode' => $order_query->row['shipping_postcode'],
                'shipping_city' => $order_query->row['shipping_city'],
                'shipping_zone_id' => $order_query->row['shipping_zone_id'],
                'shipping_zone' => $order_query->row['shipping_zone'],
                'shipping_zone_code' => $shipping_zone_code,
                'shipping_country_id' => $order_query->row['shipping_country_id'],
                'shipping_country' => $order_query->row['shipping_country'],
                'shipping_iso_code_2' => $shipping_iso_code_2,
                'shipping_iso_code_3' => $shipping_iso_code_3,
                'shipping_address_format' => $order_query->row['shipping_address_format'],
                'shipping_custom_field' => json_decode($order_query->row['shipping_custom_field'], true),
                'shipping_method' => $order_query->row['shipping_method'],
                'shipping_code' => $order_query->row['shipping_code'],
                'comment' => $order_query->row['comment'],
                'total' => $order_query->row['total'],
                'reward' => $reward,
                'order_status_id' => $order_query->row['order_status_id'],
                'order_status' => $order_query->row['order_status'],
                'affiliate_id' => $order_query->row['affiliate_id'],
                'affiliate_firstname' => $affiliate_firstname,
                'affiliate_lastname' => $affiliate_lastname,
                'commission' => $order_query->row['commission'],
                'language_id' => $order_query->row['language_id'],
                'language_code' => $language_code,
                'currency_id' => $order_query->row['currency_id'],
                'currency_code' => $order_query->row['currency_code'],
                'currency_value' => $order_query->row['currency_value'],
                'ip' => $order_query->row['ip'],
                'forwarded_ip' => $order_query->row['forwarded_ip'],
                'user_agent' => $order_query->row['user_agent'],
                'accept_language' => $order_query->row['accept_language'],
                'date_added' => $order_query->row['date_added'],
                'date_modified' => $order_query->row['date_modified']
            );
        } else {
            return;
        }
    }

    public function getOrders($data = array())
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "order` ";
        $statusArray = array_keys($this->getListStatus());
        $array = implode(",", $statusArray);
        // var_dump($array);die;
        if (!empty($data['order_status'])) {
            $sql .= "WHERE order_status = '" . (int)$data['order_status'] . "' ";
        } else {
            $sql .= "WHERE order_status IN (" . $array . ")";
        }

        if (!empty($data['order_payment'])) {
            $sql .= " AND order_payment = '" . (int)$data['order_payment'] . "' ";
        }
        if (!empty($data['order_transfer'])) {
            $sql .= " AND order_transfer = '" . (int)$data['order_transfer'] . "' ";
        }
        if (!empty($data['order_search_text'])) {
            $sql .= " AND fullname LIKE '%" . $this->db->escape($data['order_search_text']) . "%' ";
        }

        $sql .= "ORDER BY order_id";
        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getLastestOrders($data = ['start' => 0, 'limit' => 10])
    {
        $sql = "SELECT `order_id`, `fullname`, `order_status`, `total`, `date_added` FROM `" . DB_PREFIX . "order` ";
        $sql .= "ORDER BY date_added DESC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getLastestOrdersBydate($data)
    {
        $data['filter_date_added_end'] = date("Y-m-d", strtotime($data['filter_date_added_end'] . ' + 1 days')); /// compare dateTime < date
        $sql = "SELECT `date_added`, `total` FROM `" . DB_PREFIX . "order` WHERE `date_added` >= DATE('" . $this->db->escape($data['filter_date_added_start']) . "') AND `date_added` < DATE('" . $this->db->escape($data['filter_date_added_end']) . "')";
        if (!empty($data['order_status'])) {
            $sql .= "AND order_status = '" . (int)$data['order_status'] . "' ";
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getOrderProducts($order_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

        return $query->rows;
    }

    public function getOrderOptions($order_id, $order_product_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product_id . "'");

        return $query->rows;
    }

    public function getOrderVouchers($order_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");

        return $query->rows;
    }

    public function getOrderVoucherByVoucherId($voucher_id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_voucher` WHERE voucher_id = '" . (int)$voucher_id . "'");

        return $query->row;
    }

    public function getOrderTotals($order_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order");

        return $query->rows;
    }

    public function getTotalOrders($data = array())
    {
        $sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order`";
        $statusArray = array_keys($this->getListStatus());
        $array = implode(",", $statusArray);
        if (!empty($data['order_status'])) {
            $sql .= "WHERE order_status = '" . (int)$data['order_status'] . "' ";
        } else {
            $sql .= "WHERE order_status IN (" . $array . ")";
        }

        if (!empty($data['order_payment'])) {
            $sql .= " AND order_payment = '" . (int)$data['order_payment'] . "' ";
        }
        if (!empty($data['order_transfer'])) {
            $sql .= " AND order_transfer = '" . (int)$data['order_transfer'] . "' ";
        }
        if (!empty($data['order_search_text'])) {
            $sql .= " AND fullname LIKE '%" . $this->db->escape($data['order_search_text']) . "%' ";
        }
        if (!empty($data['filter_date_added'])) {
            $data['filter_date_added'] = date("Y-m-d", strtotime($data['filter_date_added'] . ' + 1 days')); /// compare dateTime < date
            $sql .= " AND date_added < '" . $data['filter_date_added'] . "' ";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalOrdersByStoreId($store_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE store_id = '" . (int)$store_id . "'");

        return $query->row['total'];
    }

    public function getTotalOrdersByOrderStatusId($order_status_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE order_status_id = '" . (int)$order_status_id . "' AND order_status_id > '0'");

        return $query->row['total'];
    }

    public function getTotalOrdersByProcessingStatus()
    {
        $implode = array();

        $order_statuses = $this->config->get('config_processing_status');

        foreach ($order_statuses as $order_status_id) {
            $implode[] = "order_status_id = '" . (int)$order_status_id . "'";
        }

        if ($implode) {
            $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE " . implode(" OR ", $implode));

            return $query->row['total'];
        } else {
            return 0;
        }
    }

    public function getTotalOrdersByCompleteStatus()
    {
        $implode = array();

        $order_statuses = $this->config->get('config_complete_status');

        foreach ($order_statuses as $order_status_id) {
            $implode[] = "order_status_id = '" . (int)$order_status_id . "'";
        }

        if ($implode) {
            $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE " . implode(" OR ", $implode) . "");

            return $query->row['total'];
        } else {
            return 0;
        }
    }

    public function getTotalOrdersByLanguageId($language_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE language_id = '" . (int)$language_id . "' AND order_status_id > '0'");

        return $query->row['total'];
    }

    public function getTotalOrdersByCurrencyId($currency_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE currency_id = '" . (int)$currency_id . "' AND order_status_id > '0'");

        return $query->row['total'];
    }

    public function getTotalSales($data = array())
    {
        $sql = "SELECT SUM(total) AS total FROM `" . DB_PREFIX . "order`";

        if (!empty($data['filter_order_status'])) {
            $implode = array();

            $order_statuses = explode(',', $data['filter_order_status']);

            foreach ($order_statuses as $order_status_id) {
                $implode[] = "order_status_id = '" . (int)$order_status_id . "'";
            }

            if ($implode) {
                $sql .= " WHERE (" . implode(" OR ", $implode) . ")";
            }
        } elseif (isset($data['filter_order_status_id']) && $data['filter_order_status_id'] !== '') {
            $sql .= " WHERE order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
        } else {
            $sql .= " WHERE order_status_id > '0'";
        }

        if (!empty($data['filter_order_id'])) {
            $sql .= " AND order_id = '" . (int)$data['filter_order_id'] . "'";
        }

        if (!empty($data['filter_customer'])) {
            $sql .= " AND CONCAT(firstname, ' ', o.lastname) LIKE '%" . $this->db->escape($data['filter_customer']) . "%'";
        }

        if (!empty($data['filter_date_added'])) {
            $sql .= " AND DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if (!empty($data['filter_date_modified'])) {
            $sql .= " AND DATE(date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')";
        }

        if (!empty($data['filter_total'])) {
            $sql .= " AND total = '" . (float)$data['filter_total'] . "'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function createInvoiceNo($order_id)
    {
        $order_info = $this->getOrder($order_id);

        if ($order_info && !$order_info['invoice_no']) {
            $query = $this->db->query("SELECT MAX(invoice_no) AS invoice_no FROM `" . DB_PREFIX . "order` WHERE invoice_prefix = '" . $this->db->escape($order_info['invoice_prefix']) . "'");

            if ($query->row['invoice_no']) {
                $invoice_no = $query->row['invoice_no'] + 1;
            } else {
                $invoice_no = 1;
            }

            $this->db->query("UPDATE `" . DB_PREFIX . "order` SET invoice_no = '" . (int)$invoice_no . "', invoice_prefix = '" . $this->db->escape($order_info['invoice_prefix']) . "' WHERE order_id = '" . (int)$order_id . "'");

            return $order_info['invoice_prefix'] . $invoice_no;
        }
    }

    public function getOrderHistories($order_id, $start = 0, $limit = 10)
    {
        if ($start < 0) {
            $start = 0;
        }

        if ($limit < 1) {
            $limit = 10;
        }

        $query = $this->db->query("SELECT oh.date_added, os.name AS status, oh.comment, oh.notify FROM " . DB_PREFIX . "order_history oh LEFT JOIN " . DB_PREFIX . "order_status os ON oh.order_status_id = os.order_status_id WHERE oh.order_id = '" . (int)$order_id . "' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY oh.date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

        return $query->rows;
    }

    public function getTotalOrderHistories($order_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_history WHERE order_id = '" . (int)$order_id . "'");

        return $query->row['total'];
    }

    public function getTotalOrderHistoriesByOrderStatusId($order_status_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_history WHERE order_status_id = '" . (int)$order_status_id . "'");

        return $query->row['total'];
    }

    public function getEmailsByProductsOrdered($products, $start, $end)
    {
        $implode = array();

        foreach ($products as $product_id) {
            $implode[] = "op.product_id = '" . (int)$product_id . "'";
        }

        $query = $this->db->query("SELECT DISTINCT email FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) WHERE (" . implode(" OR ", $implode) . ") AND o.order_status_id <> '0' LIMIT " . (int)$start . "," . (int)$end);

        return $query->rows;
    }

    public function getTotalEmailsByProductsOrdered($products)
    {
        $implode = array();

        foreach ($products as $product_id) {
            $implode[] = "op.product_id = '" . (int)$product_id . "'";
        }

        $query = $this->db->query("SELECT COUNT(DISTINCT email) AS total FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) WHERE (" . implode(" OR ", $implode) . ") AND o.order_status_id <> '0'");

        return $query->row['total'];
    }

    //tunglaso1
    public function getListStatus()
    {
        $data = array(
            1 => 'Nháp',
            2 => 'Hoàn thành',
            3 => 'Đang xử lý',
            4 => 'Không thành công',
            5 => 'Đã huỷ ',
        );

        return $data;
    }

    public function getListStatusExceptDraft()
    {
        $data = array(
            2 => 'Hoàn thành',
            3 => 'Đang xử lý',
            4 => 'Không thành công',
            5 => 'Đã huỷ ',
        );

        return $data;
    }

    public function getNameStatusOrder($status = null)
    {
        if (isset($status)) {
            $data = $this->getListStatus();
            if (isset($data[$status])) {
                return $data[$status];
            }
            return null;
        }

        return null;
    }

    public function getListPayment()
    {
        $data = array(
            1 => 'Đã thanh toán',
            2 => 'Thanh toán sau (COD)',
        );

        return $data;
    }

    public function getListShipment()
    {
        $data = array(
            1 => 'Chờ vận chuyển',
            2 => 'Đang vận chuyển',
            3 => 'Đang hoàn đơn ',
        );

        return $data;
    }

    public function getCustomerIdByEmail($data)
    {
        $email = $data['email'];
        $sql = "SELECT * FROM `" . DB_PREFIX . "customer` WHERE email = '" . $email . "'";
        $query = $this->db->query($sql);
        if (count($query->row) > 0) {
            return $query->row['customer_id'];
        }

        $customerData['firstname'] = $data['firstname'];
        $customerData['lastname'] = $data['lastname'];
        $customerData['email'] = $data['email'];
        $customerData['telephone'] = $data['telephone'];
        $customerData['customer_source'] = '';
        $customerData['customer_group_id'] = '';
        $customerData['note'] = '';

        $this->load->model('customer/customer');
        $customer_id = $this->model_customer_customer->addCustomer($customerData);

        return $customer_id;
    }

    public function addOrderCommon($data, $customerId = null)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "order SET  firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', fullname = '" . $this->db->escape($data['fullname']) . "', telephone = '" . $data['telephone'] . "', order_status='" . $data['order_status'] . "', order_payment='" . $data['order_payment'] . "', order_transfer = '" . $data['order_transfer'] . "', comment = '" . $data['order_note'] . "',customer_id = '" . $customerId . "', total = '" . $data['order_total'] . "', date_added = NOW(), date_modified = NOW()");
        $order_id = $this->db->getLastId();

        return $order_id;
    }

    public function addOrderCustom($data)
    {
        $order['fullname'] = $customer['fullname'] = $data['fullname'];
        $order['firstname'] = $customer['firstname'] = $data['firstname'];
        $order['lastname'] = $customer['lastname'] = $data['lastname'];
        $order['email'] = $customer['email'] = $data['email'];
        $order['telephone'] = $customer['telephone'] = $data['telephone'];
        $customerId = $this->getCustomerIdByEmail($data);
        $orderId = $this->addOrderCommon($data, $customerId);

        return $orderId;
    }

    public function addOrderProduct($data, $orderId)
    {
        if (isset($data['product-choose'])) {
            $productChooses = $data['product-choose'];
            foreach ($productChooses as $key => $productId) {
                //insert order_product table
                $orderProductTotal = $data['product_quantity'][$key] * $data['product_price'][$key];
                $sql = "
					INSERT INTO " . DB_PREFIX . "order_product SET  order_id = '" . (int)$orderId . "', product_id = '" . (int)$productId . "', quantity = '" . (int)$data['product_quantity'][$key] . "', price = '" . $data['product_price'][$key] . "', total = '" . $orderProductTotal . "'
				";
                $this->db->query($sql);
                //reduce quantity product in product table
                $sqlProduct = "
					UPDATE " . DB_PREFIX . "product SET quantity = quantity - '" . (int)$data['product_quantity'][$key] . "', date_modified = NOW() WHERE product_id = '" . (int)$productId . "'
				";
                $this->db->query($sqlProduct);
            }

            return true;
        }

        return false;
    }

    public function putBackQuantityByProductId($product_id, $quantity)
    {
        $sqlProduct = "
			UPDATE " . DB_PREFIX . "product SET quantity = quantity + '" . (int)$quantity . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'
		";
        $this->db->query($sqlProduct);
    }

    public function deleteOrderById($orderId)
    {
        //put back quantity product
        $sql = "
			SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$orderId . "'
		";
        $data = $this->db->query($sql)->rows;
        foreach ($data as $key => $value) {
            $this->putBackQuantityByProductId($value['product_id'], $value['quantity']);
        }
        //delete order_product where order_id in order_product table
        $sqlOrderProduct = "
			DELETE FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$orderId . "'
		";
        $this->db->query($sqlOrderProduct);
        //delete order in order table
        $sqlOrder = "
			DELETE FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$orderId . "'
		";
        $this->db->query($sqlOrder);

        return true;
    }

    public function getOrderById($orderId)
    {
        $sqlOrder = "
			SELECT * FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$orderId . "'
		";
        $order = $this->db->query($sqlOrder)->row;
        return $order;
    }

    public function getListProductByArrayId($listId = array())
    {
        $listId = implode(',', $listId);

        if (count($listId) == 0) {
            return null;
        }
        $sql = "
			SELECT DISTINCT * FROM " . DB_PREFIX . "product p JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id IN (" . $listId . ") AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'
		";

        $data = $this->db->query($sql)->rows;
    }

    public function getOrderDetailById($orderId)
    {
        $sql = "SELECT o.order_id AS order_id, o.fullname AS fullname, o.firstname AS firstname, o.lastname AS lastname, o.customer_id AS customer_id, o.order_transfer AS order_transfer, o.order_payment AS order_payment, o.order_status AS order_status, o.total AS total,p.product_id AS product_id, p.image AS image, p.quantity AS max_quantity, pd.name AS name, op.price AS price, op.quantity AS quantity, op.total AS total_product FROM " . DB_PREFIX . "order o JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) JOIN " . DB_PREFIX . "product p ON (op.product_id = p.product_id) JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
			WHERE o.order_id = '" . (int)$orderId . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
        // var_dump($sql);die;
        $result = $this->db->query($sql)->rows;
        return $result;
    }

    public function getTopProducts($data = ['start' => 0, 'limit' => 10])
    {
        $sql = "SELECT op.name as name, op.product_id as product_id, p.quantity as quantity, p.image as image, SUM(op.total) as total FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX . "product p ON (op.product_id = p .product_id) GROUP BY op.product_id ORDER BY total DESC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 10;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function updateOrderForStatusNotDraft($data, $order_id)
    {
        $sql = sprintf("UPDATE %s 
                        SET order_status = '%d', 
                            order_payment = '%d', 
                            order_transfer = '%d', 
                            comment = '%s', 
                            date_modified = NOW() 
                        WHERE order_id = '%d'",
            DB_PREFIX . "order",
            (int)$data['order_status'],
            (int)$data['order_payment'],
            (int)$data['order_transfer'],
            $data['order_note'],
            (int)$order_id
        );
        $this->db->query($sql);
    }

    public function updateOrderForStatusDraft($data, $order_id, $customerId)
    {
        $sql = sprintf("UPDATE %s   
                            SET  firstname = '%s', 
                                 lastname = '%s', 
                                 email = '%s', 
                                 fullname = '%s', 
                                 telephone = '%s', 
                                 order_status='%d', 
                                 order_payment='%s', 
                                 order_transfer = '%s', 
                                 comment = '%s',
                                 customer_id = '%d', 
                                 total = '%s', 
                                 date_added = NOW(), 
                                 date_modified = NOW() 
                            WHERE order_id = '%s'",
            DB_PREFIX . "order",
            $this->db->escape($data['firstname']),
            $this->db->escape($data['lastname']),
            $this->db->escape($data['email']),
            $this->db->escape($data['fullname']),
            $data['telephone'],
            $data['order_status'],
            $data['order_payment'],
            $data['order_transfer'],
            $data['order_note'],
            $customerId,
            $data['order_total'],
            $order_id
        );
        $this->db->query($sql);
    }

    public function deleteProductOrder($order_id)
    {
        $sql = sprintf("DELETE FROM %s 
                        WHERE order_id = '%d'",
            DB_PREFIX . "order_product",
            $order_id
        );
        $this->db->query($sql);
    }

    public function editOrderCustom($data, $order_id)
    {
        $order = $this->getOrderById($order_id);

        if (in_array($order['order_status'], [4, 5])) {
            $this->updateOrderForStatusNotDraft($data, $order_id);
        }

        if (in_array($order['order_status'], [2, 3])) {
            if (in_array($data['order_status'], [4, 5])) {
                foreach ($data['product-choose'] as $key => $value) {
                    $product_id = $value;
                    $quantity = $data['product_order_quantity'][$key];
                    $this->putBackQuantityByProductId($product_id, $quantity);
                }
                $this->updateOrderForStatusNotDraft($data, $order_id);
            } else {
                $this->updateOrderForStatusNotDraft($data, $order_id);
            }
        }

        // if order_status = 1: allow edit all fields
        if ($order['order_status'] == 1) {
            $order_products = $this->getOrderProducts($order_id);
            // put back order quantity to stock
            foreach ($order_products as $order_product) {
                $product_id = $order_product['product_id'];
                $quantity = $order_product['quantity'];
                $this->putBackQuantityByProductId($product_id, $quantity);
            }

            // delete all order product records
            $this->deleteProductOrder($order_id);

            // create new order product records
            $this->addOrderProduct($data, $order_id);

            // update order
            $customerId = $this->getCustomerIdByEmail($data);
            $this->updateOrderForStatusDraft($data, $order_id, $customerId);
        }
    }

    public function copy($order_id)
    {
        //duplicate trong bang order
        $sqlCopy = "
    		INSERT INTO " . DB_PREFIX . "order (firstname, fullname, lastname, email, telephone, order_status, order_payment, order_transfer, comment, customer_id, total, date_added, date_modified) SELECT firstname, fullname, lastname, email, telephone, order_status, order_payment, order_transfer, comment, customer_id, total, date_added, date_modified FROM " . DB_PREFIX . "order WHERE order_id = '" . $order_id . "'
    	";
        $this->db->query($sqlCopy);
        $order_id_copy = $this->db->getLastId();
        //duplicate trong order_product
        $sqlOrderProduct = "
			SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'
		";
        $data = $this->db->query($sqlOrderProduct)->rows;

        foreach ($data as $key => $value) {
            //create new record order_product
            $sql = "
				INSERT INTO " . DB_PREFIX . "order_product SET  order_id = '" . (int)$order_id_copy . "', product_id = '" . (int)$value['product_id'] . "', quantity = '" . (int)$value['quantity'] . "', price = '" . $value['price'] . "', total = '" . $value['total'] . "'
			";
            $this->db->query($sql);
            //kiem tra quantity sp trong kho so voi quantity copy
            $quantity_store = $this->db->query("SELECT quantity FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$value['product_id'] . "'")->row['quantity'];
            if (!array_key_exists('quantity', $value)) {
                continue;
            }

            if ($quantity_store < $value['quantity']) {
                //xoa order_id_copy
                $this->db->query("DELETE FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$order_id_copy . "'");
                return false;
            }
            //reduce quantity product in product table
            $sqlProduct = "
				UPDATE " . DB_PREFIX . "product SET quantity = quantity - '" . (int)$value['quantity'] . "', date_modified = NOW() WHERE product_id = '" . (int)$value['product_id'] . "'
			";
            $this->db->query($sqlProduct);
        }
        return true;
    }

}
