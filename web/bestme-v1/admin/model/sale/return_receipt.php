<?php

class ModelSaleReturnReceipt extends Model
{

    /**
     * @param $order_id
     * @param $data
     * @return array
     * Add return receipt
     */
    public function addReturnReceipt($order_id, $data)
    {
        $this->load->model('cash_flow/payment_voucher');

        $total_return = 0;
        $product_ids  = $data['product_id'];
        $store_id  = isset($data['store_id']) ? (int)$data['store_id'] : 0;

        foreach ($product_ids as $key => $product_id) {
            $quantity     = (int)str_replace(',', '', $data['product_return_quantity'][$key]);
            $prices       = (int)str_replace(',', '', $data['return_prices'][$key]);
            $total_return += $quantity * $prices;
        }
        $total_return -= (int)str_replace(',', '', $data['return_fee']);

        $code = $this->autoGenerationCode();
        $return_fee = (int)str_replace(',', '', $data['return_fee']);
        $user_id = $this->user->getId();
        $this->db->query("INSERT INTO " . DB_PREFIX . "return_receipt
                    SET order_id='" . (int)$order_id . "',
                        return_fee='" . $return_fee . "',
                        total='" . (double)$total_return . "',
                        note='" . $this->db->escape($data['note']) . "',
                        return_receipt_code = '" . $code . "',
                        user_create_id = " . (int)$user_id . ",
                        store_id = '" . $store_id . "',
                        date_added = NOW()");
        $return_receipt_id = $this->db->getLastId();

        foreach ($product_ids as $key => $product_id) {
            $quantity = (int)str_replace(',', '', $data['product_return_quantity'][$key]);
            $prices   = (int)str_replace(',', '', $data['return_prices'][$key]);

            if ($quantity > 0) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "return_product
                    SET return_receipt_id='" . (int)$return_receipt_id . "',
                        product_id='" . (int)$product_id . "',
                        product_version_id='" . (int)$data['product_version_id'][$key] . "',
                        price='" . (double)$prices . "',
                        quantity='" . (int)$quantity . "'");
            }
        }

        $this->model_cash_flow_payment_voucher->autoAddFromReturnReceipt($return_receipt_id, $total_return, $store_id);
        $this->updateProductInStore($data, $order_id);
        return [
            'total_return' => $total_return
        ];
    }

    function updateProductInStore($data, $order_id = null)
    {
        // if source order from POS, update product quantity by store_id of order
        // todo : request giang_nh1

        /*$pos_store_id = null;
        if ($order_id) {
            $order = $this->db->query("SELECT `store_id`, `source` FROM `" . DB_PREFIX . "order` WHERE `order_id` = " . (int)$order_id);
            $order = $order->row;
            if (isset($order['source']) && $order['source'] == 'pos') {
                $pos_store_id = isset($order['store_id']) ? (int)$order['store_id'] : null;
            }
        }*/

        // todo : use data['store_id']
        /*$query    = $this->db->query("SELECT product_id, default_store_id
                                      FROM " . DB_PREFIX . "product
                                      WHERE product_id IN (" . implode(',', $data['product_id']) . ")");
        $products = $query->rows;*/

        foreach ($data['product_id'] as $key => $product_id) {
            $store_id = isset($data['store_id']) ? $data['store_id'] : 0;
            $quantity = $data['product_return_quantity'][$key];

            try {
                $this->db->query("UPDATE " . DB_PREFIX . "product_to_store
                              SET quantity = quantity + " . (int)$quantity . "
                              WHERE product_id=" . (int)$product_id . "
                                AND store_id=" . (int)$store_id . "
                                AND product_version_id = " . (int)$data['product_version_id'][$key]);
            } catch (Exception $e) {
                // do something ..
            }
        }
    }

    /**
     * @param $order_id
     * get quantity of product what is returned in the order
     * @return array
     */
    public function getReturnProductByQuantityOrderId($order_id)
    {
        $query = $this->db->query("SELECT *, SUM(rp.quantity) AS quantity
                FROM " . DB_PREFIX . "return_receipt rr 
                LEFT JOIN " . DB_PREFIX . "return_product rp ON rr.return_receipt_id = rp.return_receipt_id
                WHERE rr.order_id = " . (int)$order_id . "
                GROUP BY rp.product_id, rp.product_version_id");

        return $query->rows;
    }

    /**
     * @param $return_receipt_id
     * @return mixed
     * get return receipt by id
     */
    public function getReceiptById($return_receipt_id)
    {
        $query = $this->db->query("SELECT *
                FROM " . DB_PREFIX . "return_receipt
                WHERE  `return_receipt_id` = " . (int)$return_receipt_id);

        return $query->row;
    }

    /**
     * @param $order_id
     * @param $return_receipt_id
     * @return mixed
     * get product detail in return receipt
     */
    public function getProductDetailInReceipt($order_id, $return_receipt_id)
    {
        $sql = "SELECT rp.quantity AS return_quantity, rp.product_id,  rp.price AS return_price, pd.name, pv.version, p.image, op.quantity
                FROM " . DB_PREFIX . "return_product rp
                LEFT JOIN " . DB_PREFIX . "order_product op ON op.product_id = rp.product_id AND op.product_version_id = rp.product_version_id
                LEFT JOIN " . DB_PREFIX . "product p ON p.product_id = rp.product_id
                LEFT JOIN " . DB_PREFIX . "product_description pd ON pd.product_id = rp.product_id
                LEFT JOIN " . DB_PREFIX . "product_version pv ON rp.product_version_id = pv.product_version_id
                WHERE `order_id` = " . (int)$order_id . "
                    AND `return_receipt_id` = " . (int)$return_receipt_id;

        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * @return string
     * generate return receipt code
     */
    public function autoGenerationCode()
    {
        // max
        $max = $this->getMaxReceiptId();

        // make code
        $prefix = 'PTH';
        $code   = $prefix . str_pad($max + 1, 6, "0", STR_PAD_LEFT);

        return $code;
    }

    /**
     * @return mixed
     * get last id in table
     */
    private function getMaxReceiptId()
    {
        $sql = "SELECT MAX(`return_receipt_id`) AS max_id FROM `" . DB_PREFIX . "return_receipt`";

        $query = $this->db->query($sql);

        return $query->row['max_id'];
    }

    public function getReturnReceipts($data)
    {
        $sql = "SELECT rr.*, o.order_code FROM " . DB_PREFIX . "return_receipt rr LEFT JOIN " . DB_PREFIX . "order o ON rr.order_id = o.order_id WHERE 1";

        if (!empty($data['filter_name'])) {
            $sql .= " AND(rr.return_receipt_code LIKE '%" . $this->db->escape($data['filter_name']) . "%' OR o.order_code LIKE '%" . $this->db->escape($data['filter_name']) . "%')";
        }

        if (isset($data['start_date']) && $data['start_date'] !== '') {
            $sql .= " AND rr.date_added BETWEEN '" . $data['start_date'] . "' AND '" . $data['end_date'] . "'";
        }

        if (isset($data['current_user_id']) && $data['current_user_id'] !== '') {
            $sql .= " AND rr.user_create_id = " . $data['current_user_id'];
        }

        $sql .= " ORDER BY rr.date_added DESC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalReturnReceipts($filter_data)
    {
        $sql = "SELECT COUNT(DISTINCT rr.return_receipt_id) AS total FROM " . DB_PREFIX . "return_receipt rr LEFT JOIN " . DB_PREFIX . "order o ON rr.order_id = o.order_id WHERE 1";

        if (isset($filter_data['filter_name']) && $filter_data['filter_name'] !== '') {
            $sql .= " AND(rr.return_receipt_code LIKE '%" . $this->db->escape($filter_data['filter_name']) . "%' OR o.order_code LIKE '%" . $this->db->escape($filter_data['filter_name']) . "%')";
        }

        if (isset($filter_data['start_date']) && $filter_data['start_date'] !== '') {
            $sql .= " AND rr.date_added BETWEEN '" . $filter_data['start_date'] . "' AND '" . $filter_data['end_date'] . "'";
        }

        if (isset($filter_data['current_user_id']) && $filter_data['current_user_id'] !== '') {
            $sql .= " AND rr.user_create_id = " . $filter_data['current_user_id'];
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function totalOrderReturnByDate($start, $end)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT COUNT(order_id) as total FROM `{$DB_PREFIX}return_receipt` 
                  WHERE `date_added` BETWEEN '{$start}' AND '{$end}'";
        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function totalOrderReturnByDateGroupByDate($start, $end, $store_id = '')
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT DATE(date_added) AS date_added, 
                       COUNT(order_id) as order_return, 
                       SUM(total) as total_return 
                FROM `{$DB_PREFIX}return_receipt` 
                WHERE `date_added` BETWEEN '{$start}' AND '{$end}'";
        if ($store_id !== '') {
            $sql .= " AND `store_id` = " . (int)$store_id;
        }
        $sql .= " GROUP BY DATE(date_added)";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function totalOrderReturnAndTotalReturnByDate($start, $end, $store_id = '')
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT COUNT(order_id) as total_order_return, 
                       SUM(total) as total_return FROM `{$DB_PREFIX}return_receipt` 
                  WHERE `date_added` BETWEEN '{$start}' AND '{$end}'";

        if ($store_id !== '') {
            $sql .= " AND `store_id` = " . (int)$store_id;
        }

        $query = $this->db->query($sql);

        return $query->row;
    }
}
