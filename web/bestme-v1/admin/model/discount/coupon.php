<?php

class ModelDiscountCoupon extends Model
{
    const COUPON_TYPE_VALUE = '1';
    const COUPON_TYPE_PERCENT = '2';
    
    const LIMIT_GET_ITEM_IN_DATA_EXCEL = 50;

    // voucher
    const VOUCHER_APPLY_FOR_ALL_ORDER = 1;
    const VOUCHER_APPLY_FOR_VALUE_ORDER = 2;
    const VOUCHER_APPLY_FOR_PRODUCT = 3;

    const VOUCHER_APPLICABLE_TYPE_QUANTITY_PRODUCT = 1;
    const VOUCHER_APPLICABLE_TYPE_TOTAL_ORDER = 2;

    const VOUCHER_PRODUCT_ONE_ON_ORDER = 1;
    const VOUCHER_PRODUCT_QUANTITY_ON_ORDER = 2;

    const VOUCHER_SALE_CHANEL_ALL = 0;
    const VOUCHER_SALE_CHANEL_WEB = 1;
    const VOUCHER_SALE_CHANEL_POS = 2;

    const COUPON_APPLY_FOR = [
        self::VOUCHER_APPLY_FOR_ALL_ORDER => 'Tất cả đơn hàng',
        self::VOUCHER_APPLY_FOR_VALUE_ORDER => 'Giá trị đơn hàng',
        self::VOUCHER_APPLY_FOR_PRODUCT => 'Sản phẩm'
    ];

    const VALUE_COUPON_APPLY_FOR = [
        'Tất cả đơn hàng' => self::VOUCHER_APPLY_FOR_ALL_ORDER,
        'Trị giá đơn hàng từ' => self::VOUCHER_APPLY_FOR_VALUE_ORDER,
        'Sản phẩm' => self::VOUCHER_APPLY_FOR_PRODUCT
    ];

    const VOUCHER_PRODUCT_HOW_TO_APPLY = [
        'Một lần trên một đơn hàng' => self::VOUCHER_PRODUCT_ONE_ON_ORDER,
        'Cho từng mặt hàng trong giỏ hàng' => self::VOUCHER_PRODUCT_QUANTITY_ON_ORDER,
    ];

    const VOUCHER_SALE_CHANEL = [
        'Tất cả kênh bán hàng' => self::VOUCHER_SALE_CHANEL_ALL,
        'web' => self::VOUCHER_SALE_CHANEL_WEB,
        'POS' => self::VOUCHER_SALE_CHANEL_POS,
    ];

    const VOUCHER_TYPE_LIST = [
        'Phần trăm' => self::COUPON_TYPE_PERCENT,
        'Số tiền' => self::COUPON_TYPE_VALUE
    ];

    public function getListCoupons($data = [])
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM `{$DB_PREFIX}coupon` WHERE 1";

        if (isset($data['filter_name']) && !empty($data['filter_name'])) {
            $sql .= " AND(name LIKE '%" . $this->db->escape($data['filter_name']) . "%' OR code LIKE '%" . $this->db->escape($data['filter_name']) . "%')";
        }

        $sql .= " ORDER BY coupon_id DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getTotalCoupons($data = [])
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT COUNT(*) AS `total` FROM `{$DB_PREFIX}coupon` WHERE 1";

        if (isset($data['filter_name']) && !empty($data['filter_name'])) {
            $sql .= " AND(name LIKE '%" . $this->db->escape($data['filter_name']) . "%' OR code LIKE '%" . $this->db->escape($data['filter_name']) . "%')";
        }

        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    public function getTotalCouponsIncludeAllStatuses()
    {
        $sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "coupon`";

        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    public function getCouponInfo($coupon_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM `{$DB_PREFIX}coupon` WHERE `coupon_id` = '" . $this->db->escape($coupon_id)."' ";

        $query = $this->db->query($sql);
        return $query->row;
    }

    public function getCouponInfoByCode($coupon_code)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM `{$DB_PREFIX}coupon` 
                    WHERE `code` = '" . $this->db->escape($coupon_code)."'";

        $query = $this->db->query($sql);
        return $query->row;
    }

    public function getCouponProduct($coupon_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT cp.*, pd.name, pv.version  FROM `{$DB_PREFIX}coupon_product` AS cp 
                    LEFT JOIN `{$DB_PREFIX}product_description` as pd ON cp.product_id = pd.product_id 
                    LEFT JOIN `{$DB_PREFIX}product_version` as pv ON cp.product_id = pv.product_id AND cp.product_version_id = pv.product_version_id 
                    WHERE `coupon_id` = '" . $this->db->escape($coupon_id)."' ";

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function validateCodeExist($code, $coupon_id)
    {
        $sql = "SELECT COUNT(DISTINCT coupon_id) as total FROM `" . DB_PREFIX . "coupon` WHERE `code` = '" . $this->db->escape($code) . "'";

        if (!empty($coupon_id)) {
            $sql .= " AND `coupon_id` != " . (int)$coupon_id;
        }

        $query = $this->db->query($sql);
        if ($query->row['total'] != 0) {
            return true;
        }
        return false;
    }

    public function addCoupon($data = [])
    {
        $name = isset($data['name']) ? $data['name'] : '';
        $code = isset($data['code']) ? $data['code'] : '';
        $type = isset($data['type']) ? $data['type'] : 1;
        $discount = isset($data['discount']) ? (float)str_replace(',', '', $data['discount']) : 0;
        $logged = isset($data['logged']) ? $data['logged'] : 1;
        $shipping = isset($data['shipping']) ? $data['shipping'] : 1;
        $total = isset($data['total']) ? $data['total'] : 0;
        $uses_total = isset($data['uses_total']) ? $data['uses_total'] : 0;
        $uses_customer = isset($data['uses_customer']) ? $data['uses_customer'] : 0;
        $status = isset($data['status']) ? $data['status'] : 1;

        // new data
        $limit_times = isset($data['no_limit_times']) && isset($data['limit_times']) ? (float)str_replace(',', '', $data['limit_times']) : -1;
        $init_limit_times = $limit_times;
        $max_type_value = isset($data['max_type_value']) ? $data['max_type_value'] : null;
        $allow_with_discount = isset($data['allow_with_discount']) ? $data['allow_with_discount'] : 0;

        $apply_for = isset($data['apply_for']) ? $data['apply_for'] : 1;
        $applicable_type = isset($data['applicable_type']) ? $data['applicable_type'] : 1;
        $applicable_type_value = isset($data['applicable_type_value']) ?
            (float)str_replace(',', '', $data['applicable_type_value']) : null;
        $sale_channel = isset($data['sale_channel']) ? $data['sale_channel'] : 0;
        $order_value_from = isset($data['order_value_from']) ? (float)str_replace(',', '', $data['order_value_from']) : 0;
        $how_to_apply = isset($data['how_to_apply']) ? $data['how_to_apply'] : 1;
        $subjects_of_application = isset($data['subjects_of_application']) ? $data['subjects_of_application'] : null;
        $date_start = isset($data['date_start']) ? $data['date_start'] : null;
        $date_end = isset($data['date_end']) ? $data['date_end'] : null;
        $never_expired = isset($data['never_expired']) ? $data['never_expired'] : null;

        $product_coupon = isset($data['product_coupon']) ? $data['product_coupon'] : [];

        $sql = "INSERT INTO `" . DB_PREFIX . "coupon` 
                    SET `name` = '" . $this->db->escape($name) . "', 
                        `code` = '" . $this->db->escape($code) . "', 
                        `type` = '" . $this->db->escape($type) . "', 
                        `discount` = '" . $this->db->escape($discount) . "', 
                        `logged` = '" . $this->db->escape($logged) . "', 
                        `shipping` = '" . $this->db->escape($shipping) . "', 
                        `total` = '" . $this->db->escape($total) . "', 
                        `uses_total` = '" . $this->db->escape($uses_total) . "', 
                        `uses_customer` = '" . $this->db->escape($uses_customer) . "', 
                        `status` = '" . $this->db->escape($status) . "', 
                        `date_start` = '" . $this->db->escape($date_start) . "',
                        `date_end` = '" . $this->db->escape($date_end) . "',
                        `limit_times` = '" . $this->db->escape($limit_times) . "',
                        `init_limit_times` = '" . $this->db->escape($init_limit_times) . "',
                        `max_type_value` = '" . $this->db->escape($max_type_value) . "',
                        `allow_with_discount` = '" . $this->db->escape($allow_with_discount) . "',
                        `never_expired` = '" . $this->db->escape($never_expired) . "',
                        `apply_for` = '" . $this->db->escape($apply_for) . "',
                        `applicable_type` = '" . $this->db->escape($applicable_type) . "',
                        `applicable_type_value` = '" . $this->db->escape($applicable_type_value) . "',
                        `sale_channel` = '" . $this->db->escape($sale_channel) . "',
                        `order_value_from` = '" . $this->db->escape($order_value_from) . "',
                        `how_to_apply` = '" . $this->db->escape($how_to_apply) . "',
                        `subjects_of_application` = '" . $this->db->escape($subjects_of_application) . "',
                        `date_added` = NOW()
                        ";

        $this->db->query($sql);
        $coupon_id = $this->db->getLastId();

        if (!empty($product_coupon)) {
            $this->saveProductCoupon($product_coupon, $coupon_id);
        }

        return $coupon_id;
    }

    public function editCoupon($coupon_id, $data)
    {
        $coupon_info = $this->getCouponInfo($coupon_id);

        if (empty($coupon_info)) {
            return $coupon_id;
        }

        $name = isset($data['name']) ? $data['name'] : '';
        $code = isset($data['code']) ? $data['code'] : '';
        $type = isset($data['type']) ? $data['type'] : 1;
        $discount = isset($data['discount']) ? (float)str_replace(',', '', $data['discount']) : 0;
        $logged = isset($data['logged']) ? $data['logged'] : $coupon_info['logged'];
        $shipping = isset($data['shipping']) ? $data['shipping'] : $coupon_info['shipping'];
        $total = isset($data['total']) ? $data['total'] : $coupon_info['total'];
        $uses_total = isset($data['uses_total']) ? $data['uses_total'] : $coupon_info['uses_total'];
        $uses_customer = isset($data['uses_customer']) ? $data['uses_customer'] : $coupon_info['uses_customer'];
        $status = isset($data['status']) ? $data['status'] : $coupon_info['status'];

        // new data
        $limit_times = isset($data['no_limit_times']) && isset($data['limit_times']) ? (float)str_replace(',', '', $data['limit_times']) : $coupon_info['limit_times'];
        $init_limit_times = $limit_times;
        $max_type_value = isset($data['max_type_value']) ? $data['max_type_value'] : $coupon_info['max_type_value'];
        $allow_with_discount = isset($data['allow_with_discount']) ? $data['allow_with_discount'] : null;

        $apply_for = isset($data['apply_for']) ? $data['apply_for'] : $coupon_info['apply_for'];
        $applicable_type = isset($data['applicable_type']) ? $data['applicable_type'] : $coupon_info['applicable_type'];
        $applicable_type_value = isset($data['applicable_type_value']) ?
            (float)str_replace(',', '', $data['applicable_type_value']) :
            (float)str_replace(',', '', $coupon_info['applicable_type_value']);
        $sale_channel = isset($data['sale_channel']) ? $data['sale_channel'] : $coupon_info['sale_channel'];
        $order_value_from = isset($data['order_value_from']) ? (float)str_replace(',', '', $data['order_value_from']) :
            (float)str_replace(',', '', $coupon_info['order_value_from']);
        $how_to_apply = isset($data['how_to_apply']) ? $data['how_to_apply'] : $coupon_info['how_to_apply'];
        $subjects_of_application = isset($data['subjects_of_application']) ? $data['subjects_of_application'] : null;

        $date_start = isset($data['date_start']) ? $data['date_start'] : (isset($coupon_info['date_start']) ? $coupon_info['date_start'] : '');
        $date_end = isset($data['date_end']) ? $data['date_end'] : (isset($coupon_info['date_end']) ? $coupon_info['date_end'] : '');
        $never_expired = isset($data['never_expired']) ? $data['never_expired'] : null;

        $product_coupon = isset($data['product_coupon']) ? $data['product_coupon'] : [];

        $sql = "UPDATE " . DB_PREFIX . "coupon 
                          SET name = '" . $this->db->escape($name) . "', 
                              code = '" . $this->db->escape($code) . "', 
                              type = '" . $this->db->escape($type) . "', 
                              discount = '" . $this->db->escape($discount) . "', 
                              logged = '" . $this->db->escape($logged) . "', 
                              shipping = '" . $this->db->escape($shipping) . "', 
                              total = '" . $this->db->escape($total) . "', 
                              uses_total = '" . $this->db->escape($uses_total) . "', 
                              uses_customer = '" . $this->db->escape($uses_customer) . "', 
                              status = '" . $this->db->escape($status) . "', 
                              date_start = '" . $this->db->escape($date_start) . "', 
                              date_end = '" . $this->db->escape($date_end) . "', 
                              limit_times = '" . $this->db->escape($limit_times) . "', 
                              init_limit_times = '" . $this->db->escape($init_limit_times) . "', 
                              max_type_value = '" . $this->db->escape($max_type_value) . "', 
                              allow_with_discount = '" . $this->db->escape($allow_with_discount) . "', 
                              apply_for = '" . $this->db->escape($apply_for) . "', 
                              applicable_type = '" . $this->db->escape($applicable_type) . "', 
                              applicable_type_value = '" . $this->db->escape($applicable_type_value) . "', 
                              sale_channel = '" . $this->db->escape($sale_channel) . "', 
                              order_value_from = '" . $this->db->escape($order_value_from) . "', 
                              how_to_apply = '" . $this->db->escape($how_to_apply) . "', 
                              subjects_of_application = '" . $this->db->escape($subjects_of_application) . "', 
                              never_expired = '" . $this->db->escape($never_expired) . "', 
                              date_added = '" . $this->db->escape($coupon_info['date_added']) . "' 
                          WHERE coupon_id = '" . (int)$coupon_id . "' ";

        $this->db->query($sql);

        if (!empty($product_coupon)) {
            $this->saveProductCoupon($product_coupon, $coupon_id);
        }

        return $coupon_id;
    }

    public function saveProductCoupon($product_coupon, $coupon_id)
    {
        if (!empty($product_coupon)) {
            // delete first
            $this->db->query("DELETE FROM `" . DB_PREFIX . "coupon_product` WHERE `coupon_id` = {$coupon_id}");

            $sqlArr = [];
            foreach ($product_coupon as $item) {
                if (strpos($item, '-')) {
                    $pvid = explode('-', $item);
                    $product_id = isset($pvid[0]) ? $pvid[0] : null;
                    $product_version_id = isset($pvid[1]) ? $this->db->escape($pvid[1]) : 0;
                } else {
                    $product_id = $item;
                    $product_version_id = 0;
                }

                if (!empty($product_id)) {
                    $sqlArr[] = " ({$coupon_id}, {$product_id}, {$product_version_id})";
                }
            }

            $sqlArr = implode(',', $sqlArr);
            $sql = "INSERT INTO `" . DB_PREFIX . "coupon_product` (coupon_id, product_id, product_version_id) VALUES {$sqlArr}";
            $this->db->query($sql);
        }
    }

    public function getProductByStore($store_id, $data = array())
    {
        $sql = "SELECT p.status as p_status, p.sku as psku, p.sale_on_out_of_stock as p_sale_on_out_of_stock, p.product_id as pid, p.multi_versions, p.price as o_price, p.compare_price as o_compare_price, p.weight as weight, pts.cost_price, pts.quantity as s_quantity, dp.name,p.image, pv.* 
                from " . DB_PREFIX . "product p 
                LEFT JOIN " . DB_PREFIX . "product_description as dp ON (p.product_id = dp.product_id) 
                LEFT JOIN " . DB_PREFIX . "product_version as pv ON CASE 
                                                                            WHEN p.multi_versions = 1 THEN p.product_id 
                                                                            ELSE -1
                                                                            END = pv.product_id
                INNER JOIN " . DB_PREFIX . "product_to_store as pts ON (p.product_id = pts.product_id AND CASE WHEN p.multi_versions = 1 THEN pv.product_version_id ELSE 0 END = pts.product_version_id AND pts.store_id = " . (int)$store_id .") 
                WHERE 1=1 and dp.language_id = " . (int)$this->config->get('config_language_id');

        if (isset($data['current_user_id']) && $data['current_user_id'] !== '') {
            $sql .= " AND p.user_create_id = " . $data['current_user_id'];
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND dp.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " AND ( p.`deleted` IS NULL)";
        $sql .= " AND (pv.`deleted` IS NULL)";
        $sql .= " ORDER BY dp.name ASC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);
        $result = array();
        foreach ($query->rows as $k => $item) {
            $result[$k] = array(
                'product_id' => $item['pid'],
                'name' => mb_strlen($item['name']) > 90 ? mb_substr($item['name'], 0, 90) . "..." : $item['name'],
                'product_version_id' => $item['product_version_id'],
                'product_name' => $item['name'],
                'weight' => $item['weight'],
                'image' => $item['image'],
                'version' => $item['version'],
                'sale_on_out_of_stock' => $item['p_sale_on_out_of_stock'],
                'multi_versions' => $item['multi_versions']
            );

            if ($item['product_version_id'] != null) {
                $result[$k]['id'] = $item['pid'] . '-' . $item['product_version_id'];
                $result[$k]['quantity'] = $item['s_quantity'];
                $result[$k]['price'] = $item['price'] != 0 ? $item['price'] : $item['compare_price'];
                $result[$k]['status'] = $item['status'];
                $result[$k]['p_status'] = $item['p_status'];
                $result[$k]['sku'] = $item['sku'];
                $result[$k]['cost_price'] = $item['cost_price'];
            } else {
                $result[$k]['id'] = $item['pid'];
                $result[$k]['quantity'] = $item['s_quantity'];
                $result[$k]['price'] = $item['o_price'] != 0 ? $item['o_price'] : $item['o_compare_price'];
                $result[$k]['status'] = $item['p_status'];
                $result[$k]['p_status'] = $item['p_status'];
                $result[$k]['sku'] = $item['psku'];
                $result[$k]['cost_price'] = $item['cost_price'];
            }
        }

        return $result;
    }

    public function countProductByStore($store_id, $data = array())
    {
        $language_id = $this->config->get('config_language_id');
        $sql = "SELECT p.product_id,pd.name from " . DB_PREFIX . "product p 
                    LEFT JOIN " . DB_PREFIX . "product_description as pd ON (p.product_id = pd.product_id) 
                    LEFT JOIN " . DB_PREFIX . "product_version as pv ON (p.product_id = pv.product_id) 
                    INNER JOIN " . DB_PREFIX . "product_to_store as pts ON (p.product_id = pts.product_id AND CASE WHEN p.multi_versions = 1 THEN pv.product_version_id ELSE 0 END = pts.product_version_id AND pts.store_id = " . (int)$store_id .") WHERE 1=1";

        if (isset($data['current_user_id']) && $data['current_user_id'] !== '') {
            $sql .= " AND p.user_create_id = " . $data['current_user_id'];
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " AND (p.`deleted` IS NULL OR p.`deleted` = 2)";
        $sql .= " AND (pv.`deleted` IS NULL OR pv.`deleted` = 2)";
        $sql .= " AND pd.language_id = " . $language_id . "";
        $sql .= " ORDER BY p.product_id ASC";
        $query = $this->db->query($sql);

        return count($query->rows);
    }

    public function processImportExcel($dataCoupons, $offset = 0, $limit = 50)
    {
        $length = self::LIMIT_GET_ITEM_IN_DATA_EXCEL;

        try {
            if ($limit > count($dataCoupons)) {
                $limit = count($dataCoupons);
            }

            for ($i = $offset; $i < $limit; $i++) {
                if (empty($dataCoupons[$i]['code'])) {
                    continue;
                }

                $this->importCoupon($dataCoupons[$i]);
            }


            if ($limit == count($dataCoupons)) {
                return true;
            } else {
                $offset += $length;
                $limit += $length;
                $this->processImportExcel($dataCoupons, $offset, $limit);
            }

            return true;
        } catch (\Exception $exception) {
            // do something
            $this->log->write('[processImportExcel] error : ' . $exception->getMessage());
            return false;
        }
    }

    public function importCoupon($item)
    {
        // buildData
        /*
         * code	allow_with_discount	limit_times	date_start	date_end
         * type	discount	apply_for	applicable_type	applicable_type_value	order_value_from
         * id	how_to_apply	subjects_of_application	sale_channel
         * */

        $item['name'] = isset($item['name']) ? $item['name'] : '';
        $item['code'] = isset($item['code']) ? $item['code'] : '';
        $item['type'] = isset($item['type']) ? $item['type'] : 1;
        $item['discount'] = isset($item['discount']) ? (float)str_replace(',', '', $item['discount']) : 0;
        $item['logged'] = 1;
        $item['shipping'] = 1;
        $item['total'] = 0;
        $item['uses_total'] = 0;
        $item['uses_customer'] = 0;
        $item['status'] = 1;

        // new data
        $item['limit_times'] = isset($item['limit_times']) ? $item['limit_times'] : -1;

        if ($item['limit_times'] > 0) {
            $item['no_limit_times'] = 1;
        }

        $item['max_type_value'] = isset($item['max_type_value']) ? $item['max_type_value'] : 0;
        $item['allow_with_discount'] = isset($item['allow_with_discount']) ? $item['allow_with_discount'] : 0;

        $item['date_start'] = isset($item['date_start']) ? $item['date_start'] : null;
        $item['date_end'] = isset($item['date_end']) ? $item['date_end'] : null;
        $item['never_expired'] = isset($item['never_expired']) ? $item['never_expired'] : null;
        $item['subjects_of_application'] = isset($item['subjects_of_application']) ? $item['subjects_of_application'] : null;
        $item['sale_channel'] = isset($item['sale_channel']) ? $item['sale_channel'] : 0;

        $apply_for = $item['apply_for'];

        if ($apply_for == self::VOUCHER_APPLY_FOR_VALUE_ORDER) {
            $order_value_from = isset($item['order_value_from']) ? (float)str_replace(',', '', $item['order_value_from']) : 0;
            $applicable_type = 0;
            $applicable_type_value = 0;
            $how_to_apply = 0;
        } elseif ($apply_for == self::VOUCHER_APPLY_FOR_PRODUCT) {
            $order_value_from = 0;
            $applicable_type = isset($item['applicable_type']) ? $item['applicable_type'] : 1;
            $applicable_type_value = isset($item['applicable_type_value']) ? $item['applicable_type_value'] : 0;
            $how_to_apply = isset($item['how_to_apply']) ? $item['how_to_apply'] : 1;
        } else {
            $order_value_from = 0;
            $applicable_type = 0;
            $applicable_type_value = 0;
            $how_to_apply = 0;
        }
        $item['order_value_from'] = $order_value_from;
        $item['applicable_type'] = $applicable_type;
        $item['applicable_type_value'] = $applicable_type_value;
        $item['how_to_apply'] = $how_to_apply;

        $item['product_coupon'] = isset($data['id']) ? $data['id'] : [];

        // find
        $coupon = $this->getCouponInfoByCode($item['code']);
        if ($coupon) {
            $this->editCoupon($coupon['coupon_id'], $item);
        } else {
            $this->addCoupon($item);
        }
    }
}
