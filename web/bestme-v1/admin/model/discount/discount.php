<?php

class ModelDiscountDiscount extends Model
{
    use RabbitMq_Trait;

    const INPUT_DATETIME_FORMAT = 'Y-m-d H:i:s';
    const DISCOUNT_DATETIME_FORMAT = 'Y-m-d H:i';

    public static $SOURCE_WEBSITE = 'website';
    public static $SOURCE_ADMIN = 'admin';
    public static $SOURCE_POS = 'pos';
    public static $SOURCE_CHATBOT = 'chatbot';
    public static $SOURCE_ALL = 'ALL';

    public function addDiscount($data)
    {
        $start_at = !empty($data['start_at']) ? date('Y-m-d H:i:s', strtotime($data['start_at'])) : '';
        $end_at = !empty($data['end_at']) ? date('Y-m-d H:i:s', strtotime($data['end_at'])) : '';

        $sql = "INSERT INTO `" . DB_PREFIX . "discount` SET `name` = '{$this->db->escape($data['name'])}', 
                                                            `description` = '{$this->db->escape($data['description'])}', 
                                                            `date_started` = '{$this->db->escape($start_at)}', 
                                                            `date_ended` = '{$this->db->escape($end_at)}', 
                                                            `date_modify` = NOW()
                                                            ";

        $this->db->query($sql);
        $discount_id = $this->db->getLastId();

        // add discount product
        $discount_products = $this->buildDiscountProduct($data, $discount_id);
        if (!empty($discount_products)) {
            $this->addDiscountProduct($discount_products, $discount_id);
        }

        $this->deleteProductCaches();
        return $discount_id;
    }

    public function createAddOnDeal($data)
    {
        $start_at = !empty($data['start_at']) ? date('Y-m-d H:i:s', strtotime($data['start_at'])) : '';
        $end_at = !empty($data['end_at']) ? date('Y-m-d H:i:s', strtotime($data['end_at'])) : '';

        $sql = "INSERT INTO `" . DB_PREFIX . "add_on_deal` SET `name` = '{$this->db->escape($data['name'])}', 
                                                            `limit_quantity_product` = '{$this->db->escape($data['limit_quantity_product'])}', 
                                                            `date_started` = '{$this->db->escape($start_at)}', 
                                                            `date_ended` = '{$this->db->escape($end_at)}',
                                                            `date_modify` = NOW()
                                                            ";

        $this->db->query($sql);
        $add_on_deal_id = $this->db->getLastId();

        // add discount product
        $add_on_deal_products = $this->buildAddOnDealProduct($data, $add_on_deal_id);
        if (!empty($add_on_deal_products)) {
            $this->addOnDealMainProduct($add_on_deal_products['add_on_deal_main_products'], $add_on_deal_id);
            $this->addOnDealAttachedProduct($add_on_deal_products['add_on_deal_attached_products'], $add_on_deal_id);
        }

        $this->deleteProductCaches();
        return $add_on_deal_id;
    }

    public function editDiscount($discount_id, $data)
    {
        if (empty($discount_id)) {
            return $discount_id;
        }

        $start_at = !empty($data['start_at']) ? date('Y-m-d H:i:s', strtotime($data['start_at'])) : '';
        $end_at = !empty($data['end_at']) ? date('Y-m-d H:i:s', strtotime($data['end_at'])) : '';

        $sql = "UPDATE `" . DB_PREFIX . "discount` SET `name` = '{$this->db->escape($data['name'])}', 
                                                        `description` = '{$this->db->escape($data['description'])}', 
                                                        `date_started` = '{$this->db->escape($start_at)}', 
                                                        `date_ended` = '{$this->db->escape($end_at)}', 
                                                        `date_modify` = NOW() 
                                                     WHERE `discount_id` = " . (int)$discount_id . "";

        $this->db->query($sql);

        // add discount product
        $discount_products = $this->buildDiscountProduct($data, $discount_id);
        if (!empty($discount_products)) {
            $this->addDiscountProduct($discount_products, $discount_id);
        }

        $this->deleteProductCaches();

        return $discount_id;
    }

    public function editAddOnDeal($add_on_deal_id, $data)
    {
        if (empty($add_on_deal_id)) {
            return $add_on_deal_id;
        }

        $start_at = !empty($data['start_at']) ? date('Y-m-d H:i:s', strtotime($data['start_at'])) : '';
        $end_at = !empty($data['end_at']) ? date('Y-m-d H:i:s', strtotime($data['end_at'])) : '';

        $sql = "UPDATE `" . DB_PREFIX . "add_on_deal` SET `name` = '{$this->db->escape($data['name'])}', 
                                                        `limit_quantity_product` = '{$this->db->escape($data['limit_quantity_product'])}', 
                                                        `date_started` = '{$this->db->escape($start_at)}', 
                                                        `date_ended` = '{$this->db->escape($end_at)}', 
                                                        `date_modify` = NOW() 
                                                     WHERE `add_on_deal_id` = " . (int)$add_on_deal_id . "";

        $this->db->query($sql);

        // add discount product
        $add_on_deal_products = $this->buildAddOnDealProduct($data, $add_on_deal_id);
        if (!empty($add_on_deal_products)) {
            $this->addOnDealMainProduct($add_on_deal_products['add_on_deal_main_products'], $add_on_deal_id);
            $this->addOnDealAttachedProduct($add_on_deal_products['add_on_deal_attached_products'], $add_on_deal_id);
        }

        $this->deleteProductCaches();

        return $add_on_deal_id;
    }

    public function activeDiscount($discount_id, $start, $end)
    {
        // validate discount: exists, source
        if ($this->checkDiscountSourceIsValid($discount_id) == false) {
            return false;
        }

        $this->load->model('discount/discount_status');
        $this->updateDiscountStatus($discount_id, $status = ModelDiscountDiscountStatus::STATUS_ACTIVATED,'', $start, $end);
    }

    public function pauseDiscount($discount_id, $pause_reason)
    {
        // validate discount: exists, source
        if ($this->checkDiscountSourceIsValid($discount_id) == false) {
            return false;
        }

        $this->load->model('discount/discount_status');
        $this->updateDiscountStatus($discount_id, $status = ModelDiscountDiscountStatus::STATUS_PAUSED, $pause_reason);
    }

    public function deleteDiscount($discount_id, $reason)
    {
        $db_prefix = DB_PREFIX;
        $sql = "UPDATE {$db_prefix}discount SET `deleted` = '1'";

        $sql .= " WHERE discount_id = '" . (int)$discount_id . "'";
        $this->db->query($sql);
        $this->insertDiscountHistory($discount_id, $reason, 'delete');
        $this->deleteProductCaches();
    }

    public function pauseDiscountsActivated($discount_id, $reason)
    {
        $db_prefix = DB_PREFIX;
        $sql = "UPDATE {$db_prefix}discount SET `status` = '0'";

        $sql .= " WHERE discount_id = '" . (int)$discount_id . "'";
        $this->db->query($sql);
        $this->insertDiscountHistory($discount_id, $reason, 'pause');
        $this->deleteProductCaches();
    }

    public function continueDiscountsActivated($discount_id, $reason)
    {
        $db_prefix = DB_PREFIX;
        $sql = "UPDATE {$db_prefix}discount SET `status` = '1'";

        $sql .= " WHERE discount_id = '" . (int)$discount_id . "'";
        $this->db->query($sql);
        $this->insertDiscountHistory($discount_id, $reason, 'continue');
        $this->deleteProductCaches();
    }

    public function deleteAddOnDeal($add_on_deal_id, $reason)
    {
        $db_prefix = DB_PREFIX;
        $sql = "UPDATE {$db_prefix}add_on_deal SET `deleted` = '1'";

        $sql .= " WHERE add_on_deal_id = '" . (int)$add_on_deal_id . "'";
        $this->db->query($sql);
        $this->insertAddOnDealHistory($add_on_deal_id, $reason, 'delete');
        $this->deleteProductCaches();
    }

    public function pauseAddOnDealActivated($add_on_deal_id, $reason)
    {
        $db_prefix = DB_PREFIX;
        $sql = "UPDATE {$db_prefix}add_on_deal SET `status` = '0'";

        $sql .= " WHERE add_on_deal_id = '" . (int)$add_on_deal_id . "'";
        $this->db->query($sql);
        $this->insertAddOnDealHistory($add_on_deal_id, $reason, 'pause');
        $this->deleteProductCaches();
    }

    public function continueAddOnDealActivated($add_on_deal_id, $reason)
    {
        $db_prefix = DB_PREFIX;
        $sql = "UPDATE {$db_prefix}add_on_deal SET `status` = '1'";

        $sql .= " WHERE add_on_deal_id = '" . (int)$add_on_deal_id . "'";
        $this->db->query($sql);
        $this->insertAddOnDealHistory($add_on_deal_id, $reason, 'continue');
        $this->deleteProductCaches();
    }

    public function insertDiscountHistory($discount_id, $reason, $action)
    {
        $discount = $this->getDiscountById($discount_id);
        $user_id = (int)$this->user->getId();
        $sql = "INSERT INTO `" . DB_PREFIX . "discount_history` SET `discount_id` = '{$discount['discount_id']}', 
                                                `user_id` = '{$user_id}', 
                                                `date_added` = NOW(), 
                                                `action` = '{$action}',
                                                `reson` = '{$reason}',
                                                `date_started` = '{$discount['date_started']}',
                                                `date_ended` = '{$discount['date_ended']}'";

        $query = $this->db->query($sql);
    }

    public function insertAddOnDealHistory($add_on_deal_id, $reason, $action)
    {
        $add_on_deal = $this->getAddOnDealById($add_on_deal_id);
        $user_id = (int)$this->user->getId();
        $sql = "INSERT INTO `" . DB_PREFIX . "add_on_deal_history` SET `add_on_deal_id` = '{$add_on_deal['add_on_deal_id']}', 
                                                `user_id` = '{$user_id}', 
                                                `date_added` = NOW(), 
                                                `action` = '{$action}',
                                                `reson` = '{$reason}',
                                                `date_started` = '{$add_on_deal['date_started']}',
                                                `date_ended` = '{$add_on_deal['date_ended']}'";

        $query = $this->db->query($sql);
    }

    public function getTotalDiscounts($filter_data)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT COUNT(1) AS `total` FROM `{$DB_PREFIX}discount` AS d 
                    WHERE d.status <> 0";

        $sql .= $this->sqlQueryDiscounts($filter_data);

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalAddOnDeals($filter_data)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT COUNT(1) AS `total` FROM `{$DB_PREFIX}add_on_deal` AS d 
                    WHERE d.status <> 0";

        $sql .= $this->sqlQueryAddOnDeal($filter_data);

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalDiscountsIncludeAllStatuses()
    {
        $sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "discount` d";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getDiscountsCustom($filter_data)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM  `{$DB_PREFIX}discount` AS d 
                    WHERE d.deleted = 0";

        $sql .= $this->sqlQueryDiscounts($filter_data);

        $sql .= " ORDER BY d.date_modify DESC";

        if (isset($filter_data['start']) || isset($filter_data['limit'])) {
            if ($filter_data['start'] < 0) {
                $filter_data['start'] = 0;
            }

            if ($filter_data['limit'] < 1) {
                $filter_data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$filter_data['start'] . "," . (int)$filter_data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getAddOnDealsCustom($filter_data)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM  `{$DB_PREFIX}add_on_deal` AS d 
                    WHERE d.deleted is null";

        $sql .= $this->sqlQueryAddOnDeal($filter_data);

        $sql .= " ORDER BY d.date_modify DESC";

        if (isset($filter_data['start']) || isset($filter_data['limit'])) {
            if ($filter_data['start'] < 0) {
                $filter_data['start'] = 0;
            }

            if ($filter_data['limit'] < 1) {
                $filter_data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$filter_data['start'] . "," . (int)$filter_data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function sqlQueryDiscounts($filter_data = array())
    {
        $sql = "";
        if (isset($filter_data['filter_name']) && $filter_data['filter_name'] !== '') {
            $sql .= " AND (d.`name` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%')";
        }

        if (isset($filter_data['status_discount']) && $filter_data['status_discount'] !== '') {
            $sql .= " AND (d.`status` LIKE '%" . $this->db->escape(trim($filter_data['status_discount'])) . "%')";
            if ($filter_data['status_discount'] == 1) {
                $sql.=" AND CURDATE() >= DATE(d.`date_started`) AND CURDATE() <= DATE(d.`date_ended`)";
            }
        }

        return $sql;
    }

    public function sqlQueryAddOnDeal($filter_data = array())
    {
        $sql = "";
        if (isset($filter_data['filter_name']) && $filter_data['filter_name'] !== '') {
            $sql .= " AND (d.`name` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%')";
        }

        if (isset($filter_data['status_add_on_deal']) && $filter_data['status_add_on_deal'] !== '') {
            $sql .= " AND (d.`status` LIKE '%" . $this->db->escape(trim($filter_data['status_add_on_deal'])) . "%')";
            if ($filter_data['status_add_on_deal'] == 1) {
                $sql.=" AND CURDATE() >= DATE(d.`date_started`) AND CURDATE() <= DATE(d.`date_ended`)";
            }
        }

        return $sql;
    }

    public function checkDiscountsActivated($discount_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM  `{$DB_PREFIX}discount` AS d 
                    WHERE d.deleted = 0 AND status = 1 AND CURDATE() >= DATE(d.`date_started`) AND CURDATE() <= DATE(d.`date_ended`)
                    AND d.discount_id = '$discount_id'";
        $query = $this->db->query($sql);
        if ($query->row['discount_id']) {
            return true;
        }
        return false;
    }

    public function checkAddOnDealActivated($add_on_deal_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM  `{$DB_PREFIX}add_on_deal` AS d 
                    WHERE d.deleted is null AND status = 1 AND CURDATE() >= DATE(d.`date_started`) AND CURDATE() <= DATE(d.`date_ended`)
                    AND d.add_on_deal_id = '$add_on_deal_id'";
        $query = $this->db->query($sql);
        if (isset($query->row['add_on_deal_id'])) {
            return true;
        }
        return false;
    }

    public function getDiscountInfo($discount_id)
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "discount` d 
                    WHERE d.`discount_id` = " . (int)$discount_id ;

        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getAddOnDealInfo($add_on_deal_id)
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "add_on_deal` d 
                    WHERE d.`add_on_deal_id` = " . (int)$add_on_deal_id ;

        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getDiscountProduct($discount_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT dp.*, p.user_create_id, 
                            p.sku as p_sku, p.multi_versions, 
                            p.compare_price as p_compare_price, 
                            p.weight as weight, 
                            pd.name, p.image, 
                            pts.quantity as pts_quantity, 
                            pv.version, pv.compare_price as pv_compare_price, 
                            pv.sku as pv_sku  
                            FROM `{$DB_PREFIX}discount_product` as dp 
                            INNER JOIN `{$DB_PREFIX}product` as p ON (p.product_id = dp.product_id) 
                            INNER JOIN `{$DB_PREFIX}product_description` as pd ON (pd.product_id = dp.product_id) 
                            LEFT JOIN `{$DB_PREFIX}product_version` as pv ON (pv.product_id = dp.product_id AND 
                                                                        CASE WHEN dp.product_version_id <> 0 
                                                                            THEN dp.product_version_id ELSE -1 END = pv.product_version_id) 
                            INNER JOIN `{$DB_PREFIX}product_to_store` as pts ON (dp.product_id = pts.product_id AND 
                                                                        CASE WHEN dp.product_version_id <> 0 
                                                                            THEN dp.product_version_id ELSE 0 END = pts.product_version_id) 
                 WHERE dp.discount_id = {$discount_id}";


        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getAddOnProduct($add_on_deal_id, $type)
    {
        $DB_PREFIX = DB_PREFIX;
        if ($type == "main") {
            $sql = "SELECT dp.*, p.user_create_id, 
                            p.sku as p_sku, p.multi_versions, 
                            p.compare_price as p_compare_price, 
                            p.weight as weight, 
                            pd.name, p.image, 
                            pts.quantity as pts_quantity, 
                            pv.version, pv.compare_price as pv_compare_price, 
                            pv.sku as pv_sku  
                            FROM `{$DB_PREFIX}add_on_deal_main_product` as dp 
                            INNER JOIN `{$DB_PREFIX}product` as p ON (p.product_id = dp.product_id) 
                            INNER JOIN `{$DB_PREFIX}product_description` as pd ON (pd.product_id = dp.product_id) 
                            LEFT JOIN `{$DB_PREFIX}product_version` as pv ON (pv.product_id = dp.product_id AND 
                                                                        CASE WHEN dp.product_version_id <> 0 
                                                                            THEN dp.product_version_id ELSE -1 END = pv.product_version_id) 
                            INNER JOIN `{$DB_PREFIX}product_to_store` as pts ON (dp.product_id = pts.product_id AND 
                                                                        CASE WHEN dp.product_version_id <> 0 
                                                                            THEN dp.product_version_id ELSE 0 END = pts.product_version_id) 
                 WHERE dp.add_on_deal_id = {$add_on_deal_id}";
        }
        else {
            $sql = "SELECT dp.*, p.user_create_id, 
                            p.sku as p_sku, p.multi_versions, 
                            p.compare_price as p_compare_price, 
                            p.weight as weight, 
                            pd.name, p.image, 
                            pts.quantity as pts_quantity, 
                            pv.version, pv.compare_price as pv_compare_price, 
                            pv.sku as pv_sku  
                            FROM `{$DB_PREFIX}add_on_product` as dp 
                            INNER JOIN `{$DB_PREFIX}product` as p ON (p.product_id = dp.product_id) 
                            INNER JOIN `{$DB_PREFIX}product_description` as pd ON (pd.product_id = dp.product_id) 
                            LEFT JOIN `{$DB_PREFIX}product_version` as pv ON (pv.product_id = dp.product_id AND 
                                                                        CASE WHEN dp.product_version_id <> 0 
                                                                            THEN dp.product_version_id ELSE -1 END = pv.product_version_id) 
                            INNER JOIN `{$DB_PREFIX}product_to_store` as pts ON (dp.product_id = pts.product_id AND 
                                                                        CASE WHEN dp.product_version_id <> 0 
                                                                            THEN dp.product_version_id ELSE 0 END = pts.product_version_id) 
                 WHERE dp.add_on_deal_id = {$add_on_deal_id}";
        }
        $query = $this->db->query($sql);
        return $query->rows;
    }


    public function getDiscountTypes()
    {
        $result = $this->db->query("SELECT * FROM `" . DB_PREFIX . "discount_type` WHERE `language_id` = " . (int)$this->config->get('config_language_id'));

        return $result->rows;
    }

    public function getDiscountTypeById($discount_type_id)
    {
        $query = $this->db->query("SELECT `discount_type_id` as id, `name` as title FROM `" . DB_PREFIX . "discount_type` 
                                          WHERE `discount_type_id` = " . (int)$discount_type_id . " AND `language_id` = " . (int)$this->config->get('config_language_id'));

        return $query->rows;
    }

    public function getDiscountStoresById($discount_id)
    {
        $query = $this->db->query("SELECT dt.`store_id` as id, st.`name` as title FROM `" . DB_PREFIX . "discount_to_store` dt LEFT JOIN `" . DB_PREFIX . "store` st ON dt.`store_id` = st.`store_id`
                                    WHERE dt.`discount_id` = " . (int)$discount_id);

        return $query->rows;
    }

    public function getDiscountTypePaginator()
    {
        $sql = "SELECT `discount_type_id` as id, `name` as text FROM " . DB_PREFIX . "discount_type WHERE `language_id` = " . (int)$this->config->get('config_language_id');

        if (!empty($data['filter_name'])) {
            $sql .= " AND `name` LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " ORDER BY `name` ASC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalDiscountTypeFilter()
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "discount_type WHERE `language_id` = " . (int)$this->config->get('config_language_id'));

        return $query->row['total'];
    }

    public function getDiscountsForProductActiveNow() // only get discounts with status 2: Activated; AND with type = product, category, $manufacturer for NOW()
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "discount` d LEFT JOIN `" . DB_PREFIX . "discount_description` dd ON d.`discount_id` = dd.`discount_id` 
                    WHERE (d.`times_used` < d.`usage_limit` OR d.`usage_limit` = 0) 
                    AND d.`discount_type_id` IN (2, 3, 4) 
                    AND d.`start_at` <= NOW() AND 
                        (d.`end_at` IS NULL OR d.`end_at` = '0000-00-00 00:00:00' OR d.`end_at` >= NOW()) 
                    AND d.`discount_status_id` = 2";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getDiscountsForCategoryActiveNow() // only get discounts with status 2: Activated; AND with type = product, category, $manufacturer for NOW()
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "discount` d LEFT JOIN `" . DB_PREFIX . "discount_description` dd ON d.`discount_id` = dd.`discount_id` 
                    WHERE (d.`times_used` < d.`usage_limit` OR d.`usage_limit` = 0) 
                    AND d.`discount_type_id` = 3 
                    AND d.`start_at` <= NOW() AND 
                        (d.`end_at` IS NULL OR d.`end_at` = '0000-00-00 00:00:00' OR d.`end_at` >= NOW()) 
                    AND d.`discount_status_id` = 2";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getDiscountsForManufacturerActiveNow() // only get discounts with status 2: Activated; AND with type = product, category, $manufacturer for NOW()
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "discount` d LEFT JOIN `" . DB_PREFIX . "discount_description` dd ON d.`discount_id` = dd.`discount_id` 
                    WHERE (d.`times_used` < d.`usage_limit` OR d.`usage_limit` = 0) 
                    AND d.`discount_type_id` = 4 
                    AND d.`start_at` <= NOW() AND 
                        (d.`end_at` IS NULL OR d.`end_at` = '0000-00-00 00:00:00' OR d.`end_at` >= NOW()) 
                    AND d.`discount_status_id` = 2";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getDiscountsForOrderActiveNow() // only get discounts with status 2: Activated; AND with type = order
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "discount` d 
                LEFT JOIN `" . DB_PREFIX . "discount_description` dd ON d.`discount_id` = dd.`discount_id` 
                WHERE (d.`times_used` < d.`usage_limit` OR d.`usage_limit` = 0) 
                AND d.`discount_type_id` = 1 
                AND d.`start_at` <= NOW() AND 
                    (d.`end_at` IS NULL OR d.`end_at` = '0000-00-00 00:00:00' OR d.`end_at` >= NOW()) 
                AND d.`discount_status_id` = 2";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function updateStartDate($discount_id, $start_at)
    {
        // validate discount: exists, source
        if ($this->checkDiscountSourceIsValid($discount_id) == false) {
            return false;
        }

        $discount = $this->getDiscountInfo($discount_id);
        $end_at = !isset($discount['end_at']) || $discount['end_at'] === '0000-00-00 00:00:00' ? null : $discount['end_at'];

        // auto start_at
        $discount_status_id = isset($discount['discount_status_id']) ? $discount['discount_status_id'] : 1;
        $discount_status_id = $this->autoChangeDiscountStatusId($discount_status_id, $start_at, $end_at);

        $sql = "UPDATE `" . DB_PREFIX . "discount` SET `start_at` = '" . $start_at . "', 
                                                       `discount_status_id` = '" . (int)$discount_status_id . "'
                                                   WHERE `discount_id` = " . (int)$discount_id . "";
        $this->db->query($sql);

        $this->sendDiscountBrokerMessage($discount_id);
    }

    public function updateEndDate($discount_id, $end_at)
    {
        // validate discount: exists, source
        if ($this->checkDiscountSourceIsValid($discount_id) == false) {
            return false;
        }

        $sql = "UPDATE `" . DB_PREFIX . "discount` SET `end_at` = '" . $end_at . "'
                                                   WHERE `discount_id` = " . (int)$discount_id . "";
        $this->db->query($sql);

        $this->sendDiscountBrokerMessage($discount_id);
    }

    private function updateDiscountStatus($discount_id, $status, $reason = null, $start = null, $end = null)
    {
        $db_prefix = DB_PREFIX;
        $sql = "UPDATE {$db_prefix}discount SET `discount_status_id` = '{$status}'";
        if ($start) {
            $sql .= ", `start_at` = '{$start}'";
        }
        if ($end) {
            $sql .= ", `end_at` = '{$end}'";
        }
        if ($status == ModelDiscountDiscountStatus::STATUS_DELETED) {
            $sql .= ", `delete_reason` = '{$reason}', `deleted_at` = NOW() ";
        } elseif ($status == ModelDiscountDiscountStatus::STATUS_PAUSED) {
            $sql .= ", `pause_reason` = '{$reason}', `paused_at` = NOW() ";
        }

        $sql .= " WHERE discount_id = '" . (int)$discount_id . "'";

        $this->db->query($sql);

        $this->sendDiscountBrokerMessage($discount_id);
    }

    public function getDiscountByName($name)
    {
        $sql = "select dd.name as discount_name, d.code as discount_code, d.discount_id  from  " . DB_PREFIX . "discount d 
                left join " . DB_PREFIX . "discount_description dd ON (dd.discount_id = d.discount_id)";
        $sql .= "WHERE dd.language_id = " . (int)$this->config->get('config_language_id') . "
                 AND d.discount_status_id <> '4'";

        if (isset($name) && $name !== '') {
            $sql .= " AND (dd.`name` LIKE '%" . $this->db->escape(trim($name)) . "%' 
                      OR d.`code` LIKE '%" . $this->db->escape(trim($name)) . "%')";
        }
        $sql = $sql . " group by d.discount_id";
        $sql .= " ORDER BY d.discount_status_id = '3'
                           , d.discount_status_id = '1'
                           , d.discount_status_id = '2'
                           , d.start_at DESC";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function validateNameExist($name, $discount_id)
    {
        $sql = "SELECT COUNT(DISTINCT d.discount_id) AS total FROM `" . DB_PREFIX . "discount` d LEFT JOIN `" . DB_PREFIX . "discount_description` dd ON d.`discount_id` = dd.`discount_id` 
                 WHERE dd.`language_id` = " . (int)$this->config->get('config_language_id') . " AND dd.`name` = '" . $this->db->escape($name) . "'";
        if (!empty($discount_id)) {
            $sql .= " AND d.`discount_id` != " . (int)$discount_id;
        }

        $sql .= " AND d.`discount_status_id` !=  4"; // deleted

        $query = $this->db->query($sql);
        if ($query->row['total'] != 0) {
            return true;
        }
        return false;
    }

    public function validateCodeExist($code, $discount_id)
    {
        $sql = "SELECT COUNT(DISTINCT discount_id) as total FROM `" . DB_PREFIX . "discount` WHERE `code` = '" . $this->db->escape($code) . "'";
        if (!empty($discount_id)) {
            $sql .= " AND `discount_id` != " . (int)$discount_id;
        }

        $query = $this->db->query($sql);
        if ($query->row['total'] != 0) {
            return true;
        }
        return false;
    }

    public function getDiscountsWithTypeAndDate($type_id, $start, $end, $discount_id) // only get discounts with status 1: Scheduled; 2: Activated;
    {
        if (!$end || $end == '0000-00-00 00:00:00'){
            $sql = "SELECT * FROM `" . DB_PREFIX . "discount` d WHERE `discount_type_id` = " . (int)$type_id . " AND `discount_id` != " . (int)$discount_id . " AND 
                        (
                            (`start_at` >= '" . $this->db->escape($start) . "') 
                            OR (`start_at` < '" . $this->db->escape($start) . "' 
                                AND (`end_at` >= '" . $this->db->escape($start) . "' 
                                    OR `end_at` IS NULL OR `end_at` = '0000-00-00 00:00:00'
                                )
                            )
                        )";
        }else{
            $sql = "SELECT * FROM `" . DB_PREFIX . "discount` d WHERE `discount_type_id` = " . (int)$type_id . " AND `discount_id` != " . (int)$discount_id . " AND 
                        (
                            (`start_at` >= '" . $this->db->escape($start) . "' 
                                AND `start_at` <= '" . $this->db->escape($end) . "'
                            ) 
                            OR (`end_at` >= '" . $this->db->escape($start) . "' 
                                AND `end_at` <= '" . $this->db->escape($end) . "'
                            ) 
                            OR (`start_at` <= '" . $this->db->escape($start) . "' 
                                AND `end_at` >= '" . $this->db->escape($end) . "' 
                                ) 
                            OR (
                                (`end_at` IS NULL OR `end_at` = '0000-00-00 00:00:00')
                                AND (`start_at` <= '" . $this->db->escape($end) ."')
                            )
                        )";
        }

        $sql .= " AND discount_status_id IN (1, 2)";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getDiscountsByStatus($discount_status_id)
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "discount` d 
                WHERE `discount_status_id` = " . (int)$discount_status_id;
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getActivatedDiscounts()
    {
        return $this->getDiscountsByStatus($type_id = ModelDiscountDiscountStatus::STATUS_ACTIVATED);
    }

    public function addOrderDiscount($order_id, $discount_ids)
    {
        if (!$order_id || !is_array($discount_ids) || empty($discount_ids)){
            return;
        }

        $this->db->query("DELETE FROM `" . DB_PREFIX ."order_to_discount` WHERE `order_id` = " . (int)$order_id);
        foreach ($discount_ids as $discount_id) {
            if (!$discount_id){
                continue;
            }

            $this->db->query("INSERT INTO `" . DB_PREFIX . "order_to_discount` SET `order_id` = " . (int)$order_id . ", `discount_id` = " .(int)$discount_id);

            // update used_times. Notice: support parallel updating using COUNT() instead of increase by 1
            $this->updateUsedTimesByDiscountId($discount_id);
        }
    }

    public function getUsedDiscountsByOrderId($order_id)
    {
        if (!$order_id) {
            return [];
        }

        $DB_PREFIX = DB_PREFIX;
        $order_id = (int)$order_id;
        $sql = "SELECT * FROM `{$DB_PREFIX}order_to_discount` otd 
                LEFT JOIN `{$DB_PREFIX}discount` d ON otd.`discount_id` = d.`discount_id` 
                WHERE otd.`order_id` = {$order_id}";

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function countDiscountsUsedTimes()
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT otd.`discount_id`, COUNT(otd.`order_id`) as used_times 
                FROM `{$DB_PREFIX}order_to_discount` otd 
                LEFT JOIN `{$DB_PREFIX}order` o ON otd.`order_id` = o.`order_id` 
                WHERE o.`order_status_id` IN (7, 8, 9)
                GROUP BY `discount_id`";

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function countDiscountsUsedTimesByDiscountId($discount_id)
    {
        $query = $this->db->query($this->getCountUsedTimesByDiscountIdSql($discount_id));

        return $query->rows;
    }

    public function updateUsedTimesByDiscountId($discount_id)
    {
        if (!$discount_id) {
            return 0;
        }

        $discount_id = (int)$discount_id;
        $count_sql = $this->getCountUsedTimesByDiscountIdSql($discount_id);
        if (empty($count_sql)) {
            return 0;
        }

        $DB_PREFIX = DB_PREFIX;
        $sql = "UPDATE `{$DB_PREFIX}discount` d SET d.`times_used` = ({$count_sql}) 
                WHERE d.`discount_id` = {$discount_id}";

        $this->sendDiscountBrokerMessage($discount_id);

        return $this->db->query($sql);
    }

    private function getCountUsedTimesByDiscountIdSql($discount_id)
    {
        if (!$discount_id) {
            return '';
        }

        $DB_PREFIX = DB_PREFIX;
        $discount_id = (int)$discount_id;
        $sql = "SELECT COUNT(otd.`order_id`) as used_times 
                FROM `{$DB_PREFIX}order_to_discount` otd 
                LEFT JOIN `{$DB_PREFIX}order` o ON otd.`order_id` = o.`order_id` 
                WHERE o.`order_status_id` IN (7, 8, 9) 
                AND otd.`discount_id` = {$discount_id}";

        return $sql;
    }

    private function autoChangeDiscountStatusId($discount_status_id, $start_at, $end_at)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $now = new DateTime();
        $start_at = $start_at ? new DateTime($start_at) : '';
        $end_at = $end_at ? new DateTime($end_at) : '';

        //if (in_array($discount_status_id, [ModelDiscountDiscountStatus::STATUS_PAUSED, ModelDiscountDiscountStatus::STATUS_DELETED])) {
        if (in_array($discount_status_id, [4])) {
            return $discount_status_id;
        }

        if ($start_at <= $now && ((!$end_at instanceof DateTime) || $end_at >= $now)) {
            //return ModelDiscountDiscountStatus::STATUS_ACTIVATED;
            return 2;
        }

        if ($start_at > $now && ((!$end_at instanceof DateTime) || $end_at >= $now)) {
            //return ModelDiscountDiscountStatus::STATUS_SCHEDULED;
            return 1;
        }

        if ($start_at < $now && (($end_at instanceof DateTime) && $end_at < $now)) {
            //return ModelDiscountDiscountStatus::STATUS_PAUSED;
            return 3;
        }

        return $discount_status_id;
    }

    public function getDiscountById($id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "discount` d LEFT JOIN `" . DB_PREFIX . "discount_description` dd ON d.`discount_id` = dd.`discount_id` 
                                          WHERE d.`discount_id` = " . (int)$id);

        return $query->row;
    }

    public function getAddOnDealById($id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "add_on_deal` d 
                                          WHERE d.`add_on_deal_id` = " . (int)$id);

        return $query->row;
    }

    public function getSaleProduct($product_id, $product_version_id)
    {
        $sql = "SELECT m.* 
                    FROM
                        (SELECT dp_1.* 
                            FROM " . DB_PREFIX . "discount_product AS dp_1 
                            JOIN " . DB_PREFIX . "discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                WHERE d_1.date_started <= NOW() 
                                    AND d_1.date_ended >= NOW() 
                                    AND d_1.status = 1 
                                    AND dp_1.status = 1 
                                    AND dp_1.product_id = {$product_id}
                                    AND dp_1.product_version_id = {$product_version_id}
                                    ) AS m
                        LEFT JOIN (SELECT dp_1.price_discount, dp_1.product_id 
                            FROM " . DB_PREFIX . "discount_product AS dp_1 
                            JOIN " . DB_PREFIX . "discount AS d_1 ON (dp_1.discount_id = d_1.discount_id) 
                                WHERE d_1.date_started <= NOW() 
                                    AND d_1.date_ended >= NOW() 
                                    AND d_1.status = 1 
                                    AND dp_1.status = 1 
                                    AND dp_1.product_id = {$product_id}
                                    AND dp_1.product_version_id = {$product_version_id}
                                    ) AS b                                                                                              
                        ON m.product_id = b.product_id 
                        AND m.price_discount > b.price_discount 
                        WHERE b.price_discount IS NULL 
                        AND m.product_id = {$product_id}
                        AND m.product_version_id = {$product_version_id}
                        GROUP BY m.product_id, m.product_version_id
                        ";
        $query = $this->db->query($sql);
        $price_discount = 0;
        if (isset($query->row['price_discount'])) {
            $price_discount = $query->row['price_discount'];
        }
        return $price_discount;
    }

    public function getProductPopupDiscount($data = array())
    {
        $sql = "SELECT p.user_create_id, 
                        p.status as p_status, 
                        p.sku as psku, 
                        p.sale_on_out_of_stock as p_sale_on_out_of_stock, 
                        p.product_id as pid, 
                        p.multi_versions, 
                        p.price as o_price, 
                        p.compare_price as o_compare_price, 
                        p.weight as weight, 
                        p.quantity as o_quantity, 
                        dp.name, p.image as p_image, pts.quantity as pts_quantity, 
                        pv.* 
                from " . DB_PREFIX . "product p 
                LEFT JOIN " . DB_PREFIX . "product_description as dp ON (p.product_id = dp.product_id) 
                LEFT JOIN " . DB_PREFIX . "product_version as pv ON CASE 
                                                                            WHEN p.multi_versions = 1 THEN p.product_id 
                                                                            ELSE -1
                                                                            END = pv.product_id";

        $sql.= " INNER JOIN " . DB_PREFIX . "product_to_store as pts ON (p.product_id = pts.product_id AND 
                        CASE WHEN p.multi_versions = 1 THEN pv.product_version_id ELSE 0 END = pts.product_version_id)";

        if (!empty($data['category_id'])) {
            $sql.= " LEFT JOIN " . DB_PREFIX . "product_to_category as ptc ON (p.product_id = ptc.product_id)";
        }

        $sql .= " WHERE 1=1 and dp.language_id = " . (int)$this->config->get('config_language_id');

        if (isset($data['current_user_id']) && $data['current_user_id'] !== '') {
            $sql .= " AND p.user_create_id = " . $data['current_user_id'];
        }

        if (!empty($data['category_id'])) {
            $sql .= " AND ptc.category_id = " . (int)$data['category_id'];
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND dp.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['product_ids']) && empty($data['product_version_ids'])) {
            $product_ids = implode(", ", $data['product_ids']);
            $sql .= " AND p.product_id NOT IN(" . $product_ids . ")";
        }

        if (empty($data['product_ids']) && !empty($data['product_version_ids'])) {
            $product_version_ids = implode(", ", $data['product_version_ids']);
            $sql .= " AND (CASE WHEN p.multi_versions = 1 THEN pv.product_version_id NOT IN(" . $product_version_ids . ") ELSE -1 END)";
        }

        if (!empty($data['product_ids']) && !empty($data['product_version_ids'])) {
            $product_ids = implode(", ", $data['product_ids']);
            $product_version_ids = implode(", ", $data['product_version_ids']);
            $sql .= " AND ((p.product_id NOT IN(" . $product_ids . ") AND p.multi_versions = 0)  
                        OR (pv.product_version_id NOT IN(" . $product_version_ids . ") AND p.multi_versions = 1))";
        }
        
        $sql .= " AND ( p.`deleted` IS NULL OR p.`deleted` = 2)"; // 2 is draft
        $sql .= " AND (pv.`deleted` IS NULL OR pv.`deleted` = 2 )";
        $sql .= " ORDER BY dp.name ASC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);
        $result = array();
        foreach ($query->rows as $k => $item) {
            $result[$k] = array(
                'product_id' => $item['pid'],
                'name' => mb_strlen($item['name']) > 90 ? mb_substr($item['name'], 0, 90) . "..." : $item['name'],
                'product_version_id' => $item['product_version_id'],
                'product_name' => $item['name'],
                'weight' => $item['weight'],
                'image' => $item['p_image'],
                'version' => $item['version'],
                'sale_on_out_of_stock' => $item['p_sale_on_out_of_stock'],
                'multi_versions' => $item['multi_versions'],
                'quantity' => $item['pts_quantity']
            );

            if ($item['product_version_id'] != null) {
                $result[$k]['id'] = $item['pid'] . '-' . $item['product_version_id'];
                $result[$k]['price'] = $item['compare_price'];
                $result[$k]['status'] = $item['status'];
                $result[$k]['p_status'] = $item['p_status'];
                $result[$k]['sku'] = $item['sku'];
            } else {
                $result[$k]['id'] = $item['pid'];
                $result[$k]['price'] = $item['o_compare_price'];
                $result[$k]['status'] = $item['p_status'];
                $result[$k]['p_status'] = $item['p_status'];
                $result[$k]['sku'] = $item['psku'];
            }
        }

        return $result;
    }

    public function countProductPopupDiscount($data = array())
    {
        $language_id = $this->config->get('config_language_id');
        $sql = "SELECT p.product_id,pd.name from " . DB_PREFIX . "product p 
                LEFT JOIN " . DB_PREFIX . "product_description as pd ON (p.product_id = pd.product_id) 
                LEFT JOIN " . DB_PREFIX . "product_version as pv ON (p.product_id = pv.product_id)";

        $sql.= " INNER JOIN " . DB_PREFIX . "product_to_store as pts ON (p.product_id = pts.product_id AND 
                        CASE WHEN p.multi_versions = 1 THEN pv.product_version_id ELSE 0 END = pts.product_version_id)";

        if (!empty($data['category_id'])) {
            $sql.= " LEFT JOIN " . DB_PREFIX . "product_to_category as ptc ON (p.product_id = ptc.product_id) WHERE ptc.category_id = " . (int)$data['category_id'];
        }

        if (isset($data['current_user_id']) && $data['current_user_id'] !== '') {
            $sql .= " AND p.user_create_id = " . $data['current_user_id'];
        }

        if (isset($data['user_store_id']) && $data['user_store_id'] !== '') {
            $sql .= " AND pts.store_id IN (". $data['user_store_id'] .")";
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " AND (p.`deleted` IS NULL OR p.`deleted` = 2)";
        $sql .= " AND (pv.`deleted` IS NULL OR pv.`deleted` = 2)";
        $sql .= " AND pd.language_id = " . $language_id . "";
        $sql .= " ORDER BY p.product_id ASC";
        $query = $this->db->query($sql);

        return count($query->rows);
    }

    /**
     * @param $discount_id
     * @return bool
     */
    private function checkDiscountSourceIsValid($discount_id) {
        // validate discount: exists, source
        $old_discount = $this->getDiscountById($discount_id);
        $invalid_sources = ['omihub'];
        return !isset($old_discount['source']) || (isset($old_discount['source']) && !in_array($old_discount['source'], $invalid_sources));
    }

    /**
     * send discount message to RabbitMQ
     *
     * @param $discount_id
     * @param string $old_discount_code
     */
    private function sendDiscountBrokerMessage($discount_id, $old_discount_code = '')
    {
        try {
            if (!empty($discount_id)) {
                $discount = $this->getDiscountInfo($discount_id);
                if (!empty($discount)) {
                    $message = [
                        'shop_name' => $this->config->get('shop_name'),
                        'discount' => [
                            'name' => $discount['name'],
                            'description' => $discount['description'],
                            'code' => $discount['code'],
                            'old_code' => $old_discount_code,
                            'discount_type_id' => $discount['discount_type_id'],
                            'discount_status_id' => $discount['discount_status_id'],
                            'usage_limit' => $discount['usage_limit'],
                            'times_used' => $discount['times_used'],
                            'config' => $discount['config'],
                            'order_source' => $discount['order_source'],
                            'customer_group' => $discount['customer_group'],
                            'start_at' => $discount['start_at'],
                            'end_at' => $discount['end_at']
                        ]
                    ];

                    $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'discount', '', 'discount', json_encode($message));
                } else {
                    $this->log->write('Send message discount error: Could not get discount data for blog id ' . $discount_id);
                }
            } else {
                $this->log->write('Send message discount error: Missing discount_id ');
            }
        } catch (Exception $exception) {
            $this->log->write('Send message discount error: ' . $exception->getMessage());
        }
    }

    private function buildDiscountProduct($data, $discount_id)
    {
        $discount_products = [];

        if (!empty($data['product_ids'])) {
            foreach ($data['product_ids'] as $key => $id) {
                if (empty($id)) {
                    continue;
                }

                $product_id = $id;
                $product_version_id = 0;
                if (strpos($id, '-')) {
                    $pvId = explode('-', $id);
                    $product_id = $pvId[0];
                    $product_version_id = !empty($pvId[1]) ? $pvId[1] : 0;
                }

                if (empty($product_id)) {
                    continue;
                }

                $price_discount = 0;
                if (!empty($data['price_after_reduction']) && !empty($data['price_after_reduction'][$key])) {
                    $price_discount = (float)str_replace(',', '', $data['price_after_reduction'][$key]);
                }

                $status = 0;
                if (!empty($data['active_status_products'])) {
                    foreach ($data['active_status_products'] as $item_id) {
                        if ($item_id == $id) {
                            $status = 1;
                        }
                    }
                }

                // build data
                $discount_products[] = [
                    'product_id' => $product_id,
                    'product_version_id' => $product_version_id,
                    'discount_id' => $discount_id,
                    'price_discount' => $price_discount,
                    'status' => $status,
                ];
            }
        }

        return $discount_products;
    }

    private function buildAddOnDealProduct($data, $add_on_deal_id)
    {
        $add_on_deal_attached_products = [];
        $add_on_deal_main_products = [];

        if (!empty($data['product_ids'])) {
            foreach ($data['product_ids'] as $key => $id) {
                if (empty($id)) {
                    continue;
                }

                $product_id = $id;
                $product_version_id = 0;
                if (strpos($id, '-')) {
                    $pvId = explode('-', $id);
                    $product_id = $pvId[0];
                    $product_version_id = !empty($pvId[1]) ? $pvId[1] : 0;
                }

                if (empty($product_id)) {
                    continue;
                }

                $price_discount = 0;
                if (!empty($data['price_after_reduction']) && !empty($data['price_after_reduction'][$key])) {
                    $price_discount = (float)str_replace(',', '', $data['price_after_reduction'][$key]);
                }

                $limit_quantity = 0;
                if (!empty($data['limit_quantity']) && !empty($data['limit_quantity'][$key])) {
                    $limit_quantity = (float)str_replace(',', '', $data['limit_quantity'][$key]);
                }

                $status = 0;
                if (!empty($data['active_status_products'])) {
                    foreach ($data['active_status_products'] as $item_id) {
                        if ($item_id == $id) {
                            $status = 1;
                        }
                    }
                }

                // build data
                $add_on_deal_attached_products[] = [
                    'product_id' => $product_id,
                    'product_version_id' => $product_version_id,
                    'add_on_deal_id' => $add_on_deal_id,
                    'discount_price' => $price_discount,
                    'limit_quantity' => $limit_quantity,
                    'status' => $status,
                ];
            }
        }

        if (!empty($data['product_main_ids'])) {
            foreach ($data['product_main_ids'] as $key => $id) {
                if (empty($id)) {
                    continue;
                }

                $product_id = $id;
                $product_version_id = 0;
                if (strpos($id, '-')) {
                    $pvId = explode('-', $id);
                    $product_id = $pvId[0];
                    $product_version_id = !empty($pvId[1]) ? $pvId[1] : 0;
                }

                if (empty($product_id)) {
                    continue;
                }

                $status = 0;
                if (!empty($data['active_status_products'])) {
                    foreach ($data['active_status_products'] as $item_id) {
                        if ($item_id == $id) {
                            $status = 1;
                        }
                    }
                }

                // build data
                $add_on_deal_main_products[] = [
                    'product_id' => $product_id,
                    'product_version_id' => $product_version_id,
                    'add_on_deal_id' => $add_on_deal_id,
                    'status' => $status,
                ];
            }
        }

        return [
            "add_on_deal_attached_products" => $add_on_deal_attached_products,
            "add_on_deal_main_products" => $add_on_deal_main_products
        ];
    }

    private function addDiscountProduct($data, $discount_id)
    {
        if (empty($data)) {
            return;
        }

        $DB_PREFIX = DB_PREFIX;

        try {
            // delete first
            $this->db->query("DELETE FROM `{$DB_PREFIX}discount_product` WHERE `discount_id` = {$discount_id}");

            $count = count($data);
            // add new
            $sql = "INSERT INTO `{$DB_PREFIX}discount_product` (product_id, product_version_id, discount_id, price_discount, status) VALUES ";
            foreach ($data as $key => $item) {
                if ($key == ($count - 1)) {
                    $sql .= " ({$item['product_id']}, {$item['product_version_id']}, {$item['discount_id']}, {$item['price_discount']}, {$item['status']});";
                    break;
                }
                $sql .= " ({$item['product_id']}, {$item['product_version_id']}, {$item['discount_id']}, {$item['price_discount']}, {$item['status']}),";
            }

            $this->db->query($sql);
            $this->deleteProductCaches();
        } catch (Exception $exception) {
            $this->log->write('[addDiscountProduct] error : ' . $exception->getMessage());
        }
    }

    private function addOnDealMainProduct($data, $add_on_deal_id)
    {
        if (empty($data)) {
            return;
        }

        $DB_PREFIX = DB_PREFIX;

        try {
            // delete first
            $this->db->query("DELETE FROM `{$DB_PREFIX}add_on_deal_main_product` WHERE `add_on_deal_id` = {$add_on_deal_id}");

            $count = count($data);
            // add new
            $sql = "INSERT INTO `{$DB_PREFIX}add_on_deal_main_product` (add_on_deal_id, product_id, product_version_id, status) VALUES ";
            foreach ($data as $key => $item) {
                if ($key == ($count - 1)) {
                    $sql .= " ({$item['add_on_deal_id']}, {$item['product_id']}, {$item['product_version_id']}, {$item['status']});";
                    break;
                }
                $sql .= " ({$item['add_on_deal_id']}, {$item['product_id']}, {$item['product_version_id']}, {$item['status']}),";
            }

            $this->db->query($sql);
            $this->deleteProductCaches();
        } catch (Exception $exception) {
            $this->log->write('[addDiscountProduct] error : ' . $exception->getMessage());
        }
    }

    private function addOnDealAttachedProduct($data, $add_on_deal_id)
    {
        if (empty($data)) {
            return;
        }

        $DB_PREFIX = DB_PREFIX;

        try {
            // delete first
            $this->db->query("DELETE FROM `{$DB_PREFIX}add_on_product` WHERE `add_on_deal_id` = {$add_on_deal_id}");

            $count = count($data);
            // add new
            $sql = "INSERT INTO `{$DB_PREFIX}add_on_product` (add_on_deal_id, product_id, product_version_id, limit_quantity, current_quantity, discount_price, status) VALUES ";
            foreach ($data as $key => $item) {
                $current_quantity = $item['limit_quantity'];
                if ($key == ($count - 1)) {
                    $sql .= " ({$item['add_on_deal_id']}, {$item['product_id']}, {$item['product_version_id']}, {$item['limit_quantity']}, $current_quantity, {$item['discount_price']}, {$item['status']});";
                    break;
                }
                $sql .= " ({$item['add_on_deal_id']}, {$item['product_id']}, {$item['product_version_id']}, {$item['limit_quantity']}, $current_quantity, {$item['discount_price']}, {$item['status']}),";
            }

            $this->db->query($sql);
            $this->deleteProductCaches();
        } catch (Exception $exception) {
            $this->log->write('[addDiscountProduct] error : ' . $exception->getMessage());
        }
    }

    /**
     * delete product caches
     */
    private function deleteProductCaches() {
        $product_key_redis =  DB_PREFIX . 'product';
        $this->cache->delete($product_key_redis);
    }
}
