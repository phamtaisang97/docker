<?php

class ModelDiscountAddOnDeal extends Model
{
    public function getActiveProductIdsInMainProduct($deal_id = 0)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT aodmp.* FROM `{$DB_PREFIX}add_on_deal_main_product` AS aodmp 
                     JOIN `{$DB_PREFIX}add_on_deal` AS aod ON aodmp.add_on_deal_id = aod.add_on_deal_id 
                        WHERE aod.date_started <= NOW() 
                            AND aod.date_ended >= NOW() 
                            AND aod.status = 1 
                            AND aod.deleted IS NULL";

        if (!empty($deal_id)) {
            $sql .= " AND aod.add_on_deal_id <> {$deal_id}";
        }

        $query = $this->db->query($sql);

        $pv_ids = [
            "product_id" => [],
            "product_version_id" => []
        ];

        foreach ($query->rows as $main_product) {
            if ($main_product['product_version_id']) {
                $pv_ids['product_version_id'][] = $main_product['product_version_id'];
            } else {
                $pv_ids['product_id'][] = $main_product['product_id'];
            }
        }

        return $pv_ids;
    }
}
