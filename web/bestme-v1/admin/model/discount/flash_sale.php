<?php

class ModelDiscountFlashSale extends Model
{
    const STATUS_ACTIVED = 1;
    const STATUS_UNACTIVED = 0;

    public function addFlashSale($data)
    {
        $sql = "INSERT INTO `" . DB_PREFIX . "flash_sale` SET `collection_id` = '" . (int)$this->db->escape($data['collection_id']) . "',
                                                            `starting_date` = '" . $this->db->escape($data['starting_date']) . "',
                                                            `ending_date` = '" . $this->db->escape($data['ending_date']) . "', 
                                                            `starting_time` = '" . $this->db->escape($data['starting_time']) . "',
                                                            `ending_time` = '" . $this->db->escape($data['ending_time']) . "', 
                                                            `created_at` = NOW()
                                                            ";

        $this->db->query($sql);
        return $this->db->getLastId();
    }

    public function editFlashSale($flash_sale_id, $data)
    {
        $sql = "UPDATE `" . DB_PREFIX . "flash_sale` SET `collection_id` = '" . (int)$this->db->escape($data['collection_id']) . "',
                                                            `starting_date` = '" . $this->db->escape($data['starting_date']) . "',
                                                            `ending_date` = '" . $this->db->escape($data['ending_date']) . "', 
                                                            `starting_time` = '" . $this->db->escape($data['starting_time']) . "',
                                                            `ending_time` = '" . $this->db->escape($data['ending_time']) . "',
                                                            `status` = '" . $this->db->escape($data['status']) . "'
                                                             WHERE `flash_sale_id` = " . (int)$flash_sale_id . "
                                                            ";
        $this->db->query($sql);
    }

    public function deleteFlashSale($flash_sale_id)
    {
        $sql = "DELETE FROM " . DB_PREFIX . "flash_sale WHERE flash_sale_id = '" . (int)$flash_sale_id . "'";
        $this->db->query($sql);
    }

    public function getTotalFlashSales($filter_data)
    {
        $sql = "SELECT COUNT(1) total FROM " . DB_PREFIX . "flash_sale f 
        LEFT JOIN " . DB_PREFIX . "collection c ON f.collection_id = c.collection_id 
        WHERE 1 ";

        if (!empty($data['filter_name'])) {
            $sql .= " AND c.`title` LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (isset($data['filter_status']) && '' != $data['filter_status'] && ($data['filter_status'] == self::STATUS_ACTIVED || $data['filter_status'] == self::STATUS_UNACTIVED)) {
            $sql .= " AND f.`status` = '" . (int)$this->db->escape($data['filter_status']) . "'";
        }
        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalDiscountsIncludeAllStatuses()
    {
        $sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "discount` d";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getFlashSales($data = array())
    {
        $sql = "SELECT f.*, c.`title` collection_title FROM " . DB_PREFIX . "flash_sale f 
        LEFT JOIN " . DB_PREFIX . "collection c ON f.collection_id = c.collection_id 
        WHERE 1 ";

        if (!empty($data['filter_name'])) {
            $sql .= " AND c.`title` LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (isset($data['filter_status']) && '' != $data['filter_status'] && ($data['filter_status'] == self::STATUS_ACTIVED || $data['filter_status'] == self::STATUS_UNACTIVED)) {
            $sql .= " AND f.`status` = '" . (int)$this->db->escape($data['filter_status']) . "'";
        }

        $sort_data = array(
            'f.status',
            'f.starting_date',
            'f.ending_date',
            'f.starting_time',
            'f.ending_time'
        );
        $sql .= " ORDER BY f.`status` DESC, f.created_at DESC";
        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= ", " . $data['sort'];
        } else {
            $sql .= ", f.starting_date";
        }

        if (isset($data['order']) && ($data['order'] == 'ASC')) {
            $sql .= " ASC";
        } else {
            $sql .= " DESC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }


    public function getFlashSale($flash_sale_id)
    {
        $sql = "SELECT f.*, c.`title` collection_title FROM `" . DB_PREFIX . "flash_sale` f LEFT JOIN " . DB_PREFIX . "collection c ON f.collection_id = c.collection_id WHERE f.`flash_sale_id` = " . (int)$flash_sale_id . " LIMIT 1";
        $query = $this->db->query($sql);

        return $query->row;
    }

    private function updateFlashSaleStatus($discount_id, $status)
    {
        $db_prefix = DB_PREFIX;
        $sql = "UPDATE {$db_prefix}flash_sale SET `status` = '{$status}' WHERE discount_id = '" . (int)$discount_id . "'";

        $this->db->query($sql);
    }
}
