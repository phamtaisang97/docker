<?php

class ModelOnlineStoreGoogleShopping extends Model
{
    public function getKeys()
    {
        try {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "novaon_api_credentials");

            $rows = $query->rows;
            if (count($rows) < 1) {
                return [];
            }

            return [
                'site_key' => $rows[0]['consumer_key'],
                'secure_key' => $rows[0]['consumer_secret']
            ];
        } catch (Exception $e) {
            return [];
        }
    }
}
