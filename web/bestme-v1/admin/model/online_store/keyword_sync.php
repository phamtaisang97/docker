<?php

class ModelOnlineStoreKeywordSync extends Model
{

    public function checkExistKeywordWithShop($data)
    {
       $keywords = $this->db->query("SELECT keyword FROM " . DB_PREFIX . "keyword_sync")->rows;

        foreach ($keywords as $keyword) {

            $pos = strpos($data, $keyword['keyword']);

            if ($pos !== false) {
               return true;
            }

        }
    }

}
