<?php


class ModelCashFlowPaymentVoucher extends Model
{
    const PAYMENT_VOUCHER_STATUS_ID_CANCEL = 0;
    const PAYMENT_VOUCHER_STATUS_ID_COMPLETE = 1;

    const PAYMENT_VOUCHER_TYPE_ID_AUTOMATION = 8;

    const NOT_IN_BUSINESS_REPORT = 0;
    const IN_BUSINESS_REPORT = 1;

    const OBJECT_CUSTOMER = 1;
    const OBJECT_STAFF = 2;
    const OBJECT_MANUFACTURER = 3;
    const OBJECT_SHIPPING_PARTNER = 4;
    const OBJECT_OTHER = 5;

    const CASH_FLOW_METHOD_CASH = 1;

    const SHIPPING_PARTNERS = ['Giao Hàng Nhanh', 'Giao Hàng Tiết Kiệm', 'ViettelPost'];

    /**
     * @param bool $get_all
     * @return mixed
     */
    public function getPaymentVoucherTypes($get_all = true)
    {
        $language_id = $this->getCurrentLanguageId();
        $sql = "SELECT * FROM `" . DB_PREFIX . "payment_voucher_type` 
                WHERE `language_id` = " . $language_id;
        if (!$get_all) { // exclude 'Thu tự động'
            $sql .= " AND `payment_voucher_type_id` <> " . self::PAYMENT_VOUCHER_TYPE_ID_AUTOMATION;
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * get a random payment voucher type id
     * @return int|mixed
     */
    public function getRandomPaymentVoucherTypeId()
    {
        $language_id = $this->getCurrentLanguageId();
        $sql = "SELECT `payment_voucher_type_id` FROM `" . DB_PREFIX . "payment_voucher_type` WHERE `language_id` = " . $language_id;
        // exclude 'Thu tự động'
        $sql .= " AND `payment_voucher_type_id` <> " . self::PAYMENT_VOUCHER_TYPE_ID_AUTOMATION;
        $sql .= " ORDER BY RAND() LIMIT 1";
        $query = $this->db->query($sql);

        return isset($query->row['payment_voucher_type_id']) ? $query->row['payment_voucher_type_id'] : 0;
    }

    /**
     * @param array $data filter, order direction, ...
     * @return array
     */
    public function getPaymentVouchers($data = [])
    {
        $DB_PREFIX = DB_PREFIX;
        $language_id = $this->getCurrentLanguageId();

        $sql = "SELECT R.*, T.`name` type, S.`name` store_name, M.`name` method FROM `{$DB_PREFIX}payment_voucher` R
                INNER JOIN `{$DB_PREFIX}payment_voucher_type` T ON R.`payment_voucher_type_id` = T.`payment_voucher_type_id`
                INNER JOIN `{$DB_PREFIX}store` S ON R.`store_id` = S.`store_id`
                INNER JOIN `{$DB_PREFIX}cash_flow_method` M ON R.`cash_flow_method_id` = M.`cash_flow_method_id`
                WHERE T.`language_id` = {$language_id}
                  AND M.`language_id` = {$language_id}";

        $sql = $this->getFilterQuery($sql, $data);

        if(!$this->user->isAdmin() && !$this->user->canAccessAll()) {
            $sql .= " AND R.user_create_id = '" . (int)$this->user->getId() . "'";
            if(count($this->user->getUserStores()) > 0) {
                $sql .= " AND R.store_id in (" . implode(',', $this->user->getUserStores()) . ")";
            }
        }

        $sql .= " ORDER BY R.`date_modified` DESC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = $this->config->get('config_limit_admin');
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * @param array $data filter, order direction, ...
     * @return array
     */
    public function getTotalPaymentVouchers($data = [])
    {
        $DB_PREFIX = DB_PREFIX;
        $language_id = $this->getCurrentLanguageId();

        $sql = "SELECT COUNT(1) total FROM `{$DB_PREFIX}payment_voucher` R
                INNER JOIN `{$DB_PREFIX}payment_voucher_type` T ON R.`payment_voucher_type_id` = T.`payment_voucher_type_id`
                INNER JOIN `{$DB_PREFIX}store` S ON R.`store_id` = S.`store_id`
                INNER JOIN `{$DB_PREFIX}cash_flow_method` M ON R.`cash_flow_method_id` = M.`cash_flow_method_id`
                WHERE T.`language_id` = {$language_id}
                  AND M.`language_id` = {$language_id}";

        $sql = $this->getFilterQuery($sql, $data);

        if(!$this->user->isAdmin() && !$this->user->canAccessAll()) {
            $sql .= " AND R.user_create_id = '" . (int)$this->user->getId() . "'";
            if(count($this->user->getUserStores()) > 0) {
                $sql .= " AND R.store_id in (" . implode(',', $this->user->getUserStores()) . ")";
            }
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    /**
     * @param $payment_voucher_id
     * @return mixed
     */
    public function getPaymentVoucher($payment_voucher_id)
    {
        $DB_PREFIX = DB_PREFIX;

        $sql = "SELECT R.*, S.name as store_name , RR.return_receipt_code
                FROM `{$DB_PREFIX}payment_voucher` R
                LEFT JOIN `{$DB_PREFIX}store` S ON R.`store_id` = S.`store_id`
                LEFT JOIN `{$DB_PREFIX}return_receipt` RR ON R.`return_receipt_id` = RR.`return_receipt_id`
                WHERE R.`payment_voucher_id` = {$payment_voucher_id}";
        $query = $this->db->query($sql);

        return $query->row;
    }

    /**
     * @param array $data
     */
    public function updateStatus($data)
    {
        $sql = sprintf("UPDATE %s 
                        SET `status` = '%d', 
                            `date_modified` = NOW() 
                        WHERE `payment_voucher_id` = '%d'",
            DB_PREFIX . "payment_voucher",
            (int)$data['status'],
            (int)$data['payment_voucher_id']
        );

        $this->db->query($sql);
    }

    /**
     * @param string $sql
     * @param array $data format as
     * [
     *     "filter_name" => ...,
     *     "filter_payment_voucher_type" => ...,
     *     "filter_status" => ...,
     *     "filter_cash_flow_method" => ...,
     * ]
     * @return string
     */
    private function getFilterQuery($sql, $data)
    {
        if (!empty($data['filter_name'])) {
            $sql .= " AND R.`payment_voucher_code` LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%'";
        }

        if (!empty($data['filter_payment_voucher_type'])) {
            $filter_payment_voucher_type = implode(",", $data['filter_payment_voucher_type']);
            if (!empty($filter_payment_voucher_type)) {
                $sql .= " AND R.`payment_voucher_type_id` IN (" . $filter_payment_voucher_type . ") ";
            }
        }

        if (!empty($data['filter_status'])) {
            $filter_status = implode(",", $data['filter_status']);
            $sql .= " AND R.`status` IN (" . $filter_status . ") ";
        }

        if (!empty($data['filter_cash_flow_method'])) {
            $filter_cash_flow_method = implode(",", $data['filter_cash_flow_method']);
            if (!empty($filter_cash_flow_method)) {
                $sql .= " AND R.`cash_flow_method_id` IN (" . $filter_cash_flow_method . ") ";
            }
        }

        return $sql;
    }

    /**
     * @return int|mixed
     */
    private function getCurrentLanguageId()
    {
        $this->load->model('localisation/language');
        return $this->model_localisation_language->getCurrentLanguageId();
    }

    /**
     * @param $data
     * @return bool
     */
    public function addPaymentVoucher($data)
    {
        $code = $this->autoGenerationCode();
        $order_id = isset($data['order_id']) && $data['order_id'] ? $data['order_id'] : NULL;
        $object_info = $this->objectInfoEncode($data['object_id'], $data['object_info']);
        $in_business_status = isset($data['in_business_report']) ? self::IN_BUSINESS_REPORT : self::NOT_IN_BUSINESS_REPORT;
        $amount = str_replace(',', '', $data['amount']);

        try {
            $sql = "INSERT INTO " . DB_PREFIX . "payment_voucher 
                              SET payment_voucher_code = '" . $code . "',
                                  payment_voucher_type_id = '" . $this->db->escape($data['payment_type_id']) . "', 
                                  store_receipt_id = '" . (int)$order_id . "', 
                                  status = '" . self::PAYMENT_VOUCHER_STATUS_ID_COMPLETE . "', 
                                  in_business_report_status = '" . $in_business_status . "', 
                                  amount = '" . (double)$amount . "', 
                                  store_id = " . (int)$data['store_id'] . ", 
                                  cash_flow_method_id = '" . (int)$data['method_id'] . "', 
                                  cash_flow_object_id = '" . (int)$data['object_id'] . "', 
                                  object_info = '" . $this->db->escape($object_info) . "', 
                                  payment_type_other = '" . $this->db->escape($data['payment_type_other']) . "', 
                                  note = '" . $this->db->escape($data['note']) . "', 
                                  user_create_id = '" . (int)$this->user->getId() . "',
                                  date_added = NOW(), 
                                  date_modified = NOW()";
            $this->db->query($sql);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $object_id
     * @param $object_info
     * @return false|string
     */
    public function objectInfoEncode($object_id, $object_info)
    {
        $this->load->model('customer/customer');
        $this->load->model('user/user');
        $this->load->model('catalog/manufacturer');

        switch (true) {
            case (int)$object_id === self::OBJECT_CUSTOMER && isset($object_info['customer']) && $object_info['customer']:
                $customer = $this->model_customer_customer->getCustomer($object_info['customer']);
                $result = ['id' => $object_info['customer'], 'name' => $customer['lastname'] . ' ' . $customer['firstname']];
                break;
            case (int)$object_id === self::OBJECT_STAFF && isset($object_info['staff']) && $object_info['staff']:
                $user = $this->model_user_user->getUser($object_info['staff']);
                $result = ['id' => $object_info['staff'], 'name' => $user['lastname'] . ' '. $user['firstname']];
                break;
            case (int)$object_id === self::OBJECT_MANUFACTURER && isset($object_info['manufacturer']) && $object_info['manufacturer']:
                $manufacturer = $this->model_catalog_manufacturer->getManufacturer($object_info['manufacturer']);
                $result = ['id' => $object_info['manufacturer'], 'name' => $manufacturer['name'] ? $manufacturer['name'] : ''];
                break;
            case (int)$object_id === self::OBJECT_SHIPPING_PARTNER:
                $result = ['id' => null, 'name' => $object_info['shipping-partner']];
                break;
            case (int)$object_id === self::OBJECT_OTHER:
                $result = ['id' => null, 'name' => $object_info['other']];
                break;
            default:
                $result = '';
                break;
        }

        return json_encode($result);
    }

    /**
     * @param $payment_id
     * @param $note
     * @return bool
     */
    public function updatePaymentVoucherNote($payment_id, $note)
    {
        try {
            $sql = "UPDATE `" . DB_PREFIX . "payment_voucher` SET `note` = '" . $this->db->escape($note) . "', `date_modified` = NOW() WHERE `payment_voucher_id` = " . (int)$payment_id;
            return $this->db->query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @return string
     */
    public function autoGenerationCode()
    {
        // max
        $max = $this->getMaxPaymentsId();

        // make code
        $prefix = 'PCT';
        $code = $prefix . str_pad($max + 1, 6, "0", STR_PAD_LEFT);

        return $code;
    }

    /**
     * @return mixed
     */
    private function getMaxPaymentsId()
    {
        $sql = "SELECT MAX(`payment_voucher_id`) AS max_id FROM `" . DB_PREFIX . "payment_voucher`";

        $query = $this->db->query($sql);

        return $query->row['max_id'];
    }

    /**
     * @param $store_receipt_id
     * @param $data
     */
    public function autoCreateOrUpdatePaymentVoucher($store_receipt_id, $data)
    {
        // Check if exist receipt voucher then update else create new
        $query = $this->db->query("SELECT `payment_voucher_id` FROM `" . DB_PREFIX . "payment_voucher` WHERE `store_receipt_id` = " . (int)$store_receipt_id . " ");

        if ($query->num_rows > 0 && isset($query->row['payment_voucher_id'])) {
            $this->autoUpdateVoucherFromOrder($store_receipt_id, $data);
        } else {
            $this->autoAddFromOrder($store_receipt_id, $data);
        }
    }

    /**
     * @param $return_receipt_id
     * @param $data
     * @return bool
     */
    public function autoAddFromReturnReceipt($return_receipt_id, $amount, $store_id = 0)
    {
        $code = $this->autoGenerationCode();

        try {
            $sql = "INSERT INTO " . DB_PREFIX . "payment_voucher 
                    SET payment_voucher_code = '" . $code . "',
                        payment_voucher_type_id = '" . self::PAYMENT_VOUCHER_TYPE_ID_AUTOMATION . "', 
                        return_receipt_id = '" . (int)$return_receipt_id . "', 
                        status = '" . self::PAYMENT_VOUCHER_STATUS_ID_COMPLETE . "', 
                        in_business_report_status = '" . self::IN_BUSINESS_REPORT . "', 
                        amount = '" . (double)$amount . "', 
                        store_id = ".(int)$store_id.", 
                        cash_flow_method_id = '" . self::CASH_FLOW_METHOD_CASH . "', 
                        cash_flow_object_id = '" . self::OBJECT_CUSTOMER . "', 
                        note = '', 
                        date_added = NOW(), 
                        date_modified = NOW()";
            $this->db->query($sql);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $store_receipt_id
     * @param $data
     * @return bool
     */
    public function autoAddFromOrder($store_receipt_id, $data)
    {
        $this->load->model('catalog/manufacturer');
        $code = $this->autoGenerationCode();
        $amount = $data['total_paid'] ? extract_number($data['total_paid']) : 0;
        $method = self::CASH_FLOW_METHOD_CASH;

        $manufacturer = $this->model_catalog_manufacturer->getManufacturer($data['receipt_manufacturer']);
        $object_info = json_encode(['id' => $data['receipt_manufacturer'], 'name' => $manufacturer['name']]);

        try {
            $user_create_id = $this->user->getId();
            $sql = "INSERT INTO " . DB_PREFIX . "payment_voucher 
                    SET payment_voucher_code = '" . $code . "',
                        payment_voucher_type_id = '" . self::PAYMENT_VOUCHER_TYPE_ID_AUTOMATION . "', 
                        store_receipt_id = '" . (int)$store_receipt_id . "', 
                        status = '" . self::PAYMENT_VOUCHER_STATUS_ID_COMPLETE . "', 
                        in_business_report_status = '" . self::IN_BUSINESS_REPORT . "', 
                        amount = '" . (double)$amount . "', 
                        store_id = " . (int)$data['receipt_store'] . ", 
                        cash_flow_method_id = '" . $method . "', 
                        cash_flow_object_id = '" . self::OBJECT_MANUFACTURER . "', 
                        object_info = '" . $this->db->escape($object_info) . "',  
                        note = '" . $this->db->escape($data['note']) . "', 
                        date_added = NOW(), 
                        date_modified = NOW(),
                        user_create_id = " . $user_create_id;
            $this->db->query($sql);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function autoUpdateVoucherFromOrder($store_receipt_id, $data)
    {
        $this->load->model('catalog/manufacturer');
        $amount = $data['total_paid'] ? extract_number($data['total_paid']) : 0;

        $manufacturer = $this->model_catalog_manufacturer->getManufacturer($data['receipt_manufacturer']);
        $object_info = json_encode(['id' => $data['receipt_manufacturer'], 'name' => $manufacturer['name']]);

        try {
            $sql = "UPDATE " . DB_PREFIX . "payment_voucher 
                              SET amount = '" . (double)$amount . "', 
                                  store_id = " . (int)$data['receipt_store'] . ",  
                                  object_info = '" . $this->db->escape($object_info) . "',  
                                  note = '" . $this->db->escape($data['note']) . "', 
                                  date_modified = NOW()
                                  WHERE store_receipt_id = " . (int)$store_receipt_id;
            $this->db->query($sql);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function autoRemoveVoucherFromOrder($store_receipt_id)
    {
        try {
            $this->db->query("DELETE FROM " . DB_PREFIX . "payment_voucher WHERE store_receipt_id = '" . (int)$store_receipt_id . "'");
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @param $store_receipt_id
     * @return bool
     */
    public function getReceiptVoucherByStoreReceipt($store_receipt_id)
    {
        $a = $store_receipt_id;
        try {
            $query = $this->db->query("SELECT `payment_voucher_id`, `payment_voucher_code` FROM `" . DB_PREFIX . "payment_voucher` 
                                       WHERE `store_receipt_id` = " . (int)$store_receipt_id . " ");
            if ($query->num_rows > 0 && isset($query->row['payment_voucher_id'])) {
                return $query->row;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            return false;
        }
    }
}