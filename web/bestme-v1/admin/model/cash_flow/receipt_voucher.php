<?php


class ModelCashFlowReceiptVoucher extends Model
{
    const RECEIPT_VOUCHER_STATUS_ID_CANCEL = 0;
    const RECEIPT_VOUCHER_STATUS_ID_COMPLETE = 1;

    const RECEIPT_VOUCHER_TYPE_ID_AUTOMATION = 8;

    const NOT_IN_BUSINESS_REPORT = 0;
    const IN_BUSINESS_REPORT = 1;

    const OBJECT_CUSTOMER = 1;
    const OBJECT_STAFF = 2;
    const OBJECT_MANUFACTURER = 3;
    const OBJECT_SHIPPING_PARTNER = 4;
    const OBJECT_OTHER = 5;

    const CASH_FLOW_METHOD_COD = 3;
    const CASH_FLOW_METHOD_CASH = 1;
    const CASH_FLOW_METHOD_CARD = 4;
    const CASH_FLOW_METHOD_TRANSFER = 2;

    const SHIPPING_PARTNERS = ['Giao Hàng Nhanh', 'Giao Hàng Tiết Kiệm', 'ViettelPost'];

    /**
     * @param bool $get_all
     * @return mixed
     */
    public function getReceiptVoucherTypes($get_all = true)
    {
        $language_id = $this->getCurrentLanguageId();
        $sql = "SELECT * FROM `" . DB_PREFIX . "receipt_voucher_type` 
                WHERE `language_id` = " . $language_id;
        if (!$get_all) { // exclude 'Thu tự động'
            $sql .= " AND `receipt_voucher_type_id` <> " . self::RECEIPT_VOUCHER_TYPE_ID_AUTOMATION;
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * get a random receipt voucher type id
     * @return int|mixed
     */
    public function getRandomReceiptVoucherTypeId()
    {
        $language_id = $this->getCurrentLanguageId();
        $sql = "SELECT `receipt_voucher_type_id` FROM `" . DB_PREFIX . "receipt_voucher_type` WHERE `language_id` = " . $language_id;
         // exclude 'Thu tự động'
        $sql .= " AND `receipt_voucher_type_id` <> " . self::RECEIPT_VOUCHER_TYPE_ID_AUTOMATION;
        $sql .= " ORDER BY RAND() LIMIT 1";
        $query = $this->db->query($sql);

        return isset($query->row['receipt_voucher_type_id']) ? $query->row['receipt_voucher_type_id'] : 0;
    }

    /**
     * @param array $data filter, order direction, ...
     * @return array
     */
    public function getReceiptVouchers($data = [])
    {
        $DB_PREFIX = DB_PREFIX;
        $language_id = $this->getCurrentLanguageId();

        $sql = "SELECT R.*, T.`name` type, S.`name` store_name, M.`name` method FROM `{$DB_PREFIX}receipt_voucher` R
                INNER JOIN `{$DB_PREFIX}receipt_voucher_type` T ON R.`receipt_voucher_type_id` = T.`receipt_voucher_type_id`
                INNER JOIN `{$DB_PREFIX}store` S ON R.`store_id` = S.`store_id`
                INNER JOIN `{$DB_PREFIX}cash_flow_method` M ON R.`cash_flow_method_id` = M.`cash_flow_method_id`
                WHERE T.`language_id` = {$language_id}
                  AND M.`language_id` = {$language_id}";

        $sql = $this->getFilterQuery($sql, $data);

        if(!$this->user->isAdmin() && !$this->user->canAccessAll()) {
            $sql .= " AND R.user_create_id = '" . (int)$this->user->getId() . "'";
            if(count($this->user->getUserStores()) > 0) {
                $sql .= " AND R.store_id in (" . implode(',', $this->user->getUserStores()) . ")";
            }
        }

        $sql .= " ORDER BY R.`date_modified` DESC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = $this->config->get('config_limit_admin');
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * @param array $data filter, order direction, ...
     * @return array
     */
    public function getTotalReceiptVouchers($data = [])
    {
        $DB_PREFIX = DB_PREFIX;
        $language_id = $this->getCurrentLanguageId();

        $sql = "SELECT COUNT(1) total FROM `{$DB_PREFIX}receipt_voucher` R
                INNER JOIN `{$DB_PREFIX}receipt_voucher_type` T ON R.`receipt_voucher_type_id` = T.`receipt_voucher_type_id`
                INNER JOIN `{$DB_PREFIX}store` S ON R.`store_id` = S.`store_id`
                INNER JOIN `{$DB_PREFIX}cash_flow_method` M ON R.`cash_flow_method_id` = M.`cash_flow_method_id`
                WHERE T.`language_id` = {$language_id}
                  AND M.`language_id` = {$language_id}";

        $sql = $this->getFilterQuery($sql, $data);

        if(!$this->user->isAdmin() && !$this->user->canAccessAll()) {
            $sql .= " AND R.user_create_id = '" . (int)$this->user->getId() . "'";
            if(count($this->user->getUserStores()) > 0) {
                $sql .= " AND R.store_id in (" . implode(',', $this->user->getUserStores()) . ")";
            }
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    /**
     * @param int $receipt_voucher_id
     * @return mixed
     */
    public function getReceiptVoucher($receipt_voucher_id)
    {
        $DB_PREFIX = DB_PREFIX;

        $sql = "SELECT R.*, S.name as store_name FROM `{$DB_PREFIX}receipt_voucher` R
                INNER JOIN `{$DB_PREFIX}store` S ON R.`store_id` = S.`store_id`
                WHERE R.`receipt_voucher_id` = {$receipt_voucher_id}";
        $query = $this->db->query($sql);

        return $query->row;
    }

    /**
     * @param array $data format as ["status" => ..., "receipt_voucher_id" => ...]
     */
    public function updateStatus($data)
    {
        $sql = sprintf("UPDATE %s 
                        SET `status` = '%d', 
                            `date_modified` = NOW() 
                        WHERE `receipt_voucher_id` = '%d'",
            DB_PREFIX . "receipt_voucher",
            (int)$data['status'],
            (int)$data['receipt_voucher_id']
        );

        $this->db->query($sql);
    }

    /**
     * @param string $sql sql string
     * @param array $data format as
     * [
     *     "filter_name" => ...,
     *     "filter_receipt_voucher_type" => ...,
     *     "filter_status" => ...,
     *     "filter_cash_flow_method" => ...,
     * ]
     * @return string
     */
    private function getFilterQuery($sql, $data)
    {
        if (!empty($data['filter_name'])) {
            $sql .= " AND R.`receipt_voucher_code` LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%'";
        }

        if (!empty($data['filter_receipt_voucher_type'])) {
            $filter_receipt_voucher_type = implode(",", $data['filter_receipt_voucher_type']);
            if (!empty($filter_receipt_voucher_type)) {
                $sql .= " AND R.`receipt_voucher_type_id` IN (" . $filter_receipt_voucher_type . ") ";
            }
        }

        if (!empty($data['filter_status'])) {
            $filter_status = implode(",", $data['filter_status']);
            $sql .= " AND R.`status` IN (" . $filter_status . ") ";
        }

        if (!empty($data['filter_cash_flow_method'])) {
            $filter_cash_flow_method = implode(",", $data['filter_cash_flow_method']);
            if (!empty($filter_cash_flow_method)) {
                $sql .= " AND R.`cash_flow_method_id` IN (" . $filter_cash_flow_method . ") ";
            }
        }

        return $sql;
    }

    /**
     * @param $data
     * @return bool
     */
    public function addReceiptVoucher($data)
    {
        $code = $this->autoGenerationCode();
        $order_id = isset($data['order_id']) && $data['order_id'] ? $data['order_id'] : NULL;
        $object_info = $this->objectInfoEncode($data['object_id'], $data['object_info']);
        $in_business_status = isset($data['in_business_report']) ? self::IN_BUSINESS_REPORT : self::NOT_IN_BUSINESS_REPORT;
        $amount = str_replace(',', '', $data['amount']);

        try {
            $this->db->query("INSERT INTO " . DB_PREFIX . "receipt_voucher 
                              SET receipt_voucher_code = '" . $code . "',
                                  receipt_voucher_type_id = '" . $this->db->escape($data['receipt_type_id']) . "', 
                                  order_id = '" . (int)$order_id . "', 
                                  status = '" . self::RECEIPT_VOUCHER_STATUS_ID_COMPLETE . "', 
                                  in_business_report_status = '" . $in_business_status . "', 
                                  amount = '" . (double)$amount . "', 
                                  store_id = " . (int)$data['store_id'] . ", 
                                  cash_flow_method_id = '" . (int)$data['method_id'] . "', 
                                  cash_flow_object_id = '" . (int)$data['object_id'] . "', 
                                  object_info = '" . $this->db->escape($object_info) . "', 
                                  receipt_type_other = '" . $this->db->escape($data['receipt_type_other']) . "', 
                                  note = '" . $this->db->escape($data['note']) . "', 
                                  user_create_id = '" . (int)$this->user->getId() . "', 
                                  date_added = NOW(), 
                                  date_modified = NOW()");
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param int $receipt_id
     * @param string $note
     * @return bool
     */
    public function updateReceiptVoucherNote($receipt_id, $note)
    {
        try {
            $sql = "UPDATE `" . DB_PREFIX . "receipt_voucher` SET `note` = '" . $this->db->escape($note) . "', `date_modified` = NOW() WHERE `receipt_voucher_id` = " . (int)$receipt_id;
            return $this->db->query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param int $object_id
     * @param string $object_info
     * @return string
     */
    public function objectInfoEncode($object_id, $object_info)
    {
        $this->load->model('customer/customer');
        $this->load->model('user/user');
        $this->load->model('catalog/manufacturer');

        switch (true) {
            case (int)$object_id === self::OBJECT_CUSTOMER && isset($object_info['customer']) && $object_info['customer']:
                $customer = $this->model_customer_customer->getCustomer($object_info['customer']);
                $result = ['id' => $object_info['customer'], 'name' => $customer['lastname'] . ' ' . $customer['firstname']];
                break;
            case (int)$object_id === self::OBJECT_STAFF && isset($object_info['staff']) && $object_info['staff']:
                $user = $this->model_user_user->getUser($object_info['staff']);
                $result = ['id' => $object_info['staff'], 'name' => $user['lastname'] . ' ' . $user['firstname']];
                break;
            case (int)$object_id === self::OBJECT_MANUFACTURER && isset($object_info['manufacturer']) && $object_info['manufacturer']:
                $manufacturer = $this->model_catalog_manufacturer->getManufacturer($object_info['manufacturer']);
                $result = ['id' => $object_info['manufacturer'], 'name' => $manufacturer['name'] ? $manufacturer['name'] : ''];
                break;
            case (int)$object_id === self::OBJECT_SHIPPING_PARTNER:
                $result = ['id' => null, 'name' => $object_info['shipping-partner']];
                break;
            case (int)$object_id === self::OBJECT_OTHER:
                $result = ['id' => null, 'name' => $object_info['other']];
                break;
            default:
                $result = '';
                break;
        }

        return json_encode($result);
    }

    /**
     * @return string
     */
    public function autoGenerationCode()
    {
        // max
        $max = $this->getMaxReceiptsId();

        // make code
        $prefix = 'PTT';
        $code = $prefix . str_pad($max + 1, 6, "0", STR_PAD_LEFT);

        return $code;
    }

    /**
     * @return mixed
     */
    private function getMaxReceiptsId()
    {
        $sql = "SELECT MAX(`receipt_voucher_id`) AS max_id FROM `" . DB_PREFIX . "receipt_voucher`";

        $query = $this->db->query($sql);

        return $query->row['max_id'];
    }

    /**
     * @return int|mixed
     */
    private function getCurrentLanguageId()
    {
        $this->load->model('localisation/language');
        return $this->model_localisation_language->getCurrentLanguageId();
    }


    /**
     * Auto update receipt_voucher when update order
     *
     * @param $order_id
     * @param $data
     */
    public function autoUpdateFromOrder($order_id, $data)
    {
        $payment_status = $data['payment_status'];
        if ($payment_status == 1) {
            $this->autoAddOrUpdateFromOrder($order_id, $data);
        } else {
            $this->removeReceiptFromOrder($order_id);
        }
    }


    /**
     * Auto create receipt_voucher when have order paid
     *
     * @param $order_id
     * @param $data
     */
    public function autoAddOrUpdateFromOrder($order_id, $data)
    {
        // Check if exist receipt voucher then update else create new
        $query = $this->db->query("SELECT `receipt_voucher_id` FROM `" . DB_PREFIX . "receipt_voucher` WHERE `order_id` = " . (int)$order_id . " ");

        if ($query->num_rows > 0 && isset($query->row['receipt_voucher_id'])) {
            $this->autoUpdateVoucherFromOrder($order_id, $data);
        } else {
            $this->autoAddFromOrder($order_id, $data);
        }

    }

    /**
     * Auto update receipt_voucher when update order
     *
     * @param $order_id
     * @param $data
     * @return bool
     */
    public function autoAddFromOrder($order_id, $data)
    {
        $code = $this->autoGenerationCode();
        $in_business_status = self::IN_BUSINESS_REPORT;
        $receipt_voucher_type_id = self::RECEIPT_VOUCHER_TYPE_ID_AUTOMATION;
        $method_id = 0;
        if ($data['payment_method'] == '198535319739A' || $data['payment_method'] == 'Cash on Delivery') {
            $method_id = self::CASH_FLOW_METHOD_COD;
        } else {
            $method_id = self::CASH_FLOW_METHOD_TRANSFER;
        }
        try {
            $this->db->query("INSERT INTO " . DB_PREFIX . "receipt_voucher 
                              SET receipt_voucher_code = '" . $code . "',
                                  receipt_voucher_type_id = '" . $receipt_voucher_type_id . "', 
                                  order_id = '" . (int)$order_id . "', 
                                  status = '" . self::RECEIPT_VOUCHER_STATUS_ID_COMPLETE . "', 
                                  in_business_report_status = '" . $in_business_status . "', 
                                  amount = '" . (double)$this->db->escape($data['total_pay']) . "', 
                                  cash_flow_method_id = '" . $method_id . "', 
                                  cash_flow_object_id = '" . self::OBJECT_CUSTOMER . "', 
                                  note = 'Thêm tự động từ đơn hàng đã thanh toán', 
                                  date_added = NOW(), 
                                  date_modified = NOW()");
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $order_id
     * @param $data
     * @return bool
     */
    public function autoUpdateVoucherFromOrder($order_id, $data)
    {
        $method_id = 0;
        if ($data['payment_method'] == '198535319739A' || $data['payment_method'] == 'Cash on Delivery') {
            $method_id = self::CASH_FLOW_METHOD_COD;
        } else {
            $method_id = self::CASH_FLOW_METHOD_TRANSFER;
        }
        try {
            $this->db->query("UPDATE `" . DB_PREFIX . "receipt_voucher` 
                              SET amount = '" . (double)$this->db->escape($data['total_pay']) . "', 
                                cash_flow_method_id = '" . $method_id . "', 
                                date_modified = NOW() 
                                WHERE order_id = " . (int)$order_id);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $order_id
     * @return bool
     */
    public function removeReceiptFromOrder($order_id)
    {
        try {
            $this->db->query("DELETE FROM " . DB_PREFIX . "receipt_voucher 
                              WHERE order_id = '" . (int)$order_id . "'");
            return true;
        } catch (Exception $ex) {
            return false;
        }

    }

    /**
     * @param $order_id
     * @return bool
     */
    public function getReceiptVoucherByOrder($order_id)
    {
        try {
            $query = $this->db->query("SELECT `receipt_voucher_id`, `receipt_voucher_code` FROM `" . DB_PREFIX . "receipt_voucher` 
                                       WHERE `order_id` = " . (int)$order_id . " ");
            if ($query->num_rows > 0 && isset($query->row['receipt_voucher_id'])) {
                return $query->row;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * TODO: remove?...
     *
     * @param $name
     * @return mixed
     */
    private function getReceiptVoucherTypeByName($name)
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "receipt_voucher_type` WHERE `name` = '" . $this->db->escape($name) . "' ";
        $result = $this->db->query($sql);
        $id = $result->row['receipt_voucher_type_id'];
        return $id;
    }
}