<?php


class ModelCashFlowCashFlow extends Model
{
    /**
     * @param array $data filter, order direction, ...
     * @return array
     */
    public function getCashFlowMethods($data = [])
    {
        $DB_PREFIX = DB_PREFIX;
        $language_id = $this->getCurrentLanguageId();
        $sql = "SELECT * FROM `{$DB_PREFIX}cash_flow_method` 
                WHERE `language_id` = " . $language_id;
        $sql .= " ORDER BY cash_flow_method_id ASC";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getAllObject()
    {
        $language_id = $this->getCurrentLanguageId();
        $sql = "SELECT * FROM `" . DB_PREFIX . "cash_flow_object` 
                WHERE `language_id` = " . $language_id;
        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * @param string $start datetime string format 'Y-m-d'
     * @param string $end datetime string format 'Y-m-d'
     * @param string $type "payment" or "receive"
     * @param bool $get_previous_period
     * @return int
     */
    public function getCashFlowAmountInDateRange($start, $end, $type, $get_previous_period = false)
    {
        $DB_PREFIX = DB_PREFIX;
        $table_name = $type . '_voucher';
        $sql = "SELECT SUM(`amount`) total FROM `{$DB_PREFIX}{$table_name}` 
                WHERE `status` = 1 ";
        if ($get_previous_period) {
            $sql .= " AND `date_added` < '{$start}'";
        } else {
            $sql .= " AND `date_added` BETWEEN '{$start}' AND '{$end}'";
        }

        if(!$this->user->isAdmin() && !$this->user->canAccessAll()) {
            $sql .= " AND user_create_id = '" . (int)$this->user->getId() . "'";
            if(count($this->user->getUserStores()) > 0) {
                $sql .= " AND store_id in (". implode(',', $this->user->getUserStores()) .")";
            }
        }
        $query = $this->db->query($sql);
        return is_null($query->row['total']) ? 0 : $query->row['total'];
    }

    /**
     * @param string $start datetime string format 'Y-m-d'
     * @param string $end datetime string format 'Y-m-d'
     * @param string $type "payment" or "receive"
     * @param bool $get_previous_period
     * @return bool
     */
    public function checkVoucherExistsInDateRange($start, $end, $type, $get_previous_period = false)
    {
        $DB_PREFIX = DB_PREFIX;
        $table_name = $type . '_voucher';
        $sql = "SELECT EXISTS (SELECT 1 FROM `{$DB_PREFIX}{$table_name}` 
                WHERE `status` = 1 AND
                (`date_added` BETWEEN '{$start}' AND '{$end}')";

        if(!$this->user->isAdmin() && !$this->user->canAccessAll()) {
            $sql .= " AND user_create_id = '" . (int)$this->user->getId() . "'";
            if(count($this->user->getUserStores()) > 0) {
                $sql .= " AND store_id in (". implode(',', $this->user->getUserStores()) .")";
            }
        }

        $sql .= ") result ";
        $query = $this->db->query($sql);
        $exists = $query->row['result'];

        return 1 == $exists;
    }

    /**
     * @param array $filter format as ["start" => ..., "end" => ...]
     * @return mixed
     */
    public function getCashInflowsByReceiptType($filter)
    {
        $DB_PREFIX = DB_PREFIX;
        $language_id = $this->getCurrentLanguageId();

        $sql = "SELECT T.`receipt_voucher_type_id`, T.`name`, 
                  (SELECT COALESCE(SUM(R.`amount`), 0) FROM `{$DB_PREFIX}receipt_voucher` R 
                   WHERE R.`receipt_voucher_type_id` = T.`receipt_voucher_type_id`
                     AND (R.`date_added` BETWEEN '{$filter['start']}' AND '{$filter['end']}')
                     AND R.`status` = 1";

        if(!$this->user->isAdmin() && !$this->user->canAccessAll()) {
            $sql .= " AND R.user_create_id = '" . (int)$this->user->getId() . "'";
            if(count($this->user->getUserStores()) > 0) {
                $sql .= " AND R.store_id in (" . implode(',', $this->user->getUserStores()) . ")";
            }
        }

        $sql .= ") total_amount
                FROM `{$DB_PREFIX}receipt_voucher_type` T
                WHERE T.`language_id` = {$language_id}
                ORDER BY T.`receipt_voucher_type_id` ASC";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * @param array $filter format as ["start" => ..., "end" => ...]
     * @return mixed
     */
    public function getCashOutflowsByPaymentType($filter)
    {
        $DB_PREFIX = DB_PREFIX;
        $language_id = $this->getCurrentLanguageId();

        $sql = "SELECT T.`payment_voucher_type_id`, T.`name`, 
                  (SELECT COALESCE(SUM(P.`amount`), 0) FROM `{$DB_PREFIX}payment_voucher` P
                   WHERE P.`payment_voucher_type_id` = T.`payment_voucher_type_id`
                     AND (P.`date_added` BETWEEN '{$filter['start']}' AND '{$filter['end']}')
                     AND P.`status` = 1";

        if(!$this->user->isAdmin() && !$this->user->canAccessAll()) {
            $sql .= " AND P.user_create_id = '" . (int)$this->user->getId() . "'";
            if(count($this->user->getUserStores()) > 0) {
                $sql .= " AND P.store_id in (" . implode(',', $this->user->getUserStores()) . ")";
            }
        }

        $sql .= ") total_amount
                FROM `{$DB_PREFIX}payment_voucher_type` T
                WHERE T.`language_id` = {$language_id}
                ORDER BY T.`payment_voucher_type_id` ASC";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * @param array $filter format as ["start" => ..., "end" => ...]
     * @return mixed
     */
    public function getCashInflowsByPaymentMethod($filter)
    {
        $DB_PREFIX = DB_PREFIX;
        $language_id = $this->getCurrentLanguageId();

        $sql = "SELECT M.`cash_flow_method_id`, M.`name`, 
                  (SELECT COALESCE(SUM(R.`amount`), 0) FROM `{$DB_PREFIX}receipt_voucher` R
                   WHERE R.`cash_flow_method_id` = M.`cash_flow_method_id`
                     AND (R.`date_added` BETWEEN '{$filter['start']}' AND '{$filter['end']}')
                     AND R.`status` = 1";

        if(!$this->user->isAdmin() && !$this->user->canAccessAll()) {
            $sql .= " AND R.user_create_id = '" . (int)$this->user->getId() . "'";
            if(count($this->user->getUserStores()) > 0) {
                $sql .= " AND R.store_id in (" . implode(',', $this->user->getUserStores()) . ")";
            }
        }

        $sql .= ") total_amount
                FROM `{$DB_PREFIX}cash_flow_method` M
                WHERE M.`language_id` = {$language_id}
                ORDER BY M.`cash_flow_method_id` ASC";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * @param array $filter format as ["start" => ..., "end" => ...]
     * @return mixed
     */
    public function getCashOutflowsByPaymentMethod($filter)
    {
        $DB_PREFIX = DB_PREFIX;
        $language_id = $this->getCurrentLanguageId();

        $sql = "SELECT M.`cash_flow_method_id`, M.`name`, 
                  (SELECT COALESCE(SUM(P.`amount`), 0) FROM `{$DB_PREFIX}payment_voucher` P
                   WHERE P.`cash_flow_method_id` = M.`cash_flow_method_id`
                     AND (P.`date_added` BETWEEN '{$filter['start']}' AND '{$filter['end']}')";

        if(!$this->user->isAdmin() && !$this->user->canAccessAll()) {
            $sql .= " AND P.user_create_id = '" . (int)$this->user->getId() . "'";
            if(count($this->user->getUserStores()) > 0) {
                $sql .= " AND P.store_id in (" . implode(',', $this->user->getUserStores()) . ")";
            }
        }

        $sql .= " AND P.`status` = 1) total_amount
                FROM `{$DB_PREFIX}cash_flow_method` M
                WHERE M.`language_id` = {$language_id}
                ORDER BY M.`cash_flow_method_id` ASC";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * @return int|mixed
     */
    private function getCurrentLanguageId()
    {
        $this->load->model('localisation/language');
        $language = $this->model_localisation_language->getLanguageByCode($this->config->get('config_language'));

        return isset($language['language_id']) ? $language['language_id'] : 2; // default 'vn'
    }

    /**
     * get a random cash flow method id
     * @return int|mixed
     */
    public function getRandomCashFlowMethodId()
    {
        $language_id = $this->getCurrentLanguageId();
        $sql = "SELECT `cash_flow_method_id` FROM `" . DB_PREFIX . "cash_flow_method` WHERE `language_id` = " . $language_id . " ORDER BY RAND() LIMIT 1";
        $query = $this->db->query($sql);

        return isset($query->row['cash_flow_method_id']) ? $query->row['cash_flow_method_id'] : 0;
    }
}