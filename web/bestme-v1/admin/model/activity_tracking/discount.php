<?php

class ModelActivityTrackingDiscount extends Model {

    CONST DELETED_STATUS = 4;

    public function getCountDiscount()
    {
        $sql = "SELECT count(discount_id) as count_discount from " . DB_PREFIX . "discount where discount_status_id <> ". self::DELETED_STATUS;
        $result = $this->db->query($sql);
        return $result->row['count_discount'];
    }
}