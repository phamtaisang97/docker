<?php

class ModelActivityTrackingProduct extends Model
{

    CONST NOT_PRODUCT_DEMO = 0;

    public function getCountProduct()
    {
        $sql = "SELECT count('product_id') as count_product 
            from " . DB_PREFIX . "product 
            where 
                demo = " . self::NOT_PRODUCT_DEMO . " AND 
                MONTH(date_modified) = " . date('m') . " AND 
                YEAR(date_modified) = " . date('Y');
        $result = $this->db->query($sql);
        return $result->row['count_product'];
    }


    public function getCountProductCategory()
    {
        $sql = "SELECT count('category_id') as count_category from " . DB_PREFIX . "category where 1";
        $result = $this->db->query($sql);
        return $result->row['count_category'];
    }

    public function getAvgProductPrice()
    {
        $sql = "SELECT AVG(pp.price) as avg_price 
                FROM 
                    (
                    -- select price of product (dont have multi_version)
                    SELECT IF (p.price <= 0.0000, p.compare_price, p.price) as price
                    FROM " . DB_PREFIX . "product as p
                    Where MONTH(date_modified) = ".date('m')."
                        AND YEAR(date_modified) = ".date('Y')."
                        ANd p.deleted is NULL
                        AND multi_versions = 0
                    UNION ALL
                     -- select price of product's versions
                    SELECT ppv.price
                    FROM " . DB_PREFIX . "product as p
                    LEFT JOIN (SELECT IF (pv.price <= 0.0000, pv.compare_price, pv.price) as price, pv.product_id
                        FROM " . DB_PREFIX . "product_version as pv
                        WHERE pv.deleted is NULL) ppv on p.product_id = ppv.product_id
                    Where MONTH(date_modified) = ".date('m')."
                        AND YEAR(date_modified) = ".date('Y')."
                        ANd p.deleted is NULL
                        AND multi_versions = 1
                        ) as pp";

        $result = $this->db->query($sql);
        return $result->row['avg_price'];
    }

    public function getRateProductHaveCostPrice()
    {
        $sql = "SELECT COUNT(product_to_store.product_id) AS count_product, 
                       SUM(IF(cost_price != 0.0000, 1, 0)) count_product_have_cost_price 
            FROM " . DB_PREFIX . "product AS product 
            LEFT JOIN " . DB_PREFIX . "product_to_store AS product_to_store ON product.product_id=product_to_store.product_id 
            WHERE 1 
              AND MONTH(product.date_modified) = " . date('m') . " 
              AND YEAR(product.date_modified) = " . date('Y');
        $result = $this->db->query($sql);

        return $result->row['count_product_have_cost_price'] / $result->row['count_product'];
    }
}