<?php

class ModelActivityTrackingStoreReceipt extends Model
{

    CONST ENABLE_STATUS = 1;

    public function getCountStoreReceipt()
    {
        $sql = "SELECT count(store_receipt_id) as count_store_receipt from " . DB_PREFIX . "store_receipt where status = " . self::ENABLE_STATUS;
        $result = $this->db->query($sql);
        return $result->row['count_store_receipt'];
    }

    public function getCountStoreTransferReceipt()
    {
        $sql = "SELECT count(store_transfer_receipt_id) as count_store_transfer_receipt from " . DB_PREFIX . "store_transfer_receipt where status = " . self::ENABLE_STATUS;
        $result = $this->db->query($sql);
        return $result->row['count_store_transfer_receipt'];
    }

    public function getCountStoreTakeReceipt()
    {
        $sql = "SELECT count(store_take_receipt_id) as count_store_take_receipt from " . DB_PREFIX . "store_take_receipt where status = " . self::ENABLE_STATUS;
        $result = $this->db->query($sql);
        return $result->row['count_store_take_receipt'];
    }

    public function getCountCostAdjustmentReceipt()
    {
        $sql = "SELECT count(cost_adjustment_receipt_id) as count_cost_adjustment_receipt from " . DB_PREFIX . "cost_adjustment_receipt where status = " . self::ENABLE_STATUS;
        $result = $this->db->query($sql);
        return $result->row['count_cost_adjustment_receipt'];
    }
}