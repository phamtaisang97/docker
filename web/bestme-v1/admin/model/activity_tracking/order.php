<?php

class ModelActivityTrackingOrder extends Model {

    CONST ORDER_COMPLETE_STATUS = 9;

    public function getCountOrderComplete()
    {
        $sql = "SELECT count('order_id') as count_order_complete from " . DB_PREFIX . "order where order_status_id = ". self::ORDER_COMPLETE_STATUS;
        $result = $this->db->query($sql);
        return $result->row['count_order_complete'];
    }

    public function getCountTransferStatus() {
        $sql = "SELECT count('order_history_id') as count_transfer from " . DB_PREFIX . "order_history where 1";
        $result = $this->db->query($sql);
        return $result->row['count_transfer'];
    }
}