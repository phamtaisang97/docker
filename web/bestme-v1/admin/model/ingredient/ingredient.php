<?php
class ModelIngredientIngredient extends Model {
    const LIMIT_GET_ITEM_IN_DATA_EXCEL = 50;

    public function getIngredientsCustom($filter = [])
    {
        $sql = $this->ingredientSql($filter);

        // limit
        if (isset($filter['start']) || isset($filter['limit'])) {
            if ($filter['start'] < 0) {
                $filter['start'] = 0;
            }

            if ($filter['limit'] < 1) {
                $filter['limit'] = 20;
            }
            $sql .= " LIMIT " . (int)$filter['start'] . "," . (int)$filter['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalIngredientsCustom($filter = [])
    {
        $sql = $this->ingredientSql($filter);

        $sql = "SELECT COUNT(1) as `total` FROM ($sql) as i";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getIngredientById($ingredientId)
    {
        try {
            $sql = $this->ingredientSql(['ingredient_id' => $ingredientId]);
            $query = $this->db->query($sql);

            return $query->row;
        } catch (Exception $exception) {
            $this->log->write('getIngredientById error: ' . $exception->getMessage());
            return [];
        }
    }

    public function addIngredient($data)
    {
        try {
            $this->db->query("INSERT INTO " . DB_PREFIX . "ingredient 
                          SET `name` = '" . $this->db->escape($data['name']) . "', 
                            `code` = '" . $this->db->escape($data['code']) . "', 
                            `active_ingredient` = '" . $this->db->escape($data['active_ingredient']) . "', 
                            `unit` = '" . $this->db->escape($data['unit']) . "', 
                            `basic_description` = '" . $this->db->escape($data['basic_description']) . "', 
                            `note` = '" . $this->db->escape($data['note']) . "', 
                            `overdose` = '" . $this->db->escape($data['overdose']) . "', 
                            `course_certificates` = '" . $this->db->escape($data['course_certificates']) . "', 
                            `detail_description` = '" . $this->db->escape($data['detail_description']) . "', 
                            `us_man_19_30` = '" . $this->db->escape($data['us_man_19_30']) . "', 
                            `us_man_31_50` = '" . $this->db->escape($data['us_man_31_50']) . "', 
                            `us_man_51_70` = '" . $this->db->escape($data['us_man_51_70']) . "', 
                            `us_man_gt_70` = '" . $this->db->escape($data['us_man_gt_70']) . "', 
                            `us_women_19_30` = '" . $this->db->escape($data['us_women_19_30']) . "', 
                            `us_women_31_50` = '" . $this->db->escape($data['us_women_31_50']) . "', 
                            `us_women_51_70` = '" . $this->db->escape($data['us_women_51_70']) . "', 
                            `us_women_gt_70` = '" . $this->db->escape($data['us_women_gt_70']) . "', 
                            `vn_man_19_50` = '" . $this->db->escape($data['vn_man_19_50']) . "', 
                            `vn_man_51_60` = '" . $this->db->escape($data['vn_man_51_60']) . "', 
                            `vn_man_gt_60` = '" . $this->db->escape($data['vn_man_gt_60']) . "', 
                            `vn_women_19_50` = '" . $this->db->escape($data['vn_women_19_50']) . "', 
                            `vn_women_51_60` = '" . $this->db->escape($data['vn_women_51_60']) . "', 
                            `vn_women_gt_60` = '" . $this->db->escape($data['vn_women_gt_60']) . "', 
                            `created_at` = NOW(), 
                            `updated_at` = NOW()"
            );

            $ingredient_id = $this->db->getLastId();

            return $ingredient_id;
        } catch (Exception $exception) {
            $this->log->write('Add Ingredient error: ' . $exception->getMessage());
            return 0;
        }
    }

    public function editIngredient($ingredient_id, $data)
    {
        try {
            $this->db->query("UPDATE " . DB_PREFIX . "ingredient 
                          SET `name` = '" . $this->db->escape($data['name']) . "', 
                            `code` = '" . $this->db->escape($data['code']) . "', 
                            `active_ingredient` = '" . $this->db->escape($data['active_ingredient']) . "', 
                            `unit` = '" . $this->db->escape($data['unit']) . "', 
                            `basic_description` = '" . $this->db->escape($data['basic_description']) . "', 
                            `note` = '" . $this->db->escape($data['note']) . "', 
                            `overdose` = '" . $this->db->escape($data['overdose']) . "', 
                            `course_certificates` = '" . $this->db->escape($data['course_certificates']) . "', 
                            `detail_description` = '" . $this->db->escape($data['detail_description']) . "', 
                            `us_man_19_30` = '" . $this->db->escape($data['us_man_19_30']) . "', 
                            `us_man_31_50` = '" . $this->db->escape($data['us_man_31_50']) . "', 
                            `us_man_51_70` = '" . $this->db->escape($data['us_man_51_70']) . "', 
                            `us_man_gt_70` = '" . $this->db->escape($data['us_man_gt_70']) . "', 
                            `us_women_19_30` = '" . $this->db->escape($data['us_women_19_30']) . "', 
                            `us_women_31_50` = '" . $this->db->escape($data['us_women_31_50']) . "', 
                            `us_women_51_70` = '" . $this->db->escape($data['us_women_51_70']) . "', 
                            `us_women_gt_70` = '" . $this->db->escape($data['us_women_gt_70']) . "', 
                            `vn_man_19_50` = '" . $this->db->escape($data['vn_man_19_50']) . "', 
                            `vn_man_51_60` = '" . $this->db->escape($data['vn_man_51_60']) . "', 
                            `vn_man_gt_60` = '" . $this->db->escape($data['vn_man_gt_60']) . "', 
                            `vn_women_19_50` = '" . $this->db->escape($data['vn_women_19_50']) . "', 
                            `vn_women_51_60` = '" . $this->db->escape($data['vn_women_51_60']) . "', 
                            `vn_women_gt_60` = '" . $this->db->escape($data['vn_women_gt_60']) . "', 
                            `updated_at` = NOW() WHERE `ingredient_id` = " . (int)$ingredient_id);

            return $ingredient_id;
        } catch (Exception $exception) {
            $this->log->write('Update Ingredient error: ' . $exception->getMessage());
            return 0;
        }
    }

    public function processImportExcel($dataProducts, $offset = 0, $limit = 50)
    {
        $length = self::LIMIT_GET_ITEM_IN_DATA_EXCEL;

        try {
            if ($limit > count($dataProducts)) {
                $limit = count($dataProducts);
            }

            for ($i = $offset; $i < $limit; $i++) {
                if (empty($dataProducts[$i]['code'])) {
                    continue;
                }

                $this->importProduct($dataProducts[$i]);
            }


            if ($limit == count($dataProducts)) {
                return true;
            } else {
                $offset += $length;
                $limit += $length;
                $this->processImportExcel($dataProducts, $offset, $limit);
            }

            return true;
        } catch (\Exception $exception) {
            $this->log->write('[Ingredient processImportExcel] error : ' . $exception->getMessage());
            return false;
        }
    }

    public function processImportExcelProductIngredient($data, $offset = 0, $limit = 50)
    {
        $length = self::LIMIT_GET_ITEM_IN_DATA_EXCEL;

        try {
            if ($limit > count($data)) {
                $limit = count($data);
            }

            for ($i = $offset; $i < $limit; $i++) {
                if (empty(trim($data[$i]['product_sku'])) ||
                    empty($data[$i]['ingredients']) ||
                    !is_array($data[$i]['ingredients']) ||
                    empty($data[$i]['quantitative']) ||
                    !is_array($data[$i]['quantitative'])
                ) {
                   continue;
                }

                // search product
                $productId = $this->getProductIdBySku(trim($data[$i]['product_sku']));
                if (empty($productId)) {
                    continue;
                }

                $this->importProductIngredient($data[$i], $productId);
            }

            if ($limit == count($data)) {
                return true;
            } else {
                $offset += $length;
                $limit += $length;
                $this->processImportExcelProductIngredient($data, $offset, $limit);
            }

            return true;
        } catch (\Exception $exception) {
            $this->log->write('[Ingredient processImportExcelProductIngredient] error : ' . $exception->getMessage());
            return false;
        }
    }

    public function importProduct($item)
    {
        $ingredient_id = null;
        if (trim($item['code'] != '')) {
            $ingredient_id = $this->searchIngredientByCode($item['code']);
        }

        if ($ingredient_id) {
            $this->editIngredient($ingredient_id, $item);
        } else {
            $this->addIngredient($item);
        }
    }

    public function importProductIngredient($item, $productId)
    {
        $DB_PREFIX = DB_PREFIX;
        try {
            // delete first
            $this->db->query("DELETE FROM `{$DB_PREFIX}product_ingredient` WHERE `product_id` = {$productId}");

            // add new
            $sql = "INSERT INTO `{$DB_PREFIX}product_ingredient` (product_id, ingredient_id, quantitative) VALUES ";
            foreach ($item['ingredients'] as $key => $code) {
                // get ingredient by code
                $ingredient_id = $this->searchIngredientByCode($code);
                $quantitative = (isset($item['quantitative'][$key])) ? $item['quantitative'][$key] : '';

                if ($ingredient_id && !empty($quantitative)) {
                    if ($key == (count($item['ingredients']) - 1)) {
                        $sql .= " ({$productId}, {$ingredient_id}, '" . $quantitative . "');";
                        break;
                    }

                    $sql .= " ({$productId}, {$ingredient_id}, '" . $quantitative . "'),";
                }
            }

            $this->log->write('ImportProductIngredient SQL : ' . $sql);
            $this->db->query($sql);
        } catch (Exception $exception) {
            $this->log->write('[ImportProductIngredient] error : ' . $exception->getMessage());
        }
    }

    public function getProductIdBySku($sku)
    {
        $sql = "SELECT product_id FROM " . DB_PREFIX . "product WHERE sku = '" . $this->db->escape($sku) . "' AND deleted IS NULL";
        $query = $this->db->query($sql);
        if (isset($query->row['product_id'])) {
            return (int)$query->row['product_id'];
        }

        $sql = "SELECT product_id FROM " . DB_PREFIX . "product_version WHERE sku = '" . $this->db->escape($sku) . "' AND deleted IS NULL";
        $query = $this->db->query($sql);
        if (isset($query->row['product_id'])) {
            return (int)$query->row['product_id'];
        }

        return 0;
    }

    public function searchIngredientByCode($code)
    {
        $query = $this->db->query("SELECT ingredient_id FROM " . DB_PREFIX . "ingredient WHERE code = '" . $this->db->escape($code) . "'");
        if (isset($query->row['ingredient_id'])) {
            return (int)$query->row['ingredient_id'];
        }

        return false;
    }

    public function deleteIngredient($ingredient_ids)
    {
        try {
            $this->db->query("DELETE FROM `" . DB_PREFIX . "ingredient` WHERE `ingredient_id` IN (" . $ingredient_ids . ")");
            $this->db->query("DELETE FROM `" . DB_PREFIX . "product_ingredient` WHERE `ingredient_id` IN (" . $ingredient_ids . ")");
        } catch (Exception $exception) {
            $this->log->write('[Delete Ingredient] error : ' . $exception->getMessage());
        }
    }

    public function getIngredientOfProductByProductId($product_id)
    {
        try {
            $db_prefix = DB_PREFIX;
            $sql = "SELECT pi.*, i.name, i.code, i.active_ingredient, i.unit, i.ingredient_id as id 
                        FROM `{$db_prefix}product_ingredient` as pi 
                            LEFT JOIN `{$db_prefix}ingredient` as i ON (pi.ingredient_id = i.ingredient_id) 
                                WHERE `product_id` = {$product_id}";

            $query = $this->db->query($sql);
            return $query->rows;
        } catch (Exception $exception) {
            $this->log->write('[getIngredientOfProductByProductId] error : ' . $exception->getMessage());
            return [];
        }
    }

    /**
     * @param $filter array
     * @return string
     */
    private function ingredientSql($filter = [])
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM `{$DB_PREFIX}ingredient` as i WHERE 1";

        if (!empty($filter['ingredient_id'])) {
            $sql .= " AND i.ingredient_id = '" . (int)$this->db->escape($filter['ingredient_id']) . "'";
        }

        if (!empty($filter['filter_name'])) {
            $sql .= " AND (i.`name` LIKE '%" . $this->db->escape(trim($filter['filter_name'])) . "%' ";
            $sql .= " OR i.`code` LIKE '%" . $this->db->escape(trim($filter['filter_name'])) . "%' ) ";
        }

        $sql .= " ORDER BY i.`updated_at` DESC";

        return $sql;
    }
}