<?php


class ModelAppstoreMyApp extends Model
{
    // define some free app code
    const APP_CHAT_BOT = 'mar_ons_chatbot';
    const APP_MAX_LEAD = 'mar_ons_maxlead';
    const APP_LIVE_CHAT_X9 = 'mar_ons_livechatx9';
    const APP_GHN = 'trans_ons_ghn';
    const APP_VIETTEL_POST = 'trans_ons_vtpost';
    const APP_GHTK = 'trans_ons_ghtk';
    const APP_ONFLUENCER = 'mar_ons_onfluencer';

    public function checkInstall($app_code)
    {

        $sqlCheck = "SELECT COUNT(app.id) AS total FROM " . DB_PREFIX . "my_app app 
                WHERE app.app_code = '" . $this->db->escape($app_code) . "'
                AND installed = 1 ";

        $queryCheck = $this->db->query($sqlCheck);

        if ($queryCheck->row['total'] > 0) {
            return true;
        }
        return false;
    }

    public function install($app_code, $used_time = null)
    {
        /* Kiểm tra xem app đã cài đặt chưa
           Chưa thì thêm mới
        */
        $sqlCheck = "SELECT COUNT(app.id) AS total, app.expiration_date AS expiration FROM " . DB_PREFIX . "my_app app 
                WHERE app.app_code = '" . $this->db->escape($app_code) . "' ";

        $queryCheck = $this->db->query($sqlCheck);

        if ($queryCheck->row['total'] > 0) {
            // Nếu còn thời gian sử dụng thì chuyển status về 1;
            $now = new DateTime();
            $expiration = new DateTime($queryCheck->row['expiration']);
            $status = 0;
            if ($expiration >= $now) {
                $status = 1;
            }
            $sql = "UPDATE " . DB_PREFIX . "my_app SET status=" . $status . ", installed = 1 WHERE app_code = '" . $this->db->escape($app_code) . "' ";
            $this->db->query($sql);
        } else {
            if (isset($used_time) && $used_time == -1) {
                $sql = "INSERT INTO `" . DB_PREFIX . "my_app`  
                    SET `app_code` = '" . $this->db->escape($app_code) . "', 
                        `status` = 1, 
                        `expiration_date` = NULL ";
                $this->db->query($sql);
            } else {
                $sql = "INSERT INTO `" . DB_PREFIX . "my_app`  
                    SET `app_code` = '" . $this->db->escape($app_code) . "', 
                        `status` = 1, 
                        `expiration_date` = NOW() + INTERVAL 6 DAY ";
                $this->db->query($sql);
            }

        }
    }

    public function uninstall($app_code)
    {
        $status = 0;
        $sql = "UPDATE " . DB_PREFIX . "my_app SET status=" . $status . " , installed = 0 WHERE app_code = '" . $this->db->escape($app_code) . "' ";
        $this->db->query($sql);
    }

    public function getInstalled()
    {

        $sql = "SELECT * FROM " . DB_PREFIX . "my_app WHERE installed = 1";

        $query = $this->db->query($sql);

        $extension_data = array();

        foreach ($query->rows as $result) {
            $extension_data[] = $result['app_code'];
        }
        return $extension_data;
    }

    public function getInstalledCustom($filter_data)
    {
        $extension_data = array();
        $sql = "SELECT * FROM " . DB_PREFIX . "my_app WHERE installed = 1";

        if (isset($filter_data['app_code_start_with'])) {
            $sql .= " AND `app_code` LIKE '" . $this->db->escape($filter_data['app_code_start_with']) . "%'";
        }

        /* pagination */
        if (isset($filter_data['start']) || isset($filter_data['limit'])) {

            if ($filter_data['start'] < 0) {
                $filter_data['start'] = 0;
            }

            if ($filter_data['limit'] < 1) {
                $filter_data['limit'] = $this->config->get('config_limit_admin');
            }

            $sql .= " LIMIT " . (int)$filter_data['start'] . "," . (int)$filter_data['limit'];
        }

        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
            $extension_data[] = $result['app_code'];
        }

        return $extension_data;
    }

    public function getTotalInstalledApp()
    {
        $sql = "SELECT COUNT(id) as total FROM " . DB_PREFIX . "my_app WHERE installed = 1";

        $query = $this->db->query($sql);
        $total = $query->row['total'];
        return $total;
    }

    public function getInfo($code)
    {
        $sql = $sql = "SELECT * FROM " . DB_PREFIX . "my_app WHERE `app_code` = '" . $this->db->escape($code) . "' ";
        return $this->db->query($sql)->row;
    }

    public function checkAppActive($app_code)
    {
        $sqlCheck = "SELECT COUNT(app.`id`) AS `total` FROM `" . DB_PREFIX . "my_app` app 
                WHERE app.`app_code` = '" . $this->db->escape($app_code) . "'
                AND `installed` = 1 AND `status` = 1";

        $queryCheck = $this->db->query($sqlCheck);

        if ($queryCheck->row['total'] > 0) {
            return true;
        }
        return false;
    }
}