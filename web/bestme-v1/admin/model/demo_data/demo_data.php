<?php

class ModelDemoDataDemoData extends Controller
{
    const VNPAY_METHOD = 'paym_ons_vnpay';
    const COD_METHOD = '198535319739A';
    const BANK_TRANSFER_METHOD = '888888888888';

    /**
     * @return mixed
     * get Product to generate order data
     */
    function getProducts()
    {
        $sql = "SELECT pv.product_version_id, p.weight, p.product_id, p.multi_versions, p.default_store_id,
                    CASE WHEN p.multi_versions = 1 then pv.price ELSE p.price END AS price,
                    CASE WHEN p.multi_versions = 1 then pv.compare_price ELSE p.compare_price END AS compare_price
                FROM " . DB_PREFIX . "product p
                LEFT JOIN " . DB_PREFIX . "product_description pd ON p.product_id = pd.product_id
                LEFT JOIN " . DB_PREFIX . "product_version pv ON p.product_id = pv.product_id
                WHERE (
                    p.multi_versions = 0 
                    AND p.compare_price > 0 
                    AND p.deleted IS NULL
                )
                OR 
                (
                    p.multi_versions = 1 
                    AND pv.compare_price > 0
                    AND pv.deleted IS NULL
                )
                GROUP BY p.product_id
                LIMIT 20 
                ";

        return $this->db->query($sql)->rows;
    }

    /**
     * @return mixed
     * get Customer to generate order data
     */
    function getCustomers()
    {
        $sql = "SELECT *
                FROM " . DB_PREFIX . "customer c
                inner JOIN " . DB_PREFIX . "address a ON c.customer_id = a.customer_id
                GROUP BY c.customer_id
                ORDER BY c.customer_id desc
                LIMIT 10
                ";

        return $this->db->query($sql)->rows;
    }

    /**
     * @param $orders
     * update date_added, date_modified in order
     */
    public function updateOrdersAdded($orders)
    {
        $max_order = 4;
        $count = 0;
        $sub_date = 0;

        foreach ($orders as $key => $order) {
            if ($order['status'] !== ModelSaleOrder::ORDER_STATUS_ID_CANCELLED && $order['status'] !== ModelSaleOrder::ORDER_STATUS_ID_DRAFT) {
                $date_time = date('Y-m-d h:00:00', (strtotime('-' . $sub_date . ' day', strtotime(date('Y-m-d h:i:s')))));
                $date = date('Y-m-d', strtotime($date_time));
            } else {
                $date_time = date('Y-m-d h:00:00', (strtotime('-' . rand(0, 14) . ' day', strtotime(date('Y-m-d h:i:s')))));
                $date = date('Y-m-d', strtotime($date_time));
            }

            $this->db->query("UPDATE " . DB_PREFIX . "order 
                          SET `date_added` = '" . $date_time . "', 
                          `date_modified` = '" . $date_time . "' 
                          WHERE order_id = '" . (int)$order['order_id'] . "'");

            $this->db->query("UPDATE " . DB_PREFIX . "report_order 
                          SET `report_time` = '" . $date_time . "', 
                          `report_date` = '" . $date . "' 
                          WHERE order_id = '" . (int)$order['order_id'] . "'");

            $this->db->query("UPDATE " . DB_PREFIX . "report_product 
                          SET `report_time` = '" . $date_time . "', 
                          `report_date` = '" . $date . "' 
                          WHERE order_id = '" . (int)$order['order_id'] . "'");

            if ($order['status'] !== ModelSaleOrder::ORDER_STATUS_ID_CANCELLED && $order['status'] !== ModelSaleOrder::ORDER_STATUS_ID_DRAFT) {
                $count++;
                if ($count == $max_order) {
                    $max_order = $max_order == 1 ? 1 : $max_order - 1;
                    $sub_date++;
                    $count = 0;
                }
            }
        }
    }

    /**
     * create bank transfer payment method
     */
    public function createPaymentMethod()
    {
        $this->load->model('setting/setting');

        $payment_methods = $this->model_setting_setting->getSettingValue('payment_methods');
        $payment_methods = json_decode($payment_methods, true);

        $payment_methods[] = [
            'payment_id' => self::BANK_TRANSFER_METHOD,
            'name' => 'Chuyển khoản',
            'guide' => '',
            'status' => '1'
        ];

        $this->model_setting_setting->editSettingValue('payment', 'payment_methods', $payment_methods);
    }

    /**
     * delete all demo data except product, category, collection
     */
    public function removeAllDataDemo()
    {
        $this->removeOrderAndReturnReceipt();
        $this->removeReport();
        $this->removeStoreAndReceipt();
        $this->removeCustomer();
        $this->removeBankTransferMethod();
        $this->removeCashFlow();
        $this->removeManufacturer();
        // TODO: remove removePromotion code
//        $this->removePromotion(); /* note remove discounts */
    }

    private function removePromotion() {
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "discount");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "discount_description");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "discount_to_store");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "order_to_discount");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "product_discount");
    }

    /**
     * remove manufacturers
     * @return bool
     */
    private function removeManufacturer()
    {
        // get all discounts
        $this->load->model('discount/discount');
        $this->load->model('discount/discount_type');
        $filter = [
            'discount_type_id' => ModelDiscountDiscountType::TYPE_MANUFACTURER,
            'included_statuses' => [1, 2] // active, scheduled
        ];
        $discounts = $this->model_discount_discount->getDiscountsCustom($filter);

        $manufacturer_ids = [];
        foreach ($discounts as $discount) {
            $config = json_decode($discount['config'], true);

            if (isset($config[0]['all_manufacturer']) && 1 == $config[0]['all_manufacturer']) {
                // not remove manufacturers
                return false;
            }

            if (isset($config[0]['manufacturer_id'])) {
                $manufacturer_ids[] = $config[0]['manufacturer_id'];
            }
        }

        $manufacturer_ids_query = $manufacturer_ids ? " AND `manufacturer_id` NOT IN (" . implode(',', $manufacturer_ids) . ") " : '';

        $DB_PREFIX = DB_PREFIX;
        // delete manufacturers
        $sql = "DELETE FROM `{$DB_PREFIX}manufacturer` WHERE 1 {$manufacturer_ids_query}";
        $this->db->query($sql);
        // delete manufacturer to store
        $sql = "DELETE FROM `{$DB_PREFIX}manufacturer_to_store` WHERE 1 {$manufacturer_ids_query}";
        $this->db->query($sql);

        return true;
    }

    private function removeBankTransferMethod()
    {
        $this->load->model('setting/setting');

        $payment_methods = $this->model_setting_setting->getSettingValue('payment_methods');
        $payment_methods = json_decode($payment_methods, true);

        $new_payment_method = [];
        foreach ($payment_methods as $payment_method) {
            if ($payment_method['payment_id'] !== self::BANK_TRANSFER_METHOD) {
                $new_payment_method[] = $payment_method;
            }
        }

        if ($new_payment_method == []) {
            $new_payment_method = json_decode('[{"payment_id":"198535319739A","name":"Thanh toán khi nhận hàng (COD)","guide":"Bạn sẽ thanh toán toàn bộ giá trị đơn hàng khi nhận hàng","status":"1"}]');
        }

        $this->model_setting_setting->editSettingValue('payment', 'payment_methods', $new_payment_method);
    }

    private function removeOrderAndReturnReceipt()
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "order WHERE 1");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "order_history");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "order_product");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "return_receipt");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "return_product");
    }

    private function removeReport()
    {
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "report_order");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "report_product");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "report");
    }

    private function removeStoreAndReceipt()
    {
        $this->db->query("DELETE from " . DB_PREFIX . "store WHERE `store_id` != 0");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "store_receipt");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "store_receipt_to_product");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "store_take_receipt");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "store_take_receipt_to_product");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "store_transfer_receipt");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "store_transfer_receipt_to_product");
    }

    private function removeCustomer()
    {
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "customer");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "customer_group");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "customer_group_description");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "address");
    }

    private function removeCashFlow()
    {
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "payment_voucher");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "receipt_voucher");
    }

    public function getOrderCanReturn()
    {
        $sql = "SELECT o.`order_id`, COUNT(op.`order_product_id`) as count_product FROM `" . DB_PREFIX . "order` o 
                                    LEFT JOIN `" . DB_PREFIX . "return_receipt` rr ON o.`order_id` = rr.`order_id` 
                                    LEFT JOIN `" . DB_PREFIX . "order_product` op ON o.`order_id` = op.`order_id` 
                                    WHERE o.`order_status_id` = " . ModelSaleOrder::ORDER_STATUS_ID_COMPLETED . " 
                                    AND rr.`return_receipt_id` is NULL 
                                    GROUP BY o.`order_id` 
                                    ORDER BY count_product DESC
                                    LIMIT 5 OFFSET 0";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * update store of a product (all versions)
     * @param $product_to_store
     * @param $store_id
     */
    public function updateProductStore($product_to_store, $store_id) {
        $DB_PREFIX = DB_PREFIX;
        // check if primary key exists
        $sql = "SELECT EXISTS (SELECT 1 FROM `{$DB_PREFIX}product_to_store` WHERE `product_id` = {$product_to_store['product_id']} AND `store_id` = {$store_id} AND `product_version_id` = {$product_to_store['product_version_id']}) result";
        $query = $this->db->query($sql);
        if (0 == $query->row['result']) {
            $sql = "UPDATE `{$DB_PREFIX}product_to_store` 
                    SET `store_id` = $store_id 
                    WHERE `product_id` = {$product_to_store['product_id']} 
                      AND `store_id` = {$product_to_store['store_id']} 
                      AND `product_version_id` = {$product_to_store['product_version_id']}";
            $this->db->query($sql);
        }
    }

    /**
     * get $limit random product to store records
     * @param $limit
     * @return mixed
     */
    public function getRandomProductToStore($limit) {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM `{$DB_PREFIX}product_to_store` ORDER BY RAND() LIMIT {$limit}";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * remove product to store records which store is not exists
     */
    public function removeNotExistsProductToStore() {
        $DB_PREFIX = DB_PREFIX;
        $sql = "DELETE FROM `{$DB_PREFIX}product_to_store` WHERE `store_id` NOT IN (SELECT `store_id` FROM `{$DB_PREFIX}store`)";
        $this->db->query($sql);
    }
}