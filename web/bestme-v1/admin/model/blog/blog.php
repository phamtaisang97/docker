<?php

class ModelBlogBlog extends Model
{
    use RabbitMq_Trait;

    CONST BLOG_IS_SHOW   = 1;
    CONST BLOG_IS_HIDDEN = 0;

    public function addBlog($data)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "blog 
                          SET demo = 0,
                          author = '" . $this->db->escape($data['author']) . "', 
                          status = '" . $this->db->escape($data['status']) . "', 
                          in_home = '" . $this->db->escape($data['in_home']) . "', 
                          approval_status = '" . $this->db->escape($data['approval_status']) . "', 
                          date_publish = NOW(), 
                          date_added = NOW(), 
                          date_modified = NOW()"
        );
        $blog_id = $this->db->getLastId();

        $language_id = (int)$this->config->get('config_language_id');
        $this->db->query("INSERT INTO " . DB_PREFIX . "blog_description 
                          SET blog_id = '" . $blog_id . "', 
                          language_id = '" . (int)$language_id . "', 
                          title = '" . $this->db->escape($data['title']) . "', 
                          content = '" . $this->db->escape($data['content']) . "', 
                          short_content = '" . $this->db->escape($data['short_content']) . "', 
                          image = '" . $this->db->escape($data['image']) . "',
                          alt = '" . $this->db->escape($data['alt']) . "', 
                          meta_title = '" . $this->db->escape($data['seo_name']) . "', 
                          meta_description = '" . $this->db->escape($data['body']) . "', 
                          alias = '" . $this->db->escape($data['alias']) . "',
                          seo_keywords = '" . $this->db->escape($data['seo_keywords']) . "',
                          type = '" . $this->db->escape($data['type']) . "',
                          video_url = '" . $this->db->escape($data['video_url']) . "'"
        );

        $this->db->query("INSERT INTO " . DB_PREFIX . "blog_to_blog_category 
                          SET  blog_id = '" . $blog_id . "', 
                          blog_category_id = '" . $this->db->escape($data['blog_category']) . "'"
        );

        // create seo
        $this->load->model('custom/common');

        $blog_alias = $data['title'];
        if (isset($data['alias']) && $data['alias'] != '') {
            $blog_alias = $data['alias'];
        }
        $slug = $this->model_custom_common->createSlug($blog_alias, '-');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug);
        $alias = 'blog=' . $blog_id;
        $this->db->query("UPDATE " . DB_PREFIX . "blog_description SET alias = '" . $this->db->escape($slug_custom) . "' WHERE blog_id = '" . (int)$blog_id . "'");
        $sql = "INSERT INTO " . DB_PREFIX . "seo_url 
                SET language_id = '" . (int)$language_id . "', 
                `query` = '" . $alias . "', 
                `keyword` = '" . $slug_custom . "', 
                `table_name` = 'blog'";
        $this->db->query($sql);

        if (isset($data['tag_list'])) {
            $tags_list = [];
            if (is_string($data['tag_list'])) {
                $tags_list = explode(',', $data['tag_list']);
            } elseif (is_array($data['tag_list'])) {
                $tags_list = $data['tag_list'];
            }

            foreach ($tags_list as $tag_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "blog_tag 
                                  SET blog_id = '" . (int)$blog_id . "', 
                                  tag_id = '" . (int)$tag_id . "'");
            }
        }
        $this->deleteBlogCaches();

        $this->sendBlogBrokerMessage($blog_id);

        return $blog_id;
    }

    public function getAllBlogs($data = array())
    {
        $sql = $this->getAllBlogsSql($data);

        // limit
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }
            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getListBlog($data = array())
    {
        $sql = $this->getAllBlogsSql($data);

        // limit
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }
            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getAllBlogFilter()
    {
        $language_id = (int)$this->config->get('config_language_id');
        $db_prefix = DB_PREFIX;
        $sql = "SELECT b.*, bd.*, u.`firstname`, u.`lastname` FROM `{$db_prefix}blog` b 
                LEFT JOIN `{$db_prefix}blog_description` bd ON (b.`blog_id` = bd.`blog_id`) 
                LEFT JOIN `{$db_prefix}user` u ON (b.`author` = u.`user_id`) 
                WHERE 1=1 AND bd.language_id = '" . $language_id . "'";

        $sql .= " ORDER BY b.blog_id";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalBlogs($data = [])
    {
        $sql = $this->getAllBlogsSql($data);

        $sql = "SELECT COUNT(1) as `total` FROM ($sql) as ab";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getBlog($blog_id)
    {
        $language_id = (int)$this->config->get('config_language_id');
        $sql = "SELECT b.*, bd.*, bcd.`title` blog_category_title FROM " . DB_PREFIX . "blog b 
                LEFT JOIN " . DB_PREFIX . "blog_description bd ON (b.blog_id = bd.blog_id) 
                LEFT JOIN " . DB_PREFIX . "blog_to_blog_category bc ON (b.blog_id = bc.blog_id) 
                LEFT JOIN " . DB_PREFIX . "blog_category_description bcd ON (bc.blog_category_id = bcd.blog_category_id) 
                WHERE b.blog_id = '" . (int)$blog_id . "' AND bd.language_id = '" . $language_id . "'";
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getBlogPaginator($data = array())
    {
        $sql = "SELECT blog_desc.blog_id, blog_desc.content, blog_desc.title, blog_desc.image 
                FROM " . DB_PREFIX . "blog blog
                    LEFT JOIN " . DB_PREFIX . "blog_description as blog_desc ON (blog.blog_id = blog_desc.blog_id) 
                WHERE 1=1 and blog.status='" . self::BLOG_IS_SHOW . "' and blog_desc.language_id = '" . (int)$this->config->get('config_language_id') . "'";
        if (!empty($data['filter_title'])) {
            $sql .= " AND blog_desc.title LIKE '%" . $this->db->escape($data['filter_title']) . "%'";
        }
        $sql .= " ORDER BY blog_desc.title ASC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function countBlogPaginator()
    {
        $sql = "SELECT count(blog.blog_id) as count_blog
                FROM " . DB_PREFIX . "blog blog
                    LEFT JOIN " . DB_PREFIX . "blog_description as blog_desc ON (blog.blog_id = blog_desc.blog_id) 
                WHERE 1=1 and blog.status='" . self::BLOG_IS_SHOW . "' and blog_desc.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (!empty($data['filter_title'])) {
            $sql .= " AND blog_desc.title LIKE '%" . $this->db->escape($data['filter_title']) . "%'";
        }
        $query = $this->db->query($sql);

        return (int)$query->rows[0]['count_blog'];
    }

    public function updateStatus($id, $status)
    {
        // get old blog
        $old_blog = $this->getBlog($id);
        if (!$old_blog || !isset($old_blog['blog_id']) || 'omihub' == $old_blog['source']) {
            return false;
        }

        $this->db->query("UPDATE " . DB_PREFIX . "blog SET status = " . $status . " WHERE blog_id = " . (int)$id . "");
        $this->deleteBlogCaches();
        return true;
    }

    public function updateApprovalStatus($id, $approvalStatus)
    {
        // get old blog
        $old_blog = $this->getBlog($id);
        if (!$old_blog || !isset($old_blog['blog_id']) || 'omihub' == $old_blog['source']) {
            return false;
        }

        $sql = "UPDATE " . DB_PREFIX . "blog SET approval_status = '" . $this->db->escape($approvalStatus) . "' WHERE blog_id = " . (int)$id . "";
        $this->db->query($sql);
        $this->deleteBlogCaches();
        return true;
    }

    public function updateSortOrder($id)
    {
        $result = $this->db->query("SELECT sort_order 
                                    FROM " . DB_PREFIX . "product_collection 
                                    WHERE product_collection_id = '" . (int)$id . "'");
        $order_sort = $result->row['sort_order'];
        if ($order_sort > 0) {
            $this->db->query("UPDATE " . DB_PREFIX . "product_collection SET sort_order = sort_order - 1 WHERE product_collection_id = " . (int)$id . "");
        }
        $this->deleteBlogCaches();
    }

    public function deleteBlog($blog_id)
    {
        // get old blog
        $old_blog = $this->getBlog($blog_id);
        if (!$old_blog || !isset($old_blog['blog_id']) || 'omihub' == $old_blog['source']) {
            return false;
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "blog WHERE blog_id = '" . (int)$blog_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'blog=" . (int)$blog_id . "'");
        //delete tag, collection
        $this->db->query("DELETE FROM " . DB_PREFIX . "blog_description WHERE blog_id = '" . (int)$blog_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "blog_tag WHERE blog_id = '" . (int)$blog_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "blog_to_blog_category WHERE blog_id = '" . (int)$blog_id . "'");
        $this->deleteBlogCaches();

        $this->sendBlogDeleteBrokerMessage($old_blog['title']);
    }

    public function editBlog($blog_id, $data)
    {
        // get old blog
        $old_blog = $this->getBlog($blog_id);
        if (!$old_blog || !isset($old_blog['blog_id']) || 'omihub' == $old_blog['source']) {
            return;
        }

        // update
        $language_id = $this->config->get('config_language_id');
        $this->db->query("UPDATE " . DB_PREFIX . "blog 
                          SET demo = 0,
                          author = '" . $this->db->escape($data['author']) . "', 
                          status = " . $data['status'] . ", 
                          in_home = " . $data['in_home'] . ", 
                          approval_status = '" . $this->db->escape($data['approval_status']) . "', 
                          date_modified = NOW() 
                          WHERE blog_id = '" . (int)$blog_id . "'");
        $this->db->query("UPDATE " . DB_PREFIX . "blog_to_blog_category 
                          SET blog_category_id = '" . $this->db->escape($data['blog_category']) . "' 
                          WHERE blog_id = " . (int)$blog_id . "");

        $this->db->query("DELETE FROM " . DB_PREFIX . "blog_tag WHERE blog_id = '" . (int)$blog_id . "'");
        if (isset($data['tag_list'])) {
            $tags_list = [];
            if (is_string($data['tag_list'])) {
                $tags_list = explode(',', $data['tag_list']);
            } elseif (is_array($data['tag_list'])) {
                $tags_list = $data['tag_list'];
            }

            foreach ($tags_list as $tag_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "blog_tag 
                                  SET blog_id = '" . (int)$blog_id . "', 
                                      tag_id = '" . (int)$tag_id . "'");
            }
        }

        // update seo
        $this->load->model('custom/common');
        $old_blog_alias = isset($old_blog['alias']) ? $old_blog['alias'] : '';
        $blog_alias = $old_blog_alias;
        if (isset($data['alias']) && $data['alias'] != '' && $data['alias'] != $old_blog_alias) {
            // get current seo url
            $DB_PREFIX = DB_PREFIX;
            $sql = "SELECT * FROM `{$DB_PREFIX}seo_url` WHERE `query` = 'blog={$blog_id}' LIMIT 1";
            $current_seo_url_query = $this->db->query($sql);

            $slug = $this->model_custom_common->createSlug($data['alias'], '-');
            $slug_custom = $this->model_custom_common->getSlugUnique($slug);
            $blog_alias = $slug_custom;

            // save product should change sitemap to session
            if ($current_seo_url_query->row) {
                $this->session->data['blog_' . $blog_id . '_change_sitemap'] = $current_seo_url_query->row['keyword'] != $slug_custom;
            }

            // delete seo if existing
            $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'blog=" . (int)$blog_id . "'");

            // create new seo
            $alias = 'blog=' . $blog_id;

            $this->db->query("UPDATE " . DB_PREFIX . "blog_description 
                              SET alias = '" . $this->db->escape($slug_custom) . "' 
                              WHERE blog_id = '" . (int)$blog_id . "'");

            $sqlseo = "INSERT INTO `" . DB_PREFIX . "seo_url` 
                       SET `language_id` = '" . (int)$language_id . "', 
                       `query` = '" . $alias . "', 
                       `keyword` = '" . $slug_custom . "', 
                       `table_name` = 'blog'";

            $this->db->query($sqlseo);
        }

        $this->db->query("UPDATE " . DB_PREFIX . "blog_description 
                          SET `title` = '" . $this->db->escape($data['title']) . "', 
                          `image` = '" . $this->db->escape($data['image']) . "', 
                          `alt` = '" . $this->db->escape($data['alt']) . "', 
                          `content` = '" . $this->db->escape($data['content']) . "', 
                          `short_content` = '" . $this->db->escape($data['short_content']) . "', 
                          `meta_title` = '" . $this->db->escape($data['seo_name']) . "', 
                          `meta_description` = '" . $this->db->escape($data['body']) . "', 
                          `alias` = '" . $this->db->escape($blog_alias) . "', 
                          `seo_keywords` = '" . $this->db->escape($data['seo_keywords']) . "', 
                          `type` = '" . $this->db->escape($data['type']) . "', 
                          `video_url` = '" . $this->db->escape($data['video_url']) . "' 
                          WHERE `blog_id` = " . (int)$blog_id . "");

        $this->deleteBlogCaches();

        $this->sendBlogBrokerMessage($blog_id, $old_blog['title']);
    }

    public function addOrderBy()
    {
        $sql = "SELECT MAX('order_sort') as order_sort FROM " . DB_PREFIX . "product_collection";
        $result = $this->db->query($sql);
        return $result->row['total'] + 1;
    }

    public function getNameRecordById($collection_id, $tableName, $name = null)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . $tableName . " WHERE collection_id = '" . (int)$collection_id . "'");
        if ($name) {
            if ($query->row[$name]) {
                return $query->row[$name];
            }
            return null;
        }
        if ($query->row['title']) {
            return $query->row['title'];
        }
        return null;
    }

    private function getAllBlogsSql($data = array())
    {
        $db_prefix = DB_PREFIX;
        $language_id = (int)$this->config->get('config_language_id');
        $sql = "SELECT b.*,bd.`language_id` ,bd.`title` ,bd.`content`,bd.`short_content` ,bd.`image` ,bd.`meta_description` ,bd.`alias`, bd.`source_author_name`,bc.`blog_category_id` ,
                       bc.`status` as blog_category_status, bcd.`title` as blog_category_title, u.`firstname`, u.`lastname` FROM `{$db_prefix}blog` b 
                LEFT JOIN `{$db_prefix}blog_description` bd ON (b.`blog_id` = bd.`blog_id`) 
                LEFT JOIN `{$db_prefix}user` u ON (b.`author` = u.`user_id`)
                LEFT JOIN `{$db_prefix}blog_to_blog_category` btbc ON (b.`blog_id` = btbc.`blog_id`)
                LEFT JOIN `{$db_prefix}blog_category` bc ON (bc.`blog_category_id` = btbc.`blog_category_id`)
                LEFT JOIN `{$db_prefix}blog_category_description` bcd ON (bc.`blog_category_id` = bcd.`blog_category_id`)
                ";

        if (isset($data['filter_tag']) && $data['filter_tag'] != '') {
            $sql .= " LEFT JOIN `{$db_prefix}blog_tag` bt ON (bt.`blog_id` = b.`blog_id`)";
            $sql .= " LEFT JOIN `{$db_prefix}tag` t ON (t.`tag_id` = bt.`tag_id`)";
        }

        $sql .= " WHERE 1=1 
                  AND bd.language_id = '" . $language_id . "' 
                  AND bcd.language_id = '" . $language_id . "' ";

        if (!empty($data['filter_name'])) {
            $sql .= " AND bd.`title` LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        // TODO: use array, do not set '' for multiple statuses...
        if (!empty($data['filter_status'])) {
            $order_statuses = explode(',', $data['filter_status']);
            $order_statuses = array_map(function ($blog_status_id) {
                return "'" . (int)$blog_status_id . "'";

            }, $order_statuses);

            $sql .= " AND b.`status` IN (" . implode(",", $order_statuses) . ")";
        } elseif (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $sql .= " AND b.`status` = '" . (int)$data['filter_status'] . "'";
        }

        // TODO: use this block code...
        /*if (!empty($data['filter_status']) && is_array($data['filter_status'])) {
            $order_statuses = array_map(function ($blog_status_id) {
                return "'" . (int)$blog_status_id . "'";

            }, $data['filter_status']);

            $sql .= " AND b.`status` IN (" . implode(", ", $order_statuses) . ")";
        }*/

        if (!empty($data['filter_author']) && is_array($data['filter_author'])) {
            $blog_authors = array_map(function ($blog_author_id) {
                return "'" . (int)$blog_author_id . "'";

            }, $data['filter_author']);

            $sql .= " AND b.`author` IN (" . implode(", ", $blog_authors) . ")";
        }

        if (!empty($data['filter_category']) && is_array($data['filter_category'])) {
            $blog_categories = array_map(function ($blog_catrgory_id) {
                return "'" . (int)$blog_catrgory_id . "'";

            }, $data['filter_category']);

            $sql .= " AND bc.`blog_category_id` IN (" . implode(", ", $blog_categories) . ")";
        }

        if (!empty($data['filter_approval_status']) && is_array($data['filter_approval_status'])) {
            $blog_approval_status = array_map(function ($approval_status) {
                return "'" . $approval_status . "'";

            }, $data['filter_approval_status']);

            $sql .= " AND b.`approval_status` IN (" .implode(", ", $blog_approval_status) . ")";
        }

        if (!empty($data['filter_tag']) && is_array($data['filter_tag'])) {
            $blog_tags = array_map(function ($blog_tag_value) {
                return "'" . $blog_tag_value . "'";

            }, $data['filter_tag']);

            $sql .= " AND t.`value` IN (" . implode(", ", $blog_tags) . ")";
        }

        $sql .= " ORDER BY b.`date_modified` DESC";

        // IMPORTANT: do not apply LIMIT here

        return $sql;
    }

    public function getDemoBlogIds()
    {
        $query = $this->db->query("SELECT b.blog_id FROM " . DB_PREFIX . "blog b WHERE b.demo = 1");

        return $query->rows;
    }

    public function deleteDemoBlogs()
    {
        $demo_blog_ids = $this->getDemoBlogIds();
        foreach ($demo_blog_ids as $demo_blog_id) {
            if (!array_key_exists('blog_id', $demo_blog_id)) {
                continue;
            }

            /* IMPORTANT: call this fn to remove all related data */
            $this->deleteBlog($demo_blog_id['blog_id']);
        }
    }

    public function getCategoriesAndParents($blog_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "blog_to_blog_category as ptc LEFT JOIN " . DB_PREFIX . "blog_category_description as cd ON cd.blog_category_id = ptc.blog_category_id WHERE blog_id = '" . (int)$blog_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        $list_categories = [];
        foreach ($query->rows as $row) {
            $parent_categories = $this->getAllParentsCategory($row['blog_category_id']);
            foreach ($parent_categories as $k => $category) {
                $list_categories[$k] = $category;
            }
        }

        $result = [];
        foreach ($list_categories as $category) {
            $result[] = [
                'id' => $category['blog_category_id'],
                'title' => $category['title'],
                'parent_id' => $category['parent_id']
            ];
        }

        return $result;
    }

    private function getAllParentsCategory($blog_id)
    {
        $result = [];
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "blog_category` as c LEFT JOIN `" . DB_PREFIX . "blog_category_description` as cd ON c.`blog_category_id` = cd.`blog_category_id` WHERE c.`blog_category_id` = " . (int)$blog_id);

        if ($query->num_rows <= 0) {
            return $result;
        }

        $result[$query->row['blog_category_id']] = $query->row;
        if ($query->row['parent_id'] != 0) {
            $parents = $this->getAllParentsCategory($query->row['parent_id']);
            foreach ($parents as $k => $v) {
                $result[$k] = $v;
            }
        }

        return $result;
    }

    /**
     * delete blog caches
     */
    private function deleteBlogCaches() {
        $cache_key_redis =  DB_PREFIX . 'blog';
        $this->cache->delete($cache_key_redis);
    }

    /**
     * @param $blog_id
     * @return mixed
     */
    private function getBlogData($blog_id) {
        $language_id = (int)$this->config->get('config_language_id');
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT b.date_modified, bd.*, u.firstname, u.lastname, u.email, bcd.title category_title, bcd.meta_title category_meta_title, bcd.meta_description category_meta_description, bcd.alias category_alias 
                FROM {$DB_PREFIX}blog b 
                LEFT JOIN {$DB_PREFIX}blog_description bd ON (b.blog_id = bd.blog_id) 
                LEFT JOIN {$DB_PREFIX}user u ON (b.author = u.user_id) 
                LEFT JOIN {$DB_PREFIX}blog_to_blog_category bc ON (b.blog_id = bc.blog_id) 
                LEFT JOIN {$DB_PREFIX}blog_category_description bcd ON (bc.blog_category_id = bcd.blog_category_id) 
                WHERE b.blog_id = '" . (int)$blog_id . "' AND bd.language_id = '" . $language_id . "' LIMIT 1";
        $query = $this->db->query($sql);

        return $query->row;
    }

    /**
     * send blog message to RabbitMQ
     *
     * @param $blog_id
     * @param string $old_blog_title
     * @param string $old_blog_category_title
     */
    private function sendBlogBrokerMessage($blog_id, $old_blog_title = '', $old_blog_category_title = '')
    {
        try {
            if (!empty($blog_id)) {
                $blog = $this->getBlogData($blog_id);
                if (!empty($blog)) {
                    $message = [
                        'shop_name' => $this->config->get('shop_name'),
                        'blog' => [
                            'title' => $blog['title'],
                            'old_title' => $old_blog_title,
                            'content' => $blog['content'],
                            'short_content' => $blog['short_content'],
                            'author' => [
                                'firstname' => isset($blog['firstname']) ? $blog['firstname'] : '',
                                'lastname' => isset($blog['lastname']) ? $blog['lastname'] : '',
                                'email' => isset($blog['email']) ? $blog['email'] : ''
                            ],
                            'source_author_name' => $blog['source_author_name'],
                            'image' => $blog['image'],
                            'meta_title' => $blog['meta_title'],
                            'meta_description' => $blog['meta_description'],
                            'seo_keywords' => $blog['seo_keywords'],
                            'alias' => $blog['alias'],
                            'alt' => $blog['alt'],
                            'type' => $blog['type'],
                            'video_url' => $blog['video_url'],
                            'blog_date_modify' => $blog['date_modified'],
                            'category' => [
                                'title' => $blog['category_title'],
                                'old_title' => $old_blog_category_title,
                                'meta_title' => $blog['category_meta_title'],
                                'meta_description' => $blog['category_meta_description'],
                                'alias' => $blog['category_alias']
                            ]
                        ]
                    ];

                    $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'blog', '', 'blog', json_encode($message));
                } else {
                    $this->log->write('Send message blog error: Could not get blog data for blog id ' . $blog_id);
                }
            } else {
                $this->log->write('Send message blog error: Missing blog_id ');
            }
        } catch (Exception $exception) {
            $this->log->write('Send message blog error: ' . $exception->getMessage());
        }
    }

    /**
     * Send blog delete message to RabbitMQ
     *
     * @param $blog_id
     * @param string $old_blog_title
     * @param string $old_blog_category_title
     */
    private function sendBlogDeleteBrokerMessage($title, $action='delete') {
        try {
            if ($title) {
                $message = [
                    'shop_name' => $this->config->get('shop_name'),
                    'title' => $title,
                    'action' => $action
                ];
                $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'modify_blog', '', 'modify_blog', json_encode($message));
            }
        }catch (Exception $exception) {
            $this->log->write('Send message blog delete error: ' . $exception->getMessage());
        }
    }
}
