<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;

class ModelReportProduct extends Model
{
    //report by product
    public function getReportByProducts($filter)
    {
        $this->load->model('catalog/product');
        $sql = "SELECT rp.product_id, rp.product_version_id, 
                sum(rp.quantity) as quantity, 
                sum(rp.quantity_return) as quantity_return, 
                sum(rp.quantity * rp.price) as total_amount, 
                sum(rp.return_amount) as return_amount, 
                sum(rp.discount) as discount,
                if(p.multi_versions, CONCAT(pd.name, ' - ', pv.version), pd.name) as name,
                if(p.multi_versions, pv.sku, p.sku) as sku
        FROM  " . DB_PREFIX . "report_product as rp
        LEFT JOIN " . DB_PREFIX . "product_version pv on rp.product_version_id = pv.product_version_id
        LEFT JOIN " . DB_PREFIX . "product p on p.product_id = rp.product_id
        LEFT JOIN " . DB_PREFIX . "product_description pd on pd.product_id = rp.product_id
        WHERE rp.report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'";
        if (isset($filter['store_id']) && $filter['store_id'] > -1) {
            $sql .= ' AND rp.store_id = ' . $filter['store_id'];
        }
        if (isset($filter['start_limit']) || isset($filter['end_limit'])) {
            if ($filter['start_limit'] < 0) {
                $filter['start_limit'] = 0;
            }

            if ($filter['end_limit'] < 1) {
                $filter['end_limit'] = 20;
            }

            $sql .= " GROUP BY if(rp.product_version_id = 0, rp.product_id, rp.product_version_id) " . " LIMIT " . (int)$filter['start_limit'] . "," . (int)$filter['end_limit'];
        }
        $query = $this->db->query($sql);
        $results = $query->rows;
        $json_data = [];
        foreach ($results as $result) {
            // because return data in report product is fail by time return. Temporary, get data on return receipt table
            $return_quantity = 0;
            $return_amount = 0;
            if (isset($result['product_id']) && isset($result['product_version_id'])){
                $return_info = $this->getReturnInfoByProduct($result['product_id'], $result['product_version_id'], $filter);
                $return_quantity = isset($return_info['return_quantity']) ? (int)$return_info['return_quantity'] : 0;
                $return_amount = isset($return_info['return_amount']) ? (float)$return_info['return_amount'] : 0;
            }
            $json_data[] = [
                'sku' => $result['sku'],
                'product_name' => $result['name'],
                'quantity' => $result['quantity'],
                'quantity_return' => $return_quantity,
                'total_amount' => $result['total_amount'],
                'return_amount' => $return_amount,
                'discount' => $result['discount'],
            ];
        }

        return $json_data;
    }

    private function getReturnInfoByProduct($product_id, $product_version_id, $filter)
    {
        if ($filter['end']){
            $filter['end'] = $filter['end'] . ' 23:59:59';
        }

        $sql = "SELECT SUM(rp.`quantity`) as `return_quantity`, SUM(rp.`price` * rp.quantity) as `return_amount` 
                FROM `" . DB_PREFIX . "return_product` rp LEFT JOIN `" . DB_PREFIX . "return_receipt` rr ON rr.`return_receipt_id` = rp.`return_receipt_id` 
                WHERE rp.`product_id` = " . (int)$product_id ." AND rp.`product_version_id` = " . (int)$product_version_id . " AND 
                      rr.`date_added` BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'";

//        if (isset($filter['store_id']) && $filter['store_id'] > -1) { // TODO revert later, now default store_id = 0
//            $sql .= " AND rr.`store_id` = " . $filter['store_id'];
//        }

        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getTotalNumberReportByProducts($data = array())
    {
        $sql = "SELECT 1 as total
        FROM " . DB_PREFIX . "report_product as rp 
        WHERE rp.report_date BETWEEN '" . $data['start'] . "' AND '" . $data['end'] . "'";
        if (isset($data['store_id']) && $data['store_id'] > -1) {
            $sql .= ' AND rp.store_id = ' . $data['store_id'];
        }
        $sql .= " GROUP BY if(rp.product_version_id = 0, rp.product_id, rp.product_version_id) ";
        $sql_ = ' select sum(total) as total FROM ('.$sql.') as rp';
        $query = $this->db->query($sql_);
        return $query->row['total'];
    }

    public function getTotalReportByProducts($filter)
    {
        $sql = "SELECT SUM(rp.quantity) as quantity_all, 
                       SUM(rp.quantity_return) as quantity_return_all, 
                       SUM(rp.quantity * rp.price) as total_amount_all, 
                       SUM(rp.return_amount) as return_amount_all, 
                       SUM(rp.discount) as discount_all 
                       FROM " . DB_PREFIX . "report_product as rp 
                            WHERE rp.report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'";

        if (isset($filter['store_id']) && $filter['store_id'] > -1) {
            $sql .= ' AND rp.store_id = ' . $filter['store_id'];
        }
        $query = $this->db->query($sql);

        $result = $query->row;
        // because return data in report product is fail by time return. Temporary, get data on return receipt table
        $return_info = $this->getReturnInfoByAllProduct($filter);
        $result['quantity_return_all'] = isset($return_info['return_quantity']) ? (int)$return_info['return_quantity'] : $result['quantity_return_all'];
        $result['return_amount_all'] = isset($return_info['return_amount']) ? (float)$return_info['return_amount'] : $result['return_amount_all'];

        return $result;
    }

    private function getReturnInfoByAllProduct($filter)
    {
        if ($filter['end']){
            $filter['end'] = $filter['end'] . ' 23:59:59';
        }

        $sql = "SELECT SUM(rp.`quantity`) as `return_quantity`, SUM(rp.`price` * rp.`quantity`) as `return_amount` 
                FROM `" . DB_PREFIX . "return_product` rp 
                    LEFT JOIN `" . DB_PREFIX . "return_receipt` rr ON rr.`return_receipt_id` = rp.`return_receipt_id` 
                WHERE rr.`date_added` BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'";

//        if (isset($filter['store_id']) && $filter['store_id'] > -1) { // TODO revert later, now default store_id = 0
//            $sql .= ' AND rr.`store_id` = ' . $filter['store_id'];
//        }

        $query = $this->db->query($sql);

        return $query->row;
    }

    public function export($data)
    {
        $this->load->language('report/report');
        $report_by_products = $data['report_by_products'];
        foreach ($report_by_products as $value) {
            $result[] = array(
                "SKU" => $value['sku'],
                "{$this->language->get('text_product')}"         => $value['product_name'],
                "{$this->language->get('text_quantity_sold')}"   => $value['quantity'],
                "{$this->language->get('text_amount')}"          => $value['total_amount'],
                "{$this->language->get('text_discount')}"        => $value['discount'],
                "{$this->language->get('text_quantity_return')}" => $value['quantity_return'],
                "{$this->language->get('text_return_amount')}"   => $value['return_amount'],
                "{$this->language->get('text_revenue')}"         => $value['revenue']
            );
        }

        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $activeSheet = $spreadsheet->getActiveSheet();
        $activeSheet->setTitle("{$this->language->get('text_sales_report_by_product')}");
        $activeSheet->setCellValue('C1', "{$this->language->get('text_sales_report_by_product')}");
        $activeSheet->setCellValue('D2', "{$this->language->get('text_store')} :");
        $activeSheet->setCellValue('E2', $data['store_name']);
        $activeSheet->setCellValue('D3', "{$this->language->get('text_time')}");
        $activeSheet->setCellValue('E3', $data['time']);
        $activeSheet->getStyle("C1")->getFont()->setSize(16);
        $activeSheet->getStyle("D3")->getFont()->setBold(true);
        $activeSheet->getStyle("D2")->getFont()->setBold(true);

        //output headers
        $activeSheet->fromArray(array_keys($result[0]), NULL, 'A4');

        $styleArrayHeader = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'bbbdbd'],
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'cfe2f3',
                ],
                'endColor' => [
                    'argb' => 'cfe2f3',
                ],
            ],
        ];
        $styleColumn      = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'bbbdbd'],
                ],
            ],
        ];
        $styleArrayFooter = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'bbbdbd'],
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'fce5cd',
                ],
                'endColor' => [
                    'argb' => 'fce5cd',
                ],
            ],
        ];

        $style_align_right = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            ],
        ];
        $style_align_left  = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ],
        ];

        $activeSheet->getStyle('A4:H4')->applyFromArray($styleArrayHeader);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(50);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(20);

        $spreadsheet->getActiveSheet()->getRowDimension('4')->setRowHeight(40);

        $rowCount = 5;
        foreach ($result as $key => $row) {
            $activeSheet->setCellValue('A' . $rowCount, $row['SKU']);
            $activeSheet->SetCellValue('B' . $rowCount, $row["{$this->language->get('text_product')}"]);
            $activeSheet->SetCellValue('C' . $rowCount, $row["{$this->language->get('text_quantity_sold')}"]);
            $activeSheet->SetCellValue('D' . $rowCount, $row["{$this->language->get('text_amount')}"]);
            $activeSheet->SetCellValue('E' . $rowCount, $row["{$this->language->get('text_discount')}"]);
            $activeSheet->SetCellValue('F' . $rowCount, $row["{$this->language->get('text_quantity_return')}"]);
            $activeSheet->SetCellValue('G' . $rowCount, $row["{$this->language->get('text_return_amount')}"]);
            $activeSheet->SetCellValue('H' . $rowCount, $row["{$this->language->get('text_revenue')}"]);
            $activeSheet->getStyle('A' . $rowCount . ':B' . $rowCount)->applyFromArray(array_merge($styleColumn, $style_align_left));
            $activeSheet->getStyle('C' . $rowCount . ':H' . $rowCount)->applyFromArray(array_merge($styleColumn, $style_align_right));
            $rowCount++;
        }
        $activeSheet->setCellValue('A' . $rowCount, "{$this->language->get('text_total')}");
        $activeSheet->SetCellValue('B' . $rowCount, '  ');
        $activeSheet->SetCellValue('C' . $rowCount, $data['quantity_all']);
        $activeSheet->SetCellValue('D' . $rowCount, $data['total_amount_all']);
        $activeSheet->SetCellValue('E' . $rowCount, $data['discount_all']);
        $activeSheet->SetCellValue('F' . $rowCount, $data['quantity_return_all']);
        $activeSheet->SetCellValue('G' . $rowCount, $data['return_amount_all']);
        $activeSheet->SetCellValue('H' . $rowCount, $data['revenue_all']);
        $activeSheet->getStyle('A' . $rowCount . ':B' . $rowCount)->applyFromArray(array_merge($styleArrayFooter, $style_align_left));
        $activeSheet->getStyle('C' . $rowCount . ':H' . $rowCount)->applyFromArray(array_merge($styleArrayFooter, $style_align_right));
        /*
        * write output for downloading...
        * https://stackoverflow.com/questions/27701981/phpexcel-download-using-ajax-call
        */
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        ob_start();
        $writer->save("php://output"); // for test: DIR_APPLICATION . "products.xlsx"
        $xlsxData = ob_get_contents();
        ob_end_clean();

        return 'data:application/vnd.ms-excel;base64,' . base64_encode($xlsxData);
    }

    public function updateProductReport($order_id, $args)
    {
        $total_amount_order = 0;

        foreach ($args['product_id'] as $key => $product_id) {
            // tổng tiền trả của đơn hàng
            $return_prices =  (int)str_replace(',', '', $args['return_prices'][$key]);
            $total_amount_order += $return_prices * $args['product_return_quantity'][$key];
        }

       foreach ($args['product_id'] as $key => $product_id) {

           // số lượng trả
           $return_quantity = $args['product_return_quantity'][$key];

           //giá hàng trả
           $return_prices =  (int)str_replace(',', '', $args['return_prices'][$key]);

           //tiền trả hàng 1 sản phẩm
           $return_amount_a_product = $return_quantity * $return_prices;

           //tỷ lệ giá trị
           $value_ratio = $total_amount_order ? ($return_amount_a_product)/($total_amount_order) : 0;

           //phí trả hàng
           $return_fee = (int)str_replace(',', '', $args['return_fee']);

           $return_amount = $return_amount_a_product - ($return_fee * $value_ratio);

           $this->db->query("UPDATE " . DB_PREFIX . "report_product 
                            SET quantity_return = quantity_return + " . $return_quantity . ",
                                return_amount = return_amount + ". $return_amount ." 
                            WHERE product_id = ". (int)$product_id . " AND order_id = " . (int)$order_id);
       }
    }

}
