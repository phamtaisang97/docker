<?php
class ModelReportWebTraffic extends Model {

    public function getTotalWebTraffic($filter)
    {
        $today = date("Y-m-d");
        $today_time = strtotime($today);
        $end_time = strtotime($filter['end']);
        $sql = "SELECT SUM(`value`) AS total FROM `" . DB_PREFIX . "report_web_traffic` WHERE `report_time` BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'";
        $query = $this->db->query($sql);
        $json_data['total'] = (int)$query->row['total'];
        if($end_time >= $today_time) {
            $task = BESTME_WEB_TRAFFICT_REDIS;
            $key = (DB_PREFIX.'traffic');

            $redis = $this->redisConnect();
            $check_traffict = $redis->hGet($task, $key);
            $traffic_today = 0;
            if($check_traffict) {
                $data = json_decode($check_traffict);
                $traffic_today = $data->count;
            }
            $json_data['total'] += $traffic_today;
        }
        return $json_data;
    }

    public function getConversionRate($filter)
    {
        // count customer
        $sql = "SELECT COUNT(DISTINCT o.customer_id ) AS total FROM `" . DB_PREFIX . "report_order` as o WHERE o.order_status IN('7', '8', '9', '10') AND `report_time` BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'";
        $query = $this->db->query($sql);
        $totalCustomer =  (int)$query->row['total'];


        $totalWebtraffic =  $this->getTotalWebTraffic($filter)['total'];
        if($totalWebtraffic == 0){
            $json_data['rate'] = 0;
        }else{
            $json_data['rate'] = 100 * $totalCustomer / $totalWebtraffic;
        }



        return $json_data;
    }

    private function redisConnect()
    {
        $redis = null;

        try {
            $redis = new \Redis();
            $redis->connect(MUL_REDIS_HOST, MUL_REDIS_PORT);
            $redis->select(MUL_REDIS_DB_BESTME_COUNT_TRAFFICT);
        } catch (Exception $e) {
            // could not connect to redis
        }

        return $redis;
    }

    /**
     * @param Redis $redis
     */
    private function redisClose($redis)
    {
        if (!$redis instanceof \Redis) {
            return;
        }

        try {
            $redis->close();
        } catch (Exception $e) {
            // could not connect to redis
        }
    }
}