<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;

class ModelReportStaff extends Model
{
    private $filter_order_status = [7, 8, 9];

    //report by product
    public function getReportByStaffs($filter)
    {
        $this->load->model('catalog/product');

        $sql = "SELECT 
                    ro.user_id as user_id, 
                    CONCAT(u.lastname,' ',u.firstname) AS name,
                    COUNT(ro.order_id) AS count_order,
                    SUM(ro.total_amount + ro.discount - ro.shipping_fee) AS total_amount,
                    SUM(ro.discount) AS discount,
                    SUM(ro.shipping_fee) AS shipping_fee,
                    SUM(ro.total_return) AS total_return,
                    SUM(ro.total_amount - ro.total_return) AS revenue
                FROM " . DB_PREFIX . "report_order ro
                LEFT JOIN " . DB_PREFIX . "user u on u.user_id = ro.user_id
                WHERE ro.report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'
                AND ro.order_status IN (" . implode(',', $this->filter_order_status) . ")";

        if (isset($filter['store_id']) && $filter['store_id'] > 0) {
            $sql .= ' AND ro.store_id = ' . $filter['store_id'];
        }

        $sql .= " GROUP BY ro.user_id";
        if (isset($filter['start_limit']) || isset($filter['end_limit'])) {
            if ($filter['start_limit'] < 0) {
                $filter['start_limit'] = 0;
            }

            if ($filter['end_limit'] < 1) {
                $filter['end_limit'] = 20;
            }
            $sql .= " LIMIT " . (int)$filter['start_limit'] . "," . (int)$filter['end_limit'];
        }

        $query   = $this->db->query($sql);
        $results = $query->rows;

        // because return data in report order is fail by time return. Temporary, get data on return receipt table
        foreach ($results as &$result) {
            if (isset($result['user_id'])) {
                $return_amount = $this->getReturnAmountByStaff($result['user_id'], $filter);
                $result['revenue'] = $result['revenue'] + $result['total_return'] - $return_amount;
                $result['total_return'] = $return_amount;
            }
        }

        return $results;
    }

    private function getReturnAmountByStaff($user_id, $filter)
    {
        if ($filter['end']){
            $filter['end'] = $filter['end'] . ' 23:59:59';
        }

        $sql = "SELECT SUM(rr.`total`) as `return_amount` 
                FROM `" . DB_PREFIX . "return_receipt` rr 
                    LEFT JOIN `" . DB_PREFIX . "report_order` ro ON ro.`order_id` = rr.`order_id` 
                WHERE ro.`user_id` = " . (int)$user_id . " AND 
                      rr.`date_added` BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'";

        if (isset($filter['store_id']) && $filter['store_id'] !== '' && $filter['store_id'] > -1) {
            $sql .= " AND ro.`store_id` = " . $filter['store_id'];
        }

        $query = $this->db->query($sql);

        return isset($query->row['return_amount']) ? (float)$query->row['return_amount'] : 0;
    }

    public function getTotalNumberReportByStaffs($data = array())
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT COUNT(*) as total
                FROM (SELECT ro.user_id
                    FROM {$DB_PREFIX}report_order as ro 
                    WHERE ro.report_date BETWEEN '" . $data['start'] . "' AND '" . $data['end'] . "'
                    AND ro.order_status IN (" . implode(',', $this->filter_order_status) . ")";

        if (isset($data['store_id']) && $data['store_id'] > 0) {
            $sql .= ' AND ro.store_id = ' . $data['store_id'];
        }

        $sql   .= " GROUP BY ro.user_id) AS report_order_by_staff";
        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getOverviewReport($filter)
    {
        $sql = "SELECT 
                    CONCAT(u.lastname,' ',u.firstname) AS name,
                    COUNT(ro.order_id) AS count_order,
                    SUM(ro.total_amount + ro.discount - ro.shipping_fee) AS total_amount,
                    SUM(ro.discount) AS discount,
                    SUM(ro.shipping_fee) AS shipping_fee,
                    SUM(ro.total_return) AS total_return,
                    SUM(ro.total_amount - ro.total_return) AS revenue
                FROM " . DB_PREFIX . "report_order ro
                LEFT JOIN " . DB_PREFIX . "user u on u.user_id = ro.user_id
                WHERE ro.report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'
                AND ro.order_status IN (" . implode(',', $this->filter_order_status) . ")";

        if (isset($filter['store_id']) && $filter['store_id'] > 0) {
            $sql .= ' AND ro.store_id = ' . $filter['store_id'];
        }

        $query = $this->db->query($sql);

        $result = $query->row;
        // because return data in report order is fail by time return. Temporary, get data on return receipt table
        $return_amount = $this->getReturnByAllStaffs($filter);
        $result['revenue'] = $result['revenue'] + $result['total_return'] - $return_amount;
        $result['total_return'] = $return_amount;

        return $result;
    }

    private function getReturnByAllStaffs($filter)
    {
        if ($filter['end']){
            $filter['end'] = $filter['end'] . ' 23:59:59';
        }
        $sql = "SELECT SUM(rr.`total`) as `return_amount` 
                FROM `" . DB_PREFIX . "return_receipt` rr 
                    LEFT JOIN `" . DB_PREFIX . "report_order` ro ON ro.`order_id` = rr.`order_id` 
                WHERE rr.`date_added` BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'";

        if (isset($filter['store_id']) && $filter['store_id'] !== '' && $filter['store_id'] > -1) {
            $sql .= " AND ro.`store_id` = " . $filter['store_id'];
        }

        $query = $this->db->query($sql);

        return isset($query->row['return_amount']) ? (float)$query->row['return_amount'] : 0;
    }

    public function export($data)
    {
        $this->load->language('report/staff');
        $report_by_staffs = $data['report_by_staffs'];

        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $activeSheet = $spreadsheet->getActiveSheet();
        $activeSheet->setTitle($this->language->get('text_export_title'));
        $activeSheet->setCellValue('C1', $this->language->get('text_export_title'));
        $activeSheet->setCellValue('D2', $this->language->get('text_export_store'));
        $activeSheet->setCellValue('E2', $data['store_name']);
        $activeSheet->setCellValue('D3', $this->language->get('text_export_time'));
        $activeSheet->setCellValue('E3', $data['time']);
        $activeSheet->getStyle("C1")->getFont()->setSize(16);
        $activeSheet->getStyle("D3")->getFont()->setBold(true);
        $activeSheet->getStyle("D2")->getFont()->setBold(true);

        $headers = [
            $this->language->get('text_staff_name'),
            $this->language->get('text_total_order'),
            $this->language->get('text_amount'),
            $this->language->get('text_discount'),
            $this->language->get('text_shipping_fee'),
            $this->language->get('text_return_amount'),
            $this->language->get('text_revenue'),
        ];
        //output headers
        $activeSheet->fromArray($headers, NULL, 'A4');
        $styleArrayHeader = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'bbbdbd'],
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'cfe2f3',
                ],
                'endColor' => [
                    'argb' => 'cfe2f3',
                ],
            ],
        ];
        $styleColumn      = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'bbbdbd'],
                ],
            ],
        ];
        $styleArrayFooter = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'bbbdbd'],
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'fce5cd',
                ],
                'endColor' => [
                    'argb' => 'fce5cd',
                ],
            ],
        ];

        $style_align_right = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            ],
        ];
        $style_align_left  = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ],
        ];

        $activeSheet->getStyle('A4:G4')->applyFromArray($styleArrayHeader);
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(50);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(20);

        $spreadsheet->getActiveSheet()->getRowDimension('4')->setRowHeight(40);

        $rowCount = 5;
        foreach ($report_by_staffs as $key => $row) {
            $activeSheet->setCellValue('A' . $rowCount, $row['name']);
            $activeSheet->SetCellValue('B' . $rowCount, $row['count_order']);
            $activeSheet->SetCellValue('C' . $rowCount, number_format($row['total_amount'], 0, '', ','));
            $activeSheet->SetCellValue('D' . $rowCount, number_format($row['discount'], 0, '', ','));
            $activeSheet->SetCellValue('E' . $rowCount, number_format($row['shipping_fee'], 0, '', ','));
            $activeSheet->SetCellValue('F' . $rowCount, number_format($row['total_return'], 0, '', ','));
            $activeSheet->SetCellValue('G' . $rowCount, number_format($row['revenue'], 0, '', ','));
            $activeSheet->getStyle('A' . $rowCount)->applyFromArray(array_merge($styleColumn, $style_align_left));
            $activeSheet->getStyle('B' . $rowCount . ':G' . $rowCount)->applyFromArray(array_merge($styleColumn, $style_align_right));
            $rowCount++;
        }
        $activeSheet->setCellValue('A' . $rowCount, $this->language->get('text_total'));
        $activeSheet->SetCellValue('B' . $rowCount, $data['overview']['count_order']);
        $activeSheet->SetCellValue('C' . $rowCount, $data['overview']['total_amount']);
        $activeSheet->SetCellValue('D' . $rowCount, $data['overview']['discount']);
        $activeSheet->SetCellValue('E' . $rowCount, $data['overview']['shipping_fee']);
        $activeSheet->SetCellValue('F' . $rowCount, $data['overview']['total_return']);
        $activeSheet->SetCellValue('G' . $rowCount, $data['overview']['revenue']);
        $activeSheet->getStyle('A' . $rowCount)->applyFromArray(array_merge($styleArrayFooter, $style_align_left));
        $activeSheet->getStyle('B' . $rowCount . ':G' . $rowCount)->applyFromArray(array_merge($styleArrayFooter, $style_align_right));
        /*
        * write output for downloading...
        * https://stackoverflow.com/questions/27701981/phpexcel-download-using-ajax-call
        */
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        ob_start();
        $writer->save("php://output"); // for test: DIR_APPLICATION . "products.xlsx"
        $xlsxData = ob_get_contents();
        ob_end_clean();

        return 'data:application/vnd.ms-excel;base64,' . base64_encode($xlsxData);
    }
}
