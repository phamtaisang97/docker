<?php

class ModelReportOrderProduct extends Model {

    public function saveOrderProductReport($data)
    {
        $user_id = $this->user->getId();
        $store_id = 0;

        $sql_get_cost_price = "SELECT pts.cost_price AS cost_price                                                       
                    FROM ". DB_PREFIX . "product p                                                                         
                    LEFT JOIN ". DB_PREFIX . "product_to_store pts ON p.product_id = pts.product_id AND p.default_store_id = pts.store_id 
                    WHERE p.product_id='".(int)$data['product_id']."'
                    AND if(p.multi_versions = 1, pts.product_version_id, 1) = if(p.multi_versions = 1, ".(int)$data['product_version_id'].", 1)";
        $query = $this->db->query($sql_get_cost_price);
        $cost_price = isset($query->rows[0]) ? $query->rows[0]['cost_price'] : 0;

        $discount = isset($data['discount']) ? $data['discount'] : 0;

        $exits = $this->checkExistReportProduct($data['order_id'], $data['product_id'], $data['product_version_id']);
        if ($exits) {
            $this->updateOrderProductReport($data);
            return;
        }

        $sql = "INSERT INTO ". DB_PREFIX . "report_product SET report_time = '". $data['report_time'] ."'";
        $sql .= ", report_date = '" . $data['report_date'] . "'";
        $sql .= ", order_id = '" . $data['order_id'] . "'";
        $sql .= ", product_id = '" . $data['product_id'] . "'";
        $sql .= ", product_version_id = '" . $data['product_version_id'] . "'";
        $sql .= ", quantity = '" . $data['quantity'] . "'";
        $sql .= ", price = '" . $data['price'] . "'";
        $sql .= ", total_amount = '" . $data['total_amount'] . "'";
        $sql .= ", discount = '" . $discount . "'";
        $sql .= ", store_id = '" . $store_id . "'";
        $sql .= ", cost_price = '" . $cost_price . "'";
        $sql .= ", user_id = '" . (int)$user_id . "'";
        $this->db->query($sql);
    }

    public function updateOrderProductReport($data)
    {
        $sql = "UPDATE ". DB_PREFIX . "report_product SET ";
        $sql .= "product_id = '" . $data['product_id'] . "'";
        $sql .= ", product_version_id = '" . $data['product_version_id'] . "'";
        $sql .= ", quantity = '" . $data['quantity'] . "'";
        $sql .= ", price = '" . $data['price'] . "'";
        $sql .= ", discount = '" . $data['discount'] . "'";
        $sql .= ", total_amount = '" . $data['total_amount'] . "'";
        $sql .= " WHERE order_id = '" . $data['order_id'] . "'";
        $sql .= " AND product_id = '" . $data['product_id'] ."'";
        $sql .= " AND product_version_id = '" . (int)$data['product_version_id'] ."'";
        $this->db->query($sql);
    }

    public function deleteProductInOrderReport($orderId)
    {
        $sql = sprintf("DELETE FROM %s 
                        WHERE order_id = '%d'",
            DB_PREFIX . "report_product",
            $orderId
        );
        $this->db->query($sql);
    }

    /* Get top revenue product by period */
    public function getTopRevenueProduct($filter = null)
    {
        $sql = "SELECT orp.`product_id`, p.`deleted` as p_deleted, orp.`total_amount`,
                CASE WHEN (p.multi_versions > 0) THEN (CASE WHEN (pv.price > 0) THEN pv.price ELSE pv.compare_price END) ELSE (CASE WHEN (p.price > 0) THEN p.price ELSE p.compare_price END) END as price
                , SUM(orp.`total_amount`) as `total`, pd.`name`, p.`image`, pv.`version`, pv.`deleted` as pv_deleted,
                CASE WHEN (p.`multi_versions` = 1 AND ( orp.`product_version_id` = NULL OR orp.`product_version_id` = 0)) THEN 1 ELSE 0 END as is_changed 
                FROM `" . DB_PREFIX . "report_product` orp
                LEFT JOIN `" . DB_PREFIX . "product_description` pd ON orp.`product_id` = pd.`product_id`
                INNER JOIN `" . DB_PREFIX . "product` p ON orp.`product_id` = p.`product_id`
                LEFT JOIN `" . DB_PREFIX . "product_version` pv ON orp.`product_id` = pv.`product_id` AND orp.`product_version_id` = pv.`product_version_id`
                WHERE `report_date` BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'";
        if (isset($filter['user_id'])){
            $sql .= " AND `user_id` = " . $filter['user_id'];
        }
        if (isset($filter['store_id'])){
            $sql .= " AND `store_id` = " . $filter['store_id'];
        }
        $sql .= " GROUP BY orp.`product_id`, orp.`product_version_id` ORDER BY `total` DESC LIMIT 5";

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function updateReportProductPrice($product_id, $product_version_id, $price) {
        $sql = "UPDATE ". DB_PREFIX . "report_product SET price ='". $price ."' ";
        $sql .= "WHERE product_id ='". (int)$product_id ."' ";
        if ($product_version_id != 0) {
            $sql .= "AND product_version_id ='". (int)$product_version_id ."'";
        }
        $this->db->query($sql);
    }

    /**
     * @param $order_id
     * @param $product_id
     * @param int $product_version_id
     * @return bool
     */
    private function checkExistReportProduct($order_id, $product_id, $product_version_id)
    {
        if (!$order_id || !$product_id) {
            return false;
        }

        $product_version_id = !empty($product_version_id)? $product_version_id : 0;

        $DB_PREFIX = DB_PREFIX;
        $query = $this->db->query("SELECT `order_id` FROM `{$DB_PREFIX}report_product` 
                                      WHERE `order_id` = {$order_id} 
                                        AND `product_id` = {$product_id} 
                                        AND `product_version_id` = {$product_version_id}");

        if (isset($query->row['order_id']) && !empty($query->row['order_id'])) {
            return true;
        }

        return false;
    }
}