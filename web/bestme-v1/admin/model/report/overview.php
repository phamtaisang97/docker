<?php

class ModelReportOverview extends Model
{
    private $sources = ['shop', 'admin', 'pos', 'shopee', 'lazada', 'social'];
    private $filter_order_status = [7, 8, 9];
    CONST TOP_SELL_LIMIT = 5;
    const REPORT_TYPE_REVENUE = 1;
    const REPORT_TYPE_ORDERS = 2;
    const REPORT_TYPE_CUSTOMERS = 3;
    const REPORT_TYPE_TOP_SALE_PRODUCTS = 4;
    const REPORT_TYPE_WEB_TRAFFIC = 5;

    public function getRevenue($filter = null, $type)
    {
        $this->load->model('sale/order');
        $date1 = new DateTime($filter['start']);
        $date2 = new DateTime($filter['end']);
        $interval = $date1->diff($date2);
        $filter_order_status = [
            ModelSaleOrder::ORDER_STATUS_ID_PROCESSING,
            ModelSaleOrder::ORDER_STATUS_ID_DELIVERING,
            ModelSaleOrder::ORDER_STATUS_ID_COMPLETED
        ];

        $DB_PREFIX = DB_PREFIX;
        $sqlContainer = "SELECT DISTINCT * FROM {$DB_PREFIX}report_order  
                         WHERE report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "' 
                            AND `order_status` IN (" . implode(',', $filter_order_status) . ")";
        if (isset($filter['user_id'])){
            $sqlContainer .= " AND `user_id` = " . $filter['user_id'];
        }
        if (isset($filter['store_id'])){
            $sqlContainer .= " AND `store_id` = " . $filter['store_id'];
        }

        if ($interval->days < 1) {
            $type_format_time = 1;
            $sql = "SELECT DISTINCT report_time as result_label, 
                           SUM(total_amount) as total_value 
                    FROM ($sqlContainer) as report_product 
                    WHERE report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "' 
                    GROUP BY report_time 
                    ORDER BY report_time ASC";
            $list_times = $this->getListHours($filter);
        } else if ($interval->days >= 1 && $interval->days <= 120) {
            $type_format_time = 2;
            $sql = "SELECT report_date as result_label, 
                    SUM(total_amount) as total_value 
                    FROM ($sqlContainer) as report_product 
                    WHERE  report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "' 
                    GROUP BY report_date 
                    ORDER BY report_date ASC";
            $list_times = $this->getListDay($filter);
        } else {
            $type_format_time = 3;
            $sql = "SELECT LEFT(report_date, 7) as result_label , 
                    SUM(total_amount) as total_value 
                    FROM ($sqlContainer) as report_product 
                    WHERE report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "' 
                    GROUP BY result_label 
                    ORDER BY report_date ASC";
            $list_times = $this->getListMonth($filter);
        }

        $query = $this->db->query($sql);
        $results = $query->rows;

        // get data
        foreach ($list_times as $list_time) {
            $json_data['content'][] = [
                'label' => $this->formatDate($list_time, $type_format_time),//$this->getLabelData($filter_labels, ),
                //'value' => $this->converData($this->getValueData($results, $list_time)),
                'value' => $this->getValueData($results, $list_time),
            ];
        }

        // get total
        $total = $this->currency->format($this->getTotalOrder($filter), $this->config->get('config_currency'));
        $search = [',', 'đ'];
        $replace = ['.', ''];
        $json_data['total'] = str_replace($search, $replace, $total);

        /** getHighestOder **/
        $json_data['config_chart'] = $this->getHighestOder($filter['start'], $filter['end']);

        return $json_data;
    }

    public function getRevenueAndProfitChartData($filter) {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT ro.report_date as label,
                -- revenue = quantity * price of product 
                 SUM(ro.total_amount + ro.discount - ro.shipping_fee - ro.total_return) as revenue,
                 SUM(ro.total_amount + ro.discount - ro.shipping_fee - ro.total_return) - sum(rp.cost_price) as profit
                FROM {$DB_PREFIX}report_order as ro
                LEFT JOIN (
                    SELECT order_id, SUM(cost_price * (quantity - quantity_return)) AS cost_price
                    from {$DB_PREFIX}report_product
                    GROUP BY order_id
                ) as rp on ro.order_id = rp.order_id
                WHERE ro.report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'
                    AND ro.order_status IN (" . implode(',', $this->filter_order_status) . ")";

//        if(!$this->user->isAdmin() && !$this->user->canAccessAll()) {
//            $sql .= " AND ro.user_id = '" . (int)$this->user->getId() . "'";
//        }
//        if (isset($filter['store']) && $filter['store'] !== '' && ($this->user->isUserInStore($filter['store']) || $this->user->isAdmin() || $this->user->canAccessAll())) {
//            $sql .= " AND ro.store_id = '" . (int)$filter['store'] . "'";
//        } else {
//            $sql .= " AND ro.store_id in (0,". implode(',', $this->user->getUserStores()) .")";
//        }

        if (isset($filter['store']) && $filter['store'] !== '') {
            $sql .= " AND ro.store_id = '" . (int)$filter['store'] . "'";
        }

        $sql .= " GROUP BY label 
                ORDER BY ro.report_date ASC";

        $query = $this->db->query($sql);
        $results = $query->rows;

        $list_times = $this->getListDay($filter);
        $json_data = [];

        foreach ($list_times as $list_time) {
            $key = array_search($list_time, array_column($results, 'label'));
            $json_data[] = [
                'label' => $this->formatDate($list_time, 2),
                'revenue' => $key !== false ? $results[$key]['revenue'] : 0,
                'profit' => $key !== false && $results[$key]['profit'] ? $results[$key]['profit'] : 0
            ];
        }

        return $json_data;
    }

    public function getTopBestSellCustomer($filter)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT
                 -- revenue = quantity * price of product 
                SUM(ro.total_amount + ro.discount - ro.shipping_fee - ro.total_return) AS total_amount,
                c.customer_id, CONCAT(c.lastname, ' ', c.firstname) AS name,
                c.telephone
                FROM {$DB_PREFIX}report_order as ro
                LEFT JOIN {$DB_PREFIX}customer AS c ON c.customer_id = ro.customer_id
                WHERE ro.report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'
                AND ro.order_status IN (" . implode(',', $this->filter_order_status) . ")";

        if (isset($filter['store']) && $filter['store'] !== '') {
            $sql .= " AND ro.store_id = '" . (int)$filter['store'] . "'";
        }


        $sql .= " GROUP BY customer_id
                ORDER BY total_amount DESC
                LIMIT " . self::TOP_SELL_LIMIT;

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getTopBestSellStaff($filter)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT u.user_id,
                -- revenue = quantity * price of product 
                SUM(ro.total_amount + ro.discount - ro.shipping_fee - ro.total_return) AS total_amount,
                CONCAT(u.lastname, ' ', u.firstname) AS name,
                u.email
                FROM {$DB_PREFIX}report_order as ro
                LEFT JOIN {$DB_PREFIX}user AS u ON u.user_id = ro.user_id
                WHERE ro.report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'
                AND ro.order_status IN (" . implode(',', $this->filter_order_status) . ")";

        if (isset($filter['store']) && $filter['store'] !== '') {
            $sql .= " AND ro.store_id = '" . (int)$filter['store'] . "'";
        }

        $sql .= " GROUP BY ro.user_id
                ORDER BY total_amount DESC
                LIMIT " . self::TOP_SELL_LIMIT;

        $query = $this->db->query($sql);
        return $query->rows;
    }


    public function getRevenueFromSource($filter)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT ro.source as source,
                -- revenue = quantity * price of product 
                SUM(ro.total_amount + ro.discount - ro.shipping_fee - ro.total_return) AS total_amount
                FROM {$DB_PREFIX}report_order as ro
                WHERE ro.report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'
                  AND ro.order_status IN (" . implode(',', $this->filter_order_status) . ")";

        if (isset($filter['store']) && $filter['store'] !== '') {
            $sql .= " AND ro.store_id = '" . (int)$filter['store'] . "'";
        }

        $sql .= " GROUP BY ro.source
                ORDER BY ro.source ASC";

        $query = $this->db->query($sql);
        $results = $query->rows;
        return $this->formatPercentNumbers($results, array_sum(array_column($results, 'total_amount')));
    }

    public function getProfitFromSource($filter)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT ro.source AS source,
                -- revenue = quantity * price of product 
                sum(ro.total_amount + ro.discount - ro.shipping_fee - ro.total_return) - sum(rp.cost_price) AS total_amount
                FROM {$DB_PREFIX}report_order as ro
                LEFT JOIN (
                     SELECT order_id, SUM(cost_price * (quantity - quantity_return)) AS cost_price
                     from {$DB_PREFIX}report_product
                     GROUP BY order_id
                ) as rp on ro.order_id = rp.order_id
                WHERE ro.report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'
                  AND ro.order_status IN (" . implode(',', $this->filter_order_status) . ")";

        if (isset($filter['store']) && $filter['store'] !== '') {
            $sql .= " AND ro.store_id = '" . (int)$filter['store'] . "'";
        }

        $sql .= " GROUP BY ro.source
                ORDER BY ro.source ASC";

        $query = $this->db->query($sql);
        $results = $query->rows;
        return $this->formatPercentNumbers($results, array_sum(array_column($results, 'total_amount')));
    }

    /**
     * @param $arr
     * @param $total
     * @return mixed
     */
    private function formatPercentNumbers($arr, $total)
    {
        $result = [];

        if (0 < $total) {
            $total_percent = 0;

            foreach ($this->sources as $key => $source) {
                $arr_key = array_search($source, array_column($arr, 'source'));

                if ($key < count($this->sources) - 1) {
                    // calculate percent for all items but the last one
                    $percent = $arr_key !== false ? (double)number_format($arr[$arr_key]['total_amount'] / $total * 100, 2) : 0;
                    $total_percent += $percent;
                } else {
                    // calculate last item
                    $percent = (double)number_format((100 - $total_percent), 2);
                }

                $result[] = [
                    'percent' => $percent,
                    'source' => $source,
                    'total_amount' => ($arr_key !== false && $arr[$arr_key]['total_amount']) ? $arr[$arr_key]['total_amount'] : 0
                ];
            }
        }

        return $result;
    }

    private function getListDay($filter)
    {
        $start = (new DateTime($filter['start']));
        $end = (new DateTime($filter['end']))->modify('+1 day');
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($start, $interval, $end);
        $list = [];
        foreach ($period as $dt) {
            array_push($list, $dt->format("Y-m-d"));
        }

        return $list;
    }

    private function getListMonth($filter)
    {
        $start = (new DateTime($filter['start']));
        $end = (new DateTime($filter['end']));
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);
        $list = [];
        foreach ($period as $dt) {
            array_push($list, $dt->format("Y-m"));
        }

        return $list;
    }

    public function getSumCostPrice($filter)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT sum(rp.cost_price * (rp.quantity - rp.quantity_return)) AS sum_cost_price
                FROM {$DB_PREFIX}report_order ro
                LEFT JOIN {$DB_PREFIX}report_product rp ON ro.order_id = rp.order_id
                WHERE ro.report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'
                 AND ro.order_status IN (" . implode(',', $this->filter_order_status) . ")";

        if (isset($filter['store']) && $filter['store'] !== '') {
            $sql .= " AND ro.store_id = '" . (int)$filter['store'] . "'";
        }

        $query = $this->db->query($sql);
        $results = $query->rows;
        if (!$results) {
            return 0;
        }
        return $results[0]['sum_cost_price'];
    }

    public function getCountOrder($filter)
    {
        $DB_PREFIX = DB_PREFIX;
        $sqlContainer = "SELECT DISTINCT * FROM {$DB_PREFIX}report_order
                         WHERE report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'
                            AND `order_status` IN (" . implode(',', $this->filter_order_status) . ")";

        if (isset($filter['store']) && $filter['store'] !== '') {
            $sqlContainer .= " AND store_id = '" . (int)$filter['store'] . "'";
        }

        $sql = "SELECT COUNT(order_id) as count_order 
                FROM ($sqlContainer) as report_product
                WHERE  report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'";
        $query = $this->db->query($sql);
        $results = $query->rows;
        if (!$results) {
            return 0;
        }

        return $results[0]['count_order'];
    }

    public function getTotalOrder($filter)
    {
        $DB_PREFIX = DB_PREFIX;

        $sql = "SELECT SUM(ro.total_amount + ro.discount - ro.shipping_fee - ro.total_return) as total_value
                -- revenue = quantity * price of product 
                FROM {$DB_PREFIX}report_order ro
                WHERE ro.report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'
                    AND ro.order_status IN (" . implode(',', $this->filter_order_status) . ")";

        if (isset($filter['store']) && $filter['store'] !== '') {
            $sql .= " AND ro.store_id = '" . (int)$filter['store'] . "'";
        }

        $query = $this->db->query($sql);
        $result = $query->row;
        if (!$result) {
            return 0;
        }

        return $result['total_value'] ? $result['total_value']: 0;
    }

    private function formatDate($string, $type_format_time = 1)
    {
        if ($type_format_time == 1) {
            return date("H", strtotime($string)) . 'h';
        }

        if ($type_format_time == 2) {
            return date("d/m/Y", strtotime($string));
        }

        if ($type_format_time == 3) {
            return date("m/Y", strtotime($string));
        }
    }

    //report by product
    public function getReportByProducts($filter)
    {
        $this->load->model('catalog/product');
        $sql = "SELECT rp.product_id, rp.quantity, rp.quantity_return, rp.total_amount, ro.total_return, ro.discount
        FROM " . DB_PREFIX . "report_order as ro, " . DB_PREFIX . "report_product as rp 
        WHERE  ro.order_id = rp.order_id AND ro.report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'";
        if (isset($filter['store_id']) && $filter['store_id'] > 0) {
            $sql .= ' AND ro.store_id = ' . $filter['store_id'];
        }
        if (isset($filter['start_limit']) || isset($filter['end_limit'])) {
            if ($filter['start_limit'] < 0) {
                $filter['start_limit'] = 0;
            }

            if ($filter['end_limit'] < 1) {
                $filter['end_limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$filter['start_limit'] . "," . (int)$filter['end_limit'];
        }
        $query = $this->db->query($sql);
        $results = $query->rows;
        $json_data = [];
        foreach ($results as $result) {
            $product = $this->model_catalog_product->getProduct($result['product_id'], $flag_deleted = null);
            $json_data[] = [
                'sku' => isset($product['sku']) ? $product['sku'] : '',
                'product_name' => isset($product['name']) ? $product['name'] : '',
                'quantity' => $result['quantity'],
                'quantity_return' => $result['quantity_return'],
                'total_amount' => $result['total_amount'],
                'total_return' => $result['total_return'],
                'discount' => $result['discount'],
            ];
        }

        return $json_data;
    }

    public function getTotalReportByProducts()
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "report_order` ro");
        return $query->row['total'];
    }

    public function getTotalAllReportByProduct($filter)
    {
        $sql = "SELECT SUM(rp.quantity) as quantity_all, SUM(rp.quantity_return) as quantity_return_all,
        SUM(rp.total_amount) as total_amount_all, SUM(ro.total_return) as total_return_all, SUM(ro.discount) as discount_all
        FROM " . DB_PREFIX . "report_order as ro, " . DB_PREFIX . "report_product as rp 
        WHERE  ro.order_id = rp.order_id AND ro.report_date BETWEEN '" . $filter['start'] . "' AND '" . $filter['end'] . "'";


        if (isset($filter['store_id']) && $filter['store_id'] > 0) {
            $sql .= ' AND ro.store_id = ' . $filter['store_id'];
        }

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getAllStore()
    {
        $sql = "SELECT s.name, s.store_id FROM " . DB_PREFIX . "store as s";
        $query = $this->db->query($sql);
        $results = $query->rows;
        return $results;
    }

    public function increaseOrDecreaseOrdersReport($data, $increase = true)
    {
        try {
            $sql = "SELECT * FROM " . DB_PREFIX . "report WHERE `type`=" . self::REPORT_TYPE_ORDERS . " AND `report_time` = '" . $data['report_time'] . "'";
            $query = $this->db->query($sql);
            if ($query->row) {
                $sql = "UPDATE `" . DB_PREFIX . "report` SET `m_value` = `m_value` " . ($increase ? '+' : '-') . " '" . $data['m_value'] . "', `report_time` = '" . $data['report_time'] . "' WHERE `type`=" . self::REPORT_TYPE_ORDERS . " AND `report_time` = '" . $data['report_time'] . "'";
            } else {
                $sql = "INSERT INTO " . DB_PREFIX . "report SET report_time = '" . $data['report_time'] . "', type='" . self::REPORT_TYPE_ORDERS . "', type_description= 'Số lượng đơn', m_value= '" . $data['m_value'] . "', last_updated = NOW(), report_date = '" . $data['report_date'] . "'";
            }

            $this->db->query($sql);
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    public function increaseOrDecreaseRevenue($data, $increase = true)
    {
        try {
            $sql = "SELECT * FROM " . DB_PREFIX . "report WHERE `type`=" . self::REPORT_TYPE_REVENUE . " AND `report_time` = '" . $data['report_time'] . "'";
            $query = $this->db->query($sql);
            if ($query->row) {
                $sql = "UPDATE `" . DB_PREFIX . "report` SET `m_value` = `m_value` " . ($increase ? '+' : '-') . " '" . $data['m_value'] . "', `report_time` = '" . $data['report_time'] . "' WHERE `type`=" . self::REPORT_TYPE_REVENUE . " AND `report_time` = '" . $data['report_time'] . "'";
            } else {
                $sql = "INSERT INTO " . DB_PREFIX . "report SET report_time = '" . $data['report_time'] . "', type='" . self::REPORT_TYPE_REVENUE . "', type_description= 'Doanh thu', m_value= '" . $data['m_value'] . "', last_updated = NOW(), report_date = '" . $data['report_date'] . "'";
            }

            $this->db->query($sql);
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    private function getValueData($results, $filter_date)
    {
        foreach ($results as $result) {
            if ($result['result_label'] == $filter_date) {
                return $result['total_value'];
            }
        }

        return 0;
    }

    private function getHighestOder($start, $end)
    {
        $sql = "SELECT  SUM(m_value) as total_value FROM " . DB_PREFIX . "report WHERE report_date BETWEEN '" . $start . "' AND '" . $end . "' GROUP BY report_date limit 01";
        $query = $this->db->query($sql);
        $results = $query->rows;
        if (!empty($results) && $results[0]['total_value'] > 100) {
            $data['config_chart'] = floor($results[0]['total_value'] / 5);
        } else {
            $data['config_chart'] = 20;
        }

        return $data['config_chart'];
    }

    private function getListHours($filter)
    {
        $start = (new DateTime($filter['start']));
        $end = (new DateTime($filter['end']));
        $interval = DateInterval::createFromDateString('1 hours');
        $period = new DatePeriod($start, $interval, $end);
        $list = [];
        foreach ($period as $dt) {
            array_push($list, $dt->format("Y-m-d H:00:00"));
        }

        return $list;
    }
}
