<?php
/**
 * Created by PhpStorm.
 * User: Microsoft Windows
 * Date: 11/11/2020
 * Time: 1:51 PM
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;

class ModelReportStore extends Model
{
    public function getDataByStore($filter)
    {
        $sql = "SELECT `store_id` as `store_id`,
                       COUNT(DISTINCT `order_id`) as count_order, 
                       SUM(`total_amount` + `discount`) as amount,
                       SUM(`discount`) as discount, 
                       SUM(`shipping_fee`) as shipping_fee,
                       SUM(`total_return`) as total_return   
                       FROM `" . DB_PREFIX . "report_order` WHERE `order_status` IN (7,8,9) ";

        if (!empty($filter['start_time']) && !empty($filter['end_time'])) {
            $sql .= " AND `report_time` BETWEEN '{$filter['start_time']}' AND '{$filter['end_time']}'";
        }

        $sql .= " GROUP BY `store_id`";
        $sql .= " ORDER BY `store_id` ASC";

        if (isset($filter['start']) || isset($filter['limit'])) {
            if ($filter['start'] < 0) {
                $filter['start'] = 0;
            }

            if ($filter['limit'] < 1) {
                $filter['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$filter['start'] . "," . (int)$filter['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getReturnReceiptsDataByStore($store_id, $start, $end)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT COUNT(order_id) as count_return, SUM(total) as total_return FROM `{$DB_PREFIX}return_receipt` 
                  WHERE `store_id` = '{$store_id}' AND `date_added` BETWEEN '{$start}' AND '{$end}'";
        $query = $this->db->query($sql);

        if ($query->num_rows > 0) {
            return $query->row;
        }

        return [];
    }

    public function getReturnReceiptsData($start, $end)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT COUNT(order_id) as count_return, SUM(total) as total_return FROM `{$DB_PREFIX}return_receipt` 
                  WHERE `date_added` BETWEEN '{$start}' AND '{$end}'";
        $query = $this->db->query($sql);

        if ($query->num_rows > 0) {
            return $query->row;
        }

        return [];
    }

    public function getListStoreReturnReceiptByDate($start, $end)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT distinct `store_id` FROM `{$DB_PREFIX}return_receipt` 
                  WHERE `date_added` BETWEEN '{$start}' AND '{$end}'";
        $query = $this->db->query($sql);

        $result = [];
        foreach ($query->rows as $row){
            if (!isset($row['store_id'])){
                continue;
            }
            $result[] = $row['store_id'];
        }

        return $result;
    }

    public function getDataAllStores($filter)
    {
        $sql = "SELECT COUNT(DISTINCT `store_id`) as count_store,
                       COUNT(DISTINCT `order_id`) as count_order, 
                       SUM(`total_amount` + `discount`) as amount,
                       SUM(`discount`) as discount, 
                       SUM(`shipping_fee`) as shipping_fee,
                       SUM(`total_return`) as total_return   
                       FROM `" . DB_PREFIX . "report_order` WHERE `order_status` IN (7,8,9) ";

        if (!empty($filter['start_time']) && !empty($filter['end_time'])) {
            $sql .= " AND `report_time` BETWEEN '{$filter['start_time']}' AND '{$filter['end_time']}'";
        }

        $query = $this->db->query($sql);

        return $query->row;
    }

    public function allStoresExport($data)
    {
        $this->load->language('report/store');
        $report_by_stories = $data['report_by_stories'];
        foreach ($report_by_stories as $value){
            $result[] = array(
                "{$this->language->get('text_column_store')}"                    => $value['store_name'],
                "{$this->language->get('text_column_order_buy')}"               => $value['count_order'],
                "{$this->language->get('text_column_total_amount')}"            => $value['amount'],
                "{$this->language->get('text_column_discount')}"                => $value['discount'],
                "{$this->language->get('text_column_shipping_fee')}"            => $value['shipping_fee'],
                "{$this->language->get('text_column_order_return')}"            => $value['count_order_return'],
                "{$this->language->get('text_column_money_back')}"              => $value['total_return'],
                "{$this->language->get('text_column_revenue')}"                 => $value['revenue'],
            );
        }

        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $activeSheet = $spreadsheet->getActiveSheet();
        $activeSheet->setTitle("{$this->language->get('heading_title')}");
        $activeSheet->setCellValue('C1', "{$this->language->get('heading_title')}");
        $activeSheet->setCellValue('D2', "{$this->language->get('text_column_date')}");
        $activeSheet->setCellValue('E2', $data['time']);
        $activeSheet->getStyle("C1")->getFont()->setSize(16);
        $activeSheet->getStyle("D2")->getFont()->setBold(true);

        //output headers
        $activeSheet->fromArray(array_keys($result[0]), NULL, 'A3');

        $styleArrayHeader = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'bbbdbd'],
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'cfe2f3',
                ],
                'endColor' => [
                    'argb' => 'cfe2f3',
                ],
            ],
        ];
        $styleColumn      = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'bbbdbd'],
                ],
            ],
        ];
        $styleArrayFooter = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'bbbdbd'],
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'fce5cd',
                ],
                'endColor' => [
                    'argb' => 'fce5cd',
                ],
            ],
        ];

        $style_align_right = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            ],
        ];
        $style_align_left  = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ],
        ];

        $activeSheet->getStyle('A3:H3')->applyFromArray($styleArrayHeader);
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(20);

        $spreadsheet->getActiveSheet()->getRowDimension('3')->setRowHeight(30);

        $rowCount = 4;
        foreach ($result as $key => $row) {
            $activeSheet->setCellValue('A' . $rowCount, $row["{$this->language->get('text_column_store')}"]);
            $activeSheet->SetCellValue('B' . $rowCount, $row["{$this->language->get('text_column_order_buy')}"]);
            $activeSheet->SetCellValue('C' . $rowCount, $row["{$this->language->get('text_column_total_amount')}"]);
            $activeSheet->SetCellValue('D' . $rowCount, $row["{$this->language->get('text_column_discount')}"]);
            $activeSheet->SetCellValue('E' . $rowCount, $row["{$this->language->get('text_column_shipping_fee')}"]);
            $activeSheet->SetCellValue('F' . $rowCount, $row["{$this->language->get('text_column_order_return')}"]);
            $activeSheet->SetCellValue('G' . $rowCount, $row["{$this->language->get('text_column_money_back')}"]);
            $activeSheet->SetCellValue('H' . $rowCount, $row["{$this->language->get('text_column_revenue')}"]);
            $activeSheet->getStyle('A' . $rowCount . ':A' . $rowCount)->applyFromArray(array_merge($styleColumn, $style_align_left));
            $activeSheet->getStyle('B' . $rowCount . ':H' . $rowCount)->applyFromArray(array_merge($styleColumn, $style_align_right));
            $rowCount++;
        }


        $activeSheet->setCellValue('A' . $rowCount, "{$this->language->get('text_total')}");
        $activeSheet->SetCellValue('B' . $rowCount, $data['all_stores']['count_order']);
        $activeSheet->SetCellValue('C' . $rowCount, $data['all_stores']['amount']);
        $activeSheet->SetCellValue('D' . $rowCount, $data['all_stores']['discount']);
        $activeSheet->SetCellValue('E' . $rowCount, $data['all_stores']['shipping_fee']);
        $activeSheet->SetCellValue('F' . $rowCount, $data['all_stores']['count_order_return']);
        $activeSheet->SetCellValue('G' . $rowCount, $data['all_stores']['total_return']);
        $activeSheet->SetCellValue('H' . $rowCount, $data['all_stores']['revenue']);
        $activeSheet->getStyle('A' . $rowCount . ':A' . $rowCount)->applyFromArray(array_merge($styleArrayFooter, $style_align_left));
        $activeSheet->getStyle('B' . $rowCount . ':H' . $rowCount)->applyFromArray(array_merge($styleArrayFooter, $style_align_right));

        /*
         * write output for downloading...
         * https://stackoverflow.com/questions/27701981/phpexcel-download-using-ajax-call
         */
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        ob_start();
        $writer->save("php://output"); // for test: DIR_APPLICATION . "products.xlsx"
        $xlsxData = ob_get_contents();
        ob_end_clean();

        return 'data:application/vnd.ms-excel;base64,' . base64_encode($xlsxData);
    }

    public function storeDetailExport($data)
    {
        $this->load->language('report/store');
        $report_by_orders = $data['report_by_orders'];
        foreach ($report_by_orders as $value){
            $result[] = array(
                "{$this->language->get('text_column_date')}"                    => $value['report_date'],
                "{$this->language->get('text_column_order_buy')}"               => $value['total_order'],
                "{$this->language->get('text_column_total_amount')}"            => $value['total_amount'],
                "{$this->language->get('text_column_discount')}"                => $value['discount'],
                "{$this->language->get('text_column_shipping_fee')}"            => $value['shipping_fee'],
                "{$this->language->get('text_column_order_return')}"            => $value['order_return'],
                "{$this->language->get('text_column_money_back')}"              => $value['total_return'],
                "{$this->language->get('text_column_revenue')}"                 => $value['revenue'],
            );
        }

        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $activeSheet = $spreadsheet->getActiveSheet();
        $activeSheet->setTitle("{$this->language->get('heading_title')}");
        $activeSheet->setCellValue('C1', "{$this->language->get('heading_title')}");
        $activeSheet->setCellValue('D2', "{$this->language->get('text_column_store')}");
        $activeSheet->setCellValue('E2', $data['store_name']);
        $activeSheet->setCellValue('D3', "{$this->language->get('text_column_date')}");
        $activeSheet->setCellValue('E3', $data['time']);
        $activeSheet->getStyle("C1")->getFont()->setSize(16);
        $activeSheet->getStyle("D3")->getFont()->setBold(true);
        $activeSheet->getStyle("D2")->getFont()->setBold(true);

        //output headers
        $activeSheet->fromArray(array_keys($result[0]), NULL, 'A4');

        $styleArrayHeader = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'bbbdbd'],
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'cfe2f3',
                ],
                'endColor' => [
                    'argb' => 'cfe2f3',
                ],
            ],
        ];
        $styleColumn      = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'bbbdbd'],
                ],
            ],
        ];
        $styleArrayFooter = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'bbbdbd'],
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'fce5cd',
                ],
                'endColor' => [
                    'argb' => 'fce5cd',
                ],
            ],
        ];

        $style_align_right = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            ],
        ];
        $style_align_left  = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ],
        ];

        $activeSheet->getStyle('A4:H4')->applyFromArray($styleArrayHeader);
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(20);

        $spreadsheet->getActiveSheet()->getRowDimension('4')->setRowHeight(40);

        $rowCount = 5;
        foreach ($result as $key => $row) {
            $activeSheet->setCellValue('A' . $rowCount, $row["{$this->language->get('text_column_date')}"]);
            $activeSheet->SetCellValue('B' . $rowCount, $row["{$this->language->get('text_column_order_buy')}"]);
            $activeSheet->SetCellValue('C' . $rowCount, $row["{$this->language->get('text_column_total_amount')}"]);
            $activeSheet->SetCellValue('D' . $rowCount, $row["{$this->language->get('text_column_discount')}"]);
            $activeSheet->SetCellValue('E' . $rowCount, $row["{$this->language->get('text_column_shipping_fee')}"]);
            $activeSheet->SetCellValue('F' . $rowCount, $row["{$this->language->get('text_column_order_return')}"]);
            $activeSheet->SetCellValue('G' . $rowCount, $row["{$this->language->get('text_column_money_back')}"]);
            $activeSheet->SetCellValue('H' . $rowCount, $row["{$this->language->get('text_column_revenue')}"]);
            $activeSheet->getStyle('A' . $rowCount . ':A' . $rowCount)->applyFromArray(array_merge($styleColumn, $style_align_left));
            $activeSheet->getStyle('B' . $rowCount . ':H' . $rowCount)->applyFromArray(array_merge($styleColumn, $style_align_right));
            $rowCount++;
        }


        $activeSheet->setCellValue('A' . $rowCount, "{$this->language->get('text_total')}");
        $activeSheet->SetCellValue('B' . $rowCount, $data['total_all']['total_order']);
        $activeSheet->SetCellValue('C' . $rowCount, $data['total_all']['total_amount']);
        $activeSheet->SetCellValue('D' . $rowCount, $data['total_all']['total_discount']);
        $activeSheet->SetCellValue('E' . $rowCount, $data['total_all']['total_shipping_fee']);
        $activeSheet->SetCellValue('F' . $rowCount, $data['total_all']['total_order_return']);
        $activeSheet->SetCellValue('G' . $rowCount, $data['total_all']['total_return']);
        $activeSheet->SetCellValue('H' . $rowCount, $data['total_all']['total_revenue']);
        $activeSheet->getStyle('A' . $rowCount . ':A' . $rowCount)->applyFromArray(array_merge($styleArrayFooter, $style_align_left));
        $activeSheet->getStyle('B' . $rowCount . ':H' . $rowCount)->applyFromArray(array_merge($styleArrayFooter, $style_align_right));

        /*
         * write output for downloading...
         * https://stackoverflow.com/questions/27701981/phpexcel-download-using-ajax-call
         */
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        ob_start();
        $writer->save("php://output"); // for test: DIR_APPLICATION . "products.xlsx"
        $xlsxData = ob_get_contents();
        ob_end_clean();

        return 'data:application/vnd.ms-excel;base64,' . base64_encode($xlsxData);
    }
}