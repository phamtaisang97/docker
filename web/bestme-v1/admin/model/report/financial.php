<?php
/**
 * Created by PhpStorm.
 * User: Microsoft Windows
 * Date: 10/11/2020
 * Time: 1:08 PM
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;

class ModelReportFinancial extends Model
{
    public function getOrderReport($filter)
    {
        $sql = "SELECT SUM(total_amount) as total_amount, SUM(discount) as disccount, SUM(total_return) as total_return, SUM(shipping_fee) as shipping_fee  
                                        FROM `" . DB_PREFIX . "report_order` 
                                        WHERE order_status IN (7, 8, 9)";

//        this code to query report by staff and store
//        if(!$this->user->isAdmin() && !$this->user->canAccessAll()) {
//            $sql .= " AND ro.user_id = '" . (int)$this->user->getId() . "'";
//        }
//        if (isset($filter['store_id']) && trim($filter['store_id']) !== '' && ($this->user->isUserInStore($filter['store']) || $this->user->isAdmin() || $this->user->canAccessAll())) {
//            $sql .= " AND ro.store_id = '" . (int)$filter['store_id'] . "'";
//        } else {
//            $sql .= " AND ro.store_id in (0,". implode(',', $this->user->getUserStores()) .")";
//        }

        if (isset($filter['store_id']) && trim($filter['store_id']) !== '') {
            $sql .= " AND `store_id` = " . $filter['store_id'];
        }

        if (!empty($filter['start']) && !empty($filter['end'])) {
            $sql .= " AND `report_time` BETWEEN '{$filter['start']}' AND '{$filter['end']}'";
        }
        $query = $this->db->query($sql);

        if ($query->num_rows > 0) {
            return $query->row;
        }

        return [];
    }

    public function getOderReturnAmount($filter)
    {
        $sql = "SELECT SUM(rr.`total`) as `return_amount` 
                FROM `" . DB_PREFIX . "return_receipt` rr 
                    LEFT JOIN `" . DB_PREFIX . "report_order` ro ON ro.`order_id` = rr.`order_id` 
                WHERE 1 ";

        if (isset($filter['store_id']) && $filter['store_id'] !== '' && $filter['store_id'] > -1) {
            $sql .= " AND ro.`store_id` = " . $filter['store_id'];
        }

        if (!empty($filter['start']) && !empty($filter['end'])) {
            $sql .= " AND rr.`date_added` BETWEEN '{$filter['start']}' AND '{$filter['end']}'";
        }

        $query = $this->db->query($sql);

        return isset($query->row['return_amount']) ? (float)$query->row['return_amount'] : 0;
    }

    public function GetCostOfGoodsSold($filter)
    {
        $sql = "SELECT SUM(rp.cost_price * rp.quantity)as costs 
                                        FROM `" . DB_PREFIX . "report_product` rp LEFT JOIN `" . DB_PREFIX . "order` o ON (rp.order_id = o.order_id)
                                        WHERE o.order_status_id IN (7, 8, 9)";

        if (isset($filter['store_id']) && trim($filter['store_id']) !== '') {
            $sql .= " AND rp.`store_id` = " . $filter['store_id'];
        }

        if (!empty($filter['start']) && !empty($filter['end'])) {
            $sql .= " AND rp.`report_time` BETWEEN '{$filter['start']}' AND '{$filter['end']}'";
        }
        $query = $this->db->query($sql);

        if (isset($query->row['costs'])) {
            return (float)$query->row['costs'];
        }

        return 0;
    }

    public function getDeliveryFeePaidToThePartner($filter)
    {
        $sql = "SELECT SUM(amount) as amount 
                                        FROM `" . DB_PREFIX . "payment_voucher` 
                                        WHERE status = 1 AND in_business_report_status = 1 AND payment_voucher_type_id = 1";

        if (isset($filter['store_id']) && trim($filter['store_id']) !== '') {
            $sql .= " AND `store_id` = " . $filter['store_id'];
        }

        if (!empty($filter['start']) && !empty($filter['end'])) {
            $sql .= " AND `date_added` BETWEEN '{$filter['start']}' AND '{$filter['end']}'";
        }
        $query = $this->db->query($sql);

        if (isset($query->row['amount'])) {
            return (float)$query->row['amount'];
        }

        return 0;
    }

    public function getAmountSalaryForStaff($filter)
    {
        $sql = "SELECT SUM(amount) as amount 
                                        FROM `" . DB_PREFIX . "payment_voucher` 
                                        WHERE status = 1 AND in_business_report_status = 1 AND payment_voucher_type_id = 5";

        if (isset($filter['store_id']) && trim($filter['store_id']) !== '') {
            $sql .= " AND `store_id` = " . $filter['store_id'];
        }

        if (!empty($filter['start']) && !empty($filter['end'])) {
            $sql .= " AND `date_added` BETWEEN '{$filter['start']}' AND '{$filter['end']}'";
        }
        $query = $this->db->query($sql);

        if (isset($query->row['amount'])) {
            return (float)$query->row['amount'];
        }

        return 0;
    }

    public function getOtherFee($filter)
    {
        $sql = "SELECT SUM(amount) as amount 
                                        FROM `" . DB_PREFIX . "payment_voucher` 
                                        WHERE status = 1 AND in_business_report_status = 1 AND payment_voucher_type_id NOT IN (8,1,5,7)";

        if (isset($filter['store_id']) && trim($filter['store_id']) !== '') {
            $sql .= " AND `store_id` = " . $filter['store_id'];
        }

        if (!empty($filter['start']) && !empty($filter['end'])) {
            $sql .= " AND `date_added` BETWEEN '{$filter['start']}' AND '{$filter['end']}'";
        }
        $query = $this->db->query($sql);

        if (isset($query->row['amount'])) {
            return (float)$query->row['amount'];
        }

        return 0;
    }

    public function getManualReceiptVoucher($filter)
    {
        $sql = "SELECT SUM(amount) as amount 
                                        FROM `" . DB_PREFIX . "receipt_voucher` 
                                        WHERE status = 1 AND in_business_report_status = 1 AND receipt_voucher_type_id NOT IN (8)";

        if (isset($filter['store_id']) && trim($filter['store_id']) !== '') {
            $sql .= " AND `store_id` = " . $filter['store_id'];
        }

        if (!empty($filter['start']) && !empty($filter['end'])) {
            $sql .= " AND `date_added` BETWEEN '{$filter['start']}' AND '{$filter['end']}'";
        }
        $query = $this->db->query($sql);

        if (isset($query->row['amount'])) {
            return (float)$query->row['amount'];
        }

        return 0;
    }

    public function getReturnFee($filter)
    {
        if (isset($filter['store_id']) && trim($filter['store_id']) != 0) {
            return 0;
        }

        $sql = "SELECT SUM(return_fee) as return_fee 
                                        FROM `" . DB_PREFIX . "return_receipt` 
                                        WHERE 1";

        if (!empty($filter['start']) && !empty($filter['end'])) {
            $sql .= " AND `date_added` BETWEEN '{$filter['start']}' AND '{$filter['end']}'";
        }
        $query = $this->db->query($sql);

        if (isset($query->row['return_fee'])) {
            return (float)$query->row['return_fee'];
        }

        return 0;
    }

    public function getEnterpriseIncomeTaxExpense($filter)
    {
        $sql = "SELECT SUM(amount) as amount 
                                        FROM `" . DB_PREFIX . "payment_voucher` 
                                        WHERE status = 1 AND in_business_report_status = 1 AND payment_voucher_type_id = 7";

        if (isset($filter['store_id']) && trim($filter['store_id']) !== '') {
            $sql .= " AND `store_id` = " . $filter['store_id'];
        }

        if (!empty($filter['start']) && !empty($filter['end'])) {
            $sql .= " AND `date_added` BETWEEN '{$filter['start']}' AND '{$filter['end']}'";
        }
        $query = $this->db->query($sql);

        if (isset($query->row['amount'])) {
            return (float)$query->row['amount'];
        }

        return 0;
    }

    public function export($data)
    {
        $this->load->language('report/financial');
        $additional_data = [
            'sale_revenue' => $this->language->get('text_revenue'),
            'revenue_deductions' => $this->language->get('text_revenue_reduce'),
            'trade_discount' => $this->language->get('text_trade_discount'),
            'total_return' => $this->language->get('text_total_return'),
            'net_revenue' => $this->language->get('text_net_revenue'),
            'cost_ogs' => $this->language->get('text_cost'),
            'gross_revenue' => $this->language->get('text_gross_revenue'),
            'fee' => $this->language->get('text_fee'),
            'delivery_fee_paid' => $this->language->get('text_delivery_for_partner'),
            'pay_salary_for_staff' => $this->language->get('text_pay_salary'),
            'other_fee' => $this->language->get('text_other_fee'),
            'net_profit' => $this->language->get('text_profit'),
            'other_income' => $this->language->get('text_other_income'),
            'manual_receipt_voucher' => $this->language->get('text_manual_receipt'),
            'return_fee' => $this->language->get('text_return_fee'),
            'eite' => $this->language->get('text_enterprise_income_tax_expense'),
            'revenues' => $this->language->get('text_net_income'),
        ];
        $result = [];
        foreach ($additional_data as $key => $value) {
            $result[] = array(
                0 => $value,
                1 => isset($data['current'][$key]) ? $data['current'][$key] : 0,
                2 => isset($data['previous'][$key]) ? $data['previous'][$key] : 0,
                3 => isset($data['change'][$key]) ? $data['change'][$key] : 0,
            );
        }

        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $activeSheet = $spreadsheet->getActiveSheet();
        $activeSheet->setTitle("{$this->language->get('heading_title')}");
        $activeSheet->setCellValue('A1', "{$this->language->get('heading_title')}");
        $activeSheet->setCellValue('B2', "{$this->language->get('text_store')}");
        $activeSheet->setCellValue('C2', $data['store_name']);
        $activeSheet->setCellValue('B3', "{$this->language->get('text_current_period')}");
        $activeSheet->setCellValue('C3', $data['current_time']);
        $activeSheet->setCellValue('B4', "{$this->language->get('text_compare_period')}");
        $activeSheet->setCellValue('C4', $data['previous_time']);
        $activeSheet->getStyle("A1")->getFont()->setSize(20)->setBold(true);
        $activeSheet->getStyle("B3")->getFont()->setBold(true);
        $activeSheet->getStyle("B2")->getFont()->setBold(true);
        $activeSheet->getStyle("B4")->getFont()->setBold(true);

        //output headers
        $header = [
            $this->language->get('text_financial_spending'),
            $this->language->get('text_current_period'),
            $this->language->get('text_compare_period'),
            $this->language->get('text_change')
        ];
        $activeSheet->fromArray($header, NULL, 'A5');

        $styleArrayHeader = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'bbbdbd'],
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'cfe2f3',
                ],
                'endColor' => [
                    'argb' => 'cfe2f3',
                ],
            ],
        ];
        $styleColumn      = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'bbbdbd'],
                ],
            ],
        ];
        $styleArrayFooter = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'bbbdbd'],
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'fce5cd',
                ],
                'endColor' => [
                    'argb' => 'fce5cd',
                ],
            ],
        ];

        $style_align_right = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            ],
        ];
        $style_align_left  = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ],
        ];

        $activeSheet->getStyle('A5:D5')->applyFromArray($styleArrayHeader);
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(50);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);

        $spreadsheet->getActiveSheet()->getRowDimension('5')->setRowHeight(30);

        $rowCount = 6;
        foreach ($result as $key => $row) {
            $activeSheet->setCellValue('A' . $rowCount, $row[0]);
            $activeSheet->SetCellValue('B' . $rowCount, $row[1]);
            $activeSheet->SetCellValue('C' . $rowCount, $row[2]);
            $activeSheet->SetCellValue('D' . $rowCount, $row[3]);
            $activeSheet->getStyle('A' . $rowCount . ':A' . $rowCount)->applyFromArray(array_merge($styleColumn, $style_align_left));
            $activeSheet->getStyle('B' . $rowCount . ':D' . $rowCount)->applyFromArray(array_merge($styleColumn, $style_align_right));
            $rowCount++;
        }

        /*
         * write output for downloading...
         * https://stackoverflow.com/questions/27701981/phpexcel-download-using-ajax-call
         */
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        ob_start();
        $writer->save("php://output"); // for test: DIR_APPLICATION . "products.xlsx"
        $xlsxData = ob_get_contents();
        ob_end_clean();

        return 'data:application/vnd.ms-excel;base64,' . base64_encode($xlsxData);
    }
}