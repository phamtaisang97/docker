<?php

class ModelToolImage extends Model
{
    const DIR_ROOT_IMAGE = DIR_IMAGE . '../';

    public function resize($url, $width, $height)
    {
        if (trim($url) == '') {
            return '';
        }

        if ($url == 'no_image.png') {
            return '/image/no_image.png';
        }

        if ($url == 'placeholder.png') {
            return '/image/placeholder.png';
        }

        $url_new = $this->genarateNewLink($url, $width, $height);

        return $url_new;
    }

    public function imageIsExist($url)
    {
        $file_headers = @get_headers($url);
        if (!$file_headers || strpos($file_headers[0], '404') !== false) {
            return false;
        } else {
            return true;
        }
    }

    public function genarateNewLink($old_url, $width, $height)
    {
        $ext = pathinfo($old_url, PATHINFO_EXTENSION); // file extension
        if ($ext == 'ico') {
            return $old_url;
        }

        $image_server_name = $this->getServerFromImage($old_url);
        if ($image_server_name) {
            $image_server = Image_Server::getImageServer($image_server_name);
            if ($image_server) {
                $newLink = $image_server->resizeImage($old_url, $width, $height);
            } else {
                $newLink = $old_url;
            }

            // only for cloudinary image
            $newLink = cloudinary_change_http_to_https($newLink);
        }else{
            $newLink = $old_url;
        }

        return $newLink;
    }

    function getServerFromImage($url)
    {
        if (strpos($url, 'http://res.cloudinary.com') === 0 || strpos($url, 'https://res.cloudinary.com') === 0) {
            return 'cloudinary';
        } else if (strpos($url, BESTME_IMAGE_SEVER_SERVE_URL) === 0) {
            return 'bestme';
        } else {
            return false;
        }
    }
}
