<?php

class ModelCatalogStoreReceipt extends Model
{
    const STORE_RECEIPT_CREATED_AT = 1;
    const STORE_RECEIPT_MANUFACTURER = 2;
    const STORE_RECEIPT_STATUS = 3;

    const STORE_RECEIPT_STATUS_GOODS_DELIVERING = 0; // Hàng chưa về
    const STORE_RECEIPT_STATUS_GOODS_RECEIPTED = 1; // Hàng đã về
    const STORE_RECEIPT_STATUS_GOODS_CANCEL = 99; // Hủy phiếu

    public function addStoreReceipt($data)
    {
        $receipt_code = $this->autoGenerationCode();
        $status = isset($data['submit_type']) && $data['submit_type'] == 'apply' ? self::STORE_RECEIPT_STATUS_GOODS_RECEIPTED : self::STORE_RECEIPT_STATUS_GOODS_DELIVERING;
        $user_create_id = isset($data['user_create_id']) ? $this->db->escape($data['user_create_id']) : 1;

        $sql = "INSERT INTO `" . DB_PREFIX ."store_receipt` SET 
                            `store_id` = " . (int)$data['receipt_store'] . ", 
                            `manufacturer_id` = " . (int)$data['receipt_manufacturer'] . ", 
                            `store_receipt_code` = '" . $this->db->escape($receipt_code) . "',  
                            `total` = " . (float)extract_number($data['receipt_total']) . ", 
                            `total_to_pay` = " .(float)extract_number($data['total_to_pay']) . ", 
                            `total_paid` = " . (float)extract_number($data['total_paid']) . ", 
                            `discount` = " . (float)extract_number($data['receipt_discount']) . ", 
                            `allocation_criteria_total` = " . (float)extract_number($data['receipt_fee']) . ", 
                            `allocation_criteria_type` = '" . $this->db->escape($data['type_criteria_allocation']) . "',  
                            `allocation_criteria_fees` = '" . $this->db->escape($data['fee_config']) . "',  
                            `owed` = " . (float)extract_number($data['receipt_owed']) . ", 
                            `comment` = '" . $this->db->escape($data['note']) . "', 
                            `user_create_id` = '" . $user_create_id . "', 
                            `updated_at` = NOW(), 
                            `created_at` = NOW()";

        $this->db->query($sql);
        $store_receipt_id = $this->db->getLastId();

        // save product to table store_receipt_to_product
        $list_product_ids = [];
        if (isset($data['product_version_id']) && is_array($data['product_version_id'])){
            foreach ($data['product_version_id'] as $k => $pvid){
                $arr_pvid = explode('-', $pvid);
                $product_id = isset($arr_pvid[0]) ? $arr_pvid[0] : '';
                $version_id = isset($arr_pvid[1]) ? $arr_pvid[1] : '';
                if (!$product_id){
                    continue;
                }
                $list_product_ids[] = $product_id;
                $quantity = isset($data['product_quantity'][$k]) ? (int)extract_number($data['product_quantity'][$k]) : 0;
                $cost_price = isset($data['product_cost_price'][$k]) ? (float)extract_number($data['product_cost_price'][$k]) : 0;
                $fee = isset($data['product_other_fee'][$k]) ? (float)extract_number($data['product_other_fee'][$k]) : 0;
                $discount = isset($data['product_discount'][$k]) ? (float)extract_number($data['product_discount'][$k]) : 0;
                $total = isset($data['into_money'][$k]) ? (float)extract_number($data['into_money'][$k]) : 0;
                $this->db->query("INSERT INTO `" . DB_PREFIX . "store_receipt_to_product` SET  
                                                                `store_receipt_id` = " . (int)$store_receipt_id . ", 
                                                                `product_id` = " . $product_id . ", 
                                                                `product_version_id` = '" . $version_id . "', 
                                                                `quantity` = " . (int)$quantity . ", 
                                                                `cost_price` = " . (float)$cost_price . ", 
                                                                `fee` = " . (float)$fee . ", 
                                                                `discount` = " . (float)$discount . ", 
                                                                `total` = " . (float)$total
                );
            }
        }

        // update status for draft product
        $this->applyDraftProductAfterSave($list_product_ids, (int)$data['receipt_manufacturer'], (int)$data['receipt_store']);
        $this->updateManufactureForProducts($list_product_ids, (int)$data['receipt_manufacturer']);
        $this->fillProductToStoreForDefaultStore($list_product_ids);
        $this->deleteDraftProduct();

        // calculate cost price
        if ($status == self::STORE_RECEIPT_STATUS_GOODS_RECEIPTED) {
            $this->approveStoreReceipt($store_receipt_id);
            $this->deleteProductCaches();
        } else {
            $this->updateStoreReceiptStatus($store_receipt_id, 0);
        }

        return $store_receipt_id;
    }

    public function editStoreReceipt($store_receipt_id, $data)
    {
        if (!$store_receipt_id || !$data){
            return;
        }

        // calculate cost price before edit
        $this->removeStoreReceipt($store_receipt_id);

        $status = isset($data['submit_type']) && $data['submit_type'] == 'apply' ? self::STORE_RECEIPT_STATUS_GOODS_RECEIPTED : self::STORE_RECEIPT_STATUS_GOODS_DELIVERING;
        $sql = "UPDATE `" . DB_PREFIX ."store_receipt` SET 
                            `store_id` = " . (int)$data['receipt_store'] . ", 
                            `manufacturer_id` = " . (int)$data['receipt_manufacturer'] . ", 
                            `total` = " . (float)extract_number($data['receipt_total']) . ", 
                            `total_to_pay` = " .(float)extract_number($data['total_to_pay']) . ", 
                            `total_paid` = " . (float)extract_number($data['total_paid']) . ", 
                            `discount` = " . (float)extract_number($data['receipt_discount']) . ", 
                            `allocation_criteria_total` = " . (float)extract_number($data['receipt_fee']) . ", 
                            `allocation_criteria_type` = '" . $this->db->escape($data['type_criteria_allocation']) . "',  
                            `allocation_criteria_fees` = '" . $this->db->escape($data['fee_config']) . "',  
                            `owed` = " . (float)extract_number($data['receipt_owed']) . ", 
                            `comment` = '" . $this->db->escape($data['note']) . "',  
                             `updated_at` = NOW() WHERE `store_receipt_id` = " . (int)$store_receipt_id;

        $this->db->query($sql);

        $this->db->query("DELETE FROM " . DB_PREFIX . "store_receipt_to_product WHERE `store_receipt_id` = '" . (int)$store_receipt_id . "'");
        // save product to table store_receipt_to_product
        $list_product_ids = [];
        if (isset($data['product_version_id']) && is_array($data['product_version_id'])){
            foreach ($data['product_version_id'] as $k =>$pvid){
                $arr_pvid = explode('-', $pvid);
                $product_id = isset($arr_pvid[0]) ? $arr_pvid[0] : '';
                $version_id = isset($arr_pvid[1]) ? $arr_pvid[1] : '';
                if (!$product_id){
                    continue;
                }
                $list_product_ids[] = $product_id;
                $quantity = isset($data['product_quantity'][$k]) ? (int)extract_number($data['product_quantity'][$k]) : 0;
                $cost_price = isset($data['product_cost_price'][$k]) ? (float)extract_number($data['product_cost_price'][$k]) : 0;
                $fee = isset($data['product_other_fee'][$k]) ? (float)extract_number($data['product_other_fee'][$k]) : 0;
                $discount = isset($data['product_discount'][$k]) ? (float)extract_number($data['product_discount'][$k]) : 0;
                $total = isset($data['into_money'][$k]) ? (float)extract_number($data['into_money'][$k]) : 0;
                $this->db->query("INSERT INTO `" . DB_PREFIX . "store_receipt_to_product` SET  
                                                                `store_receipt_id` = " . (int)$store_receipt_id . ", 
                                                                `product_id` = " . $product_id . ", 
                                                                `product_version_id` = '" . $version_id . "', 
                                                                `quantity` = " . (int)$quantity . ", 
                                                                `cost_price` = " . (float)$cost_price . ", 
                                                                `fee` = " . (float)$fee . ", 
                                                                `discount` = " . (float)$discount . ", 
                                                                `total` = " . (float)$total
                );
            }
        }
        // update status for draft product
        $this->applyDraftProductAfterSave($list_product_ids, (int)$data['receipt_manufacturer'], (int)$data['receipt_store']);
        $this->updateManufactureForProducts($list_product_ids, (int)$data['receipt_manufacturer']);
        $this->fillProductToStoreForDefaultStore($list_product_ids);
        $this->deleteDraftProduct();

        // calculate cost price
        if($status == self::STORE_RECEIPT_STATUS_GOODS_RECEIPTED){
            $this->approveStoreReceipt($store_receipt_id);
            $this->deleteProductCaches();
        }
    }

    private function fillProductToStoreForDefaultStore($product_ids)
    {
        if (!is_array($product_ids) || empty($product_ids)) {
            return;
        }
        $query = $this->db->query("SELECT p.product_id, p.default_store_id, pv.product_version_id 
                                   FROM `" . DB_PREFIX . "product` p 
                                   LEFT JOIN `" . DB_PREFIX . "product_version` pv 
                                          ON CASE WHEN p.multi_versions = 1 
                                                  THEN p.product_id 
                                                  ELSE -1 
                                             END = pv.product_id 
                                   WHERE p.product_id IN ( " . implode(',', $product_ids) . ") AND p.`deleted` IS NULL AND pv.`deleted` IS NULL");

        foreach ($query->rows as $pv) {
            $product_id = (int)$pv['product_id'];
            $product_version_id = (int)$pv['product_version_id'];
            $default_store_id = (int)$pv['default_store_id'];
            $pts = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_to_store` WHERE `product_id` = " . $product_id
                . " AND `product_version_id` = " . $product_version_id
                . " AND `store_id` = " . $default_store_id);

            if ($pts->num_rows > 0) {
                continue;
            } else {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "product_to_store` SET `product_id` = " . (int)$product_id
                    . ", `store_id` = " . (int)$default_store_id
                    . ", `product_version_id` = " . (int)$product_version_id
                    . ", `quantity` = 0, `cost_price` = 0");
            }
        }

        return;
    }

    private function updateManufactureForProducts($product_ids, $manufacturer_id)
    {
        if (!is_array($product_ids) || empty($product_ids) || !$manufacturer_id) {
            return;
        }

        $this->db->query("UPDATE `" . DB_PREFIX . "product` SET `manufacturer_id` = " . (int)$manufacturer_id . " WHERE `product_id` IN ( " . implode(',', $product_ids) . ")");
    }

    public function approveStoreReceipt($store_receipt_id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "store_receipt` WHERE `store_receipt_id` = " . (int)$store_receipt_id);
        $store_receipt_info = $query->row;
        if (empty($store_receipt_info)) {
            return;
        }

        if ( $store_receipt_info['status'] == self::STORE_RECEIPT_STATUS_GOODS_RECEIPTED) {
            return;
        }
        $store_id = (int)$store_receipt_info['store_id'];
        $allocation_criteria_type = $store_receipt_info['allocation_criteria_type'];
        $allocation_criteria_total = (float)$store_receipt_info['allocation_criteria_total'];
        $all_discount = (float)$store_receipt_info['discount'];
        $total = (float)$store_receipt_info['total'];

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "store_receipt_to_product` WHERE `store_receipt_id` = " . (int)$store_receipt_id);
        $store_receipt_products = $query->rows;
        if (empty($store_receipt_products)) {
            return;
        }
        $total_quantity = 0;
        foreach ($store_receipt_products as $product) {
            $total_quantity += (int)$product['quantity'];
        }
        if ($total_quantity == 0) {
            return;
        }
        foreach ($store_receipt_products as $product) {
            $product_id = (int)$product['product_id'];
            $product_version_id = (int)$product['product_version_id'];
            $quantity = (int)$product['quantity'] > 0 ? (int)$product['quantity'] : 0;
            if ($quantity < 1) {
                continue;
            }
            $store_receipt_discount_for_product = (float)$total != 0 ? $all_discount * (float)$product['total'] / (float)$total : 0;
            if ($allocation_criteria_type == 'value') {
                $store_receipt_fee_for_product = (float)$total != 0 ? $allocation_criteria_total * (float)$product['total'] / (float)$total : 0;
            } else {
                $store_receipt_fee_for_product = $allocation_criteria_total * ($quantity / $total_quantity);
            }

            $cost_price = (float)$product['cost_price'] + (float)$product['fee'] - (float)$product['discount'] + ($store_receipt_fee_for_product - $store_receipt_discount_for_product) / $quantity;
            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_to_store` WHERE `product_id` = " . $product_id . " AND `product_version_id` = " . $product_version_id . " AND `store_id` = " . $store_id);
            $product_store = $query->row;
            if (empty($product_store)) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "product_to_store` SET `product_id` = " . $product_id
                    . ", `product_version_id` = " . $product_version_id
                    . ", `store_id` = " . $store_id
                    . ", `quantity` = " . $quantity
                    . ", `cost_price` = " . (float)$cost_price
                );
            } else {
                $new_quantity = (int)$product_store['quantity'] + $quantity;
                if ($new_quantity <= 0) {
                    $new_cost_price = 0;
                } else {
                    if ((float)$product_store['cost_price'] <= 0 || (int)$product_store['quantity'] <= 0) {
                        $new_cost_price = $cost_price;
                    } else {
                        $new_cost_price = ((float)$product_store['cost_price'] * (int)$product_store['quantity'] + $cost_price * $quantity) / ($new_quantity);
                    }
                }
                $this->db->query("UPDATE `" . DB_PREFIX . "product_to_store` 
                                  SET `quantity` = " . (int)$new_quantity . ", 
                                      `cost_price` = " . (float)$new_cost_price . " 
                                  WHERE `product_id` = " . (int)$product_id . " 
                                    AND `product_version_id` = " . (int)$product_version_id . " 
                                    AND `store_id` = " . (int)$store_id
                );
            }
        }

        $this->db->query("UPDATE `" . DB_PREFIX . "store_receipt` SET `status` = 1, `updated_at` = NOW() WHERE `store_receipt_id` = " . (int)$store_receipt_id);
    }

    public function removeStoreReceipt($store_receipt_id, $status = self::STORE_RECEIPT_STATUS_GOODS_DELIVERING) // 0 or 99
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "store_receipt` WHERE `store_receipt_id` = " . (int)$store_receipt_id);
        $store_receipt_info = $query->row;
        if (empty($store_receipt_info)) {
            return;
        }

        if ($store_receipt_info['status'] != self::STORE_RECEIPT_STATUS_GOODS_RECEIPTED) {
            return;
        }
        $store_id = (int)$store_receipt_info['store_id'];
        $allocation_criteria_type = $store_receipt_info['allocation_criteria_type'];
        $allocation_criteria_total = (float)$store_receipt_info['allocation_criteria_total'];
        $all_discount = (float)$store_receipt_info['discount'];
        $total = (float)$store_receipt_info['total'];

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "store_receipt_to_product` WHERE `store_receipt_id` = " . (int)$store_receipt_id);
        $store_receipt_products = $query->rows;
        if (empty($store_receipt_products)) {
            return;
        }
        $total_quantity = 0;
        foreach ($store_receipt_products as $product) {
            $total_quantity += (int)$product['quantity'];
        }
        if ($total_quantity == 0) {
            return;
        }
        foreach ($store_receipt_products as $product) {
            $product_id = (int)$product['product_id'];
            $product_version_id = (int)$product['product_version_id'];
            $quantity = (int)$product['quantity'] > 0 ? (int)$product['quantity'] : 0;
            if ($quantity < 1) {
                continue;
            }
            $store_receipt_discount_for_product = (float)$total != 0 ? $all_discount * (float)$product['total'] / (float)$total : 0;
            if ($allocation_criteria_type == 'value') {
                $store_receipt_fee_for_product = (float)$total != 0 ? $allocation_criteria_total * (float)$product['total'] / (float)$total : 0;
            } else {
                $store_receipt_fee_for_product = $allocation_criteria_total * ($quantity / $total_quantity);
            }

            $cost_price = (float)$product['cost_price'] + (float)$product['fee'] - (float)$product['discount'] + ($store_receipt_fee_for_product - $store_receipt_discount_for_product) / $quantity;
            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_to_store` WHERE `product_id` = " . $product_id . " AND `product_version_id` = " . $product_version_id . " AND `store_id` = " . $store_id);
            $product_store = $query->row;
            if (empty($product_store)) {
                continue;
            }

            $new_quantity = (int)$product_store['quantity'] - $quantity;
            if ($new_quantity <= 0) {
                $new_cost_price = 0;
            } else {
                if ((float)$product_store['cost_price'] <= 0 || (int)$product_store['quantity'] <= 0) {
                    $new_cost_price = $cost_price;
                }else{
                    $new_cost_price = ((float)$product_store['cost_price'] * (int)$product_store['quantity'] - $cost_price * $quantity) / ($new_quantity);
                }
            }
            $this->db->query("UPDATE `" . DB_PREFIX . "product_to_store` 
                              SET `quantity` = " . (int)$new_quantity . ", 
                                  `cost_price` = " . (float)$new_cost_price . " 
                              WHERE `product_id` = " . (int)$product_id . " 
                                AND `product_version_id` = " . (int)$product_version_id . " 
                                AND `store_id` = " . (int)$store_id
            );
        }

        $this->db->query("UPDATE `" . DB_PREFIX . "store_receipt` SET `status` = " . (int)$status . ", `updated_at` = NOW() WHERE `store_receipt_id` = " . (int)$store_receipt_id);
    }

    public function addProductDraft($data)
    {
        $product_type = isset($data['product_version_type']) ? $data['product_version_type'] : 'single';
        $product_name = isset($data['name']) ? $data['name'] : 'Name';
        $sku = isset($data['sku']) ? $data['sku'] : '';
        $cost_price = isset($data['cost_price']) ? extract_number($data['cost_price']) : 0;
        $price = isset($data['price']) ? extract_number($data['price']) : 0;
        $promotion_price = isset($data['promotion_price']) ? extract_number($data['promotion_price']) : 0;
        $quantity = isset($data['quantity']) ? extract_number($data['quantity']) : 0;
        $other_fee = isset($data['other_fee']) ? extract_number($data['other_fee']) : 0;
        //deleted = 2 is status draft
        $result = [];
        if ($product_type == 'single') {
//            `cost_price` = " . (float)$cost_price . ",
//            `other_fee` = " . (float)$other_fee . ",
            // TODO update field cost_price, other_fee
            $this->db->query("INSERT INTO `" . DB_PREFIX . "product` SET `sku` = '" . $this->db->escape($sku) . "', 
                                                                          `price` = " . (float)$promotion_price . ", 
                                                                          `sale_on_out_of_stock` = 1, 
                                                                          `compare_price` = " . (float)$price . ", 
                                                                          `status` = 1, `deleted` = 2, date_added = NOW(), date_modified = NOW()");
            $product_id = $this->db->getLastId();

            if (isset($data['name'])) {
                $language_id = (int)$this->config->get('config_language_id');
                $data['description'] = isset($data['description']) ? $data['description'] : '';
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($product_name) . "', description = '', sub_description = '', seo_title = '', seo_description = ''");
            }
            $result[] = [
                'id' => $product_id,
                'title' => $product_name,
                'sku' => $sku,
                'quantity' => $quantity,
                'other_fee' => $other_fee,
                'cost_price' => $cost_price
            ];
        } else if ($product_type == 'multiple') {
            $this->db->query(
                "INSERT INTO " . DB_PREFIX . "product SET demo = 0, sku = '" . $this->db->escape($sku)
                . "', quantity = 0, price = '"
                . "', price_currency_id = 1, compare_price = '"
                . "', c_price_currency_id = 1, weight = '"
                . "', sale_on_out_of_stock = 1"
                . ",  channel = '" . (isset($data['channel']) ? (int)$data['channel'] : 2)
                . "', weight_class_id = 1, status = '1', manufacturer_id = '', barcode = '"
                . "', common_barcode = '', common_sku = '" . $this->db->escape($sku)
                . "', common_price = '" . (float)$promotion_price . "', common_compare_price = '" . (float)$price
                . "', deleted = 2, multi_versions = '1', date_added = NOW(), date_modified = NOW()");

            $product_id = $this->db->getLastId();

            if ($product_name) {
                $language_id = (int)$this->config->get('config_language_id');
                $data['description'] = isset($data['description']) ? $data['description'] : '';
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($product_name) . "', description = '', sub_description = '', seo_title = '', seo_description = ''");
            }

            if (isset($data['attribute_name'])) {
                foreach ($data['attribute_name'] as $k => $attribute) {
                    if ($attribute != '') {
                        $language_id = (int)$this->config->get('config_language_id'); //TV
                        $this->db->query("INSERT INTO " . DB_PREFIX . "attribute SET attribute_group_id = 0, sort_order = 0 ");
                        $attribute_id = $this->db->getLastId();
                        $this->db->query("INSERT INTO " . DB_PREFIX . "attribute_description SET attribute_id = '" . (int)$attribute_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($attribute) . "'");
                        $attribute_description = implode(",", $data['attribute_values'][$k]);
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$attribute_id . "', language_id = '" . (int)$language_id . "', text = '" . $this->db->escape($attribute_description) . "'");
                    }
                }
            }

            if (isset($data['product_version_names'])) {
                foreach ($data['product_version_names'] as $k => $version_name) {
                    if (isset($data['product_display'])) {
                        $display = in_array($version_name, $data['product_display']) ? 1 : 0;
                    } else {
                        $display = 0;
                    }

                    $version_name = implode(',', explode(' • ', $version_name));
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_version SET product_id = '" . (int)$product_id . "', version = '" . $this->db->escape($version_name) . "', price = '" . $this->db->escape(str_replace(',', '', $data['promotion_prices'][$k])) . "', compare_price = '" . $this->db->escape(str_replace(',', '', $data['prices'][$k])) . "', sku = '" . $this->db->escape($data['skus'][$k]) . "', barcode = '', quantity = '" . $this->db->escape($data['product_quantities'][$k]) . "', status = '" . (int)$display . "'");
                    // TODO update field cost_price, other_fee
                    $product_version_id = $this->db->getLastId();
                    $product_version_ids[] = $product_version_id;
                    $result[] = [
                        'id' => $product_id . '-' . $product_version_id,
                        'title' => $product_name . '(' . $this->language->get('text_version') . $version_name . ')',
                        'sku' => $data['skus'][$k],
                        'quantity' => extract_number($data['product_quantities'][$k]),
                        'cost_price' => extract_number($data['cost_prices'][$k]),
                        'other_fee' => extract_number($data['other_fees'][$k]),
                    ];
                }
            }
        }

        return $result;
    }

    public function applyDraftProductAfterSave($product_ids, $manufacturer_id, $default_store_id)
    {
        if (!is_array($product_ids) || empty($product_ids) || !$manufacturer_id){
            return;
        }
        $array = implode(",", $product_ids);
        $this->db->query("UPDATE `" . DB_PREFIX . "product` SET `manufacturer_id` = " . $manufacturer_id .", `default_store_id` = " . (int)$default_store_id . ", `deleted` = NULL WHERE `deleted` = 2 AND `product_id` IN (" . $array . ")");

        return;
    }

    public function deleteDraftProduct()
    {
        $query = $this->db->query("SELECT `product_id` FROM `" . DB_PREFIX . "product` WHERE `deleted` = 2");
        if ($query->num_rows < 1) {
            return;
        }
        $product_ids = [];
        foreach ($query->rows as $product) {
            $product_ids[] = $product['product_id'];
        }

        $product_ids = implode(",", $product_ids);
        $this->db->query("DELETE FROM `" . DB_PREFIX . "product` WHERE `deleted` = 2");
        $this->db->query("DELETE FROM `" . DB_PREFIX . "product_version` WHERE `product_id` IN (" . $product_ids . ")");
        $this->db->query("DELETE FROM `" . DB_PREFIX . "product_attribute` WHERE `product_id` IN (" . $product_ids . ")");
        $this->db->query("DELETE FROM `" . DB_PREFIX . "product_description` WHERE `product_id` IN (" . $product_ids . ")");
    }

    public function getStoreReceiptById($store_receipt_id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "store_receipt` WHERE `store_receipt_id` = " . (int)$store_receipt_id);

        return $query->row;
    }

    public function getStoreReceiptProductsByReceiptId($store_receipt_id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "store_receipt_to_product` WHERE `store_receipt_id` = " . (int)$store_receipt_id);

        return $query->rows;
    }

    public function getTotalStoreReceipts($filter_data)
    {
        $sql = "SELECT COUNT(DISTINCT sr.store_receipt_id) AS total from  " . DB_PREFIX . "store_receipt sr 
        LEFT JOIN " . DB_PREFIX . "store s ON (sr.store_id = s.store_id)
        LEFT JOIN " . DB_PREFIX . "manufacturer m ON (sr.manufacturer_id = m.manufacturer_id)";
        $sql .= "WHERE sr.status <> 99";

        if (isset($filter_data['filter_status']) && $filter_data['filter_status'] !== '') {
            $sql .= ' AND sr.status=' . $filter_data['filter_status'];
        }

        if (isset($filter_data['filter_name']) && $filter_data['filter_name'] !== '') {
            $sql .= " AND sr.`store_receipt_code` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%' ";
        }

        if (isset($filter_data['filter_manufacturer']) && $filter_data['filter_manufacturer'] !== '') {
            $filter_manufacturer_array = implode(",", $filter_data['filter_manufacturer']);
            if (!empty($filter_manufacturer_array)) {
                $sql .= " AND sr.`manufacturer_id` IN (" . $filter_manufacturer_array . ") ";
            }
        }

        if (isset($filter_data['start_time']) && $filter_data['start_time'] !== '') {
            $sql .= " AND created_at BETWEEN '" . $filter_data['start_time'] . "' AND '". $filter_data['end_time']."'";
        }

        if (isset($filter_data['current_user_id']) && $filter_data['current_user_id'] !== '') {
            $sql .= " AND sr.user_create_id = " . $filter_data['current_user_id'];
        }

        if (isset($filter_data['user_store_id']) && $filter_data['user_store_id'] !== '') {
            $sql .= " AND sr.store_id IN (". $filter_data['user_store_id'] .")";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }
    public function getStoreReceiptsCustom($filter_data)
    {
        if ($filter_data['filter_data'] == '') {
            $data = $this->getStoreReceipts($filter_data);
            return $data;
        }

        $sql = "SELECT sr.*, s.name as branch, m.name as manufacturer FROM " . DB_PREFIX . "store_receipt sr
        LEFT JOIN " . DB_PREFIX . "store s ON (sr.store_id = s.store_id)
        LEFT JOIN " . DB_PREFIX . "manufacturer m ON (sr.manufacturer_id = m.manufacturer_id)";
        $sql .= " WHERE sr.status <> 99";

        if (isset($filter_data['filter_status']) && $filter_data['filter_status'] !== '') {
            $sql .= ' AND sr.status=' . $filter_data['filter_status'];
        }

        if (isset($filter_data['filter_name']) && $filter_data['filter_name'] !== '') {
            $sql .= " AND sr.`store_receipt_code` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%' ";
        }

        if (isset($filter_data['filter_manufacturer']) && $filter_data['filter_manufacturer'] !== '') {
            $filter_manufacturer_array = implode(",", $filter_data['filter_manufacturer']);
            if (!empty($filter_manufacturer_array)) {
                $sql .= " AND sr.`manufacturer_id` IN (" . $filter_manufacturer_array . ") ";
            }
        }

        if (isset($filter_data['start_time']) && $filter_data['start_time'] !== '') {
            $sql .= " AND created_at BETWEEN '" . $filter_data['start_time'] . "' AND '". $filter_data['end_time']."'";
        }

        if (isset($filter_data['current_user_id']) && $filter_data['current_user_id'] !== '') {
            $sql .= " AND sr.user_create_id = " . $filter_data['current_user_id'];
        }

        if (isset($filter_data['user_store_id']) && $filter_data['user_store_id'] !== '') {
            $sql .= " AND sr.store_id IN (". $filter_data['user_store_id'] .")";
        }

        $sql .= " ORDER BY updated_at DESC";

        if (isset($filter_data['start']) || isset($filter_data['limit'])) {
            if ($filter_data['start'] < 0) {
                $filter_data['start'] = 0;
            }

            if ($filter_data['limit'] < 1) {
                $filter_data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$filter_data['start'] . "," . (int)$filter_data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * @param array $data filter, order direction, ...
     * @return array
     */
    public function getStoreReceipts($data = array())
    {
        $sql = "SELECT sr.*, s.name as branch, m.name as manufacturer FROM " . DB_PREFIX . "store_receipt sr
        LEFT JOIN " . DB_PREFIX . "store s ON (sr.store_id = s.store_id)
        LEFT JOIN " . DB_PREFIX . "manufacturer m ON (sr.manufacturer_id = m.manufacturer_id)";

        $sql.= " WHERE sr.status <> 99";
        if (!empty($data['filter_name'])) {
            $sql .= " AND store_receipt_code LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['start_time']) && !empty($data['end_time'])) {
            $sql .= " AND created_at BETWEEN " . $data['start_at'] . "AND". $data['end_at'];
        }

        if (isset($data['current_user_id']) && $data['current_user_id'] !== '') {
            $sql .= " AND sr.user_create_id = " . $data['current_user_id'];
        }

        if (isset($data['user_store_id']) && $data['user_store_id'] !== '') {
            $sql .= " AND sr.store_id IN (". $data['user_store_id'] .")";
        }

        $sql .= " ORDER BY updated_at DESC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getProductContainDraftPaginator($data = array())
    {
        $sql = "SELECT p.user_create_id, p.status as p_status, p.sku as psku, p.sale_on_out_of_stock as p_sale_on_out_of_stock, p.product_id as pid, p.multi_versions, p.price as o_price, p.compare_price as o_compare_price, p.weight as weight, p.quantity as o_quantity, dp.name,p.image, pv.* 
                from " . DB_PREFIX . "product p 
                LEFT JOIN " . DB_PREFIX . "product_description as dp ON (p.product_id = dp.product_id) 
                LEFT JOIN " . DB_PREFIX . "product_version as pv ON CASE 
                                                                            WHEN p.multi_versions = 1 THEN p.product_id 
                                                                            ELSE -1
                                                                            END = pv.product_id";

        if (isset($data['user_store_id']) && $data['user_store_id'] !== '') {
            $sql.= " LEFT JOIN " . DB_PREFIX . "product_to_store as pts ON (p.product_id = pts.product_id)";
        }

        $sql .= " WHERE 1=1 and dp.language_id = " . (int)$this->config->get('config_language_id');

        if (isset($data['current_user_id']) && $data['current_user_id'] !== '') {
            $sql .= " AND p.user_create_id = " . $data['current_user_id'];
        }

        if (isset($data['user_store_id']) && $data['user_store_id'] !== '') {
            $sql .= " AND pts.store_id IN (". $data['user_store_id'] .")";
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND dp.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }
        $sql .= " AND ( p.`deleted` IS NULL OR p.`deleted` = 2)"; // 2 is draft
        $sql .= " AND (pv.`deleted` IS NULL OR pv.`deleted` = 2 )";
        $sql .= " ORDER BY dp.name ASC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);
        $result = array();
        foreach ($query->rows as $k => $item) {
            $result[$k] = array(
                'product_id' => $item['pid'],
                'name' => mb_strlen($item['name']) > 90 ? mb_substr($item['name'], 0, 90) . "..." : $item['name'],
                'product_version_id' => $item['product_version_id'],
                'product_name' => $item['name'],
                'weight' => $item['weight'],
                'image' => $item['image'],
                'version' => $item['version'],
                'sale_on_out_of_stock' => $item['p_sale_on_out_of_stock'],
                'multi_versions' => $item['multi_versions']
            );

            if ($item['product_version_id'] != null) {
                $result[$k]['id'] = $item['pid'] . '-' . $item['product_version_id'];
                $result[$k]['quantity'] = $item['quantity'];
                $result[$k]['price'] = $item['price'] != 0 ? $item['price'] : $item['compare_price'];
                $result[$k]['status'] = $item['status'];
                $result[$k]['p_status'] = $item['p_status'];
                $result[$k]['sku'] = $item['sku'];
            } else {
                $result[$k]['id'] = $item['pid'];
                $result[$k]['quantity'] = $item['o_quantity'];
                $result[$k]['price'] = $item['o_price'] != 0 ? $item['o_price'] : $item['o_compare_price'];
                $result[$k]['status'] = $item['p_status'];
                $result[$k]['p_status'] = $item['p_status'];
                $result[$k]['sku'] = $item['psku'];
            }
        }

        return $result;
    }

    public function countProductAllContainDraft($data = array())
    {
        $language_id = $this->config->get('config_language_id');
        $sql = "SELECT p.product_id,pd.name from " . DB_PREFIX . "product p 
                LEFT JOIN " . DB_PREFIX . "product_description as pd ON (p.product_id = pd.product_id) 
                LEFT JOIN " . DB_PREFIX . "product_version as pv ON (p.product_id = pv.product_id)";

        if (isset($data['user_store_id']) && $data['user_store_id'] !== '') {
            $sql.= " LEFT JOIN " . DB_PREFIX . "product_to_store as pts ON (p.product_id = pts.product_id)";
        }

        $sql .=" WHERE 1=1";

        if (isset($data['current_user_id']) && $data['current_user_id'] !== '') {
            $sql .= " AND p.user_create_id = " . $data['current_user_id'];
        }

        if (isset($data['user_store_id']) && $data['user_store_id'] !== '') {
            $sql .= " AND pts.store_id IN (". $data['user_store_id'] .")";
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " AND (p.`deleted` IS NULL OR p.`deleted` = 2)";
        $sql .= " AND (pv.`deleted` IS NULL OR pv.`deleted` = 2)";
        $sql .= " AND pd.language_id = " . $language_id . "";
        $sql .= " ORDER BY p.product_id ASC";
        $query = $this->db->query($sql);

        return count($query->rows);
    }

    public function autoGenerationCode()
    {
        // count
        $count = $this->getTotalReceiptsIncludeAllStatuses();

        // make code
        $prefix = 'PNH';
        $code = $prefix . str_pad($count + 1, 6, "0", STR_PAD_LEFT);

        return $code;
    }

    private function getTotalReceiptsIncludeAllStatuses()
    {
        $sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "store_receipt`";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    /**
     * @param int $store_receipt_id
     * @return mixed
     */
    public function getStoreReceipt($store_receipt_id)
    {
        $sql = "SELECT sr.*, srtp.product_id as product_id, srtp.product_version_id as product_version_id, srtp.quantity as quantity,
        srtp.cost_price as cost_price, srtp.fee as fee, srtp.discount as discount, srtp.total as total FROM " . DB_PREFIX . "store_receipt sr
        LEFT JOIN " . DB_PREFIX . "store_receipt_to_product srtp ON (sr.store_receipt_id = srtp.store_receipt_id)";
        $sql.= " WHERE sr.store_receipt_id = ".(int)$store_receipt_id;
        $query = $this->db->query($sql);
        return $query->row;
    }

    /**
     * @param int $store_receipt_id
     */
    public function receiptStoreReceipt($store_receipt_id)
    {
        $this->updateStoreReceiptStatus($store_receipt_id, $status = ModelCatalogStoreReceipt::STORE_RECEIPT_STATUS_GOODS_RECEIPTED);
    }

    public function deliverStoreReceipt($store_receipt_id)
    {
        $this->updateManufacturerStatus($store_receipt_id, $status = ModelCatalogStoreReceipt::STORE_RECEIPT_STATUS_GOODS_DELIVERING);
    }

    public function cancelStoreReceipt($store_receipt_id)
    {
        $this->updateStoreReceiptStatus($store_receipt_id, $status = ModelCatalogStoreReceipt::STORE_RECEIPT_STATUS_GOODS_CANCEL);
    }


    public function updateStoreReceiptStatus($store_receipt_id, $status)
    {
        $db_prefix = DB_PREFIX;
        $sql = "UPDATE {$db_prefix}store_receipt SET `status` = '{$status}'";

        $sql .= " WHERE store_receipt_id = '" . (int)$store_receipt_id . "'";

        $this->db->query($sql);
    }

    private function getQtyFromProductToStore($store_id, $product_id, $product_version_id)
    {
        $pvi_str = '';
        if (is_null($product_version_id)) {
            $pvi_str = '`product_version_id` IS NULL';
        }
        else{
            $pvi_str = '`product_version_id` = '.$product_version_id;
        }
        $sql = "SELECT quantity FROM `" . DB_PREFIX . "product_to_store` WHERE `store_id` = " . $store_id
            ." AND `product_id` = " . $product_id." AND " . $pvi_str;
        $query = $this->db->query($sql);
        $qty = is_null($query->row['quantity']) ? 0 : $query->row['quantity'];
        return $qty;
    }

    public function updateQtyForProductToStore($store_id, $product_id, $product_version_id, $quantity, $operator = false)
    {
        $qty = $this->getQtyFromProductToStore($store_id, $product_id, $product_version_id);

        if ($operator) {
            $qty = (int)$qty + (int)$quantity;
        } else {
            $qty = (int)$qty - (int)$quantity;
        }

        if (is_null($product_version_id)) {
            $pvi_str = '`product_version_id` IS NULL';
        } else {
            $pvi_str = '`product_version_id` = ' . (int)$product_version_id;
        }

        $db_prefix = DB_PREFIX;
        $sql = "UPDATE {$db_prefix}product_to_store SET `quantity` = '{$qty}' 
                WHERE store_id = '" . (int)$store_id . "' 
                  AND product_id = '" . (int)$product_id . "' 
                  AND " . $pvi_str;

        $this->db->query($sql);
    }

    public function updateQtyForStoreReceiptToProduct($store_receipt_id)
    {
        $db_prefix = DB_PREFIX;
        $sql = "UPDATE {$db_prefix}store_receipt_to_product SET `quantity` = 0";

        $sql .= " WHERE store_receipt_id = '" . (int)$store_receipt_id . "'";

        $this->db->query($sql);
    }

    /**
     * delete product caches
     */
    private function deleteProductCaches() {
        $product_key_redis =  DB_PREFIX . 'product';
        $this->cache->delete($product_key_redis);
    }
}