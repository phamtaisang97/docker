<?php

class ModelCatalogManufacturer extends Model
{
    const TYPE_IN_TRANSACTION = 1;
    const TYPE_STOP_TRANSACTION = 2;
    const TYPE_DELETE_TRANSACTION = 99;

    use RabbitMq_Trait;

    /**
     * @param array $data
     * @return int added manufacturer id
     */
    public function addManufacturer($data)
    {
        $district = isset($data['district']) ? $data['district'] : '';
        $status = isset($data['status']) && in_array($data['status'], [self::TYPE_IN_TRANSACTION, self::TYPE_STOP_TRANSACTION, self::TYPE_DELETE_TRANSACTION]) ? $data['status'] : self::TYPE_IN_TRANSACTION;
        $user_id = $this->user->getId();
        $this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer 
                          SET `name` = '" . $this->db->escape($data['name']) . "', 
                          `sort_order` = 0,
                          `telephone` = '" . $this->db->escape($data['telephone']) . "',
                          `email` = '" . $this->db->escape($data['email']) . "',
                          `tax_code` = '" . $this->db->escape($data['tax_code']) . "',
                          `address` = '" . $this->db->escape($data['address']) . "',
                          `province` = '" . $this->db->escape($data['province']) . "',
                          `district` = '" . $this->db->escape($district) . "',
                          `user_create_id` = '" . $user_id . "',
                          `status` = ".$status.",
                          `date_added` = NOW(), 
                          `date_modified` = NOW()
                          ");

        $manufacturer_id = $this->db->getLastId();

        if (!empty($data['source'])) {
            $this->updateSourceManufacture($manufacturer_id, $data['source']);
        }

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "manufacturer SET image = '" . $this->db->escape($data['image']) . "' WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
        }

        if (isset($data['manufacturer_store'])) {
            foreach ($data['manufacturer_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = '" . (int)$store_id . "'");
            }
        } else {
            $this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = 0");
        }

        // SEO URL
//        if (isset($data['manufacturer_seo_url'])) {
//            foreach ($data['manufacturer_seo_url'] as $store_id => $language) {
//                foreach ($language as $language_id => $keyword) {
//                    if (!empty($keyword)) {
//                        $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'manufacturer_id=" . (int)$manufacturer_id . "', keyword = '" . $this->db->escape($keyword) . "'");
//                    }
//                }
//            }
//        }

        //$this->cache->delete('manufacturer');

        // update manufacturer id. e.g id = 123 => code(length=9) = NCC000123
        $prefix = 'NCC';
        $code = $prefix . str_pad($manufacturer_id, 6, "0", STR_PAD_LEFT);

        $this->db->query("UPDATE " . DB_PREFIX . "manufacturer 
                          SET `manufacturer_code` = '" . $code . "' 
                          WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

        // SEO URL
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        $manufacturer_alias = $data['name'];

        $this->load->model('custom/common');
        $slug = $this->model_custom_common->createSlug($manufacturer_alias, '-');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug);
        foreach ($languages as $key => $language) {
            $language_id = $language['language_id'];
            $alias = 'manufacturer_id=' . $manufacturer_id;
            $sql = "INSERT INTO " . DB_PREFIX . "seo_url 
                    SET language_id = '" . (int)$language_id . "', 
                        query = '" . $alias . "', 
                        keyword = '" . $slug_custom . "', 
                        table_name = 'manufacturer'";
            $this->db->query($sql);
        }

        // send product to rabbit mq
        try {
            $this->sendToRabbitMq($manufacturer_id, $this->db->escape($data['name']));
        } catch (Exception $exception) {
            $this->log->write('Send message add new manufacture error: ' . $exception->getMessage());
        }

        return $manufacturer_id;
    }

    /**
     * @param int $manufacturer_id
     * @param array $data
     */
    public function editManufacturer($manufacturer_id, $data)
    {
        if (!empty($data['source']) && $data['source'] == 'omihub') {
            return;
        }

        $district = isset($data['district']) ? $data['district'] : '';
        $this->db->query("UPDATE " . DB_PREFIX . "manufacturer 
                          SET `name` = '" . $this->db->escape($data['name']) . "', 
                          `sort_order` = 0, 
                          `telephone` = '" . $this->db->escape($data['telephone']) . "',
                          `email` = '" . $this->db->escape($data['email']) . "',
                          `tax_code` = '" . $this->db->escape($data['tax_code']) . "',
                          `address` = '" . $this->db->escape($data['address']) . "',
                          `province` = '" . $this->db->escape($data['province']) . "',
                          `district` = '" . $this->db->escape($district) . "',
                          `date_modified` = NOW()
                          WHERE `manufacturer_id` = '" . (int)$manufacturer_id . "'");

        if (!empty($data['source'])) {
            $this->updateSourceManufacture($manufacturer_id, $data['source']);
        }

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "manufacturer SET image = '" . $this->db->escape($data['image']) . "' WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "manufacturer_to_store WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

        if (isset($data['manufacturer_store'])) {
            foreach ($data['manufacturer_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = '" . (int)$store_id . "'");
            }
        } else {
            $this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = 0");
        }

//        if (isset($data['manufacturer_seo_url'])) {
//            foreach ($data['manufacturer_seo_url'] as $store_id => $language) {
//                foreach ($language as $language_id => $keyword) {
//                    if (!empty($keyword)) {
//                        $this->db->query("INSERT INTO `" . DB_PREFIX . "seo_url` SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'manufacturer_id=" . (int)$manufacturer_id . "', keyword = '" . $this->db->escape($keyword) . "'");
//                    }
//                }
//            }
//        }

        // SEO URL
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        $manufacturer_alias = $data['name'];

        $this->load->model('custom/common');
        $slug = $this->model_custom_common->createSlug($manufacturer_alias, '-');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug);

        $this->db->query("DELETE FROM `" . DB_PREFIX . "seo_url` WHERE query = 'manufacturer_id=" . (int)$manufacturer_id . "'");

        foreach ($languages as $key => $language) {
            $language_id = $language['language_id'];
            $alias = 'manufacturer_id=' . $manufacturer_id;
            $sql = "INSERT INTO " . DB_PREFIX . "seo_url 
                    SET language_id = '" . (int)$language_id . "', 
                        query = '" . $alias . "', 
                        keyword = '" . $slug_custom . "', 
                        table_name = 'manufacturer'";
            $this->db->query($sql);
        }

        // $this->cache->delete('manufacturer');

        // send product to rabbit mq
        $old_name_manufacture = !empty($data['old_name']) ? $data['old_name'] : '';
        if ($data['name'] != $old_name_manufacture) {
            try {
                $this->sendToRabbitMq($manufacturer_id, $this->db->escape($data['name']), $this->db->escape($old_name_manufacture));
            } catch (Exception $exception) {
                $this->log->write('Send message edit manufacture error: ' . $exception->getMessage());
            }
        }
    }

    /**
     * @param int $manufacturer_id
     */
    public function deleteManufacturer($manufacturer_id)
    {
        $this->updateManufacturerStatus($manufacturer_id, $status = ModelCatalogManufacturer::TYPE_DELETE_TRANSACTION);
        $this->deleteProductCaches();

        try {
            $this->modifyManufactureRabbitMq($manufacturer_id, 'delete');
        } catch (Exception $exception) {
            $this->log->write('Send message delete manufacturer error: ' . $exception->getMessage());
        }
    }

    public function activeManufacturer($manufacturer_id)
    {
        $this->updateManufacturerStatus($manufacturer_id, $status = ModelCatalogManufacturer::TYPE_IN_TRANSACTION);
    }

    public function pauseManufacturer($manufacturer_id)
    {
        $this->updateManufacturerStatus($manufacturer_id, $status = ModelCatalogManufacturer::TYPE_STOP_TRANSACTION);
    }


    private function updateManufacturerStatus($manufacturer_id, $status)
    {
        $db_prefix = DB_PREFIX;
        $sql = "UPDATE {$db_prefix}manufacturer SET `status` = '{$status}'";

        $sql .= " WHERE manufacturer_id = '" . (int)$manufacturer_id . "'";

        $this->db->query($sql);
    }

    public function modifyManufactureRabbitMq($manufacturer_id, $action = 'delete')
    {
        try {
            if (empty($manufacturer_id)) {
                return;
            }

            $manufacturer = $this->getManufacturer($manufacturer_id);
            if (!empty($manufacturer['name'])) {
                $message = [
                    'shop_name' => $this->config->get('shop_name'),
                    'manufacturer' => [
                        'name' => $manufacturer['name'],
                        'action' => $action,
                    ]
                ];

                $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'modify_manufacturer', '', 'modify_manufacturer', json_encode($message));
            }
        } catch (Exception $exception) {
            $this->log->write('Send message modifyManufactureRabbitMq error: ' . $exception->getMessage());
        }
    }

    /**
     * @param int $manufacturer_id
     * @return mixed
     */
    public function getManufacturer($manufacturer_id)
    {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "manufacturer WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

        return $query->row;
    }

    /**
     * get a random manufacturer id
     * @return mixed
     */
    public function getRandomManufacturerId()
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT `manufacturer_id` FROM `{$DB_PREFIX}manufacturer` ORDER BY RAND() LIMIT 1";
        $query = $this->db->query($sql);

        return isset($query->row['manufacturer_id']) ? $query->row['manufacturer_id'] : 0;
    }

    /**
     * @param array $data filter, order direction, ...
     * @return array
     */
    public function getManufacturers($data = array())
    {
        $sql = "SELECT m.*, CONCAT(u.lastname, ' ', u.firstname) as fullname FROM " . DB_PREFIX . "manufacturer m 
            LEFT JOIN " . DB_PREFIX ."user u ON (u.user_id = m.user_create_id) ";

        $sql.= " WHERE m.status <> 99";

        if(!$this->user->canAccessAll()) {
            $sql .= " AND m.user_create_id = ". $this->user->getId();
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND m.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " ORDER BY m.date_modified DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * @param array $data start, limit, filter, order direction, ...
     * @return array
     */
    public function getManufacturersPaginator($data = array())
    {
        $sql = "SELECT om.manufacturer_id as id, om.name as text FROM " . DB_PREFIX . "manufacturer as om WHERE 1";

        if(!$this->user->canAccessAll()) {
            $sql .= " AND om.user_create_id = ". $this->user->getId();
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND om.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_status'])) {
            $sql .= " AND om.status = " . $this->db->escape($data['filter_status']) . "";
        }

        $sql .= " ORDER BY om.name ASC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getManufacturersPauseTransaction()
    {
        $sql = "SELECT om.manufacturer_id as id, om.name as text FROM " . DB_PREFIX . "manufacturer as om 
                    WHERE om.status = " . ModelCatalogManufacturer::TYPE_STOP_TRANSACTION;

        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * @param int $manufacturer_id
     * @return array
     */
    public function getManufacturerStores($manufacturer_id)
    {
        $manufacturer_store_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer_to_store WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

        foreach ($query->rows as $result) {
            $manufacturer_store_data[] = $result['store_id'];
        }

        return $manufacturer_store_data;
    }

    /**
     * @param int $manufacturer_id
     * @return array
     */
    public function getManufacturerSeoUrls($manufacturer_id)
    {
        $manufacturer_seo_url_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query = 'manufacturer_id=" . (int)$manufacturer_id . "'");

        foreach ($query->rows as $result) {
            $manufacturer_seo_url_data[$result['store_id']][$result['language_id']] = $result['keyword'];
        }

        return $manufacturer_seo_url_data;
    }

    /**
     * @return int
     */
    public function getTotalManufacturers($filter_data)
    {
        $sql = "SELECT COUNT(DISTINCT m.manufacturer_id) AS total from  " . DB_PREFIX . "manufacturer m ";
        $sql .= "WHERE m.status <> 99";

        if (isset($filter_data['user_create_id']) && $filter_data['user_create_id'] !== '' ) {
            $sql .= ' AND m.user_create_id=' . $filter_data['user_create_id'];
        }

        if (isset($filter_data['filter_staff']) && $filter_data['filter_staff'] !== '') {
            $sql .= " AND m.user_create_id IN (" . $filter_data['filter_staff'] . ")";
        }

        if (isset($filter_data['manufacturer_status']) && $filter_data['manufacturer_status'] !== '') {
            $sql .= ' AND m.status=' . $filter_data['manufacturer_status'];
        }

        if (isset($filter_data['filter_name']) && $filter_data['filter_name'] !== '') {
            $sql .= " AND m.`name` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%' 
            OR m.`manufacturer_code` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }
    public function getManufacturersCustom($filter_data)
    {
        if ($filter_data['filter_data'] == '') {
            $data = $this->getManufacturers($filter_data);
            return $data;
        }

        $sql = "select m.*, CONCAT(u.lastname, ' ', u.firstname) as fullname from  " . DB_PREFIX . "manufacturer m  
            LEFT JOIN " . DB_PREFIX ."user u ON (u.user_id = m.user_create_id) ";

        $sql .= "WHERE m.status <> 99";

        if (isset($filter_data['user_create_id']) && $filter_data['user_create_id'] !== '' ) {
            $sql .= ' AND m.user_create_id=' . $filter_data['user_create_id'];
        }

        if (isset($filter_data['filter_staff']) && $filter_data['filter_staff'] !== '') {
            $sql .= " AND m.user_create_id IN (" . $filter_data['filter_staff'] . ")";
        }

        if (isset($filter_data['manufacturer_status']) && $filter_data['manufacturer_status'] !== '') {
            $sql .= ' AND m.status=' . $filter_data['manufacturer_status'];
        }

        if (isset($filter_data['filter_name']) && $filter_data['filter_name'] !== '') {
            $sql .= " AND m.`name` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%' 
            OR m.`manufacturer_code` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%'";
        }

        $sql = $sql . " group by m.manufacturer_id";
        $sql .= " ORDER BY m.date_modified DESC";

        if (isset($filter_data['start']) || isset($filter_data['limit'])) {
            if ($filter_data['start'] < 0) {
                $filter_data['start'] = 0;
            }

            if ($filter_data['limit'] < 1) {
                $filter_data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$filter_data['start'] . "," . (int)$filter_data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * @param array $data filter
     * @return int
     */
    public function getTotalManufacturersFilter($data = array())
    {
        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "manufacturer WHERE 1";

        if(!$this->user->canAccessAll()) {
            $sql .= " AND user_create_id =". $this->user->getId();
        }

        if (isset($data['filter_staff']) && $data['filter_staff'] !== '') {
            $sql .= " AND m.user_create_id IN (" . $data['filter_staff'] . ")";
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_status'])) {
            $sql .= " AND status = " . $this->db->escape($data['filter_status']) . "";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function addManufacturerFast($name)
    {
        $data = [
            "name" => $name,
            "sort_order" => 0,
            "manufacturer_store" => [0],
            "image" => "",
            "manufacturer_seo_url" => [[
                1 => str_replace(" ", "-", $this->db->escape($name)),
            ]]
        ];

        $user_id = $this->user->getId();
        $this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer 
                          SET name = '" . $this->db->escape($data['name']) . "', 
                          sort_order = '" . (int)$data['sort_order'] . "',`status` = 1 , 
                          user_create_id = '". (int)$user_id ."', 
                          `date_added` = NOW(), `date_modified` = NOW()");

        $manufacturer_id = $this->db->getLastId();

        $prefix = 'NCC';
        $code = $prefix . str_pad($manufacturer_id, 6, "0", STR_PAD_LEFT);

        $this->db->query("UPDATE " . DB_PREFIX . "manufacturer 
                          SET `manufacturer_code` = '" . $code . "' 
                          WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "manufacturer SET image = '" . $this->db->escape($data['image']) . "' WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
        }

        if (isset($data['manufacturer_store'])) {
            foreach ($data['manufacturer_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = '" . (int)$store_id . "'");
            }
        } else {
            $this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = 0");
        }

        // SEO URL
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        $manufacturer_alias = $data['name'];

        $this->load->model('custom/common');
        $slug = $this->model_custom_common->createSlug($manufacturer_alias, '-');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug);
        foreach ($languages as $key => $language) {
            $language_id = $language['language_id'];
            $alias = 'manufacturer_id=' . $manufacturer_id;
            $sql = "INSERT INTO " . DB_PREFIX . "seo_url SET language_id = '" . (int)$language_id . "', query = '" . $alias . "', keyword = '" . $slug_custom . "', table_name = 'manufacturer'";
            $this->db->query($sql);
        }

        //$this->cache->delete('manufacturer');

        // send product to rabbit mq
        try {
            $this->sendToRabbitMq($manufacturer_id, $this->db->escape($data['name']));
        } catch (Exception $exception) {
            $this->log->write('Send message add manufacture fast error: ' . $exception->getMessage());
        }

        return $manufacturer_id;
    }

    public function updateSourceManufacture($manufacturer_id, $source = null)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "manufacturer SET 
                                   `source` = '" . $this->db->escape($source) . "' 
                                    WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
    }

    public function getProductInManufacture($manufacture_id)
    {
        $sql = "SELECT COUNT(product_id) as total FROM ". DB_PREFIX ."product WHERE manufacturer_id = '". (int)$manufacture_id ."'";
        $query = $this->db->query($sql);
        return $query->row;
    }

    public function getManufactureByName($filter)
    {
        $sql = "SELECT * FROM ". DB_PREFIX . "manufacturer WHERE 1=1 ";
        if (isset($filter['name'])) {
            $sql .= " AND UPPER(name) = '". $this->db->escape(strtoupper($filter['name'])) ."'";
        }
        if (isset($filter['id']) && $filter['id'] != '') {
            $sql .= " AND manufacturer_id != '". (int)$filter['id'] ."'";
        }
        $query = $this->db->query($sql);
        return $query->row;
    }

    public function getManufacturersByIds(array $manufacturer_ids)
    {
        if (empty($manufacturer_ids)) {
            return [];
        }

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer 
                                   WHERE manufacturer_id IN (" . implode(',', $manufacturer_ids) . ")");

        return $query->rows;
    }

    public function editManufactureName($data)
    {
        $sql = "UPDATE ". DB_PREFIX ."manufacturer SET name = '". $this->db->escape($data['name']) ."'";
        $sql .= " WHERE manufacturer_id = '". (int)$data['id'] ."'";
        $this->db->query($sql);
    }

    public function updateProductManufacture($product_id, $manufacture_id) {
        $sql = "UPDATE ". DB_PREFIX ."product SET manufacturer_id = '" . (int)$manufacture_id . "' WHERE product_id = '". (int)$product_id ."'";
        $this->db->query($sql);
    }

    public function getManufacturerNameById($manufacturer_id)
    {
        $query = $this->db->query("SELECT `name` FROM `" . DB_PREFIX . "manufacturer` WHERE `manufacturer_id` = " .(int)$manufacturer_id);

        if (isset($query->row['name'])){
            return $query->row['name'];
        }

        return '';
    }

    public function validateNameManufacturerExist($name, $manufacturer_id)
    {
        $sql = "SELECT COUNT(DISTINCT manufacturer_id) as total FROM `" . DB_PREFIX . "manufacturer` WHERE `name` = '" . $this->db->escape($name) . "'";
        if (!empty($manufacturer_id)) {
            $sql .= " AND `manufacturer_id` != " . (int)$manufacturer_id;
        }

        $query = $this->db->query($sql);
        if ($query->row['total'] != 0) {
            return true;
        }
        return false;
    }

    public function validateTaxCodeManufacturerExist($tax_code, $manufacturer_id)
    {
        $sql = "SELECT COUNT(DISTINCT manufacturer_id) as total FROM `" . DB_PREFIX . "manufacturer` WHERE `tax_code` = '" . $this->db->escape($tax_code) . "'";
        if (!empty($manufacturer_id)) {
            $sql .= " AND `manufacturer_id` != " . (int)$manufacturer_id;
        }

        $query = $this->db->query($sql);
        if ($query->row['total'] != 0) {
            return true;
        }
        return false;
    }

    public function sendToRabbitMq($manufacture_id, $manufacture_name, $manufacture_old_name = "")
    {
        try {
            if (!empty($manufacture_id)) {
                $message = [
                    'shop_name' => $this->config->get('shop_name'),
                    'manufacture' => [
                        'name' => $manufacture_name,
                        'old_name' => $manufacture_old_name
                    ]
                ];

                $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'manufacture', '', 'manufacture', json_encode($message));
            }
        } catch (Exception $exception) {
            $this->log->write('Send message manufacture error: ' . $exception->getMessage());
        }
    }

    /**
     * delete product caches
     */
    private function deleteProductCaches() {
        $product_key_redis =  DB_PREFIX . 'product';
        $this->cache->delete($product_key_redis);
    }
}
