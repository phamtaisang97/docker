<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ModelCatalogProduct extends Model
{
    use Setting_Util_Trait;
    use RabbitMq_Trait;

    const ORDER_TAG_NEW_PREFIX = 'NewTag_';
    const LIMIT_GET_ITEM_IN_DATA_EXCEL = 50;

    /**
     * @param array $data, format
     * - single product:
     * {
     *     "name": "test social 1 - 1PB",
     *     "description": "&lt;p&gt;test 1 - desc&lt;\/p&gt;\r\n\r\n&lt;p&gt;test 1 - desc&lt;\/p&gt;\r\n\r\n&lt;p&gt;test 1 - desc&lt;\/p&gt;",
     *     "summary": "&lt;p&gt;test 1 - short&lt;\/p&gt;\r\n\r\n&lt;p&gt;test 1 - short&lt;\/p&gt;",
     *     "product_version": "0",
     *     "price": "10,000",
     *     "promotion_price": "9,000",
     *     "weight": "123",
     *     "sku": "SOCIAL001",
     *     "barcode": "82574258001",
     *     "stores_default_id": "0",
     *     "sale_on_out_of_stock": "1",
     *     "common_compare_price": "",
     *     "common_price": "",
     *     "common_weight": "0",
     *     "common_sku": "",
     *     "common_barcode": "",
     *     "stores_default_id_mul": "0",
     *     "common_cost_price": "0",
     *     "common_sale_on_out_of_stock": "1",
     *     "attribute_name": [
     *         ""
     *     ],
     *     "stores": [
     *         [
     *             "0"
     *         ]
     *     ],
     *     "original_inventory": [
     *         [
     *         "50"
     *         ]
     *     ],
     *     "cost_price": [
     *         [
     *         "5,000"
     *         ]
     *     ],
     *     "seo_title": "",
     *     "seo_desciption": "",
     *     "alias": "",
     *     "meta_keyword": "",
     *     "images": [
     *         "https:\/\/res.cloudinary.com\/bestme-test\/image\/upload\/v1587983715\/x2.bestme.test\/cf714c11ee1dab3ee6fbac9e7bea41eb.jpg",
     *         "https:\/\/res.cloudinary.com\/bestme-test\/image\/upload\/v1587983704\/x2.bestme.test\/942706098598332177618186301982977727397888n.jpg"
     *     ],
     *     "image-alts": [
     *         "",
     *         ""
     *     ],
     *     "status": "1",
     *     "category": [
     *         "add_new_social",
     *         "add_new_cat-social"
     *     ],
     *     "manufacturer": "add_new_man-social",
     *     "collection_list": [
     *         "35"
     *     ],
     *     "tag_list": [
     *         "25"
     *     ],
     *     "channel": "0"
     * }
     *
     * - multi versions:
     *
     * {
     *     "name": "test social 2 - NPB",
     *     "description": "&lt;p&gt;test social 2 - NPB - desc&lt;\/p&gt;\r\n\r\n&lt;p&gt;test social 2 - NPB - desc&lt;\/p&gt;\r\n\r\n&lt;p&gt;test social 2 - NPB\u00a0- desc&lt;\/p&gt;",
     *     "summary": "&lt;p&gt;test social 1 - NPB - short&lt;\/p&gt;\r\n\r\n&lt;p&gt;test social 1 - NPB - short&lt;\/p&gt;\r\n\r\n&lt;p&gt;test social 1 - NPB - short&lt;\/p&gt;",
     *     "product_version": "1",
     *     "price": "0",
     *     "promotion_price": "",
     *     "weight": "0",
     *     "sku": "",
     *     "barcode": "",
     *     "stores_default_id": "0",
     *     "sale_on_out_of_stock": "1",
     *     "common_compare_price": "20,000",
     *     "common_price": "18,000",
     *     "common_weight": "150",
     *     "common_sku": "SOCIALNPB2",
     *     "common_barcode": "123456789002",
     *     "stores_default_id_mul": "0",
     *     "common_cost_price": "16,000",
     *     "common_sale_on_out_of_stock": "1",
     *     "attribute_name": [
     *         "M\u00e0u s\u1eafc"
     *     ],
     *     "attribute_values": [
     *         [
     *             "XANH",
     *             "CAM"
     *         ]
     *     ],
     *     "product_display": [
     *         "XANH",
     *         "CAM"
     *     ],
     *     "product_version_names": [
     *         "XANH",
     *         "CAM"
     *     ],
     *     "product_version_ids": [
     *         "",
     *         ""
     *     ],
     *     "product_prices": [
     *         "20,000",
     *         "20,000"
     *     ],
     *     "product_promotion_prices": [
     *         "17,000",
     *         "18,000"
     *     ],
     *     "product_quantities": [
     *         "",
     *         ""
     *     ],
     *     "product_skus": [
     *         "SOCIALNPB21",
     *         "SOCIALNPB22"
     *     ],
     *     "product_barcodes": [
     *         "123456789002",
     *         "123456789002"
     *     ],
     *     "seo_title": "test social 2 - NPB seo title",
     *     "seo_desciption": "test social 2 - NPB seo desc",
     *     "alias": "test social 2 - NPB",
     *     "meta_keyword": "test social 2 - NPB,bestme-social",
     *     "images": [
     *         "https:\/\/res.cloudinary.com\/bestme-test\/image\/upload\/v1587983715\/x2.bestme.test\/cf714c11ee1dab3ee6fbac9e7bea41eb.jpg",
     *         "https:\/\/res.cloudinary.com\/bestme-test\/image\/upload\/v1587983704\/x2.bestme.test\/942706098598332177618186301982977727397888n.jpg"
     *     ],
     *     "image-alts": [
     *         "",
     *         ""
     *     ],
     *     "status": "1",
     *     "category": [
     *         "183"
     *     ],
     *     "manufacturer": "48",
     *     "collection_list": [
     *         "35"
     *     ],
     *     "tag_list": [
     *         "25"
     *     ],
     *     "channel": "0"
     *     }
     *
     * @return mixed
     */
    public function addProduct($data)
    {
        /* quickly add manufacturer if has */
        $manufacturer_id = '';
        if (isset($data['manufacturer']) && $data['manufacturer'] != null) {
            $this->load->model('catalog/manufacturer');
            if (strpos($data['manufacturer'], 'add_new_') !== false) {
                $manufacturer_id = $this->model_catalog_manufacturer->addManufacturerFast(str_replace('add_new_', '', $data['manufacturer']));
            } else {
                $manufacturer_id = $data['manufacturer'];
            }

            if (!empty($data['source'])) {
                $manufacturer = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);
                if (isset($manufacturer['source']) && $manufacturer['source'] != 'omihub') {
                    $this->model_catalog_manufacturer->updateSourceManufacture($manufacturer_id, $data['source']);
                }
            }
        }
        // get user_store
        $user_id = $this->user->getId();
        $this->load->model('user/user');
        $user_stores = $this->model_user_user->getUserStore($user_id);
        $default_store = !empty($user_stores) ? $user_stores[0]['id'] : 0;

        /* update default stores */
        if ((int)$data['product_version'] == 1) {
            $sale_on_out_of_stock = isset($data['common_sale_on_out_of_stock']) ? $data['common_sale_on_out_of_stock'] : 0;
            $weight = isset($data['common_weight']) ? extract_number($data['common_weight']) : 0;
            $default_store_id = isset($data['stores_default_id_mul']) ? $data['stores_default_id_mul'] : $default_store;
        } else {
            $sale_on_out_of_stock = isset($data['sale_on_out_of_stock']) ? $data['sale_on_out_of_stock'] : 0;
            $weight = isset($data['weight']) ? extract_number($data['weight']) : 0;
            $default_store_id = isset($data['stores_default_id']) ? $data['stores_default_id'] : $default_store;
        }

        /* create product */
        $user_create_id = $this->user->getId();
        $this->db->query("INSERT INTO " . DB_PREFIX . "product 
                          SET demo = 0, 
                              sku = '" . $this->db->escape($data['sku']) . "', 
                              quantity = '" . (int)0 . "', 
                              price = '" . (float)str_replace(',', '', $data['promotion_price']) . "', 
                              price_currency_id = 1, 
                              compare_price = '" . (float)str_replace(',', '', $data['price']) . "', 
                              c_price_currency_id = 1, 
                              weight = '" . (float)extract_number($weight) . "', 
                              channel = '" . (isset($data['channel']) ? (int)$data['channel'] : 2) . "', 
                              weight_class_id = 1, 
                              status = '" . (int)$data['status'] . "', 
                              manufacturer_id = '" . (int)$manufacturer_id . "', 
                              barcode = '" . $data['barcode'] . "', 
                              sale_on_out_of_stock = '" . (int)$sale_on_out_of_stock . "', 
                              common_barcode = '" . $this->db->escape(getValueByKey($data, 'common_barcode')) . "', 
                              common_sku = '" . $this->db->escape(getValueByKey($data, 'common_sku')) . "', 
                              common_price = '" . (float)str_replace(',', '', getValueByKey($data, 'common_price')) . "', 
                              common_cost_price = '" . (float)str_replace(',', '', getValueByKey($data, 'common_cost_price')) . "', 
                              common_compare_price = '" . (float)str_replace(',', '', getValueByKey($data, 'common_compare_price')) . "', 
                              multi_versions = '" . (int)$data['product_version'] . "', 
                              `default_store_id` = '" . (int)$default_store_id . "',
                              user_create_id = '". (int)$user_create_id ."',
                              show_gg_product_feed = '". (int)$data['show_gg_product_feed'] ."',
                              date_added = NOW(), 
                              date_modified = NOW()");

        $product_id = $this->db->getLastId();
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();

        // update source
        if (!empty($data['source'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET `source` = '" . $this->db->escape($data['source']) . "' WHERE product_id = '" . (int)$product_id . "'");
        }

        /* update main image */
        if (!empty($data['images'])) {
            if (!empty($data['images'][0])) {
                $alt = '';
                if (!empty($data['image-alts'][0])) {
                    $alt = $data['image-alts'][0];
                    array_shift($data['image-alts']);
                }
                $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['images'][0]) . "', image_alt = '" . $this->db->escape($alt) . "' WHERE product_id = '" . (int)$product_id . "'");
            }
            array_shift($data['images']);
        }

        /* update description */
        if (isset($data['name'])) {
            $language_id = (int)$this->config->get('config_language_id');
            $data['description'] = isset($data['description']) ? $data['description'] : '';
            $data['meta_keyword'] = isset($data['meta_keyword']) ? $data['meta_keyword'] : '';
            $data['seo_title'] = isset($data['seo_title']) ? $data['seo_title'] : '';
            $data['seo_desciption'] = isset($data['seo_desciption']) ? $data['seo_desciption'] : '';
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET 
            product_id = '" . (int)$product_id . "', 
            language_id = '" . (int)$language_id . "', 
            name = '" . $this->db->escape($data['name']) . "', 
            description = '" . $this->db->escape($data['description']) . "', 
            sub_description = '" . $this->db->escape($data['summary']) . "', 
            seo_title = '" . $this->db->escape($data['seo_title']) . "', 
            seo_description = '" . $this->db->escape($data['seo_desciption']) . "', 
            meta_keyword = '" . $this->db->escape($data['meta_keyword']) . "', 
            noindex = '" . $this->db->escape($data['noindex']) . "',
            nofollow = '" . $this->db->escape($data['nofollow']) . "',
            noarchive = '" . $this->db->escape($data['noarchive']) . "',
            noimageindex = '" . $this->db->escape($data['noimageindex']) . "',
            nosnippet = '" . $this->db->escape($data['nosnippet']) . "'");
        }

        /* update description tab */
        if (isset($data['title_tab']) && isset($data['description_tab'])) {
            foreach ($data['title_tab'] as $key => $title_tab) {
                $description_tab = $data['description_tab'][$key];
                if ($title_tab != null) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_description_tab SET product_id = '" . (int)$product_id . "', title = '" . $this->db->escape($title_tab) . "', description = '" . $this->db->escape($description_tab) . "'");
                }
            }
        }

        /* update product version (attribute) values */
        if (isset($data['attribute_name']) && (int)$data['product_version'] == 1) {
            foreach ($data['attribute_name'] as $k => $attribute) {
                $attribute = trim($attribute);
                if ($attribute != '') {
                    $language_id = (int)$this->config->get('config_language_id'); //TV
                    $this->db->query("INSERT INTO " . DB_PREFIX . "attribute SET attribute_group_id = 0, sort_order = 0 ");
                    $attribute_id = $this->db->getLastId();
                    $this->db->query("INSERT INTO " . DB_PREFIX . "attribute_description SET attribute_id = '" . (int)$attribute_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($attribute) . "'");
                    $attribute_description = implode(",", $data['attribute_values'][$k]);
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$attribute_id . "', language_id = '" . (int)$language_id . "', text = '" . $this->db->escape($attribute_description) . "'");
                }
            }

            // add attribute filter
            $this->load->model('catalog/attribute_filter');
            $this->model_catalog_attribute_filter->updateAttributeFilterByProduct($product_id);
        }

        /* update product version */
        $product_version_ids = [];

        if (isset($data['product_version_names']) && (int)$data['product_version'] == 1) {
            foreach ($data['product_version_names'] as $k => $version_name) {
                if (isset($data['product_display'])) {
                    $display = in_array($version_name, $data['product_display']) ? 1 : 0;
                } else {
                    $display = 0;
                }

                $product_version_image = isset($data['product_version_images'][$k]) ? $data['product_version_images'][$k] : '';
                $product_version_image_alt = isset($data['product_version_image_alts'][$k]) ? $data['product_version_image_alts'][$k] : '';

                $new_key = $this->replaceVersionValues($version_name);
                $version_name = implode(',', explode(' • ', $version_name));
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_version 
                                    SET product_id = '" . (int)$product_id . "',
                                        version = '" . $this->db->escape($version_name) . "',
                                        price = '" . $this->db->escape(str_replace(',', '', $data['product_promotion_prices'][$k])) . "', compare_price = '" . $this->db->escape(str_replace(',', '', $data['product_prices'][$k])) . "',
                                        sku = '" . $this->db->escape($data['product_skus'][$k]) . "',
                                        barcode = '" . $this->db->escape($data['product_barcodes'][$k]) . "',
                                        quantity = '" . $this->db->escape($data['product_quantities'][$k]) . "',
                                        status = '" . (int)$display . "',
                                        `image` = '" . $this->db->escape($product_version_image) . "',
                                        `image_alt` = '" . $this->db->escape($product_version_image_alt) . "'");
                $product_version_id = $this->db->getLastId();
                $product_version_ids[$new_key] = $product_version_id;
            }
        }

        /* update additional images */
        if (isset($data['images'])) {
            foreach ($data['images'] as $k => $image) {
                $alt = isset($data['image-alts'][$k]) ? $data['image-alts'][$k] : '';
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($image) . "', image_alt = '" . $this->db->escape($alt) . "', sort_order = " . (int)$k);
            }
        }

        /* update product-category */
        if (isset($data['category'])) {
            foreach ($data['category'] as $category_id) {
                if (strpos($category_id, 'add_new_') !== false) {
                    $this->load->model('catalog/category');
                    $category_id = $this->model_catalog_category->addCategoryFast(str_replace('add_new_', '', $category_id));
                }
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
            }
        }

        /* update product-collection */
        if (isset($data['collection_list'])) {
            foreach ($data['collection_list'] as $collection_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_collection SET product_id = '" . (int)$product_id . "', collection_id = '" . (int)$collection_id . "'");
            }
        }

        /* update product-tag */
        if (isset($data['tag_list'])) {
            foreach ($data['tag_list'] as $tag_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_tag SET product_id = '" . (int)$product_id . "', tag_id = '" . (int)$tag_id . "'");
            }
        }

        /* update product to store */
        if (isset($data['stores'])) {
            if (!empty($product_version_ids)) {
                foreach ($product_version_ids as $key => $pv_id) {
                    if (!isset($data['stores'][$key]) || !is_array($data['stores'][$key])) {
                        continue;
                    }

                    foreach ($data['stores'][$key] as $store_key => $store_id) {
                        $item_quantity = isset($data['original_inventory'][$key][$store_key]) ? (int)$data['original_inventory'][$key][$store_key] : 0;
                        $item_cost_price = isset($data['cost_price'][$key][$store_key]) ? (float)extract_number($data['cost_price'][$key][$store_key]) : 0;
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store 
                                          SET `product_id` = '" . (int)$product_id . "', 
                                              `store_id` = '" . (int)$store_id . "',
                                              `product_version_id` = '" . (int)$pv_id . "', 
                                              `quantity` = '" . $item_quantity . "', 
                                              `cost_price` = '" . $item_cost_price . "'");
                    }
                }
            } else {
                foreach ($data['stores'] as $key => $store) {
                    // get user_store
                    $user_id = $this->user->getId();
                    $this->load->model('user/user');
                    $user_stores = $this->model_user_user->getUserStore($user_id);
                    $default_store = !empty($user_stores) ? $user_stores[0]['id'] : 0;

                    $store_id = (isset($store[0])) ? $store[0] : $default_store;
                    $original_inventory = (isset($data['original_inventory'][$key][0])) ? $data['original_inventory'][$key][0] : 0;
                    $cost_price = (isset($data['cost_price'][$key][0])) ? $data['cost_price'][$key][0] : 0;

                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store 
                                      SET `product_id` = '" . (int)$product_id . "', 
                                          `store_id` = '" . (int)$store_id . "', 
                                          `product_version_id` = '" . (int)0 . "', 
                                          `quantity` = '" . (int)$original_inventory . "', 
                                          `cost_price` = '" . (float)extract_number($cost_price) . "'");
                }
            }
        }

        /* create seo */
        $product_alias = $data['name'];
        if (isset($data['alias']) && $data['alias'] != '') {
            $product_alias = $data['alias'];
        }

        $this->load->model('custom/common');
        $slug = $this->model_custom_common->createSlug($product_alias, '-');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug);
        foreach ($languages as $key => $language) {
            $language_id = $language['language_id'];
            $alias = 'product_id=' . $product_id;
            $sql = "INSERT INTO " . DB_PREFIX . "seo_url 
                    SET language_id = '" . (int)$language_id . "', 
                        query = '" . $alias . "', 
                        keyword = '" . $slug_custom . "', 
                        table_name = 'product'";
            $this->db->query($sql);
        }

        if (!empty($data['ingredient_id']) && is_array($data['ingredient_id'])) {
            $DB_PREFIX = DB_PREFIX;
            try {
                // add new
                $sql = "INSERT INTO `{$DB_PREFIX}product_ingredient` (product_id, ingredient_id, quantitative) VALUES ";
                foreach ($data['ingredient_id'] as $key => $item_id) {
                    $quantitative = (isset($data['quantitative'][$key])) ? (string)$data['quantitative'][$key] : '';

                    if ($key == (count($data['ingredient_id']) - 1)) {
                        $sql .= " (" . (int)$product_id . ", " . (int)$item_id . ", '".$quantitative."');";
                        break;
                    }

                    $sql .= " (" . (int)$product_id . ", " . (int)$item_id . ", '" . $quantitative . "'),";

                }

                $this->db->query($sql);
            } catch (Exception $exception) {
                $this->log->write('[Add product ingredient] error : ' . $exception->getMessage());
            }
        }

        $this->deleteProductCaches();

        // send product to rabbit mq
        $source_product = !empty($data['source']) ? $data['source'] : '';

        try {
            if ($source_product != 'excel') {
                $this->sendToRabbitMq($product_id);
            }
        } catch (Exception $exception) {
            $this->log->write('Send message add product error: ' . $exception->getMessage());
        }

        return $product_id;
    }

    /**
     * edit product
     *
     * Too many duplicated code with function add()
     * TODO: re-use code (common function, extract function, ...), do not duplicate code...
     * @param int $product_id
     * @param array $data
     */
    public function editProduct($product_id, $data)
    {
        if (!empty($data['source']) && $data['source'] == 'omihub') {
            return;
        }
        /* quickly add manufacturer if has */
        $manufacturer_id = '';
        if (isset($data['manufacturer']) && $data['manufacturer'] != null) {
            $this->load->model('catalog/manufacturer');
            if (strpos($data['manufacturer'], 'add_new_') !== false) {
                $manufacturer_id = $this->model_catalog_manufacturer->addManufacturerFast(str_replace('add_new_', '', $data['manufacturer']));
            } else {
                $manufacturer_id = $data['manufacturer'];
            }

            if (!empty($data['source'])) {
                $manufacturer = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);
                if (isset($manufacturer['source']) && $manufacturer['source'] != 'omihub') {
                    $this->model_catalog_manufacturer->updateSourceManufacture($manufacturer_id, $data['source']);
                }
            }
        }

        /* update default stores */
        if ((int)$data['product_version'] == 1) {
            $sale_on_out_of_stock = isset($data['common_sale_on_out_of_stock']) ? $data['common_sale_on_out_of_stock'] : 0;
            $weight = isset($data['common_weight']) ? extract_number($data['common_weight']) : 0;
            $default_store_id = isset($data['stores_default_id_mul']) ? $data['stores_default_id_mul'] : 0;
        } else {
            $sale_on_out_of_stock = isset($data['sale_on_out_of_stock']) ? $data['sale_on_out_of_stock'] : 0;
            $weight = isset($data['weight']) ? extract_number($data['weight']) : 0;
            $default_store_id = isset($data['stores_default_id']) ? $data['stores_default_id'] : 0;
        }

        $old_product = $this->getProductForLazada($product_id);
        $old_product_version = $this->getProductVersions($product_id);

        $user_create_id =  isset($data['user_create_id']) ? $data['user_create_id'] : $this->user->getId();
        /* update product */
        $this->db->query("UPDATE " . DB_PREFIX . "product 
                          SET demo = 0, 
                              sku = '" . $this->db->escape($data['sku']) . "', 
                              quantity = '" . 0 . "', 
                              price = '" . (float)str_replace(',', '', $data['promotion_price']) . "', 
                              price_currency_id = 1, 
                              compare_price = '" . (float)str_replace(',', '', $data['price']) . "', 
                              c_price_currency_id = 1, 
                              weight = '" . (float)$weight . "', 
                              weight_class_id = 1, 
                              status = '" . (int)$data['status'] . "', 
                              channel = '" . (isset($data['channel']) ? (int)$data['channel'] : 2) . "', 
                              manufacturer_id = '" . (int)$manufacturer_id . "', 
                              barcode = '" . $data['barcode'] . "', 
                              common_barcode = '" . $this->db->escape(getValueByKey($data, 'common_barcode')) . "', 
                              common_sku = '" . $this->db->escape(getValueByKey($data, 'common_sku')) . "', 
                              common_price = '" . (float)str_replace(',', '', getValueByKey($data, 'common_price')) . "', 
                              common_cost_price = '" . (float)str_replace(',', '', getValueByKey($data, 'common_cost_price')) . "', 
                              common_compare_price = '" . (float)str_replace(',', '', getValueByKey($data, 'common_compare_price')) . "', 
                              sale_on_out_of_stock = '" . (int)$sale_on_out_of_stock . "', 
                              multi_versions = '" . (int)$data['product_version'] . "', 
                              default_store_id = '" . (int)$default_store_id . "',
                              user_create_id = '". (int)$user_create_id ."',
                              show_gg_product_feed = '". $data['show_gg_product_feed'] ."',
                              date_modified = NOW() 
                              WHERE product_id = '" . (int)$product_id . "'");

        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();

        // update source
        if (!empty($data['source'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET `source` = '" . $this->db->escape($data['source']) . "' WHERE product_id = '" . (int)$product_id . "'");
        }

        $DB_PREFIX = DB_PREFIX;
        $sql = "DELETE FROM `{$DB_PREFIX}product_description_tab` WHERE product_id = " . (int)$product_id;
        $this->db->query($sql);

        /* update product description tab */
        if (isset($data['title_tab']) && isset($data['description_tab'])) {
            foreach ($data['title_tab'] as $key => $title_tab) {
                $description_tab = $data['description_tab'][$key];
                if ($title_tab != null) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_description_tab
                    SET product_id = '" . (int)$product_id . "', title = '" . $this->db->escape($title_tab) . "', description = '" . $this->db->escape($description_tab) . "'");
                }
            }
        }

        /* update main image */
        if (!empty($data['images'])) {
            if (!empty($data['images'][0])) {
                $alt = '';
                if (isset($data['image-alts'][0])) {
                    $alt = $data['image-alts'][0];
                    array_shift($data['image-alts']);
                }
                $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['images'][0]) . "', image_alt = '" . $this->db->escape($alt) . "' WHERE product_id = '" . (int)$product_id . "'");
            }
            array_shift($data['images']);
        } else {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '', image_alt = '' WHERE product_id = '" . (int)$product_id . "'");
        }

        /* update description */
        // remove first
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");

        // update
        if (isset($data['name'])) {
            $data['description'] = isset($data['description']) ? $data['description'] : '';
            $data['meta_keyword'] = isset($data['meta_keyword']) ? $data['meta_keyword'] : '';
            $language_id = (int)$this->config->get('config_language_id');
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description 
                              SET product_id = '" . (int)$product_id . "', 
                                  language_id = '" . (int)$language_id . "', 
                                  name = '" . $this->db->escape($data['name']) . "', 
                                  description = '" . $this->db->escape($data['description']) . "', 
                                  sub_description = '" . $this->db->escape($data['summary']) . "', 
                                  seo_title = '" . $this->db->escape($data['seo_title']) . "', 
                                  seo_description = '" . $this->db->escape($data['seo_desciption']) . "',
                                  meta_keyword = '" . $this->db->escape($data['meta_keyword']) . "',
                                  noindex = '" . $this->db->escape($data['noindex']) . "',
                                  nofollow = '" . $this->db->escape($data['nofollow']) . "',
                                  noarchive = '" . $this->db->escape($data['noarchive']) . "',
                                  noimageindex = '" . $this->db->escape($data['noimageindex']) . "',
                                  nosnippet = '" . $this->db->escape($data['nosnippet']) . "'");
        }

        /* update product version (attribute) values */
        if (!array_key_exists('shopee_update_product', $data) || $data['shopee_update_product'] != 'not_update') {
            // get all product attributes
            $this->load->model('catalog/attribute');
            $old_attributes = $this->model_catalog_attribute->getAttributes(['product_id' => $product_id]);
            $old_attributes = array_column($old_attributes, 'name');
            $old_attributes = array_map('mb_strtolower', $old_attributes);

            // remove first
            $this->deleteProductAttributes($product_id);

            // update
            if (isset($data['attribute_name']) && (int)$data['product_version'] == 1) {
                foreach ($data['attribute_name'] as $k => $attribute) {
                    $attribute = trim($attribute);
                    if ($attribute != '') {
                        $language_id = (int)$this->config->get('config_language_id'); //TV
                        $this->db->query("INSERT INTO " . DB_PREFIX . "attribute 
                                      SET attribute_group_id = 0, 
                                          sort_order = 0 ");

                        $attribute_id = $this->db->getLastId();
                        $this->db->query("INSERT INTO " . DB_PREFIX . "attribute_description 
                                      SET attribute_id = '" . (int)$attribute_id . "', 
                                          language_id = '" . (int)$language_id . "', 
                                          name = '" . $this->db->escape($attribute) . "'");

                        $attribute_description = implode(",", $data['attribute_values'][$k]);
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute 
                                      SET product_id = '" . (int)$product_id . "', 
                                          attribute_id = '" . (int)$attribute_id . "', 
                                          language_id = '" . (int)$language_id . "', 
                                          text = '" . $this->db->escape($attribute_description) . "'");

                        // remove old attribute from array to update attribute filter if it has not change (update when calling by product)
                        $old_attribute_key = array_search(mb_strtolower($attribute), $old_attributes);
                        if (false !== $old_attribute_key) {
                            unset($old_attributes[$old_attribute_key]);
                        }
                    }
                }
            }

            // update attribute filter vs old product attribute
            $this->load->model('catalog/attribute_filter');
            foreach ($old_attributes as $old_attribute) {
                $this->model_catalog_attribute_filter->updateAttributeFilterByName($old_attribute);
            }

            // add attribute filter
            $this->model_catalog_attribute_filter->updateAttributeFilterByProduct($product_id);

            /* update product version */
            // soft-delete first
            $this->db->query("UPDATE " . DB_PREFIX . "product_version 
                          SET deleted = 1 
                          WHERE product_id = '" . (int)$product_id . "'");
            $product_version_ids = [];
            if (isset($data['product_version_names']) && (int)$data['product_version'] == 1) {
                foreach ($data['product_version_names'] as $k => $version_name) {
                    if (isset($data['product_display'])) {
                        $display = in_array($version_name, $data['product_display']) ? 1 : 0;
                    } else {
                        $display = 0;
                    }

                    $product_version_image = isset($data['product_version_images'][$k]) ? $data['product_version_images'][$k] : '';
                    $product_version_image_alt = isset($data['product_version_image_alts'][$k]) ? $data['product_version_image_alts'][$k] : '';

                    $new_key = $this->replaceVersionValues($version_name);
                    $version_name = implode(',', explode(' • ', $version_name));
                    if (isset($data['product_version_ids'][$k]) && !empty($data['product_version_ids'][$k])) {
                        $this->db->query("DELETE FROM " . DB_PREFIX . "product_version 
                                            WHERE product_id = '" . (int)$product_id . "' 
                                                AND product_version_id = '" . (int)$data['product_version_ids'][$k] . "'");
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_version
                                            SET product_version_id = '" . (int)$data['product_version_ids'][$k] . "',
                                                product_id = '" . (int)$product_id . "',
                                                version = '" . $this->db->escape($version_name) . "',
                                                price = '" . $this->db->escape(str_replace(',', '', $data['product_promotion_prices'][$k])) . "',
                                                compare_price = '" . $this->db->escape(str_replace(',', '', $data['product_prices'][$k])) . "',
                                                sku = '" . $this->db->escape($data['product_skus'][$k]) . "',
                                                barcode = '" . $this->db->escape($data['product_barcodes'][$k]) . "',
                                                quantity = '" . $this->db->escape($data['product_quantities'][$k]) . "',
                                                status = '" . (int)$display . "',
                                                `image` = '" . $this->db->escape($product_version_image) . "',
                                                `image_alt` = '" . $this->db->escape($product_version_image_alt) . "'");
                        $product_version_id = $this->db->getLastId();
                    } else {
                        $product_version_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_version 
                                                                    WHERE product_id = '" . (int)$product_id . "' 
                                                                        AND version ='" . $version_name . "'");
                        $product_version = $product_version_query->row;
                        if ($product_version) {
                            $this->db->query("UPDATE " . DB_PREFIX . "product_version 
                                                SET product_id = '" . (int)$product_id . "', 
                                                    version = '" . $this->db->escape($version_name) . "', 
                                                    price = '" . $this->db->escape(str_replace(',', '', $data['product_promotion_prices'][$k])) . "', 
                                                    compare_price = '" . $this->db->escape(str_replace(',', '', $data['product_prices'][$k])) . "', 
                                                    sku = '" . $this->db->escape($data['product_skus'][$k]) . "', 
                                                    barcode = '" . $this->db->escape($data['product_barcodes'][$k]) . "', 
                                                    quantity = '" . $this->db->escape($data['product_quantities'][$k]) . "', 
                                                    status = '" . (int)$display . "', 
                                                    deleted = NULL 
                                                    WHERE product_version_id = '" . (int)$product_version['product_version_id'] . "'");
                            $product_version_id = $product_version['product_version_id'];
                        } else {
                            $this->db->query("INSERT INTO " . DB_PREFIX . "product_version 
                                                SET product_id = '" . (int)$product_id . "',
                                                    version = '" . $this->db->escape($version_name) . "',
                                                    price = '" . $this->db->escape(str_replace(',', '', $data['product_promotion_prices'][$k])) . "',
                                                    compare_price = '" . $this->db->escape(str_replace(',', '', $data['product_prices'][$k])) . "',
                                                    sku = '" . $this->db->escape($data['product_skus'][$k]) . "',
                                                    barcode = '" . $this->db->escape($data['product_barcodes'][$k]) . "',
                                                    quantity = '" . $this->db->escape($data['product_quantities'][$k]) . "',
                                                    status = '" . (int)$display . "',
                                                    `image` = '" . $this->db->escape($product_version_image) . "',
                                                    `image_alt` = '" . $this->db->escape($product_version_image_alt) . "'");
                            $product_version_id = $this->db->getLastId();
                        }
                    }
                    $product_version_ids[$new_key] = $product_version_id;
                }
            }
        }

        /* update additional images */
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");
        if (isset($data['images'])) {
            foreach ($data['images'] as $k => $image) {
                $alt = isset($data['image-alts'][$k]) ? $data['image-alts'][$k] : '';
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($image) . "', image_alt = '" . $this->db->escape($alt) . "', sort_order = " . (int)$k);
            }
        }

        /* update product-category */
        if (!array_key_exists('shopee_update_product', $data) || $data['shopee_update_product'] != 'not_update') {
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
            if (isset($data['category'])) {
                foreach ($data['category'] as $category_id) {
                    if (strpos($category_id, 'add_new_') !== false) {
                        $this->load->model('catalog/category');
                        $category_id = $this->model_catalog_category->addCategoryFast(str_replace('add_new_', '', $category_id));
                    }
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
                }
            }
        }

        /* update product-collection */
        // IMPORTANT: if delete collection here, product may lost collection if no collection set!
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_collection WHERE product_id = '" . (int)$product_id . "'");
        if (isset($data['collection_list'])) {
            foreach ($data['collection_list'] as $collection_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_collection SET product_id = '" . (int)$product_id . "', collection_id = '" . (int)$collection_id . "'");
            }
        }

        /* update product-tag */
        // IMPORTANT: if delete tag here, product may lost tag if no collection set!
        if (isset($data['tag_list'])) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_tag WHERE product_id = '" . (int)$product_id . "'");
            foreach ($data['tag_list'] as $tag_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_tag SET product_id = '" . (int)$product_id . "', tag_id = '" . (int)$tag_id . "'");
            }
        }

        /* update product to store */
        if (!array_key_exists('shopee_update_product', $data) || $data['shopee_update_product'] != 'not_update') {
            // remove first
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store 
                          WHERE product_id = '" . (int)$product_id . "'");

            // update
            if (isset($data['stores'])) {
                if (!empty($product_version_ids)) {
                    foreach ($product_version_ids as $key => $pv_id) {
                        if (!isset($data['stores'][$key]) || !is_array($data['stores'][$key])) {
                            continue;
                        }

                        foreach ($data['stores'][$key] as $store_key => $store_id) {
                            $item_quantity = isset($data['original_inventory'][$key][$store_key]) ? (int)$data['original_inventory'][$key][$store_key] : 0;
                            $item_cost_price = isset($data['cost_price'][$key][$store_key]) ? (float)extract_number($data['cost_price'][$key][$store_key]) : 0;
                            $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store 
                                          SET `product_id` = '" . (int)$product_id . "', 
                                              `store_id` = '" . (int)$store_id . "',
                                              `product_version_id` = '" . (int)$pv_id . "', 
                                              `quantity` = '" . $item_quantity . "', 
                                              `cost_price` = '" . $item_cost_price . "'");
                        }
                    }
                } else {
                    $list_used = [];
                    foreach ($data['stores'] as $key => $store) {
                        $store_id = (isset($store[0])) ? $store[0] : 0;
                        $original_inventory = (isset($data['original_inventory'][$key][0])) ? $data['original_inventory'][$key][0] : 0;
                        $cost_price = (isset($data['cost_price'][$key][0])) ? $data['cost_price'][$key][0] : 0;

                        if (in_array($store_id, $list_used)) {
                            continue;
                        }

                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store 
                                      SET `product_id` = '" . (int)$product_id . "', 
                                          `store_id` = '" . (int)$store_id . "', 
                                          `product_version_id` = '" . (int)0 . "', 
                                          `quantity` = '" . (int)$original_inventory . "', 
                                          `cost_price` = '" . (float)extract_number($cost_price) . "'");
                        $list_used[] = $store_id;
                    }
                }
            }
        }

        /* update seo */
        // IMPORTANT: if delete seo here, product may lost seo if no alias set!
        if (isset($data['alias'])) {
            // get current seo url
            $DB_PREFIX = DB_PREFIX;
            $sql = "SELECT * FROM `{$DB_PREFIX}seo_url` WHERE query = 'product_id={$product_id}' LIMIT 1";
            $current_seo_url_query = $this->db->query($sql);

            // delete first. IMPORTANT: only delete if after this always re-creating new seo!
            $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url 
                          WHERE query = 'product_id=" . (int)$product_id . "'");

            // update (auto if seo alias empty)
            if (trim($data['alias']) == '') {
                $data['alias'] = $data['name'];
            }

            $this->load->model('custom/common');
            $slug = $this->model_custom_common->createSlug($data['alias'], '-');
            $slug_custom = $this->model_custom_common->getSlugUnique($slug);

            // save product should change sitemap to session
            if ($current_seo_url_query->row) {
                $this->session->data['product_' . $product_id . '_change_sitemap'] = $current_seo_url_query->row['keyword'] != $slug_custom;
            }

            // create new seo product
            foreach ($languages as $key => $language) {
                $language_id = $language['language_id'];
                $alias = 'product_id=' . $product_id;
                $sql = "INSERT INTO " . DB_PREFIX . "seo_url 
                        SET language_id = '" . (int)$language_id . "', 
                            query = '" . $alias . "', 
                            keyword = '" . $slug_custom . "', 
                            table_name = 'product'";
                $this->db->query($sql);
            }
        }

        if (!empty($data['ingredient_id']) && is_array($data['ingredient_id'])) {
            $DB_PREFIX = DB_PREFIX;
            try {
                // delete first
                $this->db->query("DELETE FROM `{$DB_PREFIX}product_ingredient` WHERE `product_id` = {$product_id}");

                // add new
                $sql = "INSERT INTO `{$DB_PREFIX}product_ingredient` (product_id, ingredient_id, quantitative) VALUES ";
                foreach ($data['ingredient_id'] as $key => $item_id) {
                    $quantitative = (isset($data['quantitative'][$key])) ? $data['quantitative'][$key] : '';

                    if ($key == (count($data['ingredient_id']) - 1)) {
                        $sql .= " (" . (int)$product_id . ", " . (int)$item_id . ", '" . $quantitative . "');";
                        break;
                    }

                    $sql .= " (" . (int)$product_id . ", " . (int)$item_id . ", '" . $quantitative . "'),";

                }

                $this->db->query($sql);
            } catch (Exception $exception) {
                $this->log->write('[Edit product ingredient] error : ' . $exception->getMessage());
            }
        }

        /* update related order_products and order */
//        $this->load->model('sale/order');
//        $this->model_sale_order->updateRelatedOrderProducts($product_id);

        // update connection product and product_lazada
        $new_product = $this->getProductForLazada($product_id);
        $new_product_version = $this->getProductVersions($product_id);

        $this->load->model('catalog/lazada');
        $this->model_catalog_lazada->disconnectWhenProductChange($old_product, $new_product, $old_product_version, $new_product_version);

        $this->load->model('catalog/shopee');
        $this->model_catalog_shopee->disconnectWhenProductChange($old_product, $new_product, $old_product_version, $new_product_version);

        $this->deleteProductCaches();

        // send product to rabbit mq
        $source_product = !empty($data['source']) ? $data['source'] : '';

        try {
            if ($source_product != 'excel') {
                $this->sendToRabbitMq($product_id);
            }
        } catch (Exception $exception) {
            $this->log->write('Send message edit product error: ' . $exception->getMessage());
        }
    }

    /**
     * delete all attributes belong to a product
     * @param $product_id
     */
    private function deleteProductAttributes($product_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "DELETE FROM `{$DB_PREFIX}product_attribute` WHERE product_id = " . (int)$product_id;
        $this->db->query($sql);

        // delete attribute description
        $sql = "DELETE FROM `{$DB_PREFIX}attribute_description` WHERE `attribute_id` IN (SELECT `attribute_id` FROM `{$DB_PREFIX}product_attribute` WHERE product_id = '" . (int)$product_id . "')";
        $this->db->query($sql);

        // delete attribute
        $sql = "DELETE FROM `{$DB_PREFIX}attribute` WHERE `attribute_id` IN (SELECT `attribute_id` FROM `{$DB_PREFIX}product_attribute` WHERE product_id = '" . (int)$product_id . "')";
        $this->db->query($sql);

        $this->deleteProductCaches();
    }

    public function getValueArrayImage($array)
    {
        $data = [];
        foreach ($array as $value) {
            $data[] = $value['image'];
        }

        return $data;
    }

    public function copyProduct($product_id)
    {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p WHERE p.product_id = '" . (int)$product_id . "'");
        $productDes = $this->getProduct($product_id);
        $productName = $productDes['name'];
        if ($query->num_rows) {
            $productOld = $data = $query->row;

            $data['sku'] = $productDes['sku'];
            $data['upc'] = '';
            $data['viewed'] = '0';
            $data['keyword'] = '';
            // $data['status'] = '0';

            $data['product_description'] = $this->getProductDescriptions($product_id);
            $data['product_discount'] = $this->getProductDiscounts($product_id);

            // $data['product_filter'] = $this->getProductFilters($product_id);
            // $data['product_attribute'] = $this->getProductAttributes($product_id);

            $data['product_image'] = $this->getProductImages($product_id);

            $data['product_option'] = $this->getProductOptions($product_id);
            $data['product_related'] = $this->getProductRelated($product_id);
            $data['product_reward'] = $this->getProductRewards($product_id);
            $data['product_special'] = $this->getProductSpecials($product_id);
            $data['product_category'] = $this->getProductCategories($product_id);
            $data['categories'] = $this->getProductCategories($product_id);
            $data['product_download'] = $this->getProductDownloads($product_id);
            $data['product_layout'] = $this->getProductLayouts($product_id);
            $data['product_store'] = $this->getProductStores($product_id);
            $data['product_recurrings'] = $this->getRecurrings($product_id);
            $data['quantity_import'] = $productOld['quantity'];
            $data['price_unit'] = $productOld['price_currency_id'];
            $data['compare_price_unit'] = $productOld['c_price_currency_id'];
            $data['weight_unit'] = $productOld['weight_class_id'];
            $data['name'] = $productName;
            $data['images'] = $this->getValueArrayImage($this->getProductImages($product_id));
            $data['description'] = $productDes['description'];

            $data['common_barcode'] = $productDes['common_barcode'];
            $data['common_sku'] = $productDes['common_sku'];
            $data['common_compare_price'] = $productDes['common_compare_price'];
            $data['common_price'] = $productDes['common_price'];

            $productNewId = $this->addProduct($data);
            //update filter, manufacture, attribute
            $this->load->model('localisation/language');
            $languages = $this->model_localisation_language->getLanguages();
            foreach ($this->getProductAttributes($product_id) as $attribute) {
                if (isset($attribute['product_attribute_description']['attribute_id'])) {
                    $attribute_id = $attribute['product_attribute_description']['attribute_id'];
                    $text = $attribute['product_attribute_description']['text'];
                    $text = trim($text);

                    foreach ($languages as $language) {
                        $language_id = $language['language_id'];
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$productNewId . "', attribute_id = '" . (int)$attribute_id . "', language_id = '" . (int)$language_id . "', text = '" . $this->db->escape($text) . "'");
                    }
                }
            }
            //filter
            // var_dump($this->getProductFilters($product_id));
            foreach ($this->getProductFilters($product_id) as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int)$productNewId . "', filter_id = '" . (int)$filter_id . "'");
            }
            //manufacture
            $this->db->query("UPDATE " . DB_PREFIX . "product SET manufacturer_id = '" . $productOld['manufacturer_id'] . "' WHERE product_id = '" . (int)$productNewId . "'");

            $this->deleteProductCaches();
        }
    }

    public function deleteProduct($product_id, $remove_from_db = false)
    {
        $this->sendToRabbitMqModifyProduct($product_id, 'delete');
        /* update attribute filter by attribute names */
        // get all product attributes
        $this->load->model('catalog/attribute');
        $attributes = $this->model_catalog_attribute->getAttributes(['product_id' => $product_id]);

        if ($remove_from_db) {
            $this->db->query("DELETE FROM `" . DB_PREFIX . "product` WHERE product_id = '" . (int)$product_id . "'");
            $this->db->query("DELETE FROM `" . DB_PREFIX . "product_version` WHERE product_id = '" . (int)$product_id . "'");

            // delete product attribute
            $this->deleteProductAttributes($product_id);

            // delete product description
            $sql = "DELETE FROM `" . DB_PREFIX . "product_description` WHERE `product_id` = '" . (int)$product_id . "'";
            $this->db->query($sql);
        } else {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET `deleted` = 1 WHERE product_id = '" . (int)$product_id . "'");
            $this->db->query("UPDATE `" . DB_PREFIX . "product_version` SET `deleted` = 1 WHERE product_id = '" . (int)$product_id . "'");
            // delete product attribute
            $this->model_catalog_attribute->removeUnusedProductAttributes($product_id);
        }
        // update attribute filters of product after deleting product attributes
        if (!empty($attributes)) {
            $this->load->model('catalog/attribute_filter');
            foreach ($attributes as $attribute) {
                $this->model_catalog_attribute_filter->updateAttributeFilterByName($attribute['name']);
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_recurring WHERE product_id = " . (int)$product_id);
        $this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'product_id=" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "coupon_product WHERE product_id = '" . (int)$product_id . "'");
        // delete in relation table
        $this->db->query("DELETE FROM " . DB_PREFIX . "novaon_relation_table WHERE child_id = '" . (int)$product_id . "' AND child_name = 'product'");

        // delete in warehouse table, include multi versions
        // TODO: remove warehouse
        // $this->db->query("DELETE FROM " . DB_PREFIX . "warehouse WHERE product_id = '" . (int)$product_id . "'");
        // NO NEED, we have soft-delete product, and query products already check product IS NOT NULL
        //$this->db->query("DELETE FROM " . DB_PREFIX . "warehouse WHERE product_id = '" . (int)$product_id . "'");

        // delete tag
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_tag WHERE product_id = '" . (int)$product_id . "'");

        // delete collection
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_collection WHERE product_id = '" . (int)$product_id . "'");

        $this->load->model('catalog/lazada');
        $this->model_catalog_lazada->disconnectWhenProductDeleted($product_id);

        $this->load->model('catalog/shopee');
        $this->model_catalog_shopee->disconnectWhenProductDeleted($product_id);

        $this->deleteProductCaches();
    }

    public function sendToRabbitMqModifyProduct($product_id, $action = 'delete')
    {
        try {
            if (!empty($product_id)) {
                $message = [
                    'shop_name' => $this->config->get('shop_name'),
                    'product_sku' => '',
                    'action' => $action
                ];

                $infoProduct = $this->getProduct($product_id);
                $sku = !empty($infoProduct['common_sku']) ? $infoProduct['common_sku'] : $infoProduct['sku'];

                if (!empty($sku)) {
                    $message['product_sku'] = $sku;

                    $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'modify_product', '', 'modify_product', json_encode($message));
                }
            }
        } catch (Exception $exception) {
            $this->log->write('Send message delete product error: ' . $exception->getMessage());
        }
    }

    /**
     * hard delete deleted products whose id coincides new demo data
     * @param $product_ids
     * @return bool
     */
    public function hardDeleteDeletedProducts($product_ids) {
        if (empty($product_ids)) {
            return false;
        }

        $DB_PREFIX = DB_PREFIX;
        $product_ids_query = implode(',', $product_ids);
        $sql = "SELECT p.`product_id` FROM `{$DB_PREFIX}product` p WHERE p.`product_id` IN ({$product_ids_query}) AND p.`deleted` = '1'";
        $query = $this->db->query($sql);

        foreach ($query->rows as $product) {
            if (!array_key_exists('product_id', $product)) {
                continue;
            }

            $this->deleteProduct($product['product_id'], true);
        }

        $this->deleteProductCaches();

        return true;
    }

    /**
     * get product by id
     *
     * @param $product_id
     * @param bool|null $flag_deleted true=only deleted, false=only not deleted, null=all. Default null for all products
     * @param int|null $status 0=hidden, 1=visible, null=all statuses. Default null for all statuses
     * @return mixed
     */
    public function getProduct($product_id, $flag_deleted = false, $status = null)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT DISTINCT * FROM {$DB_PREFIX}product p 
                LEFT JOIN {$DB_PREFIX}product_description pd 
                       ON (p.product_id = pd.product_id) 
                WHERE p.product_id = '" . (int)$product_id . "' 
                  AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' ";

        if ($flag_deleted !== null) {
            $sql .= " AND p.`deleted` IS " . ($flag_deleted ? "NOT NULL" : "NULL");
        }

        if ($status !== null) {
            $sql .= " AND p.`status` = " . (int)$status;
        }

        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getProductForRabbitMq($product_id, $flag_deleted = false, $status = null)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT DISTINCT p.*, pd.*, CONCAT(u.lastname, ' ', u.firstname) as fullname FROM {$DB_PREFIX}product p 
                LEFT JOIN {$DB_PREFIX}product_description pd 
                       ON (p.product_id = pd.product_id) 
                LEFT JOIN {$DB_PREFIX}user u ON (u.user_id = p.user_create_id) 
                WHERE p.product_id = '" . (int)$product_id . "' 
                  AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' ";

        if ($flag_deleted !== null) {
            $sql .= " AND p.`deleted` IS " . ($flag_deleted ? "NOT NULL" : "NULL");
        }

        if ($status !== null) {
            $sql .= " AND p.`status` = " . (int)$status;
        }

        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getProductDescriptionTab($product_id) {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM {$DB_PREFIX}product_description_tab pdt
                WHERE pdt.product_id = '" . (int)$product_id . "'";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getProductForOrder($product_id, $product_version_id, $flag_deleted = true)
    {
        $sql = "SELECT *, pts.quantity as store_quantity, p.status as p_status FROM " . DB_PREFIX . "product p 
                LEFT JOIN " . DB_PREFIX . "product_version pv ON CASE WHEN p.multi_versions = 1 THEN p.product_id ELSE -1 END = pv.product_id  
                LEFT JOIN " . DB_PREFIX . "product_to_store pts ON (p.product_id = pts.product_id AND p.default_store_id = pts.store_id AND pts.product_version_id = " . $product_version_id . ") 
                WHERE 1 
                AND pts.product_version_id = " . (int)$product_version_id . " 
                AND p.product_id = '" . (int)$product_id . "' ";
        if ($flag_deleted) {
            $sql .= " AND p.`deleted` IS NULL";
        }

        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getProductQuantityForOrder($order_id, $product_id, $product_version_id)
    {
        $sql = "SELECT `quantity` FROM `" . DB_PREFIX . "order_product` WHERE `order_id` = " . (int)$order_id . " AND `product_id` = " . (int)$product_id . " AND `product_version_id` = " . (int)$product_version_id;

        $query = $this->db->query($sql);
        if ($query->num_rows > 0) {
            return (int)$query->row['quantity'];
        }

        return 0;
    }

    public function getProducts($data = array())
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT *, 
                       pd.name as product_name, 
                       p.image as product_image 
                FROM {$DB_PREFIX}product p 
                LEFT JOIN {$DB_PREFIX}product_description pd ON (p.product_id = pd.product_id)";

        if (isset($data['user_store_id']) && !empty($data['user_store_id'])) {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_to_store pts ON (p.`product_id` = pts.`product_id`)";
        }

        $sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (isset($data['current_user_id']) && !empty($data['current_user_id'])) {
            $sql .= " AND p.user_create_id = '" . (int)$data['current_user_id'] . "'";
        }

        if (isset($data['user_store_id']) && !empty($data['user_store_id'])) {
            $sql .= " AND pts.store_id IN (" . $data['user_store_id'] . ")";
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_model'])) {
            $sql .= " AND p.model LIKE '" . $this->db->escape($data['filter_model']) . "%'";
        }

        if (!empty($data['filter_price'])) {
            $sql .= " AND p.price LIKE '" . $this->db->escape($data['filter_price']) . "%'";
        }

        if (isset($data['filter_quantity']) && $data['filter_quantity'] !== '') {
            $sql .= " AND p.quantity = '" . (int)$data['filter_quantity'] . "'";
        }

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
        }

        $sql .= " AND p.`deleted` IS NULL ";

        $sql .= " GROUP BY p.product_id";

        $sort_data = array(
            'pd.name',
            'p.model',
            'p.price',
            'p.quantity',
            'p.status',
            'p.date_modified',
            'p.sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY p.date_modified";
        }

        if (isset($data['order']) && ($data['order'] == 'ASC')) {
            $sql .= " ASC";
        } else {
            $sql .= " DESC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getProductsByCategoryId($category_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2c.category_id = '" . (int)$category_id . "' AND p.deleted IS NULL ORDER BY pd.name ASC");

        return $query->rows;
    }

    public function getProductDescriptions($product_id)
    {
        $product_description_data = array();
        $language_id = $this->config->get('config_language_id');
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'description' => $result['description'],
                'sub_description' => $result['sub_description'],
                'seo_title' => $result['seo_title'],
                'seo_description' => $result['seo_description'],
                'meta_keyword' => $result['meta_keyword'],
                'meta_title' => $result['meta_title'],
                'meta_description' => $result['meta_description'],
                'meta_keyword' => $result['meta_keyword'],
                'noindex' => $result['noindex'],
                'nofollow' => $result['nofollow'],
                'noarchive' => $result['noarchive'],
                'noimageindex' => $result['noimageindex'],
                'nosnippet' => $result['nosnippet'],
                'tag' => $result['tag']
            );
        }

        return $product_description_data[$language_id];
    }

    public function getProductNameById($product_id)
    {
        $query = $this->db->query("SELECT name FROM " . DB_PREFIX . "product_description WHERE product_id = " . (int)$product_id);
        $data = $query->row;
        if (count($data) > 0) {
            return $data['name'];
        }

        return null;
    }

    public function getProductSKUById($product_id)
    {
        $query = $this->db->query("SELECT `sku` FROM " . DB_PREFIX . "product WHERE product_id = " . (int)$product_id);
        $data = $query->row;
        if (count($data) > 0) {
            return $data['sku'];
        }

        return null;
    }

    public function getProductImageById($product_id)
    {
        $query = $this->db->query("SELECT image FROM " . DB_PREFIX . "product WHERE product_id = " . (int)$product_id);
        $data = $query->row;
        if (count($data) > 0) {
            return $data['image'];
        }

        return null;
    }

    public function getProductCategories($product_id, $flag = true)
    {
        $product_category_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category as pct INNER JOIN " . DB_PREFIX . "category_description as cd ON pct.category_id = cd.category_id WHERE pct.product_id = '" . (int)$product_id . "' AND cd.language_id='" . (int)$this->config->get('config_language_id') . "'");

        foreach ($query->rows as $result) {
            if ($flag) {
                $product_category_data[] = $result['category_id'];
            } else {
                $product_category_data[] = array(
                    'id' => $result['category_id'],
                    'title' => $result['name']
                );
            }
        }

        return $product_category_data;
    }

    public function getCategoryIdsByProductId($product_id)
    {
        $query = $this->db->query("SELECT pct.category_id FROM " . DB_PREFIX . "product_to_category as pct 
                                   INNER JOIN " . DB_PREFIX . "category_description as cd ON pct.category_id = cd.category_id 
                                   WHERE pct.product_id = '" . (int)$product_id . "' 
                                   AND cd.language_id='" . (int)$this->config->get('config_language_id') . "'");

        return $query->rows;
    }

    public function getProductCollections($product_id, $flag = true)
    {
        $product_collection_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_collection as pct INNER JOIN " . DB_PREFIX . "collection_description as cd ON pct.collection_id = cd.collection_id WHERE pct.product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            if ($flag) {
                $product_collection_data[] = $result['collection_id'];
            } else {
                $product_collection_data[] = array(
                    'id' => $result['collection_id'],
                    'title' => $result['description']
                );
            }
        }

        return $product_collection_data;
    }

    public function getProductCollectionTitles($product_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_collection as pc INNER JOIN " . DB_PREFIX . "collection as c ON pc.collection_id = c.collection_id WHERE pc.product_id = '" . (int)$product_id . "'");

        $collection_titles = [];
        foreach ($query->rows as $row) {
            if (!isset($row['title']) || !trim($row['title'])) {
                continue;
            }

            $collection_titles[] = trim($row['title']);
        }

        return $collection_titles;
    }

    /**
     * @param $product_id
     * @return array array of category names, NOT ARRAY OF CATEGORY OBJECTS
     */
    public function getProductCategoryName($product_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
        $this->load->model('catalog/category');
        $results = $query->rows;
        $category = array();
        if (!empty($results)) {
            foreach ($results as $result) {
                $cat = $this->model_catalog_category->getCategory($result['category_id']);
                if (!isset($cat['name'])) {
                    continue;
                }

                $category[] = $cat['name'];
            }
        }

        return $category;
    }

    public function getProductFilters($product_id)
    {
        $product_filter_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_filter_data[] = $result['filter_id'];
        }

        return $product_filter_data;
    }

    public function getProductAttributes($product_id)
    {
        $product_attribute_data = array();

        $product_attribute_query = $this->db->query("SELECT attribute_id FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' GROUP BY attribute_id");

        foreach ($product_attribute_query->rows as $product_attribute) {
            // $product_attribute_description_data = array();
            $product_attribute_description_data = '';

            $product_attribute_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");
            // var_dump($product_attribute['attribute_id']);die;
            // var_dump(count($product_attribute_description_query->rows));die;

            // foreach ($product_attribute_description_query->rows as $product_attribute_description) {
            // $product_attribute_description_data[$product_attribute_description['language_id']] = array('text' => $product_attribute_description['text']);
            // $product_attribute_description_data = $product_attribute_description['text']);
            // var_dump($product_attribute_description['text']);die;
            // }
            $result = $product_attribute_description_query->row;
            if (count($result) > 0) {
                $product_attribute_description_data = $result;
            }
            $product_attribute_data[] = array(
                'attribute_id' => $product_attribute['attribute_id'],
                'product_attribute_description' => $product_attribute_description_data
            );
        }

        // var_dump($product_attribute_data);die;
        return $product_attribute_data;
    }

    public function getProductAttributeNamesAndValues($product_id)
    {
        $product_attribute_query = $this->db->query(sprintf("SELECT * FROM %s 
                                                             WHERE product_id = %s 
                                                             GROUP BY attribute_id",
                DB_PREFIX . 'product_attribute',
                (int)$product_id
            )
        );

        $product_attributes = $product_attribute_query->rows;

        $attributeIds = array_map(function ($pa) {
            return $pa['attribute_id'];
        }, $product_attributes);

        if (empty($attributeIds)) {
            return [];
        }

        $attribute_query = $this->db->query(sprintf("SELECT * 
                                                FROM %s a 
                                                LEFT JOIN %s ad ON (a.attribute_id = ad.attribute_id) 
                                                WHERE a.attribute_id IN (%s)
                                                AND ad.language_id = '%s'",
                DB_PREFIX . 'attribute',
                DB_PREFIX . 'attribute_description',
                implode(',', $attributeIds),
                (int)$this->config->get('config_language_id')
            )
        );

        $product_attribute_map = [];
        foreach ($product_attributes as $product_attribute) {
            $product_attribute_map[$product_attribute['attribute_id']] = $product_attribute;
        }

        $attributes = $attribute_query->rows;
        foreach ($attributes as &$attribute) {
            $attribute['text'] = isset($product_attribute_map[$attribute['attribute_id']]['text']) ? $product_attribute_map[$attribute['attribute_id']]['text'] : '';
        }
        unset($attribute);

        return $attributes;
    }

    public function getProductOptions($product_id)
    {
        $product_option_data = array();

        $product_option_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_option` po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN `" . DB_PREFIX . "option_description` od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        foreach ($product_option_query->rows as $product_option) {
            $product_option_value_data = array();

            $product_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON(pov.option_value_id = ov.option_value_id) WHERE pov.product_option_id = '" . (int)$product_option['product_option_id'] . "' ORDER BY ov.sort_order ASC");

            foreach ($product_option_value_query->rows as $product_option_value) {
                $product_option_value_data[] = array(
                    'product_option_value_id' => $product_option_value['product_option_value_id'],
                    'option_value_id' => $product_option_value['option_value_id'],
                    'quantity' => $product_option_value['quantity'],
                    'subtract' => $product_option_value['subtract'],
                    'price' => $product_option_value['price'],
                    'price_prefix' => $product_option_value['price_prefix'],
                    'points' => $product_option_value['points'],
                    'points_prefix' => $product_option_value['points_prefix'],
                    'weight' => $product_option_value['weight'],
                    'weight_prefix' => $product_option_value['weight_prefix']
                );
            }

            $product_option_data[] = array(
                'product_option_id' => $product_option['product_option_id'],
                'product_option_value' => $product_option_value_data,
                'option_id' => $product_option['option_id'],
                'name' => $product_option['name'],
                'type' => $product_option['type'],
                'value' => $product_option['value'],
                'required' => $product_option['required']
            );
        }

        return $product_option_data;
    }

    public function getProductOptionValue($product_id, $product_option_value_id)
    {
        $query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_id = '" . (int)$product_id . "' AND pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getProductImages($product_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "' ORDER BY sort_order ASC");

        // $query = $this->db->query("SELECT image FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "' ORDER BY sort_order ASC");


        return $query->rows;
    }

    public function getProductDiscounts($product_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' ORDER BY quantity, priority, price");

        return $query->rows;
    }

    public function getProductSpecials($product_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' ORDER BY priority, price");

        return $query->rows;
    }

    public function getProductRewards($product_id)
    {
        $product_reward_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_reward_data[$result['customer_group_id']] = array('points' => $result['points']);
        }

        return $product_reward_data;
    }

    public function getProductDownloads($product_id)
    {
        $product_download_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_download_data[] = $result['download_id'];
        }

        return $product_download_data;
    }

    public function getProductStores($product_id)
    {
        $product_store_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_store_data[] = $result['store_id'];
        }

        return $product_store_data;
    }

    public function getProductToStores($product_id, $product_version_id = 0)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_store pts 
                                   LEFT JOIN " . DB_PREFIX . "store s ON (pts.store_id = s.store_id)
                                   WHERE pts.product_id = '" . (int)$product_id . "' AND pts.product_version_id = '" . (int)$product_version_id . "'");

        $prodStores = [];
        $list_store_id = [];
        foreach ($query->rows as $result) {
            $result['cost_price'] = round((float)$result['cost_price']);
            $list_store_id[] = $result['store_id'];
            $prodStores[] = $result;
        }

        $store_receipts = $this->getStoresInStoreReceipt($product_id, $product_version_id);
        foreach ($store_receipts as $store) {
            if (!isset($store['store_id'])) {
                continue;
            }

            if (!in_array($store['store_id'], $list_store_id)) {
                $prodStores[] = array(
                    'store_id' => $store['store_id'],
                    'product_id' => $product_id,
                    'product_version_id' => $product_version_id,
                    'quantity' => 0,
                    'cost_price' => 0,
                    'name' => $store['name']
                );
            }
        }

        return $prodStores;
    }

    public function getStoresInStoreReceipt($product_id, $product_version_id = 0)
    {
        $sql = "SELECT DISTINCT sr.store_id, s.name FROM `" . DB_PREFIX . "store_receipt_to_product` srtp 
                                    LEFT JOIN `" . DB_PREFIX . "store_receipt` sr ON sr.store_receipt_id = srtp.store_receipt_id 
                                    LEFT JOIN `" . DB_PREFIX . "store` s ON s.store_id = sr.store_id 
                                    WHERE sr.status = 0 AND srtp.product_id =" . (int)$product_id . " AND srtp.product_version_id = " . (int)$product_version_id;

        return $this->db->query($sql);
    }

    public function getProductDelivery($store_id, $product_id, $product_version_id = 0)
    {
        $product_version_id = $product_version_id ? $product_version_id : 0;

        $query = $this->db->query("SELECT SUM(srtp.`quantity`) as sum_quantity FROM `" . DB_PREFIX . "store_receipt` sr  
                                   LEFT JOIN `" . DB_PREFIX . "store_receipt_to_product` srtp ON srtp.`store_receipt_id` = sr.`store_receipt_id`  
                                   WHERE sr.`status` = 0 AND sr.`store_id` = " . (int)$store_id . " AND srtp.`product_id` = " . (int)$product_id . "  
                                   AND srtp.`product_version_id` = " . (int)$product_version_id);

        if ($query->num_rows > 0) {
            return (int)$query->row['sum_quantity'];
        }

        return 0;
    }

    public function getProductSeoUrls($product_id)
    {
        $product_seo_url_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query = 'product_id=" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_seo_url_data[$result['store_id']][$result['language_id']] = $result['keyword'];
        }

        return $product_seo_url_data;
    }

    public function getProductLayouts($product_id)
    {
        $product_layout_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_layout_data[$result['store_id']] = $result['layout_id'];
        }

        return $product_layout_data;
    }

    public function getProductRelated($product_id)
    {
        $product_related_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_related_data[] = $result['related_id'];
        }

        return $product_related_data;
    }

    public function getRecurrings($product_id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_recurring` WHERE product_id = '" . (int)$product_id . "'");

        return $query->rows;
    }

    public function getTotalProducts($data = array())
    {
        $sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";

        $sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_model'])) {
            $sql .= " AND p.model LIKE '" . $this->db->escape($data['filter_model']) . "%'";
        }

        if (isset($data['filter_price']) && !is_null($data['filter_price'])) {
            $sql .= " AND p.price LIKE '" . $this->db->escape($data['filter_price']) . "%'";
        }

        if (isset($data['filter_quantity']) && $data['filter_quantity'] !== '') {
            $sql .= " AND p.quantity = '" . (int)$data['filter_quantity'] . "'";
        }

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalProductsByDate($data = array())
    {
        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product ";

        if (!empty($data['filter_date_added'])) {
            $sql .= "WHERE DATE(date_added) <= DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalProductsByTaxClassId($tax_class_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE tax_class_id = '" . (int)$tax_class_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByStockStatusId($stock_status_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE stock_status_id = '" . (int)$stock_status_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByWeightClassId($weight_class_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE weight_class_id = '" . (int)$weight_class_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByLengthClassId($length_class_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE length_class_id = '" . (int)$length_class_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByDownloadId($download_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_download WHERE download_id = '" . (int)$download_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByManufacturerId($manufacturer_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE manufacturer_id = '" . (int)$manufacturer_id . "' AND deleted IS NULL ");

        return $query->row['total'];
    }

    public function getTotalProductsByAttributeId($attribute_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_attribute WHERE attribute_id = '" . (int)$attribute_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByOptionId($option_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_option WHERE option_id = '" . (int)$option_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByProfileId($recurring_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_recurring WHERE recurring_id = '" . (int)$recurring_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByLayoutId($layout_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

        return $query->row['total'];
    }

    /**
     * getProductsCustom
     *
     * @param mixed $category_product NOT USE? TODO: remove...
     * @param array $filter_data
     * @return mixed
     */
    public function getProductsCustom($category_product, $filter_data)
    {
        // Why use this? TODO: remove???...
        /*if ($filter_data['filter_data'] == '') {
            $data = $this->getProducts($filter_data);
            return $data;
        }*/

        $sql = $this->getProductsCustomSql($category_product, $filter_data);

        /* pagination */
        if (isset($filter_data['start']) || isset($filter_data['limit'])) {
            if ($filter_data['start'] < 0) {
                $filter_data['start'] = 0;
            }

            if ($filter_data['limit'] < 1) {
                $data['limit'] = $this->config->get('config_limit_admin');
            }

            $sql .= " LIMIT " . (int)$filter_data['start'] . "," . (int)$filter_data['limit'];
        }

        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'product:getProductsCustom' . md5($sql);
        $products = $this->cache->get($cache_key);
        if ($products) {
            return $products;
        }
        $query = $this->db->query($sql);
        $this->cache->set($cache_key, $query->rows);
        return $query->rows;
    }

    public function getSumQuantilyProduct($product_id)
    {
        $sql = "SELECT SUM(pv.quantity) as sum FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_version pv ON (p.product_id = pv.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE 1=1 AND pv.product_id = " . $product_id . " AND pd.language_id = " . (int)$this->config->get('config_language_id') . " AND pv.`deleted` IS NULL";
        $query = $this->db->query($sql);
        return $query->row['sum'];
    }

    public function getCountVersionProduct($product_id)
    {
        $sql = "SELECT COUNT(pv.product_id) as total FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_version pv ON (p.product_id = pv.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)  WHERE 1=1 AND pv.product_id = " . $product_id . " AND pd.language_id = " . (int)$this->config->get('config_language_id') . " AND pv.`deleted` IS NULL";
        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    public function getCostPriceForProductByStore($store_id, $product_id, $product_version_id = 0)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_to_store` WHERE `store_id` = " . (int)$store_id . " AND `product_id` = " . (int)$product_id . " AND `product_version_id` = " . (int)$product_version_id);

        if (isset($query->row['cost_price'])) {
            return (float)$query->row['cost_price'];
        }

        return 0;
    }

    public function getQuantityForProductByStore($store_id, $product_id, $product_version_id = 0)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_to_store` WHERE `store_id` = " . (int)$store_id . " AND `product_id` = " . (int)$product_id . " AND `product_version_id` = " . (int)$product_version_id);

        if (isset($query->row['quantity'])) {
            return ((float)$query->row['quantity'] > 0) ? (float)$query->row['quantity'] : 0;
        }

        return 0;
    }

    public function countProductInStores($product_id)
    {
        $sql = "SELECT SUM(quantity) as quantity FROM `" . DB_PREFIX . "product_to_store` WHERE `product_id` = " . (int)$product_id;
        $query = $this->db->query($sql);

        if ($query->num_rows > 0) {
            return (int)$query->row['quantity'];
        }

        return 0;
    }

    public function countProductInStoreDefault($product_id)
    {
        $sql = "SELECT SUM(pts.quantity) as qty FROM `" . DB_PREFIX . "product_to_store` pts LEFT JOIN `" . DB_PREFIX . "product` p ON p.`product_id` = pts.`product_id` WHERE pts.`store_id` = p.`default_store_id` AND pts.`product_id` = " . (int)$product_id;
        $query = $this->db->query($sql);

        if ($query->num_rows > 0) {
            return (int)$query->row['qty'];
        }

        return 0;
    }

    /**
     * getTotalProductsCustom
     *
     * @param int $category_id
     * @param array $filter_data
     * @return mixed
     */
    public function getTotalProductsCustom($category_id, $filter_data)
    {
        $sqlAll = $this->getProductsCustomSql($category_id, $filter_data);
        $sql = "SELECT COUNT(t.`product_id`) AS `total` FROM (" . $sqlAll . ") as t";
        // get result from cache first (temporary comment)
        $cache_key = DB_PREFIX . 'product:getTotalProductsCustom' . md5($sql);
        $product_total = $this->cache->get($cache_key);
        if ($product_total) {
            return $product_total;
        }

        $query = $this->db->query($sql);
        $product_total = $query->row['total'];
        $this->cache->set($cache_key, $product_total);
        return $query->row['total'];
    }

    public function getTopProducts($data = ['start' => 0, 'limit' => 6])
    {
        $sql = "SELECT p.product_id as product_id, pd.name as name, p.quantity as quantity, p.image as image, IFNULL(SUM(op.total), 0) as total, p.multi_versions as multi_versions FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "order_product op ON (op.product_id = p .product_id) LEFT JOIN " . DB_PREFIX . "product_description as pd ON (p.product_id = pd.product_id) WHERE p.status = 1 AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY p.product_id ORDER BY total DESC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 10;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getNameRecordById($productId, $tableName, $name = null)
    {
        $language_id = $this->config->get('config_language_id');
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . $tableName . "_description WHERE product_id = '" . (int)$productId . "' AND language_id = '" . $language_id . "'");
        if ($name) {
            if ($query->row[$name]) {
                return $query->row[$name];
            }

            return null;
        }

        if (isset($query->row['name'])) {
            return $query->row['name'];
        }

        return null;
    }

    public function processImportExcel($dataProducts, $offset = 0, $limit = 50)
    {
        $length = self::LIMIT_GET_ITEM_IN_DATA_EXCEL;

        try {
            if ($limit > count($dataProducts)) {
                $limit = count($dataProducts);
            }

            for ($i = $offset; $i < $limit; $i++) {
                $this->importProduct($dataProducts[$i]);
            }


            if ($limit == count($dataProducts)) {
                return true;
            } else {
                $offset += $length;
                $limit += $length;
                $this->processImportExcel($dataProducts, $offset, $limit);
            }

            return true;
        } catch (\Exception $exception) {
            // do something
            $this->log->write('[processImportExcel] error : ' . $exception->getMessage());
            return false;
        }
    }

    public function importMultiProducts($data)
    {
        foreach ($data as $item) {
            $this->importProduct($item);
        }
    }


    public function importProduct($item)
    {
        $item['common_weight'] = isset($item['weight']) ? $item['weight'] : 0;

        /// images
        $item['images'] = array_filter($item['images'], function ($v, $k) use ($item) {
            return $v != $item['thumb'];
        }, ARRAY_FILTER_USE_BOTH);
        array_unshift($item['images'], $item['thumb']);
        $item['images'] = array_slice($item['images'], 0, 6);

        /// manufacturer
        $manufacturer = $this->searchManufacturerByName($item['manufacturer']);
        if ($manufacturer) {
            $item['manufacturer'] = $manufacturer;
        } else {
            $this->load->model('catalog/manufacturer');
            $item['manufacturer'] = $this->model_catalog_manufacturer->addManufacturerFast($item['manufacturer']);
        }

        /// categories
        $item['categories'] = is_array($item['categories']) ? $item['categories'] : [$item['categories']];
        $categories = array();
        foreach ($item['categories'] as $category) {
            $category_id = $this->searchCategoryByName($category);
            if ($category_id) {
                $categories[] = $category_id;
            } elseif (!empty($category)) {
                $this->load->model('catalog/category');
                $categories[] = $this->model_catalog_category->addCategoryFast($category);
            }
        }
        $item['category'] = $categories;

        // collections
        if (isset($item['collections'])) {
            $item['collection_list'] = $this->getCollectionListFromTitles($item['collections']);
        }

        $item['common_sku'] = '';
        $item['common_barcode'] = '';
        $item['common_compare_price'] = '';
        $item['common_price'] = '';
        $item['common_cost_price'] = 0;
        $item['summary'] = isset($item['sub_description']) ? $item['sub_description'] : '';

        $item['title_tab'] = [];
        $item['description_tab'] = [];
        if (!empty($item['product_tabs_general_information'])) {
            $item['title_tab'][] = "Thông tin chung";
            $item['description_tab'][] = $item['product_tabs_general_information'];
        }
        if (!empty($item['product_tabs_ingredients'])) {
            $item['title_tab'][] = "Thành phần";
            $item['description_tab'][] = $item['product_tabs_ingredients'];
        }
        if (!empty($item['product_tabs_main_uses'])) {
            $item['title_tab'][] = "Công dụng chính";
            $item['description_tab'][] = $item['product_tabs_main_uses'];

        }
        if (!empty($item['product_tabs_user_manual'])) {
            $item['title_tab'][] = "Hướng dẫn sử dụng";
            $item['description_tab'][] = $item['product_tabs_user_manual'];
        }

//        if (!empty($item['product_tags'])) {
//            $item['tag_list']=[] ;
//        }

        if (isset($item['summary']) && $item['summary'] == strip_tags($item['summary'])) {
            $item['summary'] = nl2br($item['summary']);
        }
        if (isset($item['description']) && $item['description'] == strip_tags($item['description'])) {
            $item['description'] = nl2br($item['description']);
        }

        $product_version_names = [];
        $product_version_images = [];
        $product_version_image_alts = [];
        $stores = [];
        $product_promotion_prices = [];
        $product_prices = [];
        $product_quantities = [];
        $product_barcodes = [];
        $product_skus = [];

        if (!empty($item['versions'])) {
            foreach ($item['versions'] as $ver) {
                $new_key = $this->replaceVersionValues($ver['attributes']);
                $product_version_names[] = $ver['attributes'];
                $product_version_images[] = $ver['thumb'];
                $product_version_image_alts[] = $ver['image-alts'];
                $stores[$new_key] = 0;
                $product_promotion_prices[] = (float)$ver['promotion_price'];
                $product_prices[] = (float)$ver['price'];
                $product_quantities[] = (float)extract_number($ver['quantity']);
                $product_barcodes[] = $ver['barcode'];
                $product_skus[] = $ver['sku'];
            }
        }

        $item['product_version_names'] = $product_version_names;
        $item['product_display'] = $product_version_names;
        $item['product_version_images'] = $product_version_images;
        $item['product_version_image_alts'] = $product_version_image_alts;
        $item['stores'] = $stores;
        $item['product_version'] = empty($item['versions']) ? 0 : 1;
        $item['product_promotion_prices'] = $product_promotion_prices;
        $item['product_prices'] = $product_prices;
        $item['product_quantities'] = $product_quantities;
        $item['product_barcodes'] = $product_barcodes;
        $item['product_skus'] = $product_skus;

        $product_id = null;
        if (trim($item['sku'] != '')) {
            $product_id = $this->searchProductBySKU($item['sku']);
        } else {
            $product_id = $this->searchProductByName($item['name']);
        }

        if ($product_id) {
            $product = $this->getProduct($product_id);

            // create seo alias if current empty (for fixing product missing seo before)
            $this->load->model('design/seo_url');
            $existing_alias = $this->model_design_seo_url->getSeoUrlByQuery('product_id=' . $product_id);
            if (empty($existing_alias)) {
                $product_alias = $product['name'];

                $this->load->model('custom/common');
                $slug = $this->model_custom_common->createSlug($product_alias, '-');
                $slug_custom = $this->model_custom_common->getSlugUnique($slug);

                $item['alias'] = !empty($item['alias']) ? $item['alias'] : $slug_custom;
            }

            // seo info
            $item['seo_title'] = !empty($item['seo_title']) ? $item['seo_title'] : getValueByKey($product, 'seo_title', '');
            $item['seo_desciption'] = !empty($item['sub_description']) ? strip_tags($item['sub_description']) : getValueByKey($product, 'seo_description', '');
            $item['seo_description'] = $item['seo_desciption']; // correct lang. TODO: remove above line...
            $item['meta_keyword'] = !empty($item['meta_keyword']) ? $item['meta_keyword'] : getValueByKey($product, 'meta_keyword', '');
            $item['stores_default_id'] = getValueByKey($product, 'default_store_id', 0); // 0 is default store

            $item['source'] = getValueByKey($product, 'source', '');
            if (!empty($item['source']) && $item['source'] == 'omihub') {
                return;
            }

            $item['source'] = 'excel';
            $this->editProduct($product_id, $item);
        } else {
            // IMPORTANT: set empty for adding only, NOT for editing!
            $item['seo_title'] = !empty($item['seo_title']) ? $item['seo_title'] : '';
            $item['seo_desciption'] = !empty($item['sub_description']) ? strip_tags($item['sub_description']) : '';
            $item['seo_description'] = $item['seo_desciption']; // correct lang. TODO: remove above line...
            $item['meta_keyword'] = !empty($item['meta_keyword']) ? $item['meta_keyword'] : '';
            $item['alias'] = !empty($item['alias']) ? $item['alias'] : $item['name'];
            $item['source'] = 'excel';

            $product_id = $this->addProduct($item);
        }

        foreach ($item['attributes'] as $attribute) {
            $attribute['attribute_name'] = trim($attribute['attribute_name']);
            if ($attribute['attribute_name'] != '') {
                $language_id = (int)$this->config->get('config_language_id'); //TV
                $this->db->query("INSERT INTO " . DB_PREFIX . "attribute SET attribute_group_id = 0, sort_order = 0 ");
                $attribute_id = $this->db->getLastId();
                $this->db->query("INSERT INTO " . DB_PREFIX . "attribute_description SET attribute_id = '" . (int)$attribute_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($attribute['attribute_name']) . "'");
                $attribute_description = $attribute['attribute_description'];
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$attribute_id . "', language_id = '" . (int)$language_id . "', text = '" . $this->db->escape($attribute_description) . "'");
            }
        }

        if (isset($item['versions'])) {
            $this->addProductVersions($product_id, $item['versions']);
        } else {
            // add product to store for product 1 version
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");
            $cost_price = isset($item['cost_price']) ? (float)$item['cost_price'] : 0;
            // get user_store
            $user_id = $this->user->getId();
            $this->load->model('user/user');
            $user_stores = $this->model_user_user->getUserStore($user_id);
            $default_store = !empty($user_stores) ? $user_stores[0]['id'] : 0;
            $this->addProductToStore($default_store, $item['quantity'], $cost_price, $product_id);
        }

        try {
            $this->sendToRabbitMq($product_id);
        } catch (Exception $exception) {
            $this->log->write('Send message excel product error: ' . $exception->getMessage());
        }
    }

    private function getCollectionListFromTitles($titles)
    {
        if (!is_array($titles) || empty($titles)) {
            return [];
        }

        $collection_list = [];
        $this->load->model('catalog/collection');
        foreach ($titles as $title) {
            $collection_id = $this->model_catalog_collection->getCollectionIdByTitle(trim($title));
            if (!$collection_id) {
                $collection_id = $this->model_catalog_collection->addCollectionTitleOnly(trim($title));
            }

            if ($collection_id) {
                $collection_list[] = $collection_id;
            }
        }

        return $collection_list;
    }

    private function addProductVersions($product_id, $versions)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");
        $version_ids = [];
        foreach ($versions as $version) {
            $cost_price = isset($version['cost_price']) ? (float)$version['cost_price'] : 0;
            $product_version_id = null;
            if (trim($version['sku']) != '') {
                $product_version_id = $this->searchProductVersionByProductAndSKU($product_id, $version['sku']);
            }
            if ($product_version_id) {
                if (!in_array($product_version_id, $version_ids)) {
                    $this->addProductToStore(0, $version['quantity'], $cost_price, $product_id, $product_version_id);
                    $version_ids[] = $product_version_id;
                    continue;
                }
            }
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_version 
                                SET product_id = '" . (int)$product_id . "',
                                    version = '" . $this->db->escape($version['attributes']) . "',
                                    price = '" . $this->db->escape(str_replace(',', '', $version['promotion_price'])) . "',
                                    compare_price = '" . $this->db->escape(str_replace(',', '', $version['price'])) . "',
                                    sku = '" . $this->db->escape($version['sku']) . "',
                                    barcode = '" . $this->db->escape($version['barcode']) . "',
                                    quantity = 0,
                                    status = '" . (int)$version['status'] . "',
                                    `image` = '" . $this->db->escape($version['thumb']) . "',
                                    `image_alt` = '" . $this->db->escape($version['image-alts']) . "'");

            // add product to store for event
            $product_version_id = $this->db->getLastId();
            // get user_store
            $user_id = $this->user->getId();
            $this->load->model('user/user');
            $user_stores = $this->model_user_user->getUserStore($user_id);
            $default_store = !empty($user_stores) ? $user_stores[0]['id'] : 0;
            $this->addProductToStore($default_store, $version['quantity'], $cost_price, $product_id, $product_version_id);
            $version_ids[] = $product_version_id;
        }

        $this->deleteProductCaches();
    }

    private function addProductToStore($store_id, $quantity, $cost_price, $product_id, $product_version_id = 0)
    {
        if (!$product_id || (int)$quantity < 1) {
            return;
        }
        $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store 
                                  SET `product_id` = '" . (int)$product_id . "', 
                                      `store_id` = '" . (int)$store_id . "', 
                                      `product_version_id` = '" . (int)$product_version_id . "', 
                                      `quantity` = '" . (float)extract_number($quantity) . "', 
                                      `cost_price` = '" . (float)extract_number($cost_price) . "'");

        $this->deleteProductCaches();

        return;
    }

    public function searchProductVersionBySKU($sku)
    {
        $query = $this->db->query("SELECT product_version_id FROM " . DB_PREFIX . "product_version WHERE sku = '" . $this->db->escape($sku) . "' AND deleted IS NULL");

        if (isset($query->row['product_version_id'])) {
            return $query->row['product_version_id'];
        } else {
            return false;
        }
    }

    public function searchProductVersionByProductAndSKU($product_id, $sku)
    {
        $query = $this->db->query("SELECT product_version_id FROM " . DB_PREFIX . "product_version WHERE product_id = " . (int)$product_id . " AND sku = '" . $this->db->escape($sku) . "' AND deleted IS NULL");

        if (isset($query->row['product_version_id'])) {
            return $query->row['product_version_id'];
        } else {
            return false;
        }
    }

    public function searchManufacturerByName($name)
    {
        $query = $this->db->query("SELECT manufacturer_id FROM " . DB_PREFIX . "manufacturer WHERE name = '" . $this->db->escape($name) . "'");

        if (isset($query->row['manufacturer_id'])) {
            return $query->row['manufacturer_id'];
        } else {
            return false;
        }
    }

    public function searchCategoryByName($name)
    {
        $name = trim($name);

        $filter_name_1 = $name; // support new value a&b
        $filter_name_2 = htmlentities($name); // support old value a&amp;b
        $query = $this->db->query("SELECT `category_id` FROM " . DB_PREFIX . "category_description 
                                   WHERE `name` = '" . $this->db->escape(trim($filter_name_1)) . "' 
                                      OR `name` = '" . $this->db->escape($filter_name_2) . "'");

        if (isset($query->row['category_id'])) {
            return $query->row['category_id'];
        } else {
            return false;
        }
    }

    public function searchProductFilterByName($name)
    {
        $query = $this->db->query("SELECT filter_id FROM " . DB_PREFIX . "filter_description WHERE name = '" . $this->db->escape($name) . "'");

        if (isset($query->row['filter_id'])) {
            return $query->row['filter_id'];
        } else {
            return false;
        }
    }

    public function searchProductBySKU($sku)
    {
        $query = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "product WHERE sku = '" . $this->db->escape($sku) . "' AND deleted IS NULL");

        if (isset($query->row['product_id'])) {
            return $query->row['product_id'];
        } else {
            return false;
        }
    }

    public function searchProductAndVersionBySKU($sku, $product_id)
    {
        $sql = "SELECT product_id FROM " . DB_PREFIX . "product WHERE sku = '" . $this->db->escape($sku) . "' AND deleted IS NULL";
        if (!empty($product_id)) {
            $sql .= " AND product_id != '" . (int)$product_id . "'";
        }
        $query = $this->db->query($sql);
        if (isset($query->row['product_id'])) {
            return false;
        }

        $sql = "SELECT product_id FROM " . DB_PREFIX . "product_version WHERE sku = '" . $this->db->escape($sku) . "' AND deleted IS NULL";
        if (!empty($product_id)) {
            $sql .= " AND product_id != '" . (int)$product_id . "'";
        }
        $query = $this->db->query($sql);
        if (isset($query->row['product_id'])) {
            return false;
        }

        return true;
    }

    public function searchProductByName($name)
    {
        $sql = "SELECT p.`product_id` FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";

        $sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND pd.name = '" . $this->db->escape($name) . "'";

        $sql .= " AND p.`deleted` IS NULL ";

        $query = $this->db->query($sql);
        if (isset($query->row['product_id'])) {
            return $query->row['product_id'];
        } else {
            return false;
        }
    }

    public function checkExistingProductByName($name, $exclude_product_id)
    {
        $product_id = $this->searchProductByName($name);

        if ($product_id && $product_id != $exclude_product_id) {
            return true;
        }

        return false;
    }

    public function searchProductByNameContainDraft($name, $product_id)
    {
        $sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";

        $sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND pd.name = '" . $this->db->escape($name) . "'";
        if (!empty($product_id)) {
            $sql .= " AND p.product_id != '" . (int)$product_id . "'";
        }

        $sql .= " AND (p.`deleted` IS NULL OR p.`deleted` = 2)";

        $query = $this->db->query($sql);
        if ($query->row['total'] != 0) {
            return true;
        }
        return false;
    }

    public function getProductManufacturer($manufacturer_id)
    {
        $this->load->model('catalog/manufacturer');
        $manufacturer = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);

        if (isset($manufacturer['name'])) {
            return $manufacturer['name'];
        } else {
            return false;
        }
    }

    public function getManufactureIdByProductId($product_id)
    {
        if (!$product_id) {
            return false;
        }

        $sql = "SELECT `manufacturer_id` FROM `" . DB_PREFIX . "product` 
                WHERE `product_id` =" . (int)$product_id;

        $query = $this->db->query($sql);
        if ($query->num_rows) {
            return (int)$query->row['manufacturer_id'];
        } else {
            return false;
        }
    }

    public function export($product_filter_data = array())
    {
        $this->load->language('catalog/product');

        /* get all products */
        $products = $this->getProducts($product_filter_data);

        // support multi_versions:
        $all_products = [];

        /* refactor data */
        // get main domain
        $main_domain = $this->getMainDomain();
        foreach ($products as $product) {
            /*
             * link => miss
             * name
             * description
             * sku
             * barcode
             * status => map
             * stock_status => miss
             * quantity
             * manufacture => miss manufacturer_id
             * category => miss
             * type => miss
             * weight
             * attributes => miss
             * price
             * compare_price
             * image
             * image_alts
             * additional_image_urls => miss
             * multi_versions
             * product_tabs_general_information
             * product_tabs_ingredients
             * product_tabs_main_uses
             * product_tabs_user_manual
             * seo_title
             * meta_keyword
             */

            // get seo url
            $product_seo_url = $this->getAliasById($product['product_id']);
            if (isset($product_seo_url['keyword'])) {
                // TODO: define schema http or https?...
                $product['link'] = $product_seo_url['keyword'];
            } else {
                $product['link'] = "";
            }

            $product['status'] = ($product['status'] == 1
                ? $this->language->get('entry_status_publish')
                : $this->language->get('entry_status_unpublish'));

            $product['quantity'] = $this->getQuantityInAllStoreByProduct($product['product_id']);
            $product['cost_price'] = $this->getCostPriceForProductByStore($product['default_store_id'], $product['product_id']);

            $collections = $this->getProductCollectionTitles($product['product_id']);
            $product['collections'] = implode(PHP_EOL, $collections);

            $product['stock_status'] = $product['quantity'] > 0
                ? ($product['language_id'] == 2 ? $this->language->get('entry_stock_in_stock') : 'in stock')
                : ($product['language_id'] == 2 ? $this->language->get('entry_status_out_of_stock') : 'out stock');

            $product['manufacture'] = $this->getProductManufacturer($product['manufacturer_id']);

            // get categories
            $productCategoryNames = array_map(function ($cat) {
                return $cat;
            }, $this->getProductCategoryName($product['product_id']));
            $product['category'] = implode(PHP_EOL, $productCategoryNames);

            $product['type'] = $product['category']; // same

            // get versions if multi versions
            $productAttributes = array_map(function ($attr) {
                return sprintf('%s:%s', $attr['name'], $attr['text']);
            }, $this->getProductAttributeNamesAndValues($product['product_id']));
            $product['attributes'] = implode(PHP_EOL, $productAttributes);

            // get images
            $productImages = array_map(function ($img) {
                return isset($img['image']) ? $img['image'] : '';
            }, $this->getProductImages($product['product_id']));
            $product['additional_image_urls'] = implode(PHP_EOL, $productImages);

            // get image_alts for additional_image_urls
            $image_alts = array_map(function ($img) {
                return isset($img['image_alt']) ? $img['image_alt'] : '';
            }, $this->getProductImages($product['product_id']));
            array_unshift($image_alts, $product['image_alt']);
            $product['image_alt'] = implode(PHP_EOL, $image_alts);

            // support multi_versions
            $is_multi_versions = $product['multi_versions'] == 1;
            $product['original_product'] = '';
            $product['multi_versions'] = $is_multi_versions
                ? random_bytes_php56(10) // use for relation ship with product version.
                : '';
            $product['description'] = $this->characterToHTMLEntity(html_entity_decode($product['description'], ENT_QUOTES, "UTF-8"));
            $product['sub_description'] = $this->characterToHTMLEntity(html_entity_decode($product['sub_description'], ENT_QUOTES, "UTF-8"));

            // get product_description_tab
            $product_description_tabs = $this->getProductDescriptionTab($product['product_id']);
            if (!empty($product_description_tabs)) {
                foreach ($product_description_tabs as $key => $description_tab) {
                    if ($key == 0 && isset($description_tab['description'])) {
                        $product['product_tabs_general_information'] = $this->characterToHTMLEntity(html_entity_decode($description_tab['description'], ENT_QUOTES, "UTF-8"));
                    }
                    if ($key == 1 && isset($description_tab['description'])) {
                        $product['product_tabs_ingredients'] = $this->characterToHTMLEntity(html_entity_decode($description_tab['description'], ENT_QUOTES, "UTF-8"));
                    }

                    if ($key == 2 && isset($description_tab['description'])) {
                        $product['product_tabs_main_uses'] = $this->characterToHTMLEntity(html_entity_decode($description_tab['description'], ENT_QUOTES, "UTF-8"));
                    }

                    if ($key == 3 && isset($description_tab['description'])) {
                        $product['product_tabs_user_manual'] = $this->characterToHTMLEntity(html_entity_decode($description_tab['description'], ENT_QUOTES, "UTF-8"));
                    }
                }
            }

            if (empty($product['product_tabs_general_information'])) {
                $product['product_tabs_general_information'] = '';
            }

            if (empty($product['product_tabs_ingredients'])) {
                $product['product_tabs_ingredients'] = '';
            }

            if (empty($product['product_tabs_main_uses'])) {
                $product['product_tabs_main_uses'] = '';
            }

            if (empty($product['product_tabs_user_manual'])) {
                $product['product_tabs_user_manual'] = '';
            }

            // add product
            $all_products[] = $product;

            // add product_versions to all products if multi_versions
            if ($is_multi_versions) {
                $product_versions = array_map(function ($pv) use ($product) {
                    // add more info from product to product version
                    $_pv = (new ArrayObject($product))->getArrayCopy();

                    // sync $pv to new
                    $_pv = array_merge($_pv, $pv);

                    // modify by language
                    $_pv['status'] = ($pv['status'] == 1
                        ? $this->language->get('entry_status_publish')
                        : $this->language->get('entry_status_unpublish'));

                    $_pv['quantity'] = $this->getQuantityInAllStoreByProduct($product['product_id'], $pv['product_version_id']);
                    $_pv['cost_price'] = $this->getCostPriceForProductByStore($product['default_store_id'], $product['product_id'], $pv['product_version_id']);
                    $_pv['collections'] = $product['collections'];

                    $_pv['stock_status'] = $pv['quantity'] > 0
                        ? $this->language->get('entry_stock_in_stock')
                        : $this->language->get('entry_status_out_of_stock');

                    // difference from original product
                    $_pv['name'] .= " - " . $_pv['version']; // e.g: Iphone 6S - Đỏ,64GB,wifi
                    $_pv['attributes'] = $pv['version']; // e.g: Đỏ,64GB,wifi
                    $_pv['original_product'] = $product['multi_versions']; // relation ship with original product
                    $_pv['multi_versions'] = ''; // not set, this is product version
                    $_pv['product_tabs_general_information'] = ''; // not set, this is product version
                    $_pv['product_tabs_ingredients'] = ''; // not set, this is product version
                    $_pv['product_tabs_main_uses'] = ''; // not set, this is product version
                    $_pv['product_tabs_user_manual'] = ''; // not set, this is product version
                    $_pv['seo_title'] = ''; // not set, this is product version
                    $_pv['meta_keyword'] = ''; // not set, this is product version
                    $_pv['link'] = ""; // not set, this is product version

                    return $_pv;
                }, $this->getProductVersions($product['product_id']));

                // add product versions
                $all_products = array_merge($all_products, $product_versions);
            }

            $product['stock_status'] = $product['quantity'] > 0
                ? ($product['language_id'] == 2 ? $this->language->get('entry_stock_in_stock') : 'in stock')
                : ($product['language_id'] == 2 ? $this->language->get('entry_status_out_of_stock') : 'out stock');
        }

        /* create xlsx and bind product data to it */
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $column = 'A';
        $row = 1;

        // map fields from language
        $EXPORT_EXCEL_COLUMN_MAPPING = [
            'link' => $this->language->get('export_file_col_link'),
            'name' => $this->language->get('export_file_col_name'),
            'description' => $this->language->get('export_file_col_description'),
            'sub_description' => $this->language->get('export_file_col_sub_description'),
            'sku' => $this->language->get('export_file_col_sku'),
            'barcode' => $this->language->get('export_file_col_barcode'),
            'status' => $this->language->get('export_file_col_status'),
            'stock_status' => $this->language->get('export_file_col_stock_status'),
            'quantity' => $this->language->get('export_file_col_quantity'),
            'manufacture' => $this->language->get('export_file_col_manufacture'),
            //'category' => $this->language->get('export_file_col_category'), // google category, temp not used
            'type' => $this->language->get('export_file_col_type'), // i.e product category. "type" for first time, now is "category"
            'weight' => $this->language->get('export_file_col_weight'),
            'attributes' => $this->language->get('export_file_col_attributes'),
            'price' => $this->language->get('export_file_col_price'),
            'compare_price' => $this->language->get('export_file_col_compare_price'),
            'cost_price' => $this->language->get('export_file_col_cost_price'),
            'collections' => $this->language->get('export_file_col_collections'),
            'image' => $this->language->get('export_file_col_image'),
            'image_alt' => $this->language->get('export_file_col_image_alts'),
            'additional_image_urls' => $this->language->get('export_file_col_additional_image_urls'),
            'original_product' => $this->language->get('export_file_col_original_product'),
            'multi_versions' => $this->language->get('export_file_col_multi_versions'),
            'product_tabs_general_information' => $this->language->get('export_file_col_general_information'),
            'product_tabs_ingredients' => $this->language->get('export_file_col_ingredients'),
            'product_tabs_main_uses' => $this->language->get('export_file_col_main_uses'),
            'product_tabs_user_manual' => $this->language->get('export_file_col_user_manual'),
            'seo_title' => $this->language->get('export_file_col_meta_title'),
            'meta_keyword' => $this->language->get('export_file_col_meta_keyword'),
        ];

        // write header
        foreach ($EXPORT_EXCEL_COLUMN_MAPPING as $headerName) {
            $sheet->setCellValue($column . $row, $headerName);
            $column++;
        }

        // write rows
        $row++;
        foreach ($all_products as $product) {
            // filter out not used columns
            $p = array_intersect_key($product, $EXPORT_EXCEL_COLUMN_MAPPING);
            // sort to sync columns
            $p = array_merge(array_flip(array_keys($EXPORT_EXCEL_COLUMN_MAPPING)), $p);

            $column = 'A';
            foreach ($p as $col => $value) {
                $sheet->setCellValueExplicit($column . $row, $value, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $column++;
            }
            $row++;
        }

        /*
         * write output for downloading...
         * https://stackoverflow.com/questions/27701981/phpexcel-download-using-ajax-call
         */
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        ob_start();
        $writer->save("php://output"); // for test: DIR_APPLICATION . "products.xlsx"
        $xlsxData = ob_get_contents();
        ob_end_clean();

        return 'data:application/vnd.ms-excel;base64,' . base64_encode($xlsxData);
    }

    /**
     * @param int $product_id
     * @param null|int $product_version_id let null for single product or all versions of product multi versions
     * @return int
     */
    public function getQuantityInAllStoreByProduct($product_id, $product_version_id = null)
    {
        $condition_pv_id = !is_null($product_version_id) ? " `product_version_id` = " . (int)$product_version_id : " 1=1 ";
        $query = $this->db->query("SELECT SUM(`quantity`) as quantity 
                                   FROM `" . DB_PREFIX . "product_to_store` 
                                   WHERE `product_id` = " . (int)$product_id . " 
                                     AND {$condition_pv_id}");

        if ($query->num_rows > 0) {
            return (int)getValueByKey($query->row, 'quantity', 0);
        }

        return 0;
    }

    public function getProductId()
    {
        $sql = "SELECT p.product_id FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_to_store ps ON (p.product_id = ps.product_id) WHERE ps.store_id = '" . (int)$this->config->get('config_store_id') . "'";
        $sql .= 'LIMIT 1';
        $query = $this->db->query($sql);
        return (int)$query->row;
    }

    public function getProductIdLivePreview()
    {
        $sql = "SELECT p.product_id FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE  pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.`deleted` IS NULL ";
        $sql .= 'LIMIT 1';
        $query = $this->db->query($sql);
        return (int)isset($query->row['product_id']) ? $query->row['product_id'] : '';
    }

    public function setStock($product_ids, $stock)
    {
        try {
            $query = "UPDATE " . DB_PREFIX . "product SET status=" . $stock . " WHERE product_id IN(" . $product_ids . ")";
            $this->db->query($query);

            $this->deleteProductCaches();

            $arr_product_ids = explode(',', $product_ids);
            if (!empty($arr_product_ids)) {
                foreach ($arr_product_ids as $product_id) {
                    // send product to rabbit mq
                    try {
                        $this->sendToRabbitMq($product_id);
                    } catch (Exception $exception) {
                        $this->log->write('Send message setStock product error: ' . $exception->getMessage());
                    }
                }
            }


            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function updateStatus($id, $status)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET status = " . $status . " WHERE product_id = " . (int)$id . "");

        /* update attribute filter by attribute names */
        $this->load->model('catalog/attribute');
        $attributes = $this->model_catalog_attribute->getAttributes(['product_id' => $id]);
        if (!empty($attributes)) {
            $this->load->model('catalog/attribute_filter');
            foreach ($attributes as $attribute) {
                $this->model_catalog_attribute_filter->updateAttributeFilterByName($attribute['name']);
            }
        }

        $this->deleteProductCaches();

        // send product to rabbit mq
        try {
            $this->sendToRabbitMq($id);
        } catch (Exception $exception) {
            $this->log->write('Send message update status product error: ' . $exception->getMessage());
        }

        return true;
    }

    public function getAllCategoryName()
    {
        $this->load->model('catalog/category');

        return $this->model_catalog_category->getLevelCategories([]);
    }

    public function getProductVersions($product_id, $product_version_id_as_key = false)
    {
        $product_versions_data = array();
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_version WHERE product_id = '" . (int)$product_id . "' AND deleted IS NULL");

        foreach ($query->rows as $result) {
            $product_versions_data[$result['product_version_id']] = [
                'version' => $result['version'],
                'price' => $result['price'],
                'compare_price' => $result['compare_price'],
                'sku' => $result['sku'],
                'barcode' => $result['barcode'],
                'quantity' => $result['quantity'],
                'status' => $result['status'],
                'product_version_id' => $result['product_version_id'],
                'image' => $result['image'],
                'image_alt' => $result['image_alt']
            ];
        }

        return $product_version_id_as_key ? $product_versions_data : array_values($product_versions_data);
    }

    public function getAliasById($product_id, $store_id = 0)
    {
        $query = $this->db->query("SELECT keyword FROM " . DB_PREFIX . "seo_url WHERE query = 'product_id=" . (int)$product_id . "' AND store_id = '" . (int)$store_id . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function deleteDemoProducts()
    {
        $demo_product_ids = $this->getDemoProducts();
        foreach ($demo_product_ids as $demo_product_id) {
            if (!array_key_exists('product_id', $demo_product_id)) {
                continue;
            }

            $this->deleteProduct($demo_product_id['product_id'], true);
        }
    }

    public function addToCategoryQuickly($category, $product_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
        foreach ($category as $category_id) {
            if (strpos($category_id, 'add_new_') !== false) {
                $this->load->model('catalog/category');
                $category_id = $this->model_catalog_category->addCategoryFast(str_replace('add_new_', '', $category_id));
            }

            $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
            $this->db->query("UPDATE " . DB_PREFIX . "product SET date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
        }

        $this->deleteProductCaches();

        // send product to rabbit mq
        try {
            $this->sendToRabbitMq($product_id);
        } catch (Exception $exception) {
            $this->log->write('Send message quick add cate of product error: ' . $exception->getMessage());
        }
    }

    public function addToManufacturerQuickly($manufacturer_id, $product_id)
    {
        if (isset($manufacturer_id) and $manufacturer_id > 0) {
            $sql = "UPDATE " . DB_PREFIX . "product  SET manufacturer_id = '" . (int)$manufacturer_id . "' WHERE product_id = '" . (int)$product_id . "'";
            $this->db->query($sql);
            $this->db->query("UPDATE " . DB_PREFIX . "product SET date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");

            $this->deleteProductCaches();
        }
    }

    public function getDemoProducts()
    {
        $query = $this->db->query("SELECT p.product_id FROM " . DB_PREFIX . "product p WHERE p.demo = 1");

        return $query->rows;
    }

    public function getProductVersionByProductIdAndVersionId($product_id, $version_id)
    {
        $sql = "SELECT * from " . DB_PREFIX . "product_version as pv WHERE pv.product_id = '" . (int)$product_id . "' AND pv.product_version_id ='" . (int)$version_id . "'";
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getDataByTableName($table_name)
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "demo_data WHERE table_name = '" . $this->db->escape($table_name) . "' ";

        try {
            $query = $this->db->query($sql);
            return $query->rows;
        } catch (Exception $e) {
            // table "demo_data" may not be existed. Skipped!
            return [];
        }
    }

    // Get skues from product
    public function getSkues()
    {
        $query = $this->db->query("SELECT sku from  " . DB_PREFIX . "product");

        return $query->rows;
    }

    // Get skues from variation
    public function getVariationSkues()
    {
        $query = $this->db->query("SELECT sku from  " . DB_PREFIX . "product_version");

        return $query->rows;
    }

    public function getProductOneVersionBySKU($sku)
    {
        $query = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "product WHERE sku = '" . $this->db->escape($sku) . "' AND multi_versions = 0");

        return $query->row;
    }

    public function getProductMultiVersionByVersionSKUES($skues)
    {
        $str_skues = "";
        foreach ($skues as $k => $sku) {
            if ($k != 0) {
                $str_skues = $str_skues . ", ";
            }
            $str_skues = $str_skues . "'$sku'";
        }

        $query = $this->db->query("SELECT DISTINCT product_id FROM " . DB_PREFIX . "product_version WHERE sku IN (" . $str_skues . ")");

        return $query->rows;
    }

    public function getStoreByProductId($productId)
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "product_to_store as pts LEFT JOIN " . DB_PREFIX .
            "store as st ON (pts.store_id = st.store_id) WHERE pts.product_id = '" . (int)$productId . "'";
        $query = $this->db->query($sql);

        $result = [];
        foreach ($query->rows as $data) {
            $result[] = array(
                'id' => $data['store_id'],
                'title' => $data['name']
            );
        }

        return $result;
    }

    public function getVersionsByVersionIds($version_ids)
    {
        $version_ids = implode(',', $version_ids);
        $sql = "SELECT * FROM `" . DB_PREFIX . "product_version` WHERE `product_version_id` IN (" . $version_ids . ")";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    function characterToHTMLEntity($str)
    {
        $replace = array('&', '<', '>', '€', '‘', '’', '“', '”', '–', '—', '¡', '¢', '£', '¤', '¥', '¦', '§', '¨', '©', 'ª', '«', '¬', '®', '¯', '°', '±', '²', '³', '´', 'µ', '¶', '·', '¸', '¹', 'º', '»', '¼', '½', '¾', '¿', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', '×', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'Þ', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', '÷', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'þ', 'ÿ', 'Œ', 'œ', '‚', '„', '…', '™', '•', '˜');

        $search = array('&amp;', '&lt;', '&gt;', '&euro;', '&lsquo;', '&rsquo;', '&ldquo;', '&rdquo;', '&ndash;', '&mdash;', '&iexcl;', '&cent;', '&pound;', '&curren;', '&yen;', '&brvbar;', '&sect;', '&uml;', '&copy;', '&ordf;', '&laquo;', '&not;', '&reg;', '&macr;', '&deg;', '&plusmn;', '&sup2;', '&sup3;', '&acute;', '&micro;', '&para;', '&middot;', '&cedil;', '&sup1;', '&ordm;', '&raquo;', '&frac14;', '&frac12;', '&frac34;', '&iquest;', '&Agrave;', '&Aacute;', '&Acirc;', '&Atilde;', '&Auml;', '&Aring;', '&AElig;', '&Ccedil;', '&Egrave;', '&Eacute;', '&Ecirc;', '&Euml;', '&Igrave;', '&Iacute;', '&Icirc;', '&Iuml;', '&ETH;', '&Ntilde;', '&Ograve;', '&Oacute;', '&Ocirc;', '&Otilde;', '&Ouml;', '&times;', '&Oslash;', '&Ugrave;', '&Uacute;', '&Ucirc;', '&Uuml;', '&Yacute;', '&THORN;', '&szlig;', '&agrave;', '&aacute;', '&acirc;', '&atilde;', '&auml;', '&aring;', '&aelig;', '&ccedil;', '&egrave;', '&eacute;', '&ecirc;', '&euml;', '&igrave;', '&iacute;', '&icirc;', '&iuml;', '&eth;', '&ntilde;', '&ograve;', '&oacute;', '&ocirc;', '&otilde;', '&ouml;', '&divide;', '&oslash;', '&ugrave;', '&uacute;', '&ucirc;', '&uuml;', '&yacute;', '&thorn;', '&yuml;', '&OElig;', '&oelig;', '&sbquo;', '&bdquo;', '&hellip;', '&trade;', '&bull;', '&asymp;');

        $str = str_replace($search, $replace, $str);

        return $str;
    }

    function replaceVersionValues($str)
    {
        // unescape version value due to version may contains special char that was escaped
        // e.g BLACK & WHITE => BLACK &apm; WHITE => BLACK__apm__WHITE instead of our expect is BLACK___WHITE
        $str = html_entity_decode($str);

        return preg_replace('/[^0-9a-zA-Z\-_]+/', '_', $str) . '_' . md5($str);
    }

    /**
     * getProductsCustomSql
     *
     * @param $category_product
     * @param $filter_data
     * @return string
     */
    private function getProductsCustomSql($category_product, $filter_data)
    {
        $DB_PREFIX = DB_PREFIX;

//        $sql = "SELECT p.*, CONCAT(u.lastname, ' ', u.firstname) as fullname ,pd.name as product_name, p.image as product_image FROM  {$DB_PREFIX}product p
//                LEFT JOIN {$DB_PREFIX}product_description pd
//                       ON (pd.product_id = p.product_id)
//                LEFT JOIN " . DB_PREFIX ."user u ON (u.user_id = p.user_create_id) ";

        $sql = "SELECT p.*, u.lastname as fullname ,pd.name as product_name, p.image as product_image FROM  {$DB_PREFIX}product p 
                LEFT JOIN {$DB_PREFIX}product_description pd 
                       ON (pd.product_id = p.product_id) 
                LEFT JOIN " . DB_PREFIX ."user u ON (u.user_id = p.user_create_id) ";

        if ($filter_data['manufacturer'] != 0) {
            $sql .= " LEFT JOIN {$DB_PREFIX}manufacturer m ON (m.manufacturer_id = p.manufacturer_id) ";
        }

        if ($filter_data['category_product'] != 0) {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_to_category c ON (p.product_id = c.product_id) ";
        }

        if ($filter_data['collection'] != 0) {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_collection pc ON (p.product_id = pc.product_id) ";
        }

        $sql .= " LEFT JOIN {$DB_PREFIX}product_tag pt ON (pt.product_id = p.product_id) ";
        $sql .= " LEFT JOIN {$DB_PREFIX}tag t ON (t.tag_id = pt.tag_id) ";
        $sql .= " WHERE pd.language_id = " . (int)$this->config->get('config_language_id') . " ";

        if(isset($filter_data['user_create_id']) && $filter_data['user_create_id'] !== '') {
            $sql .= ' AND p.user_create_id=' . $filter_data['user_create_id'];
        }

        if (isset($filter_data['filter_staff']) && $filter_data['filter_staff'] !== '') {
            $sql .= " AND p.user_create_id IN (" . $filter_data['filter_staff'] . ")";
        }

        if (isset($filter_data['status_product']) && $filter_data['status_product'] !== '') {
            $sql .= ' AND p.status=' . $filter_data['status_product'];
        }

        if ($filter_data['manufacturer'] != 0) {
            $sql .= ' AND m.manufacturer_id IN (' . $filter_data['manufacturer'] . ')';
        }

        if ($filter_data['nametag'] != '') {
            $sql .= " AND t.value RLIKE '" . $filter_data['nametag'] . "'";
        }

        if ($filter_data['category_product'] != 0) {
            $this->load->model('catalog/category');
            $filter_cat_ids = explode(',', $filter_data['category_product']);
            $all_related_cat_ids = [];
            foreach ($filter_cat_ids as $filter_cat_id) {
                $related_cat_ids = $this->model_catalog_category->getFullChildCategoryIds($filter_cat_id);
                $all_related_cat_ids = array_merge($all_related_cat_ids, [$filter_cat_id], $related_cat_ids);
            }
            $all_related_cat_ids = array_values(array_unique($all_related_cat_ids));

            if (!empty($all_related_cat_ids)) {
                $sql .= ' AND c.category_id IN (' . implode(',', $all_related_cat_ids) . ') ';
            }
        }

        if ($filter_data['collection'] != 0) {
            $sql .= ' AND pc.collection_id IN (' . $filter_data['collection'] . ')';
        }

        if (isset($filter_data['filter_name']) && $filter_data['filter_name'] !== '') {
            $sql .= " AND (pd.`name` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%' ";
            $sql .= " OR t.`value` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%' ) ";
        }

        $sql .= " AND p.`deleted` IS NULL ";
        $sql = $sql . " group by p.product_id";
        $sql = $sql . " ORDER BY p.date_modified DESC";

        return $sql;
    }

    public function getCategoriesAndParents($product_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category as ptc LEFT JOIN " . DB_PREFIX . "category_description as cd ON cd.category_id = ptc.category_id WHERE product_id = '" . (int)$product_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        $list_categories = [];
        foreach ($query->rows as $row) {
            $parent_categories = $this->getAllParentsCategory($row['category_id']);
            foreach ($parent_categories as $k => $category) {
                $list_categories[$k] = $category;
            }
        }

        $result = [];
        foreach ($list_categories as $category) {
            $result[] = [
                'id' => $category['category_id'],
                'name' => $category['name'],
                'href' => $this->url->link('common/shop', 'path=' . $category['category_id'], true),
                'parent_id' => $category['parent_id']
            ];
        }

        return $result;
    }

    public function checkProductSingleVersionExist($product_id) // check for single version product
    {
        $query = $this->db->query("SELECT COUNT(*) as count_product FROM `" . DB_PREFIX . "product` WHERE `product_id` = " . (int)$product_id . " AND `deleted` IS NULL AND `multi_versions` = 0");

        if (isset($query->row['count_product'])) {
            return (int)$query->row['count_product'];
        }

        return 0;
    }

    /**
     * check product is in stock
     *
     * @param $product_id
     * @param null|int $product_version_id null if product not multi versions or check for all versions
     * @return bool
     */
    public function isStock($product_id, $product_version_id = null)
    {
        $sql = "SELECT p.sale_on_out_of_stock, 
                       pts.quantity, 
                       pts.product_version_id 
                FROM " . DB_PREFIX . "product_to_store pts 
                LEFT JOIN `" . DB_PREFIX . "product` p ON p.`product_id` = pts.`product_id` 
                WHERE pts.`product_id` = " . (int)$product_id . " 
                  AND pts.`store_id` = p.`default_store_id`";

        if (!is_null($product_version_id)) {
            $sql .= "AND pts.`product_version_id` = " . (int)$product_version_id;
        }

        $query = $this->db->query($sql);
        if (!isset($query->rows) || empty($query->rows)) {
            return false;
        }

        $in_stock_count = 0;
        $not_selected_store = [];
        $not_selected_store_sql = [];
        foreach ($query->rows as $row) {
            /* notice: if product not select any store for quantity, known as quantity = 0, then check sale_on_out_of_stock only! */
            if (!isset($row['quantity'])) {
                $not_selected_store[] = (int)$row['product_version_id'];
                $not_selected_store_sql[] = 'pts.`product_version_id` = ' . (int)$row['product_version_id'];
            } else {
                $in_stock = ($row['quantity'] > 0) || (isset($row['sale_on_out_of_stock']) && $row['sale_on_out_of_stock'] == 1);
                if ($in_stock) {
                    $in_stock_count++;
                }

                if ($in_stock_count > 0) {
                    return true;
                }
            }
        }

        if (!empty($not_selected_store)) {
            $sql = "SELECT p.sale_on_out_of_stock 
                        FROM " . DB_PREFIX . "product p 
                        LEFT JOIN `" . DB_PREFIX . "product_to_store` pts ON p.`product_id` = pts.`product_id` 
                        WHERE p.`product_id` = " . (int)$product_id . "
                          AND (" . implode(' OR ', $not_selected_store_sql) . ")";
            $query = $this->db->query($sql);

            foreach ($query->rows as $row) {
                $in_stock = isset($row['sale_on_out_of_stock']) && $row['sale_on_out_of_stock'] == 1;
                if ($in_stock) {
                    $in_stock_count++;
                }

                if ($in_stock_count > 0) {
                    return true;
                }
            }
        }

        // old code. TODO: remove...
        // foreach ($query->rows as $row) {
        //     /* notice: if product not select any store for quantity, known as quantity = 0, then check sale_on_out_of_stock only! */
        //     if (!isset($row['quantity'])) {
        //         $sql = "SELECT p.sale_on_out_of_stock
        //                 FROM " . DB_PREFIX . "product p
        //                 WHERE p.`product_id` = " . (int)$product_id;
        //         $query = $this->db->query($sql);
        //
        //         $in_stock = isset($query->row['sale_on_out_of_stock']) && $query->row['sale_on_out_of_stock'] == 1;
        //     } else {
        //         $in_stock = ($row['quantity'] > 0) || (isset($row['sale_on_out_of_stock']) && $row['sale_on_out_of_stock'] == 1);
        //     }
        //
        //     if ($in_stock) {
        //         $in_stock_count++;
        //     }
        // }

        /* notice: if product multi versions: out of stock means as all versions out of stock! */
        return $in_stock_count > 0;
    }

    /**
     *
     * @param int|string $product_id
     * @return array
     */
    public function buildProductForRabbitMq($product_id)
    {
        try {
            $this->load->model('catalog/attribute');
            $this->load->model('catalog/manufacturer');
            $product = $this->getProductForRabbitMq($product_id);
            $record = [];

            if (isset($product['multi_versions']) && $product['multi_versions'] == 1) {
                $version_quantity = $this->getCountVersionProduct($product['product_id']);
            } else {
                $version_quantity = 0;
            }

            $category_names = $this->getProductCategoryName($product['product_id']);
            $category_name_product = is_array($category_names) ? implode(',', $category_names) : '';
            $name_cate = (!empty($category_name_product) ? $category_name_product : '');

            $product_versions = $this->getProductVersions($product['product_id']);
            foreach ($product_versions as &$version) {
                $version['version'] = implode(' • ', explode(',', $version['version']));
                $version['version_for_store'] = $this->replaceVersionValues($version['version']);

                $version['price'] = (int)str_replace(',', '', $version['price']);
                $version['compare_price'] = (int)str_replace(',', '', $version['compare_price']);
                $version['cost_price'] = $this->getCostPriceForProductByStore($product['default_store_id'], $product['product_id'], $version['product_version_id']);
                $version['quantity'] = $this->getQuantityForProductByStore($product['default_store_id'], $product['product_id'], $version['product_version_id']);
            }
            unset($version);

            // $description_tab
            $title_tab = [];
            $description_tab = [];
            $product_description_tabs = $this->getProductDescriptionTab($product['product_id']);
            foreach ($product_description_tabs as $product_description_tab) {
                $title_tab[] = html_entity_decode(trim($product_description_tab['title']));
                $description_tab[] = html_entity_decode($product_description_tab['description']);
            }

            $array_manufacturer = $this->model_catalog_manufacturer->getManufacturer($product['manufacturer_id']);
            $name_manufacturer = (isset($array_manufacturer['name']) ? $array_manufacturer['name'] : '');

            $product_cost_price = $this->getCostPriceForProductByStore($product['default_store_id'], $product['product_id']);
            $product_quantity_of_store = $this->getQuantityForProductByStore($product['default_store_id'], $product['product_id']);

            $record = [
                "id" => $product['product_id'],
                "name" => html_entity_decode($product['name']),
                "original_price" => $product['compare_price'],
                "sale_price" => $product['price'],
                "common_compare_price" => $product['common_compare_price'],
                "common_price" => $product['common_price'],
                "common_cost_price" => $product['common_cost_price'],
                "cost_price" => $product_cost_price,
                "image" => $product['image'],
                "quantity" => $product_quantity_of_store,
                "sku" => $product['multi_versions'] ? $product['common_sku'] : $product['sku'],
                "weight" => $product["weight"],
                "in_stock" => ($product["sale_on_out_of_stock"] == 1 || $product_quantity_of_store > 0) ? 1 : 0,
                "sale_on_out_of_stock" => $product["sale_on_out_of_stock"],
                "version_quantity" => $version_quantity,
                "version" => $product_versions,
                'nameCategory' => isset($name_cate) ? $name_cate : '',
                'manufacturer' => $name_manufacturer,
                'activeStatus' => $product['status'],
                'user_name' => $product['fullname'],
                'desc' => html_entity_decode($product['sub_description']),
                'content' => html_entity_decode($product['description']),
                'product_attributes' => [],
                'barcode' => $product['multi_versions'] ? $product['common_barcode'] : $product['barcode'],
                'seo_title' => isset($product['seo_title']) ? $product['seo_title'] : '',
                'seo_description' => isset($product['seo_description']) ? $product['seo_description'] : '',
                'meta_keyword' => isset($product['meta_keyword']) ? $product['meta_keyword'] : '',
                'alias' => html_entity_decode($product['name']),
                'title_tab' => $title_tab,
                'description_tab' => $description_tab,
            ];

            if (empty($query_params['product_version_id'])) {
                $product_attributes = $this->getProductAttributes($product['product_id']);
                foreach ($product_attributes as $product_attribute) {
                    $attribute_info = $this->model_catalog_attribute->getAttribute($product_attribute['attribute_id']);

                    if ($attribute_info) {
                        $record['product_attributes'][] = array(
                            'attribute_id' => $product_attribute['attribute_id'],
                            'name' => $attribute_info['name'],
                            'detail' => explode(",", $product_attribute['product_attribute_description']['text'])
                        );
                    }
                }
            }

            return $record;
        } catch (Exception $e) {
            return [
                'code' => 500,
                'message' => $e->getMessage()
            ];
        }
    }

    public function sendToRabbitMq($product_id)
    {
        try {
            if (!empty($product_id)) {
                $message = [
                    'shop_name' => $this->config->get('shop_name'),
                    'product' => []
                ];

                $product = $this->buildProductForRabbitMq($product_id);
                if (isset($product['code']) && $product['code'] == 500) {
                    $this->log->write('Build product for rabbit_mq error: ' . json_encode($product));
                    return;
                }

                $message['product'] = $product;

                $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'product', '', 'product', json_encode($message));
            }
        } catch (Exception $exception) {
            $this->log->write('Send message product error: ' . $exception->getMessage());
        }
    }

    private function getProductForLazada($product_id)
    {
        $sql = "SELECT `multi_versions`, `product_id`  
                  FROM `" . DB_PREFIX . "product` 
                      WHERE `product_id` = " . (int)$product_id . " 
                          AND `deleted` IS NULL";

        $query = $this->db->query($sql);

        return $query->row;
    }

    private function getAllParentsCategory($category_id)
    {
        $result = [];
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category` as c LEFT JOIN `" . DB_PREFIX . "category_description` as cd ON c.`category_id` = cd.`category_id` WHERE c.`category_id` = " . (int)$category_id);

        if ($query->num_rows <= 0) {
            return $result;
        }

        $result[$query->row['category_id']] = $query->row;
        if ($query->row['parent_id'] != 0) {
            $parents = $this->getAllParentsCategory($query->row['parent_id']);
            foreach ($parents as $k => $v) {
                $result[$k] = $v;
            }
        }

        return $result;
    }

    /**
     * delete product caches
     */
    private function deleteProductCaches() {
        $product_key_redis =  DB_PREFIX . 'product';
        $this->cache->delete($product_key_redis);
    }
}
