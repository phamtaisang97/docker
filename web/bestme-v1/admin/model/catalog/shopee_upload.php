<?php


class ModelCatalogShopeeUpload extends Model
{
    const STATUS_PRODUCT_NOT_ENOUGH_INFO = 0;
    const STATUS_PRODUCT_READY_FOR_UPLOAD = 1;
    const STATUS_PRODUCT_UPLOAD_FAILED = 2;
    const STATUS_PRODUCT_UPLOAD_SUCCESS = 3;

    public function addShopeeProduct($data)
    {
        $logistics = isset($data['logistics']) ? $data['logistics'] : [];
        $thumb = $this->detectImages($data)[0];
        $images = $this->detectImages($data)[1];
        $attributes = $this->detectVersionAttribute($data);
        $price = (double)str_replace(',', '', $data['price']);
        $bestme_product_id = isset($data['bestme_product_id']) ? $data['bestme_product_id'] : 'NULL';

        $sql = "INSERT INTO " . DB_PREFIX . "shopee_upload_product (bestme_product_id, name, description, category_id, image, price, stock, sku, common_price, common_stock, common_sku, version_attribute, weight, length, width, height,date_added, date_modify)
                VALUES (
                    " . $bestme_product_id . ",
                    '" . $this->db->escape($data['name']) . "',
                    '" . $this->db->escape($data['description']) . "',
                    " . (isset($data['category_id']) ? (int)$data['category_id'] : 'NULL') . ",
                    '" . $this->db->escape($thumb) . "',
                    '" . $price . "',
                    '" . (int)$data['quantity'] . "',
                    '" . $this->db->escape($data['sku']) . "',
                    '" . (double)$data['price'] . "',
                    '" . (int)$data['quantity'] . "',
                    '" . $this->db->escape($data['sku']) . "',
                    '" . $this->db->escape($attributes) . "',
                    '" . (isset($data['weight']) ? (int)$data['weight'] : NULL) . "',
                    '" . (isset($data['length']) ? (int)$data['length'] : NULL) . "',
                    '" . (isset($data['width']) ? (int)$data['width'] : NULL) . "',
                    '" . (isset($data['height']) ? (int)$data['height'] : NULL) . "',
                    now(), now()
                );";
        $this->db->query($sql);
        $product_id = $this->db->getLastId();

        if (isset($data['category_attr']) && count($data['category_attr']) > 0) {
            $this->insertCategoryAttributes($data['category_attr'], $product_id);
        }

        $this->insertProductImage($images, $product_id);
        $this->insertProductVersion($data, $product_id);
        $this->insertLogistics($product_id, $logistics);
        $this->insertProductUpdateResult($data, $product_id);
    }

    /**
     * @param array $data
     * @param int $product_id
     * @param bool $allow_update_product_status if true, get product admin to shopee_upload table
     */
    public function editShopeeProduct($data, $product_id, $allow_update_product_status = false)
    {
        $logistics = isset($data['logistics']) ? $data['logistics'] : [];
        $thumb = $this->detectImages($data)[0];
        $images = $this->detectImages($data)[1];
        $attributes = $this->detectVersionAttribute($data);
        $price = (double)str_replace(',', '', $data['price']);

        $this->db->query("UPDATE " . DB_PREFIX . "shopee_upload_product
                SET
                    name ='" . $this->db->escape($data['name']) . "',
                    description ='" . $this->db->escape($data['description']) . "',
                    category_id =" . (isset($data['category_id']) ? (int)$data['category_id'] : 'NULL') . ",
                    image ='" . $this->db->escape($thumb) . "',
                    price ='" . $price . "',
                    stock ='" . (int)$data['quantity'] . "',
                    sku ='" . $this->db->escape($data['sku']) . "',
                    common_price ='" . (double)$data['price'] . "',
                    common_stock ='" . (int)$data['quantity'] . "',
                    common_sku ='" . $this->db->escape($data['sku']) . "',
                    version_attribute ='" . $this->db->escape($attributes) . "',
                    weight ='" . (isset($data['weight']) ? (int)$data['weight'] : NULL) . "',
                    length ='" . (isset($data['length']) ? (int)$data['length'] : NULL) . "',
                    width ='" . (isset($data['width']) ? (int)$data['width'] : NULL) . "',
                    height ='" . (isset($data['height']) ? (int)$data['height'] : NULL) . "',
                    date_modify=now()
                WHERE shopee_upload_product_id=" . (int)$product_id);

        // update category attribute
        $this->db->query("DELETE FROM " . DB_PREFIX . "shopee_upload_product_attribute WHERE product_id = '" . (int)$product_id . "'");
        if (isset($data['category_attr']) && count($data['category_attr']) > 0) {
            $this->insertCategoryAttributes($data['category_attr'], $product_id);
        }

        // update product images
        $this->db->query("DELETE FROM " . DB_PREFIX . "shopee_upload_product_image WHERE product_id = '" . (int)$product_id . "'");
        $this->insertProductImage($images, $product_id);

        // update Product versions
        $this->db->query("DELETE FROM " . DB_PREFIX . "shopee_upload_product_version WHERE shopee_upload_product_id = '" . (int)$product_id . "'");
        $this->insertProductVersion($data, $product_id);

        // update Product logistics
        $this->insertLogistics($product_id, $logistics);

        $this->insertProductUpdateResult($data, $product_id, $allow_update_product_status);
    }

    public function insertLogistics($product_id, $logistics)
    {
        if (count($logistics) > 0) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "shopee_upload_product_logistic WHERE product_id='" . $product_id . "'");

            $sql_insert_logistics = "INSERT INTO " . DB_PREFIX . "shopee_upload_product_logistic (logistic_id, product_id, enabled) VALUES ";
            $insert_data = [];
            foreach ($logistics as $logistic) {
                $insert_data[] = "('" . (int)$logistic . "' ,'" . (int)$product_id . "', 1)";
            }
            $sql_insert_logistics .= implode(', ', $insert_data);
            $this->db->query($sql_insert_logistics);
        }


        $DB_PREFIX = DB_PREFIX;

        $this->db->query("UPDATE {$DB_PREFIX}shopee_upload_product 
                          SET `date_modify` = NOW() 
                            WHERE `shopee_upload_product_id` = {$product_id}");

        if ($this->checkProductFullInfo($product_id)) {
            $this->db->query("UPDATE {$DB_PREFIX}shopee_upload_product_shop_update_result 
                            SET `status`= 1 
                            WHERE `shopee_upload_product_id` = {$product_id} AND `status` IN(0)");
        }
    }

    public function getCategories()
    {
        $language_code = $this->getCurrentLanguageId();
        $DB_PREFIX = DB_PREFIX;

        $query = $this->db->query("SELECT shopee_category_id, parent_id, has_children, name_" . $language_code . " as name
                  FROM {$DB_PREFIX}shopee_category 
                  WHERE 1");

        return $query->rows;
    }

    public function getLogistics()
    {
        $DB_PREFIX = DB_PREFIX;
        $query = $this->db->query("SELECT * 
                  FROM {$DB_PREFIX}shopee_logistics 
                  WHERE 1");

        return $query->rows;
    }

    public function getProductLogistics($product_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $query = $this->db->query("SELECT logistic.*, pl.enabled 
                    FROM {$DB_PREFIX}shopee_logistics logistic
                    LEFT JOIN (
                        SELECT *
                        FROM {$DB_PREFIX}shopee_upload_product_logistic 
                        WHERE product_id = " . (int)$product_id . "
                    ) pl ON logistic.logistic_id = pl.logistic_id
                    WHERE 1
                    ORDER BY logistic_id");
        return $query->rows;
    }

    public function getProductAttrs($product_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $query = $this->db->query("SELECT * 
                    FROM {$DB_PREFIX}shopee_upload_product_attribute 
                    WHERE product_id=" . (int)$product_id);

        return $query->rows;
    }

    public function getFirstShopId()
    {
        $DB_PREFIX = DB_PREFIX;
        $query = $this->db->query("SELECT shop_id
                          FROM {$DB_PREFIX}shopee_shop_config
                          WHERE connected = 1");
        return isset($query->row['shop_id']) ? $query->row['shop_id'] : 0;
    }

    public function getProductById($product_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $query = $this->db->query("SELECT p.*, pr.in_process
                  FROM {$DB_PREFIX}shopee_upload_product as p  
                  LEFT JOIN {$DB_PREFIX}shopee_upload_product_shop_update_result as pr on p.shopee_upload_product_id = pr.shopee_upload_product_id 
                  WHERE p.shopee_upload_product_id=" . (int)$product_id);
        return $query->row;
    }

    public function getProductImages($product_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $query = $this->db->query("SELECT * 
                  FROM {$DB_PREFIX}shopee_upload_product_image as p  
                  WHERE product_id=" . (int)$product_id);
        return $query->rows;
    }

    public function getProductPaginator(array $data)
    {
        $DB_PREFIX = DB_PREFIX;

        $sql = "SELECT p.`product_id`, p.`default_store_id`, p.`price`, p.`compare_price`, p.`date_modified`, p.`multi_versions`, 
                       pd.`name` 
                  FROM {$DB_PREFIX}product as p 
                  LEFT JOIN {$DB_PREFIX}product_description as pd ON p.product_id = pd.product_id 
                  WHERE 1 
                    AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' ";

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.`name`LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " AND p.`deleted` IS NULL ";
        $sql .= " GROUP BY p.`product_id`";
        $sql .= " ORDER BY p.`date_modified` DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function countAllProduct(array $data)
    {
        $DB_PREFIX = DB_PREFIX;

        $sql = "SELECT COUNT(DISTINCT p.product_id) as total_product 
                  FROM {$DB_PREFIX}product as p 
                  LEFT JOIN {$DB_PREFIX}product_description as pd ON p.product_id = pd.product_id 
                  WHERE 1 
                    AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' ";

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.`name`LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " AND p.`deleted` IS NULL ";
        $query = $this->db->query($sql);

        return $query->row['total_product'];
    }

    public function checkProductMapWithShopee($product_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $has_linked = false;

        $this->load->model('catalog/shopee');
        $shopee_shops = $this->model_catalog_shopee->getShopeeShopList();

        foreach ($shopee_shops as $shop) {
            $sql = "SELECT COUNT(DISTINCT item_id) as total 
                    FROM {$DB_PREFIX}shopee_product 
                    WHERE `bestme_id` = {$product_id} AND `shopid` = {$shop['shop_id']}";

            $query = $this->db->query($sql);

            if (isset($query->row['total']) && $query->row['total'] != 0) {
                $has_linked = true;
            }

            if (!$has_linked) {
                $sql = "SELECT COUNT(DISTINCT spv.`item_id`) as total 
                    FROM {$DB_PREFIX}shopee_product_version as spv 
                    LEFT JOIN {$DB_PREFIX}shopee_product as sp ON sp.`item_id` = spv.`item_id` 
                    WHERE spv.`bestme_product_id` = {$product_id} AND sp.`shopid` = {$shop['shop_id']}";

                $query = $this->db->query($sql);

                if (isset($query->row['total']) && $query->row['total'] != 0) {
                    $has_linked = true;
                }
            }
        }

        return $has_linked;
    }

    public function checkProductDoSelect($product_id)
    {
        $do_not_select = false;
        $DB_PREFIX = DB_PREFIX;

        $this->load->model('catalog/product');
        $this->load->model('catalog/shopee');

        $shopee_shops = $this->model_catalog_shopee->getShopeeShopList();
        $product = $this->model_catalog_product->getProduct($product_id);

        if (!$product) {
            return $do_not_select;
        }

        foreach ($shopee_shops as $shop) {
            if (isset($product['multi_versions']) && $product['multi_versions']) {
                $sql = "SELECT COUNT(DISTINCT spv.`item_id`) as total 
                    FROM {$DB_PREFIX}shopee_product_version as spv 
                    LEFT JOIN {$DB_PREFIX}shopee_product as sp ON sp.`item_id` = spv.`item_id` 
                    WHERE spv.`bestme_product_id` = {$product_id} AND sp.`shopid` = {$shop['shop_id']}";

                $query = $this->db->query($sql);

                if (isset($query->row['total']) && $query->row['total'] > 1) {
                    $do_not_select = true;
                }

                $sql = "SELECT COUNT(DISTINCT item_id) as total 
                    FROM {$DB_PREFIX}shopee_product 
                    WHERE `bestme_id` = {$product_id} AND `shopid` = {$shop['shop_id']}";

                $query = $this->db->query($sql);

                if (isset($query->row['total']) && $query->row['total'] > 1) {
                    $do_not_select = true;
                }

            } else {
                $sql = "SELECT COUNT(DISTINCT item_id) as total 
                    FROM {$DB_PREFIX}shopee_product 
                    WHERE `bestme_id` = {$product_id} AND `has_variation` = 0 AND `shopid` = {$shop['shop_id']}";

                $query = $this->db->query($sql);

                if (isset($query->row['total']) && $query->row['total'] > 1) {
                    $do_not_select = true;
                }

                $sql = "SELECT COUNT(DISTINCT spv.`item_id`) as total 
                    FROM {$DB_PREFIX}shopee_product_version as spv 
                    LEFT JOIN {$DB_PREFIX}shopee_product as sp ON sp.`item_id` = spv.`item_id` 
                    WHERE spv.`bestme_product_id` = {$product_id} AND sp.`shopid` = {$shop['shop_id']}";

                $query = $this->db->query($sql);

                if (isset($query->row['total']) && $query->row['total'] >= 1) {
                    $do_not_select = true;
                }
            }
        }

        return $do_not_select;
    }

    public function getList(array $data)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT sup.*, sup_sur.`status`, sup_sur.`error_msg`, sup_sur.`shopee_shop_id`, MAX(sup_sur.`in_process`) as `in_process` FROM {$DB_PREFIX}shopee_upload_product as sup 
                  LEFT JOIN {$DB_PREFIX}shopee_upload_product_shop_update_result as sup_sur 
                    ON sup.`shopee_upload_product_id` = sup_sur.`shopee_upload_product_id` WHERE 1";

        if (isset($data['name']) && $data['name'] != '') {
            $sql .= " AND sup.`name` LIKE '%" . $this->db->escape($data['name']) . "%'";
        }

        if (isset($data['status'])) {
            $sql .= " AND sup_sur.`status` = {$data['status']}";
        }

        if (isset($data['shop_id'])) {
            $sql .= " AND sup_sur.`shopee_shop_id` = {$data['shop_id']}";
        }

        if (isset($data['status']) && ($data['status'] == 0 || $data['status'] == 1)) {
            $sql .= " GROUP BY sup.`shopee_upload_product_id`";
        }

        $sql .= " ORDER BY sup.`date_modify` DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getProductIdsByStatus($status)
    {
        $sql = "SELECT DISTINCT `shopee_upload_product_id` FROM `" . DB_PREFIX . "shopee_upload_product_shop_update_result` WHERE `status` = " . (int)$status;

        $query = $this->db->query($sql);
        if ($query->num_rows < 1) {
            return [];
        }

        $result = array_map(function ($row) {
            return $row['shopee_upload_product_id'];
        }, $query->rows);

        return $result;
    }

    public function updateProductInProcess($shopee_id, $product_ids, $status)
    {
        if (!is_array($product_ids) || empty($product_ids)) {
            return;
        }

        $sql = "UPDATE `" . DB_PREFIX . "shopee_upload_product_shop_update_result` SET `in_process` = 1 
                            WHERE `shopee_shop_id` = " . (int)$shopee_id . " AND `shopee_upload_product_id` IN (" . implode(',', $product_ids) . ")";

        $this->db->query($sql);

        if ($status == 1) {
            $sql = "UPDATE `" . DB_PREFIX . "shopee_upload_product_shop_update_result` SET `in_process` = 1 
                            WHERE `shopee_shop_id` is NULL AND `shopee_upload_product_id` IN (" . implode(',', $product_ids) . ")";
            $this->db->query($sql);
        }
    }

    public function getTotalProduct(array $data)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT COUNT(DISTINCT sup.`shopee_upload_product_id`) as total_product
                  FROM {$DB_PREFIX}shopee_upload_product as sup 
                  LEFT JOIN {$DB_PREFIX}shopee_upload_product_shop_update_result as sup_sur 
                    ON sup.`shopee_upload_product_id` = sup_sur.`shopee_upload_product_id` WHERE 1";

        if (isset($data['name']) && $data['name'] != '') {
            $sql .= " AND sup.`name` LIKE '%" . $this->db->escape($data['name']) . "%'";
        }

        if (isset($data['status'])) {
            $sql .= " AND sup_sur.`status` = {$data['status']}";
        }

        if (isset($data['shop_id'])) {
            $sql .= " AND sup_sur.`shopee_shop_id` = {$data['shop_id']}";
        }

        $query = $this->db->query($sql);

        return $query->row['total_product'];
    }

    public function getProductVersionById($product_id)
    {
        if (!isset($product_id)) {
            return [];
        }

        $DB_PREFIX = DB_PREFIX;

        $sql = "SELECT * FROM {$DB_PREFIX}shopee_upload_product_version WHERE `shopee_upload_product_id` = {$product_id}";

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getMinMaxVersionPriceAndStockById($product_id)
    {
        if (!isset($product_id)) {
            return [];
        }

        $DB_PREFIX = DB_PREFIX;

        $sql = "SELECT MIN(version_price) as min_version_price, 
                       MAX(version_price) as max_version_price, 
                       SUM(version_stock) as total_version_stock 
                  FROM {$DB_PREFIX}shopee_upload_product_version 
                    WHERE `shopee_upload_product_id` = {$product_id}";

        $query = $this->db->query($sql);

        if (empty($query->row['min_version_price'])) {
            return [];
        }

        return $query->row;
    }

    public function getMinMaxVersionPriceProductBestmeById($product_id)
    {
        if (!isset($product_id)) {
            return [];
        }

        $DB_PREFIX = DB_PREFIX;

        $sql = "SELECT MIN(compare_price) as min_version_price, 
                       MAX(compare_price) as max_version_price 
                  FROM {$DB_PREFIX}product_version 
                    WHERE `product_id` = {$product_id}";

        $query = $this->db->query($sql);

        if (!isset($query->row['min_version_price']) || !isset($query->row['max_version_price'])) {
            return 0;
        }

        if ($query->row['min_version_price'] == $query->row['max_version_price']) {
            return number_format((float)$query->row['min_version_price']);
        }

        return number_format((float)$query->row['min_version_price']) . ' ~ ' . number_format((float)$query->row['max_version_price']);
    }

    public function getFullCategoriesById($category_id, $name = "")
    {
        if (!isset($category_id) || !$category_id) {
            return $name;
        }

        $language_id = (int)$this->config->get('config_language_id');
        $DB_PREFIX = DB_PREFIX;

        $sql = "SELECT * FROM {$DB_PREFIX}shopee_category WHERE `shopee_category_id` = {$category_id}";

        $query = $this->db->query($sql);

        $name_en = isset($query->row['name_en']) ? $query->row['name_en'] : '';
        $name_vi = isset($query->row['name_vi']) ? $query->row['name_vi'] : '';

        $category_name = ($language_id == 1) ? $name_en : $name_vi;

        if ($name == "") {
            $name = $category_name;
        } else {
            $name = $category_name . ' > ' . $name;
        }

        if (isset($query->row['parent_id']) && $query->row['parent_id'] != 0) {
            $name = $this->getFullCategoriesById($query->row['parent_id'], $name);
        }

        return $name;
    }

    public function getAllQuantityOfProductById($product_bestme_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT SUM(quantity) as total_quantity FROM {$DB_PREFIX}product_to_store WHERE `product_id` = {$product_bestme_id}";
        $query = $this->db->query($sql);

        return isset($query->row['total_quantity']) ? $query->row['total_quantity'] : 0;

    }

    public function editPrice($product_id, $price)
    {
        if (!isset($product_id)) {
            return;
        }

        $DB_PREFIX = DB_PREFIX;
        $this->db->query("UPDATE {$DB_PREFIX}shopee_upload_product 
                          SET `price`= {$this->db->escape($price)}, 
                              `date_modify` = NOW() 
                            WHERE `shopee_upload_product_id` = {$product_id}");

        if ($this->checkProductFullInfo($product_id)) {
            $this->db->query("UPDATE {$DB_PREFIX}shopee_upload_product_shop_update_result 
                                SET `status`= 1 
                                WHERE `shopee_upload_product_id` = {$product_id} AND `status` IN(0)");
        }
    }

    public function editPriceVersion($product_id, $product_version_id, $price)
    {
        if (!isset($product_version_id)) {
            return;
        }

        $DB_PREFIX = DB_PREFIX;
        $this->db->query("UPDATE {$DB_PREFIX}shopee_upload_product_version SET `version_price`= {$this->db->escape($price)} WHERE `shopee_upload_product_version_id` = {$product_version_id}");

        if (!isset($product_id)) {
            return;
        }

        $this->db->query("UPDATE {$DB_PREFIX}shopee_upload_product 
                          SET `date_modify` = NOW() 
                            WHERE `shopee_upload_product_id` = {$product_id}");

        if ($this->checkProductFullInfo($product_id)) {
            $this->db->query("UPDATE {$DB_PREFIX}shopee_upload_product_shop_update_result 
                                SET `status`= 1 
                                WHERE `shopee_upload_product_id` = {$product_id} AND `status` IN(0)");
        }
    }

    public function editStock($product_id, $stock)
    {
        if (!isset($product_id)) {
            return;
        }

        $DB_PREFIX = DB_PREFIX;
        $this->db->query("UPDATE {$DB_PREFIX}shopee_upload_product 
                            SET `stock`= {$this->db->escape($stock)}, 
                                `date_modify` = NOW() 
                              WHERE `shopee_upload_product_id` = {$product_id}");

        if ($this->checkProductFullInfo($product_id)) {
            $this->db->query("UPDATE {$DB_PREFIX}shopee_upload_product_shop_update_result 
                                SET `status`= 1 
                                WHERE `shopee_upload_product_id` = {$product_id} AND `status` IN(0)");
        }
    }

    public function editStockVersion($product_id, $product_version_id, $stock)
    {
        if (!isset($product_version_id)) {
            return;
        }

        $DB_PREFIX = DB_PREFIX;
        $this->db->query("UPDATE {$DB_PREFIX}shopee_upload_product_version SET `version_stock`= {$this->db->escape($stock)} WHERE `shopee_upload_product_version_id` = {$product_version_id}");

        if (!isset($product_id)) {
            return;
        }

        $this->db->query("UPDATE {$DB_PREFIX}shopee_upload_product 
                          SET `date_modify` = NOW() 
                            WHERE `shopee_upload_product_id` = {$product_id}");

        if ($this->checkProductFullInfo($product_id)) {
            $this->db->query("UPDATE {$DB_PREFIX}shopee_upload_product_shop_update_result 
                                SET `status`= 1 
                                WHERE `shopee_upload_product_id` = {$product_id} AND `status` IN(0)");
        }
    }

    public function editBrand($product_id, $category_id)
    {
        if (!isset($product_id)) {
            return;
        }

        // delete product_attribute
        $this->db->query("DELETE FROM " . DB_PREFIX . "shopee_upload_product_attribute WHERE product_id = '" . (int)$product_id . "'");

        $DB_PREFIX = DB_PREFIX;
        $this->db->query("UPDATE {$DB_PREFIX}shopee_upload_product 
                            SET `category_id`= {$this->db->escape($category_id)}, 
                                `date_modify` = NOW() 
                              WHERE `shopee_upload_product_id` = {$product_id}");

        if ($this->checkProductFullInfo($product_id)) {
            $this->db->query("UPDATE {$DB_PREFIX}shopee_upload_product_shop_update_result 
                                SET `status`= 1 
                                WHERE `shopee_upload_product_id` = {$product_id} AND `status` IN(0)");
        }

        if ($this->categoryHasAttribute($category_id)) {
            $this->db->query("UPDATE {$DB_PREFIX}shopee_upload_product_shop_update_result 
                                SET `status`= 0 
                                WHERE `shopee_upload_product_id` = {$product_id} AND `status` IN(1)");
        }
    }

    public function setBrand($data, $category_id)
    {
        if (!isset($category_id)) {
            return;
        }

        $products = $this->getList($data);
        $product_ids = [];

        foreach ($products as $product) {
            if (!isset($product['shopee_upload_product_id'])) {
                continue;
            }

            $product_ids[] = $product['shopee_upload_product_id'];
        }

        $product_ids = implode(",", $product_ids);

        $this->updateBrand($product_ids, $category_id);

    }

    public function updateBrand($product_ids, $category_id)
    {
        if (!isset($product_ids)) {
            return;
        }

        // delete product_attribute
        $this->db->query("DELETE FROM " . DB_PREFIX . "shopee_upload_product_attribute WHERE product_id IN(" . $product_ids . ")");

        $DB_PREFIX = DB_PREFIX;
        $this->db->query("UPDATE {$DB_PREFIX}shopee_upload_product 
                            SET `category_id`= {$this->db->escape($category_id)}, 
                                `date_modify` = NOW() 
                              WHERE `shopee_upload_product_id` IN(" . $product_ids . ")");

        $p_ids = explode(',', $product_ids);

        foreach ($p_ids as $item) {
            if ($this->checkProductFullInfo($item)) {
                $this->db->query("UPDATE {$DB_PREFIX}shopee_upload_product_shop_update_result 
                                SET `status`= 1 
                                WHERE `shopee_upload_product_id` = {$item} AND `status` IN(0)");
            }

            if ($this->categoryHasAttribute($category_id)) {
                $this->db->query("UPDATE {$DB_PREFIX}shopee_upload_product_shop_update_result 
                                SET `status`= 0 
                                WHERE `shopee_upload_product_id` = {$item} AND `status` IN(1)");
            }
        }
    }

    public function setPackageSize(array $data, $weight = 10, $length = 1, $width = 1, $height = 1)
    {
        $products = $this->getList($data);
        $product_ids = [];

        foreach ($products as $product) {
            if (!isset($product['shopee_upload_product_id'])) {
                continue;
            }

            $product_ids[] = $product['shopee_upload_product_id'];
        }

        $product_ids = implode(",", $product_ids);

        $this->updatePackageSize($product_ids, $weight, $length, $width, $height);
    }

    public function updatePackageSize($product_ids, $weight, $length, $width, $height)
    {
        if (!isset($product_ids)) {
            return;
        }

        $DB_PREFIX = DB_PREFIX;
        $this->db->query("UPDATE {$DB_PREFIX}shopee_upload_product 
                            SET `weight`= {$this->db->escape($weight)}, 
                                `length`= {$this->db->escape($length)}, 
                                `width`= {$this->db->escape($width)}, 
                                `height`= {$this->db->escape($height)}, 
                                `date_modify` = NOW() 
                          WHERE `shopee_upload_product_id` IN(" . $product_ids . ")");

        $p_ids = explode(',', $product_ids);

        foreach ($p_ids as $item) {
            if ($this->checkProductFullInfo($item)) {
                $this->db->query("UPDATE {$DB_PREFIX}shopee_upload_product_shop_update_result 
                                SET `status`= 1 
                                WHERE `shopee_upload_product_id` = {$item} AND `status` IN(0)");
            }
        }
    }

    public function allProductLogistics(array $data, array $logistic_ids)
    {
        $products = $this->getList($data);

        foreach ($products as $product) {
            if (!isset($product['shopee_upload_product_id'])) {
                continue;
            }

            $this->insertLogistics($product['shopee_upload_product_id'], $logistic_ids);
        }
    }

    public function deleteProduct(array $data)
    {
        $products = $this->getList($data);
        $product_ids = [];

        foreach ($products as $product) {
            if (!isset($product['shopee_upload_product_id'])) {
                continue;
            }

            if ($this->checkProductInProcessUpload($product['shopee_upload_product_id'])) {
                continue;
            }

            $product_ids[] = $product['shopee_upload_product_id'];
        }

        $product_ids = implode(",", $product_ids);

        $this->deleteProductByIds($product_ids);
    }

    public function deleteProductByIds($product_ids)
    {
        if (!isset($product_ids) || empty($product_ids)) {
            return;
        }

        $DB_PREFIX = DB_PREFIX;

        // oc_shopee_upload_product,
        // oc_shopee_upload_product_version,
        // oc_shopee_upload_product_image,
        // oc_shopee_upload_product_attribute,
        // oc_shopee_upload_product_logistic,
        // oc_shopee_upload_product_shop_update_result

        $this->db->query("DELETE FROM {$DB_PREFIX}shopee_upload_product WHERE shopee_upload_product_id IN(" . $product_ids . ")");
        $this->db->query("DELETE FROM {$DB_PREFIX}shopee_upload_product_version WHERE shopee_upload_product_id IN(" . $product_ids . ")");
        $this->db->query("DELETE FROM {$DB_PREFIX}shopee_upload_product_image WHERE product_id IN(" . $product_ids . ")");
        $this->db->query("DELETE FROM {$DB_PREFIX}shopee_upload_product_attribute WHERE product_id IN(" . $product_ids . ")");
        $this->db->query("DELETE FROM {$DB_PREFIX}shopee_upload_product_logistic WHERE product_id IN(" . $product_ids . ")");
        $this->db->query("DELETE FROM {$DB_PREFIX}shopee_upload_product_shop_update_result WHERE shopee_upload_product_id IN(" . $product_ids . ")");

    }

    public function getShopeeUploadProductIdByBestmeProductId($bestme_product_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT `shopee_upload_product_id` FROM `{$DB_PREFIX}shopee_upload_product` WHERE `bestme_product_id` = {$bestme_product_id}";

        $query = $this->db->query($sql);

        return isset($query->row['shopee_upload_product_id']) ? $query->row['shopee_upload_product_id'] : 0;
    }

    public function checkProductFullInfo($product_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM `{$DB_PREFIX}shopee_upload_product` 
                    WHERE `shopee_upload_product_id` = {$product_id}";

        $query = $this->db->query($sql);
        $product = $query->row;

        if (!isset($product['name']) || empty($product['name'])) {
            return false;
        }

        if (!isset($product['description']) || empty($product['description'])) {
            return false;
        }

        if (!isset($product['category_id']) || empty($product['category_id'])) {
            return false;
        }

        if (isset($product['category_id']) && !empty($product['category_id']) && $this->categoryHasAttribute($product['category_id'])) {
            return false;
        }

        if (!isset($product['image']) || empty($product['image'])) {
            return false;
        }

        $pass = $this->checkPriceAndStock($product_id);
        if ($pass) {
            return false;
        }

        if (!isset($product['weight']) || empty($product['weight'])) {
            return false;
        }

        if (!isset($product['length']) || empty($product['length'])) {
            return false;
        }

        $sql = "SELECT COUNT(product_id) as total 
                  FROM `{$DB_PREFIX}shopee_upload_product_logistic` 
                    WHERE `product_id` = {$product_id}";
        $query = $this->db->query($sql);

        if (!isset($query->row['total']) || $query->row['total'] == 0) {
            return false;
        }

        return true;
    }

    public function checkProductInProcessUpload($product_id)
    {
        $DB_PREFIX = DB_PREFIX;

        $query = $this->db->query("SELECT COUNT(`id`) as total FROM `{$DB_PREFIX}shopee_upload_product_shop_update_result` 
                                    WHERE `shopee_upload_product_id` = {$product_id} AND `in_process` = 1");

        return $query->row['total'];
    }

    public function categoryHasAttribute($category_id)
    {
        $has_attr = false;

        $shop_id = $this->getFirstShopId();
        $language_code = $this->getCurrentLanguageId();

        $shopee = Sale_Channel::getSaleChannel('shopee', ['shop_id' => (int)$shop_id]);
        $result_api = $shopee->getAttribute(['language' => $language_code, 'category_id' => (int)$category_id]);

        if (!isset($result_api) || empty($result_api)) {
            return $has_attr;
        }

        $result = json_decode($result_api, true);

        if (!is_array($result) || !isset($result) || empty($result)) {
            return $has_attr;
        }

        $attributes = (array_key_exists('attributes', $result)) ? $result['attributes'] : [];

        if (!is_array($attributes) || !isset($attributes) || empty($attributes)) {
            return $has_attr;
        }

        foreach ($attributes as $attribute) {
            if (isset($attribute['is_mandatory']) && $attribute['is_mandatory']) {
                $has_attr = true;

                break;
            }
        }

        return $has_attr;
    }

    public function checkPriceAndStock($product_id)
    {
        $result = false;

        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM `{$DB_PREFIX}shopee_upload_product_version` 
                    WHERE `shopee_upload_product_id` = {$product_id}";

        $query = $this->db->query($sql);
        $products = $query->rows;

        foreach ($products as $product_version) {
            if (!isset($product_version['version_price']) || empty($product_version['version_price'])) {
                $result = true;

                break;
            }

            if (!isset($product_version['version_stock']) || empty($product_version['version_stock'])) {
                $result = true;

                break;
            }
        }

        if (!isset($products) || empty($products)) {
            $sql = "SELECT * FROM `{$DB_PREFIX}shopee_upload_product` 
                    WHERE `shopee_upload_product_id` = {$product_id}";

            $query = $this->db->query($sql);
            $product = $query->row;

            if (!isset($product['price']) || empty($product['price'])) {
                $result = true;
            }

            if (!isset($product['stock']) || empty($product['stock'])) {
                $result = true;
            }
        }

        return $result;
    }

    /**
     * @param $data
     * @return array
     */
    private function detectImages($data)
    {
        $thumb = NULL;
        $images = [];
        if (isset($data['images']) && count($data['images']) > 0) {
            $thumb = $data['images'][0];
            $images = array_splice($data['images'], 1, count($data['images']) - 1);
        }

        return [$thumb, $images];
    }

    /**
     * @param $data
     * @return false|mixed|string
     */
    private function detectVersionAttribute($data)
    {
        $attributes = '';

        if (isset($data['product_attributes'])) {
            $attributes = $data['product_attributes'];
        } elseif (isset($data['attribute_name']) && isset($data['attribute_values']) && count($data['attribute_name']) > 0) {
            $attributes = [];
            foreach ($data['attribute_name'] as $key => $attribute) {
                if (isset($data['attribute_values'][$key])) {
                    $attributes[] = [
                        'name' => trim($attribute),
                        'values' => $data['attribute_values'][$key]
                    ];
                }
            }
            $attributes = json_encode($attributes);
        }
        return $attributes;
    }

    /**
     * @param $images
     * @param $product_id
     */
    private function insertProductImage($images, $product_id)
    {

        if (count($images) == 0) {
            return;
        }

        $insert_data = [];
        $sql_insert_images = "INSERT INTO " . DB_PREFIX . "shopee_upload_product_image (product_id, image, sort_order) VALUES ";
        foreach ($images as $key => $image) {
            $insert_data[] = "('" . $product_id . "' ,'" . $this->db->escape($image) . "', '" . $key . "')";
        }
        $sql_insert_images .= implode(', ', $insert_data);
        $this->db->query($sql_insert_images);
    }

    /**
     * @param $category_attrs
     * @param $product_id
     */
    private function insertCategoryAttributes($category_attrs, $product_id)
    {
        $insert_data = [];
        $sql_insert_category_attr = "INSERT INTO " . DB_PREFIX . "shopee_upload_product_attribute (product_id, attribute_id, value) VALUES ";
        foreach ($category_attrs as $key => $category_attr) {
            $insert_data[] = "('" . $product_id . "' , '" . $key . "','" . $this->db->escape($category_attr) . "')";
        }
        $sql_insert_category_attr .= implode(', ', $insert_data);

        $this->db->query($sql_insert_category_attr);
    }

    /**
     * @param $data
     * @param $product_id
     */
    public function insertProductVersion($data, $product_id)
    {
        if (isset($data['product_version_names']) && count($data['product_version_names']) > 0) {
            $insert_data = [];
            $product_version_names = $data['product_version_names'];
            $sql_insert_version = "INSERT INTO " . DB_PREFIX . "shopee_upload_product_version (shopee_upload_product_id, version_name, version_price, version_stock, version_sku) VALUES ";
            foreach ($product_version_names as $key => $product_version_name) {
                $price = (float)str_replace(',', '', $data['product_prices'][$key]);

                $insert_data[] = "('" . $product_id . "',
                     '" . $this->db->escape($product_version_name) . "',
                     '" . $this->db->escape($price) . "',
                     '" . $this->db->escape($data['product_quantities'][$key]) . "',
                     '" . $this->db->escape($data['product_skus'][$key]) . "'
                     )";
            }
            $sql_insert_version .= implode(', ', $insert_data);

            $this->db->query($sql_insert_version);
        }
    }

    /**
     * @param $data
     * @param $product_id
     * @param bool $allow_update_product_status if true, get product admin to shopee_upload table
     */
    private function insertProductUpdateResult($data, $product_id, $allow_update_product_status = false)
    {
        if (isset($data['logistics']) && $data['logistics'] &&
            isset($data['images']) && $data['images'] &&
            isset($data['images'][0]) && $data['images'][0] &&
            isset($data['weight']) && $data['weight'] &&
            isset($data['length']) && $data['length'] &&
            isset($data['height']) && $data['height'] &&
            isset($data['width']) && $data['width'] &&
            isset($data['name']) && $data['name'] &&
            isset($data['description']) && $data['description'] &&
            isset($data['category_id']) && $data['category_id']
        ) {
            $status = 1;
        } else {
            $status = 0;
        }

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "shopee_upload_product_shop_update_result WHERE shopee_upload_product_id=" . (int)$product_id);
        $record = $query->row;
        if (!empty($record)) {
            if ($allow_update_product_status) {
                $this->db->query("UPDATE " . DB_PREFIX . "shopee_upload_product_shop_update_result set status='" . $status . "' WHERE `shopee_upload_product_id`=" . $product_id);
            } else {
                $this->db->query("UPDATE " . DB_PREFIX . "shopee_upload_product_shop_update_result set status='" . $status . "' WHERE `shopee_upload_product_id`=" . $product_id . " AND `status` != 2 AND `status` != 3");
            }
        } else {
            $this->db->query("INSERT INTO " . DB_PREFIX . "shopee_upload_product_shop_update_result (shopee_upload_product_id, status) VALUES ('" . (int)$product_id . "', '" . $status . "')");
        }
    }

    /**
     * @return string
     */
    private function getCurrentLanguageId()
    {
        $this->load->model('localisation/language');
        $language = $this->model_localisation_language->getLanguageByCode($this->config->get('config_language'));

        return isset($language['language_id']) && $language['language_id'] == 2 ? 'vi' : 'en'; // default 'vn'
    }
}