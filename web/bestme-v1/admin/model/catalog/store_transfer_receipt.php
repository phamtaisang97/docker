<?php
/**
 * Created by PhpStorm.
 * User: Microsoft Windows
 * Date: 19/06/2020
 * Time: 9:30 AM
 */

Class ModelCatalogStoreTransferReceipt extends Model
{
    const STORE_TRANSFER_RECEIPT_STATUS_ID_DRAFT = 0;
    const STORE_TRANSFER_RECEIPT_STATUS_ID_TRANSFERED = 1;
    const STORE_TRANSFER_RECEIPT_STATUS_ID_RECIVED = 2;
    const STORE_TRANSFER_RECEIPT_STATUS_ID_CANCELED = 9;
    

    public function getTotalStoreTransferReceipts($filter_data)
    {
        $sql = "SELECT COUNT(DISTINCT str.store_transfer_receipt_id) AS total from  " . DB_PREFIX . "store_transfer_receipt str ";

        $sql .= "WHERE str.status <> 99";

        if (isset($filter_data['filter_status']) && $filter_data['filter_status'] !== '') {
            $sql .= ' AND str.status IN (' . $filter_data['filter_status'] . ')';
        }

        if (isset($filter_data['filter_name']) && $filter_data['filter_name'] !== '') {
            $sql .= " AND str.`code` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%' ";
        }

        if (isset($filter_data['filter_store_export']) && $filter_data['filter_store_export'] !== '') {
            $sql .= ' AND str.export_store_id IN (' . $filter_data['filter_store_export'] . ')';
        }

        if (isset($filter_data['filter_store_import']) &&  $filter_data['filter_store_import'] !== '') {
            $sql .= ' AND str.import_store_id IN (' . $filter_data['filter_store_import'] . ')';
        }

        if (isset($filter_data['start_export']) && $filter_data['start_export'] !== '') {
            $sql .= " AND str.date_exported BETWEEN '" . $filter_data['start_export'] . "' AND '". $filter_data['end_export']."'";
        }

        if (isset($filter_data['start_import']) && $filter_data['start_import'] !== '') {
            $sql .= " AND str.date_imported BETWEEN '" . $filter_data['start_import'] . "' AND '". $filter_data['end_import']."'";
        }

        if (isset($filter_data['current_user_id']) && $filter_data['current_user_id'] !== '') {
            $sql .= " AND str.user_create_id = " . $filter_data['current_user_id'];
        }

        if (isset($filter_data['user_store_id']) && $filter_data['user_store_id'] !== '') {
            $sql .= " AND str.export_store_id IN (". $filter_data['user_store_id'] .")";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getStoreTransferReceiptsCustom($filter_data)
    {
        if ($filter_data['filter_data'] == '') {
            $data = $this->getStoreTransferReceipts($filter_data);
            return $data;
        }

        $sql = "SELECT str.* FROM " . DB_PREFIX . "store_transfer_receipt str ";

        $sql .= " WHERE str.status <> 99";

        if (isset($filter_data['filter_status']) && $filter_data['filter_status'] !== '') {
            $sql .= ' AND str.status IN (' . $filter_data['filter_status'] . ')';
        }

        if (isset($filter_data['filter_name']) && $filter_data['filter_name'] !== '') {
            $sql .= " AND str.`code` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%' ";
        }

        if (isset($filter_data['filter_store_export']) && $filter_data['filter_store_export'] !== '') {
            $sql .= ' AND str.export_store_id IN (' . $filter_data['filter_store_export'] . ')';
        }

        if (isset($filter_data['filter_store_import']) &&  $filter_data['filter_store_import'] !== '') {
            $sql .= ' AND str.import_store_id IN (' . $filter_data['filter_store_import'] . ')';
        }

        if (isset($filter_data['start_export']) && $filter_data['start_export'] !== '') {
            $sql .= " AND str.date_exported BETWEEN '" . $filter_data['start_export'] . "' AND '". $filter_data['end_export']."'";
        }

        if (isset($filter_data['start_import']) && $filter_data['start_import'] !== '') {
            $sql .= " AND str.date_imported BETWEEN '" . $filter_data['start_import'] . "' AND '". $filter_data['end_import']."'";
        }

        if (isset($filter_data['current_user_id']) && $filter_data['current_user_id'] !== '') {
            $sql .= " AND str.user_create_id = " . $filter_data['current_user_id'];
        }

        if (isset($filter_data['user_store_id']) && $filter_data['user_store_id'] !== '') {
            $sql .= " AND str.export_store_id IN (". $filter_data['user_store_id'] .")";
        }

        $sql .= " ORDER BY date_modified DESC";

        if (isset($filter_data['start']) || isset($filter_data['limit'])) {
            if ($filter_data['start'] < 0) {
                $filter_data['start'] = 0;
            }

            if ($filter_data['limit'] < 1) {
                $filter_data['limit'] = 20;
            }
            $sql .= " LIMIT " . (int)$filter_data['start'] . "," . (int)$filter_data['limit'];
        }

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getStoreTransferReceipts($data = array())
    {
        $sql = "SELECT str.* FROM " . DB_PREFIX . "store_transfer_receipt str ";

        $sql .= " WHERE str.status <> 99";
        if (!empty($data['filter_name'])) {
            $sql .= " AND str.code LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (isset($data['start_export']) && $data['start_export'] !== '') {
            $sql .= " AND str.date_exported BETWEEN '" . $data['start_export'] . "' AND '". $data['end_export']."'";
        }

        if (isset($data['start_import']) && $data['start_import'] !== '') {
            $sql .= " AND str.date_imported BETWEEN '" . $data['start_import'] . "' AND '". $data['end_import']."'";
        }

        if (isset($data['current_user_id']) && $data['current_user_id'] !== '') {
            $sql .= " AND str.user_create_id = " . $data['current_user_id'];
        }

        if (isset($data['user_store_id']) && $data['user_store_id'] !== '') {
            $sql .= " AND str.export_store_id IN (". $data['user_store_id'] .")";
        }

        $sql .= " ORDER BY date_modified DESC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }
    public function addStoreTransferReceipt($data)
    {

        $export_store_id = isset($data['export_store']) ? $data['export_store'] : '';
        $import_store_id = isset($data['import_store']) ? $data['import_store'] : '';
        $user_create_id = isset($data['user_create_id']) ? $data['user_create_id'] : 1;
        $status = isset($data['type_submit']) ? $data['type_submit'] : '';
        $common_quantity_transfer = isset($data['common_quantity_transfer']) ? $data['common_quantity_transfer'] : '';

        if ($export_store_id === '' || $import_store_id === '' || $status === '') {
            return;
        }

        $total_value = (isset($data['total_value']) && is_array($data['total_value'])) ? $data['total_value'] : [];
        $total = 0;
        foreach ($total_value as $value) {
            $total += (float)$value;
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "store_transfer_receipt` SET
                        `export_store_id` = " . (int)$export_store_id . ", 
                        `import_store_id` = " . (int)$import_store_id . ", 
                        `common_quantity_transfer` = " . (int)$common_quantity_transfer . ", 
                        `total` = " . (float)$total . ", 
                        `status` = " . $status . ", 
                        `user_create_id` = " . $user_create_id . ", 
                        `date_added` = NOW(), 
                        `date_modified` = NOW()
                        ");

        $store_transfer_receipt_id = $this->db->getLastId();

        if ($data['type_submit'] == ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_TRANSFERED) {
            $this->db->query("UPDATE " . DB_PREFIX . "store_transfer_receipt 
                          SET `date_exported` = NOW() 
                          WHERE store_transfer_receipt_id = '" . (int)$store_transfer_receipt_id . "'");
        }

        // complement for version [v3.1.3] Generate demo data Website
        if ($data['type_submit'] == self::STORE_TRANSFER_RECEIPT_STATUS_ID_RECIVED) {
            $this->db->query("UPDATE " . DB_PREFIX . "store_transfer_receipt 
                          SET `date_imported` = NOW(), `date_exported` = NOW()  
                          WHERE store_transfer_receipt_id = '" . (int)$store_transfer_receipt_id . "'");
        }

        $prefix = 'PCH';
        $code = $prefix . str_pad($store_transfer_receipt_id, 6, "0", STR_PAD_LEFT);

        $this->db->query("UPDATE " . DB_PREFIX . "store_transfer_receipt 
                          SET `code` = '" . $code . "' 
                          WHERE store_transfer_receipt_id = '" . (int)$store_transfer_receipt_id . "'");

        if (isset($data['product_version_id'])) {
            $product_version_ids = is_array($data['product_version_id']) ? $data['product_version_id'] : [];
            foreach ($product_version_ids as $key => $pvid) {
                $arr_pvid = explode('-', $pvid);
                $product_id = isset($arr_pvid[0]) ? $arr_pvid[0] : '';
                $version_id = isset($arr_pvid[1]) ? $arr_pvid[1] : '';

                if (!$product_id){
                    continue;
                }

                $real_quantity = isset($data['product_pre_quantity'][$key]) ? extract_number($data['product_pre_quantity'][$key]) : 0;
                $product_trans_quantity = isset($data['product_quantity'][$key]) ? extract_number($data['product_quantity'][$key]) : 0;
                $price_trans = isset($data['product_cost_price'][$key]) ? extract_number($data['product_cost_price'][$key]) : 0;
                //$after_qty = $real_quantity - $product_trans_quantity;

                $this->db->query("INSERT INTO `" . DB_PREFIX . "store_transfer_receipt_to_product` SET
                                                                `store_transfer_receipt_id` = " . (int)$store_transfer_receipt_id .", 
                                                                `product_id` = " . (int)$product_id . ", 
                                                                `product_version_id` = '" . (int)$version_id . "', 
                                                                `current_quantity` = '" . (int)$real_quantity . "', 
                                                                `quantity` = '" . (int)$product_trans_quantity . "', 
                                                                `price` = " . (float)$price_trans . "
                        ");

                // update quantity
                if ($status && $status == self::STORE_TRANSFER_RECEIPT_STATUS_ID_TRANSFERED) {
                    $this->updateQuantityProduct($status,
                        $export_store_id, $product_id, $version_id, $product_trans_quantity,0, true, $price_trans);
                }
            }
        }

        return $store_transfer_receipt_id;
    }

    public function editStoreTransferReceipt($transfer_receipt_id, $data)
    {
        $export_store_id = isset($data['export_store']) ? $data['export_store'] : '';
        $import_store_id = isset($data['import_store']) ? $data['import_store'] : '';
        $status = isset($data['type_submit']) ? $data['type_submit'] : '';
        $common_quantity_transfer = isset($data['common_quantity_transfer']) ? $data['common_quantity_transfer'] : '';
        $store_transfer_receipt_id = isset($transfer_receipt_id) ? $transfer_receipt_id : '';

        if ($export_store_id === '' || $import_store_id === '' || $status === '' || $store_transfer_receipt_id === '') {
            return;
        }

        $total_value = (isset($data['total_value']) && is_array($data['total_value'])) ? $data['total_value'] : [];
        $total = 0;
        foreach ($total_value as $value) {
            $total += (float)extract_number($value);
        }

        $before_store_transfer_receipt = $this->getStoreTransferReceiptById($store_transfer_receipt_id);
        $before_store_transfer_receipt_product = $this->getStoreTransferReceiptToProductById($store_transfer_receipt_id);
        // update quantity before update store_transfer_receipt

        if ($status == self::STORE_TRANSFER_RECEIPT_STATUS_ID_RECIVED) {
            foreach ($before_store_transfer_receipt_product as $product) {
                $this->updateQuantityBeforeEdit($before_store_transfer_receipt['export_store_id'], $product['product_id'], $product['product_version_id'], $product['quantity']);
            }
        }

        $this->db->query("UPDATE " . DB_PREFIX . "store_transfer_receipt SET 
                               `export_store_id` = " . (int)$export_store_id . ", 
                               `import_store_id` = " . (int)$import_store_id . ", 
                               `common_quantity_transfer` = " . (int)$common_quantity_transfer . ", 
                               `total` = " . (float)$total . ", 
                               `status` = " . $status . ", 
                               `date_modified` = NOW() 
                                WHERE `store_transfer_receipt_id` = '" . (int)$store_transfer_receipt_id . "'");

        if ($status && $status == self::STORE_TRANSFER_RECEIPT_STATUS_ID_TRANSFERED) {
            $this->db->query("UPDATE " . DB_PREFIX . "store_transfer_receipt 
                          SET `date_exported` = NOW()  
                          WHERE store_transfer_receipt_id = '" . (int)$store_transfer_receipt_id . "'");
        }

        if ($status && $status == self::STORE_TRANSFER_RECEIPT_STATUS_ID_RECIVED) {
            $this->db->query("UPDATE " . DB_PREFIX . "store_transfer_receipt 
                          SET `date_imported` = NOW()  
                          WHERE store_transfer_receipt_id = '" . (int)$store_transfer_receipt_id . "'");
        }

        // delete
        $this->db->query("DELETE FROM " . DB_PREFIX . "store_transfer_receipt_to_product WHERE `store_transfer_receipt_id` = '" . (int)$store_transfer_receipt_id . "'");

        if (isset($data['product_version_id'])) {
            $product_version_ids = is_array($data['product_version_id']) ? $data['product_version_id'] : [];
            foreach ($product_version_ids as $key => $pvid) {
                $arr_pvid = explode('-', $pvid);
                $product_id = isset($arr_pvid[0]) ? $arr_pvid[0] : '';
                $version_id = isset($arr_pvid[1]) ? $arr_pvid[1] : '';

                if (!$product_id){
                    continue;
                }

                $real_quantity = isset($data['product_pre_quantity'][$key]) ? extract_number($data['product_pre_quantity'][$key]) : 0;
                $product_trans_quantity = isset($data['product_quantity'][$key]) ? extract_number($data['product_quantity'][$key]) : 0;
                $price_trans = isset($data['product_cost_price'][$key]) ? extract_number($data['product_cost_price'][$key]) : 0;
                $current_quantity_trans = isset($data['current_quantity_trans'][$key]) ? extract_number($data['current_quantity_trans'][$key]) : 0;
//                $after_qty = $real_quantity - $product_trans_quantity;

                $this->db->query("INSERT INTO `" . DB_PREFIX . "store_transfer_receipt_to_product` SET 
                                                                `store_transfer_receipt_id` = " . (int)$store_transfer_receipt_id .", 
                                                                `product_id` = " . (int)$product_id . ", 
                                                                `product_version_id` = '" . (int)$version_id . "', 
                                                                `current_quantity` = '" . (int)$real_quantity . "', 
                                                                `quantity` = '" . (int)$product_trans_quantity . "', 
                                                                `price` = " . (float)$price_trans . " 
                        ");

                // update quantity
                if ($status && $status == self::STORE_TRANSFER_RECEIPT_STATUS_ID_TRANSFERED) {

                    $this->updateQuantityProduct($status,
                        $export_store_id, $product_id, $version_id, $product_trans_quantity, $current_quantity_trans,true);

                } else if ($status && $status == self::STORE_TRANSFER_RECEIPT_STATUS_ID_RECIVED) {

                    $this->updateQuantityProduct($status,
                        $export_store_id, $product_id, $version_id, $product_trans_quantity, $current_quantity_trans, true);

                    $this->updateQuantityProduct($status,
                        $import_store_id, $product_id, $version_id, $product_trans_quantity, $current_quantity_trans, false, $price_trans);

                } else if ($status && $status == self::STORE_TRANSFER_RECEIPT_STATUS_ID_CANCELED &&
                    isset($before_store_transfer_receipt['status']) && $before_store_transfer_receipt['status'] != self::STORE_TRANSFER_RECEIPT_STATUS_ID_DRAFT) {
                    // back to real quantity of export_store
                    $this->updateQuantityProduct($status,
                        $export_store_id, $product_id, $version_id, $product_trans_quantity, $current_quantity_trans, false);

                    // back to real quantity of import_store
                    $this->updateQuantityProduct($status,
                        $import_store_id, $product_id, $version_id, $product_trans_quantity, $current_quantity_trans, true, $price_trans);
                }
            }
        }
    }

    public function getStoreTransferReceiptById($store_transfer_receipt_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT DISTINCT * FROM {$DB_PREFIX}store_transfer_receipt str 
                WHERE str.store_transfer_receipt_id = '" . (int)$store_transfer_receipt_id . "' ";

        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getStoreTransferReceiptToProductById($store_transfer_receipt_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM {$DB_PREFIX}store_transfer_receipt_to_product strp 
                WHERE strp.store_transfer_receipt_id = '" . (int)$store_transfer_receipt_id . "' ";

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getProductByStore($store_id, $data = array())
    {
        $sql = "SELECT p.user_create_id, p.status as p_status, p.sku as psku, p.sale_on_out_of_stock as p_sale_on_out_of_stock, p.product_id as pid, p.multi_versions, p.price as o_price, p.compare_price as o_compare_price, p.weight as weight, pts.cost_price, pts.quantity as s_quantity, dp.name,p.image, pv.* 
                from " . DB_PREFIX . "product p 
                LEFT JOIN " . DB_PREFIX . "product_description as dp ON (p.product_id = dp.product_id) 
                LEFT JOIN " . DB_PREFIX . "product_version as pv ON CASE 
                                                                            WHEN p.multi_versions = 1 THEN p.product_id 
                                                                            ELSE -1
                                                                            END = pv.product_id
                INNER JOIN " . DB_PREFIX . "product_to_store as pts ON (p.product_id = pts.product_id AND CASE WHEN p.multi_versions = 1 THEN pv.product_version_id ELSE 0 END = pts.product_version_id AND pts.store_id = " . (int)$store_id .") 
                WHERE 1=1 and dp.language_id = " . (int)$this->config->get('config_language_id');

        if (isset($data['current_user_id']) && $data['current_user_id'] !== '') {
            $sql .= " AND p.user_create_id = " . $data['current_user_id'];
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND dp.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " AND ( p.`deleted` IS NULL)";
        $sql .= " AND (pv.`deleted` IS NULL)";
        $sql .= " AND (pts.`quantity` > 0)";
        $sql .= " ORDER BY dp.name ASC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);
        $result = array();
        foreach ($query->rows as $k => $item) {
            $result[$k] = array(
                'product_id' => $item['pid'],
                'name' => mb_strlen($item['name']) > 90 ? mb_substr($item['name'], 0, 90) . "..." : $item['name'],
                'product_version_id' => $item['product_version_id'],
                'product_name' => $item['name'],
                'weight' => $item['weight'],
                'image' => $item['image'],
                'version' => $item['version'],
                'sale_on_out_of_stock' => $item['p_sale_on_out_of_stock'],
                'multi_versions' => $item['multi_versions']
            );

            if ($item['product_version_id'] != null) {
                $result[$k]['id'] = $item['pid'] . '-' . $item['product_version_id'];
                $result[$k]['quantity'] = $item['s_quantity'];
                $result[$k]['price'] = $item['price'] != 0 ? $item['price'] : $item['compare_price'];
                $result[$k]['status'] = $item['status'];
                $result[$k]['p_status'] = $item['p_status'];
                $result[$k]['sku'] = $item['sku'];
                $result[$k]['cost_price'] = $item['cost_price'];
            } else {
                $result[$k]['id'] = $item['pid'];
                $result[$k]['quantity'] = $item['s_quantity'];
                $result[$k]['price'] = $item['o_price'] != 0 ? $item['o_price'] : $item['o_compare_price'];
                $result[$k]['status'] = $item['p_status'];
                $result[$k]['p_status'] = $item['p_status'];
                $result[$k]['sku'] = $item['psku'];
                $result[$k]['cost_price'] = $item['cost_price'];
            }
        }

        return $result;
    }

    public function countProductByStore($store_id, $data = array())
    {
        $language_id = $this->config->get('config_language_id');
        $sql = "SELECT p.product_id,pd.name from " . DB_PREFIX . "product p 
                    LEFT JOIN " . DB_PREFIX . "product_description as pd ON (p.product_id = pd.product_id) 
                    LEFT JOIN " . DB_PREFIX . "product_version as pv ON (p.product_id = pv.product_id) 
                    INNER JOIN " . DB_PREFIX . "product_to_store as pts ON (p.product_id = pts.product_id AND CASE WHEN p.multi_versions = 1 THEN pv.product_version_id ELSE 0 END = pts.product_version_id AND pts.store_id = " . (int)$store_id .") WHERE 1=1";

        if (isset($data['current_user_id']) && $data['current_user_id'] !== '') {
            $sql .= " AND p.user_create_id = " . $data['current_user_id'];
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " AND (p.`deleted` IS NULL OR p.`deleted` = 2)";
        $sql .= " AND (pv.`deleted` IS NULL OR pv.`deleted` = 2)";
        $sql .= " AND pd.language_id = " . $language_id . "";
        $sql .= " ORDER BY p.product_id ASC";
        $query = $this->db->query($sql);

        return count($query->rows);
    }

    private function updateQuantityProduct($status, $store_id, $product_id, $product_version_id, $quantity_trans_edit, $quantity_trans, $condition = false, $price = 0)
    {
        $result = $this->getProductInProductToStore($store_id, $product_id, $product_version_id);

        if (isset($result['quantity']) && isset($result['cost_price'])) {
            $qty = $result['quantity'];
            $current_cost_price = (float)$result['cost_price'];

            if ($status && $status == self::STORE_TRANSFER_RECEIPT_STATUS_ID_TRANSFERED) {
                $qty = $result['quantity'] - $quantity_trans_edit;
            } elseif ($status && $status == self::STORE_TRANSFER_RECEIPT_STATUS_ID_RECIVED) {
                $qty_use_cal_cost_price = ($result['quantity'] > 0) ? $result['quantity'] : 0;

                $qty = $condition ? (int)$result['quantity'] - (int)$quantity_trans_edit : (int)$result['quantity'] + (int)$quantity_trans_edit;
                $current_cost_price = $condition ? (float)$result['cost_price'] :
                    (((float)$result['cost_price'] * $qty_use_cal_cost_price) + ((float)$price) * (int)$quantity_trans_edit) / ((int)$qty_use_cal_cost_price + (int)$quantity_trans_edit);

            } elseif ($status && $status == self::STORE_TRANSFER_RECEIPT_STATUS_ID_CANCELED) {

                $qty = $condition ? (int)$result['quantity'] - (int)$quantity_trans : (int)$result['quantity'] + (int)$quantity_trans;

            }

            $this->db->query("UPDATE `" . DB_PREFIX . "product_to_store` 
                                SET `quantity` = " . $qty . ", 
                                    `cost_price` = " . (float)$current_cost_price . " 
                                WHERE `store_id` = " . (int)$store_id . " 
                                  AND `product_id` = " . (int)$product_id . " 
                                  AND `product_version_id` = " . (int)$product_version_id);
        } else {
            if ($status && $status != self::STORE_TRANSFER_RECEIPT_STATUS_ID_CANCELED) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "product_to_store` 
                                  SET `store_id` = " . (int)$store_id . ",  
                                      `product_id` = " . (int)$product_id . ", 
                                      `product_version_id` = " . (int)$product_version_id. ", 
                                      `quantity` = " . (int)$quantity_trans_edit . ", 
                                      `cost_price` = " . (float)$price . " ");
            }
        }

    }

    public function getProductInProductToStore($store_id, $product_id, $product_version_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT DISTINCT * FROM {$DB_PREFIX}product_to_store pts 
                WHERE pts.store_id = " . (int)$store_id . " 
                AND pts.product_id = " . (int)$product_id . " 
                AND pts.product_version_id = " . (int)$product_version_id;

        $query = $this->db->query($sql);

        return $query->row;
    }

    public function updateReceiptStatus($receiptId, $receiptStatusId)
    {
        $DB_PREFIX = DB_PREFIX;

        if (!isset($receiptId) || !isset($receiptStatusId)) {
            return;
        }

        $receipt = $this->getStoreTransferReceiptById($receiptId);
        $receipt_products = $this->getStoreTransferReceiptToProductById($receiptId);

        if (!isset($receipt['store_transfer_receipt_id']) || !isset($receipt['export_store_id']) || !isset($receipt['import_store_id'])) {
            return;
        }

        foreach ($receipt_products as $product) {
            if (!isset($product['product_id'])) {
                continue;
            }

            switch ($receiptStatusId) {
                case self::STORE_TRANSFER_RECEIPT_STATUS_ID_TRANSFERED:
                    $this->updateQuantityProduct($receiptStatusId, $receipt['export_store_id'], $product['product_id'],
                        $product['product_version_id'], $product['quantity'], 0,true);

                    $this->db->query("UPDATE {$DB_PREFIX}store_transfer_receipt 
                                  SET `date_exported` = NOW() 
                                  WHERE store_transfer_receipt_id = '" . (int)$receipt['store_transfer_receipt_id'] . "'");
                    break;

                case self::STORE_TRANSFER_RECEIPT_STATUS_ID_RECIVED:
                    $this->updateQuantityProduct($receiptStatusId, $receipt['import_store_id'], $product['product_id'],
                        $product['product_version_id'], $product['quantity'], $product['quantity'], false, $product['price']);

                    $this->db->query("UPDATE {$DB_PREFIX}store_transfer_receipt 
                                  SET `date_imported` = NOW() 
                                  WHERE store_transfer_receipt_id = '" . (int)$receipt['store_transfer_receipt_id'] . "'");
                    break;

                case self::STORE_TRANSFER_RECEIPT_STATUS_ID_CANCELED:
                    if (isset($receipt['status']) && $receipt['status'] != self::STORE_TRANSFER_RECEIPT_STATUS_ID_DRAFT) {
                    // back to real quantity of export_store
                    $this->updateQuantityProduct($receiptStatusId, $receipt['export_store_id'], $product['product_id'],
                        $product['product_version_id'], $product['quantity'], $product['quantity'], false);

                    // back to real quantity of import_store
                    $this->updateQuantityProduct($receiptStatusId, $receipt['import_store_id'], $product['product_id'],
                        $product['product_version_id'], $product['quantity'], $product['quantity'], true, $product['price']);
                }
                    break;
            }
        }

        $this->db->query("UPDATE {$DB_PREFIX}store_transfer_receipt 
                                  SET `status` = '" . (int)$receiptStatusId . "', 
                                      `date_modified` = NOW() 
                                  WHERE store_transfer_receipt_id = '" . (int)$receipt['store_transfer_receipt_id'] . "'");
    }

    public function updateQuantityBeforeEdit($store_id, $product_id, $product_version_id, $quantity)
    {
        $this->db->query("UPDATE `" . DB_PREFIX . "product_to_store` 
                          SET `quantity` = quantity + '" . (int)$quantity . "' 
                          WHERE `store_id` = " . (int)$store_id . " 
                            AND `product_id` = " . (int)$product_id . " 
                            AND `product_version_id` = " . (int)$product_version_id);
    }
}