<?php

class ModelCatalogCategory extends Model
{
    use RabbitMq_Trait;

    public function addCategory($data)
    {
        /*
         * $data sample:
         *
         * [
         *    "parent_id" => 0,
         *    "status" => 1,
         *    "category_description" => $name,
         *    "name" => $name,
         *    "image" => '',
         *    "sub_categories" => [
         *        "cat-1",
         *        "cat-2"
         *    ],
         * ];
         */

        /* TODO: check if category existed... */

        /* Create new category */
        $cateogry_image = isset($data['image']) ? trim($data['image']) : '';
        $this->db->query("INSERT INTO " . DB_PREFIX . "category 
                          SET parent_id = '" . (int)$data['parent_id'] . "', 
                          status = '1', 
                          image = '" . $this->db->escape($cateogry_image) . "', 
                          date_modified = NOW(), 
                          date_added = NOW()");

        $category_id = $this->db->getLastId();
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        if (isset($data['category_description']) && isset($data['name'])) {
            // add with all languages
            $meta_title = isset($data['meta_title']) ? $data['meta_title'] : '';
            $meta_keyword = isset($data['meta_keyword']) ? $data['meta_keyword'] : '';
            $meta_description = isset($data['meta_description']) ? $data['meta_description'] : '';
            $noindex = isset($data['noindex']) ? $data['noindex'] : 0;
            $nofollow = isset($data['nofollow']) ? $data['nofollow'] : 0;
            $noarchive = isset($data['noarchive']) ? $data['noarchive'] : 0;
            $noimageindex = isset($data['noimageindex']) ? $data['noimageindex'] : 0;
            $nosnippet = isset($data['nosnippet']) ? $data['nosnippet'] : 0;

            foreach ($languages as $language) {
                $language_id = $language['language_id'];
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_description 
                                  SET category_id = '" . (int)$category_id . "', 
                                  language_id = '" . (int)$language_id . "', 
                                  `name` = '" . $this->db->escape(trim($data['name'])) . "', 
                                  `description` = '" . $this->db->escape(trim($data['name'])) . "',
                                  `meta_title` = '" . $this->db->escape(trim($meta_title)) . "', 
                                  `meta_keyword` = '" . $this->db->escape(trim($meta_keyword)) . "', 
                                  `meta_description` = '" . $this->db->escape(trim($meta_description)) . "',
                                  `noindex` = '" . $this->db->escape(trim($noindex)) . "',
                                  `nofollow` = '" . $this->db->escape(trim($nofollow)) . "',
                                  `noarchive` = '" . $this->db->escape(trim($noarchive)) . "',
                                  `noimageindex` = '" . $this->db->escape(trim($noimageindex)) . "',
                                  `nosnippet` = '" . $this->db->escape(trim($nosnippet)) . "'"
                );
            }
        }

        // add product_to_category
        if (isset($data['product-choose'])) {
            foreach ($data['product-choose'] as $key => $product_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category 
                                  SET product_id = '" . (int)$product_id . "', 
                                  category_id = '" . (int)$category_id . "'");
            }
        }

        // assign to menu. Current not use. Keep for further reading... TODO: remove or reuse...
        /*if (isset($data['menu'])) {
            foreach ($data['menu'] as $menu) {
                $result = explode('-', $menu);
                $menuName = $result[0];
                $menuId = $result[1];
                $this->db->query("
                    INSERT INTO " . DB_PREFIX . "novaon_relation_table SET main_name = '" . $menuName . "', main_id = '" . $menuId . "', child_name = 'category', child_id = '" . $category_id . "', type_id = 2, redirect = 0
                ");
            }
        }*/

        /* category_path */
        $level = 0;

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` 
                                   WHERE category_id = '" . (int)$data['parent_id'] . "' 
                                   ORDER BY `level` ASC");

        foreach ($query->rows as $result) {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` 
                              SET `category_id` = '" . (int)$category_id . "', 
                              `path_id` = '" . (int)$result['path_id'] . "', 
                              `level` = '" . (int)$level . "'");

            $level++;
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` 
                          SET `category_id` = '" . (int)$category_id . "', 
                          `path_id` = '" . (int)$category_id . "', 
                          `level` = '" . (int)$level . "'");

        if (isset($data['category_filter'])) {
            foreach ($data['category_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_filter 
                                  SET category_id = '" . (int)$category_id . "', 
                                  filter_id = '" . (int)$filter_id . "'");
            }
        }

        /*
         * add category_to_store
         * TODO: support multi stores?...
         */
        $store_id = 0;
        $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store 
                          SET category_id = '" . (int)$category_id . "', 
                          store_id = '" . (int)$store_id . "'");

        $this->cache->delete('category');

        /* add relation ship with sub_categories */
        // create for new
        $sub_category_names = isset($data['sub_categories']) && is_array($data['sub_categories']) ? $data['sub_categories'] : [];
        $existing_sub_categories = $this->getCategoriesByNames($sub_category_names);

//        $existing_sub_category_names = array_map(function ($cat) {
//            return $cat['name'];
//        }, $existing_sub_categories);

        $new_sub_categories = array_diff($sub_category_names, $existing_sub_categories);
        $new_sub_categories = array_unique($new_sub_categories);

        foreach ($new_sub_categories as $new_sub_category) {
            $data_sub_cat = [
                "parent_id" => $category_id,
                "status" => 1,
                "category_description" => $new_sub_category,
                "name" => $new_sub_category,
                "sub_categories" => '[]'
            ];

            $this->addCategory($data_sub_cat);
        }

        // update for existing
        foreach ($existing_sub_categories as $existing_sub_category) {
            if ($existing_sub_category == $data['name']) {
                continue;
            }

            $existing_sub_category = trim($existing_sub_category);
            $filter_name_1 = html_entity_decode($existing_sub_category); // support new value a&b
            $filter_name_2 = htmlentities($filter_name_1); // support old value a&amp;b

            $this->db->query("UPDATE " . DB_PREFIX . "category c 
                              LEFT JOIN " . DB_PREFIX . "category_description cd ON c.category_id = cd.category_id
                              SET c.parent_id = '" . (int)$category_id . "', 
                              c.date_modified = NOW() 
                              WHERE (cd.`name` = '" . $filter_name_1 . "' OR cd.`name` = '" . $filter_name_2 . "') 
                              AND cd.`language_id` = " . (int)$this->config->get('config_language_id'));
        }

        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();

        // seo
        $this->load->model('custom/common');
        $slug = $this->model_custom_common->createSlug($data['name'], '-');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug);

        foreach ($languages as $key => $language) {
            $language_id = $language['language_id'];
            $alias = 'category_id=' . $category_id;
            $sql = "INSERT INTO " . DB_PREFIX . "seo_url 
                    SET language_id = '" . (int)$language_id . "', 
                        query = '" . $alias . "', 
                        keyword = '" . $slug_custom . "', 
                        table_name = 'category'
            ";
            $this->db->query($sql);
        }

        $this->deleteProductCaches();

        // send product to rabbit mq
        try {
            $this->sendToRabbitMq($category_id, $this->db->escape(trim($data['name'])));
        } catch (Exception $exception) {
            $this->log->write('Send message add category error: ' . $exception->getMessage());
        }

        return $category_id;
    }

    public function addCategoryByExcel($data)
    {
        /* validate excel */

        /* add category line by line */
        foreach ([] as $item) {
            $line_data = []; // read excel
            $data = []; // build from line_data
            $category_id = $this->addCategory($data);
        }

        /* ...more... */
    }

    public function getChildCategory($category_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c 
                          LEFT JOIN " . DB_PREFIX . "category_description cd 
                          ON c.category_id = cd.category_id 
                          WHERE c.`parent_id` = '" . $category_id . "' 
                          AND cd.`language_id` = " . (int)$this->config->get('config_language_id'));


        return $query->rows;
    }

    public function getCategoriesByNames(array $sub_category_names)
    {
        if (!is_array($sub_category_names) || empty($sub_category_names)) {
            return [];
        }
        $str_sub_category_name = "";
        foreach ($sub_category_names as $key => $sub_name) {
            if ($key > 0) {
                $str_sub_category_name .= ",";
            }
            $str_sub_category_name .= "'" . $sub_name . "'";
        }

        $query = $this->db->query("SELECT `name`  
                                   FROM `" . DB_PREFIX . "category_description` 
                                   WHERE `name` IN ( " . $str_sub_category_name . " ) 
                                   AND `language_id` = " . (int)$this->config->get('config_language_id'));

        $result = [];
        if ($query->num_rows > 0) {
            foreach ($query->rows as $row) {
                $result[] = $row['name'];
            }
        }

        return $result;
    }

    public function getFullChildCategories($data, $parentId = 0)
    {
        $result = [];
        $sql = "SELECT * FROM `" . DB_PREFIX . "category` c LEFT JOIN `" . DB_PREFIX . "category_description` cd ON (c.category_id = cd.category_id) 
                                            WHERE c.`parent_id` = " . (int)$parentId . " AND cd.`language_id` = " . (int)$this->config->get('config_language_id');

        if (!empty($data['filter_name'])) {
            $sql .= " AND cd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " ORDER BY c.`date_added` DESC";

        $query = $this->db->query($sql);

        foreach ($query->rows as $row) {
            $children = $this->getFullChildCategories([], $row['category_id']);
            $sub_ids = [];
            foreach ($children as $child) {
                $sub_ids[] = $child['id'];
                if (isset($child['sub_ids']) && is_array($child['sub_ids'])){
                    $sub_ids = array_merge($sub_ids, $child['sub_ids']);
                }
            }

            $result[] = [
                'id' => $row['category_id'],
                'text' => html_entity_decode($row['name']), // return a&b instead of a&amp;b
                'hasChildren' => count($children) > 0 ? true : false,
                'sub_ids' => $sub_ids,
                'children' => $children
            ];
        }

        return $result;
    }

    public function getFullChildCategoryIds($parentId)
    {
        if (empty($parentId)) {
            return [];
        }

        $allChildren = $this->getFullChildCategories($data = [], $parentId);
        if (!is_array($allChildren)) {
            return [];
        }

        $result = [];
        foreach ($allChildren as $child) {
            if (!isset($child['id'])) {
                continue;
            }

            $result[] = $child['id'];
            if (isset($child['sub_ids']) && is_array($child['sub_ids'])) {
                $result = array_merge($result, $child['sub_ids']);
            }
        }

        return $result;
    }

    ///////////////////////////////////////// START
    public function getFullTreeCategories($filter_data, $parentId = 0)
    {
        $result = $this->getFullTreeCategoriesNotFilter($filter_data, $parentId);


        return $this->getCategoryContainFilterName($result);
    }

    private function getCategoryContainFilterName($tree)
    {
        $result_filter = [];
        foreach ($tree as $k => $item) {
            if ($item['filter_name'] == 'YES'){
                $result_filter[] = $item;
            }/*else{
                $result_filter = array_merge($result_filter, $this->getCategoryContainFilterName($item['children']));
            }*/
        }

        return $result_filter;
    }

    private function getFullTreeCategoriesNotFilter($filter_data, $parentId = 0){
        $result = [];
        if (!isset($filter_data['filter_name'])) {
            $filter_data['filter_name'] = '';
        }

        $filter_name_1 = html_entity_decode($filter_data['filter_name']); // support new value a&b
        $filter_name_2 = htmlentities($filter_name_1); // support old value a&amp;b

        $sql = "SELECT *, IF(cd.`name` LIKE '%" . $this->db->escape($filter_name_1) . "%' OR cd.`name` LIKE '%" . $this->db->escape($filter_name_2) . "%', 'YES', 'NO') AS `filter_name` FROM `" . DB_PREFIX . "category` c 
                LEFT JOIN `" . DB_PREFIX . "category_description` cd ON (c.category_id = cd.category_id) 
                WHERE c.`parent_id` = " . (int)$parentId . " 
                AND cd.`language_id` = " . (int)$this->config->get('config_language_id');

        $sql .= " ORDER BY c.`date_added` DESC";

        $query = $this->db->query($sql);

        foreach ($query->rows as $row) {
            $children = $this->getFullTreeCategoriesNotFilter($filter_data, $row['category_id']);
            $sub_ids = [];
            $has_text = $row['filter_name'];
            foreach ($children as $child) {
                $sub_ids[] = $child['id'];
                if (isset($child['sub_ids']) && is_array($child['sub_ids'])){
                    $sub_ids = array_merge($sub_ids, $child['sub_ids']);
                }

                if (isset($child['filter_name']) && $child['filter_name'] == 'YES'){
                    $row['filter_name'] = 'YES';
                }
            }

            $result[] = [
                'id' => $row['category_id'],
                'text' => html_entity_decode($row['name']), // return a&b instead of a&amp;b
                'hasChildren' => count($children) > 0 ? true : false,
                'sub_ids' => $sub_ids,
                'children' => $children,
                'filter_name' => $row['filter_name'],
                'has_text' => $has_text
            ];
        }

        return $result;
    }
    ///////////////////////////////////////// END

    public function getAllSubCategoryIdByParentId($parent_id)
    {
        $result = [];
        $sql = "SELECT * FROM `" . DB_PREFIX . "category` c LEFT JOIN `" . DB_PREFIX . "category_description` cd ON (c.category_id = cd.category_id) 
                                            WHERE c.`parent_id` = " . (int)$parent_id . " AND cd.`language_id` = " . (int)$this->config->get('config_language_id');

        $query = $this->db->query($sql);

        foreach ($query->rows as $row) {
            $children_ids = $this->getAllSubCategoryIdByParentid($row['category_id']);
            $result = array_merge($result, $children_ids);

            $result[] = $row['category_id'];
        }

        return $result;
    }

    public function getLevelCategories($data, $parentId = 0, $level = 0){
        $result = $this->getFullLevelNotFilterNameCategories($data);
        foreach ($result as $k => $item){
            if (!isset($item['filter_name'])){
                continue;
            }
            if ($item['filter_name'] == 'NO'){
                unset($result[$k]);
            }
        }

        return $result;
    }

    private function getFullLevelNotFilterNameCategories($data, $parentId = 0, $level = 0)
    {
        $result = [];
        if (!isset($data['filter_name'])) {
            $data['filter_name'] = '';
        } else {
            $data['filter_name'] = trim($data['filter_name'], ' ');
        }

        $filter_name_1 = html_entity_decode($data['filter_name']); // support new value a&b
        $filter_name_2 = htmlentities($filter_name_1); // support old value a&amp;b

        $sql = "SELECT *, IF(cd.`name` LIKE '%" . $this->db->escape($filter_name_1) . "%' OR cd.`name` LIKE '%" . $this->db->escape($filter_name_2) . "%', 'YES', 'NO') AS `filter_name` FROM `" . DB_PREFIX . "category` c 
                LEFT JOIN `" . DB_PREFIX . "category_description` cd ON (c.category_id = cd.category_id) 
                WHERE c.`parent_id` = " . (int)$parentId . " 
                AND cd.`language_id` = " . (int)$this->config->get('config_language_id');

        $query = $this->db->query($sql);

        foreach ($query->rows as $row) {
            $children = $this->getFullLevelNotFilterNameCategories($data, $row['category_id'], $level + 1);

            // show this parent
            /*foreach ($children as $child){
                if ($child['filter_name'] == 'YES'){
                    $row['filter_name'] = 'YES';
                }
            }*/
            $result[] = [
                'id' => $row['category_id'],
                'text' => html_entity_decode($row['name']), // return a&b instead of a&amp;b
                'level' => $level,
                'parent_id' => $parentId,
                'filter_name' => $row['filter_name']
            ];

            $result = array_merge($result, $children);
        }

        return $result;
    }

    public function updateStatusCategory($category_id, $status) {
        $this->db->query("UPDATE " . DB_PREFIX . "category 
                          SET status = '" . (int)$status . "', 
                          date_modified = NOW()
                          WHERE category_id = '" . (int)$category_id . "'");

        $this->deleteProductCaches();
    }

    public function updateCategorySort($category_id, $sort_order) {
        $this->db->query("UPDATE " . DB_PREFIX . "category 
                          SET sort_order = '" . (int)$sort_order . "', 
                          date_modified = NOW()
                          WHERE category_id = '" . (int)$category_id . "'");

        $this->deleteProductCaches();
    }

    public function editCategory($category_id, $data)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "category 
                          SET parent_id = '" . (int)$data['parent_id'] . "', 
                          date_modified = NOW() 
                          WHERE category_id = '" . (int)$category_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "category  
                          SET image = '" . $this->db->escape(trim($data['image'])) . "' 
                          WHERE category_id = '" . (int)$category_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "category_description 
                          WHERE category_id = '" . (int)$category_id . "'");

        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        if (isset($data['category_description']) && isset($data['name'])) {
            // add with all languages
            $meta_title = isset($data['meta_title']) ? $data['meta_title'] : '';
            $meta_keyword = isset($data['meta_keyword']) ? $data['meta_keyword'] : '';
            $meta_description = isset($data['meta_description']) ? $data['meta_description'] : '';
            $noindex = isset($data['noindex']) ? $data['noindex'] : 0;
            $nofollow = isset($data['nofollow']) ? $data['nofollow'] : 0;
            $noarchive = isset($data['noarchive']) ? $data['noarchive'] : 0;
            $noimageindex = isset($data['noimageindex']) ? $data['noimageindex'] : 0;
            $nosnippet = isset($data['nosnippet']) ? $data['nosnippet'] : 0;

            foreach ($languages as $language) {
                $language_id = $language['language_id'];
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_description 
                                  SET category_id = '" . (int)$category_id . "', 
                                  language_id = '" . (int)$language_id . "', 
                                  `name` = '" . $this->db->escape(trim($data['name'])) . "', 
                                  `description` = '" . $this->db->escape(trim($data['name'])) . "',
                                  `meta_title` = '" . $this->db->escape(trim($meta_title)) . "', 
                                  `meta_keyword` = '" . $this->db->escape(trim($meta_keyword)) . "', 
                                  `meta_description` = '" . $this->db->escape(trim($meta_description)) . "',
                                  `noindex` = '" . $this->db->escape(trim($noindex)) . "',
                                  `nofollow` = '" . $this->db->escape(trim($nofollow)) . "',
                                  `noarchive` = '" . $this->db->escape(trim($noarchive)) . "',
                                  `noimageindex` = '" . $this->db->escape(trim($noimageindex)) . "',
                                  `nosnippet` = '" . $this->db->escape(trim($nosnippet)) . "'"
                );
            }
        }

        //thêm mới product-choose
        if (isset($data['product-choose'])) {
            //xoá record trogn table product-category với product_id va category_id
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE category_id = '" . (int)$category_id . "'");

            foreach ($data['product-choose'] as $key => $product_id) {

                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
            }
        }

        // TODO: remove or reuse...
        //xoá category trong bảng relation_table
        /*if (isset($data['menu'])) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "novaon_relation_table WHERE child_id = '" . (int)$category_id . "' AND child_name='category'");
            //thêm mới category vả menu trong bảng relation_table 
            foreach ($data['menu'] as $key => $menu) {
                $result = explode('-', $menu);
                $menuName = $result[0];
                $menuId = $result[1];
                $this->db->query("
                    INSERT INTO " . DB_PREFIX . "novaon_relation_table SET main_name = '" . $menuName . "', main_id = '" . $menuId . "', child_name = 'category', child_id = '" . $category_id . "', type_id = 2, redirect = 0
                ");
            }

        }*/
        //category path
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE path_id = '" . (int)$category_id . "' ORDER BY level ASC");

        if ($query->rows) {
            foreach ($query->rows as $category_path) {
                // Delete the path below the current one
                $this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category_path['category_id'] . "' AND level < '" . (int)$category_path['level'] . "'");

                $path = array();

                // Get the nodes new parents
                $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Get whats left of the nodes current path
                $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category_path['category_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Combine the paths with a new level
                $level = 0;

                foreach ($path as $path_id) {
                    $this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category_path['category_id'] . "', `path_id` = '" . (int)$path_id . "', level = '" . (int)$level . "'");

                    $level++;
                }
            }
        } else {
            // Delete the path below the current one
            $this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category_id . "'");

            // Fix for records with no paths
            $level = 0;

            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

            foreach ($query->rows as $result) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");

                $level++;
            }

            $this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', level = '" . (int)$level . "'");
        }

        /* add relation ship with sub_categories */
        // create for new

        // remove first (parent id = 0)
        // a,b,c,d

        // update
        // a,b,c,d => risk: loss all data if could not edit...


        // $sub_category_names, e.g: old: a,b, submit: b,c,d => find:
        // names no change: b
        // names to be added: c,d <= array_diff(submit, old);
        // name to be removed: a <= array_diff(old, submit);

        $sub_category_names = isset($data['sub_categories']) && is_array($data['sub_categories']) ? $data['sub_categories'] : [];
        $existing_sub_categories = $this->getCategoriesByNames($sub_category_names);

        $old_childs = $this->getChildCategory($category_id);
        $olds = [];

        foreach ($old_childs as $child) {
            if (!isset($child['name'])) {
                continue;
            }

            $olds[] = $child['name'];
        }

        $new_sub_categories = array_diff($sub_category_names, $existing_sub_categories);
        $remove_sub_categories = array_diff($olds, $sub_category_names);
        $new_sub_categories = array_unique($new_sub_categories);

        foreach ($new_sub_categories as $new_sub_category) {
            $data_sub_cat = [
                "parent_id" => $category_id,
                "status" => 1,
                "category_description" => $new_sub_category,
                "name" => $new_sub_category,
                "sub_categories" => '[]'
            ];

            $this->addCategory($data_sub_cat);
        }

        // update for existing
        foreach ($existing_sub_categories as $existing_sub_category) {
            if ($existing_sub_category == $data['name']) {
                continue;
            }

            $existing_sub_category = trim($existing_sub_category);
            $filter_name_1 = html_entity_decode($existing_sub_category); // support new value a&b
            $filter_name_2 = htmlentities($filter_name_1); // support old value a&amp;b

            $this->db->query("UPDATE " . DB_PREFIX . "category c 
                          LEFT JOIN " . DB_PREFIX . "category_description cd ON c.category_id = cd.category_id
                          SET c.parent_id = '" . (int)$category_id . "', 
                          c.date_modified = NOW() 
                          WHERE (cd.`name` = '" . $filter_name_1 . "' OR cd.`name` = '" . $filter_name_2 . "') 
                          AND cd.`language_id` = " . (int)$this->config->get('config_language_id'));
        }

        // rm parent_id
        foreach ($remove_sub_categories as $remove_sub_category) {
            if (!array_search($remove_sub_category, $sub_category_names)) {
                $remove_sub_category = trim($remove_sub_category);
                $filter_name_1 = html_entity_decode($remove_sub_category); // support new value a&b
                $filter_name_2 = htmlentities($filter_name_1); // support old value a&amp;b

                $this->db->query("UPDATE " . DB_PREFIX . "category c 
                                  LEFT JOIN " . DB_PREFIX . "category_description cd ON c.category_id = cd.category_id
                                  SET c.parent_id = '0', 
                                  c.date_modified = NOW() 
                                  WHERE (cd.`name` = '" . $filter_name_1 . "' OR cd.`name` = '" . $filter_name_2 . "') 
                                  AND cd.`language_id` = " . (int)$this->config->get('config_language_id'));
                }
        }

        //delete seo product
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'category_id=" . (int)$category_id . "'");
        //seo
        $this->load->model('custom/common');
        $slug = $this->model_custom_common->createSlug($data['name'], '-');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug);
        //create new seo product
        foreach ($languages as $key => $language) {
            $language_id = $language['language_id'];
            $alias = 'category_id=' . $category_id;
            $sql = "INSERT INTO " . DB_PREFIX . "seo_url SET language_id = '" . (int)$language_id . "', query = '" . $alias . "', keyword = '" . $slug_custom . "', table_name = 'category'
            ";
            $this->db->query($sql);
        }

        // send product to rabbit mq
        $old_name_manufacture = !empty($data['old_name']) ? $data['old_name'] : '';
        if ($data['name'] != $old_name_manufacture) {
            try {
                $this->sendToRabbitMq($category_id, $this->db->escape($data['name']), $data['old_name']);
            } catch (Exception $exception) {
                $this->log->write('Send message edit category error: ' . $exception->getMessage());
            }
        }
        $this->cache->delete('category');

        $this->deleteProductCaches();
    }

    public function deleteCategory($category_id)
    {
        // TODO: remove because check category has children not remove
        /*$count = $this->db->query("SELECT COUNT(*) AS count FROM " . DB_PREFIX . "category WHERE parent_id = '" . (int)$category_id . "'")->row;
        if ($count['count'] > 0) {
            return false;
        }*/

        try {
            $this->modifyCategoryRabbitMq($category_id, 'delete');
        } catch (Exception $exception) {
            $this->log->write('Send message deleteCategory error: ' . $exception->getMessage());
        }

        // remove category_path
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_path WHERE category_id = '" . (int)$category_id . "'");

        // remove all sub-categories
        $categories = $this->db->query("SELECT category_id, parent_id FROM " . DB_PREFIX . "category WHERE parent_id = '" . (int)$category_id . "'");
        foreach ($categories->rows as $result) {
            // avoid infinitive loop when parent id = sub id
            if ($result['category_id'] == $category_id) {
                continue;
            }

            $this->deleteCategory($result['category_id']);
        }

        //delete in relationtable
        $this->db->query("DELETE FROM " . DB_PREFIX . "novaon_relation_table WHERE child_id = '" . (int)$category_id . "' AND child_name = 'category'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'category_id=" . (int)$category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "coupon_category WHERE category_id = '" . (int)$category_id . "'");

        $this->cache->delete('category');

        $this->deleteProductCaches();

        return true;
    }

    public function modifyCategoryRabbitMq($category_id, $action = 'delete')
    {
        try {
            if (empty($category_id)) {
                return;
            }

            $category = $this->getCategoryById($category_id);
            if (!empty($category['name'])) {
                $message = [
                    'shop_name' => $this->config->get('shop_name'),
                    'category' => [
                        'name' => $category['name'],
                        'action' => $action,
                    ]
                ];

                $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'modify_category', '', 'modify_category', json_encode($message));
            }
        } catch (Exception $exception) {
            $this->log->write('Send message modifyCategoryRabbitMq error: ' . $exception->getMessage());
        }
    }

    public function repairCategories($parent_id = 0)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category WHERE parent_id = '" . (int)$parent_id . "'");

        foreach ($query->rows as $category) {
            // Delete the path below the current one
            $this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category['category_id'] . "'");

            // Fix for records with no paths
            $level = 0;

            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$parent_id . "' ORDER BY level ASC");

            foreach ($query->rows as $result) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category['category_id'] . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");

                $level++;
            }

            $this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category['category_id'] . "', `path_id` = '" . (int)$category['category_id'] . "', level = '" . (int)$level . "'");

            $this->repairCategories($category['category_id']);
        }

        $this->deleteProductCaches();
    }

    public function getCategory($category_id)
    {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT GROUP_CONCAT(cd1.name ORDER BY level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id AND cp.category_id != cp.path_id) WHERE cp.category_id = c.category_id AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY cp.category_id) AS path FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (c.category_id = cd2.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getCategoryById($category_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");

        return $query->row;
    }

    /**
     * @param int|array $parent_id one or array of category id
     * @param int $deep
     * @return array
     */
    public function getCategoriesRecursive($parent_id = 0, $deep = 3)
    {
        if (!is_array($parent_id)) {
            $parent_id = [(int)$parent_id];
        }

        if (count($parent_id) < 1) {
            return [];
        }

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c 
		                           LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) 
		                           LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) 
		                           WHERE c.parent_id IN (" . implode(',', $parent_id) . ") 
		                           AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
		                           AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  
		                           AND c.status = '1' 
		                           ORDER BY c.sort_order, LCASE(cd.name)");

        $result = $query->rows;
        if ($deep > 1) {
            $more_parent_ids = array_map(function ($item) {
                return $item['category_id'];

            }, $result);

            $result = array_merge($result, $this->getCategoriesRecursive($more_parent_ids, $deep - 1));
        }

        return $result;
    }

    /**
     * @param int|array $parent_id one or array of category id
     * @param int $deep
     * @return array
     */
    public function getCategoryIdsRecursive($parent_id = 0, $deep = 3)
    {
        $result = $this->getCategoriesRecursive($parent_id, $deep);

        $result = array_map(function ($item) {
            return $item['category_id'];

        }, $result);

        return $result;
    }

    /**
     * @param int $parent_id
     * @param int $deep
     * @return array categories array with additional key "sub_categories"
     */
    public function getCategoriesTree($parent_id = 0, $deep = 3)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c 
		                           LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) 
		                           LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) 
		                           WHERE c.parent_id = '" . (int)$parent_id . "' 
		                           AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
		                           AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  
		                           AND c.status = '1' 
		                           ORDER BY c.sort_order, LCASE(cd.name)");

        $result = $query->rows;
        if ($deep > 1) {
            foreach ($result as &$item) {
                $item['sub_categories'] = $this->getCategoriesTree($item['category_id'], $deep - 1);
            }
            unset($item);
        }

        return $result;
    }

    public function getAllCategories() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c 
		                           LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) 
		                           LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) 
		                           WHERE 1=1 
		                           AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
		                           AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  
		                           AND c.status = '1' 
		                           ORDER BY c.sort_order, LCASE(cd.name)");

        return $query->rows;
    }

    public function getCategoriesForClassify($filter_data)
    {
        $result = $this->getFullCategoriesNotFilterClassify($filter_data);

        $result_filter =  $this->getCategoryContainFilterNameClassify($result);

        if (isset($filter_data['page'])) {
            $start = ((int)$filter_data['page'] - 1) * 5;
            $result_filter = array_slice($result_filter, $start, 5);
        }

        return $result_filter;
    }

    private function getCategoryContainFilterNameClassify($tree)
    {
        $result_filter = [];
        foreach ($tree as $k => $item) {
            if ($item['filter_name'] == 'YES'){
                $result_filter[] = $item;
            }else{
                $result_filter = array_merge($result_filter, $this->getCategoryContainFilterNameClassify($item['children']));
            }
        }

        return $result_filter;
    }

    private function getFullCategoriesNotFilterClassify($filter_data, $parentId = 0, $level = 0){
        $result = [];

        if (!isset($filter_data['filter_name'])) {
            $filter_data['filter_name'] = '';
        } else {
            $filter_data['filter_name'] = trim($filter_data['filter_name'], ' ');
        }

        $filter_name_1 = html_entity_decode($filter_data['filter_name']); // support new value a&b
        $filter_name_2 = htmlentities($filter_name_1); // support old value a&amp;b

        $sql = "SELECT *, IF(cd.`name` LIKE '%" . $this->db->escape($filter_name_1) . "%' OR cd.`name` LIKE '%" . $this->db->escape($filter_name_2) . "%', 'YES', 'NO') AS `filter_name` 
                      FROM `" . DB_PREFIX . "category` c 
                      LEFT JOIN `" . DB_PREFIX . "category_description` cd ON (c.category_id = cd.category_id) 
                      WHERE c.`parent_id` = " . (int)$parentId . " 
                      AND cd.`language_id` = " . (int)$this->config->get('config_language_id');

        $sql .= " ORDER BY c.`sort_order` = 0, c.`sort_order` ASC, cd.`name` ASC";

        $query = $this->db->query($sql);

        foreach ($query->rows as $row) {
            $children = $this->getFullCategoriesNotFilterClassify($filter_data, $row['category_id'], $level + 1);
            $sub_ids = [];
            foreach ($children as $child) {
                $sub_ids[] = $child['id'];
                if (isset($child['sub_ids']) && is_array($child['sub_ids'])){
                    $sub_ids = array_merge($sub_ids, $child['sub_ids']);
                }
            }

            $result[] = [
                'id' => $row['category_id'],
                'text' => html_entity_decode($row['name']), // return a&b instead of a&amp;b
                'image' => $row['image'],
                'hasChildren' => count($children) > 0 ? true : false,
                'sub_ids' => $sub_ids,
                'children' => $children,
                'filter_name' => $row['filter_name'],
                'level' => $level,
                'parent_id' => $parentId,
                'sort_order' => $row['sort_order'],
                'source' => $row['source'],
                'status' => $row['status'],
                'meta_title' => html_entity_decode($row['meta_title']),
                'meta_keyword' => html_entity_decode($row['meta_keyword']),
                'meta_description' => html_entity_decode($row['meta_description']),
                'description' => $row['description'],
                'noindex' => $row['noindex'],
                'nofollow' => $row['nofollow'],
                'noarchive' => $row['noarchive'],
                'noimageindex' => $row['noimageindex'],
                'nosnippet' => $row['nosnippet'],
            ];
        }

        return $result;
    }

    public function getTotalProductByCategoryId($category_id)
    {
        $query = $this->db->query("SELECT COUNT(*) as count_product FROM `" . DB_PREFIX . "product_to_category` ptc
                                            INNER JOIN `" . DB_PREFIX . "product` p ON (p.`product_id` = ptc.`product_id`)
                                            WHERE ptc.`category_id` = ".(int)$category_id."   
                                            AND p.`deleted` is NULL");


        if (isset($query->row['count_product'])) {
            return (int)$query->row['count_product'];
        }

        return 0;
    }

    public function getProductsByCategoryId($category_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT p.product_id, dp.name,p.image, IF(p.price = 0 , p.compare_price, p.price) as price, p.date_added 
            FROM {$DB_PREFIX}product p LEFT JOIN {$DB_PREFIX}product_description as dp ON (p.product_id = dp.product_id)
            INNER JOIN `{$DB_PREFIX}product_to_category` ptc ON (p.`product_id` = ptc.`product_id`)
            WHERE ptc.`category_id`=".(int)$category_id."
            AND p.`deleted` is NULL 
            AND dp.language_id = ".(int)$this->config->get('config_language_id');

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function countCategoriesParent($filter_data)
    {
        $result = $this->getFullCategoriesNotFilterClassify($filter_data);

        $result_filter =  count($this->getCategoryContainFilterNameClassify($result));

        return $result_filter;
    }

    public function getCategories($data = array())
    {
        $sql = "SELECT cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND cd2.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " GROUP BY cp.category_id";

        $sort_data = array(
            'name',
            'sort_order',
            'c1.date_modified'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getCategoriesCheck()
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "category_description cd 
                         WHERE cd.`language_id` = " . (int)$this->config->get('config_language_id');

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getCategoriesPaginator($data = array())
    {
        $sql = "SELECT cp.category_id AS id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS text FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND cd2.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " GROUP BY cp.category_id";

        $sql .= " ORDER BY cp.path_id ASC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getCategoriesPaginatorClassify($data = array())
    {
        // get this level
        $this_level = 0;
        $categories_deep = $this->getDeepForParentCategories();

        if (isset($data['current_id'])) {
            $this_level = $this->getLevelOfCurrentCategory($data['current_id']);
        }

        $list_category_invalid_level_id = [];
        if ($this_level == 0) {
            $list_category_invalid_level_id = array_filter($categories_deep, function ($item) {
                return $item['deep'] == 0 || $item['deep'] == 1;
            });
        } else if ($this_level == 1) {
            $list_category_invalid_level_id = array_filter($categories_deep, function ($item) {
                return $item['deep'] == 0;
            });
        } else { // level 2
            return [];
        }

        $list_category_invalid_level_id = array_map(function ($item){
            return $item['id'];
        }, $list_category_invalid_level_id);

        if (isset($data['current_id'])) {
            $first_parent_id = $this->getFirstParentId($data['current_id']);
            foreach ($list_category_invalid_level_id as $key => $item_id){
                if ($item_id == $first_parent_id){
                    unset($list_category_invalid_level_id[$key]);
                }
            }
        }

        $sql = "SELECT * FROM " . DB_PREFIX . "category c
                       LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) 
                       WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ";

        if (!empty($data['filter_name'])) {
            $sql .= " AND cd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['categories_selected'])) {

            $category_name = "";
            foreach ($data['categories_selected'] as $key => $sub_name) {
                if ($key > 0) {
                    $category_name .= ",";
                }
                $category_name .= "'" . $this->db->escape($sub_name) . "'";
            }

            $sql .= " AND cd.name NOT IN (" . $category_name . ")";
        }

        if (!empty($list_category_invalid_level_id)) {
            $sql .= " AND c.`category_id` IN (" . implode(',', $list_category_invalid_level_id) . ")";
        }

        $sql .= " GROUP BY cd.category_id";
        $sql .= " ORDER BY c.date_modified DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    private function getFirstParentId($category_id)
    {
        $first_parent_id = $category_id;
        $sql = "SELECT `parent_id` FROM `" . DB_PREFIX . "category` c 
                        WHERE c.`category_id` = " . (int)$category_id;

        $query = $this->db->query($sql);
        if (isset($query->row['parent_id'])){
            if ($query->row['parent_id'] != 0) {
                $first_parent_id = $this->getFirstParentId($query->row['parent_id']);
            }
        }

        return $first_parent_id;
    }

    private function getLevelOfCurrentCategory($category_id, $level = 0)
    {
        $sql = "SELECT `parent_id`,`category_id` FROM `" . DB_PREFIX . "category` c 
                        WHERE c.`category_id` = " . (int)$category_id;

        $query = $this->db->query($sql);
        if (isset($query->row['parent_id']) && $query->row['parent_id'] != 0) {
            $level = $this->getLevelOfCurrentCategory($query->row['parent_id'], $level + 1);
        }

        return $level;

    }

    public function getDeepForParentCategories($parentId = 0, $deep = 0 )
    {
        $result = [];
        $sql = "SELECT * FROM `" . DB_PREFIX . "category` c 
                LEFT JOIN `" . DB_PREFIX . "category_description` cd 
                       ON (c.category_id = cd.category_id) 
                WHERE c.`parent_id` = " . (int)$parentId . " 
                  AND cd.`language_id` = " . (int)$this->config->get('config_language_id');

        $query = $this->db->query($sql);

        foreach ($query->rows as $row) {
            $children = $this->getDeepForParentCategories($row['category_id'], $deep + 1);
            $current_deep = $deep;
            foreach ($children as $child) {
                if ($child['deep'] > $current_deep){
                    $current_deep = $child['deep'];
                }
            }
            $result[] = [
                'id' => $row['category_id'],
                'deep' => $current_deep
            ];
        }

        // return all categories with parentId = 0; deep = 0 => không có con, deep = 1 => có con nhưng ko có cháu, deep = 2 => có con và cháu

        return $result;
    }

    public function getCategoryDescriptions($category_id)
    {
        $category_description_data = array();
        $language_id = $this->config->get('config_language_id');
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int)$category_id . "'");

        foreach ($query->rows as $result) {
            $category_description_data[$result['language_id']] = array(
                'name' => html_entity_decode($result['name']),
                'meta_title' => $result['meta_title'],
                'meta_description' => $result['meta_description'],
                'meta_keyword' => $result['meta_keyword'],
                'description' => $result['description']
            );
        }

        return $category_description_data[$language_id];
    }

    public function getCategoryPath($category_id)
    {
        $query = $this->db->query("SELECT category_id, path_id, level FROM " . DB_PREFIX . "category_path WHERE category_id = '" . (int)$category_id . "'");

        return $query->rows;
    }

    public function getCategoryFilters($category_id)
    {
        $category_filter_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");

        foreach ($query->rows as $result) {
            $category_filter_data[] = $result['filter_id'];
        }

        return $category_filter_data;
    }

    public function getCategoryStores($category_id)
    {
        $category_store_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int)$category_id . "'");

        foreach ($query->rows as $result) {
            $category_store_data[] = $result['store_id'];
        }

        return $category_store_data;
    }

    public function getCategorySeoUrls($category_id)
    {
        $category_seo_url_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query = 'category_id=" . (int)$category_id . "'");

        foreach ($query->rows as $result) {
            $category_seo_url_data[$result['store_id']][$result['language_id']] = $result['keyword'];
        }

        return $category_seo_url_data;
    }

    public function getCategoryLayouts($category_id)
    {
        $category_layout_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "'");

        foreach ($query->rows as $result) {
            $category_layout_data[$result['store_id']] = $result['layout_id'];
        }

        return $category_layout_data;
    }

    public function getCategoriesByIds(array $category_ids)
    {
        if (empty($category_ids)) {
            return [];
        }

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c 
                                   LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) 
                                   WHERE c.category_id IN (" . implode(',', $category_ids) . ") 
                                   AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->rows;
    }

    public function getTotalCategories()
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category");

        return $query->row['total'];
    }

    public function getTotalCategoriesByLayoutId($layout_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

        return $query->row['total'];
    }

    public function getFullCaseCategories($filter_data, $categoryId = 0)
    {
        $sql = "SELECT c.category_id AS category_id, cd.name AS name FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c.parent_id = '" . $categoryId . "'";
        if ($filter_data['filter_status'] != '') {
            $sql = $sql . " AND c.status = '" . $filter_data['filter_status'] . "'";
        }
        $sql .= " GROUP BY c.category_id ORDER BY c.category_id ASC";
        $listCategories = $this->db->query($sql);
        $categories = $listCategories->rows;
        foreach ($categories as &$category) {
            $category['categories'] = $this->getFullCaseCategories($filter_data, $category['category_id']);
        }

        return $categories;
    }

    public function removeCategoryAmiss($fullCaseCategories, $list_cid)
    {
        foreach ($fullCaseCategories as $k => $category) {
            if (!in_array($category['category_id'], $list_cid) && empty($category['categories'])) {
                unset($fullCaseCategories[$k]);
                continue;
            }
            if (!empty($category['categories'])) {
                $categories = $this->removeCategoryAmiss($category['categories'], $list_cid);
                if (empty($categories) && !in_array($category['category_id'], $list_cid)) {
                    unset($fullCaseCategories[$k]);
                } else {
                    $fullCaseCategories[$k]['categories'] = $categories;
                }
            }

        }

        $this->deleteProductCaches();

        return $fullCaseCategories;
    }

    public function renderCategories($userToken, $url, $filter_data)
    {
        $sql = "SELECT c.category_id AS category_id, cd.name AS name FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
        if ($filter_data['filter_status'] != '') {
            $sql = $sql . " AND c.status = '" . $filter_data['filter_status'] . "'";
        }
        if ($filter_data['filter_name'] != '') {
            $sql = $sql . " AND cd.name LIKE '%" . $this->db->escape($filter_data['filter_name']) . "%'";
        }
        $sql .= " GROUP BY c.category_id ORDER BY c.category_id ASC";
        $listCategories = $this->db->query($sql);

        $categories = $listCategories->rows;
        $list_cid = array_map(function ($category) {
            return $category['category_id'];
        }, $categories);
        $fullCaseCategories = $this->getFullCaseCategories($filter_data);
        $lastCategoriews = $this->removeCategoryAmiss($fullCaseCategories, $list_cid);
        if (isset($filter_data['start']) || isset($filter_data['limit'])) {
            if ($filter_data['start'] < 0) {
                $filter_data['start'] = 0;
            }

            if ($filter_data['limit'] < 1) {
                $filter_data['limit'] = 20;
            }
            $lastCategoriews = array_slice($lastCategoriews, $filter_data['start'], $filter_data['limit']);
        }
        $result = $this->render($lastCategoriews, $userToken, $url);

        return array('html' => $result, 'total' => count($lastCategoriews));
    }

    private function render($categories, $userToken, $url, $categoryId = 0)
    {
        $html = '';
        foreach ($categories as $key => $value) {
            $edit = $this->url->link('catalog/category/edit', 'user_token=' . $userToken . '&category_id=' . $value['category_id'] . $url, true);
            $delete = $this->url->link('catalog/category/destroy', 'user_token=' . $userToken . '&category_id=' . $value['category_id'] . $url, true);
            $prefix = $suffix = '';
            if ($categoryId == 0) {
                $prefix = '<tr>
                        <td scope="select-this" data-id="' . $value['category_id'] . '">
                          <label class="checkbox">
                            <input type="checkbox" name="select_this" value="' . $value['category_id'] . '">
                            <span></span>
                          </label>
                        </td>
                    <td colspan="2">
                ';
                $suffix = '</td></tr>';
            }

            $html .= $prefix . (($categoryId != 0) ? '<div class="list-group d-block border-left font-14">
                <div class="list-group-item border-0 border-left-3 list-group-item-action pr-0">' : '') . '
            <a class="collapse-child-cat font-20" data-toggle="collapse" href="#collapseCatTree-' . $value['category_id'] . '" role="button" aria-expanded="false" aria-controls="collapseCatTree-' . $value['category_id'] . '">››</a>' . $value['name'] . '
            <div class="float-right">
                <div class="dropdown option-link">
                    <button class="btn btn-light dropdown-toggle" type="button" id="dropdownCategoryOption-' . $value['category_id'] . '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownCategoryOption-' . $value['category_id'] . '">
                        <a class="dropdown-item" href="' . $edit . '">Sửa</a>
                        <a class="dropdown-item category_destroy_own" href="' . $delete . '" category-data-id="' . $value['category_id'] . '">Xoá</a>
                    </div>
                </div>
            </div>
            <div class="collapse child-category mt-3" id="collapseCatTree-' . $value['category_id'] . '">'
                . $this->render($value['categories'], $userToken, $url, 1)
                . '</div>'
                . (($categoryId != 0) ? '</div></div>' : '') . $suffix;

        }
        return $html;
    }

    public function getTotalCategoriesCustom($filter_data, $categoryId = 0)
    {
        $sql = "SELECT COUNT(DISTINCT c.category_id) AS total FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c.parent_id = '" . $categoryId . "'";
        if (isset($filter_data['filter_status']) && $filter_data['filter_status'] != '') {
            $sql = $sql . " AND c.status = '" . $filter_data['filter_status'] . "'";
        }
        if (isset($filter_data['filter_name']) && $filter_data['filter_name'] != '') {
            $sql = $sql . " AND cd.name LIKE '%" . $this->db->escape($filter_data['filter_name']) . "%'";
        }
        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    public function getTotalCategoriesNoCategoriesCustom($filter_data)
    {
        // get this level
        $this_level = 0;
        $categories_deep = $this->getDeepForParentCategories();

        if (isset($data['current_id'])) {
            $this_level = $this->getLevelOfCurrentCategory($filter_data['current_id']);
        }

        $list_category_invalid_level_id = [];
        if ($this_level == 0) {
            $list_category_invalid_level_id = array_filter($categories_deep, function ($item) {
                return $item['deep'] == 0 || $item['deep'] == 1;
            });
        } else if ($this_level == 1) {
            $list_category_invalid_level_id = array_filter($categories_deep, function ($item) {
                return $item['deep'] == 0;
            });
        } else { // level 2
            return 0;
        }

        $list_category_invalid_level_id = array_map(function ($item){
            return $item['id'];
        }, $list_category_invalid_level_id);

        if (isset($filter_data['current_id'])) {
            $first_parent_id = $this->getFirstParentId($filter_data['current_id']);
            foreach ($list_category_invalid_level_id as $key => $item_id){
                if ($item_id == $first_parent_id){
                    unset($list_category_invalid_level_id[$key]);
                }
            }
        }

        $sql = "SELECT COUNT(DISTINCT c.category_id) AS total FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
        if (isset($filter_data['filter_status']) && $filter_data['filter_status'] != '') {
            $sql = $sql . " AND c.status = '" . $filter_data['filter_status'] . "'";
        }
        if (isset($filter_data['filter_name']) && $filter_data['filter_name'] != '') {
            $sql = $sql . " AND cd.name LIKE '%" . $this->db->escape($filter_data['filter_name']) . "%'";
        }

        if (!empty($list_category_invalid_level_id)) {
            $sql .= " AND c.`category_id` IN (" . implode(',', $list_category_invalid_level_id) . ")";
        }

        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    public function getNameRecordById($categoryId, $tableName, $name = null)
    {
        // var_dump($tableName);die;
        $language_id = $this->config->get('config_language_id');
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . $tableName . "_description WHERE category_id = '" . (int)$categoryId . "' AND language_id = '" . $language_id . "'");
        if ($name) {
            if ($query->row[$name]) {
                return $query->row[$name];
            }
            return null;
        }
        if ($query->row['name']) {
            return $query->row['name'];
        }
        return null;
    }

    public function addCategoryFast($name)
    {
        $name = htmlspecialchars_decode($name);
        $data = [
            "parent_id" => 0,
            "status" => 1,
            "category_description" => trim($name),
            "name" => trim($name),
        ];
        $this->db->query("INSERT INTO " . DB_PREFIX . "category SET parent_id = '" . (int)$data['parent_id'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW(), date_added = NOW()");

        $category_id = $this->db->getLastId();
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        if (isset($data['category_description']) && isset($data['name'])) {
            // add with all languages
            foreach ($languages as $language) {
                $language_id = $language['language_id'];
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['category_description']) . "'");
            }
        }
        //tung
        //thêm mới product-choose
        if (isset($data['product-choose'])) {
            foreach ($data['product-choose'] as $key => $product_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
            }
        }
        //them mới menu
        if (isset($data['menu'])) {
            foreach ($data['menu'] as $menu) {
                $result = explode('-', $menu);
                $menuName = $result[0];
                $menuId = $result[1];
                $this->db->query("
                    INSERT INTO " . DB_PREFIX . "novaon_relation_table SET main_name = '" . $menuName . "', main_id = '" . $menuId . "', child_name = 'category', child_id = '" . $category_id . "', type_id = 2, redirect = 0
                ");
            }
        }
        //save into path and store
        $level = 0;

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY `level` ASC");

        foreach ($query->rows as $result) {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', `level` = '" . (int)$level . "'");

            $level++;
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', `level` = '" . (int)$level . "'");

        if (isset($data['category_filter'])) {
            foreach ($data['category_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int)$category_id . "', filter_id = '" . (int)$filter_id . "'");
            }
        }

        // if (isset($data['category_store'])) {
        //     foreach ($data['category_store'] as $store_id) {
        $store_id = 0;
        $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
        //     }
        // }

        // create seo
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        $category_alias = $data['name'];

        $this->load->model('custom/common');
        $slug = $this->model_custom_common->createSlug($category_alias, '-');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug);
        foreach ($languages as $key => $language) {
            $language_id = $language['language_id'];
            $alias = 'category_id=' . $category_id;
            $sql = "INSERT INTO " . DB_PREFIX . "seo_url SET language_id = '" . (int)$language_id . "', query = '" . $alias . "', keyword = '" . $slug_custom . "', table_name = 'category'";
            $this->db->query($sql);
        }

        $this->cache->delete('category');

        $this->deleteProductCaches();

        // send product to rabbit mq
        try {
            $this->sendToRabbitMq($category_id, $this->db->escape(trim($name)));
        } catch (Exception $exception) {
            $this->log->write('Send message add category error: ' . $exception->getMessage());
        }

        return $category_id;
    }

    public function getProductInCategory($category_id)
    {
        $sql = "SELECT COUNT(product_id) as total FROM ". DB_PREFIX ."product_to_category WHERE category_id = '". (int)$category_id ."'";
        $query = $this->db->query($sql);
        return $query->row;
    }

    public function getCategoryByName($filter) {
        $sql = "SELECT * FROM ". DB_PREFIX . "category_description WHERE 1=1 AND language_id = '". (int)$this->config->get('config_language_id') ."'";
        if (isset($filter['name'])) {
            $name_search = mb_strtoupper($filter['name']);
            $sql .= " AND hex(UPPER(name)) = hex('". $this->db->escape($name_search) ."')";
        }
        if (isset($filter['id']) && $filter['id'] != '') {
            $sql .= " AND category_id != '". (int)$filter['id'] ."'";
        }
        $query = $this->db->query($sql);
        return $query->row;
    }

    public function editCategoryName($data)
    {
        $sql = "UPDATE ". DB_PREFIX ."category_description SET name = '". $this->db->escape($data['name']) ."'";
        $sql .= " WHERE category_id = '". (int)$data['id'] ."'";
        $this->db->query($sql);

        $this->deleteProductCaches();
    }

    public function getCategoriesIdByProductId($product_id)
    {
        if (!$product_id) {
            return [];
        }

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category as ptc LEFT JOIN " . DB_PREFIX . "category_description as cd ON cd.category_id = ptc.category_id WHERE product_id = '" . (int)$product_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        $list_categories = [];
        foreach ($query->rows as $row) {
            $parent_categories = $this->getAllParentsCategory($row['category_id']);
            foreach ($parent_categories as $k => $category){
                $list_categories[$k] = $category;
            }
        }

        $result = [];
        foreach ($list_categories as $category){
            $result[] = $category['category_id'];
        }

        return $result;
    }

    private function getAllParentsCategory($category_id)
    {
        $result = [];
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category` as c LEFT JOIN `" . DB_PREFIX . "category_description` as cd ON c.`category_id` = cd.`category_id` WHERE c.`category_id` = " . (int)$category_id);

        if ($query->num_rows <= 0){
            return $result;
        }
        $result[$query->row['category_id']] = $query->row;
        if ($query->row['parent_id'] != 0){
            $parents = $this->getAllParentsCategory($query->row['parent_id']);
            foreach ($parents as $k => $v){
                $result[$k] = $v;
            }
        }

        return $result;
    }

    public function deleteProductsCategory($category_id, $product_id = null) {
        $sql = "DELETE FROM `" . DB_PREFIX . "product_to_category` WHERE `category_id` = '" . (int)$category_id . "'";
        if(!is_null($product_id)) {
            $sql .= " AND `product_id` = '" . (int)$product_id . "'";
        }

        $this->db->query($sql);

        $this->cache->delete('category');

        $this->deleteProductCaches();
    }

    public function addProductCategory($category_id, $product_id) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");

        $this->cache->delete('category');

        $this->deleteProductCaches();
    }

    public function sendToRabbitMq($category_id, $category_name, $category_old_name = "")
    {
        try {
            if (!empty($category_id)) {
                $message = [
                    'shop_name' => $this->config->get('shop_name'),
                    'category' => [
                        'name' => $category_name,
                        'old_name' => $category_old_name
                    ]
                ];

                $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'category', '', 'category', json_encode($message));
            }
        } catch (Exception $exception) {
            $this->log->write('Send message category error: ' . $exception->getMessage());
        }
    }

    /**
     * delete product caches
     */
    private function deleteProductCaches() {
        $product_key_redis =  DB_PREFIX . 'product';
        $this->cache->delete($product_key_redis);
    }
}
