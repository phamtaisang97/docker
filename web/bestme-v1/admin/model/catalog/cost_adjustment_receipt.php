<?php
/**
 * Created by PhpStorm.
 * User: Microsoft Windows
 * Date: 19/06/2020
 * Time: 9:30 AM
 */

Class ModelCatalogCostAdjustmentReceipt extends Model
{
    const RECEIPT_DRAPT = 0;  // Nháp
    const RECEIPT_APPLY = 1;  // Cân bằng kho
    const RECEIPT_CANCEL = 9; // Hủy

    public function getTotalCostAdjustmentReceipt($filter_data){
        $sql = "SELECT COUNT(DISTINCT str.cost_adjustment_receipt_id) AS total from  " . DB_PREFIX . "cost_adjustment_receipt str ";

        $sql .= "WHERE str.status <> 99";

        if (isset($filter_data['filter_status']) && $filter_data['filter_status'] !== '') {
            $sql .= ' AND str.status IN (' . $filter_data['filter_status'] . ')';
        }

        if (isset($filter_data['filter_name']) && $filter_data['filter_name'] !== '') {
            $sql .= " AND str.`cost_adjustment_receipt_code` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%' ";
        }

        if (isset($filter_data['filter_store']) && $filter_data['filter_store'] !== '') {
            $sql .= ' AND str.store_id IN (' . $filter_data['filter_store'] . ')';
        }

        if (isset($filter_data['start_date']) && $filter_data['start_date'] !== '') {
            $sql .= " AND str.date_modified BETWEEN '" . $filter_data['start_date'] . "' AND '". $filter_data['end_date']."'";
        }

        if (isset($filter_data['current_user_id']) && $filter_data['current_user_id'] !== '') {
            $sql .= " AND str.user_create_id = " . $filter_data['current_user_id'];
        }

        if (isset($filter_data['user_store_id']) && $filter_data['user_store_id'] !== '') {
            $sql .= " AND str.store_id IN (". $filter_data['user_store_id'] .")";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getCostAdjustmentReceiptCustom($filter_data)
    {
        if ($filter_data['filter_data'] == '') {
            $data = $this->getCostAdjustmentReceipt($filter_data);
            return $data;
        }

        $sql = "SELECT str.* FROM " . DB_PREFIX . "cost_adjustment_receipt str ";

        $sql .= " WHERE str.status <> 99";

        if (isset($filter_data['filter_status']) && $filter_data['filter_status'] !== '') {
            $sql .= ' AND str.status IN (' . $filter_data['filter_status'] . ')';
        }

        if (isset($filter_data['filter_name']) && $filter_data['filter_name'] !== '') {
            $sql .= " AND str.`cost_adjustment_receipt_code` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%' ";
        }

        if (isset($filter_data['filter_store']) && $filter_data['filter_store'] !== '') {
            $sql .= ' AND str.store_id IN (' . $filter_data['filter_store'] . ')';
        }

        if (isset($filter_data['start_date']) && $filter_data['start_date'] !== '') {
            $sql .= " AND str.date_modified BETWEEN '" . $filter_data['start_date'] . "' AND '". $filter_data['end_date']."'";
        }

        if (isset($filter_data['current_user_id']) && $filter_data['current_user_id'] !== '') {
            $sql .= " AND str.user_create_id = " . $filter_data['current_user_id'];
        }

        if (isset($filter_data['user_store_id']) && $filter_data['user_store_id'] !== '') {
            $sql .= " AND str.store_id IN (". $filter_data['user_store_id'] .")";
        }

        $sql .= " ORDER BY date_modified DESC";

        if (isset($filter_data['start']) || isset($filter_data['limit'])) {
            if ($filter_data['start'] < 0) {
                $filter_data['start'] = 0;
            }

            if ($filter_data['limit'] < 1) {
                $filter_data['limit'] = 20;
            }
            $sql .= " LIMIT " . (int)$filter_data['start'] . "," . (int)$filter_data['limit'];
        }
        //var_dump($sql);die();
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getCostAdjustmentReceipt($data = array()){
        $sql = "SELECT str.* FROM " . DB_PREFIX . "cost_adjustment_receipt str ";

        $sql .= " WHERE str.status <> 99";
        if (!empty($data['filter_name'])) {
            $sql .= " AND str.code LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (isset($data['start_date']) && $data['start_date'] !== '') {
            $sql .= " AND str.date_modified BETWEEN '" . $data['start_date'] . "' AND '". $data['end_date']."'";
        }

        if (isset($data['current_user_id']) && $data['current_user_id'] !== '') {
            $sql .= " AND str.user_create_id = " . $data['current_user_id'];
        }

        if (isset($data['user_store_id']) && $data['user_store_id'] !== '') {
            $sql .= " AND str.store_id IN (". $data['user_store_id'] .")";
        }

        $sql .= " ORDER BY date_modified DESC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function addReceipt($data)
    {
        $store_id = isset($data['store_id']) ? (int)$data['store_id'] : '';
        $user_create_id = isset($data['user_create_id']) ? (int)$data['user_create_id'] : '';

        if ($store_id === ''){
            return;
        }
        $code = $this->autoGenerationCode();
        $status = isset($data['type_submit']) && $data['type_submit'] == 'apply' ? self::RECEIPT_APPLY : self::RECEIPT_DRAPT;

        $this->db->query("INSERT INTO `" . DB_PREFIX . "cost_adjustment_receipt` SET
                        `store_id` = " . $store_id . ",  
                        `cost_adjustment_receipt_code` = '" . $this->db->escape($code) . "', 
                        `status` = " . (int)$status .", 
                        `user_create_id` = " . (int)$user_create_id .", 
                        `note` = '" . $this->db->escape($data['note']) ."', 
                        `date_added` = NOW(), `date_modified` = NOW();
                        ");

        $receipt_id = $this->db->getLastId();

        if (isset($data['product_version_id'])) {
            $product_version_ids = is_array($data['product_version_id']) ? $data['product_version_id'] : [];
            foreach ($product_version_ids as $key => $pvid) {
                $arr_pvid = explode('-', $pvid);
                $product_id = isset($arr_pvid[0]) ? $arr_pvid[0] : '';
                $version_id = isset($arr_pvid[1]) ? $arr_pvid[1] : '';
                if (!$product_id){
                    continue;
                }
                $current_cost_price = isset($data['product_pre_cost_price'][$key]) ? $data['product_pre_cost_price'][$key] : 0;
                $adjustment_cost_price = isset($data['product_cost_price'][$key]) ? $data['product_cost_price'][$key] : 0;
                $reason  = isset($data['reason'][$key]) ? $data['reason'][$key] : 0;
                $this->db->query("INSERT INTO `" . DB_PREFIX . "cost_adjustment_receipt_to_product` SET
                                                                `cost_adjustment_receipt_id` = " . (int)$receipt_id . ", 
                                                                `product_id` = " . (int)$product_id . ", 
                                                                `product_version_id` = '" . (int)$version_id . "', 
                                                                `current_cost_price` = " . (float)extract_number($current_cost_price) . ", 
                                                                `adjustment_cost_price` = " . (float)extract_number($adjustment_cost_price) . ", 
                                                                `note` = '" . $this->db->escape($reason) . "' 
                        ");

                // update cost_price
                if ($status == self::RECEIPT_APPLY){
                    $this->updateCostPriceProduct($store_id, $product_id, $version_id, $adjustment_cost_price);
                }
            }
        }

        return $receipt_id;
    }

    public function editReceipt($receipt_id, $data)
    {
        $store_id = isset($data['store_id']) ? (int)$data['store_id'] : '';
        if ($store_id === ''){
            return;
        }
        $status = self::RECEIPT_DRAPT;
        if (isset($data['type_submit'])) {
            if ($data['type_submit'] == 'apply') {
                $status = self::RECEIPT_APPLY;
            } else if ($data['type_submit'] == 'cancel') {
                $status = self::RECEIPT_CANCEL;
            }
        }

        $this->db->query("UPDATE `" . DB_PREFIX . "cost_adjustment_receipt` SET
                        `store_id` = " . $store_id . ",  
                        `status` = " . (int)$status .", 
                        `note` = '" . $this->db->escape($data['note']) . "', 
                        `date_modified` = NOW() WHERE `cost_adjustment_receipt_id` = " . (int)$receipt_id);


        $this->db->query("DELETE FROM " . DB_PREFIX . "cost_adjustment_receipt_to_product WHERE `cost_adjustment_receipt_id` = '" . (int)$receipt_id . "'");
        if (isset($data['product_version_id'])) {
            $product_version_ids = is_array($data['product_version_id']) ? $data['product_version_id'] : [];
            foreach ($product_version_ids as $key => $pvid) {
                $arr_pvid = explode('-', $pvid);
                $product_id = isset($arr_pvid[0]) ? $arr_pvid[0] : '';
                $version_id = isset($arr_pvid[1]) ? $arr_pvid[1] : '';
                if (!$product_id){
                    continue;
                }
                $current_cost_price = isset($data['product_pre_cost_price'][$key]) ? $data['product_pre_cost_price'][$key] : 0;
                $adjustment_cost_price = isset($data['product_cost_price'][$key]) ? $data['product_cost_price'][$key] : 0;
                $reason  = isset($data['reason'][$key]) ? $data['reason'][$key] : 0;
                $this->db->query("INSERT INTO `" . DB_PREFIX . "cost_adjustment_receipt_to_product` SET
                                                                `cost_adjustment_receipt_id` = " . (int)$receipt_id . ", 
                                                                `product_id` = " . (int)$product_id . ", 
                                                                `product_version_id` = '" . (int)$version_id . "', 
                                                                `current_cost_price` = " . (float)extract_number($current_cost_price) . ", 
                                                                `adjustment_cost_price` = " . (float)extract_number($adjustment_cost_price) . ", 
                                                                `note` = '" . $this->db->escape($reason) . "' 
                        ");

                // update cost_price
                if ($status == self::RECEIPT_APPLY){
                    $this->updateCostPriceProduct($store_id, $product_id, $version_id, $adjustment_cost_price);
                }
            }
        }

        return $receipt_id;
    }

    private function updateCostPriceProduct($store_id, $product_id, $version_id, $adjustment_cost_price)
    {
        $this->db->query("UPDATE `" . DB_PREFIX . "product_to_store` 
                          SET `cost_price` = " . (float)extract_number($adjustment_cost_price) . " 
                          WHERE `store_id` = " . (int)$store_id . " 
                            AND `product_id` = " . (int)$product_id . " 
                            AND `product_version_id` = " . (int)$version_id);
    }

    public function getReceiptById($id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "cost_adjustment_receipt` WHERE `cost_adjustment_receipt_id` = " . (int)$id);

        return $query->row;
    }

    public function getReceiptProductById($id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "cost_adjustment_receipt_to_product` WHERE `cost_adjustment_receipt_id` = " . (int)$id);

        return $query->rows;
    }

    public function autoGenerationCode()
    {
        // max
        $max = $this->getMaxReceiptsId();

        // make code
        $prefix = 'DCGV';
        $code = $prefix . str_pad($max + 1, 6, "0", STR_PAD_LEFT);

        return $code;
    }

    public function getProductByStore($store_id, $data = array())
    {
        $sql = "SELECT p.status as p_status, p.sku as psku, p.sale_on_out_of_stock as p_sale_on_out_of_stock, p.product_id as pid, p.multi_versions, p.price as o_price, p.compare_price as o_compare_price, p.weight as weight, pts.cost_price, pts.quantity as s_quantity, dp.name,p.image, pv.* 
                from " . DB_PREFIX . "product p 
                LEFT JOIN " . DB_PREFIX . "product_description as dp ON (p.product_id = dp.product_id) 
                LEFT JOIN " . DB_PREFIX . "product_version as pv ON CASE 
                                                                            WHEN p.multi_versions = 1 THEN p.product_id 
                                                                            ELSE -1
                                                                            END = pv.product_id
                INNER JOIN " . DB_PREFIX . "product_to_store as pts ON (p.product_id = pts.product_id AND CASE WHEN p.multi_versions = 1 THEN pv.product_version_id ELSE 0 END = pts.product_version_id AND pts.store_id = " . (int)$store_id .") 
                WHERE 1=1 and dp.language_id = " . (int)$this->config->get('config_language_id');

        if (isset($data['current_user_id']) && $data['current_user_id'] !== '') {
            $sql .= " AND p.user_create_id = " . $data['current_user_id'];
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND dp.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " AND ( p.`deleted` IS NULL)";
        $sql .= " AND (pv.`deleted` IS NULL)";
        $sql .= " ORDER BY dp.name ASC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);
        $result = array();
        foreach ($query->rows as $k => $item) {
            $result[$k] = array(
                'product_id' => $item['pid'],
                'name' => mb_strlen($item['name']) > 90 ? mb_substr($item['name'], 0, 90) . "..." : $item['name'],
                'product_version_id' => $item['product_version_id'],
                'product_name' => $item['name'],
                'weight' => $item['weight'],
                'image' => $item['image'],
                'version' => $item['version'],
                'sale_on_out_of_stock' => $item['p_sale_on_out_of_stock'],
                'multi_versions' => $item['multi_versions']
            );

            if ($item['product_version_id'] != null) {
                $result[$k]['id'] = $item['pid'] . '-' . $item['product_version_id'];
                $result[$k]['quantity'] = $item['s_quantity'];
                $result[$k]['price'] = $item['price'] != 0 ? $item['price'] : $item['compare_price'];
                $result[$k]['status'] = $item['status'];
                $result[$k]['p_status'] = $item['p_status'];
                $result[$k]['sku'] = $item['sku'];
                $result[$k]['cost_price'] = $item['cost_price'];
            } else {
                $result[$k]['id'] = $item['pid'];
                $result[$k]['quantity'] = $item['s_quantity'];
                $result[$k]['price'] = $item['o_price'] != 0 ? $item['o_price'] : $item['o_compare_price'];
                $result[$k]['status'] = $item['p_status'];
                $result[$k]['p_status'] = $item['p_status'];
                $result[$k]['sku'] = $item['psku'];
                $result[$k]['cost_price'] = $item['cost_price'];
            }
        }

        return $result;
    }

    public function countProductByStore($store_id, $data = array())
    {
        $language_id = $this->config->get('config_language_id');
        $sql = "SELECT p.product_id,pd.name from " . DB_PREFIX . "product p 
                    LEFT JOIN " . DB_PREFIX . "product_description as pd ON (p.product_id = pd.product_id) 
                    LEFT JOIN " . DB_PREFIX . "product_version as pv ON (p.product_id = pv.product_id) 
                    INNER JOIN " . DB_PREFIX . "product_to_store as pts ON (p.product_id = pts.product_id AND CASE WHEN p.multi_versions = 1 THEN pv.product_version_id ELSE 0 END = pts.product_version_id AND pts.store_id = " . (int)$store_id .") WHERE 1=1";

        if (isset($data['current_user_id']) && $data['current_user_id'] !== '') {
            $sql .= " AND p.user_create_id = " . $data['current_user_id'];
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " AND (p.`deleted` IS NULL OR p.`deleted` = 2)";
        $sql .= " AND (pv.`deleted` IS NULL OR pv.`deleted` = 2)";
        $sql .= " AND pd.language_id = " . $language_id . "";
        $sql .= " ORDER BY p.product_id ASC";
        $query = $this->db->query($sql);

        return count($query->rows);
    }

    public function getProductInStore($store_id, $product_id, $product_version_id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_to_store` WHERE `store_id` = " . (int)$store_id . " AND `product_id` = " . (int)$product_id . " AND `product_version_id` = " . (int)$product_version_id);

        return $query->row;
    }

    private function getMaxReceiptsId()
    {
        $sql = "SELECT MAX(`cost_adjustment_receipt_id`) AS max_id FROM `" . DB_PREFIX . "cost_adjustment_receipt`";

        $query = $this->db->query($sql);

        return $query->row['max_id'];
    }

}