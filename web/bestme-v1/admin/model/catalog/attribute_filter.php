<?php


class ModelCatalogAttributeFilter extends Model
{
    public function updateAttributeFilterByProduct($product_id) {
        // get product attributes
        $DB_PREFIX = DB_PREFIX;
        $language_id = (int)$this->config->get('config_language_id');
        $sql = "SELECT ad.`name` FROM `{$DB_PREFIX}attribute_description` ad
                INNER JOIN `{$DB_PREFIX}product_attribute` pa ON pa.attribute_id = ad.attribute_id
                    WHERE ad.`language_id` = {$language_id} AND pa.`language_id` = {$language_id} AND pa.`product_id` = $product_id";
        $query = $this->db->query($sql);
        $attributes = $query->rows;

        foreach ($attributes as $attribute) {
            $this->updateAttributeFilterByName($attribute['name']);
        }
    }

    public function updateAttributeFilterByName($attribute_name) {
        // delete current records
        $this->deleteAttributeFilter($attribute_name);

        // get all attribute values of each lower attribute name (belong to undeleted and unhidden products)
        $lower_attribute_name = mb_strtolower($attribute_name);
        $language_id = (int)$this->config->get('config_language_id');
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT pa.`text`, pa.`product_id` FROM `{$DB_PREFIX}product_attribute` pa
                        INNER JOIN `{$DB_PREFIX}product` p ON pa.`product_id` = p.`product_id`
                        WHERE pa.`attribute_id` IN (SELECT a.`attribute_id` FROM `{$DB_PREFIX}attribute` a 
                                                INNER JOIN `{$DB_PREFIX}attribute_description` ad ON a.attribute_id = ad.attribute_id
                                                WHERE ad.`language_id` = {$language_id} AND LCASE(ad.`name`) = '{$lower_attribute_name}')
                        AND pa.`language_id` = {$language_id}
                        AND p.`status` = 1
                        AND p.`deleted` IS NULL";
        $query = $this->db->query($sql);
        $attribute_values = $query->rows;

        if (!empty($attribute_values)) {
            // parse attribute values into a array
            $attribute_values_arr = [];
            $product_attribute_value_arr = [];
            foreach ($attribute_values as $attribute_value) {
                $lower_attribute_value_arr = explode(',', mb_strtolower($attribute_value['text']));
                $attribute_values_arr = array_merge($attribute_values_arr, $lower_attribute_value_arr);

                foreach ($lower_attribute_value_arr as $lower_attribute_value) {
                    $product_attribute_value_arr[$attribute_value['product_id']][] = mb_ucwords($lower_attribute_value);
                }
            }

            // remove duplicated attribute values
            $attribute_values_arr = array_unique($attribute_values_arr);

            // format attribute values to capitalize
            $attribute_values_arr = array_map(function ($ava) {
                return $this->db->escape(mb_ucwords($ava));
            }, $attribute_values_arr);

            // save data to attribute_filter table
            $attribute_filter_name = mb_ucwords($lower_attribute_name);
            $attribute_filter_value = implode(',', $attribute_values_arr);
            $sql = "INSERT INTO `{$DB_PREFIX}attribute_filter` (`name`, `value`, `language_id`)
                            SELECT '$attribute_filter_name', '$attribute_filter_value', {$language_id} FROM DUAL
                            WHERE NOT EXISTS (SELECT 1 FROM `{$DB_PREFIX}attribute_filter` WHERE `name` = '$attribute_filter_name')";
            $this->db->query($sql);
            $attribute_filter_id = $this->db->getLastId();

            // save data to product_attribute_filter table
            foreach ($product_attribute_value_arr as $product_id => $product_attribute_values) {
                // remove duplicated attribute values in each product
                $product_attribute_values = array_unique($product_attribute_values);
                foreach ($product_attribute_values as $product_attribute_value) {
                    $attribute_value = $this->db->escape($product_attribute_value);

                    $sql = "INSERT IGNORE INTO `{$DB_PREFIX}product_attribute_filter`
                                    SET `product_id` = {$product_id},
                                        `attribute_filter_id` = {$attribute_filter_id},
                                        `attribute_filter_value` = '{$attribute_value}'";
                    $this->db->query($sql);
                }
            }
        }
    }

    public function deleteAttributeFilter($attribute_filter_name) {
        $DB_PREFIX = DB_PREFIX;
        $language_id = (int)$this->config->get('config_language_id');
        // convert input to correct format
        $attribute_filter_name = mb_ucwords(mb_strtolower($attribute_filter_name));

        // delete product_attribute_filter records
        $sql = "DELETE paf FROM `{$DB_PREFIX}product_attribute_filter` paf 
                INNER JOIN `{$DB_PREFIX}attribute_filter` af ON paf.`attribute_filter_id` = af.`attribute_filter_id`
                WHERE af.`name` = '{$attribute_filter_name}' AND af.`language_id` = {$language_id}";
        $this->db->query($sql);

        // delete attribute_filter record
        $sql = "DELETE FROM `{$DB_PREFIX}attribute_filter` WHERE `name` = '{$attribute_filter_name}' AND `language_id` = {$language_id} LIMIT 1";
        $this->db->query($sql);
    }

    public function getAttributeFilters() {
        $DB_PREFIX = DB_PREFIX;
        $language_id = (int)$this->config->get('config_language_id');
        $sql = "SELECT `attribute_filter_id`, `name`, `value`, `status` FROM `{$DB_PREFIX}attribute_filter` 
                WHERE `language_id` = {$language_id} ORDER BY `name` ASC";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * update all attribute filters status
     * @param array $attribute_filters
     */
    public function updateAttributeFiltersStatus(array $attribute_filters) {
        if (is_array($attribute_filters)) {
            $DB_PREFIX = DB_PREFIX;
            foreach ($attribute_filters as $attribute_filter) {
                $sql = "UPDATE `{$DB_PREFIX}attribute_filter` SET `status` = {$attribute_filter['status']} WHERE `attribute_filter_id` = {$attribute_filter['attribute_filter_id']}";
                $this->db->query($sql);
            }
        }
    }
}