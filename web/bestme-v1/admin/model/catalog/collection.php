<?php

class ModelCatalogCollection extends Model
{
    public function addCollection($data)
    {

        $this->db->query("INSERT INTO " . DB_PREFIX . "collection SET user_create_id = " . (int)$this->user->getId() . ", title = '" . $this->db->escape($data['title']) . "', status = " . $this->db->escape($data['status']) . ", type = '". $this->db->escape($data['type_select']) ."', product_type_sort = '". $this->db->escape($data['cat_list']) ."'");
        $collection_id = $this->db->getLastId();
        $this->db->query("INSERT INTO " . DB_PREFIX . "collection_description SET  
        collection_id = '" . $collection_id . "', 
        image = '" . $this->db->escape($data['image']) . "', 
        description = '" . $this->db->escape($data['description']) . "', 
        meta_title = '" . $this->db->escape($data['seo_name']) . "', 
        meta_description = '" . $this->db->escape($data['body']) . "', 
        alias = '" . $this->db->escape($data['alias']) . "',
        noindex = '" . $this->db->escape($data['noindex']) . "',
        nofollow = '" . $this->db->escape($data['nofollow']) . "',
        noarchive = '" . $this->db->escape($data['noarchive']) . "',
        noimageindex = '" . $this->db->escape($data['noimageindex']) . "',
        nosnippet = '" . $this->db->escape($data['nosnippet']) . "'");

        // create seo
        $this->load->model('custom/common');

        $collection_alias = $data['title'];
        if (isset($data['alias']) && $data['alias'] != '') {
            $collection_alias = $data['alias'];
        }
        $slug = $this->model_custom_common->createSlug($collection_alias, '-');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug);
        $language_id = $this->config->get('config_language_id');
        $alias = 'collection='.$collection_id;
        $sql = "INSERT INTO " . DB_PREFIX . "seo_url SET language_id = '" . (int)$language_id . "', query = '".$alias."', keyword = '".$slug_custom."', table_name = 'collection'";
        $this->db->query($sql);
        // Luu vao bo suu tap
        if(isset($data['collection_list']) != '' && $data['type_select'] != 1){
            $this->db->query("DELETE FROM " . DB_PREFIX . "collection_to_collection WHERE collection_id IN (" . implode(',',$data['collection_list']) . ") and parent_collection_id = '".$collection_id."'");
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_collection WHERE product_id IN (" . implode(',',$data['collection_list']) . ") and collection_id = '".$collection_id."'");
            foreach ($data['collection_list'] as $value){
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_collection SET product_id = '" . (int)$value . "', collection_id = '".$collection_id."' ");
            }
        } else {
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_collection WHERE product_id IN (" . implode(',',$data['collection_list']) . ") and collection_id = '".$collection_id."'");
            $this->db->query("DELETE FROM " . DB_PREFIX . "collection_to_collection WHERE collection_id IN (" . implode(',',$data['collection_list']) . ") and parent_collection_id = '".$collection_id."'");
            foreach ($data['collection_list'] as $key => $value){
                $this->db->query("INSERT INTO " . DB_PREFIX . "collection_to_collection SET collection_id = '" . (int)$value . "', parent_collection_id = '". (int)$collection_id."', sort_order = '". (int)($key + 1) ."'");
            }
        }

        return $collection_id;
    }



    public function getAllcollection($data = array())
    {

        $sql = "SELECT c.*, pc.*, CONCAT(u.lastname, ' ', u.firstname) as fullname FROM " . DB_PREFIX . "collection c 
                            LEFT JOIN " . DB_PREFIX . "collection_description pc ON (c.collection_id = pc.collection_id) 
                            LEFT JOIN " . DB_PREFIX ."user u ON (u.user_id = c.user_create_id)  
                            WHERE 1=1 ";

        if (!empty($data['filter_name'])) {
            $sql .= " AND c.title LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $sql .= " AND c.status = '" . (int)$data['filter_status'] . "'";
        }

        if (isset($data['filter_staff']) && $data['filter_staff'] !== '') {
            $sql .= " AND c.user_create_id IN (" . $data['filter_staff'] . ")";
        }

        $sql .= " ORDER BY c.collection_id DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }
            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getListCollection($data = array())
    {

        $sql = "SELECT * FROM " . DB_PREFIX . "collection c LEFT JOIN " . DB_PREFIX . "collection_description pc ON (c.collection_id = pc.collection_id) WHERE 1=1";

        if (!empty($data['filter_name'])) {
            $sql .= " AND c.title LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['type'])) {
            $sql .= " AND (c.type = 0 OR c.type IS NULL) ";
        }

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $sql .= " AND c.status = '" . (int)$data['filter_status'] . "'";
        }
        $sql .= " ORDER BY c.collection_id";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }
            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getAllcollection_filter()
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "collection c LEFT JOIN " . DB_PREFIX . "collection_description pc ON (c.collection_id = pc.collection_id) WHERE 1=1 ";
        $sql .= " ORDER BY c.collection_id";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getTotalCollections($data = [])
    {
        $sql = "SELECT COUNT(DISTINCT c.collection_id) AS total FROM " . DB_PREFIX . "collection c LEFT JOIN " . DB_PREFIX . "collection_description pc ON (c.collection_id = pc.collection_id) WHERE 1=1 ";

        if (!empty($data['filter_name'])) {
            $sql .= " AND c.title LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['type'])) {
            $sql .= " AND (c.type = 0 OR c.type IS NULL) ";
        }

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $sql .= " AND c.status = '" . (int)$data['filter_status'] . "'";
        }

        if (isset($data['filter_staff']) && $data['filter_staff'] !== '') {
            $sql .= " AND c.user_create_id IN (" . $data['filter_staff'] . ")";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getCollection($collection_id) {
        $sql = "SELECT * FROM " . DB_PREFIX . "collection c LEFT JOIN " . DB_PREFIX . "collection_description pc ON (c.collection_id = pc.collection_id) WHERE c.collection_id = ".$collection_id." ";
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getCollectionPaginator($data = array()) {
        $sql = "SELECT p.product_id, dp.name,p.image, IF(p.price = 0 , p.compare_price, p.price) as price, p.date_added from ". DB_PREFIX ."product p LEFT JOIN " . DB_PREFIX . "product_description as dp ON (p.product_id = dp.product_id)  WHERE 1=1 and dp.language_id = ".(int)$this->config->get('config_language_id')."";
        if (!empty($data['filter_name'])) {
            $sql .= " AND dp.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (isset($data['current_user_id']) && !empty($data['current_user_id'])) {
            $sql .= " AND p.user_create_id = '" . (int)$data['current_user_id'] . "'";
        }

        $sql .= " AND p.`deleted` IS NULL ";
        $sql .= " ORDER BY dp.name ASC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }
    public function getProductCollections($type='',$value='',$collection_id='')
    {

        $sql = "SELECT p.product_id, p.image,pd.name,pc.collection_id,p.price,p.date_added,pc.product_collection_id,pc.sort_order from ". DB_PREFIX ."product_collection pc LEFT JOIN " . DB_PREFIX . "product as p ON (pc.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description as pd ON (p.product_id = pd.product_id) WHERE 1=1 ";
        if($collection_id != ''){
            $sql .= ' AND pc.collection_id = "'.$collection_id.'" ';
        }
        $sql .= ' GROUP BY p.product_id';

        if($value == 'az'){
            $sql .= ' ORDER BY pd.name ASC';
        }
        elseif($value == 'za'){
            $sql .= ' ORDER BY pd.name DESC';
        }
        elseif($value == 'hightolow'){
            $sql .= ' ORDER BY p.price DESC';
        }
        elseif($value == 'lowtohigh'){
            $sql .= ' ORDER BY p.price ASC';
        }
        elseif($value == 'newtoold'){
            $sql .= ' ORDER BY p.date_added DESC';
        }
        elseif($value == 'oldtonew'){
            $sql .= ' ORDER BY p.date_added ASC';
        }
        else{
            $sql .= ' ORDER BY pc.product_collection_id ASC ';
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getCollectionCollections($type='',$value='',$collection_id='')
    {

        $sql = "SELECT * from ". DB_PREFIX ."collection_to_collection ctc LEFT JOIN " . DB_PREFIX . "collection_description as cd ON (ctc.collection_id = cd.collection_id) LEFT JOIN " . DB_PREFIX . "collection as c ON (ctc.collection_id = c.collection_id) WHERE 1=1 ";
        if($collection_id != ''){
            $sql .= ' AND ctc.parent_collection_id = "'.$collection_id.'" ';
        }

        if($value == 'az'){
            $sql .= ' ORDER BY c.title ASC';
        }
        elseif($value == 'za'){
            $sql .= ' ORDER BY c.title DESC';
        }
        else{
            $sql .= ' ORDER BY ctc.sort_order ASC ';
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getCountProductCollections($collection_id)
    {
        $sql = "SELECT COUNT(product_collection_id) AS total FROM " . DB_PREFIX . "product_collection WHERE collection_id = $collection_id";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getCountCollectionCollections($collection_id)
    {
        $sql = "SELECT COUNT(collection_to_collection_id) AS total FROM " . DB_PREFIX . "collection_to_collection WHERE parent_collection_id = '". (int)$collection_id ."'";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function countProductAll($data = array()) {
        $language_id = $this->config->get('config_language_id');
        $sql = "SELECT p.product_id,pd.name from ". DB_PREFIX ."product p LEFT JOIN " . DB_PREFIX . "product_description as pd ON (p.product_id = pd.product_id) WHERE 1=1";
        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }
        $sql .= " AND p.`deleted` IS NULL ";
        $sql .= " AND pd.language_id = ".$language_id."";
        
        if (isset($data['current_user_id']) && !empty($data['current_user_id'])) {
            $sql .= " AND p.user_create_id = '" . (int)$data['current_user_id'] . "'";
        }

        $sql .= " ORDER BY p.product_id ASC";
        $query = $this->db->query($sql);

        return count($query->rows);
    }

    public function getAddListProduct($product_id='',$sort = '')
    {
        $language_id = $this->config->get('config_language_id');
        $sql = "SELECT p.product_id, p.image,pd.name,p.price,p.date_added from " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description as pd ON (p.product_id = pd.product_id) WHERE 1=1 AND p.product_id IN (".$product_id.") AND pd.language_id = ".$language_id." ";
        if($sort == 'az'){
            $sql .= ' ORDER BY pd.name ASC';
        }
        if($sort == 'za'){
            $sql .= ' ORDER BY pd.name DESC';
        }
        if ($sort == 'hightolow') {
            $sql .= ' ORDER BY p.price DESC';
        }
        if ($sort == 'lowtohigh') {
            $sql .= ' ORDER BY p.price ASC';
        }
        if ($sort == 'newtoold') {
            $sql .= ' ORDER BY p.date_added DESC';
        }
        if ($sort == 'oldtonew') {
            $sql .= ' ORDER BY p.date_added ASC';
        }
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function updateStatus($id,$status) {
        $this->db->query("UPDATE " . DB_PREFIX . "collection SET status = ".$status." WHERE collection_id = " . (int)$id . "");
        return true;
    }
    public function updateSortOrder($id) {
        $result = $this->db->query("SELECT sort_order  FROM " . DB_PREFIX . "product_collection WHERE product_collection_id = " . (int)$id . "");
        $order_sort = $result->row['sort_order'];
        if($order_sort > 0){
            $this->db->query("UPDATE " . DB_PREFIX . "product_collection SET sort_order = sort_order - 1 WHERE product_collection_id = " . (int)$id . "");
        }
    }
    public function deleteCollection($collection_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "collection WHERE collection_id = '" . (int)$collection_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'collection=" . (int)$collection_id . "'");
        //delete tag, collection
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_collection WHERE collection_id = '" . (int)$collection_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "collection_to_collection WHERE parent_collection_id = '" . (int)$collection_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "collection_description WHERE collection_id = '" . (int)$collection_id . "'");
        $this->deleteProductCaches();
    }
    public function deleteProductCollection($product_collection_id) {

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_collection WHERE product_collection_id = '" . (int)$product_collection_id . "'");

    }

    public function editCollection($collection_id, $data)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "collection SET  title = '" . $this->db->escape($data['title']) . "',status = " . $data['status'] . ", product_type_sort = '" . $this->db->escape($data['cat_list']) . "', type= '" . $this->db->escape($data['type_select']) . "'  WHERE collection_id = '" . (int)$collection_id . "'");
        $this->db->query("UPDATE " . DB_PREFIX . "collection_description SET  
        image = '" . $this->db->escape($data['image']) . "',
        description = '" . $this->db->escape($data['description']) . "',
        meta_title = '" . $this->db->escape($data['seo_name']) . "',
        meta_description = '" . $this->db->escape($data['body']) . "',
        alias = '" . $this->db->escape($data['alias']) . "',
        noindex = '" . $this->db->escape($data['noindex']) . "',
        nofollow = '" . $this->db->escape($data['nofollow']) . "',
        noarchive = '" . $this->db->escape($data['noarchive']) . "',
        noimageindex = '" . $this->db->escape($data['noimageindex']) . "',
        nosnippet = '" . $this->db->escape($data['nosnippet']) . "'
         WHERE collection_id = " . (int)$collection_id . "");

        // TODO: change "hidearray" to "product_list"...

        /*
         * Notice: if $data['hidearray'] not set, no change!
         * TODO: keep (as no changed, by check if !isset($data['hidearray'])...) or remove?
         */
        if (isset($data['hidearray']) && is_array($data['hidearray'])) {
            /*
             * Notice: if $data['hidearray'] empty, all products/sub-collections will be remove from this collection!
             * TODO: keep (as no changed, by check if !empty($data['hidearray'])... ) or remove?
             */

            // collection contains products
            if ($data['type_select'] != 1) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_collection WHERE collection_id = '" . $collection_id . "'");
                $this->db->query("DELETE FROM " . DB_PREFIX . "collection_to_collection WHERE parent_collection_id = '" . $collection_id . "'");

                foreach ($data['hidearray'] as $key => $value) {
                    //$addOrderBy = $this->addOrderBy();
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_collection SET product_id = '" . (int)$value . "', collection_id = '" . $collection_id . "' , sort_order = '" . ($key + 1) . "'");
                }
            } else { // collection contains sub-collections
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_collection WHERE collection_id = '" . $collection_id . "'");
                $this->db->query("DELETE FROM " . DB_PREFIX . "collection_to_collection WHERE parent_collection_id = '" . $collection_id . "'");

                foreach ($data['hidearray'] as $key => $value) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "collection_to_collection SET collection_id = '" . (int)$value . "', parent_collection_id = '" . (int)$collection_id . "', sort_order = '" . ($key + 1) . "'");
                }
            }
        }

        //seo
        //delete seo product
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'collection=" . (int)$collection_id . "'");

        $this->load->model('custom/common');
        $tmp_alias = (isset($data['alias']) && $data['alias'] != '') ? $data['alias'] : ((isset($data['title']) && $data['title'] != '') ? $data['title'] : '');
        $data['alias'] = $tmp_alias;

        if (isset($data['alias']) && $data['alias'] != '') {
            $slug = $this->model_custom_common->createSlug($data['alias'], '-');
            $slug_custom = $this->model_custom_common->getSlugUnique($slug);
            //create new seo product
            $language_id = $this->config->get('config_language_id');
            $alias = 'collection=' . $collection_id;
            $sqlseo = "INSERT INTO " . DB_PREFIX . "seo_url SET language_id = '" . (int)$language_id . "', query = '" . $alias . "', keyword = '" . $slug_custom . "', table_name = 'collection'";
            $this->db->query($sqlseo);
        }
        $this->deleteProductCaches();

    }

    public function addOrderBy(){
        $sql = "SELECT MAX('order_sort') as order_sort FROM " . DB_PREFIX . "product_collection";
        $result = $this->db->query($sql);
        return $result->row['total']+1;
    }

    public function getNameRecordById($collection_id, $tableName, $name = null)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . $tableName . " WHERE collection_id = '" . (int)$collection_id . "'");
        if ($name) {
            if ($query->row[$name]) {
                return $query->row[$name];
            }
            return null;
        }
        if ($query->row['title']) {
            return $query->row['title'];
        }
        return null;
    }

    public function check_collection($name = '')
    {
        $sql = "SELECT COUNT(collection_id) AS total FROM " . DB_PREFIX . "collection 
                WHERE title = '" . $this->db->escape(trim($name)) . "'";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    /**
     * check if collection contains product(s)
     * @param $collection_id
     * @param $product_id
     */
    public function hasProducts($collection_id, $product_id = null) {
        if (is_null($product_id)) {
            $product_sub_query = '';
        } elseif (is_array($product_id)) {
            if (empty($product_id)) {
                return false;
            }

            $product_sub_query = ' AND product_id IN ('. implode(",", $product_id) .') ';
        } else {
            $product_sub_query = ' AND product_id = "'. $product_id . '"';
        }

        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT EXISTS (SELECT 1 FROM `{$DB_PREFIX}product_collection` 
                WHERE `collection_id` = '{$collection_id}' {$product_sub_query}) result";

        $query = $this->db->query($sql);
        return 1 == $query->row['result'];
    }

    public function getCollectionIdByTitle($title)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "collection` WHERE `title` = '" . $this->db->escape($title) . "'");

        if (isset($query->row['collection_id'])) {
            return (int)$query->row['collection_id'];
        }

        return false;
    }

    public function addCollectionTitleOnly($title)
    {
        if (trim($title) == '') {
            return false;
        }

        $query = $this->db->query("INSERT INTO `" . DB_PREFIX . "collection` SET `title` = '" . $this->db->escape($title) . "', `status` = 1");
        $collection_id = $this->db->getLastId();
        $this->db->query("INSERT INTO " . DB_PREFIX . "collection_description SET  collection_id = '" . $collection_id . "', image = '', description = '', meta_title = '', meta_description = '', alias = ''");

        return $collection_id;
    }

    /**
     * delete product caches
     */
    private function deleteProductCaches() {
        $product_key_redis =  DB_PREFIX . 'product';
        $this->cache->delete($product_key_redis);
    }
}
