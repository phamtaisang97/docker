<?php

class ModelCatalogWarehouseOld extends Model
{
    public function addWarehouse($data)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "warehouse SET product_id = '" . (int)$data['product_id'] . "', product_version_id = '" . (int)$data['product_version_id'] . "'");

        $warehouse_id = $this->db->getLastId();

        return $warehouse_id;
    }

    public function editWarehouse($warehouse_id, $data)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "warehouse SET product_id = '" . (int)$data['product_id'] . "', product_version_id = '" . (int)$data['product_version_id'] . "' WHERE warehouse_id = '" . (int)$warehouse_id . "'");

        // update product
        $this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '" . (int)$data['quantity'] . "' WHERE product_id = (SELECT w.product_id FROM " . DB_PREFIX . "warehouse w LEFT JOIN " . DB_PREFIX . "product p ON (w.product_id = p.product_id) WHERE w.warehouse_id = '" . (int)$warehouse_id . "'");
    }

    public function editWarehouseQuantity($warehouse_id, $data, $quantity_method = 'set')
    {
        if ($quantity_method == 'add') {
            // update product not multi versions
            $this->db->query(sprintf("UPDATE " . DB_PREFIX . "product SET quantity = quantity + '%d'" .
                " WHERE product_id = (" .
                "    SELECT x.product_id FROM (" .
                "            SELECT w.* FROM " . DB_PREFIX . "warehouse w" .
                "            LEFT JOIN " . DB_PREFIX . "product p ON (w.product_id = p.product_id)" .
                "            WHERE w.warehouse_id = '%d' " .
                "            AND w.product_version_id IS NULL " .
                "        ) as x" .
                ")",
                (int)$data['quantity'],
                (int)$warehouse_id
            ));

            // update product multi versions
            $this->db->query(sprintf("UPDATE " . DB_PREFIX . "product_version SET quantity = quantity + '%d'" .
                " WHERE product_version_id = (" .
                "    SELECT x.product_version_id FROM (" .
                "            SELECT w.* FROM " . DB_PREFIX . "warehouse w" .
                "            LEFT JOIN " . DB_PREFIX . "product_version pv ON (w.product_version_id = pv.product_version_id)" .
                "            WHERE w.warehouse_id = '%d' " .
                "            AND w.product_version_id IS NOT NULL " .
                "        ) as x" .
                ")",
                (int)$data['quantity'],
                (int)$warehouse_id
            ));

            // $this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = quantity + '" . (int)$data['quantity'] . "' WHERE product_id = (SELECT w.product_id FROM " . DB_PREFIX . "warehouse w LEFT JOIN " . DB_PREFIX . "product p ON (w.product_id = p.product_id) WHERE w.warehouse_id = '" . (int)$warehouse_id . "'");
        } else {
            // update product not multi versions
            $this->db->query(sprintf("UPDATE " . DB_PREFIX . "product SET quantity = '%d'" .
                " WHERE product_id = (" .
                "    SELECT x.product_id FROM (" .
                "            SELECT w.* FROM " . DB_PREFIX . "warehouse w" .
                "            LEFT JOIN " . DB_PREFIX . "product p ON (w.product_id = p.product_id)" .
                "            WHERE w.warehouse_id = '%d' " .
                "            AND w.product_version_id IS NULL " .
                "        ) as x" .
                ")",
                (int)$data['quantity'],
                (int)$warehouse_id
            ));

            // update product multi versions
            $this->db->query(sprintf("UPDATE " . DB_PREFIX . "product_version SET quantity = '%d'" .
                " WHERE product_version_id = (" .
                "    SELECT x.product_version_id FROM (" .
                "            SELECT w.* FROM " . DB_PREFIX . "warehouse w" .
                "            LEFT JOIN " . DB_PREFIX . "product_version pv ON (w.product_version_id = pv.product_version_id)" .
                "            WHERE w.warehouse_id = '%d' " .
                "            AND w.product_version_id IS NOT NULL " .
                "        ) as x" .
                ")",
                (int)$data['quantity'],
                (int)$warehouse_id
            ));

            // $this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '" . (int)$data['quantity'] . "' WHERE product_id = (SELECT w.product_id FROM " . DB_PREFIX . "warehouse w LEFT JOIN " . DB_PREFIX . "product p ON (w.product_id = p.product_id) WHERE w.warehouse_id = '" . (int)$warehouse_id . "'");
        }
    }

    public function editWarehouseSaleOnOutOfStock($warehouse_id, $data)
    {
        $this->db->query(sprintf("UPDATE " . DB_PREFIX . "product SET sale_on_out_of_stock = '%d'" .
            " WHERE product_id IN (" .
            "    SELECT x.product_id FROM (" .
            "            SELECT w.* FROM " . DB_PREFIX . "warehouse w" .
            "            LEFT JOIN " . DB_PREFIX . "product p ON (w.product_id = p.product_id)" .
            "            WHERE w.warehouse_id = '%d' " .
            "        ) as x" .
            ")",
            (int)$data['sale_on_out_of_stock'],
            (int)$warehouse_id
        ));
    }

    public function editWarehousesSaleOnOutOfStock($warehouse_ids, $data)
    {
        $escaped_warehouse_ids = array_map(function ($warehouse_id) {
            return "'" . $warehouse_id . "'";
        }, $warehouse_ids);

        $this->db->query(sprintf("UPDATE " . DB_PREFIX . "product SET sale_on_out_of_stock = '%d'" .
            " WHERE product_id IN (" .
            "    SELECT x.product_id FROM (" .
            "            SELECT w.* FROM " . DB_PREFIX . "warehouse w" .
            "            LEFT JOIN " . DB_PREFIX . "product p ON (w.product_id = p.product_id)" .
            "            WHERE w.warehouse_id IN (%s) " .
            "        ) as x" .
            ")",
            (int)$data['sale_on_out_of_stock'],
            implode(',', $escaped_warehouse_ids)
        ));
    }

    public function deleteWarehouse($warehouse_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "warehouse WHERE warehouse_id = '" . (int)$warehouse_id . "'");
    }

    public function getWarehouse($warehouse_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "warehouse w LEFT JOIN " . DB_PREFIX . "product p ON (w.product_id = p.product_id) WHERE w.warehouse_id = '" . (int)$warehouse_id . "'");

        return $query->row;
    }

    public function getWarehouses($data = array())
    {
        $sql = $this->getAllWarehousesSql($data);

        /* pagination */
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = $this->config->get('config_limit_admin');
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalWarehouses($data = [])
    {
        $sqlAll = $this->getAllWarehousesSql($data, true);
        $query = $this->db->query("SELECT COUNT(t.`warehouse_id`) AS `total` FROM (" . $sqlAll . ") as t");

        return $query->row['total'];
    }

    public function getAllWarehousesSql($data = [], $ware_house_only = false)
    {
        if ($ware_house_only) {
            $sql = "SELECT w.* ";
        } else {
            $sql = "SELECT w.*, p.*, pd.*, pv.version, p.sale_on_out_of_stock as ware_sale_on_out_of_stock, ";
            $sql .= " CASE WHEN w.product_version_id IS NULL THEN pd.name ELSE CONCAT(pd.name, ' ', pv.version) END AS name_version, ";
            $sql .= " CASE WHEN w.product_version_id IS NULL THEN p.sku ELSE pv.sku END AS sku_version, ";
            $sql .= " CASE WHEN w.product_version_id IS NULL THEN IF(p.price=0, p.compare_price, p.price) ELSE IF(pv.price=0, pv.compare_price, pv.price) END AS price_version, ";
            $sql .= " CASE WHEN w.product_version_id IS NULL THEN p.quantity ELSE pv.quantity END AS quantity_version ";
        }

        $sql .= " FROM " . DB_PREFIX . "warehouse w ";
        $sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (w.product_id = p.product_id) ";
        $sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";
        $sql .= " LEFT JOIN " . DB_PREFIX . "product_version as pv ON (pv.product_version_id = w.product_version_id)";

        if (isset($data['store_id']) && $data['store_id'] !== ''){
            $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_store as ps ON (w.product_id = ps.product_id) ";
        }

        $sql .= " LEFT JOIN " . DB_PREFIX . "product_tag as pt ON (w.product_id = pt.product_id) ";
        $sql .= " LEFT JOIN " . DB_PREFIX . "tag t ON (t.tag_id = pt.tag_id)";

        if (isset($data['filter_category']) && $data['filter_category'] !== '') {
            $value = implode(", ", array_map('intval', $data['filter_category']));
            $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) ";
            $sql .= " WHERE p2c.category_id IN(" . $value . ") ";
        } else {
            $sql .= " WHERE 1";
        }

        /* except original product if multi versions */
        $sql .= " AND CASE WHEN p.multi_versions = 1 THEN pv.product_version_id IS NOT NULL ELSE pv.product_id IS NULL END";

        if (isset($data['store_id']) && $data['store_id'] !== ''){
            $sql .= " AND ps.store_id = '" . (int)$data['store_id'] . "' ";
        }

        if (isset($data['filter_warehouse']) && $data['filter_warehouse'] !== '') {
            $sql .= " AND w.warehouse_id = '" . (int)$data['filter_warehouse'] . "' ";
        }

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $sql .= " AND p.`status` = '" . (int)$data['filter_status'] . "' ";
        }

        if (isset($data['filter_name']) && $data['filter_name'] !== '') {
            $sql .= " AND (pd.`name` LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%' ";
            $sql .= " OR t.`value` LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%' ) ";
        }

        if (isset($data['filter_manufacturer']) && $data['filter_manufacturer']) {
            $value = implode(", ", array_map('intval', $data['filter_manufacturer']));
            $sql .= " AND p.manufacturer_id IN(" . $value . ")";
        }

        if (isset($data['filter_tag']) && $data['filter_tag']) {
            $sql .= " AND t.value RLIKE '" . $data['filter_tag'] . "'";
        }

        $sql .= " AND p.`deleted` IS NULL ";

        if (isset($data['filter_amount']) && $data['filter_amount']) {
            $count = 0;
            $sql .= " AND ( ";
            foreach ($data['filter_amount'] as $amounts) {
                $amount = explode('_', $amounts);
                $price = isset($amount[1]) ? $amount[1] : 0;
                if ($amount[0] == 1) {
                    if ($count == 0) {
                        $sql .= " CASE WHEN pv.quantity IS NULL THEN p.quantity >'" . (int)$price . "' ELSE pv.quantity >'" . (int)$price . "' END";
                    } else {
                        $sql .= " OR CASE WHEN pv.quantity IS NULL THEN p.quantity >'" . (int)$price . "' ELSE pv.quantity >'" . (int)$price . "' END";
                    }

                }
                if ($amount[0] == 2) {
                    if ($count == 0) {
                        $sql .= " CASE WHEN pv.quantity IS NULL THEN p.quantity <'" . (int)$price . "' ELSE pv.quantity <'" . (int)$price . "' END";
                    } else {
                        $sql .= " OR CASE WHEN pv.quantity IS NULL THEN p.quantity <'" . (int)$price . "' ELSE pv.quantity <'" . (int)$price . "' END";
                    }
                }
                if ($amount[0] == 3) {
                    if ($count == 0) {
                        $sql .= " CASE WHEN pv.quantity IS NULL THEN p.quantity ='" . (int)$price . "' ELSE pv.quantity ='" . (int)$price . "' END";
                    } else {
                        $sql .= " OR CASE WHEN pv.quantity IS NULL THEN p.quantity ='" . (int)$price . "' ELSE pv.quantity ='" . (int)$price . "' END";
                    }
                }
                $count++;
            }
            $sql .= " ) ";
        }

        $sql .= " GROUP BY w.warehouse_id, pv.product_version_id";

        $sort_data = array();

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY p.date_modified";
        }

        if (isset($data['order']) && ($data['order'] == 'ASC')) {
            $sql .= " ASC";
        } else {
            $sql .= " DESC";
        }

        return $sql;
    }
}
