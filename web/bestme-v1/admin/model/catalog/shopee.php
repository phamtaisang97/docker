<?php


class ModelCatalogShopee extends Model
{
    use AUTO_CREATE_RECEIPT_VOUCHER_UTIL_TRAIT;

    // product
    const SYNC_STATUS_ALL = 99;
    const SYNC_STATUS_MAPPING_PRODUCT = 1;
    const SYNC_STATUS_NOT_MAPPING_PRODUCT = 0;

    // order
    const SYNC_STATUS_SYNC_ALL = 99;
    const SYNC_STATUS_SYNC_SUCCESS = 1;
    const SYNC_STATUS_ERROR_PRODUCT_ORDER_NOT_SYNC = 0;
    const SYNC_STATUS_ERROR_ORDER_IS_EDITED = 2;
    const SYNC_STATUS_ERROR_PRODUCT_BESTMEEE_REMOVED = 3;
    const SYNC_STATUS_ERROR_PRODUCT_OUT_OF_STOCK_ON_ADMIN = 4;
    const SYNC_STATUS_ERROR_ORDER_SYNC_TIMEOUT = 5;
    const SYNC_STATUS_ORDER_NOT_YET_SYNC = 6;

    const DEFAULT_SYNC_INTERVAL = 720;

    public function updateProduct($data)
    {
        if (!array_key_exists('item_id', $data) || !$data['item_id']) {
            return;
        }
        $product = $this->db->query("SELECT item_id FROM " . DB_PREFIX . "shopee_product WHERE item_id = " . (int)$data['item_id']);
        if (isset($product->row['item_id'])) {
            $this->editProduct($data);
        } else {
            $this->addProduct($data);
        }

    }

    public function addProduct($data)
    {
        try {
            $this->db->query("INSERT INTO " . DB_PREFIX . "shopee_product SET item_id = " . (int)$data['item_id'] . ", shopid = " . (int)$data['shop_id'] . ", sku = '" . $this->db->escape($data['sku']) . "', 
                                                                          stock = " . (int)$data['stock'] . ", status = '" . $this->db->escape($data['status']) . "', name = '" . $this->db->escape($data['name']) . "',
                                                                          description = '" . $this->db->escape($data['description']) . "', images = '" . $this->db->escape($data['images']) . "',
                                                                          currency = '" . $this->db->escape($data['currency']) . "', price = '" . (float)($data['currency']) . "',
                                                                          original_price = '" . (float)($data['original_price']) . "', weight = '" . (float)($data['weight']) . "', 
                                                                          category_id = " . (int)$data['category_id'] . ", has_variation = " . (int)$data['has_variation'] . ", 
                                                                          variations = '" . $this->db->escape($data['variations']) . "', attributes = '" . $this->db->escape($data['attributes']) . "', created_at = NOW(), updated_at = NOW()");

        } catch (Exception $e) {
            return $data['item_id'];
        }

        return $this->db->getLastId();
    }

    public function editProduct($data)
    {
        try {
            $this->db->query("UPDATE " . DB_PREFIX . "shopee_product SET shopid = " . (int)$data['shop_id'] . ", sku = '" . $this->db->escape($data['sku']) . "', 
                                                                          stock = " . (int)$data['stock'] . ", status = '" . $this->db->escape($data['status']) . "', name = '" . $this->db->escape($data['name']) . "',
                                                                          description = '" . $this->db->escape($data['description']) . "', images = '" . $this->db->escape($data['images']) . "',
                                                                          currency = '" . $this->db->escape($data['currency']) . "', price = '" . (float)($data['currency']) . "',
                                                                          original_price = '" . (float)($data['original_price']) . "', weight = '" . (float)($data['weight']) . "', 
                                                                          category_id = " . (int)$data['category_id'] . ", has_variation = " . (int)$data['has_variation'] . ", 
                                                                          variations = '" . $this->db->escape($data['variations']) . "', attributes = '" . $this->db->escape($data['attributes']) . "', updated_at = NOW() 
                                                                          WHERE item_id = " . (int)$data['item_id']);

        } catch (Exception $e) {
            return $data['item_id'];
        }
    }

    public function updateBestmeId($shopee_product_id, $bestme_id)
    {
        // update link
        $this->db->query("UPDATE " . DB_PREFIX . "shopee_product 
                            SET bestme_id = " . (int)$bestme_id . ", 
                                bestme_product_version_id = NULL, 
                                sync_status = 1 
                            WHERE id = " . (int)$shopee_product_id);
    }

    public function updateLinkProductHasVersion($product_id, $product)
    {
        $DB_PREFIX = DB_PREFIX;
        // get product version
        $productVersionBestme = $this->db->query("SELECT * FROM `{$DB_PREFIX}product_version` WHERE product_id = '" . (int)$product_id . "' AND deleted IS NULL");

        // get product shopee version
        $productVersionShopee = $this->db->query("SELECT * FROM `{$DB_PREFIX}shopee_product_version` WHERE item_id = '" . (int)$product['item_id'] . "'");

        foreach ($productVersionBestme->rows as $p_bestme) {
            foreach ($productVersionShopee->rows as $p_shopee) {
                if ($p_bestme['version'] == $p_shopee['version_name']) {
                    $this->db->query("UPDATE `{$DB_PREFIX}shopee_product_version` 
                                            SET `bestme_product_id` = {$product_id}, 
                                                `bestme_product_version_id` = {$p_bestme['product_version_id']}, 
                                                `sync_status` = 1 
                                                WHERE `id` = {$p_shopee['id']} 
                                            ");
                }
            }
        }
    }

    public function updateBestmeProductIdForVersion($shopee_product_version_id, $bestme_id, $shopee_product_version_item_id)
    {
        // update link for version
        $this->db->query("UPDATE " . DB_PREFIX . "shopee_product_version 
                            SET bestme_product_id = " . (int)$bestme_id . ", 
                                bestme_product_version_id = NULL, 
                                sync_status = 1 
                            WHERE id = " . (int)$shopee_product_version_id);

        // update status for product
        $this->db->query("UPDATE " . DB_PREFIX . "shopee_product 
                            SET bestme_id = " . (int)$bestme_id . ", 
                                bestme_product_version_id = NULL, 
                                sync_status = 1 
                            WHERE item_id = " . (int)$shopee_product_version_item_id);
    }

    // TODO: remove...
    public function deleteProductNotInList($list)
    {
        if (!is_array($list) || empty($list)) {
            return;
        }
        $list = implode(', ', $list);
        $this->db->query("DELETE FROM " . DB_PREFIX . "shopee_product WHERE item_id NOT IN (" . $list . ")");
    }

    public function createTable()
    {
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "shopee_product` ( 
           `id` INT(11) NOT NULL AUTO_INCREMENT , 
           `item_id` BIGINT(64) NOT NULL , 
           `shopid` BIGINT(64) NOT NULL , 
           `sku` VARCHAR(64) NULL , 
           `stock` INT(32) NULL ,
           `status` VARCHAR(32) NULL , 
           `name` VARCHAR(255) CHARACTER SET utf8 NOT NULL ,
           `description` TEXT CHARACTER SET utf8 NOT NULL ,
           `images` TEXT NOT NULL , 
           `currency` VARCHAR(32) NOT NULL , 
           `price` FLOAT(15,4) NOT NULL , 
           `original_price` FLOAT(15,4) NOT NULL , 
           `weight` FLOAT(15,4) NOT NULL , 
           `category_id` BIGINT(64) NOT NULL , 
           `has_variation` BOOLEAN NOT NULL DEFAULT FALSE , 
           `variations` TEXT CHARACTER SET utf8 NULL , 
           `attributes` TEXT CHARACTER SET utf8 NULL , 
           `bestme_id` INT(11) NULL , 
          `created_at` datetime NOT NULL,
          `updated_at` datetime NOT NULL,
        PRIMARY KEY (`id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8;");
    }

    public function getProducts(array $filter_data)
    {
        $sql = "select * from  " . DB_PREFIX . "shopee_product p WHERE 1";
        if (!empty($filter_data['filter_name'])) {
            $sql .= " AND p.name LIKE '%" . $this->db->escape($filter_data['filter_name']) . "%'";
        }

        if (isset($filter_data['shop_id'])) {
            $sql .= " AND p.`shopid` = " . $this->db->escape($filter_data['shop_id']);
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getIds($shopee_id)
    {
        if (!$shopee_id) {
            return [];
        }

        $query = $this->db->query("SELECT id FROM " . DB_PREFIX . "shopee_product WHERE shopid = " . (int)$shopee_id);

        $ids = array_map(function ($row) {
            return $row['id'];
        }, $query->rows);

        return $ids;
    }

    public function getProductsCustom($filter_data)
    {
        $sql = "select * from  " . DB_PREFIX . "shopee_product p WHERE 1";
        if (!empty($filter_data['filter_name'])) {
            $sql .= " AND p.name LIKE '%" . $this->db->escape($filter_data['filter_name']) . "%'";
        }

        if (isset($filter_data['shop_id'])) {
            $sql .= " AND p.`shopid` = " . $this->db->escape($filter_data['shop_id']);
        }

        if (isset($filter_data['start']) || isset($filter_data['limit'])) {
            if ($filter_data['start'] < 0) {
                $filter_data['start'] = 0;
            }

            if ($filter_data['limit'] < 1) {
                $filter_data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$filter_data['start'] . "," . (int)$filter_data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getProductsFilterByStatus($filter_data)
    {
        $sql = "select spv.sync_status, spv.item_id, p.* from  " . DB_PREFIX . "shopee_product p 
                left join " . DB_PREFIX . "shopee_product_version spv ON p.`item_id` = spv.`item_id` WHERE 1";

        if (!empty($filter_data['filter_name'])) {
            $sql .= " AND p.name LIKE '%" . $this->db->escape($filter_data['filter_name']) . "%'";
        }

        if (isset($filter_data['shop_id'])) {
            $sql .= " AND p.`shopid` = " . $this->db->escape($filter_data['shop_id']);
        }

        if (isset($filter_data['status_sync']) && $filter_data['status_sync'] == self::SYNC_STATUS_MAPPING_PRODUCT) {
            $sql .= " AND IF(p.`has_variation` = 0, p.`sync_status` = " . $filter_data['status_sync'] . ", spv.`sync_status` = " . $filter_data['status_sync'] .")";
        }

        if (isset($filter_data['status_sync']) && $filter_data['status_sync'] == self::SYNC_STATUS_NOT_MAPPING_PRODUCT) {
            $sql .= " AND IF(p.`has_variation` = 0,( p.`sync_status` = " . $filter_data['status_sync'] . " OR p.`sync_status` IS NULL), (spv.`sync_status` = " . $filter_data['status_sync'] ." OR spv.`sync_status` IS NULL))";
        }

        if (!empty($filter_data['status_product'])) {
            if ($filter_data['status_product'] == 2) {
                $sql .= " AND p.bestme_id IS NOT NULL";
            } else {
                $sql .= " AND p.bestme_id IS NULL";
            }
        }

        $sql .= " GROUP BY p.item_id";
        $sql .= " ORDER BY p.created_at DESC";

        if (isset($filter_data['start']) || isset($filter_data['limit'])) {
            if ($filter_data['start'] < 0) {
                $filter_data['start'] = 0;
            }

            if ($filter_data['limit'] < 1) {
                $filter_data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$filter_data['start'] . "," . (int)$filter_data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getProductVersionShopeeList($item_id, $sync_status)
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "shopee_product_version WHERE 1";
        $sql .= " AND `item_id` = " . $item_id;

        if (isset($sync_status) && $sync_status == self::SYNC_STATUS_MAPPING_PRODUCT) {
            $sql .= " AND `sync_status` = " . $this->db->escape($sync_status);
        }

        if (isset($sync_status) && $sync_status == self::SYNC_STATUS_NOT_MAPPING_PRODUCT) {
            $sql .= " AND (`sync_status` = " . $this->db->escape($sync_status) . " OR `sync_status` IS NULL)";
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getProductById($id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "shopee_product WHERE id = " . (int)$id);

        return $query->row;
    }

    public function getShopeeProductByItemId($item_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "shopee_product WHERE item_id = " . (int)$item_id);

        return $query->row;
    }

    public function getProductVersionById($id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "shopee_product_version WHERE id = " . (int)$id);

        return $query->row;
    }

    public function removeTable()
    {
        $this->db->query("DROP TABLE " . DB_PREFIX . "shopee_product");
    }

    public function getTotalProductsCustom($filter_data)
    {
        $sql = "SELECT COUNT(DISTINCT p.id) AS total from  " . DB_PREFIX . "shopee_product p WHERE 1";

        if (!empty($filter_data['filter_name'])) {
            $sql .= " AND p.name LIKE '%" . $this->db->escape($filter_data['filter_name']) . "%'";
        }

        if (isset($filter_data['shop_id'])) {
            $sql .= " AND p.`shopid` = " . $this->db->escape($filter_data['shop_id']);
        }

        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    public function getTotalProductsFilterByStatus($filter_data)
    {
        $sql = "SELECT COUNT(DISTINCT p.item_id) AS total, spv.item_id, spv.sync_status from  " . DB_PREFIX . "shopee_product p
                left join " . DB_PREFIX . "shopee_product_version spv ON p.`item_id` = spv.`item_id`WHERE 1";

        if (!empty($filter_data['filter_name'])) {
            $sql .= " AND p.name LIKE '%" . $this->db->escape($filter_data['filter_name']) . "%'";
        }

        if (isset($filter_data['shop_id'])) {
            $sql .= " AND p.`shopid` = " . $this->db->escape($filter_data['shop_id']);
        }

        if (isset($filter_data['status_sync']) && $filter_data['status_sync'] == self::SYNC_STATUS_MAPPING_PRODUCT) {
            $sql .= " AND IF(p.`has_variation` = 0, p.`sync_status` = " . $filter_data['status_sync'] . ", spv.`sync_status` = " . $filter_data['status_sync'] .")";
        }

        if (isset($filter_data['status_sync']) && $filter_data['status_sync'] == self::SYNC_STATUS_NOT_MAPPING_PRODUCT) {
            $sql .= " AND IF(p.`has_variation` = 0,( p.`sync_status` = " . $filter_data['status_sync'] . " OR p.`sync_status` IS NULL), (spv.`sync_status` = " . $filter_data['status_sync'] ." OR spv.`sync_status` IS NULL))";
        }

        if (!empty($filter_data['status_product'])) {
            if ($filter_data['status_product'] == 2) {
                $sql .= " AND p.bestme_id IS NOT NULL";
            } else {
                $sql .= " AND p.bestme_id IS NULL";
            }
        }

        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    public function getSkues()
    {
        $query = $this->db->query("SELECT sku, variations from  " . DB_PREFIX . "shopee_product");
        $skues = [];
        foreach ($query->rows as $item) {
            if ($item['sku'] != '') {
                $skues[] = $item['sku'];
            }
            $variations = json_decode($item['variations'], true);
            foreach ($variations as $variation) {
                if ($variation['variation_sku'] != '') {
                    $skues[] = $variation['variation_sku'];
                }
            }
        }

        return $skues;
    }

    //Get skues variable by item_id
    public function getShopeeVariationSkues($item_id)
    {
        $query = $this->db->query("SELECT variations FROM " . DB_PREFIX . "shopee_product WHERE item_id = " . (int)$item_id);
        $skues = [];
        $variations = json_decode($query->row['variations'], true);
        foreach ($variations as $variation) {
            if ($variation['variation_sku'] != '') {
                $skues[] = $variation['variation_sku'];
            }
        }
        return $skues;
    }

    // v2.6.1
    public function getOrderSyncList($data = [])
    {
        $DB_PREFIX = DB_PREFIX;

        $sql = "SELECT * FROM {$DB_PREFIX}shopee_order so WHERE 1";

        if (isset($data['code']) && $data['code']) {
            $sql .= " AND so.`code` LIKE '%" . $this->db->escape($data['code']) . "%'";
        }

        if (isset($data['shop_id']) && $data['shop_id']) {
            $sql .= " AND so.`shopee_shop_id` = " . $this->db->escape($data['shop_id']);
        }

        if (isset($data['status']) && $data['status'] != self::SYNC_STATUS_SYNC_ALL) {
            if ($data['status'] == self::SYNC_STATUS_SYNC_SUCCESS || $data['status'] == self::SYNC_STATUS_ORDER_NOT_YET_SYNC) {
                $sql .= " AND so.`sync_status` = " . $this->db->escape($data['status']);
            } else {
                $sql .= " AND so.`sync_status` NOT IN (1,6)";
            }

        }

        $sql .= " AND so.`sync_time` >= now()-interval 15 day";
        $sql .= " ORDER BY so.sync_time DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getTotalOrderSyncList($data = [])
    {
        $DB_PREFIX = DB_PREFIX;

        $sql = "SELECT COUNT(*) as total FROM {$DB_PREFIX}shopee_order so WHERE 1";

        if (isset($data['code']) && $data['code']) {
            $sql .= " AND so.`code` LIKE '%" . $this->db->escape($data['code']) . "%'";
        }

        if (isset($data['shop_id']) && $data['shop_id']) {
            $sql .= " AND so.`shopee_shop_id` = " . $this->db->escape($data['shop_id']);
        }

        if (isset($data['status']) && $data['status'] != self::SYNC_STATUS_SYNC_ALL) {
            if ($data['status'] == self::SYNC_STATUS_SYNC_SUCCESS || $data['status'] == self::SYNC_STATUS_ORDER_NOT_YET_SYNC) {
                $sql .= " AND so.`sync_status` = " . $this->db->escape($data['status']);
            } else {
                $sql .= " AND so.`sync_status` NOT IN (1,6)";
            }

        }
        $sql .= " AND so.`sync_time` >= now()-interval 15 day";

        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    public function getShopeeShopList()
    {
        $DB_PREFIX = DB_PREFIX;

        $sql = "SELECT * FROM {$DB_PREFIX}shopee_shop_config ssc 
                  WHERE ssc.`connected` = 1";

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function addOrEditShopeeShop($shop_id, $shop_info)
    {
        $shop = $this->checkExitsShopeeShop($shop_id);

        if (isset($shop->row['shop_id'])) {
            $this->db->query("UPDATE `" . DB_PREFIX . "shopee_shop_config` 
                                SET `shop_name` = '" . (string)$shop_info['shop_name'] . "', 
                                    `sync_interval` = " . $this->getDefaultSyncInterval() . ", 
                                    `connected` = '" . 1 . "' 
                                WHERE `shop_id` = '" . $shop_info['shop_id'] . "'");
        } else {
            $this->addShopeeShop($shop_id, $shop_info);
        }
    }

    public function addShopeeShop($shop_id, $shoinfo)
    {
        $shop = $this->checkExitsShopeeShop($shop_id);

        if (isset($shop->row['shop_id'])) {
            return;
        } else {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "shopee_shop_config` 
                                SET `shop_id` = '" . (int)$shoinfo['shop_id'] . "', 
                                    `shop_name` = '" . $this->db->escape($shoinfo['shop_name']) . "', 
                                    `sync_interval` = " . $this->getDefaultSyncInterval() . ", 
                                    `connected` = " . 1);
        }
    }

    public function shopIsConnected($shop_id)
    {
        $query = $this->db->query("SELECT shop_id FROM " . DB_PREFIX . "shopee_shop_config WHERE connected = 1 AND shop_id = " . (int)$shop_id);

        if ($query->num_rows > 0) {
            return true;
        }

        return false;
    }

    public function checkExitsShopeeShop($shop_id)
    {
        return $this->db->query("SELECT shop_id FROM " . DB_PREFIX . "shopee_shop_config WHERE shop_id = " . (int)$shop_id);
    }

    public function updateTimeSyncById($shop_id, $time)
    {
        $this->db->query("UPDATE `" . DB_PREFIX . "shopee_shop_config` 
                                SET `sync_interval` = '" . (int)$time . "'  
                                WHERE `shop_id` = '" . $shop_id . "'");
    }

    public function getTimeIntervalById($shop_id)
    {
        $query = $this->db->query("SELECT `sync_interval` FROM `" . DB_PREFIX . "shopee_shop_config` WHERE `shop_id` = " . (int)$shop_id);

        if (isset($query->row['sync_interval'])) {
            return (int)$query->row['sync_interval'];
        }

        return 0;
    }

    public function disconnectShopById($shop_id)
    {
        $this->db->query("UPDATE `" . DB_PREFIX . "shopee_shop_config` 
                                SET `connected` = '" . 0 . "'  
                                WHERE `shop_id` = '" . $shop_id . "'");
    }

    public function saveShopeeShopConfig($shop_id, $order_sync_range = 15, $allow_update_stock_product = 0, $allow_update_price_product = 0)
    {
        $DB_PREFIX = DB_PREFIX;
        $this->db->query("UPDATE `{$DB_PREFIX}shopee_shop_config` 
                                SET `order_sync_range` = {$order_sync_range}, 
                                    `allow_update_stock_product` = {$allow_update_stock_product}, 
                                    `allow_update_price_product` = {$allow_update_price_product} 
                                WHERE `shop_id` = {$shop_id}");
    }

    public function getLastSyncTimeOrderById($shop_id)
    {
        $query = $this->db->query("SELECT `last_sync_time_order` FROM `" . DB_PREFIX . "shopee_shop_config` WHERE `shop_id` = " . (int)$shop_id);

        if (isset($query->row['last_sync_time_order'])) {
            return $query->row['last_sync_time_order'];
        }

        return null;
    }

    public function getLastSyncTimeProductById($shop_id)
    {
        $query = $this->db->query("SELECT `last_sync_time_product` FROM `" . DB_PREFIX . "shopee_shop_config` WHERE `shop_id` = " . (int)$shop_id);

        if (isset($query->row['last_sync_time_product'])) {
            return $query->row['last_sync_time_product'];
        }

        return null;
    }

    public function getOrderCodesByShop($shop_id)
    {
        if (!$shop_id) {
            return [];
        }

        $query = $this->db->query("SELECT code FROM " . DB_PREFIX . "shopee_order WHERE shopee_shop_id = " . (int)$shop_id . " AND `sync_time` >= now()-interval 15 day");

        $codes = array_map(function ($row) {
            return $row['code'];
        }, $query->rows);

        return $codes;
    }

    public function getOrderCodeById($id)
    {
        $query = $this->db->query("SELECT code FROM " . DB_PREFIX . "shopee_order WHERE id = " . (int)$id);

        if (isset($query->row['code'])) {
            return $query->row['code'];
        }

        return false;
    }

    public function getShopeeOrderbyCode($code)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "shopee_order` WHERE `code` = '" . $this->db->escape($code) . "'");

        return $query->row;
    }

    public function getProductsForShopee($data = array())
    {
        $sql = "SELECT p.status as p_status, p.sku as psku, 
                       p.sale_on_out_of_stock as p_sale_on_out_of_stock, 
                       p.product_id as pid, p.multi_versions, 
                       p.price as o_price, p.compare_price as o_compare_price, 
                       p.weight as weight, p.quantity as o_quantity, dp.name, pv.* 
                FROM " . DB_PREFIX . "product p 
                LEFT JOIN " . DB_PREFIX . "product_description as dp ON (p.product_id = dp.product_id) 
                LEFT JOIN " . DB_PREFIX . "product_version as pv ON CASE WHEN p.multi_versions = 1 THEN p.product_id 
                                                                            ELSE -1
                                                                            END = pv.product_id 
                WHERE 1=1 and dp.language_id = " . (int)$this->config->get('config_language_id');
        if (!empty($data['filter_name'])) {
            $sql .= " AND dp.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['pv_ids'])) {
            $product_ids = [];
            $product_version_ids = [];
            foreach ($data['pv_ids'] as $pv_id){
                $ids = explode("-", $pv_id);
                if (!isset($ids[1])) {
                    $product_ids[] = $ids[0];
                    continue;
                }

                $product_version_ids[] = $ids[1];
            }
            $product_ids = implode(',', $product_ids);
            $product_version_ids = implode(',', $product_version_ids);

            if ($product_ids) {
                $sql .= " AND p.product_id NOT IN (" . $product_ids . ")";
            }
            if ($product_version_ids) {
                $sql .= " AND (pv.product_version_id is NULL OR pv.product_version_id NOT IN (" . $product_version_ids . "))";
            }
        }

        $sql .= " AND ( p.`deleted` IS NULL)";
        $sql .= " AND (pv.`deleted` IS NULL)";
        $sql .= " ORDER BY dp.name ASC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);
        $result = array();
        foreach ($query->rows as $k => $item) {
            $product = array(
                'product_id' => $item['pid'],
                'name' => strlen($item['name']) > 90 ? substr($item['name'], 0, 90) . "..." : $item['name'],
                'product_version_id' => $item['product_version_id'],
                'product_name' => $item['name'],
                'weight' => $item['weight'],
                'version' => $item['version'],
                'sale_on_out_of_stock' => $item['p_sale_on_out_of_stock'],
                'multi_versions' => $item['multi_versions'],
                'edit' => $this->url->link('catalog/product/edit', 'user_token=' . $this->session->data['user_token'] . '&product_id=' . $item['pid'], true),
            );

            if ($item['product_version_id'] != null) {
                $product['id'] = $item['pid'] . '-' . $item['product_version_id'];
                $product['quantity'] = $this->getQuantityProductAndVersion($item['pid'], $item['product_version_id']);
                $product['price'] = $item['compare_price'];
                $product['status'] = $item['status'];
                $product['p_status'] = $item['p_status'];
                $product['sku'] = $item['sku'];
                $product['name'] = $item['name'] .' : '. $item['version'];
            } else {
                $product['id'] = $item['pid'];
                $product['quantity'] = $this->getQuantityProductAndVersion($item['pid']);
                $product['price'] = $item['o_compare_price'];
                $product['status'] = $item['p_status'];
                $product['p_status'] = $item['p_status'];
                $product['sku'] = $item['psku'];
            }

            $result[$product['id']] = $product;
            unset($product);
        }

        return $result;
    }

    public function countProductAllContainDraft($data = array())
    {
        $language_id = $this->config->get('config_language_id');
        $sql = "SELECT p.status as p_status, p.sku as psku, 
                       p.sale_on_out_of_stock as p_sale_on_out_of_stock, 
                       p.product_id as pid, p.multi_versions, 
                       p.price as o_price, p.compare_price as o_compare_price, 
                       p.weight as weight, p.quantity as o_quantity, dp.name, pv.* 
                FROM " . DB_PREFIX . "product p 
                LEFT JOIN " . DB_PREFIX . "product_description as dp ON (p.product_id = dp.product_id) 
                LEFT JOIN " . DB_PREFIX . "product_version as pv ON CASE WHEN p.multi_versions = 1 THEN p.product_id 
                                                                            ELSE -1
                                                                            END = pv.product_id 
                WHERE 1=1 and dp.language_id = " . (int)$this->config->get('config_language_id');
        if (!empty($data['filter_name'])) {
            $sql .= " AND dp.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['pv_ids'])) {
            $product_ids = [];
            $product_version_ids = [];
            foreach ($data['pv_ids'] as $pv_id){
                $ids = explode("-", $pv_id);
                if (!isset($ids[1])) {
                    $product_ids[] = $ids[0];
                    continue;
                }

                $product_version_ids[] = $ids[1];
            }
            $product_ids = implode(',', $product_ids);
            $product_version_ids = implode(',', $product_version_ids);

            if ($product_ids) {
                $sql .= " AND p.product_id NOT IN (" . $product_ids . ")";
            }
            if ($product_version_ids) {
                $sql .= " AND (pv.product_version_id is NULL OR pv.product_version_id NOT IN (" . $product_version_ids . "))";
            }
        }
        $sql .= " AND ( p.`deleted` IS NULL)";
        $sql .= " AND (pv.`deleted` IS NULL)";
        $sql .= " ORDER BY dp.name ASC";
        $query = $this->db->query($sql);

        return count($query->rows);
    }

    public function getProductVersionMapping($shop_id)
    {
        $sql = "SELECT spv.`bestme_product_id`, spv.`bestme_product_version_id`, spv.item_id, sp.`item_id`, sp.`shopid` FROM " . DB_PREFIX . "shopee_product sp 
                LEFT JOIN " . DB_PREFIX . "shopee_product_version spv ON sp.`item_id` = spv.`item_id` 
                WHERE spv.`bestme_product_id` IS NOT NULL AND sp.`shopid` = {$shop_id}";

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getProductMapping($shop_id)
    {
        $sql = "SELECT `bestme_id` as `bestme_product_id`, `bestme_product_version_id` FROM " . DB_PREFIX . "shopee_product WHERE `bestme_id` IS NOT NULL AND `shopid` = {$shop_id}";

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getLinkProductInfoById($product_id, $product_version_id = 0)
    {
        $sql = "SELECT p.product_id as pid, 
                       p.price as o_price, p.compare_price as o_compare_price, 
                       dp.name, pv.* 
                FROM " . DB_PREFIX . "product p 
                LEFT JOIN " . DB_PREFIX . "product_description as dp ON (p.product_id = dp.product_id) 
                LEFT JOIN " . DB_PREFIX . "product_version as pv ON CASE WHEN p.multi_versions = 1 THEN p.product_id 
                                                                            ELSE -1
                                                                            END = pv.product_id 
                WHERE 1=1 and dp.language_id = " . (int)$this->config->get('config_language_id');
        $sql .= " AND p.product_id = ".$product_id;

        if ($product_version_id) {
            $sql .= " AND pv.product_version_id = ".$product_version_id;
        }

        $query = $this->db->query($sql);

        $product = array(
            'product_id' => $query->row['pid'],
            'name' => strlen($query->row['name']) > 90 ? substr($query->row['name'], 0, 90) . "..." : $query->row['name'],
            'edit' => $this->url->link('catalog/product/edit', 'user_token=' . $this->session->data['user_token'] . '&product_id=' . $query->row['pid'], true),
        );

        if ($query->row['product_version_id'] != null) {
            $product['id'] = $query->row['pid'] . '-' . $query->row['product_version_id'];
            $product['quantity'] = $this->getQuantityProductAndVersion($query->row['pid'], $query->row['product_version_id']);
            $product['price'] = $query->row['compare_price'];
            $product['name'] = $query->row['name'] .' : '. $query->row['version'];
        } else {
            $product['id'] = $query->row['pid'];
            $product['quantity'] = $this->getQuantityProductAndVersion($query->row['pid']);
            $product['price'] = $query->row['o_compare_price'];
        }

        return $product;
    }

    public function mappingProductBestme($id, $product_id, $product_version_id)
    {
        if (strpos($id, '_product') !== false) {
            $this->db->query("UPDATE `" . DB_PREFIX . "shopee_product` 
                                SET `bestme_id` = '" . $product_id . "', 
                                    `bestme_product_version_id` = " . $product_version_id . ", 
                                    `sync_status` = " . 1 . " 
                                WHERE `id` = '" . $id . "'");
        } else {
            $this->db->query("UPDATE `" . DB_PREFIX . "shopee_product_version` 
                                SET `bestme_product_id` = '" . $product_id . "', 
                                    `bestme_product_version_id` = " . $product_version_id . ", 
                                    `sync_status` = " . 1 . " 
                                WHERE `id` = '" . $id . "'");

            $version = $this->db->query("SELECT * FROM `" . DB_PREFIX . "shopee_product_version` WHERE `id` = ".$id);

            if (isset($version->row['item_id'])) {
                $this->db->query("UPDATE `" . DB_PREFIX . "shopee_product` 
                                SET `sync_status` = '" . 1 . "'  
                                WHERE `item_id` = '" . $version->row['item_id'] . "'");
            }
        }
    }

    public function cancelMappingVersion($id, $item_id)
    {
        if (strpos($id, '_product') !== false) {
            $this->db->query("UPDATE `" . DB_PREFIX . "shopee_product` 
                                    SET `sync_status` = '" . 0 . "', 
                                        `bestme_id` = NULL, 
                                        `bestme_product_version_id` = NULL 
                                    WHERE `item_id` = '" . $item_id . "'");
        } else {
            $has_mapping = false;
            $this->db->query("UPDATE `" . DB_PREFIX . "shopee_product_version` 
                                    SET `bestme_product_id` = NULL, 
                                        `bestme_product_version_id` = NULL, 
                                        `sync_status` = " . 0 . " 
                                    WHERE `id` = '" . $id . "'");

            $products_version = $this->db->query("SELECT * FROM `" . DB_PREFIX . "shopee_product_version` WHERE `item_id` = '". $item_id ."'");

            foreach ($products_version->rows as $product) {
                if (isset($product['sync_status']) && $product['sync_status']) {
                    $has_mapping = true;
                }
            }

            if (!$has_mapping) {
                $this->db->query("UPDATE `" . DB_PREFIX . "shopee_product` 
                                    SET `sync_status` = '" . 0 . "', 
                                        `bestme_id` = NULL, 
                                        `bestme_product_version_id` = NULL 
                                    WHERE `item_id` = '" . $item_id . "'");
            }
        }
    }

    public function cancelAllMapping($item_id)
    {
        $this->db->query("UPDATE `" . DB_PREFIX . "shopee_product` 
                                    SET `sync_status` = '" . 0 . "', 
                                        `bestme_id` = NULL, 
                                        `bestme_product_version_id` = NULL 
                                    WHERE `item_id` = '" . $item_id . "'");

        $this->db->query("UPDATE `" . DB_PREFIX . "shopee_product_version` 
                                SET `bestme_product_id` = NULL, 
                                    `bestme_product_version_id` = NULL, 
                                    `sync_status` = " . 0 . " 
                                WHERE `item_id` = '" . $item_id . "'");
    }

    public function getQuantityProductAndVersion($product_id, $product_version_id = 0)
    {
        $this->load->model('catalog/product');
        $p_t_stores = $this->model_catalog_product->getProductToStores($product_id, $product_version_id);
        $quantity_total = 0;
        foreach ($p_t_stores as $key => &$item){
            if ($item['product_version_id'] == $product_version_id){
                $quantity_total += $item['quantity'];
            }
        }

        return $quantity_total;
    }

    public function disconnectWhenProductChange($old_product, $new_product, $old_product_version, $new_product_version)
    {
        if (!isset($old_product['multi_versions']) ||
            !isset($new_product['multi_versions'])) {
            return;
        }

        $product_id = isset($old_product['product_id']) ? $old_product['product_id'] : '';

        if ($product_id === '') {
            return;
        }

        $products_shopee = $this->db->query("SELECT * FROM `" . DB_PREFIX . "shopee_product` WHERE `bestme_id` = " . $product_id);
        if (!isset($products_shopee->row['item_id'])) {
            return;
        }

        if ($old_product['multi_versions'] != $new_product['multi_versions']) {
            if (isset($products_shopee->row['has_variation']) && $products_shopee->row['has_variation'] == 0) {
                $this->db->query("UPDATE `" . DB_PREFIX . "shopee_product` 
                                    SET `sync_status` = '" . 0 . "', 
                                        `bestme_id` = NULL, 
                                        `bestme_product_version_id` = NULL 
                                    WHERE `item_id` = '" . $products_shopee->row['item_id'] . "'");
            } else {
                $products_version_shopee = $this->db->query("SELECT * FROM `" . DB_PREFIX . "shopee_product_version` WHERE `bestme_product_id` = " . $product_id);

                foreach ($products_version_shopee->rows as $product_version) {
                    $has_mapping = false;
                    // B1: disconnect all product version has connect with $old_product['product_id']
                    $this->db->query("UPDATE `" . DB_PREFIX . "shopee_product_version` 
                                SET `bestme_product_id` = NULL, 
                                    `bestme_product_version_id` = NULL, 
                                    `sync_status` = " . 0 . " 
                                WHERE `id` = '" . $product_version['id'] . "'");

                    // B2: checking item_id of product disconnect has connect
                    $products_version_checking = $this->db->query("SELECT * FROM `" . DB_PREFIX . "shopee_product_version` WHERE `item_id` = '" . $product_version['item_id'] ."'");

                    foreach ($products_version_checking->rows as $product) {
                        if (isset($product['sync_status']) && $product['sync_status']) {
                            $has_mapping = true;
                        }
                    }

                    if (!$has_mapping) {
                        $this->db->query("UPDATE `" . DB_PREFIX . "shopee_product` 
                                    SET `sync_status` = '" . 0 . "', 
                                        `bestme_id` = NULL, 
                                        `bestme_product_version_id` = NULL 
                                    WHERE `item_id` = '" . $product_version['item_id'] . "'");
                    }
                }
            }
        } else {
            if ($old_product['multi_versions'] == 0 && $new_product['multi_versions'] == 0) {
                return;
            }

            if (!isset($old_product_version) || !isset($new_product_version)) {
                return;
            }

            $arr_disconnect = [];
            $arr_pv_new = [];

            foreach ($new_product_version as $pv_new) {
                if (!isset($pv_new['product_version_id'])) {
                    continue;
                }

                $arr_pv_new[] = $pv_new['product_version_id'];
            }

            foreach ($old_product_version as $pv_old) {
                if (!isset($pv_old['product_version_id'])) {
                    continue;
                }

                if (!in_array($pv_old['product_version_id'], $arr_pv_new)) {
                    $arr_disconnect[] = $pv_old['product_version_id'];
                }
            }

            if (empty($arr_disconnect)) {
                return;
            }

            foreach ($arr_disconnect as $item) {
                $this->db->query("UPDATE `" . DB_PREFIX . "shopee_product_version` 
                                SET `bestme_product_id` = NULL, 
                                    `bestme_product_version_id` = NULL, 
                                    `sync_status` = " . 0 . " 
                                WHERE `bestme_product_version_id` = '" . (int)$item . "'");
            }

            $products_version_shopee = $this->db->query("SELECT * FROM `" . DB_PREFIX . "shopee_product_version` WHERE `bestme_product_id` = " . $product_id);

            if (isset($products_version_shopee->rows) && is_array($products_version_shopee->rows) && count($products_version_shopee->rows)) {
                foreach ($products_version_shopee->rows as $product_version) {
                    $has_mapping = false;
                    $products_version_checking = $this->db->query("SELECT * FROM `" . DB_PREFIX . "shopee_product_version` WHERE `item_id` = '" . $product_version['item_id'] . "'");

                    foreach ($products_version_checking->rows as $product) {
                        if (isset($product['sync_status']) && $product['sync_status']) {
                            $has_mapping = true;
                        }
                    }

                    if (!$has_mapping) {
                        $this->db->query("UPDATE `" . DB_PREFIX . "shopee_product` 
                                    SET `sync_status` = '" . 0 . "', 
                                        `bestme_id` = NULL, 
                                        `bestme_product_version_id` = NULL 
                                    WHERE `item_id` = '" . $product_version['item_id'] . "'");
                    }
                }
            } else {
                $this->db->query("UPDATE `" . DB_PREFIX . "shopee_product` 
                                    SET `sync_status` = '" . 0 . "', 
                                        `bestme_id` = NULL, 
                                        `bestme_product_version_id` = NULL 
                                    WHERE `bestme_id` = '" . $product_id . "'");
            }
        }
    }

    public function disconnectWhenProductDeleted($product_id)
    {
        if (!isset($product_id)) {
            return;
        }

        $products_shopee = $this->db->query("SELECT * FROM `" . DB_PREFIX . "shopee_product` WHERE `bestme_id` = " . $product_id);

        foreach ($products_shopee->rows as $p_shopee) {
            if (isset($p_shopee['has_variation']) && $p_shopee['has_variation'] == 0) {
                $this->db->query("UPDATE `" . DB_PREFIX . "shopee_product` 
                                    SET `sync_status` = '" . 0 . "', 
                                        `bestme_id` = NULL, 
                                        `bestme_product_version_id` = NULL 
                                    WHERE `item_id` = '" . $p_shopee['item_id'] . "'");
            }
        }

        $products_version_shopee = $this->db->query("SELECT * FROM `" . DB_PREFIX . "shopee_product_version` WHERE `bestme_product_id` = " . $product_id);

        foreach ($products_version_shopee->rows as $product_version) {
            $has_mapping = false;
            // B1: disconnect all product version has connect with $old_product['product_id']
            $this->db->query("UPDATE `" . DB_PREFIX . "shopee_product_version` 
                        SET `bestme_product_id` = NULL, 
                            `bestme_product_version_id` = NULL, 
                            `sync_status` = " . 0 . " 
                        WHERE `id` = '" . $product_version['id'] . "'");

            // B2: checking item_id of product disconnect has connect
            $products_version_checking = $this->db->query("SELECT * FROM `" . DB_PREFIX . "shopee_product_version` WHERE `item_id` = '" . $product_version['item_id'] . "'");

            foreach ($products_version_checking->rows as $product) {
                if (isset($product['sync_status']) && $product['sync_status']) {
                    $has_mapping = true;
                }
            }

            if (!$has_mapping) {
                $this->db->query("UPDATE `" . DB_PREFIX . "shopee_product` 
                            SET `sync_status` = '" . 0 . "', 
                                `bestme_id` = NULL, 
                                `bestme_product_version_id` = NULL 
                            WHERE `item_id` = '" . $product_version['item_id'] . "'");
            }
        }
    }

    // Sync order from temporary table to Admin
    public function syncOrderToAdmin($order_code)
    {
        try {
            $shopee_order = $this->getShopeeOrderbyCode($order_code);
            if (empty($shopee_order)) {
                return false;
            }
            $is_edited = isset($shopee_order['is_edited']) ? (int)$shopee_order['is_edited'] : 0;
            if ($is_edited) {
                $sync_status = self::SYNC_STATUS_ERROR_ORDER_IS_EDITED;
                $this->db->query("UPDATE `" . DB_PREFIX . "shopee_order` SET `sync_status` = " . (int)$sync_status . " WHERE `code` = '" . $this->db->escape($order_code) . "'");
                return false;
            }
            $sync_type = 'add';
            if (isset($shopee_order['bestme_order_id']) && $shopee_order['bestme_order_id']) {
                $sync_type = 'update';
            }

            // product in order
            $shopee_order_product = $this->db->query("SELECT * FROM `" . DB_PREFIX . "shopee_order_to_product` WHERE `shopee_order_code` = '" . $this->db->escape($order_code) . "'");
            $admin_order_product = [];
            $sync_status = self::SYNC_STATUS_SYNC_SUCCESS;
            foreach ($shopee_order_product->rows as $row) {
                if (!isset($row['item_id']) || !isset($row['quantity'])) {
                    continue;
                }
                $product = $this->mapProductInOrderToAdmin($row);
                if (!$product) {
                    $sync_status = self::SYNC_STATUS_ERROR_PRODUCT_ORDER_NOT_SYNC;
                    break;
                }

                $admin_order_product[] = $product;
            }
            if ($sync_status != self::SYNC_STATUS_SYNC_SUCCESS) {
                $this->db->query("UPDATE `" . DB_PREFIX . "shopee_order` SET `sync_status` = " . (int)$sync_status . " WHERE `code` = '" . $this->db->escape($order_code) . "'");
                return false;
            } else {
                // check quantity
                if ($sync_type == 'add') {
                    foreach ($admin_order_product as $product) {
                        if (!isset($product['product_id']) || !isset($product['product_version_id']) || !isset($product['quantity']) || !isset($product['price']) || !isset($product['default_store_id']) || !isset($product['sale_on_out_of_stock'])) {
                            continue;
                        }

                        if (!$product['sale_on_out_of_stock'] && !$this->checkQuantity($product['product_id'], $product['product_version_id'], $product['default_store_id'], $product['quantity'])) {
                            $sync_status = self::SYNC_STATUS_ERROR_PRODUCT_OUT_OF_STOCK_ON_ADMIN;
                            break;
                        }
                    }

                    if ($sync_status != self::SYNC_STATUS_SYNC_SUCCESS) {
                        $this->db->query("UPDATE `" . DB_PREFIX . "shopee_order` SET `sync_status` = " . (int)$sync_status . " WHERE `code` = '" . $this->db->escape($order_code) . "'");
                        return false;
                    }
                }
                $result = $this->saveToAdmin($order_code, $admin_order_product);
                if (!$result) {
                    $sync_status = self::SYNC_STATUS_ORDER_NOT_YET_SYNC;
                }
                $this->db->query("UPDATE `" . DB_PREFIX . "shopee_order` SET `sync_status` = " . (int)$sync_status . " WHERE `code` = '" . $this->db->escape($order_code) . "'");

                return $result;
            }
        } catch (Exception $e) {
            // do something ...
        }
    }

    // v2.11.2
    function allowUpdateInventory($shopee_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $shop = $this->db->query("SELECT `allow_update_stock_product` FROM `{$DB_PREFIX}shopee_shop_config` WHERE `shop_id` = {$shopee_id}");

        if (isset($shop->row['allow_update_stock_product']) && $shop->row['allow_update_stock_product']) {
            return true;
        }

        return false;
    }

    function allowUpdatePrice($shopee_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $shop = $this->db->query("SELECT `allow_update_price_product` FROM `{$DB_PREFIX}shopee_shop_config` WHERE `shop_id` = {$shopee_id}");

        if (isset($shop->row['allow_update_price_product']) && $shop->row['allow_update_price_product']) {
            return true;
        }

        return false;
    }

    function updateProductHasLinkProductShopee($shopee_id, $product_bestme_id, $product_bestme_version_id, $stock, $price, $original_price)
    {
        try {
            $db_prefix = DB_PREFIX;
            $product = $this->db->query("SELECT `default_store_id` FROM `{$db_prefix}product` WHERE `product_id` = {$product_bestme_id}");
            $default_store_id = isset($product->row['default_store_id']) ? $product->row['default_store_id'] : 0;
            if ($original_price == $price) {
                $price = 0;
            }

            if ($product_bestme_version_id) {
                if ($this->allowUpdateInventory($shopee_id)) {
                    $this->db->query("UPDATE `{$db_prefix}product_to_store` SET `quantity` = {$stock} 
                                      WHERE `product_id` = {$product_bestme_id} 
                                        AND product_version_id = {$product_bestme_version_id} 
                                        AND `store_id` = {$default_store_id}");
                }

                if ($this->allowUpdatePrice($shopee_id)) {
                    $this->db->query("UPDATE `{$db_prefix}product_version` 
                                      SET `price` = {$price}, 
                                          `compare_price` = {$original_price} 
                                        WHERE `product_id` = {$product_bestme_id} 
                                          AND `product_version_id` = {$product_bestme_version_id}");
                }
            } else {
                if ($this->allowUpdateInventory($shopee_id)) {
                    $this->db->query("UPDATE `{$db_prefix}product_to_store` 
                                      SET `quantity` = {$stock} 
                                        WHERE `product_id` = {$product_bestme_id} 
                                          AND product_version_id = 0 
                                          AND `store_id` = {$default_store_id}");
                }

                if ($this->allowUpdatePrice($shopee_id)) {
                    $this->db->query("UPDATE `{$db_prefix}product` 
                                      SET `price` = {$price}, 
                                          `compare_price` = {$original_price} 
                                        WHERE `product_id` = {$product_bestme_id}");
                }
            }
        } catch (Exception $e) {
            // do something
        }
    }
    // end v2.11.2

    private function mapProductInOrderToAdmin($item)
    {
        $product = [
            'price' => $item['original_price'],
            'discount' => (float)$item['original_price'] - (float)$item['price'],
            'quantity' => $item['quantity']
        ];

        if ($item['variation_id']) {
            $product_version_shopee = $this->db->query("SELECT * FROM `" . DB_PREFIX . "shopee_product_version` WHERE `variation_id` = '" . $item['variation_id'] . "'");
            if ($product_version_shopee->num_rows == 0) {
                return false;
            }

            $product_version_shopee = $product_version_shopee->row;
            if (!$product_version_shopee['item_id'] || !$product_version_shopee['bestme_product_id']) {
                return false;
            }

            $product_admin = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product` WHERE `product_id` = '" . $product_version_shopee['bestme_product_id'] . "' AND `deleted` IS NULL");
            if ($product_admin->num_rows == 0) {
                return false;
            }

            $product['default_store_id'] = isset($product_admin->row['default_store_id']) ? (int)$product_admin->row['default_store_id'] : 0;
            $product['product_id'] = $product_version_shopee['bestme_product_id'];
            $product['sale_on_out_of_stock'] = isset($product_admin->row['sale_on_out_of_stock']) ? (int)$product_admin->row['sale_on_out_of_stock'] : 0;
            $product['product_version_id'] = 0;

            if ($product_version_shopee['bestme_product_version_id']) {
                $product_version_admin = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_version` 
                                                                WHERE `product_id` = '" . $product_version_shopee['bestme_product_id'] . "' 
                                                                AND `product_version_id` = '" . $product_version_shopee['bestme_product_version_id'] . "' 
                                                                AND `deleted` IS NULL");
                if ($product_version_admin->num_rows == 0) {
                    return false;
                }

                $arr_variation_name = array_map('trim', explode(',', $item['variation_name']));
                $product_version_id = 0;
                foreach ($product_version_admin->rows as $row) {
                    $arr_row_name = array_map('trim', explode(',', $row['version']));
                    if (count($arr_variation_name) == count($arr_row_name) && empty(array_diff($arr_variation_name, $arr_row_name)) && empty(array_diff($arr_row_name, $arr_variation_name))) {
                        $product_version_id = $row['product_version_id'];
                        break;
                    }
                }
                if ($product_version_id == 0) {
                    return false;
                }
                $product['product_version_id'] = $product_version_id;
            }

        } else {
            $product_shopee = $this->db->query("SELECT * FROM `" . DB_PREFIX . "shopee_product` WHERE `item_id` = '" . $item['item_id'] . "'");
            if ($product_shopee->num_rows == 0) {
                return false;
            }

            $product_shopee = $product_shopee->row;
            if (!$product_shopee['bestme_id']) {
                return false;
            }

            $product_admin = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product` WHERE `product_id` = '" . $product_shopee['bestme_id'] . "' AND `deleted` IS NULL");
            if ($product_admin->num_rows == 0) {
                return false;
            }
            $product['default_store_id'] = isset($product_admin->row['default_store_id']) ? (int)$product_admin->row['default_store_id'] : 0;
            $product['product_id'] = $product_shopee['bestme_id'];
            $product['sale_on_out_of_stock'] = isset($product_admin->row['sale_on_out_of_stock']) ? (int)$product_admin->row['sale_on_out_of_stock'] : 0;
            $product['product_version_id'] = 0;

            if ($product_shopee['bestme_product_version_id']) {
                $product_version_admin = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_version` 
                                                                WHERE `product_id` = '" . $product_shopee['bestme_id'] . "' 
                                                                AND `product_version_id` = '" . $product_shopee['bestme_product_version_id'] . "' 
                                                                AND `deleted` IS NULL");
                if ($product_version_admin->num_rows == 0) {
                    return false;
                }

                $arr_variation_name = array_map('trim', explode(',', $item['variation_name']));
                $product_version_id = 0;
                foreach ($product_version_admin->rows as $row) {
                    $arr_row_name = array_map('trim', explode(',', $row['version']));
                    if (count($arr_variation_name) == count($arr_row_name) && empty(array_diff($arr_variation_name, $arr_row_name)) && empty(array_diff($arr_row_name, $arr_variation_name))) {
                        $product_version_id = $row['product_version_id'];
                        break;
                    }
                }
                if ($product_version_id == 0) {
                    return false;
                }
                $product['product_version_id'] = $product_version_id;
            }
        }

        return $product;
    }

    private function checkQuantity($product_id, $product_version_id, $store_id, $quantity)
    {
        $product_in_store = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_to_store` WHERE 
                                                                                `product_id` = " . (int)$product_id . " 
                                                                                AND `product_version_id` = " . (int)$product_version_id . "  
                                                                                AND `store_id` = " . (int)$store_id . "   
                                                                                AND `quantity` >= " . (int)$quantity);

        if ($product_in_store->num_rows > 0) {
            return true;
        }

        return false;
    }

    private function saveToAdmin($order_code, $products)
    {
        $shopee_order_info = $this->db->query("SELECT * FROM `" . DB_PREFIX . "shopee_order` WHERE `code` = '" . $this->db->escape($order_code) . "'");
        if ($shopee_order_info->num_rows == 0) {
            return false;
        }
        $shopee_order_info = $shopee_order_info->row;

        $map_order_satus = [
            'UNPAID' => 7,
            'TO_CONFIRM_RECEIVE' => 8,
            'READY_TO_SHIP' => 8,
            'SHIPPED' => 9,
            'RETRY_SHIP' => 8,
            'COMPLETED' => 9,
            'IN_CANCEL' => 10,
            'CANCELLED' => 10,
            'TO_RETURN' => 10
        ];
        $order_status = isset($map_order_satus[$shopee_order_info['order_status']]) ? (int)$map_order_satus[$shopee_order_info['order_status']] : 0;
        $payment_status = isset($shopee_order_info['payment_status']) ? (int)$shopee_order_info['payment_status'] : 0;
        $province = $this->getAdministrativeCodeByTextValue($shopee_order_info['state']);
        $district = $province ? $this->getAdministrativeCodeByTextValue($shopee_order_info['city'], $province) : '';
        $ward = $district ? $this->getAdministrativeCodeByTextValue($shopee_order_info['district'], $district) : '';
        $customer_id = $this->getCustomerByTelephone(['phone' => $shopee_order_info['phone'], 'fullname' => $shopee_order_info['fullname'], 'province' => $province, 'district' => $district, 'ward' => $ward, 'full_address' => $shopee_order_info['full_address']]);

        $customer_info = json_encode(['id' => $customer_id, 'name' => $shopee_order_info['fullname']]);

        if ($order_status == 10) {
            if (isset($shopee_order_info['bestme_order_id']) && $shopee_order_info['bestme_order_id'] > 0) {
                $this->autoRemoveReceiptVoucher($shopee_order_info['bestme_order_id']);
            }
        } else {
            $this->autoUpdateReceiptVoucher($shopee_order_info, $customer_info);
        }

        if ($order_status == 9) {
            $this->trackingShopeeOrderCompleted();
        }

        if (isset($shopee_order_info['bestme_order_id']) && $shopee_order_info['bestme_order_id'] > 0) {
            $old_order_status_id = $this->db->query("SELECT `order_status_id` FROM `" . DB_PREFIX . "order` WHERE `order_id` = " . (int)$shopee_order_info['bestme_order_id']);
            $old_order_status_id = isset($old_order_status_id->row['order_status_id']) ? $old_order_status_id->row['order_status_id'] : 0;

            $this->db->query("UPDATE `" . DB_PREFIX . "order` 
                                    SET `fullname` = '" . $this->db->escape($shopee_order_info['fullname']) . "', 
                                    `shipping_province_code` = '" . $province . "', 
                                    `shipping_district_code` = '" . $district . "', 
                                    `shipping_ward_code` = '" . $ward . "', 
                                    `shipping_address_1` = '" . $this->db->escape($shopee_order_info['full_address']) . "', 
                                    `telephone` = '" . $this->db->escape($shopee_order_info['phone']) . "', 
                                    `shipping_method` = '" . $this->db->escape($shopee_order_info['shipping_method']) . "', 
                                    `shipping_method_value` = '-2', 
                                    `shipping_fee` = '" . (float)$shopee_order_info['actual_shipping_cost'] . "', 
                                    `order_status_id` = '" . $order_status . "', 
                                    `previous_order_status_id` = '" . $old_order_status_id . "', 
                                    `order_code` = '" . $this->db->escape($shopee_order_info['code']) . "', 
                                    `payment_method` = '" . $this->db->escape($shopee_order_info['payment_method']) . "', 
                                    `discount` = '', 
                                    `total` = '" . (float)$shopee_order_info['total_amount'] . "',
                                    `comment` = '" . $this->db->escape($shopee_order_info['note_for_shop']) . "',
                                    `user_id` = 1,
                                    `customer_id` = '" . (int)$customer_id . "',
                                    `date_modified` = '" . $shopee_order_info['update_time'] . "',
                                    `payment_status` = '" . (int)$payment_status . "',
                                    `language_id` = 2 WHERE `order_id` = '" . (int)$shopee_order_info['bestme_order_id'] . "'");

            // update quantity product
            if (in_array($old_order_status_id, [7, 8, 9]) && $order_status == 10) {
                foreach ($products as $product) {
                    $this->updateQuantity($product['product_id'], $product['product_version_id'], $product['quantity'], $product['default_store_id'], '+');
                }

            }
            // update to report
            $this->db->query("UPDATE `" . DB_PREFIX . "report_order` SET 
                                                            `customer_id` = " . (int)$customer_id . ", 
                                                            `total_amount` = " . (float)$shopee_order_info['total_amount'] . ", 
                                                            `order_status` = " . (int)$order_status . ",
                                                            `shipping_fee` = '" . (float)$shopee_order_info['actual_shipping_cost'] . "',
                                                            `report_date` = '" . $shopee_order_info['update_time'] . "'
                                                            WHERE `order_id` = " . (int)$shopee_order_info['bestme_order_id']);
        } else {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "order` 
                                    SET `fullname` = '" . $this->db->escape($shopee_order_info['fullname']) . "', 
                                    `shipping_province_code` = '" . $province . "', 
                                    `shipping_district_code` = '" . $district . "', 
                                    `shipping_ward_code` = '" . $ward . "', 
                                    `shipping_address_1` = '" . $this->db->escape($shopee_order_info['full_address']) . "', 
                                    `telephone` = '" . $this->db->escape($shopee_order_info['phone']) . "', 
                                    `shipping_method` = '" . $this->db->escape($shopee_order_info['shipping_method']) . "', 
                                    `shipping_method_value` = '-2', 
                                    `shipping_fee` = '" . (float)$shopee_order_info['actual_shipping_cost'] . "', 
                                    `order_status_id` = '" . $order_status . "', 
                                    `order_code` = '" . $this->db->escape($shopee_order_info['code']) . "', 
                                    `payment_method` = '" . $this->db->escape($shopee_order_info['payment_method']) . "', 
                                    `discount` = '', 
                                    `total` = '" . (float)$shopee_order_info['total_amount'] . "',
                                    `comment` = '" . $this->db->escape($shopee_order_info['note_for_shop']) . "',
                                    `source` = 'shopee',
                                    `user_id` = 1,
                                    `customer_id` = '" . (int)$customer_id . "',
                                    `date_added` = '" . $shopee_order_info['create_time'] . "',
                                    `date_modified` = '" . $shopee_order_info['update_time'] . "',
                                    `payment_status` = '" . (int)$payment_status . "',
                                    `language_id` = 2");

            $bestme_order_id = $this->db->getLastId();

            $this->trackingTotalAmountShopee((float)$shopee_order_info['total_amount']);

            $this->db->query("UPDATE `" . DB_PREFIX . "shopee_order` SET `bestme_order_id` = " . (int)$bestme_order_id . " WHERE `code` = '" . $this->db->escape($order_code) . "'");
            // insert order_product
            $total_discount = 0;
            foreach ($products as $product) {
                $discount = isset($product['discount']) ? $product['discount'] : 0;
                $total_discount += $discount;

                $this->db->query("INSERT INTO `" . DB_PREFIX . "order_product` 
                                                      SET `order_id` = " . (int)$bestme_order_id . ", 
                                                      `product_id` = " . (int)$product['product_id'] . ", 
                                                      `product_version_id` = " . (int)$product['product_version_id'] . ", 
                                                      `quantity` = " . (int)$product['quantity'] . ", 
                                                      `price` = " . (float)$product['price'] . ", 
                                                      `discount` = " . (float)$discount . ", 
                                                      `total` = " . (float)$product['price'] * (int)$product['quantity']);

                // update quantity
                $this->updateQuantity($product['product_id'], $product['product_version_id'], $product['quantity'], $product['default_store_id']);
            }
            // insert to report
            $report_time = $shopee_order_info['update_time'];
            $date = new DateTime($report_time);
            $report_date = $date->format('Y-m-d');
            $this->db->query("INSERT INTO `" . DB_PREFIX . "report_order` SET 
                                                            `report_time` = '" . $report_time . "', 
                                                            `report_date` = '" . $report_date . "',
                                                            `order_id` = " . (int)$bestme_order_id . ", 
                                                            `customer_id` = " . (int)$customer_id . ", 
                                                            `total_amount` = " . (float)$shopee_order_info['total_amount'] . ", 
                                                            `order_status` = " . (int)$order_status . ", 
                                                            `discount` = " . $total_discount . ",
                                                            `source` = 'shopee',
                                                            `shipping_fee` = '" . (float)$shopee_order_info['actual_shipping_cost'] . "', 
                                                            `store_id` = 0, `user_id` = 1");
            foreach ($products as $product) {
                $discount = isset($product['discount']) ? $product['discount'] : 0;
                $query = $this->db->query("SELECT cost_price 
                                                FROM `" . DB_PREFIX . "product_to_store`
                                                WHERE product_id = " . (int)$product['product_id'] . " 
                                                    AND product_version_id= " . (int)$product['product_version_id']);
                $cost_price = $query->row['cost_price'];

                // insert report order
                $user_id = 1; // Admin
                $store_id = 0;
                $total_amount = (int)$product['quantity'] * (float)$product['price'];
                $sql = "INSERT INTO " . DB_PREFIX . "report_product SET report_time = '" . $report_time . "'";
                $sql .= ", report_date = '" . $report_date . "'";
                $sql .= ", order_id = '" . (int)$bestme_order_id . "'";
                $sql .= ", product_id = '" . (int)$product['product_id'] . "'";
                $sql .= ", product_version_id = '" . (int)$product['product_version_id'] . "'";
                $sql .= ", quantity = '" . (int)$product['quantity'] . "'";
                $sql .= ", price = '" . (float)$product['price'] . "'";
                $sql .= ", total_amount = '" . (float)$total_amount . "'";
                $sql .= ", discount = '" . (float)$discount . "'";
                $sql .= ", cost_price = '" . (float)$cost_price . "'";
                $sql .= ", store_id = '" . $store_id . "'";
                $sql .= ", user_id = '" . (int)$user_id . "'";
                $this->db->query($sql);
            }
        }

        return true;
    }

    private function updateQuantity($product_id, $product_version_id, $quantity, $store_id, $condition = '-')
    {
        $this->db->query("UPDATE `" . DB_PREFIX . "product_to_store` 
                          SET `quantity` = `quantity` " . $condition . (int)$quantity . " 
                          WHERE `product_id` = " . (int)$product_id . " 
                            AND `product_version_id` = " . (int)$product_version_id . " 
                            AND `store_id` = " . (int)$store_id);
    }

    private function getAdministrativeCodeByTextValue($administrative_text, $parent_code = null)
    {
        return (new \Vietnam_Administrative())->getAdministrativeCodeByTextValue($administrative_text, $parent_code);
    }

    private function getCustomerByTelephone($data)
    {
        $customer = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "customer` WHERE telephone = '" . $this->db->escape($data['phone']) . "'");
        if (is_array($customer->row) && isset($customer->row['customer_id'])) {
            return $customer->row['customer_id'];
        }

        return $this->createNewCustomer($data);
    }

    private function createNewCustomer($data)
    {
        $customer = [
            'firstname' => isset($data['fullname']) ? extract_name($data['fullname'])[0] : '',
            'lastname' => isset($data['fullname']) ? extract_name($data['fullname'])[1] : '',
            'email' => '',
            'telephone' => $data['phone']
        ];
        $this->db->query("INSERT INTO `" . DB_PREFIX . "customer` 
                                            SET  `firstname` = '" . $this->db->escape($customer['firstname']) . "', 
                                            `lastname` = '" . $this->db->escape($customer['lastname']) . "', 
                                            `email` = '" . $this->db->escape($customer['email']) . "', 
                                            `telephone` = '" . $this->db->escape($customer['telephone']) . "', 
                                            `salt` = '" . $this->db->escape($salt = 'shopee') . "', 
                                            `password` = '" . $this->db->escape(sha1($salt . sha1($salt . sha1('shopee')))) . "', 
                                            `status` = 1,  
                                            `date_added` = NOW()");
        $customer_id = $this->db->getLastId();
        // update customer code
        $prefix = 'KH';
        $code = $prefix . str_pad($customer_id, 6, "0", STR_PAD_LEFT);
        $this->db->query("UPDATE " . DB_PREFIX . "customer SET `code` = '" . $code . "' WHERE customer_id = '" . (int)$customer_id . "'");

        $address_data = array(
            'customer_id' => $customer_id,
            'firstname' => $customer['firstname'],
            'lastname' => $customer['lastname'],
            'company' => '',
            'city' => $data['province'],
            'district' => $data['district'],
            'wards' => $data['ward'],
            'phone' => $customer['telephone'],
            'postcode' => '',
            'address' => $data['full_address'],
            'default' => true
        );

        $this->db->query("INSERT INTO `" . DB_PREFIX . "address` SET 
                                              customer_id = '" . (int)$customer_id . "', 
                                              firstname = '" . $this->db->escape($address_data['firstname']) . "', 
                                              lastname = '" . $this->db->escape($address_data['lastname']) . "', 
                                              company = '" . $this->db->escape($address_data['company']) . "', 
                                              district = '" . $this->db->escape($address_data['district']) . "', 
                                              address = '" . $this->db->escape($address_data['address']) . "', 
                                              phone = '" . $this->db->escape($address_data['phone']) . "', 
                                              postcode = '" . $this->db->escape($address_data['postcode']) . "', 
                                              city = '" . $this->db->escape($address_data['city']) . "'");

        $address_id = $this->db->getLastId();
        $this->db->query("UPDATE `" . DB_PREFIX . "customer` SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");

        // create Customer Report
        $reportTime = new DateTime();
        $reportTime->setTime($reportTime->format('H'), 0, 0);
        $sql = "INSERT INTO `" . DB_PREFIX . "report_customer` SET report_time = '" . $reportTime->format('Y-m-d H:i:s') . "'";
        $sql .= ", report_date = '" . $reportTime->format('Y-m-d') . "'";
        $sql .= ", customer_id = '" . (int)$customer_id . "'";
        $this->db->query($sql);

        return $customer_id;
    }

    /**
     * getDefaultSyncInterval
     *
     * Allow set custom value from config.php for testing. e.g 5 min instead of 12 hours
     *
     * @return int
     */
    public function getDefaultSyncInterval()
    {
        if (defined('SHOPEE_SYNC_INTERVAL_TEST_VALUE') && !empty(SHOPEE_SYNC_INTERVAL_TEST_VALUE)) {
            return (int)SHOPEE_SYNC_INTERVAL_TEST_VALUE;
        }

        return self::DEFAULT_SYNC_INTERVAL;
    }

    private function autoUpdateReceiptVoucher($data, $customer_info)
    {

        $payment_status = isset($data['payment_status']) ? (int)$data['payment_status'] : 0;
        $dataToSave = [
            'payment_status' => $payment_status,
            'payment_method' => $data['payment_method'],
            'total_pay' => $data['total_amount'],
            'created_at' => isset($data['create_time']) ? $data['create_time'] : null,
            'updated_at' => isset($data['update_time']) ? $data['update_time'] : null,
            'customer_info' => $customer_info ? $customer_info : null
        ];

        $receipt_voucher = $this->autoUpdateFromOrder($data['bestme_order_id'], $dataToSave);
    }

    private function autoRemoveReceiptVoucher($order_id)
    {
        try {
            $this->autoRemoveFromOrder($order_id);
            return;
        } catch (Exception $ex) {
            return;
        }
    }

    function trackingTotalAmountShopee($total)
    {
        try {
            $this->load->model('setting/setting');
            $shop_name = $this->config->get('shop_name');
            $packet_paid = $this->config->get('config_packet_paid');
            $domain = $_SERVER['HTTP_HOST'];
            $tracking = new Activity_Tracking_Util();
            $tracking->trackingCountShopeeOrder($domain, $shop_name, $packet_paid, $total);
        } catch (Exception $e) {
            // do something...
        }
    }

    function trackingShopeeOrderCompleted()
    {
        try {
            $this->load->model('setting/setting');
            $shop_name = $this->config->get('shop_name');
            $packet_paid = $this->config->get('config_packet_paid');
            $domain = $_SERVER['HTTP_HOST'];
            $tracking = new Activity_Tracking_Util();
            $tracking->trackingCountShopeeOrderCompleted($domain, $shop_name, $packet_paid);
        } catch (Exception $e) {
            // do something...
        }
    }
}