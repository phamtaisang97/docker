<?php

class ModelCatalogLazada extends Model
{
    use AUTO_CREATE_RECEIPT_VOUCHER_UTIL_TRAIT;

    // product
    const SYNC_STATUS_ALL = 99;
    const SYNC_STATUS_MAPPING_PRODUCT = 1;
    const SYNC_STATUS_NOT_MAPPING_PRODUCT = 0;

    // order
    const SYNC_STATUS_SYNC_SUCCESS = 1;
    const SYNC_STATUS_PRODUCT_NOT_MAPPING = 0;
    const SYNC_STATUS_ORDER_IS_EDITED = 2;
    const SYNC_STATUS_SHOP_EXPIRED = 3;
    const SYNC_STATUS_SYNC_TIME_OUT = 4;
    const SYNC_STATUS_PRODUCT_MAPPING_OUT_OF_STOCK = 5;
    const SYNC_STATUS_ORDER_NOT_YET_SYNC = 6;

    const DEFAULT_SYNC_INTERVAL = 720;

    public function getProductLazadaList($data = array())
    {
        $DB_PREFIX = DB_PREFIX;

        $sql = "SELECT lp.*, lpv.`sync_status`, lpv.`item_id` FROM {$DB_PREFIX}lazada_product lp 
                LEFT JOIN {$DB_PREFIX}lazada_product_version lpv ON lp.`item_id` = lpv.`item_id` WHERE 1";

        if (isset($data['filter_name'])) {
            $sql .= " AND lp.`name` LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (isset($data['shop_id']) && $data['shop_id']) {
            $sql .= " AND lp.`shop_id` = " . $this->db->escape($data['shop_id']);
        }

        if (isset($data['sync_status']) && $data['sync_status'] != self::SYNC_STATUS_ALL) {
            $sql .= " AND lpv.`sync_status` = " . $this->db->escape($data['sync_status']);
        }

        $sql .= " GROUP BY lp.item_id";
        $sql .= " ORDER BY lp.updated_at DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalProductLazadaList($data = array())
    {
        $DB_PREFIX = DB_PREFIX;

        $sql = "SELECT COUNT(DISTINCT lp.`item_id`) as total FROM {$DB_PREFIX}lazada_product lp 
                LEFT JOIN {$DB_PREFIX}lazada_product_version lpv ON lp.`item_id` = lpv.`item_id` WHERE 1";

        if (isset($data['filter_name'])) {
            $sql .= " AND lp.`name` LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (isset($data['shop_id']) && $data['shop_id']) {
            $sql .= " AND lp.`shop_id` = " . $this->db->escape($data['shop_id']);
        }

        if (isset($data['sync_status']) && $data['sync_status'] != self::SYNC_STATUS_ALL) {
            $sql .= " AND lpv.`sync_status` = " . $this->db->escape($data['sync_status']);
        }

        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    public function getProductVersionLazadaList($item_id = 0, $sync_status)
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "lazada_product_version WHERE `item_id`=".$item_id;

        if (isset($sync_status) && $sync_status != self::SYNC_STATUS_ALL) {
            $sql .= " AND `sync_status` = " . $this->db->escape($sync_status);
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getLazadaShopList()
    {
        $DB_PREFIX = DB_PREFIX;

        $sql = "SELECT * FROM {$DB_PREFIX}lazada_shop_config  
                  WHERE `connected` = 1";

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getProductsForLazada($data = array())
    {
        $sql = "SELECT p.status as p_status, p.sku as psku, 
                       p.sale_on_out_of_stock as p_sale_on_out_of_stock, 
                       p.product_id as pid, p.multi_versions, 
                       p.price as o_price, p.compare_price as o_compare_price, 
                       p.weight as weight, p.quantity as o_quantity, dp.name, pv.* 
                FROM " . DB_PREFIX . "product p 
                LEFT JOIN " . DB_PREFIX . "product_description as dp ON (p.product_id = dp.product_id) 
                LEFT JOIN " . DB_PREFIX . "product_version as pv ON CASE WHEN p.multi_versions = 1 THEN p.product_id 
                                                                            ELSE -1
                                                                            END = pv.product_id 
                WHERE 1=1 and dp.language_id = " . (int)$this->config->get('config_language_id');
        if (!empty($data['filter_name'])) {
            $sql .= " AND dp.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['pv_ids'])) {
            $product_ids = [];
            $product_version_ids = [];
            foreach ($data['pv_ids'] as $pv_id){
                $ids = explode("-", $pv_id);
                if (!isset($ids[1])) {
                    $product_ids[] = $ids[0];
                    continue;
                }

                $product_version_ids[] = $ids[1];
            }
            $product_ids = implode(',', $product_ids);
            $product_version_ids = implode(',', $product_version_ids);

            if ($product_ids) {
                $sql .= " AND p.product_id NOT IN (" . $product_ids . ")";
            }
            if ($product_version_ids) {
                $sql .= " AND (pv.product_version_id is NULL OR pv.product_version_id NOT IN (" . $product_version_ids . "))";
            }
        }

        $sql .= " AND ( p.`deleted` IS NULL)";
        $sql .= " AND (pv.`deleted` IS NULL)";
        $sql .= " ORDER BY dp.name ASC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);
        $result = array();
        foreach ($query->rows as $k => $item) {
            $product = array(
                'product_id' => $item['pid'],
                'name' => strlen($item['name']) > 90 ? substr($item['name'], 0, 90) . "..." : $item['name'],
                'product_version_id' => $item['product_version_id'],
                'product_name' => $item['name'],
                'weight' => $item['weight'],
                'version' => $item['version'],
                'sale_on_out_of_stock' => $item['p_sale_on_out_of_stock'],
                'multi_versions' => $item['multi_versions'],
                'edit' => $this->url->link('catalog/product/edit', 'user_token=' . $this->session->data['user_token'] . '&product_id=' . $item['pid'], true),
            );

            if ($item['product_version_id'] != null) {
                $product['id'] = $item['pid'] . '-' . $item['product_version_id'];
                $product['quantity'] = $this->getQuantityProductAndVersion($item['pid'], $item['product_version_id']);
                $product['price'] = $item['compare_price'];
                $product['status'] = $item['status'];
                $product['p_status'] = $item['p_status'];
                $product['sku'] = $item['sku'];
                $product['name'] = $item['name'] .' : '. $item['version'];
            } else {
                $product['id'] = $item['pid'];
                $product['quantity'] = $this->getQuantityProductAndVersion($item['pid']);
                $product['price'] = $item['o_compare_price'];
                $product['status'] = $item['p_status'];
                $product['p_status'] = $item['p_status'];
                $product['sku'] = $item['psku'];
            }

            $result[$product['id']] = $product;
            unset($product);
        }

        return $result;
    }

    public function getQuantityProductAndVersion($product_id, $product_version_id = 0)
    {
        $this->load->model('catalog/product');
        $p_t_stores = $this->model_catalog_product->getProductToStores($product_id, $product_version_id);
        $quantity_total = 0;
        foreach ($p_t_stores as $key => &$item){
            if ($item['product_version_id'] == $product_version_id){
                $quantity_total += $item['quantity'];
            }
        }

        return $quantity_total;
    }

    public function countProductAllContainDraft($data = array())
    {
        $language_id = $this->config->get('config_language_id');
        $sql = "SELECT p.product_id,pd.name from " . DB_PREFIX . "product p 
                    LEFT JOIN " . DB_PREFIX . "product_description as pd ON (p.product_id = pd.product_id) 
                    LEFT JOIN " . DB_PREFIX . "product_version as pv ON (p.product_id = pv.product_id) WHERE 1=1";
        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['pv_ids'])) {
            $product_ids = [];
            $product_version_ids = [];
            foreach ($data['pv_ids'] as $pv_id){
                $ids = explode("-", $pv_id);
                if (!isset($ids[1])) {
                    $product_ids[] = $ids[0];
                    continue;
                }

                $product_version_ids[] = $ids[1];
            }
            $product_ids = implode(',', $product_ids);
            $product_version_ids = implode(',', $product_version_ids);

            if ($product_ids) {
                $sql .= " AND p.product_id NOT IN (" . $product_ids . ")";
            }
            if ($product_version_ids) {
                $sql .= " AND (pv.product_version_id is NULL OR pv.product_version_id NOT IN (" . $product_version_ids . "))";
            }
        }

        $sql .= " AND (p.`deleted` IS NULL)";
        $sql .= " AND (pv.`deleted` IS NULL)";
        $sql .= " AND pd.language_id = " . $language_id . "";
        $sql .= " ORDER BY p.product_id ASC";
        $query = $this->db->query($sql);

        return count($query->rows);
    }

    public function getProductMapping($shop_id)
    {
        $sql = "SELECT lpv.`bestme_product_id`, lpv.`bestme_product_version_id`, lp.`shop_id` FROM " . DB_PREFIX . "lazada_product lp 
                LEFT JOIN " . DB_PREFIX . "lazada_product_version lpv ON lp.`item_id` = lpv.`item_id`  
                WHERE lpv.`bestme_product_id` IS NOT NULL AND lp.`shop_id` = {$shop_id}";

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function mappingProductBestme($id, $product_id, $product_version_id)
    {
        $this->db->query("UPDATE `" . DB_PREFIX . "lazada_product_version` 
                                SET `bestme_product_id` = '" . $product_id . "', 
                                    `bestme_product_version_id` = " . $product_version_id . ", 
                                    `sync_status` = " . 1 . " 
                                WHERE `id` = '" . $id . "'");

        $version = $this->db->query("SELECT * FROM `" . DB_PREFIX . "lazada_product_version` WHERE `id` = ".$id);

        if (isset($version->row['item_id'])) {
            $this->db->query("UPDATE `" . DB_PREFIX . "lazada_product` 
                                SET `sync_status` = '" . 1 . "' 
                                WHERE `item_id` = '" . $version->row['item_id'] . "'");
        }
    }

    public function cancelMappingVersion($id, $item_id)
    {
        $has_mapping = false;

        $this->db->query("UPDATE `" . DB_PREFIX . "lazada_product_version` 
                                SET `bestme_product_id` = NULL, 
                                    `bestme_product_version_id` = NULL, 
                                    `sync_status` = " . 0 . " 
                                WHERE `id` = '" . $id . "'");

        $products_version = $this->db->query("SELECT * FROM `" . DB_PREFIX . "lazada_product_version` WHERE `item_id` = ".$item_id);

        foreach ($products_version->rows as $product) {
            if (isset($product['sync_status']) && $product['sync_status']) {
                $has_mapping = true;
            }
        }

        if (!$has_mapping) {
            $this->db->query("UPDATE `" . DB_PREFIX . "lazada_product` 
                                SET `sync_status` = '" . 0 . "' 
                                WHERE `item_id` = '" . $item_id . "'");
        }
    }

    public function cancelAllMapping($item_id)
    {
        $this->db->query("UPDATE `" . DB_PREFIX . "lazada_product` 
                            SET `sync_status` = '" . 0 . "' 
                            WHERE `item_id` = '" . $item_id . "'");

        $this->db->query("UPDATE `" . DB_PREFIX . "lazada_product_version` 
                                SET `bestme_product_id` = NULL, 
                                    `bestme_product_version_id` = NULL, 
                                    `sync_status` = " . 0 . " 
                                WHERE `item_id` = '" . $item_id . "'");
    }

    public function getAccessTokenById($shop_id)
    {
        $query = $this->db->query("SELECT `access_token` FROM `" . DB_PREFIX . "lazada_shop_config` WHERE `id` = " . (int)$shop_id);

        if (isset($query->row['access_token'])){
            return $query->row['access_token'];
        }

        return '';
    }

    public function disconnectWhenProductChange($old_product, $new_product, $old_product_version, $new_product_version)
    {
        if (!isset($old_product['multi_versions']) ||
            !isset($new_product['multi_versions'])) {
            return;
        }

        $product_id = isset($old_product['product_id']) ? $old_product['product_id'] : '';

        if ($product_id === '') {
            return;
        }

        $products_version_lazada = $this->db->query("SELECT * FROM `" . DB_PREFIX . "lazada_product_version` WHERE `bestme_product_id` = " . $product_id);

        if ($old_product['multi_versions'] != $new_product['multi_versions']) {
            foreach ($products_version_lazada->rows as $product_version) {
                $has_mapping = false;
                // B1: disconnect all product version has connect with $old_product['product_id']
                $this->db->query("UPDATE `" . DB_PREFIX . "lazada_product_version` 
                                SET `bestme_product_id` = NULL, 
                                    `bestme_product_version_id` = NULL, 
                                    `sync_status` = " . 0 . " 
                                WHERE `id` = '" . $product_version['id'] . "'");

                // B2: checking item_id of product disconnect has connect
                $products_version_checking = $this->db->query("SELECT * FROM `" . DB_PREFIX . "lazada_product_version` WHERE `item_id` = '" . $product_version['item_id'] . "'");

                foreach ($products_version_checking->rows as $product) {
                    if (isset($product['sync_status']) && $product['sync_status']) {
                        $has_mapping = true;
                    }
                }

                if (!$has_mapping) {
                    $this->db->query("UPDATE `" . DB_PREFIX . "lazada_product` 
                                SET `sync_status` = '" . 0 . "' 
                                WHERE `item_id` = '" . $product_version['item_id'] . "'");
                }
            }

        } else {
            if (empty($products_version_lazada->rows)) {
                return;
            }
            if ($old_product['multi_versions'] == 0 && $new_product['multi_versions'] == 0) {
                return;
            }
            if (!isset($old_product_version) || !isset($new_product_version)) {
                return;
            }

            $arr_disconnect = [];
            $arr_pv_new = [];

            foreach ($new_product_version as $pv_new) {
                if (!isset($pv_new['product_version_id'])) {
                    continue;
                }

                $arr_pv_new[] = $pv_new['product_version_id'];
            }

            foreach ($old_product_version as $pv_old) {
                if (!isset($pv_old['product_version_id'])) {
                    continue;
                }

                if (!in_array($pv_old['product_version_id'], $arr_pv_new)) {
                    $arr_disconnect[] = $pv_old['product_version_id'];
                }
            }

            if (empty($arr_disconnect)) {
                return;
            }

            foreach ($arr_disconnect as $item) {
                $this->db->query("UPDATE `" . DB_PREFIX . "lazada_product_version` 
                                SET `bestme_product_id` = NULL, 
                                    `bestme_product_version_id` = NULL, 
                                    `sync_status` = " . 0 . " 
                                WHERE `bestme_product_version_id` = '" . (int)$item . "'");
            }

            foreach ($products_version_lazada->rows as $product_version) {
                $has_mapping = false;
                $products_version_checking = $this->db->query("SELECT * FROM `" . DB_PREFIX . "lazada_product_version` WHERE `item_id` = '" . $product_version['item_id'] . "'");

                foreach ($products_version_checking->rows as $product) {
                    if (isset($product['sync_status']) && $product['sync_status']) {
                        $has_mapping = true;
                    }
                }

                if (!$has_mapping) {
                    $this->db->query("UPDATE `" . DB_PREFIX . "lazada_product` 
                                SET `sync_status` = '" . 0 . "' 
                                WHERE `item_id` = '" . $product_version['item_id'] . "'");
                }
            }
        }
    }

    public function disconnectWhenProductDeleted($product_id)
    {
        if (!isset($product_id)) {
            return;
        }

        $products = $this->db->query("SELECT * FROM `" . DB_PREFIX . "lazada_product_version` WHERE `bestme_product_id` = " . $product_id);

        foreach ($products->rows as $product_version) {
            $has_mapping = false;
            // B1: disconnect all product version has connect with $product_id
            $this->db->query("UPDATE `" . DB_PREFIX . "lazada_product_version` 
                                SET `bestme_product_id` = NULL, 
                                    `bestme_product_version_id` = NULL, 
                                    `sync_status` = " . 0 . " 
                                WHERE `bestme_product_id` = '" . $product_id . "'");

            // B2: checking item_id of product disconnect has connect
            $products_version_checking = $this->db->query("SELECT * FROM `" . DB_PREFIX . "lazada_product_version` WHERE `item_id` = '" . $product_version['item_id'] ."'");

            foreach ($products_version_checking->rows as $product) {
                if (isset($product['sync_status']) && $product['sync_status']) {
                    $has_mapping = true;
                }
            }

            if (!$has_mapping) {
                $this->db->query("UPDATE `" . DB_PREFIX . "lazada_product` 
                                SET `sync_status` = '" . 0 . "' 
                                WHERE `item_id` = '" . $product_version['item_id'] . "'");
            }
        }
    }

    public function updateLazadaShopConfig($data)
    {
        if (!array_key_exists('access_token', $data) || !array_key_exists('refresh_token', $data) || !array_key_exists('seller_id', $data) || !array_key_exists('country', $data)){
            return;
        }

        $query = $this->db->query("SELECT `id` FROM `" . DB_PREFIX . "lazada_shop_config` WHERE `seller_id` = " . (int)$data['seller_id'] . " AND `country` = '" . $this->db->escape($data['country']) . "'");
        if ($query->num_rows > 0 && isset($query->row['id'])) {
            $id = (int)$query->row['id'];
            $this->db->query("UPDATE `" . DB_PREFIX . "lazada_shop_config` 
                                                            SET `shop_name` = '" . $this->db->escape($data['shop_name']) . "', 
                                                                `access_token` = '" . $this->db->escape($data['access_token']) . "', 
                                                                `expires_in` = '" . $this->db->escape($data['expires_in']) . "', 
                                                                `refresh_token` = '" . $this->db->escape($data['refresh_token']) . "', 
                                                                `refresh_expires_in` = '" . $this->db->escape($data['refresh_expires_in']) . "', 
                                                                `sync_interval` = '" . $this->getDefaultSyncInterval() . "', 
                                                                `connected` = '" . 1 . "' 
                                                                WHERE `id` = " . (int)$id);
            return $id;
        } else {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "lazada_shop_config` 
                                                            SET `seller_id` = " . (int)$data['seller_id'] . ", 
                                                                `country` = '" . $this->db->escape($data['country']) . "', 
                                                                `shop_name` = '" . $this->db->escape($data['shop_name']) . "', 
                                                                `access_token` = '" . $this->db->escape($data['access_token']) . "', 
                                                                `expires_in` = '" . $this->db->escape($data['expires_in']) . "', 
                                                                `refresh_token` = '" . $this->db->escape($data['refresh_token']) . "', 
                                                                `sync_interval` = '" . $this->getDefaultSyncInterval() . "', 
                                                                `refresh_expires_in` = '" . $this->db->escape($data['refresh_expires_in']) . "'");
            return $this->db->getLastId();
        }
    }

    public function shopIsConnected($data)
    {
        if (!is_array($data) || !array_key_exists('seller_id', $data)){
            return false;
        }
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "lazada_shop_config` WHERE `connected` = 1 
                                                                                            AND `seller_id` = '" . $this->db->escape($data['seller_id']) . "'");

        if ($query->num_rows > 0) {
            return true;
        }

        return false;
    }

    public function getTimeIntervalById($shop_id)
    {
        $query = $this->db->query("SELECT `sync_interval` FROM `" . DB_PREFIX . "lazada_shop_config` WHERE `id` = " . (int)$shop_id);

        if (isset($query->row['sync_interval'])) {
            return (int)$query->row['sync_interval'];
        }

        return 0;
    }

    public function getOrderIdByShop($shop_id)
    {
        if (!$shop_id) {
            return [];
        }

        $query = $this->db->query("SELECT order_id FROM " . DB_PREFIX . "lazada_order WHERE lazada_shop_id = " . (int)$shop_id . " AND `sync_time` >= now()-interval 15 day");

        $codes = array_map(function ($row) {
            return $row['order_id'];
        }, $query->rows);

        return $codes;
    }

    public function getOrderIdById($id)
    {
        $query = $this->db->query("SELECT order_id FROM " . DB_PREFIX . "lazada_order WHERE id = " . (int)$id);

        if (isset($query->row['order_id'])) {
            return $query->row['order_id'];
        }

        return false;
    }

    public function getProductVersionByVersionId($id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "lazada_product_version` WHERE `id` = " . (int)$id);

        return $query->row;
    }

    public function getProductVersionByItemId($item_id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "lazada_product_version` WHERE `item_id` = " . (int)$item_id);

        return $query->rows;
    }

    public function getProductById($id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "lazada_product` WHERE `id` = " . (int)$id);

        return $query->row;
    }

    public function getProductByItemId($item_id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "lazada_product` WHERE `item_id` = " . (int)$item_id);

        return $query->row;
    }

    public function getCategoryAttributes($category_id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "lazada_category_attributes` WHERE `lazada_cateogry_id` = " . (int)$category_id);

        if ($query->num_rows > 0) {
            return json_decode($query->row['data'], true);
        }

        return [];
    }

    public function getListLazadaShops()
    {
        $query = $this->db->query("SELECT `shop_name`, 
                                          `id`, 
                                          `sync_interval`, 
                                          `refresh_expires_in`, 
                                          `order_sync_range`, 
                                          `allow_update_stock_product`, 
                                          `allow_update_price_product`, 
                                          `last_sync_time_product`, 
                                          `last_sync_time_order`, 
                                          IF(`refresh_expires_in` > NOW(), 0, 1) as expired 
                                          FROM `" . DB_PREFIX . "lazada_shop_config` WHERE `connected` = " . 1);

        return $query->rows;
    }

    public function getLastSyncTimeOrderById($shop_id)
    {
        $query = $this->db->query("SELECT `last_sync_time_order` FROM `" . DB_PREFIX . "lazada_shop_config` WHERE `id` = " . (int)$shop_id);

        if (isset($query->row['last_sync_time_order'])) {
            return $query->row['last_sync_time_order'];
        }

        return null;
    }

    public function getLastSyncTimeProductById($shop_id)
    {
        $query = $this->db->query("SELECT `last_sync_time_product` FROM `" . DB_PREFIX . "lazada_shop_config` WHERE `id` = " . (int)$shop_id);

        if (isset($query->row['last_sync_time_product'])) {
            return $query->row['last_sync_time_product'];
        }

        return null;
    }

    public function disconnectShopLazadaById($id)
    {
        $this->db->query("UPDATE `" . DB_PREFIX . "lazada_shop_config` 
                                SET `connected` = '" . 0 . "' 
                                WHERE `id` = '" . $id . "'");
    }

    public function truncateLazadaCategoryTree()
    {
        $this->db->query("TRUNCATE TABLE `" . DB_PREFIX . "lazada_category_tree`");
    }

    public function updateLazadaCategoryTree($data, $parent_id = 0)
    {
        if (!is_array($data) || empty($data)) {
            return;
        }

        foreach ($data as $item) {
            if (!array_key_exists('name', $item) || !array_key_exists('category_id', $item) || !$item['name'] || !$item['category_id']) {
                continue;
            }

            $this->db->query("INSERT INTO `" . DB_PREFIX . "lazada_category_tree` SET 
                                        `id` = " . (int)$item['category_id'] . ", 
                                        `parent_id` = " . (int)$parent_id . ", 
                                        `name` = '" . $this->db->escape($item['name']) . "'");

            if (isset($item['children'])){
                $this->updateLazadaCategoryTree($item['children'], (int)$item['category_id']);
            }
        }
    }

    public function getCategoryNameById($category_id)
    {
        $query = $this->db->query("SELECT lct.`name` as en_name, lcd.`name` as des_name FROM `" . DB_PREFIX . "lazada_category_tree` lct 
                                          LEFT JOIN `" . DB_PREFIX . "lazada_category_description` lcd ON lct.`id` = lcd.`category_id` 
                                              WHERE lct.`id` = " . (int)$category_id);

        if ($query->num_rows > 0) {
            if (!$query->row['des_name']){
                return $query->row['en_name'];
            }

            return $query->row['des_name'];
        }

        return '';
    }

    public function addCategoryDescription($id, $name)
    {
        if (!$id || !$name){
            return;
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "lazada_category_description` SET `category_id` = " . (int)$id . ", `name` = '" . $this->db->escape($name) . "'");
    }

    public function countCategoryDescription()
    {
        $query = $this->db->query("SELECT COUNT(*) as count_des FROM `" . DB_PREFIX . "lazada_category_description`");

        if (isset($query->row['count_des'])) {
            return (int)$query->row['count_des'];
        }

        return 0;
    }

    public function getOrderSyncList($data = [])
    {
        $DB_PREFIX = DB_PREFIX;

        $sql = "SELECT * FROM {$DB_PREFIX}lazada_order WHERE 1";

        if (isset($data['filter_name']) && $data['filter_name']) {
            $sql .= " AND `order_id` LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (isset($data['shop_id']) && $data['shop_id']) {
            $sql .= " AND `lazada_shop_id` = " . $this->db->escape($data['shop_id']);
        }

        if (isset($data['sync_status']) && $data['sync_status'] != self::SYNC_STATUS_ALL) {
            if ($data['sync_status'] == self::SYNC_STATUS_SYNC_SUCCESS || $data['sync_status'] == self::SYNC_STATUS_ORDER_NOT_YET_SYNC) {
                $sql .= " AND `sync_status` = " . $this->db->escape($data['sync_status']);
            } else {
                $sql .= " AND `sync_status` NOT IN (1,6)";
            }

        }

        $sql .= " AND `sync_time` >= now()-interval 15 day";
        $sql .= " ORDER BY sync_time DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getTotalOrderSyncList($data = [])
    {
        $DB_PREFIX = DB_PREFIX;

        $sql = "SELECT COUNT(*) as total FROM {$DB_PREFIX}lazada_order WHERE 1";

        if (isset($data['filter_name']) && $data['filter_name']) {
            $sql .= " AND `order_id` LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (isset($data['shop_id']) && $data['shop_id']) {
            $sql .= " AND `lazada_shop_id` = " . $this->db->escape($data['shop_id']);
        }

        if (isset($data['sync_status']) && $data['sync_status'] != self::SYNC_STATUS_ALL) {
            if ($data['sync_status'] == self::SYNC_STATUS_SYNC_SUCCESS || $data['sync_status'] == self::SYNC_STATUS_ORDER_NOT_YET_SYNC) {
                $sql .= " AND `sync_status` = " . $this->db->escape($data['sync_status']);
            } else {
                $sql .= " AND `sync_status` NOT IN (1,6)";
            }

        }

        $sql .= " AND `sync_time` >= now()-interval 15 day";

        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    public function updateTimeSyncById($shop_id, $time)
    {
        $this->db->query("UPDATE `" . DB_PREFIX . "lazada_shop_config` 
                                SET `sync_interval` = '" . (int)$time . "'  
                                WHERE `id` = '" . $shop_id . "'");
    }

    public function checkExitsLazadaShop($shop_id)
    {
        return $this->db->query("SELECT id FROM " . DB_PREFIX . "lazada_shop_config WHERE id = " . (int)$shop_id);
    }

    public function shopLazadaExpired($shop_id)
    {
        $query = $this->db->query("SELECT `id`, `connected`, `refresh_expires_in`, IF(`refresh_expires_in` > NOW(), 0, 1) as expired 
                                   FROM `" . DB_PREFIX . "lazada_shop_config` WHERE `id` = " . $shop_id . " AND `connected` = 1");

        return $query->row['expired'];
    }

    /**
     * getDefaultSyncInterval
     *
     * Allow set custom value from config.php for testing. e.g 5 min instead of 12 hours
     *
     * @return int
     */
    public function getDefaultSyncInterval()
    {
        if (defined('LAZADA_SYNC_INTERVAL_TEST_VALUE') && !empty(LAZADA_SYNC_INTERVAL_TEST_VALUE)) {
            return (int)LAZADA_SYNC_INTERVAL_TEST_VALUE;
        }

        return self::DEFAULT_SYNC_INTERVAL;
    }

    public function getLazadaOrderbyOrderId($order_id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "lazada_order` WHERE `order_id` = '" . $this->db->escape($order_id) . "'");

        return $query->row;
    }

    public function getProductInfoById($product_id, $product_version_id = 0)
    {
        $sql = "SELECT p.product_id as pid, 
                       p.price as o_price, p.compare_price as o_compare_price, 
                       dp.name, pv.* 
                FROM " . DB_PREFIX . "product p 
                LEFT JOIN " . DB_PREFIX . "product_description as dp ON (p.product_id = dp.product_id) 
                LEFT JOIN " . DB_PREFIX . "product_version as pv ON CASE WHEN p.multi_versions = 1 THEN p.product_id 
                                                                            ELSE -1
                                                                            END = pv.product_id 
                WHERE 1=1 and dp.language_id = " . (int)$this->config->get('config_language_id');
        $sql .= " AND p.product_id = ".$product_id;

        if ($product_version_id) {
            $sql .= " AND pv.product_version_id = ".$product_version_id;
        }

        $query = $this->db->query($sql);

        $product = array(
            'product_id' => $query->row['pid'],
            'name' => strlen($query->row['name']) > 90 ? substr($query->row['name'], 0, 90) . "..." : $query->row['name'],
            'edit' => $this->url->link('catalog/product/edit', 'user_token=' . $this->session->data['user_token'] . '&product_id=' . $query->row['pid'], true),
        );

        if ($query->row['product_version_id'] != null) {
            $product['id'] = $query->row['pid'] . '-' . $query->row['product_version_id'];
            $product['quantity'] = $this->getQuantityProductAndVersion($query->row['pid'], $query->row['product_version_id']);
            $product['price'] = $query->row['compare_price'];
            $product['name'] = $query->row['name'] .' : '. $query->row['version'];
        } else {
            $product['id'] = $query->row['pid'];
            $product['quantity'] = $this->getQuantityProductAndVersion($query->row['pid']);
            $product['price'] = $query->row['o_compare_price'];
        }

        return $product;
    }

    // Save order to admin
    public function syncOrderToAdmin($order_id)
    {
        try {
            $lazada_order = $this->getLazadaOrderbyOrderId($order_id);
            if (empty($lazada_order)) {
                return false;
            }
            $is_edited = isset($lazada_order['is_edited']) ? (int)$lazada_order['is_edited'] : 0;
            if ($is_edited) {
                $sync_status = self::SYNC_STATUS_ORDER_IS_EDITED;
                $this->db->query("UPDATE `" . DB_PREFIX . "lazada_order` SET `sync_status` = " . (int)$sync_status . " WHERE `order_id` = '" . $this->db->escape($order_id) . "'");
                return false;
            }
            $sync_type = 'add';
            if (isset($lazada_order['bestme_order_id']) && $lazada_order['bestme_order_id']) {
                $sync_type = 'update';
            }

            // product in order
            $lazada_order_product = $this->db->query("SELECT * FROM `" . DB_PREFIX . "lazada_order_to_product` WHERE `lazada_order_id` = '" . $this->db->escape($order_id) . "'");
            $admin_order_product = [];
            $sync_status =  self::SYNC_STATUS_SYNC_SUCCESS;
            foreach ($lazada_order_product->rows as $row) {
                if (!isset($row['shop_sku']) || !isset($row['quantity'])) {
                    continue;
                }
                $product = $this->mapProductInOrderToAdmin($this->db, DB_PREFIX, $row);
                if (!$product) {
                    $sync_status =  self::SYNC_STATUS_PRODUCT_NOT_MAPPING;
                    break;
                }

                $admin_order_product[] = $product;
            }
            if ($sync_status !=  self::SYNC_STATUS_SYNC_SUCCESS) {
                $this->db->query("UPDATE `" . DB_PREFIX . "lazada_order` SET `sync_status` = " . (int)$sync_status . " WHERE `order_id` = '" . $this->db->escape($order_id) . "'");
                return false;
            } else {
                // check quantity
                if ($sync_type == 'add') {
                    foreach ($admin_order_product as $product) {
                        if (!isset($product['product_id']) || !isset($product['product_version_id']) || !isset($product['quantity']) || !isset($product['price']) || !isset($product['default_store_id']) || !isset($product['sale_on_out_of_stock'])) {
                            continue;
                        }

                        if (!$product['sale_on_out_of_stock'] && !$this->checkQuantity($this->db, DB_PREFIX, $product['product_id'], $product['product_version_id'], $product['default_store_id'], $product['quantity'])) {
                            $sync_status =  self::SYNC_STATUS_PRODUCT_MAPPING_OUT_OF_STOCK;
                            break;
                        }
                    }

                    if ($sync_status !=  self::SYNC_STATUS_SYNC_SUCCESS) {
                        $this->db->query("UPDATE `" . DB_PREFIX . "lazada_order` SET `sync_status` = " . (int)$sync_status . " WHERE `order_id` = '" . $this->db->escape($order_id) . "'");
                        return false;
                    }
                }
                $result = $this->saveToAdmin($this->db, DB_PREFIX, $order_id, $admin_order_product);
                if (!$result) {
                    $sync_status =  self::SYNC_STATUS_ORDER_NOT_YET_SYNC;
                }
                $this->db->query("UPDATE `" . DB_PREFIX . "lazada_order` SET `sync_status` = " . (int)$sync_status . " WHERE `order_id` = '" . $this->db->escape($order_id) . "'");

                return $result;
            }
        } catch (Exception $e) {
            // do something ...
        }
    }

    public function saveLazadaShopConfig($shop_id, $order_sync_range = 15, $allow_update_stock_product = 0, $allow_update_price_product = 0)
    {
        $DB_PREFIX = DB_PREFIX;
        $this->db->query("UPDATE `{$DB_PREFIX}lazada_shop_config` 
                                SET `order_sync_range` = {$order_sync_range}, 
                                    `allow_update_stock_product` = {$allow_update_stock_product}, 
                                    `allow_update_price_product` = {$allow_update_price_product} 
                                WHERE `id` = {$shop_id}");
    }

    public function getIds($shop_id)
    {
        if (!$shop_id) {
            return [];
        }

        $query = $this->db->query("SELECT `id` FROM " . DB_PREFIX . "lazada_product WHERE shop_id = " . (int)$shop_id);

        $ids = array_map(function ($row) {
            return $row['id'];
        }, $query->rows);

        return $ids;
    }

    // v2.11.2
    function allowUpdateInventory($shop_id)
    {
        $db_prefix = DB_PREFIX;
        $shop = $this->db->query("SELECT `allow_update_stock_product` FROM `{$db_prefix}lazada_shop_config` WHERE `id` = {$shop_id}");

        if (isset($shop->row['allow_update_stock_product']) && $shop->row['allow_update_stock_product']) {
            return true;
        }

        return false;
    }

    function allowUpdatePrice($shop_id)
    {
        $db_prefix = DB_PREFIX;
        $shop = $this->db->query("SELECT `allow_update_price_product` FROM `{$db_prefix}lazada_shop_config` WHERE `id` = {$shop_id}");

        if (isset($shop->row['allow_update_price_product']) && $shop->row['allow_update_price_product']) {
            return true;
        }

        return false;
    }

    function updateProductHasLinkProductLazada($shop_id, $product_bestme_id, $product_bestme_version_id, $stock, $price, $original_price)
    {
        try {
            $db_prefix = DB_PREFIX;
            $product = $this->db->query("SELECT `default_store_id` FROM `{$db_prefix}product` WHERE `product_id` = {$product_bestme_id}");
            $default_store_id = isset($product->row['default_store_id']) ? $product->row['default_store_id'] : 0;
            if ($original_price == $price) {
                $price = 0;
            }

            if ($product_bestme_version_id) {
                if ($this->allowUpdateInventory($shop_id)) {
                    $this->db->query("UPDATE `{$db_prefix}product_to_store` SET `quantity` = {$stock} 
                                  WHERE `product_id` = {$product_bestme_id} 
                                    AND product_version_id = {$product_bestme_version_id} 
                                    AND `store_id` = {$default_store_id}");
                }

                if ($this->allowUpdatePrice($shop_id)) {
                    $this->db->query("UPDATE `{$db_prefix}product_version` 
                                  SET `price` = {$price}, 
                                      `compare_price` = {$original_price} 
                                    WHERE `product_id` = {$product_bestme_id} 
                                      AND `product_version_id` = {$product_bestme_version_id}");
                }
            } else {
                if ($this->allowUpdateInventory($shop_id)) {
                    $this->db->query("UPDATE `{$db_prefix}product_to_store` 
                                  SET `quantity` = {$stock} 
                                    WHERE `product_id` = {$product_bestme_id} 
                                      AND product_version_id = 0 
                                      AND `store_id` = {$default_store_id}");
                }

                if ($this->allowUpdatePrice($shop_id)) {
                    $this->db->query("UPDATE `{$db_prefix}product` 
                                  SET `price` = {$price}, 
                                      `compare_price` = {$original_price} 
                                    WHERE `product_id` = {$product_bestme_id}");
                }
            }
        } catch (Exception $e) {
            // do something
        }
    }
    // end v2.11.2

    private function mapProductInOrderToAdmin(DB $db, $db_prefix, $item)
    {
        $product = [
            'price' => $item['item_price'],
            'discount' => (float)$item['item_price'] - (float)$item['paid_price'],
            'quantity' => $item['quantity']
        ];
        $product_lazada = $db->query("SELECT * FROM `{$db_prefix}lazada_product_version` WHERE `shop_sku` = '" . $item['shop_sku'] . "'");
        if ($product_lazada->num_rows == 0) {
            return false;
        }
        $product_lazada = $product_lazada->row;
        if (!$product_lazada['bestme_product_id']){
            return false;
        }
        $admin_product_id = (int)$product_lazada['bestme_product_id'];
        $product_admin = $db->query("SELECT * FROM `{$db_prefix}product` WHERE `product_id` = '" . $admin_product_id . "' AND `deleted` IS NULL");
        if ($product_admin->num_rows == 0) {
            return false;
        }
        $product['default_store_id'] = isset($product_admin->row['default_store_id']) ? (int)$product_admin->row['default_store_id'] : 0;
        $product['product_id'] = $admin_product_id;
        $product['sale_on_out_of_stock'] = isset($product_admin->row['sale_on_out_of_stock']) ? (int)$product_admin->row['sale_on_out_of_stock'] : 0;
        $product['product_version_id'] = (int)$product_lazada['bestme_product_version_id'];

        if ($product['product_version_id']){
            $product_version_admin = $db->query("SELECT * FROM `{$db_prefix}product_version` WHERE `product_id` = '" . $admin_product_id . "' AND `product_version_id` = '" . $product['product_version_id'] . "' AND `deleted` IS NULL");
            if ($product_version_admin->num_rows == 0) {
                return false;
            }
        }

        return $product;
    }

    private function checkQuantity(DB $db, $db_prefix, $product_id, $product_version_id, $store_id, $quantity)
    {
        $product_in_store = $db->query("SELECT * FROM `{$db_prefix}product_to_store` WHERE 
                                                                                `product_id` = " . (int)$product_id . " 
                                                                                AND `product_version_id` = " . (int)$product_version_id . "  
                                                                                AND `store_id` = " . (int)$store_id . "   
                                                                                AND `quantity` >= " . (int)$quantity);

        if ($product_in_store->num_rows > 0) {
            return true;
        }

        return false;
    }

    private function updateQuantity(DB $db, $db_prefix, $product_id, $product_version_id, $quantity, $store_id, $condition = '-')
    {
        $db->query("UPDATE `{$db_prefix}product_to_store` 
                    SET `quantity` = `quantity` " . $condition . (int)$quantity ." 
                    WHERE `product_id` = " . (int)$product_id . " 
                      AND `product_version_id` = " . (int)$product_version_id . " 
                      AND `store_id` = " . (int)$store_id);
    }

    private function saveToAdmin(DB $db, $db_prefix, $order_id, $products)
    {
        $lazada_order_info = $db->query("SELECT * FROM `{$db_prefix}lazada_order` WHERE `order_id` = '" . $db->escape($order_id) . "'");
        if ($lazada_order_info->num_rows == 0) {
            return false;
        }
        $lazada_order_info = $lazada_order_info->row;

        $map_order_satus = [
            'pending' => 7,
            'ready_to_ship' => 8,
            'shipped' => 8,
            'delivered' => 9,
            'canceled' => 10,
            'returned' => 10,
            'failed' => 10
        ];
        $order_status = isset($map_order_satus[$lazada_order_info['order_status']]) ? (int)$map_order_satus[$lazada_order_info['order_status']] : 0;
        $payment_status = isset($lazada_order_info['payment_status']) ? (int)$lazada_order_info['payment_status'] : 0;
        // TODO use setting config to get user_id, language_id
        $province = $this->getAdministrativeCodeByTextValue($lazada_order_info['province']);
        $district = $province ? $this->getAdministrativeCodeByTextValue($lazada_order_info['district'], $province) : '';
        $ward = $district ? $this->getAdministrativeCodeByTextValue($lazada_order_info['ward'], $district) : '';
        $customer_id = $this->getCustomerByTelephone($db, $db_prefix, ['phone' => $lazada_order_info['phone'], 'fullname' => $lazada_order_info['fullname'], 'province' => $province, 'district' => $district, 'ward' => $ward, 'full_address' => $lazada_order_info['full_address']]);
        $customer_info = json_encode(['id' => $customer_id, 'name' => $lazada_order_info['fullname']]);

        if ($order_status == 10) {
            if (isset($lazada_order_info['bestme_order_id']) && $lazada_order_info['bestme_order_id'] > 0) {
                $this->autoRemoveReceiptVoucher($lazada_order_info['bestme_order_id']);
            }
        } else {
            $this->autoUpdateReceiptVoucher($lazada_order_info, $customer_info);
        }

        if (isset($lazada_order_info['bestme_order_id']) && $lazada_order_info['bestme_order_id'] > 0) {
            $old_order_status_id = $db->query("SELECT `order_status_id` FROM `{$db_prefix}order` WHERE `order_id` = " . (int)$lazada_order_info['bestme_order_id']);
            $old_order_status_id = isset($old_order_status_id->row['order_status_id']) ? $old_order_status_id->row['order_status_id'] : 0;

            $db->query("UPDATE `{$db_prefix}order` 
                                    SET `fullname` = '" . $db->escape($lazada_order_info['fullname']) . "', 
                                    `shipping_province_code` = '" . $province . "', 
                                    `shipping_district_code` = '" . $district . "', 
                                    `shipping_ward_code` = '" . $ward . "', 
                                    `shipping_address_1` = '" . $db->escape($lazada_order_info['full_address']) . "', 
                                    `telephone` = '" . $db->escape($lazada_order_info['phone']) . "', 
                                    `shipping_method` = '" . $db->escape($lazada_order_info['shipping_method']) . "', 
                                    `shipping_method_value` = '-2', 
                                    `shipping_fee` = '" . (float)$lazada_order_info['shipping_fee'] . "', 
                                    `order_status_id` = '" . $order_status . "', 
                                    `previous_order_status_id` = '" . $old_order_status_id . "', 
                                    `order_code` = '" . $db->escape($lazada_order_info['order_id']) . "', 
                                    `payment_method` = '" . $db->escape($lazada_order_info['payment_method']) . "', 
                                    `discount` = '', 
                                    `total` = '" . (float)$lazada_order_info['total_amount'] . "',
                                    `comment` = '" . $db->escape($lazada_order_info['remarks']) . "',
                                    `user_id` = 1,
                                    `customer_id` = '" . (int)$customer_id . "',
                                    `date_modified` = '" . $lazada_order_info['update_time'] ."',
                                    `payment_status` = '" . (int)$payment_status . "',
                                    `language_id` = 2 WHERE `order_id` = '" . (int)$lazada_order_info['bestme_order_id'] . "'");

            // update quantity product
            if (in_array($old_order_status_id, [7, 8, 9]) && $order_status == 10) {
                foreach ($products as $product) {
                    $this->updateQuantity($db, $db_prefix, $product['product_id'], $product['product_version_id'], $product['quantity'], $product['default_store_id'], '+');
                }

            }
            // update to report
            $db->query("UPDATE `{$db_prefix}report_order` SET 
                                                            `customer_id` = " . (int)$customer_id . ",
                                                            `total_amount` = " . (float)$lazada_order_info['total_amount'] . ", 
                                                            `order_status` = " . (int)$order_status . ",
                                                            `shipping_fee` = '" . (float)$lazada_order_info['shipping_fee'] . "',
                                                            `report_date` = '" . $lazada_order_info['update_time'] ."'
                                                            WHERE `order_id` = " . (int)$lazada_order_info['bestme_order_id']);
        } else {
            $db->query("INSERT INTO `{$db_prefix}order` 
                                    SET `fullname` = '" . $db->escape($lazada_order_info['fullname']) . "', 
                                    `shipping_province_code` = '" . $province . "', 
                                    `shipping_district_code` = '" . $district . "', 
                                    `shipping_ward_code` = '" . $ward . "', 
                                    `shipping_address_1` = '" . $db->escape($lazada_order_info['full_address']) . "', 
                                    `telephone` = '" . $db->escape($lazada_order_info['phone']) . "', 
                                    `shipping_method` = '" . $db->escape($lazada_order_info['shipping_method']) . "', 
                                    `shipping_method_value` = '-2', 
                                    `shipping_fee` = '" . (float)$lazada_order_info['shipping_fee'] . "', 
                                    `order_status_id` = '" . $order_status . "', 
                                    `order_code` = '" . $db->escape($lazada_order_info['order_id']) . "', 
                                    `payment_method` = '" . $db->escape($lazada_order_info['payment_method']) . "', 
                                    `discount` = '', 
                                    `total` = '" . (float)$lazada_order_info['total_amount'] . "',
                                    `comment` = '" . $db->escape($lazada_order_info['remarks']) . "',
                                    `source` = 'lazada',
                                    `user_id` = 1,
                                    `customer_id` = '" . (int)$customer_id . "',
                                    `date_added` = '" . $lazada_order_info['create_time'] ."',
                                    `date_modified` = '" . $lazada_order_info['update_time'] ."',
                                    `payment_status` = '" . (int)$payment_status . "',
                                    `language_id` = 2");

            $bestme_order_id = $db->getLastId();
            $db->query("UPDATE `{$db_prefix}lazada_order` SET `bestme_order_id` = " . (int)$bestme_order_id . " WHERE `order_id` = '" . $db->escape($order_id) . "'");
            // insert order_product
            $total_discount = 0;
            foreach ($products as $product) {
                $discount = isset($product['discount']) ? $product['discount'] : 0;
                $total_discount += $discount;

                $db->query("INSERT INTO `{$db_prefix}order_product` 
                                                      SET `order_id` = " . (int)$bestme_order_id . ", 
                                                      `product_id` = " . (int)$product['product_id'] . ", 
                                                      `product_version_id` = " . (int)$product['product_version_id'] . ", 
                                                      `quantity` = " . (int)$product['quantity'] . ", 
                                                      `price` = " . (float)$product['price'] . ", 
                                                      `discount` = " . (float)$discount . ", 
                                                      `total` = " . (float)$product['price'] * (int)$product['quantity']);

                // update quantity
                $this->updateQuantity($db, $db_prefix, $product['product_id'], $product['product_version_id'], $product['quantity'], $product['default_store_id']);
            }
            // insert to report
            $report_time = $lazada_order_info['update_time'];
            $date = new DateTime($report_time);
            $report_date = $date->format('Y-m-d');
            $db->query("INSERT INTO `{$db_prefix}report_order` SET 
                                                            `report_time` = '" . $report_time . "', 
                                                            `report_date` = '" . $report_date . "',
                                                            `order_id` = " . (int)$bestme_order_id . ", 
                                                            `customer_id` = " . (int)$customer_id . ",
                                                            `total_amount` = " . (float)$lazada_order_info['total_amount'] . ", 
                                                            `order_status` = " . (int)$order_status . ",
                                                            `discount` = " . $total_discount . ", 
                                                            `source` = 'lazada',
                                                            `shipping_fee` = '" . (float)$lazada_order_info['shipping_fee'] . "',
                                                            `store_id` = 0, `user_id` = 1");
            foreach ($products as $product) {
                $discount = isset($product['discount']) ? $product['discount'] : 0;
                $query = $this->db->query("SELECT cost_price 
                                                FROM `" . DB_PREFIX . "product_to_store`
                                                WHERE product_id = " . (int)$product['product_id'] . " 
                                                    AND product_version_id= " . (int)$product['product_version_id']);
                $cost_price = $query->row['cost_price'];

                // insert report order
                $user_id = 1; // Admin
                $store_id = 0;
                $total_amount = (int)$product['quantity'] * (float)$product['price'];
                $sql = "INSERT INTO ". $db_prefix . "report_product SET report_time = '". $report_time ."'";
                $sql .= ", report_date = '" . $report_date . "'";
                $sql .= ", order_id = '" . (int)$bestme_order_id . "'";
                $sql .= ", product_id = '" . (int)$product['product_id'] . "'";
                $sql .= ", product_version_id = '" . (int)$product['product_version_id'] . "'";
                $sql .= ", quantity = '" . (int)$product['quantity'] . "'";
                $sql .= ", price = '" . (float)$product['price'] . "'";
                $sql .= ", total_amount = '" . (float)$total_amount . "'";
                $sql .= ", discount = '" . (float)$discount . "'";
                $sql .= ", cost_price = '" . (float)$cost_price . "'";
                $sql .= ", store_id = '" . $store_id . "'";
                $sql .= ", user_id = '" . (int)$user_id . "'";
                $db->query($sql);
            }
        }

        return true;
    }

    private function getAdministrativeCodeByTextValue($administrative_text, $parent_code = null)
    {
        return (new \Vietnam_Administrative())->getAdministrativeCodeByTextValue($administrative_text, $parent_code);
    }

    private function getCustomerByTelephone(DB $db, $db_prefix, $data)
    {
        $customer = $db->query("SELECT DISTINCT * FROM `{$db_prefix}customer` WHERE telephone = '" . $db->escape($data['phone']) . "'");
        if (is_array($customer->row) && isset($customer->row['customer_id'])) {
            return $customer->row['customer_id'];
        }

        return $this->createNewCustomer($db, $db_prefix, $data);
    }

    private function createNewCustomer(DB $db, $db_prefix, $data)
    {
        $customer = [
            'firstname' => isset($data['fullname']) ? extract_name($data['fullname'])[0] : '',
            'lastname' => isset($data['fullname']) ? extract_name($data['fullname'])[1] : '',
            'email' => '',
            'telephone' => $data['phone']
        ];
        $db->query("INSERT INTO `{$db_prefix}customer` 
                                            SET  `firstname` = '" . $db->escape($customer['firstname']) . "', 
                                            `lastname` = '" . $db->escape($customer['lastname']) . "', 
                                            `email` = '" . $db->escape($customer['email']) . "', 
                                            `telephone` = '" . $db->escape($customer['telephone']) . "', 
                                            `salt` = '" . $db->escape($salt = 'lazada') . "', 
                                            `password` = '" . $db->escape(sha1($salt . sha1($salt . sha1('lazada')))) . "', 
                                            `status` = 1,  
                                            `date_added` = NOW()");
        $customer_id = $db->getLastId();
        // update customer code
        $prefix = 'KH';
        $code = $prefix . str_pad($customer_id, 6, "0", STR_PAD_LEFT);
        $db->query("UPDATE " . DB_PREFIX . "customer SET `code` = '" . $code . "' WHERE customer_id = '" . (int)$customer_id . "'");

        $address_data = array(
            'customer_id' => $customer_id,
            'firstname' => $customer['firstname'],
            'lastname' => $customer['lastname'],
            'company' => '',
            'city' => $data['province'],
            'district' => $data['district'],
            'wards' => $data['ward'],
            'phone' => $customer['telephone'],
            'postcode' => '',
            'address' => $data['full_address'],
            'default' => true
        );

        $db->query("INSERT INTO `{$db_prefix}address` SET 
                                              customer_id = '" . (int)$customer_id . "', 
                                              firstname = '" . $db->escape($address_data['firstname']) . "', 
                                              lastname = '" . $db->escape($address_data['lastname']) . "', 
                                              company = '" . $db->escape($address_data['company']) . "', 
                                              district = '" . $db->escape($address_data['district']) . "', 
                                              address = '" . $db->escape($address_data['address']) . "', 
                                              phone = '" . $db->escape($address_data['phone']) . "', 
                                              postcode = '" . $db->escape($address_data['postcode']) . "', 
                                              city = '" . $db->escape($address_data['city']) . "'");

        $address_id = $db->getLastId();
        $db->query("UPDATE `{$db_prefix}customer` SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");

        // create Customer Report
        $reportTime = new DateTime();
        $reportTime->setTime($reportTime->format('H'), 0, 0);
        $sql = "INSERT INTO `{$db_prefix}report_customer` SET report_time = '" . $reportTime->format('Y-m-d H:i:s') . "'";
        $sql .= ", report_date = '" . $reportTime->format('Y-m-d') . "'";
        $sql .= ", customer_id = '" . (int)$customer_id . "'";
        $db->query($sql);

        return $customer_id;
    }

    private function autoUpdateReceiptVoucher($data, $customer_info)
    {

        $payment_status = isset($data['payment_status']) ? (int)$data['payment_status'] : 0;
        $dataToSave = [
            'payment_status' => $payment_status,
            'payment_method' => $data['payment_method'],
            'total_pay' => $data['total_amount'],
            'created_at' => isset($data['create_time']) ? $data['create_time'] : null,
            'updated_at' => isset($data['update_time']) ? $data['update_time'] : null,
            'customer_info' => $customer_info ? $customer_info : null,
        ];

        $receipt_voucher = $this->autoUpdateFromOrder($data['bestme_order_id'], $dataToSave);
    }

    private function autoRemoveReceiptVoucher($order_id)
    {
        try {
            $this->autoRemoveFromOrder($order_id);
            return;
        } catch (Exception $ex) {
            return;
        }
    }
    // End save order to admin
}