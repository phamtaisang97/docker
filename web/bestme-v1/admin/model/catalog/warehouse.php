<?php

class ModelCatalogWarehouse extends Model
{
    public function getWarehouses($data = array())
    {
        $sql = $this->getWarehousesSql($data);

        /* pagination */
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalWarehouses($filter_data = [])
    {
        $sqlAll = $this->getWarehousesSql($filter_data);
        $query = $this->db->query("SELECT COUNT(t.`product_id`) AS `total` FROM (" . $sqlAll . ") as t");

        return $query->row['total'];
    }

    public function getTotalValueStores($filter_store)
    {
        $this->load->model('user/user');
        $user_id = $this->user->getId();
        $filter_data = [];

        if (!$this->user->canAccessAll()) {
            $user_store_ids = $this->model_user_user->getStoreIdsOffUser($user_id);
            $filter_data['user_store_id'] = implode(",", $user_store_ids);
            $filter_data['current_user_id'] = (int)$user_id;
        }

        $sql_warehouses = $this->getWarehousesSql($filter_data);

        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT SUM(`quantity` * cost_price) AS total 
                FROM `{$DB_PREFIX}product_to_store` pts 
                JOIN ({$sql_warehouses}) AS w
                       ON (pts.product_id = w.product_id 
                           AND pts.product_version_id = w.product_version_id ";

        if ($filter_store == 'all') {
            $sql .= ") "; // close join-ON
        } else if (is_numeric($filter_store)) {
            $sql .= " AND pts.store_id = " . $filter_store . ") ";
        } else {
            $sql .= " AND pts.store_id = w.default_store_id) ";
        }

        $sql .= " WHERE `quantity` > 0";

        $query = $this->db->query($sql);

        if ($query->num_rows > 0) {
            return $query->row['total'];
        }

        return 0;
    }

    public function getTotalQuantityStores($filter_store)
    {
        $this->load->model('user/user');
        $user_id = $this->user->getId();
        $filter_data = [];

        if (!$this->user->canAccessAll()) {
            $user_store_ids = $this->model_user_user->getStoreIdsOffUser($user_id);
            $filter_data['user_store_id'] = implode(",", $user_store_ids);
            $filter_data['current_user_id'] = (int)$user_id;
        }

        $sql_warehouses = $this->getWarehousesSql($filter_data);

        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT SUM(`quantity`) AS total 
                FROM `{$DB_PREFIX}product_to_store` pts 
                JOIN ({$sql_warehouses}) AS w
                       ON (pts.product_id = w.product_id 
                           AND pts.product_version_id = w.product_version_id ";

        if ($filter_store == 'all') {
            $sql .= ") "; // close join-ON
        } else if (is_numeric($filter_store)) {
            $sql .= " AND pts.store_id = " . $filter_store . ") ";
        } else {
            $sql .= " AND pts.store_id = w.default_store_id) ";
        }

        $sql .= " WHERE `quantity` > 0";

        $query = $this->db->query($sql);

        if ($query->num_rows > 0) {
            return $query->row['total'];
        }

        return 0;
    }

    /**
     * @param array $filter_data
     * @param bool $for_default_store_only default true
     * @param bool $include_deleted default false - exclude deleted products and deleted product versions
     * @return string
     */
    public function getWarehousesSql($filter_data = [], $include_deleted = false)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT p.product_id, 
                       CASE WHEN pv.product_version_id IS NULL THEN p.sku ELSE pv.sku END AS sku, 
                       p.sale_on_out_of_stock,
                       CASE WHEN pv.product_version_id IS NULL THEN pd.name ELSE CONCAT(pd.name, ' ', pv.version) END AS product_name, 
                       IF(pv.product_version_id IS NULL, 0, pv.product_version_id) AS product_version_id,
                       CASE WHEN pv.product_version_id IS NULL THEN IF(p.price=0, p.compare_price, p.price) ELSE IF(pv.price=0, pv.compare_price, pv.price) END AS price_version, 
                       p.default_store_id, 
                       p.image 
                FROM  {$DB_PREFIX}product p 
                LEFT JOIN {$DB_PREFIX}product_description pd ON (pd.product_id = p.product_id)  
                LEFT JOIN {$DB_PREFIX}product_version pv ON (CASE WHEN p.multi_versions = 1 THEN p.product_id ELSE -1 END = pv.product_id)";

        if (isset($filter_data['filter_store']) && $filter_data['filter_store'] != '') {
            if ($filter_data['filter_store'] == 'all') {
                $sql .= " LEFT JOIN {$DB_PREFIX}product_to_store pts ON (pts.product_id = p.product_id AND CASE WHEN pv.product_version_id IS NULL THEN 0 ELSE pv.product_version_id END = pts.product_version_id)";
            } else if ($filter_data['filter_store'] == 'default') {
                $sql .= " LEFT JOIN {$DB_PREFIX}product_to_store pts ON (pts.product_id = p.product_id AND CASE WHEN pv.product_version_id IS NULL THEN 0 ELSE pv.product_version_id END = pts.product_version_id AND pts.store_id = p.default_store_id)"; // for default store only
            } else if (is_numeric($filter_data['filter_store'])) {
                $sql .= " INNER JOIN {$DB_PREFIX}product_to_store pts ON (pts.product_id = p.product_id AND CASE WHEN pv.product_version_id IS NULL THEN 0 ELSE pv.product_version_id END = pts.product_version_id AND pts.store_id = " . $filter_data['filter_store'] . ")";
            }else {
                $sql .= " LEFT JOIN {$DB_PREFIX}product_to_store pts ON (pts.product_id = p.product_id AND CASE WHEN pv.product_version_id IS NULL THEN 0 ELSE pv.product_version_id END = pts.product_version_id AND pts.store_id = p.default_store_id)"; // for default store only
            }
        } else {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_to_store pts ON (pts.product_id = p.product_id AND CASE WHEN pv.product_version_id IS NULL THEN 0 ELSE pv.product_version_id END = pts.product_version_id AND pts.store_id = p.default_store_id)"; // for default store only
        }

        if (isset($filter_data['filter_manufacturer']) && $filter_data['filter_manufacturer'] != 0) {
            $sql .= " LEFT JOIN {$DB_PREFIX}manufacturer m ON (m.manufacturer_id = p.manufacturer_id)";
        }

        if (isset($filter_data['filter_category']) && $filter_data['filter_category'] != 0) {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_to_category c ON (p.product_id = c.product_id)";
        }

        if (isset($filter_data['filter_collection']) && $filter_data['filter_collection'] != 0) {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_collection pc ON (p.product_id = pc.product_id)";
        }

        if ((isset($filter_data['filter_tag']) && $filter_data['filter_tag'] != '') ||
            (isset($filter_data['filter_name']) && $filter_data['filter_name'] !== '')
        ) {
            $sql .= " LEFT JOIN {$DB_PREFIX}product_tag pt ON (pt.product_id = p.product_id) ";
            $sql .= " LEFT JOIN {$DB_PREFIX}tag t ON (t.tag_id = pt.tag_id) ";
        }

        $sql .= " WHERE 1=1 ";

        $sql .= " AND pd.language_id = " . (int)$this->config->get('config_language_id') . "";

        if (isset($filter_data['current_user_id']) && !empty($filter_data['current_user_id'])) {
            $sql .= ' AND p.user_create_id=' . $filter_data['current_user_id'];
        }

        if (isset($filter_data['user_store_id']) && $filter_data['user_store_id'] !== '') {
            $sql .= " AND pts.store_id IN (". $filter_data['user_store_id'] .")";
        }

        if (isset($filter_data['filter_status']) && $filter_data['filter_status'] !== '') {
            $sql .= ' AND p.status=' . $filter_data['filter_status'];
        }

        if (isset($filter_data['filter_manufacturer']) && $filter_data['filter_manufacturer'] != 0) {
            $sql .= ' AND m.manufacturer_id IN (' . $filter_data['filter_manufacturer'] . ')';
        }

        if (isset($filter_data['filter_category']) && $filter_data['filter_category'] != 0) {
            //$sql .= ' AND c.category_id IN (' . $filter_data['filter_category'] . ')';

            $this->load->model('catalog/category');
            $filter_cat_ids = explode(',', $filter_data['filter_category']);
            $all_related_cat_ids = [];
            foreach ($filter_cat_ids as $filter_cat_id) {
                $related_cat_ids = $this->model_catalog_category->getFullChildCategoryIds($filter_cat_id);
                $all_related_cat_ids = array_merge($all_related_cat_ids, [$filter_cat_id], $related_cat_ids);
            }
            $all_related_cat_ids = array_values(array_unique($all_related_cat_ids));

            if (!empty($all_related_cat_ids)) {
                $sql .= ' AND c.category_id IN (' . implode(',', $all_related_cat_ids) . ') ';
            }

        }

        if (isset($filter_data['filter_tag']) && $filter_data['filter_tag'] != '') {
            $sql .= " AND t.`value` RLIKE '" . $filter_data['filter_tag'] . "'";
        }

        if (isset($filter_data['filter_collection']) && $filter_data['filter_collection'] != 0) {
            $sql .= ' AND pc.collection_id IN (' . $filter_data['filter_collection'] . ')';
        }

        if (isset($filter_data['filter_name']) && $filter_data['filter_name'] !== '') {
            $sql .= " AND (pd.`name` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%' ";
            $sql .= " OR t.`value` LIKE '%" . $this->db->escape(trim($filter_data['filter_name'])) . "%' ) ";
        }

        if (isset($filter_data['filter_amount']) && $filter_data['filter_amount']) {
            $count = 0;
            $sql .= " AND ( ";
            foreach ($filter_data['filter_amount'] as $amounts) {
                $amount = explode('_', $amounts);
                $quantity = isset($amount[1]) ? $amount[1] : 0;

                if ($amount[0] == 1) {
                    if ($count == 0) {
                        $sql .= " pts.quantity > " . (int)$quantity;
                    } else {
                        $sql .= " OR pts.quantity > " . (int)$quantity;
                    }
                }

                if ($amount[0] == 2) {
                    if ($count == 0) {
                        $sql .= " pts.quantity < " . (int)$quantity;
                    } else {
                        $sql .= " OR pts.quantity < " . (int)$quantity;
                    }
                }

                if ($amount[0] == 3) {
                    if ($count == 0) {
                        $sql .= " pts.quantity = " . (int)$quantity;
                    } else {
                        $sql .= " OR pts.quantity = " . (int)$quantity;
                    }
                }
                $count++;
            }
            $sql .= " ) ";
        }

        // exclude deleted if set
        if (!$include_deleted) {
            $sql .= " AND p.`deleted` IS NULL ";
            $sql .= " AND pv.`deleted` IS NULL ";
        }

        $sql = $sql . " GROUP BY p.product_id, pv.product_version_id";
        $sql = $sql . " ORDER BY p.date_modified DESC";

        return $sql;
    }

    public function getQtyGoodsAreComing($productInfo, $filter_store)
    {
        if (is_null($productInfo['product_version_id'])) {
            $pvi_str = '(srtp.product_version_id IS NULL or srtp.product_version_id = 0)';
        }
        else{
            $pvi_str = 'srtp.product_version_id = '.(int)$productInfo['product_version_id'];
        }
        $sql = "SELECT SUM(srtp.quantity) AS quantity from  " . DB_PREFIX . "store_receipt_to_product srtp 
                LEFT JOIN " . DB_PREFIX . "store_receipt sr ON (srtp.store_receipt_id = sr.store_receipt_id)";
        $sql.= " WHERE srtp.product_id = ".(int)$productInfo['product_id'];
        $sql.= " AND ".$pvi_str;
        $sql.= " AND sr.status = 0 ";
        if ($filter_store == 'default') {
            $default_store_id = isset($productInfo['default_store_id']) ? $productInfo['default_store_id'] : 0;
            $sql .= " AND sr.store_id = " . $default_store_id;
        } else if (is_numeric($filter_store)) {
            $sql .= " AND sr.store_id = " . $filter_store;
        }
        $query = $this->db->query($sql);
        return $query->row['quantity'];
    }

    public function getInfoFromProductToStore($productInfo, $filter_store)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT quantity, cost_price FROM {$DB_PREFIX}product_to_store 
                WHERE product_id = " . (int)$productInfo['product_id'] . " 
                  AND product_version_id = " . (int)$productInfo['product_version_id'];

        if ($filter_store == 'default') {
            $sql .= " AND store_id = " . (int)$productInfo['default_store_id'];
        } else if (is_numeric($filter_store)) {
            $sql .= " AND store_id = " . $filter_store;
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function countProductOutOfStock($store_id = '')
    {
        $sql = "SELECT p.`default_store_id`, p.`product_id` as product_id, IFNULL(pv.`product_version_id`, 0) as product_version_id FROM `" . DB_PREFIX ."product` p LEFT JOIN `" . DB_PREFIX . "product_version` pv ON p.`product_id` = pv.`product_id` 
                                                                                     WHERE p.`deleted` IS NULL AND pv.`deleted` IS NULL";
        $sqlStore = "SELECT count(*) as count  FROM (" . $sql . ") ppv LEFT JOIN `" . DB_PREFIX . "product_to_store` pts ON ppv.`product_id` = pts.`product_id` AND ppv.`product_version_id` = pts.`product_version_id` 
                                                        WHERE pts.`quantity` <= 0";
        if ($store_id == ''){
            $sqlStore .= " AND pts.`store_id` = ppv.`default_store_id`";
        }else{
            $sqlStore .= " AND pts.`store_id` = " . (int)$store_id;
        }

        $query = $this->db->query($sqlStore);

        return isset($query->row['count']) ? (int)$query->row['count'] : 0;
    }

    public function editWarehouseSaleOnOutOfStock($product_id, $data)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product 
                            SET sale_on_out_of_stock = " . (int)$data['sale_on_out_of_stock'] . " 
                            WHERE product_id = " . (int)$product_id . "");
    }

    public function editWarehousesSaleOnOutOfStock($product_ids, $data)
    {
        try {
            $query = "UPDATE " . DB_PREFIX . "product SET sale_on_out_of_stock=" . (int)$data['sale_on_out_of_stock'] . " 
            WHERE product_id IN(" . $product_ids . ")";
            $this->db->query($query);
        } catch (Exception $e) {
        }
    }

    public function editProductDefaultStoreQuantity($product_id, $product_version_id, $quantity, $quantity_method = 'set')
    {
        $default_store_id = $this->getDefaultStoreIdByProductId($product_id);
        if ($default_store_id === '') {
            return;
        }

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_to_store` WHERE `product_id` = " . (int)$product_id
            . " AND `product_version_id` = " . (int)$product_version_id
            . " AND `store_id` = " . (int)$default_store_id);
        if ($query->num_rows > 0) {
            $data = $query->row;
            if ($quantity_method == 'add') {
                $quantity = (int)$data['quantity'] + (int)$quantity;
            } else { // == 'set
                $quantity = (int)$quantity;
            }

            $this->db->query("UPDATE `" . DB_PREFIX . "product_to_store` 
                              SET `quantity` = " . $quantity . " 
                              WHERE `product_id` = " . (int)$product_id . " 
                                AND `store_id` = " . (int)$default_store_id . " 
                                AND `product_version_id` = " . (int)$product_version_id);
        } else {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "product_to_store` 
                              SET `product_id` = " . (int)$product_id . ", 
                                  `store_id` = " . (int)$default_store_id . ", 
                                  `product_version_id` = " . (int)$product_version_id . ", 
                                  `quantity` = " . (int)$quantity . ", 
                                  `cost_price` = 0");
        }
    }

    private function getDefaultStoreIdByProductId($product_id)
    {
        $query = $this->db->query("SELECT default_store_id FROM `" . DB_PREFIX . "product` WHERE `product_id` = " . (int)$product_id);

        if ($query->num_rows > 0) {
            return (int)$query->row['default_store_id'];
        }

        return '';
    }

    /**
     * delete product caches
     */
    public function deleteProductCaches() {
        $product_key_redis =  DB_PREFIX . 'product';
        $this->cache->delete($product_key_redis);
    }

}
