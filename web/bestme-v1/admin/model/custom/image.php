<?php

class ModelCustomImage extends Model
{
    public function addImage($data)
    {
        if (!array_key_exists('url', $data) || !array_key_exists('source_id', $data) || !array_key_exists('name', $data) || !array_key_exists('folder_path', $data)) {
            return;
        }

        $source = isset($data['source']) ? $data['source'] : 'Bestme';

        $size = isset($data['size']) ? $data['size'] : 0;

        $this->db->query("INSERT INTO `" . DB_PREFIX . "images` SET 
                                `name` = '" . $this->db->escape($data['name']) . "', 
                                `url` = '" . $this->db->escape($data['url']) . "', 
                                `source` = '" . $this->db->escape($source) . "', 
                                `source_id` = '" . $this->db->escape($data['source_id']) . "', 
                                `size` = '" . $this->db->escape($size) . "',
                                `folder_path` = '" . $this->db->escape($data['folder_path']) . "'");
        return;
    }

    public function getImages($filter)
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "images` AS img WHERE 1";

        if (isset($filter['image_folder'])) {
            $sql .= " AND folder_path = ". $filter['image_folder'];
        }

        if (isset($filter['status']) && $filter['status'] !== '') {
            $sql .= " AND `status` = " . $filter['status'];
        }

        if (isset($filter['name'])) {
            $sql .= " AND img.`name` LIKE '%" . $filter['name'] . "%'";
        }

        if (isset($filter['start']) || isset($filter['limit'])) {
            if ($filter['start'] < 0) {
                $filter['start'] = 0;
            }

            if ($filter['limit'] < 1) {
                $filter['limit'] = 20;
            }
        } else {
            $filter['start'] = 0;
            $filter['limit'] = 20;
        }

        $sql .= " ORDER BY img.`image_id` DESC LIMIT " . (int)$filter['start'] . "," . (int)$filter['limit'];

        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function countImages($filter = [])
    {
        $sql = "SELECT count(*) as `total` FROM `" . DB_PREFIX . "images` WHERE 1";

        if (isset($filter['image_folder'])) {
            $sql .= " AND folder_path = ". $filter['image_folder'];
        }

        if (isset($filter['status']) && $filter['status'] !== '') {
            $sql .= " AND `status` = " . $filter['status'];
        }

        if (isset($filter['name'])) {
            $sql .= " AND `name` LIKE '%" . $filter['name'] . "%'";
        }

        $result = $this->db->query($sql);

        return $result->row['total'];
    }

    public function disableImages($image_ids)
    {
        if (!is_array($image_ids) || empty($image_ids)) {
            return;
        }

        $image_ids = implode(",", $image_ids);
        $this->db->query("UPDATE `" . DB_PREFIX . "images` SET `status` = 0 WHERE `image_id` IN (" . $image_ids . ")");

        return;
    }

    public function enableImages($image_ids)
    {
        if (!is_array($image_ids) || empty($image_ids)) {
            return;
        }

        $image_ids = implode(",", $image_ids);
        $this->db->query("UPDATE `" . DB_PREFIX . "images` SET `status` = 1 WHERE `image_id` IN (" . $image_ids . ")");

        return;
    }

    public function getSourceIds($image_ids)
    {
        if (!is_array($image_ids) || empty($image_ids)) {
            return [];
        }

        $image_ids = implode(",", $image_ids);
        $result = $this->db->query("SELECT `image_id`, `url`, `source`, `source_id` FROM `" . DB_PREFIX . "images` WHERE `image_id` IN (" . $image_ids . ")");

        return $result->rows;
    }

    public function deleteImages($image_ids)
    {
        if (!is_array($image_ids) || empty($image_ids)) {
            return;
        }

        $image_ids = implode(",", $image_ids);
        $this->db->query("DELETE FROM `" . DB_PREFIX . "images` WHERE `image_id` IN (" . $image_ids . ")");

        return;
    }

    public function deleteImageById($image_id)
    {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "images` WHERE `image_id` = " . $image_id);
    }

    public function getTotalSizeImages()
    {
        $sql = "SELECT SUM(size) as total FROM `" . DB_PREFIX . "images`";
        $result = $this->db->query($sql);
        return $result->row['total'] ;
    }

    public function getImagesByFolders($folder_ids) {
        $result = [];

        if (!is_array($folder_ids) || empty($folder_ids)) {
            return $result;
        }

        $folder_ids = implode(",", $folder_ids);
        $sql = "SELECT `image_id` FROM `" . DB_PREFIX . "images` WHERE `folder_path` IN (" . $folder_ids . ")";
        $query = $this->db->query($sql);

        foreach ($query->rows as $row) {
            $result[] = $row['image_id'];
        }

        return $result;
    }

    public function moveImages($move_folder, $image_ids) {
        if (!is_array($image_ids) || empty($image_ids)) {
            return false;
        }

        $DB_PREFIX = DB_PREFIX;
        $image_ids = implode(',', $image_ids);
        $sql = "UPDATE `{$DB_PREFIX}images` SET 
                        `folder_path` = " . (int)$move_folder . " 
                        WHERE `image_id` IN ({$image_ids})";

        $this->db->query($sql);
    }
}
