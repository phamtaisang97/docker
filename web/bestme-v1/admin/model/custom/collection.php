<?php
class ModelCustomCollection extends Model {

    public function getAllTagValueProduct(){
        $sql = "SELECT c.collection_id, c.title from ". DB_PREFIX ."collection as c ORDER BY c.title ASC";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getCollectionPaginator($data = array()) {
        $sql = "SELECT c.collection_id, c.title from ". DB_PREFIX ."collection as c WHERE 1=1";
        if (!empty($data['filter_name'])) {
            $sql .= " AND c.title LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }
        if(!$this->user->canAccessAll()) {
            $sql .= " AND c.user_create_id = " . $this->user->getId();
        }
        if (isset($data['filter_type'])){
            $sql .= " AND c.type = " . (int)$data['filter_type'];
        }
        $sql .= " ORDER BY c.title ASC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function countCollection($data = array()) {
        $sql = "SELECT c.collection_id, c.title from " . DB_PREFIX . "collection as c WHERE 1=1";
        if (!empty($data['filter_name'])) {
            $sql .= " AND c.title LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }
        if(!$this->user->canAccessAll()) {
            $sql .= " AND c.user_create_id = " . $this->user->getId();
        }
        if (isset($data['filter_type'])) {
            $sql .= " AND c.type = " . (int)$data['filter_type'];
        }
        $sql .= " ORDER BY c.title ASC";
        $query = $this->db->query($sql);

        return count($query->rows);
    }

    public function insertCollectionProduct($product_ids, $collection_list) {
        $values = '';
        $this->deleteCollectionProduct($product_ids, $collection_list);
        foreach ($product_ids as $key => $product) {
            foreach ($collection_list as $key_t => $collection) {
                if ($key == count($product_ids) -1 && $key_t == count($collection_list)-1) {
                    $values .= "(".preg_replace( '/[^0-9]/', '', $product ).",".$collection.")";
                } else {
                    $values .= "(".preg_replace( '/[^0-9]/', '', $product ).",".$collection."),";
                }
            }
        }
        $sql_update = "INSERT INTO ". DB_PREFIX . "product_collection(product_id, collection_id) VALUES ".$values;
        $this->db->query($sql_update);
    }

    public function deleteCollectionProduct($product_ids, $collection_list) {
        $values_product = '';
        $values_collection = '';

        foreach ($product_ids as $key => $product) {
            if ($key == count($product_ids)-1) {
                $values_product .= preg_replace( '/[^0-9]/', '', $product );
            } else {
                $values_product .= preg_replace( '/[^0-9]/', '', $product ).',';
            }
        }
        foreach ($collection_list as $key_t => $collection) {
            if ($key_t == count($collection_list)-1) {
                $values_collection .= $collection;
            } else {
                $values_collection .= $collection.',';
            }
        }
        $sql_delete = "DELETE FROM ".DB_PREFIX. "product_collection WHERE product_id IN (".$values_product.") AND collection_id IN (" .$values_collection. ")";
        $this->db->query($sql_delete);
    }

    public function getCollectionsByProductId($product_id) {
        $query = "SELECT c.collection_id as id , c.title as title FROM ". DB_PREFIX ."product_collection as pc INNER JOIN ". DB_PREFIX ."collection as c ON c.collection_id = pc.collection_id WHERE pc.product_id=".(int)$product_id." ORDER BY c.title ASC";
        $results = $this->db->query($query);
        return $results->rows;
    }

    public function getCollectionTitleById($collection_id)
    {
        $query = "SELECT title FROM " . DB_PREFIX . "collection WHERE collection_id =" . (int)$collection_id;
        $result = $this->db->query($query);
        if (isset($result->row['title'])){
            return $result->row['title'];
        }

        return null;
    }
}