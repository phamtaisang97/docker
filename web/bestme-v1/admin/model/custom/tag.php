<?php
class ModelCustomTag extends Model {

    public function getAllTagValueProduct(){
        $sql = "SELECT t.tag_id, t.value from ". DB_PREFIX ."tag as t ORDER BY t.value ASC";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getTagPaginator($data = array()) {
        $sql = "SELECT t.tag_id, t.value from ". DB_PREFIX ."tag as t WHERE 1=1";
        if (!empty($data['filter_name'])) {
            $sql .= " AND t.value LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }
        if (isset($data['sort']) && $data['sort'] == 'tag_id') {
            $sql .= " ORDER BY t.tag_id DESC";
        } else {
            $sql .= " ORDER BY t.value ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function countTag($data = array()) {
        $sql = "SELECT t.tag_id, t.value from ". DB_PREFIX ."tag as t WHERE 1=1";
        if (!empty($data['filter_name'])) {
            $sql .= " AND t.value LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }
        $sql .= " ORDER BY t.value ASC";
        $query = $this->db->query($sql);

        return count($query->rows);
    }

    public function insertTag($tag_list) {
        $list_add = [];
        foreach ($tag_list as $key_t => $tag) {
            $sql_search = "SELECT tag_id FROM ". DB_PREFIX ."tag WHERE tag_id='".$this->db->escape($tag)."'";
            $data = $this->db->query($sql_search);
            if (!$data->row) {
                $sql_update = "INSERT INTO ". DB_PREFIX . "tag (value) VALUES ('". $this->db->escape($tag) ."')";
                $this->db->query($sql_update);
                $list_add[$this->db->getLastId()] = $tag;
            }
        }
        return $list_add;
    }

    public function insertTagProduct($product_ids, $tag_list) {
        $values = '';
        $this->deleteTagProduct($product_ids, $tag_list);
        foreach ($product_ids as $key => $product) {
            foreach ($tag_list as $key_t => $tag) {
                if ($key == count($product_ids) -1 && $key_t == count($tag_list)-1) {
                    $values .= "(".preg_replace( '/[^0-9]/', '', $product ).",".$tag.")";
                } else {
                    $values .= "(".preg_replace( '/[^0-9]/', '', $product ).",".$tag."),";
                }
            }
        }
        $sql_update = "INSERT INTO ". DB_PREFIX . "product_tag(product_id, tag_id) VALUES ".$values;
        $this->db->query($sql_update);
    }

    public function deleteTagProduct($product_ids, $tag_list) {
        $values_product = '';
        foreach ($product_ids as $key => $product) {
            if ($key == count($product_ids)-1) {
                $values_product .= preg_replace( '/[^0-9]/', '', $product );
            } else {
                $values_product .= preg_replace( '/[^0-9]/', '', $product ).',';
            }
        }
        foreach ($tag_list as $key_t => $tag) {
            $sql_search = "SELECT tag_id FROM ". DB_PREFIX ."tag WHERE tag_id='".$this->db->escape($tag)."'";
            $data = $this->db->query($sql_search);
            if (!$data->row) {
                $sql_delete = "DELETE pt FROM ".DB_PREFIX. "product_tag as pt LEFT JOIN ". DB_PREFIX ."tag as t ON t.tag_id = pt.tag_id WHERE pt.product_id IN (".$values_product.") AND t.value = '" .$this->db->escape($tag). "'";
                $this->db->query($sql_delete);
            } else {
                $sql_delete = "DELETE FROM ".DB_PREFIX. "product_tag WHERE product_id IN (".$values_product.") AND tag_id = '" .$this->db->escape($tag). "'";
                $this->db->query($sql_delete);
            }
        }
    }

    public function insertTagOrder($product_ids, $tag_list) {
        $values = '';
        $this->deleteTagOrder($product_ids, $tag_list);
        foreach ($product_ids as $key => $product) {
            foreach ($tag_list as $key_t => $tag) {
                if ($key == count($product_ids) -1 && $key_t == count($tag_list)-1) {
                    $values .= "(".preg_replace( '/[^0-9]/', '', $product ).",".$tag.")";
                } else {
                    $values .= "(".preg_replace( '/[^0-9]/', '', $product ).",".$tag."),";
                }
            }
        }
        $sql_update = "INSERT INTO ". DB_PREFIX . "order_tag(order_id, tag_id) VALUES ".$values;
        $this->db->query($sql_update);
    }

    public function insertTagOnlyOrder($order_id, $tag_list) {
        $this->deleteTag($order_id);
        foreach ($tag_list as $tag) {
            $sql_update = "INSERT INTO ". DB_PREFIX . "order_tag(order_id, tag_id) VALUES (".$order_id.','.$tag.")";
            $this->db->query($sql_update);
        }
    }

    public function deleteTag($order_id) {
        $sql_delete = "DELETE FROM ".DB_PREFIX. "order_tag WHERE order_id ='". $order_id ."'";
        $this->db->query($sql_delete);
    }

    public function deleteTagOrder($product_ids, $tag_list) {
        $values_product = '';
        foreach ($product_ids as $key => $product) {
            if ($key == count($product_ids)-1) {
                $values_product .= preg_replace( '/[^0-9]/', '', $product );
            } else {
                $values_product .= preg_replace( '/[^0-9]/', '', $product ).',';
            }
        }

        foreach ($tag_list as $key_t => $tag) {
            $sql_search = "SELECT tag_id FROM ". DB_PREFIX ."tag WHERE tag_id='".$this->db->escape($tag)."'";
            $data = $this->db->query($sql_search);
            if (!$data->row) {
                $sql_delete = "DELETE ot FROM ".DB_PREFIX. "order_tag as ot LEFT JOIN ". DB_PREFIX ."tag as t ON t.tag_id = ot.tag_id WHERE ot.order_id IN (".$values_product.") AND t.value = '" .$this->db->escape($tag). "'";
                $this->db->query($sql_delete);
            } else {
                $sql_delete = "DELETE FROM ".DB_PREFIX. "order_tag WHERE order_id IN (".$values_product.") AND tag_id = '" .$this->db->escape($tag). "'";
                $this->db->query($sql_delete);
            }
        }
    }

    public function getTagsByProductId($product_id) {
        $query = "SELECT t.tag_id as id , t.value as title FROM ". DB_PREFIX ."product_tag as pt INNER JOIN ". DB_PREFIX ."tag as t ON t.tag_id = pt.tag_id WHERE pt.product_id=".(int)$product_id." ORDER BY t.value ASC";
        $results = $this->db->query($query);
        return $results->rows;
    }

    public function getTagsByOrderId($order_id) {
        $query = "SELECT t.tag_id as id , t.value as title FROM ". DB_PREFIX ."order_tag as ot INNER JOIN ". DB_PREFIX ."tag as t ON t.tag_id = ot.tag_id WHERE ot.order_id=".(int)$order_id." ORDER BY t.value ASC";
        $results = $this->db->query($query);
        return $results->rows;
    }

    public function addTagFast($text) {
        $sql_update = "INSERT INTO ". DB_PREFIX . "tag(value) VALUES ('". $this->db->escape($text) ."')";
        $this->db->query($sql_update);
        return $this->db->getLastId();
    }

    public function getTagById($tag_id)
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "tag WHERE tag_id = " . $tag_id . " ";
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getTagsByBlogId($blog_id) {
        $query = "SELECT t.tag_id as id , t.value as title FROM ". DB_PREFIX ."blog_tag as bt INNER JOIN ". DB_PREFIX ."tag as t ON t.tag_id = bt.tag_id WHERE bt.blog_id=".(int)$blog_id." ORDER BY t.value ASC";
        $results = $this->db->query($query);
        return $results->rows;
    }

    public function insertTagBlog($blog_ids, $tag_list) {
        $values = '';
        $this->deleteTagBlog($blog_ids, $tag_list);
        foreach ($blog_ids as $key => $blog) {
            foreach ($tag_list as $key_t => $tag) {
                if ($key == count($blog_ids) -1 && $key_t == count($tag_list)-1) {
                    $values .= "(".preg_replace( '/[^0-9]/', '', $blog ).",".$tag.")";
                } else {
                    $values .= "(".preg_replace( '/[^0-9]/', '', $blog ).",".$tag."),";
                }
            }
        }
        $sql_update = "INSERT INTO ". DB_PREFIX . "blog_tag(blog_id, tag_id) VALUES ".$values;
        $this->db->query($sql_update);
    }
    
    public function deleteTagBlog($blog_ids, $tag_list) {
        $values_blog = '';
        foreach ($blog_ids as $key => $blog) {
            if ($key == count($blog_ids)-1) {
                $values_blog .= preg_replace( '/[^0-9]/', '', $blog );
            } else {
                $values_blog .= preg_replace( '/[^0-9]/', '', $blog ).',';
            }
        }
        foreach ($tag_list as $key_t => $tag) {
            $sql_search = "SELECT tag_id FROM ". DB_PREFIX ."tag WHERE tag_id='".$this->db->escape($tag)."'";
            $data = $this->db->query($sql_search);
            if (!$data->row) {
                $sql_delete = "DELETE pt FROM ".DB_PREFIX. "blog_tag as pt LEFT JOIN ". DB_PREFIX ."tag as t ON t.tag_id = pt.tag_id WHERE pt.blog_id IN (".$values_blog.") AND t.value = '" .$this->db->escape($tag). "'";
                $this->db->query($sql_delete);
            } else {
                $sql_delete = "DELETE FROM ".DB_PREFIX. "blog_tag WHERE blog_id IN (".$values_blog.") AND tag_id = '" .$this->db->escape($tag). "'";
                $this->db->query($sql_delete);
            }
        }
    }

    public function getTagByName($filter)
    {
        $sql = "SELECT * FROM ". DB_PREFIX . "tag WHERE 1=1 ";
        if (isset($filter['name'])) {
            $sql .= " AND UPPER(value) = '". $this->db->escape(strtoupper($filter['name'])) ."'";
        }
        if (isset($filter['id']) && $filter['id'] != '') {
            $sql .= " AND tag_id != '". (int)$filter['id'] ."'";
        }
        $query = $this->db->query($sql);
        return $query->row;
    }

    public function editTagName($data)
    {
        $sql = "UPDATE ". DB_PREFIX ."tag SET value = '". $this->db->escape($data['name']) ."'";
        $sql .= " WHERE tag_id = '". (int)$data['id'] ."'";
        $this->db->query($sql);
    }

    public function deleteTagAll($tag_id)
    {
        $this->db->query("DELETE FROM ". DB_PREFIX ."tag WHERE tag_id = '". (int)$tag_id ."'");
        $this->db->query("DELETE FROM ". DB_PREFIX ."blog_tag WHERE tag_id = '". (int)$tag_id ."'");
        $this->db->query("DELETE FROM ". DB_PREFIX ."order_tag WHERE tag_id = '". (int)$tag_id ."'");
        $this->db->query("DELETE FROM ". DB_PREFIX ."product_tag WHERE tag_id = '". (int)$tag_id ."'");
        $this->deleteProductCaches();
    }

    /**
     * delete product caches
     */
    private function deleteProductCaches() {
        $product_key_redis =  DB_PREFIX . 'product';
        $this->cache->delete($product_key_redis);
    }
}