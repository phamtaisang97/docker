<?php
class ModelCustomMenuItem extends Model{
   
    public function getNameRecordById($id, $tableName, $name = null)
    {
    	$language_id = $this->config->get('config_language_id');
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX .'novaon_'. $tableName. "_description WHERE menu_item_id = '" . (int)$id . "' AND language_id = '". $language_id ."'");
        if ($name) {
            return $query->row[$name];
        }
        if (isset($query->row['name'])) {
            return $query->row['name'];
        }
        return null;
    }
    public function getNameFieldOld($data, $key)
    {
        if (isset($data['category_id_old'][$key])) {
            return 'category_id_old';
        }
        if (isset($data['product_choose_old'][$key])) {
            return 'product_choose_old';
        }
        if (isset($data['url_old'][$key])) {
            return 'url_old';
        }   
        return null;
    }

    public function edit($menu_item_id, $data)
    {
        $this->load->model('custom/relation_table');
        if (isset($data['product_choose_old'][$menu_item_id])) {
            //xoa relation cua menu_item
            $sql = "
                DELETE  FROM " .DB_PREFIX. "novaon_relation_table WHERE main_id = '".(int)$menu_item_id."' AND main_name = 'menu_item' AND redirect = 1
            ";
            $this->db->query($sql);
            $sqlGroupMenuUpdate = "
                UPDATE " . DB_PREFIX . "novaon_relation_table SET type_id = 1 WHERE id = '" . (int)$data['menu_item_old_menu'][$menu_item_id] . "' 
            ";
            $this->db->query($sqlGroupMenuUpdate);
            //tao moi
            $dataMenuItem['main_name'] = 'menu_item';
            $dataMenuItem['main_id'] = $menu_item_id;
            $dataMenuItem['child_name'] = 'product';
            $dataMenuItem['child_id'] = $data['product_choose_old'][$menu_item_id];
            $dataMenuItem['type_id'] = 1;
            $dataMenuItem['url'] = '';
            $dataMenuItem['redirect'] = 1;
            $this->model_custom_relation_table->add($dataMenuItem); 
        }
        if (isset($data['category_id_old'][$menu_item_id])) {
            //xoa relation cua menu_item
            $sql = "
                DELETE  FROM " .DB_PREFIX. "novaon_relation_table WHERE main_id = '".(int)$menu_item_id."' AND main_name = 'menu_item' AND redirect = 1
            ";
            $this->db->query($sql);
            //tao moi
            $sqlGroupMenuUpdate = "
                UPDATE " . DB_PREFIX . "novaon_relation_table SET type_id = 2 WHERE id = '" . (int)$data['menu_item_old_menu'][$menu_item_id] . "' 
            ";
            $this->db->query($sqlGroupMenuUpdate);

            $dataMenuItem['main_name'] = 'menu_item';
            $dataMenuItem['main_id'] = $menu_item_id;
            $dataMenuItem['child_name'] = 'category';
            $dataMenuItem['child_id'] = $data['category_id_old'][$menu_item_id];
            $dataMenuItem['type_id'] = 2;
            $dataMenuItem['url'] = '';
            $dataMenuItem['redirect'] = 1;
            $this->model_custom_relation_table->add($dataMenuItem); 
        }
        if (isset($data['url_old'][$menu_item_id])) {

            //xoa relation cua menu_item
            $sql = "
                DELETE  FROM " .DB_PREFIX. "novaon_relation_table WHERE main_id = '".(int)$menu_item_id."' AND main_name = 'menu_item' AND redirect = 1
            ";
            $this->db->query($sql);
            //tao moi
            $sqlGroupMenuUpdate = "
                UPDATE " . DB_PREFIX . "novaon_relation_table SET type_id = 5, redirect =1, url = '".$data['url_old'][$menu_item_id]."' WHERE id = '" . (int)$data['menu_item_old_menu'][$menu_item_id] . "' 
            ";
            $this->db->query($sqlGroupMenuUpdate);

            $dataMenuItem['main_name'] = 'menu_item';
            $dataMenuItem['main_id'] = $menu_item_id;
            $dataMenuItem['child_name'] = '';
            $dataMenuItem['child_id'] = '';
            $dataMenuItem['type_id'] = 5;
            $dataMenuItem['url'] = $data['url_old'][$menu_item_id];
            $dataMenuItem['redirect'] = 1;
            $this->model_custom_relation_table->add($dataMenuItem); 
        }

        if (isset($data['url-old'][$menu_item_id])) {
            //xoa relation cua menu_item
            $sql = "
                DELETE  FROM " .DB_PREFIX. "novaon_relation_table WHERE main_id = '".(int)$menu_item_id."' AND main_name = 'menu_item' AND redirect = 1
            ";
            $this->db->query($sql);

            //tao moi
            $sqlGroupMenuUpdate = "
                UPDATE " . DB_PREFIX . "novaon_relation_table SET redirect =1, url = '".$data['url-old'][$menu_item_id]."' WHERE id = '" . (int)$data['menu_item_old_menu'][$menu_item_id] . "' 
            ";
            $this->db->query($sqlGroupMenuUpdate);

            $dataMenuItem['main_name'] = 'menu_item';
            $dataMenuItem['main_id'] = $menu_item_id;
            $dataMenuItem['child_name'] = '';
            $dataMenuItem['child_id'] = '';
            $dataMenuItem['type_id'] = 5;
            $dataMenuItem['url'] = $data['url-old'][$menu_item_id];
            $dataMenuItem['redirect'] = 1;
            $this->model_custom_relation_table->add($dataMenuItem); 
        }
        if (isset($data['category-id-old'][$menu_item_id])) {
            //xoa relation cua menu_item
            $sql = "
                DELETE  FROM " .DB_PREFIX. "novaon_relation_table WHERE main_id = '".(int)$menu_item_id."' AND main_name = 'menu_item' AND redirect = 1
            ";
            $this->db->query($sql);
            //tao moi
            $dataMenuItem['main_name'] = 'menu_item';
            $dataMenuItem['main_id'] = $menu_item_id;
            $dataMenuItem['child_name'] = 'category';
            $dataMenuItem['child_id'] = $data['category-id-old'][$menu_item_id];
            $dataMenuItem['type_id'] = 2;
            $dataMenuItem['url'] = '';
            $dataMenuItem['redirect'] = 1;
            $this->model_custom_relation_table->add($dataMenuItem); 
        }
        //TODO fix type_id = 6, child_name = collection
        if (isset($data['collection-id-old'][$menu_item_id])) {
            //xoa relation cua menu_item
            $sql = "
                DELETE  FROM " .DB_PREFIX. "novaon_relation_table WHERE main_id = '".(int)$menu_item_id."' AND main_name = 'menu_item' AND redirect = 1
            ";
            $this->db->query($sql);
            //tao moi
            $dataMenuItem['main_name'] = 'menu_item';
            $dataMenuItem['main_id'] = $menu_item_id;
            $dataMenuItem['child_name'] = 'collection';
            $dataMenuItem['child_id'] = $data['collection-id-old'][$menu_item_id];
            $dataMenuItem['type_id'] = 6;
            $dataMenuItem['url'] = '?route=common/shop&collection=' . $data['collection-id-old'][$menu_item_id];
            $dataMenuItem['redirect'] = 1;
            $this->model_custom_relation_table->add($dataMenuItem);
        }
    }

    public function add($name, $group_menu_id, $data, $key = null, $redirect = null)
    {
        $parent_id = 0;
        $status = 1;
        $position = 0;
        $sql = "INSERT INTO " . DB_PREFIX . "novaon_menu_item SET parent_id = '" . (int)$parent_id . "', status = '" . (int)$status . "', position = '".$position."', group_menu_id = '".(int)$group_menu_id."', updated_at = NOW(), created_at = NOW()";
        $this->db->query($sql);
        $menu_item_id = $this->db->getLastId();

        // create menu_item_description
        $this->createMenuItemDes($menu_item_id, $name);

        // create relation table: main_name = menu_item, main_id = menu_item_id, child_name depend on type_id
        $this->load->model('custom/relation_table');

        if (isset($data['type_id'])) {
            $dataMenuItem['main_name'] = 'menu_item';
            $dataMenuItem['main_id'] = $menu_item_id;
            $dataMenuItem['child_name'] = $this->model_custom_relation_table->getChildName($data['type_id'][$key]);
            $dataMenuItem['child_id'] = $this->model_custom_relation_table->getChildId($data, $key);
            $dataMenuItem['type_id'] = $data['type_id'][$key];
            $dataMenuItem['url'] = $this->model_custom_relation_table->getUrl($data, $key);
            if ($dataMenuItem['type_id'] == 6) {
                $dataMenuItem['url'] = '?route=common/shop&collection=' . $data['collection_id'][$key];
            }
            if ($redirect) {
                $dataMenuItem['redirect'] = $redirect;
            } else{
                $dataMenuItem['redirect'] = 1;
            }

            $this->model_custom_relation_table->add($dataMenuItem);
        }

        // create seo_url: create record in the seo_url table
        $slug = $this->model_custom_relation_table->getSlug($data, $key);
        $this->createMenuItemSeo($menu_item_id, $slug, 'menu_item');

        return $menu_item_id;
    }

    public function addCustomForCollection($name, $group_menu_id, $data, $key = null, $redirect = null)
    {
        $parent_id = 0;
        $status = 1;
        $position = 0;
        $sql = "INSERT INTO " . DB_PREFIX . "novaon_menu_item SET parent_id = '" . (int)$parent_id . "', status = '" . (int)$status . "', position = '".$position."', group_menu_id = '".(int)$group_menu_id."', updated_at = NOW(), created_at = NOW()";
        $this->db->query($sql);
        $menu_item_id = $this->db->getLastId();

        // create menu_item_description
        $this->createMenuItemDes($menu_item_id, $name);

        // create relation table: main_name = menu_item, main_id = menu_item_id, child_name depend on type_id
        $this->load->model('custom/relation_table');

        if (isset($data['type_id'])) {
            // add column menu_item
            $dataMenuItem['main_name'] = 'menu_item';
            $dataMenuItem['main_id'] = $menu_item_id;
            $dataMenuItem['child_name'] = $data['child_name'];
            $dataMenuItem['child_id'] = $data['child_id'];
            $dataMenuItem['type_id'] = $data['type_id'];
            $dataMenuItem['url'] = '';
            if ($redirect) {
                $dataMenuItem['redirect'] = $redirect;
            } else{
                $dataMenuItem['redirect'] = 1;
            }

            $this->model_custom_relation_table->add($dataMenuItem);

            // add column group
            // TODO remove back version
            $dataMenuItem['main_name'] = 'group_menu';
            $dataMenuItem['main_id'] = $group_menu_id;
            $dataMenuItem['child_name'] = "menu_item";
            $dataMenuItem['child_id'] = $menu_item_id;
            $dataMenuItem['type_id'] = $data['type_id'];
            $dataMenuItem['url'] = '';
            $dataMenuItem['redirect'] = 0;
            $this->model_custom_relation_table->add($dataMenuItem);

        }

        // create seo_url: create record in the seo_url table
//        $slug = $this->model_custom_relation_table->getSlug($data, $key);
//        $this->createMenuItemSeo($menu_item_id, $slug, 'menu_item');

        return $menu_item_id;
    }

    public function editChild($menu_item_id,$data)
    {
        $this->load->model('custom/relation_table');
        //xoa toan bo
        $sql = "
            DELETE FROM " . DB_PREFIX . "novaon_relation_table WHERE main_name = 'menu_item' AND main_id = '".$menu_item_id."' AND redirect = 0
        ";
        $this->db->query($sql);
        //tao moi
        if (isset($data['type_id'])) {
            foreach ($data['type_id'] as $key => $value) {
                $dataMenu['main_name'] = 'menu_item';
                $dataMenu['main_id'] = $menu_item_id;
                $dataMenu['child_id'] = $this->model_custom_relation_table->getChildId($data, $key);
                $dataMenu['child_name'] = $this->model_custom_relation_table->getChildName($value);
                $dataMenu['type_id'] = $value;
                $dataMenu['url'] = $this->model_custom_relation_table->getUrl($data, $key);
                $dataMenu['redirect'] = 0;
                $dataMenu['title'] = $data['title'][$key];

                $this->model_custom_relation_table->add($dataMenu);
            }
        }
        if (isset($data['custom_slug'])) {
            $slug = $data['custom_slug'];
            $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'menu_item_id=" . (int)$menu_item_id . "' AND table_name = 'menu_item'");
            $this->createMenuItemSeo($menu_item_id, $slug, 'menu_item');
        }
    }
    
    public function createMenuItemDes($menu_item_id, $name)
    {
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        foreach ($languages as $key => $value) {
            $language_id = $value['language_id'];
                $this->db->query("INSERT INTO " . DB_PREFIX . "novaon_menu_item_description SET menu_item_id = '" . (int)$menu_item_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($name) . "'");
        }
    }
    public function updateMenuItemDes($menu_item_id, $name)
    {
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        foreach ($languages as $key => $value) {
            $language_id = $value['language_id'];
                $this->db->query("UPDATE " . DB_PREFIX . "novaon_menu_item_description SET name = '" . $this->db->escape($name) . "' WHERE menu_item_id = '" . (int)$menu_item_id . "' AND language_id = '" . (int)$language_id . "'");
        }
        //update seo
        $this->load->model('custom/common');
        $slug = $this->model_custom_common->createSlug($name, '-');
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'menu_item_id=" . (int)$menu_item_id . "' AND table_name = 'menu_item'");
        $this->createMenuItemSeo($menu_item_id, $slug, 'menu_item');

        return true;
    }

    public function createMenuItemSeo($menu_item_id, $slug, $tableName = null)
    {
        $this->load->model('localisation/language');
        $this->load->model('custom/common');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug); // removed param "$tableName". TODO: remove this comment...
        $languages = $this->model_localisation_language->getLanguages();
        foreach ($languages as $key => $language) {
            $language_id = $language['language_id'];

            $alias = 'menu_item_id=' . $menu_item_id;
            // $sql = "INSERT INTO " . DB_PREFIX . "seo_url SET language_id = '" . (int)$language_id . "', query = '" . $alias . "', keyword = '" . $slug_custom . "'";
            $sql = sprintf("INSERT INTO %s 
                            SET language_id = '%d', query = '%s', keyword = '%s'",
                DB_PREFIX . "seo_url",
                (int)$language_id,
                $alias,
                $slug_custom
            );

            if (isset($tableName)) {
                $sql .= ", table_name = '" . $tableName . "'";
            }
            $this->db->query($sql);
        }
    }
    
    public function deleteById($menu_item_id)
    {
        //delete relation table
        $sqlRelation = "
            DELETE FROM " .DB_PREFIX. "novaon_relation_table WHERE main_name = 'menu_item' AND main_id = '". (int)$menu_item_id ."'
        ";
        $this->db->query($sqlRelation);
        $sqlRelationMenu = "
            DELETE FROM " .DB_PREFIX. "novaon_relation_table WHERE child_name = 'menu_item' AND child_id = '". (int)$menu_item_id ."'
        ";
        $this->db->query($sqlRelationMenu);
        //delete seo
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'menu_item_id=" . (int)$menu_item_id . "' AND table_name = 'menu_item'");
        //delete menu_item_description
        $sqlDes = "
            DELETE FROM " .DB_PREFIX. "novaon_menu_item_description WHERE menu_item_id = '". (int)$menu_item_id ."'
        ";
        $this->db->query($sqlDes);
        //delete menu_item
        $sql = "
            DELETE FROM " .DB_PREFIX. "novaon_menu_item WHERE id = '". (int)$menu_item_id ."'
        ";

        $this->db->query($sql);
    }
}
