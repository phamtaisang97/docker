<?php
class ModelCustomCommon extends Model {
    public function getListMenu()
    {
        $sql = "SELECT m.id AS group_menu_id, md.name AS name FROM " . DB_PREFIX . "novaon_group_menu m LEFT JOIN " . DB_PREFIX . "novaon_group_menu_description md ON (m.id = md.group_menu_id) WHERE md.language_id = '" . (int)$this->config->get('config_language_id') . "'";
        $query = $this->db->query($sql);
        $group_menu = $query->rows;
        $menu_item = $this->db->query("SELECT m.id AS menu_item_id, md.name AS name FROM " . DB_PREFIX . "novaon_menu_item m LEFT JOIN " . DB_PREFIX . "novaon_menu_item_description md ON (m.id = md.menu_item_id) WHERE md.language_id = '" . (int)$this->config->get('config_language_id') . "'")->rows;
        // var_dump($menu_item);die;
        $arrayMenu = $arrayMenuItem = array();
        foreach ($menu_item as $key => $value) {
            $menu_item_id = $value['menu_item_id'];
            $menu_item_name = $value['name'];
            $arrayMenuItem[] = [
                'id' => 'menu_item-'.$menu_item_id,
                'name' => $menu_item_name,
            ];
        }

        foreach ($group_menu as $key => $value) {
            $group_menu_id = $value['group_menu_id'];
            $group_menu_name = $value['name'];
            $arrayMenu[] = [
                // 'group_menu-'.$group_menu_id => $group_menu_name,
                'id' => 'group_menu-'.$group_menu_id,
                'name' => $group_menu_name,
            ];
        }

        $data = array_merge($arrayMenu, $arrayMenuItem);

        return $data;
    }

    public function getCategoryMenu($category_id)
    {
        if ($category_id == '') {
            return array();
        }

        $query = $this->db->query("
            SELECT main_name, main_id FROM " . DB_PREFIX . "novaon_relation_table WHERE child_name = 'category' AND child_id = '".$category_id."'
        ");
        $data = array();
        foreach ($query->rows as $value) {
            $sql = "SELECT md.name AS name FROM " . DB_PREFIX . "novaon_".$value['main_name']." m LEFT JOIN " . DB_PREFIX . "novaon_".$value['main_name']."_description md ON (m.id = md.".$value['main_name']."_id) WHERE md.language_id = '" . (int)$this->config->get('config_language_id') . "' AND m.id = '".$value['main_id']."' ";
            if(empty($this->db->query($sql)->row)){
                continue;
            }
            $data[] = ['id' => $value['main_name'].'-'.$value['main_id'],
                'name' => $this->db->query($sql)->row['name']
            ];
        }

        return $data;
    }


    /**
     * original: getSlugUnique($slug, $tableName = null) temp not used! TODO: remove?...
     *
     * @param $slug
     * @return string
     */
    public function getSlugUnique($slug)
    {
        //we only bother doing this if there is a conflicting slug already
        $sql = "
            SELECT keyword FROM " . DB_PREFIX . "seo_url WHERE keyword  LIKE '" . $slug . "%'
        ";

        /*if (isset($tableName)) {
            $sql .= " AND table_name = '" . $tableName . "' ";
        }*/

        $query = $this->db->query($sql);
        $data = $query->rows;
        $slugs = [];
        foreach ($data as $value) {
            $slugs[] = $value['keyword'];
        }
        $count = count($data);

        if ($count != 0 && in_array($slug, $slugs)) {
            $max = 0;
            //keep incrementing $max until a space is found
            while (in_array(($slug . '-' . ++$max), $slugs)) ;
            //update $slug with the appendage
            $slug .= '-' . $max;
        }

        return $slug;
    }

    public function createSlug($str, $delimiter = '-'){

        $str = $this->to_slug($str);
        $slug = mb_strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));

        return $slug;
    }

    public function to_slug($str)
    {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', '-', $str);

        return $str;
    }

    public function getListGroupMenu($language_id = null)
    {
        if (!$language_id) {
            $language_id = $this->config->get('config_language_id');
        }

        $sql = "SELECT m.id AS id, md.name AS name FROM " . DB_PREFIX . "novaon_group_menu m LEFT JOIN " . DB_PREFIX . "novaon_group_menu_description md ON (m.id = md.group_menu_id) WHERE md.language_id = '" . (int)$language_id . "'";
        $data = $this->db->query($sql)->rows;

        return $data;
    }

    public function getListGroupMenuPaginator($data, $language_id = null)
    {
        if (!$language_id) {
            $language_id = $this->config->get('config_language_id');
        }

        $sql = "SELECT m.id AS id, md.name AS name FROM " . DB_PREFIX . "novaon_group_menu m LEFT JOIN " . DB_PREFIX . "novaon_group_menu_description md ON (m.id = md.group_menu_id) WHERE md.language_id = '" . (int)$language_id . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND md.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }
        $sql .= " ORDER BY md.name ASC";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $data = $this->db->query($sql)->rows;

        return $data;
    }

    public function countListGroupMenuPaginator($data, $language_id = null) {
        if (!$language_id) {
            $language_id = $this->config->get('config_language_id');
        }

        $sql = "SELECT COUNT(m.id) as total FROM " . DB_PREFIX . "novaon_group_menu m LEFT JOIN " . DB_PREFIX . "novaon_group_menu_description md ON (m.id = md.group_menu_id) WHERE md.language_id = '" . (int)$language_id . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND md.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $data = $this->db->query($sql)->row;

        return $data['total'];
    }

    public function getListProduct($language_id = null)
    {
        if (!$language_id) {
            $language_id = $this->config->get('config_language_id');
        }

        $query = $this->db->query("SELECT p.product_id AS id, pd.name AS name FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE pd.language_id = '" . (int)$language_id . "' GROUP BY p.product_id");
        $data = $query->rows;

        return $data;
    }

    public function getListMenuItemByGroupMenuId($group_menu_id)
    {
        $sql = "
            SELECT * FROM " . DB_PREFIX . "novaon_relation_table WHERE main_name = 'group_menu' AND main_id = '".$group_menu_id."'
        ";
        $data = $this->db->query($sql)->rows;

        return $data;
    }

    public function getSlugUrlForGroupMenu($group_menu_id)
    {
        $sql = "
            SELECT keyword FROM " . DB_PREFIX . "seo_url WHERE query = 'group_menu_id=" . $group_menu_id . "'
        ";
        $data = $this->db->query($sql)->row;
        if (count($data) > 0) {
            return $data['keyword'];
        }

        return null;
    }

    public function getSlugUrlForMenuItem($menu_item_id)
    {
        $sql = "
            SELECT keyword FROM " . DB_PREFIX . "seo_url WHERE query = 'menu_item_id=" . $menu_item_id . "'
        ";
        $data = $this->db->query($sql)->row;
        if (count($data) > 0) {
            return $data['keyword'];
        }

        return null;
    }

    public function getProductByIds($ids, $language_id = null)
    {
        $ids = is_array($ids) ? $ids : [$ids];
        $ids = implode(",",$ids);
        if (!$language_id) {
            $language_id = $this->config->get('config_language_id');
        }

        $query = $this->db->query("SELECT p.product_id AS id, p.image as image, pd.name AS name FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id IN (" . $ids . ") AND pd.language_id = '" . (int)$language_id . "' GROUP BY p.product_id");
        $data = $query->rows;

        return $data;
    }

    public function getAttribute($language_id = null)
    {
        if (!$language_id) {
            $language_id = $this->config->get('config_language_id');
        }

        $query = $this->db->query("SELECT DISTINCT(a.name) AS name FROM " . DB_PREFIX . "attribute_description a WHERE a.language_id = '" . (int)$language_id . "'");
        $data = $query->rows;

        return $data;
    }
}