<?php


class ModelCustomImageFolder extends Model
{
    public function addFolder($data)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "INSERT INTO `{$DB_PREFIX}image_folder` SET 
                            `parent_id` = " . (int)$data['parent_id'] . ",
                            `status` = 1, 
                            `date_added` = NOW(),  
                            `date_modified` = NOW(),  
                            `name` = '" . $data['name'] . "'";

        $this->db->query($sql);
    }

    public function editFolder($data)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "UPDATE `{$DB_PREFIX}image_folder`  
                SET `name` = '{$data['name']}',
                    `date_modified` = NOW()
                WHERE `id` = {$data['id']}";

        $this->db->query($sql);
    }

    public function getFolders($image_folder)
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "image_folder` WHERE  parent_id = " . (int)$image_folder . " ";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getFolderActive($image_folder)
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "image_folder` WHERE  parent_id = " . (int)$image_folder . " AND status = 1 ";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getParent($image_folder)
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "image_folder` WHERE  id = " . (int)$image_folder . " ";
        $query = $this->db->query($sql);
        $folder_now = $query->row;
        $folder_parent = [];
        if (!empty($folder_now)) {
            $sql = "SELECT * FROM `" . DB_PREFIX . "image_folder` WHERE  id = " . (int)$folder_now['parent_id'] . " ";
            $query = $this->db->query($sql);
            $folder_parent = $query->row;
        }

        if (!empty($folder_parent)) {
            $folder = [
                'folder_now' => $folder_now,
                'folder_parent' => $folder_parent['id']
            ];
            return $folder;
        }

        $folder = [
            'folder_now' => $folder_now,
            'folder_parent' => 0
        ];

        return $folder;
    }

    public function disableFolders($folder_ids)
    {
        if (!is_array($folder_ids) || empty($folder_ids)) {
            return;
        }

        $folder_ids = implode(",", $folder_ids);
        $sql = "UPDATE `" . DB_PREFIX . "image_folder` 
                SET `status` = 0,
                    `date_modified` = NOW()
                WHERE `id` IN (" . $folder_ids . ")";
        $this->db->query($sql);
    }

    public function enableFolders($folder_ids)
    {
        if (!is_array($folder_ids) || empty($folder_ids)) {
            return;
        }

        $folder_ids = implode(",", $folder_ids);
        $sql = "UPDATE `" . DB_PREFIX . "image_folder` 
                SET `status` = 1,
                    `date_modified` = NOW()
                WHERE `id` IN (" . $folder_ids . ")";
        $this->db->query($sql);
    }

    public function deleteFolders($folder_ids)
    {
        if (!is_array($folder_ids) || empty($folder_ids)) {
            return;
        }

        $folder_ids = implode(",", $folder_ids);
        $sql = "DELETE FROM `" . DB_PREFIX . "image_folder` WHERE `id` IN (" . $folder_ids . ")";
        $this->db->query($sql);
    }

    /**
     * get all descendant of a folder
     * @param $folder_id
     * @param array $result
     * @return array|mixed
     */
    public function getDescendantFolders($folder_id, $result = [])
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT `id` FROM `{$DB_PREFIX}image_folder` WHERE `parent_id` = {$folder_id}";
        $query = $this->db->query($sql);

        foreach ($query->rows as $row) {
            if (!in_array($row['id'], $result)) {
                $result[] = $row['id'];

                // call recursive
                $result = $this->getDescendantFolders($row['id'], $result);
            }
        }

        return $result;
    }

    /**
     * get all
     * @param $folder_id
     * @param array $data
     * @return mixed
     */
    public function getFoldersPaginator($data = [])
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT `id`, `name` FROM `{$DB_PREFIX}image_folder` f WHERE `status` = 1";

        if (isset($data['filter_name'])) {
            $sql .= " AND `name` LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (isset($data['need_to_move_folders']) && !empty($data['need_to_move_folders'])) {
            /**
             * get all can-not-move folders:
             * - all descendent folders of need-to-move-folders
             * - parent folder of need-to-move-folders
             * - need-to-move-folders themselves
             * - folders that contain folder name like need-to-move-folders
             */

            $descendant_folders = $data['need_to_move_folders'];
            foreach ($data['need_to_move_folders'] as $folder_id) {
                $descendant_folders = array_merge($descendant_folders, $this->getDescendantFolders($folder_id));
            }

            if (!empty($descendant_folders)) {
                $folder_ids = implode(',', $descendant_folders);
                $sql .= " AND `id` NOT IN ({$folder_ids})";

                // check if exists need-to-move-folders name in each moveable folder
                $folder_names = $this->getFolderNamesFromIds($data['need_to_move_folders']);
                $folder_names = "'" . implode("','", $folder_names) . "'";
                $sql .= " AND NOT EXISTS (SELECT 1 FROM `{$DB_PREFIX}image_folder` WHERE `name` IN ({$folder_names}) AND `parent_id` = f.`id` LIMIT 1)";
            }

            // remove current parent folder of moveable folders list
            // get first folder (all need-to-move-folders are in the same parent folder) in $data['need_to_move_folders']
            $sql .= " AND `id` <> (SELECT `parent_id` FROM `{$DB_PREFIX}image_folder` WHERE `id` = {$data['need_to_move_folders'][0]} LIMIT 1)";
        } elseif (isset($data['need_to_move_images']) && !empty($data['need_to_move_images'])) {
            // remove current parent folder of moveable images list
            // get first image (all need-to-move-images are in the same folder) in $data['need_to_move_images']
            $sql .= " AND `id` <> (SELECT `folder_path` FROM `{$DB_PREFIX}images` WHERE `image_id` = {$data['need_to_move_images'][0]} LIMIT 1)";
        }

        $sql .= " ORDER BY `name` ASC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = $this->config->get('config_limit_admin');
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalFolders($data = [])
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT COUNT(1) total FROM `{$DB_PREFIX}image_folder` f WHERE `status` = 1";

        if (!empty($data['filter_name'])) {
            $sql .= " AND `name` LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (isset($data['need_to_move_folders']) && !empty($data['need_to_move_folders'])) {
            /**
             * get all can-not-move folders:
             * - all descendent folders of need-to-move-folders
             * - parent folder of need-to-move-folders
             * - need-to-move-folders themselves
             * - folders that contain folder name like need-to-move-folders
             */

            $descendant_folders = $data['need_to_move_folders'];
            foreach ($data['need_to_move_folders'] as $folder_id) {
                $descendant_folders = array_merge($descendant_folders, $this->getDescendantFolders($folder_id));
            }

            if (!empty($descendant_folders)) {
                $folder_ids = implode(',', $descendant_folders);
                $sql .= " AND `id` NOT IN ({$folder_ids})";

                // check if exists need-to-move-folders name in each moveable folder
                $folder_names = $this->getFolderNamesFromIds($data['need_to_move_folders']);
                $folder_names = "'" . implode("','", $folder_names) . "'";
                $sql .= " AND NOT EXISTS (SELECT 1 FROM `{$DB_PREFIX}image_folder` WHERE `name` IN ({$folder_names}) AND `parent_id` = f.`id` LIMIT 1)";
            }

            // remove current parent folder of moveable folders list
            // get first folder (all need-to-move-folders are in the same parent folder) in $data['need_to_move_folders']
            $sql .= " AND `id` <> (SELECT `parent_id` FROM `{$DB_PREFIX}image_folder` WHERE `id` = {$data['need_to_move_folders'][0]} LIMIT 1)";
        } elseif (isset($data['need_to_move_images']) && !empty($data['need_to_move_images'])) {
            // remove current parent folder of moveable images list
            // get first image (all need-to-move-images are in the same folder) in $data['need_to_move_images']
            $sql .= " AND `id` <> (SELECT `folder_path` FROM `{$DB_PREFIX}images` WHERE `image_id` = {$data['need_to_move_images'][0]} LIMIT 1)";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function moveFolders($move_folder, $folder_ids)
    {
        if (!is_array($folder_ids) || empty($folder_ids)) {
            return false;
        }

        $DB_PREFIX = DB_PREFIX;
        $folder_ids = implode(',', $folder_ids);
        $sql = "UPDATE `{$DB_PREFIX}image_folder` SET 
                        `parent_id` = " . (int)$move_folder . ",
                        `date_modified` = NOW() 
                        WHERE `id` IN ({$folder_ids})";

        $this->db->query($sql);
    }

    public function checkFolderNamesExists($folder_names, $folder_id, $edit_folder_id = 0)
    {
        if (!is_array($folder_names) || empty($folder_names)) {
            return false;
        }

        $DB_PREFIX = DB_PREFIX;
        $folder_names = "'" . implode("','", $folder_names) . "'";
        // for case edit folder: accept old folder name
        $edit_folder_query = 0 < $edit_folder_id ? " AND `id` <> {$edit_folder_id}" : '';
        $sql = "SELECT EXISTS (SELECT 1 FROM `{$DB_PREFIX}image_folder` WHERE `name` IN ({$folder_names}) AND `parent_id` = {$folder_id} {$edit_folder_query} LIMIT 1) result";
        $query = $this->db->query($sql);

        return 1 == $query->row['result'];
    }

    public function getFolderNamesFromIds($folder_ids)
    {
        $result = [];

        if (!is_array($folder_ids) || empty($folder_ids)) {
            return $result;
        }

        $DB_PREFIX = DB_PREFIX;
        $folder_ids = implode(',', $folder_ids);
        $sql = "SELECT `name` FROM `{$DB_PREFIX}image_folder` WHERE `id` IN ({$folder_ids})";
        $query = $this->db->query($sql);

        foreach ($query->rows as $folder) {
            $result[] = $folder['name'];
        }

        return $result;
    }

    public function isHiddenFolder($folder_id)
    {
        // root folder is always unhidden
        if (0 == $folder_id) {
            return false;
        }

        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT EXISTS (SELECT 1 FROM `{$DB_PREFIX}image_folder` WHERE `id` = {$folder_id} AND `status` = 0 LIMIT 1) result";
        $query = $this->db->query($sql);

        return 1 == $query->row['result'];
    }
}
