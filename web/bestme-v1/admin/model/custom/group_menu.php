<?php

class ModelCustomGroupMenu extends Model {
    public function add($data)
    {
        // create group_menu in group_menu_table, group_menu_description
        $group_menu_id = $this->createGroupMenu();
        $this->createGroupMenuDes($group_menu_id, $data['group_menu_name']);

        // create menu_item
        $this->load->model('custom/menu_item');
        //// init slugs for children
        $this->load->model('custom/common');
        $data['slug_child'] = array_map(function ($name) {
            return $this->model_custom_common->createSlug($name, '-');
        }, $data['menu_item_name']);

        foreach ($data['menu_item_name'] as $key => $menuItem) {
            $data['list_id_child'][$key] = $this->model_custom_menu_item->add($menuItem, $group_menu_id, $data, $key);
        }

        // create relation table
        $this->load->model('custom/relation_table');
        foreach ($data['list_id_child'] as $key => $value) {
            if (isset($data['type_id'][$key])) {
                $dataGroupMenu['main_name'] = 'group_menu';
                $dataGroupMenu['main_id'] = $group_menu_id;
                $dataGroupMenu['child_name'] = 'menu_item';
                $dataGroupMenu['child_id'] = $value;
                $dataGroupMenu['type_id'] = $data['type_id'][$key];
                $dataGroupMenu['url'] = '';
                $dataGroupMenu['redirect'] = 0;

                $this->model_custom_relation_table->add($dataGroupMenu);
            }
        }

        // create seo_url: create record in the seo_url table
        //// unset slugs for children
        unset($data['slug_child']);
        $slug = $this->model_custom_relation_table->getSlug($data, 0);
        $this->createGroupMenuSeo($group_menu_id, $slug, 'group_menu');
    }

    public function editGroupMenuDes($group_menu_id, $group_menu_name)
    {
        $sqlDes = "
            DELETE FROM " .DB_PREFIX. "novaon_group_menu_description WHERE group_menu_id = '". (int)$group_menu_id ."'
        ";
        $this->db->query($sqlDes);
        $this->createGroupMenuDes($group_menu_id, $group_menu_name);
    }

    public function edit($group_menu_id, $data)
    {
        $this->editGroupMenuDes($group_menu_id, $data['group_menu_name']);

        if (isset($data['menu_item_name'])) {
            foreach ($data['menu_item_name'] as $key => $menuItem) {
                $data['list_id_child'][$key] = $this->model_custom_menu_item->add($menuItem, $group_menu_id, $data, $key);
            }

            $this->load->model('custom/relation_table');
            foreach ($data['list_id_child'] as $key => $value) {
                if (isset($data['type_id'][$key])) {
                    $dataGroupMenu['main_name'] = 'group_menu';
                    $dataGroupMenu['main_id'] = $group_menu_id;
                    $dataGroupMenu['child_name'] = 'menu_item';
                    $dataGroupMenu['child_id'] = $value;
                    $dataGroupMenu['type_id'] = $data['type_id'][$key];
                    $dataGroupMenu['url'] = '';
                    $dataGroupMenu['redirect'] = 0;
                    $this->model_custom_relation_table->add($dataGroupMenu);
                }
            }
        }

        // update slug
        $this->db->query(sprintf("DELETE FROM %s 
                                  WHERE query = 'group_menu_id=%d'
                                  AND table_name = 'group_menu'",
            DB_PREFIX . "seo_url",
            (int)$group_menu_id
        ));
        $slug = $this->model_custom_relation_table->getSlug($data, 0);
        $this->createGroupMenuSeo($group_menu_id, $slug, 'group_menu');
    }

    public function createGroupMenu()
    {
        $parent_id = 0;
        $status = 1;
        $sql = "INSERT INTO " . DB_PREFIX . "novaon_group_menu SET parent_id = '" . (int)$parent_id . "', status = '" . (int)$status . "', updated_at = NOW(), created_at = NOW()";
        $this->db->query($sql);
        $group_menu_id = $this->db->getLastId();

        return $group_menu_id;
    }

    public function createGroupMenuDes($group_menu_id, $name)
    {
        //get multiple language
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        foreach ($languages as $key => $value) {
            $language_id = $value['language_id'];
            $this->db->query("INSERT INTO " . DB_PREFIX . "novaon_group_menu_description SET group_menu_id = '" . (int)$group_menu_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($name) . "'");
        }
    }

    public function getGroupMenuById($group_menu_id)
    {
        $sql = "SELECT m.id AS group_menu_id, md.name AS name FROM " . DB_PREFIX . "novaon_group_menu m LEFT JOIN " . DB_PREFIX . "novaon_group_menu_description md ON (m.id = md.group_menu_id) WHERE md.language_id = '" . (int)$this->config->get('config_language_id') . "' AND m.id = '".$group_menu_id."'";
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getListMenuCustom()
    {
        $url = '';
        $sql = "SELECT m.id AS group_menu_id, md.name AS name FROM " . DB_PREFIX . "novaon_group_menu m LEFT JOIN " . DB_PREFIX . "novaon_group_menu_description md ON (m.id = md.group_menu_id) WHERE md.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY m.created_at DESC";
        $query = $this->db->query($sql);
        $data = $query->rows;
        foreach ($data as $key => $value) {
            $sql = "
                SELECT * FROM " .DB_PREFIX. "novaon_relation_table rt WHERE main_name = 'group_menu' AND main_id = ". $value['group_menu_id'] ."
            ";
            $data[$key]['child'] = $this->db->query($sql)->rows;
            foreach ($data[$key]['child'] as $k => $v) {
                $model = $this->getModel($v['child_name']);
                if ($model) {
                    $data[$key]['child'][$k]['child_name_detail'] = $model->getNameRecordById($v['child_id'], $v['child_name']);
                } else {
                    $data[$key]['child'][$k]['child_name_detail'] = '';
                }
                $data[$key]['child'][$k]['child_name'] = $v['child_name'];
                $data[$key]['child'][$k]['child_name_append'] = $this->getChildNameAppend($v);
                $data[$key]['child'][$k]['child_href_append'] = $this->url->link('custom/menu_item/edit', 'user_token=' . $this->session->data['user_token'] . '&menu_item_id=' . $v['child_id'] .'&relation_id='.$v['id'].$url, true);
            }
        }

        return $data;
    }

    public function getModel($name)
    {
        if ($name == 'menu_item') {
            $this->load->model('custom/menu_item');
            return $this->model_custom_menu_item;
        }
        if ($name == 'category') {
            $this->load->model('catalog/category');
            return $this->model_catalog_category;
        }
        if ($name == 'product') {
            $this->load->model('catalog/product');
            return $this->model_catalog_product;
        }

        return null;
    }

    public function getChildNameAppend($data)
    {
        if ($data['child_name'] == 'menu_item') {
            $sql = "
                SELECT * FROM " .DB_PREFIX. "novaon_relation_table rt WHERE main_name = 'menu_item' AND main_id = ". $data['child_id'] ." AND redirect = 0
            ";
            $result = $this->db->query($sql)->rows;
            $name = array();
            foreach ($result as $key => $value) {
                $name[] = $value['title'];
//                $model = $this->getModel($value['child_name']);
//                if ($model) {
//                    $name = $name.$model->getNameRecordById($value['child_id'], $value['child_name']).',';
//                } else {
//                    $name = $name.$value['url'].',';
//                }
            }

            return implode($name,', ');
        }
        
        return null;
    }

    public function deleteById($group_menu_id)
    {
        // delete relation table
        $sqlRelation = "
            DELETE FROM " .DB_PREFIX. "novaon_relation_table WHERE main_name = 'group_menu' AND main_id = '". (int)$group_menu_id ."'
        ";
        $this->db->query($sqlRelation);

        // delete group_menu_description
        $sqlDes = "
            DELETE FROM " .DB_PREFIX. "novaon_group_menu_description WHERE group_menu_id = '". (int)$group_menu_id ."'
        ";
        $this->db->query($sqlDes);

        // delete group_menu
        $sql = "
            DELETE FROM " .DB_PREFIX. "novaon_group_menu WHERE id = '". (int)$group_menu_id ."'
        ";
        $this->db->query($sql);
    }

    public function createGroupMenuSeo($menu_item_id, $slug, $tableName = null)
    {
        $this->load->model('localisation/language');
        $this->load->model('custom/common');
        $slug_custom = $this->model_custom_common->getSlugUnique($slug); // removed param "$tableName". TODO: remove this comment...
        $languages = $this->model_localisation_language->getLanguages();
        foreach ($languages as $key => $language) {
            $language_id = $language['language_id'];

            $alias = 'group_menu_id=' . $menu_item_id;
            // $sql = "INSERT INTO " . DB_PREFIX . "seo_url SET language_id = '" . (int)$language_id . "', query = '" . $alias . "', keyword = '" . $slug_custom . "'";
            $sql = sprintf("INSERT INTO %s 
                            SET language_id = '%d', query = '%s', keyword = '%s'",
                DB_PREFIX . "seo_url",
                (int)$language_id,
                $alias,
                $slug_custom
            );

            if (isset($tableName)) {
                $sql .= ", table_name = '" . $tableName . "'";
            }
            $this->db->query($sql);
        }
    }

    public function getMenuGroupByChild($child_name, $child_id) {

        $sql = "SELECT m.id AS id, md.name AS title FROM " . DB_PREFIX . "novaon_group_menu m LEFT JOIN " . DB_PREFIX . "novaon_group_menu_description md ON (m.id = md.group_menu_id)";
        $sql .= "INNER JOIN ". DB_PREFIX . 'novaon_menu_item as nmi ON nmi.group_menu_id = m.id ';
        $sql .= "INNER JOIN ". DB_PREFIX ."novaon_relation_table as nrt ON nrt.main_id = nmi.id AND nrt.main_name = 'menu_item' AND nrt.child_name = '".$child_name."' AND nrt.child_id ='". $this->db->escape($child_id) ."'";
        $sql .= " WHERE md.language_id = '". (int)$this->config->get('config_language_id') ."'";

        $query = $this->db->query($sql);

        return $query->rows;
    }
}
