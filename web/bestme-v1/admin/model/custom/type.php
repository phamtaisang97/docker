<?php
class ModelCustomType extends Model{
   
    public function getAllType()
    {
        $sql = "
            SELECT t.id AS type_id, td.name AS type_name FROM " . DB_PREFIX . "novaon_type t LEFT JOIN " . DB_PREFIX . "novaon_type_description td ON (t.id = td.type_id) WHERE td.language_id = '" . (int)$this->config->get('config_language_id') . "'
        ";
        $query = $this->db->query($sql);
        $result = [];
        foreach ($query->rows as $row) { // tempory disable blog
            if ($row['type_id'] == 3) { // blog
                continue;
            }
            $result[] = $row;
        }
        return $result;
    }
}
