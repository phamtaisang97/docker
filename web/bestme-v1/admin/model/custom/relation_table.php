<?php
class ModelCustomRelationTable extends Model{
    public function add($data)
    {
        $title = '';
        if (isset($data['title'])) {
            $title = $data['title'];
        }

        $sql = "
            INSERT INTO " . DB_PREFIX . "novaon_relation_table SET main_id = '" . (int)$data['main_id'] . "', child_id = '" . (int)$data['child_id'] . "', main_name = '". $this->db->escape($data['main_name']) ."', child_name = '". $this->db->escape($data['child_name']) ."', url = '". $data['url'] ."', title = '".$title."', redirect = '". $data['redirect'] ."', type_id = '" .(int)$data['type_id']. "'
        ";

        $this->db->query($sql);
    }

    public function getChildName($data)
    {
        if (!isset($data)) {
            return '';
        }

        $type_id = $data;
        $childName = '';
        if ($type_id == 1) {
            $childName = 'product';
        }

        if ($type_id == 2) {
            $childName = 'category';
        }

        if ($type_id == 3) {
            $childName = 'blog';
        }

        if ($type_id == 4) {
            $childName = 'blog_category';
        }

        return $childName;
    }

    public function getFieldName($data, $key)
    {
        $childName = $this->getChildName($data['type_id'][$key]);
        if ($childName == 'product') {
            return 'product-choose';
        }

        if ($childName == 'category') {
            return 'category_id';
        }

        return '';
    }

    public function getChildId($data, $key)
    {
        if (!$data['type_id'][$key]) {
            return '';
        }

        $childName = $this->getChildName($data['type_id'][$key]);
        if ($childName == '') {
            return '';
        }

        $field = $this->getFieldName($data, $key);
        if ($field == '') {
            return '';
        }

        return $data[$field][$key];
    }

    public function getUrl($data, $key, $field = null)
    {
        if ($field) {
            return $data[$field][$key];
        }

        if (!isset($data['type_id'][$key])) {
            return '';
        }

        if ($data['type_id'][$key] != 5) {
            return '';
        }

        if (!isset($data['url'][$key])) {
            return '';
        }

        return $data['url'][$key];
    }

    public function getSlug($data, $key)
    {
        if (!isset($data['type_id'][$key]) && !isset($data['type_id-old'])) {
            return '';
        }

        // slug_child comes from group_menu when creating
        if (isset($data['slug_child'][$key])) {
            return $data['slug_child'][$key];
        }

        // slug comes from menu_item when creating
        if (!isset($data['slug'][$key])) {
            return '';
        }

        return $data['slug'][$key];
    }

    public function deleteById($relation_id)
    {
        $sql = "
            DELETE  FROM " .DB_PREFIX. "novaon_relation_table WHERE id = '".(int)$relation_id."'
        ";
        $this->db->query($sql);
    }

    public function getFieldById($relation_id, $field = null)
    {
        $sql = "
            SELECT ".$field." FROM " .DB_PREFIX. "novaon_relation_table WHERE id = '".(int)$relation_id."'
        ";

        return $this->db->query($sql)->row[$field];
    }

    public function getFieldByMainId($mainName, $mainId, $field = null)
    {
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');
        if ($field == null) {
            $field = '*';
        }

        $sql = "
            SELECT ".$field." FROM " .DB_PREFIX. "novaon_relation_table rt WHERE main_name = '".$mainName."' AND main_id = ". $mainId ."
        ";
        $query = $this->db->query($sql);
        $result = $query->rows;
        $array = [];
        foreach ($result as $key => $value) {
            $array[] = $value[$field];
        }

        return $array;
    }

    public function getAllByMainId($mainName, $mainId, $redirect = null)
    {
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');
        $this->load->model('catalog/collection');
        $sql = "
            SELECT * FROM " .DB_PREFIX. "novaon_relation_table rt WHERE main_name = '".$mainName."' AND main_id = ". $mainId ."
        ";
        if (isset($redirect)) {
            $sql .= " AND redirect = '".$redirect."'";
        }
        $query = $this->db->query($sql);
        $result = $query->rows;
        $data = array();
        foreach ($result as $key => $value) {
            if ($value['child_name'] == 'menu_item') {
                $url_link = '';
                $data[$key]['name'] = $this->db->query(" SELECT * FROM " .DB_PREFIX. "novaon_menu_item_description WHERE menu_item_id = ". $value['child_id'] ." AND language_id = '" . (int)$this->config->get('config_language_id') . "'")->row['name'];
               
                $menu_item_data = $this->db->query("SELECT * FROM " .DB_PREFIX. "novaon_relation_table WHERE main_name = 'menu_item' AND main_id = '".(int)$value['child_id']."' AND redirect = 1");

                $data[$key]['type_id'] = $menu_item_data->row['type_id'];
                $child_append_name_table = $menu_item_data->row['child_name'];
                $child_append_name = '';
                if ($menu_item_data->row['type_id'] == 5) {
                    $url_link = $menu_item_data->row['url'];
                }
                if ($child_append_name_table != 'menu_item' && $child_append_name_table != '') {
                    $model = 'model_catalog_'.$menu_item_data->row['child_name'];
                    $child_append_name = $this->$model->getNameRecordById($menu_item_data->row['child_id'], $menu_item_data->row['child_name']);
                }
                $data[$key]['child_append_name'] = $child_append_name;
                $data[$key]['child_append_id'] = $menu_item_data->row['child_id'];
                $data[$key]['child_id'] = $value['child_id'];
                $data[$key]['url_link'] = $url_link;
                $data[$key]['relation_id'] = $value['id'];
                $data[$key]['title'] = $value['title'];
            }
            if ($value['child_name'] == 'category') {
                $data[$key]['name'] = $this->db->query(" SELECT * FROM " .DB_PREFIX. "category_description WHERE category_id = ". $value['child_id'] ." AND language_id = '" . (int)$this->config->get('config_language_id') . "'")->row['name'];
                $data[$key]['child_id'] = $value['child_id'];
                $data[$key]['relation_id'] = $value['id'];
                $data[$key]['type_id'] = $value['type_id'];
                $data[$key]['url_link'] = $value['url'];
                $data[$key]['title'] = $value['title'];

            }
            if ($value['child_name'] == 'product') {
                $data[$key]['name'] = $this->db->query(" SELECT * FROM " .DB_PREFIX. "product_description WHERE product_id = ". $value['child_id'] ." AND language_id = '" . (int)$this->config->get('config_language_id') . "'")->row['name'];
                $data[$key]['child_id'] = $value['child_id'];
                $data[$key]['relation_id'] = $value['id'];
                $data[$key]['type_id'] = $value['type_id'];
                $data[$key]['url_link'] = $value['url'];
                $data[$key]['title'] = $value['title'];
            }
            if ($value['child_name'] == '') {
                $data[$key]['child_id'] = $value['child_id'];
                $data[$key]['relation_id'] = $value['id'];
                $data[$key]['type_id'] = $value['type_id'];
                $data[$key]['url_link'] = $value['url'];
                $data[$key]['title'] = $value['title'];
                $data[$key]['name'] = '';
            }

        }

        return $data;
    }

    public function getByChild($child_name, $child_id) {
        $sql = "SELECT * FROM ". DB_PREFIX ."novaon_relation_table WHERE child_name='". $child_name ."' AND child_id ='". $child_id ."'";
        $query = $this->db->query($sql);
        return $query->rows;
    }
}
