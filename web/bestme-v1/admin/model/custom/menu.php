<?php

class ModelCustomMenu extends Model
{
    const MENU_TYPE_CATEGORY = 1;
    const MENU_TYPE_COLLECTION = 2;
    const MENU_TYPE_PRODUCT = 3;
    const MENU_TYPE_URL = 4;
    const MENU_TYPE_BUILT_IN_PAGE = 5;
    const MENU_TYPE_BLOG = 6;

    public function add($data)
    {
        $sqlAddMenu = "INSERT INTO " . DB_PREFIX . "menu SET parent_id = NULL";
        $this->db->query($sqlAddMenu);
        $menu_id = $this->db->getLastId();
        // add menu description
        if ($menu_id) {
            $this->load->model('localisation/language');
            $menu_name = isset($data['menu_name']) ? $data['menu_name'] : '';
            $languages = $this->model_localisation_language->getLanguages();
            foreach ($languages as $key => $value) {
                $language_id = $value['language_id'];
                $this->db->query("INSERT INTO " . DB_PREFIX . "menu_description SET menu_id = '" . (int)$menu_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($menu_name) . "'");
            }

            // add menu items
            if (isset($data['menu_item_name']) && is_array($data['menu_item_name'])) {
                foreach ($data['menu_item_name'] as $key => $menu_item_name) {
                    $menu_type_id = isset($data['type_id'][$key]) ? $data['type_id'][$key] : '';
                    $menu_target_value = isset($data['menu_item_value'][$key]) ? $data['menu_item_value'][$key] : '';
                    $this->db->query("INSERT INTO " . DB_PREFIX . "menu SET parent_id = " . (int)$menu_id . ", menu_type_id = " . (int)$menu_type_id . ", target_value = '" . $this->db->escape($menu_target_value) . "'");
                    $menu_item_id = $this->db->getLastId();
                    if ($menu_item_id) {
                        foreach ($languages as $k => $value) {
                            $language_id = $value['language_id'];
                            $this->db->query("INSERT INTO " . DB_PREFIX . "menu_description SET menu_id = '" . (int)$menu_item_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($menu_item_name) . "'");
                        }
                    }
                }
            }

            // insert SEO
            $slug = isset($data['slug']) ? $data['slug'] : $menu_name;
            if ($slug) {
                $this->load->model('custom/common');
                $slug = $this->model_custom_common->createSlug($slug, '-');
                $slug = $this->model_custom_common->getSlugUnique($slug);
                foreach ($languages as $key => $value) {
                    $language_id = $value['language_id'];
                    $this->db->query("INSERT INTO `" . DB_PREFIX . "seo_url` SET store_id = '" . (int)$this->config->get('config_store_id') . "', language_id = '" . (int)$language_id . "', query = 'menu_id=" . $menu_id . "', keyword = '" . $this->db->escape($slug) . "'");
                }
            }
        }

        return $menu_id;
    }

    public function edit($menu_id, $data)
    {
        // edit menu description
        if ($menu_id) {
            $this->load->model('localisation/language');
            $languages = $this->model_localisation_language->getLanguages();
            $menu_name = isset($data['menu_name']) ? $data['menu_name'] : '';
            foreach ($languages as $key => $value) {
                $language_id = $value['language_id'];
                $this->db->query("UPDATE " . DB_PREFIX . "menu_description SET name = '" . $this->db->escape($menu_name) . "' WHERE menu_id = " . (int)$menu_id . " AND language_id = " . (int)$language_id);
            }


            // remove all menu item not in array map_menu_edit  : // menu removed
            $list_ids = $this->db->query("SELECT menu_id FROM " . DB_PREFIX . "menu WHERE parent_id = " . (int)$menu_id);
            $list_ids = $list_ids->rows;
            $remove_ids = [];
            $map_menu_edit = (isset($data['map_menu_edit']) && is_array($data['map_menu_edit'])) ? $data['map_menu_edit'] : [];
            foreach ($list_ids as $item) {
                if (!isset($item['menu_id'])) {
                    continue;
                }
                if (in_array($item['menu_id'], $map_menu_edit)) {
                    continue;
                }
                $remove_ids[] = $item['menu_id'];
            }
            if (count($remove_ids) > 0) {
                foreach ($remove_ids as $remove_id) {
                    $this->deleteMenuById($remove_id);
                }
                /*$remove_ids = implode(', ', $remove_ids);
                $this->db->query("DELETE FROM " . DB_PREFIX . "menu WHERE menu_id IN (" . $remove_ids . ")");
                $this->db->query("DELETE FROM " . DB_PREFIX . "menu_description WHERE menu_id IN (" . $remove_ids . ")");*/
            }
            // add menu items
            if (isset($data['menu_item_name']) && is_array($data['menu_item_name'])) {
                foreach ($data['menu_item_name'] as $key => $menu_item_name) {
                    $menu_type_id = isset($data['type_id'][$key]) ? $data['type_id'][$key] : '';
                    $menu_target_value = isset($data['menu_item_value'][$key]) ? $data['menu_item_value'][$key] : '';
                    $menu_edit_id = isset($map_menu_edit[$key]) ? $map_menu_edit[$key] : false;
                    if ($menu_edit_id) {
                        $this->db->query("UPDATE " . DB_PREFIX . "menu SET menu_type_id = " . (int)$menu_type_id . ", target_value = '" . $this->db->escape($menu_target_value) . "' WHERE menu_id = " . (int)$menu_edit_id);
                        foreach ($languages as $k => $value) {
                            $language_id = $value['language_id'];
                            $this->db->query("UPDATE " . DB_PREFIX . "menu_description SET name = '" . $this->db->escape($menu_item_name) . "' WHERE menu_id = " . (int)$menu_edit_id . " AND language_id = " . (int)$language_id);
                        }
                    } else {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "menu SET parent_id = " . (int)$menu_id . ", menu_type_id = " . (int)$menu_type_id . ", target_value = '" . $this->db->escape($menu_target_value) . "'");
                        $menu_item_id = $this->db->getLastId();
                        if ($menu_item_id) {
                            foreach ($languages as $k => $value) {
                                $language_id = $value['language_id'];
                                $this->db->query("INSERT INTO " . DB_PREFIX . "menu_description SET menu_id = '" . (int)$menu_item_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($menu_item_name) . "'");
                            }
                        }
                    }
                }
            }
            // remove old seo
            $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'menu_id=" . $menu_id . "'");
            // insert SEO
            $slug = isset($data['slug']) ? $data['slug'] : $menu_name;
            if ($slug) {
                $this->load->model('custom/common');
                $slug = $this->model_custom_common->createSlug($slug, '-');
                $slug = $this->model_custom_common->getSlugUnique($slug);
                foreach ($languages as $key => $value) {
                    $language_id = $value['language_id'];
                    $this->db->query("INSERT INTO `" . DB_PREFIX . "seo_url` SET store_id = '" . (int)$this->config->get('config_store_id') . "', language_id = '" . (int)$language_id . "', query = 'menu_id=" . $menu_id . "', keyword = '" . $this->db->escape($slug) . "'");
                }
            }
        }

        return $menu_id;
    }

    public function insertMenu($data)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "menu SET parent_id = " . (int)$data['parent_id'] . ", menu_type_id = " . (int)$data['menu_type_id'] . ", target_value = " . $this->db->escape($data['target_value']));
        $menu_id = $this->db->getLastId();

        if ($menu_id) {
            $this->load->model('localisation/language');
            $menu_name = isset($data['menu_name']) ? $data['menu_name'] : '';
            $languages = $this->model_localisation_language->getLanguages();
            foreach ($languages as $key => $value) {
                $language_id = $value['language_id'];
                $this->db->query("INSERT INTO " . DB_PREFIX . "menu_description SET menu_id = '" . (int)$menu_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($menu_name) . "'");
            }
        }

        return $menu_id;
    }

    public function updateMenu($data)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "menu SET parent_id = " . (int)$data['parent_id'] . ", menu_type_id = " . (int)$data['menu_type_id'] . ", target_value = " . $this->db->escape($data['target_value']) . " WHERE menu_id = " . (int)$data['menu_id']);
        $menu_id = $this->db->getLastId();

        if ($menu_id) {
            $this->load->model('localisation/language');
            $menu_name = isset($data['menu_name']) ? $data['menu_name'] : '';
            $languages = $this->model_localisation_language->getLanguages();
            foreach ($languages as $key => $value) {
                $language_id = $value['language_id'];
                $this->db->query("UPDATE " . DB_PREFIX . "menu_description SET name = '" . $this->db->escape($menu_name) . "' WHERE menu_id = " . (int)$menu_id . " AND language_id = " . (int)$language_id);
            }
        }

        return $menu_id;
    }


    public function getMenuNameById($menu_id)
    {
        $sql = "SELECT md.name as name FROM " . DB_PREFIX . "menu as m LEFT JOIN " . DB_PREFIX . "menu_description as md ON (m.menu_id = md.menu_id) WHERE m.menu_id = " . (int)$menu_id . " AND md.language_id = " . (int)$this->config->get('config_language_id');
        $query = $this->db->query($sql);
        $name = isset($query->row['name']) ? $query->row['name'] : '';

        return $name;
    }

    public function getMenuById($menu_id)
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "menu as m LEFT JOIN " . DB_PREFIX . "menu_description as md ON (m.menu_id = md.menu_id) WHERE m.parent_id = " . (int)$menu_id . " AND md.language_id = " . (int)$this->config->get('config_language_id') . " ORDER BY m.menu_id ASC";
        $query = $this->db->query($sql);

        $menu = array();
        foreach ($query->rows as $row) {
            $items = $this->getMenuById($row['menu_id']);
            $menu[] = array(
                'menu_id' => $row['menu_id'],
                'menu_type_id' => $row['menu_type_id'],
                'target_value' => $row['target_value'],
                'parent_id' => $row['parent_id'],
                'name' => $row['name'],
                'items' => $items
            );

        }

        return $menu;
    }

    public function getAllMenu()
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "menu as m LEFT JOIN " . DB_PREFIX . "menu_description as md ON (m.menu_id = md.menu_id) WHERE m.parent_id IS NULL AND md.language_id = " . (int)$this->config->get('config_language_id') . " ORDER BY m.menu_id ASC";
        $query = $this->db->query($sql);

        $menu = array();
        foreach ($query->rows as $row) {
            $items = $this->getMenuById($row['menu_id']);
            $menu[] = array(
                'menu_id' => $row['menu_id'],
                'menu_type_id' => $row['menu_type_id'],
                'target_value' => $row['target_value'],
                'name' => $row['name'],
                'items' => $items
            );

        }

        return $menu;
    }

    public function getMenuTypes()
    {
        // temp commented
        // $language_id = $this->config->get('config_language_id');
        // temp hardcode. TODO: better multi lang...
        $this->load->model('localisation/language');
        $LANGUAGE_MAP = $this->model_localisation_language->getLanguagesMapCodeAndId();
        $language_id = isset($LANGUAGE_MAP[$this->config->get('config_admin_language')]) ? $LANGUAGE_MAP[$this->config->get('config_admin_language')] : $this->config->get('config_language_id');

        $sql = "SELECT * FROM " . DB_PREFIX . "menu_type as mt 
                LEFT JOIN " . DB_PREFIX . "menu_type_description as mtd ON (mt.menu_type_id = mtd.menu_type_id) 
                WHERE mtd.language_id = " . (int)$language_id;
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getMenuInfoById($menu_id)
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "menu as m LEFT JOIN " . DB_PREFIX . "menu_description as md ON (m.menu_id = md.menu_id) WHERE m.menu_id = " . (int)$menu_id . " AND md.language_id = " . (int)$this->config->get('config_language_id');
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getMenuParentIdByTarget($menu_type_id, $target_value)
    {
        $query = $this->db->query("SELECT parent_id, menu_id FROM " . DB_PREFIX . "menu WHERE menu_type_id = " . (int)$menu_type_id . " AND target_value = '" . $this->db->escape($target_value) . "' ORDER BY menu_id ASC");

        return $query->rows;
    }

    public function getMenuParentInfoByTarget($menu_type_id, $target_value)
    {
        $query = $this->db->query("SELECT parent_id FROM " . DB_PREFIX . "menu  WHERE menu_type_id = " . (int)$menu_type_id . " AND target_value = '" . $this->db->escape($target_value) . "' ORDER BY menu_id ASC");

        $result = array();
        $list_menu = array();
        $list_parent_ids = array();
        foreach ($query->rows as $row) {
            if (!in_array($row['parent_id'], $list_parent_ids)) {
                $list_parent_ids[] = $row['parent_id'];
            }

        }
        foreach ($list_parent_ids as $menu_id) {
            $menu_info = $this->getMenuInfoById($menu_id);
            if (!empty($menu_info)) {
                $list_menu[] = $menu_info;
            }
        }

        foreach ($list_menu as $menu) {
            $result[] = array(
                'id' => $menu['menu_id'],
                'title' => $menu['name']
            );
        }

        return $result;
    }

    public function deleteMenuById($menu_id)
    {
        $list_item = $this->db->query("SELECT menu_id FROM " . DB_PREFIX . "menu WHERE parent_id = " . (int)$menu_id);
        $list_item = $list_item->rows;
        $list_ids = [];
        foreach ($list_item as $item) {
            if (!isset($item['menu_id'])) {
                continue;
            }
            $list_ids[] = $item['menu_id'];
        }
        $list_ids[] = $menu_id;
        $list_ids = implode(', ', $list_ids);
        $this->db->query("DELETE FROM " . DB_PREFIX . "menu WHERE menu_id IN (" . $list_ids . ")");
        $this->db->query("DELETE FROM " . DB_PREFIX . "menu_description WHERE menu_id IN (" . $list_ids . ")");
    }

    public function removeFromMenu($parent_id, $menu_type_id, $target_value)
    {
        $query = $this->db->query("SELECT menu_id FROM " . DB_PREFIX . "menu WHERE parent_id = " . (int)$parent_id . " AND menu_type_id = " . (int)$menu_type_id . " AND target_value = '" . $this->db->escape($target_value) . "'");

        foreach ($query->rows as $row) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "menu WHERE menu_id = " . (int)$row['menu_id']);
            $this->db->query("DELETE FROM " . DB_PREFIX . "menu_description WHERE menu_id = " . (int)$row['menu_id']);
        }
    }

    public function getListMenuPaginator($filter_data)
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "menu as m LEFT JOIN " . DB_PREFIX . "menu_description as md on (m.menu_id = md.menu_id) WHERE 1";

        if (!empty($filter_data['filter_name'])) {
            $sql = $sql . " AND md.name LIKE '%" . $this->db->escape($filter_data['filter_name']) . "%'";
        }

        $sql = $sql . " AND md.language_id = " . (int)$this->config->get('config_language_id') . " ORDER BY m.menu_id ASC";

        $query = $this->db->query($sql);
        $data = array();
        foreach ($query->rows as $row) {
            $data[$row['menu_id']] = $row;
        }
        $data = $this->fillParent($data); // return array 1 level have menu and menu's parent
        $data = $this->buildcaseMenu($data); // return array menu case
        $data = $this->menuRender($data); // return array 1 level include level of menu : support for pagination
        $result = array();
        $result['count'] = count($data);
        if (!empty($filter_data['limit'])) {
            $limit = (int)$filter_data['limit'] < 1 ? 20 : $filter_data['limit'];
            $start = !empty($filter_data['start']) ? $filter_data['start'] : 0;
            $result['data'] = array_splice($data, $start, $limit);
        }

        return $result;
    }

    private function fillParent($data)
    {
        if (!is_array($data)) {
            return [];
        }

        foreach ($data as $k => $item) {
            if ($item['parent_id']) {
                if (!array_key_exists($item['parent_id'], $data)) {
                    $curent_parent = $this->getMenuInfoById($item['parent_id']);
                    if (isset($curent_parent['menu_id'])) {
                        $data[$curent_parent['menu_id']] = $curent_parent;

                        if (isset($curent_parent['parent_id']) && $curent_parent['parent_id']) {
                            $data = $this->fillParent($data);
                        }
                    }
                }
            }
        }

        return $data;
    }

    private function buildcaseMenu(array $data, $parent_id = 0)
    {
        if (!is_array($data)) {
            return [];
        }
        $case = array();
        foreach ($data as $item) {
            if ($item['parent_id'] != $parent_id) {
                continue;
            }
            $item['child'] = $this->buildcaseMenu($data, $item['menu_id']);
            $case[] = $item;
        }

        return $case;
    }

    private function menuRender(array $data, $level = 0)
    {
        if ($level > 2) { // Chỉ lấy 2 level đầu
            return [];
        }
        if (!is_array($data)) {
            return [];
        }

        $menu_array_1_level = array();
        foreach ($data as $item) {
            $item['level'] = $level;
            $menu_array_1_level[] = array(
                'menu_id' => $item['menu_id'],
                'parent_id' => $item['parent_id'],
                'menu_type_id' => $item['menu_type_id'],
                'name' => $item['name'],
                'level' => $level
            );
            if (!empty($item['child'])) {
                $menu_array_1_level = array_merge($menu_array_1_level, $this->menuRender($item['child'], $level + 1));
            }
        }

        return $menu_array_1_level;
    }

    // Check menu is added to parent menu
    public function isExistMenu($parent_id, $target_value)
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "menu as m WHERE m.parent_id = " . (int)$parent_id . " AND m.target_value = " . $target_value;
        $query = $this->db->query($sql);

        return $query->num_rows > 0 ? true : false;
    }

    public function getAllParentMenu()
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "menu as m WHERE m.parent_id IS NULL";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    /**
     * @param int $menu_id
     * @param array $new_menu_order e.g [2, 5, 6, 4, 3]
     * @return bool
     */
    public function reOrderMenuItem($menu_id, array $new_menu_order)
    {
        /* get old menu item datas */
        $menu_items = $this->getMenuById($menu_id);
        if (!is_array($menu_items) || empty($menu_items)) {
            return true;
        }

        $new_menu_order = array_map(function ($nmo) {
            return "mi_$nmo";
        }, $new_menu_order);

        /* sort old menu items by $new_menu_order */
        $new_menu_items = [];

        // map menu_id => menu_item
        foreach ($menu_items as $menu_item) {
            $new_menu_items['mi_' . $menu_item['menu_id']] = $menu_item;
        }

        // sort due to $new_menu_order
        $new_menu_items = array_merge(array_flip($new_menu_order), $new_menu_items);

        /* update menu due to new menu item order */
        foreach ($new_menu_items as $new_menu_item) {
            $old_menu_id = $new_menu_item['menu_id'];
            $menu_parent_id = $menu_id;
            $DB_PREFIX = DB_PREFIX;

            /* delete menu item first */
            $sql_delete = "DELETE FROM `{$DB_PREFIX}menu` WHERE menu_id = {$old_menu_id}";
            $this->db->query($sql_delete);

            /* insert new by new menu order */
            $sql = "INSERT INTO `{$DB_PREFIX}menu` (parent_id, menu_type_id, target_value) VALUES 
                    ({$menu_parent_id}, '{$new_menu_item['menu_type_id']}', '{$new_menu_item['target_value']}')";
            $this->db->query($sql);
            $new_menu_id = $this->db->getLastId();

            /* update menu_id in menu_description table*/
            $this->db->query("UPDATE {$DB_PREFIX}menu_description SET menu_id = '{$new_menu_id}' WHERE menu_id = " . (int)$old_menu_id);

            /* update menu_id in seo_url table*/
            $this->db->query("UPDATE {$DB_PREFIX}seo_url SET query = 'menu_id={$new_menu_id}' WHERE query = 'menu_id=" . (int)$old_menu_id . "'");

            /* update menu_id in menu table for all children */
            $this->db->query("UPDATE {$DB_PREFIX}menu SET parent_id = {$new_menu_id} WHERE parent_id = " . (int)$old_menu_id);
        }

        return true;
    }
}