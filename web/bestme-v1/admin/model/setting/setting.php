<?php

class ModelSettingSetting extends Model
{
    public function getSetting($code, $store_id = 0)
    {
        $setting_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `code` = '" . $this->db->escape($code) . "'");

        foreach ($query->rows as $result) {
            if (!$result['serialized']) {
                $setting_data[$result['key']] = $result['value'];
            } else {
                $setting_data[$result['key']] = json_decode($result['value'], true);
            }
        }

        return $setting_data;
    }

    public function editSetting($code, $data, $store_id = 0)
    {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE store_id = '" . (int)$store_id . "' AND `code` = '" . $this->db->escape($code) . "'");

        foreach ($data as $key => $value) {
            if (substr($key, 0, strlen($code)) == $code) {
                if (!is_array($value)) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
                } else {
                    $test = $value;
                    $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(json_encode($value, true)) . "', serialized = '1'");
                }
            }
        }
    }

    public function editExistingSetting($code, $data, $store_id = 0)
    {
        foreach ($data as $key => $value) {
            $this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE store_id = '" . (int)$store_id . "' AND `code` = '" . $this->db->escape($code) . "' AND `key` = '" . $this->db->escape($key) . "'");
            if (substr($key, 0, strlen($code)) == $code) {
                if (!is_array($value)) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(json_encode($value, true)) . "', serialized = '1'");
                }
            }
        }
    }

    public function deleteSetting($code, $store_id = 0)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `code` = '" . $this->db->escape($code) . "'");
    }

    public function deleteSettingValue($code, $key, $store_id = 0)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `code` = '" . $this->db->escape($code) . "' AND `key` = '". $key ."'");
    }

    public function getSettingValue($key, $store_id = 0)
    {
        $query = $this->db->query("SELECT value FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `key` = '" . $this->db->escape($key) . "'");

        if ($query->num_rows) {
            return $query->row['value'];
        } else {
            return null;
        }
    }

    public function getSettingModuleValue($key, $store_id = 0) {
        $query = $this->db->query("SELECT value FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `key` = '" . $this->db->escape($key) . "'");

        if ($query->num_rows) {
            return $query->row['value'];
        } else {
            return 1;
        }
    }

    public function getSettingPacketWaitForPayValue($key, $store_id = 0) {
        $query = $this->db->query("SELECT value FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `key` = '" . $this->db->escape($key) . "'");

        if ($query->num_rows) {
            return $query->row['value'];
        } else {
            return 0;
        }
    }

    public function editSettingValue($code = '', $key = '', $value = '', $store_id = 0)
    {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE store_id = '" . (int)$store_id . "' AND `code` = '" . $this->db->escape($code) . "' AND `key` = '" . $this->db->escape($key) . "'");

        if (!is_array($value)) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
        } else {
            $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(json_encode($value, true)) . "', serialized = '1'");
        }
    }



    public function updateThemeSettings($code, $data, $store_id = 0)
    {  // If exists then update else insert
        if (!is_array($data) || empty($data) || !$code) {
            return;
        }
//        $sql = "INSERT INTO " . DB_PREFIX . "setting (`store_id`, `code`, `key`, `value`, `serialized`) VALUES ";
//        $flag = false;
//        foreach ($data as $key => $value) {
//            if ($flag) {
//                $sql = $sql . ",";
//            }
//            $flag = true;
//            $sql = $sql . "('" . $store_id . "', 'config', '" . $key . "', '" . $value . "', 0) ";
//        }
//
//        $sql = $sql . " ON DUPLICATE KEY UPDATE `key` = VALUES(`key`)";
//
//        $this->db->query($sql);
        foreach ($data as $key => $value) {
            $setting = $this->getSettingByKey($key);
            if (empty($setting) || !isset($setting['setting_id'])) {
                $sql = "INSERT INTO " . DB_PREFIX . "setting (`store_id`, `code`, `key`, `value`, `serialized`) VALUES ('" . $store_id . "', '" . $code . "', '" . $key . "', '" . $value . "', 0)";

                $this->db->query($sql);
            } else {
                $sql = "UPDATE " . DB_PREFIX . "setting SET `store_id` = '" . (int)$store_id . "', `code` = '" . $code . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "', `serialized` = '0' WHERE `setting_id` = '" . (int)$setting['setting_id'] . "'";

                $this->db->query($sql);
            }

        }
    }

    public function getMultiSettingValue($keys, $store_id = 0)
    {
        $keys = "'" . implode("','", $keys) . "'";
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `key` IN (" . $keys . ")");

        return $query->rows;
    }

    public function updateMultiSettingValue($data, $store_id = 0)
    {  // If exists then update else insert
        if (!is_array($data) || empty($data)) {
            return;
        }
//        $sql = "INSERT INTO " . DB_PREFIX . "setting (`store_id`, `code`, `key`, `value`, `serialized`) VALUES ";
//        $flag = false;
//        foreach ($data as $key => $value) {
//            if ($flag) {
//                $sql = $sql . ",";
//            }
//            $flag = true;
//            $sql = $sql . "('" . $store_id . "', 'config', '" . $key . "', '" . $value . "', 0) ";
//        }
//
//        $sql = $sql . " ON DUPLICATE KEY UPDATE `key` = VALUES(`key`)";
//
//        $this->db->query($sql);
        foreach ($data as $key => $value) {
            $setting = $this->getSettingByKey($key);
            if (empty($setting) || !isset($setting['setting_id'])) {
                $sql = "INSERT INTO " . DB_PREFIX . "setting (`store_id`, `code`, `key`, `value`, `serialized`) VALUES ('" . $store_id . "', 'config', '" . $key . "', '" . $value . "', 0)";

                $this->db->query($sql);
            } else {
                $sql = "UPDATE " . DB_PREFIX . "setting SET `store_id` = '" . (int)$store_id . "', `code` = 'config', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "', `serialized` = '0' WHERE `setting_id` = '" . (int)$setting['setting_id'] . "'";

                $this->db->query($sql);
            }

        }
    }

    public function getSettingByKey($key, $store_id = 0){
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `key` = '" . $this->db->escape($key) . "'");

        return $query->row;
    }

    /**
     * Get shopname for common use
     *
     * @return mixed
     */
    public function getShopName()
    {
        $query = $this->db->query("SELECT `value` FROM `" . DB_PREFIX . "setting` WHERE `key`= 'shop_name'");
        return $query->row['value'];
    }

    /**
     * Get shop url for common use
     * @return mixed
     */
    public function getShopUrl()
    {
        // do not hardcode. TODO: config http or https
        return sprintf("https://%s%s", $this->getShopName(), SHOP_NAME_SUFFIX);
    }

    public function addConfig($key, $value, $store_id = 0)
    {
        $setting = $this->getSettingByKey($key);
        if (empty($setting) || !isset($setting['setting_id'])) {
            $sql = "INSERT INTO " . DB_PREFIX . "setting (`store_id`, `code`, `key`, `value`, `serialized`) VALUES ('" . $store_id . "', 'config', '" . $key . "', '" . $value . "', 0)";

            $this->db->query($sql);
        } else {
            $sql = "UPDATE " . DB_PREFIX . "setting SET `store_id` = '" . (int)$store_id . "', `code` = 'config', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "', `serialized` = '0' WHERE `setting_id` = '" . (int)$setting['setting_id'] . "'";

            $this->db->query($sql);
        }
    }

    public function increaseNumberConfig($key, $store_id = 0)
    {
        $value = 1;
        $setting = $this->getSettingByKey($key);
        if (empty($setting) || !isset($setting['setting_id'])) {
            $sql = "INSERT INTO " . DB_PREFIX . "setting (`store_id`, `code`, `key`, `value`, `serialized`) VALUES ('" . $store_id . "', 'config', '" . $key . "', '1', 0)";

            $this->db->query($sql);
        } else {
            $sql = "UPDATE " . DB_PREFIX . "setting SET `store_id` = '" . (int)$store_id . "', `code` = 'config', `key` = '" . $this->db->escape($key) . "', `value` = `value` + 1, `serialized` = '0' WHERE `setting_id` = '" . (int)$setting['setting_id'] . "'";

            $this->db->query($sql);
            $value = $setting['value'] + 1;
        }

        return $value;
    }
}
