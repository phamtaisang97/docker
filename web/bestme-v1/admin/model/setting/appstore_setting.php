<?php
class ModelSettingAppstoreSetting extends Model {
    public function addModule($code, $data) {
        $this->db->query("INSERT INTO `" . DB_PREFIX . "appstore_setting` SET `name` = '" . $this->db->escape($data['name']) . "', `code` = '" . $this->db->escape($code) . "', `setting` = '" . $this->db->escape(json_encode($data)) . "'");
    }

    public function editModule($module_id, $data) {
        $this->db->query("UPDATE `" . DB_PREFIX . "appstore_setting` SET `name` = '" . $this->db->escape($data['name']) . "', `setting` = '" . $this->db->escape(json_encode($data)) . "' WHERE `module_id` = '" . (int)$module_id . "'");
    }

    public function deleteModule($module_id) {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "appstore_setting` WHERE `module_id` = '" . (int)$module_id . "'");
        //$this->db->query("DELETE FROM `" . DB_PREFIX . "layout_appstore` WHERE `code` LIKE '%." . (int)$module_id . "'");
    }

    public function getModule($module_id) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "appstore_setting` WHERE `module_id` = '" . (int)$module_id . "'");

        if ($query->row) {
            return json_decode($query->row['setting'], true);
        } else {
            return array();
        }
    }

    public function getModules() {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "appstore_setting` ORDER BY `code`");

        return $query->rows;
    }

    public function getModulesByCode($code) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "appstore_setting` WHERE `code` = '" . $this->db->escape($code) . "' ORDER BY `name`");

        return $query->rows;
    }

    public function deleteModulesByCode($code) {
        /* Xóa các module và cài đặt vị trí các module của app */
        $sqlModules = "SELECT module_id FROM " . DB_PREFIX . "appstore_setting  
                WHERE `code` = '" . $this->db->escape($code) . "'";
        $modules = $this->db->query($sqlModules)->rows;
        foreach($modules as $module){
            $sqlDeleteModule = "DELETE FROM " . DB_PREFIX . "appstore_setting
                            WHERE `module_id` = '" .$module['module_id'] ."' ";

            $sqlDeleteConfig = "DELETE FROM " . DB_PREFIX . "app_config_theme
                            WHERE `module_id` = '" .$module['module_id'] ."' ";

            $this->db->query($sqlDeleteModule);
            $this->db->query($sqlDeleteConfig);
        }
        //$this->db->query("DELETE FROM `" . DB_PREFIX . "appstore_setting` WHERE `code` = '" . $this->db->escape($code) . "'");
        //$this->db->query("DELETE FROM `" . DB_PREFIX . "layout_appstore` WHERE `code` LIKE '" . $this->db->escape($code) . "' OR `code` LIKE '" . $this->db->escape($code . '.%') . "'");
    }
}