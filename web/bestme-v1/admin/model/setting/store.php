<?php
class ModelSettingStore extends Model {
	public function addStore($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "store SET name = '" . $this->db->escape($data['config_name']) . "', `url` = '" . $this->db->escape($data['config_url']) . "', `ssl` = '" . $this->db->escape($data['config_ssl']) . "'");

		$store_id = $this->db->getLastId();

		// Layout Route
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "layout_route WHERE store_id = '0'");

		foreach ($query->rows as $layout_route) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "layout_route SET layout_id = '" . (int)$layout_route['layout_id'] . "', route = '" . $this->db->escape($layout_route['route']) . "', store_id = '" . (int)$store_id . "'");
		}

		$this->cache->delete('store');

		return $store_id;
	}

	public function editStore($store_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "store SET name = '" . $this->db->escape($data['config_name']) . "', `url` = '" . $this->db->escape($data['config_url']) . "', `ssl` = '" . $this->db->escape($data['config_ssl']) . "' WHERE store_id = '" . (int)$store_id . "'");

		$this->cache->delete('store');
	}

	public function deleteStore($store_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "store WHERE store_id = '" . (int)$store_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "layout_route WHERE store_id = '" . (int)$store_id . "'");

		$this->cache->delete('store');
	}

	public function getStore($store_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "store WHERE store_id = '" . (int)$store_id . "'");

		return $query->row;
	}

    /**
     * get a random store id
     * @return mixed|null
     */
	public function getRandomStoreId() {
        $DB_PREFIX = DB_PREFIX;
	    $sql = "SELECT `store_id` FROM `{$DB_PREFIX}store` ORDER BY RAND() LIMIT 1";
		$query = $this->db->query($sql);

		return isset($query->row['store_id']) ? $query->row['store_id'] : null;
	}

    /**
     * get a random store id not get default store id = 0
     * @return mixed|null
     */
    public function getRandomStoreIdNotDefaultStoreId() {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT `store_id` FROM `{$DB_PREFIX}store` WHERE `store_id` <> 0 ORDER BY RAND() LIMIT 1";
        $query = $this->db->query($sql);

        return isset($query->row['store_id']) ? $query->row['store_id'] : null;
    }

    public function getStoreNameById($store_id)
    {
        $query = $this->db->query("SELECT `name` FROM `" . DB_PREFIX . "store` WHERE `store_id` = " .(int)$store_id);

        if (isset($query->row['name'])){
            return $query->row['name'];
        }

        return '';
    }

	public function getStores($data = array()) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "store ORDER BY `store_id` DESC");
        $store_data = $query->rows;

		return $store_data;
	}

	public function getStoreForUserLogin()
    {
        $this->load->model('user/user_group');
        $this->load->model('user/user');
        $current_user_id = $this->user->getId();

        if (!$current_user_id) {
            return;
        }

        if ($this->user->canAccessAll()) {
            $stores = $this->getStores();
        } else {
            $stores = $this->model_user_user->getUserStoreForList($current_user_id);
        }

        return $stores;
    }

	public function getStorePaginator($data = [])
    {
        $sql = "SELECT st.store_id as id, st.name as text FROM " . DB_PREFIX . "store as st WHERE 1";

        if (!empty($data['filter_name'])) {
            $sql .= " AND st.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['store_ids'])) {
            $store_id = implode(",", $data['store_ids']);
            $sql .= " AND st.store_id NOT IN (" . $store_id . ")";
        }

        if (isset($data['store_list']) && $data['store_list']) {
            $sql .= " ORDER BY st.store_id DESC";
        } else {
            $sql .= " ORDER BY st.name ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

	public function getStoresForUser(array $data)
    {
        $this->load->model('user/user_group');
        $this->load->model('user/user');
        $current_user_id = $this->user->getId();

        if (!$current_user_id) {
            return;
        }

        if ($this->user->canAccessAll()) {
            return $this->getStorePaginator($data);
        } else {
            return $this->getStorePaginatorForCurrentUser($data, $current_user_id);
        }
    }

    public function getStorePaginatorForCurrentUser(array $data, $user_id)
    {
        $sql = "SELECT us.store_id as id, s.name as text 
                  FROM " . DB_PREFIX . "user_store as us 
                    INNER JOIN " . DB_PREFIX . "store as s ON us.store_id = s.store_id 
                      WHERE us.user_id=" . (int)$user_id . " ";

        if (!empty($data['filter_name'])) {
            $sql .= " AND s.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['store_ids'])) {
            $store_id = implode(",", $data['store_ids']);
            $sql .= " AND s.store_id NOT IN (" . $store_id . ")";
        }

        $sql .= " ORDER BY us.id ASC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalStoreFilter($data = [])
    {
        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "store WHERE 1";

        if (!empty($data['filter_name'])) {
            $sql .= " AND name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['store_ids'])) {
            $store_id = implode(",", $data['store_ids']);
            $sql .= " AND store_id NOT IN (" . $store_id . ")";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalUserStoreFilter($data = [])
    {
        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "store WHERE 1";

        if (!empty($data['filter_name'])) {
            $sql .= " AND name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['store_ids'])) {
            $sql .= " AND store_id IN (" . implode(',', $this->user->getUserStores()) . ")";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getUserStorePaginator($data = [])
    {
        $sql = "SELECT st.store_id as id, st.name as text FROM " . DB_PREFIX . "store as st WHERE 1";

        if (!empty($data['filter_name'])) {
            $sql .= " AND st.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }


        if (!empty($data['store_ids'])) {
            $sql .= " AND st.store_id IN (" . implode(',', $this->user->getUserStores()) . ")";
        }

        $sql .= " ORDER BY st.name ASC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalStoreForUser(array $data)
    {
        $this->load->model('user/user_group');
        $this->load->model('user/user');
        $current_user_id = $this->user->getId();

        if (!$current_user_id) {
            return;
        }

        if ($this->user->canAccessAll()) {
            return $this->getTotalStoreFilter($data);
        } else {
            return $this->getTotalStoreFilterForCurrentUser($data, $current_user_id);
        }
    }

    public function getTotalStoreFilterForCurrentUser(array $data, $user_id)
    {
        $sql = "SELECT COUNT(us.store_id) as total 
                    FROM " . DB_PREFIX . "user_store as us 
                     INNER JOIN " . DB_PREFIX . "store as s ON us.store_id = s.store_id 
                      WHERE us.user_id=" . (int)$user_id;

        if (!empty($data['filter_name'])) {
            $sql .= " AND s.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['store_ids'])) {
            $store_id = implode(",", $data['store_ids']);
            $sql .= " AND s.store_id NOT IN (" . $store_id . ")";
        }

        $sql .= " ORDER BY us.id ASC";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

	public function getTotalStores() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "store");

		return $query->row['total'];
	}

	public function getTotalStoresByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "setting WHERE `key` = 'config_layout_id' AND `value` = '" . (int)$layout_id . "' AND store_id != '0'");

		return $query->row['total'];
	}

	public function getTotalStoresByLanguage($language) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "setting WHERE `key` = 'config_language' AND `value` = '" . $this->db->escape($language) . "' AND store_id != '0'");

		return $query->row['total'];
	}

	public function getTotalStoresByCurrency($currency) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "setting WHERE `key` = 'config_currency' AND `value` = '" . $this->db->escape($currency) . "' AND store_id != '0'");

		return $query->row['total'];
	}

	public function getTotalStoresByCountryId($country_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "setting WHERE `key` = 'config_country_id' AND `value` = '" . (int)$country_id . "' AND store_id != '0'");

		return $query->row['total'];
	}

	public function getTotalStoresByZoneId($zone_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "setting WHERE `key` = 'config_zone_id' AND `value` = '" . (int)$zone_id . "' AND store_id != '0'");

		return $query->row['total'];
	}

	public function getTotalStoresByCustomerGroupId($customer_group_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "setting WHERE `key` = 'config_customer_group_id' AND `value` = '" . (int)$customer_group_id . "' AND store_id != '0'");

		return $query->row['total'];
	}

	public function getTotalStoresByInformationId($information_id) {
		$account_query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "setting WHERE `key` = 'config_account_id' AND `value` = '" . (int)$information_id . "' AND store_id != '0'");

		$checkout_query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "setting WHERE `key` = 'config_checkout_id' AND `value` = '" . (int)$information_id . "' AND store_id != '0'");

		return ($account_query->row['total'] + $checkout_query->row['total']);
	}

	public function getTotalStoresByOrderStatusId($order_status_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "setting WHERE `key` = 'config_order_status_id' AND `value` = '" . (int)$order_status_id . "' AND store_id != '0'");

		return $query->row['total'];
	}

	public function getTotalQuantityProductOfStoreByStoreId($store_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $query = $this->db->query("SELECT COUNT(DISTINCT product_id) as total FROM `{$DB_PREFIX}product_to_store` WHERE store_id =".(int)$store_id);

        return $query->row['total'];
    }

    public function getTotalStaffsOfStoreByStoreId($store_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $query = $this->db->query("SELECT COUNT(user_id) as total FROM `{$DB_PREFIX}user_store` WHERE store_id =".(int)$store_id);

        return $query->row['total'];
    }

    public function getTotalStoreReceiptGoodsDeliveringOfStoreByStoreId($store_id)
    {
        $DB_PREFIX = DB_PREFIX;
       $sql = "SELECT COUNT(store_receipt_id) as total 
                                    FROM `{$DB_PREFIX}store_receipt` 
                                    WHERE store_id = " . (int)$store_id;
       $sql .= " AND status = 0";
       $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalStoreTransReceiptDraftAndTransferredOfStoreByStoreId($store_id)
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT COUNT(store_transfer_receipt_id) as total 
                                    FROM `{$DB_PREFIX}store_transfer_receipt` 
                                    WHERE import_store_id = " . (int)$store_id;
        $sql .= " AND status IN(0,1)";
        $query = $this->db->query($sql);

        return $query->row['total'];
    }
}