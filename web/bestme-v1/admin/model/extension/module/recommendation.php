<?php

class ModelExtensionModuleRecommendation extends Model
{
    public function install()
    {
        $this->log->write('Installing ModelExtensionModuleRecommendation (CREATE TABLE IF NOT EXISTS)...');

        $this->load->model('extension/module/recommendation');

        $this->db->query("
		CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "recommendation` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `event_setting` varchar(255) NOT NULL ,
		  PRIMARY KEY (`id`)
		) DEFAULT COLLATE=utf8_general_ci;");

        $this->log->write('Installing ModelExtensionModuleRecommendation (CREATE TABLE IF NOT EXISTS)... done!');
    }

    public function uninstall()
    {
        $this->log->write('Uninstalling ModelExtensionModuleRecommendation (DROP TABLE IF EXISTS)...');

        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "recommendation`");

        $this->log->write('Uninstalling ModelExtensionModuleRecommendation (DROP TABLE IF EXISTS)... done!');
    }

    public function saveEventSetting($id, array $eventSetting = [])
    {
        if (!is_array($eventSetting)) {
            $eventSetting = [];
        }

        $event_setting_encoded = json_encode($eventSetting);

        $this->db->query("
			INSERT INTO `" . DB_PREFIX . "recommendation`
			SET `event_setting` = '" . $this->db->escape($event_setting_encoded) . "'
			WHERE `id` = '" . (int)$id . "'");
    }

    public function getEventSetting($id) {
        return $this->db->query("
			SELECT * FROM `" . DB_PREFIX . "recommendation`
			WHERE `id` = '" . $id . "'")->rows;
    }
}