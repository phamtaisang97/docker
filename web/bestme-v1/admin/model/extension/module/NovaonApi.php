<?php
class ModelExtensionModuleNovaonApi extends Model {
	public function updateCredentials($testimonial_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "novaon_api_credentials SET consumer_key = '" . $this->db->escape($data['consumer_key']) . "', consumer_secret = '" . $this->db->escape($data['consumer_secret']) . "', create_date = CURRENT_TIMESTAMP WHERE status = '1'");
		$this->cache->delete('NovaonApi');
	}

	public function getApiDetails() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "novaon_api_credentials WHERE status = '1' order by id DESC limit 1 ");
		return $query->rows;
	}
}
?>
