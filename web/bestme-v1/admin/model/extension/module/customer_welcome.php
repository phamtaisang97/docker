<?php

class ModelExtensionModuleCustomerWelcome extends Model
{
    public function install()
    {
        $this->log->write('Installing ModelExtensionModuleCustomerWelcome (CREATE TABLE IF NOT EXISTS)...');

        $this->load->model('extension/module/customer_welcome');

        $this->db->query("
		CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "customer_welcome_config` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `customer_id` varchar(255) NOT NULL ,
		  `is_shown_welcome_message` tinyint(1) NOT NULL DEFAULT '0',
		  PRIMARY KEY (`id`)
		) DEFAULT COLLATE=utf8_general_ci;");

        $this->log->write('Installing ModelExtensionModuleCustomerWelcome (CREATE TABLE IF NOT EXISTS)... done!');
    }

    public function uninstall()
    {
        $this->log->write('Uninstalling ModelExtensionModuleCustomerWelcome (DROP TABLE IF EXISTS)...');

        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customer_welcome_config`");

        $this->log->write('Uninstalling ModelExtensionModuleCustomerWelcome (DROP TABLE IF EXISTS)... done!');
    }
}