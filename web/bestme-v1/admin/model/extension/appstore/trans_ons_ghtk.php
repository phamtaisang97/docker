<?php
class ModelExtensionAppstoreTransOnsGhtk extends Model
{
    function createTables()
    {
        $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "db_app_trans_ons_ghtk_account (
                id INT(11) NOT NULL AUTO_INCREMENT,
                ghtk_username VARCHAR(255) NOT NULL,
                ghtk_token VARCHAR(255) NOT NULL,
                PRIMARY KEY (id)
            )
            COLLATE='utf8_general_ci'
            ENGINE=MyISAM;");
    }
    function dropTables(){
        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "db_app_trans_ons_ghtk_account`");
    }

    function addInfoAccountGHTK($data) {
        $sql = "INSERT INTO " . DB_PREFIX . "db_app_trans_ons_ghtk_account" . " (ghtk_username, ghtk_token)
                VALUES ('" . $data['ghtk_username'] . "', '" . $data['ghtk_token'] . "')";
        $this->db->query($sql);
        return $this->db->getLastId();
    }

    function getLastAccountGHTK() {
        $sql = "SELECT * FROM " . DB_PREFIX . "db_app_trans_ons_ghtk_account ORDER BY id DESC LIMIT 1";
        $query = $this->db->query($sql);
        return $query->row;
    }

    function deleteAccountGHTKById($id) {
        $sql = "DELETE FROM " . DB_PREFIX . "db_app_trans_ons_ghtk_account" . " where id = " . $id;
        $this->db->query($sql);
    }

}
