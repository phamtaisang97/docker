<?php
class ModelExtensionAppstorePaymOnsVnpay extends Model
{
    function createTables()
    {
        $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "db_app_payment_vn_pay_account (
                id INT(11) NOT NULL AUTO_INCREMENT,
                vnp_web_code VARCHAR(255) NOT NULL,
                vnp_hash_secret VARCHAR(255) NOT NULL,
                PRIMARY KEY (id)
            )
            COLLATE='utf8_general_ci'
            ENGINE=MyISAM;");
    }
    function dropTables(){
        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "db_app_payment_vn_pay_account`");
    }

    function addInfoAccountPayment($data) {
        $sql = "INSERT INTO " . DB_PREFIX . "db_app_payment_vn_pay_account" . " (vnp_web_code, vnp_hash_secret)
                VALUES ('" . $data['vnp_web_code'] . "', '" . $data['vnp_hash_secret'] . "')";

        $this->db->query($sql);
    }

    function getAccountPayment() {
        $sql = "SELECT * FROM " . DB_PREFIX . "db_app_payment_vn_pay_account";
        $accountPayment = $this->db->query($sql);
        return $accountPayment;
    }

    function updateInfoAccountPayment($data, $accountPaymentId) {
        $sql = "UPDATE " . DB_PREFIX . "db_app_payment_vn_pay_account  
                SET vnp_web_code = '" . $data['vnp_web_code'] . "', 
                    vnp_hash_secret = '" . $data['vnp_hash_secret'] . "' 
                WHERE id = " . (int)$accountPaymentId;
        $this->db->query($sql);
    }
}
