<?php
class ModelLocalisationTimeZone extends Model {
	public function getTimeZone($id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "timezones WHERE id = '" . (int)$id . "'");

		return $query->row;
	}

	public function getTimeZoneByValue($value) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "timezones WHERE value = '" . $this->db->escape($value) . "'");

		return $query->row;
	}

	public function getTimeZones() {
		$sql = "SELECT * FROM " . DB_PREFIX . "timezones";

		$query = $this->db->query($sql);

		return $query->rows;
	}
}