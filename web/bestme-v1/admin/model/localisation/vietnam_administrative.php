<?php

class ModelLocalisationVietnamAdministrative extends Model
{
    const DB_NAME_PROVINCE = 'novaon_tinhthanhpho';
    const DB_NAME_DISTRICT = 'novaon_quanhuyen';
    const DB_NAME_WARD = 'novaon_xaphuongthitran';

    const JSON_FILE_PROVINCE = DIR_APPLICATION . 'model/localisation/vietnam/province.json';
    const JSON_FILE_DISTRICT = DIR_APPLICATION . 'model/localisation/vietnam/district.json';
    const JSON_FILE_WARD = DIR_APPLICATION . 'model/localisation/vietnam/ward.json';

    protected $cached_provinces_by_code = [];
    protected $cached_provinces_by_name = [];
    protected $cached_districts_by_code = [];
    protected $cached_wards_by_code = [];

    /**
     * @return array
     */
    public function getProvinces()
    {
        // try from db
        /*try {
            $query = $this->db->query("SELECT `matp` as `code`, `name`, `type` FROM " . self::DB_NAME_PROVINCE);

            return $query->rows;
        } catch (Exception $e) {
        }*/

        // try again from file
        if (empty($this->cached_provinces_by_name)) {
            $this->getProvincesFromFile();
        }

        // then get from cache
        if (!empty($this->cached_provinces_by_name)) {
            $result = $this->cached_provinces_by_name;

            // sort alphabet asc
            uksort($result, function ($k1, $k2) {
                $k1 = escapeVietnamese($k1);
                $k2 = escapeVietnamese($k2);

                if ($k1 == $k2) {
                    return 0;
                }

                return ($k1 < $k2) ? -1 : 1;
            });

            return $result;
        }

        return [];
    }

    /**
     * @param $code
     * @return mixed
     */
    public function getProvinceByCode($code)
    {
        // try from db
        /*try {
            $query = $this->db->query("SELECT `matp` as `code`, `name`, `type` FROM " . self::DB_NAME_PROVINCE . " WHERE `matp` = '" . $code . "'");

            return $query->row;
        } catch (Exception $e) {
        }*/

        // try again from file
        if (empty($this->cached_provinces_by_code) && !array_key_exists($code, $this->cached_provinces_by_code)) {
            $this->getProvincesFromFile();
        }

        // then get from cache
        if (!empty($this->cached_provinces_by_code) && array_key_exists($code, $this->cached_provinces_by_code)) {
            return $this->cached_provinces_by_code[$code];
        }

        return null;
    }

    /**
     * @return array
     */
    public function getDistricts()
    {
        // try from db
        /*try {
            $query = $this->db->query("SELECT `maqh` as `code`, `name`, `type` FROM " . self::DB_NAME_DISTRICT . " WHERE 1");

            return $query->rows;
        } catch (Exception $e) {
        }*/

        // try again from file
        if (empty($this->cached_districts_by_code)) {
            $this->getDistrictsFromFile();
        }

        // then get from cache
        if (!empty($this->cached_districts_by_code)) {
            // TODO: also cache by province code...
            $districts = [];
            foreach ($this->cached_districts_by_code as $district) {
                if (!is_array($district) ||
                    !array_key_exists('name', $district)
                ) {
                    continue;
                }

                $districts[$district['name']] = [
                    'code' => $district['code'],
                    'name' => $district['name'],
                    'name_with_type' => $district['name_with_type']
                ];
            }

            // sort alphabet asc
            uksort($districts, function ($k1, $k2) {
                $k1 = escapeVietnamese($k1);
                $k2 = escapeVietnamese($k2);

                if ($k1 == $k2) {
                    return 0;
                }

                return ($k1 < $k2) ? -1 : 1;
            });

            return $districts;
        }

        return [];
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getProvinceByName($name)
    {
        // try from db
        /*try {
            $query = $this->db->query("SELECT `matp` as `code`, `name`, `type` FROM " . self::DB_NAME_PROVINCE . " WHERE `name` = '" . $name . "'");

            return $query->row;
        } catch (Exception $e) {
            $result = null;
        }*/

        // try again from file
        if (empty($this->cached_provinces_by_name) && !array_key_exists($name, $this->cached_provinces_by_name)) {
            $this->getProvincesFromFile();
        }

        // then get from cache
        if (!empty($this->cached_provinces_by_name) && array_key_exists($name, $this->cached_provinces_by_name)) {
            return $this->cached_provinces_by_name[$name];
        }

        return null;
    }

    /**
     * @param $provinceCode
     * @return array
     */
    public function getDistrictsByProvinceCode($provinceCode)
    {
        // try from db
        /*try {
            $query = $this->db->query("SELECT `maqh` as `code`, `name`, `type` FROM " . self::DB_NAME_DISTRICT . " WHERE `matp` = '" . $provinceCode . "'");

            return $query->rows;
        } catch (Exception $e) {
        }*/

        // try again from file
        if (empty($this->cached_districts_by_code)) {
            $this->getDistrictsFromFile();
        }

        // then get from cache
        if (!empty($this->cached_districts_by_code)) {
            // TODO: also cache by province code...
            $districts = [];
            foreach ($this->cached_districts_by_code as $district) {
                if (!is_array($district) ||
                    !array_key_exists('name', $district) ||
                    !array_key_exists('parent_code', $district) ||
                    $district['parent_code'] !== $provinceCode
                ) {
                    continue;
                }

                $districts[$district['name']] = [
                    'code' => $district['code'],
                    'name' => $district['name'],
                    'name_with_type' => $district['name_with_type']
                ];
            }

            // sort alphabet asc
            uksort($districts, function ($k1, $k2) {
                $k1 = escapeVietnamese($k1);
                $k2 = escapeVietnamese($k2);

                if ($k1 == $k2) {
                    return 0;
                }

                return ($k1 < $k2) ? -1 : 1;
            });

            return $districts;
        }

        return [];
    }

    /**
     * @param $code
     * @return mixed
     */
    public function getDistrictByCode($code)
    {
        // try from db
        /*try {
            $query = $this->db->query("SELECT `maqh` as `code`, `name`, `type` FROM " . self::DB_NAME_DISTRICT . " WHERE `maqh` = '" . $code . "'");

            return $query->row;
        } catch (Exception $e) {
        }*/

        // try again from file
        if (empty($this->cached_districts_by_code) && !array_key_exists($code, $this->cached_districts_by_code)) {
            $this->getDistrictsFromFile();
        }

        // then get from cache
        if (!empty($this->cached_districts_by_code) && array_key_exists($code, $this->cached_districts_by_code)) {
            $district = $this->cached_districts_by_code[$code];
            if (!is_array($district) ||
                !array_key_exists('code', $district) ||
                !array_key_exists('name', $district)
            ) {
                return null;
            }

            return [
                'code' => $district['code'],
                'name' => $district['name'],
                'name_with_type' => $district['name_with_type']
            ];
        }

        return null;
    }

    /**
     * @return array
     */
    public function getWards()
    {
        // try from db
        /*try {
            $query = $this->db->query("SELECT `xaid` as `code`, `name`, `type` FROM " . self::DB_NAME_WARD . " WHERE 1");

            return $query->rows;
        } catch (Exception $e) {
        }*/

        // try again from file
        if (empty($this->cached_wards_by_code)) {
            $this->getWardsFromFile();
        }

        // then get from cache
        if (!empty($this->cached_wards_by_code)) {
            // TODO: also cache by district code...
            $wards = [];
            foreach ($this->cached_wards_by_code as $ward) {
                if (!is_array($ward) ||
                    !array_key_exists('name', $ward)
                ) {
                    continue;
                }

                $wards[$ward['name']] = [
                    'code' => $ward['code'],
                    'name' => $ward['name'],
                    'name_with_type' => $ward['name_with_type']
                ];
            }

            // sort alphabet asc
            uksort($wards, function ($k1, $k2) {
                $k1 = escapeVietnamese($k1);
                $k2 = escapeVietnamese($k2);

                if ($k1 == $k2) {
                    return 0;
                }

                return ($k1 < $k2) ? -1 : 1;
            });

            return $wards;
        }

        return [];
    }

    /**
     * @param $districtCode
     * @return array
     */
    public function getWardsByDistrictCode($districtCode)
    {
        // try from db
        /*try {
            $query = $this->db->query("SELECT `xaid` as `code`, `name`, `type` FROM " . self::DB_NAME_WARD . " WHERE `maqh` = '" . $districtCode . "'");

            return $query->rows;
        } catch (Exception $e) {
        }*/

        // try again from file
        if (empty($this->cached_wards_by_code)) {
            $this->getWardsFromFile();
        }

        // then get from cache
        if (!empty($this->cached_wards_by_code)) {
            // TODO: also cache by district code...
            $wards = [];
            foreach ($this->cached_wards_by_code as $ward) {
                if (!is_array($ward) ||
                    !array_key_exists('name', $ward) ||
                    !array_key_exists('parent_code', $ward) ||
                    $ward['parent_code'] !== $districtCode
                ) {
                    continue;
                }

                $wards[$ward['name']] = [
                    'code' => $ward['code'],
                    'name' => $ward['name'],
                    'name_with_type' => $ward['name_with_type']
                ];
            }

            // sort alphabet asc
            uksort($wards, function ($k1, $k2) {
                $k1 = escapeVietnamese($k1);
                $k2 = escapeVietnamese($k2);

                if ($k1 == $k2) {
                    return 0;
                }

                return ($k1 < $k2) ? -1 : 1;
            });

            return $wards;
        }

        return [];
    }

    /**
     * @param $code
     * @return mixed
     */
    public function getWardByCode($code)
    {
        // try from db
        /*try {
            $query = $this->db->query("SELECT `xaid` as `code`, `name`, `type` FROM " . self::DB_NAME_WARD . " WHERE `xaid` = '" . $code . "'");

            return $query->row;
        } catch (Exception $e) {
        }*/

        // try again from file
        if (empty($this->cached_wards_by_code) && !array_key_exists($code, $this->cached_wards_by_code)) {
            $this->getWardsFromFile();
        }

        // then get from cache
        if (!empty($this->cached_wards_by_code) && array_key_exists($code, $this->cached_wards_by_code)) {
            $ward = $this->cached_wards_by_code[$code];
            if (!is_array($ward) ||
                !array_key_exists('code', $ward) ||
                !array_key_exists('name', $ward)
            ) {
                return null;
            }

            return [
                'code' => $ward['code'],
                'name' => $ward['name'],
                'name_with_type' => $ward['name_with_type']
            ];
        }

        return null;
    }

    private function getProvincesFromFile()
    {
        try {
            $provincesRaw = file_get_contents(self::JSON_FILE_PROVINCE);
            $result = json_decode($provincesRaw, true);
            if (json_last_error() === JSON_ERROR_NONE) {
                // update cached by code and name
                foreach ($result as $province) {
                    if (!is_array($province) || !array_key_exists('code', $province) || !array_key_exists('name', $province)) {
                        continue;
                    }

                    $this->cached_provinces_by_code[$province['code']] = [
                        'code' => $province['code'],
                        'name' => $province['name'],
                        'name_with_type' => $province['name_with_type']
                    ];

                    $this->cached_provinces_by_name[$province['name']] = [
                        'code' => $province['code'],
                        'name' => $province['name'],
                        'name_with_type' => $province['name_with_type']
                    ];
                }

                return $result;
            }
        } catch (Exception $e) {
        }

        return [];
    }

    private function getDistrictsFromFile()
    {
        try {
            $districtsRaw = file_get_contents(self::JSON_FILE_DISTRICT);
            $result = json_decode($districtsRaw, true);
            if (json_last_error() === JSON_ERROR_NONE) {
                // update cached by code
                foreach ($result as $district) {
                    if (!is_array($district)) {
                        continue;
                    }

                    if (array_key_exists('code', $district)) {
                        $this->cached_districts_by_code[$district['code']] = $district;
                    }
                }

                return $result;
            }
        } catch (Exception $e) {
        }

        return [];
    }

    private function getWardsFromFile()
    {
        try {
            $wardsRaw = file_get_contents(self::JSON_FILE_WARD);
            $result = json_decode($wardsRaw, true);
            if (json_last_error() === JSON_ERROR_NONE) {
                // update cached by code
                foreach ($result as $ward) {
                    if (!is_array($ward)) {
                        continue;
                    }

                    if (array_key_exists('code', $ward)) {
                        $this->cached_wards_by_code[$ward['code']] = $ward;
                    }
                }

                return $result;
            }
        } catch (Exception $e) {
        }

        return [];
    }
}