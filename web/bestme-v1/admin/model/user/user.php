<?php

class ModelUserUser extends Model
{
    public function addUser($data)
    {
        if (!isset($data['type'])) {
            $data['type'] = 'Staff';
        }
        if (!isset($data['type'])) {
            $data['info'] = '';
        }
        if (!isset($data['telephone'])) {
            $data['telephone'] = '';
        }
        if (!isset($data['permission'])) {
            $data['permission'] = [];
        }
        $this->db->query("INSERT INTO `" . DB_PREFIX . "user` SET username = '" . $this->db->escape($data['username']) . "', user_group_id = '" . (int)$data['user_group_id'] . "', salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "' ,telephone = '" . $this->db->escape($data['telephone']) . "', image = '" . $this->db->escape($data['image']) . "', status = '" . (int)$data['status'] . "', info = '" . $this->db->escape($data['info']) . "', type = '" . $this->db->escape($data['type']) . "', permission = '" . $this->db->escape(json_encode($data['permission'])) . "', date_added = NOW()");

        return $this->db->getLastId();
    }

    public function editUser($user_id, $data)
    {
        if (!isset($data['type'])) {
            $data['type'] = 'Staff';
        }
        if (!isset($data['type'])) {
            $data['info'] = '';
        }
        if (!isset($data['telephone'])) {
            $data['telephone'] = '';
        }
        if (!isset($data['permission'])) {
            $data['permission'] = [];
        }
        $this->db->query("UPDATE `" . DB_PREFIX . "user` SET username = '" . $this->db->escape($data['username']) . "', user_group_id = '" . (int)$data['user_group_id'] . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', image = '" . $this->db->escape($data['image']) . "', status = '" . (int)$data['status'] . "', telephone = '" . $this->db->escape($data['telephone']) . "', info = '" . $this->db->escape($data['info']) . "', type = '" . $this->db->escape($data['type']) . "', permission = '" . $this->db->escape(json_encode($data['permission'])) . "' WHERE user_id = '" . (int)$user_id . "'");

        if (isset($data['password']) && $data['password']) {
            $this->db->query("UPDATE `" . DB_PREFIX . "user` SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "' WHERE user_id = '" . (int)$user_id . "'");
        }
    }

    public function editPassword($user_id, $password)
    {
        $this->db->query("UPDATE `" . DB_PREFIX . "user` SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "', code = '' WHERE user_id = '" . (int)$user_id . "'");
    }

    public function editPasswordActivationSuccess($user_id)
    {
        $this->db->query("UPDATE `" . DB_PREFIX . "user` SET salt = salt_temp, password = password_temp, password_temp = '', salt_temp = '', code = '' WHERE user_id = '" . (int)$user_id . "'");
    }

    public function editPasswordTemp($user_id, $password_temp)
    {
        $this->db->query("UPDATE `" . DB_PREFIX . "user` SET salt_temp = '" . $this->db->escape($salt = token(9)) . "', password_temp = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password_temp)))) . "' WHERE user_id = '" . (int)$user_id . "'");
    }

    public function editCode($email, $code, $shop)
    {
        // notice: passing $shop to this to let listener mail/forgotten be able to get $shop for email
        $this->db->query("UPDATE `" . DB_PREFIX . "user` SET code = '" . $this->db->escape($code) . "' WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
    }

    public function editLastLoggedIn($user_id, $last_logged_in = null)
    {
        if (!$last_logged_in instanceof DateTime) {
            $timezone = ini_get('date.timezone') ? ini_get('date.timezone') : date_default_timezone_get();
            $last_logged_in = new DateTime('now', new DateTimeZone($timezone));
        }

        $this->db->query("UPDATE `" . DB_PREFIX . "user` SET last_logged_in = '" . $last_logged_in->format('Y-m-d H:i:s') . "' WHERE user_id = '" . (int)$user_id . "'");
    }

    public function deleteUser($user_id)
    {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "user` WHERE user_id = '" . (int)$user_id . "'");
        $this->db->query("DELETE FROM `" . DB_PREFIX . "user_store` WHERE user_id = '" . (int)$user_id . "'");
    }

    public function getUser($user_id)
    {
        $query = $this->db->query("SELECT *, (SELECT ug.name FROM `" . DB_PREFIX . "user_group` ug WHERE ug.user_group_id = u.user_group_id) AS user_group FROM `" . DB_PREFIX . "user` u WHERE u.user_id = '" . (int)$user_id . "'");

        return $query->row;
    }

    /**
     * get a random user id
     * @return int|mixed
     */
    public function getRandomUserId()
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT `user_id` FROM `{$DB_PREFIX}user` ORDER BY RAND() LIMIT 1";
        $query = $this->db->query($sql);

        return isset($query->row['user_id']) ? $query->row['user_id'] : 0;
    }

    public function getUserByUsername($username)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "user` WHERE username = '" . $this->db->escape($username) . "'");

        return $query->row;
    }

    public function getUserByEmail($email)
    {
        $query = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "user` WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

        return $query->row;
    }

    public function getUserByDevelop($user_id)
    {
        $query = $this->db->query("SELECT theme_develop FROM `" . DB_PREFIX . "user` WHERE user_id = '" . $user_id . "'");
        return $query->row['theme_develop'];
    }

    public function getUserByCode($code)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "user` WHERE code = '" . $this->db->escape($code) . "' AND code != ''");

        return $query->row;
    }

    public function getUsers($data = array(), $selects = [])
    {
        $select_sql = '';
        if (empty($selects)) {
            $select_sql .= '*';
        } else {
            foreach ($selects as $select) {
                $select_sql .= $select . ', ';
            }
            $select_sql = substr($select_sql, 0, -2);
        }

        $sql = "SELECT " . $select_sql . " FROM `" . DB_PREFIX . "user`";

        $sort_data = array(
            'username',
            'status',
            'date_added'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY username";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getStaffs()
    {
        $query = $this->db->query("SELECT `user_id`, `firstname`, `lastname` FROM `" . DB_PREFIX . "user` WHERE `type` = 'Staff' AND `status` = 1");

        return $query->rows;
    }

    public function getTotalStaff()
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "user` WHERE `type` = 'Staff' ");

        return $query->row['total'];
    }

    public function getTotalUsers()
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "user`");

        return $query->row['total'];
    }

    public function getTotalUsersByGroupId($user_group_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "user` WHERE user_group_id = '" . (int)$user_group_id . "'");

        return $query->row['total'];
    }

    public function getTotalUsersByEmail($email)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "user` WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

        return $query->row['total'];
    }

    public function checkTokenSession($user_token)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "session` WHERE `data` LIKE '%\"user_token\":\"" . $this->db->escape($user_token) . "\"%' AND expire > " . (int)time());

        if ($query->num_rows) {
            $user_id = false;
            foreach ($query->rows as $row) {
                if (!isset($row['data'])) {
                    continue;
                }

                $data = json_decode($row['data']);
                if (isset($data->user_id)) {
                    return $data->user_id;
                }
            }
        } else {
            return false;
        }
    }

    public function getEmailByUser($user_id)
    {
        $query = $this->db->query("SELECT email FROM `" . DB_PREFIX . "user` WHERE user_id = '" . $user_id . "'");
        return $query->row['email'];
    }

    public function getAuthorOfBlogs()
    {
        $query = $this->db->query("SELECT `user_id`, `firstname`, `lastname` FROM `" . DB_PREFIX . "user`");
        return $query->rows;
    }

    public function addUserStore($data)
    {
        if (!isset($data['store_id'])) {
            $data['store_id'] = '';
        }
        if (!isset($data['user_id'])) {
            $data['user_id'] = '';
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "user_store` SET user_id = '" . $this->db->escape($data['user_id']) . "', store_id = '" . (int)$data['store_id'] . "' ");

        return $this->db->getLastId();
    }

    public function editUserStore($user_id, $data)
    {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "user_store` WHERE user_id = '" . (int)$user_id . "'");

        foreach ($data as $store) {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "user_store` SET user_id = '" . $this->db->escape($user_id) . "', store_id = '" . (int)$store . "' ");
        }

        return 1;
    }

    public function getUserStore($user_id)
    {
        $query = "SELECT us.store_id as id , s.name as title FROM " . DB_PREFIX . "user_store as us INNER JOIN " . DB_PREFIX . "store as s ON us.store_id = s.store_id WHERE us.user_id=" . (int)$user_id . " ORDER BY us.id ASC";
        $results = $this->db->query($query);
        return $results->rows;
    }

    public function getUserStoreForList($user_id)
    {
        $query = "SELECT us.store_id, s.name FROM " . DB_PREFIX . "user_store as us 
                    INNER JOIN " . DB_PREFIX . "store as s ON us.store_id = s.store_id 
                    WHERE us.user_id=" . (int)$user_id . " ORDER BY us.id ASC";

        $results = $this->db->query($query);
        return $results->rows;
    }

    public function getTypeUser($user_id)
    {
        $type = "Admin";
        $query = $this->db->query("SELECT `type` FROM `" . DB_PREFIX . "user` WHERE user_id = '" . $user_id . "'");

        if (isset($query->row['type'])) {
            $type = $query->row['type'];
        }

        return $type;
    }

    public function getStoreIdsOffUser($user_id)
    {
        $store_ids = [];
        $query = "SELECT us.store_id 
                    FROM " . DB_PREFIX . "user_store as us 
                      INNER JOIN " . DB_PREFIX . "store as s ON us.store_id = s.store_id 
                        WHERE us.user_id=" . (int)$user_id . " ORDER BY us.id ASC";

        $results = $this->db->query($query);

        foreach ($results->rows as $item) {
            $store_ids[] = $item['store_id'];
        }

        return $store_ids;
    }

    public function getFullNameByUserId($user_id)
    {
        $sql = "SELECT CONCAT(lastname, ' ', firstname) as fullname FROM `" . DB_PREFIX . "user` WHERE `user_id` = " . (int)$user_id;

        $query = $this->db->query($sql);

        return isset($query->row['fullname']) ? $query->row['fullname'] : '';
    }
}