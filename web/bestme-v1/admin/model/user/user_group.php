<?php
class ModelUserUserGroup extends Model {
	public function addUserGroup($data) {
        if(!isset($data['access_all'])) {
            $data['access_all'] = 1;
        }

		$this->db->query("INSERT INTO " . DB_PREFIX . "user_group 
                        SET 
		                    name = '" . $this->db->escape($data['name']) . "',
                            access_all = '" . (int)$data['access_all'] . "',
		                    permission = '" . (isset($data['permission']) ? $this->db->escape(json_encode($data['permission'])) : '') . "'");
	
		return $this->db->getLastId();
	}

	public function editUserGroup($user_group_id, $data) {
	    if(!isset($data['access_all'])) {
            $data['access_all'] = 1;
        }

		$this->db->query("UPDATE " . DB_PREFIX . "user_group SET 
                            name = '" . $this->db->escape($data['name']) . "', 
                            access_all = '" . (int)$data['access_all'] . "',
                            permission = '" . (isset($data['permission']) ? $this->db->escape(json_encode($data['permission'])) : '') . "' 
                            WHERE user_group_id = '" . (int)$user_group_id . "'");
	}

	public function deleteUserGroup($user_group_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_group_id . "'");
	}

	public function getUserGroup($user_group_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_group_id . "'");

		$user_group = array(
			'name'       => isset($query->row['name']) ? $query->row['name'] : '',
			'permission' => json_decode(isset($query->row['permission']) ? $query->row['permission'] : '', true),
			'access_all' =>isset($query->row['access_all']) ? $query->row['access_all'] : 0
		);

		return $user_group;
	}

    public function getUserGroupByName($group_name, $except_id = null) {
	    $sql = "SELECT * FROM " . DB_PREFIX . "user_group WHERE name = '" . $this->db->escape($group_name) . "'";
	    if($except_id) {
            $sql .= " AND user_group_id != ".(int)$except_id;
        }
        return $this->db->query($sql)->row;
    }

	public function getUserGroups($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "user_group";

		$sql .= " ORDER BY name";

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getUserGroupLazyLoad($data = array()) {
        $sql = "SELECT usg.user_group_id as id, usg.name as text FROM " . DB_PREFIX . "user_group as usg WHERE usg.name != '' AND usg.user_group_id NOT IN (1, 10) ";

        if (!empty($data['filter_name'])) {
            $sql .= " AND usg.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " ORDER BY usg.name ASC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getCountUserGroupLazyLoad($data)
    {
        $sql = "SELECT usg.user_group_id as id, usg.name as text FROM " . DB_PREFIX . "user_group as usg WHERE usg.name != '' AND usg.user_group_id NOT IN (1, 10) ";
        if (!empty($data['filter_name'])) {
            $sql .= " AND usg.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }
        $query = $this->db->query($sql);

        return count($query->rows);
    }

    public function getCountUserInGroup($user_group_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total 
            FROM " . DB_PREFIX . "user
            WHERE user_group_id = ".(int)$user_group_id);
        return $query->row['total'];
    }

	public function getTotalUserGroups() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "user_group");

		return $query->row['total'];
	}

	public function addPermission($user_group_id, $type, $route) {
		$user_group_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_group_id . "'");

		if ($user_group_query->num_rows) {
			$data = json_decode($user_group_query->row['permission'], true);

			$data[$type][] = $route;

			$this->db->query("UPDATE " . DB_PREFIX . "user_group SET permission = '" . $this->db->escape(json_encode($data)) . "' WHERE user_group_id = '" . (int)$user_group_id . "'");
		}
	}

	public function removePermission($user_group_id, $type, $route) {
		$user_group_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_group_id . "'");

		if ($user_group_query->num_rows) {
			$data = json_decode($user_group_query->row['permission'], true);

			$data[$type] = array_diff($data[$type], array($route));

			$this->db->query("UPDATE " . DB_PREFIX . "user_group SET permission = '" . $this->db->escape(json_encode($data)) . "' WHERE user_group_id = '" . (int)$user_group_id . "'");
		}
	}

    public function getUserGroupIdByUserId($user_id)
    {
        $query = $this->db->query("SELECT `user_group_id` FROM `" . DB_PREFIX . "user` WHERE `user_id` = " . (int)$user_id);

        if (isset($query->row['user_group_id'])){
            return (int)$query->row['user_group_id'];
        }

        return 1;
    }

    public function getDetailUserGroup($user_group_id)
    {
        $query = $this->db->query("SELECT `user_group_id`, `name` FROM `" . DB_PREFIX . "user_group` WHERE `user_group_id` = " . (int)$user_group_id);
        if (isset($query->row['user_group_id'])){
            $user_group = array(
                'id'       => $query->row['user_group_id'],
                'title'    => $query->row['name'],
            );

            return $user_group;
        }
        return [];
    }

    public function getUserGroupWithCountStaff() {
        $sql = "SELECT ug.name as name , u.total, ug.user_group_id as id
                    FROM " . DB_PREFIX . "user_group as ug 
                LEFT JOIN 
                (SELECT COUNT(1) as total, user_group_id 
                FROM " . DB_PREFIX . "user 
                GROUP BY user_group_id)  as u ON ug.user_group_id = u.user_group_id 
                WHERE ug.name != ''
                    AND ug.user_group_id not in (1, 10)
                GROUP BY ug.user_group_id ORDER BY ug.user_group_id DESC
                ";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getAccessAll($user_group_id)
    {
	    $sql = "SELECT `access_all` FROM `" . DB_PREFIX . "user_group` WHERE `user_group_id` = " . (int)$user_group_id;
        $query = $this->db->query($sql);

        if (isset($query->row['access_all'])){
            return (int)$query->row['access_all'];
        }

        return 1;
    }
}