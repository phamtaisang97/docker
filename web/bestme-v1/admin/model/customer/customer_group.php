<?php
class ModelCustomerCustomerGroup extends Model {
	public function addCustomerGroup($data) {
	    $user_id = $this->user->getId();
		$this->db->query("INSERT INTO " . DB_PREFIX . "customer_group SET approval = '" . 1 . "', user_create_id = '" . $user_id . "' ,sort_order = '" . 1 . "'");
		$customer_group_id = $this->db->getLastId();
        // update  id. e.g id = 123 => code(length=9) = NKH000123
        $prefix = 'NKH';
        $code = $prefix . str_pad($customer_group_id, 6, "0", STR_PAD_LEFT);

        $this->db->query("UPDATE " . DB_PREFIX . "customer_group 
                          SET `customer_group_code` = '" . $code . "' 
                          WHERE customer_group_id = '" . (int)$customer_group_id . "'");

        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        foreach ($languages as $language) {
            $language_id = $language['language_id'];
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_group_description SET customer_group_id = '" . (int)$customer_group_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($data['name_group']) . "', description = '" . $this->db->escape($data['description']) . "'");
		}
		
		return $customer_group_id;
	}

	public function editCustomerGroup($customer_group_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer_group 
		                    SET approval = 1, 
		                        sort_order = 1 
		                        WHERE customer_group_id = '" . (int)$customer_group_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_group_description 
		                    WHERE customer_group_id = '" . (int)$customer_group_id . "'");

        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        foreach ($languages as $language) {
            $language_id = $language['language_id'];
            $this->db->query("INSERT INTO " . DB_PREFIX . "customer_group_description 
                                SET customer_group_id = '" . (int)$customer_group_id . "', 
                                language_id = '" . (int)$language_id . "', 
                                `name` = '" . $this->db->escape($data['name_group']) . "', 
                                description = '" . $this->db->escape($data['description']) . "'");
        }
	}

    public function validateNameExist($group_id, $name_group)
    {
        $sql = "SELECT COUNT(DISTINCT customer_group_id) as total FROM `" . DB_PREFIX . "customer_group_description` WHERE `name` = '" . $this->db->escape($name_group) . "'";
        if (!empty($group_id)) {
            $sql .= " AND `customer_group_id` != " . (int)$group_id;
        }

        $query = $this->db->query($sql);
        if ($query->row['total'] != 0) {
            return true;
        }
        return false;
    }

	public function deleteCustomerGroup($customer_group_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_group WHERE customer_group_id = '" . (int)$customer_group_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_group_description WHERE customer_group_id = '" . (int)$customer_group_id . "'");
//		$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE customer_group_id = '" . (int)$customer_group_id . "'");
//		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE customer_group_id = '" . (int)$customer_group_id . "'");
//		$this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE customer_group_id = '" . (int)$customer_group_id . "'");
//		$this->db->query("DELETE FROM " . DB_PREFIX . "tax_rate_to_customer_group WHERE customer_group_id = '" . (int)$customer_group_id . "'");
	}

    public function getCountCustomerGroup($customer_group_id)
    {
        $sql = "SELECT COUNT(customer_group_id) AS total FROM " . DB_PREFIX . "customer WHERE customer_group_id = $customer_group_id";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

	public function getCustomerGroup($customer_group_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer_group cg LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (cg.customer_group_id = cgd.customer_group_id) WHERE cg.customer_group_id = '" . (int)$customer_group_id . "' AND cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}

	public function getCustomerGroups($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "customer_group cg 
		        LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (cg.customer_group_id = cgd.customer_group_id) 
		        WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        $this->load->model('user/user');
        if(isset($data['user_create_id']) && $data['user_create_id'] !== '') {
            $sql .= ' AND cg.user_create_id=' . $data['user_create_id'];
        }

		$sort_data = array(
			'cgd.name',
			'cg.sort_order'
		);

        if (!empty($data['filter_name'])) {
            $key_search = trim($this->db->escape($data['filter_name']));
            $sql .= " AND ( cgd.name LIKE '%" . $key_search . "%' 
             OR cg.customer_group_code LIKE '%" . $key_search . "%')
             ";
        }

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY cgd.name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getCustomerGroupDescriptions($customer_group_id) {
		$customer_group_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_group_description WHERE customer_group_id = '" . (int)$customer_group_id . "'");

		foreach ($query->rows as $result) {
			$customer_group_data[$result['language_id']] = array(
				'name'        => $result['name'],
				'description' => $result['description']
			);
		}

		return $customer_group_data;
	}

	public function getTotalCustomerGroups($data = array()) {
        $sql = "SELECT COUNT(DISTINCT cg.customer_group_id) AS total FROM " . DB_PREFIX . "customer_group cg 
		        LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (cg.customer_group_id = cgd.customer_group_id) 
		        WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if(isset($data['user_create_id']) && $data['user_create_id'] !== '') {
            $sql .= ' AND cg.user_create_id=' . $data['user_create_id'];
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND cgd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'
                OR cg.customer_group_code LIKE '%" . $this->db->escape($data['filter_name']) . "%'
            ";
        }

        $query = $this->db->query($sql);

		return $query->row['total'];
	}

    public function getCustomerGroupNameById($group_id)
    {
        $query = $this->db->query("SELECT `name` FROM `" . DB_PREFIX . "customer_group_description` WHERE `customer_group_id` = " . (int)$group_id . " AND `language_id` = " . (int)$this->config->get('config_language_id'));

        return isset($query->row['name']) ? $query->row['name'] : "";
    }

    public function getCustomerGroupByName($group_name)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_group_description` WHERE `name` = '" . $this->db->escape($group_name) . "' AND `language_id` = " . (int)$this->config->get('config_language_id'));

        return $query->row;
    }
}
