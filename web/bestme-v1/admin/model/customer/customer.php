<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ModelCustomerCustomer extends Model
{
    public function addCustomer($data)
    {
        if (isset($data['full_name'])) {
            $name = extract_name($data['full_name']);
            $data['firstname'] = $name[0];
            $data['lastname'] = $name[1];
        }

        $data['customer_source'] = 'admin';

        $status = isset($data['status']) ? (int)$data['status'] : 1;
        $user_id = $this->user->getId();
        // set random password for customer
        $sql = "INSERT INTO " . DB_PREFIX . "customer 
                            SET firstname = '" . $this->db->escape($data['firstname']) . "', 
                                lastname = '" . $this->db->escape($data['lastname']) . "', 
                                full_name = '" . $this->db->escape($data['full_name']) . "', 
                                telephone = '" . $this->db->escape($data['telephone']) . "', 
                                email = '" . $this->db->escape($data['email']) . "', 
                                customer_source = '" . $this->db->escape($data['customer_source']) . "', 
                                note = '" . $this->db->escape($data['note']) . "', 
                                salt = '" . $this->db->escape($salt = token(9)) . "', 
                                password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1(token(9))))) . "', 
                                sex = '" . $this->db->escape($data['sex']) . "', 
                                website = '" . $this->db->escape($data['website']) . "', 
                                tax_code = '" . $this->db->escape($data['tax_code']) . "', 
                                staff_in_charge = '" . $this->db->escape($data['staff_in_charge']) . "',
                                user_create_id = '" . $user_id. "',  
                                status = '" . $status . "',  
                                date_added = NOW()";
        if (isset($data['birthday']) && $data['birthday']) {
            $sql .= ", birthday = '" . $this->db->escape($data['birthday']) . "'";
        }

        $this->db->query($sql);

        $customer_id = $this->db->getLastId();
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();

        // update customer code
        $prefix = 'KH';
        $code = $prefix . str_pad($customer_id, 6, "0", STR_PAD_LEFT);

        $this->db->query("UPDATE " . DB_PREFIX . "customer 
                          SET `code` = '" . $code . "' 
                          WHERE customer_id = '" . (int)$customer_id . "'");

        //set group_id
        if (isset($data['customer_group_id']) && $data['customer_group_id'] != null) {
            if (strpos($data['customer_group_id'], 'group_') !== false) {
                $customer_group = explode("_", $data['customer_group_id']);
                $data['customer_group_name'] = $customer_group[1];

                $this->db->query("INSERT INTO " . DB_PREFIX . "customer_group SET approval = 1, sort_order = 1");
                $customer_group_id = $this->db->getLastId();

                $prefix = 'NKH';
                $code = $prefix . str_pad($customer_group_id, 6, "0", STR_PAD_LEFT);

                $this->db->query("UPDATE " . DB_PREFIX . "customer_group 
                          SET `customer_group_code` = '" . $code . "' 
                          WHERE customer_group_id = '" . (int)$customer_group_id . "'");

                $this->db->query("UPDATE " . DB_PREFIX . "customer 
                                    SET customer_group_id = '" . (int)$customer_group_id . "' 
                                    WHERE customer_id = '" . (int)$customer_id . "'");

                foreach ($languages as $language) {
                    $language_id = $language['language_id'];
                    $this->db->query("INSERT INTO " . DB_PREFIX . "customer_group_description 
                                        SET customer_group_id = '" . (int)$customer_group_id . "', 
                                            language_id = '" . (int)$language_id . "', 
                                            `name` = '" . $this->db->escape($data['customer_group_name']) . "', 
                                            `description` = ''");
                }
            } else {
                $this->db->query("UPDATE " . DB_PREFIX . "customer 
                                    SET customer_group_id = '" . (int)$data['customer_group_id'] . "' 
                                    WHERE customer_id = '" . (int)$customer_id . "'");
            }
        }

        if (isset($data['city'])) {
            $district = isset($data['district']) ? $this->db->escape($data['district']) : "";
            $wards = isset($data['wards']) ? $this->db->escape($data['wards']) : "";

            $this->db->query("INSERT INTO " . DB_PREFIX . "address 
                            SET customer_id = '" . (int)$customer_id . "', 
                                firstname = '" . $this->db->escape($data['firstname']) . "', 
                                lastname = '" . $this->db->escape($data['lastname']) . "', 
                                address = '" . $this->db->escape($data['address']) . "', 
                                city = '" . $this->db->escape($data['city']) . "', 
                                district = '" . $district . "', 
                                wards = '" . $wards . "', 
                                phone = '" . $this->db->escape($data['telephone']) . "'");


            $address_id = $this->db->getLastId();

            $this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
        }

        return $customer_id;
    }

    public function editCustomer($customer_id, $data)
    {
        if (isset($data['full_name'])) {
            $name = extract_name($data['full_name']);
            $data['firstname'] = $name[0];
            $data['lastname'] = $name[1];
        }

        $sql = "UPDATE " . DB_PREFIX . "customer 
                            SET firstname = '" . $this->db->escape($data['firstname']) . "', 
                                lastname = '" . $this->db->escape($data['lastname']) . "', 
                                full_name = '" . $this->db->escape($data['full_name']) . "', 
                                telephone = '" . $this->db->escape($data['telephone']) . "'";

        if (isset($data['sex']) && $data['sex']) {
            $sql .= ", sex = '" . $this->db->escape($data['sex']) . "'";
        }

        if (isset($data['birthday']) && $data['birthday']) {
            $sql .= ", birthday = '" . $this->db->escape($data['birthday']) . "'";
        }

        if (isset($data['note']) && $data['note']) {
            $sql .= ", note = '" . $this->db->escape($data['note']) . "'";
        }

        if (isset($data['email']) && $data['email']) {
            $sql .= ", email = '" . $this->db->escape($data['email']) . "'";
        }

        if (isset($data['website']) && $data['website']) {
            $sql .= ", website = '" . $this->db->escape($data['website']) . "'";
        }

        if (isset($data['tax_code']) && $data['tax_code']) {
            $sql .= ", tax_code = '" . $this->db->escape($data['tax_code']) . "'";
        }

        if (isset($data['staff_in_charge']) && $data['staff_in_charge']) {
            $sql .= ", staff_in_charge = '" . $this->db->escape($data['staff_in_charge']) . "'";
        }

        $sql .= " WHERE customer_id = " . (int)$customer_id;

        $this->db->query($sql);

        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();

        //set group_id
        if (isset($data['customer_group_id']) && $data['customer_group_id'] != '') {
            if (strpos($data['customer_group_id'], 'group_') !== false) {
                $customer_group = explode("_", $data['customer_group_id']);
                $data['customer_group_name'] = $customer_group[1];

                $this->db->query("INSERT INTO " . DB_PREFIX . "customer_group SET approval = 1, sort_order = 1");
                $customer_group_id = $this->db->getLastId();

                $prefix = 'NKH';
                $code = $prefix . str_pad($customer_group_id, 6, "0", STR_PAD_LEFT);

                $this->db->query("UPDATE " . DB_PREFIX . "customer_group 
                          SET `customer_group_code` = '" . $code . "' 
                          WHERE customer_group_id = '" . (int)$customer_group_id . "'");

                $this->db->query("UPDATE " . DB_PREFIX . "customer 
                                    SET customer_group_id = '" . (int)$customer_group_id . "' 
                                    WHERE customer_id = '" . (int)$customer_id . "'");

                foreach ($languages as $language) {
                    $language_id = $language['language_id'];
                    $this->db->query("INSERT INTO " . DB_PREFIX . "customer_group_description 
                                        SET customer_group_id = '" . (int)$customer_group_id . "', 
                                            language_id = '" . (int)$language_id . "', 
                                            `name` = '" . $this->db->escape($data['customer_group_name']) . "', 
                                            `description` = ''");
                }
            } else {
                $this->db->query("UPDATE " . DB_PREFIX . "customer 
                                    SET customer_group_id = '" . (int)$data['customer_group_id'] . "' 
                                    WHERE customer_id = '" . (int)$customer_id . "'");
            }
        } else {
            $this->db->query("UPDATE " . DB_PREFIX . "customer 
                                    SET customer_group_id = '" . (int)$data['customer_group_id'] . "' 
                                    WHERE customer_id = '" . (int)$customer_id . "'");
        }

        if (isset($data['address_id']) && $data['address_id'] != '') {
            $this->db->query("DELETE FROM " . DB_PREFIX . "address 
                                WHERE customer_id = '" . (int)$customer_id . "' 
                                AND address_id = " .(int)$data['address_id']);

            if (isset($data['full_name_for_default_address']) && $data['full_name_for_default_address'] != '') {
                $name = extract_name($data['full_name_for_default_address']);
                $data['firstname_address_default'] = $name[0];
                $data['lastname_address_default'] = $name[1];
            } else {
                $data['firstname_address_default'] = $data['firstname'];
                $data['lastname_address_default'] = $data['lastname'];
            }

            if (!isset($data['telephone_for_default_address'])){
                $data['telephone_for_default_address'] = $data['telephone'];
            }

            $district = isset($data['district']) ? $this->db->escape($data['district']) : "";
            $wards = isset($data['wards']) ? $this->db->escape($data['wards']) : "";

            $this->db->query("INSERT INTO " . DB_PREFIX . "address 
                                SET customer_id = '" . (int)$customer_id . "', 
                                    firstname = '" . $this->db->escape($data['firstname_address_default']) . "', 
                                    lastname = '" . $this->db->escape($data['lastname_address_default']) . "', 
                                    address = '" . $this->db->escape($data['address']) . "', 
                                    city = '" . $this->db->escape($data['city']) . "', 
                                    district = '" . $district . "', 
                                    wards = '" . $wards . "', 
                                    phone = '" . $this->db->escape($data['telephone_for_default_address']) . "'");

            $address_id = $this->db->getLastId();

            $this->db->query("UPDATE " . DB_PREFIX . "customer 
                                SET address_id = '" . (int)$address_id . "' 
                                WHERE customer_id = '" . (int)$customer_id . "'");
        }

        return $customer_id;
    }

    public function editToken($customer_id, $token)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "customer SET token = '" . $this->db->escape($token) . "' WHERE customer_id = '" . (int)$customer_id . "'");
    }

    public function deleteCustomer($customer_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");
        //$this->db->query("DELETE FROM " . DB_PREFIX . "customer_activity WHERE customer_id = '" . (int)$customer_id . "'");
        //$this->db->query("DELETE FROM " . DB_PREFIX . "customer_affiliate WHERE customer_id = '" . (int)$customer_id . "'");
        //$this->db->query("DELETE FROM " . DB_PREFIX . "customer_approval WHERE customer_id = '" . (int)$customer_id . "'");
        //$this->db->query("DELETE FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "'");
        //$this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$customer_id . "'");
        //$this->db->query("DELETE FROM " . DB_PREFIX . "customer_ip WHERE customer_id = '" . (int)$customer_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");
    }

    public function getCustomer($customer_id)
    {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");

        return $query->row;
    }

    /**
     * get a random customer id
     * @return int|mixed
     */
    public function getRandomCustomerId()
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT `customer_id` FROM `{$DB_PREFIX}customer` ORDER BY RAND() LIMIT 1";
        $query = $this->db->query($sql);

        return isset($query->row['customer_id']) ? $query->row['customer_id'] : 0;
    }

    public function getAddressDefaultCustomer($address_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address 
                                    WHERE address_id = '" . (int)$address_id . "'");

        return $query->row;
    }

    public function getCustomerByEmail($email)
    {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

        return $query->row;
    }

    public function getCustomerByTelephone($telephone)
    {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer WHERE LCASE(telephone) = '" . $this->db->escape(utf8_strtolower($telephone)) . "'");

        return $query->row;
    }

    public function getCustomers($data = array())
    {
        $language_id = (int)$this->config->get('config_language_id');
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT c.*, cgd.name as group_name, 
                       CONCAT(c.lastname, ' ', c.firstname) AS name, 
                       IFNULL(COUNT(od.order_id), 0) AS count_order, 
                       IFNULL(SUM(od.total), 0) AS order_total, 
                       MAX(od.date_added) as order_lastest 
                FROM {$DB_PREFIX}customer c 
                LEFT JOIN {$DB_PREFIX}customer_group_description cgd 
                        ON (c.customer_group_id = cgd.customer_group_id AND cgd.language_id = " . $language_id . ") 
                LEFT JOIN {$DB_PREFIX}order od 
                       ON (c.customer_id = od.customer_id AND od.order_status_id NOT IN (6,10))";

        $sql .= " WHERE 1 ";

        $implode = array();

        $filter = $data;
        if(isset($data['user_create_id'])) {
            $sql .= " AND c.user_create_id = ". $data['user_create_id'];
        }

        if (!empty($data['filter_name'])) {
            $implode[] = "(c.code LIKE '%" . $this->db->escape($data['filter_name']) . "%' OR CONCAT(c.lastname, ' ', c.firstname) LIKE '%" . $this->db->escape($data['filter_name']) . "%')";
        }

        if (isset($data['filter_customer_group_id']) && $data['filter_customer_group_id'] !== '') {
            $implode[] = "c.customer_group_id IN (" . $data['filter_customer_group_id'] . ")";
        }

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $implode[] = "c.status IN (" . $data['filter_status'] . ")";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $sql = $sql . " group by c.customer_id";

        $sql .= " ORDER BY c.date_added DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getCustomersForOrder($data = array())  // filter_name  by fullname or telephone
    {
        $sql = "SELECT c.*, CONCAT(c.lastname, ' ', c.firstname) AS name FROM " . DB_PREFIX . "customer c";
        //echo $sql; die();
        if (!empty($data['filter_affiliate'])) {
            $sql .= " LEFT JOIN " . DB_PREFIX . "customer_affiliate ca ON (c.customer_id = ca.customer_id)";
        }

        $sql .= " WHERE c.status = 1 ";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "( CONCAT(c.lastname, ' ', c.firstname) LIKE '%" . $this->db->escape($data['filter_name']) . "%' OR c.telephone LIKE '%" . $this->db->escape($data['filter_name']) . "%')";
        }

        if (!empty($data['filter_email'])) {
            $implode[] = "c.email LIKE '%" . $this->db->escape($data['filter_email']) . "%'";
        }

        if (isset($data['filter_newsletter']) && !is_null($data['filter_newsletter'])) {
            $implode[] = "c.newsletter = '" . (int)$data['filter_newsletter'] . "'";
        }

        if (!empty($data['filter_customer_group_id'])) {
            $implode[] = "c.customer_group_id = '" . (int)$data['filter_customer_group_id'] . "'";
        }

        if (!empty($data['filter_affiliate'])) {
            $implode[] = "ca.status = '" . (int)$data['filter_affiliate'] . "'";
        }

        if (!empty($data['filter_ip'])) {
            $implode[] = "c.customer_id IN (SELECT customer_id FROM " . DB_PREFIX . "customer_ip WHERE ip = '" . $this->db->escape($data['filter_ip']) . "')";
        }

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $implode[] = "c.status = '" . (int)$data['filter_status'] . "'";
        }

        if(isset($data['current_user_id']) && $data['current_user_id'] !== '') {
            $sql .= " AND c.user_create_id = ". $data['current_user_id'];
        }

        if (!empty($data['filter_date_added'])) {
            $implode[] = "DATE(c.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'name',
            'c.email',
            'c.status',
            'c.ip',
            'c.date_added'
        );

        $sql = $sql . " group by c.customer_id";

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getAddress($address_id)
    {
        $address_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int)$address_id . "'");

        if ($address_query->num_rows) {
            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$address_query->row['country_id'] . "'");

            if ($country_query->num_rows) {
                $country = $country_query->row['name'];
                $iso_code_2 = $country_query->row['iso_code_2'];
                $iso_code_3 = $country_query->row['iso_code_3'];
                $address_format = $country_query->row['address_format'];
            } else {
                $country = '';
                $iso_code_2 = '';
                $iso_code_3 = '';
                $address_format = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$address_query->row['zone_id'] . "'");

            if ($zone_query->num_rows) {
                $zone = $zone_query->row['name'];
                $zone_code = $zone_query->row['code'];
            } else {
                $zone = '';
                $zone_code = '';
            }

            return array(
                'address_id' => $address_query->row['address_id'],
                'customer_id' => $address_query->row['customer_id'],
                'firstname' => $address_query->row['firstname'],
                'lastname' => $address_query->row['lastname'],
                'company' => $address_query->row['company'],
                'address' => $address_query->row['address'],
                'postcode' => $address_query->row['postcode'],
                'city' => $address_query->row['city'],
                'district' => $address_query->row['district'],
                'wards' => $address_query->row['wards'],
                'phone' => $address_query->row['phone'],
                'country_id' => $address_query->row['country_id'],
                'country' => $country,
                'address_format' => $address_format,
                'custom_field' => json_decode($address_query->row['custom_field'], true)
            );
        }
    }

    public function getAddressByCustomerId($customer_id)
    {
        $address_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");

        if ($address_query->num_rows) {
            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$address_query->row['country_id'] . "'");

            if ($country_query->num_rows) {
                $country = $country_query->row['name'];
                $iso_code_2 = $country_query->row['iso_code_2'];
                $iso_code_3 = $country_query->row['iso_code_3'];
                $address_format = $country_query->row['address_format'];
            } else {
                $country = '';
                $iso_code_2 = '';
                $iso_code_3 = '';
                $address_format = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$address_query->row['zone_id'] . "'");

            if ($zone_query->num_rows) {
                $zone = $zone_query->row['name'];
                $zone_code = $zone_query->row['code'];
            } else {
                $zone = '';
                $zone_code = '';
            }

            return array(
                'address_id' => $address_query->row['address_id'],
                'customer_id' => $address_query->row['customer_id'],
                'firstname' => $address_query->row['firstname'],
                'lastname' => $address_query->row['lastname'],
                'company' => $address_query->row['company'],
                'address' => $address_query->row['address'],
                'postcode' => $address_query->row['postcode'],
                'city' => $address_query->row['city'],
                'district' => $address_query->row['district'],
                'wards' => $address_query->row['wards'],
                'phone' => $address_query->row['phone'],
                'country_id' => $address_query->row['country_id'],
                'country' => $country,
                'address_format' => $address_format,
                'custom_field' => json_decode($address_query->row['custom_field'], true)
            );
        }
    }

    public function getAddresses($customer_id)
    {
        $address_data = array();

        $query = $this->db->query("SELECT address_id FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");

        foreach ($query->rows as $result) {
            $address_info = $this->getAddress($result['address_id']);

            if ($address_info) {
                $address_data[$result['address_id']] = $address_info;
            }
        }

        return $address_data;
    }

    public function getTotalCustomers($data = array())
    {
        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "(code LIKE '%" . $this->db->escape($data['filter_name']) . "%' OR CONCAT(lastname, ' ', firstname) LIKE '%" . $this->db->escape($data['filter_name']) . "%')";
        }

        if (isset($data['filter_customer_group_id']) && $data['filter_customer_group_id'] !== '') {
            $implode[] = "customer_group_id IN (" . $data['filter_customer_group_id'] . ")";
        }

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $implode[] = "status IN (" . $data['filter_status'] . ")";
        }

        $sql .= " WHERE 1 ";

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        if(isset($data['user_create_id'])) {
            $sql .= " AND user_create_id = ". $data['user_create_id'];
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalCustomersForOrder($data = array()) // filter_name  by fullname or telephone
    {
        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "( CONCAT(lastname, ' ', firstname) LIKE '%" . $this->db->escape($data['filter_name']) . "%' OR telephone LIKE '%" . $this->db->escape($data['filter_name']) . "%')";
        }

        if (!empty($data['filter_email'])) {
            $implode[] = "email LIKE '" . $this->db->escape($data['filter_email']) . "%'";
        }

        if (isset($data['filter_newsletter']) && !is_null($data['filter_newsletter'])) {
            $implode[] = "newsletter = '" . (int)$data['filter_newsletter'] . "'";
        }

        if (!empty($data['filter_customer_group_id'])) {
            $implode[] = "customer_group_id = '" . (int)$data['filter_customer_group_id'] . "'";
        }

        if (!empty($data['filter_ip'])) {
            $implode[] = "customer_id IN (SELECT customer_id FROM " . DB_PREFIX . "customer_ip WHERE ip = '" . $this->db->escape($data['filter_ip']) . "')";
        }

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $implode[] = "status = '" . (int)$data['filter_status'] . "'";
        }

        if (!empty($data['filter_date_added'])) {
            $implode[] = "DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        $sql .= " WHERE 1";

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        if(isset($data['current_user_id']) && $data['current_user_id'] !== '') {
            $sql .= " AND user_create_id = ". $data['current_user_id'];
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalCustomersByDate($data = array())
    {
        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer";

        $implode = array();


        if (!empty($data['filter_date_added'])) {
            $implode[] = "DATE(date_added) <= DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if ($implode) {
            $sql .= " WHERE " . implode(" AND ", $implode);
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getAffliateByTracking($tracking)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_affiliate WHERE tracking = '" . $this->db->escape($tracking) . "'");

        return $query->row;
    }

    public function getAffiliate($customer_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_affiliate WHERE customer_id = '" . (int)$customer_id . "'");

        return $query->row;
    }

    public function getAffiliates($data = array())
    {
        $sql = "SELECT DISTINCT *, CONCAT(c.lastname, ' ', c.firstname) AS name FROM " . DB_PREFIX . "customer_affiliate ca LEFT JOIN " . DB_PREFIX . "customer c ON (ca.customer_id = c.customer_id)";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "CONCAT(c.lastname, ' ', c.firstname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if ($implode) {
            $sql .= " WHERE " . implode(" AND ", $implode);
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql . "ORDER BY name");

        return $query->rows;
    }

    public function getTotalAffiliates($data = array())
    {
        $sql = "SELECT DISTINCT COUNT(*) AS total FROM " . DB_PREFIX . "customer_affiliate ca LEFT JOIN " . DB_PREFIX . "customer c ON (ca.customer_id = c.customer_id)";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "CONCAT(c.lastname, ' ', c.firstname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if ($implode) {
            $sql .= " WHERE " . implode(" AND ", $implode);
        }
        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalAddressesByCustomerId($customer_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");

        return $query->row['total'];
    }

    public function getTotalAddressesByCountryId($country_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE country_id = '" . (int)$country_id . "'");

        return $query->row['total'];
    }

    public function getTotalAddressesByZoneId($zone_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE zone_id = '" . (int)$zone_id . "'");

        return $query->row['total'];
    }

    public function getTotalCustomersByCustomerGroupId($customer_group_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE customer_group_id = '" . (int)$customer_group_id . "'");

        return $query->row['total'];
    }

    public function addHistory($customer_id, $comment)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "customer_history SET customer_id = '" . (int)$customer_id . "', comment = '" . $this->db->escape(strip_tags($comment)) . "', date_added = NOW()");
    }

    public function getHistories($data = array())
    {
//        $query = $this->db->query("SELECT comment, date_added FROM " . DB_PREFIX . "customer_history WHERE customer_id = '" . (int)$customer_id . "' ORDER BY date_added DESC LIMIT " . (int)$start . "," . (int)$limit);
        $this->load->model("sale/order");
        $order_status_canceled = ModelSaleOrder::ORDER_STATUS_ID_CANCELLED;
        $sql = "SELECT od.order_status_id, od.order_id, od.order_code, od.date_added, od.total, rr.total as return_receipt
         FROM " . DB_PREFIX . "order od 
         LEFT JOIN " . DB_PREFIX . "return_receipt rr ON (od.order_id = rr.order_id)
         WHERE od.customer_id = '"
            . (int)$data['customer_id'] . "' 
            AND od.order_status_id != $order_status_canceled
            ORDER BY od.date_added DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getTotalHistories($customer_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order od 
         LEFT JOIN " . DB_PREFIX . "return_receipt rr ON (od.order_id = rr.order_id)
        WHERE customer_id = '" . (int)$customer_id . "'");

        return $query->row['total'];
    }

    public function addTransaction($customer_id, $description = '', $amount = '', $order_id = 0)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "customer_transaction SET customer_id = '" . (int)$customer_id . "', order_id = '" . (int)$order_id . "', description = '" . $this->db->escape($description) . "', amount = '" . (float)$amount . "', date_added = NOW()");
    }

    public function deleteTransactionByOrderId($order_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE order_id = '" . (int)$order_id . "'");
    }

    public function getTransactions($customer_id, $start = 0, $limit = 10)
    {
        if ($start < 0) {
            $start = 0;
        }

        if ($limit < 1) {
            $limit = 10;
        }

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$customer_id . "' ORDER BY date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

        return $query->rows;
    }

    public function getTotalTransactions($customer_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total  FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$customer_id . "'");

        return $query->row['total'];
    }

    public function getTransactionTotal($customer_id)
    {
        $query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$customer_id . "'");

        return $query->row['total'];
    }

    public function getTotalTransactionsByOrderId($order_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_transaction WHERE order_id = '" . (int)$order_id . "'");

        return $query->row['total'];
    }

    public function addReward($customer_id, $description = '', $points = '', $order_id = 0)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "customer_reward SET customer_id = '" . (int)$customer_id . "', order_id = '" . (int)$order_id . "', points = '" . (int)$points . "', description = '" . $this->db->escape($description) . "', date_added = NOW()");
    }

    public function deleteReward($order_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_reward WHERE order_id = '" . (int)$order_id . "' AND points > 0");
    }

    public function getRewards($customer_id, $start = 0, $limit = 10)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "' ORDER BY date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

        return $query->rows;
    }

    public function getTotalRewards($customer_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "'");

        return $query->row['total'];
    }

    public function getRewardTotal($customer_id)
    {
        $query = $this->db->query("SELECT SUM(points) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "'");

        return $query->row['total'];
    }

    public function getTotalCustomerRewardsByOrderId($order_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_reward WHERE order_id = '" . (int)$order_id . "' AND points > 0");

        return $query->row['total'];
    }

    public function getIps($customer_id, $start = 0, $limit = 10)
    {
        if ($start < 0) {
            $start = 0;
        }
        if ($limit < 1) {
            $limit = 10;
        }

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_ip WHERE customer_id = '" . (int)$customer_id . "' ORDER BY date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

        return $query->rows;
    }

    public function getTotalIps($customer_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_ip WHERE customer_id = '" . (int)$customer_id . "'");

        return $query->row['total'];
    }

    public function getTotalCustomersByIp($ip)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_ip WHERE ip = '" . $this->db->escape($ip) . "'");

        return $query->row['total'];
    }

    public function getTotalLoginAttempts($email)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_login` WHERE `email` = '" . $this->db->escape($email) . "'");

        return $query->row;
    }

    public function deleteLoginAttempts($email)
    {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "customer_login` WHERE `email` = '" . $this->db->escape($email) . "'");
    }

    public function blockCustomers($ids)
    {
        if (!is_array($ids) || empty($ids)) {
            return;
        }
        $ids = implode(',', $ids);
        $this->db->query("UPDATE `" . DB_PREFIX . "customer` SET `status` = 0 WHERE `customer_id` IN (" . $ids . ")");
    }

    public function unBlockCustomers($ids)
    {
        if (!is_array($ids) || empty($ids)) {
            return;
        }
        $ids = implode(',', $ids);
        $this->db->query("UPDATE `" . DB_PREFIX . "customer` SET `status` = 1 WHERE `customer_id` IN (" . $ids . ")");
    }

    public function checkCustomersHasOrder($ids)
    {
        if (!is_array($ids) || empty($ids)) {
            return;
        }
        $ids = implode(',', $ids);

        $query = $this->db->query("SELECT `order_id` FROM `" . DB_PREFIX . "order` WHERE `customer_id` IN (" . $ids . ")");
        if ($query->num_rows > 0) {
            return true;
        }

        return false;
    }

    public function deleteCustomers($ids)
    {
        if (!is_array($ids) || empty($ids)) {
            return;
        }
        $ids = implode(',', $ids);
        $this->db->query("DELETE FROM `" . DB_PREFIX . "customer` WHERE `customer_id` IN (" . $ids . ")");
    }

    public function getAddressBook($customer_id)
    {
        $address_default_id = $this->getAddressIdCustomer($customer_id);
        $query = $this->db->query("SELECT *, IF(`address_id` = " . $address_default_id . ", 1, 0) as `default_address` 
                                    FROM `" . DB_PREFIX . "address` WHERE `customer_id` = " .$customer_id);

        $address = $query->rows;
        $this->load->model('localisation/vietnam_administrative');
        foreach ($address as &$data) {
            $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($data['city']);
            $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($data['district']);
            $ward = $this->model_localisation_vietnam_administrative->getWardByCode($data['wards']);

            $data['address_display'] = $data['address'] . (isset($ward['name_with_type']) ? ', '. $ward['name_with_type'] : '') . (isset($district['name_with_type']) ? ', '. $district['name_with_type'] : '') . (isset($province['name_with_type']) ? ', '. $province['name_with_type'] : '');
        }

        return $address;
    }

    public function getAddressIdCustomer($customer_id)
    {
        $query = $this->db->query("SELECT `address_id` FROM `" . DB_PREFIX . "customer` WHERE `customer_id` = '" .$customer_id. "'");

        return $query->row['address_id'];
    }

    public function addNewAddressBook($data, $customer_id)
    {
        if (isset($data['full_name'])) {
            $name = extract_name($data['full_name']);
            $data['firstname'] = $name[0];
            $data['lastname'] = $name[1];
        }

        $district = isset($data['district']) ? $this->db->escape($data['district']) : "";
        $wards = isset($data['wards']) ? $this->db->escape($data['wards']) : "";

        $this->db->query("INSERT INTO " . DB_PREFIX . "address 
                            SET customer_id = '" . (int)$customer_id . "', 
                                firstname = '" . $this->db->escape($data['firstname']) . "', 
                                lastname = '" . $this->db->escape($data['lastname']) . "', 
                                address = '" . $this->db->escape($data['address']) . "', 
                                city = '" . $this->db->escape($data['city']) . "', 
                                district = '" . $district . "', 
                                wards = '" . $wards . "', 
                                phone = '" . $this->db->escape($data['telephone']) . "'");


        $address_id = $this->db->getLastId();

        if (isset($data['default_address'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
        }
    }

    public function editAddressBook($data, $customer_id)
    {
        if (!isset($data['address_id'])) {
            return;
        }

        if (isset($data['full_name'])) {
            $name = extract_name($data['full_name']);
            $data['firstname'] = $name[0];
            $data['lastname'] = $name[1];
        }

        $district = isset($data['district']) ? $this->db->escape($data['district']) : "";
        $wards = isset($data['wards']) ? $this->db->escape($data['wards']) : "";

        $this->db->query("DELETE FROM " . DB_PREFIX . "address 
                                WHERE address_id = " . (int)$data['address_id']);

        $this->db->query("INSERT INTO " . DB_PREFIX . "address 
                                SET customer_id = '" . (int)$customer_id . "', 
                                    firstname = '" . $this->db->escape($data['firstname']) . "', 
                                    lastname = '" . $this->db->escape($data['lastname']) . "', 
                                    address = '" . $this->db->escape($data['address']) . "', 
                                    city = '" . $this->db->escape($data['city']) . "', 
                                    district = '" . $district . "', 
                                    wards = '" . $wards . "', 
                                    phone = '" . $this->db->escape($data['telephone']) . "'");

        $address_id = $this->db->getLastId();

        if (isset($data['default_address'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
        }
    }

    public function deleteAddress($address_id, $customer_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "address 
                                WHERE address_id = '" . (int)$address_id . "' AND customer_id = ".$customer_id);
    }

    public function setDefaultAddress($address_id, $customer_id)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "customer 
                            SET address_id = '" . (int)$address_id . "' 
                            WHERE customer_id = '" . (int)$customer_id . "'");
    }

    public function getAddressDetail($customer_id)
    {
        $address_default_id = $this->getAddressIdCustomer($customer_id);
        $query = $this->db->query("SELECT *, IF(`address_id` = " . $address_default_id . ", 1, 0) as `default_address` 
                                    FROM `" . DB_PREFIX . "address` WHERE `customer_id` = " .$customer_id);

        $address = $query->rows;
        $this->load->model('localisation/vietnam_administrative');
        foreach ($address as &$data) {
            $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($data['city']);
            $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($data['district']);
            $ward = $this->model_localisation_vietnam_administrative->getWardByCode($data['wards']);

            $data['address_display'] = $data['address'] . '; ' . (isset($ward['name_with_type']) ? $ward['name_with_type'] : '') . '; ' . (isset($district['name_with_type']) ? $district['name_with_type'] : '') . '; ' . (isset($province['name_with_type']) ? $province['name_with_type'] : '');
        }

        return $address;
    }

    public function export()
    {
        $this->load->language('customer/customer');
        $this->load->model('user/user');

        /* get all customers */
        $customers = $this->getCustomers();
        // support multi_versions:
        $all_customers = [];

        // refactor data
        foreach ($customers as $customer) {
            if (!isset($customer['customer_id'])) {
                continue;
            }
            /*
             * code
             * address
             * status
             * email
             * group
             * birthday
             * sex
             * website
             * tax_code
             * staff_in_charge
             * note
             */
            if(!$customer['full_name']){
                $customer['full_name'] = $customer['lastname'] . ' ' . $customer['firstname'];
            }

            $customer['status'] = $customer['status'] == 1 ? 'Đang hoạt động' : 'Đã khóa'; // not translate
            if ($customer['birthday']) {
                $date = date_create($customer['birthday']);
                $customer['birthday'] = date_format($date, "d/m/Y");
            }
            if ($customer['sex']) {
                if ($customer['sex'] == 1) {
                    $customer['sex'] = 'Nam';
                } else if ($customer['sex'] == 2) {
                    $customer['sex'] = 'Nữ';
                } else if ($customer['sex'] == 3) {
                    $customer['sex'] = 'Khác';
                }
            }
            if ($customer['staff_in_charge']) {
                $user_info = $this->model_user_user->getUser($customer['staff_in_charge']);
                if (array_key_exists('username', $user_info)) {
                    $customer['staff_in_charge'] = $user_info['username'];
                }
            }

            $all_address = [];
            $address = $this->getAddressDetail($customer['customer_id']);
            foreach ($address as $addr) {
                if (!is_array($addr) || !array_key_exists('address_id', $addr)) {
                    continue;
                }
                $addr_display = '';
                $addr_display = $addr['lastname'] . ' ' . $addr['firstname'];
                $addr_display .= '; ' . $addr['phone'];
                $addr_display .= '; ' . $addr['address_display'];
                $addr_display .= '; ' . $addr['default_address'];
                $all_address[] = $addr_display;
            }
            $customer['all_address'] = implode(PHP_EOL, $all_address);

            $all_customers[] = $customer;
        }

        /* create xlsx and bind customer data to it */
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $column = 'A';
        $row = 1;

        // map fields from language
        $EXPORT_EXCEL_COLUMN_MAPPING = [
            //'code' => 'mã_khách_hàng',
            'full_name' => 'họ_và_tên',
            'telephone' => 'số_điện_thoại',
            'all_address' => 'thông_tin_địa_chỉ',
            'status' => 'trạng_thái',
            'email' => 'email',
            'group_name' => 'nhóm_khách_hàng',
            'birthday' => 'ngày_sinh',
            'sex' => 'giới_tính',
            'website' => 'website',
            'tax_code' => 'mã_số_thuế',
            'staff_in_charge' => 'nhân_viên_phụ_trách',
            'note' => 'ghi_chú',
        ];

        // write header
        foreach ($EXPORT_EXCEL_COLUMN_MAPPING as $headerName) {
            $sheet->setCellValue($column . $row, $headerName);
            $column++;
        }

        // write rows
        $row++;
        foreach ($all_customers as $customer) {
            // filter out not used columns
            $p = array_intersect_key($customer, $EXPORT_EXCEL_COLUMN_MAPPING);
            // sort to sync columns
            $p = array_merge(array_flip(array_keys($EXPORT_EXCEL_COLUMN_MAPPING)), $p);

            $column = 'A';
            foreach ($p as $col => $value) {
                $sheet->setCellValueExplicit($column . $row, $value, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $column++;
            }
            $row++;
        }

        /*
         * write output for downloading...
         * https://stackoverflow.com/questions/27701981/phpexcel-download-using-ajax-call
         */
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        ob_start();
        $writer->save("php://output"); // for test: DIR_APPLICATION . "products.xlsx"
        $xlsxData = ob_get_contents();
        ob_end_clean();

        return 'data:application/vnd.ms-excel;base64,' . base64_encode($xlsxData);
    }

    public function getTotalEmailsSubscribers($filter)
    {
        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "email_subscribers";

        $sql .= " WHERE 1 ";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getEmailsSubscribers($data = array())
    {
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM {$DB_PREFIX}email_subscribers";

        $sql .= " WHERE 1 ";

        $implode = array();

        $filter = $data;

        $sql .= " ORDER BY date_added DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function deleteEmailsSubscribers($email_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "email_subscribers WHERE email_id = '" . (int)$email_id . "'");
    }

    public function importMultiCustomer($data)
    {
        if (!is_array($data)) {
            return;
        }
        $this->load->model('user/user');
        $this->load->model('customer/customer_group');

        foreach ($data as $item) {
            $customer = [];
            $customer['full_name'] = $item['full_name'];
            $customer['telephone'] = $item['phone'];
            $customer['email'] = $item['email'];
            $customer['status'] = $item['status'];
            $customer['address_id'] = '';

            $customer['birthday'] = '';
            if ($item['birthday']){
                $date = str_replace('/', '-', $item['birthday']);
                $customer['birthday'] = date("Y-m-d", strtotime($date));
            }

            if ($item['sex'] == 'Nam') {
                $customer['sex'] = 1;
            } else if ($item['sex'] == 'Nữ') {
                $customer['sex'] = 2;
            } else {
                $customer['sex'] = 3;
            }
            $customer['website'] = $item['website'];
            $customer['tax_code'] = $item['tax_code'];
            $customer['staff_in_charge'] = '';
            if ($item['staff_in_charge']){
                $user = $this->model_user_user->getUserByUsername($item['staff_in_charge']);
                if (isset($user['user_id'])){
                    $customer['staff_in_charge'] = $user['user_id'];
                }
            }

            $customer['note'] = $item['note'];
            $customer['customer_group_id'] = '';
            if ($item['group']){
                $group = $this->model_customer_customer_group->getCustomerGroupByName($item['group']);
                if (isset($group['customer_group_id'])){
                    $customer['customer_group_id'] = $group['customer_group_id'];
                }else{
                    $customer['customer_group_id'] = 'group_' . $item['group'];
                }
            }

            $customer_id = $this->importCustomer($customer);
            if ($customer_id){
                $this->importCustomerAddress($customer_id, $item['addresses']);
            }
        }
    }

    private function importCustomer($data)
    {
        if (!isset($data['telephone'])) {
            return false;
        }

        if ($data['email']) {
            $email_customer_id = $this->checkEmailExist($data['email']);
            if ($email_customer_id) {
                $phone_customer_id = $this->checkPhoneExist($data['telephone']);
                if ($phone_customer_id) {
                    if ($phone_customer_id == $email_customer_id) {
                        // update customer
                        return $this->editCustomer($email_customer_id, $data);
                    } else {
                        return false;
                    }
                } else {
                    // update customer
                    return $this->editCustomer($email_customer_id, $data);
                }
            } else {
                $phone_customer_id = $this->checkPhoneExist($data['telephone']);
                if ($phone_customer_id) {
                    return false;
                } else {
                    // add new customer
                    return $this->addCustomer($data);
                }
            }
        } else {
            $phone_customer_id = $this->checkPhoneExist($data['telephone']);
            if ($phone_customer_id) {
                // update customer
                return $this->editCustomer($phone_customer_id, $data);
            } else {
                // add new customer
                return $this->addCustomer($data);
            }
        }
    }

    private function importCustomerAddress($customer_id, $addresses)
    {
        if (!$customer_id || !is_array($addresses) || empty($addresses)) {
            return;
        }
        //Remove all old address
        $this->db->query("DELETE FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");

        foreach ($addresses as $address) {
            $new_address = [];
            $addr = explode(';', $address);
            if (count($addr) < 7) {
                continue;
            }
            $new_address['full_name'] = trim($addr[0]);
            $new_address['telephone'] = trim($addr[1]);
            $new_address['address'] = trim($addr[2]);
            $new_address['wards'] = trim($addr[3]);
            $new_address['district'] = trim($addr[4]);
            $new_address['city'] = trim($addr[5]);
            if (trim(end($addr)) == 1) {
                $new_address['default_address'] = 1;
            }

            $vna = new \Vietnam_Administrative();
            if ($new_address['city']){
                $new_address['city'] = $vna->getAdministrativeCodeByTextValue($new_address['city']);
                if ($new_address['district'] && $new_address['city']){
                    $new_address['district'] = $vna->getAdministrativeCodeByTextValue($new_address['district'], $new_address['city']);
                }
                if ($new_address['wards'] && $new_address['district']){
                    $new_address['wards'] = $vna->getAdministrativeCodeByTextValue($new_address['wards'], $new_address['district']);
                }
            }

            $this->addNewAddressBook($new_address, $customer_id);
        }
    }

    private function checkPhoneExist($phone)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer` WHERE `telephone` = '" . $this->db->escape($phone) . "'");
        if (isset($query->row['customer_id'])) {
            return $query->row['customer_id'];
        }

        return false;
    }

    private function checkEmailExist($email)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer` WHERE `email` = '" . $this->db->escape($email) . "'");
        if (isset($query->row['customer_id'])) {
            return $query->row['customer_id'];
        }

        return false;
    }
}
