<?php

class ModelApiChatboxNovaonV2 extends Model
{
    const __ERR_MESSAGE_MAX_LENGTH = 500;

    /**
     * get products list data (include one product), old version, for Further reading
     *
     * @param array $query_params
     * @param bool $is_all_statuses if true, includes status hide, not_sale_out_of_stock, deleted, ...
     * @param array $more_return_fields
     * @return array|mixed
     */
    public function getProductsListData_v1_0(array $query_params, $is_all_statuses = false, $more_return_fields = [])
    {
        $this->load->language('api/chatbot_novaon');

        $data_filter = [];
        if (isset($query_params['page']) && !empty($query_params['page'])) {
            $data_filter = [
                'start' => ((int)$query_params['page'] - 1) * ((int)isset($query_params['limit']) ? $query_params['limit'] : 15),
                'limit' => isset($query_params['limit']) ? $query_params['limit'] : 15
            ];
        }

        if (isset($query_params['searchKey']) && isset($query_params['searchField'])) {
            $data_filter = array_merge($data_filter, [
                'filter_' . $query_params['searchField'] => $query_params['searchKey'],
            ]);
        }

        if (isset($query_params['except_product_ids'])) {
            $data_filter = array_merge($data_filter, [
                'filter_except_product_ids' => $query_params['except_product_ids']
            ]);
        }

        if (isset($query_params['id'])) {
            $data_filter = [
                'filter_product_id' => $query_params['id'],
                'filter_product_version_id' => isset($query_params['product_version_id']) ? $query_params['product_version_id'] : '',
            ];
        }

        if ($is_all_statuses) {
            $data_filter = array_merge($data_filter, [
                'filter_is_all_statuses' => true
            ]);
        }

        if (isset($query_params['is_all'])) {
            $data_filter = array_merge($data_filter, [
                'filter_is_all' => $query_params['is_all']
            ]);
        }

        try {
            $this->load->model('api/chatbox_novaon');
            $products = $this->model_api_chatbox_novaon->getAPIProducts($data_filter);
            $products_total = $this->model_api_chatbox_novaon->getAPIProductsTotal($data_filter);
            $records = [];
            foreach ($products as $product) {
                $r = [
                    "id" => $product['product_id'],
                    "name" => $product['name'],
                    "description" => $product['description'],
                    "short_description" => $product['sub_description'],
                    "original_price" => $product['compare_price_version'],
                    "sale_price" => $product['price_version'],
                    "image" => $product['image'],
                    "url" => $this->url->link('product/product', 'product_id=' . $product['product_id']),
                    "version" => $product["name_version"],
                    "product_version_id" => $product["product_version_id"],
                    "quantity" => $product["quantity_in_stock"],
                    "default_store" => [
                        'id' => $product["store_id"],
                        'name' => $product["store_name"]
                    ],
                ];

                if (!empty($more_return_fields)) {
                    foreach ($more_return_fields as $mr_field) {
                        if (array_key_exists($mr_field, $product)) {
                            $r[$mr_field] = $product[$mr_field];
                        }
                    }
                }

                $records[] = $r;
            }

            // get list
            if (!isset($query_params['id'])) {
                return [
                    'total' => $products_total,
                    'page' => (isset($query_params['page']) && !empty($query_params['page'])) ? $query_params['page'] : '',
                    'records' => $records,
                ];
            } else {
                // get one
                if ($records) {
                    return $records[0];
                }

                return [
                    'code' => 102,
                    'message' => 'products info invalid'
                ];
            }
        } catch (Exception $e) {
            $len = mb_strlen($e->getMessage()) > self::__ERR_MESSAGE_MAX_LENGTH ? self::__ERR_MESSAGE_MAX_LENGTH : null; // null for sub string to the end

            return [
                'code' => 201,
                'message' => 'unknown error',
                '__error' => mb_substr($e->getMessage(), 0, $len)
            ];
        }
    }

    /**
     * get products list data (include one product)
     * @param array $query_params
     * @param bool $is_all_statuses if true, includes status hide, not_sale_out_of_stock, deleted, ...
     * @param array $more_return_fields
     * @return array|mixed
     */
    public function getProductsListData_v1_1(array $query_params, $is_all_statuses = false, $more_return_fields = [])
    {
        $this->load->language('api/chatbot_novaon');

        $data_filter = [];
        if (isset($query_params['page']) && !empty($query_params['page'])) {
            $data_filter = [
                'start' => ((int)$query_params['page'] - 1) * ((int)isset($query_params['limit']) ? $query_params['limit'] : 15),
                'limit' => isset($query_params['limit']) ? $query_params['limit'] : 15
            ];
        }

        if (isset($query_params['searchKey']) && isset($query_params['searchField'])) {
            $data_filter = array_merge($data_filter, [
                'filter_' . $query_params['searchField'] => $query_params['searchKey'],
            ]);
        }

        if (isset($query_params['product_ids'])) {
            $data_filter = array_merge($data_filter, [
                'filter_product_ids' => $query_params['product_ids']
            ]);
        }

        if (isset($query_params['except_product_ids'])) {
            $data_filter = array_merge($data_filter, [
                'filter_except_product_ids' => $query_params['except_product_ids']
            ]);
        }

        if (isset($query_params['is_all'])) {
            $data_filter = array_merge($data_filter, [
                'filter_is_all' => $query_params['is_all']
            ]);
        }

        if (isset($query_params['id'])) {
            $data_filter = [
                'filter_product_id' => $query_params['id'],
                'filter_product_version_id' => isset($query_params['product_version_id']) ? $query_params['product_version_id'] : '',
            ];
        }

        if ($is_all_statuses) {
            $data_filter = array_merge($data_filter, [
                'filter_is_all_statuses' => true
            ]);
        }

        try {
            $this->load->model('api/chatbox_novaon');
            $products = $this->model_api_chatbox_novaon->getAPIProducts($data_filter);
            $products_total = $this->model_api_chatbox_novaon->getAPIProductsTotal($data_filter);
            $records = [];
            $grouped_reports = [];
            foreach ($products as $product) {
                $quantity = $product["multi_versions"] == 0 ? $product["product_master_quantity"] : $product["product_version_quantity"];

                $r = [
                    "id" => $product['product_id'],
                    "name" => $product['name'],
                    "description" => $product['description'],
                    "short_description" => $product['sub_description'],
                    "original_price" => $product['compare_price_version'],
                    "sale_price" => $product['price_version'],
                    "image" => $product['image'],
                    "url" => $this->url->link('product/product', 'product_id=' . $product['product_id']),
                    "version" => $product["name_version"],
                    "product_version_id" => $product["product_version_id"],
                    "quantity" => $quantity,
                    "default_store" => [
                        'id' => $product["store_id"],
                        'name' => $product["store_name"]
                    ],
                    "sku" => $product["sku"],
                    "in_stock" => ($product["sale_on_out_of_stock"] == 1 || $quantity > 0) ? 1 : 0,
                    "sale_on_out_of_stock" => $product["sale_on_out_of_stock"]
                ];

                if (!empty($more_return_fields)) {
                    foreach ($more_return_fields as $mr_field) {
                        if (array_key_exists($mr_field, $product)) {
                            $r[$mr_field] = $product[$mr_field];
                        }
                    }
                }

                $records[] = $r;

                // update list grouped_reports (master products list)
                if (!array_key_exists($r['id'], $grouped_reports)) {
                    $grouped_reports[$r['id']] = $r;
                }
            }

            // organize all versions into same product
            // NOTICE: $products_total may wrong when group...
            foreach ($records as $record) {
                $prod_id = $record['id'];

                // init versions
                if (!array_key_exists('versions', $grouped_reports[$prod_id])) {
                    $grouped_reports[$prod_id]['versions'] = [];
                }

                // skip master product
                if (empty($record['product_version_id'])) {
                    continue;
                }

                // else: product version => add to versions list of associated master product by product_id
                //// remove no need field
                unset($record['id']);
                unset($record['name']);
                unset($record['description']);
                unset($record['short_description']);
                unset($record['image']);
                unset($record['url']);

                $grouped_reports[$prod_id]['versions'][] = $record;
            }

            // standardize output
            foreach ($grouped_reports as &$grouped_report) {
                // unset some fields from product master
                unset($grouped_report['version']);
                unset($grouped_report['product_version_id']);

                // unset more if has versions
                if (!empty($grouped_report['versions'])) {
                    unset($grouped_report['original_price']);
                    unset($grouped_report['sale_price']);
                    unset($grouped_report['quantity']);
                    unset($grouped_report['default_store']);
                    unset($grouped_report['sku']);
                }
            }
            unset($grouped_report);

            // get list
            if (!isset($query_params['id'])) {
                return [
                    'total' => $products_total,
                    'page' => (isset($query_params['page']) && !empty($query_params['page'])) ? $query_params['page'] : '',
                    'records' => array_values($grouped_reports),
                ];
            } else {
                // get one
                if ($grouped_reports) {
                    return reset($grouped_reports);
                }

                return [
                    'code' => 102,
                    'message' => 'products info invalid'
                ];
            }
        } catch (Exception $e) {
            $len = mb_strlen($e->getMessage()) > self::__ERR_MESSAGE_MAX_LENGTH ? self::__ERR_MESSAGE_MAX_LENGTH : null; // null for sub string to the end

            return [
                'code' => 201,
                'message' => 'unknown error',
                '__error' => mb_substr($e->getMessage(), 0, $len)
            ];
        }
    }

    /**
     * getOrdersList
     * @param array $data_filter
     * @return array
     */
    public function getOrdersListData(array $data_filter)
    {
        try {
            $this->load->model('api/chatbox_novaon');
            $this->load->model('api/chatbox_novaon_v2');
            $this->load->model('sale/order');
            $this->load->model('localisation/vietnam_administrative');
            $orders = $this->model_api_chatbox_novaon->getOrderList($data_filter);
            $orders_total = $this->model_api_chatbox_novaon->getOrdersListTotal($data_filter);
            $orders_total_revenue = $this->model_api_chatbox_novaon->getOrderTotalRevenue($data_filter);

            $records = [];
            foreach ($orders as $order) {
                $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($order['shipping_province_code']);
                $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($order['shipping_district_code']);
                $ward = $this->model_localisation_vietnam_administrative->getWardByCode($order['shipping_ward_code']);

                $products_of_order = $this->model_sale_order->getProductsDetailByOrder($order['order_id']);
                $products_info = [];
                foreach ($products_of_order as $product) {
                    $product_id = $product['product_id'];
                    $product_version_id = isset($product['product_version_id']) && !empty($product['product_version_id']) ? $product['product_version_id'] : null;

                    // get product info same as api get product detail
                    $product_info = $this->getProductsListData_v1_0([
                        'id' => $product_id,
                        'product_version_id' => $product_version_id,
                        'is_all' => 1 /* always get product regardless out of stock or not */
                    ], $all_statuses = true);

                    // patch quantity in order
                    $product_info['quantity'] = $product['quantity'];

                    // patch sale price, original_price at buy time as in order_product
                    $product_info['original_price'] = isset($product['price']) ? $product['price'] : (isset($product['p_price']) ? $product['p_price'] : 0);
                    $product_info['sale_price'] = isset($product['price']) ? $product['price'] : (isset($product['p_price']) ? $product['p_price'] : 0);

                    $products_info[] = $product_info;
                }

                $tags = getValueByKey($order, 'tag_value', '');
                $tags = empty($tags) ? [] : explode(',', $tags);
                $tags = array_values(array_unique($tags));

                $records[] = [
                    "customer" => [
                        "name" => $order['order_full_name'],
                        "phone_number" => $order['telephone_order'],
                        "email" => $order['email'],
                        "delivery_addr" => $order['shipping_address_1'],
                        "full_delivery_addr" => sprintf('%s%s%s%s',
                            $order['shipping_address_1'],
                            isset($ward['name_with_type']) ? (', ' . $ward['name_with_type']) : '',
                            isset($district['name_with_type']) ? (', ' . $district['name_with_type']) : '',
                            isset($province['name_with_type']) ? (', ' . $province['name_with_type']) : ''
                        ),
                        "ward" => isset($ward['code']) ? $ward['code'] : '',
                        "district" => isset($district['code']) ? $district['code'] : '',
                        "city" => isset($province['code']) ? $province['code'] : '',
                        "note" => $order['comment'],
                        "subscriber_id" => $order['customer_subscriber_id'],
                        "customer_group_id" => $order['customer_customer_group_id'],
                    ],
                    "status" => $order['order_status_id'],
                    "customer_ref_id" => $order['customer_ref_id'],
                    "order_id" => $order['order_id'],
                    "order_code" => isset($order['order_code']) ? $order['order_code'] : '',
                    "tags" => $tags,
                    "total" => isset($order['total']) ? $order['total'] : '',
                    "date_added" => isset($order['date_added']) ? $order['date_added'] : '',
                    "date_modified" => isset($order['date_modified']) ? $order['date_modified'] : '',
                    "products" => $products_info,
                    "payment_trans" => $order['shipping_method'],
                    "payment_trans_custom_name" => $order['shipping_method'] == '-3' ? $order['shipping_method_value'] : '',
                    "payment_trans_custom_fee" => $order['shipping_method'] == '-3' ? $order['shipping_fee'] : '',
                    "payment_status" => $order['payment_status'] == 1 ? 1 : 0,
                    "delivery_fee" => $order['shipping_fee'],
                    "campaign_id" => $order['campaign_id'],
                    "channel_id" => $order['channel_id']
                ];
            }

            $json = [
                'total' => $orders_total,
                'page' => (isset($this->request->get['page']) && !empty($this->request->get['page'])) ? $this->request->get['page'] : '',
                'total_revenue' => $orders_total_revenue,
                'records' => $records
            ];
        } catch (Exception $e) {
            $len = mb_strlen($e->getMessage()) > self::__ERR_MESSAGE_MAX_LENGTH ? self::__ERR_MESSAGE_MAX_LENGTH : null; // null for sub string to the end

            $json = [
                'code' => 201,
                'message' => 'unknown error',
                '__error' => mb_substr($e->getMessage(), 0, $len)
            ];
        }

        return $json;
    }
}
