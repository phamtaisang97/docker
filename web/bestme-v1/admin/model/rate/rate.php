<?php

class ModelRateRate extends Model
{
    CONST RATE_IS_SHOW = 1;
    CONST RATE_IS_HIDDEN = 0;

    public function addRate($data)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "rate_website
                          SET customer_name = '" . $this->db->escape($data['customer_name']) . "',
                          sub_info = '" . $this->db->escape($data['sub_info']) . "',
                          content = '" . $this->db->escape($data['content']) . "',
                          image = '" . $this->db->escape($data['image']) . "',
                          alt = '" . $this->db->escape($data['alt']) . "',
                          status = '" . $this->db->escape($data['status']) . "',
                          date_added = NOW(),
                          date_modified = NOW()"
        );
        $rate_id = $this->db->getLastId();
        return $rate_id;
    }

    public function getAllRates($data = array())
    {
        $sql = $this->getAllRatesSql($data);

        // limit
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }
            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalRates($data = [])
    {
        $sql = $this->getAllRatesSql($data);

        $sql = "SELECT COUNT(1) as `total` FROM ($sql) as ab";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getRate($rate_id)
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "rate_website b
                WHERE b.rate_id = '" . (int)$rate_id . "'";
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function updateStatus($id, $status)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "rate_website SET status = " . $status . " WHERE rate_id = " . (int)$id . "");
        return true;
    }

    public function deleteRate($rate_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "rate_website WHERE rate_id = '" . (int)$rate_id . "'");
    }

    public function editRate($rate_id, $data)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "rate_website
                          SET `customer_name` = '" . $this->db->escape($data['customer_name']) . "',
                          `sub_info` = '" . $this->db->escape($data['sub_info']) . "',
                          `content` = '" . $this->db->escape($data['content']) . "', 
                          `image` = '" . $this->db->escape($data['image']) . "',
                          `status` = '" . $this->db->escape($data['status']) . "',
                          `alt` = '" . $this->db->escape($data['alt']) . "' 
                          WHERE `rate_id` = " . (int)$rate_id . "");
    }

    private function getAllRatesSql($data = array())
    {
        $db_prefix = DB_PREFIX;
        $sql = "SELECT * FROM `{$db_prefix}rate_website` r";

        if (!empty($data['filter_name'])) {
            $sql .= " WHERE r.`customer_name` LIKE '%" . $this->db->escape($data['filter_name']) . "%'
            OR r.`content` LIKE '%" . $this->db->escape($data['filter_name']) . "%'
            OR r.`sub_info` LIKE '%" . $this->db->escape($data['filter_name']) . "%'
            ";
        }
        $sql .= " ORDER BY r.`date_modified` DESC";
        return $sql;
    }
}
