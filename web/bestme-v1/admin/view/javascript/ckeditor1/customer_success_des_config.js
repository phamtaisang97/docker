/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here.
    // For complete reference see:
    // https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_config.html
    config.filebrowserUploadMethod  = "form";
    // The toolbar groups arrangement, optimized for two toolbar rows.
    config.toolbarGroups = [
        { name: 'tools', groups: [ 'Maximize' ] },
        { name: 'document', groups: [ 'mode' ]},
        { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
        { name: 'links', groups: [ 'links' ] },
        { name: 'insert', groups: [ 'insert' ] },
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        '/',
        { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align'] },
        { name: 'styles' },
        { name: 'colors' }
    ];

    // Remove some buttons provided by the standard plugins, which are
    // not needed in the Standard(s) toolbar.
    config.removeButtons = 'Underline,Subscript,Superscript,Anchor,Flash,Iframe,CreateDiv';
    config.removeDialogTabs = 'link:upload;image:Upload'

    // Set the most common block elements.
    config.format_tags = 'p;h1;h2;h3;pre';
    // fix turn off html endcode
    config.htmlEncodeOutput = false;
    config.entities = false;
    config.basicEntities = false;

    // Simplify the dialog windows.
    config.removeDialogTabs = 'image:advanced;link:advanced';
    config.extraPlugins = 'imagebrowse,confighelper,wordcount';
    config.wordcount = {

        // Whether or not you want to show the Word Count
        showWordCount: false,

        // Whether or not you want to show the Char Count
        showCharCount: true,

        // Maximum allowed Word Count
        maxWordCount: 4,

        // Maximum allowed Char Count
        maxCharCount: 20000,
        language: 'vi',
        showParagraphs : false
    };

    /* prevent auto removing empty tags */
    config.protectedSource.push(/<i[\s\S]*?\/i>/g);
};
