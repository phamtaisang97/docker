/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here.
    // For complete reference see:
    // https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_config.html
    config.filebrowserUploadMethod  = "form";
    // The toolbar groups arrangement, optimized for two toolbar rows.
    config.toolbarGroups = [
        {name: 'insert', groups: ['insert', 'basicstyles', 'colors']},
        {name: 'basicstyles', groups: ['cleanup']},
        {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align']},
        {name: 'styles'},
    ];

    // Remove some buttons provided by the standard plugins, which are
    // not needed in the Standard(s) toolbar.
    config.removeButtons = 'Underline,Subscript,Superscript,Anchor,Flash,Iframe,CreateDiv';
    config.removeButtons = 'Smiley,SpecialChar,CopyFormatting,RemoveFormat,Subscript,Superscript,Iframe,CreateDiv,Blockquote,Flash';
    config.removeDialogTabs = 'link:upload;image:Upload'

    // Set the most common block elements.
    config.format_tags = 'p;h1;h2;h3;pre';
    // fix turn off html endcode
    config.htmlEncodeOutput = false;
    config.entities = false;
    config.basicEntities = false;
    config.allowedContent = true;
    // Simplify the dialog windows.
    config.removeDialogTabs = 'image:advanced;link:advanced';
    config.extraPlugins = 'imagebrowse,confighelper,wordcount';
    config.wordcount = {

        // Whether or not you want to show the Word Count
        showWordCount: false,

        // Whether or not you want to show the Char Count
        showCharCount: true,

        // Maximum allowed Word Count
        maxWordCount: 4,

        // Maximum allowed Char Count
        maxCharCount: 30000,
        language: 'vi',
        showParagraphs : false
    };
};
