$(document).ready(function () {

    $('#add_partner').click(function () {
        var count = $('.collapse').length + 1;
        var html = '<div class="border-bottom border-secondary input-item-slide d-flex flex-column  input px-3 py-2">\n' +
            '                        <div class="d-flex justify-content-between mb-2" data-toggle="collapse" data-target="#collapseMenuWebsite' + count + '" aria-expanded="true" aria-controls="collapseMenuWebsite' + count + '">\n' +
            '                            <div class="d-flex align-items-center">\n' +
            '                                <div class="mr-3">\n' +
            '                                    <svg width="8" height="5" viewBox="0 0 8 5" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
            '                                        <path d="M4.00149 3.19523L7.02642 0.167919C7.25032 -0.0559732 7.61236 -0.0559732 7.83387 0.167919C8.05538 0.391812 8.05538 0.753851 7.83387 0.977744L4.4064 4.40759C4.18965 4.62434 3.8419 4.6291 3.61801 4.42426L0.166728 0.980126C0.0547824 0.868179 -3.14943e-08 0.720505 -2.51434e-08 0.575213C-1.87925e-08 0.429921 0.0547824 0.282247 0.166728 0.170301C0.390622 -0.0535915 0.752661 -0.0535915 0.974172 0.170301L4.00149 3.19523Z" fill="white" />\n' +
            '                                    </svg>\n' +
            '                                </div>\n' +
            '                                <h6 class="m-0">\n' +
            '                                    Đối tác 0' + count + '\n' +
            '                                </h6>\n' +
            '                            </div>\n' +
            '                            <div>\n' +
            '                                <span class="mx-2">\n' +
            '                                    <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
            '                                        <path d="M0.423408 2.56331H0.888124L1.97246 11.6279C1.99312 11.845 2.179 12 2.39587 12H9.60413C9.821 12 9.99656 11.845 10.0275 11.6279L11.1119 2.56331H11.5766C11.8141 2.56331 12 2.37726 12 2.13953C12 1.90181 11.8141 1.71576 11.5766 1.71576H10.7298H8.33391V0.423773C8.33391 0.186047 8.14802 0 7.9105 0H4.0895C3.85198 0 3.66609 0.186047 3.66609 0.423773V1.71576H1.27022H0.423408C0.185886 1.71576 0 1.90181 0 2.13953C0 2.37726 0.196213 2.56331 0.423408 2.56331ZM4.51291 0.847545H7.48709V1.71576H4.51291V0.847545ZM10.2547 2.56331L9.23236 11.1628H2.76764L1.74527 2.56331H10.2547Z" fill="white" />\n' +
            '                                        <path d="M4.59614 9.87072C4.83386 9.87072 5.01991 9.68468 5.01991 9.44695V4.37202C5.01991 4.13429 4.83386 3.94824 4.59614 3.94824C4.35841 3.94824 4.17236 4.13429 4.17236 4.37202V9.44695C4.17236 9.68468 4.35841 9.87072 4.59614 9.87072Z" fill="white" />\n' +
            '                                        <path d="M7.40522 9.87072C7.64294 9.87072 7.82899 9.68468 7.82899 9.44695V4.37202C7.82899 4.13429 7.64294 3.94824 7.40522 3.94824C7.16749 3.94824 6.98145 4.13429 6.98145 4.37202V9.44695C6.98145 9.68468 7.17783 9.87072 7.40522 9.87072Z" fill="white" />\n' +
            '                                    </svg>\n' +
            '                                </span>\n' +
            '                                <span>\n' +
            '                                    <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
            '                                        <ellipse cx="1.16667" cy="1.2" rx="1.16667" ry="1.2" fill="white" />\n' +
            '                                        <ellipse cx="1.16667" cy="6.00005" rx="1.16667" ry="1.2" fill="white" />\n' +
            '                                        <ellipse cx="1.16667" cy="10.8001" rx="1.16667" ry="1.2" fill="white" />\n' +
            '                                        <ellipse cx="5.83317" cy="1.2" rx="1.16667" ry="1.2" fill="white" />\n' +
            '                                        <ellipse cx="5.83317" cy="6.00005" rx="1.16667" ry="1.2" fill="white" />\n' +
            '                                        <ellipse cx="5.83317" cy="10.8001" rx="1.16667" ry="1.2" fill="white" />\n' +
            '                                    </svg>\n' +
            '                                </span>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                        <div class="collapse" id="collapseMenuWebsite' + count + '">\n' +
            '                            <div>\n' +
            '                                <small>Tải ảnh lên</small>\n' +
            '                                <div class="form-control form-control-sm">\n' +
            '                                    <input type="file" class="form-control-file" />\n' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                            <div>\n' +
            '                                <small>Đường dẫn (link URL)</small>\n' +
            '                                <input placeholder="Gắn link hoặc tìm kiếm" class="form-control  form-control-sm" />\n' +
            '                            </div>\n' +
            '                            <div>\n' +
            '                                <small>Mô tả ảnh</small>\n' +
            '                                <input placeholder="" class="form-control  form-control-sm" />\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>';
        $('#add_partner').before(html);

    });

});
