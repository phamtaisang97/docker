$(document).ready(function () {
    var x, i, j, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName("custom-selectag");
    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        /*for each element, create a new DIV that will act as the selected item:*/
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < selElmnt.length; j++) {
            /*for each option in the original select element,
            create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function (e) {
                /*when an item is clicked, update the original select box,
                and the selected item:*/
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function (e) {
            /*when the select box is clicked, close any other select boxes,
            and open/close the current select box:*/
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
        });
    }

    function closeAllSelect(elmnt) {
        /*a function that will close all select boxes in the document,
        except the current select box:*/
        var x, y, i, arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        for (i = 0; i < y.length; i++) {
            if (elmnt == y[i]) {
                arrNo.push(i)
            } else {
                y[i].classList.remove("select-arrow-active");
            }
        }
        for (i = 0; i < x.length; i++) {
            if (arrNo.indexOf(i)) {
                x[i].classList.add("select-hide");
            }
        }
    }
    /*if the user clicks anywhere outside the select box,
    then close all select boxes:*/
    document.addEventListener("click", closeAllSelect);


    //***Sidebar***//
    /* ACTION RESPONSIVE */
    $(".action-responsive ul li a").click(function () {
        $(".action-responsive ul li a").removeClass('active');
        $(this).toggleClass("active");
    });
    $(".btn-eye").click(function () {
        $(this).parent().toggleClass("hide");
    });
    $(".accordion").click(function () {
        var panel = $(this).parent().next();
        if(false == panel.is(':visible')){
            $('.panel').slideUp("fast");
            
        }
        panel.slideDown('fast');
        $('.item-sidebar').removeClass('sub-item');
        $(this).parent().toggleClass("active");
        $(this).parent().parent().addClass("sub-item");
    });
    
    $(".custom-check .input").click(function () {
        $(this).parent().next().toggle("fast");
    });

    // active upload
    $(".focus-check").click(function () {
        $(this).parent().toggleClass("active");
    });

    //SET HEIGHT// 
    var up_width = $('.upload-item .thumb').width();
    $('.upload-item .thumb').css('height', up_width);
    // SHOW SIDEBAR
    $('.open-nav').click(function () {
        $('#sidebar').toggleClass('show-sidebar');
    });
    // popup //
    $('.button-popup').click(function () {
        console.log('hsdgfahsgfhjsgfhsg')
        var $parent = $(this).closest('.bdt-item');
        var itemId = $(this).attr('data-id');
        var buttonId = $(this).attr('id');
        $('#modal-container').removeAttr('class').addClass(buttonId);
        $('body').addClass('modal-active');
    });

    $('.close-btn').click(function () {
        $('#modal-container').addClass('out');
        $('body').removeClass('modal-active');
    });
    $('.modal-background').click(function () {
        $('#modal-container').addClass('out');
        $('body').removeClass('modal-active');
    });
    $(".modal-background .modal").click(function (e) {
        e.stopPropagation();
    });
    if ($(window).width() > 767) {
        if ($(".modal-background .modal").height() > $(window).height()) {
            $('#modal-container').css('display', 'block');
            $('#modal-container').css('overflow-y', 'scroll');
            $('#modal-container .modal-background').css('display', 'block');
        } else {
            $('#modal-container').css('display', 'table');
            $('#modal-container').css('overflow-y', 'auto');
            $('#modal-container .modal-background').css('display', 'table-cell');
        }
    } else {

    }
    // TOOLTIP
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    // DROPDOWN
    $('.dropdown-menu').find("a").click(function(){
        $('.dropdown-toggle').html($(this).html());
      });
    //   VIEW
    $('.btn-view-mobile').click(function (e) { 
        $('.main-content').addClass('view-mobile');
        $('.main-content').removeClass('fullscreen');
    });
    $('.btn-fullscreen').click(function (e) { 
        $('.main-content').addClass('fullscreen');
        $('.main-content').removeClass('view-mobile');
    });
    $('.btn-viewdefault').click(function (e) { 
        $('.main-content').removeClass('fullscreen');
        $('.main-content').removeClass('view-mobile');
    });
});