// Show password change input in profile setting page
function showPasswordChange(e) {
    $('.change-password').removeClass('d-none');
    $(e).remove();
}

// Enter page to pagination
function gotoPage(elem,max){
    var page = $(elem).val()
    page = page.replace(/[$,]+/g,"");
    if(isNumber(page)){
        if(page <= 0){
            alert("Trang cần đến phải là số dương!");
            $(elem).val('')
        }else{
            if(page > max){
                page = max;
            }
            filter(page);
            $('html, body').animate({
                scrollTop: ($('.box-head').offset().top)
            }, 300);
        }
    }else {
        alert("Bạn phải nhập vào số!");
        $(elem).val('')
    }
}

function isNumber(n) { return /^-?[\d.]+(?:e-?\d+)?$/.test(n); }

function number_format(number, decimals, dec_point, thousands_sep) {
    decimals = parseFloat(decimals);
    var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
    var d = dec_point == undefined ? "," : dec_point;
    var t = thousands_sep == undefined ? "." : thousands_sep, s = n < 0 ? "-" : "";
    var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

jQuery(document).ready(function ($) {

    // $('#order-form').on('focus', '#product-id-input', function(){
    // 	$(this).next().dropdown('update');
    // })

    // Check all row intable list
    $('body').on('change', '[scope="check-all"] input[name="select_all"]', function () {
        $('[scope="select-this"] input[name="select_this"]').prop('checked', $(this).is(':checked')).change();
    })
    $('body').on('change', '[scope="select-this"] input[name="select_this"]', function () {
        $('[scope="check-all"] .dropdown>.text-all').remove();
        if (!$(this).is(":checked")) {
            $('[scope="check-all"] input[name="select_all"]').prop('checked', false);
        }
        var _length = $('[scope="select-this"] input[name="select_this"]:checked').length;
        $('[scope="check-all"] .text').hide().empty();
        if (_length) {
            $('[scope="check-all"] .dropdown').addClass('has-selected');
            $(this).closest('table').addClass('has-selected');
            //update hidden chose all
            $('#hiddenSelectAll').addClass('hidden-chose-all');
            $('[scope="check-all"] .text').show();
            if (_length == $('[scope="select-this"] input[name="select_this"]').length) {
                $('[scope="check-all"] input[name="select_all"]').prop('checked', true);
                $('[scope="check-all"] .dropdown').append('<span class="text-all">' + $('[scope="check-all"] input[name="select_all"]').attr('data-text-language-choose-all') + '</span>');
            }
            $('[scope="check-all"] .text').append($('[scope="check-all"] input[name="select_all"]').attr('data-text-language-choose') + ' ' + _length + ' ' + $('[scope="check-all"] input[name="select_all"]').attr('data-text-language-item'));
        } else {
            $('[scope="check-all"] .dropdown').removeClass('has-selected');
            $(this).closest('table').removeClass('has-selected');
            //update hidden chose all
            $('#hiddenSelectAll').removeClass('hidden-chose-all');
        }
    });

    if ($('.repeater-weight').length) {
        $('.repeater-weight').repeater({
            // defaultValues: {
            // 	'weight-from': $(".weight_to").last().val(),
            // },
            show: function () {
                var prev = $(this).prev();
                // console.log($(this));
                // console.log(prev.find('.weight_from').val());
                // console.log(prev.find('.weight_to').val());
                if (prev.find('.weight_to').val() == '' || prev.find('.weight_to').val() < 1 || (parseFloat(prev.find('.weight_from').val().replace(new RegExp(',', 'g'), '')) >= parseFloat(prev.find('.weight_to').val().replace(new RegExp(',', 'g'), '')))) {
                    var message = $('.repeater-weight').attr('data-message');
                    var element_alert = $(this).prev().find('.weight_to').closest('.col-6.weight-wrap');
                    if (!element_alert.find('.text-danger.error-1').length) {
                        element_alert.append('<div class="text-danger error-1" style="margin-top: 5px; overflow: hidden; width: 100%">' + message + '</div>');
                    }
                    $(this).remove();
                } else {
                    $(this).prev().find('.weight-wrap .text-danger.error-1').remove();
                    $(this).slideDown();
                }
                $(this).find('.weight_from').val(prev.find('.weight_to').val());
            },
            hide: function (deleteElement) {
                // if(confirm('Are you sure you want to delete this element?')) {
                $(this).next().find('.weight_from').val($(this).prev().find('.weight_to').val());
                $(this).slideUp(deleteElement);
                // }
            },
            // isFirstItemUndeletable: true,
        });
    }

    if ($('.repeater').length) {
        $('.repeater').repeater({
            // defaultValues: {
            // 	'weight-from': $(".weight_to").last().val(),
            // },
            repeaters: [{
                // Specify the jQuery selector for this nested repeater
                selector: '.repeater-price'
            }],
            show: function () {
                // console.log(this);
                var prev = $(this).prev();
                // console.log(prev);
                // console.log(prev.find('.price_from').val());
                // console.log(prev.find('.price_to').val());
                if (prev.find('.price_to').val() == '' || prev.find('.price_to').val() < 1 || (parseFloat(prev.find('.price_from').val().replace(new RegExp(',', 'g'), '')) >= parseFloat(prev.find('.price_to').val().replace(new RegExp(',', 'g'), '')))) {
                    var message = $('.repeater').attr('data-message');
                    var element_alert = $(this).prev().find('.price_to').closest('.col-6.weight-wrap');
                    if (!element_alert.find('.text-danger.error-1').length) {
                        element_alert.append('<div class="text-danger error-1" style="margin-top: 5px; overflow: hidden; width: 100%">' + message + '</div>');
                    }
                    $(this).remove();
                } else {
                    $(this).prev().find('.weight-wrap .text-danger.error-1').remove();
                    $(this).slideDown();
                }
                $(this).find('.price_from').val(prev.find('.price_to').val());
            },
            hide: function (deleteElement) {
                // if(confirm('Are you sure you want to delete this element?')) {
                $(this).next().find('.price_from').val($(this).prev().find('.price_to').val());
                $(this).slideUp(deleteElement);
                // }
            },
            // isFirstItemUndeletable: true,
        });
    }


    if ($('.message_total_order').length) {
        $('.message_total_order').repeater({
            show: function () {
                var prev = $(this).prev();
                var next = $(this);
                if (prev.find('.price_order').val() == '' || prev.find('.price_order').val() < 1 ) {
                    var message = $('.repeater').attr('data-message');
                    var element_alert = $(this).prev().find('.price_order').closest('.col-6.weight-wrap');
                    if (!element_alert.find('.text-danger.error-1').length) {
                        element_alert.append('<div class="text-danger error-1" style="margin-top: 5px; overflow: hidden; width: 100%">' + message + '</div>');
                    }
                    $(this).remove();
                } else {
                    $(this).prev().find('.weight-wrap .text-danger.error-1').remove();
                    $(this).slideDown();
                }
                $(this).find('.price_order').val(prev.find('.price_order').val());
            },
            hide: function (deleteElement) {

            //     if(confirm('Are you sure you want to delete this element?')) {
                $(this).next().find('.price_order').val($(this).prev().find('.price_order').val());
                $(this).slideUp(deleteElement);
                }
            // },
            // isFirstItemUndeletable: true,
        });
    }

    if ($('.repeater-category_product').length) {
        $('.repeater-category_product').repeater({
            // defaultValues: {
            // 	'weight-from': $(".weight_to").last().val(),
            // },
            repeaters: [{
                // Specify the jQuery selector for this nested repeater
                selector: '.repeater-price'
            }],
            show: function () {
                var prev = $(this).prev();

                var quantity_from = prev.find('.quantity_from').val();
                quantity_from = quantity_from ? parseFloat(quantity_from.replace(new RegExp(',', 'g'), '')) : 0;

                var quantity_to = prev.find('.quantity_to').val();
                quantity_to = quantity_to ? parseFloat(quantity_to.replace(new RegExp(',', 'g'), '')) : 0;

                if (quantity_to < 1 || (quantity_from >= quantity_to)) {
                    var message = $('.repeater').attr('data-message');
                    var element_alert = $(this).prev().find('.price_to').closest('.col-6.weight-wrap');
                    if (!element_alert.find('.text-danger.error-1').length) {
                        element_alert.append('<div class="text-danger error-1" style="margin-top: 5px; overflow: hidden; width: 100%">' + message + '</div>');
                    }
                    $(this).remove();
                } else {
                    $(this).prev().find('.weight-wrap .text-danger.error-1').remove();
                    $(this).slideDown();
                }
                $(this).find('.quantity_from').val(quantity_to);
            },
            hide: function (deleteElement) {
                // if(confirm('Are you sure you want to delete this element?')) {
                $(this).next().find('.quantity_from').val($(this).prev().find('.quantity_to').val());
                $(this).slideUp(deleteElement);
                // }
            },
            // isFirstItemUndeletable: true,
        });
    }

    if ($('.repeater-manufacturer').length) {
        $('.repeater-manufacturer').repeater({
            // defaultValues: {
            // 	'weight-from': $(".weight_to").last().val(),
            // },
            repeaters: [{
                // Specify the jQuery selector for this nested repeater
                selector: '.repeater-price'
            }],
            show: function () {
                var prev = $(this).prev();
                if (prev.find('.quantity_to').val() == '' || prev.find('.quantity_to').val() < 1 || (parseFloat(prev.find('.quantity_from').val().replace(new RegExp(',', 'g'), '')) >= parseFloat(prev.find('.quantity_to').val().replace(new RegExp(',', 'g'), '')))) {
                    var message = $('.repeater').attr('data-message');
                    var element_alert = $(this).prev().find('.price_to').closest('.col-6.weight-wrap');
                    if (!element_alert.find('.text-danger.error-1').length) {
                        element_alert.append('<div class="text-danger error-1" style="margin-top: 5px; overflow: hidden; width: 100%">' + message + '</div>');
                    }
                    $(this).remove();
                } else {
                    $(this).prev().find('.weight-wrap .text-danger.error-1').remove();
                    $(this).slideDown();
                }
                $(this).find('.quantity_from').val(prev.find('.quantity_to').val());
            },
            hide: function (deleteElement) {
                // if(confirm('Are you sure you want to delete this element?')) {
                $(this).next().find('.quantity_from').val($(this).prev().find('.quantity_to').val());
                $(this).slideUp(deleteElement);
                // }
            },
            // isFirstItemUndeletable: true,
        });
    }

    if ($('select.select2').length) {
        $('select.select2').each(function (index, el) {
            $(this).wrap("<div class='select-2-container position-relative" + ($(this).attr('data-tags') ? " select2-tag" : '') + "'></div>");
            $(this).css('width', '100%');
            $(this).after('<div class="select-2-wrapper ' + ($(this).attr('multiple') ? 'multiple' : 'single') + '"></div>');
            $(this).select2({
                theme: 'bootstrap4',
                dropdownParent: $(this).parent().find('>.select-2-wrapper'),
                placeholder: $(this).attr('data-placeholder'),
                tags: $(this).attr('data-tags'),
                minimumResultsForSearch: ($(this).attr('data-search') == "true") ? 1 : Infinity,
                closeOnSelect: ($(this).attr('multiple') && !$(this).attr('data-tags') ? false : true)
            });
        });

        // Chọn tag bài viết
        $('#tag-section .select2').on('change', function (e) {
            $(this).closest('#tag-section').find('.remove-tag').addClass('d-none');
            if ($(this).val().length) {
                $(this).closest('#tag-section').find('.remove-tag').removeClass('d-none');
            }
        })
        $('#tag-section .remove-tag').on('click', function (e) {
            $(this).closest('#tag-section').find('.select2').val(null).trigger('change');
            return false;
        })
    }

    $(document).scroll(function (e) {
        if ($('.fix-on-scroll').length === 0) return;
        var elementOffsetTop = $('.fix-on-scroll').offset().top;
        var windowScrollTop = (window.pageYOffset || document.scrollTop) - (document.clientTop || 0) + 80;
        if (windowScrollTop >= elementOffsetTop) {
            $('.fix-on-scroll>.fix-content').addClass('fixed-top');
        } else {
            $('.fix-on-scroll>.fix-content.fixed-top').removeClass('fixed-top');
        }
    })

    // Check all permission in setting account page
    $('input#checkAllPermission').change(function () {
        $('.permission-list [type="checkbox"]').prop('checked', false);
        if ($(this).prop('checked')) {
            $('.permission-list [type="checkbox"]').prop('checked', true);
        }
    })

    // Chọn hình thức phí thu hộ - trang cài đặt vận chuyển
    $(document).on('change', '#choose-delivery-payment input[type="radio"]', function () {
        $(this).closest('#choose-delivery-payment').find('input[type="text"]').attr('disabled', 'disabled');
        if ($(this).is(":checked")) {
            $(this).closest('.payment-type').find('input[type="text"]').removeAttr('disabled');
        }
    })

    // Xem trước kết quả tìm kiếm - tùy chỉnh SEO - thêm mới blog, content, category
    $('#collapseSeoInput').on('input', 'input, textarea', function () {
        var name = $(this).closest('#collapseSeoInput').find('input#name').val();
        var body = $(this).closest('#collapseSeoInput').find('textarea#body').val();
        var alias = $(this).closest('#collapseSeoInput').find('input#alias').val();
        console.log(name, body, alias);
        $('.seo-preview #title').text(name);
        $('.seo-preview #alias>span').text(alias);
        $('.seo-preview #description').text(body);

        var url = $('.seo-preview #alias').text();
        $('.seo-preview #alias').attr('href', url);
        $('.seo-preview #title').attr('href', url);
    })

    // Cập nhật số lượng sản phẩm trong kho
    $('body').on('input', '.modal#updateQuantity .quantity-warehouse-product input', function () {
        var val = $(this).closest('.quantity-warehouse-product').find('input#quantity-value').val();
        val = $.trim(val) === '' ? 0 : parseInt(val);
        var type = $(this).closest('.quantity-warehouse-product').find('[name="add_type_quantity"]:checked').val();
        if (val > 0 || type !== 'add') {
            if (true || val > 0) { /* allow quantity negative in warehouse */
                $('#list-product-add-quantity-modal table tr').each(function () {
                    $(this).find('.new-val').remove();
                    $(this).find('.old-val').parent().append('<span class="new-val"><span></span></span>');
                    var old_val = parseInt($(this).find('.old-val').text());
                    var new_val = val + (type === "add" ? old_val : 0);
                    $(this).find('.new-val>span').text(new_val);
                })
            }
        } else {
            $(this).find('.new-val').remove();
            $('#list-product-add-quantity-modal table tr').each(function () {
                $(this).find('.new-val').remove();
            })
        }
    })

    // Cập nhật kho hàng cho từng sản phẩm trong trang danh sách sản phẩm trong kho hàng
    $('body').on('input', 'table#list-product-warehouse .quantity-warehouse-product input', function () {
        var val = parseInt($(this).closest('.quantity-warehouse-product').find('input#quantity-value').val().replace(/(?!-)[^0-9]/g, ""));
        val = isNaN(val) ? 0 : val;
        var type = $(this).closest('.quantity-warehouse-product').find('[type="radio"]:checked').val();
        if (true || val > 0) { /* allow quantity negative in warehouse */
            $(this).closest('tr').find('td.quantity-col .new-val').remove();
            $(this).closest('tr').find('td.quantity-col .old-val').parent().append('<span class="new-val"><span></span></span>');
            var old_val = parseInt($(this).closest('tr').find('td.quantity-col .old-val').text());
            var new_val = val + (type === "add" ? old_val : 0);
            $(this).closest('tr').find('td.quantity-col .new-val>span').text(new_val);
        }
    })


    /// Number format
    $('input.number-format').each(function () {
        var val = Number($(this).val().replace(/(?!-)[^0-9]/g, ""));
        $(this).val(number_format(val, 0, ',', '.'));
    })
    $(document).on('input', 'input.number-format', function (e) {
        var val = Number($(this).val().replace(/(?!-)[^0-9]/g, ""));
        $(this).val(number_format(val, 0, ',', '.'));
    })

    if ($('.date-picker').length) {
        $('.date-picker').datetimepicker({
            format: 'l',
            locale: 'vi',
            // inline: true,
        });

    }

    if ($('.time-picker').length) {
        $('.time-picker').datetimepicker({
            format: 'LT',
            locale: 'vi',
            // inline: true,
        });
    }

    // Form validate
    if ($('form.form-validate').length) {
        $('form.form-validate').validate({
            // focusCleanup: true,
            // focusInvalid: true,
            highlight: function (element, errorClass, validClass) {
                $(element).addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    }

});