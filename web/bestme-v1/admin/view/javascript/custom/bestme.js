jQuery(document).ready(function($) {
	$('[scope="check-all"] .dropdown-item#copy').on('click', function(){
		var url = $(this).attr('href');
		var selected = $(this).closest('[scope="check-all"]').find('input[name="selected"]').val();
		if (selected === '') {
			return false;
		};
		selected = JSON.parse(selected);
		console.log(selected);
		if (confirm(copy_confirm_common) && selected.length) {
			$.ajax({
			  url: url,
			  method: 'POST',
			  data:{
			    selected:selected,
			  },
                beforeSend: function(){
                    $('#image-loading').show();
                },
			  success: function(json) {
                  $('#image-loading').hide();
			  	console.log(json);
			    if (json.status === true) {
			      location.reload();
			    } else {
			      alert(json.message);
			    }
			  }
			});
		};
		return false;
	});

    /// Number format
    $('input.number-format-2').each(function(){
        var val = Number($(this).val().replace(/(?!-)[^0-9]/g, ""));
        $(this).val(number_format(val, 0, '', ''));
    });

    $(document).on('input', 'input.number-format-2', function(e){
        var val = Number($(this).val().replace(/(?!-)[^0-9]/g, ""));
        $(this).val(number_format(val, 0, '', ''));
    });

    $('input.number-format-3').each(function(){
        var val = Number($(this).val().replace(/(?!-)[^0-9]/g, ""));
        $(this).val(number_format(val, 0, '.', ','));
    });

    $(document).on('input', 'input.number-format-3', function(e){
        var val = Number($(this).val().replace(/(?!-)[^0-9]/g, ""));
        $(this).val(number_format(val, 0, '.', ','));
    });

    $('input.number-format-4').each(function(){
        var val = parseFloatWithDefaultValue($(this).val(), "");
        if (val !== '') {
            $(this).val(number_format(val, 0, '.', ','));
        } else {
            $(this).val('');
        }
    });

    $(document).on('input', 'input.number-format-4', function(e){
        var val = parseFloatWithDefaultValue($(this).val(), "");
        if (val !== '') {
            $(this).val(number_format(val, 0, '.', ','));
        } else {
            $(this).val('');
        }
    });

    $('input.number-max-7').each(function(){
        var val = $(this).val().replace(/(?!-)[^0-9]/g, "");
        if (val == '-' || val == ''){
            $(this).val(val);
        }else{
            var val = Number($(this).val().replace(/(?!-)[^0-9]/g, ""));
            $(this).val(number_format(val, 0, '', ''));
        }
    });

    $(document).on('input', 'input.number-max-7', function(e){
        var val = $(this).val().replace(/(?!-)[^0-9]/g, "");
        if (val == '-' || val == ''){
            $(this).val(val);
        }else{
            var val = Number($(this).val().replace(/(?!-)[^0-9]/g, ""));
            $(this).val(number_format(val, 0, '', ''));
        }
    });

    // Xem trước kết quả tìm kiếm - tùy chỉnh SEO - thêm mới blog, content, category
    $('#collapseSeoInput').on('input', 'input, textarea', function(){
        var name = $(this).closest('#collapseSeoInput').find('input#seo_title').val();
        var body = $(this).closest('#collapseSeoInput').find('textarea#body').val();
        var alias = $(this).closest('#collapseSeoInput').find('input#alias').val();
        console.log(name, body, alias);
        $('.seo-preview #title').text(name);
        $('.seo-preview #alias>span').text(alias);
        $('.seo-preview #seo_desciption').text(body);

        var url = $('.seo-preview #alias').text();
        $('.seo-preview #alias').attr('href', url);
        $('.seo-preview #title').attr('href', url);
    });
    $('body').on('click', '.page-link', function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        if (typeof href != 'undefined') {
            $('.alert.alert-success').remove();
            $('.alert.alert-danger').remove();
        }
    });
});

function extract_number(str) {
    return str.replace(/,/g, '');
}

function parseFloatWithDefaultValue(str, defaultValue = 0) {
    let result = parseFloat(str.replace(/,/g, ''));

    return isNaN(result) ? defaultValue : result;
}

function parseIntWithDefaultValue(str, defaultValue = 0) {
    let result = parseInt(str.replace(/,/g, ''));

    return isNaN(result) ? defaultValue : result;
}