jQuery(document).ready(function ($) {

    // Dropdown filter options
    $('.filter-form .search-group .dropdown-toggle').on('click', function (e) {
        $(this).parent().find('.dropdown-menu').toggleClass('show');
    });

    // Hide dropdown if click outside
    $('body').on('click', function (e) {
        if (!$('.filter-form .search-group').is(e.target)
            && $('.filter-form .search-group').has(e.target).length === 0
            && $('.show').has(e.target).length === 0
        ) {
            $('.filter-form .search-group .dropdown-menu').removeClass('show');
        }
    });

    var val;
    var input_name;
    var input_label;
    var input_value;
    var container;

    $('#filter_button').on('click', function () {
        var filter_total = $('input[name="filter_total_value"]');
        var filter_tag = $('input[name="filter_tag"]');
        var filter_utm = $('input[name="filter_utm"]');
        var amounttype = $('.filter-form select[name="amounttype"]');

        var date = $('.date-picker');
        var amount = $('#textamount');

        if (amounttype.length && !filter_total.hasClass('d-none') && amounttype.val() == '') {
            $('.filter-form #showoptionone .select2-container--bootstrap4 .select2-selection').css('border', '1px solid red');
            return;
        } else {
            $('.filter-form #showoptionone .select2-container--bootstrap4 .select2-selection').css('border', '1px solid #ced4da');
        }

        if (amount.length && amount.val() != '') {
            var type = $('.amountoption').val();
            var price = amount.val();
            val = type + "_" + price;
            if (type == 1) {
                input_value = '> ' + price;
            }
            if (type == 2) {
                input_value = '< ' + price;
            }
            if (type == 3) {
                input_value = '= ' + price;
            }
        }

        if (amount.length && amount.val() == '') {
            $('.filter-form input[name^=textamount]').css('border', '1px solid red');
            return;
        }

        // validate start date and end date
        var enable_filter_start = true;
        var enable_filter_end = true;
        if (!$('.filter_created_at_value').hasClass('d-none')) {
            if ($('.filter-form input[name^=filter_created_at_from]').val() == '') {
                $('.filter-form input[name^=filter_created_at_from]').css('border', '1px solid red');
                enable_filter_start = false;
                return;
            }

            if ($('.filter-form input[name^=filter_created_at_to]').val() == '') {
                $('.filter-form input[name^=filter_created_at_to]').css('border', '1px solid red');
                enable_filter_start = false;
                return;
            }
        }

        if (!enable_filter_start && !enable_filter_end) {
            return;
        }

        if (filter_total.length && filter_total.val() != '') {
            var number = filter_total.val();
            var type = $('#filter_total_type').val();
            val = type + "_" + number;
            input_value = type + " " + number;
        }

        if (filter_total.length && !filter_total.hasClass('d-none') && filter_total.val() == '') {
            $('.filter-form input[name^=filter_total_value]').css('border', '1px solid red');
            return;
        }

        // Filter by tag
        if (filter_tag.length && !filter_tag.parent().hasClass('d-none') && filter_tag.val().trim() == '') {
            $('.filter-form input[name^=filter_tag]').css('border', '1px solid red');
            return;
        }

        // Filter by tag
        if (filter_utm.length && !filter_utm.parent().hasClass('d-none') && filter_utm.val().trim() == '') {
            $('.filter-form input[name^=filter_utm]').css('border', '1px solid red');
            return;
        }

        var isStoreReceiptDateFilter = false;
        var storeReceiptDateFilterStartDate = '';
        var storeReceiptDateFilterEndDate = '';
        if (date.length && date.val() != '') {
            if (date.length > 1) {
                $.each(date, function (idx) {
                    if($('.filter_exported_at_value').length > 0 || $('.filter_imported_at_value').length > 0){
                        if($('.filter_exported_at_value').length > 0){
                            if ($(this).hasClass('date-picker-store-receipt-filter') && !$('.filter_exported_at_value').hasClass('d-none')) {
                                isStoreReceiptDateFilter = true;
                            }
                        }
                        if($('.filter_imported_at_value').length > 0){
                            if ($(this).hasClass('date-picker-store-receipt-filter') && !$('.filter_imported_at_value').hasClass('d-none')) {
                                isStoreReceiptDateFilter = true;
                            }
                        }

                    }else{
                        if ($(this).hasClass('date-picker-store-receipt-filter') && !$('.filter_created_at_value').hasClass('d-none')) {
                            isStoreReceiptDateFilter = true;
                        }
                    }
                    var value = $(this).val();
                    var name = $(this).attr('name');
                    if (name === 'filter_created_at_from') {
                        storeReceiptDateFilterStartDate = value;
                    }

                    if (name === 'filter_created_at_to') {
                        storeReceiptDateFilterEndDate = value;
                    }

                    if (name === 'filter_exported_at_from') {
                        storeReceiptDateFilterStartDate = value;
                    }

                    if (name === 'filter_exported_at_to') {
                        storeReceiptDateFilterEndDate = value;
                    }

                    if (name === 'filter_imported_at_from') {
                        storeReceiptDateFilterStartDate = value;
                    }

                    if (name === 'filter_imported_at_to') {
                        storeReceiptDateFilterEndDate = value;
                    }
                });
            }
            else {
                var date_val = $('.date-picker').val();
                var type = $('#filter_updated_time_type').val();
                val = type + "_" + date_val;
                input_value = type + " " + date_val;
            }
        }

        if (date.length && !date.parent().hasClass('d-none') && date.val() == '') {
            $('.filter-form input[name^=filter_updated_time_value]').css('border', '1px solid red');
            return;
        }

        if (input_value != undefined && val !== "" && (input_label != '' && input_value != '') && $('input[data-element="' + input_name + val + "[]" + '"]').length < 1) {

            if (input_name == 'filter_total_type' && type != '=') {
                var text_search = input_name + type;
                console.log(text_search,'text_search');
                container.find('input[data-element^="' + text_search + '"]').parent().remove();
            }

            if (input_name == 'filter_phone') {
                var text_search = input_name;
                container.find('input[data-element^="' + text_search + '"]').parent().remove();
            }

            if (input_name == 'filter_updated_time_type') {
                var text_search = input_name + type;
                container.find('input[data-element^="' + text_search + '"]').parent().remove();
            }

            if (input_name == 'filter_created_at_type') {
                container.find('input[data-element^="' + input_name + '"]').parent().remove();
            }

            if (input_name == 'filter_exported_at_type') {
                container.find('input[data-element^="' + input_name + '"]').parent().remove();
            }

            if (input_name == 'filter_imported_at_type') {
                container.find('input[data-element^="' + input_name + '"]').parent().remove();
            }

            if (isStoreReceiptDateFilter) {
                var created_at_option = storeReceiptDateFilterStartDate + ' - ' + storeReceiptDateFilterEndDate;
                container.append('\
				<div class="item bg-light p-2 rounded-3 mr-2 my-2 d-inline-block" data-id="' + input_name + '" id="' + input_name + '">\
				' + input_label + ': ' + created_at_option + ' <input type="hidden" data-element="' + input_name + '" name="' + input_name + '" value="' + created_at_option + '"> <a href="javascript:void(0)" class="remove-item font-20 lineheight-1 ml-3" aria-hidden="true">&times;</a>\
				</div>\
				');
            } else {
                container.append('\
				<div class="item bg-light p-2 rounded-3 mr-2 my-2 d-inline-block" data-id="' + input_name + '" id="' + input_name + '">\
				' + input_label + ': ' + input_value + ' <input type="hidden" data-element="' + input_name + val + '[]" name="' + input_name + '[]" value="' + val + '"> <a href="javascript:void(0)" class="remove-item font-20 lineheight-1 ml-3" aria-hidden="true">&times;</a>\
				</div>\
				');
            }
        }
        filter(1,false);
    });

    // show filter name item
    $('.filter-form .search-group .dropdown-menu').on('change', 'input, select', function (e) {
        if (!$(this).is('input') ||
            $(this).attr('name') == 'nametag' ||
            $(this).attr('name') == 'filter_tag' ||
            $(this).attr('name') == 'filter_status' ||
            $(this).attr('name') == 'filter_phone' ||
            $(this).attr('name') == 'filter_utm') {
            val = $(this).val();
            input_name = $(this).attr('id');
            input_label = $(this).attr('data-label');
            input_value = $(this).attr('data-value');

        }

        container = $(this).closest('form').find('.filter-item');

        // already moved to above. TODO: remove?...
        if ($(this).is('select')) {
            input_value = $(this).find("option[value='" + val + "']").attr("data-value");
        }
        if ($(this).is('input')) {
            if ($(this).attr('name') == 'filter_status') {
                input_value = $(this).attr('data-value');
            } else {
                input_value = val;
            }
        }
        if (typeof input_value == 'undefined' | typeof input_label == 'undefined') return;

        container.find('.item#' + input_name).each(function (index) {
            if (!$(this).hasClass('had_filter')) {
                //$(this).remove();
            }
        });
    });

    // hierarchical select
    $('.filter-form .search-group .dropdown-menu').on('change', 'select#hierarchical-select', function (e) {
        // TODO not remove when select
        // $(this).closest('form').find('.filter-item').empty();
        $('.hierarchical-child input, .hierarchical-child select').val('').change();
        $('.hierarchical-child').addClass('d-none');
        var val = $(this).val();
        $(val).removeClass('d-none');
    });


    // Remove a filter item
    $('.filter-form .filter-item').on('click', '.item .remove-item', function () {
        var id = $(this).closest('.item').attr("data-id");

        $(this).closest('form').find('select[name="' + id + '"]').val("").change();
        $(this).closest('form').find('select[name="' + id + '[]"]').val("").change();
        $(this).closest('form').find('input[name="' + id + '"]').prop('checked', false);
        $(this).closest('form').find('input[value=""]').prop('checked', true).change();
        $(this).closest('.item').remove();
        filter(1, true);
    });


});