jQuery(document).ready(function($) {

	$('#add-domain-modal').on('submit', function(e){
		$('#add-domain-modal').modal('hide');
		$('.toast .toast-body').empty().append('<i class="fas fa-check text-pink mr-2"></i> Lưu tên miền thành công!');
		setTimeout(function(){
			$('.toast').toast('show');
		}, 500);
		e.preventDefault();
	})

	$('#verify-domain-modal-fail').on('hidden.bs.modal', function(){
		$('.toast .toast-body').empty().append('<i class="fas fa-check text-pink mr-2"></i> Xác thực tên miền thành công!');
		$('.toast').toast('show');
	})

});