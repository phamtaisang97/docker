jQuery(document).ready(function($) {

	$('input[name="daterange"]').daterangepicker({
		opens: 'right',
		alwaysShowCalendars: true,
		maxDate: moment(),
		autoApply: true,
		locale:{
			format: "DD/MM/YYYY",
			separator: " - ",
			customRangeLabel: "Tùy chọn",
			"daysOfWeek": [
			"CN",
			"T2",
			"T3",
			"T4",
			"T5",
			"T6",
			"T7"
			],
			"monthNames": [
			"Tháng 1",
			"Tháng 2",
			"Tháng 3",
			"Tháng 4",
			"Tháng 5",
			"Tháng 6",
			"Tháng 7",
			"Tháng 8",
			"Tháng 9",
			"Tháng 10",
			"Tháng 11",
			"Tháng 12"
			],
			"firstDay": 1
		},
		ranges: {
			'Hôm nay': [moment(), moment()],
			'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			'1 tuần': [moment().subtract(1, 'weeks'), moment()],
			'Tuần này': [moment().startOf('week'), moment().endOf('week')],
			'1 tháng': [moment().subtract(1, 'months'), moment()],
			'Tháng này': [moment().startOf('month'), moment().endOf('month')],
		}
	}, function(start, end, label) {
		console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
	});

	Chart.defaults.global.legend.display = false;
	Chart.defaults.global.tooltips.backgroundColor = "#212B36";
	Chart.defaults.global.tooltips.xPadding = 10;
	Chart.defaults.global.tooltips.yPadding = 10;
	Chart.defaults.global.defaultFontFamily = "'Roboto', 'sans-serif'";
	var options = {
		scales: {
			yAxes: [{
				gridLines: {
					drawBorder: false,
					color: "#F4F6F8"
				},
				ticks: {
					min: 0,
					max: 50,
					stepSize: 10
				}
			}],
			xAxes: [{
				gridLines: {
					borderDash: [5,5],
				}
			}]
		}
	};
	var ctx_revenue = document.getElementById('revenue-chart').getContext('2d');
	var chart = new Chart(ctx_revenue, {
		type: 'line',
		data: {
			labels: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6"],
			datasets: [{
				label: "Doanh thu",
				data: [15, 10, 20, 18, 35, 50],
				fill: false,
				showLine: true,
				pointBorderWidth: 0,
				lineTension: 0,
				borderColor: "#B9256A",
				borderJoinStyle: "miter",
				pointRadius: 0,
				pointHitRadius: 10,
				pointHoverBackgroundColor: "#B9256A",
			}]
		},
		options: options
	});


	var ctx_order = document.getElementById('order-chart').getContext('2d');
	var chart = new Chart(ctx_order, {
		type: 'line',
		data: {
			labels: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6"],
			datasets: [{
				label: "Đơn hàng",
				data: [15, 30, 32, 46, 48, 50],
				fill: false,
				showLine: true,
				pointBorderWidth: 0,
				lineTension: 0,
				borderColor: "#B9256A",
				borderJoinStyle: "miter",
				pointRadius: 0,
				pointHitRadius: 10,
				pointHoverBackgroundColor: "#B9256A",
			}]
		},
		options: options
	});

	var ctx_customer = document.getElementById('customer-chart').getContext('2d');
	var chart = new Chart(ctx_customer, {
		type: 'line',
		data: {
			labels: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6"],
			datasets: [{
				label: "Đơn hàng",
				showLine: true,
				pointBorderWidth: 0,
				lineTension: 0,
				borderColor: "#B9256A",
				borderJoinStyle: "miter",
				pointRadius: 0,
				pointHitRadius: 10,
				pointHoverBackgroundColor: "#B9256A",
			}]
		},
		options: options
	});

	var ctx_customer = document.getElementById('best-sell-chart').getContext('2d');
	var chart = new Chart(ctx_customer, {
		type: 'bar',
		data: {
			labels: [['Tivi Sony màn', 'hình 29inchs'], ["iPhone 6","Gray - 32Gb"], ["Macbook Pro","2015 Retina"], ["Miband 3","Xiaomi"], ["Miband 2","Xiaomi"]],
			datasets: [{
				label: "Số lượng",
				data: [15, 30, 32, 46, 50],
				backgroundColor: '#B9256A',
				fill: false,
				showLine: true,
			}]
		},
		options: {
			responsive: true,
			scales: {
				yAxes: [{
					ticks: {
						min: 0,
						stepSize: 10,
					}
				}],
				xAxes: [{
					position: 'bottom',
					gridLines: {
						display: false,
					},
					ticks: {
						fontSize: 12,
						padding: 5,
						autoSkip: false,
						maxRotation: 0
					}
				}]
			},
		}
	});

});