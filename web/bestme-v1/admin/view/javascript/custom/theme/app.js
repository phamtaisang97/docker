jQuery(document).ready(function($) {
	
	// $('#order-form').on('focus', '#product-id-input', function(){
	// 	$(this).next().dropdown('update');
	// })

	$('[scope="check-all"] input[name="select_all"]').on('change', function(){
		$('[scope="select-this"] input[name="select_this"]').prop('checked', $(this).is(':checked'));
	})

	if( $('.repeater').length ){
		$('.repeater').repeater({
			show: function () {
                $(this).slideDown();
            },
            hide: function (deleteElement) {
                if(confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp(deleteElement);
                }
            },
            isFirstItemUndeletable: true,
		});
	}

});