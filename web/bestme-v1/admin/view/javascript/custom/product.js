jQuery(document).ready(function($) {


	$('.modal.product-modal').on('show.bs.modal', function(event){
		var title = $(event.relatedTarget).data('title');
		if( typeof title != "undefined" )
			$(this).find('.data-title').text(title);
	})

	if( $( ".sortable").length ){
		$( ".sortable tbody").sortable({
			placeholder: "bg-light",
			handle: ".sortable-button",
			stop: ( event, ui ) => {
				$(event.target).find('tr>td:nth-child(1)').each(function(e){
					var eq = $(this).parent().index();
					$(this).text((eq+1)+".");
				});
			}
		});
	}

	$('input[name="product_version"]~label').on('click', function(){
		var id = $(this).attr('data-tab');
		$(".nav-tabs a[href='#"+id+"']").tab('show');
	})

	$('form.product-form-validate').validate({
		// focusCleanup: true,
		// focusInvalid: true,
		highlight: function(element, errorClass, validClass) {
			$(element).addClass( errorClass ).removeClass( validClass );
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).removeClass( errorClass ).addClass( validClass );
		},
		errorPlacement: function(error,element) {
			return true;
		}
	});

})