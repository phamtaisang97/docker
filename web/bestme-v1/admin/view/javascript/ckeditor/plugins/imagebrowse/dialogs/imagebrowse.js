﻿CKEDITOR.dialog.add('imagebrowseDialog', function (editor) {
    return {
        title: editor.lang.imagebrowse.images,
        minWidth: 780,
        minHeight: 500,
        contents: [
            {
                id: 'tab-products',
                label: editor.lang.imagebrowse.images,
                elements: [
                    {
                        type: 'text',
                        id: 'selected-product-image',
                        style: 'display: none',
                        className: 'selected-product-image'
                    },
                    {
                        type: 'text',
                        id: 'selected-thumb-type-product-image',
                        style: 'display: none',
                        className: 'selected-thumb-type-product-image'
                    },
                    {
                        type: 'text',
                        id: 'selected-product-image-sizes',
                        style: 'display: none',
                        className: 'selected-product-image-sizes'
                    },
                    {
                        type: 'html',
                        html: '<a href="javascript: void(0);" class="cke-product-images"></a>'
                    }
                ]
            },
            {
                id: 'tab-image-url',
                label: editor.lang.imagebrowse.pathImage,
                elements: [
                    {
                        type: 'text',
                        id: 'image-url',
                        label: editor.lang.imagebrowse.pathImage
                    }
                ]
            }
            // ,
            // {
            //     id: 'tab-image-upload',
            //     label: "Tải lên",
            //     elements: [
            //         {
            //             type: 'file',
            //             id: 'image-file'
            //         },
            //         {
            //             type: 'select',
            //             id: 'thumb-type',
            //             className: 'thumb-type',
            //             label: 'Kích thước ảnh',
            //             labelStyle: 'font-weight: bold; margin-top: 15px;',
            //             items: [
            //                 ['Original', 'original'],
            //                 ['Pico (16x16)', 'pico'],
            //                 ['Icon (32x32)', 'icon'],
            //                 ['Thumb (50x50)', 'thumb'],
            //                 ['Small (100x100)', 'small'],
            //                 ['Compact (160x160)', 'compact'],
            //                 ['Medium (240x240)', 'medium'],
            //                 ['Large (480x480)', 'large'],
            //                 ['Grande (600x600)', 'grande'],
            //                 ['1024x1024 (1024x1024)', '1024x1024'],
            //                 ['2048x2048 (2048x2048)', '2048x2048'],
            //             ],
            //             'default': 'original'
            //         }
            //     ]
            // }
        ],
        onShow: function () {
            openChooseImageDialog();
        },
        onOk: function () {
            var dialog = this;
            var CurrObj = CKEDITOR.dialog.getCurrent();
            var currTab = CurrObj.definition.dialog._.currentTabId;
            var image = editor.document.createElement('input');

            if (currTab == "tab-products") {
                var ImageThumbSize = $('select.media-thumb-type').val();
                $('.list-upload').find('.chosen-image input.image-selected').each(function () {
                    var path = $(this).val();
                    if(ImageThumbSize !== 'original'){
                        path = cloudinary_resize(path, ImageThumbSize, ImageThumbSize);
                    }
                    var imgHtml = CKEDITOR.dom.element.createFromHtml('<img style="max-width: 100%;" src="' + path + '" />');
                    editor.insertElement(imgHtml);
                });
            } else if (currTab == "tab-image-url") {
                var src = this.getValueOf('tab-image-url', 'image-url');
                if (src != "") {
                    var imgHtml = CKEDITOR.dom.element.createFromHtml('<img style="max-width: 100%;" src="' + src + '" />');
                    editor.insertElement(imgHtml);
                }
            }
            // else if (currTab == "tab-image-upload") {
            //     var thumbType = this.getValueOf('tab-image-upload', 'thumb-type');
            //     var fileElement = this.getContentElement('tab-image-upload', 'image-file').getInputElement().$;
            //
            //     if (fileElement.files.length > 0) {
            //         var file = fileElement.files[0];
            //         if (file.size > 2098576) {
            //             alert("Kích thước file tối đa được upload là 2MB.");
            //             return false;
            //         }
            //         if (!file.name.toLowerCase().match(/\.(jpg|jpeg|png|gif)$/)) {
            //             alert("File upload không đúng định dạng.");
            //             return false;
            //         }
            //
            //         var formData = new FormData();
            //         formData.append("mediaImages", file);
            //         console.log(upload_image);
            //         $.ajax({
            //             type: "POST",
            //             url: "" + upload_image + "",
            //             data: formData,
            //             contentType: false,
            //             processData: false,
            //             success: function (response) {
            //                 if (Object.prototype.toString.call(response) === '[object Array]' && response.length > 0) {
            //                     image.setAttribute('src', changeSrcByThumbType(response[0].src, thumbType));
            //                     image.setAttribute('data-thumb', thumbType);
            //
            //                     if (response[0].width)
            //                         image.setAttribute('original-width', response[0].width);
            //
            //                     if (response[0].height)
            //                         image.setAttribute('original-height', response[0].height);
            //
            //                     editor.insertElement(image);
            //                     CKEDITOR.dialog.getCurrent().hide();
            //                 } else if (response.error) {
            //                     alert(response.error);
            //                 } else {
            //                     alert("Đã có lỗi xảy ra. Tải lên thất bại.")
            //                 }
            //             },
            //             error: function (error) {
            //                 alert("Đã có lỗi xảy ra. Tải lên thất bại.")
            //             }
            //         });
            //     }
            //
            //     return false;
            // }
        }
    };

    function openChooseImageDialog() {
        $.ajax({
            url: "" + filemanager + "",
            beforeSend: function () {
                $('#image-loading').show();
            },
            success: function (n) {
                $(".cke-product-images").html(n);
                $('#image-loading').hide();
            }
        })
    }

    function cloudinary_resize(url, width, height)
    {
        if (url.toLowerCase().indexOf("http://res.cloudinary.com") === 0 || url.toLowerCase().indexOf("https://res.cloudinary.com") === 0){
            var arrLink = url.split('/');
            var scale = 'c_fill_pad,g_auto,b_auto,w_' + width + ',h_' + height;
            arrLink.splice(arrLink.indexOf('upload') + 1, 0, scale);
            url = arrLink.join('/');
        }

        return url;
    }
});