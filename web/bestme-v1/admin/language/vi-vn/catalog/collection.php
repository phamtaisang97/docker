<?php
// Heading
$_['heading_title']               = 'Danh sách bộ sưu tập';
$_['heading_title_add']           = 'Thêm mới bộ sưu tập';
$_['heading_title_edit']          = 'Sửa bộ sưu tập';
$_['text_success']                = 'Thêm mới bộ sưu tập thành công';
$_['text_success_edit']           = 'Thay đổi bộ sưu tập thành công';

// breadcrumb
$_['breadcrumb_product']                 = 'Bộ sưu tập';
$_['page_title']                  = 'bộ sưu tập';
$_['breadcrumb_product_collection']      = 'Bộ sưu tập sản phẩm';
//
$_['collection_list_title']              = 'Danh sách';
$_['collection_list_filter']             = 'Lọc bộ sưu tập';
$_['breadcrumb_product_collection']      = 'Số lượng sản phẩm/bộ sưu tập';
$_['breadcrumb_status']                  = 'Hiển thị';
$_['breadcrumb_status_hide']             = 'Ẩn';

// entry
$_['entry_menu_placeholder']             = 'Chọn menu';

// filter
$_['collection_list_filter_collection_all']    = 'Tất cả';
$_['collection_list_filter_status']            = 'Trạng thái';
$_['collection_list_filter_action']            = 'Chọn thao tác';
$_['collection_list_filter_status_show']       = 'Hiện bộ sưu tập';
$_['collection_list_filter_status_disabled']   = 'Ẩn bộ sưu tập';
$_['collection_list_filter_status_delete']     = 'Xóa bộ sưu tập';
$_['filter_staff_placeholder']                 = 'Chọn nhân viên';

$_['delete_confirm']       = 'Bạn có chắc chắn muốn xóa';
$_['confirm_show']         = 'Bạn có chắc muốn thay đổi ';
$_['text_add']             = 'Thêm mới ';
$_['text_search']          = 'Lọc';
$_['text_search_list']          = 'Tìm kiếm bộ sưu tập';
$_['text_cancel']          = 'Bỏ qua ';
$_['text_save']            = 'Lưu thay đổi ';

$_['text_name']               = 'Tên bộ sưu tập ';
$_['entry_name_placeholder']  = 'Nhập tên bộ sưu tập';
$_['text_category']           = 'Danh mục';
$_['text_action']             = 'Hành động';
$_['text_creator']            = 'Người tạo';
$_['text_staff']              = 'Nhân viên';
$_['text_content']            = 'Nội dung bộ sưu tập ';
$_['text_set_colletion']      = 'Chọn sản phẩm vào bộ sưu tập';
$_['text_see_search']         = 'xem trước kết quả tìm kiếm';
$_['text_seo']                = 'Tùy chỉnh SEO';
$_['text_more']               = 'Nhập tiêu đề và mô tả để xem cách bài viết được hiển thị trên danh sách tìm kiếm.';
$_['text_tag_title']          = 'Thẻ tiêu đề';
$_['text_abouts']             = 'Thêm mô tả';
$_['text_alias']              = 'Đường dẫn / Alias';
$_['text_content_seo']        = 'Nội dung bài viết';
$_['text_image']              = 'Ảnh bộ sưu tập';
$_['text_add_image']          = 'Thêm hình ảnh';
$_['text_set_menu']           = 'Gắn vào menu';
$_['text_choose_menu']        = 'Chọn Menu';
$_['text_choose_menu_text']   = 'Bạn có thể thêm bộ sưu tập này vào một liên kết bằng cách sử dụng nút Chọn menu';
$_['text_used_text']          = 'Đã sử dụng:';
$_['text_used_char']          = 'ký tự';
$_['text_add_to_menu']        = 'Gắn vào menu';
$_['text_add_collection_to_menu']        = 'Bạn sẽ thêm bộ sưu tập vào các menu sau:';

$_['sort_by']             = 'Sắp xếp: ';
$_['sort_zero']           = 'Thủ công';
$_['sort_az']             = 'Theo tên: A-Z';
$_['sort_za']             = 'Theo tên: Z-A';
$_['sort_hightolow']      = 'Theo giá: Từ cao đến thấp';
$_['sort_lowtohigh']      = 'Theo giá: Từ thấp đến cao';
$_['sort_newtoold']       = 'Theo ngày tạo: từ mới đến cũ';
$_['sort_oldtonew']       = 'Theo ngày tạo: từ cũ đến mới';

// Filter
$_['filter_list_follow']      = "Lọc sản phẩm theo";
$_['choose_filter']           = "Chọn điều kiện lọc";
$_['filter_status']           = "Trạng thái hiển thị";
$_['filter_manufacture']      = "Nhà cung cấp";
$_['filter_category']         = "Loại sản phẩm";
$_['filter_tag']              = "Đã được tag với";
$_['filter_staff']            = "Nhân viên";
$_['filter_length_product']   = "Số lượng sản phẩm";
$_['filter_status']                              = "Trạng thái hiển thị";
$_['filter_status_placeholder']                  = "Chọn trạng thái";
$_['entry_status']           = 'TRẠNG THÁI';
$_['entry_status_publish_short']   = 'Hiện';
$_['entry_status_publish']   = 'Hiển thị';
$_['entry_status_unpublish'] = 'Ẩn';
// Select 2
$_['select2_notice_not_result']   =    "Không tìm thấy kết quả phù hợp";
$_['select2_notice_search']   =    "Tìm kiếm ...";
$_['select2_notice_load_more']   =    "Đang tải ...";

$_['select2_notice_not_result']   =    "Không tìm thấy kết quả phù hợp";
$_['select2_notice_search']   =    "Tìm kiếm ...";
$_['select2_notice_load_more']   =    "Đang tải ...";
$_['entry_collection_placeholder']   =    "Chọn sản phẩm vào bộ sưu tập";


$_['delete_collection_confirm_show']          = 'Bạn có chắc chắn muốn xóa bộ sưu tập không ?';
$_['error_max_length_100']             = 'Không vượt quá 100 ký tự';
$_['error_product']             = 'Bạn phải chọn ít nhất 1 sản phẩm';
$_['error_title_add']             = 'Tên bộ sưu tập không được để trống';
$_['error_text_char']           = 'Không được nhập ký tự đặc biệt';
$_['error_text_title']           = 'Thẻ tiêu đề không được để trống';
$_['error_text_check_colletion']           = 'Đã tồn tại bộ sưu tập';
$_['choose']                                     = "Đã chọn ";
$_['item']                                       = " mục";
$_['txt_selected_all']                           = "Đã chọn tất cả trong trang này";
$_['change_success']                 = "Thay đổi thành công.";
$_['delete_success']                 = "Xóa thành công.";
$_['add_collection_new'] = "+ Thêm BST";
$_['collection_new']     = "Bộ sưu tập sản phẩm";
$_['collection_filter_new']     = "Lọc";
$_['collection_count_product']     = "sản phẩm";
$_['collection_form_cancel_mobile']     = "Hủy";
$_['collection_form_save_mobile']     = "Lưu";
$_['collection_confirm_show']          = 'Bạn có chắc chắn muốn xóa ';
$_['collection_confirm_choose']        = " bộ sưu tập đã chọn ?";

$_['choose_product_to_collection']        = "Chọn sản phẩm vào bộ sưu tập";
$_['choose_collection_to_collection']        = "Chọn bộ sưu tập vào bộ sưu tập";
$_['placeholder_collection_to_collection']        = "Thêm bộ sưu tập vào bộ sưu tập";

$_['action_close']        = "Đóng";
$_['action_delete']       = "Xóa";

$_['text_contenttext_content']        = "Nhập nội dung";