<?php
// Heading
$_['heading_title']          = 'Sản phẩm';
$_['heading_title_add']      = 'Thêm sản phẩm';
$_['heading_title_edit']      = 'Sửa sản phẩm';

// Text
$_['text_success']           = 'Thành công: Bạn đã cập nhật thành công Sản phẩm!';
$_['text_success_add']       = 'Thêm sản phẩm mới thành công!';
$_['text_success_edit']      = 'Sửa sản phẩm thành công!';
$_['text_list']              = 'Danh sách Sản phẩm';
$_['text_add']               = 'Thêm sản phẩm';
$_['general_info']           = 'Thông tin chung';
$_['text_product_info']      = 'Thông tin sản phẩm';
$_['text_only_version']      = 'Sản phẩm có một phiên bản';
$_['text_multi_version']     = 'Sản phẩm có nhiều phiên bản';
$_['text_edit']              = 'Sửa sản phẩm';
$_['text_filter']            = 'Bộ lọc';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Mặc định';
$_['text_option']            = 'Tuỳ chọn';
$_['text_option_value']      = 'Giá trị tuỳ chọn';
$_['text_percent']           = 'Phần trăm';
$_['text_amount']            = 'Giá cố định';
$_['text_keyword']           = 'Không sử dụng dấu cách, thay vào sử dụng dấu - và đảm bảo SEO URL là duy nhất trong toàn hệ thống.';
$_['text_buy_when_out_of_stock']           = 'Cho phép tiếp tục đặt mua khi sản phẩm đã hết hàng';
$_['txt_image_preview']                    = 'Xem trước ảnh';
$_['txt_image_alt']                        = 'ALT của ảnh:';
$_['txt_image_set_thumb']                  = 'Đặt làm ảnh đại diện sản phẩm';
$_['txt_warning_import_product']           = 'Chú ý: các sản phẩm trùng sku hoặc trùng tên (không có sku) sẽ bị ghi đè!';
$_['txt_image_description']                = 'Để hình ảnh hiển thị đẹp nhất, vui lòng lựa chọn ảnh dạng hình vuông (kích thước gợi ý: 500x500 pixel)';
$_['text_creator']                         = 'Người tạo';
$_['text_staff']                           = 'Nhân viên';
// SEO
$_['text_seo_preview']       = 'Xem trước kết quả tìm kiếm';
$_['text_seo_config']        = 'Tùy chỉnh SEO';
$_['text_seo_guide']         = 'Nhập tiêu đề và mô tả để xem cách bài viết được hiển thị trên danh sách tìm kiếm.';
$_['text_seo_title_exp']     = '3 món quà Giáng sinh ý nghĩa nhất dành tặng trong mùa Noel 14/12/2017';
$_['text_seo_alias_exp']     = 'ba-mon-qua-y-nghia-nhat-danh-tang-ban-gai';
$_['text_seo_des_exp']       = 'Bước 1: Truy cập website và lựa chọn sản phẩm cần mua để mua hàng Bước 2: Click và sản phẩm muốn mua, màn hình hiển thị ra popup...';
$_['txt_seo_title']                            = 'Thẻ tiêu đề';
$_['txt_seo_title_place_holder']               = 'Nhập tiêu đề bài viết';
$_['txt_seo_description']                      = 'Nội dung bài viết';
$_['txt_seo_description_place_holder']         = 'Thêm mô tả';
$_['txt_seo_keywords_place_holder']             = 'Nhập từ khóa tại đây, mỗi từ khóa cách nhau bởi dấu phẩy';
$_['txt_seo_alias']                            = 'Đường dẫn / Alias';
$_['txt_characters']                       = 'ký tự';
$_['txt_used']                             = 'Đã sử dụng: ';
$_['txt_seo_image_alt']                        = 'Bổ sung thẻ ALT để tối ưu SEO';
$_['text_options']           = 'Chọn thao tác';
$_['text_show_product']      = 'Hiện sản phẩm';
$_['text_hide_product']      = 'Ẩn sản phẩm';
$_['text_delete_product']    = 'Xóa sản phẩm';
$_['text_add_tag']           = 'Thêm tag';
$_['text_remove_tag']        = 'Bỏ tag';
$_['text_add_collection']    = 'Thêm sản phẩm vào bộ sưu tập';
$_['text_delete_collection'] = 'Xóa sản phẩm khỏi bộ sưu tập';
$_['text_add_product_attribute'] = 'Thêm thuộc tính khác';
$_['text_show_gg_product_feed'] = 'Nguồn cấp dữ liệu sản phẩm google';

// Column
$_['column_name']            = 'Tên sản phẩm';
$_['column_model']           = 'Model';
$_['column_image']           = 'Ảnh sản phẩm';
$_['column_image_add']       = 'Thêm ảnh sản phẩm';
$_['column_price_title']     = 'GÍA CẢ';
$_['column_price']           = 'Giá bán lẻ';
$_['column_price_des']       = 'Là giá gốc của sản phẩm, sẽ bị gạch đi khi có giá sau khuyến mãi. Nhập giá trị 0 cho giá LIÊN HỆ';
$_['column_price_promotion'] = 'Giá sau KM';
$_['column_price_promotion_des'] = 'Giá bán cuối cùng của sản phẩm để khách hàng có thể mua được.';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';
$_['column_description']      = 'Mô tả chi tiết';
$_['column_sub_description']  = 'Mô tả ngắn';
$_['column_barcode']          = 'Mã vạch (ISBN, UPC, v.v...)';
$_['column_barcode_sort']     = 'Barcode';
$_['column_quantity']         = 'Số lượng nhập kho';
$_['column_display']          = 'Hiện';
$_['column_version']          = 'Phiên bản';

// Entry
$_['entry_name']             = 'Nhập tên sản phẩm';
$_['entry_description']      = 'Nhập nội dung';
$_['entry_meta_title']       = 'Tiêu đề thẻ Meta';
$_['entry_meta_keyword']     = 'Từ khoá thẻ Meta';
$_['entry_meta_description'] = 'Mô tả thẻ Meta';
$_['entry_store']            = 'Các cửa hàng';
$_['entry_quantity_placeholder']         = 'nhập số lượng';
$_['entry_keyword']          = 'Từ khoá';
$_['entry_model']            = 'Mẫu Sản phẩm';
$_['entry_sku']              = 'Mã sản phẩm/SKU';
$_['entry_sku_sort']         = 'SKU';
$_['entry_barcode']          = 'ISBN, UPC, v.v...';
$_['entry_upc']              = 'UPC';
$_['entry_ean']              = 'EAN';
$_['entry_jan']              = 'JAN';
$_['entry_isbn']             = 'ISBN';
$_['entry_mpn']              = 'MPN';
$_['entry_location']         = 'Địa chỉ';
$_['entry_shipping']         = 'Yêu cầu vận chuyển';
$_['entry_manufacturer']     = 'Nhà sản xuất';
$_['entry_date_available']   = 'Ngày còn hàng';
$_['entry_quantity']         = 'Số lượng';
$_['entry_minimum']          = 'Số lượng tối thiểu';
$_['entry_stock_status']     = 'Trạng thái hết hàng';
$_['entry_price']            = 'Nhập giá';
$_['entry_tax_class']        = 'Đơn vị tính Thuế';
$_['entry_points']           = 'Điểm';
$_['entry_option_points']    = 'Điểm';
$_['entry_subtract']         = 'Trừ số lượng trong kho';
$_['entry_weight_class']     = 'Đơn vị Khối lượng';
$_['entry_weight']           = 'Khối lượng sản phẩm';
$_['entry_weight_sub']       = '(Đơn vị gram)';
$_['entry_weight_des']       = 'Nhập khối lượng sản phẩm';
$_['entry_dimension']        = 'Kích thước (D x R x C)';
$_['entry_length_class']     = 'Đơn vị chiều dài';
$_['entry_length']           = 'Chiều dài';
$_['entry_width']            = 'Chiều rộng';
$_['entry_height']           = 'Chiều cao';
$_['entry_image']            = 'Ảnh';
$_['entry_additional_image'] = 'Ảnh bổ sung';
$_['entry_customer_group']   = 'Nhóm khách hàng';
$_['entry_date_start']       = 'Ngày bắt đầu';
$_['entry_date_end']         = 'Ngày kết thúc';
$_['entry_priority']         = 'Độ ưu tiên';
$_['entry_attribute']        = 'THUỘC TÍNH';
$_['entry_attribute_name']   = 'Tên thuộc tính';
$_['entry_attribute_des']    = 'Giá trị thuộc tính (Các giá trị cách nhau bới dấu phẩy)';
$_['entry_attribute_placeholder']    = 'Mỗi tag phân cách bằng dấu phẩy (,)';
$_['entry_attribute_value']  = 'Giá trị';
$_['entry_attribute_add']    = 'Thêm thuộc tính';
$_['entry_attribute_group']  = 'Nhóm thuộc tính';
$_['entry_text']             = 'Văn bản';
$_['entry_option']           = 'Tuỳ chọn';
$_['entry_option_value']     = 'Giá trị tuỳ chọn';
$_['entry_required']         = 'Yêu cầu';
$_['entry_status']           = 'TRẠNG THÁI';
$_['entry_status_publish_short']   = 'Hiện';
$_['entry_status_publish']   = 'Hiển thị';
$_['entry_status_unpublish'] = 'Ẩn';
$_['entry_stock_in_stock']      = 'Còn hàng';
$_['entry_status_out_of_stock'] = 'Hết hàng';
$_['entry_multi_versions_yes']  = 'Có';
$_['entry_multi_versions_no']   = 'Không';
$_['entry_sort_order']       = 'Sắp xếp';
$_['entry_category']         = 'Danh mục';
$_['entry_category_select']         = 'Chọn danh mục';
$_['entry_category_select_explain']         = 'Thêm sản phẩm vào danh mục để nó dễ dàng được tìm kiếm trên website của bạn.';
$_['entry_classify']         = 'Phân loại';
$_['entry_product_type']     = 'Loại sản phẩm';
$_['entry_collection']       = 'Thêm vào bộ sưu tập sản phẩm';
$_['entry_collection_placeholder']       = 'Tìm kiếm bộ sưu tập';
$_['entry_search_tag_placeholder']       = 'Tìm kiếm tag';
$_['entry_collection_selected']          = '• Bạn sẽ thêm sản phẩm vào các bộ sưu tập sau:';
$_['entry_tag']              = 'Gắn tag';
$_['entry_tag_placeholder']  = 'Nhập tag';
$_['entry_tag_des']          = 'Thêm sản phẩm vào danh mục để nó dễ dàng được tìm kiếm trên website của bạn.';
$_['entry_supplier']         = 'Nhà cung cấp';
$_['entry_supplier_input']         = 'Nhập nhà cung cấp';
$_['entry_filter']           = 'Bộ lọc';
$_['entry_download']         = 'Downloads';
$_['entry_related']          = 'Sản phẩm liên quan';
$_['entry_reward']           = 'Điểm thưởng';
$_['entry_layout']           = 'Ghi đè Layout';
$_['entry_recurring']        = 'Thông tin';
$_['entry_category_placeholder']       = 'Loại sản phẩm';
$_['entry_manufacturer_placeholder']       = 'Nhà cung cấp';

// Help
$_['help_sku']               = 'Stock Keeping Unit';
$_['help_upc']               = 'Universal Product Code';
$_['help_ean']               = 'European Article Number';
$_['help_jan']               = 'Japanese Article Number';
$_['help_isbn']              = 'International Standard Book Number';
$_['help_mpn']               = 'Manufacturer Part Number';
$_['help_manufacturer']      = '(Tự động)';
$_['help_minimum']           = 'Số lượng đặt hàng tối thiểu';
$_['help_stock_status']      = 'Trạng thái hiển thị khi hết hàng';
$_['help_points']            = 'Số điểm cần thiết để mua được mặt hàng này.';
$_['help_category']          = '(Tự động)';
$_['help_filter']            = '(Tự động)';
$_['help_download']          = '(Tự động)';
$_['help_related']           = '(Tự động)';
$_['help_tag']               = 'Phần cách bởi dấu ,';

// Error
$_['error_warning']          = 'Cảnh báo: Vui lòng kiểm tra kỹ form nội dung để biết lỗi!';
$_['error_permission']       = 'Cảnh báo: Bạn không có quyền chỉnh sửa Sản phẩm!';
$_['error_name']             = 'Tên sản phẩm quá ngắn hoặc quá dài (1-255 ký tự)!';
$_['error_meta_title']       = 'Tiêu đề thẻ Meta dài từ 1 đến 255 ký tự!';
$_['error_model']            = 'Mẫu Sản phẩm dài từ 1 đến 255 ký tự!';
$_['error_keyword']          = 'SEO URL đã được sử dụng!';
$_['error_unique']           = 'SEO URL phải là duy nhất!';
$_['error_price']            = 'Giá bán không được lớn hơn giá so sánh!';
$_['error_name_exist']             = 'Tên sản phẩm đã tồn tại!';
$_['error_sku_exist']             = 'SKU sản phẩm đã tồn tại!';
$_['error_product_version_limit']             = 'Số lượng phiên bản sản phẩm tối đa là 100.';
$_['error_product_attribute_name_exist']             = 'Tên thuộc tính đã tồn tại';
$_['error_has_discount']                             = 'Cảnh báo: sản phẩm đang nằm trong chương trình khuyến mại đang hoạt động';
$_['error_has_discount_fail']                        = 'Không thành công: sản phẩm đang nằm trong chương trình khuyến mại đang hoạt động';
$_['error_edit_error_when_has_discount']             = 'Cảnh báo: không cập nhật được sản phẩm đang nằm trong chương trình khuyến mại đang hoạt động';
$_['error_delete_error_when_has_discount']           = 'Cảnh báo: không xoá được sản phẩm đang nằm trong chương trình khuyến mại đang hoạt động';

// Error File
$_['error_permission'] = 'Cảnh báo: Yêu cầu bị từ chối do thẩm quyền!';
$_['error_filename']   = 'Cảnh báo: Tên tệp dài từ 3 đến 255!';
$_['error_folder']     = 'Cảnh báo: Tên thư mục dài từ 3 đến 255!';
$_['error_exists']     = 'Cảnh báo: Đã tồn tại thư mục!';
$_['error_directory']  = 'Cảnh báo: Thư mục chưa tồn tại!';
$_['error_filesize']   = 'Cảnh báo: Kích thước tệp không được quá 1MB!';
$_['error_filetype']   = 'Cảnh báo: Chỉ chấp nhận định dạng .xls và xlsx !';
$_['error_upload']     = 'Cảnh báo: Không thể tải tệp lên hệ thống (lỗi không xác định)!';
$_['error_delete']     = 'Cảnh báo: Không thể xoá thư mục này!';
$_['error_limit']      = 'Cảnh báo: Số sản phẩm và phiên bản sản phẩm import tối đa là 500!';
$_['error_not_found_fields']      = 'Cảnh báo: Không tìm thấy tiêu_đề hoặc mô_tả!';
$_['error_file_incomplete']       = 'Cảnh báo: File không đầy đủ thông tin (thiếu cột)!';
$_['error_file_incomplete_in_row']       = 'Cảnh báo: Thiếu tiêu_đề, giá, đo_lường_định_giá_theo_đơn_vị, hoặc giá ưu đãi lơn hơn giá bán tại dòng ';
$_['error_image_alts']       = 'Cảnh báo: Số lượng alt nhiều hơn số lượng ảnh tại dòng ';
$_['error_data_import_excel']       = 'Cảnh báo: Trường "giá_ưu_đãi" phải nhỏ hơn trường "giá" và "giá" không được nhỏ hơn 0 hoặc số lượng alt nhiều hơn số lượng ảnh tại dòng ';
$_['error_unknown']                      = 'Lỗi ko xác định';
$_['error_no_file_selected']             = 'Chưa có file nào được chọn!';

$_['error_input_null']                 = 'Vui lòng điền vào trường này';
$_['error_input_price_null']                 = 'Vui lòng nhập giá sản phẩm';
$_['error_input_product_name']         = 'Vui lòng nhập tên sản phẩm';
$_['error_type_mail']                  = 'Vui lòng nhập đúng định dạng email';
$_['error_max_length_255']             = 'Không vượt quá 255 ký tự';
$_['error_max_length_15']              = 'Không vượt quá 15 ký tự';
$_['error_max_length_12']              = 'Không vượt quá 10 ký tự';
$_['error_max_length_14']              = 'Không vượt quá 11 ký tự';
$_['error_type_phone']                 = 'Vui lòng nhập số cho trường này';
$_['error_max_length_10']              = 'Không vượt quá 10 ký tự';
$_['error_max_length_100']             = 'Không vượt quá 100 ký tự';
$_['error_max_length_30']             = 'Không vượt quá 30 ký tự';
$_['error_max_length_20']             = 'Không vượt quá 20 ký tự';
$_['error_max_sku_length_60']             = 'Không vượt quá 60 ký tự';
$_['error_max_length_48']             = 'Không vượt quá 48 ký tự';
$_['error_type_barcode']             = 'Chỉ cho phép nhập chữ và số';
$_['error_type_sku']             =   'Không nhập tiếng việt có dấu';
$_['error_type_weight']         = "Khối lượng sản phẩm lớn hơn hoặc bằng 1 gram và nhỏ hơn 9,999,999 gram";
$_['error_special_char']         = "Không nhập ký tự đặc biệt";
$_['error_type_sku_same']             =   'Mã SKU đã tồn tại';
$_['error_promotion_price']           = 'Giá khuyến mại phải nhỏ hơn giá bán lẻ';
$_['error_promotion_price2']           = 'Giá bán lẻ phải lớn hơn giá khuyến mãi';
$_['error_text_char']           = 'Không được nhập ký tự đặc biệt';

// Text file
$_['text_uploaded']    = 'Thành công!';
$_['text_directory']   = 'Thành cồng: Tạo thành công thư mục!';
$_['text_delete']      = 'Thành công: Tệp hoặc thư mục đã được xoá!';
$_['text_product_uploaded']      = ' sản phẩm đã được cập nhật.';


///text
$_['text_product_data']           = 'DỮ LIỆU SẢN PHẨM';
$_['text_depot_mamagement']       = 'Quản lý kho';
$_['text_input_number']           = 'Số lượng nhập';
$_['text_weight_explain']         = 'Được dùng để tính phí vận chuyển ở trang thanh toán';
$_['text_attribute_explain']      = 'Thêm mới thuộc tính giúp sản phẩm có nhiều lựa chọn, như kích cỡ hay màu sắc.';
$_['txt_product_title']           = 'Sản phẩm';

//Text product list
$_['product_heading']               = 'Danh sách sản phẩm';
$_['product_list']                  = 'Sản phẩm';
$_['product_category']              = 'Danh sách thư mục';
$_['product_import_file']           = 'Nhập danh sách';
$_['product_export_file']           = 'Xuất danh sách';
$_['product_code']                  = 'Mã sản phẩm';
$_['product_price']                 = 'Giá';
$_['product_quantity']              = 'Số lượng';
$_['product_status']                = 'Trạng thái';
$_['product_date_modified']         = 'Ngày cập nhật';
$_['product_action']                = 'Hành động';
$_['product_edit']                  = 'Sửa';
$_['product_view']                  = 'Xem';
$_['product_delete']                = 'Xoá sản phẩm';
$_['product_copy']                  = 'Copy';
$_['product_filter']                = 'Bộ lọc sản phẩm';
$_['product_type']                  = 'Loại';
$_['product_filter']                = 'Bộ lọc sản phẩm';
$_['product_search']                = 'Tìm kiếm sản phẩm';
$_['product_all_status']            = 'Tất cả';
$_['product_filter_button']         = 'Tìm kiếm';
$_['product_cancel_button']         = 'Huỷ';
$_['product_save_button']           = 'Xác nhận';
$_['product_delete_confirm']        = 'Bạn có muốn xoá ?';
$_['product_confirm_show']          = 'Bạn có chắc chắn muốn xóa ';
$_['product_confirm_choose']        = " sản phẩm đã chọn ?";
$_['product_text_list']             = 'sản phẩm';
$_['product_of_list']               = 'của';
$_['product_of_version']            = 'phiên bản';


$_['action_show_product']       = 'Hiện sản phẩm';
$_['action_hidden_product']       = 'Ẩn sản phẩm';
$_['action_add_tag_product']       = 'Thêm tag';
$_['action_remove_tag_product']       = 'Bỏ tag';
$_['action_add_collection_product']       = 'Thêm sản phẩm vào bộ sưu tập';
$_['action_remove_collection_product']       = 'Xóa sản phẩm khỏi bộ sưu tập';
$_['action_list_product']       = 'Chọn thao tác';
$_['action_filter']       = 'Lọc';

$_['search_collection']  = 'Tìm kiếm bộ sưu tập';
$_['use_tag_description']  = 'Mỗi tag phân cách bằng dấu phẩy (,)';
$_['search_collection']  = 'Tìm kiếm bộ sưu tập';
$_['label_add'] = 'Thêm';
$_['label_remove'] = 'Bỏ';
$_['label_delete'] = 'Xóa';
$_['label_close'] = 'Đóng';
$_['product_to_collection']      = "sản phẩm vào bộ sưu tập";
$_['product_to_collection_after']            = "sản phẩm vào các bộ sưu tập sau:";
$_['product_delete_collection']      = "sản phẩm ra khỏi bộ sưu tập";
$_['you_will_add']             =   "Bạn sẽ ";
$_['tag_for_product_choose']   = "tag cho những sản phẩm đã chọn";
$_['list_tag_for_product']    = "những tag sau cho sản phẩm:";
$_['choose_from_list_tag']  = "Chọn từ các tag đã có sẵn";
$_['notice_add_tag_success']  = "Thêm tag cho sản phẩm thành công";
$_['notice_remove_tag_success']  = "Bỏ tag cho sản phẩm thành công";
$_['notice_add_tag_error']  = "Bạn chưa thêm tag nào cho sản phẩm đã chọn";
$_['notice_add_collection_error']  = "Bạn cần chọn ít nhất 1 bộ sưu tập";
$_['notice_remove_collection_success']  =  "Xóa sản phẩm ra khỏi bộ sưu tập thành công";
$_['notice_add_collection_success']  =  "Thêm sản phẩm cho bộ sưu tập thành công";
$_['select2_notice_not_result']   =    "Không tìm thấy kết quả phù hợp";
$_['select2_notice_search']   =    "Tìm kiếm ...";
$_['select2_notice_load_more']   =    "Đang tải ...";
$_['notice_remove_tag_try']  = "Lỗi không thể xóa tag thử lại!";
// export
$_['export_file_col_link']           = 'liên_kết';
$_['export_file_col_name']           = 'tiêu_đề';
$_['export_file_col_description']    = 'mô_tả';
$_['export_file_col_sub_description']    = 'mô_tả_ngắn';
$_['export_file_col_sku']            = 'id';
$_['export_file_col_barcode']        = 'mã_số_sản_phẩm_thương_mại_toàn_cầu';
$_['export_file_col_status']         = 'ẩn_hiện';
$_['export_file_col_stock_status']   = 'tình_trạng_còn_hàng';
$_['export_file_col_quantity']       = 'số_lượng';
$_['export_file_col_manufacture']    = 'thương_hiệu';
$_['export_file_col_category']       = 'danh_mục_sản_phẩm_của_google';
$_['export_file_col_type']           = 'loại_sản_phẩm';
$_['export_file_col_weight']         = 'đo_lường_định_giá_theo_đơn_vị';
$_['export_file_col_attributes']     = 'thuộc_tính';
$_['export_file_col_price']          = 'giá_ưu_đãi';
$_['export_file_col_compare_price']  = 'giá';
$_['export_file_col_cost_price']     = 'giá_vốn';
$_['export_file_col_collections']    = 'bộ_sưu_tập';
$_['export_file_col_image']          = 'liên_kết_hình_ảnh';
$_['export_file_col_additional_image_urls'] = 'liên_kết_hình_ảnh_bổ_sung';
$_['export_file_col_image_alts']      = 'alt_ảnh';
$_['export_file_col_original_product']      = 'id_sản_phẩm_gốc';
$_['export_file_col_multi_versions']        = 'nhiều_phiên_bản';
$_['export_file_name']               = 'Danh_sách_sản_phẩm.xls';
$_['export_file_error']              = 'Xuất file lỗi. Vui lòng thử lại hoặc liên hệ Bestme để được trợ giúp.';

// for import different language
$_['export_file_col_link_2']           = 'link';
$_['export_file_col_name_2']           = 'title';
$_['export_file_col_sub_description_2']    = 'sub_description';
$_['export_file_col_sku_2']            = 'id';
$_['export_file_col_barcode_2']        = 'global_trade_code';
$_['export_file_col_status_2']         = 'hide_show';
$_['export_file_col_stock_status_2']   = 'stock_status';
$_['export_file_col_quantity_2']       = 'quantity';
$_['export_file_col_manufacture_2']    = 'brand';
$_['export_file_col_category_2']       = 'google_product_category';
$_['export_file_col_type_2']           = 'product_type';
$_['export_file_col_weight_2']         = 'measurement_of_unit_prices';
$_['export_file_col_attributes_2']     = 'attribute';
$_['export_file_col_price_2']          = 'promotion_price';
$_['export_file_col_compare_price_2']  = 'price';
$_['export_file_col_cost_price_2']     = 'cost_price';
$_['export_file_col_collections_2']    = 'collections';
$_['export_file_col_image_2']          = 'image_link';
$_['export_file_col_image_alts_2']          = 'images_alts';
$_['export_file_col_additional_image_urls_2'] = 'additional_image_link';
$_['export_file_col_original_product_2']      = 'id_original_product';
$_['export_file_col_multi_versions_2']        = 'multi_versions';

// validate tag
$_['notice_add_tag_error_max_length']   = "Tag không vượt quá 30 ký tự";

// filter
$_['filter_list_follow']                         = "Lọc sản phẩm theo";
$_['choose_filter']                              = "Chọn điều kiện lọc...";
$_['filter_status']                              = "Trạng thái hiển thị";
$_['filter_status_placeholder']                  = "Chọn trạng thái";
$_['filter_manufacture']                         = "Nhà cung cấp";
$_['filter_manufacture_placeholder']             = "Chọn nhà cung cấp";
$_['filter_category']                            = "Loại sản phẩm";
$_['filter_category_placeholder']                = "Chọn loại sản phẩm";
$_['filter_tag']                                 = "Đã được tag với";
$_['filter_tag_placeholder']                     = "Nhập tag";
$_['filter_collection']                          = "Bộ sưu tập";
$_['filter_collection_placeholder']              = "Chọn bộ sưu tập";
$_['filter_length_product']                      = "Số lượng sản phẩm";
$_['filter_length_product_placeholder']          = "Chọn điều kiện lọc";
$_['filter_length_product_greater']              = "Lớn hơn";
$_['filter_length_product_smaller']              = "Nhỏ hơn";
$_['filter_length_product_equal']                = "Bằng";
$_['filter_length_product_input_placeholder']    = "Nhập số lượng";
$_['filter_staff_placeholder']                   = "Chọn nhân viên";
$_['filter_staff']                               = "Nhân viên";

$_['add_tag_des']                                = 'Chọn tag cho sản phẩm';
$_['remove_all_tag']                             = 'Xóa tất cả các tag';
$_['do_you_want_copy']                              = 'Bạn có muốn sao chép';
$_['choose']                                     = "Đã chọn";
$_['item']                                       = "mục";
$_['change_status_success']                      = "Thay đổi status thành công";
$_['change_status_error']                        = "Thay đổi status thất bại";
$_['add_tag_success']                      = "Thêm tag thành công";
$_['remove_tag_error']                        = "Lỗi thêm tag";
$_['txt_add']                                   = "Thêm";
$_['error_product_price_must_be_greater_than']  = "Giá sản phẩm gốc phải lớn hơn 0";
$_['error_product_price_must_be_greater_or_than_equal']  = "Giá sản phẩm gốc phải lớn hơn hoặc bằng 0";
$_['error_product_promotion_price_must_be_greater_than']  = "Giá sản phẩm khuyến mại phải lớn hơn 0";
$_['remove_manufacturer_confirm_message']          = 'Bạn có chắc chắn xóa nhà cung cấp này không ?';
$_['remove_category_confirm_message']          = 'Bạn có chắc chắn xóa loại sản phẩm này không ?';
$_['select_all']                           = 'Chọn tất cả';

//Upload
$_['button_upload_continue']                              = 'Nhập danh sách sản phẩm';
$_['upload_product_list_title']                           = 'Nhập danh sách';
$_['upload_product_sample_text']                          = 'Tải file mẫu nhập danh sách';
$_['here']                                                = 'tại đây';
$_['choose_file']                                                = 'Chọn file';
$_['view_more']                       = 'Xem ảnh';

$_['alert_delete']                            = "Xóa sản phẩm";
$_['delete_success']                          = "Xóa sản phẩm thành công";
$_['notice_product_delete'] = 'Sản phẩm đã bị xóa!';

$_['popup_cancel_title']  = "Hủy thông tin thay đổi";
$_['popup_cancel_message']  = "Các thay đổi sẽ bị mất";
$_['popup_cancel_ok'] = "Đồng ý";


$_['website']                           = "Cửa hàng";
$_['choose_website']                    = "Chọn cửa hàng";
$_['channel_text']                      = "Kênh bán hàng";
$_['channel_website']                   = "Website";
$_['channel_offline']                   = "Cửa hàng";
$_['channel_website_offline']           = "Website và Cửa hàng";
$_['error_store']                       = "Vui lòng chọn ít nhất 1 cửa hàng.";

// Export
$_['button_export_continue']            = 'Xuất danh sách sản phẩm';
$_['txt_warning_export_product']        = 'Cảnh báo: quá nhiều sản phẩm, vui lòng chọn một phần để xuất danh sách!';
$_['error_select_product_part']         = 'Vui lòng chọn một phần để xuất danh sách';
$_['txt_part_from']                     = 'Từ';
$_['txt_part_to']                       = 'đến';

//form validate
$_['form_not_empty']                           = "Không được để trống";
$_['form_error_cost_price']                    = 'Giá vốn phải nhỏ hơn giá gốc';

// text storage receipt
$_['text_header_storage_receipt']                           = "Nhập kho";
$_['text_storage_receipt']                                  = "Kho hàng";
$_['text_storage_receipt_initial']                          = "Tồn kho";
$_['text_cost_price']                                       = "Giá vốn";
$_['text_storage_receipt_initial_for_product_version']      = "Tổng tồn";
$_['text_chose_store']                                      = "Chọn kho";
$_['text_default_store']                                    = "Kho mặc định";
$_['text_info_storage_receipt_initial']                     = "THÔNG TIN TỒN KHO";
$_['text_can_sell']                                         = "Có thể bán";
$_['text_goods_are_coming']                                 = "Hàng đang về";
$_['text_add_store']                                        = "Thêm kho";
$_['text_detail_version']                                   = "Chi tiết";

// text 2.3.2
$_['text_sort_desc_default_store']                           = "Mọi đơn hàng phát sinh từ website, POS, chatbot đều được trừ tồn kho sản phẩm từ kho này";
$_['text_place_input_common_cost_price']                     = "Nhập giá vốn";
$_['text_alert_change_cost_price']                           = "Giá vốn của tất cả các phiên bản sẽ được cập nhật thành giá này. Bạn có chắc chắn muốn thay đổi không?";
$_['text_manufacturer_exist_and_pause_transaction']          = "NCC đã tồn tại và đang ngừng giao dịch";

//v2.11.2
$_['txt_description']              = "MÔ TẢ SẢN PHẨM";
$_['txt_title_tab']                = "Tiêu đề tab";
$_['txt_content_tab']              = "Nội dung";
$_['txt_btn_delete_tab']           = "Xóa tab";
$_['txt_btn_add_description_tab']  = "Thêm tab mô tả sản phẩm";
$_['txt_placeholder_title_tab']                = "Nhập tiêu đề mô tả tab";
$_['txt_placeholder_description_tab']          = "Nhập nội dung";
$_['error_input_description_tab']          = "Tiêu đề tab không được để trống";
$_['error_max_length_50']          = "Tiêu đề tab không được vượt quá 50 ký tự";
$_['delete_description_tab_success']          = "Xóa thành công";
$_['txt_max_item']          = "Chỉ cho phép thêm tối đa 5 mục";

$_['error_keyword_check_name']              = ' " tên sản phẩm " đã vi phạm từ khóa';
$_['error_keyword_check_description']            = ' " Mô tả " đã vi phạm từ khóa';
$_['error_keyword_check_summary']      = ' " Mô tả ngắn " đã vi phạm từ khóa';

$_['error_empty_image']             = "Ảnh không được để trống";
$_['error_empty_image_alt']         = "Alt ảnh không được để trống";
$_['error_empty_seo_title']         = "Seo thẻ tiêu đề không được để trống";
$_['error_empty_seo_desciption']         = "Seo nội dung bài viết không được để trống";
$_['error_empty_meta_keyword']         = "Seo keywords không được để trống";

// new column excel
$_['export_file_col_general_information'] = 'thông_tin_chung';
$_['export_file_col_ingredients'] = 'thành_phần';
$_['export_file_col_main_uses'] = 'công_dụng_chính';
$_['export_file_col_user_manual'] = 'hướng_dẫn_sử_dụng';
$_['export_file_col_meta_title'] = 'thẻ_tiêu_đề';
$_['export_file_col_meta_keyword'] = 'từ_khóa';
$_['export_file_col_tags'] = 'tags';

$_['enter_ingredient']                              = 'Nhập thành phần';
$_['text_ingredient']                               = 'Thành phần';
$_['text_num_stt']                                  = 'STT';
$_['text_ingredient_code']                          = 'Mã';
$_['text_ingredient_quantitative']                  = 'Định lượng';
$_['text_ingredient_unit']                          = 'Đơn vị';