<?php
/**
 * Created by PhpStorm.
 * User: Microsoft Windows
 * Date: 19/06/2020
 * Time: 9:09 AM
 */
// Heading
$_['heading_title']                 = 'Phiếu chuyển hàng';
$_['heading_title_top']             = 'Phiếu chuyển hàng';
$_['heading_title_list']            = 'Danh sách phiếu chuyển';
$_['heading_title_add']             = 'Phiếu chuyển hàng';
$_['heading_title_edit']            = 'Phiếu chuyển hàng';
$_['breadcrumb_store']              = 'Kho hàng';
$_['breadcrumb_transfer']           = 'Chuyển hàng';


// error
$_['error_permission']              = 'Cảnh báo: Bạn không có quyền chỉnh sửa Phiếu chuyển hàng!';
$_['error_data']                    = 'Cảnh báo: Dữ liệu đầu vào không đúng!';

// Text
$_['text_add_list']                  = 'Thêm phiếu chuyển';
$_['store_transfer_receipt_search']  = 'Tìm kiếm phiếu chuyển';
$_['store_transfer_receipt_filter']  = 'Bộ lọc phiếu chuyển';
$_['filter_list_follow']             = "Lọc phiếu chuyển theo";

$_['txt_store_transfer_receipt_code']        =  "Mã phiếu";
$_['txt_store_transfer_receipt_store_from']  =  "Kho chuyển";
$_['txt_store_transfer_receipt_store_to']    =  "Kho nhận";
$_['txt_store_transfer_receipt_date_exported']   =  "Ngày chuyển";
$_['txt_store_transfer_receipt_date_imported']   =  "Ngày nhận";
$_['txt_store_transfer_receipt_total']           =  "Giá trị chuyển";
$_['txt_store_transfer_receipt_status']          =  "Trạng thái";


// filter
$_['text_filter_method']                          = 'Lọc phiếu chuyển theo';
$_['method_filter']                               = 'Trạng thái';
$_['choose_filter']                               = 'Chọn điều kiện lọc';
$_['action_filter']                               = 'Lọc';
$_['store_receipt_search']                        = 'Tìm kiếm phiếu chuyển';
$_['text_created_at_filter']                      = 'Thời gian tạo phiếu';
$_['text_status_filter']                          = 'Trạng thái';

// filter store export
$_['filter_store_export']                         = "Kho chuyển";
$_['filter_store_export_placeholder']             = "Chọn kho chuyển";

// filter store import
$_['filter_store_import']                         = "Kho nhận";
$_['filter_store_import_placeholder']             = "Chọn kho nhận";

// filter status
$_['filter_status']                              = "Trạng thái";
$_['filter_status_placeholder']                  = "Chọn trạng thái";
$_['filter_status_draft']                        = "Lưu nháp";
$_['filter_status_transfered']                   = "Đã chuyển";
$_['filter_status_recived']                      = "Đã nhận";
$_['filter_status_canceled']                     = "Đã hủy";

// filter date
$_['filter_exported_date']                       =  "Ngày chuyển";
$_['filter_exported_date_placeholder']           =  "Chọn ngày chuyển";

$_['filter_imported_date']                       =  "Ngày nhận";
$_['filter_imported_date_placeholder']           =  "Chọn ngày nhận";

$_['filter_date_range_export']                   = "Khoảng thời gian chuyển";
$_['filter_date_range_import']                   = "Khoảng thời gian nhận";
$_['filter_created_at']                          = "Ngày tạo";
$_['filter_created_at_placeholder']              = "Chọn khoảng thời gian";
$_['filter_created_at_today']                    = "Hôm nay";
$_['filter_created_at_this_week']                = "Tuần này";
$_['filter_created_at_this_month']               = "Tháng này";
$_['filter_created_at_option']                   = "Tùy chọn";
$_['filter_select_date_from']                    = "Từ ngày";
$_['filter_select_date_to']                      = "Đến ngày";
$_['filter_txt_date']                            = "Ngày";

// store transfer receipt status

$_['store_transfer_receipt_status_draft']        = "Lưu nháp";
$_['store_transfer_receipt_status_transfered']   = "Đã chuyển";
$_['store_transfer_receipt_status_recived']      = "Đã nhận";
$_['store_transfer_receipt_status_canceled']     = "Đã hủy";

// error
$_['error_update_receipt_status_receipt_id']                        = "Không tồn tại phiếu chuyển hàng";
$_['error_update_receipt_status_receipt_status_id']                 = "Không tồn tại trạng thái phiếu chuyển hàng";
$_['error_store_transfer_receipt_not_exist']                        = "Phiếu chuyển hàng không tồn tại";
$_['error_store_transfer_receipt_product_more_quantity']            = "Số lượng chuyển không được lớn hơn tồn kho thực tế của sản phẩm.";
$_['error_store_transfer_receipt_product_is_deleted']               = "Sản phẩm không còn tồn tại, vui lòng tạo lại phiếu.";
$_['error_store_transfer_receipt_input_data_is_invalid']            = "Dữ liệu đầu vào không hợp lệ";

//
$_['warning_select_store']                      = 'Vui lòng chọn kho kiểm trước khi chọn sản phẩm!';

// text btn submit
$_['txt_btn_back']                  = "Hủy";
$_['txt_btn_received']              = "Nhận hàng";
$_['txt_btn_transfer']              = "Chuyển hàng";
$_['txt_btn_cancel']                = "Hủy phiếu";
$_['txt_btn_draft']                 = "Lưu nháp";
$_['txt_btn_close']                 = "Hủy bỏ";
$_['txt_btn_apply']                 = "Xác nhận";

// text
$_['txt_product']                               = "Sản phẩm";
$_['txt_chose_product']                         = "Chọn sản phẩm";
$_['txt_common_quantity_transfer']              = "Số lượng chuyển đồng loạt";
$_['txt_input_common_quantity_transfer']        = "Nhập số lượng";
$_['text_version']                              = 'Phiên bản:';
$_['text_success_draft']                        = 'Lưu nháp phiếu thành công!';
$_['text_success_add']                          = 'Chuyển hàng thành công!';
$_['text_success_edit']                         = 'Nhận hàng thành công!';
$_['text_success_cancel']                       = 'Hủy phiếu chuyển thành công!';
$_['text_home']                                 = 'Trang chủ';
$_['text_confirm_received']                     = 'Xác nhận đã nhận được hàng';
$_['text_confirm_cancel_receipt']               = 'Xác nhận hủy phiếu chuyển hàng';

// table column
$_['col_product_name']                               = "Sản phẩm";
$_['col_inventory_transfer']                         = "Tồn kho chuyển";
$_['col_quantity_transfer']                          = "Số lượng chuyển";
$_['col_price_transfer']                             = "Giá chuyển";
$_['col_into_money']                                 = "Thành tiền";
$_['col_total_amount']                               = "Tổng cộng";

//
$_['warning_product_delete']                         = 'Sản phẩm đã thay đổi hoặc bị xóa.';
$_['warning_change_store']                           = 'Toàn bộ thông tin trong phiếu sẽ bị xóa. Bạn có chắc chắn muốn thay đổi kho chuyển không?';
$_['warning_change_status_transfer']                 = 'Sau khi chuyển hàng, hệ thống sẽ trừ lượng hàng tương ứng trong kho chuyển nhưng chưa cộng vào kho nhận cho đến khi xác nhận đã nhận được hàng.';
$_['warning_change_status_received']                 = 'Hành động này không thể hoàn tác. Xác nhận tiếp tục?';
$_['warning_change_status_cancel']                   = 'Bạn có chắc chắn muốn hủy phiếu này không?';
$_['warning_select_at_least_1_product']              = "Bạn phải chọn ít nhất 1 sản phẩm!";
$_['warning_common_quantity_transfer']               = "Số lượng chuyển của tất cả sản phẩm sẽ cập nhật thành số lượng mới này Bạn có chắc chắn muốn thay đổi không?";

// validate form
$_['mess_not_empty']                                             = "Không được để trống";
$_['mess_more_than_14_char']                                     = "Không vượt quá 14 ký tự";
$_['mess_more_than_export_store_quantity']                       = "Không vượt quá tồn kho chuyển";
$_['mess_input_0_and_more_than_export_store_quantity']           = "Nhập số > 0 và không vượt quá tồn kho chuyển";

// Select 2
$_['select2_notice_not_result']   =    "Không tìm thấy kết quả phù hợp";
$_['select2_notice_search']       =    "Tìm kiếm ...";
$_['select2_notice_load_more']    =    "Đang tải ...";