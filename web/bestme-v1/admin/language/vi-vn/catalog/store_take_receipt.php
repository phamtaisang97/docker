<?php
/**
 * Created by PhpStorm.
 * User: Microsoft Windows
 * Date: 19/06/2020
 * Time: 9:09 AM
 */
// Heading
$_['heading_title']                             = 'Kiểm hàng';
$_['heading_title_top']                         = 'Kiểm hàng';
$_['heading_title_add']                         = 'Phiếu kiểm hàng';
$_['heading_title_edit']                        = 'Phiếu kiểm hàng';
$_['heading_title_list']                        = 'Danh sách phiếu kiểm hàng';

//button
$_['btn_balance_warehouse']                     = 'Cân bằng kho';
$_['btn_draft']                                 = 'Lưu nháp';
$_['btn_receipt_cancel']                        = 'Hủy phiếu';

// text
$_['text_version']                              = 'Phiên bản:';
$_['text_success_edit']                         = 'Sửa phiếu kiểm hàng thành công!';
$_['text_success_add']                          = 'Thêm phiếu kiểm hàng thành công!';
$_['text_check_goods_note']                     = '*Sau khi hoàn tất kiểm hàng sản phẩm nào thì click checkbox cuối sản phẩm đó để xác nhận đã kiểm.';
$_['text_receipt_canceled']                     = 'Phiếu đã hủy';
$_['text_note_inventory']                       = '<b>Tồn kho:</b> là số lượng tồn kho sản phẩm ghi nhận trên hệ thống';
$_['text_note_actual']                          = '<b>Thực tế:</b> là số lượng sản phẩm khi người dùng kiểm kho';
$_['text_note_diff_values']                     = '<b>Giá trị lệch: </b> tính bằng công thức ( Thực tế - Tồn kho) * Giá vốn sản phẩm';

// Text
$_['text_add_list']                             = 'Thêm phiếu kiểm hàng';
$_['store_take_receipt_search']                 = 'Tìm kiếm phiếu kiểm hàng';
$_['store_take_receipt_filter']                 = 'Bộ lọc phiếu kiểm hàng';

$_['txt_store_take_receipt_code']               =  "Mã phiếu";
$_['txt_store_take_receipt_store']              =  "Kho kiểm hàng";
$_['txt_store_take_receipt_date']               =  "Ngày kiểm";
$_['txt_store_take_receipt_status']             =  "Trạng thái";
$_['txt_store_take_receipt_difference_amount']  =  "Giá trị lệch";
$_['text_success_draft']                        = 'Lưu nháp phiếu thành công!';
$_['text_success_cancel']                       = 'Hủy phiếu thành công!';

// filter
$_['text_filter_method']                          = 'Lọc phiếu kiểm hàng theo';
$_['method_filter']                               = 'Trạng thái';
$_['choose_filter']                               = 'Chọn điều kiện lọc';
$_['action_filter']                               = 'Lọc';
$_['store_receipt_search']                        = 'Tìm kiếm phiếu kiểm hàng';
$_['text_created_at_filter']                      = 'Thời gian tạo phiếu';
$_['text_status_filter']                          = 'Trạng thái';

// filter store take
$_['filter_store_take']                           = "Kho kiểm";
$_['filter_store_take_placeholder']               = "Chọn kho kiểm hàng";

// filter status
$_['filter_status']                              = "Trạng thái kiểm hàng";
$_['filter_status_placeholder']                  = "Chọn trạng thái";
$_['filter_status_draft']                        = "Lưu nháp";
$_['filter_status_balanced']                     = "Đã cân bằng kho";
$_['filter_status_canceled']                     = "Đã hủy";

// filter date
$_['filter_taked_date']                       =  "Ngày kiểm hàng";
$_['filter_taked_date_placeholder']           =  "Chọn ngày kiểm hàng";

$_['filter_imported_date']                       =  "Ngày nhận";
$_['filter_imported_date_placeholder']           =  "Chọn ngày nhận";

$_['filter_date_range']                          = "Khoảng thời gian kiểm hàng";
$_['filter_created_at']                          = "Ngày tạo";
$_['filter_created_at_placeholder']              = "Chọn khoảng thời gian";
$_['filter_created_at_today']                    = "Hôm nay";
$_['filter_created_at_this_week']                = "Tuần này";
$_['filter_created_at_this_month']               = "Tháng này";
$_['filter_created_at_option']                   = "Tùy chọn";
$_['filter_select_date_from']                    = "Từ ngày";
$_['filter_select_date_to']                      = "Đến ngày";
$_['filter_txt_date']                            = "Ngày";

// store take receipt status
$_['store_take_receipt_status_draft']            = "Lưu nháp";
$_['store_take_receipt_status_balanced']         = "Đã cân bằng kho";
$_['store_take_receipt_status_canceled']         = "Đã hủy";

// entry
$_['entry_store']                               = 'Kho kiểm';
$_['entry_store_placeholder']                   = 'Chọn kho kiểm';
$_['entry_product']                             = 'Sản phẩm';
$_['entry_product_placeholder']                 = 'Chọn sản phẩm';
$_['entry_inventory']                           = 'Tồn kho';
$_['entry_real_inventory']                      = 'Thực tế';
$_['entry_diff_value']                          = 'Giá trị lệch';
$_['entry_reason']                              = 'Lý do';
$_['entry_goods_checked']                       = 'Đã kiểm';

// warngin
$_['warning_select_store']                      = 'Vui lòng chọn kho kiểm trước khi chọn sản phẩm!';
$_['warning_product_not_checked']               = 'Sản phẩm chưa được kiểm';
$_['warning_change_store']                      = 'Đổi kho sẽ xóa toàn bộ thông tin trong phiếu kiểm và tồn kho sản phẩm trong kho kiểm hiện tại không thay đổi. Xác nhận tiếp tục?';
$_['warning_quantity_input']                    = 'Vui lòng nhập số lượng';
$_['warning_apply']                             = 'Thực hiện thao tác này sẽ làm thay đổi cân băng kho. Xác nhận tiếp tục?';
$_['warning_cancel']                            = 'Bạn có chắc chắn muốn hủy phiếu kiểm hàng này ko?';
$_['warning_product_delete']                    = 'Sản phẩm đã thay đổi hoặc bị xóa.';

// error
$_['error_permission']                          = 'Cảnh báo: Bạn không có quyền chỉnh sửa Phiếu kiểm hàng!';
$_['error_data']                                = 'Cảnh báo: Dữ liệu đầu vào không đúng!';


// Select 2
$_['select_store_placeholder']                  =    "Chọn kho hàng";
$_['select2_notice_not_result']                 =    "Không tìm thấy kết quả phù hợp";
$_['select2_notice_search']                     =    "Tìm kiếm ...";
$_['select2_notice_load_more']                  =    "Đang tải ...";

// validate
$_['txt_select_at_least_1_product']             = "Bạn phải chọn ít nhất 1 sản phẩm!";