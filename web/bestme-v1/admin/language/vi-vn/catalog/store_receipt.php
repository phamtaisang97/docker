<?php
// Heading
$_['heading_title']         = 'Phiếu nhập hàng';
$_['heading_title_add']     = 'Thêm phiếu nhập hàng';
$_['heading_title_edit']    = 'Sửa phiếu nhập hàng';
$_['heading_title_list']    = 'Danh sách phiếu nhập';


// Select 2
$_['select2_notice_not_result']   =    "Không tìm thấy kết quả phù hợp";
$_['select2_notice_search']       =    "Tìm kiếm ...";
$_['select2_notice_load_more']    =    "Đang tải ...";

// breadcrumbs
$_['text_home']                           = 'Trang chủ';
$_['text_success_add']                    = 'Thêm phiếu nhập hàng mới thành công!';
$_['text_success_edit']                   = 'Sửa phiếu nhập hàng thành công';

// button
$_['text_add_list']                       = 'Thêm phiếu nhập';
$_['text_save_apply']                     = 'Đặt hàng và duyệt';
$_['text_save_draft']                     = 'Lưu nháp';

// title table
$_['txt_store_receipt_code']                      = "Mã phiếu";
$_['txt_store_receipt_manufacturer']              = "Nhà cung cấp";
$_['txt_store_receipt_branch']                    = "Chi nhánh";
$_['txt_store_receipt_status']                    = "Trạng thái";
$_['txt_store_receipt_total_to_pay']              = "Tiền cần trả";
$_['txt_store_receipt_owed']                      = "Chưa thanh toán";
$_['txt_store_receipt_created_at']                = "Ngày tạo phiếu";
$_['txt_delivering']                              = "Hàng đang về";
$_['txt_receipted']                               = "Đã nhập hàng";
$_['txt_store']                                   = "Kho nhập";
$_['txt_store_placeholder']                       = "Chọn kho nhập";
$_['txt_manufacturer']                            = "Nhà cung cấp";
$_['txt_manufacturer_placeholder']                = "Chọn nhà cung cấp";
$_['txt_products']                                = "Thông tin sản phẩm";
$_['txt_product_search']                          = "Tìm kiếm sản phẩm";
$_['txt_product_name']                            = "Tên sản phẩm";
$_['txt_quantity']                                = "Số lượng";
$_['txt_quantity_placeholder']                    = "Nhập số lượng";
$_['txt_value']                                   = "Giá trị";
$_['txt_import_price']                            = "Giá nhập";
$_['txt_other_fee']                               = "Chi phí khác";
$_['txt_discount']                                = "Chiết khấu";
$_['txt_into_money']                              = "Thành tiền";
$_['txt_billing_info']                            = "Thông tin thanh toán";
$_['txt_total_money']                             = "Tổng tiền hàng";
$_['txt_total_pay']                               = "Tiền cần trả";
$_['txt_total_paid']                              = "Tiền đã trả";
$_['txt_total_owed']                              = "Còn nợ";
$_['txt_note']                                    = "Ghi chú";
$_['txt_add_other_fee']                           = "Thêm chi phí khác";
$_['txt_fee_name_placeholder']                    = "Nhập tên chi phí";
$_['txt_money_placeholder']                       = "Nhập số tiền";
$_['txt_select_allocation_criteria_type']         = "Chọn tiêu thức phân bổ vào giá vốn";
$_['txt_total_fee']                               = "Tổng chi phí";
$_['txt_fee_modal_warning']                       = "Phải nhập tên chi phí và số tiền đầy đủ";
$_['txt_add_manufacturer']                        = "Thêm nhà cung cấp";
$_['txt_add_product']                             = "Thêm sản phẩm";
$_['txt_add_product_placeholder']                 = "Nhập tên sản phẩm";
$_['txt_product_single_version']                  = "Sản phẩm 1 phiên bản";
$_['txt_product_multiple_version']                = "Sản phẩm nhiều phiên bản";
$_['txt_enter_price_placeholder']                 = "Nhập giá";
$_['txt_retail_price']                            = "Giá bán lẻ";
$_['txt_promotion_price']                         = "Giá sau khuyến mãi";
$_['txt_sku']                                     = "Mã SKU";
$_['txt_sku_placeholder']                         = "Nhập mã SKU";
$_['txt_attribute_name']                          = "Tên thuộc tính";
$_['txt_attribute_value']                         = "Giá trị thuộc tính";
$_['txt_attribute_value_placeholder']             = "Mỗi tag phân cách bằng dấu (,)";
$_['txt_add_attribute']                           = "Thêm thuộc tính khác";
$_['txt_th_display']                              = "Hiện";
$_['txt_th_version']                              = "Phiên bản";
$_['txt_th_promotion_price']                      = "Giá sau KM";
$_['text_deleted_or_no_exist']                    = 'Phiếu nhập hàng không tồn tại hoặc đã bị xoá!';
$_['text_version']                                = 'Phiên bản : ';
$_['text_payment_voucher']                        = 'Phiếu chi tạo tự động gắn với hóa đơn này là  ';
//validate
$_['txt_select_at_least_1_product']               = "Bạn phải chọn ít nhất 1 sản phẩm!";
$_['error_empty_store']                           = "Vui lòng chọn kho nhập";
$_['error_empty_manufacturer']                    = "Vui lòng chọn nhà cung cấp";
$_['error_money_paid']                            = "Tiền đã trả không được lớn hơn số tiền cần trả";
$_['error_empty_price']                           = "Vui lòng nhập giá nhập";
$_['error_empty_quantity']                        = "Vui lòng nhập số lượng";
$_['error_create_manufacturer']                   = "Tạo mới nhà cung cấp không thành công";
$_['error_empty_product_name']                    = "Vui lòng nhập tên sản phẩm";
$_['text_error_form_manufacturer_name_duplicate']    = 'Tên nhà cung cấp đã tồn tại';
$_['text_error_form_tax_code_more_16']    = 'Mã số thuế không quá 16 ký tự';
$_['text_error_form_tax_code_special_characters']    = 'Mã số thuế không đúng định dạng';
$_['text_error_form_manufacturer_tax_code_duplicate']    = 'Mã số thuế đã tồn tại';


// filter
$_['text_filter_method']                          = 'Lọc phiếu nhập theo';
$_['method_filter']                               = 'Trạng thái';
$_['choose_filter']                               = 'Chọn phương thức';
$_['action_filter']                               = 'Lọc';
$_['store_receipt_search']                        = 'Tìm kiếm phiếu nhập';
$_['text_created_at_filter']                      = 'Thời gian tạo phiếu';
$_['text_manufacturer_filter']                    = 'Nhà cung cấp';
$_['text_status_filter']                          = 'Trạng thái';

// filter created at
$_['filter_date_range']                          = "Khoảng thời gian";
$_['filter_created_at']                          = "Ngày tạo";
$_['filter_created_at_placeholder']              = "Chọn khoảng thời gian";
$_['filter_created_at_today']                    = "Hôm nay";
$_['filter_created_at_this_week']                = "Tuần này";
$_['filter_created_at_this_month']               = "Tháng này";
$_['filter_created_at_option']                   = "Tùy chọn";
$_['filter_select_date_from']                    = "Từ ngày";
$_['filter_select_date_to']                      = "Đến ngày";
$_['filter_txt_date']                            = "Ngày";

// filter status
$_['filter_status']                              = "Trạng thái nhập hàng";
$_['filter_status_placeholder']                  = "Chọn trạng thái";
$_['filter_status_delivering']                   = "Hàng đang về";
$_['filter_status_receipted']                    = "Đã nhập hàng";

// filter manufacturer
$_['filter_manufacturer']                        = "Nhà cung cấp";
$_['filter_manufacturer_placeholder']            = "Chọn nhà cung cấp";

// checkbox
$_['text_options']                                = 'Chọn thao tác';
$_['txt_all']                                     = 'Danh sách phiếu nhập';
$_['store_receipt_filter']                        = 'Bộ lọc phiếu nhập';
$_['txt_receipted_checkbox']                      = 'Nhập hàng';
$_['txt_cancel_checkbox']                         = 'Hủy phiếu';

// popup result
$_['text_no_results']                             = 'Không có dữ liệu!';
$_['text_manufacturer_code']                      = 'Mã phiếu';
$_['text_manufacturer_result']                    = 'Kết quả';
$_['text_manufacturer_reason']                    = 'Lí do';
$_['text_failure']                                = 'Không thành công';
$_['text_success']                                = 'Thành công';
$_['txt_failure_receipt']                         = 'Phiếu này đã được nhập!';
$_['txt_success_receipt']                         = 'Nhập hàng thành công';
$_['txt_success_cancel']                          = 'Hủy phiếu nhập thành công!';
$_['txt_note_view_detail']                        = 'Xem chi tiết >';

// modal
$_['text_close']                                  = 'Đóng';
$_['txt_cancel']                                  = 'Bỏ qua';
$_['txt_confirm']                                 = 'Xác nhận';
$_['txt_heading_receipt']                         = 'Nhập hàng';
$_['txt_receipt_content']                         = 'Bạn có chắc chắn nhập hàng phiếu này không?';
$_['txt_heading_cancel']                          = 'Hủy phiếu';
$_['txt_cancel_content']                          = 'Bạn có chắc chắn hủy phiếu không?';

// for manufacture form
// form
$_['text_form_save']                         = 'Lưu lại';
$_['text_form_cancel']                       = 'Hủy bỏ';
$_['text_form_contact_info']                 = 'Thông tin liên hệ';
$_['text_form_name']                         = 'Tên nhà cung cấp';
$_['text_form_name_placeholder']             = 'Nhập tên nhà cung cấp';
$_['text_form_phone']                        = 'Số điện thoại';
$_['text_form_phone_placeholder']            = 'Nhập số điện thoại';
$_['text_form_email']                        = 'Email';
$_['text_form_email_placeholder']            = 'Nhập email';
$_['text_form_tax_code']                     = 'Mã số thuế';
$_['text_form_tax_code_placeholder']         = 'Nhập mã số thuế';
$_['text_form_address']                      = 'Địa chỉ';
$_['text_form_tax_address_placeholder']      = 'Nhập địa chỉ';
$_['text_form_province']                     = 'Thành phố';
$_['text_form_tax_province_placeholder']     = 'Chọn thành phố';
$_['text_form_district']                     = 'Quận huyện';
$_['text_form_tax_district_placeholder']     = 'Chọn quận huyện';

// validate form
$_['text_error_form_name_empty']                     = 'Tên nhà cung cấp không được để trống';
$_['text_error_form_name_more_255']                  = 'Tên nhà cung cấp không được lớn hơn 255 kí tự';
$_['text_error_form_telephone_empty']                = 'Số điện thoại không được để trống';
$_['text_error_form_telephone_start']                = 'Bạn phải nhập kiểu số, bắt đầu bởi 0 hoặc 84!';
$_['text_error_form_telephone_wrong_format']         = 'Số điện thoại không đúng định dạng';
$_['text_error_form_telephone_more_15']              = 'Số điện thoại không quá 15 ký tự!';
$_['text_error_form_email_wrong_format']             = 'Email không đúng định dạng!';

// form product

// validate
$_['error_warning']          = 'Cảnh báo: Vui lòng kiểm tra kỹ form nội dung để biết lỗi!';
$_['error_permission']       = 'Cảnh báo: Bạn không có quyền chỉnh sửa Sản phẩm!';
$_['error_name']             = 'Tên sản phẩm quá ngắn hoặc quá dài (1-100 ký tự)!';
$_['error_meta_title']       = 'Tiêu đề thẻ Meta dài từ 1 đến 255 ký tự!';
$_['error_model']            = 'Mẫu Sản phẩm dài từ 1 đến 255 ký tự!';
$_['error_keyword']          = 'SEO URL đã được sử dụng!';
$_['error_unique']           = 'SEO URL phải là duy nhất!';
$_['error_name_exist']             = 'Tên sản phẩm đã tồn tại!';
$_['error_sku_exist']             = 'SKU sản phẩm đã tồn tại!';
$_['error_product_version_limit']             = 'Số lượng phiên bản sản phẩm tối đa là 100.';
$_['error_product_attribute_name_exist']             = 'Tên thuộc tính đã tồn tại';
$_['error_has_discount']                             = 'Cảnh báo: sản phẩm đang nằm trong chương trình khuyến mại đang hoạt động';
$_['error_edit_error_when_has_discount']             = 'Cảnh báo: không cập nhật được sản phẩm đang nằm trong chương trình khuyến mại đang hoạt động';
$_['error_delete_error_when_has_discount']           = 'Cảnh báo: không xoá được sản phẩm đang nằm trong chương trình khuyến mại đang hoạt động';

// Error File
$_['error_permission'] = 'Cảnh báo: Yêu cầu bị từ chối do thẩm quyền!';
$_['error_filename']   = 'Cảnh báo: Tên tệp dài từ 3 đến 255!';
$_['error_folder']     = 'Cảnh báo: Tên thư mục dài từ 3 đến 255!';
$_['error_exists']     = 'Cảnh báo: Đã tồn tại thư mục!';
$_['error_directory']  = 'Cảnh báo: Thư mục chưa tồn tại!';
$_['error_filesize']   = 'Cảnh báo: Kích thước tệp không được quá 1MB!';
$_['error_filetype']   = 'Cảnh báo: Chỉ chấp nhận định dạng .xls và xlsx !';
$_['error_upload']     = 'Cảnh báo: Không thể tải tệp lên hệ thống (lỗi không xác định)!';
$_['error_delete']     = 'Cảnh báo: Không thể xoá thư mục này!';
$_['error_limit']      = 'Cảnh báo: Số sản phẩm và phiên bản sản phẩm import tối đa là 500!';
$_['error_not_found_fields']      = 'Cảnh báo: Không tìm thấy tiêu_đề hoặc mô_tả!';
$_['error_file_incomplete']       = 'Cảnh báo: File không đầy đủ thông tin (thiếu cột)!';
$_['error_file_incomplete_in_row']       = 'Cảnh báo: Thiếu tiêu_đề, giá, đo_lường_định_giá_theo_đơn_vị, hoặc giá ưu đãi lơn hơn giá bán tại dòng ';
$_['error_unknown']                      = 'Lỗi ko xác định';
$_['error_no_file_selected']             = 'Chưa có file nào được chọn!';

$_['error_input_null']                 = 'Vui lòng điền vào trường này';
$_['error_input_price_null']                 = 'Vui lòng nhập giá sản phẩm';
$_['error_input_product_name']         = 'Vui lòng nhập tên sản phẩm';
$_['error_type_mail']                  = 'Vui lòng nhập đúng định dạng email';
$_['error_max_length_255']             = 'Không vượt quá 255 ký tự';
$_['error_max_length_15']              = 'Không vượt quá 15 ký tự';
$_['error_max_length_12']              = 'Không vượt quá 12 ký tự';
$_['error_type_phone']                 = 'Vui lòng nhập số cho trường này';
$_['error_max_length_10']              = 'Không vượt quá 10 ký tự';
$_['error_max_length_100']             = 'Không vượt quá 100 ký tự';
$_['error_max_length_30']             = 'Không vượt quá 30 ký tự';
$_['error_max_length_20']             = 'Không vượt quá 20 ký tự';
$_['error_max_length_48']             = 'Không vượt quá 48 ký tự';
$_['error_type_barcode']             = 'Chỉ cho phép nhập chữ và số';
$_['error_type_sku']             =   'Không nhập tiếng việt có dấu';
$_['error_type_weight']         = "Khối lượng sản phẩm lớn hơn hoặc bằng 1 gram và nhỏ hơn 100.000 gram";
$_['error_special_char']         = "Không nhập ký tự đặc biệt";
$_['error_type_sku_same']             =   'Mã SKU đã tồn tại';
$_['error_promotion_price']           = 'Giá khuyến mại phải nhỏ hơn giá bán lẻ';
$_['error_price']           = 'Giá bán lẻ phải lơn hơn giá khuyến mại';
$_['error_text_char']           = 'Không được nhập ký tự đặc biệt';

$_['notice_add_tag_error_max_length']   = "Tag không vượt quá 30 ký tự";

$_['error_product_price_must_be_greater_than']  = "Giá sản phẩm gốc phải lớn hơn 0";
$_['error_product_price_must_be_greater_or_than_equal']  = "Giá sản phẩm gốc phải lớn hơn hoặc bằng 0";
$_['error_product_promotion_price_must_be_greater_than']  = "Giá sản phẩm khuyến mại phải lớn hơn 0";


$_['error_store']                       = "Vui lòng chọn ít nhất 1 cửa hàng.";