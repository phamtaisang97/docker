<?php
// Heading
$_['heading_title']      = 'Danh sách cửa hàng';
$_['breadcrumb_setting'] = 'Cài đặt';
$_['search']             = 'Tìm kiếm';
$_['name_store']         = 'Tên cửa hàng';

$_['close']   = 'Đóng';
$_['add_new'] = 'Thêm mới';
$_['save']    = 'Lưu';
$_['delete']  = 'Xóa';
$_['delete_confirm']  = 'Xác nhận';

$_['store']         = 'Cửa hàng';
$_['add_new_store'] = 'Thêm cửa hàng mới';
$_['edit_store']    = 'Sửa thông tin cửa hàng';

$_['delete_store']           = 'Xóa cửa hàng';
$_['confirm_delete_store']   = 'Bạn có chắc chắn muốn xóa cửa hàng này?';
$_['error_name_store_empty'] = 'Tên cửa hàng rỗng';
$_['error_name_store_isset'] = 'Tên cửa hàng đã tồn tại';
$_['add_store_success']      = "Thêm cửa hàng thành công";
$_['edit_store_success']     = "Sửa cửa hàng thành công";
$_['delete_store_success']   = "Xóa cửa hàng thành công!";
$_['delete_store_error']     = "Xóa cửa hàng thất bại";

$_['paginator_show']  = "Hiện thị";
$_['paginator_total'] = "Trên tổng số";
$_['paginator_page']  = "trang";

$_['error_name_empty']      = "Nhập dữ liệu vào tên trường";
$_['error_name_max_length'] = "Tên trường không vượt quá 30 ký tự";
$_['error_name_isset']      = "Tên trường đã tồn tại";
$_['error_not_delete']      = "Không xóa được cửa hàng do đang gắn với sản phẩm hoặc nhân viên";

$_['error_limit_store'] = "Thao tác không thực hiện được do bạn đã sử dụng hết hạn mức số cửa hàng của gói dịch vụ. 
                                        Vui lòng liên hệ với chúng tôi để nâng cấp thêm.";
$_['alarm_limit_store'] = "Bạn đã sử dụng 80% hạn mức số cửa hàng của gói dịch vụ";

$_['text_no_result'] = "Không có dữ liệu";

$_['text_products_in_store'] = "Sản phẩm trong cửa hàng";
$_['text_nox'] = "STT";
$_['text_note'] = "Lưu ý: Các cửa hàng (kho) đang gắn với nhân viên hoặc sản phẩm sẽ không xóa được!";

$_['error_permission']           = 'Cảnh báo: Bạn không có quyền chỉnh sửa Cửa hàng!';