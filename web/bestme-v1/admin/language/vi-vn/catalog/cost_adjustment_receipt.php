<?php
/**
 * Created by PhpStorm.
 * User: Microsoft Windows
 * Date: 19/06/2020
 * Time: 9:09 AM
 */
// Heading
$_['heading_title']                             = 'Phiếu điều chỉnh giá vốn';
$_['heading_title_add']                         = 'Thêm phiếu';
$_['heading_title_edit']                        = 'Sửa phiếu';
$_['heading_title_list']                        = 'Danh sách phiếu điều chỉnh';

// text
$_['text_version']                              = 'Phiên bản:';
$_['text_success_edit']                         = 'Sửa phiếu điều chỉnh giá vốn thành công!';
$_['text_success_add']                          = 'Thêm phiếu điều chỉnh giá vốn thành công!';
$_['text_receipt_canceled']                     = 'Phiếu đã hủy';

// Text
$_['text_add_list']                             = 'Thêm phiếu điều chỉnh';
$_['cost_adjustment_receipt_search']                 = 'Tìm kiếm phiếu điều chỉnh';
$_['cost_adjustment_receipt_filter']                 = 'Bộ lọc phiếu điều chỉnh';

$_['txt_cost_adjustment_receipt_code']               =  "Mã phiếu";
$_['txt_cost_adjustment_receipt_store']              =  "Kho điều chỉnh";
$_['txt_cost_adjustment_receipt_date_created']       =  "Ngày tạo";
$_['txt_cost_adjustment_receipt_date']               =  "Ngày điều chỉnh";
$_['txt_cost_adjustment_receipt_status']             =  "Trạng thái";
$_['txt_cost_adjustment_receipt_difference_amount']  =  "Giá trị lệch";
$_['text_success_draft']                             = 'Lưu nháp phiếu thành công!';
$_['text_success_cancel']                            = 'Hủy phiếu thành công!';

// filter
$_['text_filter_method']                          = 'Lọc phiếu điều chỉnh theo';
$_['method_filter']                               = 'Trạng thái';
$_['choose_filter']                               = 'Chọn phương thức';
$_['action_filter']                               = 'Lọc';
$_['store_receipt_search']                        = 'Tìm kiếm phiếu điều chỉnh';
$_['text_created_at_filter']                      = 'Thời gian tạo phiếu';
$_['text_status_filter']                          = 'Trạng thái';

// filter store take
$_['filter_cost_adjustment']                           = "Kho điều chỉnh";
$_['filter_cost_adjustment_placeholder']               = "Chọn kho điều chỉnh";

// filter status
$_['filter_status']                              = "Trạng thái điều chỉnh";
$_['filter_status_placeholder']                  = "Chọn trạng thái";
$_['filter_status_draft']                        = "Lưu nháp";
$_['filter_status_apply']                        = "Đã điều chỉnh";
$_['filter_status_canceled']                     = "Đã hủy";

// filter date
$_['filter_added_date']                       =  "Ngày điều chỉnh";
$_['filter_added_date_placeholder']           =  "Chọn ngày điều chỉnh";

$_['filter_date_range']                          = "Khoảng thời gian điều chỉnh";
$_['filter_created_at']                          = "Ngày tạo";
$_['filter_created_at_placeholder']              = "Chọn khoảng thời gian";
$_['filter_created_at_today']                    = "Hôm nay";
$_['filter_created_at_this_week']                = "Tuần này";
$_['filter_created_at_this_month']               = "Tháng này";
$_['filter_created_at_option']                   = "Tùy chọn";
$_['filter_select_date_from']                    = "Từ ngày";
$_['filter_select_date_to']                      = "Đến ngày";
$_['filter_txt_date']                            = "Ngày";

// status description

$_['cost_adjustment_receipt_status_draft']       =  "Lưu nháp";
$_['cost_adjustment_receipt_status_apply']       =  "Đã điều chỉnh";
$_['cost_adjustment_receipt_status_canceled']    =  "Đã hủy";

// entry
$_['entry_store']                               = 'Kho điều chỉnh';
$_['entry_store_placeholder']                   = 'Chọn kho điều chỉnh';
$_['entry_product']                             = 'Sản phẩm';
$_['entry_product_placeholder']                 = 'Chọn sản phẩm';
$_['entry_current_cost_price']                  = 'Giá vốn hiện tại';
$_['entry_adjustment_cost_price']               = 'Sau điều chỉnh';
$_['entry_reason']                              = 'Lý do';
$_['entry_note']                                = 'Ghi chú';
$_['entry_note_placeholder']                    = 'Nhập nội dung';

// warning
$_['warning_store_select']                      = 'Vui lòng chọn kho kiểm trước khi chọn sản phẩm!';
$_['warning_change_store']                      = 'Đổi kho sẽ xóa toàn bộ thông tin trong phiếu điều chỉnh và giá vốn sản phẩm trong kho kiểm hiện tại không thay đổi. Xác nhận tiếp tục?';
$_['warning_receipt_apply']                     = 'Thực hiện thao tác này sẽ làm thay đổi giá vốn sản phẩm trong kho. Xác nhận tiếp tục?';
$_['warning_receipt_cancel']                    = 'Bạn có chắc chắn muốn hủy phiếu điều chỉnh này ko?';
$_['warning_product_delete']                    = 'Sản phẩm đã thay đổi hoặc bị xóa.';

// button
$_['btn_adjustment']                            = 'Điều chỉnh';
$_['btn_draft']                                 = 'Lưu nháp';
$_['btn_receipt_cancel']                        = 'Hủy phiếu';

// error
$_['error_permission']                          = 'Cảnh báo: Bạn không có quyền chỉnh sửa Phiếu điều chỉnh giá vốn!';
$_['error_data']                                = 'Cảnh báo: Dữ liệu đầu vào không đúng!';


// Select 2
$_['select_store_placeholder']                  =    "Chọn kho hàng";
$_['select2_notice_not_result']                 =    "Không tìm thấy kết quả phù hợp";
$_['select2_notice_search']                     =    "Tìm kiếm ...";
$_['select2_notice_load_more']                  =    "Đang tải ...";

// validate
$_['txt_select_at_least_1_product']             = "Bạn phải chọn ít nhất 1 sản phẩm!";