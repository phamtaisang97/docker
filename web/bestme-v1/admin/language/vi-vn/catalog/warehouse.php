<?php
// Heading
$_['heading_title']          = 'Quản lý tồn kho';

// breadcrumb
$_['breadcrumb_product']                = 'Sản phẩm';
$_['breadcrumb_warehouse']              = 'Quản lý tồn kho';

// list
$_['warehouse_setting_title']           = 'Quản lý tồn kho';

$_['warehouse_list_title']              = 'Danh sách';
$_['warehouse_list_filter']             = 'Bộ lọc sản phẩm';
$_['warehouse_list_filter_mobile']             = 'Lọc';

// filter
$_['warehouse_list_filter_category']         = 'Danh mục';
$_['warehouse_list_filter_category_all']     = 'Tất cả';

$_['warehouse_list_filter_warehouse']        = 'Kho hàng';
$_['warehouse_list_filter_warehouse_all']    = 'Tất cả';

$_['warehouse_list_filter_status']           = 'Trạng thái';
$_['warehouse_list_filter_status_all']       = 'Tất cả';
$_['warehouse_list_filter_status_enabled']   = 'Hiển thị';
$_['warehouse_list_filter_status_disabled']  = 'Ẩn';

$_['warehouse_list_filter_search_button']    = 'Lọc';

// search
$_['warehouse_list_search_placeholder']      = 'Tìm kiếm sản phẩm';

// update modal
$_['warehouse_update_quantity_modal_title']      = 'Cập nhật số lượng';
$_['warehouse_update_quantity_modal_message']    = 'Chưa có sản phẩm nào được chọn. Vui lòng chọn sản phẩm!';
$_['warehouse_update_quantity_modal_close']      = 'Đóng';
$_['warehouse_update_quantity_modal_add']        = 'Thêm';
$_['warehouse_update_quantity_modal_set']        = 'Đặt';
$_['warehouse_update_quantity_modal_save']       = 'Lưu';
$_['warehouse_update_quantity_modal_cancel']     = 'Bỏ qua';

// list
$_['warehouse_list_choose_action']               = 'Chọn thao tác';
$_['warehouse_list_action_stop_sale']            = 'Ngừng bán khi hết hàng';
$_['warehouse_list_action_continue_sale']        = 'Tiếp tục bán khi hết hàng';
$_['warehouse_list_action_update_quantity']      = 'Cập nhật số lượng';

$_['warehouse_list_header_name']                         = 'Sản phẩm';
$_['warehouse_list_header_sku']                          = 'SKU';
$_['warehouse_list_header_cost_price']                   = 'Giá vốn';
$_['warehouse_list_header_price']                        = 'Giá sản phẩm';
$_['warehouse_list_header_on_out_of_stock']              = 'Khi hết hàng';
$_['warehouse_list_header_date_modified']                = 'Ngày cập nhật';
$_['warehouse_list_header_quantity']                     = 'Số lượng';
$_['warehouse_list_header_update_quantity']              = 'Cập nhật số lượng';
$_['warehouse_list_header_update_quantity_add']          = 'Thêm';
$_['warehouse_list_header_update_quantity_set']          = 'Đặt';
$_['warehouse_list_header_update_quantity_save_button']  = 'Lưu';

// sale_on_out_of_stock
$_['warehouse_list_sale_on_out_of_stock_continue']       = 'Tiếp tục bán';
$_['warehouse_list_sale_on_out_of_stock_stop']           = 'Ngừng bán';

// pagination
$_['warehouse_list_pagination_message']          = 'Hiển thị %s-%s trên tổng %s (%s Pages)';
$_['warehouse_list_pagination_page']             = 'Trang';

// Error form
$_['error_quantity']                             = 'Lỗi: số lượng sản phẩm phải có giá trị từ 1 đến 9,999,999,999';
$_['text_success']              = "Cập nhật thành công!";

// Filter
$_['filter_list_follow']                         = "Lọc sản phẩm theo";
$_['choose_filter']                              = "Chọn điều kiện lọc...";
$_['filter_status_text']                              = "Trạng thái hiển thị";
$_['filter_status_placeholder']                  = "Chọn trạng thái";
$_['filter_manufacture_text']                         = "Nhà cung cấp";
$_['filter_manufacture_placeholder']             = "Chọn nhà cung cấp";
$_['filter_category_text']                            = "Loại sản phẩm";
$_['filter_category_placeholder']                = "Chọn loại sản phẩm";
$_['filter_tag_text']                                 = "Đã được tag với";
$_['filter_tag_placeholder']                     = "Nhập tag";
$_['filter_collection_text']                          = "Bộ sưu tập";
$_['filter_collection_placeholder']              = "Chọn bộ sưu tập";
$_['filter_length_product']                      = "Số lượng sản phẩm";
$_['filter_length_product_placeholder']          = "Chọn điều kiện lọc";
$_['filter_length_product_greater']              = "Lớn hơn";
$_['filter_length_product_smaller']              = "Nhỏ hơn";
$_['filter_length_product_equal']                = "Bằng";
$_['filter_length_product_input_placeholder']    = "Nhập số lượng";
$_['choose']                                     = "Đã chọn";
$_['item']                                       = "mục";
$_['warehouse_list_header_update_quantity_add_title']          = 'Tăng thêm số lượng sản phẩm trong kho';
$_['warehouse_list_header_update_quantity_set_title']          = 'Đặt lại số lượng sản phẩm trong kho';
$_['select_all']                            = 'Chọn tất cả';

// table
$_['column_price_cost']                   = 'Giá vốn';
$_['column_inventory']                    = 'Tồn kho';
$_['column_deliver']                      = 'Hàng đang về';
$_['total_quantity_of_the_warehouse']     = 'Tổng tồn kho ';
$_['total_value_of_the_warehouse']        = 'Tổng giá trị kho ';
$_['text_no_results']                     = 'Không có dữ liệu!';
$_['text_column_inventory_default_store']                     = 'kho mặc định';

// Select 2
$_['select_store_placeholder']    =    "Chọn kho hàng";
$_['select2_notice_not_result']   =    "Không tìm thấy kết quả phù hợp";
$_['select2_notice_search']       =    "Tìm kiếm ...";
$_['select2_notice_load_more']    =    "Đang tải ...";
$_['txt_all_store']               =    "< Tất cả kho >";
$_['txt_default_store']           =    "< Kho mặc định >";

$_['switch_inventory_basic']    = "Chuyển sang chế độ cơ bản";
$_['switch_inventory_advance']  = "Chuyển sang chế độ nâng cao";