<?php
// Heading
$_['heading_title']     = 'Danh sách nhà cung cấp';

// Text
$_['text_success']      = 'Thành công';
$_['text_list']         = 'Manufacturer List';
$_['text_add']          = 'Thêm nhà cung cấp';
$_['text_edit']         = 'Sửa nhà cung cấp';
$_['text_product']      = 'Sản phẩm';
$_['text_default']      = 'Default';
$_['text_percent']      = 'Percentage';
$_['text_amount']       = 'Fixed Amount';
$_['text_keyword']      = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';

// Column
$_['column_name']       = 'Manufacturer Name';
$_['column_sort_order'] = 'Sort Order';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']        = 'Manufacturer Name';
$_['entry_store']       = 'Stores';
$_['entry_keyword']     = 'Keyword';
$_['entry_image']       = 'Image';
$_['entry_sort_order']  = 'Sort Order';
$_['entry_type']        = 'Type';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify manufacturers!';
$_['error_name']        = 'Manufacturer Name must be between 1 and 64 characters!';
$_['error_keyword']     = 'SEO URL already in use!';
$_['error_unique']      = 'SEO URL must be unique!';
$_['error_product']     = 'Cảnh báo: không thể xóa nhà cung cấp này do đang được gán với %s sản phẩm! Vui lòng bỏ tất sản phẩm khỏi nhà cung cấp này rồi thực hiện lại.';
$_['delete_success']    = "Xóa nhà cung cấp thành công";
$_['delete_error']      = "Xóa nhà cung cấp thất bại";
$_['add_success']    = "Thêm nhà cung cấp thành công";
$_['add_error']      = "Thêm nhà cung cấp thất bại";

// title
$_['txt_manufacture_code']                = "Mã nhà cung cấp";
$_['txt_manufacture_name']                = "Tên nhà cung cấp";
$_['txt_manufacture_phone']               = "Số điện thoại";
$_['txt_manufacture_address']             = "Địa chỉ";
$_['txt_manufacture_status']              = "Trạng thái";
$_['text_filter_method']                  = 'Lọc theo trạng thái';
$_['method_filter']                       = 'Trạng thái';
$_['choose_filter']                       = 'Chọn phương thức';
$_['action_filter']                       = 'Lọc';
$_['manufacture_search']                  = 'Tìm kiếm nhà cung cấp';
$_['manufacturer_filter']                 = "Lọc nhà cung cấp";
// filter

$_['filter_status']                       = "Trạng thái";
$_['filter_staff']                        = "Nhân viên";
$_['filter_staff_placeholder']            = "Chọn nhân viên";
$_['filter_list_follow']                  = "Chọn điều kiện lọc";

$_['txt_creator']                         = "Người tạo";
$_['txt_staff']                           = "Nhân viên";
$_['txt_pause_filter']                    = "Ngừng giao dịch";
$_['txt_again_filter']                    = "Giao dịch lại";
$_['txt_delete_filter']                   = "Xóa nhà cung cấp";
$_['text_options']                        = 'Chọn thao tác';
$_['txt_all']                             = 'Danh sách nhà cung cấp';
$_['manufacture_filter']                  = 'Bộ lọc';
$_['txt_heading_pause']                   = 'Ngừng giao dịch';
$_['txt_heading_again']                   = 'Giao dịch lại';
$_['txt_heading_delete']                  = 'Xóa nhà cung cấp';
$_['txt_pause_content']                   = 'Bạn có chắc chắn ngừng giao dịch với nhà cung cấp này không?';
$_['txt_again_content']                   = 'Bạn có chắc chắn giao dịch lại với nhà cung cấp này không?';
$_['txt_delete_content']                  = 'Bạn có chắc chắn xóa nha cung cấp này không?';

// status
$_['text_in_transaction']                 = 'Đang giao dịch';
$_['text_stop_transaction']               = 'Ngừng giao dịch';
$_['text_delete_transaction']             = 'Xóa';
$_['text_home']                           = 'Trang chủ';
$_['text_add_list']                       = 'Tạo nhà cung cấp';

// modal
$_['text_close']                          = 'Đóng';
$_['txt_cancel']                          = 'Bỏ qua';
$_['txt_confirm']                         = 'Xác nhận';

// form
$_['text_form_save']                         = 'Lưu lại';
$_['text_form_cancel']                       = 'Hủy bỏ';
$_['text_form_contact_info']                 = 'Thông tin liên hệ';
$_['text_form_name']                         = 'Tên nhà cung cấp';
$_['text_form_name_placeholder']             = 'Nhập tên nhà cung cấp';
$_['text_form_phone']                        = 'Số điện thoại';
$_['text_form_phone_placeholder']            = 'Nhập số điện thoại';
$_['text_form_email']                        = 'Email';
$_['text_form_email_placeholder']            = 'Nhập email';
$_['text_form_tax_code']                     = 'Mã số thuế';
$_['text_form_tax_code_placeholder']         = 'Nhập mã số thuế';
$_['text_form_address']                      = 'Địa chỉ';
$_['text_form_tax_address_placeholder']      = 'Nhập địa chỉ';
$_['text_form_province']                     = 'Thành phố';
$_['text_form_tax_province_placeholder']     = 'Chọn thành phố';
$_['text_form_district']                     = 'Quận huyện';
$_['text_form_tax_district_placeholder']     = 'Chọn quận huyện';

// validate form
$_['text_error_form_name_empty']                     = 'Tên nhà cung cấp không được để trống';
$_['text_error_form_name_more_255']                  = 'Tên nhà cung cấp không được lớn hơn 255 kí tự';
$_['text_error_form_manufacturer_name_duplicate']    = 'Tên nhà cung cấp đã tồn tại';
$_['text_error_form_manufacturer_tax_code_duplicate']    = 'Mã số thuế đã tồn tại';
$_['text_error_form_tax_code_more_16']    = 'Mã số thuế không quá 16 ký tự';
$_['text_error_form_tax_code_special_characters']    = 'Mã số thuế không đúng định dạng';
$_['text_error_form_telephone_empty']                = 'Số điện thoại không được để trống';
$_['text_error_form_telephone_start']                = 'Bạn phải nhập kiểu số, bắt đầu bởi 0 hoặc 84!';
$_['text_error_form_telephone_wrong_format']         = 'Số điện thoại không đúng định dạng';
$_['text_error_form_telephone_more_15']              = 'Số điện thoại không quá 15 ký tự!';
$_['text_error_form_email_wrong_format']             = 'Email không đúng định dạng!';

// Select 2
$_['select2_notice_not_result']   =    "Không tìm thấy kết quả phù hợp";
$_['select2_notice_search']       =    "Tìm kiếm ...";
$_['select2_notice_load_more']    =    "Đang tải ...";

//note
$_['txt_note_delete']                             = 'Xóa';
$_['txt_note_manufacture']                        = 'Nhà cung cấp';
$_['txt_note_manufacture_one']                    = 'Nhà cung cấp này đã';
$_['txt_note_manufacture_pause']                  = 'ngừng giao dịch!';
$_['txt_note_manufacture_active']                 = 'đang giao dịch !';
$_['txt_note_active']                             = 'Giao dịch lại';
$_['txt_note_pause']                              = 'Ngừng giao dịch';
$_['txt_success_lower']                           = 'thành công';
$_['txt_manufacture_lower']                       = 'nhà cung cấp';
$_['txt_note_view_detail']                        = 'Xem chi tiết';
$_['text_discount_result']                        = 'Kết quả thao tác';
$_['text_discount_error']                         = 'Lỗi';
$_['text_failure']                                = 'Thất bại';
$_['txt_exit']                                    = 'Thoát';
$_['txt_action_one_success']                      = 'với nhà cung cấp thành công!';

// popup result
$_['text_no_results']                             = 'Không có dữ liệu!';
$_['text_manufacturer_code']                      = 'Mã nhà cung cấp';
$_['text_manufacturer_result']                    = 'Kết quả';
$_['text_manufacturer_reason']                    = 'Lí do';