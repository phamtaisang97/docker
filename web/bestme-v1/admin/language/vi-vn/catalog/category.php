<?php
// Heading
$_['heading_title']          = 'Danh mục';
$_['heading_title_edit']     = 'Sửa danh mục';
$_['heading_title_add']      = 'Thêm danh mục';

// Text
$_['text_success']           = 'Thành công: Bạn đã cập nhật thành công Danh mục!';
$_['text_list']              = 'Danh sách Danh mục';
$_['text_add']               = 'Thêm mới danh mục';
$_['text_edit']              = 'Sửa danh mục';
$_['text_default']           = 'Mặc định';
$_['text_keyword']           = 'Không sử dụng dấu cách, thay vào đó sử dụng dấu - và chắc chắn SEO URL là duy nhất trong hệ thống.';

// Column
$_['column_name']            = 'Tên Danh mục';
$_['column_sort_order']      = 'Sắp xếp';
$_['column_action']          = 'Hành động';

// Entry
$_['entry_name']             = 'Tên Danh mục';
$_['entry_description']      = 'Mô tả';
$_['entry_meta_title'] 	     = 'Tiêu đề thẻ Meta';
$_['entry_meta_keyword']     = 'Từ khoá thẻ Meta';
$_['entry_meta_description'] = 'Mô tả thẻ Meta';
$_['entry_store']            = 'Các cửa hàng';
$_['entry_keyword']          = 'Từ khoá';
$_['entry_parent']           = 'Mục cha';
$_['entry_filter']           = 'Bộ lọc';
$_['entry_image']            = 'Ảnh';
$_['entry_top']              = 'Top';
$_['entry_column']           = 'Các cột';
$_['entry_sort_order']       = 'Sắp xếp';
$_['entry_status']           = 'Trạng thái';
$_['entry_layout']           = 'Ghi đè Layout';

// Help
$_['help_filter']            = '(Tự động)';
$_['help_top']               = 'Hiển thị ở trên cùng thanh Menu. Chỉ hoạt động đối với Danh mục mức cha.';
$_['help_column']            = 'Số lượng cột sử dụng cho 3 Danh mục ở dưới. Chỉ hoạt động đối với Danh mục mức cha.';

// Error
$_['error_warning']          = 'Cảnh báo: Vui lòng kiểm tra kỹ form thông tin để xác định lỗi!';
$_['error_permission']       = 'Cảnh báo: Bạn không có quyền sửa thông tin Danh mục!';
$_['error_name']             = 'Tên Danh mục dài từ 1 đến 255 ký tự!';
$_['error_meta_title']       = 'Tiêu để thẻ Meta dài từ 1 đến 255 ký tự!';
$_['error_keyword']          = 'SEO URL đã tồn tại!';
$_['error_unique']           = 'SEO URL yêu cầu là duy nhất!';
$_['error_parent']           = 'Danh mục cha mà bạn chọn đang là Danh mục con của một Danh mục khác!';
$_['category_heading']           = 'Danh mục sản phẩm';
$_['category_add']           = 'Thêm danh mục';
$_['category_filter']           = 'Lọc';
$_['category_detail']           = 'Thông tin danh mục';
$_['category_name']           = 'Tên danh mục';
$_['category_description_text']           = 'Mô tả';
$_['category_parent']           = 'Danh mục';
$_['category_parent_des']           = 'Chọn danh mục <b>chứa danh mục vừa tạo</b>';
$_['category_choose_product']           = 'Chọn sản phẩm thuộc danh mục';
$_['category_choose_menu']           = 'Chọn menu';
$_['category_choose_menu_des']           = 'Chọn menu <b>chứa danh mục vừa tạo</b>';
$_['category_edit']           = 'Sửa danh mục';
$_['category_product_add']           = 'Thêm mới sản phẩm';
$_['category_cancel_button']           = 'Huỷ';
$_['category_save_button']           = 'Xác nhận';
$_['category_order_by']           = 'Sắp xếp theo';
$_['category_created_at']           = 'Ngày tạo';
$_['category_updated_at']           = 'Ngày sửa';
$_['category_choose_option']           = 'Tuỳ chọn';
$_['category_delete_success']         = "Xóa loại sản phẩm thành công";
$_['category_delete_error']         = "Xóa loại sản phẩm thất bại";
$_['error_category']     = 'Cảnh báo: không thể xóa loại sản phẩm này do đang được gán với %s sản phẩm! Vui lòng bỏ tất sản phẩm khỏi loại sản phẩm này rồi thực hiện lại.';
$_['category_add_success']         = "Thêm loại sản phẩm thành công";
$_['category_add_error']         = "Thêm loại sản phẩm thất bại";
$_['category_delete_product_error'] = "Bỏ gắn sản phẩm thất bại";
$_['category_delete_product_success'] = "Bỏ gắn sản phẩm thành công";
$_['category_add_product_error'] = "Gắn sản phẩm thất bại";
$_['category_add_product_success'] = "Gắn sản phẩm thành công";
