<?php
// Heading
$_['heading_title']               = 'Danh sách bài viết';
$_['heading_title_add']           = 'Thêm mới bài viết';
$_['heading_title_edit']          = 'Sửa bài viết';
$_['text_success']                = 'Thêm mới bài viết thành công';
$_['text_success_edit']           = 'Thay đổi bài viết thành công';

// breadcrumb
$_['breadcrumb_product']                 = 'Bài viết';
$_['breadcrumb_product_collection']      = 'Bài viết';
//
$_['collection_list_title']              = 'Danh sách';
$_['collection_list_filter']             = 'Lọc bài viết';
$_['breadcrumb_status']                  = 'Hiển thị';
$_['breadcrumb_status_hide']             = 'Ẩn';

$_['text_list_all']                      = 'Tất cả';
$_['text_list_title']                    = 'Tiêu đề';
$_['text_list_category']                 = 'Danh mục';
$_['text_list_author']                   = 'Tác giả';
$_['text_list_status']                   = 'Trạng thái';
$_['text_approval_status']               = 'Trạng thái phê duyệt';
$_['text_list_modified_date']            = 'Ngày cập nhật';

// filter
$_['collection_list_filter_title']             = 'Bộ lọc bài viết';
$_['collection_list_filter_title_mobile']      = 'Lọc';
$_['collection_list_filter_collection_all']    = 'Tất cả';
$_['collection_list_filter_status']            = 'Trạng thái';

$_['collection_list_filter_action']            = 'Chọn thao tác';
$_['collection_list_filter_status_show']       = 'Hiện bài viết';
$_['collection_list_filter_status_disabled']   = 'Ẩn bài viết';
$_['collection_list_filter_status_delete']     = 'Xóa bài viết';

$_['delete_confirm']       = 'Bạn có chắc chắn muốn xóa';
$_['confirm_show']         = 'Bạn có chắc muốn thay đổi ';
$_['text_add']             = 'Thêm mới ';
$_['text_search']          = 'Lọc';
$_['text_search_list']     = 'Tìm kiếm bài viết';
$_['text_cancel']          = 'Bỏ qua ';
$_['text_save']            = 'Lưu thay đổi ';

$_['text_name']               = 'Tiêu đề';
$_['entry_name_placeholder']  = 'Nhập tiêu đề';
$_['text_category']           = 'Danh mục bài viết';
$_['text_action']             = 'Hành động';
$_['text_content']            = 'Nội dung bài viết ';
$_['text_set_blog']           = 'Chọn sản phẩm vào bài viết';
$_['text_see_search']         = 'xem trước kết quả tìm kiếm';
$_['text_seo']                = 'Tùy chỉnh SEO';
$_['txt_seo_keywords_place_holder']             = 'Nhập từ khóa tại đây, mỗi từ khóa cách nhau bởi dấu phẩy';
$_['text_more']               = 'Nhập tiêu đề và mô tả để xem cách bài viết được hiển thị trên danh sách tìm kiếm.';
$_['text_tag_title']          = 'Thẻ tiêu đề';
$_['text_abouts']             = 'Thêm mô tả';
$_['text_alias']              = 'Đường dẫn / Alias';
$_['text_content_seo']        = 'Nội dung bài viết';
$_['text_image']              = 'Ảnh bài viết';
$_['text_add_image']          = 'Thêm hình ảnh';
$_['txt_place_holder_alt']    = 'Nhập alt của ảnh';

$_['txt_use_image']           = 'Sử dụng hình ảnh';
$_['txt_use_video']           = 'Sử dụng video';
$_['txt_place_holder_video']  = 'Nhập link video tại đây';
$_['txt_note']                = 'Lưu ý';
$_['txt_play_video_click']    = 'Khi click vào video thì video mới phát';

$_['sort_by']             = 'Sắp xếp: ';
$_['sort_zero']           = 'Thủ công';
$_['sort_az']             = 'Theo tên: A-Z';
$_['sort_za']             = 'Theo tên: Z-A';
$_['sort_hightolow']      = 'Theo giá: Từ cao đến thấp';
$_['sort_lowtohigh']      = 'Theo giá: Từ thấp đến cao';
$_['sort_newtoold']       = 'Theo ngày tạo: từ mới đến cũ';
$_['sort_oldtonew']       = 'Theo ngày tạo: từ cũ đến mới';

// Filter
$_['filter_list_follow']      = "Lọc sản phẩm theo";
$_['choose_filter']           = "Chọn điều kiện lọc";
$_['filter_status']           = "Trạng thái hiển thị";
$_['filter_approval_status']  = "Trạng thái phê duyệt";
$_['filter_author']           = "Tác giả";
$_['filter_manufacture']      = "Nhà cung cấp";
$_['filter_category']         = "Danh mục";
$_['filter_tag']              = "Đã được tag với";
$_['filter_length_product']   = "Số lượng sản phẩm";
// Select 2
$_['select2_notice_not_result']      =    "Không tìm thấy kết quả phù hợp";
$_['select2_notice_search']          =    "Tìm kiếm ...";
$_['select2_notice_load_more']       =    "Đang tải ...";

$_['entry_collection_placeholder']   =    "Chọn sản phẩm vào bài viết";

$_['delete_collection_confirm_show']          = 'Bạn có chắc chắn muốn xóa bài viết không ?';
$_['error_max_length_250']                    = 'Không vượt quá 250 ký tự';
$_['error_product']                           = 'Bạn phải chọn ít nhất 1 sản phẩm';
$_['error_title_add']                         = 'Tên bài viết không được để trống';
$_['error_text_char']                         = 'Không được nhập ký tự đặc biệt';
$_['error_text_title']                        = 'Thẻ tiêu đề không được để trống';
$_['error_text_check_blog']                   = 'Tạo bài viết thất bại!';
$_['txt_not_video']                           = 'Bạn phải thêm đường dẫn video';
$_['txt_video_correct']                       = 'Đường dẫn video không chính xác';

$_['choose']                                     = "Đã chọn ";
$_['item']                                       = " mục";
$_['txt_selected_all']                           = "Đã chọn tất cả trong trang này";
$_['change_success']                 = "Thay đổi thành công.";
$_['delete_success']                 = "Xóa thành công.";
$_['add_collection_new']             = "+ Thêm BST";
$_['collection_new']                 = "Bài viết";
$_['collection_filter_new']          = "Lọc";
$_['collection_count_product']       = "sản phẩm";
$_['collection_form_cancel_mobile']    = "Hủy";
$_['collection_form_save_mobile']      = "Lưu";
$_['collection_confirm_show']          = 'Bạn có chắc chắn muốn xóa ';
$_['collection_confirm_choose']        = " bài viết đã chọn ?";

$_['text_contenttext_content']        = "Nhập nội dung";
$_['text_short_name']                 = "TÓM TẮT";
$_['sum_description']                 = "Đoạn tóm tắt ngắn sẽ hiển thị trên trang chủ hoặc trang bài viết của bạn";
$_['text_alt_image']                  = "Alt của ảnh";
$_['text_source']                     = "Nguồn bài viết";
$_['text_author']                     = "Tác giả";
$_['text_category']                   = "Danh mục bài viết";
$_['text_set_tag']                    = 'Gắn vào tag';
$_['text_choose_tag']                 = 'Chọn Tag';
$_['text_choose_tag_text']            = 'Bạn có thể thêm bài viết này vào một liên kết bằng cách sử dụng nút Chọn tag';
$_['text_used_text']                  = 'Đã sử dụng:';
$_['text_used_char']                  = 'ký tự';
$_['text_add_to_tag']                 = 'Gắn vào tag';
$_['entry_tag_placeholder']           = 'Nhập tag';
$_['txt_add']                         = "Thêm bài viết";
$_['remove_all_tag']                  = 'Xóa tất cả các tag';
$_['text_add_collection_to_tag']      = 'Bạn sẽ thêm bài viết vào các tag sau:';
$_['entry_tag']                       = 'Gắn tag';
$_['notice_add_tag_error']            = "Bạn chưa thêm tag nào cho bài viết đã chọn";
$_['notice_remove_tag_error']         = "Bạn chưa chọn tag";
$_['notice_add_tag_success']          = "Thêm tag cho bài viết thành công";
$_['text_select_author']              = "Chọn tác giả...";
$_['text_select_category']            = "Chọn danh mục...";
$_['error_select_category']           = "Bạn chưa chọn danh mục";

$_['action_publish']  = "Hiển thị";
$_['action_unpublish']  = "Ẩn";
$_['action_add_tag_product']  = "Gắn tag";
$_['action_remove_tag_product']  = "Bỏ tag";
$_['action_remove_blog']  = "Xóa bài viết";
$_['label_add'] = 'Thêm';
$_['label_remove'] = 'Bỏ';
$_['label_close'] = 'Đóng';
$_['tag_for_product_choose'] = 'tag cho những bài viết đã chọn';
$_['list_tag_for_product'] = 'những tag sau cho bài viết:';
$_['use_tag_description']  = 'Mỗi tag phân cách bằng dấu phẩy (,)';
$_['choose_from_list_tag']  = "Chọn từ các tag đã có sẵn";
$_['notice_add_tag_error_max_length']   = "Tag không vượt quá 30 ký tự";
$_['notice_remove_tag_try']  = "Lỗi không thể xóa tag thử lại!";
$_['notice_remove_tag_success']         = "Xóa tag thành công";

// Select 2
$_['remove_tag_error']                        = "Lỗi thêm tag";
$_['entry_search_tag_placeholder']       = 'Tìm kiếm tag';

$_['popup_cancel_title']  = "Hủy thông tin thay đổi";
$_['popup_cancel_message']  = "Các thay đổi sẽ bị mất";
$_['popup_cancel_ok'] = "Đồng ý";

$_['text_success_add']           = 'Tạo mới bài viết thành công';
$_['text_home']                     = 'Trang chủ';
$_['text_pagination']               = 'Hiển thị %d - %d trên tổng số %d (%d trang)';
$_['product_all_status']            = 'Tất cả';
$_['entry_status_publish']   = 'Hiển thị';
$_['entry_status_unpublish'] = 'Ẩn';
$_['filter_category_placeholder']                = "Chọn loại sản phẩm";
$_['error_permission']           = 'Cảnh báo: Bạn không có quyền chỉnh sửa bài viết!';
$_['error_max_length_30']              = 'Không vượt quá 30 ký tự';
$_['error_warning']              = 'Cảnh báo: Vui lòng kiểm tra kỹ form để biết lỗi!';
$_['success']                = 'thành công';
$_['choose_all_in_filter']                                 = "Đã chọn tất cả trong trang này.";
$_['choose_in_filter']                                     = "Đã chọn";
$_['item_in_filter']                                       = "mục";
$_['text_no_results']               = 'Không có dữ liệu!';
$_['filter_tag']                                 = "Được tag với";

$_['text_close']                    = 'Đóng';
$_['text_remove']                    = 'Xóa';
$_['text_on']                    = 'Bật';
$_['text_off']                    = 'Tắt';
$_['text_select_all']               = 'Chọn tất cả';
$_['text_page_title']               = 'Bài viết';

$_['filter_status_placeholder']                  = "Chọn Trạng thái";
$_['filter_approval_status_placeholder']                  = "Chọn trạng thái phê duyệt";
$_['filter_select_category']            = "Chọn danh mục";
$_['filter_select_author']              = "Chọn tác giả";

$_['error_keyword_check_title']              = ' " tiêu đề " đã vi phạm từ khóa';
$_['error_keyword_check_content']            = ' " Nội dung bài viết " đã vi phạm từ khóa';
$_['error_keyword_check_short_content']      = ' " Đoạn tóm tắt ngắn " đã vi phạm từ khóa';

$_['error_seo_name']                = "Seo thẻ tiêu đề không được để trống";
$_['error_seo_description']         = "Seo nội dung bài viết không được để trống";
$_['error_seo_keywords']            = "Seo keywords không được để trống";
$_['error_image']                   = "Ảnh không được để trống";
$_['error_image_alt']               = "Alt ảnh không được để trống";