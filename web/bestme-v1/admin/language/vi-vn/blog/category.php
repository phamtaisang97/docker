<?php
// Heading
$_['heading_title']               = 'Danh mục bài viết';
$_['heading_blog_title']          = 'Bài viết';
$_['heading_title_add']           = 'Thêm mới danh mục';
$_['heading_title_edit']          = 'Sửa danh mục';

$_['text_save']          = 'Lưu thay đổi';
$_['text_cancel']        = 'Bỏ qua';

$_['text_seo_preview']       = 'Xem trước kết quả tìm kiếm';
$_['text_seo_config']        = 'Tùy chỉnh SEO';
$_['text_seo_guide']         = 'Nhập tiêu đề và mô tả để xem cách bài viết được hiển thị trên danh sách tìm kiếm.';
$_['text_seo_title_exp']     = '3 món quà Giáng sinh ý nghĩa nhất dành tặng trong mùa Noel 14/12/2017';
$_['text_seo_alias_exp']     = 'ba-mon-qua-y-nghia-nhat-danh-tang-ban-gai';
$_['txt_seo_title']                            = 'Thẻ tiêu đề';
$_['txt_seo_title_place_holder']               = 'Nhập tiêu đề bài viết';
$_['txt_seo_description']                      = 'Nội dung bài viết';
$_['txt_seo_description_place_holder']         = 'Thêm mô tả';
$_['txt_seo_alias']                            = 'Đường dẫn / Alias';

$_['text_add_to_menu']                   = 'Gắn vào menu';
$_['text_add_to_children']               = 'Gắn vào danh mục';
$_['text_add_collection_to_menu']        = 'Bạn sẽ thêm danh mục vào các menu sau:';
$_['text_choose_menu']        = 'Chọn Menu';
$_['text_choose_category']        = 'Chọn Danh mục con';
$_['text_choose_menu_text']   = 'Bạn có thể thêm danh mục này vào một liên kết bằng cách sử dụng nút Chọn menu';
$_['text_choose_category_text']   = 'Bạn có thể thêm danh mục con bằng cách sử dụng nút Chọn danh mục';

$_['collection_list_filter_status']            = 'Trạng thái';
$_['breadcrumb_status']                  = 'Hiển thị';
$_['breadcrumb_status_hide']             = 'Ẩn';

$_['breadcrumbs_setting_title']             = 'Cài đặt';
$_['breadcrumbs_blog_title']                = 'Bài viết';
$_['breadcrumbs_add_blog_category_title']   = 'Thêm danh mục bài viết';
$_['blog_add_category_title']               = 'Thêm danh mục bài viết';

$_['error_permission']               = 'Bạn không có quyền';
$_['error_name_exist']               = 'Tên danh mục bài viết đã tồn tại';
$_['error_name_empty']               = 'Tên danh mục không để trống';
$_['error_name_long']                = 'Tên danh mục dài hơn 100 ký tự';
$_['text_success_add']               = 'Thêm danh mục bài viết thành công';
$_['text_success_edit']              = 'Sửa danh mục bài viết thành công';

$_['category_list_filter_action']        = 'Chọn thao tác';
$_['category_list_filter_status_delete'] = 'Xóa danh mục';
$_['text_title']                         = 'Tiêu đề';
$_['text_number_blog']                   = 'Số bài viết trong danh mục';
$_['category_list_filter_status']        = 'Trạng thái';

$_['text_search']                        = 'Lọc';
$_['text_search_placeholder']            = 'Tìm kiếm danh mục';
$_['filter_list_follow']                 = "Lọc theo";
$_['choose_filter']                      = "Chọn điều kiện lọc";
$_['filter_status']                      = "Trạng thái hiển thị";

$_['placeholder_title']                  = "Nhập tiêu đề danh mục";
$_['delete_blog_category']               = "Xóa danh mục bài viết";
$_['blog_category_input_title']          = "Tiêu đề";

$_['select2_notice_not_result']          = "Không tìm thấy kết quả phù hợp";
$_['select2_notice_search']              = "Tìm kiếm ...";
$_['select2_notice_load_more']           = "Đang tải ...";

$_['entry_menu_placeholder']             = 'Chọn menu';
$_['entry_category_placeholder']             = 'Chọn danh mục';

$_['change_success']                     = "Thay đổi thành công.";
$_['delete_success']                     = "Xóa thành công.";

$_['category_confirm_show']              = 'Bạn có chắc chắn muốn xóa ';
$_['category_confirm_choose']            = " danh mục bài viết đã chọn ?";
$_['category_confirm_delete']            = "Xóa danh mục bài viết";

$_['popup_cancel_title']  = "Hủy thông tin thay đổi";
$_['popup_cancel_message']  = "Các thay đổi sẽ bị mất";
$_['popup_cancel_ok'] = "Đồng ý";
$_['text_close']                    = 'Đóng';
$_['text_remove']                    = 'Xóa';
$_['error_category']                     = 'Cảnh báo: không thể xóa danh mục bài viết đang chứa bài viết! Vui lòng bỏ tất bài viết khỏi danh mục bài viết này rồi thực hiện lại.';