<?php

// Heading
$_['heading_title']                      = 'Chatbot';

// Text
$_['text_intro_title']                   = 'Kết nối với NovaonX Chatbot';
$_['text_intro_content']                 = 'Chatbot giúp bạn gia tăng doanh số, tiết kiệm chi phí quảng cáo, chăm sóc khách hàng tốt hơn ngay từ những tương tác đầu tiên. Để sử dụng Chatbot, bạn cần đăng ký tài khoản NovaonX Chatbot.';

$_['text_intro_button']                  = 'Sử dụng Chatbot';

$_['text_setting_title']                 = 'Kết nối với Chatbot';
$_['text_setting_content']               = 'Cho phép tạo đơn hàng ngay trên Chatbot, và quản lý tập trung tại ' . PRODUCTION_BRAND_NAME . '.';
$_['text_setting_api_key_title']         = 'Mã kết nối';
$_['text_setting_api_key_placeholder']   = 'MIAY2jDKYhvYjAWfdV8TPNoQHy1nugCL';
$_['text_setting_api_key_example']       = 'Cung cấp đoạn mã trên cho Chatbot để kết nối cửa hàng của bạn với Chatbot';
$_['text_setting_api_key_button']        = 'Sao chép';

$_['text_more']                          = 'Xem hướng dẫn';

$_['text_api_key_copy_button']           = 'Sao chép';

$_['text_api_key_copy_success']          = 'Đã sao chép';
$_['text_api_key_copy_failed']           = 'Lỗi! Không thể sao chép';
