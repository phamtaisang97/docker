<?php

// Heading
$_['heading_title']                      = 'Onfluencer Marketing';
$_['heading_title_description']          = 'Onfluencer Marketing';

// Text
$_['text_intro_title']                   = 'Kết nối với Onfluencer';
$_['text_intro_content']                 = 'Nền tảng kết nối Influencer Marketing hàng đầu Việt Nam Giúp việc kết nối Influencer/KOLs trở nên dễ dàng hơn';

$_['text_intro_button']                  = 'Sử dụng Onfluencer';
$_['text_go_to_app']                     = 'Truy cập ứng dụng';

$_['text_setting_title']                 = 'Kết nối với Onfluencer';
$_['text_setting_content']               = 'Giúp việc kết nối Influencer/KOLs trở nên dễ dàng hơn.';
$_['text_setting_api_key_title']         = 'Mã kết nối';
$_['text_setting_api_key_placeholder']   = 'MIAY2jDKYhvYjAWfdV8TPNoQHy1nugCL';
$_['text_setting_api_key_example']       = 'Cung cấp đoạn mã trên cho Onfluencer để kết nối cửa hàng của bạn với Onfluencer';
$_['text_setting_api_key_button']        = 'Sao chép';

$_['text_more']                          = 'Xem hướng dẫn';

$_['text_api_key_copy_button']           = 'Sao chép';

$_['text_api_key_copy_success']          = 'Đã sao chép';
$_['text_api_key_copy_failed']           = 'Lỗi! Không thể sao chép';


// Introduce page
$_['text_intro_content_1']               = 'Nền tảng kết nối Influencer Marketing';
$_['text_intro_content_2']               = 'hàng đầu Việt Nam';
$_['text_intro_content_3']               = 'Giúp việc kết nối Influencer/KOLs trở nên dễ dàng hơn';

$_['text_description_1_title_1']         = 'TẠO NGUYÊN LIỆU QUẢNG CÁO CHÂN THỰC';
$_['text_description_1_title_2']         = 'Giúp nhãn hàng tạo ra nguồn nguyên liệu quảng cáo đa dạng trên các kênh Social';
$_['text_description_1_content']         = 'Ý tưởng quảng cáo, content, hình ảnh, video, livestream trên các kênh Website, Fanpage, Instagram, Youtube...';

$_['text_description_2_title_1']         = 'ĐỘT PHÁ HIỆU QUẢ CHIẾN DỊCH BÁN HÀNG';
$_['text_description_2_title_2']         = 'Tăng đột biến số lượng khách hàng đến với nhãn hàng từ offline và online qua các chiến dịch điển hình';
$_['text_description_2_content']         = 'Ra mắt sản phẩm mới, khai trương cửa hàng, sự kiện giảm giá, khuyến mãi với hiệu ứng sóng từ hàng trăm ngàn follower';

$_['text_description_3_title_1']         = 'GIA TĂNG ĐỘ TIN CẬY VỚI THƯƠNG HIỆU';
$_['text_description_3_title_2']         = 'Thúc đẩy quyết định mua cuối cùng của khách hàng khi họ "lang thang" tìm kiếm review trên internet.';
$_['text_description_3_content']         = '95% hành vi mua hàng của người Á Đông phụ thuộc vào đánh giá của người khác, 4/10 người đặt mua sản phẩm khi thấy Influencer đang sử dụng sản phẩm đó';

