<?php
/**
 * Created by PhpStorm.
 * User: Microsoft Windows
 * Date: 29/05/2020
 * Time: 11:21 AM
 */

// Heading
$_['heading_title']         = 'OnCustomer livechat';
$_['heading_title_description']         = 'OnCustomer livechat';
$_['heading_title_setting']             = 'Cấu hình website';

//text
$_['text_livechat_code_placeholder']             = 'Nhập mã OnCustomer livechat';
$_['text_success']                               = 'Lưu cấu hình thành công!';
$_['text_go_to_app']                             = 'Truy cập ứng dụng';