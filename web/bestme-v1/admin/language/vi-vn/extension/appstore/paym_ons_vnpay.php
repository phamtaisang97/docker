<?php
/**
 * Created by PhpStorm.
 * User: Microsoft Windows
 * Date: 29/05/2020
 * Time: 11:21 AM
 */

// Heading
$_['heading_title'] = 'Thiết lập phương thức thanh toán VNPay';
$_['heading_sub_title'] = 'Điền mã website và chuỗi tạo checksum vào ô tương ứng, nhấn Kết nối để hoàn tất thiết lập.';
$_['heading_title_description'] = 'Thanh toán VNPay';
$_['heading_title_setting'] = 'Cấu hình ứng dụng VNPay';

//text
$_['text_vnpay_code_placeholder'] = 'Nhập mã OnCustomer livechat';
$_['text_success'] = 'Lưu cấu hình thành công!';
$_['text_go_to_app'] = 'Truy cập ứng dụng';
$_['vnp_web_code_text'] = 'Mã website';
$_['vnp_hash_secret_text'] = 'Chuỗi bí mật tạo checksum';
$_['not_empty'] = 'Vui lòng điền vào trường này';
$_['max_length'] = 'Chuỗi phải nhỏ hơn 80 ký tự';
$_['special_characters'] = 'Chuỗi không chứa giá trị đặc biệt';
$_['alert_success'] = 'Cập nhật tài khoản VNPay thành công';
$_['btn_connect'] = 'Kết nối';
$_['btn_update'] = 'Cập nhật';

$_['create_vn_pay_title'] = 'Tạo tài khoản merchant VNPay';
$_['vn_pay_text'] = 'VNPay';
$_['create_vn_pay_description'] = 'Nếu bạn chưa đăng ký tài khoản merchant VNPay thì click nút Tạo tài khoản mới. <br>Nếu bạn đã có tài khoản merchant VNPay, vui lòng chuẩn bị mã website và chuỗi tạo checksum để checksum để cấu hình tại phần bên dưới.';
$_['vn_pay_login'] = 'Truy cập hệ thống merchant VNPay';
$_['create_vn_pay_btn'] = 'Tạo tài khoản mới';


