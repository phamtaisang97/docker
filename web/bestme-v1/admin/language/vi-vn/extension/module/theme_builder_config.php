<?php
// Heading
$_['heading_title']    = '<i class="fa fa-wrench" style="    font-size: 14px;margin: 0 5px;padding: 5px;background: #56a03e;color: white; border-radius: 3px;"></i></i> Cài đặt giao diện';

// Text
$_['text_extension']   = 'Phần mở rộng';
$_['text_success']     = 'Thành công: Bạn đã cập nhật thành công Mô đun Cài đặt Giao diện!';
$_['text_edit']        = 'Chỉnh sửa Mô đun cài đặt Giao diện';

// Entry
$_['entry_status']     = 'Trạng thái';

// Error
$_['error_permission'] = 'Cảnh báo: Bạn không có đủ thẩm quyền để chỉnh sửa mô đun Cài đặt Giao diện!';

// Config section
$_['text_config_section'] = 'Khu vực Cài đặt Giao diện';
$_['config_section_message'] = 'Chào mừng bạn đến với khu vực Cài đặt Giao diện!';
$_['config_section_confirm_label']   = 'Xác nhận';
$_['config_section_confirm_help']    = 'Hãy nhập "CONFIRM" để xác nhận việc Đặt lại cấu hình';
$_['config_section_confirm_button']  = 'Đặt lại cấu hình';
