<?php
// Heading
$_['heading_title'] = 'HelloWorldModule';

$_['text_module'] = 'Modules';
$_['text_success'] = 'Success: You have modified "HelloWorld Module" module!';
$_['text_edit'] = 'Edit "HelloWorld Module" Module';

// Entry
$_['entry_status'] = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify "HelloWorld Module" module!';

// Custom
$_['custom_data_label'] = 'My custom data';