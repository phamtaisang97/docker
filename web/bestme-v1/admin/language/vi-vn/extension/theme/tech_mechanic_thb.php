<?php
// Heading
$_['heading_title']                    = 'Tech Cơ khí Thái Bình Theme';

// Text
$_['text_extension']                   = 'Phần mở rộng';
$_['text_success']                     = 'Thành công: đã cập nhật Tech Cơ khí Thái Bình Theme!';
$_['text_edit']                        = 'Chỉnh sửa Tech Cơ khí Thái Bình Theme';
$_['text_general']                     = 'Chung';
$_['text_product']                     = 'Sản phẩm';
$_['text_image']                       = 'Ảnh';

// Entry
$_['entry_directory']                  = 'Thư mục Theme';
$_['entry_status']                     = 'Trạng thái';
$_['entry_product_limit']              = 'Số lượng hiển thị trên 1 trang';
$_['entry_product_description_length'] = 'List Description Limit';
$_['entry_image_category']             = 'Category Image Size (W x H)';
$_['entry_image_thumb']                = 'Product Image Thumb Size (W x H)';
$_['entry_image_popup']                = 'Product Image Popup Size (W x H)';
$_['entry_image_product']              = 'Product Image List Size (W x H)';
$_['entry_image_additional']           = 'Additional Product Image Size (W x H)';
$_['entry_image_related']              = 'Related Product Image Size (W x H)';
$_['entry_image_compare']              = 'Compare Image Size (W x H)';
$_['entry_image_wishlist']             = 'Wish List Image Size (W x H)';
$_['entry_image_cart']                 = 'Cart Image Size (W x H)';
$_['entry_image_location']             = 'Store Image Size (W x H)';
$_['entry_width']                      = 'Chiều rộng';
$_['entry_height']                     = 'Chiều cao';

// Help
$_['help_directory']                   = 'This field is only to enable older themes to be compatible with the new theme system. You can set the theme directory to use on the image size settings defined here.';
$_['help_product_limit']               = 'Determines how many catalog items are shown per page (products, categories, etc)';
$_['help_product_description_length']  = 'In the list view, short description character limit (categories, special etc)';

// Error
$_['error_permission']                 = 'Cảnh báo: Bạn không có quyền chỉnh sửa Tech Cơ khí Thái Bình Theme!';
$_['error_limit']                      = 'Product Limit bắt buộc nhập!';
$_['error_image_thumb']                = 'Product Image Thumb Size dimensions bắt buộc nhập!';
$_['error_image_popup']                = 'Product Image Popup Size dimensions bắt buộc nhập!';
$_['error_image_product']              = 'Product List Size dimensions bắt buộc nhập!';
$_['error_image_category']             = 'Category List Size dimensions bắt buộc nhập!';
$_['error_image_additional']           = 'Additional Product Image Size dimensions bắt buộc nhập!';
$_['error_image_related']              = 'Related Product Image Size dimensions bắt buộc nhập!';
$_['error_image_compare']              = 'Compare Image Size dimensions bắt buộc nhập!';
$_['error_image_wishlist']             = 'Wish List Image Size dimensions bắt buộc nhập!';
$_['error_image_cart']                 = 'Cart Image Size dimensions bắt buộc nhập!';
$_['error_image_location']             = 'Store Image Size dimensions bắt buộc nhập!';