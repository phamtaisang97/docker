<?php

// Heading
$_['heading_title']      = 'Phiếu thu';
$_['heading_title_add']  = 'Tạo phiếu thu';
$_['heading_title_list'] = 'Danh sách phiếu thu';

// Select 2
$_['select2_notice_not_result'] = "Không tìm thấy kết quả phù hợp";
$_['select2_notice_search']     = "Tìm kiếm ...";
$_['select2_notice_load_more']  = "Đang tải ...";

// breadcrumbs
$_['text_home']                  = 'Trang chủ';
$_['text_success_add']           = 'Thêm phiếu thu mới thành công!';
$_['text_success_edit']          = 'Sửa phiếu thu thành công';
$_['text_success_update']        = 'Cập nhật phiếu thu thành công';
$_['breadcrumb_cash_flow']       = 'Sổ quỹ';
$_['breadcrumb_receipt_voucher'] = 'Phiếu thu';

// button
$_['text_add_list']       = 'Thêm phiếu thu';
$_['heading_title_edit']  = 'Chi tiết phiếu thu';
$_['text_create_receipt'] = 'Tạo Phiếu';
$_['text_update_receipt'] = 'Cập nhật';
$_['text_cancel_receipt'] = 'Hủy phiếu';
$_['text_cancel']         = 'Hủy';

// title table
$_['txt_receipt_voucher_code']            = "Mã phiếu";
$_['txt_receipt_voucher_type']            = "Loại thu";
$_['txt_receipt_voucher_status']          = "Trạng thái";
$_['txt_receipt_voucher_amount']          = "Giá trị";
$_['txt_receipt_voucher_method']          = "Phương thức";
$_['txt_receipt_voucher_prepared_by']     = "Người nộp";
$_['txt_receipt_voucher_created_at']      = "Ngày tạo phiếu";
$_['txt_delivering']                      = "Hàng đang về";
$_['txt_receipted']                       = "Đã nhập hàng";
$_['txt_store']                           = "Kho nhập";
$_['txt_store_placeholder']               = "Chọn kho nhập";
$_['txt_manufacturer']                    = "Nhà cung cấp";
$_['txt_manufacturer_placeholder']        = "Chọn nhà cung cấp";
$_['txt_products']                        = "Thông tin sản phẩm";
$_['txt_product_search']                  = "Tìm kiếm sản phẩm";
$_['txt_product_name']                    = "Tên sản phẩm";
$_['txt_quantity']                        = "Số lượng";
$_['txt_quantity_placeholder']            = "Nhập số lượng";
$_['txt_value']                           = "Giá trị";
$_['txt_import_price']                    = "Giá nhập";
$_['txt_other_fee']                       = "Chi phí khác";
$_['txt_discount']                        = "Chiết khấu";
$_['txt_into_money']                      = "Thành tiền";
$_['txt_billing_info']                    = "Thông tin thanh toán";
$_['txt_total_money']                     = "Tổng tiền hàng";
$_['txt_total_pay']                       = "Tiền cần trả";
$_['txt_total_paid']                      = "Tiền đã trả";
$_['txt_total_owed']                      = "Còn nợ";
$_['txt_note']                            = "Ghi chú";
$_['txt_add_other_fee']                   = "Thêm chi phí khác";
$_['txt_fee_name_placeholder']            = "Nhập tên chi phí";
$_['txt_money_placeholder']               = "Nhập số tiền";
$_['txt_select_allocation_criteria_type'] = "Chọn tiêu thức phân bổ vào giá vốn";
$_['txt_total_fee']                       = "Tổng chi phí";
$_['txt_fee_modal_warning']               = "Phải nhập tên chi phí và số tiền đầy đủ";
$_['txt_add_manufacturer']                = "Thêm nhà cung cấp";
$_['txt_add_product']                     = "Thêm sản phẩm";
$_['txt_add_product_placeholder']         = "Nhập tên sản phẩm";
$_['txt_product_single_version']          = "Sản phẩm 1 phiên bản";
$_['txt_product_multiple_version']        = "Sản phẩm nhiều phiên bản";
$_['txt_enter_price_placeholder']         = "Nhập giá";
$_['txt_retail_price']                    = "Giá bán lẻ";
$_['txt_promotion_price']                 = "Giá sau khuyến mãi";
$_['txt_sku']                             = "Mã SKU";
$_['txt_sku_placeholder']                 = "Nhập mã SKU";
$_['txt_attribute_name']                  = "Tên thuộc tính";
$_['txt_attribute_value']                 = "Giá trị thuộc tính";
$_['txt_attribute_value_placeholder']     = "Mỗi tag phân cách bằng dấu (,)";
$_['txt_add_attribute']                   = "Thêm thuộc tính khác";
$_['txt_th_display']                      = "Hiện";
$_['txt_th_version']                      = "Phiên bản";
$_['txt_th_promotion_price']              = "Giá sau KM";
$_['text_deleted_or_no_exist']            = 'Phiếu thu không tồn tại hoặc đã bị xoá!';
$_['text_version']                        = 'Phiên bản : ';
$_['txt_auto_generated_from_order']       = 'Phiếu thu tự động tạo và gắn với chứng từ ';

//validate
$_['txt_select_at_least_1_product']                   = "Bạn phải chọn ít nhất 1 sản phẩm!";
$_['error_empty_store']                               = "Vui lòng chọn kho nhập";
$_['error_empty_manufacturer']                        = "Vui lòng chọn nhà cung cấp";
$_['error_money_paid']                                = "Tiền đã trả không được lớn hơn số tiền cần trả";
$_['error_empty_price']                               = "Vui lòng nhập giá nhập";
$_['error_empty_quantity']                            = "Vui lòng nhập số lượng";
$_['error_create_manufacturer']                       = "Tạo mới nhà cung cấp không thành công";
$_['error_empty_product_name']                        = "Vui lòng nhập tên sản phẩm";
$_['text_error_form_manufacturer_name_duplicate']     = 'Tên nhà cung cấp đã tồn tại';
$_['text_error_form_tax_code_more_16']                = 'Mã số thuế không quá 16 ký tự';
$_['text_error_form_tax_code_special_characters']     = 'Mã số thuế không đúng định dạng';
$_['text_error_form_manufacturer_tax_code_duplicate'] = 'Mã số thuế đã tồn tại';

$_['error_amount_empty']                 = 'Vui lòng nhập số tiền';
$_['error_amount_max_length']            = 'Số tiền không được vượt quá 11 chữ số';
$_['error_store_empty']                  = 'Vui lòng chọn chi nhánh';
$_['error_receipt_type_empty']           = 'Vui lòng chọn loại thu';
$_['error_receipt_type_other_empty']     = 'Vui lòng nhập loại thu khác';
$_['error_payment_method_empty']         = 'Vui lòng chọn phương thức thanh toán';
$_['error_note_max_length']              = 'Ghi chú không được vượt quá 256 ký tự';
$_['error_receipt_type_other_special']   = 'Loại thu Khác chỉ được nhập chữ và số.';
$_['error_object_info_other_special']    = 'Đối tượng chỉ được nhập chữ và số.';
$_['error_object_info_other_max_length'] = 'Đối tượng không được vượt quá 50 ký tự';

// filter
$_['text_filter_method']       = 'Lọc phiếu thu theo';
$_['method_filter']            = 'Trạng thái';
$_['choose_filter']            = 'Chọn phương thức';
$_['action_filter']            = 'Lọc';
$_['receipt_voucher_search']   = 'Tìm kiếm phiếu thu';
$_['text_manufacturer_filter'] = 'Nhà cung cấp';
$_['text_status_filter']       = 'Trạng thái';
$_['text_receipt_type_filter'] = 'Loại thu';
$_['text_method_filter']       = 'Phương thức';

// filter created at
$_['filter_date_range']             = "Khoảng thời gian";
$_['filter_created_at']             = "Ngày tạo";
$_['filter_created_at_placeholder'] = "Chọn khoảng thời gian";
$_['filter_created_at_today']       = "Hôm nay";
$_['filter_created_at_this_week']   = "Tuần này";
$_['filter_created_at_this_month']  = "Tháng này";
$_['filter_created_at_option']      = "Tùy chọn";
$_['filter_select_date_from']       = "Từ ngày";
$_['filter_select_date_to']         = "Đến ngày";
$_['filter_txt_date']               = "Ngày";

// filter status
$_['filter_status']             = "Trạng thái";
$_['filter_status_placeholder'] = "Chọn trạng thái";
$_['filter_status_complete']    = "Hoàn thành";
$_['filter_status_cancel']      = "Đã hủy";

// filter receipt voucher type
$_['filter_receipt_voucher_type']             = "Loại thu";
$_['filter_receipt_voucher_type_placeholder'] = "Chọn loại thu";

// filter cash flow method
$_['filter_cash_flow_method']             = "Phương thức";
$_['filter_cash_flow_method_placeholder'] = "Chọn phương thức";

// checkbox
$_['text_options']           = 'Chọn thao tác';
$_['txt_all']                = 'Danh sách phiếu thu';
$_['receipt_voucher_filter'] = 'Bộ lọc phiếu thu';
$_['txt_receipted_checkbox'] = 'Nhập hàng';
$_['txt_cancel_checkbox']    = 'Hủy phiếu';

// popup result
$_['text_no_results']          = 'Không có dữ liệu!';
$_['text_manufacturer_code']   = 'Mã phiếu';
$_['text_manufacturer_result'] = 'Kết quả';
$_['text_manufacturer_reason'] = 'Lí do';
$_['text_failure']             = 'Không thành công';
$_['text_success']             = 'Thành công';
$_['txt_failure_receipt']      = 'Phiếu này đã được nhập!';
$_['txt_success_receipt']      = 'Nhập hàng thành công';
$_['txt_success_cancel']       = 'Hủy phiếu thu thành công!';
$_['txt_note_view_detail']     = 'Xem chi tiết >';

// modal
$_['text_close']          = 'Đóng';
$_['txt_cancel']          = 'Bỏ qua';
$_['txt_confirm']         = 'Xác nhận';
$_['txt_heading_receipt'] = 'Nhập hàng';
$_['txt_receipt_content'] = 'Bạn có chắc chắn nhập hàng phiếu này không?';
$_['txt_heading_cancel']  = 'Hủy phiếu';
$_['txt_cancel_content']  = 'Bạn có chắc chắn hủy phiếu không?';

// form create and edit
$_['entry_form_save']          = 'Tạo phiếu';
$_['entry_form_cancel']        = 'Hủy';
$_['entry_receipt_type']       = 'Loại thu';
$_['entry_amount']             = 'Số tiền';
$_['entry_payment_method']     = 'Phương thức';
$_['entry_prepared_by']        = 'Người nộp';
$_['entry_store']              = 'Chi nhánh';
$_['entry_object']             = 'Đối tượng';
$_['entry_note']               = 'Ghi chú';
$_['entry_in_business_report'] = 'Hạch toán vào kết quả hoạt động kinh doanh';

$_['placeholder_receipt_type_other'] = 'Nhập loại thu khác';
$_['placeholder_store']              = 'Chọn chi nhánh';
$_['placeholder_receipt_type']       = 'Chọn loại phiếu thu';
$_['placeholder_payment_method']     = 'Chọn phương thức thanh toán';
$_['placeholder_prepared_by']        = 'Chọn người nộp';
$_['placeholder_select_prepared_by'] = 'Chọn người nộp';
$_['placeholder_input_prepared_by']  = 'Nhập người nộp';
$_['placeholder_amount']             = 'Nhập số tiền';
$_['placeholder_object']             = 'Chọn đối tượng';

// validate form
$_['error_cancel_receipt_voucher_id'] = 'Lỗi huỷ phiếu thu [Thiếu id phiếu thu]';
$_['error_update_receipt_voucher_id'] = 'Lỗi cập nhật phiếu thu [Không tìm thấy id của phiếu thu]';

// Error File
$_['error_permission']             = 'Cảnh báo: Yêu cầu bị từ chối do thẩm quyền!';
$_['error_filename']               = 'Cảnh báo: Tên tệp dài từ 3 đến 255!';
$_['error_folder']                 = 'Cảnh báo: Tên thư mục dài từ 3 đến 255!';
$_['error_exists']                 = 'Cảnh báo: Đã tồn tại thư mục!';
$_['error_directory']              = 'Cảnh báo: Thư mục chưa tồn tại!';
$_['error_filesize']               = 'Cảnh báo: Kích thước tệp không được quá 1MB!';
$_['error_filetype']               = 'Cảnh báo: Chỉ chấp nhận định dạng .xls và xlsx !';
$_['error_upload']                 = 'Cảnh báo: Không thể tải tệp lên hệ thống (lỗi không xác định)!';
$_['error_delete']                 = 'Cảnh báo: Không thể xoá thư mục này!';
$_['error_limit']                  = 'Cảnh báo: Số sản phẩm và phiên bản sản phẩm import tối đa là 500!';
$_['error_not_found_fields']       = 'Cảnh báo: Không tìm thấy tiêu_đề hoặc mô_tả!';
$_['error_file_incomplete']        = 'Cảnh báo: File không đầy đủ thông tin (thiếu cột)!';
$_['error_file_incomplete_in_row'] = 'Cảnh báo: Thiếu tiêu_đề, giá, đo_lường_định_giá_theo_đơn_vị, hoặc giá ưu đãi lơn hơn giá bán tại dòng ';
$_['error_unknown']                = 'Lỗi ko xác định';
$_['error_no_file_selected']       = 'Chưa có file nào được chọn!';

$_['error_input_null']         = 'Vui lòng điền vào trường này';
$_['error_input_price_null']   = 'Vui lòng nhập giá sản phẩm';
$_['error_input_product_name'] = 'Vui lòng nhập tên sản phẩm';
$_['error_type_mail']          = 'Vui lòng nhập đúng định dạng email';
$_['error_max_length_255']     = 'Không vượt quá 255 ký tự';
$_['error_max_length_15']      = 'Không vượt quá 15 ký tự';
$_['error_max_length_12']      = 'Không vượt quá 12 ký tự';
$_['error_type_phone']         = 'Vui lòng nhập số cho trường này';
$_['error_max_length_10']      = 'Không vượt quá 10 ký tự';
$_['error_max_length_100']     = 'Không vượt quá 100 ký tự';
$_['error_max_length_30']      = 'Không vượt quá 30 ký tự';
$_['error_max_length_20']      = 'Không vượt quá 20 ký tự';
$_['error_max_length_48']      = 'Không vượt quá 48 ký tự';
$_['error_type_barcode']       = 'Chỉ cho phép nhập chữ và số';
$_['error_type_sku']           = 'Không nhập tiếng việt có dấu';
$_['error_type_weight']        = "Khối lượng sản phẩm lớn hơn hoặc bằng 1 gram và nhỏ hơn 100.000 gram";
$_['error_special_char']       = "Không nhập ký tự đặc biệt";
$_['error_type_sku_same']      = 'Mã SKU đã tồn tại';
$_['error_promotion_price']    = 'Giá khuyến mại phải nhỏ hơn giá bán lẻ';
$_['error_price']              = 'Giá bán lẻ phải lơn hơn giá khuyến mại';
$_['error_text_char']          = 'Không được nhập ký tự đặc biệt';
$_['error_user_not_in_store']  = 'Bạn không có quyền thêm phiếu thu vào cửa hàng này';

$_['notice_add_tag_error_max_length'] = "Tag không vượt quá 30 ký tự";

$_['error_store'] = "Vui lòng chọn ít nhất 1 cửa hàng.";

$_['txt_complete'] = "Hoàn thành";
$_['txt_cancel']   = "Đã hủy";

// update status
$_['text_update_order_status']                   = "Cập nhật trạng thái";
$_['text_update_order_status_to_cancel_confirm'] = "Hủy phiếu thu/chi sẽ làm khoản thu/chi trong phiếu không được tính vào báo cáo. Bạn chắc chắn muốn tiếp tục không?";
$_['text_update_order_close']                    = "Đóng";
$_['text_update_order_submit']                   = "Xác nhận";