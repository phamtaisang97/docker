<?php

// Heading
$_['heading_title']                      = 'Tổng quan';
// Title chart
$_['revenue']                            = 'Doanh thu';
$_['number_of_orders']                   = 'Số lượng đơn hàng';
$_['a_new_customer']                     = 'Khách hàng mới';
$_['top_5_best_selling_products']        = 'Top 5 sản phẩm bán chạy';
$_['see_details']                        = 'Xem chi tiết';
//message
$_['no_cash_flow_data_available']        = 'Chưa có dữ liệu thống kê.';
//text
$_['customer']                           = 'Khách hàng';
$_['order']                              = 'đơn';
$_['amount']                             = 'Số lượng';
$_['order_count']                        = 'Đơn hàng';
$_['currency']                           = 'đ';
$_['_currency']                          = '₫';
$_['_trieu']                             = ' đ';

$_['text_opening_balance']               = 'Số dư đầu kỳ';
$_['text_cash_inflows']                  = 'Thu trong kỳ';
$_['text_cash_outflows']                 = 'Chi trong kỳ';
$_['text_closing_balance']               = 'Số dư cuối kỳ';

// Text chart
$_['options']                            = 'Tùy chọn';
$_['monday']                             = 'T2';
$_['tuesday']                            = 'T3';
$_['wednesday']                          = 'T4';
$_['thursday']                           = 'T5';
$_['friday']                             = 'T6';
$_['saturday']                           = 'T7';
$_['sunday']                             = 'CN';
$_['january']                            = 'Tháng 1';
$_['february']                           = 'Tháng 2';
$_['march']                              = 'Tháng 3';
$_['april']                              = 'Tháng 4';
$_['may']                                = 'Tháng 5';
$_['june']                               = 'Tháng 6';
$_['july']                               = 'Tháng 7';
$_['august']                             = 'Tháng 8';
$_['september']                          = 'Tháng 9';
$_['october']                            = 'Tháng 10';
$_['november']                           = 'Tháng 11';
$_['december']                           = 'Tháng 12';
$_['today']                              = 'Hôm nay';
$_['yesterday']                          = 'Hôm qua';
$_['one_week']                           = '1 tuần';
$_['this_week']                          = 'Tuần này';
$_['one_month']                          = '1 tháng';
$_['this_month']                         = 'Tháng này';
$_['last_month']                         = 'Tháng trước';
$_['decimal_point']                      = ',';
$_['thousand_point']                     = '.';
$_['places']                             = 'Thứ hạng';
$_['amount_money']                       = 'Số tiền';

$_['text_cash_inflows_receipt_type']     = 'Biểu đồ dòng tiền thu theo loại';
$_['text_cash_outflows_payment_type']    = 'Biểu đồ dòng tiền chi theo loại';
$_['text_cash_inflows_payment_method']   = 'Biểu đồ dòng tiền thu theo hình thức thanh toán';
$_['text_cash_outflows_payment_method']  = 'Biểu đồ dòng tiền chi theo hình thức thanh toán';

// breadcrumbs
$_['text_home']                           = 'Trang chủ';
$_['text_success_add']                    = 'Thêm phiếu chi mới thành công!';
$_['text_success_edit']                   = 'Sửa phiếu chi thành công';
$_['breadcrumb_cash_flow']                = 'Sổ quỹ';
$_['breadcrumb_overview']                 = 'Tổng quan';