<?php
// Heading
$_['blog_category_title']          = 'Danh mục bài viết';
$_['blog_category_setting_title']  = 'Cài đặt';
$_['blog_category_display_title']  = 'Hiển thị';


$_['txt_blog_category_title']                  = 'Tiêu đề';
$_['txt_blog_category_title_placeholder']      = 'Danh mục';
$_['txt_blog_category_display_list']           = 'Danh sách hiển thị';
$_['txt_blog_category_menu_edit']              = 'Chỉnh sửa menu';
