<?php
// Heading
$_['latest_blog_title']                  = 'Bài viết mới nhất';
$_['latest_blog_setting_title']   = 'Cài đặt';
$_['latest_blog_display_title']   = 'Hiển thị';


$_['txt_latest_blog_title']                  = 'Tiêu đề';
$_['txt_latest_blog_recent']                 = 'Mới nhất';
$_['txt_latest_blog_display_list']           = 'Danh sách hiển thị';
$_['txt_latest_blog_num_blog_display']       = 'Số bài viết được hiển thị';

