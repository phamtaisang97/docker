<?php
$_['slideshow_title_1'] = 'Slide show';
$_['slideshow_title_2'] = 'CÀI ĐẶT';
$_['slideshow_title_3'] = 'Thời gian chuyển slide';
$_['slideshow_title_4'] = 'HIỂN THỊ';
$_['slideshow_title_5'] = 'Thêm ảnh banner';


$_['txt_slide_image']  = 'Ảnh slide';
$_['txt_upload_image'] = 'Tải ảnh lên';
$_['txt_link']         = 'Đường dẫn (Link Url)';
$_['txt_image_des']    = 'Mô tả ảnh';

$_['txt_use_image']    = 'Sử dụng ảnh';
$_['txt_use_video']    = 'Sử dụng video';
$_['txt_video_link']   = 'Nhập link video tại đây';
$_['txt_video_play']   = 'Video phát hết mới next slideshow';
$_['txt_video_start']  = 'Khi click vào video thì video mới phát';
$_['txt_note']         = 'Chú ý';
$_['txt_not_video']    = 'Bạn phải thêm đường dẫn video vào slide số ';
$_['txt_video_correct']  = 'Đường dẫn video không chính xác cho slide số ';
// text
$_['txt_seconds']                         = 'Giây';
$_['txt_select_image']                    = 'Chọn ảnh';
$_['txt_banner_image_size_note']          = 'Kích thước: $SIZE1*$SIZE2';
$_['txt_change']                          = 'Thay đổi';
$_['txt_remove']                          = 'Xóa';
$_['txt_image_redirect_to']               = 'Ảnh liên kết tới';
$_['txt_image_redirect_placeholder']      = 'Nhập liên kết';
$_['txt_slider_description']              = 'Alt của ảnh';
$_['txt_slider_description_place_holder'] = 'Nhập alt của ảnh';
$_['text_detail']                         = 'Hướng dẫn chi tiết.';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";

