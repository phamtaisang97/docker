<?php
$_['heading_content_customize_title']       = 'Nội dung tùy chỉnh';
$_['content_customize_title']               = 'Tiêu đề';
$_['content_customize_title_placeholder']   = 'Nhập tiêu đề';
$_['content_customize_content']             = 'Nội dung';
$_['content_customize_content_placeholder'] = 'Nhập nội dung';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";
