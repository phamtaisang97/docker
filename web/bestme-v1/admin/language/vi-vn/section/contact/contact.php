<?php
// Left menu
$_['contact_title']         = 'Liên hệ';
$_['contact_setting']       = 'Cài đặt';
$_['contact_setting_title'] = 'Tiêu đề';
$_['contact_display']       = 'Hiển thị';


$_['txt_store']     = 'Store';
$_['txt_telephone'] = 'Telephone';
$_['txt_social']    = 'Social follow';
$_['txt_title']     = 'Tiêu đề';
$_['txt_content']   = 'Nội dung';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";
