<?php
// Left menu
$_['map_title']                       = 'Bản đồ';
$_['map_setting']                     = 'Cài đặt';
$_['map_setting_title']               = 'Tiêu đề';
$_['map_setting_placeholder']         = 'Bản đồ';
$_['map_display']                     = 'Hiển thị';
$_['map_display_title']               = 'Tiêu đề';
$_['map_display_address_header']      = 'Nhập địa chỉ hiển thị trên bản đồ';
$_['map_display_address_placeholder'] = 'Nhập địa chỉ hiển thị trên bản đồ';
$_['map_display_address_help']        = '(Lấy link trong google map)';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";
