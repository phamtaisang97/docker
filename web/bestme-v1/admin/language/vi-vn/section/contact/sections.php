<?php
// Section
$_['txt_title_header']   = 'Đầu trang';
$_['txt_title_footer']   = 'Chân trang';
$_['tab_title_sections'] = 'Sections';
$_['title_map']          = 'Bản đồ';
$_['title_info']         = 'Thông tin cửa hàng';
$_['title_form']         = 'Form liên hệ';

// text
$_['txt_enter_link_map']             = 'Nhập liên kết bản đồ google';
$_['txt_enter_link_map_placeholder'] = 'Nhập liên kết';
$_['txt_get_map_link']               = 'Truy cập Google map';
$_['txt_get_map_link_title']         = 'Để nhúng bản đồ vào website, bạn truy nhập Google map, chọn địa điểm nhấn nút share, chọn tab \'Nhúng/Embed\' và copy HTML dán vào đây';
$_['txt_email']                      = 'Email';
$_['txt_email_placeholder']          = 'Nhập địa chỉ email';
$_['txt_phone']                      = 'Số điện thoại';
$_['txt_phone_placeholder']          = 'txt_phone_placeholder';
$_['txt_address']                    = 'Địa chỉ';
$_['txt_address_placeholder']        = 'Nhập địa chỉ';
$_['txt_address_edit']               = 'Sửa địa chỉ';
$_['txt_title']                      = 'Tiêu đề';
$_['txt_title_placeholder']          = 'Nhập nội dung tiêu đề';
$_['txt_form_email']                 = 'Email lưu trữ dữ liệu';
$_['txt_form_email_place_holder']    = 'Nhập địa chỉ email';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";