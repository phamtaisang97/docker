<?php
// Left menu
$_['form_title']                  = 'Biểu mẫu';
$_['form_setting']                = 'Cài đặt';
$_['form_setting_title']          = 'Tiêu đề';
$_['form_data_title']             = 'Dữ liệu';
$_['form_setting_placeholder']    = 'Biểu mẫu';
$_['form_display']                = 'Display';
$_['form_data_email_des']         = 'Dữ liệu khách hàng sẽ đổ về email';
$_['form_data_email_placeholder'] = 'user@gmail.com';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";
