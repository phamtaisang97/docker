<?php
// Heading
$_['list_product_title_1'] = 'Danh mục sản phẩm';
$_['list_product_title_2'] = 'Cài đặt';
$_['list_product_title_3'] = 'Tiêu đề';
$_['list_product_title_4'] = 'HIỂN THỊ';
$_['list_product_title_5'] = 'Danh sách hiển thị';
$_['list_product_title_6'] = 'Chỉnh sửa menu';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";
