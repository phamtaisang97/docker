<?php
// Heading
$_['detail_product_title']    = 'Chi tiết sản phẩm';
$_['detail_product_display']  = 'Hiển thị';
$_['detail_product_name']     = 'Tên sản phẩm';
$_['detail_product_des']      = 'Mô tả sản phẩm';
$_['detail_product_price']    = 'Giá sản phẩm';
$_['detail_product_compare']  = 'Giá so sánh';
$_['detail_product_status']   = 'Trạng thái sản phẩm';
$_['detail_product_discount'] = 'Khuyến mãi';
$_['detail_product_vote']     = 'Đánh giá sản phẩm';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";
