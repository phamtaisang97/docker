<?php
// Heading
// Heading
$_['product_heading_title']                               = 'Sản phẩm bán chạy';
$_['best_sales_product_title']                            = 'Sản phẩm bán chạy';
$_['best_sales_product_setting_title']                    = 'CÀI ĐẶT';
$_['best_sales_product_setting_title_placeholder']        = 'Sản phẩm bán chạy';
$_['best_sales_product_setting_header']                   = 'Tiêu đề';
$_['best_sales_product_display_title']                    = 'HIỂN THỊ';
$_['best_sales_product_display_header_grid']              = 'Cài đặt lưới';
$_['best_sales_product_display_grid_quantity']            = 'Số lượng sản phẩm hiển thị';
$_['best_sales_product_setting_title']                    = 'Tiêu đề';
$_['best_sales_product_setting_title_placeholder']        = 'Nhập tiêu đề';
$_['best_sales_product_setting_sub_title_placeholder']    = 'Mô tả tiêu đề';

// text
$_['txt_collection_select']  = 'Chọn bộ sưu tập';
$_['txt_collection_edit']    = 'Sửa bộ sưu tập';
$_['txt_category_edit']      = 'Sửa loại sản phẩm';
$_['txt_auto_retrieve_data'] = 'Tự động lấy dữ liệu';
$_['txt_collection_option']  = 'Bộ sưu tập tuỳ chọn';
$_['txt_category_option']    = 'Loại sản phẩm tuỳ chọn';
$_['txt_num_display']        = 'Số lượng hiển thị/ hàng';
$_['txt_num_display_mobile'] = 'Số lượng hiển thị/ hàng trên mobile';
$_['txt_row_num_display']        = 'Số hàng hiển thị';
$_['txt_row_num_display_mobile'] = 'Số hàng hiển thị trên mobile';
$_['txt_product_suffix']     = 'Sản phẩm';
$_['txt_row_suffix']         = 'hàng';

$_['txt_auto_play_slides']        = "Tự động chạy";
$_['txt_auto_repeat_list']        = "Tự động lặp";
$_['txt_page_switching_time']     = "Thời gian chuyển trang";
$_['txt_one_seconds']       = '1 Giây';
$_['txt_two_seconds']       = '2 Giây';
$_['txt_three_seconds']     = '3 Giây';
$_['txt_four_seconds']      = '4 Giây';
$_['txt_five_seconds']      = '5 Giây';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";
