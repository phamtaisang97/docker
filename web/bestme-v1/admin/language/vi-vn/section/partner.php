<?php
// Heading
$_['partner_title'] = 'Đối tác';
// Content
$_['partner_header']          = 'Hiển thị';
$_['partner_upload_title']    = 'Tải ảnh lên';
$_['partner_url_title']       = 'Đường dẫn (link URL)';
$_['partner_url_placeholder'] = 'Gắn link hoặc tìm kiếm';
$_['partner_desc_title']      = 'Mô tả ảnh';
$_['partner_add']             = 'Thêm đối tác';

// text
$_['txt_partner_logo']                    = 'Logo đối tác';
$_['txt_partner']                         = 'Đối tác';
$_['txt_limit_per_line']                  = 'Số lượng hiển thị trên 1 hàng';
$_['txt_add_partner']                     = 'Thêm đối tác';
$_['txt_seconds']                         = 'Giây';
$_['txt_select_image']                    = 'Chọn ảnh';
$_['txt_banner_image_size_note']          = 'Kích thước: 1440*350';
$_['txt_change']                          = 'Thay đổi';
$_['txt_remove']                          = 'Xóa';
$_['txt_image_redirect_to']               = 'Ảnh liên kết tới';
$_['txt_image_redirect_placeholder']      = 'Nhập liên kết';
$_['txt_banner_description']              = 'Mô tả';
$_['txt_banner_description_place_holder'] = 'Nhập nội dung mô tả';
$_['txt_setting']                         = "Cấu hình";
$_['txt_auto_play_partner_slides']        = "Tự động chạy slide đối tác";
$_['txt_auto_repeat_partner_list']        = "Tự động lặp slide đối tác";
$_['txt_page_switching_time']     = "Thời gian chuyển trang";
$_['txt_one_seconds']       = '1 Giây';
$_['txt_two_seconds']       = '2 Giây';
$_['txt_three_seconds']     = '3 Giây';
$_['txt_four_seconds']      = '4 Giây';
$_['txt_five_seconds']      = '5 Giây';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";
$_['txt_title_partner']        = "Tiêu đề";