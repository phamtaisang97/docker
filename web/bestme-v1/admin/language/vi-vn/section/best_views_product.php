<?php
// Heading
$_['best_views_product_title']                     = 'Sản phẩm xem nhiều';
$_['best_views_product_setting_title']             = 'CÀI ĐẶT';
$_['best_views_product_setting_title_placeholder'] = 'Sản phẩm xem nhiều';
$_['best_views_product_setting_header']            = 'Tiêu đề';
$_['best_views_product_display_title']             = 'HIỂN THỊ';
$_['best_views_product_display_header_grid']       = 'Cài đặt lưới';
$_['best_views_product_display_grid_quantity']     = 'Số lượng sản phẩm hiển thị';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";
