<?php
// Section
$_['tab_title_sections']               = 'Sections';
$_['section_title_header']             = 'Đầu trang';
$_['section_title_slideshow']          = 'Slide show';
$_['section_title_list_product']       = 'Danh mục sản phẩm';
$_['section_title_hot_product']        = 'Sản phẩm nổi bật';
$_['section_title_best_sales_product'] = 'Sản phẩm bán chạy';
$_['section_title_best_views_product'] = 'Sản phẩm xem nhiều';
$_['section_title_product_detail']     = 'Chi tiết sản phẩm';
$_['section_title_banner']             = 'Banner';
$_['section_title_partner']            = 'Đối tác';
$_['section_title_blog']               = 'Blogs';
$_['section_title_footer']             = 'Chân trang';
$_['section_title_rate']             = 'Đánh giá website';

$_['section_discount_product']   = "Sản phẩm khuyến mại ";
$_['section_new_product']        = "Sản phẩm mới";
$_['text_add_section']           = "Thêm danh sách";
$_['section_customized_content'] = "Nội dung tùy chỉnh";
$_['section_ads_banner']         = "Banner quảng cáo";
$_['section_partner']            = "Đối tác";
$_['section_post']               = "Bài viết";

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";
$_['text_title_blog']           = "Tiêu đề";

$_['txt_customize_layout']     = "Tùy chỉnh bố cục giao diện";
$_['txt_note_customize_layout']  = "Cho phép tùy chỉnh bố cục bằng cách kéo thả các phần bên dưới";