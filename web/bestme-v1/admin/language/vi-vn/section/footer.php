<?php
// Title
$_['footer_title'] = 'Chân trang';
// Contact
$_['footer_contact']               = 'Liên hệ';
$_['footer_contact_more']          = 'Địa chỉ liên hệ';
$_['footer_header']                = 'Tiêu đề';
$_['footer_header_placeholder']    = 'Nhập tiêu đề';
$_['footer_subscribe_placeholder'] = 'Đăng kí nhận khuyến mại';
$_['footer_address']               = 'Địa chỉ';
$_['footer_address_placeholder']   = 'Nhập địa chỉ';
$_['footer_phone']                 = 'Số điện thoại';
$_['footer_phone_placeholder']     = 'Nhập số điện thoại';
$_['footer_email']                 = 'Email';
$_['footer_email_placeholder']     = 'Nhập email';
$_['text_add_contact']             = 'Thêm địa chỉ liên hệ';
$_['text_note_contact']            = 'Địa chỉ này chỉ hiển thị vị trí cố định trên footer';
$_['text_note_contact_more']       = 'Được thêm tối đa 9 địa chỉ, hiển thị 3 địa chỉ/dòng';
// Link
$_['footer_link']       = 'Liên kết';
$_['footer_collection'] = 'Bộ sưu tập';
$_['footer_quick_link'] = 'Liên kết nhanh';
$_['footer_product']    = 'Sản phẩm';
$_['footer_company']    = 'Công ty chúng tôi';
$_['footer_account']    = 'Tài khoản';
$_['footer_custom']     = 'Tùy chọn liên kết';
$_['footer_link_title'] = 'Tiêu đề';
$_['footer_link_menu']  = 'Danh sách hiển thị';
$_['footer_edit_menu']  = 'Chỉnh sửa menu';

// text
$_['txt_collection_display'] = 'Bộ sưu tập hiển thị';
$_['txt_menu_display']       = 'Menu hiển thị';
$_['txt_subscribe']          = 'Đăng ký theo dõi';
$_['txt_collection_edit']    = 'Sửa bộ sưu tập';
$_['txt_menu_edit']          = 'Sửa menu';
$_['txt_menu_add']           = 'Thêm menu';
$_['txt_social_network']     = 'Liên kết mạng xã hội';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";
