<?php
// Heading
$_['list_product_title']   = 'Danh sách sản phẩm';
$_['list_product_display'] = 'Hiển thị';

$_['txt_product_display_des'] = 'Số sản phẩm hiển thị tối đa trên page';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";
