<?php
// Heading
$_['filter_title']         = 'Bộ lọc';
$_['filter_title_setting'] = 'Cài đặt';
$_['filter_display']       = 'Hiển thị';

$_['filter_txt_title']           = 'Tiêu đề';
$_['filter_txt_display_list']    = 'Danh sách hiển thị';
$_['filter_txt_select_property'] = 'Chọn thuộc tính';
$_['filter_txt_selected']        = 'Đã chọn';

$_['filter_txt_supplier_filter']      = 'Lọc theo nhà cung cấp';
$_['filter_txt_product_type_filter']  = 'Lọc theo loại sản phẩm';
$_['filter_txt_property_filter']      = 'Bộ lọc thuộc tính';
$_['filter_txt_product_price_filter'] = 'Lọc theo giá sản phẩm';
$_['filter_txt_collection_filter']    = 'Lọc theo bộ sưu tập';
$_['filter_txt_tag_filter']           = 'Lọc theo tag';
$_['filter_txt_attribute_filter']     = 'Lọc theo thuộc tính';

$_['filter_txt_supplier']      = 'Nhà cung cấp';
$_['filter_txt_product_type']  = 'Loại sản phẩm';
$_['filter_txt_property']      = 'Thuộc tính';
$_['filter_txt_product_price'] = 'Giá sản phẩm';


$_['filter_txt_range']     = 'Khoảng giá';
$_['filter_txt_price_min'] = 'Giá nhỏ nhất';
$_['filter_txt_price_max'] = 'Giá lớn nhất';
$_['filter_txt_range_add'] = 'Thêm khoảng giá';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";

// v2.10.2-tuning
$_['txt_modal_title_attribute'] = 'Bộ lọc nâng cao theo thuộc tính';
$_['txt_modal_body_attribute'] = 'Danh sách các thuộc tính của bộ lọc nâng cao (hiển thị dưới bộ lọc cơ bản trên trang Danh sách sản phẩm). Nếu không muốn thuộc tính hiển thị, bạn chọn tắt thuộc tính tại danh sách dưới đây:';
$_['attribute_name'] = 'Tên thuộc tính';
$_['attribute_status'] = 'Hiển thị/Ẩn';
$_['attribute_value'] = 'Giá trị';
$_['text_no_results'] = 'Không có dữ liệu!';
$_['btn_save'] = 'Lưu lại';
$_['btn_dismiss'] = 'Bỏ qua';
