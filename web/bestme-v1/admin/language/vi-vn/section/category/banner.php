<?php
// Left menu
$_['banner_title']           = 'Banner';
$_['banner_display']         = 'Hiển thị';
$_['banner_img']             = 'Ảnh banner';
$_['banner_upload_img']      = 'Tải ảnh lên';
$_['banner_url']             = 'Đường dẫn(link URL)';
$_['banner_url_placeholder'] = 'Gắn link hoặc tìm kiếm';
$_['banner_des_img']         = 'Mô tả ảnh';
$_['banner_note_img']        = '* Kích thước banner đang dùng: 320x120px';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";
