<?php
// Section
$_['tab_title_sections']             = 'Danh mục';
$_['section_title_list_product']     = 'Danh sách sản phẩm';
$_['section_title_product_category'] = 'Danh mục sản phẩm';
$_['section_title_banner']           = 'Banner quảng cáo';
$_['section_title_filter']           = 'Bộ lọc';
$_['section_title_header']           = 'Đầu trang';
$_['section_title_footer']           = 'Chân trang';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";