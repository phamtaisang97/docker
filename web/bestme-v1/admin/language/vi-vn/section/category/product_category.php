<?php
// Heading
$_['category_product_title']         = 'Danh mục sản phẩm';
$_['category_product_title_setting'] = 'Cài đặt';
$_['category_product_title_display'] = 'Hiển thị';


$_['category_product_txt_title']        = 'Tiêu đề';
$_['category_product_txt_display_list'] = 'Danh sách hiển thị';
$_['category_product_txt_menu_edit']    = 'Chỉnh sửa menu';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";
