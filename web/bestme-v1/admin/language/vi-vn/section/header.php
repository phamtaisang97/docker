<?php
// Heading
$_['header_title']               = 'Đầu trang';
$_['header_title_2']             = 'Thông báo đầu trang';
$_['header_show_home']           = 'Chỉ hiển thị ở trang chủ';
$_['header_content']             = 'Nội dung thông báo';
$_['header_content_placeholder'] = 'Nhập nội dung thông báo';
$_['header_link']                = 'Link thông báo';
$_['header_link_placeholder']    = 'Nhập liên kết';
$_['header_logo']                = 'LOGO';
$_['header_img_logo']            = 'Tải ảnh lên';
$_['header_menu']                = 'Menu hiển thị';
$_['header_list_show']           = 'Danh sách hiển thị';

// text
$_['txt_logo_edit']                       = 'Chỉnh sửa Logo';
$_['txt_menu_edit']                       = 'Sửa menu';
$_['txt_menu_add']                        = 'Thêm menu';
$_['txt_select_image']                    = 'Chọn ảnh';
$_['txt_banner_image_size_note']          = 'Kích thước: $SIZE1*$SIZE2';
$_['txt_change']                          = 'Thay đổi';
$_['txt_remove']                          = 'Xóa';
$_['txt_image_redirect_to']               = 'Ảnh liên kết tới';
$_['txt_image_redirect_placeholder']      = 'Nhập liên kết';
$_['txt_banner_description']              = 'Mô tả';
$_['txt_banner_description_place_holder'] = 'Nhập nội dung mô tả';
$_['text_alt_of_image']                   = 'Alt của ảnh';
$_['text_enter_alt_of_image']             = 'Nhập alt của ảnh';
$_['text_logo_height']                    = 'Chiều cao của logo';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";
