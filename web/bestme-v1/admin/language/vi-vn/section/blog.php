<?php
// Heading
$_['blog_title']        = 'Blogs';
$_['blog_setting']      = 'CÀI ĐẶT';
$_['blog_title_2']      = 'Tiêu đề';
$_['blog_display']      = 'Hiển thị';
$_['blog_display_2']    = 'Danh sách hển thị';
$_['blog_list_created'] = 'Theo danh sách đã tạo';
$_['blog_choose']       = 'Tự chọn bài viết đã tạo';
$_['blog_setting_2']    = 'Cài đặt lưới';
$_['blog_display_3']    = 'Số lượng sản phẩm hiển thị';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";

// text
$_['menu_text_homepage'] = 'Trang chủ';
$_['txt_collection_select'] = 'Chọn bộ sưu tập';
$_['txt_category_select'] = 'Chọn loại sản phẩm';
$_['txt_collection_edit'] = 'Sửa bộ sưu tập';
$_['txt_category_edit'] = 'Sửa loại sản phẩm';
$_['txt_auto_retrieve_data'] = 'Tự động lấy dữ liệu';
$_['txt_collection_option'] = 'Bộ sưu tập tuỳ chọn';
$_['txt_category_option'] = 'Loại sản phẩm tùy chọn';
$_['txt_num_display'] = 'Số lượng hiển thị/ hàng';
$_['txt_num_display_mobile'] = 'Số lượng hiển thị/ hàng trên mobile';
$_['txt_row_num_display'] = 'Số hàng hiển thị';
$_['txt_row_num_display_mobile'] = 'Số hàng hiển thị trên mobile';
$_['txt_blog_suffix'] = 'bài viết';
$_['txt_row_suffix'] = 'hàng';
$_['txt_title_placeholder'] = 'Nhập tiêu đề';
