<?php
// Left menu
$_['banner_title']           = 'Banner quảng cáo';
$_['banner_display']         = 'Hiển thị';
$_['banner_img']             = 'Ảnh banner';
$_['banner_img']             = 'Ảnh banner';
$_['banner_upload_img']      = 'Tải ảnh lên';
$_['banner_url']             = 'Đường dẫn(link URL)';
$_['banner_url_placeholder'] = 'Gắn link hoặc tìm kiếm';
$_['banner_des_img']         = 'Mô tả ảnh';
$_['banner_note_img0']       = '* Kích thước banner đang dùng: 1240x320px';
$_['banner_note_img1']       = '* Kích thước banner đang dùng: 280x1200px';
$_['banner_note_img2']       = '* Kích thước banner đang dùng: 1240x200px';

// text
$_['txt_select_image']                    = 'Chọn ảnh';
$_['txt_banner_image_size_note']          = 'Kích thước: $SIZE1*$SIZE2';
$_['txt_change']                          = 'Thay đổi';
$_['txt_remove']                          = 'Xóa';
$_['txt_image_redirect_to']               = 'Ảnh liên kết tới';
$_['txt_image_redirect_placeholder']      = 'Nhập liên kết';
$_['txt_banner_description']              = 'Alt của ảnh';
$_['txt_banner_description_place_holder'] = 'Nhập alt của ảnh';
$_['text_detail']                         = 'Hướng dẫn chi tiết.';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";
