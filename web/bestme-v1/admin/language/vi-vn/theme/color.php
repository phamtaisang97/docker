<?php
// Heading
$_['heading_title']          = 'Màu sắc';
$_['heading_main']           = 'Chủ đạo';

// Text
$_['text_success']           = 'Thành công: Bạn đã chỉnh sửa Màu sắc giao diện!';

// Error
$_['error_permission']       = 'Cảnh báo: Bạn không có đủ quyền để chỉnh sửa màu sắc giao diện!';

// Colors
$_['color_background_title'] = 'Màu nền';
$_['color_text_big_header']  = 'Màu chữ tiêu đề lớn';
$_['color_text_content']     = 'Màu chữ nội dung';
$_['color_horizon_line']     = 'Màu đường kẻ ngang';
$_['color_button']           = 'Màu nút bấm';
$_['color_button_text']      = 'Màu chữ nút bấm';
$_['color_extra']            = 'Màu bổ trợ';
$_['color_sale_card']        = 'Màu thẻ khuyến mại';