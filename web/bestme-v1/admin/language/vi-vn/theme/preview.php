<?php
// Heading
$_['heading_title']          = 'Xem trước giao diện';

// Text
$_['text_success']           = 'Thành công: Bạn đã chỉnh sửa Xem trước giao diện!';
$_['confirm_box_title']          =    'Xác nhận';
$_['confirm_box_btn_yes']        =    'Đồng ý';
$_['confirm_box_btn_no']         =    'Hủy';

// Error
$_['error_permission']       = 'Cảnh báo: Bạn không có đủ quyền để chỉnh sửa xem trước giao diện!';

// Left menu
$_['menu_text_homepage']                  = 'Trang chủ';
$_['menu_text_category']                  = 'Danh sách sản phẩm';
$_['menu_text_product_detail']            = 'Chi tiết sản phẩm';
$_['menu_text_blog']                      = 'Danh mục Blog';
$_['menu_text_contact']                   = 'Trang liên hệ';

// Top menu
$_['button_save']                         = 'Lưu giao diện';
$_['button_export']                       = 'Xuất bản';