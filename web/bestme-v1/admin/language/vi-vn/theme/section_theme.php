<?php
// Theme
$_['tab_title_theme_setting']             = 'Cài đặt theme';
$_['theme_title_color']                   = 'Màu sắc';
$_['theme_title_text']                    = 'Hiển thị chữ';
$_['theme_title_favicon']                 = 'Favicon';
$_['theme_title_social']                  = 'Mạng xã hội';
$_['theme_title_checkout_page']           = 'Checkout page';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";
