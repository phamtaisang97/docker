<?php
// Heading
$_['social']                    = 'Mạng xã hội';
$_['social_setup']              = 'Cài đặt đường dẫn';
$_['social_facebook']           = 'Facebook';
$_['social_twitter']            = 'Twitter';
$_['social_instagram']          = 'Instagram';
$_['social_tumblr']             = 'Tumblr';
$_['social_youtube']            = 'Youtube';
$_['social_google_plus']        = 'Google Plus';