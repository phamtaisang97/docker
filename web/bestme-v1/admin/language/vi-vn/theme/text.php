<?php
// Heading
$_['heading_title']        = 'Hiển thị chữ';
$_['heading_font']         = 'HEADINGS';
$_['text_heading_font']     = 'Font chữ';
$_['text_heading_type']     = 'Kiểu chữ';
$_['text_heading_size']     = 'Cỡ chữ cơ bản';

// Body text
$_['body_font']             = 'BODY TEXT';
$_['text_body_font']        = 'Font chữ';
$_['text_body_type']        = 'Kiểu chữ';
$_['text_body_size']        = 'Cỡ chữ';