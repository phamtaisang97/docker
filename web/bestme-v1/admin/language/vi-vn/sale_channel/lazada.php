<?php
$_['heading_title']                     = 'Lazada';

// error, warning
$_['error_permission']                                      = 'Bạn không có quyền truy cập.';

$_['warning_validate_sync_config']                          = 'Thiếu trường trường config.';
$_['warning_validate_create_prod']                          = 'Thiếu trường dữ liệu!';
$_['warning_validate_not_exits_prod']                       = 'Không tồn tồn tại sản phẩm!';
$_['warning_validate_info_prod_not_complete']               = 'Thông tin sản phẩm không đầy đủ!';
$_['warning_connect_shop_fails']                            = 'Kết nối ko thành công!';
$_['warning_sync_processing']                               = 'Quá trình đồng bộ có thể mất 30 phút đến 1 tiếng bạn có thể quay lại sau.';

//json
$_['json_refresh_success']                                  = 'Đồng bộ dữ liệu thành công!';
$_['json_not_permission_or_missing_data']                   = 'Bạn không có quyền hoặc thiếu dữ liệu liên kết!';
$_['json_connection_success']                               = 'Liên kết thành công!';
$_['json_cancel_connection_success']                        = 'Hủy kết nối thành công';

//
$_['create_prod_lazada_success']                            = 'Tạo mới sản phẩm và liên kết thành công!';
$_['create_shop_lazada_success']                            = 'Thêm gian hàng thành công!';

//
$_['txt_version']                                           = 'Phiên bản: ';
$_['txt_sync_refresh']                                      = 'Làm mới dữ liệu';
$_['txt_sync_products']                                     = 'Đồng bộ sản phẩm';
$_['txt_sync_orders']                                       = 'Đồng bộ đơn hàng';
$_['txt_connect_prod_bestme']                               = 'Liên kết sản phẩm ' . PRODUCTION_BRAND_NAME . '';
$_['txt_cancel_connect_prod_bestme']                        = 'Hủy liên kết sản phẩm ' . PRODUCTION_BRAND_NAME . '';
$_['txt_create_prod_lazada']                                = 'Tạo mới';
$_['txt_process']                                           = 'Tiến trình';
$_['txt_confirm_box_cancel']                                = 'Bạn có chắc chắn muốn hủy liên kết không?';
$_['txt_sync_completed']                                    = 'Đồng bộ hoàn thành';

//
$_['text_current_stand']                                    = 'Gian hàng hiện tại:';
$_['text_all_status']                                       = 'Tất cả trạng thái';
$_['text_status_connection']                                = 'Trạng thái liên kết';
$_['text_connected_product_bestme']                                             = 'Đã liên kết sản phẩm ' . PRODUCTION_BRAND_NAME . '';
$_['text_not_connect_product_bestme']                                           = 'Chưa liên kết sản phẩm ' . PRODUCTION_BRAND_NAME . '';
$_['text_search_product']                                                       = 'Tìm kiếm sản phẩm';
$_['text_collect_order_information_lazada_to_bestme']                           = 'Thu thập thông tin sản phẩm từ Lazada về ' . PRODUCTION_BRAND_NAME . '';
$_['text_lazada_product_title']                                                 = 'Sản phẩm Lazada';
$_['text_info_prod_bestme_inventory']                                           = 'Tồn';
$_['text_info_prod_bestme_price']                                               = 'Giá';
$_['text_cancel_connect_prod_version']                                          = 'Hủy liên kết phiên bản';

//
$_['txt_table_column_prod_lazada']                                              = 'Sản phẩm Lazada';
$_['txt_table_column_action']                                                   = 'Hành động';
$_['txt_table_column_version']                                                  = 'Phiên bản';
$_['txt_table_column_prod_connect_bestme']                                      = 'Sản phẩm liên kết trên ' . PRODUCTION_BRAND_NAME . '';

//
$_['txt_select2_placeholder']                   = 'Chọn sản phẩm ' . PRODUCTION_BRAND_NAME . ' để liên kết';
$_['select2_notice_not_result']                 = "Không tìm thấy kết quả phù hợp";
$_['select2_notice_search']                     = "Tìm kiếm ...";
$_['select2_notice_load_more']                  = "Đang tải ...";

//
$_['txt_config']                             = 'Cấu hình';
$_['text_btn_add_shop']                      = 'Thêm gian hàng';

//
$_['text_disconnect_success']                                   = 'Ngắt kết nối thành công!';
$_['text_disconnect_shop_lazada']                               = 'Ngắt kết nối kênh bán hàng với Lazada';
$_['text_disconnect']                                           = 'Ngắt kết nối';
$_['text_expiration_date']                                      = 'Ngày hết hạn';
$_['text_note_expiration_date_1']                               = '* Sau ngày hết hạn, bạn cần ';
$_['text_note_expiration_date_2']                               = 'kết nối lại ';
$_['text_note_expiration_date_3']                               = 'với gian hàng Lazada để việc đồng bộ diễn ra bình thường.';

// v2.7.1
// table
$_['column_order_code']                                     = 'Mã đơn hàng';
$_['column_order_total']                                    = 'Tổng tiền';
$_['column_order_status_on_lazada']                         = 'Trạng thái Lazada';
$_['column_order_status_sync']                              = 'Trạng thái đồng bộ';
$_['column_action']                                         = 'Hành động';
$_['txt_column_action']                                     = 'Đồng bộ lại';

//
$_['status_sync_success']                                   = 'Đồng bộ thành công';
$_['status_sync_error']                                     = 'Đồng bộ không thành công';
$_['text_status_not_yet_sync']                              = 'Chưa đồng bộ';

//
$_['text_lazada_order_title']                                               = 'Đơn hàng Lazada';
$_['text_order_search']                                                     = 'Tìm kiếm đơn hàng';
$_['text_order_error_not_in_process_remove_after_15_day']                   = '*Đơn hàng lỗi không được xử lý trong vòng 15 ngày kể từ ngày đồng bộ về sẽ bị xóa khỏi hệ thống!';
$_['text_collect_order_information_lazada_to_bestme']                       = 'Thu thập thông tin đơn hàng từ Lazada về ' . PRODUCTION_BRAND_NAME . '';
$_['txt_sync_all']                                                          = 'Đồng bộ tất cả';
$_['text_update_information_order_bestme_admin_system']                     = 'Cập nhật thông tin đơn hàng đồng bộ thành công vào hệ thống admin ' . PRODUCTION_BRAND_NAME . '';

// status, error, solution
$_['status_lazada_pending']                                         = 'Đang chờ';
$_['status_lazada_canceled']                                        = 'Đã hủy';
$_['status_lazada_ready_to_ship']                                   = 'Sẵn sàng giao hàng';
$_['status_lazada_shipped']                                         = 'Đang giao hàng';
$_['status_lazada_delivered']                                       = 'Đã giao hàng';
$_['status_lazada_failed']                                          = 'Giao hàng thất bại';
$_['status_lazada_returned']                                        = 'Trả hàng';

$_['status_sync_error_product_not_mapping']                                 = 'Sản phẩm trong đơn hàng chưa liên kết với sản phẩm ' . PRODUCTION_BRAND_NAME . '';
$_['status_sync_error_order_has_been_process']                              = 'Đơn hàng đã được xử lý trên ' . PRODUCTION_BRAND_NAME . '.';
$_['status_sync_error_shop_expired']                                        = 'Gian hàng đã hết thời hạn kết nối ';
$_['status_sync_error_sync_timeout']                                        = 'Quá thời gian thực hiện yêu cầu đồng bộ.';
$_['status_sync_error_product_out_of_stock_on_admin']                       = 'Sản phẩm ' . PRODUCTION_BRAND_NAME . ' liên kết với sản phẩm trong đơn không đủ tồn kho';

$_['status_sync_solution_product_not_mapping']                              = 'Liên kết sản phẩm trong đơn hàng với sản phẩm ' . PRODUCTION_BRAND_NAME . ' trước, sau đó đơn hàng sẽ được đồng bộ.';
$_['status_sync_solution_order_has_been_process']                           = 'Đơn hàng đồng bộ về sẽ được tự động thay đổi trạng thái trên admin. Nếu thay đổi trạng thái thủ công thì từ đó hệ thống sẽ không tự động đồng bộ trạng thái với đơn đó nữa.';
$_['status_sync_error_shop_expired']                                        = 'Vui lòng kết nối lại gian hàng trong phần Cài đặt, sau đó đơn hàng sẽ được đồng bộ';
$_['status_sync_solution_sync_timeout']                                     = 'Hệ thống hiện tại đang xử lý quá nhiều yêu cầu, do vậy đơn hàng sẽ tự động đồng bộ lại sau khoảng thời gian đã cài đặt hoặc click button Đồng bộ lại tại phần Đồng bộ đơn hàng';
$_['status_sync_solution_product_out_of_stock_on_admin']                    = 'Điều chỉnh tồn kho sản phẩm ' . PRODUCTION_BRAND_NAME . ' liên kết trong admin, sau đó đồng bộ lại đơn hàng.';

$_['text_btn_add_shop']                                                         = 'Thêm gian hàng';
$_['text_hour_time']                                                            = 'tiếng / lần';
$_['text_day_time']                                                             = 'ngày / lần';
$_['text_sync_manually']                                                        = 'Đồng bộ thủ công';
$_['text_set_up_order_sync_schedule']                                           = 'Cài đặt lịch đồng bộ đơn hàng';
$_['text_order_information_will_be_sync_after_the_set_time_period']             = 'Thông tin đơn hàng sẽ được đồng bộ sau khoảng thời gian đã cài đặt';
$_['text_schedule']                                                             = 'Đặt lịch';

$_['txt_confirm_sync']                                                      = 'Xác nhận đồng bộ';
$_['text_confirm_sync_order_content']                                       = 'Bạn có chắc chắn muốn đồng bộ thông tin và trạng thái đơn hàng từ Lazada về ' . PRODUCTION_BRAND_NAME . ' không?';
$_['confirm_box_btn_no']                                                    = 'Đóng';
$_['confirm_box_btn_yes']                                                   = 'Đồng bộ';
$_['text_instructions_to_fix_orders_in_sync']                               = 'Hướng dẫn khắc phục đơn hàng đồng bộ lỗi';
$_['text_order_error']                                                      = 'Lỗi:';
$_['text_order_solution']                                                   = 'Khắc phục:';
$_['text_display_status_sync']                                              = 'Trạng thái đồng bộ';

$_['warning_validate_time_sync_config']                     = 'Thiếu trường config';
$_['warning_validate_shoppe_shop_exits']                    = 'Shop không tồn tại';
$_['text_save_time_config_success']                         = 'Cài đặt thời gian đồng bộ thành công.';

$_['text_shop_expired']                                     = 'Gian hàng hết hạn';
$_['text_shop_expired_note']                                = 'Gian hàng hiện tại đã hết hạn vui lòng kết nối lại trong trang cấu hình';
$_['text_go_to_config']                                     = 'Đi đến trang cấu hình';

// config new v2.11.2
$_['text_sync_orders_that_have_created_date']                                   = 'Cài đặt lịch đồng bộ đơn hàng';
$_['text_within_x_days']                                                        = 'Trong vòng $date ngày';
$_['text_product_synchronization_setting']                                      = 'Cài đặt cập nhật thông tin sản phẩm';
$_['text_allow_updating']                                                       = 'Cho phép cập nhật thông tin từ sản phẩm trên Lazada vào sản phẩm ' . PRODUCTION_BRAND_NAME . ' liên kết với nó.';
$_['text_update_inventory']                                                     = 'Cập nhật tồn kho';
$_['text_update_price']                                                         = 'Cập nhật giá bán';
$_['text_last_sync']                                                            = 'Lần đồng bộ cuối';
$_['txt_header_btn_sync_all']                                                   = 'Đồng bộ sản phẩm Lazada vào hệ thống admin để quản lý';
$_['txt_confirm_sync_content']                      = 'Thao tác này sẽ tạo mới sản phẩm và loại sản phẩm từ Lazada vào hệ thống admin ' . PRODUCTION_BRAND_NAME . ' để quản lý. Bạn có chắc chắn không?';
$_['alert_created_and_updated']                     = 'Tạo mới $COUNT_CREATE : sản phẩm';