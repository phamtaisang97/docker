<?php

// Heading
$_['heading_title']                     = 'NovaonX Social';

// Text
$_['text_intro_title']                  = 'Kết nối với NovaonX Social';
$_['text_intro_content']                = 'Công cụ bán hàng tự động trên mạng xã hội.
                                           <br>
                                           Chốt đơn tự động qua Livestream, bình luận, tạo chiến dịch thúc đẩy hành vi mua hàng của khách hàng.';

$_['text_intro_button']                 = 'Sử dụng NovaonX Social';

$_['text_more']                         = 'Xem hướng dẫn';