<?php
$_['heading_title']                     = 'Shopee';


$_['txt_shopee_connect']                     = 'Kết nối kênh bán hàng Shopee';
$_['txt_shopee_desc']                        = 'Kết nối hệ thống của ' . PRODUCTION_BRAND_NAME . ' và Shopee, giúp đưa sản phẩm từ Shopee về Website, giúp đồng bộ về giá và tồn kho';
$_['txt_shopee_req']                         = 'Bạn cần kích hoạt kênh bán hàng Shopee';
$_['txt_shopee_active_btn']                  = 'Kích hoạt kênh bán hàng';
$_['txt_sync_products']                      = 'Đồng bộ sản phẩm';
$_['txt_sync_orders']                        = 'Đồng bộ đơn hàng';
$_['txt_process']                            = 'Tiến trình';
$_['txt_sync_refresh']                       = 'Làm mới dữ liệu';
$_['txt_sync_refresh_tooltip']               = 'Thu thập thông tin sản phẩm mới nhất từ cửa hàng Shopee của bạn';
$_['txt_sync_all']                           = 'Đồng bộ tất cả';
$_['txt_sync_all_tooltip']                   = 'Cập nhật sản phẩm Shopee vào ' . PRODUCTION_BRAND_NAME . '';
$_['txt_config']                             = 'Cấu hình';
$_['txt_cancel']                             = 'Bỏ qua';
$_['txt_save']                               = 'Lưu thay đổi';
$_['txt_sync_info_title']                    = 'Thông tin đồng bộ';
$_['txt_sync_info_content']                  = 'Với các sản phẩm đã tồn tại trong kho hàng ' . PRODUCTION_BRAND_NAME . ', chọn những thông tin bạn muốn đồng bộ từ sản phẩm Shopee.';
$_['txt_product_info']                       = 'Thông tin sản phẩm';
$_['txt_disconnect_shopee_title']            = 'Ngắt kết nối với Shopee';
$_['txt_disconnect_shopee_content']          = 'Sau khi ngắt kết nối, bạn sẽ không thể cập nhật thay đổi thông tin sản phẩm trên gian hàng ở Shopee về website.';
$_['txt_disconnect_shopee']                  = 'Bạn sẽ ngắt kết nối với kênh bán hàng Shopee';
$_['txt_disconnect']                         = 'Ngắt kết nối';
$_['txt_disconnect_alert_title']             = 'Ngắt kết nối kênh bán hàng';
$_['txt_disconnect_alert_content']           = 'Bạn có chắc chắn muốn ngắt kết nối với Shopee';
$_['txt_disconnect_fail']                    = 'Ngắt kết nối ko thành công';
$_['txt_confirm_sync']                       = 'Xác nhận đồng bộ';
$_['txt_confirm_sync_content']               = 'Thao tác này sẽ tạo mới sản phẩm và loại sản phẩm từ Shopee vào hệ thống admin ' . PRODUCTION_BRAND_NAME . ' để quản lý. Bạn có chắc chắn không?';
$_['txt_sync_completed']                     = 'Đồng bộ hoàn thành';
$_['txt_classify']                           = 'Phân loại hàng';
$_['error_permission']                       = 'Cảnh báo: Bạn không có quyền kết nối shop!';
$_['connection_failed']                      = 'Cảnh báo: Kết nối không thành công';

//entry
$_['entry_name']                     = 'Tên';
$_['entry_desc']                     = 'Mô tả';
$_['entry_price']                    = 'Giá';
$_['entry_quantity']                 = 'Số lượng';
$_['entry_weight']                   = 'Khối lượng';

$_['alert_created']                               = 'Tạo mới thành công $COUNT_CREATE sản phẩm';
$_['alert_updated']                               = 'Cập nhật thành công $COUNT_UPDATE sản phẩm';
//$_['alert_created_and_updated']                   = 'Tạo mới $COUNT_CREATE : sản phẩm, Cập nhật : $COUNT_UPDATE sản phẩm';
$_['alert_created_and_updated']                   = 'Tạo mới $COUNT_CREATE : sản phẩm';

//warning
$_['warning_leave_page']                     = 'Việc rời trang web sẽ dừng việc đồng bộ dữ liệu từ Shopee.';
$_['warning_validate_sync_config']           = 'Thiếu trường trường config';
$_['warning_sync_fail']                      = 'Không thành công';

// error
$_['warning_active_fail']                      = 'Kích hoạt không thành công';
$_['shopee_list_sync_title']                   = 'Danh sách sản phẩm';
$_['shopee_product_title']                     = 'Sản phẩm Shopee';

// filter
$_['product_filter']                             = 'Bộ lọc sản phẩm';
$_['filter_list_follow']                         = "Lọc sản phẩm theo";
$_['choose_filter']                              = "Chọn điều kiện lọc...";
$_['filter_status']                              = "Trạng thái";
$_['filter_status_placeholder']                  = "Chọn trạng thái";
$_['action_filter']       = 'Lọc';

$_['choose']                                     = "Đã chọn";
$_['text_action_sync']                           = "Cập nhật";
$_['text_action_create']                         = "Tạo mới";
$_['text_action_update']                         = "Cập nhật";
$_['change_status_success']                      = "Thay đổi status thành công";
$_['change_status_error']                        = "Thay đổi status thất bại";
//column
$_['product_sku']                                = 'SKU';
$_['txt_product_title']                          = 'Sản phẩm';
$_['product_status']                             = 'Trạng thái';
$_['product_action']                             = 'Hành động';
$_['entry_category']                             = 'Danh mục';
$_['entry_category_select']                      = 'Chọn danh mục';
$_['product_all_status']                         = 'Tất cả';
$_['entry_status_duplicate']                     = 'Sản phẩm bị trùng SKU';
$_['entry_status_duplicate_version_product']     = 'Phiên bản sản phẩm bị trùng SKU trên ' . PRODUCTION_BRAND_NAME . '';
$_['entry_status_exist']                         = 'Đã có sản phẩm ở ' . PRODUCTION_BRAND_NAME . '';
$_['entry_status_without_sku']                   = 'Sản phẩm bị thiếu SKU';
$_['entry_status_create']                        = 'Có thể tạo mới';
$_['text_duplicate_sku']                         = 'Sản phẩm bị trùng SKU: ';
$_['text_duplicate_sku_parent']                  = 'Sản phẩm bị trùng SKU sản phẩm cha: ';
$_['text_duplicate_sku_child']                   = 'Sản phẩm bị trùng SKU sản phẩm con: ';
$_['multi_version']                              = 'Nhiều phiên bản';
$_['text_options']                               = 'Chọn thao tác';
$_['text_action_create_tooltip']                 = 'Tạo sản phẩm mới tại ' . PRODUCTION_BRAND_NAME . ' bằng thông tin sản phẩm từ Shopee';
$_['text_action_sync_tooltip']                   = 'Cập nhật thông tin sản phẩm tại ' . PRODUCTION_BRAND_NAME . ' bằng thông tin đã thu thập từ Shopee';
$_['missing_sku']                                = 'Sản phẩm thiếu SKU';
$_['entry_status_exist_shopee']                  = 'Đã có sản phẩm trên Shoppee';
$_['more_than_duplicate']                        = 'Có nhiều hơn 1 sản phẩm trùng sku của version';
$_['not_all_variation_product']                  = 'Không có sản phẩm phiên bản (tất cả sản phẩm phiên bản trên Shopee thiếu SKU)';
$_['confirm_box_btn_no']                         = 'Đóng';
$_['confirm_box_btn_yes']                        = 'Đồng bộ';
$_['warning_sync_processing']                    = 'Vui lòng đợi một chút trong lúc hệ thống đồng bộ.';

$_['text_detail']                 = 'Hướng dẫn chi tiết.';

// v2.6.1
// text
$_['text_shopee_order_title']                                               = 'Đơn hàng Shopee';
$_['text_order_search']                                                     = 'Tìm kiếm đơn hàng';
$_['text_current_stand']                                                    = 'Gian hàng hiện tại:';
$_['text_display_status_sync']                                              = 'Trạng thái đồng bộ';
$_['text_instructions_to_fix_orders_in_sync']                               = 'Hướng dẫn khắc phục đơn hàng đồng bộ lỗi';
$_['text_order_error']                                                      = 'Lỗi:';
$_['text_order_solution']                                                   = 'Khắc phục:';
$_['text_confirm_sync_order_content']                                       = 'Bạn có chắc chắn muốn đồng bộ thông tin và trạng thái đơn hàng từ Shopee về ' . PRODUCTION_BRAND_NAME . ' không?';
$_['text_order_error_not_in_process_remove_after_15_day']                   = '*Đơn hàng lỗi không được xử lý trong vòng 15 ngày kể từ ngày đồng bộ về sẽ bị xóa khỏi hệ thống!';
$_['text_collect_order_information_shopee_to_bestme']                       = 'Thu thập thông tin đơn hàng từ Shopee về ' . PRODUCTION_BRAND_NAME . '';
$_['text_update_information_order_bestme_admin_system']                     = 'Cập nhật thông tin đơn hàng đồng bộ thành công vào hệ thống admin ' . PRODUCTION_BRAND_NAME . '';

// table
$_['column_order_code']                                     = 'Mã đơn hàng';
$_['column_order_total']                                    = 'Tổng tiền';
$_['column_order_status_on_shopee']                         = 'Trạng thái Shopee';
$_['column_order_status_sync']                              = 'Trạng thái đồng bộ';
$_['column_action']                                         = 'Hành động';
$_['txt_column_action']                                     = 'Đồng bộ lại';

// status, error, solution
$_['status_un_paid']                                        = 'Chờ xác nhận';
$_['status_to_ship']                                        = 'Chờ lấy hàng (Đã xác nhận)';
$_['status_completed']                                      = 'Đã giao';
$_['status_cancelled']                                      = 'Đã hủy';
$_['status_to_return']                                      = 'Yêu cầu Trả hàng/Hoàn tiền';
$_['status_to_confirm_receive']                             = 'Xác nhận đã nhận hàng';
$_['status_retry_ship']                                     = 'Giao lại';
$_['status_shipped']                                        = 'Đã giao';
$_['status_sync_success']                                   = 'Đồng bộ thành công';
$_['status_sync_error']                                     = 'Đồng bộ không thành công';
$_['text_all_status']                                       = 'Tất cả trạng thái';
$_['text_status_not_yet_sync']                              = 'Chưa đồng bộ';

$_['status_sync_error_order_has_been_process']                              = 'Đơn hàng đã được xử lý trên ' . PRODUCTION_BRAND_NAME . '.';
$_['status_sync_error_product_not_sync']                                    = 'Sản phẩm trong đơn hàng chưa được đồng bộ.';
$_['status_sync_error_sync_timeout']                                        = 'Quá thời gian thực hiện yêu cầu đồng bộ.';
$_['status_sync_error_product_deleted_on_shopee']                           = 'Sản phẩm đã được đồng bộ về nhưng bị xóa trên Shopee.';
$_['status_sync_error_product_out_of_stock_on_admin']                       = 'Sản phẩm trong đơn không đủ tồn kho trên admin.';

$_['status_sync_solution_order_has_been_process']                           = 'Đơn hàng đồng bộ về sẽ được tự động thay đổi trạng thái trên admin. Nếu thay đổi trạng thái thủ công thì từ đó hệ thống sẽ không tự động đồng bộ trạng thái với đơn đó nữa.';
$_['status_sync_solution_product_not_sync']                                 = 'Vào Shopee -> Đồng bộ sản phẩm để đồng bộ sản phẩm trong đơn về trước. Khi đồng bộ thành công sản phẩm thì đơn hàng sẽ được tự động đồng bộ lại.';
$_['status_sync_solution_sync_timeout']                                     = 'Hệ thống hiện tại đang xử lý quá nhiều yêu cầu, do vậy đơn hàng sẽ tự động đồng bộ lại sau khoảng thời gian đã cài đặt hoặc click nút Đồng bộ lại tại phần Đồng bộ đơn hàng.';
$_['status_sync_solution_product_deleted_on_shopee']                        = '';
$_['status_sync_solution_product_out_of_stock_on_admin']                    = 'Điều chỉnh tồn kho sản phẩm trong admin, sau đó đồng bộ lại đơn hàng.';

// config
$_['text_btn_add_shop']                                                         = 'Thêm gian hàng';
$_['text_hour_time']                                                            = 'tiếng/lần';
$_['text_day_time']                                                             = 'ngày/lần';
$_['text_sync_manually']                                                        = 'Đồng bộ thủ công';
$_['text_set_up_order_sync_schedule']                                           = 'Cài đặt lịch đồng bộ đơn hàng';
$_['text_order_information_will_be_sync_after_the_set_time_period']             = 'Thông tin đơn hàng sẽ được đồng bộ sau khoảng thời gian đã cài đặt';
$_['text_schedule']                                                             = 'Đặt lịch';

// config new v2.11.2
$_['text_sync_orders_that_have_created_date']                                   = 'Cài đặt lịch đồng bộ đơn hàng';
$_['text_within_x_days']                                                        = 'Trong vòng $date ngày';
$_['text_product_synchronization_setting']                                      = 'Cài đặt cập nhật thông tin sản phẩm';
$_['text_allow_updating']                                                       = 'Cho phép cập nhật thông tin từ sản phẩm trên Shopee vào sản phẩm ' . PRODUCTION_BRAND_NAME . ' liên kết với nó.';
$_['text_update_inventory']                                                     = 'Cập nhật tồn kho';
$_['text_update_price']                                                         = 'Cập nhật giá bán';
$_['text_last_sync']                                                            = 'Lần đồng bộ cuối';

$_['warning_validate_time_sync_config']                     = 'Thiếu trường config';
$_['warning_validate_shoppe_shop_exits']                    = 'Shop không tồn tại';

// v2.8.1
$_['text_status_connection']                                                    = 'Trạng thái liên kết';
$_['text_connected_product_bestme']                                             = 'Đã liên kết sản phẩm ' . PRODUCTION_BRAND_NAME . '';
$_['text_not_connect_product_bestme']                                           = 'Chưa liên kết sản phẩm ' . PRODUCTION_BRAND_NAME . '';

$_['text_info_prod_bestme_inventory']                                           = 'Tồn';
$_['text_info_prod_bestme_price']                                               = 'Giá';
$_['text_cancel_connect_prod_version']                                          = 'Hủy liên kết phiên bản';
$_['txt_connect_prod_bestme']                                                   = 'Liên kết sản phẩm ' . PRODUCTION_BRAND_NAME . '';
$_['txt_cancel_connect_prod_bestme']                                            = 'Hủy liên kết sản phẩm ' . PRODUCTION_BRAND_NAME . '';
$_['txt_create_prod_lazada']                                                    = 'Tạo mới';
$_['text_search_product']                                                       = 'Tìm kiếm sản phẩm';
$_['txt_table_column_version']                                                  = 'Phiên bản';
$_['txt_table_column_prod_connect_bestme']                                      = 'Sản phẩm liên kết trên ' . PRODUCTION_BRAND_NAME . '';

//
$_['txt_select2_placeholder']                                                   = 'Chọn sản phẩm ' . PRODUCTION_BRAND_NAME . ' để liên kết';
$_['select2_notice_not_result']                                                 = "Không tìm thấy kết quả phù hợp";
$_['select2_notice_search']                                                     = "Tìm kiếm ...";
$_['select2_notice_load_more']                                                  = "Đang tải ...";

//
$_['txt_confirm_box_cancel']                                                    = 'Bạn có chắc chắn muốn hủy liên kết không?';
$_['txt_version']                                                               = 'Phiên bản:';
$_['txt_header_btn_sync_refresh']                                               = 'Thu thập thông tin mới nhất từ cửa hàng Shopee của bạn';
$_['txt_header_btn_sync_all']                                                   = 'Đồng bộ sản phẩm Shopee vào hệ thống admin để quản lý';
$_['create_prod_shopee_success']                                                = 'Tạo mới sản phẩm và liên kết thành công!';