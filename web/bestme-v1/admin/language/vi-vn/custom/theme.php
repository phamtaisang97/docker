<?php
// Theme default
$_['heading_title']                              = 'Giao diện';
$_['theme_default_title']                        = 'Giao diện hiện tại';
$_['theme_default_description']                  = 'Khách hàng sẽ thấy giao diện này khi họ xem website của bạn.';
$_['theme_default_text']                         = 'Giao diện mặc định';
$_['theme_default_setting_button']               = 'Thiết lập giao diện';

// Theme store
$_['theme_store']                                = 'Kho giao diện';
$_['theme_store_description']                    = 'Danh sách tất những giao diện bạn đã sử dụng miễn phí hoặc trả phí.';
$_['theme_store_your_theme']                     = 'Giao diện của bạn';
$_['theme_store_your_theme_description']         = 'Danh sách các giao diện đã được mua cho website';
$_['theme_store_action']                         = 'Hành động';
$_['theme_store_customize']                      = 'Chỉnh sửa';
$_['theme_store_bestme']                         = 'Kho giao diện Bestme';
$_['theme_store_bestme_more']                    = 'Xem thêm các giao diện khác trên kho giao diện của chúng tôi.';
$_['theme_store_button']                         = 'Kho giao diện';

$_['theme_delete_sample_data'] = "Xoá sản phẩm mẫu";
$_['theme_delete_theme'] = "Xóa theme";
$_['theme_load_sample_data'] = "Tải sản phẩm mẫu";
$_['theme_using'] = "Đang sử dụng";
$_['theme_use'] = "Sử dụng";
$_['theme_delete'] = "Xóa";
$_['theme_load'] = "Tải";
$_['theme_sample_data_is_faulty_undefined'] = "dữ liệu mẫu bị lỗi. Chi tiết: không xác định";
$_['theme_sample_data_successfully'] = "dữ liệu mẫu thành công";
$_['theme_sample_data_is_faulty_detail'] = "dữ liệu mẫu bị lỗi. Chi tiết:";
$_['error_upload_theme'] = "Lỗi upload file";
$_['error_upload_theme_type'] = "Lỗi định dạng file upload";
$_['upload_theme_success'] = "Thêm theme thành công";
$_['upload_theme_file_not_isset'] = "Lỗi file upload không tồn tại";
$_['button_upload_theme'] = "Tải theme";
$_['button_fix_theme'] = "Sửa theme";
$_['error_upload_theme_or_file_isset'] = "Lỗi file upload không tồn tại hoăc đã tồn tại theme";
$_['develope_theme'] = "Phát triển giao diện";
$_['error_upload_theme_file_require'] = 'File cần thiết cho theme không tồn tại';
$_['alert_when_remove_theme_in_use'] = "Không thể xóa theme đang sử dụng!";
$_['confirm_remove_theme']          = "Bạn có chắc chắn muốn xóa theme này!";
$_['alert_remove_success']          = "Xóa thành công!";
$_['alert_remove_fail']             = "Xóa không thành công!";
$_['file_theme_isset']              = "Theme đã tồn tại hãy xóa trước khi upload";
$_['error_change_theme']            = "Thay đổi không thành công";
$_['theme_title']                                = 'Giao diện';