<?php
// Website
$_['heading_title']                              = 'Cấu hình';
$_['website_title']                              = 'Cấu hình website';
$_['website_description']                        = 'Thông tin sẽ được hiển thị khi khách hàng tìm kiếm website của bạn.';
$_['website_homepage_title']                     = 'Tiêu đề trang chủ';
$_['website_homepage_title_placeholder']         = 'Nhập tiêu đề bài viết';
$_['website_homepage_description']               = 'Mô tả trang chủ';
$_['website_homepage_description_placeholder']   = 'Nhập mô tả để nâng cao xếp hạng trên công cụ  tìm kiếm Google';

// MaxLead
$_['maxlead_title']                               = 'MaxLead';
$_['maxlead_description']                         = 'Max Lead hỗ trợ nhiều cách liên hệ (Facebook, Zalo, LiveChat,...) nhằm thu hút tối đa khách hàng tiềm năng truy cập website của bạn; cung cấp báo cáo đo lường chuyển đổi được xây dựng dễ dàng, tiện lợi.<br>Tìm hiểu thêm về Max Lead <a href="' . AUTOADS_MAXLEAD_SERVER . '" target="_blank" class="link-here">tại đây</a>.';
$_['maxlead_code']                                = 'Mã MaxLead';
$_['maxlead_code_placeholder']                    = 'Nhập mã MaxLead';

// Google Tracting Manager (GTM) Code
$_['gtm_code_title']                               = 'Google Tag Manager (GTM)';
$_['gtm_code_description']                         = 'Trình quản lý thẻ của Google (Google Tag Manager) là một hệ thống quản lý thẻ cho phép bạn cập nhật mã theo dõi và các đoạn mã liên quan (gọi chung là "thẻ") trên trang web hoặc ứng dụng dành cho thiết bị di động của bạn một cách nhanh chóng và dễ dàng.<br>Tìm hiểu thêm về GTM <a href="' . "#" . '" target="_blank" class="link-here">tại đây</a>.';
$_['gtm_code_header']                              = 'Mã GTM gắn lên đầu trang';
$_['gtm_code_header_placeholder']                  = 'Nhập mã GTM gắn lên đầu trang';
$_['gtm_code_body']                                = 'Mã GTM gắn lên thân trang';
$_['gtm_code_body_placeholder']                    = 'Nhập mã GTM gắn lên thân trang';

// Google Analytics
$_['google_title']                               = 'Google Analytics';
$_['google_description']                         = 'Google Analytics cho phép bạn theo dõi và xác định đặc điểm người dùng truy cập website bán hàng của mình, cũng như cung cấp các báo cáo số liệu đo lường hiệu quả hoạt động marketing.<br>Tìm hiểu thêm về Google Analytics <a href="' . GG_ANALYTICS_SERVER . '" target="_blank" class="link-here">tại đây</a>.';
$_['google_code']                                = 'Mã Google Analytics';
$_['google_code_placeholder']                    = 'Nhập mã Google Analytics';

// Google Adwords
$_['google_aw_title']                               = 'Theo dõi chuyển đổi Google Ads';
$_['google_aw_description']                         = 'Tính năng theo dõi chuyển đổi của Google Ads cho bạn biết hành động xảy ra sau khi khách hàng nhấp vào quảng cáo của bạn.<br>Tìm hiểu thêm về Theo dõi chuyển đổi Google Ads <a href="' . "#" . '" target="_blank" class="link-here">tại đây</a>.';
$_['google_aw_code']                                = 'Mã Thẻ Google Ads toàn cầu';
$_['google_aw_code_placeholder']                    = 'Nhập mã thẻ Google Ads toàn cầu';

// Google Adwords event tracking
$_['text_config_google_aw_code_cart']                           = 'Mã chuyển đổi trang giỏ hàng';
$_['text_config_google_aw_code_cart_placeholder']               = 'Nhập mã chuyển đổi trang giỏ hàng';
$_['text_config_google_aw_code_order']                          = 'Mã chuyển đổi trang thanh toán';
$_['text_config_google_aw_code_order_placeholder']              = 'Nhập mã chuyển đổi trang thanh toán';
$_['text_config_google_aw_code_cart_success']                   = 'Mã chuyển đổi trang mua hàng thành công';
$_['text_config_google_aw_code_cart_success_placeholder']       = 'Nhập mã chuyển đổi trang mua hàng thành công';

// Facebook
$_['facebook_title']                             = 'Facebook Pixel';
$_['facebook_description']                       = 'Facebook Pixel giúp bạn tạo những chiến dịch quảng cáo nhắm tới tập khách hàng mới có đặc điểm giống với những khách hàng đã từng mua hàng tại website của bạn.<br>Tìm hiểu thêm về Facebook Pixel <a href="' . FB_PIXEL_SERVER . '" target="_blank" class="link-here">tại đây</a>.';
$_['facebook_pixel']                             = 'Mã Facebook Pixel';
$_['facebook_placeholder']                       = 'Nhập mã Facebook Pixel';

// Facebook event tracking
$_['text_config_facebook_pixel_cart']                           = 'Mã Facebook pixel trang giỏ hàng';
$_['text_config_facebook_pixel_cart_placeholder']               = 'Nhập mã Facebook pixel trang giỏ hàng';
$_['text_config_facebook_pixel_order']                          = 'Mã Facebook pixel trang thanh toán';
$_['text_config_facebook_pixel_order_placeholder']              = 'Nhập mã Facebook pixel trang thanh toán';
$_['text_config_facebook_pixel_cart_success']                   = 'Mã Facebook pixel trang mua hàng thành công';
$_['text_config_facebook_pixel_cart_success_placeholder']       = 'Nhập mã Facebook pixel trang mua hàng thành công';

// site map
$_['txt_site_map_auto']                                = 'Tự động';
$_['txt_site_map_manual']                              = 'Tùy chỉnh';

// Button
$_['button_save']                                = 'Lưu cấu hình';

$_['setting_seo']                                = 'Cấu hình SEO';
$_['site_map_content_text']                      = 'Nội dung Sitemap';
$_['site_map_content_placeholder']               = 'Nội dung file sitemap.xml cho website của bạn.';
$_['robots_content_text']                        = 'Nội dung Robots.txt';
$_['robots_content_placeholder']                 = 'Nội dung file Robots.txt cho website của bạn.';

$_['text_detail']                 = 'Hướng dẫn chi tiết.';