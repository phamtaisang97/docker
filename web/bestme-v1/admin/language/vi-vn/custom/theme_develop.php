<?php
// Heading
$_['heading_title']                      = 'Danh sách giao diện';

$_['all_theme']                          = 'Tất cả giao diện';
$_['title_theme']                        = 'Tên theme';
$_['action_theme']                       = 'Sửa / Xóa';
$_['button_upload_theme']                = "Tải theme";
$_['button_fix_theme']                   = "Sửa theme";
$_['theme_using']                        = "Đang sử dụng";
$_['theme_use']                          = "Sử dụng";
$_['theme_delete']                       = "Xóa";
$_['theme_load']                         = "Tải";
$_['theme_store_action']                 = 'Hành động';
$_['theme_delete_sample_data']           = "Xoá dữ liệu mẫu";
$_['theme_delete_theme']                 = "Xóa theme";
$_['theme_load_sample_data']             = "Tải dữ liệu mẫu";
$_['error_theme']                        = "Chưa có giao diện";
$_['alert_when_remove_theme_in_use']     = "Không thể xóa theme đang sử dụng!";
$_['confirm_remove_theme']               = "Bạn có chắc chắn muốn xóa theme này!";
$_['alert_remove_success']               = "Xóa thành công!";
$_['alert_remove_fail']                  = "Xóa không thành công!";
$_['theme_fix']                          = "Sửa theme";
$_['download_theme']                     = "Tải theme (.zip)";

// error
$_['error_partner_theme_dir_not_found']        = "Khởi tạo công cụ phát triển lỗi (chưa phân quyền đầy đủ cho đối tác)! Vui lòng liên hệ " . PRODUCTION_BRAND_NAME . " để được hỗ trợ.";
