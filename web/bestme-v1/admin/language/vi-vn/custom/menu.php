<?php
// Text
$_['heading_title']             = 'Menu';
$_['heading_title_add']         = 'Thêm menu';
$_['heading_title_edit']        = 'Sửa menu';
$_['add_menu_text']             = 'Thêm menu';
$_['menu_description_one']      = 'Danh sách menu sẽ giúp khách hàng duyệt web của bạn dễ dàng hơn. ';
$_['menu_description_two']      = 'Nội dung chọn chỉnh sửa hiển thị trong giao diện website của bạn. ';
$_['menu_link_text']            = 'Liên kết';
$_['menu_edit_text']            = 'Chỉnh sửa Menu';
$_['menu_title_text']           = 'Tiêu đề';
$_['menu_choose_option']        = 'Chọn liên kết';
$_['menu_add_more_text']        = 'Thêm liên kết';
$_['menu_description_seo_text'] = 'Định danh làm đường dẫn hiển thị trên thanh địa chỉ trình duyệt và có thể được dùng để truy cập các thuộc tính của Menu trong Liquid';
$_['menu_delete_text']          = 'Xoá';
$_['menu_cancel_text']          = 'Huỷ';
$_['menu_submit_text']          = 'Xác nhận';
$_['menu_text_heading']         = 'Website';
$_['menu_text']                 = 'Menu';
$_['menu_item_text']            = 'Menu items';
$_['menu_seo_title']            = 'SEO';
$_['menu_item_edit']            = 'Chỉnh sửa Menu item';
$_['menu_item_connect']         = 'Liên kết';
$_['menu_text_edit']            = 'Sửa';
$_['menu_text_link_select']     = 'Chọn liên kết';
$_['menu_text_add_link']        = 'Thêm liên kết';
$_['choose_link']               = 'Chọn liên kết';

// Error
$_['error_warning']          = 'Cảnh báo: Vui lòng kiểm tra kỹ form nội dung để biết lỗi!';
$_['error_permission']       = 'Cảnh báo: Bạn không có quyền chỉnh sửa Sản phẩm!';

// define built in pages
$_['txt_page_home']                     = 'Trang chủ';
$_['txt_page_product']                  = 'Sản phẩm';
$_['txt_page_contact']                  = 'Liên hệ';
$_['txt_page_login']                    = 'Đăng nhập';
$_['txt_page_register']                 = 'Đăng ký';
$_['txt_page_policy']                   = 'Chính sách thành viên';
$_['txt_page_about']                    = 'Về chúng tôi';
$_['txt_page_blog']                     = 'Bài viết';
$_['txt_page_checkout_cart']            = 'Giỏ hàng';
$_['text_page_account_profile']         = 'Thông tin tài khoản';

$_['not_attach']                = "Chưa gắn vào menu";
$_['attach']                    = "Đã gắn vào menu:";
$_['attach_header']                     = "Đầu trang";
$_['attach_footer_collection']          = "Chân trang - Bộ sưu tập";
$_['attach_footer_quick_link']          = "Chân trang - Liên kết nhanh";
$_['delete_error']              = "Xóa menu thất bại";
$_['delete_success']            = "Xóa menu thành công";
$_['attach_menu_to']            = "Gắn menu vào:";
$_['edit_success']              = "Sửa menu thành công";
$_['edit_menu_order_success']                          = "Thay đổi thứ tự menu thành công";
$_['edit_menu_order_failed']                           = "Thay đổi thứ tự menu lỗi. Vui lòng thực hiện lại";
$_['edit_menu_order_failed_missing_menu_id']           = "Thay đổi thứ tự menu lỗi. Vui lòng thực hiện lại (Lỗi: 01)";
$_['edit_menu_order_failed_invalid_menu_ids']          = "Thay đổi thứ tự menu lỗi. Vui lòng thực hiện lại (Lỗi: 02)";
