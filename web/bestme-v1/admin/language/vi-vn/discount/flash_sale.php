<?php
// Heading
$_['heading_title']                              = 'Flash Sale';
$_['heading_coupon_title']                       = 'Mã khuyến mãi';
$_['heading_setting_title']                      = 'Cấu hình flash sale';
$_['heading_title_add']                          = 'Tạo mới flash sale';
$_['heading_title_edit']                         = 'Sửa flash sale';

// text
$_['text_add']                                    = 'Tạo mới flash sale';
$_['text_summary']                                = 'Tóm tắt';
$_['text_edit']                                   = 'Sửa flash sale';
$_['text_options']                                = 'Chọn thao tác';
$_['text_deleted_or_no_exist']                    = 'Chương trình khuyến mại không tồn tại hoặc đã bị xoá!';
$_['text_flash_sale_name']                          = 'Tên chương trình';
$_['text_flash_sale_code']                          = 'Mã chương trình';
$_['text_auto_generate_flash_sale_code']            = 'Tạo mã tự động';
$_['text_flash_sale_type']                          = 'Phương thức';
$_['text_flash_sale_type_select_placeholder']       = 'Chọn phương thức';
$_['text_flash_sale_usage_limit']                   = 'Số lượng áp dụng';
$_['text_no_limit']                               = 'Không giới hạn';
$_['text_description']                            = 'Mô tả';
$_['text_time_apply']                             = 'Thời gian áp dụng';
$_['text_start_at']                               = 'Từ ngày';
$_['text_setting_ent_time']                       = 'Cài đặt ngày kết thúc';
$_['text_end_at']                                 = 'Đến ngày';
$_['text_no_end_at']                              = '--';
$_['text_flash_sale_store']                         = 'Cửa hàng áp dụng';
$_['text_flash_sale_store_all']                     = 'Áp dụng toàn bộ cửa hàng';
$_['text_flash_sale_store_placeholder']             = 'Chọn chi nhánh';
$_['text_flash_sale_order_source']                  = 'Nguồn đơn hàng áp dụng';
$_['text_flash_sale_order_source_all']              = 'Áp dụng toàn bộ đơn hàng';
$_['text_flash_sale_order_source_placeholder']      = 'Chọn nguồn đơn hàng';
$_['text_flash_sale_customer_group']                = 'Đối tượng khách hàng áp dụng';
$_['text_flash_sale_customer_all']                  = 'Áp dụng toàn khách hàng';
$_['text_summary_content_type_order']             = 'Giá trị đơn hàng tối thiểu';
$_['text_summary_content_type_manufacturer']      = 'Nhà cung cấp được chiết khấu';
$_['text_summary_content_type_category']          = 'Loại sản phẩm được chiết khấu';
$_['text_summary_content_type_product']           = 'Sản phẩm được chiết khấu';
$_['text_order_value_from']                       = 'Giá trị đơn hàng từ';
$_['text_order_value_flash_sale']                   = 'Chiết khấu';
$_['text_add_condition']                          = 'Thêm điều kiện';
$_['text_all_categories']                         = 'Tất cả các loại sản phẩm';
$_['text_category']                               = 'Loại sản phẩm';
$_['text_minimum_number']                         = 'Số lượng tối thiểu';
$_['text_maximum_number']                         = 'Số lượng tối đa';
$_['warning_choose_all']                          = 'Thao tác này sẽ xóa toàn bộ thông tin bạn đã chọn. Bạn chắc chắn muốn tiếp tục?';
$_['txt_search_category']                         = 'Tìm kiếm loại sản phẩm';
$_['txt_search_manufacturer']                     = 'Tìm kiếm nhà cung cấp';
$_['txt_all_manufacturer']                        = 'Tất cả nhà cung cấp';
$_['txt_manufacturer']                            = 'Nhà cung cấp';
$_['txt_search_product']                          = 'Tìm kiếm sản phẩm';
$_['txt_all_product']                             = 'Tất cả sản phẩm';
$_['txt_product_name']                            = 'Tên sản phẩm';
$_['txt_contain']                                 = 'Bao gồm';
$_['txt_version']                                 = 'phiên bản';
$_['txt_version_prefix']                          = 'Phiên bản';
$_['txt_origin_price']                            = 'Giá giốc';

// Heading List
$_['heading_title_list']                          = 'Khuyến mãi';
$_['heading_title_add_list']                      = 'Tạo mới';

// Text List
$_['text_home']                                   = 'Trang chủ';
$_['text_add_list']                               = 'Tạo mới';
$_['txt_all']                                     = 'Tất cả';
$_['txt_actived']                                 = 'Đang hoạt động';
$_['txt_unactived']                               = 'Không hoạt động';
$_['txt_scheduled']                               = 'Đã lập lịch';
$_['txt_paused']                                  = 'Hết hạn';
$_['txt_deleted']                                 = 'Đã xóa';
$_['text_filter_method']                          = 'Lọc Flash Sale theo phương thức';
$_['choose_filter']                               = 'Chọn phương thức';
$_['method_filter']                               = 'Phương thức khuyến mãi';
$_['action_filter']                               = 'Lọc';
$_['text_options']                                = 'Chọn thao tác';
$_['text_date_range']                             = 'Ngày bắt đầu - kết thúc';
$_['text_time_range']                             = 'Thời gian bắt đầu - kết thúc';
$_['text_starting_time']                          = 'Thời gian bắt đầu';
$_['text_ending_time']                            = 'Thời gian kết thúc';
$_['text_choose_starting_date']                   = 'Chọn ngày bắt đầu';
$_['text_choose_ending_date']                     = 'Chọn ngày kết thúc';
$_['text_collection']                             = 'Bộ sưu tập';
$_['text_choose_collection']                      = 'Chọn bộ sưu tập';

//Text flash_sale list
$_['flash_sale_heading_list']                       = 'Flash Sale';
$_['flash_sale_search']                             = 'Tìm khuyến mãi';
$_['flash_sale_filter']                             = 'Bộ lọc';
$_['text_no_results']                             = 'Không có dữ liệu!';
$_['flash_sale_delete']                             = 'Xoá chương trình';
$_['flash_sale_delete_confirm']                     = 'Bạn có muốn xoá ?';
$_['flash_sale_confirm_show']                       = 'Bạn có chắc chắn muốn xóa ';
$_['flash_sale_confirm_show']                       = 'Bạn có chắc chắn muốn xóa ';
$_['flash_sale_confirm_choose']                     = " flash sale đã chọn ?";

// Entry List
$_['txt_collection_title']                        = 'Bộ sưu tập';
$_['entry_status']                                = 'Trạng thái';
$_['entry_usage_limit']                           = 'Số lần sử dụng';
$_['entry_times_used']                            = 'Số lần đã sử dụng';
$_['entry_start_at']                              = 'Ngày bắt đầu';
$_['entry_end_at']                                = 'Ngày kết thúc';

// Select 2
$_['select2_notice_not_result']                   =  "Không tìm thấy kết quả phù hợp";
$_['select2_notice_search']                       =  "Tìm kiếm ...";
$_['select2_notice_load_more']                    =  "Đang tải ...";
$_['entry_category_placeholder']                  =  "Tìm kiếm loại sản phẩm";

// Tab

// Validate
$_['validate_success']                            = "Không có lỗi";
$_['validate_invalid_config']                     = "Cấu hình khuyến mại lỗi";
$_['validate_same_product']                       = "Sản phẩm đã tồn tại trong một flash sale thuộc khoảng thời gian này. Vui lòng thay đổi sản phẩm hoặc thời gian áp dụng.";
$_['validate_same_category']                      = "Loại sản phẩm đã tồn tại trong một flash sale thuộc khoảng thời gian này. Vui lòng thay đổi loại sản phẩm hoặc thời gian áp dụng.";
$_['validate_same_manufacturer']                  = "Nhà cung cấp đã tồn tại trong một flash sale thuộc khoảng thời gian này. Vui lòng thay đổi nhà cung cấp hoặc thời gian áp dụng.";
$_['validate_same_order']                         = "Đã tồn tại một flash sale chiết khấu theo tổng giá trị đơn hàng thuộc khoảng thời gian này. Vui lòng thay đổi thời gian áp dụng.";
$_['validate_unknown_error']                      = "";
$_['validate_name_empty']                         = "Vui lòng nhập tên chương trình";
$_['validate_name_length_255']                    = "Không vượt quá 255 ký tự";
$_['validate_special_char']                       = "Không được nhập ký tự đặc biệt";
$_['validate_code_empty']                         = "Vui lòng nhập mã chương trình";
$_['validate_code_length_20']                     = "Không vượt quá 20 ký tự";
$_['validate_flash_sale_type_empty']                = "Chưa chọn phương thức";
$_['validate_flash_sale_end_at']                    = "Không được chọn sớm hơn thời gian bắt đầu";
$_['validate_flash_sale_start_at']                  = "Không được chọn muộn hơn thời gian kết thúc";
$_['validate_flash_sale_store_empty']               = "Chưa chọn cửa hàng";
$_['validate_flash_sale_order_source_empty']        = "Chưa chọn nguồn đơn hàng";
$_['validate_flash_sale_code_duplicate']            = "Mã chương trình đã được sử dụng trước đó";

// error
$_['error_permission']                           = "Bạn không có quyền chỉnh sửa flash sale!";
$_['error_name']                                 = "Tên flash sale không được quá 255 ký tự!";
$_['error_name_exist']                           = "Tên flash sale đã tồn tại!";
$_['error_code_exist']                           = "Mã flash sale đã tồn tại!";
$_['error_flash_sale_type_order_error_count']      = "Lỗi: Số lượng phải lớn hơn 0";
$_['error_flash_sale_type_order_error_value_0']    = "Giá trị đơn hàng phải lớn hơn 0";
$_['error_flash_sale_error_flash_sale_value_0']      = "Giá trị chiết khấu phải lớn hơn 0";
$_['error_flash_sale_quantity']                    = "Số lượng phải lớn hơn 0";
$_['error_flash_sale_type_order_error_value_asc']  = "Giá trị đơn hàng phải tăng dần";
$_['error_flash_sale_type_order_error_value_100']  = "Không được nhập giá trị lớn hơn 100%";
$_['error_amount_must_asc']                      = "Số lượng tối đa phải tăng dần";
$_['error_max_more_min_quantity']                = "Số lượng tối đa phải lớn hơn số lượng tối thiểu";
$_['error_min_quantity_more_than_0']             = "Số lượng tối thiểu phải lớn hơn 0";
$_['error_max_quantity_more_than_0']             = "Số lượng tối đa phải lớn hơn 0";
$_['error_collection_missing']                   = "Bộ sưu tập còn thiếu";
$_['error_collection_not_exists']                = "Bộ sưu tập không tồn tại";
$_['error_starting_date_missing']                = "Ngày bắt đầu còn thiếu";
$_['error_starting_date_wrong_format']           = "Ngày bắt đầu sai định dạng";
$_['error_ending_date_missing']                  = "Ngày kết thúc còn thiếu";
$_['error_ending_date_wrong_format']             = "Ngày kết thúc sai định dạng";
$_['error_starting_time_missing']                = "Thời gian bắt đầu còn thiếu";
$_['error_starting_time_wrong_format']           = "Thời gian bắt đầu sai định dạng";
$_['error_ending_time_missing']                  = "Thời gian kết thúc còn thiếu";
$_['error_ending_time_wrong_format']             = "Thời gian kết thúc sai định dạng";
$_['error_status_wrong_format']                  = "Trạng thái sai định dạng";

// Text chart
$_['options']                            = 'Tùy chọn';
$_['monday']                             = 'T2';
$_['tuesday']                            = 'T3';
$_['wednesday']                          = 'T4';
$_['thursday']                           = 'T5';
$_['friday']                             = 'T6';
$_['saturday']                           = 'T7';
$_['sunday']                             = 'CN';
$_['january']                            = 'Tháng 1';
$_['february']                           = 'Tháng 2';
$_['march']                              = 'Tháng 3';
$_['april']                              = 'Tháng 4';
$_['may']                                = 'Tháng 5';
$_['june']                               = 'Tháng 6';
$_['july']                               = 'Tháng 7';
$_['august']                             = 'Tháng 8';
$_['september']                          = 'Tháng 9';
$_['october']                            = 'Tháng 10';
$_['november']                           = 'Tháng 11';
$_['december']                           = 'Tháng 12';
$_['today']                              = 'Hôm nay';
$_['yesterday']                          = 'Hôm qua';
$_['one_week']                           = '1 tuần';
$_['this_week']                          = 'Tuần này';
$_['one_month']                          = '1 tháng';
$_['this_month']                         = 'Tháng này';
$_['decimal_point']                      = ',';
$_['thousand_point']                     = '.';
$_['last_week']                          = 'Tuần trước';
$_['last_month']                         = 'Tháng trước';

// Feauture comming soon
$_['featured_comming_soon']                       = '* Tính năng đang phát triển';

// flash_sale setting
$_['txt_flash_sale_setting']                       = 'Cấu hình flash sale';
$_['txt_save_active']                            = 'Lưu và kích hoạt';
$_['txt_auto_apply_flash_sale']                    = 'Tự động áp dụng khuyến mãi';
$_['txt_flash_sale_combine']                       = 'Gộp các flash sale';
$_['txt_flash_sale_setting_note']                  = 'Lưu ý: Việc tùy chọn cấu hình chỉ áp dụng cho POS và Admin. Đối với nguồn đơn hàng từ website và chatbot, hệ thống sẽ tự động áp dụng và gộp flash sale.';
$_['txt_flash_sale_save_fail']                     = 'Thay đổi không thành công';
$_['txt_flash_sale_save_success']                  = 'Thay đổi thành công';
$_['txt_active_success']                         = 'Kích hoạt flash sale thành công. Thông tin flash sale đã được cập nhật.';

// text button daterangepicker
$_['txt_cancel']                        = 'Bỏ qua';
$_['txt_apply']                         = 'Lưu lại';

// text on popup create flash_sale
$_['txt_create_new_flash_sale']                        = 'Tạo khuyến mãi mới';
$_['txt_flash_sale_description']                       = 'Khách hàng sẽ được khuyến mãi tự động trong đơn hàng của họ';
$_['txt_flash_sale_code_description']                  = 'Khách hàng sẽ được khuyến mãi nếu nhập mã này khi thanh toán';

// filter
$_['txt_active_filter']                          = 'Kích hoạt chương trình';
$_['txt_paused_filter']                          = 'Ngừng chương trình';
$_['txt_heading_pause']                          = 'Ngừng flash sale đã chọn';
$_['txt_placehold__pause']                       = 'Mô tả lý do ngừng tại đây';
$_['txt_heading_delete']                         = 'Xóa flash sale đã chọn';
$_['txt_placehold__delete']                      = 'Nhập lý do xóa flash sale tại đây';
$_['txt_confirm']                                = 'Xác nhận';

//note
$_['txt_note_pause']                              = 'Ngừng flash sale thành công.';
$_['txt_note_delete']                             = 'Xóa thành công.';
$_['txt_note_pause_one']                          = 'Không thực hiện được thao tác này do flash sale đã hết hạn.';
$_['txt_note_active_one']                         = 'Flash Sale hiện đang hoạt động.';
$_['txt_note_active']                             = 'Kích hoạt';
$_['txt_note_pause']                              = 'Ngừng';
$_['txt_note_flash_sale_success']                   = 'flash sale thành công.';
$_['txt_note_view_detail']                        = 'Xem chi tiết';
$_['text_flash_sale_result']                        = 'Kết quả thao tác';
$_['text_flash_sale_error']                         = 'Lỗi';
$_['text_success']                                = 'Thành công';
$_['text_failure']                                = 'Thất bại';
$_['txt_exit']                                    = 'Thoát';
$_['txt_pause_success']                           = 'Ngừng flash sale thành công.';

// filter method
$_['txt_filter_category']                         = 'Chiết khấu theo loại sản phẩm';
$_['txt_filter_manufacture']                      = 'Chiết khấu theo nhà cung cấp';
$_['txt_filter_product']                          = 'Chiết khấu theo từng sản phẩm';
$_['txt_filter_order']                            = 'Chiết khấu theo tổng giá trị đơn hàng';

//tree
$_['txt_inventory']                               = 'Tồn';
$_['txt_price']                                   = 'Giá';

// warning
$_['warning_category_flash_sale']                   = 'Lưu ý: Tất cả loại sản phẩm con sẽ được áp dụng theo cấu hình khuyến mãi của loại sản phẩm cha.';

$_['txt_coupon_search']                             = 'Tìm mã khuyến mãi';
$_['txt_coupon_title']                              = 'Tên chương trình';
$_['txt_error_name']                                = "Tên chương trình không được để trống và không quá 128 ký tự!";
$_['txt_error_code']                                = "Mã chương trình không được để trống và không quá 20 ký tự!";
$_['txt_chose_method']                              = "Chọn phương thức";
$_['txt_chose_method_value']                        = "Giá trị";
$_['txt_chose_method_percent']                      = "Phần trăm";
$_['txt_flash_sale_value']                            = "Giá trị khuyến mãi";
$_['txt_flash_sale_flash_sale_value']                   = "Giá trị khuyến mãi không được để trống";
$_['txt_flash_sale_max_percent']                      = "Không được bằng 0 hoặc vượt quá 100%";