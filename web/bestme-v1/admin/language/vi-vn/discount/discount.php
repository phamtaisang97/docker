<?php
// Heading
$_['heading_title']                              = 'Chương trình khuyến mãi';
$_['heading_coupon_title']                       = 'Mã khuyến mãi';
$_['heading_setting_title']                      = 'Cấu hình chương trình khuyến mại';
$_['heading_title_add']                          = 'Tạo mới chương trình khuyến mãi';
$_['heading_title_edit']                         = 'Sửa chương trình khuyến mãi';

// text
$_['text_add']                                    = 'Tạo mới chương trình khuyến mãi';
$_['text_summary']                                = 'Tóm tắt';
$_['text_edit']                                   = 'Sửa chương trình khuyến mãi';
$_['text_options']                                = 'Chọn thao tác';
$_['text_deleted_or_no_exist']                    = 'Chương trình khuyến mại không tồn tại hoặc đã bị xoá!';
$_['text_discount_name']                          = 'Tên chương trình';
$_['text_discount_code']                          = 'Mã chương trình';
$_['text_auto_generate_discount_code']            = 'Tạo mã tự động';
$_['text_discount_type']                          = 'Phương thức';
$_['text_discount_type_select_placeholder']       = 'Chọn phương thức';
$_['text_discount_usage_limit']                   = 'Số lượng áp dụng';
$_['text_no_limit']                               = 'Không giới hạn';
$_['text_description']                            = 'Mô tả';
$_['text_time_apply']                             = 'Thời gian áp dụng';
$_['text_start_at']                               = 'Từ ngày';
$_['text_setting_ent_time']                       = 'Cài đặt ngày kết thúc';
$_['text_end_at']                                 = 'Đến ngày';
$_['text_no_end_at']                              = '--';
$_['text_discount_store']                         = 'Cửa hàng áp dụng';
$_['text_discount_store_all']                     = 'Áp dụng toàn bộ cửa hàng';
$_['text_discount_store_placeholder']             = 'Chọn chi nhánh';
$_['text_discount_order_source']                  = 'Nguồn đơn hàng áp dụng';
$_['text_discount_order_source_all']              = 'Áp dụng toàn bộ đơn hàng';
$_['text_discount_order_source_placeholder']      = 'Chọn nguồn đơn hàng';
$_['text_discount_customer_group']                = 'Đối tượng khách hàng áp dụng';
$_['text_discount_customer_all']                  = 'Áp dụng toàn khách hàng';
$_['text_summary_content_type_order']             = 'Giá trị đơn hàng tối thiểu';
$_['text_summary_content_type_manufacturer']      = 'Nhà cung cấp được chiết khấu';
$_['text_summary_content_type_category']          = 'Loại sản phẩm được chiết khấu';
$_['text_summary_content_type_product']           = 'Sản phẩm được chiết khấu';
$_['text_order_value_from']                       = 'Giá trị đơn hàng từ';
$_['text_order_value_discount']                   = 'Chiết khấu';
$_['text_add_condition']                          = 'Thêm điều kiện';
$_['text_all_categories']                         = 'Tất cả các loại sản phẩm';
$_['text_category']                               = 'Loại sản phẩm';
$_['text_minimum_number']                         = 'Số lượng tối thiểu';
$_['text_maximum_number']                         = 'Số lượng tối đa';
$_['warning_choose_all']                          = 'Thao tác này sẽ xóa toàn bộ thông tin bạn đã chọn. Bạn chắc chắn muốn tiếp tục?';
$_['txt_search_category']                         = 'Tìm kiếm loại sản phẩm';
$_['txt_search_manufacturer']                     = 'Tìm kiếm nhà cung cấp';
$_['txt_all_manufacturer']                        = 'Tất cả nhà cung cấp';
$_['txt_manufacturer']                            = 'Nhà cung cấp';
$_['txt_search_product']                          = 'Tìm kiếm sản phẩm';
$_['txt_all_product']                             = 'Tất cả sản phẩm';
$_['txt_product_name']                            = 'Tên sản phẩm';
$_['txt_contain']                                 = 'Bao gồm';
$_['txt_version']                                 = 'phiên bản';
$_['txt_version_prefix']                          = 'Phiên bản';
$_['txt_origin_price']                            = 'Giá giốc';

// Heading List
$_['heading_title_list']                          = 'Khuyến mãi';
$_['heading_title_add_list']                      = 'Tạo mới';

// Text List
$_['text_home']                                   = 'Trang chủ';
$_['text_add_list']                               = 'Tạo mới';
$_['txt_all']                                     = 'Tất cả';
$_['txt_active']                                  = 'Đang hoạt động';
$_['txt_scheduled']                               = 'Đã lập lịch';
$_['txt_paused']                                  = 'Hết hạn';
$_['txt_deleted']                                 = 'Đã xóa';
$_['text_filter_method']                          = 'Lọc khuyến mãi theo phương thức';
$_['choose_filter']                               = 'Chọn phương thức';
$_['method_filter']                               = 'Phương thức khuyến mãi';
$_['action_filter']                               = 'Lọc';
$_['text_options']                                = 'Chọn thao tác';

//Text discount list
$_['discount_heading_list']                       = 'Khuyến mãi';
$_['discount_search']                             = 'Tìm khuyến mãi';
$_['discount_filter']                             = 'Bộ lọc';
$_['text_no_results']                             = 'Không có dữ liệu!';
$_['discount_delete']                             = 'Xoá chương trình';
$_['discount_delete_confirm']                     = 'Bạn có muốn xoá ?';
$_['discount_confirm_show']                       = 'Bạn có chắc chắn muốn xóa ';
$_['discount_confirm_show']                       = 'Bạn có chắc chắn muốn xóa ';
$_['discount_confirm_choose']                     = " chương trình khuyến mãi đã chọn ?";

// Entry List
$_['txt_discount_title']                          = 'Tên chương trình';
$_['entry_status']                                = 'Trạng thái';
$_['entry_usage_limit']                           = 'Số lần sử dụng';
$_['entry_times_used']                            = 'Số lần đã sử dụng';
$_['entry_start_at']                              = 'Ngày bắt đầu';
$_['entry_end_at']                                = 'Ngày kết thúc';

// Select 2
$_['select2_notice_not_result']                   =  "Không tìm thấy kết quả phù hợp";
$_['select2_notice_search']                       =  "Tìm kiếm ...";
$_['select2_notice_load_more']                    =  "Đang tải ...";
$_['entry_category_placeholder']                  =  "Tìm kiếm loại sản phẩm";

// Tab

// Validate
$_['validate_success']                            = "Không có lỗi";
$_['validate_invalid_config']                     = "Cấu hình khuyến mại lỗi";
$_['validate_same_product']                       = "Sản phẩm đã tồn tại trong một chương trình khuyến mãi thuộc khoảng thời gian này. Vui lòng thay đổi sản phẩm hoặc thời gian áp dụng.";
$_['validate_same_category']                      = "Loại sản phẩm đã tồn tại trong một chương trình khuyến mãi thuộc khoảng thời gian này. Vui lòng thay đổi loại sản phẩm hoặc thời gian áp dụng.";
$_['validate_same_manufacturer']                  = "Nhà cung cấp đã tồn tại trong một chương trình khuyến mãi thuộc khoảng thời gian này. Vui lòng thay đổi nhà cung cấp hoặc thời gian áp dụng.";
$_['validate_same_order']                         = "Đã tồn tại một chương trình khuyến mãi chiết khấu theo tổng giá trị đơn hàng thuộc khoảng thời gian này. Vui lòng thay đổi thời gian áp dụng.";
$_['validate_unknown_error']                      = "";
$_['validate_name_empty']                         = "Vui lòng nhập tên chương trình";
$_['validate_name_length_255']                    = "Không vượt quá 255 ký tự";
$_['validate_special_char']                       = "Không được nhập ký tự đặc biệt";
$_['validate_code_empty']                         = "Vui lòng nhập mã chương trình";
$_['validate_code_length_20']                     = "Không vượt quá 20 ký tự";
$_['validate_discount_type_empty']                = "Chưa chọn phương thức";
$_['validate_discount_end_at']                    = "Không được chọn sớm hơn thời gian bắt đầu";
$_['validate_discount_start_at']                  = "Không được chọn muộn hơn thời gian kết thúc";
$_['validate_discount_store_empty']               = "Chưa chọn cửa hàng";
$_['validate_discount_order_source_empty']        = "Chưa chọn nguồn đơn hàng";
$_['validate_discount_code_duplicate']            = "Mã chương trình đã được sử dụng trước đó";

// error
$_['error_permission']                           = "Bạn không có quyền chỉnh sửa chương trình khuyến mãi!";
$_['error_name']                                 = "Tên chương trình khuyến mãi không được quá 255 ký tự!";
$_['error_name_exist']                           = "Tên chương trình khuyến mãi đã tồn tại!";
$_['error_code_exist']                           = "Mã chương trình khuyến mãi đã tồn tại!";
$_['error_discount_type_order_error_count']      = "Lỗi: Số lượng phải lớn hơn 0";
$_['error_discount_type_order_error_value_0']    = "Giá trị đơn hàng phải lớn hơn 0";
$_['error_discount_error_discount_value_0']      = "Giá trị chiết khấu phải lớn hơn 0";
$_['error_discount_quantity']                    = "Số lượng phải lớn hơn 0";
$_['error_discount_type_order_error_value_asc']  = "Giá trị đơn hàng phải tăng dần";
$_['error_discount_type_order_error_value_100']  = "Không được nhập giá trị lớn hơn 100%";
$_['error_amount_must_asc']                      = "Số lượng tối đa phải tăng dần";
$_['error_max_more_min_quantity']                = "Số lượng tối đa phải lớn hơn số lượng tối thiểu";
$_['error_min_quantity_more_than_0']             = "Số lượng tối thiểu phải lớn hơn 0";
$_['error_max_quantity_more_than_0']             = "Số lượng tối đa phải lớn hơn 0";

// Text chart
$_['options']                            = 'Tùy chọn';
$_['monday']                             = 'T2';
$_['tuesday']                            = 'T3';
$_['wednesday']                          = 'T4';
$_['thursday']                           = 'T5';
$_['friday']                             = 'T6';
$_['saturday']                           = 'T7';
$_['sunday']                             = 'CN';
$_['january']                            = 'Tháng 1';
$_['february']                           = 'Tháng 2';
$_['march']                              = 'Tháng 3';
$_['april']                              = 'Tháng 4';
$_['may']                                = 'Tháng 5';
$_['june']                               = 'Tháng 6';
$_['july']                               = 'Tháng 7';
$_['august']                             = 'Tháng 8';
$_['september']                          = 'Tháng 9';
$_['october']                            = 'Tháng 10';
$_['november']                           = 'Tháng 11';
$_['december']                           = 'Tháng 12';
$_['today']                              = 'Hôm nay';
$_['yesterday']                          = 'Hôm qua';
$_['one_week']                           = '1 tuần';
$_['this_week']                          = 'Tuần này';
$_['one_month']                          = '1 tháng';
$_['this_month']                         = 'Tháng này';
$_['decimal_point']                      = ',';
$_['thousand_point']                     = '.';
$_['last_week']                          = 'Tuần trước';
$_['last_month']                         = 'Tháng trước';

// Feauture comming soon
$_['featured_comming_soon']                       = '* Tính năng đang phát triển';

// discount setting
$_['txt_discount_setting']                       = 'Cấu hình chương trình khuyến mại';
$_['txt_save_active']                            = 'Lưu và kích hoạt';
$_['txt_auto_apply_discount']                    = 'Tự động áp dụng khuyến mãi';
$_['txt_discount_combine']                       = 'Gộp các chương trình khuyến mại';
$_['txt_discount_setting_note']                  = 'Lưu ý: Việc tùy chọn cấu hình chỉ áp dụng cho POS và Admin. Đối với nguồn đơn hàng từ website và chatbot, hệ thống sẽ tự động áp dụng và gộp chương trình khuyến mãi.';
$_['txt_discount_save_fail']                     = 'Thay đổi không thành công';
$_['txt_discount_save_success']                  = 'Thay đổi thành công';
$_['txt_active_success']                         = 'Kích hoạt chương trình khuyến mãi thành công. Thông tin chương trình khuyến mãi đã được cập nhật.';

// text button daterangepicker
$_['txt_cancel']                        = 'Bỏ qua';
$_['txt_apply']                         = 'Lưu lại';

// text on popup create discount
$_['txt_create_new_discount']                        = 'Tạo khuyến mãi mới';
$_['txt_discount_description']                       = 'Khách hàng sẽ được khuyến mãi tự động trong đơn hàng của họ';
$_['txt_discount_code_description']                  = 'Khách hàng sẽ được khuyến mãi nếu nhập mã này khi thanh toán';

// filter
$_['txt_active_filter']                          = 'Kích hoạt chương trình';
$_['txt_paused_filter']                          = 'Ngừng chương trình';
$_['txt_heading_pause']                          = 'Ngừng chương trình khuyến mãi đã chọn';
$_['txt_placehold__pause']                       = 'Mô tả lý do ngừng tại đây';
$_['txt_heading_delete']                         = 'Xóa chương trình khuyến mãi đã chọn';
$_['txt_placehold__delete']                      = 'Nhập lý do xóa chương trình khuyến mãi tại đây';
$_['txt_confirm']                                = 'Xác nhận';

//note
$_['txt_note_pause']                              = 'Ngừng chương trình khuyến mãi thành công.';
$_['txt_note_delete']                             = 'Xóa thành công.';
$_['txt_note_pause_one']                          = 'Không thực hiện được thao tác này do chương trình khuyến mãi đã hết hạn.';
$_['txt_note_active_one']                         = 'Chương trình khuyến mãi hiện đang hoạt động.';
$_['txt_note_active']                             = 'Kích hoạt';
$_['txt_note_pause']                              = 'Ngừng';
$_['txt_note_discount_success']                   = 'chương trình khuyến mãi thành công.';
$_['txt_note_view_detail']                        = 'Xem chi tiết';
$_['text_discount_result']                        = 'Kết quả thao tác';
$_['text_discount_error']                         = 'Lỗi';
$_['text_success']                                = 'Thành công';
$_['text_failure']                                = 'Thất bại';
$_['txt_exit']                                    = 'Thoát';
$_['txt_pause_success']                           = 'Ngừng chương trình khuyến mãi thành công.';

// filter method
$_['txt_filter_category']                         = 'Chiết khấu theo loại sản phẩm';
$_['txt_filter_manufacture']                      = 'Chiết khấu theo nhà cung cấp';
$_['txt_filter_product']                          = 'Chiết khấu theo từng sản phẩm';
$_['txt_filter_order']                            = 'Chiết khấu theo tổng giá trị đơn hàng';

//tree
$_['txt_inventory']                               = 'Tồn';
$_['txt_price']                                   = 'Giá';

// warning
$_['warning_category_discount']                   = 'Lưu ý: Tất cả loại sản phẩm con sẽ được áp dụng theo cấu hình khuyến mãi của loại sản phẩm cha.';

$_['txt_coupon_search']                             = 'Tìm mã khuyến mãi';
$_['txt_coupon_title']                              = 'Tên chương trình';
$_['txt_error_name']                                = "Tên chương trình không được để trống và không quá 128 ký tự!";
$_['txt_error_code']                                = "Mã chương trình không được để trống và không quá 20 ký tự!";
$_['txt_chose_method']                              = "Chọn phương thức";
$_['txt_chose_method_value']                        = "Giá trị";
$_['txt_chose_method_percent']                      = "Phần trăm";
$_['txt_discount_value']                            = "Giá trị khuyến mãi";
$_['txt_discount_discount_value']                   = "Giá trị khuyến mãi không được để trống";
$_['txt_discount_max_percent']                      = "Không được bằng 0 hoặc vượt quá 100%";

//
$_['text_version']                          = 'Phiên bản : ';
$_['error_no_file_selected']                = 'Chưa có file nào được chọn!';
$_['error_filesize']                        = 'Cảnh báo: Kích thước tệp không được quá 1MB!';
$_['error_filetype']                        = 'Cảnh báo: Chỉ chấp nhận định dạng .xls và xlsx !';
$_['error_upload']                          = 'Cảnh báo: Không thể tải tệp lên hệ thống (lỗi không xác định)!';
$_['error_limit']                           = 'Warning: The maximum product number and product import version is 500!';
$_['error_file_incomplete']                 = 'Cảnh báo: File không đầy đủ thông tin (thiếu cột)!';
$_['error_not_found_fields']                = 'Cảnh báo: Không tìm thấy coupon_code';
$_['error_file_incomplete_in_row']          = 'Cảnh báo: Thiếu coupon_code tại dòng ';
$_['text_uploaded']                         = 'Thành công!';
$_['text_coupon_uploaded']                  = ' coupon đã được cập nhật.';
$_['text_import_list']                      = 'Nhập danh sách';

$_['text_allow_with_discount']                      = 'Cho phép sử dụng với khuyến mãi';
$_['text_apply_for']                                = 'Áp dụng cho';