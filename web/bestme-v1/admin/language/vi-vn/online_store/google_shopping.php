<?php
// Heading
$_['heading_title']             = 'Google Shopping';

// Text
$_['text_online_store']                 = 'Website';

$_['text_intro_title']                  = 'Kết nối với Novaon Autoads';
$_['text_intro_content']                = 'Để sử dụng tính năng bán hàng trên Google (Google Shopping), bạn cần đăng ký tài khoản Novaon Autoads.  Nền tảng hỗ trợ quảng cáo Autoads sẽ giúp bạn quản lý toàn bộ dữ liệu bán hàng trên Google.';

$_['text_more']                 = 'Xem hướng dẫn chi tiết.';
$_['text_detail']                 = 'Hướng dẫn chi tiết.';
$_['text_intro_button']                 = 'Kết nối ngay với Autoads';

$_['text_verify_title']                 = 'Xác minh cửa hàng online';
$_['text_verify_content']               = 'Để sử dụng (Google Shopping), cửa hàng của bạn phải được xác minh bởi Google. ' . PRODUCTION_BRAND_NAME . ' sử dụng cách chèn thêm thẻ Meta vào trang web.';
$_['text_verify_meta_title']            = 'Nội dung thẻ Meta của Google';
$_['text_verify_meta_placeholder']      = '&lt;meta name=&quot;google-site-verification&quot; content=&quot;XXXXXXXXXXXXXXXXX&quot;&gt;';
$_['text_verify_meta_example']          = 'Ví dụ: &lt;meta name=&quot;google-site-verification&quot; content=&quot;XXXXXXXXXXXXXXXXX&quot;&gt;';
$_['text_verify_meta_button']           = 'Lưu';

$_['text_api_key_title']                 = 'Mã khách hàng';
$_['text_api_key_content']               = 'Tất cả các ứng dụng muốn can thiệp vào cửa hàng của bạn đều cần có mã số này. Cung cấp hai mã này cho Autoads để sử dụng tính năng Google Shopping.';
$_['text_api_key_site_key_title']        = 'Mã khách hàng';
$_['text_api_key_secure_key_title']      = 'Mã bí mật khách hàng';
$_['text_api_key_copy_button']           = 'Sao chép';

$_['text_api_key_copy_success']         = 'Đã sao chép';
$_['text_api_key_copy_failed']          = 'Lỗi! Không thể sao chép';

// error form
$_['success_change_meta']               = 'Cập nhật giá trị thẻ meta thành công';
$_['error_change_meta']                 = 'Lỗi: cập nhật giá trị thẻ meta không thành công, vui lòng thử lại';
