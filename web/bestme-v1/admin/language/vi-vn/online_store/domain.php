<?php
// Heading
$_['heading_title']             = 'Cài đặt tên miền';

// Text
$_['txt_domain']                        = 'Tên miền';
$_['txt_main_domain']                   = 'Tên miền chính';
$_['txt_status']                        = 'Trạng thái';
$_['txt_status_connected']              = 'Đã kết nối';
$_['txt_status_pending']                = 'Đang xác thực';
$_['txt_status_not_auth']               = 'Chưa xác thực';
$_['txt_domain_management']             = 'Quản lý tên miền';
$_['txt_domain_management_des']         = 'Danh sách tên miền. Bạn có thể xóa tên miền khỏi cửa hàng của bạn.';
$_['txt_main_domain_config']            = 'Cấu hình tên miền chính';
$_['txt_main_domain_config_des']        = 'Tên miền chính là tên miền hiển thị trên công cụ tìm kiếm của Google, và cũng là tên miền khách hàng dùng để truy cập vào website của bạn.';
$_['txt_redirect_all_to_domain']        = 'Chuyển hướng tất cả truy cập vào tên miền này';
$_['txt_redirect_all_to_domain_des']    = 'Chuyển hướng các truy cập từ tất cả tên miền của bạn về một tên miền chính duy nhất';
$_['txt_add_domain']                    = 'Thêm tên miền';
$_['txt_delete_domain']                 = 'Xóa tên miền';
$_['txt_delete_domain_alert_1']           = 'Bạn có chắc chắn muốn xóa tên miền “';
$_['txt_delete_domain_alert_2']           = '”?  Sau hành động này sẽ không thể khôi phục lại được.';
$_['txt_domain_auth']                   = 'Xác thực tên miền ';
$_['txt_domain_auth_des_1']             = 'Quá trình xác thực sẽ diễn ra sau khi bạn nhấn nút “Xác thực tên miền”.';
$_['txt_domain_auth_des_2']             = 'Để quá trình xác thực thành công, bạn cần truy cập trang quản trị tên miền, tìm đến phần Quản lý/Cấu hình DNS. Tại đây, bạn cần tạo 2 bản ghi:';
$_['txt_domain_auth_des_3']             = '<strong>Bản ghi %s</strong><br>
                                            Host record: %s<br>
                                            Type (Loại): %s<br>
                                            Value (Giá trị): %s';
$_['cname_record']                      = 'CNAME';
$_['cname_host_record']                 = 'www';
$_['cname_type']                        = 'CNAME';
$_['a_record']                          = 'A';
$_['a_host_record']                     = '@ hoặc để trống';
$_['a_type']                            = 'A';
$_['txt_domain_auth_des_4']             = 'Sau khi lưu lại thành công, bạn quay lại đây và xác thực tên miền.';
$_['txt_domain_sub_domain_note']        = '<hr>
                                        <i>Nếu tên miền của bạn là sub-domain của tên miền chính, ví dụ: "your-shop.your-company.com",
                                            bạn cần tạo bản ghi CNAME sau thay cho bản ghi CNAME trên:</i><br>
                                        <b>Bản ghi CNAME</b><br>
                                        Host record: www.your-shop<br>
                                        Type (Loại): CNAME<br>
                                        Value (Giá trị): $BESTME_DOMAIN<br>';
$_['txt_wait']                          = 'Bạn vui lòng đợi trong giây lát! ';
$_['txt_domain_being_auth']             = 'Tên miền đang được xác thực...';
$_['txt_domain_auth_alert']             = 'A Record và CNAME không đúng. Vui lòng kiểm tra với nhà cung cấp tên miền và xác thực lại.';
$_['txt_save_domain_success']           = 'Lưu tên miền thành công!';
$_['txt_remove_domain_success']         = 'Đã xóa tên miền!';

// button
$_['btn_add_domain']                    = 'Thêm tên miền';
$_['btn_domain_auth']                   = 'Xác thực tên miền';
$_['btn_cancel']                        = 'Bỏ qua';
$_['btn_re_auth']                       = 'Xác thực lại';

// entry
$_['entry_enter_domain']               = 'Nhập tên miền';
$_['validate_enter_domain']            = 'Nhập đúng định dạng tên miền, không bắt đầu bằng http://, https://, www.';

// error form
$_['error_domain']                 = 'Lỗi: Tên miền đang để trống hoặc quá ngắn, quá dài (1-100 ký tự) hoặc không hợp lệ';
$_['error_redis_connect']          = 'Lỗi từ hệ thống [10023], không thể xác thực được, vui lòng thử lại sau';
$_['error_domain_max']             = 'Lỗi: Số tên miền tối đa là 15!';
$_['error_exist']                  = 'Lỗi: Tên miền đã tồn tại';
$_['error_find_domain']            = 'Lỗi: Tên miền không tìm thấy';
$_['error_permission']             = 'Lỗi: Bạn không có quyền thêm/sửa domain';
$_['success_change_redirect']      = 'Thay đổi chuyển hướng tất cả truy cập thành công!';
$_['fail_change_redirect']         = 'Không thành công, chưa chọn tên miền chính!';
$_['fail_change_main_domain']      = 'Thay đổi tên miền chính thất bại!';
$_['success_change_main_domain']   = 'Thay đổi tên miền chính thành công!';
$_['fail_remove_main_domain']      = 'Lỗi: Không thể xóa tên miền chính!';
$_['fail_domain_auth']             = 'Xác thực không thành công!';
$_['success_domain_auth']          = 'Xác thực tên miền thành công!';
$_['fail_domain_alert_suffix']     = ' không đúng. Vui lòng kiểm tra với nhà cung cấp tên miền và xác thực lại.';
$_['validate_input_domain']        = 'Tên miền không đúng định dạng, không bắt đầu bằng http://, https://, www.';
$_['validate_input_empty']         = 'Vui lòng điền vào trường này';
$_['validate_input_length']        = 'Tên miền đang quá dài (1-100 ký tự)';
