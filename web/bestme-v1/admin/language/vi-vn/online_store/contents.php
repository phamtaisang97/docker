<?php
// Heading
$_['heading_title']         = 'Trang nội dung';
$_['add_heading_title']     = 'Thêm trang nội dung';
$_['edit_heading_title']     = 'Sửa trang nội dung';

// Text
$_['breadcrumb_setting']        = 'Cài đặt';
$_['breadcrumb_blog']           = 'Bài viết';
$_['breadcrumb_contents']       = 'Trang nội dung';
$_['breadcrumb_add_contents']   = 'Thêm nội dung';
$_['breadcrumb_edit_contents']  = 'Sửa nội dung';

$_['txt_contents_list']                    = 'Danh sách';
$_['txt_contents_filter']                  = 'Bộ lọc trang';
$_['txt_filter_status']                    = 'Trạng thái';
$_['txt_filter_status_all']                = 'Tất cả';
$_['txt_filter_status_show']               = 'Hiển thị';
$_['txt_filter_status_hide']               = 'Ẩn';
$_['txt_filter_status_search']             = 'Tìm kiếm';
$_['txt_search_place_holder']              = 'Tìm kiếm trang nội dung';

$_['txt_select_action']                    = 'Chọn thao tác';
$_['txt_action_show']                      = 'Hiển thị';
$_['txt_action_hide']                      = 'Ẩn';
$_['txt_action_delete']                    = 'Xóa trang';

//alert
$_['text_delete_success']                 = 'Xóa thành công!';
$_['text_delete_fail']                    = 'Xóa thất bại ! không được phép xóa mục được đồng bộ từ nhãn hàng';
$_['text_confirm_change_status']          = 'Bạn có muốn thay đổi trạng thái tất cả!';
$_['text_change_status_success']          = 'Thay đổi trạng thái thành công!';
$_['text_change_status_fail']             = 'Thay đổi trạng thái thất bại ! không được phép thay đổi mục được đồng bộ từ nhãn hàng';

$_['txt_title']                            = 'Tiêu đề';
$_['txt_title_place_holder']               = 'Nhập tiêu đề bài viết';
$_['txt_description']                      = 'Nội dung bài viết';
$_['txt_description_place_holder']         = 'Thêm mô tả';
$_['txt_alias']                            = 'Đường dẫn / Alias';
$_['txt_preview']                          = 'Xem trước kết quả tìm kiếm';
$_['txt_custom_seo']                       = 'Tùy chỉnh SEO';
$_['txt_custom_seo_des']                   = 'Nhập tiêu đề và mô tả để xem cách bài viết được hiển thị trên danh sách tìm kiếm.';
$_['txt_date_updated']                     = 'Ngày cập nhật';
$_['txt_date_publish']                     = 'Chọn ngày xuất bản';
$_['txt_characters']                       = 'ký tự';
$_['txt_used']                             = 'Đã sử dụng';
$_['txt_seo_preview_title_demo']           = '3 món quà Giáng sinh ý nghĩa nhất dành tặng trong mùa Noel 14/12/2017';
$_['txt_seo_preview_description_demo']     = 'Bước 1: Truy cập website và lựa chọn sản phẩm cần mua để mua hàng Bước 2: Click và sản phẩm muốn mua, màn hình hiển thị ra popup...';
$_['txt_seo_preview_alias_demo']           = 'ba-mon-qua-y-nghia-nhat-danh-tang-ban-gai';
$_['txt_preview_on_website']               = 'Xem trên website';
$_['txt_selected']                         = 'Đã chọn';
$_['txt_item']                             = 'mục';

// button
$_['contents_form_button_save']                 = 'Lưu thay đổi';
$_['contents_form_button_cancel']               = 'Bỏ qua';

// Error
$_['error_warning']             = 'Cảnh báo: Vui lòng kiểm tra kỹ form để biết lỗi!';
$_['error_permission']          = 'Cảnh báo: Bạn không có quyền chỉnh sửa Trang nội dung!';
$_['error_name']                = 'Warning: Tiêu đề trang nội dung không hợp lệ, quá ngắn hoặc quá dài (1-256 ký tự)!';

$_['text_success_add']           = 'Tạo mới nội dung thành công';
$_['text_success_edit']           = 'Sửa nội dung thành công';
$_['error_check_source']           = 'Không được phép thay đổi mục đã được đồng bộ từ nhãn hàng';