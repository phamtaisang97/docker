<?php
// Heading
$_['heading_title']                  = 'Cài đặt thanh toán';

// Text
$_['breadcrumb_setting']                        = 'Cài đặt';
$_['breadcrumb_payment_setting']                = 'Cài đặt thanh toán';

$_['text_success']                              = 'Thành công';

$_['payment_setting_title']                     = 'Thanh toán';
$_['payment_setting_subtitle']                  = 'Phương thức thanh toán';
$_['payment_setting_description']               = 'Các phương thức thanh toán khi mua hàng trên website của bạn.';

$_['payment_methods_title']                     = 'Các phương thức thanh toán';
$_['payment_methods_description']               = 'Lựa chọn phương thức thanh toán bạn muốn kích hoạt cho website bán hàng của bạn.';
$_['payment_methods_add_button']                = 'Thêm phương thức thanh toán';

$_['payment_methods_form_name']                 = 'Tên phương thức';
$_['payment_methods_form_name_placeholder']     = 'Tên phương thức';
$_['payment_methods_form_guide']                = 'Hướng dẫn thanh toán';
$_['payment_methods_form_guide_placeholder']    = 'Mô tả phương thức';
$_['payment_methods_form_button_save']          = 'Lưu thay đổi';
$_['payment_methods_form_button_cancel']        = 'Bỏ qua';

$_['payment_methods_item_status_activated']     = 'Đang kích hoạt:';
$_['payment_methods_item_status_deactivated']   = 'Ngưng kích hoạt:';
$_['payment_methods_item_edit_button']          = 'Chỉnh sửa';
$_['payment_methods_item_activated_button']     = 'Kích hoạt';
$_['payment_methods_item_deactivated_button']   = 'Ngưng kích hoạt';
$_['payment_methods_item_delete_button']        = 'Xóa';
$_['payment_methods_item_delete_confirm']       = 'Bạn có chắc chắn xóa phương thức thanh toán này?';

// Error form
$_['error_payment_id']                          = 'Lỗi: khởi tạo thông tin phương thức thanh toán mới bị lỗi, vui lòng thử lại';
$_['error_name']                                = 'Lỗi: Tên phương thức vận chuyển đang để trống hoặc quá ngắn hoặc quá dài (1-255 ký tự) hoặc đã tồn tại';
$_['error_guide']                               = 'Lỗi: hướng dẫn phương thức thanh toán quá ngắn hoặc quá dài (1-1000 ký tự)';
$_['error_status']                              = 'Lỗi: trạng thái phương thức thanh toán không đúng, vui lòng thử lại';
$_['error_name_min']                            = 'Vui lòng nhập dữ liệu vào trường bắt buộc';
$_['error_name_max']                            = 'Vui lòng nhập dữ liệu vào trường ít hơn 255 ký tự';
$_['error_payment_isset']                       = 'Tên phương thức thanh toán đã tồn tại';
$_['error_require_and_active']                  = 'Phải tồn tại ít nhất 1 phương thức thanh toán ở trạng thái kích hoạt.';
$_['you_trail'] = 'Bạn còn 4 ngày dùng thử!';
$_['choose_package'] = "Chọn một gói sử dụng";
$_['unique_notice'] = "Bạn không thể ngưng kích hoạt phương thức thanh toán duy nhất";