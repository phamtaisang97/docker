<?php
// Heading
$_['heading_title']                  = 'Cài đặt tài khoản';

// Text
$_['breadcrumb_setting']                        = 'Cài đặt';
$_['breadcrumb_account_setting']                = 'Cài đặt tài khoản';

$_['account_setting_title']                     = 'Cài đặt tài khoản';
$_['account_setting_subtitle']                  = 'Tài khoản và phân quyền';
$_['account_setting_description']               = 'Cài đặt tài khoản và phân quyền quản trị website.';

$_['admin_account_title']                       = 'Chủ cửa hàng';
$_['admin_account_edit_button']                 = 'Chỉnh sửa';
$_['admin_account_last_login']                  = 'Đăng nhập lần cuối lúc %s ngày %s';

$_['staff_account_title']                       = 'Tài khoản nhân viên';
$_['staff_account_description']                 = 'Thêm tài khoản quản lý cho nhân viên của bạn và cấp quyền truy cập các cài đặt cho họ.';
$_['staff_account_add_button']                  = 'Thêm tài khoản';
$_['staff_account_kill_all_sessions']           = 'Ngắt kết nối các phiên đăng nhập';
$_['staff_account_kill_all_sessions_confirm']   = 'Bạn chắc chắn muốn ngắt kết nối các phiên đăng nhập?';
$_['staff_account_kill_all_sessions_success']   = 'Đã ngắt kết nối các phiên đăng nhập thành công!';
$_['staff_account_kill_all_sessions_failed']    = 'Ngắt kết nối các phiên đăng nhập bị lỗi, vui lòng thử lại sau.';
$_['staff_account_last_login']                  = 'Đăng nhập lần cuối lúc %s ngày %s';
$_['staff_account_permission_full']             = 'Toàn quyền truy cập';
$_['staff_account_permission_limit']            = 'Giới hạn';
$_['txt_invalid_password']                      = 'Mật khẩu không đúng';
$_['txt_invalid_permission']                    = 'Bạn không có quyền thay đổi thông tin tài khoản này !';

$_['txt_success_add']                    = 'Thêm nhân viên thành công!';
$_['txt_success_edit']                    = 'Sửa thông tin nhân viên thành công!';
$_['txt_success_delete']                    = 'Xóa thông tin nhân viên thành công!';
$_['txt_success_edit_admin']                    = 'Sửa thông tin admin thành công!';
$_['txt_view_detail']                           = 'Xem chi tiết';

$_['staff_group_decentralization']       = 'Phân quyền nhóm nhân viên';
$_['txt_staff_group']                    = 'Nhóm nhân viên';
$_['txt_group_name']                     = 'Tên nhóm';
$_['number_staff_in_group']              = 'Số nhân viên thuộc nhóm';
$_['txt_action']                         = 'Hành động';
$_['action_add_group']                   = 'Thêm nhóm nhân viên';

$_['trial']        =  'Dùng thử';
$_['basic']        =  'Cơ bản';
$_['optimal']      =  'Tối Ưu';
$_['advance']      =  'Nâng cao';
$_['benefit_package']      =  'Gói dịch vụ';
$_['benefit_detail']      =  'Chi tiết về gói dịch vụ website đang sử dụng.Nâng cấp hoặc gia hạn gói dịch vụ';
$_['package_is_in_use']      =  'Gói đang sử dụng';
$_['renewal_date']      =  'Ngày gia hạn';
$_['expiration_date']      =  'Ngày hết hạn';
$_['here']             = 'tại đây';
$_['shop_status']      =  'Trạng thái';
$_['shop_status_active']        =  'Hoạt động';
$_['shop_status_disabled']      =  'Tạm ngừng';

$_['txt_actual_use']                = 'Thực tế sử dụng';
$_['txt_capacity']                  = 'Dung lượng';
$_['txt_staff']                     = 'Nhân viên';
$_['txt_store']                     = 'Cửa hàng';
$_['txt_unlimited']                 = 'Không giới hạn';
$_['txt_invalid_max_staff']         = 'Thao tác không thực hiện được do bạn đã sử dụng hết hạn mức số tài khoản nhân viên của gói dịch vụ. 
                                        Vui lòng liên hệ với chúng tôi để nâng cấp thêm.';
$_['txt_not_apply']                 = 'Không áp dụng';
$_['txt_created']                   = 'Đã tạo';
$_['txt_account']                   = 'Tài khoản';
$_['txt_used']                      = 'Đã sử dụng';
$_['alert_create_staff_group']      = "Bạn phải tạo nhóm nhân viên trước !";