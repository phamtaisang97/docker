<?php
// Heading
$_['heading_title']                         = 'Thông báo';
$_['heading_settings']                      = 'Cài đặt';

// button
$_['btn_save']                              = 'Lưu thay đổi';
$_['btn_cancel']                            = 'Bỏ qua';

// Error
$_['error_warning']                         = 'Cảnh báo: Vui lòng kiểm tra form cẩn thận cho các lỗi!';
$_['error_permission']                      = 'Cảnh báo: Bạn không có quyền sửa đổi thông báo!';

$_['text_update_success']                   = 'Cập nhật thông tin thành công!';

$_['text_title_order_success']              = 'Thông báo khi đặt đơn hàng thành công';
$_['text_description_order_success']        = 'Tùy chỉnh thông báo được hiển thị trên website sau khi khách hoàn tất quá trình đặt hàng';
$_['text_label_order_success']              = 'Nội dung thông báo';

$_['text_title_customer_success']           = 'Thông báo khi đăng ký thành công';
$_['text_description_customer_success']     = 'Tùy chỉnh thông báo được hiển thi khi đăng ký thành công';
$_['text_label_customer_success']           = 'Nội dung thông báo';