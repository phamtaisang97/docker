<?php
// Heading
$_['heading_title']                  = 'Cài đặt chung';
$_['heading_settings']               = 'Cài đặt';

// title
$_['title_website_info']               = 'Thông tin website';
$_['title_contact_info']               = 'Thông tin liên hệ';
$_['title_formats']                    = 'Tiêu chuẩn và định dạng';

// text
$_['text_website_info']                = 'Thông tin cơ bản của website giúp ' . PRODUCTION_BRAND_NAME . ' và khách hàng liên hệ với bạn.';
$_['text_shop_name']                   = 'Tên cửa hàng';
$_['text_home_title']                  = 'Tiêu đề trang chủ';
$_['text_home_des']                    = 'Mô tả trang chủ';
$_['text_management_email']            = 'Email quản lý';
$_['text_management_email_des']        = '' . PRODUCTION_BRAND_NAME . ' sẽ gửi thông báo tới bạn qua địa chỉ email này.';
$_['text_cs_email']                    = 'Email chăm sóc khách hàng';
$_['text_cs_email_des']                = 'Bạn sẽ dùng địa chỉ email này để gửi thông tin chăm sóc tới khách hàng.';
$_['text_contact_info']                = 'Thông tin được sử dụng trong các thông báo về đơn hàng và địa chỉ để liên hệ đến cửa hàng.';
$_['text_hotline']                     = 'Hotline liên hệ';
$_['text_hotline_des']                 = 'Hotline sẽ hiển thị trong trang chi tiết sản phẩm. Hiện chỉ áp dụng cho theme Egomall, Farm88, Chili, Car Parts Auto, Hani, Grocerie, Sun Electro, Flavor, S Building và S Furniture';
$_['error_hotline']                    = 'Warning: Hotline không hợp lệ, không đúng định dạng hoặc vượt quá 15 ký tự.';

$_['text_business_name']               = 'Tên kinh doanh';
$_['text_business_tax_code']           = 'Mã số kinh doanh / Mã số thuế';
$_['text_telephone']                   = 'Số điện thoại';
$_['text_address']                     = 'Địa chỉ';
$_['text_city']                        = 'Tỉnh / Thành phố';
$_['text_country']                     = 'Quốc gia';

$_['text_formats']                     = 'Thông tin được sử dụng trong các thông báo về đơn hàng và địa chỉ để liên hệ đến cửa hàng.';
$_['text_time_zone']                   = 'Múi giờ';
$_['text_weight']                      = 'Khối lượng';
$_['text_currency']                    = 'Đơn vị tiền tệ';
$_['text_order_code_format']           = 'Định dạng Mã của đơn hàng';
$_['text_order_code_format_des']       = 'Mặc định mã đơn hàng của bạn sẽ được đánh số từ #1001, bạn có thể thay đổi tiền tố hoặc hậu tố  để tạo mã có định dạng khác, như “EN1001” hay “1001-A”';
$_['text_order_code_prefix']           = 'Tiền tố';
$_['text_order_code_suffix']           = 'Hậu tố';
$_['text_order_code_prefix_des']       = 'Mã đơn hàng của bạn sẽ có định dạng:';

//entry
$_['entry_shop_name']                   = 'Nhập tên cửa hàng';
$_['entry_home_title']                  = 'Nhập tiêu đề trang chủ';
$_['entry_home_des']                    = 'Nhập mô tả trang chủ';
$_['entry_email']                       = 'Nhập email';

$_['entry_business_name']               = 'Nhập tên kinh doanh';
$_['entry_tax_code']                    = 'Nhập mã số thuế';
$_['entry_telephone']                   = 'Nhập số điện thoại';
$_['entry_address']                     = 'Nhập địa chỉ';


// button
$_['btn_save']                      = 'Lưu thay đổi';
$_['btn_cancel']                    = 'Bỏ qua';

// Error
$_['error_warning']             = 'Warning: Please check the form carefully for errors!';
$_['error_permission']          = 'Warning: You do not have permission to modify customers!';
$_['error_name']                = 'Warning: Tên cửa hàng không hợp lệ, quá ngắn hoặc quá dài (1-50 ký tự)!';
$_['error_title']               = 'Warning: Title trang chủ không hợp lệ, quá ngắn hoặc quá dài (1-128 ký tự)!';

$_['error_input_null']                 = 'Vui lòng điền vào trường này';
$_['error_type_mail']                  = 'Vui lòng nhập đúng định dạng email';
$_['error_max_length_255']             = 'Không vượt quá 255 ký tự';
$_['error_max_length_300']             = 'Không vượt quá 300 ký tự';
$_['error_max_length_50']              = 'Không vượt quá 50 ký tự';
$_['error_max_length_15']              = 'Không vượt quá 15 ký tự';
$_['error_type_phone']                 = 'Vui lòng nhập số cho trường này';
$_['error_max_length_10']              = 'Không vượt quá 10 ký tự';
$_['error_min_length_8']               = 'Vui lòng nhập ít nhất 8 ký tự';

$_['choose_city']   = "Chọn Thành phố";
$_['choose_country']   = "Chọn Quốc gia";

$_['text_success_update']           = 'Cập nhật thông tin thành công!';

$_['image_qr_code_for_bill']                    = 'Ảnh QR Code cho đơn hàng';
$_['text_popup_image_edit']                     = "Thay đổi";
$_['text_popup_image_remove']                   = "Xóa";