<?php
// Heading
$_['heading_title']                 = 'Phân loại';

$_['breadcrumb_setting']            = 'Cài đặt';

$_['category']                      = 'Loại sản phẩm';
$_['manager_category']              = 'Quản lý loại sản phẩm';
$_['search']                        = 'Tìm kiếm';
$_['add_category']                  = 'Thêm loại sản phẩm mới';
$_['name_category']                 = 'Tên loại sản phẩm';
$_['add_new_category']              = 'Thêm loại mới';

$_['manufacture']                   = 'Nhà cung cấp';
$_['manager_manufacture']           = 'Quản lý nhà cung cấp';
$_['add_new_manufacture']           = 'Thêm nhà cung cấp mới';
$_['name_manufacture']              = 'Tên nhà cung cấp';
$_['name_store']                    = 'Tên cửa hàng';
$_['add_manufacture']               = 'Thêm NCC mới';

$_['tag']                           = 'Tags';
$_['manager_tag']                   = 'Quản lý tags';
$_['add_new_tag']                   = 'Thêm tag mới';
$_['name_tag']                      = 'Tên tag';
$_['add_tag']                       = 'Thêm tag mới';

$_['close']                         = 'Đóng';
$_['add_new']                       = 'Thêm mới';
$_['save']                          = 'Lưu';
$_['delete']                        = 'Xóa';

$_['edit_category']                 = 'Chỉnh sửa loại sản phẩm';
$_['edit_manufacture']              = 'Chỉnh sửa nhà cung cấp';

$_['text_category_note']            = 'Lưu ý: cài đặt vị trí hiển thị của loại sản phẩm trên website bằng cách đánh số vào ô trống cuối mỗi dòng. Thứ tự hiển thị: theo đánh số từ nhỏ đến lớn, chỉ cho phép đánh số từ 0 trở lên.';

$_['store']                         = 'Cửa hàng';
$_['manager_store']                 = 'Quản lý cửa hàng';
$_['edit_tag']                      = 'Chỉnh sửa tag';
$_['add_store']                     = 'Thêm cửa hàng';
$_['add_new_store']                 = 'Thêm cửa hàng mới';
$_['edit_store']                    = 'Chỉnh cửa hàng';

$_['delete_category']               = 'Xóa loại sản phẩm';
$_['delete_manufacture']            = 'Xóa nhà cung cấp';
$_['delete_tag']                    = 'Xóa tag';
$_['delete_store']                  = 'Xóa cửa hàng';

$_['confirm_delete_category']       = 'Bạn có chắc chắn muốn xóa loại sản phẩm này?';
$_['confirm_delete_manufacture']    = 'Bạn có chắc chắn muốn xóa nhà cung cấp này?';
$_['confirm_delete_tag']            = 'Bạn có chắc chắn muốn xóa tag này?';
$_['confirm_delete_store']          = 'Bạn có chắc chắn muốn xóa cửa hàng này?';

$_['error_name_category_empty']     = 'Tên loại sản phẩm rỗng';
$_['error_name_manufacture_empty']  = 'Tên nhà cung cấp rỗng';
$_['error_name_tag_empty']          = 'Tên tag rỗng';
$_['error_name_store_empty']         = 'Tên cửa hàng rỗng';

$_['error_name_category_isset']     = 'Tên loại sản phẩm đã tồn tại';
$_['error_name_manufacture_isset']  = 'Tên nhà cung cấp đã tồn tại';
$_['error_name_tag_isset']          = 'Tên tag đã tồn tại';
$_['error_name_store_isset']        = 'Tên cửa hàng đã tồn tại';

// Error File
$_['error_permission'] = 'Cảnh báo: Yêu cầu bị từ chối do thẩm quyền!';
$_['error_filename']   = 'Cảnh báo: Tên tệp dài từ 3 đến 255!';
$_['error_folder']     = 'Cảnh báo: Tên thư mục dài từ 3 đến 255!';
$_['error_exists']     = 'Cảnh báo: Đã tồn tại thư mục!';
$_['error_directory']  = 'Cảnh báo: Thư mục chưa tồn tại!';
$_['error_filesize']   = 'Cảnh báo: Kích thước tệp không được quá 1MB!';
$_['error_filetype']   = 'Cảnh báo: Chỉ chấp nhận định dạng .xls và xlsx !';
$_['error_upload']     = 'Cảnh báo: Không thể tải tệp lên hệ thống (lỗi không xác định)!';
$_['error_delete']     = 'Cảnh báo: Không thể xoá thư mục này!';
$_['error_limit']      = 'Cảnh báo: Số sản phẩm và phiên bản sản phẩm import tối đa là 500!';
$_['error_not_found_fields']      = 'Cảnh báo: Không tìm thấy tiêu_đề hoặc mô_tả!';
$_['error_file_incomplete']       = 'Cảnh báo: File không đầy đủ thông tin (thiếu cột)!';
$_['error_file_incomplete_in_row']       = 'Cảnh báo: Thiếu cấp_1, cấp_2, cấp_3 hoặc trạng thái không phải là 0, 1 hoặc đường dẫn ảnh không hợp lệ tại dòng ';
$_['error_file_wrong_level_format_in_row']       = 'Cảnh báo: Sai định dạng các cấp sản phẩm tại dòng ';
$_['error_file_category_exist_in_row']       = 'Cảnh báo: Loại sản phẩm đã tồn tại tại dòng ';
$_['error_unknown']                      = 'Lỗi ko xác định';
$_['error_no_file_selected']             = 'Chưa có file nào được chọn!';

$_['error_input_null']                 = 'Vui lòng điền vào trường này';
$_['error_input_price_null']                 = 'Vui lòng nhập giá sản phẩm';
$_['error_input_product_name']         = 'Vui lòng nhập tên sản phẩm';
$_['error_type_mail']                  = 'Vui lòng nhập đúng định dạng email';
$_['error_max_length_255']             = 'Không vượt quá 255 ký tự';
$_['error_max_length_15']              = 'Không vượt quá 15 ký tự';
$_['error_max_length_12']              = 'Không vượt quá 10 ký tự';
$_['error_max_length_14']              = 'Không vượt quá 11 ký tự';
$_['error_type_phone']                 = 'Vui lòng nhập số cho trường này';
$_['error_max_length_10']              = 'Không vượt quá 10 ký tự';
$_['error_max_length_100']             = 'Không vượt quá 100 ký tự';
$_['error_max_length_30']             = 'Không vượt quá 30 ký tự';
$_['error_max_length_20']             = 'Không vượt quá 20 ký tự';
$_['error_max_length_48']             = 'Không vượt quá 48 ký tự';
$_['error_max_length_50']             = 'Không vượt quá 50 ký tự';
$_['error_type_barcode']             = 'Chỉ cho phép nhập chữ và số';
$_['error_type_sku']             =   'Không nhập tiếng việt có dấu';
$_['error_type_weight']         = "Khối lượng sản phẩm lớn hơn hoặc bằng 1 gram và nhỏ hơn 9,999,999 gram";
$_['error_special_char']         = "Không nhập ký tự đặc biệt";
$_['error_type_sku_same']             =   'Mã SKU đã tồn tại';
$_['error_promotion_price']           = 'Giá khuyến mại phải nhỏ hơn giá bán lẻ';
$_['error_promotion_price2']           = 'Giá bán lẻ phải lớn hơn giá khuyến mãi';
$_['error_text_char']           = 'Không được nhập ký tự đặc biệt';

$_['add_category_success']          = "Thêm loại sản phẩm thành công";
$_['add_manufacture_success']       = "Thêm nhà cung cấp thành công";
$_['add_tag_success']               = "Thêm tag thành công";
$_['add_store_success']             = "Thêm cửa hàng thành công";

$_['edit_category_success']                    = "Sửa loại sản phẩm thành công";
$_['edit_category_error_has_discount']         = "Cảnh báo: không sửa được loại sản phẩm đang nằm trong chương trình khuyến mại đang hoạt động";
$_['edit_manufacture_success']                 = "Sửa nhà cung cấp thành công";
$_['edit_manufacture_error_has_discount']      = "Cảnh báo: không sửa được nhà cung cấp đang nằm trong chương trình khuyến mại đang hoạt động";
$_['edit_tag_success']                         = "Sửa tag thành công";
$_['edit_store_success']                       = "Sửa cửa hàng thành công";

$_['delete_category_success']       = "Xóa loại sản phẩm thành công";
$_['delete_manufacture_success']    = "Xóa nhà cung cấp thành công";
$_['delete_tag_success']            = "Xóa tag thành công";
$_['delete_store_success']          = "Xóa cửa hàng thành công";

$_['delete_category_error']                      = "Xóa loại sản phẩm thất bại";
$_['delete_category_error_has_discount']         = "Cảnh báo: không xoá được loại sản phẩm đang nằm trong chương trình khuyến mại đang hoạt động";
$_['delete_manufacture_error']                   = "Xóa nhà cung cấp thất bại";
$_['delete_manufacture_error_has_discount']      = "Cảnh báo: không xoá được nhà cung cấp đang nằm trong chương trình khuyến mại đang hoạt động";
$_['delete_tag_error']                           = "Xóa tag thất bại";
$_['delete_store_error']                         = "Xóa cửa hàng thất bại";

$_['paginator_show']              = "Hiện thị";
$_['paginator_total']             = "Trên tổng số";
$_['paginator_page']              = "trang";

$_['tooltip_category']            = "sản phẩm thuộc loại";
$_['tooltip_manufacture']         = "sản phẩm được cung cấp bởi";


$_['error_name_empty']         = "Nhập dữ liệu vào tên trường";
$_['error_name_max_length']    = "Tên trường không vượt quá 200 ký tự";
$_['error_name_isset']         = "Tên trường đã tồn tại";
$_['error_not_delete']         = "Không thể xóa khi tồn tại sản phẩm";

// Text file
$_['text_uploaded']              = 'Thành công!';
$_['text_directory']             = 'Thành cồng: Tạo thành công thư mục!';
$_['text_delete']                = 'Thành công: Tệp hoặc thư mục đã được xoá!';
$_['text_product_uploaded']      = ' loại sản phẩm đã được cập nhật.';

//
$_['text_note_max_level_categories']         = "Để khách hàng có trải nghiệm mua sắm tốt nhất trên website, mỗi loại sản phẩm chỉ có tối đa 3 cấp.";
$_['text_count_product_of_category']         = "sản phẩm thuộc loại này";
$_['text_popup_title_category']              = "Tiêu đề loại sản phẩm";
$_['text_enter_popup_title_category']        = "Nhập tiêu đề loại sản phẩm";
$_['text_popup_list_child_category']         = "Loại sản phẩm con";
$_['text_popup_add_child_category']          = "Thêm loại sản phẩm con";
$_['text_popup_enter_name_sub_category']     = "Nhập tên loại sản phẩm con";
$_['text_popup_add_category_of_select2']     = "Thêm";
$_['text_popup_enter_new_category']          = "Nhập loại sản phẩm mới";
$_['text_popup_image_note']                  = "Ảnh mô tả loại sản phẩm sẽ hiển thị trên giao diện website. Mỗi loại sản phẩm chị được dùng 1 ảnh mô tả.";
$_['text_popup_image_title']                 = "Ảnh mô tả";
$_['text_popup_image_edit']                  = "Thay đổi";
$_['text_popup_image_remove']                = "Xóa";

$_['select2_notice_search']                  = "Tìm kiếm ...";
$_['select2_notice_load_more']               = "Đang tải ...";
$_['select2_notice_category_exist']          = "Loại sản phẩm đã tồn tại";
$_['select2_notice_not_result']              = "Không tìm thấy kết quả phù hợp";

// export
$_['export_file_col_level_1']           = 'cấp_1';
$_['export_file_col_level_2']           = 'cấp_2';
$_['export_file_col_level_3']           = 'cấp_3';
$_['export_file_col_status']            = 'trạng_thái';
$_['export_file_col_image_url']         = 'đường_dẫn_ảnh';

// for import different language
$_['export_file_col_level_1_2']           = 'level_1';
$_['export_file_col_level_2_2']           = 'level_2';
$_['export_file_col_level_3_2']           = 'level_3';
$_['export_file_col_status_2']            = 'status';
$_['export_file_col_image_url_2']         = 'image_url';

$_['category_import_file']                    = 'Nhập danh sách';
$_['upload_category_list_title']              = 'Nhập danh sách';
$_['button_upload_continue']                  = 'Nhập danh sách loại sản phẩm';
$_['upload_category_sample_text']             = 'Tải file mẫu nhập danh sách';
$_['here']                                    = 'tại đây';
$_['choose_file']                             = 'Chọn file';

$_['text_toggle_category_products']       = 'Chọn sản phẩm gắn/ bỏ gắn với loại sản phẩm';
$_['search_product_category_placeholder'] = 'Chọn sản phẩm gắn với loại sản phẩm';
$_['search_product']                      = 'Tìm kiếm sản phẩm';
$_['text_remove_all_category_products']   = 'Bỏ gắn tất cả sản phẩm';

// v2.8.3
$_['txt_category_title']                = 'Tiêu đề hiển thị trên website';
$_['txt_des_category']                  = 'Mặc định sẽ hiểu thị là danh mục sản phẩm';
$_['txt_placeholder_category']          = 'Danh mục sản phẩm';
$_['txt_confirm']                           = 'Xác nhận';
$_['error_category_title']                  = 'Danh mục sản phẩm không được vượt quá 50 ký tự';
$_['text_success_update']                   = 'Cập nhật thành công';