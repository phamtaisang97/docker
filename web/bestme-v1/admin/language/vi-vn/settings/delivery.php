<?php
// Heading
$_['heading_title']                  = 'Cài đặt vận chuyển';

// Text
$_['breadcrumb_setting']                        = 'Cài đặt';
$_['breadcrumb_delivery_setting']               = 'Cài đặt vận chuyển';

$_['delivery_setting_title']                    = 'Vận chuyển';

$_['delivery_setting_fee_title']                = 'Phí vận chuyển';
$_['delivery_setting_fee_description']          = 'Thêm phí vận chuyển cho từng khu vực vận chuyển khác nhau.';

$_['delivery_methods_title']                    = 'Phương thức vận chuyển';
$_['delivery_methods_description']              = 'Lựa chọn phương thức vận chuyển bạn muốn kích hoạt cho website bán hàng của bạn.';
$_['delivery_methods_add_button']               = 'Thêm phương thức vận chuyển';

// form
$_['delivery_methods_form_name']                         = 'Tên phương thức';
$_['delivery_methods_form_name_placeholder']             = 'Tên phương thức';
$_['delivery_methods_form_fee']                          = 'Phí vận chuyển';
$_['delivery_methods_form_fee_region']                   = 'Khu vực:';
$_['delivery_methods_form_fee_region_setting_button']    = 'Chỉnh sửa';
$_['delivery_methods_form_fee_region_add_button']        = 'Thêm khu vực';

$_['delivery_methods_form_button_save']                  = 'Lưu thay đổi';
$_['delivery_methods_form_button_cancel']                = 'Bỏ qua';

$_['delivery_methods_item_status_activated']             = 'Đang kích hoạt:';
$_['delivery_methods_item_status_deactivated']           = 'Ngưng kích hoạt:';
$_['delivery_methods_item_edit_button']                  = 'Chỉnh sửa';
$_['delivery_methods_item_activated_button']             = 'Kích hoạt';
$_['delivery_methods_item_deactivated_button']           = 'Ngưng kích hoạt';
$_['delivery_methods_item_delete_button']                = 'Xóa';

// fee region form
$_['delivery_methods_form_fee_region_title']                      = 'Cài đặt khu vực';
$_['delivery_methods_form_fee_region_name']                       = 'Tên khu vực vận chuyển';
$_['delivery_methods_form_fee_region_name_placeholder']           = 'Nhập tên khu vực';
$_['delivery_methods_form_fee_region_provinces']                  = 'Các tỉnh thành thuộc khu vực vận chuyển';
$_['delivery_methods_form_fee_region_provinces_placeholder']      = 'Chọn tỉnh thành';
$_['delivery_methods_form_fee_region_weight_step']                = 'Nấc khối lượng';
$_['delivery_methods_form_fee_region_price']                      = 'Giá vận chuyển';
$_['delivery_methods_form_fee_region_weight_step_from']           = 'Từ';
$_['delivery_methods_form_fee_region_weight_step_above']           = 'Trên';
$_['delivery_methods_form_fee_region_weight_step_from_type']      = 'FROM';
$_['delivery_methods_form_fee_region_weight_step_upper']          = 'Trên';
$_['delivery_methods_form_fee_region_weight_step_upper_type']     = 'ABOVE';
$_['delivery_methods_form_fee_region_weight_step_add_button']     = '+ Thêm nấc khối lượng';
$_['delivery_methods_form_fee_region_quick_delete']               = 'Xóa';
$_['delivery_methods_form_fee_region_delete_button']              = 'Xóa khu vực';
$_['delivery_methods_form_fee_region_save_button']                 = 'Lưu';
$_['delivery_methods_form_fee_region_cancel_button']              = 'Bỏ qua';

$_['delivery_methods_form_fee_region_delete_confirm']       = 'Bạn có chắc chắn xóa khu vực này?';
$_['delivery_methods_form_delete_confirm']                  = 'Bạn có chắc chắn xóa phương thức vận chuyển này?';

// fee COD
$_['delivery_methods_form_cod_title']                  = 'Phí thu hộ';
$_['delivery_methods_form_cod_fixed_price']            = 'Giá cố định';
$_['delivery_methods_form_cod_fixed_title']            = 'Phí mặc định';
$_['delivery_methods_default_text']                    = 'Phương thức mặc định: ';
$_['delivery_methods_form_cod_fixed_price_suffix']     = 'VND / mỗi đơn hàng';
$_['delivery_methods_form_cod_dynamic_price']          = 'Theo % giá trị đơn hàng';
$_['delivery_methods_form_cod_dynamic_price_suffix']   = '(%) giá trị đơn hàng';
$_['help_text']                                = 'Phương thức vận chuyển mặc định, hiển thị khi không có phương thức nào hỗ trợ địa chỉ nhận hàng';
// Error form
$_['error_delivery_id']                          = 'Lỗi: Khởi tạo thông tin phương thức vận chuyển mới bị lỗi, vui lòng thử lại';
$_['error_name']                                 = 'Lỗi: Tên phương thức vận chuyển đang để trống hoặc quá ngắn hoặc quá dài (1-128 ký tự) hoặc đã tồn tại';
$_['error_guide']                                = 'Lỗi: Hướng dẫn phương thức vận chuyển quá ngắn hoặc quá dài (1-2000 ký tự)';
$_['error_status']                               = 'Lỗi: Trạng thái phương thức vận chuyển không đúng, vui lòng thử lại';
$_['error_provinces']                            = 'Lỗi: Vui lòng chọn tỉnh thành';
$_['error_step_price']                           = 'Lỗi: Giá vận chuyển phải lớn hơn hoặc bằng không';
$_['error_weight']                               = 'Lỗi: khối lượng vận chuyển phải tăng dần';
$_['error_weight_from_to']                       = 'Lỗi: khối lượng vận chuyển phải tăng dần';

$_['error_input_null']                 = 'Vui lòng điền vào trường này';
$_['error_type_mail']                  = 'Vui lòng nhập đúng định dạng email';
$_['error_max_length_255']             = 'Không vượt quá 255 ký tự';
$_['error_max_length_15']              = 'Không vượt quá 15 ký tự';
$_['error_type_phone']                 = 'Vui lòng nhập số cho trường này';
$_['error_max_length_10']              = 'Không vượt quá 10 ký tự';
$_['error_require_and_active']         = 'Phải tồn tại ít nhất 1 phương thức vận chuyển ở trạng thái kích hoạt.';

$_['text_success']              = 'Cập nhật thông tin thành công!';
$_['text_success_add_delivery']              = 'Thêm phương thức vận chuyển thành công!';
$_['text_success_delete']              = 'Xóa phương thức vận chuyển thành công!';
$_['text_success_edit_status']              = 'Cập nhật trạng thái phương thức vận chuyển thành công!';

$_['text_ghn']   =  'Giao hàng nhanh';
$_['text_viettelpost']   =  'ViettelPost';
$_['text_shipping_unit']    = 'Đơn vị vận chuyển';
$_['text_bestme_connection_with_shipping_partners']    = 'Kết nối ' . PRODUCTION_BRAND_NAME . ' với các đối tác vận chuyển giúp bạn tạo vận đơn và quản lý thuận tiện hơn.';
$_['text_fast_delivery_connection']    = 'Kết nối giao hàng nhanh';
$_['text_after_processing_the_order_on_Bestme']    = 'Sau khi xử lý đơn hàng trên ' . PRODUCTION_BRAND_NAME . ', hệ thống sẽ tự động tạo vận đơn trên Giao Hàng Nhanh.';
$_['text_connected']    = 'Đã kết nối';
$_['text_see_the_instructions']    = 'Xem hướng dẫn';
$_['text_connect']    = 'Kết nối ngay';
$_['text_create_new_account']    = 'Tạo tài khoản mới';
$_['text_collection_of_goods']    = 'Địa điểm lấy hàng';
$_['text_cancel_collection']    = 'Hủy kết nối';
$_['text_user_name_ghn']    = 'Tài khoản GHN (Email/Số điện thoại)';
$_['text_password']    = 'Mật khẩu';
$_['text_button_connect']    = 'Kết nối';
$_['text_button_cancel']    = 'Bỏ qua';
$_['text_button_otp'] = 'Lấy mã OTP';
$_['ghn_otp_placeholder'] = 'Nhập mã OTP tại đây';
$_['text_ghn_otp'] = 'Bấm "Kết nối" sẽ cấp quyền cho ' . PRODUCTION_BRAND_NAME . ' có thể tạo đơn trên cửa hàng bạn chọn tại Giao hàng nhanh. Vui lòng nhập mã OTP để xác nhận.';
$_['text_vp_delivery_connection']    = 'Kết nối ViettelPost';
$_['text_after_processing_the_order_on_Bestme_vp']    = 'Sau khi xử lý đơn hàng trên ' . PRODUCTION_BRAND_NAME . ', hệ thống sẽ tự động tạo vận đơn trên ViettelPost.';
$_['text_user_name_vp']    = 'Tài khoản ViettelPost (Email/Số điện thoại)';
$_['text_disconnecting_from']   =  'Hủy kết nối với đơn vị vận chuyển';
$_['text_after_canceling_the_connection']   =  'Sau khi hủy kết nối, website sẽ không thể tiếp tục tự động tạo vận đơn trên';
$_['text_are_you_sure']   =  'Bạn có chắc chắn muốn hủy';
$_['text_close']   =  'Đóng';
$_['text_destroy']   =  'Hủy';
$_['text_link_location']   =  'Liên kết địa điểm lấy hàng';
$_['text_address_config']   =  'Cấu hình địa chỉ';
$_['text_token'] = 'Mã thông báo API';
$_['text_token_placeholder'] = 'Nhập Token API vào đây';
$_['text_token_error'] = 'Token đã nhập không tồn tại. Vui lòng kiểm tra lại.';
$_['text_ghn_error_403'] = '<p>Bạn cần lấy mã OTP từ GHN để hoàn tất cài đặt sử dụng.</p> 
<p>Bước 1: Truy cập ứng dụng GHN và ấn nút Liên kết địa điểm.</p>
<p>Bước 2: Ấn nút Lấy mã OTP, nhập mã nhận được vào ô tương ứng.</p>
<p>Bước 3: Ấn Lưu để hoàn tất cài đặt.</p>';

$_['delivery_methods_form_fee_region_price_step']                = 'Nấc giá trị';
$_['delivery_methods_form_fee_region_price_step_add_button']     = '+ Thêm nấc giá trị';
$_['text_error']   =  '(Lỗi)';
$_['text_for_weight_step']   =  'Theo nấc khối lượng';
$_['text_for_price_step']   =  'Theo nấc giá trị';
$_['error_price']                               = 'Lỗi: nấc giá trị vận chuyển phải tăng dần';
$_['error_price_from_to']                       = 'Lỗi: nấc giá trị vận chuyển phải tăng dần';
