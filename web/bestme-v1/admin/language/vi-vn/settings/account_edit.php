<?php
// Heading
$_['heading_title']                  = 'Cài đặt tài khoản';

// Text
$_['breadcrumb_setting']                        = 'Cài đặt';
$_['breadcrumb_account_setting']                = 'Cài đặt tài khoản';
$_['breadcrumb_admin_account_setting']          = 'Chủ cửa hàng';

$_['save_setting_button']                       = 'Lưu thay đổi';
$_['skip_setting_button']                       = 'Bỏ qua';


$_['txt_account']                       = 'Tài khoản';

// Confirm password
$_['confirm_password_title']                    = 'Xác nhận mật khẩu của bạn';
$_['confirm_password_message']                  = 'Vui lòng xác nhận bạn có quyền thay đổi ';
$_['confirm_password_placeholder']              = 'Xác nhận mật khẩu';
$_['confirm_password_skip']                     = 'Bỏ qua';
$_['confirm_password_accept']                   = 'Xác nhận';

// Form
$_['admin_account_title']                       = 'Chủ cửa hàng';
$_['admin_account_description']                 = 'Thay đổi thông tin tài khoản quản trị trang web.';
$_['admin_account_profile']                     = 'Hồ sơ tài khoản';
$_['admin_account_add_image']                   = 'Thêm ảnh';
$_['admin_account_firstname']                   = 'Tên';
$_['admin_account_firstname_placeholder']       = 'Nhập tên';
$_['admin_account_lastname']                    = 'Họ';
$_['admin_account_lastname_placeholder']        = 'Nhập họ';
$_['admin_account_email']                       = 'Địa chỉ email';
$_['admin_account_email_placeholder']           = 'Nhập Email';
$_['admin_account_phone']                       = 'Số điện thoại';
$_['admin_account_phone_placeholder']           = 'Nhập số điện thoại';
$_['admin_account_intro']                       = 'Thông tin giới thiệu (Tùy chọn)';
$_['admin_account_intro_placeholder']           = 'Nhập thông tin giới thiệu';

// Change password
$_['admin_account_change_password']                       = 'Đổi mật khẩu';
$_['admin_account_change_password_description']           = 'Đổi mật khẩu mà bạn dùng để đăng nhập vào tài khoản ';
$_['admin_account_change_password_button']                = 'Đổi mật khẩu';
$_['admin_account_new_password']                          = 'Nhập mật khẩu mới';
$_['admin_account_new_password_placeholder']              = 'Mật khẩu mới';
$_['admin_account_new_password_confirm']                  = 'Xác nhận mật khẩu mới';
$_['admin_account_new_password_confirm_placeholder']      = 'Nhập lại mật khẩu';
// alert
$_['alet_password_confirm']                     = 'Mật khẩu và mật khẩu xác nhận không giống nhau !';

$_['error_empty']                  = 'Không được bỏ trống!';
$_['error_input_max']              = 'Dữ liệu trường dài hơn 255!';
$_['error_input_max_min_telephone']          = 'Dữ liệu trường phải lớn hơn 8 nhỏ hơn 15!';
$_['error_input_max_min_password']           = 'Dữ liệu trường phải lớn hơn 6 nhỏ hơn 255!';
$_['error_number']                           = 'Bạn phải nhập kiểu số!';