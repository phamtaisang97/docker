<?php
// Heading
$_['heading_title']                  = 'Cài đặt';

// title
$_['title_general_settings']         = 'Cài đặt chung';
$_['title_account_settings']         = 'Cài đặt tài khoản';
$_['title_payment_methods']          = 'Phương thức thanh toán';
$_['title_transport']                = 'Vận chuyển';

// text
$_['text_general_settings']          = 'Cập nhật, thay đổi thông tin cửa hàng của bạn';
$_['text_account_settings']          = 'Quản lý tài khoản và phân quyền quản lý';
$_['text_payment_methods']           = 'Cài đặt phương thức thanh toán cho cửa hàng online';
$_['text_transport']                 = 'Cài đặt phương thức vận chuyển đơn hàng';

$_['vtp_error_singin']               = 'Thông tin tài khoản chưa chính xác vui lòng kiểm tra lại';
$_['vtp_error_address']              = 'Tài khoản chưa có địa chỉ lấy hàng, vui lòng cập nhật tại https://viettelpost.vn. Sau đó kết nối lại';
$_['vtp_error_token']                = 'Bạn cần kết nối đúng tài khoản ViettelPost';

$_['classify']                       = 'Phân loại';
$_['classify_description']           = 'Quản lý loại sản phẩm, thẻ tags ...';
$_['notify']                         = 'Thông báo';
$_['notify_description']             = 'Quản lý thông báo đơn hàng thành công, đăng ký thành công ...';