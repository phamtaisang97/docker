<?php
// Heading
$_['heading_title'] = 'Nhóm nhân viên';

$_['heading_add']  = 'Thêm nhóm nhân viên';
$_['heading_edit'] = 'Sửa nhóm nhân viên';

$_['button_save']   = 'Lưu lại';
$_['button_cancel'] = 'Hủy bỏ';

// Text
$_['breadcrumb_setting']         = 'Cài đặt';
$_['breadcrumb_account_setting'] = 'Cài đặt tài khoản';
$_['breadcrumb_add_group']       = 'Thêm nhóm nhân viên';
$_['breadcrumb_edit_group']      = 'Sửa nhóm nhân viên';

$_['entry_group_name']       = 'Tên nhóm nhân viên';
$_['placeholder_group_name'] = 'Nhập tên nhóm nhân viên';
$_['entry_rule']             = 'Phân quyền';
$_['entry_block_access_all'] = 'Giới hạn tài khoản thuộc nhóm này chỉ xem/xem và sửa dữ liệu do mình tạo ra';
$_['note_block_access_all']  = 'Nếu tắt lựa chọn này, tài khoản thuộc nhóm nhân viên này sẽ có quyền chỉ xem/xem và sửa dữ liệu toàn bộ dữ liệu hệ thống (bao gồm cả dữ liệu do tài khoản khác tạo ra).';

$_['entry_feature']         = 'Tính năng';
$_['entry_block']           = 'Không có quyền';
$_['entry_just_view']       = 'Chỉ xem';
$_['entry_view_and_modify'] = 'Xem và sửa';
$_['text_permission']       = 'Phân quyền';

$_['text_order']                   = 'Đơn hàng';
$_['text_return_receipt']          = 'Phiếu trả hàng';
$_['text_product']                 = 'Sản phẩm';
$_['text_collection']              = 'Bộ sưu tập';
$_['text_manufacturer']            = 'Nhà cung cấp';
$_['text_store_receipt']           = 'Nhập hàng';
$_['text_store_transfer_receipt']  = 'Chuyển hàng';
$_['text_store_take_receipt']      = 'Kiểm hàng';
$_['text_cost_adjustment_receipt'] = 'Điều chỉnh giá vốn';
$_['text_customer']                = 'Khách hàng';
$_['text_customer_group']          = 'Nhóm khách hàng';
$_['text_cash_flow']               = 'Sổ quỹ';
$_['text_report']                  = 'Báo cáo';
$_['text_discount']                = 'Khuyến mại';
$_['text_website']                 = 'Website';
$_['text_pos']                     = 'Pos';
$_['text_shopee']                  = 'Shopee';
$_['text_lazada']                  = 'Lazada';
$_['text_novaonx_social']          = 'Novaonx social';
$_['text_list_store']              = 'Danh sách cửa hàng';
$_['text_setting']                 = 'Cài đặt hệ thống';
$_['text_blog']                 = 'Bài viết';
$_['text_ingredient']                 = 'Thành phần';

$_['text_error_delete']   = 'Xóa nhóm nhân viên thất bại';
$_['text_success_delete'] = 'Xóa nhóm nhân viên thành công';
$_['text_success_add']    = 'Tạo nhóm nhân viên Thành công';
$_['text_success_edit']   = 'Sửa nhóm nhân viên thành công!';

$_['error_permission']  = 'Cảnh báo: Yêu cầu bị từ chối do thẩm quyền!';
$_['error_name_length'] = 'Tên nhóm nhân viên tối đa 50 ký tự';
$_['error_name_empty']  = 'Tên nhóm nhân viên không được để trống';
$_['error_name_unique'] = 'Tên nhóm nhân viên này đã tồn tại, vui lòng nhập tên khác';