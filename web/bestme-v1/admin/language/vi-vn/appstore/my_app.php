<?php

// Heading
$_['heading_title']                      = 'Kho ứng dụng';
$_['heading_title_expiration']           = 'Ứng dụng hết hạn';

// Text
$_['text_detail']                        = 'Xem chi tiết';
$_['text_expire']                        = 'Hết hạn';
$_['text_app_installed']                 = 'Ứng dụng đã cài đặt';
$_['text_app_suggest']                   = 'Ứng dụng được đề xuất';
$_['text_delete_app']                    = 'Xóa ứng dụng';
$_['text_alert_delete_app']              = "Hành động này sẽ xóa toàn bộ dữ liệu của ứng dụng, ngoại trừ số ngày sử dụng còn lại.
                                            Bạn có chắc chắn muốn xóa ứng dụng này không ? Hành động này không thể khôi phục!";
$_['text_alert_expiration']              = "Ứng dụng của bạn đã hết thời gian sử dụng. Vui lòng liên hệ hotline <strong>" . PRODUCTION_BRAND_PHONE . "</strong> để được tư vấn.";
$_['text_empty_app']                     = "Bạn chưa cài đặt ứng dụng nào";
$_['text_to_app_store']                  = "Kho ứng dụng " . PRODUCTION_BRAND_NAME . "";
$_['text_see_more_app']                  = "Xem thêm các ứng dụng trên kho ứng dụng của chúng tôi";
$_['text_free']                          = "Miễn phí";
$_['text_forever']                       = "Vĩnh viễn";

// Action
$_['action_accept']                      = "Xác nhận";
$_['action_skip']                        = "Bỏ qua";
