<?php
// Heading
$_['heading_title']          = 'App store';
$_['heading_title_edit']     = 'Edit App';

// Text
$_['text_home']           = 'Dashboard';
$_['text_success']           = 'Success: You have successfully updated the App!';
$_['text_list']              = 'App list';
$_['text_add']               = 'Add new category';
$_['text_edit']              = 'Edit category';
$_['text_default']           = 'Default';
$_['text_keyword']           = 'Don\'t use spaces, instead use diacritics - and certainly SEO URLs are unique in the system.';

// Column
$_['column_name']            = 'Category name';
$_['column_sort_order']      = 'Sort';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Category name';
$_['entry_description']      = 'Description';
$_['entry_meta_title'] 	     = 'Meta tag title';
$_['entry_meta_keyword']     = 'Meta tag keyword';
$_['entry_meta_description'] = 'Meta tag description';
$_['entry_store']            = 'Stores';
$_['entry_keyword']          = 'Keyword';
$_['entry_parent']           = 'Parent folder';
$_['entry_filter']           = 'Filter';
$_['entry_image']            = 'Image';
$_['entry_top']              = 'Top';
$_['entry_column']           = 'Columns';
$_['entry_sort_order']       = 'Sort';
$_['entry_status']           = 'Status';
$_['entry_layout']           = 'Override Layout';

// Help
$_['help_filter']            = '(Auto)';
$_['help_top']               = 'Displayed at the top of the Menu bar. Works only for parent level catalogs.';
$_['help_column']            = 'Number of columns used for 3 Categories below. Works only for parent level catalogs.';

// Error
$_['error_warning']          = 'Warning: Please double-check the information form to identify the error!';
$_['error_permission']       = 'Warning: You do not have permission to edit Category information!';
$_['error_name']             = 'Category name is between 1 and 255 characters long!';
$_['error_meta_title']       = 'Text to Meta tag is 1 to 255 characters long!';
$_['error_keyword']          = 'SEO URL already exists!';
$_['error_unique']           = 'The required SEO URL is unique! ';
$_['error_parent']           = 'The parent category you have selected is a Subcategory of another Category!';
$_['category_heading']           = 'Product category';
$_['category_add']           = 'Add category';
$_['category_filter']           = 'Filter';
$_['category_detail']           = 'Category information';
$_['category_name']           = 'Category name';
$_['category_description_text']           = 'Description ';
$_['category_parent']           = 'Category';
$_['category_parent_des']           = 'Select the category <b> contains the newly created category </b>';
$_['category_choose_product']           = 'Select product of category ';
$_['category_choose_menu']           = 'Select manu ';
$_['category_choose_menu_des']           = 'Select the menu <b> contains the newly created category </b>';
$_['category_edit']           = 'Edit category';
$_['category_product_add']           = 'Add new product';
$_['category_cancel_button']           = 'Cancel';
$_['category_save_button']           = 'Confirm';
$_['category_order_by']           = 'Sort by';
$_['category_created_at']           = 'Created day';
$_['category_updated_at']           = 'Updated day';
$_['category_choose_option']           = 'Choose option';
$_['category_delete_success']         = "Delete success";
$_['category_delete_error']         = "Delete error";
$_['category_add_success']         = "Add success";
$_['category_add_error']         = "Add error";
$_['error_install_app'] = "Có lỗi khi cài đặt app";