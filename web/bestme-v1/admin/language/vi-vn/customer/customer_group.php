<?php
// Heading
$_['heading_title']          = 'Nhóm khách hàng';
$_['heading_title_list']     = 'Danh sách nhóm';

// Text
$_['text_success']      = 'Thêm mới thành công !';
$_['text_del_success']  = 'Xóa nhóm thành công !';
$_['text_edit_success'] = 'Sửa nhóm thành công !';
$_['text_del']          = 'Xóa nhóm';
$_['text_list']         = 'Nhóm khách hàng';
$_['text_add']          = 'Thêm nhóm';
$_['text_edit']         = 'Sửa nhóm';
$_['text_name']         = 'Tên nhóm';
$_['text_description']  = 'Mô tả';
$_['text_close']        = 'Hủy';
$_['text_search_list']  = 'Tìm kiếm';
$_['text_customer']  = 'Khách hàng';

//table
$_['text_customer_group_code']  = 'Mã nhóm khách hàng';
$_['text_customer_group_name']  = 'Tên nhóm khách hàng';
$_['text_number_of_customer']   = 'Số lượng khách hàng';
$_['text_action']               = 'Hành động';

$_['text_no_results']               = 'Không có dữ liệu !';
// form
$_['upload_group_customer_title']   = 'Thêm nhóm mới';
$_['edit_group_customer_title']     = 'Sửa nhóm khách hàng';

// Entry
$_['entry_name']        = 'Nhập tên nhóm';
$_['entry_description'] = 'Nhập mô tả';

//alert
$_['alert_delete']          = 'Cảnh báo';
$_['alert_message_del']     = 'Bạn có chắc chắn muốn xóa nhóm khách hàng này không ? nhóm khách hàng này sẽ không xuất hiện trong phần thông tin khách hàng nữa';


// Error
$_['error_name_empty']         = "Nhập dữ liệu vào tên trường";
$_['error_name_max_length']    = "Tên trường không vượt quá 50 ký tự";
$_['error_name_isset']         = "Tên trường đã tồn tại";