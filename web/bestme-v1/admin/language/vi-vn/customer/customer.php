<?php
// Heading
$_['heading_title']             = 'Khách hàng';
$_['heading_title_add']         = 'Thêm khách hàng';
$_['heading_title_list']        = 'Danh sách khách hàng';
$_['heading_title_list_email_subcribers']        = 'Email đã đăng ký nhận thông tin';
$_['heading_detail_title']      = 'Chi tiết khách hàng';

// Text
$_['text_success']              = 'Thành công: Bạn đã cập nhật thành công Khách hàng!';
$_['text_list']                 = 'Danh sách khách hàng';
$_['text_add']                  = 'Thêm khách hàng';
$_['text_edit']                 = 'Sửa thông tin khách hàng';
$_['text_default']              = 'Mặc định';
$_['text_account']              = 'Thông tin khách hàng';
$_['text_password']             = 'Mật khẩu';
$_['text_other']                = 'Khác';
$_['text_affiliate']            = 'Chi tiết tiếp thị';
$_['text_payment']              = 'Chi tiết thanh toán';
$_['text_balance']              = 'Tài khoản';
$_['text_cheque']               = 'Cheque';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Chuyển khoản ngân hàng';
$_['text_history']              = 'Lịch sử';
$_['text_history_add']          = 'Thêm lịch sử';
$_['text_transaction']          = 'Vận chuyển';
$_['text_transaction_add']      = 'Thêm vận chuyển';
$_['text_reward']               = 'Điểm thưởng';
$_['text_reward_add']           = 'Điểm thưởng';
$_['text_ip']                   = 'IP';
$_['text_option']               = 'Tuỳ chọn';
$_['text_login']                = 'Đăng nhập vào cửa hàng';
$_['text_unlock']               = 'Mở khoá tài khoản';
$_['text_lock']                 = 'Khoá tài khoản';
$_['text_delete']               = 'Xóa tài khoản';
$_['text_choose_action']        = 'Chọn thao tác';
$_['text_delete_success']       = 'Xóa thành công!';
$_['text_delete_fail']          = 'Xóa thất bại!';
$_['text_search_placehoder']    = 'Tìm kiếm theo họ tên';
$_['text_selected_all']         = 'Chọn tất cả';
$_['choose_in_filter']          = "Đã chọn";
$_['item_in_filter']            = "mục";
$_['choose_all_in_filter']      = "Đã chọn tất cả trong trang này.";
$_['text_import_list']          = "Nhập danh sách";
$_['text_export_list']          = "Xuất danh sách";
$_['text_filter_customer']      = "Bộ lọc khách hàng";
$_['text_filter_by']            = "Lọc khách hàng theo:";
$_['text_filter_condition_select']            = "Chọn điều kiện lọc...";
$_['text_filter_status']        = "Trạng thái";
$_['text_filter_group']         = "Nhóm khách hàng";
$_['text_filter']               = "Lọc";
$_['text_search']               = "Tìm kiếm theo tên hoặc mã khách hàng";
$_['text_warning']              = "Cảnh báo";
$_['text_cancel']               = "Hủy bỏ";
$_['text_confirm']              = "Xác nhận";
$_['text_choose_file']          = "Chọn file";
$_['text_here']                 = "tại đây";
$_['upload_customer_sample_text']                   = 'Tải file mẫu nhập danh sách';
$_['upload_customer_note']                          = 'Chú ý khách hàng trùng email(hoặc số điện thoại) sẽ bị ghi đè!';
$_['upload_import_customer_list']                   = 'Nhập danh sách khách hàng';
$_['error_no_file_selected']                        = 'Chưa có file nào được chọn!';
$_['export_file_error']                             = 'Xuất file lỗi. Vui lòng thử lại hoặc liên hệ Bestme để được trợ giúp.';
$_['export_file_name']                              = 'Danh_sách_khách_hàng.xls';
$_['text_customer_active']                          = 'Đang hoạt động';
$_['text_customer_block']                           = 'Đã khóa';
$_['text_warning_block']                            = 'Bạn có chắc chắn muốn khóa tài khoản đã chọn không? Tài khoản bị khóa không thể đăng nhập vào website và không hiển thị trong danh sách khách hàng khi tạo đơn tại admin và POS.';
$_['text_warning_unblock']                          = 'Bạn có chắc chắn muốn bỏ khóa tài khoản đã chọn không? Sau khi bỏ khóa, tài khoản sẽ đăng nhập bình thường vào website và hiển thị trong danh sách khách hàng khi tạo đơn tại admin và POS.';
$_['text_warning_delete']                           = 'Chỉ xóa được tài khoản chưa phát sinh đơn hàng. Bạn muốn tiếp tục không?';
$_['text_warning_file_size']                        = 'Kích thước file không được lớn quá ';
$_['error_filetype']                                = 'Chỉ chấp nhận định dạng .xls và xlsx !';
$_['error_upload']                                  = 'Không thể tải tệp lên hệ thống (lỗi không xác định)!';
$_['error_file_incomplete']                         = 'File không đầy đủ thông tin (thiếu cột)!';
$_['error_file_missing_columns']                    = 'Thiếu họ_và_tên, số_điện_thoại, thông_tin_địa_chỉ tại dòng: ';
$_['error_file_error_columns']                      = ' tại dòng: ';
$_['txt_success']                                   = 'Thành công!';
$_['txt_success_suffix']                            = ' khách hàng đã được cập nhật!';
$_['txt_not_success']                               = 'Không thành công!';
$_['txt_block_success']                             = 'Khóa tài khoản thành công!';
$_['txt_unblock_success']                           = 'Bỏ khóa tài khoản thành công!';
$_['txt_delete_success']                            = 'Xóa tài khoản thành công!';
$_['txt_customer_have_order']                       = 'Tài khoản đã phát sinh đơn!';

// button

$_['button_add_customer']       = 'Thêm khách hàng';
$_['button_add_customer_address']       = 'Thêm thông tin địa chỉ';

// tab
$_['tab_customer_info']         = 'Thông tin khách hàng';
$_['tab_customer_address']      = 'Thông tin địa chỉ';


// Column
$_['column_code']               = 'Mã khách hàng';
$_['column_name']               = 'Tên khách hàng';
$_['column_email']              = 'E-Mail';
$_['column_customer_group']     = 'Nhóm khách hàng';
$_['column_status']             = 'Trạng thái';
$_['column_date_added']         = 'Ngày thêm';
$_['column_note']               = 'Ghi chú';
$_['column_description']        = 'Mô tả';
$_['column_amount']             = 'Tổng';
$_['column_points']             = 'Điểm';
$_['column_ip']                 = 'IP';
$_['column_total']              = 'Tổng số tài khoản';
$_['column_action']             = 'Hành động';
$_['column_customer_source']    = 'Nguồn khách hàng';
$_['column_customer_kind']      = 'Loại khách hàng';

// Entry
$_['entry_customer_group']      = 'Nhóm Khách hàng';
$_['entry_fullname']            = 'Họ tên';
$_['entry_firstname']           = 'Tên';
$_['entry_lastname']            = 'Họ';
$_['entry_email']               = 'Email';
$_['entry_order']               = 'Đơn hàng';
$_['entry_number_of_order']     = 'Tổng số đơn mua';
$_['entry_order_recent']        = 'Đơn hàng gần nhất';
$_['entry_telephone']           = 'Số điện thoại';
$_['entry_newsletter']          = 'Thư thông báo';
$_['entry_status']              = 'Trạng thái';
$_['entry_approved']            = 'Đã chấp nhận';
$_['entry_safe']                = 'An toàn';
$_['entry_password']            = 'Mật khẩu';
$_['entry_confirm']             = 'Xác nhận';
$_['entry_company']             = 'Công ty';
$_['entry_address']             = 'Địa chỉ';
$_['entry_address_2']           = 'Địa chỉ thứ 2';
$_['entry_address_3']           = 'Địa chỉ thứ 3';
$_['entry_city']                = 'Thành phố';
$_['entry_district']            = 'Quận huyện';
$_['entry_postcode']            = 'Mã bưu điện';
$_['entry_country']             = 'Quốc gia';
$_['entry_zone']                = 'Quận / Huyện';
$_['entry_default']             = 'Địa chỉ mặc định';
$_['entry_affiliate']           = 'Tiếp thị';
$_['entry_tracking']            = 'Mã theo dõi';
$_['entry_website']             = 'WebSite';
$_['entry_commission']          = 'Commission (%)';
$_['entry_tax']                 = 'Mã số Thuế';
$_['entry_payment']             = 'Phương thức thanh toán';
$_['entry_cheque']              = 'Tên người thanh toán Cheque';
$_['entry_paypal']              = 'Email tài khoản PayPal';
$_['entry_bank_name']           = 'Tên ngân hàng';
$_['entry_bank_branch_number']  = 'Số ABA/BSB (Số chi nhánh)';
$_['entry_bank_swift_code']     = 'Mã SWIFT Code';
$_['entry_bank_account_name']   = 'Tên Tài khoản';
$_['entry_bank_account_number'] = 'Số Tài khoản';
$_['entry_comment']             = 'Ghi chú';
$_['entry_description']         = 'Mô tả';
$_['entry_amount']              = 'Giá trị đơn mua';
$_['entry_points']              = 'Số điểm';
$_['entry_name']                = 'Tên Khách hàng';
$_['entry_ip']                  = 'IP';
$_['entry_date_added']          = 'Ngày tạo';
$_['entry_customer_source']     = 'Từ website';
$_['entry_customer_kind']       = 'Bình thường';
$_['column_index']               = 'STT';
$_['entry_created']            = 'Ngày đăng ký';
$_['column_options']            = 'Tùy chọn';

// Help
// more...

// Error
$_['error_warning']             = 'Cảnh báo: Vui lòng kiểm tra kỹ form để biết lỗi!';
$_['error_permission']          = 'Cảnh báo: Bạn không có quyền chỉnh sửa Khách hàng!';
$_['error_exists']              = 'Cảnh báo: Địa chỉ email đã được đăng ký trước đó!';
$_['error_phone_exists']        = 'Cảnh báo: Số điện thoại đã được đăng ký trước đó!';
$_['error_phone']               = 'Số điện thoại không hợp lệ!';
// more...

$_['error_input_null']                 = 'Vui lòng điền vào trường này';
$_['error_type_mail']                  = 'Vui lòng nhập đúng định dạng email';
$_['error_max_length_255']             = 'Không vượt quá 255 ký tự';
$_['error_max_length_128']             = 'Không vượt quá 128 ký tự';
$_['error_max_length_15']              = 'Không vượt quá 15 ký tự';
$_['error_type_phone']                 = 'Vui lòng nhập số cho trường này';
$_['error_max_length_10']              = 'Không vượt quá 10 ký tự';
$_['error_min_length_8']              = 'Vui lòng nhập nhiều hơn 8 ký tự';


// Text chart
$_['options']                            = 'Tùy chọn';
$_['monday']                             = 'T2';
$_['tuesday']                            = 'T3';
$_['wednesday']                          = 'T4';
$_['thursday']                           = 'T5';
$_['friday']                             = 'T6';
$_['saturday']                           = 'T7';
$_['sunday']                             = 'CN';
$_['january']                            = 'Tháng 1';
$_['february']                           = 'Tháng 2';
$_['march']                              = 'Tháng 3';
$_['april']                              = 'Tháng 4';
$_['may']                                = 'Tháng 5';
$_['june']                               = 'Tháng 6';
$_['july']                               = 'Tháng 7';
$_['august']                             = 'Tháng 8';
$_['september']                          = 'Tháng 9';
$_['october']                            = 'Tháng 10';
$_['november']                           = 'Tháng 11';
$_['december']                           = 'Tháng 12';
$_['today']                              = 'Hôm nay';
$_['yesterday']                          = 'Hôm qua';
$_['one_week']                           = '1 tuần';
$_['this_week']                          = 'Tuần này';
$_['one_month']                          = '1 tháng';
$_['this_month']                         = 'Tháng này';
$_['decimal_point']                      = ',';
$_['thousand_point']                     = '.';
$_['last_week']                          = 'Tuần trước';
$_['last_month']                         = 'Tháng trước';

// v2.7.3
// form
$_['text_general_information']                      = 'THÔNG TIN CHUNG';
$_['text_full_name']                                = 'Họ và tên';
$_['text_placeholder_full_name']                    = 'Nhập họ và tên';
$_['text_placeholder_telephone']                    = 'Nhập số điện thoại';
$_['text_placeholder_telephone']                    = 'Nhập số điện thoại';
$_['text_placeholder_email']                        = 'Nhập email';
$_['text_group_customer']                           = 'Nhóm khách hàng';
$_['text_chose_group_customer']                     = 'Chọn nhóm khách hàng';
$_['text_province']                                 = 'Tỉnh thành';
$_['text_chose_province']                           = 'Chọn tỉnh thành';
$_['text_district']                                 = 'Quận/Huyện';
$_['text_chose_district']                           = 'Chọn quận/huyện';
$_['text_wards']                                    = 'Phường/Xã';
$_['text_chose_wards']                              = 'Chọn phường/xã';
$_['text_placeholder_address']                      = 'Nhập địa chỉ';
$_['text_advanced_information']                     = 'THÔNG TIN NÂNG CAO';
$_['text_birthday']                                 = 'Ngày sinh';
$_['text_chose_birthday']                           = 'Chọn ngày sinh';
$_['text_sex']                                      = 'Giới tính';
$_['text_sex_male']                                 = 'Nam';
$_['text_sex_female']                               = 'Nữ';
$_['text_sex_other']                                = 'Khác';
$_['text_enter_website']                            = 'Nhập website';
$_['text_tax_code']                                 = 'Mã số thuế';
$_['text_enter_tax_code']                           = 'Nhập mã số thuế';
$_['text_other_information']                        = 'THÔNG TIN KHÁC';
$_['text_staff_in_charge']                          = 'Nhân viên phụ trách';
$_['text_chose_staff_in_charge']                    = 'Chọn nhân viên phụ trách';
$_['text_enter_note']                               = 'Nhập ghi chú';
$_['text_select2_add_new']                          = 'Thêm mới';
$_['text_form_cancel']                              = 'Hủy';
$_['text_form_save']                                = 'Lưu';
$_['text_choose_status']                            = 'Chọn trạng thái';
$_['text_edit']                                     = 'Chỉnh sửa';

//
$_['error_name_max_length']                             = "Tên trường không vượt quá 50 ký tự";
$_['text_error_form_tax_code_more_16']                  = 'Mã số thuế không quá 16 ký tự';
$_['text_error_form_tax_code_special_characters']       = 'Mã số thuế không đúng định dạng';

// Select 2
$_['select2_notice_not_result']   =    "Không tìm thấy kết quả phù hợp";
$_['select2_notice_search']       =    "Tìm kiếm ...";
$_['select2_notice_load_more']    =    "Đang tải ...";

//
$_['error_full_name']                                   = 'Họ và tên có đội dài từ 1 đến 50 ký tự!';
$_['text_error_form_telephone_start']                   = 'Bạn phải nhập kiểu số, bắt đầu bởi 0 hoặc 84!';
$_['text_error_not_found_address']                      = 'Không tìm thấy địa chỉ!';
$_['text_confirm_remove_address']                       = 'Bạn có chắc chắn muốn xóa không?';

//
$_['text_default_address']                                      = 'Địa chỉ mặc định';
$_['text_default_address_desc']                                 = '* Các thông tin bên dưới ứng với thông tin địa chỉ mặc định ( địa chỉ nhận hàng của tài khoản này ).';
$_['text_detail_customer']                                      = 'Chi tiết khách hàng';
$_['text_personal_information']                                 = 'Thông tin cá nhân';
$_['text_address_book']                                         = 'Sổ địa chỉ';
$_['text_history']                                              = 'Lịch sử mua hàng';
$_['text_history_title']                                        = 'Lịch sử đơn hàng';
$_['text_list_address']                                         = 'Danh sách địa chỉ';
$_['text_add_new_address']                                      = 'Thêm địa chỉ mới';
$_['text_title_edit_address']                                   = 'Sửa địa chỉ';
$_['text_remove']                                               = 'Xóa';
$_['text_edit_address']                                         = 'Chỉnh sửa';
$_['text_setting_default_address']                              = 'Đặt làm mặc định';
$_['text_form_setting_default_address']                         = 'Đặt làm địa chỉ mặc định';
$_['text_form_add_address_success']                             = 'Thêm mới địa chỉ thành công!';
$_['text_form_edit_address_success']                            = 'Cập nhật địa chỉ thành công!';
$_['text_form_delete_address_success']                          = 'Xóa địa chỉ thành công!';

// warning import
$_['warning_max_10_address']                          = 'không được quá 10 địa chỉ';
$_['warning_invalid']                                 = 'không đúng định dạng';
$_['warning_max_length_255']                          = 'không được quá 255 ký tự';
$_['warning_max_length_50']                           = 'không được quá 50 ký tự';
$_['warning_max_date']                                = 'không được lớn hơn ngày hiện tại';

//history
$_['column_history_index']                                = 'STT';
$_['column_history_order_code']                           = 'Mã đơn hàng';
$_['column_history_date_added']                           = 'Thời gian đặt';
$_['column_history_total']                                = 'Giá trị đơn';
$_['column_history_status']                               = 'Trạng thái đơn hàng';
$_['column_history_refunds']                              = 'Tiền hàng trả lại';

