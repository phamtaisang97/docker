<?php

// Heading
$_['heading_title'] = 'Tổng quan';
// Title chart
$_['text_revenue_chart_title']    = 'Biều đồ doanh thu và lợi nhuận toàn hệ thống';
$_['number_of_orders']            = 'Số lượng đơn hàng';
$_['a_new_customer']              = 'Khách hàng mới';
$_['top_5_best_selling_products'] = 'Top 5 sản phẩm bán chạy';
$_['see_details']                 = 'Xem chi tiết';
//message
$_['no_statistical_data_available'] = 'Chưa có dữ liệu thống kê. Hãy bắt đầu bán hàng với ' . PRODUCTION_BRAND_NAME . ' Website và kiểm tra các thông số!';
//text
$_['customer']    = 'Khách hàng';
$_['order']       = 'đơn';
$_['amount']      = 'Số lượng';
$_['order_count'] = 'Đơn hàng';
$_['currency']    = 'đ';
$_['_currency']   = '₫';
//$_['_trieu']                             = 'Triệu đ';
$_['_trieu'] = ' đ';

// Text chart
$_['options']        = 'Tùy chọn';
$_['monday']         = 'T2';
$_['tuesday']        = 'T3';
$_['wednesday']      = 'T4';
$_['thursday']       = 'T5';
$_['friday']         = 'T6';
$_['saturday']       = 'T7';
$_['sunday']         = 'CN';
$_['january']        = 'Tháng 1';
$_['february']       = 'Tháng 2';
$_['march']          = 'Tháng 3';
$_['april']          = 'Tháng 4';
$_['may']            = 'Tháng 5';
$_['june']           = 'Tháng 6';
$_['july']           = 'Tháng 7';
$_['august']         = 'Tháng 8';
$_['september']      = 'Tháng 9';
$_['october']        = 'Tháng 10';
$_['november']       = 'Tháng 11';
$_['december']       = 'Tháng 12';
$_['today']          = 'Hôm nay';
$_['yesterday']      = 'Hôm qua';
$_['one_week']       = '1 tuần';
$_['this_week']      = 'Tuần này';
$_['last_week']      = 'Tuần trước';
$_['one_month']      = '1 tháng';
$_['this_month']     = 'Tháng này';
$_['last_month']     = 'Tháng trước';
$_['decimal_point']  = ',';
$_['thousand_point'] = '.';
$_['places']         = 'Thứ hạng';
$_['amount_money']   = 'Số tiền';

$_['text_revenue']                   = 'Doanh thu bán hàng';
$_['text_sales']                     = 'Doanh thu';
$_['text_order']                     = 'Đơn hàng';
$_['text_profit']                    = 'Lợi nhuận';
$_['text_sell_profit']               = 'Lợi nhuận bán hàng';
$_['text_average_revenue_per_order'] = 'Doanh thu / đơn hàng';
$_['text_revenue_rate_from_sources'] = 'Tỷ lệ doanh thu bán hàng từ các nguồn';
$_['text_profit_rate_from_sources']  = 'Tỷ lệ lợi nhuận bán hàng từ các nguồn';
$_['text_title_top_staff']           = 'Top nhân viên có doanh thu cao nhất';
$_['text_title_top_customer']        = 'Top khách hàng chi tiêu nhiều nhất';
$_['text_all_stores']                = 'Tất cả cửa hàng';
$_['text_staff']                     = 'Nhân viên';
$_['text_account']                   = 'Tài khoản';
$_['text_customer']                  = 'Khách hàng';
$_['text_phone']                     = 'Số điện thoại';

// Breadcrumb
$_['breadcrumb_report']   = 'Báo cáo';
$_['breadcrumb_overview'] = 'Tổng quan';
