<?php

// Heading
$_['heading_title']         = 'Báo các kết quả kinh doanh';
$_['heading_title_2']       = 'Báo cáo tài chính';

// Text daterangepicker
$_['options']        = 'Tùy chọn';
$_['monday']         = 'T2';
$_['tuesday']        = 'T3';
$_['wednesday']      = 'T4';
$_['thursday']       = 'T5';
$_['friday']         = 'T6';
$_['saturday']       = 'T7';
$_['sunday']         = 'CN';
$_['january']        = 'Tháng 1';
$_['february']       = 'Tháng 2';
$_['march']          = 'Tháng 3';
$_['april']          = 'Tháng 4';
$_['may']            = 'Tháng 5';
$_['june']           = 'Tháng 6';
$_['july']           = 'Tháng 7';
$_['august']         = 'Tháng 8';
$_['september']      = 'Tháng 9';
$_['october']        = 'Tháng 10';
$_['november']       = 'Tháng 11';
$_['december']       = 'Tháng 12';
$_['today']          = 'Hôm nay';
$_['yesterday']      = 'Hôm qua';
$_['one_week']       = '1 tuần';
$_['this_week']      = 'Tuần này';
$_['last_week']      = 'Tuần trước';
$_['one_month']      = '1 tháng';
$_['this_month']     = 'Tháng này';
$_['last_month']     = 'Tháng trước';

// Breadcrumb
$_['breadcrumb_report']   = 'Báo cáo';
$_['breadcrumb_report_order'] = 'Báo cáo bán hàng';

//
$_['text_no_results'] = 'Không có dữ liệu!';

// text
$_['text_total']                                = 'Tổng';
$_['text_report']                               = 'Báo cáo';
$_['text_export']                               = 'Xuất báo cáo';
$_['text_notes']                                = 'Chú giải';
$_['text_report_notes']                         = 'Chú giải báo cáo';
$_['text_financial_report_notes']               = 'Chú giải báo cáo kết quả kinh doanh';
$_['text_all_stores']                           = 'Tất cả cửa hàng';
$_['text_store']                                = 'Cửa hàng';
$_['text_current_period']                       = 'Kỳ hiện tại';
$_['text_compare_period']                       = 'Kỳ so sánh';
$_['text_previous_period']                      = 'Cùng kỳ trước';
$_['text_financial_spending']                   = 'Chi tiêu tài chính';
$_['text_change']                               = 'Thay đổi(%)';
$_['text_revenue']                              = 'Doanh thu bán hàng (1)';
$_['text_revenue_reduce']                       = 'Giảm trừ doanh thu (2 = 2.1 + 2.2)';
$_['text_trade_discount']                       = 'Chiết khấu thương mại (2.1)';
$_['text_total_return']                         = 'Giá trị hàng bán bị trả lại (2.2)';
$_['text_net_revenue']                          = 'Doanh thu thuần (3 = 1 - 2)';
$_['text_cost']                                 = 'Giá vốn hàng bán (4)';
$_['text_gross_revenue']                        = 'Lợi nhuận gộp (5 = 3 - 4)';
$_['text_fee']                                  = 'Chi phí (6 = 6.1 + 6.2)';
$_['text_delivery_for_partner']                 = 'Phí giao hàng trả đối tác (6.1)';
$_['text_pay_salary']                           = 'Trả lương nhân viên (6.2)';
$_['text_other_fee']                            = 'Chi phí khác (7)';
$_['text_profit']                               = 'Lợi nhuận hoạt động (8 = 5 - 6 -7)';
$_['text_other_income']                         = 'Thu nhập khác (9 = 9.1 + 9.2)';
$_['text_manual_receipt']                       = 'Phiếu thu chủ động (9.1)';
$_['text_return_fee']                           = 'Phí trả hàng (9.2)';
$_['text_enterprise_income_tax_expense']        = 'Chi phí thuế TNDN (10)';
$_['text_net_income']                           = 'Lợi nhuận thuần (11 = 8 + 9 - 10)';


$_['text_note_1']                                   = 'Tổng ( giá bán * số lượng ) và phí vận chuyển của các đơn hàng ( trừ trạng thái Đơn nháp và Đã hủy )';
$_['text_note_2_1']                                 = 'Tổng chiết khấu ( sản phẩm và tổng đơn ) của các đơn hàng ( trừ trạng thái Đơn nháp và Đã hủy )';
$_['text_note_2_2']                                 = 'Tổng tiền ở các phiếu trả hàng';
$_['text_note_3']                                   = 'Doanh thu thực tế của cửa hàng, được sử dụng để tính lợi nhuận thuần';
$_['text_note_4']                                   = 'Tổng ( giá vốn * số lượng sản phẩm) của các đơn hàng ( trừ trạng thái Đơn nháp và Đã hủy )';
$_['text_note_5']                                   = 'Lợi nhuận thu được sau khi trừ các chi phí giá vốn hàng bán';
$_['text_note_6_1']                                 = 'Tổng phí vận chuyển các đơn hàng và lấy từ phiếu chi có loại là Trả nợ đối tác vận chuyển';
$_['text_note_6_2']                                 = 'Lấy từ phiếu chi có loại là Chi phí nhân công';
$_['text_note_7']                                   = 'Từ các phiếu chi có hạch toán kết quả kinh doanh và có loại khác các loại sau: Chi tự động, Trả nợ đối tác vận chuyển, Chi phí nhân công và Chi phí thuế TNDN.';
$_['text_note_8']                                   = 'Lợi nhuận thu được từ hoạt động kinh doanh';
$_['text_note_9_1']                                 = 'Các phiếu thu có hạch toán kết quả kinh doanh và loại khác Thu tự động';
$_['text_note_9_2']                                 = 'Tổng phí trả hàng trong các phiếu trả hàng';
$_['text_note_10']                                  = 'Tổng các phiếu chi có loại Chi phí thuế TNDN';
$_['text_note_11']                                  = 'Lợi nhuận thực tế của cửa hàng sau khi trừ đi tất cả các loại chi phí.';

//
$_['lang_export_error']                              = "Xuất file lỗi. Vui lòng thử lại hoặc liên hệ Bestme để được trợ giúp.";