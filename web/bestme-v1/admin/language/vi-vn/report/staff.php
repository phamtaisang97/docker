<?php
// Heading
$_['heading_title'] = 'Reports';

// Text
$_['text_success'] = 'Success: You have modified reports!';
$_['text_list']    = 'Report List';
$_['text_type']    = 'Choose the report type';
$_['text_filter']  = 'Filter';

// report by product
$_['text_report']                = 'Báo cáo';
$_['text_sales_report']          = 'Báo cáo bán hàng';
$_['text_sales_report_by_staff'] = 'Báo cáo bán hàng theo nhân viên';
$_['text_product']               = 'Sản phẩm';
$_['text_quantity_sold']         = 'Số lượng bán';
$_['text_amount']                = 'Tiền hàng';
$_['text_discount']              = 'Chiết khấu';
$_['text_quantity_return']       = 'Số lượng trả lại';
$_['text_return_amount']         = 'Tiền trả lại';
$_['text_revenue']               = 'Doanh thu';
$_['text_total']                 = 'Tổng';
$_['text_export']                = 'Xuất báo cáo';
$_['text_all_stores']            = 'Tất cả cửa hàng';
$_['text_staff_name']            = 'Tên nhân viên';
$_['text_total_order']           = 'Tổng đơn';
$_['text_shipping_fee']          = 'Phí giao hàng';
$_['export_file_name']           = 'bao_cao_ban_hang_theo_nhan_vien';
$_['text_export_title']          = 'Báo cáo bán hàng theo nhân viên';
$_['text_export_store']          = 'Cửa hàng: ';
$_['text_export_time']           = 'Thời gian: ';


$_['options']        = 'Tùy chọn';
$_['monday']         = 'T2';
$_['tuesday']        = 'T3';
$_['wednesday']      = 'T4';
$_['thursday']       = 'T5';
$_['friday']         = 'T6';
$_['saturday']       = 'T7';
$_['sunday']         = 'CN';
$_['january']        = 'Tháng 1';
$_['february']       = 'Tháng 2';
$_['march']          = 'Tháng 3';
$_['april']          = 'Tháng 4';
$_['may']            = 'Tháng 5';
$_['june']           = 'Tháng 6';
$_['july']           = 'Tháng 7';
$_['august']         = 'Tháng 8';
$_['september']      = 'Tháng 9';
$_['october']        = 'Tháng 10';
$_['november']       = 'Tháng 11';
$_['december']       = 'Tháng 12';
$_['today']          = 'Hôm nay';
$_['yesterday']      = 'Hôm qua';
$_['one_week']       = '1 tuần';
$_['this_week']      = 'Tuần này';
$_['last_week']      = 'Tuần trước';
$_['one_month']      = '1 tháng';
$_['this_month']     = 'Tháng này';
$_['last_month']     = 'Tháng trước';
$_['decimal_point']  = ',';
$_['thousand_point'] = '.';