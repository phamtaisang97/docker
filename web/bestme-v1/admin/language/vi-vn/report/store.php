<?php
// Heading
$_['heading_title']         = 'Báo cáo bán hàng theo cửa hàng';
$_['heading_title_2']       = 'Báo cáo bán hàng';

// Text daterangepicker
$_['options']        = 'Tùy chọn';
$_['monday']         = 'T2';
$_['tuesday']        = 'T3';
$_['wednesday']      = 'T4';
$_['thursday']       = 'T5';
$_['friday']         = 'T6';
$_['saturday']       = 'T7';
$_['sunday']         = 'CN';
$_['january']        = 'Tháng 1';
$_['february']       = 'Tháng 2';
$_['march']          = 'Tháng 3';
$_['april']          = 'Tháng 4';
$_['may']            = 'Tháng 5';
$_['june']           = 'Tháng 6';
$_['july']           = 'Tháng 7';
$_['august']         = 'Tháng 8';
$_['september']      = 'Tháng 9';
$_['october']        = 'Tháng 10';
$_['november']       = 'Tháng 11';
$_['december']       = 'Tháng 12';
$_['today']          = 'Hôm nay';
$_['yesterday']      = 'Hôm qua';
$_['one_week']       = '1 tuần';
$_['this_week']      = 'Tuần này';
$_['last_week']      = 'Tuần trước';
$_['one_month']      = '1 tháng';
$_['this_month']     = 'Tháng này';
$_['last_month']     = 'Tháng trước';

$_['text_no_results'] = 'Không có dữ liệu!';

// column table
$_['text_column_date']                          = 'Thời gian';
$_['text_column_store']                         = 'Cửa hàng';
$_['text_column_order_buy']                     = 'Đơn bán';
$_['text_column_total_amount']                  = 'Tiền hàng';
$_['text_column_discount']                      = 'Chiết khấu';
$_['text_column_shipping_fee']                  = 'Phí vận chuyển';
$_['text_column_order_return']                  = 'Đơn trả';
$_['text_column_money_back']                    = 'Tiền hàng trả lại';
$_['text_column_revenue']                       = 'Doanh thu';
$_['export_file_name']                          = 'bao_cao_ban_hang_tong_quan';

// text
$_['text_total']                                = 'Tổng';
$_['text_report']                               = 'Báo cáo';
$_['text_export']                               = 'Xuất báo cáo';
$_['text_back']                                = 'Quay lại';

//
$_['lang_export_error']                         = "Xuất file lỗi. Vui lòng thử lại hoặc liên hệ Bestme để được trợ giúp.";
