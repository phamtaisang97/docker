<?php
// Heading
$_['heading_title']  = 'Quản trị Bestme';

// Text
$_['text_heading']   = 'Quản trị Bestme';
$_['text_login']     = 'Xin vui lòng nhập thông tin đăng nhập.';
$_['text_forgotten'] = 'Quên mật khẩu';

// Entry
$_['entry_username'] = 'Email';
$_['entry_password'] = 'Mật khẩu';

// Button
$_['button_login']   = 'Đăng nhập';

// Error
$_['error_login']    = 'Email và/hoặc Mật khẩu không đúng.';
$_['error_token']    = 'Phiên làm việc không khả dụng. Vui lòng đăng nhập lại.';