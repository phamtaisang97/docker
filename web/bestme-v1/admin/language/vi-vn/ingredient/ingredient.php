<?php
$_['text_home']                     = 'Trang chủ';
$_['heading_title']                 = 'Thành phần';
$_['heading_title_add']             = 'Thêm mới thành phần';
$_['text_success']                  = 'Thêm mới thành phần thành công';
$_['text_add_failed']               = 'Thêm mới thành phần thất bại';
$_['heading_title_edit']            = 'Sửa thành phần';
$_['breadcrumb_ingredient']         = 'Danh sách thành phần';
$_['text_update_success']                       = 'Cập nhật thành phần thành công';
$_['text_edit_not_found_id']                    = 'Không tìm thấy id thành phần';

// get list
$_['choose_all_in_filter']                                  = "Đã chọn tất cả trong trang này.";
$_['choose_in_filter']                                      = "Đã chọn";
$_['item_in_filter']                                        = "mục";
$_['collection_list_filter_action']                         = 'Chọn thao tác';
$_['action_remove_ingredient']                              = "Xóa thành phần";
$_['collection_confirm_show']                               = 'Bạn có chắc chắn muốn xóa ';
$_['collection_confirm_choose']                             = " thành phần đã chọn ?";

$_['text_list_all']                         = 'Tất cả';
$_['text_code_ingredient']                  = 'Mã thành phần';
$_['text_name_ingredient']                  = 'Tên thành phần';
//$_['text_list_author']                   = 'Tác giả';
//$_['text_list_status']                   = 'Trạng thái';
$_['text_list_modified_date']               = 'Ngày cập nhật';

$_['text_search_list']                  = 'Tìm kiếm thành phần';
$_['txt_add']                           = "Thêm thành phần";
$_['text_close']                        = 'Đóng';
$_['text_remove']                       = 'Xóa';
$_['text_add']                          = 'Thêm mới ';
$_['text_cancel']                       = 'Bỏ qua ';
$_['text_save']                         = 'Lưu thay đổi ';
$_['popup_cancel_title']                = "Hủy thông tin thay đổi";
$_['popup_cancel_message']              = "Các thay đổi sẽ bị mất";
$_['popup_cancel_ok']                   = "Đồng ý";

// error
$_['error_text_check_blog']                     = 'Tạo thành phần thất bại!';
$_['error_permission']                          = 'Cảnh báo: Bạn không có quyền chỉnh sửa thành phần!';

// form
$_['text_name']                                         = 'Tên thành phần';
$_['text_name_placeholder']                             = 'Nhập tên thành phần';
$_['text_code']                                         = 'Mã thành phần';
$_['text_code_placeholder']                             = 'Nhập mã thành phần';
$_['text_active_ingredient']                            = 'Hoạt chất';
$_['text_active_ingredient_placeholder']                = 'Nhập hoạt chất';
$_['text_unit']                                         = 'Đơn vị';
$_['text_unit_placeholder']                             = 'Nhập đơn vị';
$_['text_basic_description']                            = 'Mô tả cơ bản';
$_['text_basic_description_placeholder']                = 'Nhập mô tả cơ bản';
$_['text_note']                                         = 'Lưu ý';
$_['text_note_placeholder']                             = 'Nhập lưu ý';
$_['text_overdose']                                         = 'Quá liều';
$_['text_overdose_placeholder']                             = 'Khuyến cáo quá liều';
$_['text_course_certificates']                              = 'Minh chứng khoá học ';
$_['text_course_certificates_placeholder']                  = 'Nhập minh chứng khoá học';
$_['text_detail_description']                               = 'Mô tả chi tiết';
$_['text_detail_description_placeholder']                   = 'Nhập mô tả chi tiết';
$_['text_recommended_daily_us_man']                         = 'Nhu cầu khuyến nghị hàng ngày cho Nam / Mỹ';
$_['text_recommended_daily_us_women']                       = 'Nhu cầu khuyến nghị hàng ngày cho Nữ / Mỹ';
$_['text_recommended_daily_vn_man']                         = 'Nhu cầu khuyến nghị hàng ngày cho Nam / Việt Nam';
$_['text_recommended_daily_vn_women']                       = 'Nhu cầu khuyến nghị hàng ngày cho Nữ / Việt Nam';
$_['text_input_placeholder']                                = 'Nhập thông tin';

$_['text_us_man_19_30']                                         = 'Từ 19 đến 30 tuổi';
$_['text_us_man_31_50']                                         = 'Từ 31 đến 50 tuổi';
$_['text_us_man_51_70']                                         = 'Từ 51 đến 70 tuổi';
$_['text_us_man_gt_70']                                         = 'Lớn hơn 70 tuổi';

$_['text_us_women_19_30']                                       = 'Từ 19 đến 30 tuổi';
$_['text_us_women_31_50']                                       = 'Từ 31 đến 50 tuổi';
$_['text_us_women_51_70']                                       = 'Từ 51 đến 70 tuổi';
$_['text_us_women_gt_70']                                       = 'Lớn hơn 70 tuổi';

$_['text_vn_man_19_50']                                         = 'Từ 19 đến 50 tuổi';
$_['text_vn_man_51_60']                                         = 'Từ 51 đến 60 tuổi';
$_['text_vn_man_gt_60']                                         = 'Lớn hơn 60 tuổi';

$_['text_vn_women_19_50']                                       = 'Từ 19 đến 50 tuổi';
$_['text_vn_women_51_60']                                       = 'Từ 51 đến 60 tuổi';
$_['text_vn_women_gt_60']                                       = 'Lớn hơn 60 tuổi';

// validate
$_['text_name_empty']                               = 'Tên thành phần không được để trống';
$_['text_name_max_length']                          = 'Tên thành phần không quá 250 kí tự';
$_['text_code_empty']                               = 'Tên thành phần không được để trống';
$_['text_code_max_length']                          = 'Tên thành phần không quá 250 kí tự';

// excel
$_['export_file_col_name']                                          = 'tên thành phần';
$_['export_file_col_code']                                          = 'mã thành phần';
$_['export_file_col_active_ingredient']                             = 'hoạt chất';
$_['export_file_col_unit']                                          = 'đơn vị';
$_['export_file_col_basic_description']                             = 'mô tả cơ bản';
$_['export_file_col_note']                                          = 'lưu ý';
$_['export_file_col_overdose']                                      = 'quá liều';
$_['export_file_col_course_certificates']                           = 'minh chứng khoa học';
$_['export_file_col_detail_description']                            = 'mô tả chi tiết';
$_['export_file_col_us_man_19_30']                                  = 'us_man_19_30';
$_['export_file_col_us_man_31_50']                                  = 'us_man_31_50';
$_['export_file_col_us_man_51_70']                                  = 'us_man_51_70';
$_['export_file_col_us_man_gt_70']                                  = 'us_man_gt_70';
$_['export_file_col_us_women_19_30']                                = 'us_women_19_30';
$_['export_file_col_us_women_31_50']                                = 'us_women_31_50';
$_['export_file_col_us_women_51_70']                                = 'us_women_51_70';
$_['export_file_col_us_women_gt_70']                                = 'us_women_gt_70';
$_['export_file_col_vn_man_19_50']                                  = 'vn_man_19_50';
$_['export_file_col_vn_man_51_60']                                  = 'vn_man_51_60';
$_['export_file_col_vn_man_gt_60']                                  = 'vn_man_gt_60';
$_['export_file_col_vn_women_19_50']                                = 'vn_women_19_50';
$_['export_file_col_vn_women_51_60']                                = 'vn_women_51_60';
$_['export_file_col_vn_women_gt_60']                                = 'vn_women_gt_60';

//excel 2
$_['export_file_col_product_sku']                                   = 'mã sản phẩm';
$_['export_file_col_ingredients']                                   = 'thành phần';
$_['export_file_col_quantitative']                                  = 'định lượng';

$_['ingredient_import_file']               = 'Nhập danh sách';
$_['import_file_product_ingredient']       = 'Nhập thành phần cho sản phẩm';
$_['choose_file']                          = 'Chọn file';
$_['button_upload_continue']               = 'Nhập danh sách sản phẩm';
$_['upload_product_list_title']            = 'Nhập danh sách';
$_['txt_warning_import_product']           = 'Chú ý: các thành phần trùng "code" (mã thành phần) sẽ bị ghi đè!';

$_['error_file_incomplete']                         = 'Cảnh báo: File không đầy đủ thông tin (thiếu cột)!';
$_['error_unknown']                                 = 'Lỗi ko xác định';
$_['error_filesize']                                = 'Cảnh báo: Kích thước tệp không được quá 1MB!';
$_['error_filetype']                                = 'Cảnh báo: Chỉ chấp nhận định dạng .xls và xlsx !';
$_['error_not_found_fields']                        = 'Cảnh báo: Không tìm thấy "tên thành phần" hoặc "mã thành phần"!';
$_['error_no_file_selected']                        = 'Chưa có file nào được chọn!';
$_['error_limit']                                   = 'Cảnh báo: Số thành phần import tối đa là 500!';

$_['text_uploaded']                                 = 'Thành công!';
$_['text_product_uploaded']                         = ' thành phần đã được cập nhật.';
$_['text_product_ingredient_uploaded']              = ' sản phẩm đã được cập nhật.';
$_['delete_success']                                = "Xóa thành phần thành công.";
