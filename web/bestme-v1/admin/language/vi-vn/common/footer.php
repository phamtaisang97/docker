<?php
// Text
$_['text_footer']  = '<a href="' . BESTME_SERVER . '">Bestme</a> &copy; 2018-' . date('Y') . ' All Rights Reserved.';
$_['text_version'] = 'Phiên bản %s';
$_['alert_title'] = "Thông báo";
$_['alert_close'] = "Đồng ý";
$_['you_still']   =     'Bạn còn';
$_['trial_date_lang']   =  'ngày dùng thử';
$_['choose_a_usage_package']  = "Chọn một gói sử dụng";
$_['you_are_out_of_trial']        =    'Bạn hết hạn dùng thử';
$_['confirm_box_title']          =    'Xác nhận';
$_['confirm_box_btn_yes']        =    'Đồng ý';
$_['confirm_box_btn_no']         =    'Hủy';

$_['txt_packet_wait_for_pay_in_time']           =    'Chúng tôi sẽ liên hệ với bạn để xác thực thanh toán gói dịch vụ trong tối đa là %date% ngày';
$_['txt_packet_wait_for_pay_out_time']          =    'Hết thời gian xác thực thanh toán. Vui lòng liên hệ hotline ' . PRODUCTION_BRAND_PHONE . ' để được hỗ trợ.';