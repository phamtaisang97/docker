<?php
// Heading
$_['heading_title']      = 'Cửa hàng';

// Text
$_['text_welcome']       = 'Xin chào';
$_['text_profile']       = 'Thông tin của bạn';
$_['text_store']         = 'Các cửa hàng';
$_['text_help']          = 'Trợ giúp';
$_['text_homepage']      = 'Trang chủ Cửa hàng';
$_['text_support']       = 'Forum hỗ trợ';
$_['text_documentation'] = 'Tài liệu';
$_['text_logout']        = 'Logout';
$_['change_password']        = 'Đổi mật khẩu';
$_['delete_confirm_common']           = 'Bạn có muốn xoá ?';
$_['copy_confirm_common']           = 'Bạn có muốn copy ?';
$_['view_store']              = 'Xem Website';
$_['language_vi']              = 'Tiếng Việt';
$_['language_en']              = 'Tiếng Anh';
$_['config_account']           = 'Cấu hình tài khoản';
$_['log_out']           = 'Đăng xuất';
$_['txt_package']       = 'Gói';
$_['trial']        =  'Dùng thử';
$_['basic']        =  'Cơ bản';
$_['optimal']      =  'Tối ưu';
$_['advance']      =  'Nâng cao';
$_['txt_use_sample_data'] = 'Sử dụng dữ liệu mẫu';
$_['txt_delete_sample_data'] = 'Xóa dữ liệu mẫu';
$_['txt_delete_sample_data_successfully'] = 'Xóa dữ liệu mẫu thành công';
$_['alert_title'] = 'Thông báo';

// Novaon common products
$_['txt_title_product_novaon_digital_marketing']            =  "DIGITAL MARKETING";
$_['txt_product_novaon_autoads_maxlead']                    =  "AutoAds MaxLead";
$_['txt_product_novaon_autoads_maxpush']                    =  "AutoAds MaxPush";
$_['txt_product_novaon_google_shopping']                    =  "Google Shopping";
$_['txt_product_novaon_cfp']                                =  "Chặn click ảo";
$_['txt_product_novaon_on_google']                          =  "Dịch vụ On-Google";
$_['txt_product_novaon_oncustomer']                             =  "OnCustomer";

$_['txt_title_product_novaon_ecom']                         =  "THƯƠNG MẠI ĐIỆN TỬ";
$_['txt_product_novaon_bestme_website']                     =  "" . PRODUCTION_BRAND_NAME . " Website";
$_['txt_product_novaon_bestme_pos']                         =  "" . PRODUCTION_BRAND_NAME . " POS";
$_['txt_product_novaon_bestme_platform']                    =  "" . PRODUCTION_BRAND_NAME . " Sàn";
$_['txt_product_novaon_novaonx_chatbot']                    =  "NovaonX Chatbot";
$_['txt_product_novaon_novaonx_social']                     =  "NovaonX Social";
$_['txt_product_novaon_onfluencer_review']                  =  "Onfluencer Review";
$_['txt_product_novaon_onfluencer_seller']                  =  "Onfluencer Seller";

$_['txt_title_product_novaon_digital_trans']                =  "CHUYỂN ĐỔI SỐ";
$_['txt_product_novaon_onsales_crm']                        =  "Onsales CRM";
$_['txt_product_novaon_onsales_caller']                     =  "Onsales Caller";
$_['txt_product_novaon_onfinance_invoice']                  =  "Onfinance Invoice";
$_['txt_product_novaon_onfinance_onsign']                   =  "Onfinance OnSign";
$_['txt_product_novaon_onpeople_hrm']                       =  "Onpeople HRM";

$_['txt_wait_for_payment_verification']                     =  "Chờ xác thực thanh toán";