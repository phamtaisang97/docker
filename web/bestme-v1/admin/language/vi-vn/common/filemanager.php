<?php
// Heading
$_['heading_title']    = 'Chọn ảnh';
$_['heading_title_image_manager']    = 'Quản lý hình ảnh';
$_['heading_title_image']            = 'Hình ảnh';

// Text
$_['text_uploaded']          = 'Thành công: Ảnh của bạn đã được tải lên!';
$_['text_delete_title']      = 'Xóa sản phẩm đã chọn';
$_['text_directory']         = 'Thành công: Thư mục đã được tạo!';
$_['text_delete']            = 'Thành công!';
$_['text_confirm_delete_images']            = 'Bạn có chắc chắn muốn xóa $numImage ảnh không?';

// Entry
$_['entry_search']     = 'Tìm kiếm ảnh';
$_['entry_folder']     = 'Tên thư mục';

// Error
$_['error_permission'] = 'Cảnh báo: Quyền bị từ chối!';
$_['error_filename']   = 'Cảnh báo: Tên tệp phải trong khoảng 3-255 ký tự!';
$_['error_folder']     = 'Cảnh báo: Tên thư mục phải trong khoảng 1-255 ký tự!';
$_['error_exists']     = 'Cảnh báo: tên tệp hoặc thư mục đã được sử dụng trước đó!';
$_['error_directory']  = 'cảnh báo: Thư mục không tồn tại!';
$_['error_filesize']   = 'Cảnh báo: Kích thước hình ảnh quá lớn!';
$_['error_filetype']   = 'Cảnh báo: loại tệp không chính xác (Hỗ trợ: .jpg, .jpeg, .png, .ico)!';
$_['error_upload']     = 'Cảnh báo: Không thể tải ảnh lên vì 1 lý do không xác định!';
$_['error_delete']     = 'Cảnh báo: Bạn không thể xóa thư mục này!';
$_['error_upload_default']     = 'Cảnh báo: Không thể tải ảnh lên vì 1 lý do không xác định! (Ảnh phải đúng định dạng : jpg, jpeg, png, ico. và kích thướng < ';
$_['error_limit_capacity']     = 'Thao tác không thực hiện được do bạn đã sử dụng hết hạn mức dung lượng của gói dịch vụ. Vui lòng liên hệ với chúng tôi để nâng cấp thêm.';
$_['warning_maximum_upload_images']    = 'Cảnh báo: Chỉ cho phép upload tối đa 7 ảnh 1 lúc';
$_['warning_maximum_upload_all_images']    = 'Cảnh báo: 1 tài khoản chỉ cho phép upload tối đa 1000 ảnh, bạn có thể xóa những ảnh ko sử dụng đi để upload thêm ảnh mới';


// new
$_['warning_for_num_of_images'] = 'Số hình ảnh tối đa cho 1 sản phẩm là {max_images}. Được phép chọn thêm: ';
$_['warning_for_single_select'] = 'Chỉ được chọn 1 ảnh!';
$_['no_selected_to_delete']     = 'Bạn chưa chọn ảnh để xóa!';
$_['no_image_selected']         = 'Chưa có ảnh nào được chọn!';

// btn
$_['button_parent']                 = 'Thư mục cha';
$_['button_folder']                 = 'Tạo thư mục';
$_['button_apply']                  = 'Sử dụng';
$_['ckeditor_original']                  = 'Không điều chỉnh';
$_['view_more']                       = 'Xem ảnh';
$_['ckeditor_imagesize']                       = 'Kích thước ảnh';

//////image manager /////////////
$_['image_filter']                  = 'Bộ lọc hình ảnh';
$_['image_filter_short']            = 'Bộ lọc';
$_['filter_list_follow']            = 'Lọc hình ảnh theo';
$_['filter_image_status']           = 'Trạng thái hình ảnh';
$_['filter_image_enable']           = 'Hiển thị';
$_['filter_image_disable']          = 'Ẩn';
$_['action_filter']                 = 'Lọc';
$_['image_search']                  = 'Tìm kiếm hình ảnh';
$_['choose_all_in_filter']                                 = "Đã chọn tất cả trong trang này.";
$_['choose_in_filter']                                     = "Đã chọn";
$_['item_in_filter']                                       = "mục";
$_['text_no_results']                                      = 'Không có dữ liệu!';
$_['collection_list_filter_action']                        = 'Chọn thao tác';
$_['choose_filter']                              = "Chọn điều kiện lọc...";
$_['filter_status']                              = "Trạng thái hiển thị";
$_['filter_status_placeholder']                  = "Chọn trạng thái";
$_['entry_status_publish']   = 'Hiển thị';
$_['entry_status_unpublish'] = 'Ẩn';
$_['txt_confirm_hide_images'] = 'Ẩn ảnh sẽ không thể tìm thấy tại : Thêm sản phẩm, bộ sưu tập, thiết lập giao diện ....';
$_['txt_confirm_delete_images'] = 'CẢNH BÁO: Xóa ảnh tại đây sẽ không thể khôi phục!';
$_['txt_confirm_show_images'] =   'Bạn có muốn hiển thị những ảnh đã chọn?';
$_['text_not_selected_yet'] = 'Chưa ảnh nào được chọn!';
$_['text_success_show'] = 'Hiển thị ảnh thành công!';
$_['text_success_hide'] = 'Ẩn ảnh thành công!';
$_['text_success_delete'] = 'Xóa ảnh thành công!';
$_['btn_add_image'] = 'Thêm hình ảnh';
$_['btn_hide_image'] = 'Ẩn';
$_['btn_show_image'] = 'Hiển thị';
$_['btn_delete_image'] = 'Xóa ảnh';

/* v2.10.1: folder */
$_['btn_add_folder'] = 'Thêm thư mục';
$_['btn_cancel'] = 'Hủy bỏ';
$_['btn_cancel_sort'] = 'Hủy';
$_['btn_dismiss'] = 'Bỏ qua';
$_['btn_create'] = 'Tạo';
$_['btn_agree'] = 'Đồng ý';
$_['btn_save'] = 'Lưu lại';
$_['txt_confirm'] = 'Xác nhận';
$_['txt_modal_add_folder_title'] = 'Thêm mới thư mục';
$_['txt_modal_edit_folder_title'] = 'Cập nhật thư mục';
$_['warning_missing_folder_name'] = 'Chưa nhập tên thư mục';
$_['txt_success_add_folder'] = 'Thêm thư mục thành công';
$_['txt_success_edit_folder'] = 'Cập nhật thư mục thành công';
$_['label_folder_name'] = 'Tên thư mục';
$_['label_edit_folder_name'] = 'Nhập tên thư mục';
$_['placeholder_edit_folder_name'] = 'Nhập tên thư mục tại đây';
$_['action_show_folder_image'] = 'Hiện thư mục/ hình ảnh';
$_['action_hide_folder_image'] = 'Ẩn thư mục/ hình ảnh';
$_['action_delete_folder_image'] = 'Xóa thư mục/ hình ảnh';
$_['action_move_folder_image'] = 'Di chuyển thư mục/ hình ảnh';
$_['txt_hide_folder_image'] = 'Thư mục/ hình ảnh bị ẩn sẽ không thể tìm thấy tại: Thêm sản phẩm, bộ sưu tập, thiết lập giao diện...';
$_['txt_show_folder_image'] = 'Bạn có muốn hiển thị những thư mục/ hình ảnh đã chọn không?';
$_['txt_delete_folder_image'] = 'CẢNH BÁO: Thư mục/ hình ảnh bị xóa sẽ không thể khôi phục';
$_['txt_success_hide'] = 'Ẩn thư mục/ hình ảnh thành công';
$_['txt_success_show'] = 'Hiển thị thư mục/ hình ảnh thành công';
$_['txt_success_delete'] = 'Xóa thư mục/ hình ảnh thành công';
$_['txt_success_move'] = 'Di chuyển thư mục/ hình ảnh thành công';
$_['txt_move_folder_image'] = 'Di chuyển thư mục/ hình ảnh';
$_['txt_choose_folder_move'] = 'Chọn thư mục muốn di chuyển đến';
$_['txt_search_folder_move'] = 'Tìm kiếm thư mục muốn di chuyển đến';
$_['txt_not_choose_move_folder'] = 'Chưa chọn thư mục muốn di chuyển đến';
$_['warning_exists_folder_name'] = 'Tên thư mục đã tồn tại';
$_['root_folder_name'] = '--Chưa phân thư mục--';
$_['txt_cannot_move_to_child'] = 'Bạn không thể di chuyển thư mục vào thư mục con của chính nó';
$_['txt_no_more_folders'] = 'Không còn thư mục nào';

$_['choose'] = "Đã chọn";
$_['item'] = "mục";
$_['text_failure'] = 'Thất bại';
$_['select2_notice_not_result'] = "Không tìm thấy kết quả phù hợp";
$_['select2_notice_search'] = "Tìm kiếm ...";
$_['select2_notice_load_more'] = "Đang tải ...";
$_['txt_folder'] = 'Thư mục';