<?php
// Heading
$_['heading_title'] = 'Tổng quan';

// Error
$_['error_install'] = 'Cảnh báo: Tồn tại thư mục cài đặt, cần xoá thư mục này vì vấn đề bảo mật!';

//cart_title
$_['text_order_title']           = 'Tổng đơn';
$_['text_customer_title']        = 'Tổng khách';
$_['text_product_title']         = 'Tổng sản phẩm';
$_['text_top_products']          = 'Top sản phẩm';
$_['text_business_report']       = 'Báo cáo kinh doanh';
$_['text_lastest_orders']        = 'Đơn hàng gần đây';
$_['text_view_statistics']       = 'Xem thống kê >>';
$_['text_view_more']             = 'Xem thêm';

//order column text
$_['column_id']                       = 'Mã đơn hàng';
$_['column_name']                     = 'Tên';
$_['column_status_dashboard']         = 'Trạng thái';
$_['column_date_added_dashboard']     = 'Ngày tạo đơn';
$_['column_total_dashboard']          = 'Tổng';
$_['column_view']                     = 'Xem';

//top product
$_['txt_exist']                          = 'Còn';
$_['txt_depot']                          = 'trong kho';
$_['txt_total_sold']                     = 'Tổng bán';
$_['no_statistical_data_available']      = 'Chưa có dữ liệu thống kê. Hãy bắt đầu bán hàng với ' . PRODUCTION_BRAND_NAME . ' Website và kiểm tra các thông số!';
$_['format_million']                     = 'triệu';
$_['format_billion']                     = 'tỷ';
$_['_trieu']                             = 'Triệu đ';

// Text chart
$_['options']                            = 'Tùy chọn';
$_['monday']                             = 'T2';
$_['tuesday']                            = 'T3';
$_['wednesday']                          = 'T4';
$_['thursday']                           = 'T5';
$_['friday']                             = 'T6';
$_['saturday']                           = 'T7';
$_['sunday']                             = 'CN';
$_['january']                            = 'Tháng 1';
$_['february']                           = 'Tháng 2';
$_['march']                              = 'Tháng 3';
$_['april']                              = 'Tháng 4';
$_['may']                                = 'Tháng 5';
$_['june']                               = 'Tháng 6';
$_['july']                               = 'Tháng 7';
$_['august']                             = 'Tháng 8';
$_['september']                          = 'Tháng 9';
$_['october']                            = 'Tháng 10';
$_['november']                           = 'Tháng 11';
$_['december']                           = 'Tháng 12';
$_['today']                              = 'Hôm nay';
$_['yesterday']                          = 'Hôm qua';
$_['one_week']                           = '1 tuần';
$_['this_week']                          = 'Tuần này';
$_['one_month']                          = '1 tháng';
$_['this_month']                         = 'Tháng này';
$_['decimal_point']                      = ',';
$_['thousand_point']                     = '.';
$_['last_week']                          = 'Tuần trước';
$_['last_month']                         = 'Tháng trước';

// text dashboard new
$_['txt_hello']                                 = 'Xin chào';
$_['txt_all_stores']                            = 'Tất cả cửa hàng';
$_['txt_all_staffs']                            = 'Tất cả nhân viên';
$_['txt_price']                                 = 'Giá';
$_['txt_revenue']                               = 'Doanh thu';
$_['txt_total_revenue']                         = 'Tổng doanh thu';
$_['txt_des_total_revenue']                     = 'Tổng giá trị các đơn đã được đặt trong khoảng thời gian đã chọn, bao gồm cả các đơn chưa thanh toán.';
$_['txt_general_index']                         = 'Chỉ số tổng quan';
$_['txt_total_order']                           = 'Tổng số đơn hàng';
$_['txt_des_total_order']                       = 'Tổng số đơn hàng được tạo trong khoảng thời gian báo cáo, bao gồm cả đơn đã hủy (không tính đơn nháp)';
$_['txt_total_visits']                          = 'Tổng lượt truy cập';
$_['txt_des_total_visits']                      = 'Tổng số lượt xem/truy cập vào website trong khoảng thời gian báo cáo. Một người xem nhiều trang, chỉ tính là 1 lượt truy cập.';
$_['txt_total_customer_by']                     = 'Tổng số khách mua hàng';
$_['txt_des_total_customer_by']                 = 'Tổng số khách phát sinh đơn hàng trong khoảng thời gian báo cáo.';
$_['txt_conversion_rate']                       = 'Tỉ lệ chuyển đổi';
$_['txt_des_conversion_rate']                   = 'Tỷ lệ chuyển đổi lượt truy cập thành khách mua hàng';

$_['txt_current_status']                        = 'Tình trạng hiện tại';
$_['txt_current_status_product']                = 'sản phẩm';
$_['txt_current_status_out_of_stock']           = 'Đã hết hàng';
$_['txt_current_status_order']                  = 'đơn hàng';
$_['txt_current_status_order_processing']       = 'Đang xử lý';
$_['txt_current_status_order_delivering']       = 'Đang vận chuyển';
$_['txt_view_detail']                           = 'Xem chi tiết';

$_['txt_product_of_the_highest']                = 'Top sản phẩm doanh thu cao nhất';
$_['txt_table_rank']                            = 'Thứ hạng';
$_['txt_table_product']                         = 'Sản phẩm';
$_['txt_table_revenue']                         = 'Doanh thu';
$_['txt_no_product']                            = 'Top sản phẩm không có dữ liệu';

$_['voucher_not_user']                    =       "Bạn có một voucher chưa sử dụng!";
$_['you_when_you_start_to_eat']           =       "Trước khi bắt đầu, chúng ta cần làm quen với nhau đã.";
$_['gift_when_success']                   =       "Sẽ có một phần quà dành cho bạn ngay khi hoàn tất";
$_['step']                                =       "bước";
$_['get_acquainted_below']                =       "làm quen dưới đây!";
$_['you_ready']                           =       "Bạn đã sẵn sàng kinh doanh!";
$_['only']                                =       "Chỉ còn";
$_['step_start_business']                 =       "bước để bắt đầu kinh doanh";
$_['add_new_product']                     =       "Thêm sản phẩm mới";
$_['customize_interface']                 =       "Tùy chỉnh giao diện";
$_['attack_domain_name']                  =       "Gắn tên miền";
$_['add_the_first_product_to_the_store']  =       "Thêm sản phẩm đầu tiên vào cửa hàng";
$_['fill_out_product_information']        =       "Điền đầy đủ thông tin sản phẩm giúp thỏa mãn nhu cầu tìm hiểu chi tiết của người mua sắm.";
$_['add_product']                         =       "Thêm sản phẩm";
$_['set_up_a_website_interface']          =       "Thiết lập giao diện website phù hợp với nhu cầu";
$_['you_can_add_logo']                    =       "Bạn có thể, thêm logo, chỉnh sửa màu sắc chủ đạo, ... cho website tùy theo nhu cầu của mình.";
$_['reinforce_brand_with_concise_domain_name']          =       "Củng cố thương hiệu với tên miền ngắn gọn";
$_['add_a_new_domain_name_to_make_it']                  =       "Thêm một tên miền mới để giúp khách hàng dễ dàng ghi nhớ thương hiệu của bạn hơn.";
$_['you_have_completed_the_Bestme_user_guide']          =       "Bạn đã hoàn thành hướng dẫn sử dụng " . PRODUCTION_BRAND_NAME . "!";
$_['bestme_for_use']                                    =       "" . PRODUCTION_BRAND_NAME . " tặng bạn thêm 02 tháng miễn phí khi mua gói sử dụng bất kỳ.";
$_['month_free_use']                                    =       "Nhận 02 tháng sử dụng miễn phí";
$_['getting_a_successful_gift']                         =       "Chúc mừng bạn đã nhận quà thành công!";
$_['please_contact']                                    =       "Vui lòng liên hệ " . PRODUCTION_BRAND_NAME . " theo hotline";
$_['provider_voucher_use']                              =       "và cung cấp mã sau để sử dụng:";
$_['thanks_use_bestme']                                 =       "Cảm ơn bạn đã lựa chọn sử dụng " . PRODUCTION_BRAND_NAME . "! Mong bạn tiếp tục đồng hành cùng " . PRODUCTION_BRAND_NAME . " trên con đường phát triển kinh doanh online!";
$_['continue_use']                                      =       "Tiếp tục sử dụng";
$_['gift_when_success_free']                            =  "Hoàn thành hướng dẫn để nhận thêm 02 tháng miễn phí sử dụng";
$_['getting_a_successful_gift_mobile']                  =  "Nhận quà thành công!";
$_['text_product_deleted']                               =  "Sản phẩm đã xóa";
$_['text_product_changed']                               =  "Sản phẩm đã thay đổi";
// onboarding gift
if (defined('SOURCE_PRODUCTION_FOR_ADG') && SOURCE_PRODUCTION_FOR_ADG) {
    $_['txt_onboarding_gitf']                               =  "Tặng bộ tài liệu kinh doanh";
} else {
    $_['txt_onboarding_gitf']                               =  "Tặng bộ tài liệu kinh doanh online";
}

$_['txt_sample_data'] = "Khi nâng cấp lên các gói trả phí, tất cả dữ liệu mẫu sẽ bị xóa";