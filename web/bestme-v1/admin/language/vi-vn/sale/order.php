<?php
// Heading
$_['heading_title']              = 'Đơn hàng';

// button
$_['btn_save_apply']               = 'Lưu và duyệt';
$_['btn_save_draft']               = 'Lưu đơn nháp';

// Text
$_['text_product']               = 'Sản phẩm';
$_['text_version']               = 'Phiên bản : ';
$_['text_unit_price']            = 'Đơn giá';
$_['text_quantity']              = 'Số lượng';
$_['text_amount']                = 'Thành tiền';
$_['text_total_amount']          = 'Tổng tiền hàng';
$_['text_shipping_fee']          = 'Phí vận chuyển';
$_['text_need_pay']              = 'Tổng cần thanh toán';
$_['text_payments']              = 'Hình thức thanh toán';
$_['text_cod_payment']           = 'Thanh toán khi nhận hàng';
$_['text_pre_payment']           = 'Thanh toán trước';
$_['text_success_payment']       = 'Đã thanh toán';
$_['text_success_payment_cod']       = 'Thanh toán sau (COD)';
$_['text_payment_status']        = 'Trạng thái thanh toán';
$_['text_order_info']            = 'Thông tin đơn hàng';
$_['text_order_create_date']     = 'Ngày tạo đơn';
$_['text_order_create_user']     = 'Người tạo';
$_['text_note']                  = 'Ghi chú';
$_['text_utm_tracking']                  = 'Tham số utm';
$_['text_note_placeholder']      = 'Viết ghi chú cho đơn hàng';
$_['text_tag']                   = 'Gắn tag';
$_['text_tag_placeholder']       = 'Nhập tag';
$_['text_order_copy']            = 'Nhân bản';
$_['text_order_delete']          = 'Xóa đơn';
$_['text_vnpay_payment']         = 'Thanh toán qua VNPAY';
$_['text_shopee_shopname']       = 'Tên gian hàng';

//text customer, address
$_['text_edit_address']          = 'Chỉnh sửa địa chỉ';
$_['text_create_customer']       = 'Tạo khách hàng mới';
$_['text_fullname']              = 'Họ tên';
$_['text_fullname_delivery']     = 'Tên người nhận';
$_['text_lastname']              = 'Họ';
$_['text_firstname']             = 'Tên';
$_['text_email']                 = 'Email';
$_['text_telephone']             = 'Số điện thoại';
$_['text_province']              = 'Tỉnh / Thành phố';
$_['text_district']              = 'Quận / Huyện';
$_['text_wards']                 = 'Phường / Xã';
$_['text_addr']                  = 'Địa chỉ';
$_['text_province_placeholder']  = 'Chọn';
$_['text_delivery_address']      = 'Địa chỉ giao hàng';


$_['text_success']               = 'Cập nhật thông tin đơn hàng thành công';
$_['text_success_add']           = 'Tạo mới đơn hàng thành công';
$_['text_list']                  = 'Danh sách đơn hàng';
$_['text_export']                = 'Xuất danh sách';
$_['text_add']                   = 'Tạo đơn hàng';
$_['text_edit']                  = 'Sửa đơn hàng';
$_['text_order_detail']          = 'Chi tiết đơn hàng';
$_['text_filter']                = 'Bộ lọc đơn hàng';
$_['text_filter_mobile']         = 'Lọc';
$_['text_all_orders']            = 'Tất cả đơn hàng';
$_['text_order_complete']        = 'Đơn hàng đã hoàn thành';
$_['text_cancel_order']          = 'Hủy đơn hàng';
$_['text_order_cancelled']       = 'Đơn hàng đã bị hủy';
$_['text_duplicate_order']       = 'Nhân bản đơn hàng';
$_['text_order_history']         = 'Lịch sử đơn hàng';
$_['text_order_create_now']      = 'Hiện tại';
$_['text_order_status']          = 'Trạng thái';
$_['text_save_confirm']          = 'Bạn có muốn lưu đơn hàng hiện tại không?';
$_['text_validate_product']      = 'Bạn phải chọn ít nhất 1 sản phẩm!';
$_['text_validate_shipping_info'] = 'Vui lòng nhập đầy đủ thông tin khách hàng!';
$_['text_validate_product_quantity']      = 'Số lượng tối thiểu cho một sản phẩm trong đơn hàng là 1';
$_['txt_receipt_voucher']        = 'Phiếu thu tạo tự động gắn với hóa đơn này là ';
//btn
$_['btn_order_update']           = 'Cập nhật đơn hàng';

$_['text_customer_detail']       = 'Khách hàng';
$_['text_customer_add']          = 'Thêm khách hàng mới';
$_['text_option']                = 'Options';
$_['text_store']                 = 'Store';
$_['text_date_added']            = 'Date Added';
$_['text_payment_method']        = 'Phương thức thanh toán';
$_['text_payment_method_choose'] = 'Chọn phương thức thanh thoán';
$_['text_shipping_method']       = 'Phương thức vận chuyển';
$_['text_customer']              = 'Khách hàng';
$_['text_customer_group']        = 'Nhóm khách hàng';
$_['text_fax']                   = 'Fax';
$_['text_invoice']               = 'Yêu cầu thanh toán';
$_['text_reward']                = 'Điểm thưởng';
$_['text_affiliate']             = 'Tiếp thị';
$_['text_order']                 = 'Đơn hàng (#%s)';
$_['text_payment_address']       = 'Địa chỉ thanh toán';
$_['text_shipping_address']      = 'Địa chỉ giao hàng';
$_['text_comment']               = 'Ghi chú Khách hàng';
$_['text_history']               = 'Lịch sử đơn hàng';
$_['text_history_add']           = 'Thêm lịch sử đơn hàng';
$_['text_account_custom_field']  = 'Tuỳ chỉnh Tài khoản';
$_['text_payment_custom_field']  = 'Tuỳ chỉnh địa chỉ thanh toán';
$_['text_shipping_custom_field'] = 'Tuỳ chỉnh địa chỉ giao hàng';
$_['text_browser']               = 'Trình duyệt';
$_['text_ip']                    = 'Địa chỉ IP';
$_['text_forwarded_ip']          = 'Chuyển tiếp IP';
$_['text_user_agent']            = 'User Agent';
$_['text_accept_language']       = 'Ngôn ngữ';
$_['text_order_id']              = 'ID Đơn hàng:';
$_['text_website']               = 'WebSite:';
$_['text_invoice_no']            = 'Số yêu cầu thanh toán.:';
$_['text_invoice_date']          = 'Ngày yêu cầu thanh toán:';
$_['text_sku']                   = 'SKU:';
$_['text_upc']                   = 'UPC:';
$_['text_ean']                   = 'EAN:';
$_['text_jan']                   = 'JAN:';
$_['text_isbn']                  = 'ISBN:';
$_['text_mpn']                   = 'MPN:';
// more...

// Column
// more...

// Entry
// more...
$_['entry_override']             = 'Ghi đè';
$_['entry_comment']              = 'Ghi chú';
$_['entry_currency']             = 'Tiền tệ';
$_['entry_shipping_method']      = 'Phương thức vận chuyển';
$_['entry_payment_method']       = 'Phương thức thanh toán';
$_['entry_coupon']               = 'Coupon';
$_['entry_voucher']              = 'Voucher';
$_['entry_reward']               = 'Thưởng';
$_['entry_order_id']             = 'ID Đơn hàng';
$_['entry_total']                = 'Tổng';
$_['entry_date_added']           = 'Ngày tạo';
$_['entry_date_modified']        = 'Ngày cập nhật';
$_['entry_product_placeholder']  = 'Tìm kiếm sản phẩm';
$_['entry_customer_placeholder'] = 'Tìm hoặc tạo khách hàng mới';

// Help
// more...

// Select 2
$_['select2_notice_not_result']   =    "Không tìm thấy kết quả phù hợp";
$_['select2_notice_search']       =    "Tìm kiếm ...";
$_['select2_notice_load_more']    =    "Đang tải ...";

// Error
$_['error_customer_warning']     = 'Đã có lôi xảy ra!';
$_['error_customer_info']        = 'Không tìm thấy khách hàng!';
$_['error_notfound_order']       = 'Không tìm thấy đơn hàng!';
$_['error_order_delete_s_6']     = 'Chỉ xóa được đơn hàng trạng thái nháp!';
$_['error_warning']              = 'Cảnh báo: Vui lòng kiểm tra kỹ form để biết lỗi!';
$_['error_permission']           = 'Cảnh báo: Bạn không có quyền chỉnh sửa Đơn hàng!';
$_['error_action']               = 'Cảnh bảo: Không thể hoàn thành thao tác này!';
$_['error_filetype']             = 'Loại tệp không đúng!';
$_['order_delete']               = 'Xoá';
$_['order_text_copy']            = 'Sao chép';
// product order
$_['error_not_add_product']      = 'ban chưa add product';
$_['error_not_add_product_min']  = 'Sản phẩm add nhiều hơn 0';
$_['error_not_add_product_max']  = 'Sản phẩm add nhiều sản phẩm hiện có';

$_['error_input_null']                 = 'Vui lòng điền vào trường này';
$_['error_type_mail']                  = 'Vui lòng nhập đúng định dạng email';
$_['error_max_length_255']             = 'Không vượt quá 255 ký tự';
$_['error_max_length_15']              = 'Không vượt quá 15 ký tự';
$_['error_type_phone']                 = 'Vui lòng nhập số cho trường này';
$_['error_max_length_10']              = 'Không vượt quá 10 ký tự';
$_['error_add_product_max']            = 'Không thêm quá 10 sản phẩm';
$_['error_max_length_1000']            = 'Không vượt quá 1000 ký tự';
$_['error_product_max_length']         = 'Số lượng sản phẩm thêm vào quá nhiều';
$_['error_input_only_number']          = 'Chỉ được nhập số cho trường này';
$_['error_max_length_30']              = 'Không vượt quá 30 ký tự';
$_['error_max_length_300']             = 'Không vượt quá 300 ký tự';
$_['error_a_z_0_9']                    = 'Cho phép nhập ký tự chữ hoa,thường, ký tự số,';
$_['error_shipping_address']           = 'Cho phép nhập ký tự số, chữ hoa, thường, ký tự đặc biệt là dấu gạch ngang -, dấu phẩy,';
$_['error_max_length_100']              = 'Không vượt quá 100 ký tự';

// List
$_['text_table_order_code']                        = "Mã đơn hàng";
$_['text_table_customer']                          = "Khách hàng";
$_['text_table_order_status']                      = "Trạng thái đơn hàng";
$_['text_table_order_payment_status']              = "Trạng thái thanh toán";
$_['text_table_order_total']                       = "Giá trị đơn";
$_['text_table_updated_time']                      = "Ngày cập nhật";

$_['order_status_delivering_wait']              = "Chờ vận chuyển";
$_['order_status_draft']                        = "Đơn nháp";
$_['order_status_processing']                   = "Đang xử lý";
$_['order_status_delivering']                   = "Đang vận chuyển";
$_['order_status_comback']                      = "Đang hoàn đơn";
$_['order_status_complete']                     = "Hoàn thành";
$_['order_status_canceled']                     = "Đã huỷ";
$_['order_payment_status_paid']                 = "Đã thanh toán";
$_['order_payment_status_not_paid']             = "Chưa thanh toán";
$_['order_payment_status_refunded']             = "Đã hoàn tiền";

// Filter
$_['filter_list_follow']                         = "Lọc đơn hàng theo:";
$_['choose_filter_1']                            = "Lọc theo";
$_['choose_filter_2']                            = "Chọn điều kiện lọc";
$_['filter_button']                              = "Lọc";

$_['filter_status']                              = "Trạng thái đơn hàng";
$_['filter_status_placeholder']                  = "Chọn Trạng thái";
$_['filter_status_draft']                        = "Nháp";
$_['filter_status_processing']                   = "Đang xử lý";
$_['filter_status_delivering']                   = "Đang vận chuyển";
$_['filter_status_complete']                     = "Hoàn thành";
$_['filter_status_canceled']                     = "Huỷ";
$_['filter_staff']                               = "Nhân viên";
$_['filter_staff_placeholder']                   = 'Chọn nhân viên';

$_['filter_payment_status']                      = "Trạng thái thanh toán";
$_['filter_payment_status_placeholder']          = "Chọn Trạng thái";
$_['filter_payment_status_paid']                 = "Đã thanh toán";
$_['filter_payment_status_not_paid']             = "Chưa thanh toán";
$_['filter_payment_status_return']               = "Đã hoàn tiền";
$_['filter_payment_error']                       = "Thanh toán qua VNPay lỗi";
$_['text_payment_code']                          = "Mã giao dịch(Transaction ID):";
$_['text_confirm_change_status_payment_when_error']                 = "Quá trình thanh toán chưa hoàn tất, bạn muốn tiếp tục thao tác này không?";

$_['filter_grant_total']                         = "Giá trị đơn";

$_['filter_tag']                                 = "Được tag với";
$_['filter_tag_placeholder']                     = "Nhập tag";

$_['filter_phone']                               = "Số điện thoại nhận hàng";
$_['filter_phone_placeholder']                   = "Nhập số điện thoại";

$_['filter_source']                              = "Nguồn hàng";
$_['filter_source_placeholder']                  = "Lựa chọn nguồn hàng";

$_['filter_discount']                            = "Chương trình khuyến mãi";
$_['filter_discount_placeholder']                = "Lựa chọn chương trình";

$_['filter_updated_time']                        = "Ngày cập nhật";
$_['filter_updated_time_placeholder']            = "Chọn khoảng thời gian";
$_['filter_updated_time_from']                   = "Kể từ ngày";
$_['filter_updated_time_from_back']              = "Từ ngày này về trước";

$_['filter_count']                               = "Số lượng";
$_['filter_count_placeholder']                   = "Chọn điều kiện lọc";
$_['filter_count_greater']                       = "Lớn hơn";
$_['filter_count_smaller']                       = "Nhỏ hơn";
$_['filter_count_equal']                         = "Bằng";
$_['filter_count_input_placeholder']             = "Nhập giá trị";
$_['filter_date_range']                          = "Khoảng thời gian";
$_['filter_select_date']                         = "Chọn ngày";
$_['filter_txt_date']                            = "Ngày";
$_['text_choose_action']                         = 'Chọn thao tác';

// Search
$_['search_placeholder']                         = "Tìm kiếm theo mã đơn";

$_['notice_add_tag_success']            = "Cập nhật thông tin đơn hàng thành công";
$_['notice_remove_tag_success']         = "Cập nhật thông tin đơn hàng thành công";
$_['notice_add_tag_error']              = "Bạn chưa thêm tag nào cho đơn hàng đã chọn";
$_['notice_remove_tag_error']           = "Bạn chưa chọn tag";
$_['notice_add_tag_error_max_length']   = "Tag không vượt quá 30 ký tự";
$_['tag_for_order_choose']              = "tag cho những đơn hàng đã chọn";
$_['list_tag_for_order']                = "những tag sau cho đơn hàng:";
$_['use_tag_description']               = 'Mỗi tag phân cách bằng dấu phẩy (,)';

// Action modal
$_['text_cancel_order_confirm']          = 'Bạn chắc chắn muốn hủy đơn hàng? 
                                            Sau khi hủy đơn, bạn sẽ không thể tiếp tục làm việc với đơn hàng này nữa. 
                                            Thao tác này cũng không thể được khôi phục.';
$_['text_cancel_order_note']             = 'Ghi chú lý do hủy đơn hàng';
$_['text_cancel_order_note_placeholder'] = 'Ghi chú...';
$_['text_cancel_order_return_product']   = 'Sản phẩm hoàn trả';
$_['text_cancel_order_return_total']     = 'Tổng tiền hoàn trả:';
$_['text_cancel_order_send_email']       = 'Gửi Email thông báo tới khách hàng';
$_['text_cancel_order_close']            = 'Đóng';
$_['text_cancel_order_submit']           = 'Huỷ';

$_['text_update_order_status']                        = 'Cập nhật trạng thái';
$_['text_update_order_close']                         = 'Đóng';
$_['text_update_order_submit']                        = 'Xác nhận';

$_['text_update_order_status_to_processing']          = 'Chuyển sang trạng thái Đang xử lý.';
$_['text_update_order_status_to_processing_confirm']  = 'Sau khi duyệt đơn hàng, bạn sẽ không thể xóa đơn hàng ra khỏi danh sách.
                                                         Bạn có chắc chắn đơn hàng đang được xử lý?';

$_['text_update_order_status_to_delivering']          = 'Chuyển sang trạng thái Đang vận chuyển.';
$_['text_update_order_status_to_delivering_confirm']  = 'Sau khi vận chuyển đơn hàng, mọi thông tin trên đơn hàng không thể chỉnh
                                                        sửa, ngoại trừ Ghi chú và Tags. Bạn có chắc chắn đơn hàng đã được chuyển
                                                        tới đối tác vận chuyển?';

$_['text_update_order_status_to_complete']           = 'Xác nhận đơn hàng đã hoàn thành';
$_['text_update_order_status_to_complete_confirm']   = 'Sau khi hoàn thành đơn hàng, mọi thông tin trên đơn hàng không thể chỉnh
                                                        sửa, ngoại trừ ghi chú và tags. Bạn có chắc chắn đơn hàng đã hoàn
                                                        thành?';

$_['text_update_order_status_to_cancelled_title']    = 'Huỷ đơn hàng';
$_['text_delete_confirm']          = 'Bạn có chắc chắn xóa đơn hàng? Thao tác này không thể khôi phục.';
$_['text_delete_order_success']    = 'Xóa đơn hàng thành công';

$_['text_update_order_tag_close']      = 'Đóng';
$_['text_update_order_tag_add']        = 'Thêm';
$_['text_update_order_tag_remove']     = 'Xoá';


$_['payment_status_success']     = 'Đã thanh toán';
$_['payment_status_fail']        = 'Chưa thanh toán';

$_['export_order_code']          = 'Mã đơn hàng';
$_['export_customer_name']       = 'Tên khách hàng';
$_['export_customer_phone']      = 'Số điện thoại';
$_['export_customer_email']      = 'Email';
$_['export_customer_address']    = 'Địa chỉ nhận hàng';
$_['export_customer_note']       = 'Ghi chú';
$_['export_product_name']        = 'Tên sản phẩm';
$_['export_product_image']       = 'Ảnh sản phẩm';
$_['export_product_price']       = 'Giá sản phẩm';
$_['export_product_amount']      = 'Số lượng';
$_['export_order_status']        = 'Trạng thái đơn hàng';
$_['export_order_total']         = 'Giá trị đơn hàng';
$_['export_payment_status']      = 'Trạng thái thanh toán';
$_['export_payment_note']        = 'Hình thức thanh toán';
$_['export_delivery']            = 'Phương thức vận chuyển';
$_['export_delivery_fee']        = 'Phí vận chuyển';
$_['export_delivery_free']       = 'Miễn phí vận chuyển';
$_['export_order_update']        = 'Ngày cập nhật';
$_['export_payment_code']          = 'Mã giao dịch';
$_['txt_vnppay']          = 'VNPAY';

// validate form - update status
$_['error_update_order_status_order_id']            = 'Lỗi cập nhật trạng thái đơn hàng [Thiếu id đơn hàng]';
$_['error_update_order_status_order_status_id']     = 'Lỗi cập nhật trạng thái đơn hàng [Thiếu id trạng thái]';
$_['error_cancel_order_order_id']                   = 'Lỗi huỷ đơn hàng [Thiếu id đơn hàng]';
$_['error_cancel_order_send_email']                 = 'Lỗi huỷ đơn hàng [Thiếu tuỳ chọn gửi email]';
$_['error_cancel_order_comment']                    = 'Lỗi huỷ đơn hàng [Ghi chú không được dài quá %d ký tự]';
$_['error_email_exist']                             = 'Email đã được sử dụng, vui lòng dùng email khác!';
$_['error_phone_exist']                             = 'Số điện thoại đã được sử dụng, vui lòng dùng số điện thoại khác!';

$_['success_order_customer_info']         = 'Cập nhật thông tin đơn hàng thành công';
$_['error_order_customer_info']           = 'Thay đổi thông tin thất bại';
$_['error_total_amount']                  = 'Bạn phải chọn sản phẩm';
$_['error_customer_phone']                = 'Thiếu số điện thoại khách hàng.';
$_['error_customer_phone_validate']       = 'Số điện thoại không hợp lệ.';
$_['error_customer_firstname']            = 'Thiếu Tên khách hàng.';
$_['error_customer_lastname']             = 'Thiếu Họ khách hàng.';
$_['error_customer_full_name']            = 'Họ và tên không được để trống';
$_['error_customer_firstname_validate']   = 'Tên khách hàng chỉ được nhập chữ và số.';
$_['error_customer_lastname_validate']    = 'Họ khách hàng chỉ được nhập chữ và số.';
$_['error_customer_fullname_validate']    = 'Họ tên khách hàng chỉ được nhập chữ và số.';
$_['error_customer_email']                = 'Thiếu Email khách hàng.';
$_['error_customer_email_validate']       = 'Email không đúng định dạng.';
$_['error_customer_province']             = 'Chọn tỉnh.';
$_['error_customer_district']             = 'Chọn quận/huyện.';
$_['error_customer_wards']                = 'Chọn xã/phường';
$_['error_customer_address']              = 'Điền địa chỉ';
$_['error_delivery_phone']                = 'Thiếu số điện thoại giao hàng.';
$_['error_other_delivery_method_name']    = 'Thiếu tên phương thức.';

$_['order_history_create']                  = "Đơn hàng được tạo bởi";
$_['order_history_update']                  = "Cập nhật đơn hàng bởi";
$_['order_history_change_status']           = "Cập nhật sang trạng thái";

//invoice
$_['invoice_date_add_order']        = 'Ngày đặt hàng';
$_['invoice_order_code']        = 'Mã đơn hàng';
$_['invoice_order_store_url']        = 'Website';
$_['invoice_order_store_email']        = 'Email';
$_['invoice_order_customer_info']        = 'Thông tin khách hàng';
$_['invoice_order_customer_address']        = 'Địa chỉ nhận hàng';
$_['invoice_sku']        = 'SKU';
$_['invoice_order_total']        = 'Tổng tiền hàng';
$_['invoice_order_shipping_fee']        = 'Phí vận chuyển';
$_['invoice_order_real_total']        = 'Thành tiền';

//order list
$_['update']                             = "Cập nhật";
$_['input_comment']                      = "Nhập ghi chú";
$_['print_order']                        = "In đơn hàng";
$_['text_save']                          = "Lưu";
$_['order_fix_text']                     = 'Chỉnh sửa';
$_['order_by']                     = 'bởi';

$_['delete_order_text']                  = 'Xóa đơn hàng';
$_['order_status_delete']                = 'Đã hủy';

$_['success']                = 'thành công';
$_['failure']                = 'thất bại';
$_['not_copy']               = 'không thể copy vì số lượng sản phẩm trong kho quá ít';
$_['warning_shipping']                       = 'Thiếu thông tin giao hàng, vui lòng điền đủ thông tin giao hàng.';
$_['lang_out_of_stock']                      = 'Sản phẩm vừa hết hàng hoặc thay đổi, vui lòng chọn lại';
$_['lang_add_tag']                           = "Thêm tag";
$_['txt_add']                               = "Thêm ";
$_['txt_enter_new_tag']                      = "Nhập tag mới";
$_['lang_remove_tag']                        = "Bỏ tag";
$_['remove_tag']                             = "Bỏ";
$_['currency']                               = "VND";
$_['lang_export_error']                              = "Xuất file lỗi. Vui lòng thử lại hoặc liên hệ Bestme để được trợ giúp.";
$_['lang_system_error_001']                          = "Lỗi hệ thống [Không lấy được thông tin sản phẩm trong đơn hàng] [#001]. Vui lòng thử lại sau!";
$_['lang_system_error_002']                          = "Lỗi hệ thống [Không lấy được thông tin sản phẩm trong đơn hàng] [#002]. Vui lòng thử lại sau!";
$_['lang_system_error']                              = "Lỗi hệ thống [Không lấy được thông tin sản phẩm trong đơn hàng]. Vui lòng thử lại sau!";
$_['lang_text_draft']            = 'Nháp';
$_['lang_text_processing']  = 'Đang xử lý';
$_['lang_text_delivering']  = 'Đang giao hàng';
$_['lang_text_complete']    = 'Hoàn thành';
$_['lang_text_canceled']    = 'Đã hủy';

$_['transport_delivery']   = 'Giao hàng';
$_['transport_address_to_collect']   = 'Địa chỉ lấy hàng';
$_['transport_service']   = 'Dịch vụ';
$_['transport_bill_of_lading']   = 'Mã vận đơn';
$_['transport_cod']   = 'Số tiền thu hộ';
$_['transport_status_delivery']   = 'Trạng thái giao vận';
$_['transport_cancel_delivery']   = 'Hủy giao hàng';
$_['transport_weight']   = 'Tổng khối lượng(gr)';
$_['transport_shipping_unit']   = 'Đơn vị vận chuyển';
$_['transport_benefit_package']   = 'Gói dịch vụ';
$_['transport_cod_cod']   = 'Số tiền thu hộ (COD)';
$_['transport_delivery_preview']   = 'Cho phép xem hàng trước khi nhận';
$_['transport_length']   = 'Chiều dài gói hàng (cm)';
$_['transport_length_mobile']   = 'Dài (cm)';
$_['transport_width']   = 'Chiều rộng gói hàng (cm)';
$_['transport_width_mobile']   = 'Rộng (cm)';
$_['transport_height']   = 'Chiều cao gói hàng (cm)';
$_['transport_height_mobile']   = 'Cao (cm)';
$_['transport_note']   = 'Ghi chú';
$_['transport_bill_custom']   = 'Mã vận đơn(tùy chọn)';
$_['transport_cancel']   = 'Bỏ qua';
$_['transport_apply']    = 'Áp dụng';
$_['transport_cancel_delivery']   = 'Hủy giao hàng';
$_['transport_you_are_cancel_delivery']   = 'Bạn có chắc muốn hủy giao hàng cho đơn này?';

$_['vtp_transport_error_address'] = "Bạn cần nhập đủ thông tin Phường/xã, Quận/huyện, Thành phố/tỉnh";
$_['vtp_transport_error'] = "Lỗi không tạo được đơn hàng từ ViettelPost";

$_['ghn'] = "Giao hàng nhanh";
$_['ghn_ReadyToPick'] = 'Đơn đặt hàng vừa được tạo';
$_['ghn_Picking'] = 'Shipper đang đến lấy hàng';
$_['ghn_Storing'] = 'Hàng đã được chuyển đến trung tâm phân loại GHN';
$_['ghn_Delivering'] = 'Shipper đang giao hàng cho khách hàng';
$_['ghn_Delivered'] = 'Hàng đã được giao cho khách hàng';
$_['ghn_WaitingToFinish'] = 'Đơn đặt hàng đang chờ để chuyển COD';
$_['ghn_Finish'] = 'Đặt hàng đã được hoàn thành';
$_['ghn_Return'] = 'Hàng đang chờ để trả lại cho người bán / người bán sau 3 lần giao hàng không thành công';
$_['ghn_Returned'] = 'Hàng đã được trả lại cho người bán / người bán';
$_['ghn_LostOrder'] = 'Hàng đã bị mất';
$_['ghn_Cancel'] = 'Đã bị hủy';

$_['ghn_ready_to_pick'] = 'Đơn đặt hàng vừa được tạo';
$_['ghn_picking'] = 'Shipper đang đến lấy hàng';
$_['ghn_storing'] = 'Hàng đã được chuyển đến trung tâm phân loại GHN';
$_['ghn_delivering'] = 'Shipper đang giao hàng cho khách hàng';
$_['ghn_delivered'] = 'Hàng đã được giao cho khách hàng';
$_['ghn_return'] = 'Hàng đang chờ để trả lại cho người bán / người bán sau 3 lần giao hàng không thành công';
$_['ghn_returned'] = 'Hàng đã được trả lại cho người bán / người bán';
$_['ghn_lost'] = 'Hàng đã bị mất';
$_['ghn_cancel'] = 'Đã bị hủy';

$_['ghn_money_collect_picking'] = 'Shipper đang tương tác với người bán';
$_['ghn_picked'] = 'Shipper đã lấy hàng';
$_['ghn_money_collect_delivering'] = 'Shipper đang tương tác với người mua';
$_['ghn_delivery_fail'] = 'Hàng chưa được giao cho khách hàng';
$_['ghn_waiting_to_return'] = 'Hàng đang chờ để trả lại cho người bán (có thể trả lại trong vòng 24 / 48h)';
$_['ghn_return_transporting'] = 'Hàng đang được luân chuyển';
$_['ghn_return_sorting'] = 'Hàng đang được phân loại (tại kho)';
$_['ghn_returning'] = 'Shipper đang trả lại hàng cho người bán';
$_['ghn_exception'] = 'Ngoại lệ trong quá trình giao hàng';
$_['ghn_damage'] = 'Hàng hóa bị hư hỏng';

$_['text_source']  =  "Nguồn";
$_['admin']   = "Quản trị";
$_['shop']   = "Cửa hàng";
$_['chatbot']   = "Chatbot";

$_['approved']   = "Chờ lấy hàng";
$_['delivery_successful']   = "Giao thành công";
$_['destroyed_spot']   = "Tiêu hủy tại chỗ";
$_['refund_successful']   = "Hoàn trả thành công";
$_['cancel_ballot']   = "Hủy phiếu gửi";
$_['cancel_bill']   = "Hủy vận đơn";
$_['selected_all']   = "Chọn tất cả";
$_['viettel_post_approved']   = "Chờ lấy hàng";
$_['viettel_post_delivery_successful']   = "Giao thành công";
$_['viettel_post_destroyed_spot']   = "Tiêu hủy tại chỗ";
$_['viettel_post_refund_successful']   = "Hoàn trả thành công";
$_['viettel_post_cancel_ballot']   = "Hủy phiếu gửi";
$_['viettel_post_cancel_bill']   = "Hủy vận đơn";
$_['btn_save_order_mobile']   = "Lưu";
$_['btn_back_to_order_list_mobile']   = "Quay lại";
$_['btn_update_order_detail_mobile']   = "Cập nhật";
$_['remove_all_tag'] = 'Xóa tất cả các tag';
$_['order_product_data_not_empty']       =  'Trong đơn hàng phải tồn tại sản phẩm!';
$_['product_not_empty']       =  'Sản phẩm không còn tồn tại, vui lòng tạo lại đơn!';
$_['warning_product_changed']       =  'Sản phẩm đã bị thay đổi, vui lòng tạo lại đơn!';
$_['error_dont_have_product_or_removed']       =  'Sản phẩm vừa hết hàng hoặc bị xóa!';
// discount
$_['txt_discount_code']   = "Mã CTKM";
$_['txt_discount_name']   = "Tên CTKM";
$_['txt_discount_detail'] = "Chi tiết";
$_['txt_discount']                            = "Khuyến mãi";
$_['text_discount_col']                       = "Chiết khấu";
$_['txt_heading_detail']                      = 'Điều kiện khuyến mãi';
$_['txt_for_oder_from']                       = 'cho đơn hàng từ';
$_['txt_for_oder_to']                         = 'đến';
$_['txt_for_all_products']                    = 'tất cả sản phẩm';
$_['txt_for_per_product_if_oder_from']        = '/sản phẩm nếu đơn hàng mua từ';
$_['txt_for_products_of_all_categories']      = 'sản phẩm thuộc tất cả loại sản phẩm';
$_['txt_for_products_of_category']            = 'sản phẩm thuộc loại';
$_['txt_for_products_of_all_manufacturers']   = 'sản phẩm thuộc tất cả nhà cung cấp';
$_['txt_for_products_of_manufacturer']        = 'sản phẩm thuộc nhà cung cấp';
$_['txt_view_detail']                         = 'Xem chi tiết';
$_['txt_hide_detail']                         = 'Ẩn chi tiết';

// order source status
$_['txt_order_source_shop']                   = 'Cửa hàng';
$_['txt_order_source_admin']                  = 'Quản trị';
$_['txt_order_source_pos']                    = 'POS';
$_['txt_order_source_chatbot']                = 'Chat bot';
$_['txt_order_source_social']                 = 'Social';
$_['txt_order_source_lazada']                 = 'Lazada';
$_['txt_order_source_shopee']                 = 'Shopee';
$_['txt_order_source_tiki']                   = 'Tiki';

// v2.6.1
$_['text_update_order_status_of_shopee']           = 'Cập nhật trạng thái đơn hàng thủ công sẽ khiến hệ thống không thể tự động đồng bộ trạng thái đơn. Bạn muốn tiếp tục không?';

// v2.7.3
$_['txt_full_name']                         = 'Họ và tên ';
$_['txt_classify']                          = 'Phân loại ';

//v2.8.2
$_['text_message_order_status']             = "Cập nhật trang thái đơn hàng thủ công sẽ khiến hệ thống không thể tự động đồng bộ trạng thái giao vận và trạng thái đơn . Bạn muốn tiếp tục không ?";

// v3.4.3
$_['txt_print_bill']                                = 'In vận đơn';
$_['txt_transport_name']                            = 'ĐVVC';
$_['txt_date_order']                                = 'Ngày đặt đơn';
$_['txt_delivery_note']                             = 'Phiếu giao hàng';
$_['txt_order_code']                                = 'Mã đơn';
$_['txt_sender']                                    = 'Người gửi:';
$_['txt_receiver']                                  = 'Người nhận:';
$_['txt_collection_amount']                         = 'Thu hộ:';

//
$_['txt_table_price_product']                       = 'Đơn giá sản phẩm';
$_['txt_table_quantity_new']                        = 'SL';
$_['txt_bill_of_sale']                              = 'Hóa đơn bán hàng';
$_['txt_phone_number_new']                          = 'SĐT: ';
$_['txt_note_final_order']                          = '*Một số sản phẩm có thể bị ẩn do danh sách quá dài';

$_['txt_discount_by_voucher_code']                          = 'Giảm qua voucher';