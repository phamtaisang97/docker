<?php
// Heading
$_['heading_title']     = 'Đơn hàng hoàn trả';
$_['heading_title_top'] = 'Danh sách phiếu trả hàng';

// button
$_['btn_save_apply'] = 'Trả hàng';
$_['btn_cancel']     = 'Hủy bỏ';
$_['btn_go_back']    = 'Quay lại';
$_['btn_action']     = 'Thao tác';

// Text
$_['text_receipt_code']                = 'Mã phiếu';
$_['text_detail']                      = 'Phiếu trả hàng';
$_['text_create_receipt']              = 'Phiếu trả hàng';
$_['text_product']                     = 'Sản phẩm';
$_['text_version']                     = 'Phiên bản : ';
$_['text_price_return']                = 'Giá trả hàng';
$_['text_quantity']                    = 'Số lượng';
$_['text_quantity_return']             = 'Số lượng trả';
$_['text_amount']                      = 'Thành tiền';
$_['text_total_product_return_amount'] = 'Tổng tiền trả hàng';
$_['text_fee_amount']                  = 'Phí trả hàng';
$_['text_total_amount']                = 'Tổng cần thanh toán';
$_['text_order_info']                  = 'Thông tin đơn hàng';
$_['text_order_create_date']           = 'Ngày tạo đơn';
$_['text_order_create_user']           = 'Người tạo';
$_['text_order_code']                  = 'Mã đơn hàng';
$_['text_order_amount']                = 'Giá trị đơn';
$_['text_return_amount']               = 'Giá trị trả lại';
$_['text_source']                      = 'Nguồn';
$_['text_return_note']                 = 'Ghi chú trả hàng';
$_['text_customer']                    = 'Khách hàng';
$_['text_customer_name']               = 'Tên khách hàng ';
$_['text_customer_phone']              = 'Số điện thoại';
$_['text_no_results']                  = 'Không có dữ liệu!';
$_['text_return_reason']               = 'Lý do trả hàng';
$_['text_return_date']                 = 'Ngày trả hàng';
$_['text_choose_store']                = "Chọn cửa hàng trả";

// filter
$_['filter_return_receipt']             = 'Bộ lọc phiếu trả hàng';
$_['filter_by']                         = 'Lọc phiếu theo';
$_['filter_condition']                  = 'Chọn điều kiện lọc';
$_['filter']                            = 'Lọc';
$_['filter_by_receipt_code']            = 'Tìm kiếm theo mã phiếu';

// popup order list
$_['p_order_list']                      = 'Danh sách đơn hàng';
$_['p_search_placeholder']              = 'Tìm kiếm';
$_['p_filter_source']                   = 'Chọn nguồn đơn';
$_['p_filter_source_shop']              = 'Cửa hàng';
$_['p_filter_source_admin']             = 'Quản trị';

//placeholder
$_['placeholder_return_note'] = 'Nhập ghi chú trả hàng';

//Error
$_['error_return_quantity_more_than_product_quantity'] = 'Số lượng trả không được lớn hơn số lượng sản phẩm trong đơn hàng';
$_['error_return_quantity_empty']                      = 'Số lượng trả phải lớn hơn 0';
$_['error_return_fee']                                 = 'Phí trả hàng không thể lớn hơn tổng tiền trả hàng';
$_['error_note']                                       = 'Ghi chú không được vượt quá 255 ký tự';
$_['error_order_not_found']                            = 'Đơn hàng không tồn tại';
$_['error_all_product_is_returned']                    = 'Sản phẩm trong đơn hàng đã trả hết';
$_['error_return_receipt_not_found']                   = 'Phiếu trả hàng không tồn tại';

//source
$_['shop'] = 'Cửa hàng';
$_['pos'] = 'Pos';
$_['admin'] = 'Quản trị';
$_['chatbot'] = 'Chatbot';
$_['social'] = 'Social';

// Text chart
$_['options']                            = 'Tùy chọn';
$_['monday']                             = 'T2';
$_['tuesday']                            = 'T3';
$_['wednesday']                          = 'T4';
$_['thursday']                           = 'T5';
$_['friday']                             = 'T6';
$_['saturday']                           = 'T7';
$_['sunday']                             = 'CN';
$_['january']                            = 'Tháng 1';
$_['february']                           = 'Tháng 2';
$_['march']                              = 'Tháng 3';
$_['april']                              = 'Tháng 4';
$_['may']                                = 'Tháng 5';
$_['june']                               = 'Tháng 6';
$_['july']                               = 'Tháng 7';
$_['august']                             = 'Tháng 8';
$_['september']                          = 'Tháng 9';
$_['october']                            = 'Tháng 10';
$_['november']                           = 'Tháng 11';
$_['december']                           = 'Tháng 12';
$_['today']                              = 'Hôm nay';
$_['yesterday']                          = 'Hôm qua';
$_['one_week']                           = '1 tuần';
$_['this_week']                          = 'Tuần này';
$_['one_month']                          = '1 tháng';
$_['this_month']                         = 'Tháng này';
$_['decimal_point']                      = ',';
$_['thousand_point']                     = '.';
$_['last_week']                          = 'Tuần trước';
$_['last_month']                         = 'Tháng trước';

//
$_['text_success_add']                         = 'Trả hàng thành công!';