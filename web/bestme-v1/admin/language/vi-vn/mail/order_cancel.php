<?php
// Text
$_['text_subject']               = '[' . PRODUCTION_BRAND_NAME . '] Thông báo huỷ đơn hàng từ %s';
$_['text_greeting']              = 'Xin chào,' . "\n" .'Bạn đã huỷ đơn hàng có mã #%s. Thông tin chi tiết:';

$_['text_order_code']            = 'Mã đơn hàng     : #%s';
$_['text_order_products']        = 'Sản phẩm        : ' . "\n" .'%s';
$_['text_order_product']         = '  - %s, số lượng: %s, đơn giá: %s₫, thành tiền: %s₫'; // name - quantity - price - total
$_['text_order_total']           = 'Giá trị đơn hàng: %s₫';

$_['text_wish']                  = PRODUCTION_BRAND_NAME  . ' cảm ơn quý khách và hẹn gặp lại!';
$_['text_end']                   = 'Trân trọng! ' . "\n" . 'Đội ngũ Bestme.';