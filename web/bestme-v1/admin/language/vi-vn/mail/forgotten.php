<?php
// Text
$_['text_subject']  = 'Chào bạn, đây là Yêu cầu đặt lại mật khẩu từ %s';
$_['text_greeting'] = 'Mật khẩu mới đã được gửi cho quản trị viên gian hàng %s.';
$_['text_change']   = 'Để kích hoạt mật khẩu mới, vui lòng bấm vào liên kết dưới đây: ';
$_['text_ip']       = 'Địa chỉ IP đã gửi yêu cầu đặt lại mật khẩu là: ';

// Temp (sent password without require click to link)
$_['text_new_password']       = 'Vui lòng sử dụng mật khẩu mới sau đây để đăng nhập gian hàng của bạn: ';
$_['text_end']                = 'Trân trọng! ' . "\n" . 'Đội ngũ Bestme.';