<?php
// Text
$_['text_subject'] = '%s - Tài khoản của bạn đã được kích hoạt!';
$_['text_welcome'] = 'Chào mừng và cảm ơn bạn đã đăng ký tại %s!';
$_['text_login']   = 'Tài khoản của bạn hiện đã được tạo và bạn có thể đăng nhập bằng địa chỉ email và mật khẩu của mình bằng cách truy cập trang web của chúng tôi hoặc tại URL sau:';
$_['text_service'] = 'Khi đăng nhập, bạn sẽ có thể truy cập các dịch vụ khác bao gồm xem xét các đơn đặt hàng trước đây, in hóa đơn và chỉnh sửa thông tin tài khoản của bạn.';
$_['text_thanks']  = 'Cảm ơn,';