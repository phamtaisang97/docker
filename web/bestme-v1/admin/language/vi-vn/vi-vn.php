<?php
// Locale
$_['code']                          = 'vi';
$_['direction']                     = 'ltr';
$_['date_format_short']             = 'd/m/Y';
$_['date_format_long']              = 'l dS F Y';
$_['time_format']                   = 'h:i:s A';
$_['datetime_format']               = 'd/m/Y H:i:s';
$_['decimal_point']                 = '.';
$_['thousand_point']                = ',';

// Text
$_['text_yes']                      = 'Có';
$_['text_no']                       = 'Không';
$_['text_enabled']                  = 'Hiển thị';
$_['text_disabled']                 = 'Ẩn';
$_['text_none']                     = ' --- Chưa chọn --- ';
$_['text_select']                   = ' --- Vui lòng chọn --- ';
$_['text_select_all']               = 'Chọn tất cả';
$_['text_all']                      = 'Tất cả';
$_['text_unselect_all']             = 'Bỏ chọn tất cả';
$_['text_all_zones']                = 'Tất cả Zones';
$_['text_default']                  = ' <b>(Mặc định)</b>';
$_['text_close']                    = 'Đóng';
$_['text_pagination']               = 'Hiển thị %d - %d trên tổng số %d (%d trang)';
$_['text_other']                    = 'Khác';
$_['text_go_to_page']               = "Đi đến trang";
$_['text_loading']                  = 'Đang tải...';
$_['text_no_results']               = 'Không có dữ liệu!';
$_['text_confirm']                  = 'Bạn có chắc chắn?';
$_['text_home']                     = 'Trang chủ';

// Button
$_['button_add']                    = 'Thêm mới';
$_['button_delete']                 = 'Xóa';
$_['button_delete_all']             = 'Xóa tất cả';
$_['button_save']                   = 'Lưu';
$_['button_save_and_exit']          = 'Lưu và thoát';
$_['button_exit']                   = 'Thoát';
$_['button_update']                 = 'Cập nhật';
$_['button_cancel']                 = 'Hủy';
$_['button_cancel_recurring']       = 'Cancel Recurring Payments';
$_['button_continue']               = 'Tiếp tục';
$_['button_clear']                  = 'Xoá';
$_['button_close']                  = 'Đóng';
$_['button_enable']                 = 'Cho phép';
$_['button_disable']                = 'Không cho phép';
$_['button_filter']                 = 'Lọc';
$_['button_send']                   = 'Gửi';
$_['button_edit']                   = 'Sửa';
$_['button_copy']                   = 'Sao chép';
$_['button_back']                   = 'Quay lại';
$_['button_remove']                 = 'Xoá';
$_['button_refresh']                = 'Làm mới';
$_['button_export']                 = 'Xuất file';
$_['button_import']                 = 'Nhập file';
$_['button_download']               = 'Tải về';
$_['button_rebuild']                = 'Xây dựng lại';
$_['button_upload']                 = 'Tải lên';
$_['button_submit']                 = 'Đồng ý';
$_['button_invoice_print']          = 'In thanh toán';
$_['button_shipping_print']         = 'In danh sách vận chuyển';
$_['button_address_add']            = 'Thêm Địa chỉ';
$_['button_attribute_add']          = 'Thêm thuộc tính';
$_['button_banner_add']             = 'Thêm Banner';
$_['button_custom_field_value_add'] = 'Thêm Trường';
$_['button_product_add']            = 'Thêm Sản phẩm';
$_['button_filter_add']             = 'Thêm Bộ lọc';
$_['button_option_add']             = 'Thêm Thuộc tính';
$_['button_option_value_add']       = 'Thêm Giá trị thuộc tính';
$_['button_recurring_add']          = 'Thêm Định kỳ';
$_['button_discount_add']           = 'Thêm Giảm giá';
$_['button_special_add']            = 'Thêm Giảm giá đặc biệt';
$_['button_image_add']              = 'Thêm Ảnh';
$_['button_geo_zone_add']           = 'Thêm Vùng địa lý';
$_['button_history_add']            = 'Thêm Lịch sử';
$_['button_translation']            = 'Load Phiên dịch mặc định';
$_['button_translation_add']        = 'Thêm Phiên dịch';
$_['button_transaction_add']        = 'Thêm Phiên làm việc';
$_['button_route_add']              = 'Thêm Điều hướng';
$_['button_rule_add']               = 'Thêm Quy tắc';
$_['button_module_add']             = 'Thêm Mô đun';
$_['button_link_add']               = 'Thêm Liên kết';
$_['button_approve']                = 'Chấp nhận';
$_['button_deny']                   = 'Từ chối';
$_['button_reset']                  = 'Đặt lại';
$_['button_generate']               = 'Tạo';
$_['button_voucher_add']            = 'Thêm Voucher';
$_['button_reward_add']             = 'Thêm Điểm thưởng';
$_['button_reward_remove']          = 'Xoá Reward Points';
$_['button_commission_add']         = 'Thêm Uỷ quyền';
$_['button_commission_remove']      = 'Xoá Uỷ quyền';
$_['button_credit_add']             = 'Thêm Tín dụng';
$_['button_credit_remove']          = 'Xoá Tín dụng';
$_['button_ip_add']                 = 'Thêm IP';
$_['button_parent']                 = 'Mục cha';
$_['button_folder']                 = 'Thư mục mới';
$_['button_search']                 = 'Tìm kiếm';
$_['button_view']                   = 'Xem';
$_['button_install']                = 'Cài đặt';
$_['button_uninstall']              = 'Gỡ cài đặt';
$_['button_link']                   = 'Liên kết';
$_['button_currency']               = 'Làm mới giá trị tiền tệ';
$_['button_apply']                  = 'Áp dụng';
$_['button_category_add']           = 'Thêm Danh mục';
$_['button_order']                  = 'Xem đơn';
$_['button_order_recurring']        = 'Xem đơn định kỳ';
$_['button_buy']                    = 'Mua';

// Tab
$_['tab_affiliate']                 = 'Tiếp thị';
$_['tab_address']                   = 'Địa chỉ';
$_['tab_additional']                = 'Bổ sung thêm';
$_['tab_admin']                     = 'Quản trị viên';
$_['tab_attribute']                 = 'Thuộc tính';
$_['tab_customer']                  = 'Thông tin khách hàng';
//customize
$_['customer_firstname']        	= 'Họ';
$_['customer_lastname']        		= 'Tên';
$_['customer_phone']        		= 'Số điện thoại';
$_['customer_detail']        		= 'Khách hàng';
$_['customer_email']        		= 'Email';

$_['tab_comment']                   = 'Nhận xét';
$_['tab_data']                      = 'Dữ liệu';
$_['tab_description']               = 'Mô tả';
$_['tab_design']                    = 'Thiết kết';
$_['tab_discount']                  = 'Giảm giá';
$_['tab_documentation']             = 'Tài liệu';
$_['tab_general']                   = 'Thông thường';
$_['tab_history']                   = 'Lịch sử';
$_['tab_ftp']                       = 'FTP';
$_['tab_ip']                        = 'Địa chỉ IP';
$_['tab_links']                     = 'Liên kết';
$_['tab_log']                       = 'Nhật ký';
$_['tab_image']                     = 'Ảnh';
$_['tab_option']                    = 'Thuộc tính';
$_['tab_server']                    = 'Máy chủ';
$_['tab_seo']                       = 'SEO';
$_['tab_store']                     = 'Cửa hàng';
$_['tab_special']                   = 'Đặc biệt';
$_['tab_session']                   = 'Phiên';
$_['tab_local']                     = 'Địa phương';
$_['tab_mail']                      = 'Mail';
$_['tab_module']                    = 'Mô đun';
$_['tab_payment']                   = 'Chi tiết thanh toán';
$_['tab_product']                   = 'Sản phẩm';
$_['tab_reward']                    = 'Điểm thưởng';
$_['tab_shipping']                  = 'Chi tiết vận chuyển';
$_['tab_total']                     = 'Tổng';
$_['tab_transaction']               = 'Giao dịch';
$_['tab_voucher']                   = 'Voucher';
$_['tab_sale']                      = 'Giảm giá';
$_['tab_marketing']                 = 'Marketing';
$_['tab_online']                    = 'Đang Online';
$_['tab_activity']                  = 'Hoạt động gần đây';
$_['tab_recurring']                 = 'Định kỳ';
$_['tab_action']                    = 'Thao tác';
$_['tab_google']                    = 'Google';

// Error
$_['error_exception']               = 'Mã lỗi(%s): %s trong %s tại dòng %s';
$_['error_upload_1']                = 'Cảnh báo: Tệp tải lên vượt quá cấu hình upload_max_filesize trong tệp php.ini!';
$_['error_upload_2']                = 'Cảnh báo: Tệp tải lên vượt quá MAX_FILE_SIZE được mô tả ở form HTML!';
$_['error_upload_3']                = 'Cảnh báo: Tệp tải lên được một phẩn!';
$_['error_upload_4']                = 'Cảnh báo: Không có tệp nào được tải lên!';
$_['error_upload_6']                = 'Cảnh báo: Thiếu thư mục tạm thời!';
$_['error_upload_7']                = 'Cảnh báo: Ghi thông tin lưu trữ bị lỗi!';
$_['error_upload_8']                = 'Cảnh báo: Quá trình tải tệp lên bị tạm dừng do lỗi!';
$_['error_upload_999']              = 'Cảnh báo: Không tồn tại mã lỗi!';
$_['error_curl']                    = 'CURL: Mã lỗi(%s): %s';

$_['choose_in_filter']                                     = "Đã chọn";
$_['item_in_filter']                                       = "mục";
$_['choose_all_in_filter']                                 = "Đã chọn tất cả trong trang này.";
$_['alert_notice']                            = "Thông báo!";
$_['detailed_instructions_here']    = 'Hướng dẫn chi tiết tại đây';