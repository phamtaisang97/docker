<?php
// Heading

$_['favicon']                   = 'Favicon';
$_['img_favicon']               = 'FAVICON PHOTO';
$_['upload_favicon']            = 'Upload photos';
$_['note_favicon']              = '* Note: The format must be .ico, the image will be scaled to 32x32px';