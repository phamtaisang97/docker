<?php
// Heading
$_['heading_title']          = 'color';
$_['heading_main']           = 'main';

// Text
$_['text_success']           = 'Success: You have edited the Interface color!';

// Error
$_['error_permission']       = 'Warning: You do not have sufficient permissions to edit the interface color!';

// Colors
$_['color_background_title'] = 'Background color';
$_['color_text_big_header']  = 'Large header text color';
$_['color_text_content']     = 'Content text color';
$_['color_horizon_line']     = 'Color horizontal line';
$_['color_button']           = 'Button color';
$_['color_button_text']      = 'Button text color';
$_['color_extra']            = 'Support color';
$_['color_sale_card']        = 'Promotional card color';