<?php
// Theme
$_['tab_title_theme_setting']             = 'theme setting';
$_['theme_title_color']                   = 'color';
$_['theme_title_text']                    = 'text display';
$_['theme_title_favicon']                 = 'Favicon';
$_['theme_title_social']                  = 'social media';
$_['theme_title_checkout_page']           = 'Checkout page';

// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";