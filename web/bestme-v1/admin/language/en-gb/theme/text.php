<?php
// Heading
$_['heading_title']        = 'text display';
$_['heading_font']         = 'HEADINGS';
$_['text_heading_font']     = 'Font';
$_['text_heading_type']     = 'type';
$_['text_heading_size']     = 'Basic font size ';

// Body text
$_['body_font']             = 'BODY TEXT';
$_['text_body_font']        = 'Font';
$_['text_body_type']        = 'type';
$_['text_body_size']        = 'size ';