<?php
// Heading
$_['social']                    = 'social media';
$_['social_setup']              = 'path setting';
$_['social_facebook']           = 'Facebook';
$_['social_twitter']            = 'Twitter';
$_['social_instagram']          = 'Instagram';
$_['social_tumblr']             = 'Tumblr';
$_['social_youtube']            = 'Youtube';
$_['social_google_plus']        = 'Google Plus';