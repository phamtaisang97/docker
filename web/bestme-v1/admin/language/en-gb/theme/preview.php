<?php
// Heading
$_['heading_title']          =  'Preview the interface';

// Text
$_['text_success']           = 'Success: You have edited the Preview of the interface!';
$_['confirm_box_title']        =    'Confirm';
$_['confirm_box_btn_yes']        =    'Yes';
$_['confirm_box_btn_no']         =    'No';

// Error
$_['error_permission']       = 'Warning: You do not have sufficient permissions to edit the interface preview!';

// Left menu
$_['menu_text_homepage']                  = 'Home';
$_['menu_text_category']                  = 'Product list';
$_['menu_text_product_detail']            = 'Product details';
$_['menu_text_blog']                      = 'Blog Catalog';
$_['menu_text_contact']                   = 'Contact page';

// Top menu
$_['button_save']                         = 'Save interface';
$_['button_export']                       = 'Publishing';