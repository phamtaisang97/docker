<?php
// Heading
$_['heading_title']                  = 'General settings';
$_['heading_settings']               = 'Settings';

// title
$_['title_website_info']               = 'Website information';
$_['title_contact_info']               = 'Contact information';
$_['title_formats']                    = 'Standard and format';

// text
$_['text_website_info']                = 'The basic information of the website helps ' . PRODUCTION_BRAND_NAME . ' and customers contact you.';
$_['text_shop_name']                   = 'Store name';
$_['text_home_title']                  = 'Homepage title';
$_['text_home_des']                    = 'Homepage description';
$_['text_management_email']            = 'Managed email';
$_['text_management_email_des']        = '' . PRODUCTION_BRAND_NAME . ' will send a message to you via this email address.';
$_['text_cs_email']                    = 'Customer care email';
$_['text_cs_email_des']                = 'You will use this email address to send care information to customers.';
$_['text_contact_info']                = 'Information used in order notifications and addresses to contact the store.';
$_['text_hotline']                     = 'Hotline';
$_['text_hotline_des']                 = 'Hotline will display in the Product detail page. Applies only to theme Egomall, Farm88, Chili, Car Parts Auto, Hani, Grocerie, Sun Electro, Flavor, S Building and S Furniture';
$_['error_hotline']                    = 'Invalid contact hotline !';

$_['text_business_name']               = 'Business name';
$_['text_business_tax_code']           = 'Business code / Tax code';
$_['text_telephone']                   = 'Phone number';
$_['text_address']                     = 'Address';
$_['text_city']                        = 'Province / city';
$_['text_country']                     = 'Country';

$_['text_formats']                     = 'Information used in order notifications and addresses to contact the store.';
$_['text_time_zone']                   = 'Time zone';
$_['text_weight']                      = 'Weight';
$_['text_currency']                    = 'Currency unit';
$_['text_order_code_format']           = 'Order code format';
$_['text_order_code_format_des']       = 'By default your order code will be numbered from # 1001, you can change the prefix or suffix to create another format code, like "EN1001" or "1001- A ”';
$_['text_order_code_prefix']           = 'Prefix';
$_['text_order_code_suffix']           = 'Suffix';
$_['text_order_code_prefix_des']       = 'Your order code will have the format:';

//entry
$_['entry_shop_name']                   = 'Enter store name';
$_['entry_home_title']                  = 'Enter the homepage title';
$_['entry_home_des']                    = 'Enter homepage description';
$_['entry_email']                       = 'Enter email';

$_['entry_business_name']               = 'Enter business name';
$_['entry_tax_code']                    = 'Enter tax code';
$_['entry_telephone']                   = 'Enter phone number';
$_['entry_address']                     = 'Enter address';


// button
$_['btn_save']                      = 'Save change';
$_['btn_cancel']                    = 'Skip';

// Error
$_['error_warning']             = 'Warning: Please check the form carefully for errors!';
$_['error_permission']          = 'Warning: You do not have permission to modify customers!';
$_['error_name']                = 'Warning: The store name is invalid, too short or too long (1-50 characters)! ';
$_['error_title']               = 'Warning: Title homepage is invalid, too short or too long (1-128 characters)!';

$_['error_input_null']                 = 'Please fill in this field';
$_['error_type_mail']                  = 'Please enter the correct email format';
$_['error_max_length_255']             = 'Do not exceed 255 characters';
$_['error_max_length_300']             = 'Do not exceed 300 characters';
$_['error_max_length_50']             = 'Do not exceed 50 characters';
$_['error_max_length_15']              = 'Do not exceed 15 characters';
$_['error_type_phone']                 = 'Please enter a number for this field';
$_['error_max_length_10']              = 'Do not exceed 10 characters';
$_['error_min_length_8']               = 'Please enter at least 8 characters';

$_['choose_city']   = "Choose city";
$_['choose_country']   = "Choose country";

$_['text_success_update']           = 'Update information successfully!';

$_['image_qr_code_for_bill']                    = 'Photo QR Code for orders';
$_['text_popup_image_edit']                     = "Edit";
$_['text_popup_image_remove']                   = "Remove";