<?php
// Heading
$_['heading_title']                  = 'Add account';
$_['edit_heading_title']             = 'Edit account';

// Text
$_['breadcrumb_setting']                        = 'Settings';
$_['breadcrumb_account_setting']                = 'Account Settings';
$_['breadcrumb_account_setting_add_staff']      = 'Add account';

$_['title_logout_account']                      = 'Logout account';
$_['title_logout_account_des']                  = 'Terminate login sessions of this account.';
$_['title_logout_account_button']               = 'Terminate login sessions';
$_['title_logout_account_button_des']           = 'This account will have to log back in from all devices.';

$_['title_delete_account']                      = 'Delete employee account';
$_['title_delete_account_des']                  = 'Information used in order notifications and addresses to contact the store.';
$_['btn_delete_account']                        = 'Delete account';
$_['btn_delete_account_des']                    = 'The account will be permanently deleted from the system and cannot be restored.';

$_['save_setting_button']                       = 'Save changes';
$_['skip_setting_button']                       = 'Skip';

$_['txt_not_change_pass']                       = 'Skip if not changed';
$_['txt_group_staff']                           = 'Group staff';
$_['txt_store_staff']                           = 'Store';

// Form
$_['staff_account_title']                       = 'Employee account';
$_['staff_account_fill']                        = 'Enter employee account information.';
$_['staff_account_profile']                     = 'Account profile';
$_['staff_account_add_image']                   = 'Select photo';
$_['staff_account_lastname']                    = 'Lastname';
$_['staff_account_lastname_placeholder']        = 'Enter lastname';
$_['staff_account_firstname']                   = 'Name';
$_['staff_account_firstname_placeholder']       = 'Enter name';
$_['staff_account_email']                       = 'Email address';
$_['staff_account_email_placeholder']           = 'Enter Email';
$_['staff_account_phone']                       = 'Phone number';
$_['staff_account_phone_placeholder']           = 'Enter phone number';
$_['staff_account_intro']                       = 'Referral information (Optional)';
$_['staff_account_intro_placeholder']           = 'Enter referral information';
$_['group_staff_placeholder']                   = 'Select group staff';
$_['store_placeholder']                         = 'Select store';
// password
$_['staff_account_new_password']                          = 'Enter new password';
$_['staff_account_new_password_placeholder']              = 'New password';
$_['staff_account_new_password_confirm']                  = 'Confirm new password';
$_['staff_account_new_password_confirm_placeholder']      = 'Re-enter the password';

// permission
$_['staff_account_all_permission']                          = 'This account has access to all functions';
$_['staff_account_permission_choose']                       = 'Select the functions this account can access:';
$_['staff_permission_management']                           = 'Management';
$_['staff_permission_home']                                 = 'Home';
$_['staff_permission_order']                                = 'Order';
$_['staff_permission_product']                              = 'Product';
$_['staff_permission_customer']                             = 'Customer';
$_['staff_permission_setting']                              = 'Setting';
$_['staff_permission_online_store']                         = 'Online store';
$_['staff_permission_interface']                            = 'Interface';
$_['staff_permission_menu']                                 = 'Menu';
$_['staff_permission_blog_page']                            = 'Blog and Content page';
$_['staff_permission_domain']                               = 'Domain name';


// alert
$_['alet_password_confirm']                                 = 'Confirm password and password are not the same!';


// Error
$_['error_warning']             = 'Warning: Please check the form carefully for errors!';
$_['error_permission']          = 'Warning: You do not have permission to modify customers!';
$_['error_exists']              = 'Warning: Email address has been registered before!';
$_['error_phone_exists']        = 'Warning: Phone number has been registered before!';
$_['error_phone']               = 'Phone number is invalid!';
$_['error_firstname']           = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']            = 'Last Name must be between 1 and 32 characters!';
$_['error_email']               = 'Địa chỉ email không hợp lệ!';
$_['error_telephone']           = 'Telephone must be between 3 and 32 characters!';
$_['error_password']            = 'Password must be between 4 and 20 characters!';
$_['error_confirm']             = 'Password and password confirmation do not match!';
$_['error_address_1']           = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']                = 'City must be less than 128 characters!';
$_['error_district']            = 'District must be less than 128 characters!';
$_['error_postcode']            = 'Postcode must be between 2 and 10 characters for this country!';
$_['error_country']             = 'Please select a country!';
$_['error_zone']                = 'Please select a region / state!';
$_['error_custom_field']        = '%s required!';
$_['error_tracking']            = 'Tracking Code required!';
$_['error_tracking_exists']     = 'Tracking code is being used by another affiliate!';
$_['error_cheque']              = 'Cheque Payee Name required!';
$_['error_paypal']              = 'PayPal Email Address does not appear to be valid!';
$_['error_bank_account_name']   = 'Account Name required!';
$_['error_bank_account_number'] = 'Account Number required!';

$_['error_empty']                  = 'Do not leave blank!';
$_['error_input_max']              = 'Field data is longer than 255!';
$_['error_input_max_min_telephone']          = 'Field data must be greater than 8 smaller than 15!';
$_['error_input_max_min_password']           = 'Field data must be greater than 6 smaller than 255!';
$_['error_number']                           = 'Phone number is invalid!';
$_['error_mail_address']                     = 'You entered the wrong email!';

$_['txt_account']                       = 'account';
$_['confirm_password_title']                    = 'Confirm your password';
$_['confirm_password_message']                  = 'Please confirm you have the right to change';
$_['confirm_password_placeholder']              = 'confirm password';
$_['confirm_password_skip']                     = 'skip';
$_['confirm_password_accept']                   = 'confirm';
$_['alarm_limit_staff']                         = "You have used 80% of the staff number limit of service package";