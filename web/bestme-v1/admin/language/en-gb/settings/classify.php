<?php
// Heading
$_['heading_title']                 = 'Classify';

$_['breadcrumb_setting']            = 'Setting';

$_['category']                      = 'Category';
$_['manager_category']              = 'Manager Category';
$_['search']                        = 'Search';
$_['add_category']                  = 'Add Category';
$_['name_category']                 = 'Category name';
$_['add_new_category']              = 'Add Category';

$_['manufacture']                   = 'Manufacture';
$_['manager_manufacture']           = 'Manager Manufacture';
$_['add_new_manufacture']           = 'Add Manufacture';
$_['name_manufacture']              = 'Manufacture name';
$_['name_store']                    = 'Store name';
$_['add_manufacture']               = 'Add Manufacture';

$_['tag']                           = 'Tags';
$_['manager_tag']                   = 'Manager Tags';
$_['add_new_tag']                   = 'Add Tag';
$_['name_tag']                      = 'Tag name';
$_['add_tag']                       = 'Add Tag';

$_['close']                         = 'Close';
$_['add_new']                       = 'Add';
$_['save']                          = 'Save';
$_['delete']                        = 'Delete';

$_['edit_category']                 = 'Edit Category';
$_['edit_manufacture']              = 'Edit Manufacture';

$_['text_category_note']            = 'Note: Setting displayed position of the product type on the website by numbering on blank box at the end of line. Display order: numbered from small to large, only allow numbering from 0 and up';

$_['store']                         = 'Store';
$_['manager_store']                 = 'Manager Store';
$_['edit_tag']                      = 'Edit Tag';
$_['add_store']                     = 'Add Store';
$_['add_new_store']                 = 'Add new Store';
$_['edit_store']                    = 'Edit Store';

$_['delete_category']               = 'Delete Category';
$_['delete_manufacture']            = 'Delete Manufacturer';
$_['delete_tag']                    = 'Delete Tag';
$_['delete_store']                  = 'Delete Store';

$_['confirm_delete_category']       = 'Are you sure you want to delete this Category?';
$_['confirm_delete_manufacture']    = 'Are you sure you want to delete this Manufacturer?';
$_['confirm_delete_tag']            = 'Are you sure you want to delete this Tag?';
$_['confirm_delete_store']          = 'Are you sure you want to delete this Store?';

$_['error_name_category_empty']     = 'Category name empty';
$_['error_name_manufacture_empty']  = 'Manufacturer name empty';
$_['error_name_tag_empty']          = 'Tag name empty';
$_['error_name_store_empty']         = 'Store name empty';

$_['error_name_category_isset']     = 'Category name already existed';
$_['error_name_manufacture_isset']  = 'Manufacturer name already existed';
$_['error_name_tag_isset']          = 'Tag name already existed';
$_['error_name_store_isset']        = 'Store name already existed';

$_['add_category_success']          = "Add Category successfully";
$_['add_manufacture_success']       = "Add Manufacturer successfully";
$_['add_tag_success']               = "Add Tag successfully";
$_['add_store_success']             = "Add Store successfully";

$_['edit_category_success']                    = "Edit Category successfully";
$_['edit_category_error_has_discount']         = "Warning: could not edit the category that is in a running discount";
$_['edit_manufacture_success']                 = "Edit Manufacturer successfully";
$_['edit_manufacture_error_has_discount']      = "Warning: could not edit the manufacturer that is in a running discount";
$_['edit_tag_success']                         = "Edit Tag successfully";
$_['edit_store_success']                       = "Edit Store successfully";

$_['delete_category_success']       = "Delete Category successfully";
$_['delete_manufacture_success']    = "Delete Manufacturer successfully";
$_['delete_tag_success']            = "Delete Tag successfully";
$_['delete_store_success']          = "Delete Store successfully";

$_['delete_category_error']                      = "Delete Category failed";
$_['delete_category_error_has_discount']         = "Warning: could not delete the category that is in a running discount";
$_['delete_manufacture_error']                   = "Delete Manufacturer failed";
$_['delete_manufacture_error_has_discount']      = "Warning: could not delete the manufacturer that is in a running discount";
$_['delete_tag_error']                           = "Delete Tag failed";
$_['delete_store_error']                         = "Delete Store failed";

$_['paginator_show']              = "Display";
$_['paginator_total']             = "of total";
$_['paginator_page']              = "pages";

$_['tooltip_category']            = "production(s) belongs to category";
$_['tooltip_manufacture']         = "production(s) provided by";

$_['error_name_empty']         = "Enter text to field name";
$_['error_name_max_length']    = "Name could not be over 200 characters";
$_['error_name_isset']         = "Name has already existed";
$_['error_not_delete']         = "Could not delete because of already has products belong to this";

// Error File
$_['error_permission'] = 'Warning: Request denied due to authority!';
$_['error_filename']   = 'Warning: The file name is from 3 to 255!';
$_['error_folder']     = 'Warning: Folder names are from 3 to 255!';
$_['error_exists']     = 'Warning: Directory already exists!';
$_['error_directory']  = 'Warning: The directory does not exist yet!';
$_['error_filesize']   = 'Warning: File size cannot exceed 1MB!';
$_['error_filetype']   = 'Warning: Only .xls and xlsx! formats are accepted!';
$_['error_upload']     = 'Warning: Unable to upload files to the system (unknown error)!';
$_['error_delete']     = 'Warning: This folder cannot be deleted!';
$_['error_limit']      = 'Warning: The maximum product number and product import version is 500!';
$_['error_not_found_fields']      = 'Warning: No title or description found!';
$_['error_file_incomplete']       = 'Warning: File is incomplete (missing columns)!';
$_['error_file_incomplete_in_row']       = 'Warning: Missing level_1, level_2, level_3 or status is not 0, 1 or image url is invalid in line ';
$_['error_file_wrong_level_format_in_row']       = 'Warning: Wrong category levels format in line ';
$_['error_file_category_exist_in_row']       = 'Warning: Category already exists in line ';
$_['error_unknown']                      = 'Unknown error';
$_['error_no_file_selected']             = 'No files have been selected!';

$_['error_input_null']                 = 'Please fill out this field';
$_['error_input_price_null']           = 'Please enter price';
$_['error_input_product_name']         = 'Please fill out this field product name';
$_['error_type_mail']                  = 'Please enter the correct email format';
$_['error_max_length_255']             = 'Does not exceed 255 characters';
$_['error_max_length_15']              = 'Do not exceed 15 characters';
$_['error_max_length_12']              = 'Do not exceed 10 characters';
$_['error_max_length_14']              = 'Do not exceed 11 characters';
$_['error_type_phone']                 = 'Please enter a number for this field';
$_['error_max_length_10']              = 'Do not exceed 10 characters';
$_['error_max_length_100']             = 'Not exceeding 100 characters';
$_['error_max_length_30']              = 'Do not exceed 30 characters';
$_['error_max_length_20']              = 'Do not exceed 20 characters';
$_['error_max_length_48']              = 'Do not exceed 48 characters';
$_['error_max_length_50']              = 'Do not exceed 50 characters';
$_['error_type_barcode']               = 'Only allow alphanumeric input';
$_['error_type_sku']                   =   'Do not enter Vietnamese accented';
$_['error_type_weight']                = "Product weight greater than or equal 1 gram and less than 9,999,999 gram";
$_['error_special_char']               = "Do not enter special characters";
$_['error_type_sku_same']              =   'SKU code already exists';
$_['error_promotion_price']            = 'Promotional price must be less than the original price';
$_['error_promotion_price2']            = 'Original price must be greater than the promotional price';
$_['error_text_char']                  = 'Do not enter special characters';

// Text file
$_['text_uploaded']              = 'Success!';
$_['text_directory']             = 'Success: Create directory successfully!!';
$_['text_delete']                = 'Success: The file or folder has been deleted!';
$_['text_product_uploaded']      = ' category(s) has been updated.';

//
$_['text_note_max_level_categories']         = "For customers to have the best shopping experience on the website, each product has a maximum of 3 levels.";
$_['text_count_product_of_category']         = "products of this type";
$_['text_popup_title_category']              = "Category title";
$_['text_enter_popup_title_category']        = "Enter category title";
$_['text_popup_list_child_category']         = "Sub categories";
$_['text_popup_add_child_category']          = "Add sub category";
$_['text_popup_enter_name_sub_category']     = "Enter name sub category";
$_['text_popup_add_category_of_select2']     = "Add";
$_['text_popup_enter_new_category']          = "Enter new category";
$_['text_popup_image_note']                  = "Image depicting the category will display on the website interface. Each category only use a description image.";
$_['text_popup_image_title']                 = "Image description";
$_['text_popup_image_edit']                  = "Edit";
$_['text_popup_image_remove']                = "Remove";

$_['select2_notice_search']                  = "Search ...";
$_['select2_notice_load_more']               = "Load ...";
$_['select2_notice_category_exist']          = "Category already exists";
$_['select2_notice_not_result']              = "No matching results were found";

// export
$_['export_file_col_level_1']           = 'level_1';
$_['export_file_col_level_2']           = 'level_2';
$_['export_file_col_level_3']           = 'level_3';
$_['export_file_col_status']            = 'status';
$_['export_file_col_image_url']         = 'image_url';

// for import different language
$_['export_file_col_level_1_2']           = 'cấp_1';
$_['export_file_col_level_2_2']           = 'cấp_2';
$_['export_file_col_level_3_2']           = 'cấp_3';
$_['export_file_col_status_2']            = 'trạng_thái';
$_['export_file_col_image_url_2']         = 'đường_dẫn_ảnh';

$_['category_import_file']                    = 'Import';
$_['upload_category_list_title']              = 'Upload the categories list';
$_['button_upload_continue']                  = 'Upload';
$_['upload_category_sample_text']             = 'Download file sample categories list upload here';
$_['here']                                    = 'here';
$_['choose_file']                             = 'Choose File';

$_['text_toggle_category_products']       = 'Select the product (not) associated with the category';
$_['search_product_category_placeholder'] = 'Select product to category';
$_['search_product']                      = 'Search product';
$_['text_remove_all_category_products']   = 'Unmounted all products';

// v2.8.3
$_['txt_category_title']                = 'Title shows on the webside';
$_['txt_des_category']                  = 'The default displayed is Product category';
$_['txt_placeholder_category']          = 'Product list';
$_['txt_confirm']                       = 'Confirm';
$_['error_category_title']              = 'The product list cannot be empty and must not exceed 50 characters';
$_['text_success_update']               = 'Update successful';
