<?php
// Heading
$_['heading_title']                  = 'Delivery settings';

// Text
$_['breadcrumb_setting']                        = 'Settings';
$_['breadcrumb_delivery_setting']               = 'Delivery settings';

$_['delivery_setting_title']                    = 'Delivery';

$_['delivery_setting_fee_title']                = 'Shipping fee';
$_['delivery_setting_fee_description']          = 'Add shipping fees for different areas.';

$_['delivery_methods_title']                    = 'delivery method';
$_['delivery_methods_description']              = 'Select the delivery method you want to activate for your sales website.';
$_['delivery_methods_add_button']               = 'Add delivery method';

// form
$_['delivery_methods_form_name']                         = 'Method name';
$_['delivery_methods_form_name_placeholder']             = 'Method name';
$_['delivery_methods_form_fee']                          = 'Shipping fee';
$_['delivery_methods_form_fee_region']                   = 'Region:';
$_['delivery_methods_form_fee_region_setting_button']    = 'Setting';
$_['delivery_methods_form_fee_region_add_button']        = 'Add region';

$_['delivery_methods_form_button_save']                  = 'Save change';
$_['delivery_methods_form_button_cancel']                = 'Skip';

$_['delivery_methods_item_status_activated']             = 'Activate:';
$_['delivery_methods_item_status_deactivated']           = 'Deactivated:';
$_['delivery_methods_item_edit_button']                  = 'Edit';
$_['delivery_methods_item_activated_button']             = 'Activate';
$_['delivery_methods_item_deactivated_button']           = 'Deactivated';
$_['delivery_methods_item_delete_button']                = 'Delete';

// fee region form
$_['delivery_methods_form_fee_region_title']                      = 'Area settings';
$_['delivery_methods_form_fee_region_name']                       = 'Shipping area name';
$_['delivery_methods_form_fee_region_name_placeholder']           = 'Enter region name';
$_['delivery_methods_form_fee_region_provinces']                  = 'Provinces in shipping areas';
$_['delivery_methods_form_fee_region_provinces_placeholder']      = 'Select province';
$_['delivery_methods_form_fee_region_weight_step']                = 'Weight step ';
$_['delivery_methods_form_fee_region_price']                      = 'Shipping price';
$_['delivery_methods_form_fee_region_weight_step_from']           = 'From';
$_['delivery_methods_form_fee_region_weight_step_above']           = 'Above';
$_['delivery_methods_form_fee_region_weight_step_from_type']      = 'FROM';
$_['delivery_methods_form_fee_region_weight_step_upper']          = 'Above';
$_['delivery_methods_form_fee_region_weight_step_upper_type']     = 'ABOVE';
$_['delivery_methods_form_fee_region_weight_step_add_button']     = '+ Add weight step';
$_['delivery_methods_form_fee_region_quick_delete']               = 'Delete';
$_['delivery_methods_form_fee_region_delete_button']              = 'Delete region';
$_['delivery_methods_form_fee_region_save_button']                 = 'Save';
$_['delivery_methods_form_fee_region_cancel_button']              = 'Skip';

$_['delivery_methods_form_fee_region_delete_confirm']       = 'Are you sure you delete this area?';
$_['delivery_methods_form_delete_confirm']                  = 'Are you sure to delete this shipping method?';

// fee COD
$_['delivery_methods_form_cod_title']                  = 'Collection fee';
$_['delivery_methods_form_cod_fixed_price']            = 'Fixed price';
$_['delivery_methods_form_cod_fixed_title']            = 'Default fee';
$_['delivery_methods_default_text']                    = 'Default Method: ';
$_['delivery_methods_form_cod_fixed_price_suffix']     = 'VND / Order';
$_['delivery_methods_form_cod_dynamic_price']          = 'By % Order value';
$_['delivery_methods_form_cod_dynamic_price_suffix']   = '(%) Order value';
$_['help_text']                                  = 'Default shipping method, displayed when no shipping method supports the shipping address.';
// Error form
$_['error_delivery_id']                          = 'Error: Initializing new shipping method information failed, please try again ';
$_['error_name']                                 = 'Error: Shipping method name is empty or too short or too long (1-128 characters) or already exists';
$_['error_guide']                                = 'Error: Instructions for shipping methods are too short or too long (1-2000 characters)';
$_['error_status']                               = 'Error: Shipping method status is incorrect, please try again';
$_['error_provinces']                            = 'Error: Not entered the area for shipping method';
$_['error_step_price']                           = 'Error: Shipping price must be greater or equal to zero';
$_['error_weight']                               = 'Error: shipping volume must increase gradually';
$_['error_weight_from_to']                       = 'Error: shipping volume must increase gradually';

$_['error_input_null']                 = 'Please fill in this field';
$_['error_type_mail']                  = 'Please enter the correct email format';
$_['error_max_length_255']             = 'Do not exceed 255 characters';
$_['error_max_length_15']              = 'Do not exceed 15 characters';
$_['error_type_phone']                 = 'Please enter a number for this field';
$_['error_max_length_10']              = 'Do not exceed 10 characters';
$_['error_require_and_active']         = 'Must be have at least 1 delivery method in active.';

$_['text_success']              = 'Update information successfully!';
$_['text_success_add_delivery']              = 'Add delivery method successfully!';
$_['text_success_delete']              = 'Delete delivery method successfully!';
$_['text_success_edit_status']              = 'Update status delivery method successfully!';

$_['text_ghn']   =  'Giao hàng nhanh';
$_['text_shipping_unit']    = 'Shipping unit';
$_['text_bestme_connection_with_shipping_partners']    = 'Connecting ' . PRODUCTION_BRAND_NAME . ' with shipping partners helps you create bills of lading and manage more conveniently.';
$_['text_fast_delivery_connection']    = 'Connect To Giao Hang Nhanh';
$_['text_after_processing_the_order_on_Bestme']    = 'After processing your order on ' . PRODUCTION_BRAND_NAME . ', the system will automatically create a waybill on Express Delivery.';
$_['text_connected']    = 'Already connected';
$_['text_button_connect']    = 'Connect';
$_['text_button_cancel']    = 'Cancel';
$_['text_button_otp'] = 'Get OTP';
$_['ghn_otp_placeholder'] = 'Enter OTP here';
$_['text_ghn_otp'] = 'Clicking "Connect" will give ' . PRODUCTION_BRAND_NAME . ' the right to create order on the store of your choice at Giao hang nhanh. Please enter the OTP code to confirm.';
$_['text_link_location']   =  'Link to pick up location';
$_['text_token'] = 'Token API';
$_['text_token_placeholder'] = 'Enter Token API here';
$_['text_token_error'] = 'The entered token does not exist. Please check again.';
$_['text_ghn_error_403'] = '<p>You need to get OTP from GHN to complete the installation.</p> 
<p>Step 1: Access GHN app, click Link Location button.</p>
<p>Step 2: Click Get OTP button and enter the received code in the box.</p> 
<p>Step 3: Click Save button to complete the installation.</p>';

$_['text_viettelpost']   =  'ViettelPost';
$_['text_vp_delivery_connection']    = 'Connect To ViettelPost';
$_['text_after_processing_the_order_on_Bestme_vp']    = 'After processing orders on ' . PRODUCTION_BRAND_NAME . ', the system will automatically create a waybill on ViettelPost.';
$_['text_user_name_vp']    = 'ViettelPost account (Email / Phone number)';
$_['text_see_the_instructions']    = 'See the instructions';
$_['text_connect']    = 'Connect now';
$_['text_create_new_account']    = 'Create a new account';
$_['text_collection_of_goods']    = 'Pickup location';
$_['text_cancel_collection']    = 'Cancel the connection';
$_['text_error']   =  '(Error)';
$_['text_for_weight_step']   =  'Follow the volume hiccups';
$_['text_for_price_step']   =  'According to value rung';
$_['error_price']                               = 'Error: hiccup shipping value must increase gradually';
$_['error_price_from_to']                       = 'Error: hiccup shipping value must increase gradually';