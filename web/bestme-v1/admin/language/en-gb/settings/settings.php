<?php
// Heading
$_['heading_title']                  = 'Setting';

// title
$_['title_general_settings']         = 'General setting';
$_['title_account_settings']         = 'Account setting';
$_['title_payment_methods']          = 'Payment method';
$_['title_transport']                = 'Shipping';

// text
$_['text_general_settings']          = 'Update, change your store information';
$_['text_account_settings']          = 'Manage accounts and manage permissions';
$_['text_payment_methods']           = 'Install payment methods for Websites';
$_['text_transport']                 = 'Set up shipping method';

$_['vtp_error_singin']               = 'Account information is not correct, please check again';
$_['vtp_error_address']              = 'The account does not have a shipping address, please update at https://viettelpost.vn. Then reconnect';
$_['vtp_error_token']                = 'You need to connect to the correct ViettelPost account';

$_['classify']                       = 'Classify';
$_['classify_description']           = 'Manage product categories, tags ...';
$_['notify']                         = 'Notify';
$_['notify_description']             = 'Manage notification of successful orders, successful registration ...';