<?php
// Heading
$_['heading_title']                  = 'Account settings';

// Text
$_['breadcrumb_setting']                        = 'Settings';
$_['breadcrumb_account_setting']                = 'Account settings';

$_['account_setting_title']                     = 'Account settings';
$_['account_setting_subtitle']                  = 'Account and authorization';
$_['account_setting_description']               = 'Account settings and website administration permissions';

$_['admin_account_title']                       = 'Admin account';
$_['admin_account_edit_button']                 = 'Edit';
$_['admin_account_last_login']                  = 'Last login at %s days %s';

$_['staff_account_title']                       = 'Employee account';
$_['staff_account_description']                 = 'Add a manager account to your employees and grant access to their settings';
$_['staff_account_add_button']                  = 'Add account';
$_['staff_account_kill_all_sessions']           = 'Disconnecting login sessions';
$_['staff_account_kill_all_sessions_confirm']   = 'Are you sure you want to disconnect login sessions?';
$_['staff_account_kill_all_sessions_success']   = 'Disconnected successful login sessions!';
$_['staff_account_kill_all_sessions_failed']    = 'Disconnecting failed login sessions, please try again later.';
$_['staff_account_last_login']                  = 'Last login at %s days %s';
$_['staff_account_permission_full']             = 'Full access';
$_['staff_account_permission_limit']            = 'Limit';
$_['txt_invalid_password']                      = 'Password is incorrect';
$_['txt_invalid_permission']                    = 'You do not have permission to change this account information!';

$_['txt_success_add']                    = 'Add successful staff!';
$_['txt_success_edit']                   = 'Edit successful staff!';
$_['txt_success_delete']                 = 'Delete successful staff!';
$_['txt_success_edit_admin']             = 'Edit information admin successful!';
$_['txt_view_detail']                    = 'View detail';

$_['staff_group_decentralization']       = 'Staff group decentralization';
$_['txt_staff_group']                    = 'Staff group';
$_['txt_group_name']                     = 'Group name';
$_['number_staff_in_group']              = 'Number staff in the group';
$_['txt_action']                         = 'Action';
$_['action_add_group']                   = 'Add group staff';

$_['trial']        =  'Trial';
$_['basic']        =  'Basic';
$_['optimal']      =  'Optimal';
$_['advance']      =  'Advance';
$_['benefit_package']      =  'Benefit package';
$_['benefit_detail']      =  'Details of the website service package in use. Upgrade or renew the service package';
$_['package_is_in_use']      =  'Package is in use';
$_['renewal_date']      =  'Renewal date';
$_['expiration_date']      =  'Expiration date';
$_['here']             = 'here';
$_['shop_status']      =  'Status';
$_['shop_status_active']        =  'Active';
$_['shop_status_disabled']      =  'Disable';

$_['txt_actual_use']                = 'Actual use';
$_['txt_capacity']                  = 'Capacity';
$_['txt_staff']                     = 'Staff';
$_['txt_store']                     = 'Store';
$_['txt_unlimited']                 = 'Unlimited';
$_['txt_invalid_max_staff']         = 'Operation cannot be performed because you have used up the service staff number limit. Please contact us for further upgrades';
$_['txt_not_apply']                 = 'Do not apply';
$_['txt_created']                   = 'Created';
$_['txt_account']                   = 'Account';
$_['txt_used']                      = 'Used';
$_['alert_create_staff_group']      = "You must create group staff before !";