<?php
// Heading
$_['heading_title']                  = 'Account Settings';

// Text
$_['breadcrumb_setting']                        = 'Settings';
$_['breadcrumb_account_setting']                = 'Account Settings';
$_['breadcrumb_admin_account_setting']          = 'Admin account';

$_['save_setting_button']                       = 'Save change';
$_['skip_setting_button']                       = 'skip';


$_['txt_account']                       = 'Account';

// Confirm password
$_['confirm_password_title']                    = 'Confirm your password';
$_['confirm_password_message']                  = 'Please confirm you have the right to change';
$_['confirm_password_placeholder']              = 'Confirm password';
$_['confirm_password_skip']                     = 'Skip';
$_['confirm_password_accept']                   = 'Confirm';

// Form
$_['admin_account_title']                       = 'Admin account';
$_['admin_account_description']                 = 'Change admin web account information';
$_['admin_account_profile']                     = 'Account profile';
$_['admin_account_add_image']                   = 'Add image';
$_['admin_account_firstname']                   = 'Firstname';
$_['admin_account_firstname_placeholder']       = 'Enter firstname';
$_['admin_account_lastname']                    = 'Lastname';
$_['admin_account_lastname_placeholder']        = 'Enter lastname';
$_['admin_account_email']                       = 'Email address';
$_['admin_account_email_placeholder']           = 'Enter Email';
$_['admin_account_phone']                       = 'Phone number';
$_['admin_account_phone_placeholder']           = 'Enter phone number';
$_['admin_account_intro']                       = 'Referral information (Optional)';
$_['admin_account_intro_placeholder']           = 'Enter referral information';

// Change password
$_['admin_account_change_password']                       = 'Change password';
$_['admin_account_change_password_description']           = 'Change the password you use to log in to your account';
$_['admin_account_change_password_button']                = 'Change password';
$_['admin_account_new_password']                          = 'Enter new password';
$_['admin_account_new_password_placeholder']              = 'New password';
$_['admin_account_new_password_confirm']                  = 'Confirm new password';
$_['admin_account_new_password_confirm_placeholder']      = 'Re-enter password';
// alert
$_['alet_password_confirm']                     = 'Confirm password and password are not the same!';

$_['error_empty']                  = 'Do not leave blank!';
$_['error_input_max']              = 'Field data is longer than 255!';
$_['error_input_max_min_telephone']          = 'Field data must be greater than 8 smaller than 15!';
$_['error_input_max_min_password']           = 'Field data must be greater than 6 smaller than 255!';
$_['error_number']                           = 'You must enter the number type!';