<?php
// Heading
$_['heading_title']                         = 'Notify';
$_['heading_settings']                      = 'Settings';

// button
$_['btn_save']                              = 'Save change';
$_['btn_cancel']                            = 'Skip';

// Error
$_['error_warning']                         = 'Warning: Please check the form carefully for errors!';
$_['error_permission']                      = 'Warning: You do not have permission to modify notify!';

$_['text_update_success']                   = 'Update information successfully!';

$_['text_title_order_success']              = 'Notice when placing an order successfully';
$_['text_description_order_success']        = 'Customize the message displayed on the website after the customer has completed the ordering process';
$_['text_label_order_success']              = 'Message content';

$_['text_title_customer_success']           = 'Notice when registering successfully';
$_['text_description_customer_success']     = 'Custom notification is displayed when the registration is successful';
$_['text_label_customer_success']           = 'Message content';