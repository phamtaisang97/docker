<?php
// Heading
$_['heading_title']                  = 'payment setting';

// Text
$_['breadcrumb_setting']                        = 'Setting';
$_['breadcrumb_payment_setting']                = 'Payment setting';

$_['text_success']                              = 'Success';

$_['payment_setting_title']                     = 'Payment';
$_['payment_setting_subtitle']                  = 'Payment method';
$_['payment_setting_description']               = 'Payment methods when shopping on your website.';

$_['payment_methods_title']                     = 'Payment methods';
$_['payment_methods_description']               = 'Select the payment method you want to activate for your sales website.';
$_['payment_methods_add_button']                = 'Add payment method';

$_['payment_methods_form_name']                 = 'Method name';
$_['payment_methods_form_name_placeholder']     = 'Method name';
$_['payment_methods_form_guide']                = 'Payment instructions';
$_['payment_methods_form_guide_placeholder']    = 'Method description';
$_['payment_methods_form_button_save']          = 'Save changes';
$_['payment_methods_form_button_cancel']        = 'Skip';

$_['payment_methods_item_status_activated']     = 'Activated:';
$_['payment_methods_item_status_deactivated']   = 'Deactivated:';
$_['payment_methods_item_edit_button']          = 'Edit';
$_['payment_methods_item_activated_button']     = 'Activated';
$_['payment_methods_item_deactivated_button']   = 'Deactivated';
$_['payment_methods_item_delete_button']        = 'Delete';
$_['payment_methods_item_delete_confirm']       =  'Are you sure to delete this payment method?';

// Error form
$_['error_payment_id']                          = 'Error: initializing new payment method information failed, please try again';
$_['error_name']                                = 'Error: The shipping method name is empty or too short or too long (1-255 characters) or already exists';
$_['error_guide']                               = 'Error: instructing the payment method is too short or too long (1-1000 characters)';
$_['error_status']                              = 'Error: improper payment method status, please try again';
$_['error_name_min']                            = 'Please enter data in the required field';
$_['error_name_max']                            = 'Please enter data in the field less than 255 characters';
$_['error_payment_isset']                       = 'The payment method name already exists';
$_['error_require_and_active']                  = 'Must be have at least 1 payment method in active.';
$_['you_trail'] =  'You have 4 days of trial left!';
$_['choose_package'] ="Select a user package";
$_['unique_notice'] = "You cannot deactivate the unique payment method";