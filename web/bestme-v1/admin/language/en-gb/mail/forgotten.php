<?php
// Text
$_['text_subject']  = 'Hi, this is a password reset request from %s';
$_['text_greeting'] = 'The new password has been sent to the store administrator %s.';
$_['text_change']   = 'To activate a new password, please click on the link below;: ';
$_['text_ip']       = 'The IP address that sent the password reset request is:';

// Temp (sent password without require click to link)
$_['text_new_password']       = 'Please use the following new password to log in to your store:'; 
$_['text_end']                = 'Sincerely!' . "\n" . 'Bestme team.';