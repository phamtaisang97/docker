<?php
// Text
$_['text_subject']               = '[' . PRODUCTION_BRAND_NAME . '] Notice of cancellation from store %s';
$_['text_greeting']              = 'Hi,'. "n". 'You canceled the order with the code#%s.Detailed information:';

$_['text_order_code']            = 'Order code     : #%s';
$_['text_order_products']        = 'Product        : ' . "\n" .'%s';
$_['text_order_product']         = 'name - %s, quantity: %s, unit price: %s₫, Total: %s₫'; // name - quantity - price - total
$_['text_order_total']           = 'Order value: %s₫'; 

$_['text_wish']                  = 'Wish you can sell more goods with ' . PRODUCTION_BRAND_NAME . '!';
$_['text_end']                   = 'Sincerely!' . "\n". 'Bestme team.';