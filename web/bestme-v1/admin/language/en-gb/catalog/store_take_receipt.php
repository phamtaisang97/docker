<?php
/**
 * Created by PhpStorm.
 * User: Microsoft Windows
 * Date: 19/06/2020
 * Time: 9:09 AM
 */
// Heading
$_['heading_title']                             = 'Store take receipt';
$_['heading_title_top']                         = 'Store take receipt';
$_['heading_title_add']                         = 'Take receipt';
$_['heading_title_edit']                        = 'Take receipt';
$_['heading_title_list']                        = 'List take receipt';

//button
$_['btn_balance_warehouse']                     = 'Balance inventory';
$_['btn_draft']                                 = 'Draft';
$_['btn_receipt_cancel']                        = 'Cancel';

// text
$_['text_version']                              = 'Version:';
$_['text_success_edit']                         = 'Edit take receipt is successfully!';
$_['text_success_add']                          = 'Add take receipt is successfully!';
$_['text_check_goods_note']                     = '*After the product has been checked, click the checkbox at the end of the product to confirm .';
$_['text_receipt_canceled']                     = 'Receipt cancel';
$_['text_note_inventory']                       = '<b>Inventory:</b> is the quantity of product inventories recorded in the system';
$_['text_note_actual']                          = '<b>Actual:</b> is the quantity of products when a user checks inventory';
$_['text_note_diff_values']                     = '<b>Difference value: </b> calculated by the formula (Actual - Inventory) * Cost of products';

// Text
$_['text_add_list']                             = 'Add take receipt';
$_['store_take_receipt_search']                 = 'Search take receipt';
$_['store_take_receipt_filter']                 = 'Filter take receipt';

$_['txt_store_take_receipt_code']               =  "Code receipt";
$_['txt_store_take_receipt_store']              =  "Take warehouse";
$_['txt_store_take_receipt_date']               =  "Date taked";
$_['txt_store_take_receipt_status']             =  "Status";
$_['txt_store_take_receipt_difference_amount']  =  "Difference amount";
$_['text_success_draft']                        = 'Successfully saved the draft!';
$_['text_success_cancel']                            = 'Successfully canceled the receipt!';

// filter
$_['text_filter_method']                          = 'Filter receipt by';
$_['method_filter']                               = 'Status';
$_['choose_filter']                               = 'Chose method';
$_['action_filter']                               = 'Filter';
$_['store_receipt_search']                        = 'Search take receipt';
$_['text_created_at_filter']                      = 'Created at';
$_['text_status_filter']                          = 'Status';

// filter store take
$_['filter_store_take']                           = "Take warehouse";
$_['filter_store_take_placeholder']               = "Chose take warehouse";

// filter status
$_['filter_status']                              = "Status";
$_['filter_status_placeholder']                  = "Chose status";
$_['filter_status_draft']                        = "Draft";
$_['filter_status_balanced']                     = "Balanced";
$_['filter_status_canceled']                     = "Canceled";

// filter date
$_['filter_taked_date']                       =  "Date taked";
$_['filter_taked_date_placeholder']           =  "Chose date taked";

$_['filter_imported_date']                       =  "Date imported";
$_['filter_imported_date_placeholder']           =  "Chose date imported";

$_['filter_date_range']                          = "Date range";
$_['filter_created_at']                          = "Created at";
$_['filter_created_at_placeholder']              = "Created at";
$_['filter_created_at_today']                    = "Today";
$_['filter_created_at_this_week']                = "This week";
$_['filter_created_at_this_month']               = "This month";
$_['filter_created_at_option']                   = "Custome";
$_['filter_select_date_from']                    = "Date from";
$_['filter_select_date_to']                      = "Date to";
$_['filter_txt_date']                            = "Date";

// store take receipt status
$_['store_take_receipt_status_draft']            = "Draft";
$_['store_take_receipt_status_balanced']         = "Balanced";
$_['store_take_receipt_status_canceled']         = "Canceled";

// entry
$_['entry_store']                               = 'Store';
$_['entry_store_placeholder']                   = 'Select store';
$_['entry_product']                             = 'Product';
$_['entry_product_placeholder']                 = 'Select product';
$_['entry_inventory']                           = 'Inventory';
$_['entry_real_inventory']                      = 'Actual';
$_['entry_diff_value']                          = 'Difference value';
$_['entry_reason']                              = 'Reason';
$_['entry_goods_checked']                       = 'Checked';

// warngin
$_['warning_select_store']                      = 'Please select a store before selecting products!';
$_['warning_product_not_checked']               = 'The product has not been checked';
$_['warning_change_store']                      = 'Replace the store will delete all information in the receipt and the quantity products in the current store will not change. Are you sure? ';
$_['warning_quantity_input']                    = 'Please enter quantity';
$_['warning_apply']                             = 'This action will change the quantity of products in the stock. Are you sure to continue?';
$_['warning_cancel']                            = 'Are you sure?';
$_['warning_product_delete']                    = 'The product has been changed or deleted.';

// error
$_['error_permission']                          = 'Warning: You have not permission to edit store take receipt!';
$_['error_data']                                = 'Warning: Data input not correct!';


// Select 2
$_['select_store_placeholder']                  =    "Chose store";
$_['select2_notice_not_result']                 =    "No result";
$_['select2_notice_search']                     =    "Searching ...";
$_['select2_notice_load_more']                  =    "Loading ...";

// validate
$_['txt_select_at_least_1_product']             = "You must select at least one product!";