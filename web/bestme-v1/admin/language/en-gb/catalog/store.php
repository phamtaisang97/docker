<?php
// Heading
$_['heading_title']      = 'Store List';
$_['breadcrumb_setting'] = 'Setting';
$_['search']             = 'Search';
$_['name_store']         = 'Store name';

$_['close']   = 'Close';
$_['add_new'] = 'Add';
$_['save']    = 'Save';
$_['delete']  = 'Delete';
$_['delete_confirm']  = 'Confirm';

$_['store']         = 'Store';
$_['add_new_store'] = 'Add new Store';
$_['edit_store']    = 'Edit Store';

$_['delete_store']           = 'Delete Store';
$_['confirm_delete_store']   = 'Are you sure you want to delete this Store?';
$_['error_name_store_empty'] = 'Store name empty';
$_['error_name_store_isset'] = 'Store name already existed';
$_['add_store_success']      = "Add Store successfully";
$_['edit_store_success']     = "Edit Store successfully";
$_['delete_store_success']   = "Delete Store successfully!";
$_['delete_store_error']     = "Delete Store failed";

$_['paginator_show']  = "Display";
$_['paginator_total'] = "of total";
$_['paginator_page']  = "pages";

$_['error_name_empty']      = "Enter text to field name";
$_['error_name_max_length'] = "Name could not be over 30 characters";
$_['error_name_isset']      = "Name has already existed";
$_['error_not_delete']      = "The store cannot be deleted because it is attached to the product or staff";

$_['error_limit_store'] = "Operation cannot be performed because you have used up the service store number limit. Please contact us for further upgrades.";

$_['alarm_limit_store'] = "You have used 80% of the store number limit of service package";

$_['text_no_result'] = "Not found";

$_['text_products_in_store'] = "Products in the store";
$_['text_nox'] = "No.";
$_['text_note'] = "Note: Stores (warehouses) that are attached to employees or products will not be deleted!";

$_['error_permission']           = 'Warning: You do not have permission to edit Stores!';