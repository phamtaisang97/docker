<?php
/**
 * Created by PhpStorm.
 * User: Microsoft Windows
 * Date: 19/06/2020
 * Time: 9:09 AM
 */
// Heading
$_['heading_title']                 = 'Store transfer receipt';
$_['heading_title_top']             = 'Store transfer receipt';
$_['heading_title_list']            = 'List tranfer receipts';
$_['heading_title_add']             = 'Transfer receipt';
$_['heading_title_edit']            = 'Transfer receipt';
$_['breadcrumb_store']              = 'Store';
$_['breadcrumb_transfer']           = 'Transfer';


// error
$_['error_permission']              = 'Warning: You have not permission to edit transfer receipt!';
$_['error_data']                    = 'Warning: Data input not correct!';

// Text
$_['text_add_list']                  = 'Add transfer receipt';
$_['store_transfer_receipt_search']  = 'Search transfer receipt';
$_['store_transfer_receipt_filter']  = 'Filter transfer receipt';
$_['filter_list_follow']             = "Filter by";

$_['txt_store_transfer_receipt_code']        =  "Receipt code";
$_['txt_store_transfer_receipt_store_from']  =  "Warehouse export";
$_['txt_store_transfer_receipt_store_to']    =  "Warehouse import";
$_['txt_store_transfer_receipt_date_exported']   =  "Export date";
$_['txt_store_transfer_receipt_date_imported']   =  "Import date";
$_['txt_store_transfer_receipt_total']           =  "Total transfer";
$_['txt_store_transfer_receipt_status']          =  "Status";


// filter
$_['text_filter_method']                          = 'Method filter';
$_['method_filter']                               = 'Status';
$_['choose_filter']                               = 'Chose method';
$_['action_filter']                               = 'Filter';
$_['store_receipt_search']                        = 'Filter by code transfer';
$_['text_created_at_filter']                      = 'Date created receipt';
$_['text_status_filter']                          = 'Status';

// filter store export
$_['filter_store_export']                         = "Warehouse export";
$_['filter_store_export_placeholder']             = "Chose warehouse export";

// filter store import
$_['filter_store_import']                         = "Warehouse import";
$_['filter_store_import_placeholder']             = "Chose warehouse import";

// filter status
$_['filter_status']                              = "Transfer status";
$_['filter_status_placeholder']                  = "Chose status";
$_['filter_status_draft']                        = "Draft";
$_['filter_status_transfered']                   = "Transfered";
$_['filter_status_recived']                      = "Recived";
$_['filter_status_canceled']                     = "Canceled";

// filter date
$_['filter_exported_date']                       =  "Export date";
$_['filter_exported_date_placeholder']           =  "Chose export date";

$_['filter_imported_date']                       =  "Import date";
$_['filter_imported_date_placeholder']           =  "Chose import date";

$_['filter_date_range_export']                   = "Date range export";
$_['filter_date_range_import']                   = "Date range import";
$_['filter_created_at']                          = "Created at";
$_['filter_created_at_placeholder']              = "Select date";
$_['filter_created_at_today']                    = "Today";
$_['filter_created_at_this_week']                = "This week";
$_['filter_created_at_this_month']               = "This month";
$_['filter_created_at_option']                   = "Custom";
$_['filter_select_date_from']                    = "Date from";
$_['filter_select_date_to']                      = "Date to";
$_['filter_txt_date']                            = "Date";

// store transfer receipt status

$_['store_transfer_receipt_status_draft']        = "Draft";
$_['store_transfer_receipt_status_transfered']   = "Transfered";
$_['store_transfer_receipt_status_recived']      = "Recived";
$_['store_transfer_receipt_status_canceled']     = "Canceled";

// error
$_['error_update_receipt_status_receipt_id']                        = "Store transfer receipt not exist";
$_['error_update_receipt_status_receipt_status_id']                 = "Store transfer receipt status not exist";
$_['error_store_transfer_receipt_not_exist']                        = "Store transfer receipt not exist";
$_['error_store_transfer_receipt_product_more_quantity']            = "The quantity transferred must not be greater than the actual inventory of the product.";
$_['error_store_transfer_receipt_product_is_deleted']               = "Product no longer exists, please regenerate transfer receipt.";
$_['error_store_transfer_receipt_input_data_is_invalid']            = "Data input is invalid";

//
$_['warning_select_store']                      = 'Please select a export store before selecting products!';

// text btn submit
$_['txt_btn_back']                  = "Cancel";
$_['txt_btn_received']              = "Receive";
$_['txt_btn_transfer']              = "Transfer";
$_['txt_btn_cancel']                = "Cancel receipt";
$_['txt_btn_draft']                 = "Save draft";

// text
$_['txt_product']                               = "Product";
$_['txt_chose_product']                         = "Chose product";
$_['txt_common_quantity_transfer']              = "Bulk store transfer";
$_['txt_input_common_quantity_transfer']        = "Enter quantity";
$_['text_version']                              = 'Version:';
$_['text_success_draft']                        = 'Successfully saved the draft!';
$_['text_success_add']                          = 'Successful delivery!';
$_['text_success_edit']                         = 'Receive the goods successfully!';
$_['text_success_cancel']                       = 'Successfully canceled the transfer!';
$_['text_home']                                 = 'Home';
$_['txt_btn_close']                             = "Cancel";
$_['txt_btn_apply']                             = "Confirm";
$_['text_confirm_received']                     = 'Confirm received';
$_['text_confirm_cancel_receipt']               = 'Confirm cancel receipt';

// table column
$_['col_product_name']                               = "Product";
$_['col_inventory_transfer']                         = "Quantity of store transfer";
$_['col_quantity_transfer']                          = "Quantity transfer";
$_['col_price_transfer']                             = "Price transfer";
$_['col_into_money']                                 = "Total";
$_['col_total_amount']                               = "Total amount";

//
$_['warning_product_delete']                         = 'The product has been changed or deleted.';
$_['warning_change_store']                           = 'All information in the voucher will be deleted. Are you sure you want to change the transfer warehouse?';
$_['warning_change_status_transfer']                 = 'After transfer, the system will minus quantity of the corresponding products in the transfer store but not add to the receive store until confirm received the products.';
$_['warning_change_status_received']                 = 'This action cannot be undone. Confirmation continue?';
$_['warning_change_status_cancel']                   = 'Are you sure you want to cancel this receipt?';
$_['warning_select_at_least_1_product']              = "You must select at least 1 product!";
$_['warning_common_quantity_transfer']               = "The quantity transferred for all products will update to this new quantity. Are you sure you want to change?";

// validate form
$_['mess_not_empty']                                             = "Not empty";
$_['mess_more_than_14_char']                                     = "Do not exceed 14 characters";
$_['mess_more_than_export_store_quantity']                       = "Do not exceed inventory transfer";
$_['mess_input_0_and_more_than_export_store_quantity']           = "Enter the number > 0 and do not exceed the transfer inventory";

// Select 2
$_['select2_notice_not_result']   =    "No match found";
$_['select2_notice_search']       =    "Search ...";
$_['select2_notice_load_more']    =    "Load ...";