<?php
// Heading
$_['heading_title']               = 'Collection list';
$_['heading_title_add']           = 'Add new collection';
$_['heading_title_edit']          = 'Edit collection';
$_['text_success']                = 'Add new collection successfully';
$_['text_success_edit']           = 'Change the collection successfully';

// breadcrumb
$_['breadcrumb_product']                 = 'Collection';
$_['page_title']                  = 'collection';
$_['breadcrumb_product_collection']      = 'Product collection';
//
$_['collection_list_title']              = 'List';
$_['collection_list_filter']             = 'Filter collection';
$_['breadcrumb_product_collection']      = 'Number of products/collections';
$_['breadcrumb_status']                  = 'Display';
$_['breadcrumb_status_hide']             = 'Hide';

// entry
$_['entry_menu_placeholder']             = 'Select menu';

// filter
$_['collection_list_filter_collection_all']    = 'All';
$_['collection_list_filter_status']            = 'Status';
$_['collection_list_filter_action']            = 'Select operation';
$_['collection_list_filter_status_show']       = 'Show collection';
$_['collection_list_filter_status_disabled']   = 'Hide collection';
$_['collection_list_filter_status_delete']     = 'Delete collection';
$_['filter_staff_placeholder']                 = 'Select staff';

$_['delete_confirm']       = 'Are you sure to delete';
$_['confirm_show']         = 'Are you sure to change '; 
$_['text_add']             = 'Add new ';
$_['text_search']          = 'Filter';
$_['text_search_list']          = 'Search collection';
$_['text_cancel']          = 'Ignore';
$_['text_save']            = 'Save change';

$_['text_name']               = 'Collection name';
$_['entry_name_placeholder']  = 'Enter collection name';
$_['text_category']           = 'Category';
$_['text_creator']            = 'Creator';
$_['text_staff']              = 'Staff';
$_['text_action']             = 'Action';
$_['text_content']            = 'Collection content';
$_['text_set_colletion']      = 'Select product into collection';
$_['text_see_search']         = 'Preview search result';
$_['text_seo']                = 'Customizing SEO';
$_['text_more']               = 'Enter a title and description to see how the article is displayed on the search list.'; 
$_['text_tag_title']          = 'Title tag';
$_['text_abouts']             = 'Add description';
$_['text_alias']              = 'Path / Alias';
$_['text_content_seo']        = 'Article content';
$_['text_image']              = 'Image collection';
$_['text_add_image']          = 'Add image'; 
$_['text_set_menu']           = 'Attach to the menu';
$_['text_choose_menu']        = 'Select Menu'; 
$_['text_choose_menu_text']   = 'You can add this collection to a link using the Select menu button';
$_['text_used_text']          = 'Used:';
$_['text_used_char']          = 'Character';
$_['text_add_to_menu']        = 'Add to menu';
$_['text_add_collection_to_menu']        = 'You will add the collection to the following menus:';


$_['sort_by']             = 'Sort: ';
$_['sort_zero']           = 'Handicraft'; 
$_['sort_az']             = 'By name: A-Z'; 
$_['sort_za']             = 'By name: Z-A';
$_['sort_hightolow']      = 'By price: High to low';
$_['sort_lowtohigh']      = 'By price: Low to high';
$_['sort_newtoold']       = 'By creation date: New to old words';
$_['sort_oldtonew']       = 'By creation date: Old to new words';

// Filter
$_['filter_list_follow']      = "Filter products by";
$_['choose_filter']           = "Select filter condition";
$_['filter_status']           = "Display status ";
$_['filter_manufacture']      = "Supplier"; 
$_['filter_category']         = "Product type";
$_['filter_tag']              = "Tagged with";
$_['filter_staff']            = "Staff";
$_['filter_length_product']   = "Number of products";
$_['filter_status']                              = "Display status";
$_['filter_status_placeholder']                  = "Select status";
$_['entry_status']           = 'Status';
$_['entry_status_publish']   = 'Display';
$_['entry_status_publish_short']   = 'Publish';
$_['entry_status_unpublish'] = 'Hide';
// Select 2
$_['select2_notice_not_result']   =    "No matching results were found"; 
$_['select2_notice_search']   =    "Search ..."; 
$_['select2_notice_load_more']   =    "Loading ...";  

$_['select2_notice_not_result']   =    "No matching results were found";
$_['select2_notice_search']   =    "Search ...";
$_['select2_notice_load_more']   =    "Loading ...";
$_['entry_collection_placeholder']   =    "Select product to collection";


$_['delete_collection_confirm_show']          = 'Are you sure you want to delete the collection?';
$_['error_max_length_100']             = 'Not exceeding 100 characters';
$_['error_product']             = 'You must select at least 1 product';
$_['error_title_add']             = 'Collection name cannot be empty';
$_['error_text_char']           = 'Do not enter special characters';
$_['error_text_title']           = 'The title tag cannot be blank';
$_['error_text_check_colletion']           = 'The collection already exists';
$_['choose']                                     = "Selected ";
$_['item']                                       = " item";
$_['txt_selected_all']                           = "Selected all item in this page";
$_['change_success']                 = "Change successfully.";
$_['delete_success']                 = "Delete successfully.";
$_['add_collection_new'] = "+ Add collection";
$_['collection_new']     = "Product collection";
$_['collection_filter_new']     = "Filter";
$_['collection_count_product']     = "product";
$_['collection_form_cancel_mobile']     = "Cancel";
$_['collection_form_save_mobile']     = "Save";
$_['collection_confirm_show']          = 'Are you sure you want to delete ';
$_['collection_confirm_choose']        = " selected collection ?";

$_['choose_product_to_collection']        = "Select products into the collection";
$_['choose_collection_to_collection']        = "Select a collection into the collection";
$_['placeholder_collection_to_collection']        = "Select collection to collection";

$_['action_close']        = "Close";
$_['action_delete']       = "Delete";

$_['text_contenttext_content']        = "Enter content";