<?php
// Heading
$_['heading_title']     = 'Manufacturers';

// Text
$_['text_success']      = 'Success';
$_['text_list']         = 'Manufacturer List';
$_['text_add']          = 'Add Manufacturer';
$_['text_edit']         = 'Edit Manufacturer';
$_['text_product']      = 'Product';
$_['text_default']      = 'Default';
$_['text_percent']      = 'Percentage';
$_['text_amount']       = 'Fixed Amount';
$_['text_keyword']      = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';

// Column
$_['column_name']       = 'Manufacturer Name';
$_['column_sort_order'] = 'Sort Order';
$_['column_action']     = 'Action';
$_['text_options']      = 'Choose action';

// Entry
$_['entry_name']        = 'Manufacturer Name';
$_['entry_store']       = 'Stores';
$_['entry_keyword']     = 'Keyword';
$_['entry_image']       = 'Image';
$_['entry_sort_order']  = 'Sort Order';
$_['entry_type']        = 'Type';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify manufacturers!';
$_['error_name']        = 'Manufacturer Name must be between 1 and 64 characters!';
$_['error_keyword']     = 'SEO URL already in use!';
$_['error_unique']      = 'SEO URL must be unique!';
$_['error_product']     = 'Warning: This supplier cannot be deleted as it is currently assigned to %s products!';
$_['delete_success']         = "Delete the supplier success";
$_['delete_error']         = "Delete the supplier error";
$_['add_success']         = "Add the supplier success";
$_['add_error']         = "Add the supplier error";

// form
$_['text_form_save']                         = 'Save';
$_['text_form_cancel']                       = 'Cancel';
$_['text_form_contact_info']                 = 'Contact information';
$_['text_form_name']                         = 'Manufacturer name';
$_['text_form_name_placeholder']             = 'Enter manufacture name';
$_['text_form_phone']                        = 'Phone';
$_['text_form_phone_placeholder']            = 'Enter phone number';
$_['text_form_email']                        = 'Email';
$_['text_form_email_placeholder']            = 'Enter email';
$_['text_form_tax_code']                     = 'Tax code';
$_['text_form_tax_code_placeholder']         = 'Enter tax code';
$_['text_form_address']                      = 'Address';
$_['text_form_tax_address_placeholder']      = 'Enter address';
$_['text_form_province']                     = 'Province';
$_['text_form_tax_province_placeholder']     = 'Select province';
$_['text_form_district']                     = 'District';
$_['text_form_tax_district_placeholder']     = 'Select district';

// validate form
$_['text_error_form_name_empty']                     = 'Manufacture cannot empty';
$_['text_error_form_name_more_255']                  = 'Manufacture name must be greater than 255 characters';
$_['text_error_form_manufacturer_name_duplicate']    = 'Manufacturer name already exists';
$_['text_error_form_manufacturer_tax_code_duplicate']    = 'Tax code already exists';
$_['text_error_form_tax_code_more_16']               = 'Tax code must not exceed 16 characters';
$_['text_error_form_tax_code_special_characters']    = 'Invalid tax identification number';
$_['text_error_form_telephone_empty']                = 'Phone number cannot empty';
$_['text_error_form_telephone_start']                = 'Prefix must be 84 or 0!';
$_['text_error_form_telephone_wrong_format']         = 'Phone number is invalid.';
$_['text_error_form_telephone_more_15']              = 'Phone number cannot exceed 15 char!';
$_['text_error_form_email_wrong_format']             = 'Email is invalid!';

// Select 2
$_['select2_notice_not_result']   =    "No match found";
$_['select2_notice_search']       =    "Search ...";
$_['select2_notice_load_more']    =    "Load ...";

// title
$_['txt_manufacture_code']                = "Manufacture Code";
$_['txt_manufacture_name']                = "Manufacture Name";
$_['txt_manufacture_phone']               = "Phone";
$_['txt_manufacture_address']             = "Address";
$_['txt_manufacture_status']              = "Status";
$_['text_filter_method']                  = 'Filter by status';
$_['method_filter']                       = 'Status';
$_['choose_filter']                       = 'Select filter condition';
$_['action_filter']                       = 'Filter';
$_['manufacture_search']                  = 'Search manufacturer';

// filter
$_['filter_status']                       = "Status";
$_['filter_staff']                        = "Staff";
$_['filter_staff_placeholder']            = "Select staff";
$_['filter_list_follow']                  = "Filter by";

$_['txt_creator']                         = "Creator";
$_['txt_staff']                           = "Staff";
$_['txt_pause_filter']                    = "Pause transaction";
$_['txt_again_filter']                    = "Transaction again";
$_['txt_delete_filter']                   = "Delete manufacturer";
$_['text_options']                        = 'Select action';
$_['txt_all']                             = 'Manufacturer List';
$_['manufacture_filter']                  = 'Filter';
$_['txt_heading_pause']                   = 'Pause transaction';
$_['txt_heading_again']                   = 'Transaction again';
$_['txt_heading_delete']                  = 'Delete manufacturer';
$_['txt_pause_content']                   = 'Are you sure to pause transaction with this manufacturer?';
$_['txt_again_content']                   = 'Are you sure transaction with this manufacturer again?';
$_['txt_delete_content']                  = 'Are you sure you delete this manufacturer?';

// status
$_['text_in_transaction']                 = 'In transaction';
$_['text_stop_transaction']               = 'Pause transaction';
$_['text_delete_transaction']             = 'Delete';
$_['text_home']                           = 'Home';
$_['text_add_list']                       = 'Create manufacturer';

// modal
$_['text_close']                          = 'Close';
$_['txt_cancel']                          = 'Cancel';
$_['txt_confirm']                         = 'Confirm';

//note
$_['txt_note_delete']                             = 'Delete';
$_['txt_note_manufacture']                        = 'Manufacture';
$_['txt_note_manufacture_one']                    = 'This manufacture';
$_['txt_note_manufacture_pause']                  = 'paused transaction!';
$_['txt_note_manufacture_active']                 = 'in transaction !';
$_['txt_note_active']                             = 'Transaction again';
$_['txt_note_pause']                              = 'Pause transaction';
$_['txt_success_lower']                           = 'success';
$_['txt_manufacture_lower']                       = 'manufacturer';
$_['txt_note_view_detail']                        = 'View detail';
$_['text_discount_result']                        = 'Results of operations';
$_['text_discount_error']                         = 'Error';
$_['text_failure']                                = 'Failure';
$_['txt_exit']                                    = 'Exit';
$_['txt_action_one_success']                      = 'with a successful manufacturer!';

// popup result
$_['text_no_results']                             = 'No data!';
$_['text_manufacturer_code']                      = 'Manufacturer Code';
$_['text_manufacturer_result']                    = 'Result';
$_['text_manufacturer_reason']                    = 'Reason';