<?php
// Heading
$_['heading_title']          = 'Inventory management';

// breadcrumb
$_['breadcrumb_product']                = 'Product';
$_['breadcrumb_warehouse']              = 'Inventory management';

// list
$_['warehouse_setting_title']           = 'Inventory management';

$_['warehouse_list_title']              = 'List';
$_['warehouse_list_filter']             = 'Product filter';
$_['warehouse_list_filter_mobile']             = 'Filter';

// filter
$_['warehouse_list_filter_category']         = 'category';
$_['warehouse_list_filter_category_all']     = 'all';

$_['warehouse_list_filter_warehouse']        = 'warehouse';
$_['warehouse_list_filter_warehouse_all']    = 'all̉';

$_['warehouse_list_filter_status']           = 'Status';
$_['warehouse_list_filter_status_all']       = 'All';
$_['warehouse_list_filter_status_enabled']   = 'Displaỵ';
$_['warehouse_list_filter_status_disabled']  = 'Hide';

$_['warehouse_list_filter_search_button']    = 'Filter';

// search
$_['warehouse_list_search_placeholder']      = 'Search product';

// update modal
$_['warehouse_update_quantity_modal_title']      = 'Update quantity';
$_['warehouse_update_quantity_modal_message']    = 'No products have been selected yet. Please choose a product!';
$_['warehouse_update_quantity_modal_close']      = 'Close';
$_['warehouse_update_quantity_modal_add']        = 'Add';
$_['warehouse_update_quantity_modal_set']        = 'Set';
$_['warehouse_update_quantity_modal_save']       = 'Save';
$_['warehouse_update_quantity_modal_cancel']     = 'Skip';

// list
$_['warehouse_list_choose_action']               = 'Choose action';
$_['warehouse_list_action_stop_sale']            = 'Stop selling when out of stock';
$_['warehouse_list_action_continue_sale']        = 'Continue selling when out of stock';
$_['warehouse_list_action_update_quantity']      = 'Update quantity';

$_['warehouse_list_header_name']                         = 'Product';
$_['warehouse_list_header_sku']                          = 'SKU';
$_['warehouse_list_header_cost_price']                   = 'Cost price';
$_['warehouse_list_header_price']                        = 'Product price';
$_['warehouse_list_header_on_out_of_stock']              = 'Out of stock';
$_['warehouse_list_header_date_modified']                = 'Update date';
$_['warehouse_list_header_quantity']                     = 'Quantity';
$_['warehouse_list_header_update_quantity']              = 'Quantity update';
$_['warehouse_list_header_update_quantity_add']          = 'Add';
$_['warehouse_list_header_update_quantity_set']          = 'Set';
$_['warehouse_list_header_update_quantity_save_button']  = 'Save';

// sale_on_out_of_stock
$_['warehouse_list_sale_on_out_of_stock_continue']       = 'Continue selling';
$_['warehouse_list_sale_on_out_of_stock_stop']           = 'Stop selling';

// pagination
$_['warehouse_list_pagination_message']          = 'Display %s-%s on total %s (%s Pages)';
$_['warehouse_list_pagination_page']             = 'Page';

// Error form
$_['error_quantity']                             = 'Error: the number of products must be between 1 and 9,999,999,999';
$_['text_success']                               = "Successfully updated!";

// Filter
$_['filter_list_follow']                         = "Filter products by";
$_['choose_filter']                              = "Select filter condition ...";
$_['filter_status_text']                         = "Display status";
$_['filter_status_placeholder']                  = "Select status";
$_['filter_manufacture_text']                    = "Supplier";
$_['filter_manufacture_placeholder']             = "Select supplier";
$_['filter_category_text']                       = "Product type";
$_['filter_category_placeholder']                = "Select product type";
$_['filter_tag_text']                            = "Tagged with";
$_['filter_tag_placeholder']                     = "Enter tag";
$_['filter_collection_text']                     = "Collection";
$_['filter_collection_placeholder']              = "Select collection";
$_['filter_length_product']                      = "Number of products";
$_['filter_length_product_placeholder']          = "Select filter conditions";
$_['filter_length_product_greater']              = "greater";
$_['filter_length_product_smaller']              = "maller";
$_['filter_length_product_equal']                = "equal";
$_['filter_length_product_input_placeholder']    = "Enter quantity";
$_['warehouse_list_header_update_quantity_add_title']          = 'Increase the number of products in stock';
$_['warehouse_list_header_update_quantity_set_title']          = 'Reset the number of products in stock';
$_['select_all']                            = 'Select all';

// table
$_['column_price_cost']                   = 'Price cost';
$_['column_inventory']                    = 'Inventory';
$_['column_deliver']                      = 'Deliver';
$_['total_quantity_of_the_warehouse']     = 'Total quantity';
$_['total_value_of_the_warehouse']        = 'Total value';
$_['text_no_results']                     = 'No data!';
$_['text_column_inventory_default_store']                     = 'default store';

// Select 2
$_['select_store_placeholder']   =     "Select store";
$_['select2_notice_not_result']   =    "No match found";
$_['select2_notice_search']       =    "Search ...";
$_['select2_notice_load_more']    =    "Load ...";
$_['txt_all_store']               =    "< All stores >";
$_['txt_default_store']           =    "< Default store >";

$_['switch_inventory_basic']    = "Switch to basic mode";
$_['switch_inventory_advance']  = "Switch to advance mode";