<?php
// Heading
$_['heading_title']         = 'Store receipt';
$_['heading_title_add']     = 'Add store receipt';
$_['heading_title_edit']    = 'Edit store receipt';
$_['heading_title_list']    = 'List store receipt';


// Select 2
$_['select2_notice_not_result']   =    "No match found";
$_['select2_notice_search']       =    "Search ...";
$_['select2_notice_load_more']    =    "Load ...";

// breadcrumbs
$_['text_home']                           = 'Home';
$_['text_success_add']                    = 'Add new store receipt successfully!';
$_['text_success_edit']                   = 'Edit store receipt successfully!';

// button
$_['text_add_list']                       = 'Add store receipt';
$_['text_save_apply']                     = 'Save and approve';
$_['text_save_draft']                     = 'Save draft';

// title table
$_['txt_store_receipt_code']                      = "Code";
$_['txt_store_receipt_manufacturer']              = "Manufacturer";
$_['txt_store_receipt_branch']                    = "Branch";
$_['txt_store_receipt_status']                    = "Status";
$_['txt_store_receipt_total_to_pay']              = "Money to pay";
$_['txt_store_receipt_owed']                      = "Owed";
$_['txt_store_receipt_created_at']                = "Create at";
$_['txt_delivering']                              = "Goods delivering";
$_['txt_receipted']                               = "Imported goods";
$_['txt_store']                                   = "Store";
$_['txt_store_placeholder']                       = "Select store";
$_['txt_manufacturer']                            = "Manufacturer";
$_['txt_manufacturer_placeholder']                = "Select manufacturer";
$_['txt_products']                                = "Products information";
$_['txt_product_search']                          = "Search product";
$_['txt_product_name']                            = "Product name";
$_['txt_quantity']                                = "Quantity";
$_['txt_quantity_placeholder']                    = "Enter quantity";
$_['txt_value']                                   = "Value";
$_['txt_import_price']                            = "Import price";
$_['txt_other_fee']                               = "Other fee";
$_['txt_discount']                                = "Discount";
$_['txt_into_money']                              = "Into money";
$_['txt_billing_info']                            = "Billing Information";
$_['txt_total_money']                             = "Total money";
$_['txt_total_pay']                               = "Total to pay";
$_['txt_total_paid']                              = "Total paid";
$_['txt_total_owed']                              = "Owed";
$_['txt_note']                                    = "Note";
$_['txt_add_other_fee']                           = "Add other fee";
$_['txt_fee_name_placeholder']                    = "Enter fee name";
$_['txt_money_placeholder']                       = "Enter fee";
$_['txt_select_allocation_criteria_type']         = "Select allocation criteria type";
$_['txt_total_fee']                               = "Total fee";
$_['txt_fee_modal_warning']                       = "Must enter fee name and fee value";
$_['txt_add_manufacturer']                        = "Add manufacturer";
$_['txt_add_product']                             = "Add product";
$_['txt_add_product_placeholder']                 = "Add product name";
$_['txt_product_single_version']                  = "Product single version";
$_['txt_product_multiple_version']                = "Product multiple versions";
$_['txt_enter_price_placeholder']                 = "Enter price";
$_['txt_retail_price']                            = "Retail price";
$_['txt_promotion_price']                         = "Promotion price";
$_['txt_sku']                                     = "SKU";
$_['txt_sku_placeholder']                         = "Enter SKU";
$_['txt_attribute_name']                          = "Attribute name";
$_['txt_attribute_value']                         = "Attribute values";
$_['txt_attribute_value_placeholder']             = "Each tag is separated by commas (,)";
$_['txt_add_attribute']                           = "Add attribute";
$_['txt_th_display']                              = "Display";
$_['txt_th_version']                              = "Version";
$_['txt_th_promotion_price']                      = "Promotion price";
$_['text_deleted_or_no_exist']                    = 'The store receipt does not existed or was deleted!';
$_['text_version']                                = 'Version : ';
$_['text_payment_voucher']                        = 'Payment voucher are generated automatically and attached the store receipt is ';
//validate
$_['txt_select_at_least_1_product']               = "You must select at least 1 product!";
$_['error_empty_store']                           = "Please select store";
$_['error_empty_manufacturer']                    = "Please select manufacturer";
$_['error_money_paid']                            = "The money paid must be less than the total to pay";
$_['error_empty_price']                           = "Please enter price";
$_['error_empty_quantity']                        = "Please enter quantity";
$_['error_create_manufacturer']                   = "Add new manufacturer failed";
$_['error_empty_product_name']                    = "Please enter product name";
$_['text_error_form_manufacturer_name_duplicate']    = 'Manufacturer name already exists';
$_['text_error_form_tax_code_more_16']               = 'Tax code must not exceed 16 characters';
$_['text_error_form_tax_code_special_characters']    = 'Invalid tax identification number';
$_['text_error_form_manufacturer_tax_code_duplicate']    = 'Tax code already exists';


// filter
$_['text_filter_method']                          = 'Filter by';
$_['method_filter']                               = 'Status';
$_['choose_filter']                               = 'Select method';
$_['action_filter']                               = 'Filter';
$_['store_receipt_search']                        = 'Search store receipt';
$_['text_created_at_filter']                      = 'Create time';
$_['text_manufacturer_filter']                    = 'Manufacturer';
$_['text_status_filter']                          = 'Status';

// filter created at
$_['filter_date_range']                          = "Date range";
$_['filter_created_at']                          = "Create at";
$_['filter_created_at_placeholder']              = "Select date";
$_['filter_created_at_today']                    = "Today";
$_['filter_created_at_this_week']                = "This week";
$_['filter_created_at_this_month']               = "This month";
$_['filter_created_at_option']                   = "Custom";
$_['filter_select_date_from']                    = "Date from";
$_['filter_select_date_to']                      = "Date to";
$_['filter_txt_date']                            = "Date";

// filter status
$_['filter_status']                              = "Status";
$_['filter_status_placeholder']                  = "Select status";
$_['filter_status_delivering']                   = "Goods delivering";
$_['filter_status_receipted']                    = "Imported goods";

// filter manufacturer
$_['filter_manufacturer']                        = "Manufacturer";
$_['filter_manufacturer_placeholder']            = "Select manufacturer";

// checkbox
$_['text_options']                                = 'Choose action';
$_['txt_all']                                     = 'List';
$_['store_receipt_filter']                        = 'Store receipt filter';
$_['txt_receipted_checkbox']                      = 'Imported';
$_['txt_cancel_checkbox']                         = 'Cancel';

// popup result
$_['text_no_results']                             = 'No result!';
$_['text_manufacturer_code']                      = 'Code';
$_['text_manufacturer_result']                    = 'Result';
$_['text_manufacturer_reason']                    = 'Reason';
$_['text_failure']                                = 'failed';
$_['text_success']                                = 'successfully';
$_['txt_failure_receipt']                         = 'This receipt has been import before!';
$_['txt_success_receipt']                         = 'Import successfully!';
$_['txt_success_cancel']                          = 'Cancel successfully!';
$_['txt_note_view_detail']                        = 'View detail >';

// modal
$_['text_close']                                  = 'Close';
$_['txt_cancel']                                  = 'Skip';
$_['txt_confirm']                                 = 'Confirm';
$_['txt_heading_receipt']                         = 'Import goods';
$_['txt_receipt_content']                         = 'Are you sure import goods this receipt?';
$_['txt_heading_cancel']                          = 'Cancel';
$_['txt_cancel_content']                          = 'Are you sure cancel this receipt?';

// for manufacture form
// form
$_['text_form_save']                         = 'Save';
$_['text_form_cancel']                       = 'Cancel';
$_['text_form_contact_info']                 = 'Contact information';
$_['text_form_name']                         = 'Manufacture name';
$_['text_form_name_placeholder']             = 'Enter manufacturer name';
$_['text_form_phone']                        = 'Phone';
$_['text_form_phone_placeholder']            = 'Enter phone number';
$_['text_form_email']                        = 'Email';
$_['text_form_email_placeholder']            = 'Enter email';
$_['text_form_tax_code']                     = 'Tax code';
$_['text_form_tax_code_placeholder']         = 'Enter tax code';
$_['text_form_address']                      = 'Address';
$_['text_form_tax_address_placeholder']      = 'Enter address';
$_['text_form_province']                     = 'Province';
$_['text_form_tax_province_placeholder']     = 'Select province';
$_['text_form_district']                     = 'District';
$_['text_form_tax_district_placeholder']     = 'Select district';

// validate form
$_['text_error_form_name_empty']                     = 'Manufacturer name cannot be empty';
$_['text_error_form_name_more_255']                  = 'Manufacture name cannot exceed 255 characters';
$_['text_error_form_telephone_empty']                = 'Phone cannot be empty';
$_['text_error_form_telephone_start']                = 'Prefix must be 84 or 0!';
$_['text_error_form_telephone_wrong_format']         = 'Phone number is invalid.';
$_['text_error_form_telephone_more_15']              = 'Phone number cannot exceed 15 char!';
$_['text_error_form_email_wrong_format']             = 'Email is invalid!';

// form product

// validate
$_['error_name']             = 'Product name is too short or too long (1-100 characters)!';
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify stores!';
$_['error_meta_title']       = 'Title must be between 1 and 255 characters!';
$_['error_model']            = 'Product Model is 1 to 255 characters!';
$_['error_keyword']          = 'SEO URL has been used!';
$_['error_unique']           = 'SEO URL must be unique!';
$_['error_name_exist']             = 'Product name already exists!';
$_['error_sku_exist']             = 'Product SKU already exists!';
$_['error_product_version_limit']             = 'Product versions limit is 100.';
$_['error_product_attribute_name_exist']             = 'Attribute name already exists';
$_['error_has_discount']                             = 'Warning: product has already been in an activated Discount';
$_['error_edit_error_when_has_discount']             = 'Warning: could not update the product that has already been in an activated Discount';
$_['error_delete_error_when_has_discount']           = 'Warning: could not delete the product that has already been in an activated Discount';

// Error File
$_['error_permission'] = 'Warning: Request denied due to authority!';
$_['error_filename']   = 'Warning: The file name is from 3 to 255!';
$_['error_folder']     = 'Warning: Folder names are from 3 to 255!';
$_['error_exists']     = 'Warning: Directory already exists!';
$_['error_directory']  = 'Warning: The directory does not exist yet!';
$_['error_filesize']   = 'Warning: File size cannot exceed 1MB!';
$_['error_filetype']   = 'Warning: Only .xls and xlsx! formats are accepted!';
$_['error_upload']     = 'Warning: Unable to upload files to the system (unknown error)!';
$_['error_delete']     = 'Warning: This folder cannot be deleted!';
$_['error_limit']      = 'Warning: The maximum product number and product import version is 500!';
$_['error_not_found_fields']      = 'Warning: No title or description found!';
$_['error_file_incomplete']       = 'Warning: File is incomplete (missing columns)!';
$_['error_file_incomplete_in_row']       = 'Warning: Missing title, price, measurement_of_unit_prices, or promotion price greater than the price in line ';
$_['error_unknown']                      = 'Unknown error';
$_['error_no_file_selected']             = 'No files have been selected!';

$_['error_input_null']                 = 'Please fill out this field';
$_['error_input_price_null']           = 'Please enter price';
$_['error_input_product_name']         = 'Please fill out this field product name';
$_['error_type_mail']                  = 'Please enter the correct email format';
$_['error_max_length_255']             = 'Does not exceed 255 characters';
$_['error_max_length_15']              = 'Do not exceed 15 characters';
$_['error_max_length_12']              = 'Do not exceed 10 characters';
$_['error_type_phone']                 = 'Please enter a number for this field';
$_['error_max_length_10']              = 'Do not exceed 10 characters';
$_['error_max_length_100']             = 'Not exceeding 100 characters';
$_['error_max_length_30']             = 'Do not exceed 30 characters';
$_['error_max_length_20']             = 'Do not exceed 20 characters';
$_['error_max_length_48']             = 'Do not exceed 48 characters';
$_['error_type_barcode']             = 'Only allow alphanumeric input';
$_['error_type_sku']             =   'Do not enter Vietnamese accented';
$_['error_type_weight']         = "Product weight greater than or equal 1 gram and less than 100.000 gram";
$_['error_special_char']         = "Do not enter special characters";
$_['error_type_sku_same']             =   'SKU code already exists';
$_['error_promotion_price']           = 'Promotional price must be less than the original price';
$_['error_text_char']           = 'Do not enter special characters';

$_['notice_add_tag_error_max_length']   = "Tag could not be over 30 characters";

$_['error_product_price_must_be_greater_than']  = "The original product price must be greater than 0";
$_['error_product_price_must_be_greater_or_than_equal']  = "The original product price must be greater than or equal 0";
$_['error_product_promotion_price_must_be_greater_than']  = "Promotional product prices must be greater than 0";


$_['error_store']                       = "Please select store.";