<?php
// Heading
$_['heading_title']          = 'Product';
$_['heading_title_add']      = 'Add product';
$_['heading_title_edit']      = 'Edit product';

// Text
$_['text_success']           = 'Success: You have successfully updated the Product!';
$_['text_success_add']       = 'Add new product successfully!';
$_['text_success_edit']      = 'Edit product successfully!';
$_['text_list']              = 'Product List';
$_['text_add']               = 'Add product';
$_['general_info']           = 'General information';
$_['text_product_info']      = 'Product information';
$_['text_only_version']      = 'Product has one version';
$_['text_multi_version']     = 'Product has multiple versions';
$_['text_edit']              = 'Edit product';
$_['text_filter']            = 'Filter';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Default';
$_['text_option']            = 'Option';
$_['text_option_value']      = 'Option value';
$_['text_percent']           = 'Percent';
$_['text_amount']            = 'Fixed price';
$_['text_keyword']           = 'Do not use spaces, instead use - and ensure SEO URLs are unique throughout the system.';
$_['text_buy_when_out_of_stock']           = 'Allow to continue ordering when the product is out of stock';
$_['txt_image_preview']                    = 'Image preview';
$_['txt_image_alt']                        = 'Image ALT:';
$_['txt_image_set_thumb']                  = 'Set as product avatar';
$_['txt_warning_import_product']           = 'Notice: products have same sku or same name (without sku) will be overwritten!';
$_['txt_image_description']                = 'For the best displaying of product images, please choose a square image (suggested size: 500x500 pixel)';
$_['text_creator']                         = 'Creator';
$_['text_staff']                           = 'Staff';
// SEO
$_['text_seo_preview']       = 'Preview search results';
$_['text_seo_config']        = 'Customizing SEO';
$_['text_seo_guide']         = 'Enter a title and description to see how the article is displayed on the search list.';
$_['text_seo_title_exp']     = '3 most meaningful Christmas gifts for Christmas 14/12/2017';
$_['text_seo_alias_exp']     = 'Three-year-old-school-funeral-fun-ban-gai';
$_['text_seo_des_exp']       = 'Step 1: Access the website and select the product to buy for purchase Step 2: Click and the product you want to buy, the screen displays popup ...';
$_['txt_seo_title']                            = 'Title tag';
$_['txt_seo_title_place_holder']               = 'Enter article title';
$_['txt_seo_description']                      = 'Article content';
$_['txt_seo_description_place_holder']         = 'Add description';
$_['txt_seo_keywords_place_holder']            = 'Enter keywords here, each keyword separated by commas';
$_['txt_seo_alias']                            = 'Path / Alias';
$_['txt_characters']                       = 'character';
$_['txt_used']                             = 'Used: ';
$_['txt_seo_image_alt']                        = 'Add ALT tags to optimize SEO';
$_['text_options']           = 'Select action';
$_['text_show_product']      = 'Show product';
$_['text_hide_product']      = 'Hide product';
$_['text_delete_product']    = 'Delete product';
$_['text_add_tag']           = 'Add tag';
$_['text_remove_tag']        = 'Remove tag';
$_['text_add_collection']    = 'Add product to collection';
$_['text_delete_collection'] = 'Delete product from collection';
$_['text_add_product_attribute'] = 'Add another attribute';
$_['notice_remove_tag_try']  = "Error could not be done!";
$_['text_show_gg_product_feed'] = 'Show gg product feed';
// Column
$_['column_name']            = 'Product name';
$_['column_model']           = 'Model';
$_['column_image']           = 'Product picture ';
$_['column_image_add']       = 'Add product image';
$_['column_price_title']     = 'PRICE';
$_['column_price']           = 'Original price';
$_['column_price_des']       = 'As the original price of the product, it will be removed when there is a price after the promotion. Enter value equal 0 for CONTACT';
$_['column_price_promotion'] = 'Price after promotion';
$_['column_price_promotion_des'] = 'The final sale price of the product for customers to buy.';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';
$_['column_description']      = 'Detailed description';
$_['column_sub_description']  = 'Short description';
$_['column_barcode']          = 'Barcode (ISBN, UPC, v.v...)';
$_['column_barcode_sort']     = 'Barcode';
$_['column_quantity']         = 'Quantity in stock';
$_['column_display']          = 'Display';
$_['column_version']          = 'Version';

// Entry
$_['entry_name']             = 'Enter product name';
$_['entry_description']      = 'Enter content';
$_['entry_meta_title']       = 'Meta tag title';
$_['entry_meta_keyword']     = 'Meta tag keyword';
$_['entry_meta_description'] = 'Meta tag description';
$_['entry_store']            = 'Stores';
$_['entry_quantity_placeholder']         = 'Enter quantity';
$_['entry_keyword']          = 'Keyword';
$_['entry_model']            = 'Product Model';
$_['entry_sku']              = 'Product code/SKU';
$_['entry_sku_sort']         = 'SKU';
$_['entry_barcode']          = 'ISBN, UPC, v.v...';
$_['entry_upc']              = 'UPC';
$_['entry_ean']              = 'EAN';
$_['entry_jan']              = 'JAN';
$_['entry_isbn']             = 'ISBN';
$_['entry_mpn']              = 'MPN';
$_['entry_location']         = 'Location';
$_['entry_shipping']         = 'Shipping requirement';
$_['entry_manufacturer']     = 'Manufacturer';
$_['entry_date_available']   = 'Available product dale';
$_['entry_quantity']         = 'Quantity';
$_['entry_minimum']          = 'minimum number';
$_['entry_stock_status']     = 'Status out of stock';
$_['entry_price']            = 'Enter price';
$_['entry_tax_class']        = 'Tax calculation unit';
$_['entry_points']           = 'Point';
$_['entry_option_points']    = 'Point';
$_['entry_subtract']         = 'Subtract the quantity in stock';
$_['entry_weight_class']     = 'Volume Unit';
$_['entry_weight']           = 'Product weight';
$_['entry_weight_sub']       = '(Gram units)';
$_['entry_weight_des']       = 'Enter product volume';
$_['entry_dimension']        = 'Size (D x R x C)';
$_['entry_length_class']     = 'Length unit';
$_['entry_length']           = 'Length';
$_['entry_width']            = 'Width';
$_['entry_height']           = 'Height';
$_['entry_image']            = 'Image';
$_['entry_additional_image'] = 'Additional image';
$_['entry_customer_group']   = 'Customer group';
$_['entry_date_start']       = 'Start date';
$_['entry_date_end']         = 'End date';
$_['entry_priority']         = 'Priority';
$_['entry_attribute']        = 'Atribute';
$_['entry_attribute_name']   = 'Atribute name';
$_['entry_attribute_des']    = 'Attribute value (Values ​​separated by commas) ';
$_['entry_attribute_placeholder']    = 'Each tag is separated by commas (,)';
$_['entry_attribute_value']  = 'Value';
$_['entry_attribute_add']    = 'Add attribute';
$_['entry_attribute_group']  = 'Atribute group';
$_['entry_text']             = 'Text';
$_['entry_option']           = 'Option';
$_['entry_option_value']     = 'Option value';
$_['entry_required']         = 'Requirement';
$_['entry_status']           = 'Status';
$_['entry_status_publish']   = 'Display';
$_['entry_status_publish_short']   = 'Publish';
$_['entry_status_unpublish'] = 'Hide';
$_['entry_stock_in_stock']      = 'in stock';
$_['entry_status_out_of_stock'] = 'out of stock';
$_['entry_multi_versions_yes']  = 'yes';
$_['entry_multi_versions_no']   = 'no';
$_['entry_sort_order']       = 'Sort';
$_['entry_category']         = 'Category';
$_['entry_category_select']         = 'Select category';
$_['entry_category_select_explain']         = 'Add products to the catalog so it can be easily searched on your website.';
$_['entry_classify']         = 'Classify';
$_['entry_product_type']     = 'Product type';
$_['entry_collection']       = 'Add to product collection';
$_['entry_collection_placeholder']       = 'Search collection';
$_['entry_search_tag_placeholder']       = 'Search tag';
$_['entry_collection_selected']          = '• You will add products to the following collections:';
$_['entry_tag']              = 'attached tag';
$_['entry_tag_placeholder']  = 'Receive tag';
$_['entry_tag_des']          = 'Add products to the list so that it can be easily searched on your website.';
$_['entry_supplier']         = 'Supplier';
$_['entry_supplier_input']         = 'Enter supplier';
$_['entry_filter']           = 'Filter';
$_['entry_download']         = 'Downloads';
$_['entry_related']          = 'Related product';
$_['entry_reward']           = 'Reward points';
$_['entry_layout']           = 'Override Layout';
$_['entry_recurring']        = 'Information';
$_['entry_category_placeholder']       = 'Product type';
$_['entry_manufacturer_placeholder']       = 'Supplier';

// Help
$_['help_sku']               = 'Stock Keeping Unit';
$_['help_upc']               = 'Universal Product Code';
$_['help_ean']               = 'European Article Number';
$_['help_jan']               = 'Japanese Article Number';
$_['help_isbn']              = 'International Standard Book Number';
$_['help_mpn']               = 'Manufacturer Part Number';
$_['help_manufacturer']      = '(Auto)';
$_['help_minimum']           = 'Minimum order quantity';
$_['help_stock_status']      = 'Status displayed when out of stock';
$_['help_points']            = 'Number of points needed to purchase this item.';
$_['help_category']          = '(auto)';
$_['help_filter']            = '(auto)';
$_['help_download']          = '(auto)';
$_['help_related']           = '(auto)';
$_['help_tag']               = 'way by the sign ,';

// Error
$_['error_warning']          = 'Warning: Please double-check the content form for errors!';
$_['error_permission']       = 'Warning: You do not have permission to edit the Product!';
$_['error_name']             = 'Product name is too short or too long (1-255 characters)!';
$_['error_meta_title']       = 'Meta tag header is 1 to 255 characters long!';
$_['error_model']            = 'Product Model is 1 to 255 characters long!';
$_['error_keyword']          = 'SEO URL has been used!';
$_['error_unique']           = 'SEO URL must be unique!';
$_['error_price']            = 'Price cannot be greater than comparable price!';
$_['error_name_exist']             = 'Product name already exists!';
$_['error_sku_exist']             = 'Product SKU already exists!';
$_['error_product_version_limit']             = 'Product versions limit is 100.';
$_['error_product_attribute_name_exist']             = 'Attribute name already exists';
$_['error_has_discount']                             = 'Warning: product has already been in an activated Discount';
$_['error_has_discount_fail']                        = 'Fail: product has already been in an activated Discount';
$_['error_edit_error_when_has_discount']             = 'Warning: could not update the product that has already been in an activated Discount';
$_['error_delete_error_when_has_discount']           = 'Warning: could not delete the product that has already been in an activated Discount';

// Error File
$_['error_permission'] = 'Warning: Request denied due to authority!';
$_['error_filename']   = 'Warning: The file name is from 3 to 255!';
$_['error_folder']     = 'Warning: Folder names are from 3 to 255!';
$_['error_exists']     = 'Warning: Directory already exists!';
$_['error_directory']  = 'Warning: The directory does not exist yet!';
$_['error_filesize']   = 'Warning: File size cannot exceed 1MB!';
$_['error_filetype']   = 'Warning: Only .xls and xlsx! formats are accepted!';
$_['error_upload']     = 'Warning: Unable to upload files to the system (unknown error)!';
$_['error_delete']     = 'Warning: This folder cannot be deleted!';
$_['error_limit']      = 'Warning: The maximum product number and product import version is 500!';
$_['error_not_found_fields']      = 'Warning: No title or description found!';
$_['error_file_incomplete']       = 'Warning: File is incomplete (missing columns)!';
$_['error_file_incomplete_in_row']       = 'Warning: Missing title, price, measurement_of_unit_prices, or promotion price greater than the price in line ';
$_['error_unknown']                      = 'Unknown error';
$_['error_no_file_selected']             = 'No files have been selected!';

$_['error_input_null']                 = 'Please fill out this field';
$_['error_input_price_null']           = 'Please enter price';
$_['error_input_product_name']         = 'Please fill out this field product name';
$_['error_type_mail']                  = 'Please enter the correct email format';
$_['error_max_length_255']             = 'Does not exceed 255 characters';
$_['error_max_length_15']              = 'Do not exceed 15 characters';
$_['error_max_length_12']              = 'Do not exceed 10 characters';
$_['error_max_length_14']              = 'Do not exceed 11 characters';
$_['error_type_phone']                 = 'Please enter a number for this field';
$_['error_max_length_10']              = 'Do not exceed 10 characters';
$_['error_max_length_100']             = 'Not exceeding 100 characters';
$_['error_max_length_30']              = 'Do not exceed 30 characters';
$_['error_max_length_20']              = 'Do not exceed 20 characters';
$_['error_max_length_48']              = 'Do not exceed 48 characters';
$_['error_type_barcode']               = 'Only allow alphanumeric input';
$_['error_type_sku']                   =   'Do not enter Vietnamese accented';
$_['error_type_weight']                = "Product weight greater than or equal 1 gram and less than 9,999,999 gram";
$_['error_special_char']               = "Do not enter special characters";
$_['error_type_sku_same']              =   'SKU code already exists';
$_['error_promotion_price']            = 'Promotional price must be less than the original price';
$_['error_promotion_price2']            = 'Original price must be greater than the promotional price';
$_['error_text_char']                  = 'Do not enter special characters';

// Text file
$_['text_uploaded']              = 'Success!';
$_['text_directory']             = 'Success: Create directory successfully!!';
$_['text_delete']                = 'Success: The file or folder has been deleted!';
$_['text_product_uploaded']      = ' product has been updated.';


///text
$_['text_product_data']           = 'PRODUCT DATA';
$_['text_depot_mamagement']       = 'Warehouse management';
$_['text_input_number']           = 'Number of entries';
$_['text_weight_explain']         = 'Used to calculate the shipping page fee';
$_['text_attribute_explain']      = 'Add new properties to make the product more selective, such as size or color.';
$_['txt_product_title']           = 'Product';

//Text product list
$_['product_heading']               = 'Product list';
$_['product_list']                  = 'Product';
$_['product_category']              = 'Folder list';
$_['product_import_file']           = 'Import';
$_['product_export_file']           = 'Export';
$_['product_code']                  = 'Product code';
$_['product_price']                 = 'Price';
$_['product_quantity']              = 'Quantity';
$_['product_status']                = 'Status';
$_['product_date_modified']         = 'Updated date';
$_['product_action']                = 'Action';
$_['product_edit']                  = 'Edit';
$_['product_view']                  = 'View';
$_['product_delete']                = 'Delete product';
$_['product_copy']                  = 'Copy';
$_['product_filter']                = 'Product filter';
$_['product_type']                  = 'Type';
$_['product_filter']                = 'Product filter';
$_['product_search']                = 'Search product';
$_['product_all_status']            = 'All';
$_['product_filter_button']         = 'Search';
$_['product_cancel_button']         = 'Cancel';
$_['product_save_button']           = 'Confirm';
$_['product_delete_confirm']        = 'Do you want to delete?';
$_['product_confirm_show']          = 'Are you sure you want to delete ';
$_['product_confirm_choose']        = " selected product ?";
$_['product_text_list']             = 'Product';
$_['product_of_list']               = 'Product of list';
$_['product_of_version']            = 'Version';


$_['action_show_product']       = 'Show product';
$_['action_hidden_product']       = 'Hide product';
$_['action_add_tag_product']       = 'Add tag';
$_['action_remove_tag_product']       = 'Remove tag';
$_['action_add_collection_product']       = 'Add products to the collection';
$_['action_remove_collection_product']       = 'Delete product from collection';
$_['action_list_product']       = 'Choose action';
$_['action_filter']       = 'Filter';

$_['search_collection']  = 'Search collection';
$_['use_tag_description']  = 'Each tag is separated by commas (,)';
$_['search_collection']  = 'Search collection';
$_['label_add'] = 'Add';
$_['label_remove'] = 'Remove';
$_['label_delete'] = 'Delete';
$_['label_close'] = 'Close';
$_['product_to_collection']      = "products into collection";
$_['product_to_collection_after']            = "product into the following collections:";
$_['product_delete_collection']      = "product out of collection";
$_['you_will_add']             =   "You will ";
$_['tag_for_product_choose']   = "tag for selected products";
$_['list_tag_for_product']    = " The following tags for the product:";
$_['choose_from_list_tag']  = "Select from existing tags";
$_['notice_add_tag_success']  = "Add a tag to the product successfully";
$_['notice_remove_tag_success']  = "Remove tag from the product successfully";
$_['notice_add_tag_error']  = "You have not added any tags for the selected product";
$_['notice_add_collection_error']  = "You need to select at least 1 collection";
$_['notice_remove_collection_success']  =  "Remove the product from the collection successfully";
$_['notice_add_collection_success']  =  "Add product to the collection successfully";
$_['select2_notice_not_result']   =    "No match found";
$_['select2_notice_search']   =    "Search ...";
$_['select2_notice_load_more']   =    "Loading ...";

// export
$_['export_file_col_link']           = 'link';
$_['export_file_col_name']           = 'title';
$_['export_file_col_description']    = 'description';
$_['export_file_col_sub_description']    = 'sub_description';
$_['export_file_col_sku']            = 'id';
$_['export_file_col_barcode']        = 'global_trade_code';
$_['export_file_col_status']         = 'hide_show';
$_['export_file_col_stock_status']   = 'stock_status';
$_['export_file_col_quantity']       = 'quantity';
$_['export_file_col_manufacture']    = 'brand';
$_['export_file_col_category']       = 'google_product_category';
$_['export_file_col_type']           = 'product_type';
$_['export_file_col_weight']         = 'measurement_of_unit_prices';
$_['export_file_col_attributes']     = 'attribute';
$_['export_file_col_price']          = 'promotion_price';
$_['export_file_col_compare_price']  = 'price';
$_['export_file_col_cost_price']     = 'cost_price';
$_['export_file_col_collections']    = 'collections';
$_['export_file_col_image']          = 'image_link';
$_['export_file_col_image_alts']      = 'image_alts';
$_['export_file_col_additional_image_urls'] = 'additional_image_link';
$_['export_file_col_original_product']      = 'id_original_product';
$_['export_file_col_multi_versions']        = 'multi_versions';
$_['export_file_name']               = 'Product_list.xls';
$_['export_file_error']              = 'Export error file. Please try again or contact Bestme for assistance.';

// for import different language
$_['export_file_col_link_2']           = 'liên_kết';
$_['export_file_col_name_2']           = 'tiêu_đề';
$_['export_file_col_description_2']    = 'mô_tả';
$_['export_file_col_sub_description_2']    = 'mô_tả_ngắn';
$_['export_file_col_sku_2']            = 'id';
$_['export_file_col_barcode_2']        = 'mã_số_sản_phẩm_thương_mại_toàn_cầu';
$_['export_file_col_status_2']         = 'ẩn_hiện';
$_['export_file_col_stock_status_2']   = 'tình_trạng_còn_hàng';
$_['export_file_col_quantity_2']       = 'số_lượng';
$_['export_file_col_manufacture_2']    = 'thương_hiệu';
$_['export_file_col_category_2']       = 'danh_mục_sản_phẩm_của_google';
$_['export_file_col_type_2']           = 'loại_sản_phẩm';
$_['export_file_col_weight_2']         = 'đo_lường_định_giá_theo_đơn_vị';
$_['export_file_col_attributes_2']     = 'thuộc_tính';
$_['export_file_col_price_2']          = 'giá_ưu_đãi';
$_['export_file_col_compare_price_2']  = 'giá';
$_['export_file_col_cost_price_2']     = 'giá vốn';
$_['export_file_col_collections_2']    = 'bộ_sưu_tập';
$_['export_file_col_image_2']          = 'liên_kết_hình_ảnh';
$_['export_file_col_additional_image_urls_2'] = 'liên_kết_hình_ảnh_bổ_sung';
$_['export_file_col_original_product_2']      = 'id_sản_phẩm_gốc';
$_['export_file_col_multi_versions_2']        = 'nhiều_phiên_bản';

// validate tag
$_['notice_add_tag_error_max_length']   = "Tag does not exceed 30 characters";

// filter
$_['filter_list_follow']                         = "Filter products by";
$_['choose_filter']                              = "Select filter condition ...";
$_['filter_status']                              = "Display status";
$_['filter_status_placeholder']                  = "Select status";
$_['filter_manufacture']                         = "Supplier";
$_['filter_manufacture_placeholder']             = "Select supplier";
$_['filter_category']                            = "Product type";
$_['filter_category_placeholder']                = "Select product type";
$_['filter_tag']                                 = "Tagged with";
$_['filter_tag_placeholder']                     = "Enter tag";
$_['filter_collection']                          = "Collection";
$_['filter_collection_placeholder']              = "Select collection";
$_['filter_length_product']                      = "Number of products";
$_['filter_length_product_placeholder']          = "Select filter conditions";
$_['filter_length_product_greater']              = "Greater";
$_['filter_length_product_smaller']              = "Smaller";
$_['filter_length_product_equal']                = "Equal";
$_['filter_length_product_input_placeholder']    = "Enter quantity";
$_['filter_staff_placeholder']                   = "Select staff";
$_['filter_staff']                               = "Staff";

$_['add_tag_des']                                = 'Choose a tag for the product';
$_['remove_all_tag']                             = 'Delete all tags';
$_['do_you_want_copy']                              = 'Do you want to copy';
$_['choose']                                     = "Selected";
$_['item']                                       = "item";
$_['change_status_success']                      = "Change status successfully";
$_['change_status_error']                        = "Change status failed";
$_['add_tag_success']                      = "Add tag successfully";
$_['remove_tag_error']                        = "Error adding tags";
$_['txt_add']                                   = "Add";
$_['error_product_price_must_be_greater_than']  = "The original product price must be greater than 0";
$_['error_product_price_must_be_greater_or_than_equal']  = "The original product price must be greater than or equal 0";
$_['error_product_promotion_price_must_be_greater_than']  = "Promotional product prices must be greater than 0";
$_['select_all']                           = 'Select all';

//Upload
$_['button_upload_continue']                              = 'Upload';
$_['upload_product_list_title']                           = 'Upload the product list';
$_['upload_product_sample_text']                          = 'Download file sample product list upload';
$_['here']                                                = 'here';
$_['choose_file']                                                = 'Choose File';
$_['view_more']                       = 'View image';
$_['alert_delete']                            = "Delete product";
$_['remove_category_confirm_message']          = 'Are you sure you want to delete this product type ?';
$_['remove_manufacturer_confirm_message']          = 'Are you sure you want to delete this supplier ?';

$_['popup_cancel_title']  = "Discard the change information";
$_['popup_cancel_message']  = "Changes will be lost";
$_['popup_cancel_ok'] = "Ok";

$_['website']                           = "Store";
$_['choose_website']                    = "Choose a store";
$_['channel_text']                      = "Sales Channel";
$_['channel_website']                   = "Website";
$_['channel_offline']                   = "Store";
$_['channel_website_offline']           = "Website and Store";
$_['error_store']                       = "Please select at lest 1 store";

// Export
$_['button_export_continue']            = 'Export';
$_['txt_warning_export_product']        = 'Notice: too many products, please select a part to export!';
$_['error_select_product_part']         = 'Please select a part to export';
$_['txt_part_from']                     = 'From';
$_['txt_part_to']                       = 'to';

//form validate
$_['form_not_empty']                           = "Not empty";
$_['form_error_cost_price']                    = 'The cost price must be smaller than the original price';

// text storage receipt
$_['text_header_storage_receipt']                           = "Warehousing";
$_['text_storage_receipt']                                  = "Store";
$_['text_storage_receipt_initial']                          = "Inventory";
$_['text_cost_price']                                       = "Cost Price";
$_['text_storage_receipt_initial_for_product_version']      = "Total inventory";
$_['text_chose_store']                                      = "Chose store";
$_['text_default_store']                                    = "Default store";
$_['text_info_storage_receipt_initial']                     = "INVENTORY INFORMATION";
$_['text_can_sell']                                         = "Can sell";
$_['text_goods_are_coming']                                 = "Goods are coming";
$_['text_add_store']                                        = "Add store";
$_['text_detail_version']                                   = "Detail";

// text 2.3.2
$_['text_sort_desc_default_store']                           = "All orders arising from the website, POS, chatbot are allowed to subtract the product inventory from this warehouse";
$_['text_place_input_common_cost_price']                     = "Enter cost price";
$_['text_alert_change_cost_price']                           = "Cost of all versions will be updated to this price. Are you sure?";
$_['text_manufacturer_exist_and_pause_transaction']          = "Supplier already exists and pause transaction";

//v2.11.2
$_['txt_description']              = "Product description";
$_['txt_title_tab']                = "Tab title";
$_['txt_content_tab']              = "Content";
$_['txt_btn_delete_tab']           = "Delete tab";
$_['txt_btn_add_description_tab']  = "Add Product description tab";
$_['txt_placeholder_title_tab']                = "Enter a title that describes the tab";
$_['txt_placeholder_description_tab']          = "Enter Content";
$_['error_input_description_tab']          = "Please fill out this field product description tab";
$_['error_max_length_50']          = "Title tab from 1 to 50 characters !";
$_['delete_description_tab_success']          = "Xóa thành công";
$_['txt_max_item']          = "Only maximum 5 items";

$_['error_keyword_check_name']              = ' " name " violated the keyword';
$_['error_keyword_check_description']            = ' " description " violated the keyword';
$_['error_keyword_check_summary']      = ' " summary " violated the keyword';

$_['error_empty_image']                     = "Image not empty";
$_['error_empty_image_alt']                 = "Alt image not empty";
$_['error_empty_seo_title']                 = "Seo title not empty";
$_['error_empty_seo_desciption']            = "Seo description not empty";
$_['error_empty_meta_keyword']              = "Seo keywords not empty";

// new column excel
$_['export_file_col_general_information'] = 'general_information';
$_['export_file_col_ingredients'] = 'ingredients';
$_['export_file_col_main_uses'] = 'main_uses';
$_['export_file_col_user_manual'] = 'user_manual';
$_['export_file_col_meta_title'] = 'meta_title';
$_['export_file_col_meta_keyword'] = 'meta_keyword';
$_['export_file_col_tags'] = 'tags';

$_['enter_ingredient']                              = 'Enter ingredient';
$_['text_ingredient']                               = 'Ingredient';
$_['text_num_stt']                                  = 'No.';
$_['text_ingredient_code']                          = 'Code';
$_['text_ingredient_quantitative']                  = 'Quantitative';
$_['text_ingredient_unit']                          = 'Unit';