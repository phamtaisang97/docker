<?php
/**
 * Created by PhpStorm.
 * User: Microsoft Windows
 * Date: 19/06/2020
 * Time: 9:09 AM
 */
// Heading
$_['heading_title']                             = 'Cost adjustment receipt';
$_['heading_title_add']                         = 'Add receipt';
$_['heading_title_edit']                        = 'Edit receipt';
$_['heading_title_list']                        = 'List cost adjustment receipt';

// text
$_['text_version']                              = 'Version:';
$_['text_success_edit']                         = 'Edit cost adjustment receipt is successfully!';
$_['text_success_add']                          = 'Add cost adjustment receipt is successfully!';
$_['text_receipt_canceled']                     = 'Cacel receipt';

// Text
$_['text_add_list']                             = 'Add cost adjustment receipt';
$_['cost_adjustment_receipt_search']                 = 'Search adjustment receipt';
$_['cost_adjustment_receipt_filter']                 = 'Filter adjustment receipt';

$_['txt_cost_adjustment_receipt_code']               =  "Code recipt";
$_['txt_cost_adjustment_receipt_store']              =  "Adjustment warehouse";
$_['txt_cost_adjustment_receipt_date_created']       =  "Date created";
$_['txt_cost_adjustment_receipt_date']               =  "Date adjustment";
$_['txt_cost_adjustment_receipt_status']             =  "Status";
$_['txt_cost_adjustment_receipt_difference_amount']  =  "Difference amount";
$_['text_success_draft']                             = 'Successfully saved the draft!';
$_['text_success_cancel']                            = 'Successfully canceled the receipt!';

// filter
$_['text_filter_method']                          = 'Filter receipt by';
$_['method_filter']                               = 'Status';
$_['choose_filter']                               = 'Chose method';
$_['action_filter']                               = 'Filter';
$_['store_receipt_search']                        = 'Search adjustment receipt';
$_['text_created_at_filter']                      = 'Created at';
$_['text_status_filter']                          = 'Status';

// filter store take
$_['filter_cost_adjustment']                           = "Adjustment warehouse";
$_['filter_cost_adjustment_placeholder']               = "Chose adjustment warehouse";

// filter status
$_['filter_status']                              = "Status";
$_['filter_status_placeholder']                  = "Chose status";
$_['filter_status_draft']                        = "Draft";
$_['filter_status_apply']                        = "Applied";
$_['filter_status_canceled']                     = "Canceled";

// filter date
$_['filter_added_date']                       =  "Date adjustment";
$_['filter_added_date_placeholder']           =  "Chose date adjustment";

$_['filter_date_range']                          = "Date range";
$_['filter_created_at']                          = "Created at";
$_['filter_created_at_placeholder']              = "Date range";
$_['filter_created_at_today']                    = "Today";
$_['filter_created_at_this_week']                = "This week";
$_['filter_created_at_this_month']               = "This month";
$_['filter_created_at_option']                   = "Custome";
$_['filter_select_date_from']                    = "Date from";
$_['filter_select_date_to']                      = "Date to";
$_['filter_txt_date']                            = "Date";

// status description

$_['cost_adjustment_receipt_status_draft']       =  "Draft";
$_['cost_adjustment_receipt_status_apply']       =  "Applied";
$_['cost_adjustment_receipt_status_canceled']    =  "Canceled";

// entry
$_['entry_store']                               = 'Adjustment warehouse';
$_['entry_store_placeholder']                   = 'Chose adjustment warehouse';
$_['entry_product']                             = 'Product';
$_['entry_product_placeholder']                 = 'Chose product';
$_['entry_current_cost_price']                  = 'Current cost price';
$_['entry_adjustment_cost_price']               = 'Adjustment cose price';
$_['entry_reason']                              = 'Reason';
$_['entry_note']                                = 'Note';
$_['entry_note_placeholder']                    = 'Enter note';

// warning
$_['warning_store_select']                      = 'Please select a store before select products!';
$_['warning_change_store']                      = 'Replace the store will delete all information in the adjustment receipt and the cost of products in the current store will not change. Are you sure? ';
$_['warning_receipt_apply']                     = 'This action will change the cost of products in the stock. Are you sure to continue?';
$_['warning_receipt_cancel']                    = 'Are you sure?';
$_['warning_product_delete']                    = 'The product has been changed or deleted.';

// button
$_['btn_adjustment']                            = 'Adjustment';
$_['btn_draft']                                 = 'Draft';
$_['btn_receipt_cancel']                        = 'Canceled';

// error
$_['error_permission']                          = 'Warning: You have not permission to edit cost adjustment receipt!';
$_['error_data']                                = 'Warning: Data input not correct!';


// Select 2
$_['select_store_placeholder']                  =    "Chose warehouse";
$_['select2_notice_not_result']                 =    "No result";
$_['select2_notice_search']                     =    "Searching ...";
$_['select2_notice_load_more']                  =    "Loading ...";

// validate
$_['txt_select_at_least_1_product']             = "You must select at least one product!";