<?php
// Heading
$_['heading_title']          = 'Customer group';
$_['heading_title_list']     = 'Group list';

// Text
$_['text_success']      = 'Add new customer successfully!';
$_['text_del_success']  = 'Delete customer group successfully !';
$_['text_edit_success'] = 'Edit customer successfully!';
$_['text_del']          = 'Delete';
$_['text_list']         = 'Customer group';
$_['text_add']          = 'Add group';
$_['text_edit']         = 'Edit group';
$_['text_name']         = 'Group name';
$_['text_description']  = 'Description';
$_['text_close']        = 'Cancel';
$_['text_search_list']  = 'Search';
$_['text_customer']  = 'Customer';
$_['text_no_results']               = 'No results!';
//table
$_['text_customer_group_code']  = 'Customer group code';
$_['text_customer_group_name']  = 'Customer group name';
$_['text_number_of_customer']   = 'Number of customer';
$_['text_action']               = 'Action';


// form
$_['upload_group_customer_title']   = 'Add new group';
$_['edit_group_customer_title']     = 'Edit group';

// Entry
$_['entry_name']        = 'Enter group name';
$_['entry_description'] = 'Enter description';

//alert
$_['alert_delete']          = 'Warning';
$_['alert_message_del']     = 'Are you sure to delele this customer group? It will not be shown in customer information.';


// Error
$_['error_name_empty']         = "Customer group name could not be empty";
$_['error_name_max_length']    = "Could not be over 50 characters";
$_['error_name_isset']         = "Name has already existed";