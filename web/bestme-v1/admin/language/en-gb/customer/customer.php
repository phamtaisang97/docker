<?php
// Heading
$_['heading_title']             = 'Customer';
$_['heading_title_add']         = 'Add customer';
$_['heading_title_list']        = 'Customer list';
$_['heading_detail_title']      = 'Customer detail';

// Text
$_['text_success']              = 'Success: You have successfully updated Customers!'; 
$_['text_list']                 = 'Customer list';
$_['text_add']                  = 'Add customer';
$_['text_edit']                 = 'Edit customer information'; 
$_['text_default']              = 'Default';
$_['text_account']              = 'Customer information'; 
$_['text_password']             = 'Password';
$_['text_other']                = 'Other';
$_['text_affiliate']            = 'Marketing details'; 
$_['text_payment']              = 'Payment details';
$_['text_balance']              = 'Account';
$_['text_cheque']               = 'Cheque';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Bank transfer';
$_['text_history']              = 'History';
$_['text_history_add']          = 'Add history';
$_['text_transaction']          = 'Transaction';
$_['text_transaction_add']      = 'Add transaction '; 
$_['text_reward']               = 'Reward';
$_['text_reward_add']           = 'Reward';
$_['text_ip']                   = 'IP';
$_['text_option']               = 'Option';
$_['text_login']                = 'Log in to the store '; 
$_['text_unlock']               = 'Unlock account';
$_['text_lock']                 = 'Lock account';
$_['text_delete']               = 'Delete account';
$_['text_choose_action']        = 'Choose action';
$_['text_delete_success']       = 'Delete successfully!';
$_['text_delete_fail']          = 'Delete failed!';
$_['text_search_placehoder']    = 'Search by name';
$_['text_selected_all']         = 'Selected all';
$_['choose_in_filter']          = "Selected";
$_['item_in_filter']            = "item";
$_['choose_all_in_filter']      = "Selected all item in this page";
$_['text_import_list']          = "Import";
$_['text_export_list']          = "Export";
$_['text_filter_customer']      = "Filter customers";
$_['text_filter_by']            = "Filter by:";
$_['text_filter_condition_select']            = "Select filter condition...";
$_['text_filter_status']        = "Status";
$_['text_filter_group']         = "Customer group";
$_['text_filter']               = "Filter";
$_['text_search']               = "Search by name or code";
$_['text_warning']              = "Warning";
$_['text_cancel']               = "Cancel";
$_['text_confirm']              = "Confirm";
$_['text_choose_file']          = "Choose file";
$_['text_here']                 = "here";
$_['upload_customer_sample_text']                         = 'Download file sample product list upload';
$_['upload_customer_note']                                = 'Notice: Customer who has duplicate email or phone will be updated';
$_['upload_import_customer_list']                         = 'Import customer list';
$_['error_no_file_selected']                              = 'No files have been selected!';
$_['export_file_error']                                   = 'Export error file. Please try again or contact Bestme for assistance.';
$_['export_file_name']                                    = 'customer_list.xls';
$_['text_customer_active']                                = 'Active';
$_['text_customer_block']                                 = 'Blocked';
$_['text_warning_block']                                  = 'Are you sure to block this customer? Blocked customer can not log in to website and not be shown in customer list when creating order in Admin and POS. ';
$_['text_warning_unblock']                                = 'Are you sure to unblock this customer? After unblocking, customer can log in to website and be shown in customer list when creating order in Admin and POS. ';
$_['text_warning_delete']                                 = 'Only delete accounts that have not generated orders. Do you want to continue?';
$_['text_warning_file_size']                              = 'The file size should not be large than ';
$_['error_filetype']                                      = 'Only .xls and xlsx! formats are accepted!';
$_['error_upload']                                        = 'Unable to upload files to the system (unknown error)!';
$_['error_file_incomplete']                               = 'File is incomplete (missing columns)!';
$_['error_file_missing_columns']                          = 'Missing họ_và_tên, số_điện_thoại, thông_tin_địa_chỉ in line: ';
$_['error_file_error_columns']                            = ' in line: ';
$_['txt_success']                                         = 'Success!';
$_['txt_success_suffix']                                  = ' customers have been updated!';
$_['txt_not_success']                                     = 'Unsuccessful!';
$_['txt_block_success']                                   = 'Customer lock is successful!';
$_['txt_unblock_success']                                 = 'Customer unlock is successful!';
$_['txt_delete_success']                                  = 'Customer deleted is successful!';
$_['txt_customer_have_order']                             = 'Customer has orders!';

// button

$_['button_add_customer']       = 'Customer name';
$_['button_add_customer_address']       = 'Add customer address';

// tab
$_['tab_customer_info']         = 'Customer information';
$_['tab_customer_address']      = 'Customer address';


// Column
$_['column_code']               = 'Code';
$_['column_name']               = 'Customer name';
$_['column_email']              = 'E-Mail';
$_['column_customer_group']     = 'Customer group';
$_['column_status']             = 'Status';
$_['column_date_added']         = 'Added date';
$_['column_note']               = 'Note';
$_['column_description']        = 'Description';
$_['column_amount']             = 'Total';
$_['column_points']             = 'Points';
$_['column_ip']                 = 'IP';
$_['column_total']              = 'Total account';
$_['column_action']             = 'Action';
$_['column_customer_source']    = 'Customer source';
$_['column_customer_kind']      = 'Customer type';

// Entry
$_['entry_customer_group']      = 'Customer group';
$_['entry_fullname']            = 'Fullname';
$_['entry_firstname']           = 'First name';
$_['entry_lastname']            = 'Lastname';
$_['entry_email']               = 'Email';
$_['entry_order']               = 'Order';
$_['entry_number_of_order']     = 'Number of order';
$_['entry_order_recent']        = 'Latest order'; 
$_['entry_telephone']           = 'Phone number';
$_['entry_newsletter']          = 'Notification email';
$_['entry_status']              = 'Status';
$_['entry_approved']            = 'Approved';
$_['entry_safe']                = 'Safe';
$_['entry_password']            = 'Password';
$_['entry_confirm']             = 'Confirm';
$_['entry_company']             = 'Company';
$_['entry_address']             = 'Address';
$_['entry_address_2']           = '2nd address';
$_['entry_address_3']           = '3rd address';
$_['entry_city']                = 'City';
$_['entry_district']            = 'District';
$_['entry_postcode']            = 'Postcode';
$_['entry_country']             = 'Country';
$_['entry_zone']                = 'District / Town';
$_['entry_default']             = 'Default address';
$_['entry_affiliate']           = 'Marketing';
$_['entry_tracking']            = 'Tracking code';
$_['entry_website']             = 'WebSite';
$_['entry_commission']          = 'Commission (%)';
$_['entry_tax']                 = 'Tax code';
$_['entry_payment']             = 'Payment method '; 
$_['entry_cheque']              = 'Payer\'s Name Check';
$_['entry_paypal']              = 'Email PayPal account';
$_['entry_bank_name']           = 'Bank name';
$_['entry_bank_branch_number']  = 'ABA / BSB Number (Branch Number)';
$_['entry_bank_swift_code']     = 'SWIFT Code'; 
$_['entry_bank_account_name']   = 'Account name';
$_['entry_bank_account_number'] = 'Account Number';
$_['entry_comment']             = 'Comment';
$_['entry_description']         = 'Description';
$_['entry_amount']              = 'Total';
$_['entry_points']              = 'Points';
$_['entry_name']                = 'Customer name';
$_['entry_ip']                  = 'IP';
$_['entry_date_added']          = 'Created date';
$_['entry_customer_source']     = 'From website';
$_['entry_customer_kind']       = 'Normal';
$_['column_index']               = 'STT';
$_['entry_created']            = 'Registration Date';
$_['column_options']            = 'Options';

// Help
$_['help_safe']                 = 'Set to true to avoid this customer from being caught by the anti-fraud system';
$_['help_affiliate']            = 'Enable / Disable the customers ability to use the affiliate system.';
$_['help_tracking']             = 'The tracking code that will be used to track referrals.';
$_['help_commission']           = 'Percentage the affiliate receives on each order.';
$_['help_points']               = 'Use minus to remove points';

// Error
$_['error_warning']             = 'Warning: Please check the form carefully for errors!';
$_['error_permission']          = 'Warning: You do not have permission to modify customers!';
$_['error_exists']              = 'Warning: E-Mail Address is already registered!';
$_['error_phone_exists']        = 'Warning: Telephone is already registered!';
$_['error_phone']               = 'Telephone does not appear to be valid!!';
$_['error_firstname']           = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']            = 'Last Name must be between 1 and 32 characters!';
$_['error_email']               = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']           = 'Telephone must be between 3 and 32 characters!';
$_['error_password']            = 'Password must be between 4 and 20 characters!';
$_['error_confirm']             = 'Password and password confirmation do not match!';
$_['error_address_1']           = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']                = 'City must be less than 128 characters!';
$_['error_district']            = 'District must be less than 128 characters!';
$_['error_postcode']            = 'Postcode must be between 2 and 10 characters for this country!';
$_['error_country']             = 'Please select a country!';
$_['error_zone']                = 'Please select a region / state!';
$_['error_custom_field']        = '%s required!';
$_['error_tracking']            = 'Tracking Code required!';
$_['error_tracking_exists']     = 'Tracking code is being used by another affiliate!';
$_['error_cheque']              = 'Cheque Payee Name required!';
$_['error_paypal']              = 'PayPal Email Address does not appear to be valid!';
$_['error_bank_account_name']   = 'Account Name required!';
$_['error_bank_account_number'] = 'Account Number required!';

$_['error_input_null']                 = 'Please fill in this field';
$_['error_type_mail']                  = 'Please enter the correct email format';
$_['error_max_length_255']             = 'Does not exceed 255 characters';
$_['error_max_length_128']             = 'Does not exceed 128 characters';
$_['error_max_length_15']              = 'Do not exceed 15 characters';
$_['error_type_phone']                 = 'Please enter a number for this field';
$_['error_max_length_10']              = 'Do not exceed 10 characters'; 
$_['error_min_length_8']              = 'Please enter more than 8 characters';

// Text chart
$_['options']                            = 'Custom';
$_['monday']                             = 'Mon';
$_['tuesday']                            = 'Tue';
$_['wednesday']                          = 'Wed';
$_['thursday']                           = 'Thu';
$_['friday']                             = 'Fri';
$_['saturday']                           = 'Sat';
$_['sunday']                             = 'Sun';
$_['january']                            = 'January';
$_['february']                           = 'February';
$_['march']                              = 'March';
$_['april']                              = 'April';
$_['may']                                = 'May';
$_['june']                               = 'June';
$_['july']                               = 'July';
$_['august']                             = 'August';
$_['september']                          = 'September';
$_['october']                            = 'October';
$_['november']                           = 'November';
$_['december']                           = 'December';
$_['today']                              = 'Today';
$_['yesterday']                          = 'Yesterday';
$_['one_week']                           = '1 week';
$_['this_week']                          = 'This week';
$_['one_month']                          = '1 month';
$_['this_month']                         = 'This month';
$_['decimal_point']                      = ',';
$_['thousand_point']                     = '.';
$_['last_week']                          = 'Last week';
$_['last_month']                         = 'Last month';

// v2.7.3
// form
$_['text_general_information']                      = 'General information';
$_['text_full_name']                                = 'Full name';
$_['text_placeholder_full_name']                    = 'Enter full name';
$_['text_placeholder_telephone']                    = 'Phone number';
$_['text_placeholder_telephone']                    = 'Enter phone number';
$_['text_placeholder_email']                        = 'Enter emal';
$_['text_group_customer']                           = 'Customer group';
$_['text_chose_group_customer']                     = 'Choose customer group';
$_['text_province']                                 = 'Province';
$_['text_chose_province']                           = 'Choose province';
$_['text_district']                                 = 'District';
$_['text_chose_district']                           = 'Choose district';
$_['text_wards']                                    = 'Ward';
$_['text_chose_wards']                              = 'Choose ward';
$_['text_placeholder_address']                      = 'Enter address';
$_['text_advanced_information']                     = 'Advanced information';
$_['text_birthday']                                 = 'Birthday';
$_['text_chose_birthday']                           = 'Choose birthday';
$_['text_sex']                                      = 'Sex';
$_['text_sex_male']                                 = 'Male';
$_['text_sex_female']                               = 'Female';
$_['text_sex_other']                                = 'Other';
$_['text_enter_website']                            = 'Enter website';
$_['text_tax_code']                                 = 'Tax code';
$_['text_enter_tax_code']                           = 'Enter tax code';
$_['text_other_information']                        = 'Other information';
$_['text_staff_in_charge']                          = 'Staff in charge';
$_['text_chose_staff_in_charge']                    = 'Choose staff in charge';
$_['text_enter_note']                               = 'Enter note';
$_['text_select2_add_new']                          = 'Add new';
$_['text_form_cancel']                              = 'Cancel';
$_['text_form_save']                                = 'Save';
$_['text_choose_status']                            = 'Select status';
$_['text_edit']                                     = 'Edit';
$_['text_history_title']                            = 'order history';
//
$_['error_name_max_length']                             = "Name could not be over 50 characters";
$_['text_error_form_tax_code_more_16']                  = 'Tax code must not exceed 16 characters';
$_['text_error_form_tax_code_special_characters']       = 'Invalid tax identification number';

// Select 2
$_['select2_notice_not_result']   =    "No match found";
$_['select2_notice_search']       =    "Search ...";
$_['select2_notice_load_more']    =    "Load ...";

//
$_['error_full_name']                                   = 'First and last names are 1 to 50 characters long!';
$_['text_error_form_telephone_start']                   = 'Prefix must be 84 or 0!';
$_['text_error_not_found_address']                      = 'Not found address!';
$_['text_confirm_remove_address']                       = 'Are you sure you want to delete?';

//
$_['text_default_address']                                      = 'Default address';
$_['text_default_address_desc']                                 = '* The information below corresponds to the default address information (shipping address of this account).';
$_['text_detail_customer']                                      = 'Customer details';
$_['text_personal_information']                                 = 'Personal information';
$_['text_address_book']                                         = 'Address books';
$_['text_list_address']                                         = 'Address list';
$_['text_add_new_address']                                      = 'Add a new address';
$_['text_title_edit_address']                                   = 'Edit address';
$_['text_edit_address']                                         = 'Edit address';
$_['text_remove']                                               = 'Remove';
$_['text_edit_address']                                         = 'Edit';
$_['text_setting_default_address']                              = 'Set as default';
$_['text_form_setting_default_address']                         = 'Set as default address';
$_['text_form_add_address_success']                             = 'Add new address successfully!';
$_['text_form_edit_address_success']                            = 'Successful address update!';
$_['text_form_delete_address_success']                          = 'Address deleted successfully!';

// warning import
$_['warning_max_10_address']                          = 'maximum is 10 addresses';
$_['warning_invalid']                                 = 'invalid';
$_['warning_max_length_255']                          = 'does not exceed 255 characters';
$_['warning_max_length_50']                           = 'does not exceed 50 characters';
$_['warning_max_date']                                = 'cannot be greater than the current date';

//history
$_['column_history_index']                                = 'STT';
$_['column_history_order_code']                           = 'Order code';
$_['column_history_date_added']                           = 'Time set';
$_['column_history_total']                                = 'Order value';
$_['column_history_status']                               = 'Order status';
$_['column_history_refunds']                              = 'Return amount';