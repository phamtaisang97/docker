<?php
// Heading
$_['detail_product_title']    = 'Product details';
$_['detail_product_display']  = 'display';
$_['detail_product_name']     = 'product name';
$_['detail_product_des']      = 'Product Description';
$_['detail_product_price']    = 'Product price';
$_['detail_product_compare']  = 'Comparison price';
$_['detail_product_status']   = 'Product status';
$_['detail_product_discount'] = 'Promotion';
$_['detail_product_vote']     = 'Product evaluation';

// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";