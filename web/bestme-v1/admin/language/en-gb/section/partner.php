<?php
// Heading
$_['partner_title'] = 'partner';
// Content
$_['partner_header']          = 'display';
$_['partner_upload_title']    = 'upload image';
$_['partner_url_title']       = 'path (link URL)';
$_['partner_url_placeholder'] = 'tag link search link';
$_['partner_desc_title']      = 'Image description';
$_['partner_add']             = 'Add partner';

// text
$_['txt_partner_logo']                    = 'partner logo';
$_['txt_partner']                         = 'partner';
$_['txt_limit_per_line']                  = 'Number of impressions per row';
$_['txt_add_partner']                     = 'add partner';
$_['txt_seconds']                         = 'seconds';
$_['txt_select_image']                    = 'select image';
$_['txt_banner_image_size_note']          = 'size: 1440*350';
$_['txt_change']                          = 'change';
$_['txt_remove']                          = 'delete';
$_['txt_image_redirect_to']               = 'Photos linked to';
$_['txt_image_redirect_placeholder']      = 'Enter link';
$_['txt_banner_description']              = 'Description';
$_['txt_banner_description_place_holder'] = 'Enter description text';
$_['txt_setting']                         = "Setting";
$_['txt_auto_play_partner_slides']        = "Auto-play partner slides";
$_['txt_auto_repeat_partner_list']        = "Auto-repeat partner list";
$_['txt_page_switching_time']             = "Page switching time";
$_['txt_one_seconds']       = '1 seconds';
$_['txt_two_seconds']       = '2 seconds';
$_['txt_three_seconds']     = '3 seconds';
$_['txt_four_seconds']      = '4 seconds';
$_['txt_five_seconds']      = '5 seconds';
// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";
$_['txt_title_partner']        = "Title";