<?php
// Section
$_['tab_title_sections']               = 'Sections';
$_['section_title_header']             = 'header';
$_['section_title_slideshow']          = 'Slide show';
$_['section_title_list_product']       = 'product list';
$_['section_title_hot_product']        = 'hot product';
$_['section_title_best_sales_product'] = 'best sales product';
$_['section_title_best_views_product'] = 'best views product';
$_['section_title_product_detail']     = 'product detail';
$_['section_title_banner']             = 'Banner';
$_['section_title_partner']            = 'partner';
$_['section_title_blog']               = 'Blogs';
$_['section_title_footer']             = 'footer';

$_['section_discount_product']   = "Discount products";
$_['section_new_product']        = "New products";
$_['text_add_section']           = "Add new list";
$_['section_customized_content'] = "Customized content";
$_['section_ads_banner']         = "Ads banner";

// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";
$_['text_title_blog']          = "Title";
$_['txt_customize_layout']     = "Customize interface layout";
$_['txt_note_customize_layout']  = "Allows customizing the layout by dragging and dropping the sections below";

$_['txt_slide_show_banner']     = 'Slideshow banner 1, 2';
$_['txt_slide_show']            = 'Slideshow';
$_['txt_display_customize']     = 'Block display customize';
$_['txt_blocK_collection']      = 'Block collection';
$_['txt_blocK_collection_banner'] = 'Block collection banner';
$_['txt_block_product']         = 'Block products';
$_['txt_block_banner']          = 'Block banner';
$_['txt_block_banner2']         = 'Block banner 2';
$_['txt_block_partner']         = 'Block partner';
$_['txt_block_blog']            = 'Block blog';
$_['txt_href_back']             = 'Back';
$_['txt_slide_show_product_hot']  = "Slideshow & hot product";
$_['txt_block_rate']              = "Website reviews";

$_['txt_hot_product']             = 'Discount products';
$_['txt_new_product']             = 'New products';
$_['txt_best_sales_product']      = 'Best sales product';

// v3.6.1.2-adg
$_['txt_about_us']  = "Giới thiệu";
$_['txt_why_choose']  = "Ưu điểm & Lý do chọn chúng tôi";
$_['txt_service']  = "Dịch vụ";
$_['txt_intro_award']  = "Thành tích và giải thưởng";
$_['txt_block_projects']  = "Dự án";
$_['txt_block_news']  = "Tin tức";