<?php
// Heading
$_['list_product_title_1'] = 'product category';
$_['list_product_title_2'] = 'setting';
$_['list_product_title_3'] = 'Title';
$_['list_product_title_4'] = 'title';
$_['list_product_title_5'] = 'display list';
$_['list_product_title_6'] = 'edit menu';

// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";