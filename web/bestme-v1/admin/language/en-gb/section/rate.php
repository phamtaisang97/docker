<?php
// Heading
$_['rate_website_heading_title']                          = 'Website reviews';
$_['best_sales_product_setting_title']                    = 'settings';
$_['best_sales_product_setting_header']                   = 'Tiêu đề';
$_['best_sales_product_display_title']                    = 'HIỂN THỊ';
$_['best_sales_product_display_header_grid']              = 'Cài đặt lưới';
$_['best_sales_product_display_grid_quantity']            = 'Số lượng sản phẩm hiển thị';
$_['rate_setting_title']                    = 'Title';
$_['rate_setting_title_placeholder']        = 'Enter title';
$_['best_sales_product_setting_description_placeholder']  = 'Mô tả tiêu đề';

// text
$_['menu_text_homepage']     = 'Trang chủ';
$_['txt_collection_select']  = 'Chọn bộ sưu tập';
$_['txt_category_select']    = 'Chọn loại sản phẩm';
$_['txt_collection_edit']    = 'Sửa bộ sưu tập';
$_['txt_category_edit']      = 'Sửa loại sản phẩm';
$_['txt_auto_retrieve_data'] = 'Tự động lấy dữ liệu';
$_['txt_collection_option']  = 'Bộ sưu tập tuỳ chọn';
$_['txt_category_option']    = 'Loại sản phẩm tùy chọn';
$_['txt_num_display']        = 'Number of display per row';
$_['txt_num_display_mobile'] = 'Number of display per row on mobile';
$_['txt_row_num_display']        = 'Displayed row number';
$_['txt_row_num_display_mobile'] = 'Displayed row number on mobile';
$_['txt_rate_suffix']     = 'Review';
$_['txt_row_suffix']         = 'row';

$_['txt_auto_play_slides']        = "Auto play";
$_['txt_page_switching_time']     = "Page switching time";
$_['txt_auto_repeat_list']        = "Auto repeat";
$_['txt_one_seconds']       = '1 seconds';
$_['txt_two_seconds']       = '2 seconds';
$_['txt_three_seconds']     = '3 seconds';
$_['txt_four_seconds']      = '4 seconds';
$_['txt_five_seconds']      = '5 seconds';

// header
$_['text_touch_here_to_page']  = "Bấm để chọn trang thiết lập";
$_['text_home_page_page']      = "Trang chủ";
$_['text_category_page']       = "Danh sách sản phẩm";
$_['text_product_detail_page'] = "Chi tiết sản phẩm";
$_['text_contact_page']        = "Liên hệ";
$_['text_news_page']           = "Tin tức";
