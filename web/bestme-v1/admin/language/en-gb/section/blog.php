<?php
// Heading
$_['blog_title']        = 'Blogs';
$_['blog_setting']      = 'setting';
$_['blog_title_2']      = 'title';
$_['blog_display']      = 'display';
$_['blog_display_2']    = 'display list';
$_['blog_list_created'] = 'According to the created list';
$_['blog_choose']       = 'Self-selected posts';
$_['blog_setting_2']    = 'Grid settings';
$_['blog_display_3']    = 'Number of products displayed';

// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";

// text
$_['menu_text_homepage'] = 'Home';
$_['txt_collection_select'] = 'Select collection';
$_['txt_category_select'] = 'Select category';
$_['txt_collection_edit'] = 'edit collection';
$_['txt_auto_retrieve_data'] = 'Auto retrieve data';
$_['txt_collection_option'] = 'Optional collection';
$_['txt_category_option'] = 'Optional category';
$_['txt_num_display'] = 'Number of display per row';
$_['txt_num_display_mobile'] = 'Number of display per row on mobile';
$_['txt_row_num_display'] = 'Displayed row number';
$_['txt_row_num_display_mobile'] = 'Displayed row number on mobile';
$_['txt_blog_suffix'] = 'blog';
$_['txt_row_suffix'] = 'row';
$_['txt_title_placeholder'] = 'Enter title';