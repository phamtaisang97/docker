<?php
// Heading
$_['category_product_title']         = 'product category';
$_['category_product_title_setting'] = 'setting';
$_['category_product_title_display'] = 'display';


$_['category_product_txt_title']        = 'title';
$_['category_product_txt_display_list'] = 'display_list';
$_['category_product_txt_menu_edit']    = 'edit menu';

// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";