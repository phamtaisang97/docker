<?php
// Section
$_['tab_title_sections']             = 'category';
$_['section_title_list_product']     = 'product list';
$_['section_title_product_category'] = 'product category';
$_['section_title_banner']           = 'advertising banner';
$_['section_title_filter']           = 'filter';
$_['section_title_header']           = 'header';
$_['section_title_footer']           = 'footer';

$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";