<?php
// Left menu
$_['banner_title']           = 'Banner';
$_['banner_display']         = 'display';
$_['banner_img']             = 'image banner';
$_['banner_upload_img']      = 'Upload photos';
$_['banner_url']             = 'Path (link URL)';
$_['banner_url_placeholder'] = 'attach link or search link ';
$_['banner_des_img']         = 'Image description';
$_['banner_note_img']        = '* Current banner size: 320x120px';

// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";