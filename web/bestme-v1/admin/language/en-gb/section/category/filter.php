<?php
// Heading
$_['filter_title']         = 'Filter';
$_['filter_title_setting'] = 'setting';
$_['filter_display']       = 'display';

$_['filter_txt_title']           = 'title';
$_['filter_txt_display_list']    = 'Display list';
$_['filter_txt_select_property'] = 'Select properties';
$_['filter_txt_selected']        = 'Selected';

$_['filter_txt_supplier_filter']      = 'Filter by supplier';
$_['filter_txt_product_type_filter']  = 'Filter by product type';
$_['filter_txt_property_filter']      = 'Property filter';
$_['filter_txt_product_price_filter'] = 'Filter by product price';
$_['filter_txt_collection_filter']    = 'Filter by collection';
$_['filter_txt_tag_filter']           = 'Filter by tag';
$_['filter_txt_attribute_filter']     = 'Filter by attribute';

$_['filter_txt_supplier']      = 'supplier';
$_['filter_txt_product_type']  = 'Product type';
$_['filter_txt_property']      = 'Attribute';
$_['filter_txt_product_price'] = 'Product price';


$_['filter_txt_range']     = 'Price range';
$_['filter_txt_price_min'] = 'Lowest price';
$_['filter_txt_price_max'] = 'Maximum price';
$_['filter_txt_range_add'] = 'Add price range';

// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";

// v2.10.2-tuning
$_['txt_modal_title_attribute'] = '';
$_['txt_modal_body_attribute'] = '';
$_['attribute_name'] = '';
$_['attribute_status'] = '';
$_['attribute_value'] = '';
$_['text_no_results'] = 'No result!';
$_['btn_save'] = 'Save changes';
$_['btn_dismiss'] = 'Dismiss';