<?php
// Heading
$_['list_product_title']   = 'Danh sách sản phẩm';
$_['list_product_display'] = 'Hiển thị';

$_['txt_product_display_des'] = 'Số sản phẩm hiển thị tối đa trên page';

// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";