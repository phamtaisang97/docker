<?php
// Title
$_['footer_title'] = 'footer';
// Contact
$_['footer_contact']               = 'contact';
$_['footer_contact_more']          = 'address';
$_['footer_header']                = 'header';
$_['footer_header_placeholder']    = 'Enter title';
$_['footer_subscribe_placeholder'] = 'Sign up to receive promotions';
$_['footer_address']               = 'address';
$_['footer_address_placeholder']   = 'Enter address';
$_['footer_phone']                 = 'phone number';
$_['footer_phone_placeholder']     = 'Enter phone number';
$_['footer_email']                 = 'Email';
$_['footer_email_placeholder']     = 'Enter email';
$_['text_add_contact']             = 'Ather address';
$_['text_note_contact']            = 'This address only displays a fixed location in the footer';
$_['text_note_contact_more']       = 'Add up to 9 positions, display 3 positions / line';
// Link
$_['footer_link']       = 'link';
$_['footer_collection'] = 'collection';
$_['footer_quick_link'] = 'quick link';
$_['footer_product']    = 'product';
$_['footer_company']    = 'our company';
$_['footer_account']    = 'account';
$_['footer_custom']     = 'custom link';
$_['footer_link_title'] = 'title';
$_['footer_link_menu']  = 'display list';
$_['footer_edit_menu']  = 'edit menu';

// text
$_['txt_collection_display'] = 'collection display';
$_['txt_menu_display']       = 'display menu';
$_['txt_subscribe']          = 'subscribe';
$_['txt_collection_edit']    = 'edit collection';
$_['txt_menu_edit']          = 'Edit menu';
$_['txt_menu_add']           = 'Add menu';
$_['txt_social_network']     = 'social network link';

// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";