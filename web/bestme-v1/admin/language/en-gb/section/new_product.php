<?php
// Heading
$_['heading_title']                                = 'new product';
$_['best_sales_product_setting_title']             = 'settings';
$_['best_sales_product_setting_title_placeholder'] = 'best sales product';
$_['best_sales_product_setting_header']            = 'settings';
$_['best_sales_product_display_title']             = 'displays';
$_['best_sales_product_display_header_grid']       = 'grid setting';
$_['best_sales_product_display_grid_quantity']     = 'display grid quantity';
$_['new_product_setting_title']                    = 'Title';
$_['new_product_setting_title_placeholder']        = 'Enter title';
$_['new_product_setting_sub_title_placeholder']    = 'Enter sub title';


// text
$_['txt_collection_select']  = 'Select collection';
$_['txt_collection_edit']    = 'edit collection';
$_['txt_auto_retrieve_data'] = 'Auto retrieve data';
$_['txt_collection_option']  = 'Optional collection';
$_['txt_category_option']    = 'Optional category';
$_['txt_num_display']        = 'Number of display per row';
$_['txt_num_display_mobile'] = 'Number of display per row on mobile';
$_['txt_row_num_display']        = 'Displayed row number';
$_['txt_row_num_display_mobile'] = 'Displayed row number on mobile';
$_['txt_product_suffix']     = 'product';
$_['txt_row_suffix']         = 'row';

$_['txt_auto_play_slides']        = "Auto play";
$_['txt_page_switching_time']     = "Page switching time";
$_['txt_auto_repeat_list']        = "Auto repeat";
$_['txt_one_seconds']       = '1 seconds';
$_['txt_two_seconds']       = '2 seconds';
$_['txt_three_seconds']     = '3 seconds';
$_['txt_four_seconds']      = '4 seconds';
$_['txt_five_seconds']      = '5 seconds';

// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";