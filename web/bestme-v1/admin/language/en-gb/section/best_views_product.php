<?php
// Heading
$_['best_views_product_title']                     = 'most views product';
$_['best_views_product_setting_title']             = 'setting';
$_['best_views_product_setting_title_placeholder'] = 'most views product';
$_['best_views_product_setting_header']            = 'header';
$_['best_views_product_display_title']             = 'display';
$_['best_views_product_display_header_grid']       = 'Grid settings';
$_['best_views_product_display_grid_quantity']     = 'Number of products displayed';

// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";