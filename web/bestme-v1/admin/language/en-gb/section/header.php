<?php
// Heading
$_['header_title']               = 'header';
$_['header_title_2']             = 'header notification';
$_['header_show_home']           = 'Only displayed on the homepage';
$_['header_content']             = 'Message content';
$_['header_content_placeholder'] = 'Enter notification content';
$_['header_link']                = 'Link notification';
$_['header_link_placeholder']    = 'Enter link';
$_['header_logo']                = 'LOGO';
$_['header_img_logo']            = 'Upload photos';
$_['header_menu']                = 'Display menu';
$_['header_list_show']           = 'Display list';

// text
$_['txt_logo_edit']                       = 'edit Logo';
$_['txt_menu_edit']                       = 'Edit menu';
$_['txt_menu_add']                        = 'Add menu';
$_['txt_select_image']                    = 'Choose photo';
$_['txt_banner_image_size_note']          = 'Size:  $SIZE1*$SIZE2';
$_['txt_change']                          = 'Change';
$_['txt_remove']                          = 'Delete';
$_['txt_image_redirect_to']               = 'Photos linked to';
$_['txt_image_redirect_placeholder']      = 'Enter link';
$_['txt_banner_description']              = 'Description';
$_['txt_banner_description_place_holder'] = 'Enter description text';
$_['text_alt_of_image']                   = 'Alt of image';
$_['text_enter_alt_of_image']             = 'Enter alt of image';
$_['text_logo_height']                    = 'Logo height';

// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";