<?php
// Heading
$_['blog_category_title']          = 'List of articles';
$_['blog_category_setting_title']  = 'Settings';
$_['blog_category_display_title']  = 'Display';


$_['txt_blog_category_title']                  = 'Title';
$_['txt_blog_category_title_placeholder']      = 'Category';
$_['txt_blog_category_display_list']           = 'Display list';
$_['txt_blog_category_menu_edit']              = 'Edit menu';
