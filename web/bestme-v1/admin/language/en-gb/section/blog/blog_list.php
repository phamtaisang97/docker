<?php
// Heading
$_['blog_list_title']                  = 'List of articles';
$_['blog_list_display']                = 'Display';

$_['txt_blog_list_display_des']             = 'Maximum number of posts displayed on the page';
