<?php
// Heading
$_['latest_blog_title']                  = 'New post';
$_['latest_blog_setting_title']   = 'Settings';
$_['latest_blog_display_title']   = 'display';


$_['txt_latest_blog_title']                  = 'Title';
$_['txt_latest_blog_recent']                 = 'Latest';
$_['txt_latest_blog_display_list']           = 'Display list';
$_['txt_latest_blog_num_blog_display']       = 'Number of posts displayed';

