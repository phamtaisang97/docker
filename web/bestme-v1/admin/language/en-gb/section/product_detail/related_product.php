<?php
$_['product_heading_title']                        = 'Related products';
$_['best_sales_product_setting_title']             = 'Title';
$_['best_sales_product_setting_title_placeholder'] = 'Best_sales_product';
$_['best_sales_product_setting_header']            = 'header';
$_['best_sales_product_display_title']             = 'display';
$_['best_sales_product_display_header_grid']       = 'Grid settings';
$_['best_sales_product_display_grid_quantity']     = 'Number of products displayed';

// text
$_['txt_collection_select']  = 'Select collection';
$_['txt_collection_edit']    = 'Edit collection';
$_['txt_auto_retrieve_data'] = 'Automatically retrieve data';
$_['txt_collection_option']  = 'Optional collection';
$_['txt_num_display']        = 'Number of display';
$_['txt_num_display_mobile'] = 'Number of display on mobile';
$_['txt_product_suffix']     = 'Product';

// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";

$_['txt_auto_play_slides']        = "Auto play";
$_['txt_page_switching_time']     = "Page switching time";
$_['txt_auto_repeat_list']        = "Auto repeat";
$_['txt_one_seconds']       = '1 seconds';
$_['txt_two_seconds']       = '2 seconds';
$_['txt_three_seconds']     = '3 seconds';
$_['txt_four_seconds']      = '4 seconds';
$_['txt_five_seconds']      = '5 seconds';
