<?php
// Section
$_['section_title_related_product'] = 'related_product';
$_['section_title_header']          = 'header';
$_['section_title_footer']          = 'footer';

$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";