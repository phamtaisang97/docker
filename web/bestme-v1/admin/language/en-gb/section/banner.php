<?php
// Left menu
$_['banner_title']           = 'Advertising banner';
$_['banner_display']         = 'Display';
$_['banner_img']             = 'Banner image';
$_['banner_img']             = 'Banner image';
$_['banner_upload_img']      = 'Upload photos';
$_['banner_url']             = 'path(link URL)';
$_['banner_url_placeholder'] = 'attached link or search link';
$_['banner_des_img']         = 'Image description';
$_['banner_note_img0']       = '* Banner size in use: 1240x320px';
$_['banner_note_img1']       = '* Banner size in use: 280x1200px';
$_['banner_note_img2']       = '* Banner size in use: 1240x200px';

// text
$_['txt_select_image']                    = 'select image';
$_['txt_banner_image_size_note']          = 'Size: $SIZE1*$SIZE2';
$_['txt_change']                          = 'change';
$_['txt_remove']                          = 'Delete';
$_['txt_image_redirect_to']               = 'Photos linked to';
$_['txt_image_redirect_placeholder']      = 'Enter link';
$_['txt_banner_description']              = 'Alt for image';
$_['txt_banner_description_place_holder'] = 'Enter alt for image';
$_['text_detail']                         = 'Detailed instructions.';

// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";