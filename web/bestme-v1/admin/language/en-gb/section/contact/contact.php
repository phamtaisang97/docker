<?php
// Left menu
$_['contact_title']         = 'contact';
$_['contact_setting']       = 'Setting';
$_['contact_setting_title'] = 'setting';
$_['contact_display']       = 'display';


$_['txt_store']     = 'Store';
$_['txt_telephone'] = 'Telephone';
$_['txt_social']    = 'Social follow';
$_['txt_title']     = 'title';
$_['txt_content']   = 'content';

// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";