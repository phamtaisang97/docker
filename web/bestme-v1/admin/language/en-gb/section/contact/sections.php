<?php
// Section
$_['txt_title_header']   = 'header';
$_['txt_title_footer']   = 'footer';
$_['tab_title_sections'] = 'Sections';
$_['title_map']          = 'map';
$_['title_info']         = 'Store information';
$_['title_form']         = 'Contact form';

// text
$_['txt_enter_link_map']             = 'Enter google map link';
$_['txt_enter_link_map_placeholder'] = 'Enter link';
$_['txt_get_map_link']               = 'Access Google map';
$_['txt_get_map_link_title']         = 'To embed a map on the website, you access the Google map, select the location to press the share button, select the \'Embed/Embed\' tab and copy the paste HTML here';
$_['txt_email']                      = 'Email';
$_['txt_email_placeholder']          = 'Enter email address';
$_['txt_phone']                      = 'phone number';
$_['txt_phone_placeholder']          = 'txt_phone_placeholder';
$_['txt_address']                    = 'address';
$_['txt_address_placeholder']        = 'Enter address';
$_['txt_address_edit']               = 'Edit address';
$_['txt_title']                      = 'Title';
$_['txt_title_placeholder']          = 'Enter title text';
$_['txt_form_email']                 = 'Email archived data';
$_['txt_form_email_place_holder']    = 'Enter email address';

$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";