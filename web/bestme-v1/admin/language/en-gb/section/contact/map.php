<?php
// Left menu
$_['map_title']                       = 'map';
$_['map_setting']                     = 'setting';
$_['map_setting_title']               = 'title';
$_['map_setting_placeholder']         = 'map';
$_['map_display']                     = 'display';
$_['map_display_title']               = 'title';
$_['map_display_address_header']      = 'Enter the address shown on the map ';
$_['map_display_address_placeholder'] = 'Enter the address displayed on the map';
$_['map_display_address_help']        = '(Get the link in google map)';

// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";