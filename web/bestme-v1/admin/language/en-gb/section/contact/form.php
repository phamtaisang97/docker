<?php
// Left menu
$_['form_title']                  = 'form';
$_['form_setting']                = 'setting';
$_['form_setting_title']          = 'title';
$_['form_data_title']             = 'Data';
$_['form_setting_placeholder']    = 'data';
$_['form_display']                = 'Display';
$_['form_data_email_des']         = 'Customer data will flow to email';
$_['form_data_email_placeholder'] = 'user@gmail.com';

// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";