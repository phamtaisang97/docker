<?php
// Left menu
$_['banner_title']           = 'Banner';
$_['banner_display']         = 'Hiển thị';
$_['banner_img']             = 'Ảnh banner';
$_['banner_upload_img']      = 'Tải ảnh lên';
$_['banner_url']             = 'Đường dẫn(link URL)';
$_['banner_url_placeholder'] = 'Gắn link hoặc tìm kiếm';
$_['banner_des_img']         = 'Mô tả ảnh';
$_['banner_note_img']        = '* Kích thước banner đang dùng: 320x120px';

// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";