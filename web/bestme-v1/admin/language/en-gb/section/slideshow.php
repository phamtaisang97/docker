<?php
$_['slideshow_title_1'] = 'Slide show';
$_['slideshow_title_2'] = 'SETTING';
$_['slideshow_title_3'] = 'Slide transition time';
$_['slideshow_title_4'] = 'DISPLAY';
$_['slideshow_title_5'] = 'Add banner image';


$_['txt_slide_image']  = 'Slide image';
$_['txt_upload_image'] = 'Upload photos';
$_['txt_link']         = 'Path (Link Url)';
$_['txt_image_des']    = 'Image description';

$_['txt_use_image']    = 'Use image';
$_['txt_use_video']    = 'Use video';
$_['txt_video_link']   = 'Enter video link here';
$_['txt_video_play']   = 'When the video completes, the slideshow will be next';
$_['txt_video_start']  = 'Video will be played when clicking on it';
$_['txt_note']         = 'Note';
$_['txt_not_video']    = 'You must enter video url into slide number ';
$_['txt_video_correct']  = 'Video url is not correct for slide number ';

// text
$_['txt_seconds']                         = 'seconds';
$_['txt_select_image']                    = 'select image';
$_['txt_banner_image_size_note']          = 'Size: $SIZE1*$SIZE2';
$_['txt_change']                          = 'change';
$_['txt_remove']                          = 'Delete';
$_['txt_image_redirect_to']               = 'Photos linked to';
$_['txt_image_redirect_placeholder']      = 'Enter link';
$_['txt_slider_description']              = 'Alt for image';
$_['txt_slider_description_place_holder'] = 'Enter alt for image';
$_['text_detail']                         = 'Detailed instructions.';

// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";