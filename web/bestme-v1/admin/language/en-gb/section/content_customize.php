<?php
$_['heading_content_customize_title']       = 'Content customize';
$_['content_customize_title']               = 'Title';
$_['content_customize_title_placeholder']   = 'Enter title';
$_['content_customize_content']             = 'Content';
$_['content_customize_content_placeholder'] = 'Enter content';

// header
$_['text_touch_here_to_page']  = "Touch here to choose setting page";
$_['text_home_page_page']      = "Home Page";
$_['text_category_page']       = "Category Page";
$_['text_product_detail_page'] = "Product detail Page";
$_['text_contact_page']        = "Contact Page";
$_['text_news_page']           = "News Page";