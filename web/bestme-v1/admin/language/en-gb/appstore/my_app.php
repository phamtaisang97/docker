<?php

// Heading
$_['heading_title']                      = 'App Store';
$_['heading_title_expiration']           = 'App Expired';

// Text
$_['text_detail']                        = 'View Detail';
$_['text_expire']                        = 'Expiration';
$_['text_app_installed']                 = 'Installed applications';
$_['text_app_suggest']                   = 'Suggested applications';
$_['text_delete_app']                    = 'Delete app';
$_['text_alert_delete_app']              = "This action will erase all application data, except for the remaining days.
                                            Are you sure want to delete this application? This action cannot be undone!";
$_['text_alert_expiration']              = "Your application has expired. Please contact the hotline <strong>" . PRODUCTION_BRAND_PHONE . "</strong> for advice.";
$_['text_empty_app']                     = "You have not installed any applications";
$_['text_to_app_store']                  = "" . PRODUCTION_BRAND_NAME . " App Store";
$_['text_see_more_app']                  = "See more apps on App Store";
$_['text_free']                          = "Free";
$_['text_forever']                       = "Permanently";

// Action
$_['action_accept']                      = "Accept";
$_['action_skip']                        = "Skip";
