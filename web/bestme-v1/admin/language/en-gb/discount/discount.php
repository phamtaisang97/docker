<?php
// Heading
$_['heading_title']                              = 'Discount';
$_['heading_coupon_title']                       = 'Discount code';
$_['heading_setting_title']                      = 'Config Discount';
$_['heading_title_add']                          = 'Create Discount';
$_['heading_title_edit']                         = 'Edit Discount';

// text
$_['text_add']                                    = 'Create Discount';
$_['text_summary']                                = 'Summary';
$_['text_edit']                                   = 'Edit Discount';
$_['text_options']                                = 'Choose action';
$_['text_deleted_or_no_exist']                    = 'The Discount does not existed or was deleted!';
$_['text_discount_name']                          = 'Discount name';
$_['text_discount_code']                          = 'Discount code';
$_['text_auto_generate_discount_code']            = 'Generate code';
$_['text_discount_type']                          = 'Method';
$_['text_discount_type_select_placeholder']       = 'Choose method';
$_['text_discount_usage_limit']                   = 'Usage limit';
$_['text_no_limit']                               = 'No limit';
$_['text_description']                            = 'Description';
$_['text_time_apply']                             = 'Time apply';
$_['text_start_at']                               = 'Start at';
$_['text_setting_ent_time']                       = 'Setting End at';
$_['text_end_at']                                 = 'To time';
$_['text_no_end_at']                              = '--';
$_['text_discount_store']                         = 'Stores';
$_['text_discount_store_all']                     = 'Apply for all stores';
$_['text_discount_store_placeholder']             = 'Choose store';
$_['text_discount_order_source']                  = 'Sources';
$_['text_discount_order_source_all']              = 'Apply for all sources';
$_['text_discount_order_source_placeholder']      = 'Choose source';
$_['text_discount_customer_group']                = 'Customer group';
$_['text_discount_customer_all']                  = 'Apply for all Customers';
$_['text_summary_content_type_order']             = 'Minimum order value';
$_['text_summary_content_type_manufacturer']      = 'Manufacturers discount';
$_['text_summary_content_type_category']          = 'Categories discounted';
$_['text_summary_content_type_product']           = 'Products are discounted';
$_['text_order_value_from']                       = 'Order value from';
$_['text_order_value_discount']                   = 'Discount';
$_['text_add_condition']                          = 'Add condition';
$_['text_all_categories']                         = 'All categories';
$_['text_category']                               = 'Category';
$_['text_minimum_number']                         = 'Minimum number';
$_['text_maximum_number']                         = 'Maximum number';
$_['warning_choose_all']                          = 'This will erase all the information you have selected. Are you sure you want to continue?';
$_['txt_search_category']                         = 'Search category';
$_['txt_search_manufacturer']                     = 'Search manufacturer';
$_['txt_all_manufacturer']                        = 'All manufacturer';
$_['txt_manufacturer']                            = 'Manufacturer';
$_['txt_search_product']                          = 'Search product';
$_['txt_all_product']                             = 'All product';
$_['txt_product_name']                            = 'Product name';
$_['txt_contain']                                 = 'Contain';
$_['txt_version']                                 = 'version';
$_['txt_version_prefix']                          = 'Version';
$_['txt_origin_price']                            = 'Origin price';

// Heading List
$_['heading_title_list']                          = 'Discount';
$_['heading_title_add_list']                      = 'Create';

// Text List
$_['text_home']                                   = 'Homepage';
$_['text_add_list']                               = 'Create';
$_['txt_all']                                     = 'All';
$_['txt_active']                                  = 'Running';
$_['txt_scheduled']                               = 'Scheduled';
$_['txt_paused']                                  = 'Suspended';
$_['txt_deleted']                                 = 'Deleted';
$_['text_filter_method']                          = 'Filter Discount by method';
$_['choose_filter']                               = 'Choose method';
$_['method_filter']                               = 'Discount method';
$_['action_filter']                               = 'Filter';
$_['text_options']                                = 'Choose action';

//Text discount list
$_['discount_heading_list']                       = 'Discount';
$_['discount_search']                             = 'Search Discount';
$_['discount_filter']                             = 'Filter';
$_['text_no_results']                             = 'No result!';
$_['discount_delete']                             = 'Delete Discount';
$_['discount_delete_confirm']                     = 'Are you sure you want to delete?';
$_['discount_confirm_show']                       = 'Are you sure you want to delete';
$_['discount_confirm_show']                       = 'Are you sure you want to delete';
$_['discount_confirm_choose']                     = " selected discounts?";

// Entry List
$_['txt_discount_title']                          = 'Discount Name';
$_['entry_status']                                = 'Status';
$_['entry_usage_limit']                           = 'Usage limit';
$_['entry_times_used']                            = 'Used Times';
$_['entry_start_at']                              = 'Start Date';
$_['entry_end_at']                                = 'End Date';

// Select 2
$_['select2_notice_not_result']                   =  "No result!";
$_['select2_notice_search']                       =  "Search ...";
$_['select2_notice_load_more']                    =  "Loading ...";
$_['entry_category_placeholder']                  =  "Search category";

// Tab

// Validate
$_['validate_success']                            = "Success";
$_['validate_invalid_config']                     = "Config discount error";
$_['validate_same_product']                       = "Product already exists in a discount of this time period. Please change the product or application period.";
$_['validate_same_category']                      = "The category of product that existed in a discount of this time period. Please change the product type or application period.";
$_['validate_same_manufacturer']                  = "The manufacturer already exists in a discount of this time period. Please change provider or application period.";
$_['validate_same_order']                         = "A discount exists for the total order value during this period. Please change the time of application.";
$_['validate_unknown_error']                      = "";
$_['validate_name_empty']                         = "Please enter the discount name";
$_['validate_name_length_255']                    = "Do not exceed 255 characters";
$_['validate_special_char']                       = "Do not enter special characters";
$_['validate_code_empty']                         = "Please enter the discount code";
$_['validate_code_length_20']                     = "Do not exceed 20 characters";
$_['validate_discount_type_empty']                = "No method selected";
$_['validate_discount_end_at']                    = "Not selected earlier than start time";
$_['validate_discount_start_at']                  = "Not selected later than end time";
$_['validate_discount_store_empty']               = "No stores selected";
$_['validate_discount_order_source_empty']        = "No source orders selected";
$_['validate_discount_code_duplicate']            = "Discount code already in use";

// error
$_['error_permission']                           = "You do not have permission to edit the discount!";
$_['error_name']                                 = "Discount name must not exceed 255 characters!";
$_['error_name_exist']                           = "Discount name already exists!";
$_['error_code_exist']                           = "Discount code already exists!";
$_['error_discount_type_order_error_count']      = "Error: Quantity must be greater than 0";
$_['error_discount_type_order_error_value_0']    = "Order value must be greater than 0";
$_['error_discount_error_discount_value_0']      = "Discount value must be greater than 0";
$_['error_discount_quantity']                    = "Quantity must be greater than 0";
$_['error_discount_type_order_error_value_asc']  = "Order value must increase";
$_['error_discount_type_order_error_value_100']  = "Do not enter values greater than to 100%";
$_['error_amount_must_asc']                      = "The maximum number must be increase";
$_['error_max_more_min_quantity']                = "The maximum quantity must be greater than the minimum quantity";
$_['error_min_quantity_more_than_0']             = "The minimum number must be greater than 0";
$_['error_max_quantity_more_than_0']             = "Maximum number must be greater than 0";

// Text chart
$_['options']                            = 'Custom';
$_['monday']                             = 'Mon';
$_['tuesday']                            = 'Tue';
$_['wednesday']                          = 'Wed';
$_['thursday']                           = 'Thu';
$_['friday']                             = 'Fri';
$_['saturday']                           = 'Sat';
$_['sunday']                             = 'Sun';
$_['january']                            = 'January';
$_['february']                           = 'February';
$_['march']                              = 'March';
$_['april']                              = 'April';
$_['may']                                = 'May';
$_['june']                               = 'June';
$_['july']                               = 'July';
$_['august']                             = 'August';
$_['september']                          = 'September';
$_['october']                            = 'October';
$_['november']                           = 'November';
$_['december']                           = 'December';
$_['today']                              = 'Today';
$_['yesterday']                          = 'Yesterday';
$_['one_week']                           = '1 week';
$_['this_week']                          = 'This week';
$_['one_month']                          = '1 month';
$_['this_month']                         = 'This month';
$_['decimal_point']                      = ',';
$_['thousand_point']                     = '.';
$_['last_week']                          = 'Last week';
$_['last_month']                         = 'Last month';

// Feauture comming soon
$_['featured_comming_soon']                       = '* Coming soon...';

// discount setting
$_['txt_discount_setting']                       = 'Config Discount';
$_['txt_save_active']                            = 'Save and Active';
$_['txt_auto_apply_discount']                    = 'Auto apply Discount';
$_['txt_discount_combine']                       = 'Combine more Discounts';
$_['txt_discount_setting_note']                  = 'Notice: The config Discount is only applied for POS and Admin. For website and chatbot, the system will auto apply and combine Discounts.';
$_['txt_discount_save_fail']                     = 'Could not change Discount';
$_['txt_discount_save_success']                  = 'Discount is changed successfully';
$_['txt_active_success']                         = 'Discount actives successfully. Promotion information has been updated.';

// text button daterangepicker
$_['txt_cancel']                        = 'Cancel';
$_['txt_apply']                         = 'Save';

// text on popup create discount
$_['txt_create_new_discount']                        = 'Create new discount';
$_['txt_discount_description']                       = 'Customers will get automatic discount in their orders';
$_['txt_discount_code_description']                  = 'Customers will receive a discount if they enter this code at checkout';


// filter
$_['txt_active_filter']                          = 'Active Discount';
$_['txt_paused_filter']                          = 'Pause Discount';
$_['txt_heading_pause']                          = 'Stop discounts selected';
$_['txt_placehold__pause']                       = 'Describe the reason for stopping here';
$_['txt_heading_delete']                         = 'Delete discounts selected';
$_['txt_placehold__delete']                      = 'Enter reason for deleting the discount here';
$_['txt_confirm']                                = 'Confirm';

//note
$_['txt_note_pause']                              = 'Stopped the discount successfully.';
$_['txt_note_delete']                             = 'Delete successfully.';
$_['txt_note_pause_one']                          = 'The operation could not be performed because the discount has paused.';
$_['txt_note_active_one']                         = 'Discount currently active.';
$_['txt_note_active']                             = 'Active';
$_['txt_note_pause']                              = 'Pause';
$_['txt_note_discount_success']                   = ' discounts successfully.';
$_['txt_note_view_detail']                        = 'View detail';
$_['text_discount_result']                        = 'Action results';
$_['text_discount_error']                         = 'Errors';
$_['text_success']                                = 'Success';
$_['text_failure']                                = 'Failure';
$_['txt_exit']                                    = 'Exit';
$_['txt_pause_success']                           = 'Stop discount successfully.';

// filter method
$_['txt_filter_category']                         = 'Discount by product type';
$_['txt_filter_manufacture']                      = 'Discount by supplier';
$_['txt_filter_product']                          = 'Discount by each product';
$_['txt_filter_order']                            = 'Discount by total order value';

// warning
$_['warning_category_discount']                   = 'Note: All subcategories will be applied according to the promotional configuration of the parent category.';

$_['txt_coupon_search']                             = 'Search coupon';
$_['txt_coupon_title']                              = 'Coupon name';
$_['txt_error_name']                                = "Promotion name cannot be blank and must not exceed 128 characters!";
$_['txt_error_code']                                = "Promo code cannot be empty and cannot exceed 20 characters!";
$_['txt_chose_method']                              = "Chose method";
$_['txt_chose_method_value']                        = "Value";
$_['txt_chose_method_percent']                      = "Percent";
$_['txt_discount_value']                            = "Discount value";
$_['txt_discount_discount_value']                   = "Discount value not empty";
$_['txt_discount_max_percent']                      = "Must not be zero or exceed 100%";

//
$_['text_version']                          = 'Version : ';
$_['error_no_file_selected']                = 'No files have been selected!';
$_['error_filesize']                        = 'Warning: File size cannot exceed 1MB!';
$_['error_filetype']                        = 'Warning: Only .xls and xlsx! formats are accepted!';
$_['error_upload']                          = 'Warning: Unable to upload files to the system (unknown error)!';
$_['error_limit']                           = 'Warning: The maximum product number and product import version is 500!';
$_['error_file_incomplete']                 = 'Warning: File is incomplete (missing columns)!';
$_['error_not_found_fields']                = 'Warning: No coupon_code found!';
$_['error_file_incomplete_in_row']          = 'Warning: Missing coupon_code in line ';
$_['text_uploaded']                         = 'Success!';
$_['text_coupon_uploaded']                  = ' coupon updated.';
$_['text_import_list']                      = 'Import list';

$_['text_allow_with_discount']                      = 'Allow with discount';
$_['text_apply_for']                                = 'Apply for';