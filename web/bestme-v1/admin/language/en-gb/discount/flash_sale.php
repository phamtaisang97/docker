<?php
// Heading
$_['heading_title']                              = 'Flash Sale';
$_['heading_coupon_title']                       = 'Flash Sale code';
$_['heading_setting_title']                      = 'Config Flash Sale';
$_['heading_title_add']                          = 'Create Flash Sale';
$_['heading_title_edit']                         = 'Edit Flash Sale';

// text
$_['text_add']                                    = 'Create Flash Sale';
$_['text_summary']                                = 'Summary';
$_['text_edit']                                   = 'Edit Flash Sale';
$_['text_options']                                = 'Choose action';
$_['text_deleted_or_no_exist']                    = 'The Flash Sale does not existed or was deleted!';
$_['text_flash_sale_name']                          = 'Flash Sale name';
$_['text_flash_sale_code']                          = 'Flash Sale code';
$_['text_auto_generate_flash_sale_code']            = 'Generate code';
$_['text_flash_sale_type']                          = 'Method';
$_['text_flash_sale_type_select_placeholder']       = 'Choose method';
$_['text_flash_sale_usage_limit']                   = 'Usage limit';
$_['text_no_limit']                               = 'No limit';
$_['text_description']                            = 'Description';
$_['text_time_apply']                             = 'Time apply';
$_['text_start_at']                               = 'Start at';
$_['text_setting_ent_time']                       = 'Setting End at';
$_['text_end_at']                                 = 'To time';
$_['text_no_end_at']                              = '--';
$_['text_flash_sale_store']                         = 'Stores';
$_['text_flash_sale_store_all']                     = 'Apply for all stores';
$_['text_flash_sale_store_placeholder']             = 'Choose store';
$_['text_flash_sale_order_source']                  = 'Sources';
$_['text_flash_sale_order_source_all']              = 'Apply for all sources';
$_['text_flash_sale_order_source_placeholder']      = 'Choose source';
$_['text_flash_sale_customer_group']                = 'Customer group';
$_['text_flash_sale_customer_all']                  = 'Apply for all Customers';
$_['text_summary_content_type_order']             = 'Minimum order value';
$_['text_summary_content_type_manufacturer']      = 'Manufacturers flash_sale';
$_['text_summary_content_type_category']          = 'Categories flash_saleed';
$_['text_summary_content_type_product']           = 'Products are flash_saleed';
$_['text_order_value_from']                       = 'Order value from';
$_['text_order_value_flash_sale']                   = 'Flash Sale';
$_['text_add_condition']                          = 'Add condition';
$_['text_all_categories']                         = 'All categories';
$_['text_category']                               = 'Category';
$_['text_minimum_number']                         = 'Minimum number';
$_['text_maximum_number']                         = 'Maximum number';
$_['warning_choose_all']                          = 'This will erase all the information you have selected. Are you sure you want to continue?';
$_['txt_search_category']                         = 'Search category';
$_['txt_search_manufacturer']                     = 'Search manufacturer';
$_['txt_all_manufacturer']                        = 'All manufacturer';
$_['txt_manufacturer']                            = 'Manufacturer';
$_['txt_search_product']                          = 'Search product';
$_['txt_all_product']                             = 'All product';
$_['txt_product_name']                            = 'Product name';
$_['txt_contain']                                 = 'Contain';
$_['txt_version']                                 = 'version';
$_['txt_version_prefix']                          = 'Version';
$_['txt_origin_price']                            = 'Origin price';

// Heading List
$_['heading_title_list']                          = 'Flash Sale';
$_['heading_title_add_list']                      = 'Create';

// Text List
$_['text_home']                                   = 'Homepage';
$_['text_add_list']                               = 'Create';
$_['txt_all']                                     = 'All';
$_['txt_actived']                                 = 'Actived';
$_['txt_unactived']                               = 'Unactived';
$_['txt_scheduled']                               = 'Scheduled';
$_['txt_paused']                                  = 'Suspended';
$_['txt_deleted']                                 = 'Deleted';
$_['text_filter_method']                          = 'Filter Flash Sale by method';
$_['choose_filter']                               = 'Choose method';
$_['method_filter']                               = 'Flash Sale method';
$_['action_filter']                               = 'Filter';
$_['text_options']                                = 'Choose action';
$_['text_date_range']                             = 'Start date - End date';
$_['text_time_range']                             = 'Start time - End time';
$_['text_starting_time']                          = 'Starting time';
$_['text_ending_time']                            = 'Ending time';
$_['text_choose_starting_date']                   = 'Choose starting date';
$_['text_choose_ending_date']                     = 'Choose ending date';
$_['text_collection']                             = 'Collection';
$_['text_choose_collection']                      = 'Choose collection';

//Text flash_sale list
$_['flash_sale_heading_list']                       = 'Flash Sale';
$_['flash_sale_search']                             = 'Search Flash Sale';
$_['flash_sale_filter']                             = 'Filter';
$_['text_no_results']                             = 'No result!';
$_['flash_sale_delete']                             = 'Delete Flash Sale';
$_['flash_sale_delete_confirm']                     = 'Are you sure you want to delete?';
$_['flash_sale_confirm_show']                       = 'Are you sure you want to delete';
$_['flash_sale_confirm_show']                       = 'Are you sure you want to delete';
$_['flash_sale_confirm_choose']                     = " selected flash_sales?";

// Entry List
$_['txt_flash_sale_title']                          = 'Flash Sale Name';
$_['entry_status']                                = 'Status';
$_['entry_usage_limit']                           = 'Usage limit';
$_['entry_times_used']                            = 'Used Times';
$_['entry_start_at']                              = 'Start Date';
$_['entry_end_at']                                = 'End Date';

// Select 2
$_['select2_notice_not_result']                   =  "No result!";
$_['select2_notice_search']                       =  "Search ...";
$_['select2_notice_load_more']                    =  "Loading ...";
$_['entry_category_placeholder']                  =  "Search category";

// Tab

// Validate
$_['validate_success']                            = "Success";
$_['validate_invalid_config']                     = "Config flash_sale error";
$_['validate_same_product']                       = "Product already exists in a flash_sale of this time period. Please change the product or application period.";
$_['validate_same_category']                      = "The category of product that existed in a flash_sale of this time period. Please change the product type or application period.";
$_['validate_same_manufacturer']                  = "The manufacturer already exists in a flash_sale of this time period. Please change provider or application period.";
$_['validate_same_order']                         = "A flash_sale exists for the total order value during this period. Please change the time of application.";
$_['validate_unknown_error']                      = "";
$_['validate_name_empty']                         = "Please enter the flash_sale name";
$_['validate_name_length_255']                    = "Do not exceed 255 characters";
$_['validate_special_char']                       = "Do not enter special characters";
$_['validate_code_empty']                         = "Please enter the flash_sale code";
$_['validate_code_length_20']                     = "Do not exceed 20 characters";
$_['validate_flash_sale_type_empty']                = "No method selected";
$_['validate_flash_sale_end_at']                    = "Not selected earlier than start time";
$_['validate_flash_sale_start_at']                  = "Not selected later than end time";
$_['validate_flash_sale_store_empty']               = "No stores selected";
$_['validate_flash_sale_order_source_empty']        = "No source orders selected";
$_['validate_flash_sale_code_duplicate']            = "Flash Sale code already in use";

// error
$_['error_permission']                           = "You do not have permission to edit the flash_sale!";
$_['error_name']                                 = "Flash Sale name must not exceed 255 characters!";
$_['error_name_exist']                           = "Flash Sale name already exists!";
$_['error_code_exist']                           = "Flash Sale code already exists!";
$_['error_flash_sale_type_order_error_count']      = "Error: Quantity must be greater than 0";
$_['error_flash_sale_type_order_error_value_0']    = "Order value must be greater than 0";
$_['error_flash_sale_error_flash_sale_value_0']      = "Flash Sale value must be greater than 0";
$_['error_flash_sale_quantity']                    = "Quantity must be greater than 0";
$_['error_flash_sale_type_order_error_value_asc']  = "Order value must increase";
$_['error_flash_sale_type_order_error_value_100']  = "Do not enter values greater than to 100%";
$_['error_amount_must_asc']                      = "The maximum number must be increase";
$_['error_max_more_min_quantity']                = "The maximum quantity must be greater than the minimum quantity";
$_['error_min_quantity_more_than_0']             = "The minimum number must be greater than 0";
$_['error_max_quantity_more_than_0']             = "Maximum number must be greater than 0";
$_['error_collection_missing']                   = "Collection missing";
$_['error_collection_not_exists']                = "Collection already exists";
$_['error_starting_date_missing']                = "Starting date missing";
$_['error_starting_date_wrong_format']           = "Starting date format is wrong";
$_['error_ending_date_missing']                  = "Ending date missing";
$_['error_ending_date_wrong_format']             = "Ending date format is wrong";
$_['error_starting_time_missing']                = "Starting time missing";
$_['error_starting_time_wrong_format']           = "Starting time format is wrong";
$_['error_ending_time_missing']                  = "Ending time missing";
$_['error_ending_time_wrong_format']             = "Ending time format is wrong";
$_['error_status_wrong_format']                  = "Status format is wrong";

// Text chart
$_['options']                            = 'Custom';
$_['monday']                             = 'Mon';
$_['tuesday']                            = 'Tue';
$_['wednesday']                          = 'Wed';
$_['thursday']                           = 'Thu';
$_['friday']                             = 'Fri';
$_['saturday']                           = 'Sat';
$_['sunday']                             = 'Sun';
$_['january']                            = 'January';
$_['february']                           = 'February';
$_['march']                              = 'March';
$_['april']                              = 'April';
$_['may']                                = 'May';
$_['june']                               = 'June';
$_['july']                               = 'July';
$_['august']                             = 'August';
$_['september']                          = 'September';
$_['october']                            = 'October';
$_['november']                           = 'November';
$_['december']                           = 'December';
$_['today']                              = 'Today';
$_['yesterday']                          = 'Yesterday';
$_['one_week']                           = '1 week';
$_['this_week']                          = 'This week';
$_['one_month']                          = '1 month';
$_['this_month']                         = 'This month';
$_['decimal_point']                      = ',';
$_['thousand_point']                     = '.';
$_['last_week']                          = 'Last week';
$_['last_month']                         = 'Last month';

// Feauture comming soon
$_['featured_comming_soon']                       = '* Coming soon...';

// flash_sale setting
$_['txt_flash_sale_setting']                       = 'Config Flash Sale';
$_['txt_save_active']                            = 'Save and Active';
$_['txt_auto_apply_flash_sale']                    = 'Auto apply Flash Sale';
$_['txt_flash_sale_combine']                       = 'Combine more Flash Sales';
$_['txt_flash_sale_setting_note']                  = 'Notice: The config Flash Sale is only applied for POS and Admin. For website and chatbot, the system will auto apply and combine Flash Sales.';
$_['txt_flash_sale_save_fail']                     = 'Could not change Flash Sale';
$_['txt_flash_sale_save_success']                  = 'Flash Sale is changed successfully';
$_['txt_active_success']                         = 'Flash Sale actives successfully. Promotion information has been updated.';

// text button daterangepicker
$_['txt_cancel']                        = 'Cancel';
$_['txt_apply']                         = 'Save';

// text on popup create flash_sale
$_['txt_create_new_flash_sale']                        = 'Create new flash_sale';
$_['txt_flash_sale_description']                       = 'Customers will get automatic flash_sale in their orders';
$_['txt_flash_sale_code_description']                  = 'Customers will receive a flash_sale if they enter this code at checkout';


// filter
$_['txt_active_filter']                          = 'Active Flash Sale';
$_['txt_paused_filter']                          = 'Pause Flash Sale';
$_['txt_heading_pause']                          = 'Stop flash_sales selected';
$_['txt_placehold__pause']                       = 'Describe the reason for stopping here';
$_['txt_heading_delete']                         = 'Delete flash_sales selected';
$_['txt_placehold__delete']                      = 'Enter reason for deleting the flash_sale here';
$_['txt_confirm']                                = 'Confirm';

//note
$_['txt_note_pause']                              = 'Stopped the flash_sale successfully.';
$_['txt_note_delete']                             = 'Delete successfully.';
$_['txt_note_pause_one']                          = 'The operation could not be performed because the flash_sale has paused.';
$_['txt_note_active_one']                         = 'Flash Sale currently active.';
$_['txt_note_active']                             = 'Active';
$_['txt_note_pause']                              = 'Pause';
$_['txt_note_flash_sale_success']                   = ' flash_sales successfully.';
$_['txt_note_view_detail']                        = 'View detail';
$_['text_flash_sale_result']                        = 'Action results';
$_['text_flash_sale_error']                         = 'Errors';
$_['text_success']                                = 'Success';
$_['text_failure']                                = 'Failure';
$_['txt_exit']                                    = 'Exit';
$_['txt_pause_success']                           = 'Stop flash_sale successfully.';

// filter method
$_['txt_filter_category']                         = 'Flash Sale by product type';
$_['txt_filter_manufacture']                      = 'Flash Sale by supplier';
$_['txt_filter_product']                          = 'Flash Sale by each product';
$_['txt_filter_order']                            = 'Flash Sale by total order value';

// warning
$_['warning_category_flash_sale']                   = 'Note: All subcategories will be applied according to the promotional configuration of the parent category.';

$_['txt_coupon_search']                             = 'Search coupon';
$_['txt_coupon_title']                              = 'Coupon name';
$_['txt_error_name']                                = "Promotion name cannot be blank and must not exceed 128 characters!";
$_['txt_error_code']                                = "Promo code cannot be empty and cannot exceed 20 characters!";
$_['txt_chose_method']                              = "Chose method";
$_['txt_chose_method_value']                        = "Value";
$_['txt_chose_method_percent']                      = "Percent";
$_['txt_flash_sale_value']                            = "Flash Sale value";
$_['txt_flash_sale_flash_sale_value']                   = "Flash Sale value not empty";
$_['txt_flash_sale_max_percent']                      = "Must not be zero or exceed 100%";
