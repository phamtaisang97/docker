<?php
// Heading
$_['heading_title']             = 'Google Shopping';

// Text
$_['text_online_store']                 = 'Website';

$_['text_intro_title']                  = 'Connect to Novaon Autoads';
$_['text_intro_content']                = 'To use features from Google (Google Shopping), you need register an account in Novaon Autoads. Advertising Management Platform Autoads will help you manage all your shopping data from Google. View guide detail.';
$_['text_intro_button']                 = 'Connect Autoads Now';
$_['text_detail']                         = 'Detailed instructions.';
$_['text_verify_title']                 = 'Verify Website';
$_['text_verify_content']               = 'For using feature (Google Shopping), your store must be verified by Google. ' . PRODUCTION_BRAND_NAME . ' uses the way as inserting Meta tag to website.';
$_['text_verify_meta_title']            = 'Meta tag from Google';
$_['text_verify_meta_placeholder']      = '&lt;meta name=&quot;google-site-verification&quot; content=&quot;XXXXXXXXXXXXXXXXX&quot;&gt;';
$_['text_verify_meta_example']          = 'Example: &lt;meta name=&quot;google-site-verification&quot; content=&quot;XXXXXXXXXXXXXXXXX&quot;&gt;';
$_['text_verify_meta_button']           = 'Save';

$_['text_api_key_title']                = 'Customer code';
$_['text_api_key_content']              = 'All Apps, that want to call to your website, need these codes. Please provide two codes to Autoads to use Google Shopping feature.';
$_['text_api_key_site_key_title']       = 'Customer code';
$_['text_api_key_secure_key_title']     = 'Customer secure code';
$_['text_api_key_copy_button']          = 'Copy';

$_['text_api_key_copy_success']         = 'Copied';
$_['text_api_key_copy_failed']          = 'Error! Could not copy';

// error form
$_['success_change_meta']               = 'Update the meta tag value successfully'; 
$_['error_change_meta']                 = 'Error: updating meta tag value failed, please try again'; 
