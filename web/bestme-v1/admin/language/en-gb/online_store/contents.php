<?php
// Heading
$_['heading_title']         = 'Content page';
$_['add_heading_title']     = 'Add content page';
$_['edit_heading_title']     = 'Edit content page';

// Text
$_['breadcrumb_setting']        = 'Setting';
$_['breadcrumb_blog']           = 'Blog';
$_['breadcrumb_contents']       = 'Content page';
$_['breadcrumb_add_contents']   = 'Add content';
$_['breadcrumb_edit_contents']  = 'Edit content';

$_['txt_contents_list']                    = 'List';
$_['txt_contents_filter']                  = 'Filter content';
$_['txt_filter_status']                    = 'Status';
$_['txt_filter_status_all']                = 'All';
$_['txt_filter_status_show']               = 'Show';
$_['txt_filter_status_hide']               = 'Hide';
$_['txt_filter_status_search']             = 'Search';
$_['txt_search_place_holder']              = 'Search content page';

$_['txt_select_action']                    = 'Select action';
$_['txt_action_show']                      = 'Show';
$_['txt_action_hide']                      = 'Hide';
$_['txt_action_delete']                    = 'Delete page';

//alert
$_['text_delete_success']                 = 'Delete successfully!';
$_['text_delete_fail']                    = 'Delete failed!';
$_['text_confirm_change_status']          = 'Do you want to change the status of everything!!';
$_['text_change_status_success']          = 'Change status successfully!';
$_['text_change_status_fail']             = 'Change status failed!';

$_['txt_title']                            = 'Title';
$_['txt_title_place_holder']               = 'Enter title'; 
$_['txt_description']                      = 'Content';
$_['txt_description_place_holder']         = 'Add description';
$_['txt_alias']                            = 'Path / Alias';
$_['txt_preview']                          = 'Preview search resutls'; 
$_['txt_custom_seo']                       = 'Customize SEO';
$_['txt_custom_seo_des']                   = 'Enter a title and description to see how the post is displayed on the search list.';
$_['txt_date_updated']                     = 'Updated date';
$_['txt_date_publish']                     = 'Choose publish date';
$_['txt_characters']                       = 'Characters';
$_['txt_used']                             = 'Used';
$_['txt_seo_preview_title_demo']           = '3 most meaningful Christmas gifts for Christmas 14/12/2017';
$_['txt_seo_preview_description_demo']     = 'Step 1: Access the website and select the product to buy for purchase Step 2: Click and the product you want to buy, the screen will show popup ...';
$_['txt_seo_preview_alias_demo']           = 'three most meaningful gifts for girlsi';
$_['txt_preview_on_website']               = 'View on website';
$_['txt_selected']                         = 'Selected';
$_['txt_item']                             = 'item';

// button
$_['contents_form_button_save']                 = 'Save change';
$_['contents_form_button_cancel']               = 'Ignore';

// Error
$_['error_warning']             = 'Warning: Please check the form carefully for errors!';
$_['error_permission']          = 'Warning: You do not have permission to edit the Content Page!';
$_['error_name']                = 'Warning: The content page title is invalid, too short or too long (1-256 characters)!';

$_['text_success_add']           = 'Add new content successfully';
$_['text_success_edit']           = 'Edit content successfully';
$_['error_check_source']           = 'Unable to change synced ';