<?php
// Heading
$_['heading_title']             = 'Domain settings'; 

// Text
$_['txt_domain']                        = 'Domain name';
$_['txt_main_domain']                   = 'Main domain';
$_['txt_status']                        = 'Status'; 
$_['txt_status_connected']              = 'Connected';
$_['txt_status_pending']                = 'Pending';
$_['txt_status_not_auth']               = 'Not authenticated';
$_['txt_domain_management']             = 'Domain management';
$_['txt_domain_management_des']         = 'List of domain names. You can delete the domain name from your store.'; 
$_['txt_main_domain_config']            = 'Configure primary domain'; 
$_['txt_main_domain_config_des']        = 'The domain name is the domain name displayed on the Google search engine, and also the domain name that users use to access your website..';
$_['txt_redirect_all_to_domain']        = 'Redirect all access to this domain'; 
$_['txt_redirect_all_to_domain_des']    = 'Redirect access from all your domains to a single main domain'; 
$_['txt_add_domain']                    = 'Add domain name';
$_['txt_delete_domain']                 = 'Delete domain name'; 
$_['txt_delete_domain_alert_1']           = 'Are you sure you want to delete the domain name “';
$_['txt_delete_domain_alert_2']           = '”?  After this action will not be restored. ';
$_['txt_domain_auth']                   = 'Authenticate domain names ';
$_['txt_domain_auth_des_1']             = 'The authentication process will take place after you press the "Validate domain name" button.';
$_['txt_domain_auth_des_2']             = 'In order for the authentication process to succeed, you need to visit the domain administrator page, go to the DNS Management / Configuration section. Here, you need to create 2 records:'; 
$_['txt_domain_auth_des_3']             = '<strong>Record %s</strong><br>
                                            Host record: %s<br>
                                            Type: %s<br>
                                            Value: %s';
$_['cname_record']                      = 'CNAME';
$_['cname_host_record']                 = 'www';
$_['cname_type']                        = 'CNAME';
$_['a_record']                      = 'A';
$_['a_host_record']                 = '@ or leave blank';
$_['a_type']                        = 'A';
$_['txt_domain_auth_des_4']             = 'After you save it successfully, you come back here and verify the domain name.';
$_['txt_domain_sub_domain_note']        = '<hr>
                                        <i>If your domain name is a sub-domain of the primary domain, for example: "your-shop.your-company.com",
                                        You need to create the following CNAME record instead of the above CNAME record:</i><br>
                                        <b>Record CNAME</b><br>
                                        Host record: www.your-shop<br>
                                        Type: CNAME<br>
                                        Value: $BESTME_DOMAIN<br>>';
$_['txt_wait']                          = 'Please wait a moment!'; 
$_['txt_domain_being_auth']             = 'Domain name being validated ...';
$_['txt_domain_auth_alert']             = 'A Record and CNAME is not correct. Please check with your domain provider and re-authenticate.';
$_['txt_save_domain_success']           = 'Save the domain name successfully!'; 
$_['txt_remove_domain_success']         = 'Deleted domain name!';

// button
$_['btn_add_domain']                    = 'Add a domain name';
$_['btn_domain_auth']                   = 'Authenticate domain names';
$_['btn_cancel']                        = 'Ignore';
$_['btn_re_auth']                       = 'Authenticate again';

// entry
$_['entry_enter_domain']               = 'Enter domain name';
$_['validate_enter_domain']            = 'Enter the correct domain name format, do not start with http: //, https: //, www.';

// error form
$_['error_domain']                 = 'Error: Domain is empty or too short, too long (1-100 characters) or invalid'; 
$_['error_redis_connect']          = 'Error from the system [10023], could not be verified, please try again later'; 
$_['error_domain_max']             = 'Error: Maximum domain number is 15!';
$_['error_exist']                  = 'Error: Domain already exists'; 
$_['error_find_domain']            = 'Error: Domain not found'; 
$_['error_permission']             = 'Error: You do not have permission to add / edit domain';
$_['success_change_redirect']      = 'Change the redirection of all access successfully!';
$_['fail_change_redirect']         = 'Unsuccess, no primary domain name selected!';
$_['fail_change_main_domain']      = 'Change domain name failed!'; 
$_['success_change_main_domain']   = 'Change the main domain name successfully!';
$_['fail_remove_main_domain']      = 'Error: Cannot delete primary domain!';
$_['fail_domain_auth']             = 'Authentication failed!';
$_['success_domain_auth']          = 'Validate the domain name successfully!';
$_['fail_domain_alert_suffix']     = 'incorrect. Please check with your domain provider and re-authenticate.';
$_['validate_input_domain']        = 'Invalid domain name, do not start with http: //, https: //, www.';
$_['validate_input_empty']         = 'Please fill in this field '; 
$_['validate_input_length']        = 'Domain name is too long (1-100 characters)';
