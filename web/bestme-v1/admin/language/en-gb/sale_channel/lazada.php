<?php
$_['heading_title']                     = 'Lazada';

// error, warning
$_['error_permission']                                      = 'You do not have access permission.';

$_['warning_validate_sync_config']                          = 'Missing config field.';
$_['warning_validate_create_prod']                          = 'Missing data field!';
$_['warning_validate_not_exits_prod']                       = 'Product not exits!';
$_['warning_validate_info_prod_not_complete']               = 'Product information is not enough!';
$_['warning_connect_shop_fails']                            = 'Connection failed!';
$_['warning_sync_processing']                               = 'Syncing may take 30 minutes to 1 hours, you can return later.';

//json
$_['json_refresh_success']                                  = 'Data sync successful!';
$_['json_not_permission_or_missing_data']                   = 'You do not have permission or missing linked data.';
$_['json_connection_success']                               = 'Successfully linked!';
$_['json_cancel_connection_success']                        = 'Successfully unlinked!';

//
$_['create_prod_lazada_success']                            = 'Created product and linked successfully.';
$_['create_shop_lazada_success']                            = 'Created shop successfully.';

//
$_['txt_version']                                           = 'Version: ';
$_['txt_sync_refresh']                                      = 'Refresh data';
$_['txt_sync_products']                                     = 'Sync product';
$_['txt_sync_orders']                                       = 'Sync orders';
$_['txt_connect_prod_bestme']                               = 'Link ' . PRODUCTION_BRAND_NAME . '’s products';
$_['txt_cancel_connect_prod_bestme']                        = 'Unlink ' . PRODUCTION_BRAND_NAME . '’s products';
$_['txt_create_prod_lazada']                                = 'Create';
$_['txt_process']                                           = 'Process';
$_['txt_confirm_box_cancel']                                = 'Are you sure to unlink?';
$_['txt_sync_completed']                                    = 'Sync complete!';

//
$_['text_current_stand']                                    = 'Current shop:';
$_['text_all_status']                                       = 'All Status';
$_['text_status_connection']                                = 'Link status';
$_['text_connected_product_bestme']                                             = 'Already linked ' . PRODUCTION_BRAND_NAME . ' products';
$_['text_not_connect_product_bestme']                                           = 'Not yet linked ' . PRODUCTION_BRAND_NAME . ' products';
$_['text_search_product']                                                       = 'Search product';
$_['text_collect_order_information_lazada_to_bestme']                           = 'Collect product information from Lazada about ' . PRODUCTION_BRAND_NAME . '';
$_['text_lazada_product_title']                                                 = 'Product Lazada';
$_['text_info_prod_bestme_inventory']                                           = 'Inventory';
$_['text_info_prod_bestme_price']                                               = 'Price';
$_['text_cancel_connect_prod_version']                                          = 'Unlink version';

//
$_['txt_table_column_prod_lazada']                                              = 'Product Lazada';
$_['txt_table_column_action']                                                   = 'Action';
$_['txt_table_column_version']                                                  = 'Version';
$_['txt_table_column_prod_connect_bestme']                                      = 'Affiliated products on ' . PRODUCTION_BRAND_NAME . '';

//
$_['txt_select2_placeholder']                   = 'Select ' . PRODUCTION_BRAND_NAME . ' product to link';
$_['select2_notice_not_result']                 = "No match found";
$_['select2_notice_search']                     = "Search ...";
$_['select2_notice_load_more']                  = "Load ...";

//
$_['txt_config']                             = 'Config';
$_['text_btn_add_shop']                      = 'Add shop';

//
$_['text_disconnect_success']                                   = 'Successfully disconnected!';
$_['text_disconnect_shop_lazada']                               = 'Disconnect sales channels with Lazada';
$_['text_disconnect']                                           = 'Disconnect';
$_['text_expiration_date']                                      = 'Expiration date';
$_['text_note_expiration_date_1']                               = '* After the expiration date, you need ';
$_['text_note_expiration_date_2']                               = 'to reconnect ';
$_['text_note_expiration_date_3']                               = 'to your Lazada store for the sync to take place normally.';

// v2.7.1
// table
$_['column_order_code']                                     = 'Code order';
$_['column_order_total']                                    = 'Total';
$_['column_order_status_on_lazada']                         = 'Status Lazada';
$_['column_order_status_sync']                              = 'Sync status';
$_['column_action']                                         = 'Action';
$_['txt_column_action']                                     = 'Sync refresh';

//
$_['status_sync_success']                                   = 'Sync success';
$_['status_sync_error']                                     = 'Sync not success';
$_['text_status_not_yet_sync']                              = 'Not yet synchronized';

//
$_['text_lazada_order_title']                                               = 'Order Lazada';
$_['text_order_search']                                                     = 'Search order';
$_['text_order_error_not_in_process_remove_after_15_day']                   = '*Any defective order that is not processed within 15 days of the synchronization date will be deleted from the system!';
$_['text_collect_order_information_lazada_to_bestme']                       = 'Collect order information from Lazada to ' . PRODUCTION_BRAND_NAME . '';
$_['txt_sync_all']                                                          = 'Sync all';
$_['text_update_information_order_bestme_admin_system']                     = 'Update order information successfully synchronized with ' . PRODUCTION_BRAND_NAME . ' admin system';

// status, error, solution
$_['status_lazada_pending']                                         = 'Pending';
$_['status_lazada_canceled']                                        = 'Canceled';
$_['status_lazada_ready_to_ship']                                   = 'Ready to ship';
$_['status_lazada_shipped']                                         = 'Shipped';
$_['status_lazada_delivered']                                       = 'Delivered';
$_['status_lazada_failed']                                          = 'Failed';
$_['status_lazada_returned']                                        = 'Returned';

$_['status_sync_error_product_not_mapping']                                 = 'The products in the Lazada’s order are not linked to ' . PRODUCTION_BRAND_NAME . ' products';
$_['status_sync_error_order_has_been_process']                              = 'The order has been processed on ' . PRODUCTION_BRAND_NAME . '.';
$_['status_sync_error_shop_expired']                                        = 'The connection of Lazada shop has been expired';
$_['status_sync_error_sync_timeout']                                        = 'Over time synchronous request execution';
$_['status_sync_error_product_out_of_stock_on_admin']                       = '' . PRODUCTION_BRAND_NAME . ' products associated with the products in the application are not sufficient in stock';

$_['status_sync_solution_product_not_mapping']                              = 'Link the product in the Lazada’s order with the ' . PRODUCTION_BRAND_NAME . ' product first, then the order will be synchronized';
$_['status_sync_solution_order_has_been_process']                           = 'Order synced will be automatically changed status on admin. If the state is changed manually, then the system will not automatically synchronize the status with that application.';
$_['status_sync_error_shop_expired']                                        = 'Please reconnect the Lazada’s shop in Setting, then your order will be synchronized.';
$_['status_sync_solution_sync_timeout']                                     = 'The current system is processing too many requests, so the order will be resynchronized automatically after the set time or click the Reset button at Order Synchronize section.';
$_['status_sync_solution_product_out_of_stock_on_admin']                    = 'Adjust the ' . PRODUCTION_BRAND_NAME . ' product inventory linked in admin, then sync the order again.';

$_['text_btn_add_shop']                                                         = 'Add shop';
$_['text_hour_time']                                                            = 'hour / time';
$_['text_day_time']                                                             = 'day / time';
$_['text_sync_manually']                                                        = 'Manual synchronization';
$_['text_set_up_order_sync_schedule']                                           = 'Set up a sync order schedule';
$_['text_order_information_will_be_sync_after_the_set_time_period']             = 'Order information will be synchronized after the set time';
$_['text_schedule']                                                             = 'Schedule';

$_['txt_confirm_sync']                                                      = 'Confirm sync';
$_['text_confirm_sync_order_content']                                       = 'Are you sure you want to synchronize information and order status from Lazada to ' . PRODUCTION_BRAND_NAME . '?';
$_['confirm_box_btn_no']                                                    = 'Close';
$_['confirm_box_btn_yes']                                                   = 'Sync';
$_['text_instructions_to_fix_orders_in_sync']                               = 'Instructions to fix orders synchronize errors';
$_['text_order_error']                                                      = 'Error:';
$_['text_order_solution']                                                   = 'Solution:';
$_['text_display_status_sync']                                              = 'Sync status';

$_['warning_validate_time_sync_config']                     = 'The config field is missing';
$_['warning_validate_lazada_shop_exits']                    = 'Shop not exist';
$_['text_save_time_config_success']                         = 'Setting time sync success.';

$_['text_shop_expired']                                     = 'Shop expired';
$_['text_shop_expired_note']                                = 'The current shop has expired, please connect again in the configuration page';
$_['text_go_to_config']                                     = 'Go to config page';

// config new v2.11.2
$_['text_sync_orders_that_have_created_date']                                   = 'Sync orders that have created date';
$_['text_within_x_days']                                                        = 'Within $date days';
$_['text_product_synchronization_setting']                                      = 'Setting update product information';
$_['text_allow_updating']                                                       = 'Allow updating information from Lazada’s products to ' . PRODUCTION_BRAND_NAME . '’s products linked to it. ';
$_['text_update_inventory']                                                     = 'Update inventory';
$_['text_update_price']                                                         = 'Update price';
$_['text_last_sync']                                                            = 'Last sync';
$_['txt_header_btn_sync_all']                                                   = 'Synchronize Lazada products into the admin system to manage';
$_['txt_confirm_sync_content']                      = 'This action will create new product and product category from Lazada to ' . PRODUCTION_BRAND_NAME . ' Admin systerm to manage. Are you sure?';
$_['alert_created_and_updated']                     = 'Create new $COUNT_CREATE: product';