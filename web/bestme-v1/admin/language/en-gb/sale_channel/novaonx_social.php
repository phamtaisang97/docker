<?php

// Heading
$_['heading_title']                     = 'NovaonX Social';

// Text
$_['text_intro_title']                  = 'Connect to NovaonX Social';
$_['text_intro_content']                = 'Vending tool on social networks.
                                           <br>
                                           Closing order automatically via Livestream, commenting, creating campaigns to promote customer buying behavior.';

$_['text_intro_button']                 = 'Use NovaonX Social';

$_['text_more']                         = 'View guide';