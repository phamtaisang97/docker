<?php
$_['heading_title']                     = 'Shopee';


$_['txt_shopee_connect']                     = 'Connect Shopee sales channel';
$_['txt_shopee_desc']                        = 'Connect the system of ' . PRODUCTION_BRAND_NAME . ' and Shopee, help bring products from Shopee to the Website, help synchronize prices and inventory';
$_['txt_shopee_req']                         = 'You need to activate the Shopee sales channel';
$_['txt_shopee_active_btn']                  = 'Activate sales channel';
$_['txt_sync_products']                      = 'Sync products';
$_['txt_sync_orders']                        = 'Sync orders';
$_['txt_process']                            = 'Process';
$_['txt_sync_refresh']                       = 'Refresh data';
$_['txt_sync_refresh_tooltip']               = 'Collect the latest product information from your Shopee store';
$_['txt_sync_all']                           = 'Sync all';
$_['txt_sync_all_tooltip']                   = 'Update all products from Shopee to ' . PRODUCTION_BRAND_NAME . '';
$_['txt_config']                             = 'Config';
$_['txt_cancel']                             = 'Cancel';
$_['txt_save']                               = 'Save';
$_['txt_sync_info_title']                    = 'Synchronize information';
$_['txt_sync_info_content']                  = 'For products already in store ' . PRODUCTION_BRAND_NAME . ', select the information you want to synchronize from Shopee products.';
$_['txt_product_info']                       = 'Product information';
$_['txt_disconnect_shopee_title']            = 'Disconnect from Shopee';
$_['txt_disconnect_shopee_content']          = 'After disconnecting, you will not be able to update the product information changes on the shop at Shopee to the website.';
$_['txt_disconnect_shopee']                  = 'You will disconnect from the Shopee sales channel';
$_['txt_disconnect']                         = 'Disconnect';
$_['txt_disconnect_alert_title']             = 'Disconnect the sales channel';
$_['txt_disconnect_alert_content']           = 'Are you sure you want to disconnect from Shopee';
$_['txt_disconnect_fail']                    = 'Disconnection failed';
$_['txt_confirm_sync']                       = 'Confirm synchronization';
$_['txt_confirm_sync_content']               = 'This action will create new product and product category from Shopee to ' . PRODUCTION_BRAND_NAME . ' Admin systerm to manage. Are you sure?';
$_['txt_sync_completed']                     = 'Sync complete!';
$_['txt_classify']                           = 'Classify';
$_['error_permission']                       = 'Warning: You do not have permission to connect shop!';
$_['connection_failed']                      = 'Warning: Connection failed';

//entry
$_['entry_name']                     = 'Name';
$_['entry_desc']                     = 'Description';
$_['entry_price']                    = 'Price';
$_['entry_quantity']                 = 'Quantity';
$_['entry_weight']                   = 'Weight';

$_['alert_created']                               = 'Create new successful $COUNT_CREATE products';
$_['alert_updated']                               = 'Successfully updated $COUNT_UPDATE product';
//$_['alert_created_and_updated']                   = 'Create new $COUNT_CREATE: product, Update: $COUNT_UPDATE product';
$_['alert_created_and_updated']                   = 'Create new $COUNT_CREATE: product';

//warning
$_['warning_leave_page']                     = 'Leaving the site will stop syncing data from Shopee.';
$_['warning_validate_sync_config']           = 'The config field is missing';
$_['warning_sync_fail']                      = 'Unsuccessful';

// error
$_['warning_active_fail']                        = 'Activation failed';
$_['shopee_list_sync_title']                     = 'Product list';
$_['shopee_product_title']                       = 'Shopee product';

// filter
$_['product_filter']                             = 'Product filter';
$_['filter_list_follow']                         = "Filter products by";
$_['choose_filter']                              = "Select filter condition ...";
$_['filter_status']                              = "Display status";
$_['filter_status_placeholder']                  = "Select status";
$_['action_filter']       = 'Filter';

$_['choose']                                     = "Selected";
$_['text_action_sync']                           = "Update";
$_['text_action_create']                         = "Create";
$_['text_action_update']                         = "Update";
$_['change_status_success']                      = "Change status successfully";
$_['change_status_error']                        = "The status change failed";
//column
$_['product_sku']                       = 'SKU';
$_['txt_product_title']                 = 'Product';
$_['product_status']                    = 'Status';
$_['product_action']                    = 'Action';
$_['entry_category']                    = 'Category';
$_['entry_category_select']             = 'Select Category ';
$_['product_all_status']                = 'All';
$_['entry_status_duplicate']            = 'Product duplicate SKU';
$_['entry_status_exist']                = 'There are products available on ' . PRODUCTION_BRAND_NAME . '';
$_['entry_status_without_sku']          = 'Product is missing SKU';
$_['entry_status_create']               = 'Can create new';
$_['text_duplicate_sku']                = 'Product duplicate SKU: ';
$_['text_duplicate_sku_parent']         = 'Product duplicate SKU parent: ';
$_['text_duplicate_sku_child']          = 'Product duplicate SKU variation: ';
$_['multi_version']                     = 'Multi version';
$_['text_options']                      = 'Select option';
$_['text_action_create_tooltip']        = 'Create new products on ' . PRODUCTION_BRAND_NAME . ' with product information from Shopee';
$_['text_action_sync_tooltip']          = 'Update product information at ' . PRODUCTION_BRAND_NAME . ' with information collected from Shopee';
$_['missing_sku']                       = 'Product missing SKU';
$_['entry_status_exist_shopee']         = 'There are products on Shopee';
$_['more_than_duplicate']               = 'There are more than 1 product of the version';
$_['not_all_variation_product']         = 'No product version (all Shopee version products lack SKU)';
$_['confirm_box_btn_no']                = 'Close';
$_['confirm_box_btn_yes']               = 'Update';
$_['warning_sync_processing']           = 'Please wait a moment while the system synchronizes.';

$_['text_detail']                         = 'Detailed instructions.';

// v2.6.1
// text
$_['text_shopee_order_title']                                               = 'Order Shopee';
$_['text_order_search']                                                     = 'Search order';
$_['text_current_stand']                                                    = 'Current shop:';
$_['text_display_status_sync']                                              = 'Sync status:';
$_['text_instructions_to_fix_orders_in_sync']                               = 'Instructions to fix orders in sync';
$_['text_order_error']                                                      = 'Error:';
$_['text_order_solution']                                                   = 'Solution:';
$_['text_confirm_sync_order_content']                                       = 'Are you sure you want to synchronize information and order status from Shopee to ' . PRODUCTION_BRAND_NAME . '?';
$_['text_order_error_not_in_process_remove_after_15_day']                   = '* Error orders not processed within 15 days of synchronization will be deleted from the system!';


// table
$_['column_order_code']                                     = 'Order Code';
$_['column_order_total']                                    = 'Total';
$_['column_order_status_on_shopee']                         = 'Shopee status';
$_['column_order_status_sync']                              = 'Sync status';
$_['column_action']                                         = 'Action';
$_['txt_column_action']                                     = 'Sync refresh';

// status, error, solution
$_['status_un_paid']                                        = 'Unpaid';
$_['status_to_ship']                                        = 'To ship';
$_['status_completed']                                      = 'Completed';
$_['status_cancelled']                                      = 'Cancelled';
$_['status_to_return']                                      = 'To return';
$_['status_to_confirm_receive']                             = 'To confirm receive';
$_['status_retry_ship']                                     = 'Retry ship';
$_['status_shipped']                                        = 'Shipped';
$_['status_sync_success']                                   = 'Sync success';
$_['status_sync_error']                                     = 'Sync error';
$_['text_all_status']                                       = 'All statuses';
$_['text_status_not_yet_sync']                              = 'Not yet sync';

$_['status_sync_error_order_has_been_process']                              = 'Order has been processed on ' . PRODUCTION_BRAND_NAME . '.';
$_['status_sync_error_product_not_sync']                                    = 'Products in the order have not been synchronized.';
$_['status_sync_error_sync_timeout']                                        = 'Request Timeout.';
$_['status_sync_error_product_deleted_on_shopee']                           = 'The product has been synchronized but deleted on Shopee.';
$_['status_sync_error_product_out_of_stock_on_admin']                       = 'Products in the order are not enough inventory at ' . PRODUCTION_BRAND_NAME . '.';

$_['status_sync_solution_order_has_been_process']                           = 'Synchronized orders will be automatically change status at ' . PRODUCTION_BRAND_NAME . '. If changing the status manually, then the system will not automatically sync the status of the order.';
$_['status_sync_solution_product_not_sync']                                 = 'Go to Shopee at admin -> Sync products to sync products in the menu to ' . PRODUCTION_BRAND_NAME . '. When the product is successfully synchronized, the order will be automatically re-synchronized.';
$_['status_sync_solution_sync_timeout']                                     = 'Currently, the system is processing too many requests, so the order will automatically re-sync after the set time or click the Re-sync button at the Order synchronization section.';
$_['status_sync_solution_product_deleted_on_shopee']                        = '';
$_['status_sync_solution_product_out_of_stock_on_admin']                    = 'Adjust the inventory of that product at admin, then re-sync orders.';
$_['text_collect_order_information_shopee_to_bestme']                       = 'Gather order information from Shopee to ' . PRODUCTION_BRAND_NAME . '';
$_['text_update_information_order_bestme_admin_system']                     = 'Update information on successful sync orders to the ' . PRODUCTION_BRAND_NAME . ' admin system';

// config
$_['text_btn_add_shop']                                                         = 'Add shop';
$_['text_hour_time']                                                            = 'hour/time';
$_['text_day_time']                                                             = 'day/time';
$_['text_sync_manually']                                                        = 'Sync manually';
$_['text_set_up_order_sync_schedule']                                           = 'Set up order sync schedule';
$_['text_order_information_will_be_sync_after_the_set_time_period']             = 'Order information will be sync after the set time period';
$_['text_schedule']                                                             = 'Schedule';

// config new v2.11.2
$_['text_sync_orders_that_have_created_date']                                   = 'Sync orders that have created date';
$_['text_within_x_days']                                                        = 'Within $date days';
$_['text_product_synchronization_setting']                                      = 'Setting update product information';
$_['text_allow_updating']                                                       = 'Allow updating information from Shopee’s products to ' . PRODUCTION_BRAND_NAME . '’s products linked to it. ';
$_['text_update_inventory']                                                     = 'Update inventory';
$_['text_update_price']                                                         = 'Update price';
$_['text_last_sync']                                                            = 'Last sync';

$_['warning_validate_time_sync_config']                     = 'The config field is missing';
$_['warning_validate_shoppe_shop_exits']                    = 'Shop not exist';

// v2.8.1
$_['text_status_connection']                                                    = 'Link status';
$_['text_connected_product_bestme']                                             = 'Already linked ' . PRODUCTION_BRAND_NAME . ' products';
$_['text_not_connect_product_bestme']                                           = 'Not yet linked ' . PRODUCTION_BRAND_NAME . ' products';

$_['text_info_prod_bestme_inventory']                                           = 'Inventory';
$_['text_info_prod_bestme_price']                                               = 'Price';
$_['text_cancel_connect_prod_version']                                          = 'Unlink version';
$_['txt_connect_prod_bestme']                                                   = 'Link ' . PRODUCTION_BRAND_NAME . '’s products';
$_['txt_cancel_connect_prod_bestme']                                            = 'Unlink ' . PRODUCTION_BRAND_NAME . '’s products';
$_['txt_create_prod_lazada']                                                    = 'Create';
$_['text_search_product']                                                       = 'Search product';
$_['txt_table_column_version']                                                  = 'Version';
$_['txt_table_column_prod_connect_bestme']                                      = 'Affiliated products on ' . PRODUCTION_BRAND_NAME . '';

//
$_['txt_select2_placeholder']                                                   = 'Select ' . PRODUCTION_BRAND_NAME . ' product to link';
$_['select2_notice_not_result']                                                 = "No match found";
$_['select2_notice_search']                                                     = "Search ...";
$_['select2_notice_load_more']                                                  = "Load ...";

//
$_['txt_confirm_box_cancel']                                                    = 'Are you sure to unlink?';
$_['txt_version']                                                               = 'Version:';
$_['txt_header_btn_sync_refresh']                                               = 'Gather the latest information from your Shopee store';
$_['txt_header_btn_sync_all']                                                   = 'Synchronize Shopee products into the admin system to manage';
$_['create_prod_shopee_success']                                                = 'Created product and linked successfully.';