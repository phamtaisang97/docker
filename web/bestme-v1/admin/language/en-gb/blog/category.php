<?php
// Heading
$_['heading_title']               = 'Categories List';
$_['heading_blog_title']          = 'Blog';
$_['heading_title_add']           = 'Add new category';
$_['heading_title_edit']          = 'Edit category';

$_['text_save']                          = 'Save';
$_['text_cancel']                        = 'Cancel';

$_['text_seo_preview']       = 'Preview search result';
$_['text_seo_config']        = 'Customize SEO';
$_['text_seo_guide']         = 'Enter title and description to view the way the category will be displayed in search list.';
$_['text_seo_title_exp']     = '3 best Christmas gifts in Noel season 14/12/2019';
$_['text_seo_alias_exp']     = '3-best-Christmas-gifts-in-noel-season';
$_['txt_seo_title']                            = 'Meta title';
$_['txt_seo_title_place_holder']               = 'Enter meta title';
$_['txt_seo_description']                      = 'Content';
$_['txt_seo_description_place_holder']         = 'Add content';
$_['txt_seo_alias']                            = 'Url / Alias';

//$_['text_add_to_menu']                   = 'Gắn vào menu';
//$_['text_add_collection_to_menu']        = 'Bạn sẽ thêm bộ sưu tập vào các menu sau:';
//$_['text_choose_menu']                   = 'Chọn Menu';
//$_['text_choose_menu_text']              = 'Bạn có thể thêm bộ sưu tập này vào một liên kết bằng cách sử dụng nút Chọn menu';

$_['breadcrumbs_setting_title']             = 'Setting';
$_['breadcrumbs_blog_title']                = 'Post';
$_['breadcrumbs_add_blog_category_title']   = 'Add category';
$_['blog_add_category_title']               = 'Add category';

$_['error_permission']               = 'You do not have permission';
$_['error_name_exist']               = 'Category name is already existing';
$_['error_name_empty']               = 'Category title could not be empty';
$_['error_name_long']                = 'Category name is more than 100 chars';
$_['text_success_add']               = 'Add category successfully';
$_['text_success_edit']              = 'Edit category successfully';

$_['category_list_filter_action']        = 'Choose action';
$_['category_list_filter_status_delete'] = 'Delete category';
$_['text_title']                         = 'Title';
$_['text_number_blog']                   = 'Number of posts in category';
$_['category_list_filter_status']        = 'Status';

$_['text_add_to_menu']                   = 'Add to menu';
$_['text_add_collection_to_menu']        = 'You will add the category to the following menus:';
$_['text_choose_menu']        = 'Select Menu';
$_['text_choose_menu_text']   = 'You can add this category to a link using the Select menu button';

$_['collection_list_filter_status']            = 'Status';
$_['breadcrumb_status']                  = 'Show';
$_['breadcrumb_status_hide']             = 'Hide';

$_['text_search']                        = 'Search';
$_['text_search_placeholder']            = 'Search category';
$_['filter_list_follow']                 = "Filter by";
$_['choose_filter']                      = "Select filter";
$_['filter_status']                      = "Filter status";

$_['placeholder_title']                  = "Enter category title";
$_['delete_blog_category']               = "Delete category";
$_['blog_category_input_title']          = "Title";

$_['select2_notice_not_result']          = "Could not found result";
$_['select2_notice_search']              = "Searching ...";
$_['select2_notice_load_more']           = "Loading ...";

$_['entry_menu_placeholder']             = 'Select menu';

$_['change_success']                     = "Changed successfully.";
$_['delete_success']                     = "Deleted successfully.";

$_['category_confirm_show']              = 'Are you sure you want to delete ';
$_['category_confirm_choose']            = " selected categories?";
$_['category_confirm_delete']            = "Delete category";
$_['popup_cancel_title']  = "Discard the change information";
$_['popup_cancel_message']  = "Changes will be lost";
$_['popup_cancel_ok'] = "Ok";
$_['text_close']                    = 'Close';
$_['text_remove']                    = 'Remove';

$_['error_category']                     = 'Warning: could not delete the blog category that is assigned to some blogs! Please remove all blogs from this blog category and try again.';