<?php
// Heading
$_['heading_title']               = 'Post list';
$_['heading_title_add']           = 'Add post';
$_['heading_title_edit']          = 'Edit post';
$_['text_success']                = 'Add post successfully';
$_['text_success_edit']           = 'Edit post successfully';

// breadcrumb
$_['breadcrumb_product']                 = 'Post';
$_['breadcrumb_product_collection']      = 'Post';
//
$_['collection_list_title']              = 'List';
$_['collection_list_filter']             = 'Filter post';
$_['breadcrumb_product_collection']      = 'Blogs';
$_['breadcrumb_status']                  = 'Show';
$_['breadcrumb_status_hide']             = 'Hide';

$_['text_list_all']                      = 'All';
$_['text_list_title']                    = 'Title';
$_['text_list_category']                 = 'Category';
$_['text_list_author']                   = 'Author';
$_['text_list_status']                   = 'Status';
$_['text_approval_status']               = 'Approval status';
$_['text_list_modified_date']            = 'Modified Date';

// filter
$_['collection_list_filter_title']             = 'Post filter';
$_['collection_list_filter_title_mobile']      = 'Lọc';
$_['collection_list_filter_collection_all']    = 'All';
$_['collection_list_filter_status']            = 'Status';

$_['collection_list_filter_action']            = 'Select action';
$_['collection_list_filter_status_show']       = 'Show post';
$_['collection_list_filter_status_disabled']   = 'Hide post';
$_['collection_list_filter_status_delete']     = 'Delete post';

$_['delete_confirm']       = 'Are you sure you want to delete';
$_['confirm_show']         = 'Are you sure you want to change ';
$_['text_add']             = 'Add ';
$_['text_search']          = 'Search';
$_['text_search_list']     = 'Search post';
$_['text_cancel']          = 'Cancel ';
$_['text_save']            = 'Save change ';

$_['text_name']               = 'Post name ';
$_['entry_name_placeholder']  = 'Enter post name';
$_['text_category']           = 'Category';
$_['text_action']             = 'Action';
$_['text_content']            = 'Post content ';
$_['text_set_blog']           = 'Select product to post';
$_['text_see_search']         = 'preview search result';
$_['text_seo']                = 'Customize SEO';
$_['txt_seo_keywords_place_holder']            = 'Enter keywords here, each keyword separated by commas';
$_['text_more']               = 'Enter title and description to view the way the post will be displayed in search list.';
$_['text_tag_title']          = 'Meta Title';
$_['text_abouts']             = 'Meta Description';
$_['text_alias']              = 'Url / Alias';
$_['text_content_seo']        = 'Post content';
$_['text_image']              = 'Post image';
$_['text_add_image']          = 'Add image';
$_['txt_place_holder_alt']    = 'Enter alt image';

$_['txt_use_image']           = 'Use image';
$_['txt_use_video']           = 'Use video';
$_['txt_place_holder_video']  = 'Enter link video here';
$_['txt_note']                = 'Note';
$_['txt_play_video_click']    = 'Video will be played when clicking on it';

$_['sort_by']             = 'Sort: ';
$_['sort_zero']           = 'Manual';
$_['sort_az']             = 'By name: A-Z';
$_['sort_za']             = 'By name: Z-A';
$_['sort_hightolow']      = 'By price: High to low';
$_['sort_lowtohigh']      = 'By price: Low to high';
$_['sort_newtoold']       = 'By created date: from new to old';
$_['sort_oldtonew']       = 'By created date: from old to new';

// Filter
$_['filter_list_follow']      = "Filter product by";
$_['choose_filter']           = "Select filter";
$_['filter_status']           = "Status";
$_['filter_manufacture']      = "Manufacturer";
$_['filter_category']         = "Category";
$_['filter_tag']              = "Tag";
$_['filter_length_product']   = "Quantity";

$_['entry_collection_placeholder']   =    "Select product to post";

$_['delete_collection_confirm_show']          = 'Are you sure you want to delete post?';
$_['error_max_length_250']                    = 'Could not be over 250 characters';
$_['error_product']                           = 'You must select at least one product';
$_['error_title_add']                         = 'Post title could not be empty';
$_['error_text_char']                         = 'Do not enter special character';
$_['error_text_title']                        = 'Meta title could not be empty';
$_['error_text_check_blog']                   = 'Add post failed!';
$_['txt_not_video']                           = 'You must enter video url';
$_['txt_video_correct']                       = 'Video url is not correct';
$_['choose']                                  = "Selected ";
$_['item']                                    = " item(s)";
$_['txt_selected_all']                        = "Selected all in this page";
$_['change_success']                 = "Change successfully.";
$_['delete_success']                 = "Delete successfully.";
$_['add_collection_new']             = "+ Add Collection";
$_['collection_new']                 = "Post";
$_['collection_filter_new']          = "Filter";
$_['collection_count_product']       = "product";
$_['collection_form_cancel_mobile']    = "Cancel";
$_['collection_form_save_mobile']      = "Save";
$_['collection_confirm_show']          = 'Are you sure you want to delete ';
$_['collection_confirm_choose']        = " selected post(s) ?";

$_['text_contenttext_content']        = "Enter content";
$_['text_short_name']                 = "SUMMARY";
$_['sum_description']                 = "Short content will be displayed on Homepage or Blog page";
$_['text_alt_image']                  = "Alt for image";
$_['text_source']                     = "Post resource";
$_['text_author']                     = "Author";
$_['text_category']                   = "Category";
$_['text_set_tag']                    = 'Add tag';
$_['text_choose_tag']                 = 'Select tag';
$_['text_choose_tag_text']            = 'You could add this post to link by using button Select tag';
$_['text_used_text']                  = 'Used:';
$_['text_used_char']                  = 'chars';
$_['text_add_to_tag']                 = 'Add tag';
$_['entry_tag_placeholder']           = 'Enter tag';
$_['txt_add']                                    = "Add post";
$_['remove_all_tag']                             = 'Delete all tags';
$_['text_add_collection_to_tag']                 = 'You will add post to following tags:';
$_['entry_tag']                       = 'Add tag';
$_['notice_add_tag_error']            = "You have not added any tag to selected post";
$_['notice_remove_tag_error']         = "You have not select tag";
$_['notice_add_tag_success']          = "Add tag to post successfully";
$_['text_select_author']              = "Select author...";
$_['text_select_category']            = "Select category...";
$_['error_select_category']           = "You did not select category";

$_['action_publish']                    = "Show";
$_['action_unpublish']                  = "Hide";
$_['action_add_tag_product']            = "Add tag";
$_['action_remove_tag_product']         = "Remove tag";
$_['action_remove_blog']                = "Delete post";
$_['label_add']                         = 'Add';
$_['label_remove']                      = 'Cancel';
$_['label_close']                       = 'Close';
$_['tag_for_product_choose']            = 'tag to selected post';
$_['list_tag_for_product']              = 'following tags for post:';
$_['use_tag_description']               = 'Each tag is separated by comma (,)';
$_['choose_from_list_tag']              = "Choose from exising tags";
$_['notice_add_tag_error_max_length']   = "Tag could not be over 30 characters";
$_['notice_remove_tag_try']             = "Error: could not remove tag, please retry later!";
$_['notice_remove_tag_success']         = "Removed tag successfully";

// Select 2
$_['select2_notice_not_result']   =    "Could not found result";
$_['select2_notice_search']       =    "Searching ...";
$_['select2_notice_load_more']    =    "Loading ...";
$_['popup_cancel_title']  = "Discard the change information";
$_['popup_cancel_message']  = "Changes will be lost";
$_['popup_cancel_ok'] = "Ok";
$_['remove_tag_error']                        = "Error adding tags";
$_['entry_search_tag_placeholder']       = 'Search tag';
$_['text_success_add']           = 'Create new orders successfully';
$_['text_home']                     = 'Home';
$_['text_pagination']               = 'Showing %d to %d of %d (%d Pages)';
$_['product_all_status']            = 'All';
$_['entry_status_publish']   = 'Display';
$_['entry_status_unpublish'] = 'Hide';
$_['filter_category_placeholder']                = "Select blog type";
$_['error_permission']           = 'Warning: You do not have permission to modify blog!';
$_['error_max_length_30']              = 'Do not exceed 30 characters';
$_['error_warning']              = 'Warning: Please check the form carefully for errors!';
$_['success']                = 'Success';
$_['choose_all_in_filter']                                 = "Selected all item in this page";
$_['choose_in_filter']                                     = "Selected";
$_['item_in_filter']                                       = "item";
$_['text_no_results']               = 'No results!';
$_['filter_tag']                                 = "Tagged with";

$_['text_close']                    = 'Close';
$_['text_remove']                    = 'Remove';
$_['text_on']                    = 'On';
$_['text_off']                    = 'Off';
$_['text_select_all']               = 'Select All';
$_['text_page_title']               = 'Blog';

$_['filter_status_placeholder']                  = "Select status";
$_['filter_select_category']            = "Select category";
$_['filter_select_author']              = "Select Author";

$_['error_keyword_check_title']              = ' " title " violated the keyword';
$_['error_keyword_check_content']            = ' " content " violated the keyword';
$_['error_keyword_check_short_content']      = ' " short content " violated the keyword';

$_['error_seo_name']                = "Seo title not empty";
$_['error_seo_description']         = "Seo description not empty";
$_['error_seo_keywords']            = "Seo keywords not empty";
$_['error_image']                   = "Image not empty";
$_['error_image_alt']               = "Image alt not empty";

$_['filter_approval_status_placeholder']                  = "Chọn Trạng thái";
$_['filter_approval_status']                              = "Trạng thái phê duyệt";