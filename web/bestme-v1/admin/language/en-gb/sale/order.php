<?php
// Heading
$_['heading_title']              = 'Order';

// button
$_['btn_save_apply']               = 'Save and approve';
$_['btn_save_draft']               = 'Save draft menu';

// Text
$_['text_product']               = 'Product';
$_['text_version']               = 'Version';
$_['text_unit_price']            = 'Unit price';
$_['text_quantity']              = 'Quantity';
$_['text_amount']                = 'Amount';
$_['text_total_amount']          = 'Total amount of goods';
$_['text_shipping_fee']          = 'Shipping fee';
$_['text_need_pay']              = 'Total payment need';
$_['text_payments']              = 'Payment method';
$_['text_payment_method_choose'] = 'Choose payment method';
$_['text_cod_payment']           = 'Payment on delivery';
$_['text_pre_payment']           = 'Advance payment';
$_['text_success_payment']       = 'Success payment';
$_['text_success_payment_cod']   = 'Postpay(COD)';
$_['text_payment_status']        = 'Payment status';
$_['text_order_info']            = 'Order information';
$_['text_order_create_date']     = 'Created date';
$_['text_order_create_user']     = 'Creator';
$_['text_note']                  = 'Note';
$_['text_utm_tracking']                  = 'Tracking utm';
$_['text_note_placeholder']      = 'Write notes for orders';
$_['text_tag']                   = 'Attached tag';
$_['text_tag_placeholder']       = 'Enter tag';
$_['text_order_copy']            = 'Duplicate';
$_['text_order_delete']          = 'Delete order';
$_['text_vnpay_payment']         = 'Payment via VNPAY';
$_['text_shopee_shopname']       = 'Shop name';
$_['txt_vnppay']          = 'VNPAY';

//text customer, address
$_['text_edit_address']          = 'Address editing';
$_['text_create_customer']       = 'Create new customers';
$_['text_fullname']              = 'Fullname';
$_['text_fullname_delivery']     = 'Recipient name';
$_['text_lastname']              = 'Lastname';
$_['text_firstname']             = 'Firstname';
$_['text_email']                 = 'Email';
$_['text_telephone']             = 'Telephone number';
$_['text_province']              = 'Province / City';
$_['text_district']              = 'District /Town';
$_['text_wards']                 = 'Ward / Commune';
$_['text_addr']                  = 'Address';
$_['text_province_placeholder']  = 'Choose';
$_['text_delivery_address']      = 'Delivery address';


$_['text_success']               = 'Update order information successfully ';
$_['text_success_add']           = 'Create new orders successfully';
$_['text_list']                  = 'Order list';
$_['text_export']                = 'Export';
$_['text_add']                   = 'Create order';
$_['text_edit']                  = 'Edit order';
$_['text_order_detail']          = 'Order detail';
$_['text_filter']                = 'Order filter';
$_['text_filter_mobile']         = 'Filter';
$_['text_all_orders']            = 'All orders';
$_['text_order_complete']        = 'Order completed';
$_['text_cancel_order']          = 'Cancel order';
$_['text_order_cancelled']       = 'The order has been canceled';
$_['text_duplicate_order']       = 'Duplicate orders';
$_['text_order_history']         = 'Order history';
$_['text_order_create_now']      = 'Now';
$_['text_order_status']          = 'Status';
$_['text_save_confirm']          = 'Do you want to save the current order?';
$_['text_validate_product']      = 'You must select at least 1 product!';
$_['text_validate_shipping_info'] = 'Please enter full customer information!';
$_['text_validate_product_quantity']      = 'The minimum quantity for a product in the order is 1';
$_['txt_receipt_voucher']        = 'The receipt voucher that is automatically created and attached to this order is ';
//btn
$_['btn_order_update']           = 'Order update';

$_['text_customer_detail']       = 'Customer';
$_['text_customer_add']          = 'Add new customer';
$_['text_option']                = 'Options';
$_['text_store']                 = 'Store';
$_['text_date_added']            = 'Date Added';
$_['text_payment_method']        = 'Payment method';
$_['text_shipping_method']       = 'Shipping method';
$_['text_customer']              = 'Customer';
$_['text_customer_group']        = 'Customer group';
$_['text_fax']                   = 'Fax';
$_['text_invoice']               = 'Request payment';
$_['text_reward']                = 'Reward';
$_['text_affiliate']             = 'marketing';
$_['text_order']                 = 'Order (#%s)';
$_['text_payment_address']       = 'Payment address';
$_['text_shipping_address']      = 'Shipping address';
$_['text_comment']               = 'Customer Note';
$_['text_history']               = 'Order history';
$_['text_history_add']           = 'Add Order History';
$_['text_account_custom_field']  = 'Customize Account';
$_['text_payment_custom_field']  = 'Customize payment address';
$_['text_shipping_custom_field'] = 'Customize shipping address';
$_['text_browser']               = 'Browser';
$_['text_ip']                    = 'IP address';
$_['text_forwarded_ip']          = 'Forward IP';
$_['text_user_agent']            = 'User Agent';
$_['text_accept_language']       = 'language';
$_['text_order_id']              = 'ID order:';
$_['text_website']               = 'WebSite:';
$_['text_invoice_no']            = 'Payment request number .:';
$_['text_invoice_date']          = 'Date of payment request:';
$_['text_sku']                   = 'SKU:';
$_['text_upc']                   = 'UPC:';
$_['text_ean']                   = 'EAN:';
$_['text_jan']                   = 'JAN:';
$_['text_isbn']                  = 'ISBN:';
$_['text_mpn']                   = 'MPN:';
$_['text_missing']               = 'Missing Orders';
$_['text_default']               = 'Default';
$_['text_product']               = 'Product(s)';
$_['text_voucher']               = 'Add Voucher(s)';
$_['text_shipping']              = 'Shipping';
$_['text_contact']               = 'Contact';
$_['text_reward_added']          = 'Success: Reward points added!';
$_['text_reward_removed']        = 'Success: Reward points removed!';
$_['text_commission_added']      = 'Success: Commission added!';
$_['text_commission_removed']    = 'Success: Commission removed!';
$_['text_restock']               = 'Success: Products have been restocked!';
$_['text_upload']                = 'Your file was successfully uploaded!';
$_['text_picklist']              = 'Dispatch Note';

// Column
$_['column_order_id']            = 'Order ID';
$_['column_customer']            = 'Customer';
$_['column_status']              = 'Status';
$_['column_date_added']          = 'Date Added';
$_['column_date_modified']       = 'Date Modified';
$_['column_total']               = 'Total';
$_['column_product']             = 'Product';
$_['column_model']               = 'Model';
$_['column_quantity']            = 'Quantity';
$_['column_price']               = 'Unit Price';
$_['column_comment']             = 'Comment';
$_['column_notify']              = 'Customer Notified';
$_['column_location']            = 'Location';
$_['column_reference']           = 'Reference';
$_['column_action']              = 'Action';
$_['column_weight']              = 'Product Weight';

// Entry
$_['entry_store']                = 'Store';
$_['entry_customer']             = 'Customer';
$_['entry_customer_group']       = 'Customer Group';
$_['entry_firstname']            = 'First Name';
$_['entry_lastname']             = 'Last Name';
$_['entry_email']                = 'E-Mail';
$_['entry_telephone']            = 'Telephone';
$_['entry_address']              = 'Choose Address';
$_['entry_company']              = 'Company';
$_['entry_address_1']            = 'Address 1';
$_['entry_address_2']            = 'Address 2';
$_['entry_city']                 = 'City';
$_['entry_postcode']             = 'Postcode';
$_['entry_country']              = 'Country';
$_['entry_zone']                 = 'Region / State';
$_['entry_zone_code']            = 'Region / State Code';
$_['entry_product']              = 'Choose Product';
$_['entry_option']               = 'Choose Option(s)';
$_['entry_quantity']             = 'Quantity';
$_['entry_to_name']              = 'Recipient\'s Name';
$_['entry_to_email']             = 'Recipient\'s E-mail';
$_['entry_from_name']            = 'Sender\'s Name';
$_['entry_from_email']           = 'Sender\'s E-mail';
$_['entry_theme']                = 'Gift Certificate Theme';
$_['entry_message']              = 'Message';
$_['entry_amount']               = 'Amount';
$_['entry_affiliate']            = 'Affiliate';
$_['entry_order_status']         = 'Order Status';
$_['entry_notify']               = 'Notify Customer';
$_['entry_override']             = 'override';
$_['entry_comment']              = 'comment';
$_['entry_currency']             = 'currency';
$_['entry_shipping_method']      = 'Shipping method';
$_['entry_payment_method']       = 'payment method';
$_['entry_coupon']               = 'Coupon';
$_['entry_voucher']              = 'Voucher';
$_['entry_reward']               = 'Reward';
$_['entry_order_id']             = 'ID order';
$_['entry_total']                = 'Total';
$_['entry_date_added']           = 'Created date';
$_['entry_date_modified']        = 'Updated date';
$_['entry_product_placeholder']  = 'Search product';
$_['entry_customer_placeholder'] = 'Find or create new customers';

// Help
$_['help_override']              = 'If the customers order is being blocked from changing the order status due to an anti-fraud extension enable override.';
// Select 2
$_['select2_notice_not_result']   =    "No match found";
$_['select2_notice_search']       =    "Search ...";
$_['select2_notice_load_more']    =    "Load ...";

// Error
$_['error_customer_warning']     = 'There has been a problem!';
$_['error_customer_info']        = 'No customers found!';
$_['error_notfound_order']       = 'No orders found!';
$_['error_order_delete_s_6']     = 'Only delete draft status orders!';
$_['error_warning']              = 'Warning: Please check the form carefully for errors!';
$_['error_permission']           = 'Warning: You do not have permission to edit Orders!';
$_['error_action']               = 'Warning: This operation cannot be completed!';
$_['error_filetype']             = 'File type is incorrect!';
$_['order_delete']               = 'Delete';
$_['order_text_copy']            = 'Copy';
// product order
$_['error_not_add_product']      = 'You haven\'t added product ';
$_['error_not_add_product_min']  = 'Add product more than 0';
$_['error_not_add_product_max']  = 'Product add many existing products';

$_['error_input_null']                 = 'Please fill in this field';
$_['error_type_mail']                  = 'Please enter the correct email format';
$_['error_max_length_255']             = 'Does not exceed 255 characters';
$_['error_max_length_15']              = 'Do not exceed 15 characters';
$_['error_type_phone']                 = 'Please enter the correct email format';
$_['error_max_length_10']              =  'Do not exceed 10 characters';
$_['error_add_product_max']            = 'Do not exceed 10 product';
$_['error_max_length_1000']            = 'Does not exceed 1000 characters';
$_['error_product_max_length']         = 'The number of products added too much';
$_['error_input_only_number']          = 'Only enter numbers for this field';
$_['error_max_length_30']              = 'Do not exceed 30 characters';
$_['error_max_length_300']             = 'Do not exceed 300 characters';
$_['error_a_z_0_9']                    = 'Allow to enter uppercase, lowercase, numeric characters,';
$_['error_shipping_address']           = 'Allow to enter numbers, uppercase, lowercase, special characters - hyphens, commas,';
$_['error_max_length_100']             = 'Not exceeding 100 characters';

// List
$_['text_table_order_code']                        = "Order code";
$_['text_table_customer']                          = "Customer";
$_['text_table_order_status']                      = "Order status";
$_['text_table_order_payment_status']              = "Payment status";
$_['text_table_order_total']                       = "Order value";
$_['text_table_updated_time']                      = "Updated date";

$_['order_status_delivering_wait']              = "Delivery wait";
$_['order_status_draft']                        = "Draft";
$_['order_status_processing']                   = "Processing";
$_['order_status_delivering']                   = "Shipping";
$_['order_status_comback']                      = "Completing the application";
$_['order_status_complete']                     = "Complete";
$_['order_status_canceled']                     = "Canceled";
$_['order_payment_status_paid']                 = "Already paid";
$_['order_payment_status_not_paid']             = "Unpaid";
$_['order_payment_status_refunded']             = "Refunded";

// Filter
$_['filter_list_follow']                         = "Sort order by:";
$_['choose_filter_1']                            = "Sort by";
$_['choose_filter_2']                            = "Choose filter conditions";
$_['filter_button']                              = "Filter";

$_['filter_status']                              = "Order status";
$_['filter_status_placeholder']                  = "Select status";
$_['filter_status_draft']                        = "Draft";
$_['filter_status_processing']                   = "Processing";
$_['filter_status_delivering']                   = "Delivery";
$_['filter_status_complete']                     = "Complete";
$_['filter_status_canceled']                     = "Cancel";

$_['filter_payment_status']                      =  "Payment status";
$_['filter_payment_status_placeholder']                  = "Select Status";
$_['filter_payment_status_paid']                 = "Already paid";
$_['filter_payment_status_not_paid']             = "Unpaid";
$_['filter_payment_status_return']               = "Refunded";
$_['filter_payment_error']                       = "Payment via VNPay error";
$_['text_payment_code']                          = "Transaction ID:";
$_['text_confirm_change_status_payment_when_error']                 = "The payment process is not complete, would you like to continue this action?";

$_['filter_grant_total']                         = "Order value";

$_['filter_tag']                                 = "Tagged with";
$_['filter_tag_placeholder']                     = "Enter tag";

$_['filter_phone']                               = "Phone number";
$_['filter_phone_placeholder']                   = "Enter phone number";

$_['filter_source']                              = "Source";
$_['filter_source_placeholder']                  = "Choose source";

$_['filter_discount']                            = "Promotions";
$_['filter_discount_placeholder']                = "Choose promotion";

$_['filter_updated_time']                        = "Update date";
$_['filter_updated_time_placeholder']            = "Select time period";
$_['filter_updated_time_from']                   = "Since the date";
$_['filter_updated_time_from_back']              = "From this day forward";

$_['filter_count']                               = "Quantity";
$_['filter_count_placeholder']                   = "Select filter conditions";
$_['filter_count_greater']                       = "Greater";
$_['filter_count_smaller']                       = "Smaller";
$_['filter_count_equal']                         = "Equal";
$_['filter_count_input_placeholder']             = "Input value";
$_['filter_date_range']                          = "Date range";
$_['filter_select_date']                         = "Select date";
$_['filter_txt_date']                            = "Date";
$_['filter_staff']                               = "Staff";
$_['filter_staff_placeholder']                 = 'Select staff';

// Search
$_['search_placeholder']                         = "Search by order code";

$_['notice_add_tag_success']            = "Update order information successfully";
$_['notice_remove_tag_success']         = "Update order information successfully";
$_['notice_add_tag_error']              = "You have not added any tags for the selected order";
$_['notice_remove_tag_error']           = "You have not select tag";
$_['notice_add_tag_error_max_length']   = "Tag does not exceed 30 characters";
$_['tag_for_order_choose']              = "tag for selected orders";
$_['list_tag_for_order']                = "The following tags for the order:";
$_['use_tag_description']               = 'Each tag is separated by commas (,)';

// Action modal
$_['text_cancel_order_confirm']          = 'Are you sure you want to cancel the order?
                                            After canceling your application, you will no longer be able to continue working with this order.
                                            This operation cannot be restored either. ';
$_['text_cancel_order_note']             = 'Note the reason for canceling the order';
$_['text_cancel_order_note_placeholder'] = 'Note ...';
$_['text_cancel_order_return_product']   = 'Returned product';
$_['text_cancel_order_return_total']     = 'Total refund amount:';
$_['text_cancel_order_send_email']       = 'Send Email notification to customers';
$_['text_cancel_order_close']            = 'Close';
$_['text_cancel_order_submit']           = 'Cancel';

$_['text_update_order_status']                        = 'Update status';
$_['text_update_order_close']                         = 'Close';
$_['text_update_order_submit']                        = 'Confirm';

$_['text_update_order_status_to_processing']          = 'Switch to Processing state.';
$_['text_update_order_status_to_processing_confirm']  = 'After browsing the order, you will not be able to remove the order from the list.sách.
                                                         Are you sure the order is being processed? ';

$_['text_update_order_status_to_delivering']          = 'Switch to Shipping status.';
$_['text_update_order_status_to_delivering_confirm']  = 'After shipping the order, all information on the order cannot be adjusted.
                                                        Edit, except Notes and Tags. Are you sure the order has been shipped?
                                                        to shipping partners? ';

$_['text_update_order_status_to_complete']           = 'Confirm order completed';
$_['text_update_order_status_to_complete_confirm']   = 'After completing the order, all information on the order cannot be edited, except notes and tags. Are you sure the order is complete? ';

$_['text_update_order_status_to_cancelled_title']    = 'Cancel order';
$_['text_delete_confirm']          = 'Arr you sure to delete order? This operation cannot be restored. ';
$_['text_delete_order_success']    = 'Delete order success';

$_['text_update_order_tag_close']      = 'Close';
$_['text_update_order_tag_add']        = 'Add';
$_['text_update_order_tag_remove']     = 'Delete';


$_['payment_status_success']     = 'payment success';
$_['payment_status_fail']        = 'payment failed';

$_['export_order_code']          = 'Code orders';
$_['export_customer_name']       = 'Customer name';
$_['export_customer_phone']      = 'Phone number';
$_['export_customer_email']      = 'Email';
$_['export_customer_address']    = 'Shipping address';
$_['export_customer_note']       = 'Note';
$_['export_product_name']        = 'Product name';
$_['export_product_image']       = 'Product image';
$_['export_product_price']       = 'Product price';
$_['export_product_amount']      = 'Quantity';
$_['export_order_status']        = 'Order status';
$_['export_order_total']         = 'Order value';
$_['export_payment_status']      = 'Payment status';
$_['export_payment_note']        = 'Payment form';
$_['export_delivery']            = 'Shipping method';
$_['export_delivery_fee']        = 'Shipping fee';
$_['export_delivery_free']       = 'Free shipping';
$_['export_order_update']        = 'Update date';
$_['export_payment_code']        = 'Payment code';

// validate form - update status
$_['error_update_order_status_order_id']            = 'Error updating order status [Missing order id]';
$_['error_update_order_status_order_status_id']     = 'Error updating order status [Missing status id]';
$_['error_cancel_order_order_id']                   = 'Order cancellation error [Missing order id]';
$_['error_cancel_order_send_email']                 = 'Order cancellation error [Missing email sending option]';
$_['error_cancel_order_comment']                    = 'Order cancellation error [Note cannot be longer than% d characters]';
$_['error_email_exist']                             = 'Email has been used, please use another email!';
$_['error_phone_exist']                             = 'Phone has been used, please use another phone!';

$_['success_order_customer_info']         = 'Update order information successfully';
$_['error_order_customer_info']           = 'Change information failed';
$_['error_total_amount']                  = 'You must select a product';
$_['error_customer_phone']                = 'Missing customer phone number.';
$_['error_customer_phone_validate']       = 'Phone number is invalid.';
$_['error_customer_firstname']            = 'Missing Customer firstname.';
$_['error_customer_lastname']             = 'Missing Customer lastname.';
$_['error_customer_full_name']            = 'Full name not empty';
$_['error_customer_firstname_validate']   = 'Customer name can only enter letters and numbers.';
$_['error_customer_lastname_validate']    = 'The client&apos;s name can only enter letters and numbers.';
$_['error_customer_fullname_validate']    = 'Customer name is only alphanumeric.';
$_['error_customer_email']                = 'Missing customer email.';
$_['error_customer_email_validate']       = 'Email is malformed.';
$_['error_customer_province']             = 'Select province.';
$_['error_customer_district']             = 'Select district/district.';
$_['error_customer_wards']                = 'Choose a commune/ward';
$_['error_customer_address']              = 'Fill in the address';
$_['error_delivery_phone']                = 'Missing delivery phone number.';
$_['error_other_delivery_method_name']    = 'Missing method name.';

$_['order_history_create']                  = "Order created by";
$_['order_history_update']                  = "Update order by";
$_['order_history_change_status']           = "Update to status";

//invoice
$_['invoice_date_add_order']             = 'Order date';
$_['invoice_order_code']                 = 'Order code';
$_['invoice_order_store_url']            = 'Website';
$_['invoice_order_store_email']          = 'Email';
$_['invoice_order_customer_info']        = 'Customer information';
$_['invoice_order_customer_address']     = 'Shipping address';
$_['invoice_sku']                        = 'SKU';
$_['invoice_order_total']                = 'Total invoice order';
$_['invoice_order_shipping_fee']         = 'Shipping fee';
$_['invoice_order_real_total']           = 'Real total';

//order list
$_['update']                             = "Update";
$_['input_comment']                      = "Input comment";
$_['print_order']                        = "Print order";
$_['text_save']                          = "Save";
$_['order_fix_text']                     = 'Edit';
$_['order_by']                     = 'by';

$_['delete_order_text']                  = 'Delete order';
$_['order_status_delete']                = 'Canceled';

$_['success']                = 'Success';
$_['failure']                = 'Failure';
$_['not_copy']               = 'Cannot copy because the number of products in stock is too small';
$_['warning_shipping']                       = 'Missing delivery information, please fill in the shipping information.';
$_['lang_out_of_stock']                      = 'Product is out of stock or changed, please choose again';
$_['lang_add_tag']                           = "Add tag";
$_['txt_add']                                = "Add ";
$_['txt_enter_new_tag']                      = "Enter new tag";
$_['lang_remove_tag']                        = "Remove tag";
$_['remove_tag']                             = "Remove";
$_['currency']                               = "VND";
$_['lang_export_error']                              = "Export an error file. Please try again or contact Bestme for assistance.";
$_['lang_system_error_001']                          = "System error [Cannot get product information in order] [# 001]. Please try again later!";
$_['lang_system_error_002']                          = "System error [Cannot get product information in order] [# 002]. Please try again later!";
$_['lang_system_error']                              = "System error [Could not obtain product information in the order]. Please try again later!";
$_['text_choose_action']                             = "Choose action";
$_['lang_text_draft']            = 'Draft';
$_['lang_text_processing']  = 'Processing';
$_['lang_text_delivering']  = 'Delivering';
$_['lang_text_complete']    = 'Complete';
$_['lang_text_canceled']    = 'Canceled';

$_['transport_delivery']   = 'Delivery';
$_['transport_address_to_collect']   = 'Shipping address';
$_['transport_service']   = 'Service';
$_['transport_bill_of_lading']   = 'Bill of lading';
$_['transport_cod']   = 'Amount collection';
$_['transport_status_delivery']   = 'Transport status';
$_['transport_cancel_delivery']   = 'Cancel delivery';
$_['transport_weight']   = 'Total weight (gr)';
$_['transport_shipping_unit']   = 'Shipping unit';
$_['transport_benefit_package']   = 'Package service';
$_['transport_cod_cod']   = 'Amount collection (COD)';
$_['transport_delivery_preview']   = 'Lets see the goods before receiving';
$_['transport_length']   = 'Package length (cm)';
$_['transport_length_mobile']   = 'Length (cm)';
$_['transport_width']   = 'Package width (cm)';
$_['transport_width_mobile']   = 'Width (cm)';
$_['transport_height']   = 'Package height (cm)';
$_['transport_height_mobile']   = 'Height (cm)';
$_['transport_note']   = 'Note';
$_['transport_bill_custom']   = 'Bill of lading (optional)';
$_['transport_cancel']   = 'Ignore';
$_['transport_apply']    = 'Apply';
$_['transport_cancel_delivery']   = 'Cancel delivery';
$_['transport_you_are_cancel_delivery']   = 'Are you sure you want to cancel delivery for this application?';

$_['vtp_transport_error_address'] = "You need to enter the information of Ward/Commune, District/District, City/Province";
$_['vtp_transport_error'] = "Error not creating orders from ViettelPost";

$_['ghn'] = "Fast delivery";
$_['ghn_ReadyToPick'] = 'Shipping order has just been created';
$_['ghn_Picking'] = 'Shipper is coming to pick up the goods';
$_['ghn_Storing'] = 'The goods has been shipped to GHN sorting hub';
$_['ghn_Delivering'] = 'Shipper is delivering the goods to customer';
$_['ghn_Delivered'] = 'The goods has been delivered to customer';
$_['ghn_WaitingToFinish'] = 'Shipping order is waiting to transfer COD ';
$_['ghn_Finish'] = 'Shipping order has been finished';
$_['ghn_Return'] = 'The goods is waiting to return to seller/merchant after 3 times delivery failed';
$_['ghn_Returned'] = 'The goods has been returned to seller/merchant';
$_['ghn_LostOrder'] = 'The goods has been lost';
$_['ghn_Cancel'] = 'Shipping order has been cancelled';

$_['ghn_ready_to_pick'] = 'Shipping order has just been created';
$_['ghn_picking'] = 'Shipper is coming to pick up the goods';
$_['ghn_storing'] = 'The goods has been shipped to GHN sorting hub';
$_['ghn_delivering'] = 'Shipper is delivering the goods to customer';
$_['ghn_delivered'] = 'The goods has been delivered to customer';
$_['ghn_return'] = 'The goods is waiting to return to seller/merchant after 3 times delivery failed';
$_['ghn_returned'] = 'The goods has been returned to seller/merchant';
$_['ghn_lost'] = 'The goods are lost';
$_['ghn_cancel'] = 'Shipping order has been cancelled';

$_['ghn_money_collect_picking'] = 'Shipper are interacting with the seller';
$_['ghn_picked'] = 'Shipper is picked the goods';
$_['ghn_money_collect_delivering'] = 'Shipper is interacting with the buyer';
$_['ghn_delivery_fail'] = 'The goods hasn\'t been delivered to customer';
$_['ghn_waiting_to_return'] = 'The goods are pending delivery (can be delivered within 24/48h)';
$_['ghn_return_transporting'] = 'The goods are being rotated';
$_['ghn_return_sorting'] = 'The goods are being classified (at the warehouse classification)';
$_['ghn_returning'] = 'The shipper is returning for seller';
$_['ghn_exception'] = 'The goods exception handling';
$_['ghn_damage'] = 'Damaged goods';

$_['text_source']  =  "Source";
$_['admin']   = "Admin";
$_['shop']   = "Shop";
$_['chatbot']   = "Chatbot";
// v2.7.3
$_['txt_full_name']                         = 'Full name ';
$_['txt_classify']                          = 'Classify ';
$_['approved']   = "Wait for the goods";
$_['delivery_successful']   = "Successfully delivered";
$_['destroyed_spot']   = "Destruction on the spot";
$_['refund_successful']   = "Successful refund";
$_['cancel_ballot']   = "Cancel the coupon";
$_['cancel_bill']   = "Cancel the bill of lading";
$_['selected_all']   = "Selected all";

$_['viettel_post_approved']   = "Wait for the goods";
$_['viettel_post_delivery_successful']   = "Successfully delivered";
$_['viettel_post_destroyed_spot']   = "Destruction on the spot";
$_['viettel_post_refund_successful']   = "Successful refund";
$_['viettel_post_cancel_ballot']   = "Cancel the coupon";
$_['viettel_post_cancel_bill']   = "Cancel the bill of lading";
$_['btn_save_order_mobile']   = "Save";
$_['btn_back_to_order_list_mobile']   = "Back";
$_['btn_update_order_detail_mobile']   = "Update";
$_['remove_all_tag'] = 'Delete all tags';
$_['order_product_data_not_empty']       =  'Orders must exist product!';
$_['product_not_empty']       =  'Product no longer exists, please create order!';
$_['warning_product_changed']       =  'Product has been changed, please create order!';


// discount
$_['txt_discount_code']   = "Discount Code";
$_['txt_discount_name']   = "Discount Name";
$_['txt_discount_detail'] = "Detail";
$_['txt_discount']                            = "Discount";
$_['text_discount_col']                       = "Discount";
$_['txt_heading_detail']                      = 'Discount condition';
$_['txt_for_oder_from']                       = 'for order from';
$_['txt_for_oder_to']                         = 'to';
$_['txt_for_all_products']                    = 'all products';
$_['txt_for_per_product_if_oder_from']        = '/product if order from';
$_['txt_for_products_of_all_categories']      = 'products of all categories';
$_['txt_for_products_of_category']            = 'products of category';
$_['txt_for_products_of_all_manufacturers']   = 'products of all manufacturers';
$_['txt_for_products_of_manufacturer']        = 'products of manufacturer';
$_['txt_view_detail']                         = 'View detail';
$_['txt_hide_detail']                         = 'Hide detail';

// order source status
$_['txt_order_source_shop']                   = 'Website';
$_['txt_order_source_admin']                  = 'Administrator';
$_['txt_order_source_pos']                    = 'POS';
$_['txt_order_source_chatbot']                = 'Chat bot';
$_['txt_order_source_social']                 = 'Social';
$_['txt_order_source_lazada']                 = 'Lazada';
$_['txt_order_source_shopee']                 = 'Shopee';
$_['txt_order_source_tiki']                   = 'Tiki';

// v2.6.1
$_['text_update_order_status_of_shopee']           = 'If the order status is updated manually, the system cannot automatically synchronize the order status. Do you want to continue?';

// v2.7.3
$_['txt_full_name']                         = 'Full name ';
$_['txt_classify']                          = 'Classify ';

//v2.8.2
$_['text_message_order_status']              = "If the order status is updated manually, the system cannot automatically synchronize the transport status and order status. Do you want to continue?";

// v3.4.3
$_['txt_print_bill']                            = 'Print bill of lading';
$_['txt_transport_name']                        = 'Shipping company';
$_['txt_date_order']                            = 'Order date';
$_['txt_delivery_note']                         = 'Bill of lading';
$_['txt_order_code']                            = 'Order code';
$_['txt_sender']                                = 'Sender:';
$_['txt_receiver']                              = 'Receiver:';
$_['txt_collection_amount']                     = 'Collection amount:';

//
$_['txt_table_price_product']                       = 'Product unit price';
$_['txt_table_quantity_new']                        = 'Qty';
$_['txt_bill_of_sale']                              = 'Bill of sale';
$_['txt_phone_number_new']                          = 'Phone: ';
$_['txt_note_final_order']                          = '*Some products may be hidden due to the long list';

$_['txt_discount_by_voucher_code']                          = 'Discount by voucher';