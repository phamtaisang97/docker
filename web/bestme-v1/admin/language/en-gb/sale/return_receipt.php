<?php
// Heading
$_['heading_title']     = 'Return receipt';
$_['heading_title_top'] = 'Return receipt list';

// button
$_['btn_save_apply'] = 'Return';
$_['btn_cancel']     = 'Cancel';
$_['btn_go_back']    = 'Back';
$_['btn_action']     = 'Action';

// Text
$_['text_receipt_code']                = 'Return code';
$_['text_detail']                      = 'Return Receipt';
$_['text_create_receipt']              = 'Return Receipt';
$_['text_product']                     = 'Product';
$_['text_version']                     = 'version : ';
$_['text_price_return']                = 'Return item price';
$_['text_quantity']                    = 'Quantity';
$_['text_quantity_return']             = 'Return quantity';
$_['text_amount']                      = 'Total amount';
$_['text_total_product_return_amount'] = 'Total return amount ';
$_['text_fee_amount']                  = 'Return fee';
$_['text_total_amount']                = 'Total amount to pay';
$_['text_order_info']                  = 'Return order information';
$_['text_order_create_date']           = 'Return day';
$_['text_order_create_user']           = 'User';
$_['text_order_code']                  = 'Order code';
$_['text_order_amount']                = 'Order total amount';
$_['text_return_amount']               = 'Return amount';
$_['text_source']                      = 'Source';
$_['text_return_note']                 = 'Return note';
$_['text_customer']                    = 'Customer';
$_['text_customer_name']               = 'Customer name ';
$_['text_customer_phone']              = 'Customer phone';
$_['text_no_results']                  = 'No result!';
$_['text_return_reason']               = 'Return reason';
$_['text_return_date']                 = 'Return day';
$_['text_choose_store']                = "Choose store";

// filter
$_['filter_return_receipt']             = 'Filter return receipt';
$_['filter_by']                         = 'Filter by';
$_['filter_condition']                  = 'Choose filter condition';
$_['filter']                            = 'Filter';
$_['filter_by_receipt_code']            = 'Search by Return Receipt code';

// popup order list
$_['p_order_list']                      = 'Order list';
$_['p_search_placeholder']              = 'Search';
$_['p_filter_source']                   = 'Choose order source';
$_['p_filter_source_shop']              = 'Shop';
$_['p_filter_source_admin']             = 'Admin';

//placeholder
$_['placeholder_return_note'] = 'Enter return note';

//Error
$_['error_return_quantity_more_than_product_quantity'] = 'Return quantity must lesser than product quantity in order';
$_['error_return_quantity_empty']                      = 'Return quantity must greater than 0';
$_['error_return_fee']                                 = 'Return fee cannot more than Total return amount ';
$_['error_note']                                       = 'The note must be less than 255 characters long';
$_['error_order_not_found']                            = 'Order Not Found';
$_['error_all_product_is_returned']                    = 'All products in the order is returned';
$_['error_return_receipt_not_found']                   = 'Return receipt note found';

//source
$_['shop'] = 'Shop';
$_['pos'] = 'Pos';
$_['admin'] = 'Admin';
$_['chatbot'] = 'Chatbot';
$_['social'] = 'Social';

// Text chart
$_['options']                            = 'Options';
$_['monday']                             = 'Mo';
$_['tuesday']                            = 'Tu';
$_['wednesday']                          = 'We';
$_['thursday']                           = 'Th';
$_['friday']                             = 'Fr';
$_['saturday']                           = 'Sa';
$_['sunday']                             = 'Su';
$_['january']                            = 'January';
$_['february']                           = 'February';
$_['march']                              = 'March';
$_['april']                              = 'April';
$_['may']                                = 'May';
$_['june']                               = 'June';
$_['july']                               = 'July';
$_['august']                             = 'August';
$_['september']                          = 'September';
$_['october']                            = 'October';
$_['november']                           = 'November';
$_['december']                           = 'December';
$_['today']                              = 'Today';
$_['yesterday']                          = 'Yesterday';
$_['one_week']                           = 'One week';
$_['this_week']                          = 'This week';
$_['one_month']                          = 'One month';
$_['this_month']                         = 'This month';
$_['decimal_point']                      = ',';
$_['thousand_point']                     = '.';
$_['last_week']                          = 'Last week';
$_['last_month']                         = 'Last month';

//
$_['text_success_add']                         = 'Successful return!';