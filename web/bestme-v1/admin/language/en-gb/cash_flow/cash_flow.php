<?php

// Heading
$_['heading_title']                      = 'Overview';
// Title chart
$_['revenue']                            = 'Revenwe';
$_['number_of_orders']                   = 'Number of order';
$_['a_new_customer']                     = 'New customer';
$_['top_5_best_selling_products']        = 'Top 5 best selling products';
$_['see_details']                        = 'See details';
//message
$_['no_cash_flow_data_available']        = 'No cash flow data available.';
//text
$_['customer']                           = 'Customer';
$_['order']                              = 'order';
$_['amount']                             = 'amount';
$_['order_count']                        = 'order';
$_['currency']                           = 'đ';
$_['_currency']                          = '₫';
$_['_trieu']                             = ' đ';

$_['text_opening_balance']               = 'Opening balance';
$_['text_cash_inflows']                  = 'Cash Inflows';
$_['text_cash_outflows']                 = 'Cash Outflows';
$_['text_closing_balance']               = 'Closing balance';

// Text chart
$_['options']                            = 'Options';
$_['monday']                             = 'Mo';
$_['tuesday']                            = 'Tu';
$_['wednesday']                          = 'We';
$_['thursday']                           = 'Th';
$_['friday']                             = 'Fr';
$_['saturday']                           = 'Sa';
$_['sunday']                             = 'Su';
$_['january']                            = 'January';
$_['february']                           = 'Febuary';
$_['march']                              = 'March';
$_['april']                              = 'April';
$_['may']                                = 'May';
$_['june']                               = 'June';
$_['july']                               = 'July';
$_['august']                             = 'August';
$_['september']                          = 'September';
$_['october']                            = 'October';
$_['november']                           = 'November';
$_['december']                           = 'December';
$_['today']                              = 'Today';
$_['yesterday']                          = 'Yesterday';
$_['one_week']                           = 'One_week';
$_['this_week']                          = 'This week';
$_['one_month']                          = 'One_month';
$_['this_month']                         = 'This month';
$_['last_month']                         = 'Last month';
$_['decimal_point']                      = ',';
$_['thousand_point']                     = '.';
$_['places']                             = 'Places';
$_['amount_money']                       = 'Amount';

$_['text_cash_inflows_receipt_type']     = 'Cash Inflows chart by receipt type';
$_['text_cash_outflows_payment_type']    = 'Cash Outflows chart by payment type';
$_['text_cash_inflows_payment_method']   = 'Cash Inflows chart by payment method';
$_['text_cash_outflows_payment_method']  = 'Cash Outflows chart by payment method';

// breadcrumbs
$_['text_home']                           = 'Home';
$_['text_success_add']                    = 'Add new receipt voucher successfully!';
$_['text_success_edit']                   = 'Edit receipt voucher successfully!';
$_['breadcrumb_cash_flow']                = 'Cash flow';
$_['breadcrumb_overview']                 = 'Overview';