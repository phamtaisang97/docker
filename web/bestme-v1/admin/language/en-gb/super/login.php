<?php
// Heading
$_['heading_title']  = 'Manage Bestme'; 

// Text
$_['text_heading']   = 'Manage Bestme'; 
$_['text_login']     = 'Please enter login information.'; 
$_['text_forgotten'] = 'Forgot Password'; 

// Entry
$_['entry_username'] = 'Email';
$_['entry_password'] = 'Password';

// Button
$_['button_login']   = 'Login';

// Error
$_['error_login']    = 'Email and / or Password is incorrect.';
$_['error_token']    = 'The session is not available. Please log in again.'; 