<?php
// Heading
$_['heading_title']    = 'Picture manager';
$_['heading_title_image_manager']    = 'Image Management';
$_['heading_title_image']            = 'Image';

// Text
$_['text_uploaded']          = 'Success: Your photo has been uploaded! ';
$_['text_delete_title']      = 'Delete images selected';
$_['text_directory']         = 'Success: The folder has been created!';
$_['text_delete']            = 'Success!';
$_['text_confirm_delete_images']            = 'Are you sure delete  $numImage images?';

// Entry
$_['entry_search']     = 'Search image';
$_['entry_folder']     = 'More than 3 figures'; 

// Error
$_['error_permission'] = 'Warning: Right to be denied'; 
$_['error_filename']   = 'Warning: File name must be between 3-255 characters!'; 
$_['error_folder']     = 'Warning: folder name must be between 3-255 characters!'; 
$_['error_exists']     = 'Warning: the file or folder name has been used before!'; 
$_['error_directory']  = 'Warning: Folder does not exist!'; 
$_['error_filesize']   = 'Warning: The image size is too large '; 
$_['error_filetype']   = 'Warning: file type is incorrect (support: .jpg, .jpeg, .png, .ico)!';
$_['error_upload']     = 'Warning: Cannot upload photos for an unknown reason! '; 
$_['error_delete']     = 'Warning: You cannot delete this folder!';
$_['error_upload_default']     = 'Warning: Cannot upload photos for an unknown reason! (The image type must be : jpg, jpeg, png, ico. and file size < ';
$_['error_limit_capacity']     = 'Operation cannot be performed because you have used up the service capacity limit. Please contact us for further upgrades';
$_['warning_maximum_upload_images']    = 'Warning: Only upload up to 7 photos at a time';
$_['warning_maximum_upload_all_images']    = 'Warning: 1 account only allows upload of up to 1000 images, you can delete unused images to upload new images';


// new
$_['warning_for_num_of_images'] = 'The maximum number of images for 1 product is {max_images}. Allowed to choose more:';
$_['warning_for_single_select'] = 'Only select 1 image!';
$_['no_selected_to_delete']     = 'No photos selected to delete!';
$_['no_image_selected']         = 'No photos have been selected!';

// btn
$_['button_parent']                 = 'Parent folder'; 
$_['button_folder']                 = 'Create folder'; 
$_['button_apply']                  = 'Apply';
$_['ckeditor_original']                  = 'Do not adjust';
$_['view_more']                       = 'View image';
$_['ckeditor_imagesize']                       = 'Size photo';

//////image manager /////////////
$_['image_filter']                  = 'Image filter';
$_['image_filter_short']            = 'Filter';
$_['filter_list_follow']            = 'Filter image by';
$_['filter_image_status']           = 'Image status';
$_['filter_image_enable']           = 'Display';
$_['filter_image_disable']          = 'Hide';
$_['action_filter']                 = 'Filter';
$_['image_search']                  = 'Image search';
$_['choose_all_in_filter']                                 = "Selected all item in this page";
$_['choose_in_filter']                                     = "Selected";
$_['item_in_filter']                                       = "item";
$_['text_no_results']                                      = 'No results!';
$_['collection_list_filter_action']                        = 'Select action';
$_['choose_filter']           = "Select filter condition";
$_['filter_status']           = "Display status ";
$_['filter_status_placeholder']                  = "Select status";
$_['entry_status_publish']   = 'Display';
$_['entry_status_unpublish'] = 'Hide';
$_['txt_confirm_hide_images'] = 'Hide images will not be found at: Add product, collection, set up theme ...';
$_['txt_confirm_delete_images'] = 'WARNING: Deleting images here will not be restored!';
$_['txt_confirm_show_images'] =   'Do you want to display the selected images?';
$_['text_not_selected_yet'] = 'No photos have been selected!';
$_['text_success_show']     = 'Show images successfully!';
$_['text_success_hide']     = 'Hide images successfully!';
$_['text_success_delete']   = 'Delete images successfully!';
$_['btn_add_image'] = 'Add images';
$_['btn_hide_image'] = 'Hide';
$_['btn_show_image'] = 'Show';
$_['btn_delete_image'] = 'Delete';

/* v2.10.1: folder */
$_['btn_add_folder'] = 'Add Folder';
$_['btn_cancel'] = 'Cancel';
$_['btn_cancel_sort'] = 'Cancel';
$_['btn_dismiss'] = 'Dismiss';
$_['btn_create'] = 'Create';
$_['btn_agree'] = 'Agree';
$_['btn_save'] = 'Save';
$_['txt_confirm'] = 'Confirm';
$_['txt_modal_add_folder_title'] = 'Add new folder';
$_['txt_modal_edit_folder_title'] = 'Edit folder';
$_['warning_missing_folder_name'] = 'Please enter folder name';
$_['txt_success_add_folder'] = 'Add folder successfully';
$_['txt_success_edit_folder'] = 'Edit folder successfully';
$_['label_folder_name'] = 'Folder name';
$_['label_edit_folder_name'] = 'Enter folder name';
$_['placeholder_edit_folder_name'] = 'Enter folder name here';
$_['action_show_folder_image'] = 'Unhide folder/ image';
$_['action_hide_folder_image'] = 'Hide folder/ image';
$_['action_delete_folder_image'] = 'Delete folder/ image';
$_['action_move_folder_image'] = 'Move folders/images';
$_['txt_hide_folder_image'] = 'Hidden folders/images will not be found in: Add product, collection, theme setting,...';
$_['txt_show_folder_image'] = 'Do you want to display the selected folders/images?';
$_['txt_delete_folder_image'] = 'Warning: Deleted folder/image will not be recovered.';
$_['txt_success_hide'] = 'Hide folder/image successfully';
$_['txt_success_show'] = 'Display folder/image successfully';
$_['txt_success_delete'] = 'Delete folder/image successfully';
$_['txt_success_move'] = 'Move folder/image successfully';
$_['txt_move_folder_image'] = 'Move folders/images';
$_['txt_choose_folder_move'] = 'Choose the folder you want to move to';
$_['txt_search_folder_move'] = 'Find the folder you want to move to';
$_['txt_not_choose_move_folder'] = 'Please select folder to move to';
$_['warning_exists_folder_name'] = 'Folder name is already exists';
$_['root_folder_name'] = '--Undistributed folder--';
$_['txt_cannot_move_to_child'] = 'You can not move a folder into its own subdirectory';
$_['txt_no_more_folders'] = 'No more folders';

$_['choose'] = "Selected";
$_['item'] = "item";
$_['text_failure'] = 'Failure';
$_['select2_notice_not_result'] = "No match found";
$_['select2_notice_search'] = "Search ...";
$_['select2_notice_load_more'] = "Load ...";
$_['txt_folder'] = 'Folder';