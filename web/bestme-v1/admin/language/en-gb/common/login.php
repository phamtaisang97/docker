<?php
// Heading
$_['heading_title']  = 'Manage Bestme';

// Text
$_['text_heading']   = 'Manage Bestme';
$_['text_login']     = 'Login information.';
$_['text_forgotten'] = 'Forgot password';

// Entry
$_['entry_username'] = 'Email';
$_['entry_password'] = 'Password';

// Button
$_['button_login']   = 'Login';

// Error
$_['error_login']    = 'Email and / or Password is incorrect.';
$_['error_token']    = 'The session is not available. Please log in again.';

// More
$_['warning_message']    = 'Wish you a good day! </br> Please perform a store administration login from <a href="'. BESTME_SERVER.'"> Bestme homepage </a>.';