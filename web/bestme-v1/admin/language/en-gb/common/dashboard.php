<?php
// Heading
$_['heading_title'] = 'Dashboard'; 

// Error
$_['error_install'] = 'Warning: There is an installation directory, you need to delete this folder because of security issues!'; 

//cart_title
$_['text_order_title']               = 'Total order';
$_['text_customer_title']            = 'Total customer';
$_['text_product_title']             = 'Total product ';
$_['text_top_products']              = 'Top product';
$_['text_business_report']           = 'Business report';
$_['text_lastest_orders']            = 'Lastest orders';
$_['text_view_statistics']           = 'View statisitcs >>';
$_['text_view_more']                 = 'View more';

//order column text
$_['column_id']                          = 'Order id';
$_['column_name']                        = 'Name';
$_['column_status_dashboard']            = 'Status';
$_['column_date_added_dashboard']        = 'Created date';
$_['column_total_dashboard']             = 'Total';
$_['column_view']                        = 'View';

//top product
$_['txt_exist']                          = 'Exist';
$_['txt_depot']                          = 'In stock';
$_['txt_total_sold']                     = 'Total sold';
$_['no_statistical_data_available']      = 'No statistical data yet. Start selling with ' . PRODUCTION_BRAND_NAME . ' Website and check out the statistics!';
$_['format_million']                     = 'million';
$_['format_billion']                     = 'billion';
$_['_trieu']                             = 'Million đ';

// Text chart
$_['options']                            = 'Options';
$_['monday']                             = 'Mo';
$_['tuesday']                            = 'Tu';
$_['wednesday']                          = 'We';
$_['thursday']                           = 'Th';
$_['friday']                             = 'Fr';
$_['saturday']                           = 'Sa';
$_['sunday']                             = 'Su';
$_['january']                            = 'January';
$_['february']                           = 'February';
$_['march']                              = 'March';
$_['april']                              = 'April';
$_['may']                                = 'May';
$_['june']                               = 'June';
$_['july']                               = 'July';
$_['august']                             = 'August';
$_['september']                          = 'September';
$_['october']                            = 'October';
$_['november']                           = 'November';
$_['december']                           = 'December';
$_['today']                              = 'Today';
$_['yesterday']                          = 'Yesterday';
$_['one_week']                           = 'One week';
$_['this_week']                          = 'This week';
$_['one_month']                          = 'One month';
$_['this_month']                         = 'This month';
$_['decimal_point']                      = ',';
$_['thousand_point']                     = '.';
$_['last_week']                          = 'Last week';
$_['last_month']                         = 'Last month';

// text dashboard new
$_['txt_hello']                                 = 'Hello';
$_['txt_all_stores']                            = 'All stores';
$_['txt_all_staffs']                            = 'All staffs';
$_['txt_price']                                 = 'Price';
$_['txt_revenue']                               = 'Revenue';
$_['txt_total_revenue']                         = 'Total Revenue';
$_['txt_des_total_revenue']                     = 'Total revenue of orders during selected date range, include all not paid orders.';
$_['txt_general_index']                         = 'Summary indexes';
$_['txt_total_order']                           = 'Total orders';
$_['txt_des_total_order']                       = 'Total orders that had been created during selected date range, include cancelled orders (without draft orders)';
$_['txt_total_visits']                          = 'Total visits';
$_['txt_des_total_visits']                      = 'Total page views/visits website during selected date range. One person viewed some pages only makes 1 page view/visit.';
$_['txt_total_customer_by']                     = 'Total bought customers';
$_['txt_des_total_customer_by']                 = 'Total customers made orders during selected date range.';
$_['txt_conversion_rate']                       = 'Conversion rate';
$_['txt_des_conversion_rate']                   = 'Rate of converting visited customers to bought customers';

$_['txt_current_status']                        = 'Current statistic';
$_['txt_current_status_product']                = 'products';
$_['txt_current_status_out_of_stock']           = 'Out of stock';
$_['txt_current_status_order']                  = 'orders';
$_['txt_current_status_order_processing']       = 'Processing';
$_['txt_current_status_order_delivering']       = 'Shipping';
$_['txt_view_detail']                           = 'View detail';

$_['txt_product_of_the_highest']                = 'Top products by revenue';
$_['txt_table_rank']                            = 'Rank';
$_['txt_table_product']                         = 'Product';
$_['txt_table_revenue']                         = 'Revenue';
$_['txt_no_product']                            = 'Top products by revenue has no data';
$_['txt_product_of_the_highest']                = 'Top products by revenue';
$_['txt_table_rank']                            = 'Rank';
$_['txt_table_product']                         = 'Product';
$_['txt_table_revenue']                         = 'Revenue';
$_['txt_no_product']                            = 'Top products by revenue has no data';

$_['voucher_not_user']                    =       "You have an unused voucher!";
$_['you_when_you_start_to_eat']           =       "Before starting, we need to get to know each other first.";
$_['gift_when_success']                   =       "A gift will be for you once you finish the on-boarding with";
$_['step']                                =       "steps";
$_['get_acquainted_below']                =       "to get acquainted below!";
$_['you_ready']                           =       "You are ready for business!";
$_['only']                                =       "Only remain";
$_['step_start_business']                 =       "step(s) to start business";
$_['add_new_product']                     =       "Add product";
$_['customize_interface']                 =       "Customize theme";
$_['attack_domain_name']                  =       "Add domain";
$_['add_the_first_product_to_the_store']  =       "Add first product to store";
$_['fill_out_product_information']        =       "Fill all information of product to satisfy the need for details of shoppers.";
$_['add_product']                         =       "Add product";
$_['set_up_a_website_interface']          =       "Customize theme fit you need";
$_['you_can_add_logo']                    =       "You can change logo, change primary color, ... of your website as you want.";
$_['reinforce_brand_with_concise_domain_name']          =       "Reinforce brand with concise domain";
$_['add_a_new_domain_name_to_make_it']                  =       "Add a new domain to make it easier for customers to remember your brand.";
$_['you_have_completed_the_Bestme_user_guide']          =       "You have completed the " . PRODUCTION_BRAND_NAME . " user guide!";
$_['bestme_for_use']                                    =       "" . PRODUCTION_BRAND_NAME . " offers you an extra 2 months free of charge when you buy any business plan.";
$_['month_free_use']                                    =       "Get 02 months of free use";
$_['getting_a_successful_gift']                         =       "Congratulations on getting a successful gift!";
$_['please_contact']                                    =       "Please contact " . PRODUCTION_BRAND_NAME . " at hotline";
$_['provider_voucher_use']                              =       "and provide the following code to use:";
$_['thanks_use_bestme']                                 =       "Thank you for choosing to use " . PRODUCTION_BRAND_NAME . "! Hope you continue to accompany " . PRODUCTION_BRAND_NAME . " on the path of developing an online business!";
$_['continue_use']                                      =       "Continue using";
$_['gift_when_success_free']                            =  "Complete the guide to get free 02 months using the package free";
$_['getting_a_successful_gift_mobile']                  =  "Received gifts successfully!";
$_['text_product_deleted']                              =  "This product was deleted";
$_['text_product_changed']                              =  "This product was changed";
// onboarding gift
// onboarding gift
if (defined('SOURCE_PRODUCTION_FOR_ADG') && SOURCE_PRODUCTION_FOR_ADG) {
    $_['txt_onboarding_gitf']                               =  "Free business documents";
} else {
    $_['txt_onboarding_gitf']                               =  "Free online business documents";
}

$_['txt_sample_data'] = "When upgrading to paid plans, all demo data will be deleted";