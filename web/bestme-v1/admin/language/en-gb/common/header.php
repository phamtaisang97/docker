<?php
// Heading
$_['heading_title']      = 'Store';

// Text
$_['text_welcome']             = 'Welcome';
$_['text_profile']             = 'Your profile';
$_['text_store']               = 'Stores';
$_['text_help']                = 'Help';
$_['text_homepage']            = 'Store homepage';
$_['text_support']             = 'Forum support';
$_['text_documentation']       = 'document';
$_['text_logout']              = 'Logout';
$_['delete_confirm_common']           = 'Do you want to delete ?';
$_['copy_confirm_common']             = 'Do you want to copy ?';
$_['view_store']               = 'View Website';
$_['language_vi']              = 'Vietnamese';
$_['language_en']              = 'English';
$_['config_account']           = 'Account configuration';
$_['log_out']                  = 'Logout';
$_['change_password']           = 'Change password';
$_['trial']             = 'trial';
$_['txt_package']       = 'Package';
$_['txt_use_sample_data'] = 'Use demo data';
$_['txt_delete_sample_data'] = 'Delete demo data';
$_['txt_delete_sample_data_successfully'] = 'Delete demo data successfully';
$_['alert_title'] = 'Notify';

// Novaon common products
$_['txt_title_product_novaon_digital_marketing']            =  "DIGITAL MARKETING";
$_['txt_product_novaon_autoads_maxlead']                    =  "AutoAds MaxLead";
$_['txt_product_novaon_autoads_maxpush']                    =  "AutoAds MaxPush";
$_['txt_product_novaon_google_shopping']                    =  "Google Shopping";
$_['txt_product_novaon_cfp']                                =  "Block click fraud";
$_['txt_product_novaon_on_google']                          =  "On-Google Service";
$_['txt_product_novaon_oncustomer']                             =  "OnCustomer";

$_['txt_title_product_novaon_ecom']                         =  "E-COMMERCE";
$_['txt_product_novaon_bestme_website']                     =  "" . PRODUCTION_BRAND_NAME . " Website";
$_['txt_product_novaon_bestme_pos']                         =  "" . PRODUCTION_BRAND_NAME . " POS";
$_['txt_product_novaon_bestme_platform']                    =  "" . PRODUCTION_BRAND_NAME . " Platform";
$_['txt_product_novaon_novaonx_chatbot']                    =  "NovaonX Chatbot";
$_['txt_product_novaon_novaonx_social']                     =  "NovaonX Social";
$_['txt_product_novaon_onfluencer_review']                  =  "Onfluencer Review";
$_['txt_product_novaon_onfluencer_seller']                  =  "Onfluencer Seller";

$_['txt_title_product_novaon_digital_trans']                =  "DIGITAL TRANSFORMATION";
$_['txt_product_novaon_onsales_crm']                        =  "Onsales CRM";
$_['txt_product_novaon_onsales_caller']                     =  "Onsales Caller";
$_['txt_product_novaon_onfinance_invoice']                  =  "Onfinance Invoice";
$_['txt_product_novaon_onfinance_onsign']                   =  "Onfinance OnSign";
$_['txt_product_novaon_onpeople_hrm']                       =  "Onpeople HRM";

$_['txt_wait_for_payment_verification']                     =  "Wait for payment verification";