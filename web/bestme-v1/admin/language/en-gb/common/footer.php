<?php
// Text
$_['text_footer']  = '<a href="' . BESTME_SERVER . '">Bestme</a> &copy; 2018-' . date('Y') . ' All Rights Reserved.';
$_['text_version'] = 'Version %s';
$_['alert_title'] = "Alert";
$_['alert_close'] = "ok";
$_['you_still']   =     'You still';
$_['trial_date_lang']   =  'trial date';
$_['choose_a_usage_package']  = "Choose a usage package";
$_['you_are_out_of_trial']        =    'You are out of trial';
$_['confirm_box_title']        =    'Confirm';
$_['confirm_box_btn_yes']        =    'Yes';
$_['confirm_box_btn_no']         =    'No';

$_['txt_packet_wait_for_pay_in_time']           =    'We will contact you to verify payment for the package within %date% days';
$_['txt_packet_wait_for_pay_out_time']          =    'Out of payment verification time. Please contact hotline ' . PRODUCTION_BRAND_PHONE . ' for support.';