<?php
// Heading
$_['heading_title']  = 'Reset your password ';

// Text
$_['text_password']  = 'Please enter your new password.';
$_['text_success']   = 'Success: Your password has been successfully changed.';

// Entry
$_['entry_password'] = 'Password';
$_['entry_confirm']  = 'Confirm password';

// Error
$_['error_password'] = 'Password length must be between 4 and 20 characters!';
$_['error_confirm']  = 'Password and Confirm password does not match!';

// More
$_['error_disabled_forgotten']       = 'Currently the forgotten password feature is not supported (ERROR: 0). Please log in!';
$_['error_invalid_activation_link']  = 'The new password activation link is no longer valid. Please resend the password reset request!';
