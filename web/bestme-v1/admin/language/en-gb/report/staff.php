<?php
// Heading
$_['heading_title'] = 'Reports';

// Text
$_['text_success'] = 'Success: You have modified reports!';
$_['text_list']    = 'Report List';
$_['text_type']    = 'Choose the report type';
$_['text_filter']  = 'Filter';

// report by product
$_['text_report']                = 'Report';
$_['text_sales_report']          = 'Sales report';
$_['text_sales_report_by_staff'] = 'Sales report by staff';
$_['text_product']               = 'Product';
$_['text_quantity_sold']         = 'Quantity';
$_['text_amount']                = 'Amount';
$_['text_discount']              = 'Discount';
$_['text_quantity_return']       = 'Quantity return';
$_['text_return_amount']         = 'Return amount';
$_['text_revenue']               = 'Revenue';
$_['text_total']                 = 'Total';
$_['text_export']                = 'Export excel';
$_['text_all_stores']            = 'All store';
$_['text_staff_name']            = 'Staff';
$_['text_total_order']           = 'Orders';
$_['text_shipping_fee']          = 'Shipping';
$_['export_file_name']           = 'sale_report_by_staff';
$_['text_export_title']          = 'Sale Report By Staff';
$_['text_export_store']          = 'Store: ';
$_['text_export_time']           = 'time: ';

// date picker
$_['options']                            = 'Options';
$_['monday']                             = 'Mo';
$_['tuesday']                            = 'Tu';
$_['wednesday']                          = 'We';
$_['thursday']                           = 'Th';
$_['friday']                             = 'Fr';
$_['saturday']                           = 'Sa';
$_['sunday']                             = 'Su';
$_['january']                            = 'January';
$_['february']                           = 'Febuary';
$_['march']                              = 'March';
$_['april']                              = 'April';
$_['may']                                = 'May';
$_['june']                               = 'June';
$_['july']                               = 'July';
$_['august']                             = 'August';
$_['september']                          = 'September';
$_['october']                            = 'October';
$_['november']                           = 'November';
$_['december']                           = 'December';
$_['today']                              = 'Today';
$_['yesterday']                          = 'Yesterday';
$_['one_week']                           = 'One_week';
$_['this_week']                          = 'This week';
$_['last_week']                          = 'Last week';
$_['one_month']                          = 'One month';
$_['this_month']                         = 'This month';
$_['last_month']                         = 'Last month';
$_['decimal_point']                      = ',';
$_['thousand_point']                     = '.';
$_['places']                             = 'Places';
$_['amount_money']                       = 'Amount';