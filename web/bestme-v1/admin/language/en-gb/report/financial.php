<?php

// Heading
$_['heading_title']         = 'Financial report';
$_['heading_title_2']       = 'Financial report';

// Text chart
$_['options']                            = 'Options';
$_['monday']                             = 'Mo';
$_['tuesday']                            = 'Tu';
$_['wednesday']                          = 'We';
$_['thursday']                           = 'Th';
$_['friday']                             = 'Fr';
$_['saturday']                           = 'Sa';
$_['sunday']                             = 'Su';
$_['january']                            = 'January';
$_['february']                           = 'Febuary';
$_['march']                              = 'March';
$_['april']                              = 'April';
$_['may']                                = 'May';
$_['june']                               = 'June';
$_['july']                               = 'July';
$_['august']                             = 'August';
$_['september']                          = 'September';
$_['october']                            = 'October';
$_['november']                           = 'November';
$_['december']                           = 'December';
$_['today']                              = 'Today';
$_['yesterday']                          = 'Yesterday';
$_['one_week']                           = 'One_week';
$_['this_week']                          = 'This week';
$_['last_week']                          = 'Last week';
$_['one_month']                          = 'One_month';
$_['this_month']                         = 'This month';
$_['last_month']                         = 'Last month';

// Breadcrumb
$_['breadcrumb_report']   = 'Report';

//
$_['text_no_results'] = 'No result!';

// text
$_['text_total']                                = 'Total';
$_['text_report']                               = 'Report';
$_['text_export']                               = 'Export';
$_['text_notes']                                = 'Notes';
$_['text_report_notes']                         = 'Report notes';
$_['text_financial_report_notes']               = 'Financial report notes';
$_['text_all_stores']                           = 'All stores';
$_['text_store']                                = 'Store';
$_['text_current_period']                       = 'Current period';
$_['text_compare_period']                       = 'Compare period';
$_['text_previous_period']                      = 'Previous period';
$_['text_financial_spending']                   = 'Financial indicators';
$_['text_change']                               = 'Changed(%)';
$_['text_revenue']                              = 'Sales revenue (1)';
$_['text_revenue_reduce']                       = 'Revenue deductions (2 = 2.1 + 2.2)';
$_['text_trade_discount']                       = 'Trade discounts (2.1)';
$_['text_total_return']                         = 'Return amount (2.2)';
$_['text_net_revenue']                          = 'Net revenue (3 = 1 - 2)';
$_['text_cost']                                 = 'Costs of goods sold  (4)';
$_['text_gross_revenue']                        = 'Gross revenues (5 = 3 - 4)';
$_['text_fee']                                  = 'Expenses (6 = 6.1 + 6.2)';
$_['text_delivery_for_partner']                 = 'Delivery fee paid to the partner (6.1)';
$_['text_pay_salary']                           = 'Pay salary for staff (6.2)';
$_['text_other_fee']                            = 'Other expenses (7)';
$_['text_profit']                               = 'Net profit from operations  (8 = 5 - 6 -7)';
$_['text_other_income']                         = 'Other income (9 = 9.1 + 9.2)';
$_['text_manual_receipt']                       = 'Manual receipt vouche (9.1)';
$_['text_return_fee']                           = 'Return fee (9.2)';
$_['text_enterprise_income_tax_expense']        = 'Enterprise income tax expense (10)';
$_['text_net_income']                           = 'Profits after enterprise income tax (11 = 8 + 9 - 10)';


$_['text_note_1']                                   = 'Total price (price*quantity) and shipping fee of orders (except Draft orders and Cancelled orders)';
$_['text_note_2_1']                                 = 'Total ( discount product + discount order) of orders (minus Draft and Canceled status)';
$_['text_note_2_2']                                 = 'Total amount of all return receipt';
$_['text_note_3']                                   = 'Actual store revenue that used to calculate net profit';
$_['text_note_4']                                   = 'Total (cost * quantity product) of orders (minus Draft and Canceled status)';
$_['text_note_5']                                   = 'Profit obtained after deducting the cost of goods sold';
$_['text_note_6_1']                                 = 'The total shipping charge for the orders and taken from the check is of the type of Payments for shipping partner';
$_['text_note_6_2']                                 = 'Retrieved from the check has category of Labor cost';
$_['text_note_7']                                   = 'Accounting checks with accounting results and has different types of following types: automatic expenses, Payment of shipping partner\'s debt, Labor cost and CIT expense.';
$_['text_note_8']                                   = 'Profits earned from business operations';
$_['text_note_9_1']                                 = 'Receipts with accounting of business results and other types of automatic collection';
$_['text_note_9_2']                                 = 'The total return charge on the returns';
$_['text_note_10']                                  = 'The total of payment checks included Profit tax expense';
$_['text_note_11']                                  = 'The store\'s actual profit after subtracting all costs.';

//
$_['lang_export_error']                              = "Xuất file lỗi. Vui lòng thử lại hoặc liên hệ Bestme để được trợ giúp.";