<?php

// Heading
$_['heading_title'] = 'Summary sales report';

// Text daterangepicker
$_['options']                            = 'Options';
$_['monday']                             = 'Mo';
$_['tuesday']                            = 'Tu';
$_['wednesday']                          = 'We';
$_['thursday']                           = 'Th';
$_['friday']                             = 'Fr';
$_['saturday']                           = 'Sa';
$_['sunday']                             = 'Su';
$_['january']                            = 'January';
$_['february']                           = 'February';
$_['march']                              = 'March';
$_['april']                              = 'April';
$_['may']                                = 'May';
$_['june']                               = 'June';
$_['july']                               = 'July';
$_['august']                             = 'August';
$_['september']                          = 'September';
$_['october']                            = 'October';
$_['november']                           = 'November';
$_['december']                           = 'December';
$_['today']                              = 'Today';
$_['yesterday']                          = 'Yesterday';
$_['one_week']                           = 'One week';
$_['this_week']                          = 'This week';
$_['this_month']                         = 'This month';
$_['last_week']                          = 'Last week';
$_['last_month']                         = 'Last month';

// Breadcrumb
$_['breadcrumb_report']   = 'Report';
$_['breadcrumb_report_order'] = 'Sales report';

//
$_['text_no_results'] = 'No results!';

// column table
$_['text_column_date']                          = 'Date';
$_['text_column_order_buy']                     = 'Orders';
$_['text_column_total_amount']                  = 'Amount';
$_['text_column_discount']                      = 'Discount';
$_['text_column_shipping_fee']                  = 'Shipping fee';
$_['text_column_order_return']                  = 'Returned orders';
$_['text_column_money_back']                    = 'Return amount';
$_['text_column_revenue']                       = 'Revenue';
$_['export_file_name']                          = 'Summary sales report';

// text
$_['text_total']                                        = 'Total';
$_['text_excel_store']                                  = 'Store : ';
$_['text_excel_time']                                   = 'Time : ';

$_['text_all_store']                                    = 'All stores';
$_['text_export_report']                                = 'Export report';

//
$_['lang_export_error']                         = "Export an error file. Please try again or contact Bestme for assistance.";