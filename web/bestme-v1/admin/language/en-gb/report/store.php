<?php
// Heading
$_['heading_title']         = 'Sales report by store';
$_['heading_title_2']       = 'Sales report';

// Text chart
$_['options']                            = 'Options';
$_['monday']                             = 'Mo';
$_['tuesday']                            = 'Tu';
$_['wednesday']                          = 'We';
$_['thursday']                           = 'Th';
$_['friday']                             = 'Fr';
$_['saturday']                           = 'Sa';
$_['sunday']                             = 'Su';
$_['january']                            = 'January';
$_['february']                           = 'Febuary';
$_['march']                              = 'March';
$_['april']                              = 'April';
$_['may']                                = 'May';
$_['june']                               = 'June';
$_['july']                               = 'July';
$_['august']                             = 'August';
$_['september']                          = 'September';
$_['october']                            = 'October';
$_['november']                           = 'November';
$_['december']                           = 'December';
$_['today']                              = 'Today';
$_['yesterday']                          = 'Yesterday';
$_['one_week']                           = 'One_week';
$_['this_week']                          = 'This week';
$_['last_week']                          = 'Last week';
$_['one_month']                          = 'One_month';
$_['this_month']                         = 'This month';
$_['last_month']                         = 'Last month';

$_['text_no_results'] = 'No result!';

// column table
$_['text_column_date']                          = 'Date';
$_['text_column_store']                         = 'Store';
$_['text_column_order_buy']                     = 'Orders';
$_['text_column_total_amount']                  = 'Amount';
$_['text_column_discount']                      = 'Discount';
$_['text_column_shipping_fee']                  = 'Shipping fee';
$_['text_column_order_return']                  = 'Returned orders';
$_['text_column_money_back']                    = 'Return amount';
$_['text_column_revenue']                       = 'Revenue';
$_['export_file_name']                          = 'Summary sales report';

// text
$_['text_total']                                = 'Total:';
$_['text_report']                               = 'Report';
$_['text_export']                               = 'Export';
$_['text_back']                                 = 'Back';

//
$_['lang_export_error']                         = "Xuất file lỗi. Vui lòng thử lại hoặc liên hệ Bestme để được trợ giúp.";
