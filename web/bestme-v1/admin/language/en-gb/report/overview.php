<?php

// Heading
$_['heading_title'] = 'Synthesis report';
// Title chart
$_['text_revenue_chart_title']    = 'Chart of revenue and profit of the whole system';
$_['number_of_orders']            = 'Number of order';
$_['a_new_customer']              = 'New customer';
$_['top_5_best_selling_products'] = 'Top 5 best selling products';
$_['see_details']                 = 'See details';
//message
$_['no_statistical_data_available'] = 'No statistics available. Start selling with ' . PRODUCTION_BRAND_NAME . ' Website and check the parameters!';
//text
$_['customer']    = 'Customer';
$_['order']       = 'order';
$_['amount']      = 'amount';
$_['order_count'] = 'order';
$_['currency']    = 'đ';
$_['_currency']   = '₫';
$_['_trieu']      = ' đ';

// Text chart
$_['options']        = 'Options';
$_['monday']         = 'Mo';
$_['tuesday']        = 'Tu';
$_['wednesday']      = 'We';
$_['thursday']       = 'Th';
$_['friday']         = 'Fr';
$_['saturday']       = 'Sa';
$_['sunday']         = 'Su';
$_['january']        = 'January';
$_['february']       = 'Febuary';
$_['march']          = 'March';
$_['april']          = 'April';
$_['may']            = 'May';
$_['june']           = 'June';
$_['july']           = 'July';
$_['august']         = 'August';
$_['september']      = 'September';
$_['october']        = 'October';
$_['november']       = 'November';
$_['december']       = 'December';
$_['today']          = 'Today';
$_['yesterday']      = 'Yesterday';
$_['one_week']       = 'One_week';
$_['this_week']      = 'This week';
$_['last_week']      = 'Last week';
$_['one_month']      = 'One month';
$_['this_month']     = 'This month';
$_['decimal_point']  = ',';
$_['thousand_point'] = '.';
$_['places']         = 'Places';
$_['amount_money']   = 'Amount';

$_['text_revenue']                   = 'Revenue';
$_['text_sales']                     = 'Sales';
$_['text_order']                     = 'Orders';
$_['text_profit']                    = 'Profit';
$_['text_average_revenue_per_order'] = 'Average sales per order';
$_['text_revenue_rate_from_sources'] = 'Revenue rate from all sources';
$_['text_profit_rate_from_sources']  = 'Profit rate from all sources';
$_['text_title_top_staff']           = 'Top staff with highest sales';
$_['text_title_top_customer']        = 'Top customers spend the most';
$_['text_all_stores']                = 'All store';
$_['text_staff']                     = 'Staff Name';
$_['text_account']                   = 'Staff Account';
$_['text_customer']                  = 'Customer';
$_['text_phone']                     = 'Phone';

// Breadcrumb
$_['breadcrumb_report']   = 'Report';
$_['breadcrumb_overview'] = 'Overview';
