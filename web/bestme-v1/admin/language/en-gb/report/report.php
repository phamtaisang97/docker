<?php
// Heading
$_['heading_title'] = 'Reports';

// Text
$_['text_success']  = 'Success: You have modified reports!';
$_['text_list']     = 'Report List';
$_['text_type']     = 'Choose the report type';
$_['text_filter']   = 'Filter';

// report by product
$_['text_store']                    = 'Store';
$_['text_store_all']                = 'All stores';
$_['text_time']                     = 'Time';
$_['text_report']                   = 'Reports';
$_['text_sales_report']             = 'Sales Report';
$_['text_sales_report_by_product']  = 'Sales report by product';
$_['text_product']                  = 'Product';
$_['text_quantity_sold']            = 'Quantity sold';
$_['text_amount']                   = 'Amount';
$_['text_discount']                 = 'Discount';
$_['text_quantity_return']          = 'Quantity return';
$_['text_return_amount']            = 'Return amount';
$_['text_revenue']                  = 'Revenue';
$_['text_total']                    = 'Total';
$_['text_export']                   = 'Export';
$_['text_all_stores']               = 'All stores';
$_['export_file_name']              = 'sales_report_by_product';

// Text chart
$_['options']                            = 'Options';
$_['monday']                             = 'Mo';
$_['tuesday']                            = 'Tu';
$_['wednesday']                          = 'We';
$_['thursday']                           = 'Th';
$_['friday']                             = 'Fr';
$_['saturday']                           = 'Sa';
$_['sunday']                             = 'Su';
$_['january']                            = 'January';
$_['february']                           = 'Febuary';
$_['march']                              = 'March';
$_['april']                              = 'April';
$_['may']                                = 'May';
$_['june']                               = 'June';
$_['july']                               = 'July';
$_['august']                             = 'August';
$_['september']                          = 'September';
$_['october']                            = 'October';
$_['november']                           = 'November';
$_['december']                           = 'December';
$_['today']                              = 'Today';
$_['yesterday']                          = 'Yesterday';
$_['one_week']                           = 'One_week';
$_['this_week']                          = 'This week';
$_['last_week']                          = 'Last week';
$_['one_month']                          = 'One month';
$_['this_month']                         = 'This month';
$_['last_month']                         = 'Last month';
$_['decimal_point']                      = ',';
$_['thousand_point']                     = '.';
$_['places']                             = 'Places';
$_['amount_money']                       = 'Amount';