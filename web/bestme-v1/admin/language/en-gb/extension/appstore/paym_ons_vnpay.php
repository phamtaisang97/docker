<?php

// Heading
$_['heading_title'] = 'VNPay';
$_['heading_sub_title'] = 'Enter the website code and checksum generation string in the corresponding box, click Connect to complete the setup and click Skip to cancel the setting.';
$_['heading_title_description'] = 'VNPay';
$_['heading_title_setting'] = 'System VNPay App';

//text
$_['text_vnpay_code_placeholder'] = 'Enter VNPay code';
$_['text_success'] = 'Success!';
$_['text_go_to_app'] = 'Enter App';
$_['vnp_web_code_text'] = 'Website code';
$_['vnp_hash_secret_text'] = 'Secret text generate the checksum';
$_['not_empty'] = 'Please fill in this field';
$_['max_length'] = 'This field must be less than 80 characters long';
$_['special_characters'] = 'This field can only consist of alphabetical and number';
$_['alert_success'] = 'Update VNPay account success';
$_['btn_connect'] = 'Connect';
$_['btn_update'] = 'Update';

$_['create_vn_pay_title'] = 'Create account merchant VNPay';
$_['vn_pay_text'] = 'VNPay';
$_['create_vn_pay_description'] = 'If you have not registered a Merchant VNPay account then click the Create new account button.<br>
If you already have a merchant VNPay account, please prepare the website code and the checksum generator string to check the configuration to configure below.';
$_['vn_pay_login'] = 'Login merchant VNPay';
$_['create_vn_pay_btn'] = 'Create new account';