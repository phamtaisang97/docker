<?php
// Heading
$_['heading_title'] = 'Config theme for app module';

// Text
$_['text_module'] = 'Modules';
$_['text_success'] = 'Success: You have modified module Flash Sale !';
$_['text_content_top'] = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left'] = 'Column Left';
$_['text_column_right'] = 'Column Right';
$_['text_btn_update'] = "Update";
// Entry
$_['entry_theme'] = 'Chose theme:';
$_['entry_page'] = 'Chose Page:';
$_['entry_position'] = 'Position:';
$_['entry_status'] = 'Status:';
$_['entry_priority'] = 'Priority:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify module Flash Sale!';
$_['error_code'] = 'Code Required';

