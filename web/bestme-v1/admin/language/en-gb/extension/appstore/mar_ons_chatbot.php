<?php

// Heading
$_['heading_title']                      = 'Chatbot';

// Text
$_['text_intro_title']                   = 'Connect to NovaonX Chatbot';
$_['text_intro_content']                 = 'Chatbot helps you increase sales, save advertising costs, and better take care of customers right from the first interaction. To use Chatbot, you need to sign up for a NovaonX Chatbot account.';

$_['text_intro_button']                  = 'Use Chatbot';

$_['text_setting_title']                 = 'Connect to Chatbot';
$_['text_setting_content']               = 'Allows creating orders right on Chatbot, and centrally managed at ' . PRODUCTION_BRAND_NAME . ' .';
$_['text_setting_api_key_title']         = 'Connection Api key';
$_['text_setting_api_key_placeholder']   = 'ck_24772a8e3b1b47996911a3c73377f219dde41595';
$_['text_setting_api_key_example']       = 'Provide the above code for Chatbot to connect your store with Chatbot';
$_['text_setting_api_key_button']        = 'Copy';

$_['text_more']                          = 'View guide';

$_['text_api_key_copy_button']           = 'Copy';

$_['text_api_key_copy_success']          = 'Copied';
$_['text_api_key_copy_failed']           = 'Error! Could not copy';
