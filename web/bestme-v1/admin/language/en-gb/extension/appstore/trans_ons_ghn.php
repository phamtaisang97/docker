<?php
$_['ghn_code_ready_to_pick']                  = "Shipping order has just been created";
$_['ghn_code_picking']                        = "Shipper is coming to pick up the goods";
$_['ghn_code_cancel']                         = "Shipping order has been cancelled";
$_['ghn_code_money_collect_picking']          = "Shipper are interacting with the seller ";
$_['ghn_code_picked']                         = "Shipper is picked the goods";
$_['ghn_code_storing']                        = "The goods has been shipped to GHN sorting hub";
$_['ghn_code_transporting']                   = "The goods are being rotated";
$_['ghn_code_sorting']                        = "The goods are being classified (at the warehouse classification)";
$_['ghn_code_delivering']                     = "Shipper is delivering the goods to customer";
$_['ghn_code_money_collect_delivering']       = "Shipper is interacting with the buyer";
$_['ghn_code_delivered']                      = "The goods has been delivered to customer";
$_['ghn_code_delivery_fail']                  = "The goods hasn't been delivered to customer";
$_['ghn_code_waiting_to_return']              = "The goods are pending delivery (can be delivered within 24/48h)";
$_['ghn_code_return']                         = "The goods are waiting to return to seller/merchant after 3 times delivery failed";
$_['ghn_code_return_transporting']            = "The goods are being rotated";
$_['ghn_code_return_sorting']                 = "The goods are being classified (at the warehouse classification)";
$_['ghn_code_returning']                      = "The shipper is returning for seller";
$_['ghn_code_return_fail']                    = "The returning is failed";
$_['ghn_code_returned']                       = "The goods has been returned to seller/merchant";
$_['ghn_code_exception']                      = "The goods exception handling (cases that go against the process)";
$_['ghn_code_damage']                         = "Damaged goods";
$_['ghn_code_lost']                           = "The goods are lost";

// migrate old - incorrect status orders
$_['ghn_code_Cancel'] = "Shipping order has been cancelled"; // ghn_code_cancel