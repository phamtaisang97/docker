<?php

// Heading
$_['heading_title']                      = 'Onfluencer Marketing';
$_['heading_title_description']          = 'Onfluencer Marketing';

// Text
$_['text_intro_title']                   = 'Connect to Onfluencer';
$_['text_intro_content']                 = 'Vietnam\'s Leading Influencer Marketing Platform Makes Connecting Influencer / KOLs Easier';

$_['text_intro_button']                  = 'Use Onfluencer';
$_['text_go_to_app']                     = 'Enter App';

$_['text_setting_title']                 = 'Connect to Onfluencer';
$_['text_setting_content']               = 'Making connecting Influencer / KOLs even easier.';
$_['text_setting_api_key_title']         = 'Connection Api Key';
$_['text_setting_api_key_placeholder']   = 'MIAY2jDKYhvYjAWfdV8TPNoQHy1nugCL';
$_['text_setting_api_key_example']       = 'Provide the above code for Onfluencer to connect your website with Onfluencer';
$_['text_setting_api_key_button']        = 'Copy';

$_['text_more']                          = 'View Guide';

$_['text_api_key_copy_button']           = 'Copy';

$_['text_api_key_copy_success']          = 'Copied';
$_['text_api_key_copy_failed']           = 'Error! Could not copy';


// Introduce page
$_['text_intro_content_1']               = 'Influencer Marketing platform';
$_['text_intro_content_2']               = 'top of Vietnam';
$_['text_intro_content_3']               = 'Makes connecting Influencer/KOLs easier';

$_['text_description_1_title_1']         = 'CREATE TRUE ADVERTISING MATERIALS';
$_['text_description_1_title_2']         = 'Helping brands to create a diverse source of advertising materials on Social channels';
$_['text_description_1_content']         = 'Advertising ideas, content, images, videos, livestreams on Website, Fanpage, Instagram, Youtube ...';

$_['text_description_2_title_1']         = 'EXPERIENCE SALES Campaign Effectiveness';
$_['text_description_2_title_2']         = 'Suddenly increase the number of customers coming to the brand from offline and online through typical campaigns';
$_['text_description_2_content']         = 'Launch new products, open stores, discount events, promotions with wave effects from hundreds of thousands of followers';

$_['text_description_3_title_1']         = 'INCREASE RELIABILITY WITH BRANDS';
$_['text_description_3_title_2']         = 'Drive customers\' final buying decisions as they roam the internet looking for reviews.';
$_['text_description_3_content']         = '95% of Asian buying behavior depends on other people\'s reviews, 4/10 people order a product when they see that influencers are using that product.';