<?php
// Heading
$_['heading_title']    = 'Recommendation';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified Recommendation module!';
$_['text_edit']        = 'Edit Recommendation Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Recommendation module!';

// Custom
$_['custom_data_title'] = 'Recommendation';
$_['custom_data_message'] = 'Welcome to Recommendation Module';

// Setting
$_['event_setting_title'] = 'Event Setting';