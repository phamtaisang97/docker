<?php
// Heading
$_['heading_title']    = 'Customer Welcome';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified account module!';
$_['text_edit']        = 'Edit Account Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify account module!';

// Custom
$_['custom_data_title'] = 'Customer Welcome';
$_['custom_data_message'] = 'Welcome to Customer Welcome Module';