<?php
// Heading
$_['heading_title']    = '<i class="fa fa-wrench" style="    font-size: 14px;margin: 0 5px;padding: 5px;background: #56a03e;color: white; border-radius: 3px;"></i></i> Theme Config';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified Theme Config module!';
$_['text_edit']        = 'Edit Theme Config Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Theme Config module!';

// Config section
$_['text_config_section'] = 'Theme Config section';
$_['config_section_message'] = 'Welcome to Theme Config section! This feature will come later.';
$_['config_section_confirm_label']   = 'Confirm';
$_['config_section_confirm_help']    = 'Please type "CONFIRM" to reset config';
$_['config_section_confirm_button']  = 'Reset Config';
