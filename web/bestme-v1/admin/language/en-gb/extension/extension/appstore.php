<?php
// Heading
$_['heading_title']    = 'AppStore';

// Text
$_['text_success']     = 'Success: You have modified modules!';
$_['text_layout']      = 'After you have installed and configured a module you can add it to a layout <a href="%s" class="alert-link">here</a>!';
$_['text_add']         = 'Add Module';
$_['text_list']        = 'Module List';

// Column
$_['column_name']      = 'Module Name';
$_['column_status']    = 'Status';
$_['column_action']    = 'Action';

// Entry
$_['entry_code']       = 'Appstore';
$_['entry_name']       = 'Appstore';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify modules!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_code']       = 'Extension required!';