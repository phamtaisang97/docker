<?php
// Website
$_['heading_title']                              = 'Configuration'; 
$_['website_title']                              = 'Website configuration'; 
$_['website_description']                        = 'Information will be displayed when customers search for your website.'; 
$_['website_homepage_title']                     = 'Homepage title'; 
$_['website_homepage_title_placeholder']         = 'Enter article title';
$_['website_homepage_description']               = 'Homepage description'; 
$_['website_homepage_description_placeholder']   = 'Enter description to improve ranking on Google search engine';

// MaxLead
$_['maxlead_title']                               = 'MaxLead';
$_['maxlead_description']                         = 'Max Lead supports multiple ways of contact (Facebook, Zalo, LiveChat, ...) to attract maximum potential customers to visit your website; provides easy, convenient built-in conversion measurement reports.<br>Learn more about Max Lead <a href="' . AUTOADS_MAXLEAD_SERVER . '" target="_blank" class="link-here">here</a>.';
$_['maxlead_code']                                = 'MaxLead Code';
$_['maxlead_code_placeholder']                    = 'Enter MaxLead code';

// Google Tracting Manager (GTM) Code
$_['gtm_code_title']                               = 'Google Tag Manager (GTM)';
$_['gtm_code_description']                         = 'Google Tag Manager is a tag management system (TMS) that allows you to quickly and easily update measurement codes and related code fragments collectively known as tags on your website or mobile app.<br>Learn more about GTM <a href="' . "#" . '" target="_blank" class="link-here">tại đây</a>.';
$_['gtm_code_header']                              = 'GTM code for header';
$_['gtm_code_header_placeholder']                  = 'Enter GTM code for header';
$_['gtm_code_body']                                = 'GTM code for body';
$_['gtm_code_body_placeholder']                    = 'Enter GTM code for header';

// Google Analytics
$_['google_title']                               = 'Google Analytics';
$_['google_description']                         = 'Google Analytics allows you to track and determine the characteristics of users accessing your sale website, as well as providing measurement data reporting effective marketing activities.<br>Learn more about Google Analytics <a href="' . GG_ANALYTICS_SERVER . '" target="_blank" class="link-here">here</a>.';
$_['google_code']                                = 'Google Analytics Code';
$_['google_code_placeholder']                    = 'Enter Google Analytics code';

// Google Adwords
$_['google_aw_title']                               = 'Google Conversion Tracking';
$_['google_aw_description']                         = 'Tính năng theo dõi chuyển đổi của Google Ads cho bạn biết hành động xảy ra sau khi khách hàng nhấp vào quảng cáo của bạn.<br>Tìm hiểu thêm về Theo dõi chuyển đổi Google Ads <a href="' . "#" . '" target="_blank" class="link-here">tại đây</a>.';
$_['google_aw_code']                                = 'Google Global Tag code';
$_['google_aw_code_placeholder']                    = 'Enter Google Global Tag code';

// Google Adwords event tracking
$_['text_config_google_aw_code_cart']                           = 'Conversion code for checkout page';
$_['text_config_google_aw_code_cart_placeholder']               = 'Enter Conversion code for checkout page';
$_['text_config_google_aw_code_order']                          = 'Conversion code for payment page';
$_['text_config_google_aw_code_order_placeholder']              = 'EnterConversion code for payment page';
$_['text_config_google_aw_code_cart_success']                   = 'Conversion code for order success page';
$_['text_config_google_aw_code_cart_success_placeholder']       = 'Enter Conversion code for order success page';

// Facebook
$_['facebook_title']                             = 'Facebook Pixel';
$_['facebook_description']                       = 'Facebook Pixel helps you create advertising campaigns targeting new customers with characteristics similar to customers who have purchased at your website.<br>Learn more about Facebook Pixel <a href="' . FB_PIXEL_SERVER . '" target="_blank" class="link-here">here</a>.';
$_['facebook_pixel']                             = 'Facebook Pixel Code';
$_['facebook_placeholder']                       = 'Enter Facebook Pixel code';

// Facebook event tracking
$_['text_config_facebook_pixel_cart']                           = 'Facebook pixel code for checkout page';
$_['text_config_facebook_pixel_cart_placeholder']               = 'Enter Facebook pixel code for checkout page';
$_['text_config_facebook_pixel_order']                          = 'Facebook pixel code for payment page';
$_['text_config_facebook_pixel_order_placeholder']              = 'Enter Facebook pixel code for payment page';
$_['text_config_facebook_pixel_cart_success']                   = 'Facebook pixel code for order success page';
$_['text_config_facebook_pixel_cart_success_placeholder']       = 'Enter Facebook pixel code for order success page';

// site map
$_['txt_site_map_auto']                                = 'Auto';
$_['txt_site_map_manual']                              = 'Manual';

// Button
$_['button_save']                                = 'Save configuration';

$_['setting_seo']                                = 'SEO configuration';
$_['site_map_content_text']                      = 'Sitemap content';
$_['site_map_content_placeholder']               = 'Content of the sitemap.xml file for your website.';
$_['robots_content_text']                        = 'Robots.txt content';
$_['robots_content_placeholder']                 = 'Content of Robots.txt file for your website.';

$_['text_config_facebook_pixel_cart']                           = 'Code Facebook pixel shopping cart page';
$_['text_config_facebook_pixel_cart_placeholder']               = 'Enter the code Facebook pixel shopping cart page';
$_['text_config_facebook_pixel_order']                          = 'Code Facebook pixel payment page';
$_['text_config_facebook_pixel_order_placeholder']              = 'Enter the code Facebook pixel payment page';
$_['text_config_facebook_pixel_cart_success']                   = 'Code Facebook pixel payment page successfully';
$_['text_config_facebook_pixel_cart_success_placeholder']       = 'Enter the code Facebook pixel payment page successfully';

$_['text_detail']                         = 'Detailed instructions.';