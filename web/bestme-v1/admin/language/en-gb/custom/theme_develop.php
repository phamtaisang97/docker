<?php
// Heading
$_['heading_title']                       = 'List theme';
$_['all_theme']                           = 'All theme';
$_['title_theme']                         = 'Name';
$_['action_theme']                        = 'Edit / Delete';
$_['button_upload_theme']                 = "Update theme";
$_['button_fix_theme']                    = "Fix theme";
$_['theme_using']                         = "In used";
$_['theme_use']                           = "Use this";
$_['theme_delete']                        = "Delete";
$_['theme_load']                          = "Load";
$_['theme_store_action']                  = 'Action';
$_['theme_delete_sample_data']            = "Delete sample data";
$_['theme_delete_theme']                  = "Remove";
$_['theme_load_sample_data']              = "Load sample data";
$_['error_theme']                         = "No theme";
$_['alert_when_remove_theme_in_use']      = "Cannot remove the theme in use!";
$_['confirm_remove_theme']                = "Are you sure you want to delete this theme!";
$_['alert_remove_success']                = "Success!";
$_['alert_remove_fail']                   = "Failed!";
$_['theme_fix']                           = "Fix theme";
$_['download_theme']                      = "Download Theme (.zip)";

// error
$_['error_partner_theme_dir_not_found']        = "Initialize develop tool failed (not yet grant fully permissions for partner)! Please contact " . PRODUCTION_BRAND_NAME . " for help.";