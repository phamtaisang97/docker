<?php
// Text
$_['heading_title']             = 'Menu';
$_['heading_title_add']         = 'Add menu';
$_['heading_title_edit']        = 'Edit menu';
$_['add_menu_text']             = 'Add menu '; 
$_['menu_description_one']      = 'The menu list will make your browsing experience easier.'; 
$_['menu_description_two']      = 'The content selects the edit shown in the interface of your website. '; 
$_['menu_link_text']            = 'Link';
$_['menu_edit_text']            = 'Edit Menu'; 
$_['menu_title_text']           = 'Title'; 
$_['menu_choose_option']        = 'Select link'; 
$_['menu_add_more_text']        = 'Add link'; 
$_['menu_description_seo_text'] = 'Identify as the path displayed in the browser address bar and can be used to access the properties of Liquid Menu';
$_['menu_delete_text']          = 'Delete';
$_['menu_cancel_text']          = 'Cancel';
$_['menu_submit_text']          = 'Confirm';
$_['menu_text_heading']         = 'Website';
$_['menu_text']                 = 'Menu';
$_['menu_item_text']            = 'Menu items';
$_['menu_seo_title']            = 'SEO';
$_['menu_item_edit']            = 'Edit menu item'; 
$_['menu_item_connect']         = 'Link';
$_['menu_text_edit']            = 'Edit';
$_['menu_text_link_select']     = 'Select link '; 
$_['menu_text_add_link']        = 'Add link'; 
$_['choose_link']               = 'Choose link';

// Error
$_['error_warning']          = 'Warning: Please double-check the content form for errors!';
$_['error_permission']       = 'Warning: You do not have permission to edit the Product!';

// define built in pages
$_['txt_page_home']                     = 'Home';
$_['txt_page_product']                  = 'Product';
$_['txt_page_contact']                  = 'Contact';
$_['txt_page_login']                    = 'Login';
$_['txt_page_register']                 = 'Register';
$_['txt_page_policy']                   = 'Policy';
$_['txt_page_about']                    = 'About us';
$_['txt_page_blog']                     = 'Blog';
$_['txt_page_checkout_cart']            = 'Checkout cart';
$_['text_page_account_profile']         = 'Account profile';

$_['not_attach']                = "Not attached to menu";
$_['attach']                    = "Attached to menu:";
$_['attach_header']                     = "Header";
$_['attach_footer_collection']          = "Footer - Collection";
$_['attach_footer_quick_link']          = "Footer - Quick links";
$_['delete_error']              = "Menu deletion failed";
$_['delete_success']            = "Successfully deleted the menu";
$_['attach_menu_to']            = "Attach menu:";
$_['edit_success']              = "Edit menu successful";
$_['edit_menu_order_success']                          = "Change menu order successfully";
$_['edit_menu_order_failed']                           = "Change menu order failed. Please try again";
$_['edit_menu_order_failed_missing_menu_id']           = "Change menu order failed. Please try again (Error: 01)";
$_['edit_menu_order_failed_invalid_menu_ids']          = "Change menu order failed. Please try again (Error: 02)";