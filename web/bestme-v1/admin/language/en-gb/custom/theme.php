<?php
// Theme default
$_['heading_title']                              = 'Theme'; 
$_['theme_default_title']                        = 'Current theme'; 
$_['theme_default_description']                  = 'Customers will see this theme when they view your website.'; 
$_['theme_default_text']                         = 'Default theme'; 
$_['theme_default_setting_button']               = 'Set up theme'; 

// Theme store
$_['theme_store']                                = 'Theme store'; 
$_['theme_store_description']                    = 'List of all skins you have used for free or for free.';
$_['theme_store_your_theme']                     = 'Your theme'; 
$_['theme_store_your_theme_description']         = 'List of themes purchased for the website'; 
$_['theme_store_action']                         = 'Action'; 
$_['theme_store_customize']                      = 'Edit';
$_['theme_store_bestme']                         = 'Bestme theme repositoryp';
$_['theme_store_bestme_more']                    = 'See other interfaces on our theme repository';
$_['theme_store_button']                         = 'Theme store';

$_['theme_delete_sample_data'] = "Delete demo data";
$_['theme_load_sample_data'] = "Load demo data";
$_['theme_using'] = "In used";
$_['theme_use'] = "Use this";
$_['theme_delete'] = "Delete";
$_['theme_load'] = "Load";
$_['theme_sample_data_is_faulty_undefined'] = "sample data got error. Detail: unknown";
$_['theme_sample_data_successfully'] = "sample data successfully";
$_['theme_sample_data_is_faulty_detail'] = "sample data got error. Detail:";
$_['error_upload_theme'] = "Error upload file";
$_['error_upload_theme_type'] = "Error type file upload";
$_['upload_theme_success'] = "Upload file success";
$_['upload_theme_file_not_isset'] = "Error file not isset";
$_['button_upload_theme'] = "Upload theme";
$_['button_fix_theme'] = "Fix theme";
$_['error_upload_theme_or_file_isset'] = "Error file not isset or name theme isset";
$_['develope_theme'] = "Theme development";
$_['error_upload_theme_file_require'] = 'Error upload theme file require';
$_['file_theme_isset']                = "Theme is isset, remove before upload";
$_['error_change_theme']              = "Failed!";
$_['theme_title']                                = 'Theme';