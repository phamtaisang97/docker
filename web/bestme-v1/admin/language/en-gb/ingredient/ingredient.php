<?php
$_['text_home']                     = 'Home';
$_['heading_title']                 = 'Ingredient';
$_['heading_title_add']             = 'Add new ingredient';
$_['text_success']                  = 'Add new ingredient success';
$_['text_add_failed']               = 'Add new ingredient failed';
$_['heading_title_edit']            = 'Edit ingredient';
$_['breadcrumb_ingredient']         = 'Ingredient list';
$_['text_update_success']                       = 'Update ingredient success';
$_['text_edit_not_found_id']                    = 'Not found ingredient';

// get list
$_['choose_all_in_filter']                                  = "Select all in this page.";
$_['choose_in_filter']                                      = "Selected";
$_['item_in_filter']                                        = "item";
$_['collection_list_filter_action']                         = 'Select action';
$_['action_remove_ingredient']                              = "Remove ingredient";
$_['collection_confirm_show']                               = 'Are you sure you want to delete ';
$_['collection_confirm_choose']                             = " selected ingredients?";

$_['text_list_all']                         = 'All';
$_['text_code_ingredient']                  = 'Ingredient code';
$_['text_name_ingredient']                  = 'Ingredient name';
//$_['text_list_author']                   = 'Tác giả';
//$_['text_list_status']                   = 'Trạng thái';
$_['text_list_modified_date']               = 'Update day';

$_['text_search_list']                  = 'Search ingredient';
$_['txt_add']                           = "Add ingredient";
$_['text_close']                        = 'Close';
$_['text_remove']                       = 'Remove';
$_['text_add']                          = 'Add new ';
$_['text_cancel']                       = 'Cancel ';
$_['text_save']                         = 'Save changes ';
$_['popup_cancel_title']                = "Cancel change information";
$_['popup_cancel_message']              = "Changes will be lost";
$_['popup_cancel_ok']                   = "Ok";

// error
$_['error_text_check_blog']                     = 'Component creation failed!';
$_['error_permission']                          = 'Warning: You do not have permission to edit the ingredient!';

// form
$_['text_name']                                         = 'Ingredient name';
$_['text_name_placeholder']                             = 'Enter ingredient name';
$_['text_code']                                         = 'Ingredient code';
$_['text_code_placeholder']                             = 'Enter ingredient code';
$_['text_active_ingredient']                            = 'Active ingredient';
$_['text_active_ingredient_placeholder']                = 'Enter active ingredient';
$_['text_unit']                                         = 'Unit';
$_['text_unit_placeholder']                             = 'Enter unit';
$_['text_basic_description']                            = 'Basic description';
$_['text_basic_description_placeholder']                = 'Enter basic description';
$_['text_note']                                         = 'Note';
$_['text_note_placeholder']                             = 'Enter note';
$_['text_overdose']                                         = 'Overdose';
$_['text_overdose_placeholder']                             = 'Overdose recommendation';
$_['text_course_certificates']                              = 'Course certificates ';
$_['text_course_certificates_placeholder']                  = 'Enter course certificates';
$_['text_detail_description']                               = 'Detail description';
$_['text_detail_description_placeholder']                   = 'Enter detail description';
$_['text_recommended_daily_us_man']                         = 'Recommended daily US / Man';
$_['text_recommended_daily_us_women']                       = 'Recommended daily US / Women';
$_['text_recommended_daily_vn_man']                         = 'Recommended daily VN / Man';
$_['text_recommended_daily_vn_women']                       = 'Recommended daily VN / Women';
$_['text_input_placeholder']                                = 'Enter information';

$_['text_us_man_19_30']                                         = 'From 19 to 30 years old';
$_['text_us_man_31_50']                                         = 'From 31 to 50 years old';
$_['text_us_man_51_70']                                         = 'From 51 to 70 years old';
$_['text_us_man_gt_70']                                         = 'More than 70 years old';

$_['text_us_women_19_30']                                       = 'From 19 to 30 years old';
$_['text_us_women_31_50']                                       = 'From 31 to 50 years old';
$_['text_us_women_51_70']                                       = 'From 51 to 70 years old';
$_['text_us_women_gt_70']                                       = 'More than 70 years old';

$_['text_vn_man_19_50']                                         = 'From 19 to 50 years old';
$_['text_vn_man_51_60']                                         = 'From 51 to 60 years old';
$_['text_vn_man_gt_60']                                         = 'More than 60 years old';

$_['text_vn_women_19_50']                                       = 'From 19 to 50 years old';
$_['text_vn_women_51_60']                                       = 'From 51 to 60 years old';
$_['text_vn_women_gt_60']                                       = 'More than 60 years old';

// validate
$_['text_name_empty']                               = 'Ingredient name not empty';
$_['text_name_max_length']                          = 'Ingredient name should not exceed 255 characters';
$_['text_code_empty']                               = 'Ingredient code not empty';
$_['text_code_max_length']                          = 'Ingredient code should not exceed 255 characters';

// excel
$_['export_file_col_name']                                          = 'tên thành phần';
$_['export_file_col_code']                                          = 'mã thành phần';
$_['export_file_col_active_ingredient']                             = 'hoạt chất';
$_['export_file_col_unit']                                          = 'đơn vị';
$_['export_file_col_basic_description']                             = 'mô tả cơ bản';
$_['export_file_col_note']                                          = 'lưu ý';
$_['export_file_col_overdose']                                      = 'quá liều';
$_['export_file_col_course_certificates']                           = 'minh chứng khoa học';
$_['export_file_col_detail_description']                            = 'mô tả chi tiết';
$_['export_file_col_us_man_19_30']                                  = 'us_man_19_30';
$_['export_file_col_us_man_31_50']                                  = 'us_man_31_50';
$_['export_file_col_us_man_51_70']                                  = 'us_man_51_70';
$_['export_file_col_us_man_gt_70']                                  = 'us_man_gt_70';
$_['export_file_col_us_women_19_30']                                = 'us_women_19_30';
$_['export_file_col_us_women_31_50']                                = 'us_women_31_50';
$_['export_file_col_us_women_51_70']                                = 'us_women_51_70';
$_['export_file_col_us_women_gt_70']                                = 'us_women_gt_70';
$_['export_file_col_vn_man_19_50']                                  = 'vn_man_19_50';
$_['export_file_col_vn_man_51_60']                                  = 'vn_man_51_60';
$_['export_file_col_vn_man_gt_60']                                  = 'vn_man_gt_60';
$_['export_file_col_vn_women_19_50']                                = 'vn_women_19_50';
$_['export_file_col_vn_women_51_60']                                = 'vn_women_51_60';
$_['export_file_col_vn_women_gt_60']                                = 'vn_women_gt_60';

//excel 2
$_['export_file_col_product_sku']                                   = 'mã sản phẩm';
$_['export_file_col_ingredients']                                   = 'thành phần';
$_['export_file_col_quantitative']                                  = 'định lượng';

$_['ingredient_import_file']               = 'Input list';
$_['import_file_product_ingredient']       = 'Import ingredient for product';
$_['choose_file']                          = 'Choose file';
$_['button_upload_continue']               = 'Input list product';
$_['upload_product_list_title']            = 'Input list';
$_['txt_warning_import_product']           = 'Attention: products with the same "code" (ingredient code) will be overwritten!';

$_['error_file_incomplete']                         = 'Warning: File is incomplete (missing columns)!';
$_['error_unknown']                                 = 'Error unknown';
$_['error_filesize']                                = 'Warning: File size should not be more than 1MB!';
$_['error_filetype']                                = 'Warning: Only .xls and xlsx formats are accepted!';
$_['error_not_found_fields']                        = 'Warning: No "ingredient name" or "ingredient code" found!';
$_['error_no_file_selected']                        = 'No files have been selected yet!';
$_['error_limit']                                   = 'Warning: The maximum number of imported components is 500!';

$_['text_uploaded']                                 = 'Success!';
$_['text_product_uploaded']                         = ' ingredient have been updated.';
$_['text_product_ingredient_uploaded']              = ' product have been updated.';
$_['delete_success']                                = "Deleted successfully.";
