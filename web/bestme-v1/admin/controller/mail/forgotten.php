<?php

class ControllerMailForgotten extends Controller
{
    public function index(&$route, &$args, &$output)
    {
        $this->load->language('mail/forgotten');

        $data['text_greeting'] = sprintf($this->language->get('text_greeting'), html_entity_decode($args[2], ENT_QUOTES, 'UTF-8'));

        $data['reset'] = str_replace('&amp;', '&', $this->url->link('common/reset', 'code=' . $args[1], true));
        $data['new_password'] = $args[3];
        $data['ip'] = $this->request->server['REMOTE_ADDR'];

        $mail = new Mail($this->config->get('config_mail_engine'));
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = MAIL_SMTP_HOST_NAME;
        $mail->smtp_username = MAIL_SMTP_USERNAME;
        $mail->smtp_password = html_entity_decode(MAIL_SMTP_PASSWORD, ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = MAIL_SMTP_PORT;
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($args[0]);
        $mail->setFrom(MAIL_SMTP_FROM);
        $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        $mail->setSubject(html_entity_decode(sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8'));
        $mail->setText($this->load->view('mail/forgotten', $data));
        $mail->send();

        /* update new password */
        $this->load->model('user/user');
        $user = $this->model_user_user->getUserByEmail($args[0]);
        $this->model_user_user->editPasswordTemp($user['user_id'], $data['new_password']);
    }
}
