<?php
/**
 * Created by PhpStorm.
 * User: Microsoft Windows
 * Date: 14/02/2020
 * Time: 9:39 AM
 */

class ControllerDiscountSetting extends  Controller{

    const DISCOUNT_AUTO_APPLY_KEY = 'discount_auto_apply';
    const DISCOUNT_COMBINE_KEY = 'discount_combine';

    public function index()
    {
        $this->load->language('discount/discount');
        $this->document->setTitle($this->language->get('heading_setting_title'));
        $data = [];

        $data['discount_auto_apply'] = $this->config->get(self::DISCOUNT_AUTO_APPLY_KEY);
        $data['discount_combine'] = $this->config->get(self::DISCOUNT_COMBINE_KEY);

        $data['access_modify'] = $this->user->hasPermission('modify', 'discount/setting');

        $data['save_href'] = $this->url->link('discount/setting/save', 'user_token=' . $this->session->data['user_token'], true);
        $data['discount_href'] = $this->url->link('discount/discount', 'user_token=' . $this->session->data['user_token'], true);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('discount/setting', $data));
    }

    public function save()
    {
        $this->load->language('discount/discount');
        $json = array(
            'status' => false,
            'msg' =>  $this->language->get('txt_discount_save_fail')
        );

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateDiscountSetting()) {
            $this->load->model('setting/setting');
            $discount_auto_apply = $this->request->post['discount_auto_apply'] == 1 ? 1 : 0;
            $discount_combine = $this->request->post['discount_combine'] == 1 ? 1 : 0;
            $this->model_setting_setting->addConfig(self::DISCOUNT_AUTO_APPLY_KEY, $discount_auto_apply);
            $this->model_setting_setting->addConfig(self::DISCOUNT_COMBINE_KEY, $discount_combine);

            $json['status'] = true;
            $json['msg'] = $this->language->get('txt_discount_save_success');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function validateDiscountSetting()
    {
        if (!isset($this->request->post['discount_auto_apply']) || !isset($this->request->post['discount_combine'])){
            return false;
        }

        return true;
    }
}