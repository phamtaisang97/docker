<?php

class ControllerDiscountFlashSale extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('discount/flash_sale');

        $this->document->setTitle($this->language->get('heading_title'));
        // add css,js only list product
        $this->document->addStyle('view/stylesheet/custom/product.css');

        $this->load->model('discount/flash_sale');

        $this->getList();
    }

    public function add()
    {
        $this->load->model('discount/flash_sale');
        $this->load->language('discount/flash_sale');
        $this->document->setTitle($this->language->get('heading_title_add_list'));
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $data = $this->request->post;

            $this->model_discount_flash_sale->addFlashSale($data);

            $this->response->redirect($this->url->link('discount/flash_sale', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();
    }

    public function edit()
    {
        $this->load->model('discount/flash_sale');
        $this->load->language('discount/flash_sale');
        $this->document->setTitle($this->language->get('heading_title_edit'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $data = $this->request->post;

            $this->model_discount_flash_sale->editFlashSale($this->request->get['flash_sale_id'], $data);

            $this->response->redirect($this->url->link('discount/flash_sale', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();
    }

    public function delete()
    {
        $this->load->language('discount/flash_sale');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('discount/flash_sale');

        $flash_sale_ids = $this->request->get['flash_sale_ids'];
        $flash_sale_ids = explode(',', $flash_sale_ids);
        if (isset($flash_sale_ids)) {
            foreach ($flash_sale_ids as $flash_sale_id) {
                $this->model_discount_flash_sale->deleteFlashSale($flash_sale_id);
            }
        }
        $this->session->data['success'] = $this->language->get('txt_note_delete');
        $this->response->redirect($this->url->link('discount/flash_sale', 'user_token=' . $this->session->data['user_token'], true));
    }

    protected function getList()
    {
        $filter_status = $this->getValueFromRequest('get', 'filter_status');
        $filter_name = $this->getValueFromRequest('get', 'filter_name');
        $filter_collection_id = $this->getValueFromRequest('get', 'filter_collection_id');
        $page = $this->getValueFromRequest('get', 'page', 1);
        $filter_data = $this->getValueFromRequest('get', 'filter_data');

        $url = '';
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        // end default
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('discount/flash_sale', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        $data['add'] = $this->url->link('discount/flash_sale/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['delete'] = $this->url->link('discount/flash_sale/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['change_status'] = $this->url->link('discount/flash_sale/changeStatus', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['list'] = $this->url->link('discount/flash_sale', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['access_modify'] = $this->user->hasPermission('modify', 'discount/flash_sale');

        $data['loadOptionOne'] = $this->replace_url($this->url->link('discount/flash_sale/loadoptionone', 'user_token=' . $this->session->data['user_token'] . $url, true));
        $data['flash_sales'] = array();

        if (!empty($filter_data)) {
            if (isset($this->request->get['filter_status'])) {
                $filter_status = ($this->request->get['filter_status']);
                if (is_array($filter_status)) {
                    if (count($filter_status) > 1) {
                        $filter_status = '';
                    } else {
                        $filter_status = (int)$filter_status[0];
                    }
                }
            }
            if (!empty($filter_collection_id)) {
                if (count($filter_collection_id) > 0) {
                    $filter_collection_id = implode(',', ($filter_collection_id));
                } else {
                    $filter_collection_id = (int)$filter_collection_id[0];
                }
            } else {
                $filter_collection_id = '';
            }
        }

        $filter_data = array(
            'filter_data' => $filter_data,
            'filter_status' => $filter_status,
            'filter_name' => $filter_name,
            'filter_collection_id' => $filter_collection_id,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $data['STATUS_ACTIVED'] = ModelDiscountFlashSale::STATUS_ACTIVED;
        $data['STATUS_UNACTIVED'] = ModelDiscountFlashSale::STATUS_UNACTIVED;
        $DISCOUNT_STATUS = [
            // Unactivated
            $data['STATUS_UNACTIVED'] => [
                'status' => $this->language->get('txt_unactived')
            ],
            // Activated
            $data['STATUS_ACTIVED'] => [
                'status' => $this->language->get('txt_actived')
            ]
        ];

        $flash_sale_total = $this->model_discount_flash_sale->getTotalFlashSales($filter_data);
        $results = $this->model_discount_flash_sale->getFlashSales($filter_data);
        foreach ($results as $result) {
            $data['flash_sales'][] = array(
                'flash_sale_id' => $result['flash_sale_id'],
                'collection_title' => $result['collection_title'],
                'status_text' => $DISCOUNT_STATUS[$result['status']]['status'],
                'status' => $result['status'],
                'starting_date' => $result['starting_date'],
                'ending_date' => $result['ending_date'],
                'starting_time' => $result['starting_time'],
                'ending_time' => $result['ending_time'],
                'edit' => $this->url->link('discount/flash_sale/edit', 'user_token=' . $this->session->data['user_token'] . '&flash_sale_id=' . $result['flash_sale_id'] . $url, true)
            );
        }
        $data['user_token'] = $this->session->data['user_token'];

        // paginate
        $url = '';
        $pagination = new CustomPaginate();
        $pagination->total = $flash_sale_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('discount/flash_sale', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        $data['pagination'] = $pagination->render();

        if (isset($this->session->data['success']) && $this->session->data['success'] != 'text_success') {
            $data['success'] = $this->session->data['success'];

            $this->session->data['success'] = '';
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->session->data['discounts'])) {
            $data['discounts_action'] = json_encode($this->session->data['discounts']);
        }

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];

            $this->session->data['error'] = '';
            unset($this->session->data['error']);
        } else {
            $data['error_warning'] = '';
        }

        $data['results'] = sprintf($this->language->get('text_pagination'), ($flash_sale_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($flash_sale_total - $this->config->get('config_limit_admin'))) ? $flash_sale_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $flash_sale_total, ceil($flash_sale_total / $this->config->get('config_limit_admin')));

        $data['url_filter'] = $this->url->link('discount/flash_sale', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['auto_complete'] = $this->url->link('discount/flash_sale/autoComplete', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('discount/flash_sale_list_append', $data));
        } else {
            $this->response->setOutput($this->load->view('discount/flash_sale_list', $data));
        }
    }

    protected function getForm()
    {
        $data = array();

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = [];
        }

        $data['text_form'] = !isset($this->request->get['flash_sale_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('discount/flash_sale', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $data['text_form'],
            'href' => ''
        );

        if (isset($this->request->get['flash_sale_id'])) {
            $data['flash_sale_id'] = $this->request->get['flash_sale_id'];
        }

        // get discount info
        if (isset($this->request->get['flash_sale_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $flash_sale_info = $this->model_discount_flash_sale->getFlashSale($this->request->get['flash_sale_id']);
        }

        $data['collection_title'] = '';
        if (isset($this->request->post['collection_id'])) {
            $data['collection_id'] = $this->request->post['collection_id'];
        } else if (!empty($flash_sale_info)) {
            $data['collection_id'] = $flash_sale_info['collection_id'];
            $data['collection_title'] = $flash_sale_info['collection_title'];
        } else {
            $data['collection_id'] = '';
        }

        $today = date("Y-m-d");
        if (isset($this->request->post['starting_date'])) {
            $data['starting_date'] = $this->request->post['starting_date'];
        } else if (!empty($flash_sale_info)) {
            $data['starting_date'] = $flash_sale_info['starting_date'];
        } else {
            $data['starting_date'] = $today;
        }

        if (isset($this->request->post['ending_date'])) {
            $data['ending_date'] = $this->request->post['ending_date'];
        } else if (!empty($flash_sale_info)) {
            $data['ending_date'] = $flash_sale_info['ending_date'];
        } else {
            $data['ending_date'] = $today;
        }

        $now = date("H:i:s");
        if (isset($this->request->post['starting_time'])) {
            $data['starting_time'] = $this->request->post['starting_time'];
        } else if (!empty($flash_sale_info)) {
            $data['starting_time'] = $flash_sale_info['starting_time'];
        } else {
            $data['starting_time'] = $now;
        }

        if (isset($this->request->post['ending_time'])) {
            $data['ending_time'] = $this->request->post['ending_time'];
        } else if (!empty($flash_sale_info)) {
            $data['ending_time'] = $flash_sale_info['ending_time'];
        } else {
            $data['ending_time'] = $now;
        }

        $data['STATUS_ACTIVED'] = ModelDiscountFlashSale::STATUS_ACTIVED;
        $data['STATUS_UNACTIVED'] = ModelDiscountFlashSale::STATUS_UNACTIVED;
        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } else if (!empty($flash_sale_info)) {
            $data['status'] = $flash_sale_info['status'];
        } else {
            $data['status'] = $data['STATUS_ACTIVED'];
        }

        $data['user_token'] = $this->session->data['user_token'];
        $data['cancel'] = $this->url->link('discount/flash_sale', 'user_token=' . $this->session->data['user_token'], true);
        $data['fetch_collection_url'] = $this->url->link('catalog/collection/getCollectionListLazyLoad', 'user_token=' . $this->session->data['user_token'], true);
        if (!isset($this->request->get['flash_sale_id'])) {
            $data['action'] = $this->url->link('discount/flash_sale/add', 'user_token=' . $this->session->data['user_token'], true);
        } else {
            $data['action'] = $this->url->link('discount/flash_sale/edit', 'user_token=' . $this->session->data['user_token'] . '&flash_sale_id=' . $this->request->get['flash_sale_id'], true);
        }
        $data['icon_delete'] = 'view/image/custom/icons/trash.png';

        $data['access_modify'] = $this->user->hasPermission('modify', 'discount/flash_sale');
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('discount/flash_sale_form', $data));
    }

    protected function validateForm()
    {
        // reset error
        $this->error = [];

        if (!$this->user->hasPermission('modify', 'discount/flash_sale')) {
            $this->error['warning'][] = $this->language->get('error_permission');
        }

        if (empty($this->request->post['collection_id'])) {
            $this->error['warning'][] = $this->language->get('error_collection_missing');
        } else {
            $this->load->model('catalog/collection');
            $collection = $this->model_catalog_collection->getCollection($this->request->post['collection_id']);
            if (empty($collection)) {
                $this->error['warning'][] = $this->language->get('error_collection_not_exists');
            }
        }

        if (empty($this->request->post['starting_date'])) {
            $this->error['warning'][] = $this->language->get('error_starting_date_missing');
        } else {
            $starting_date = DateTime::createFromFormat("Y-m-d", $this->request->post['starting_date']);
            if ($starting_date === false || array_sum($starting_date::getLastErrors())) {
                $this->error['warning'][] = $this->language->get('error_starting_date_wrong_format');
            }
        }

        if (empty($this->request->post['ending_date'])) {
            $this->error['warning'][] = $this->language->get('error_ending_date_missing');
        } else {
            $starting_date = DateTime::createFromFormat("Y-m-d", $this->request->post['ending_date']);
            if ($starting_date === false || array_sum($starting_date::getLastErrors())) {
                $this->error['warning'][] = $this->language->get('error_ending_date_wrong_format');
            }
        }

        if (empty($this->request->post['starting_time'])) {
            $this->error['warning'][] = $this->language->get('error_starting_time_missing');
        } else {
            $starting_date = DateTime::createFromFormat("H:i:s", $this->request->post['starting_time']);
            if ($starting_date === false || array_sum($starting_date::getLastErrors())) {
                $this->error['warning'][] = $this->language->get('error_starting_time_wrong_format');
            }
        }

        if (empty($this->request->post['ending_time'])) {
            $this->error['warning'][] = $this->language->get('error_ending_time_missing');
        } else {
            $starting_date = DateTime::createFromFormat("H:i:s", $this->request->post['ending_time']);
            if ($starting_date === false || array_sum($starting_date::getLastErrors())) {
                $this->error['warning'][] = $this->language->get('error_ending_time_wrong_format');
            }
        }

        if (!isset($this->request->post['status']) || '' == $this->request->post['status'] || ($this->request->post['status'] == ModelDiscountFlashSale::STATUS_ACTIVED && $this->request->post['status'] == ModelDiscountFlashSale::STATUS_UNACTIVED)) {
            $this->error['warning'][] = $this->language->get('error_status_wrong_format');
        }

        return !$this->error;
    }

    public function replace_url($url)
    {
        return str_replace("amp;", "", $url);
    }

    public function autoComplete()
    {
        $this->load->model('discount/discount');
        $filter_name = $this->request->get['filter_name'];
        $results = $this->model_discount_flash_sale->getDiscountByName($filter_name);
        $discounts = array();
        foreach ($results as $result) {
            $discounts[] = array(
                'name' => $result['discount_name'],
                'code' => $result['discount_code'],
                'edit' => $this->url->link('discount/discount/edit', 'user_token=' . $this->session->data['user_token'] . '&discount_id=' . $result['discount_id'], true),
            );
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($discounts));
    }
}

?>