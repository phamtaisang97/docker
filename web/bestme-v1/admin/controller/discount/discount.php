<?php

class ControllerDiscountDiscount extends Controller
{
    use Device_Util;

    private $error = array();

    /* duplicate with defined in ModelDiscountDiscount. TODO: other way? */
    private $source_order_data_map = [
        ['id' => 'website', 'text' => 'Website'],
        ['id' => 'pos', 'text' => 'POS'],
        ['id' => 'admin', 'text' => 'Admin'],
        ['id' => 'chatbot', 'text' => 'Chatbot']
    ];

    const STATUS_DISCOUNT = [
        'status_upcoming'  => [
            'value' => 'status_upcoming',
            'text' => 'Sắp tới'
        ],
        'status_happening' => [
            'value' => 'status_happening',
            'text' => 'Đang diễn ra'
        ],
        'status_ended'  => [
            'value' => 'status_ended',
            'text' => 'Đã kết thúc'
        ],
        'status_paused' => [
            'value' => 'status_paused',
            'text' => 'Đã tạm dừng'
        ]
    ];

    public function index()
    {
        $this->load->language('discount/discount');

        $this->document->setTitle($this->language->get('heading_title'));
        // add css,js only list product
        $this->document->addStyle('view/stylesheet/custom/product.css');

        $this->load->model('discount/discount');
        $this->load->model('discount/discount_type');

        $this->getList();
    }

    public function add()
    {
        $this->load->model('discount/discount');
        $this->load->language('discount/discount');
        $this->document->setTitle($this->language->get('heading_title_add_list'));
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $data = $this->request->post;

            $this->model_discount_discount->addDiscount($data);

            $this->response->redirect($this->url->link('discount/discount', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->document->addScript('view/javascript/pagination/pagination.js');
        $this->document->addStyle('view/stylesheet/custom/pagination.css');

        $this->getForm();
    }

    public function edit()
    {
        $this->load->model('discount/discount');
        $this->load->model('catalog/category');
        $this->load->language('discount/discount');
        $this->document->setTitle($this->language->get('heading_title_edit'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $data = $this->request->post;

            $this->model_discount_discount->editDiscount($this->request->get['discount_id'], $data);

            $this->response->redirect($this->url->link('discount/discount', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->document->addScript('view/javascript/pagination/pagination.js');
        $this->document->addStyle('view/stylesheet/custom/pagination.css');

        $this->getForm();
    }

    public function delete()
    {
        $this->load->language('discount/discount');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('discount/discount');

        $discount_ids = isset($this->request->get['discount_ids']) ? $this->request->get['discount_ids'] : [];
        $reason = isset($this->request->get['reason']) ? $this->request->get['reason'] : "";
        $discount_ids = explode(',', $discount_ids);
        if (isset($discount_ids)) {
            foreach ($discount_ids as $discount_id) {
                $checkDiscountsActivated = $this->model_discount_discount->checkDiscountsActivated($discount_id);
                if ($checkDiscountsActivated) {
                    $this->session->data['error'] = 'Chương trình đang diễn ra không cho phép xóa';
                    $this->response->redirect($this->url->link('discount/discount', 'user_token=' . $this->session->data['user_token'], true));
                } else {
                    $this->model_discount_discount->deleteDiscount($discount_id, $reason);
                }
            }
        }
        $this->session->data['success'] = $this->language->get('txt_note_delete');
        $this->response->redirect($this->url->link('discount/discount', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function pause()
    {
        $this->load->language('discount/discount');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('discount/discount');

        $discount_id = isset($this->request->get['discount_id']) ? $this->request->get['discount_id'] : [];
        $reason = isset($this->request->get['reason']) ? $this->request->get['reason'] : "";

        $this->session->data['discounts'] = [];

        $this->model_discount_discount->pauseDiscountsActivated($discount_id, $reason);
        $this->session->data['success'] = "Đã tạm dừng chương trình khuyến mãi";
        $this->response->redirect($this->url->link('discount/discount', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function continue()
    {
        $this->load->language('discount/discount');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('discount/discount');

        $discount_id = isset($this->request->get['discount_id']) ? $this->request->get['discount_id'] : [];
        $reason = isset($this->request->get['reason']) ? $this->request->get['reason'] : "";

        $this->session->data['discounts'] = [];

        $this->model_discount_discount->continueDiscountsActivated($discount_id, $reason);
        $this->session->data['success'] = "Đã tiếp tục chương trình khuyến mãi";
        $this->response->redirect($this->url->link('discount/discount', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function config_discount($data)
    {
        if (!empty($data['price_order'])) {
            $price_order = is_array($data['price_order']) ? $data['price_order'] : [];
            $type_order = is_array($data['type_order']) ? $data['type_order'] : [];
            $quantity_value = is_array($data['quantity_value']) ? $data['quantity_value'] : [];
            $weight_steps = [];
            $config = [];
            foreach ($price_order as $key_order => $value_order) {
                if (!empty($value_order[0]) and $value_order[0] != '') {
                    $price_orders = isset($price_order[$key_order]) ? $price_order[$key_order] : [];
                    $type_orders = isset($type_order[$key_order]) ? $type_order[$key_order] : [];
                    $quantity_values = isset($quantity_value[$key_order]) ? $quantity_value[$key_order] : [];
                    foreach ($price_orders as $key => $value) {
                        $value = isset($price_orders[$key]) ? extract_number(trim($price_orders[$key])) : '';
                        $type = isset($type_orders[$key]) ? trim($type_orders[$key]) : '';
                        $discount = isset($quantity_values[$key]) ? extract_number(trim($quantity_values[$key])) : '';
                        $weight_steps['conditions'][] = array(
                            'value' => $value,
                            'type' => $type,
                            'discount' => $discount
                        );
                    }

                    $config = array($weight_steps);
                }
            }
        }

        if (!empty($data['category_id'])) {
            $all_categories = isset($data['all_category']) ? $data['all_category'] : 0;
            $category_id = is_array($data['category_id']) ? array_unique($data['category_id']) : [];
            $min_quantity = is_array($data['min_quantity']) ? $data['min_quantity'] : [];
            $max_quantity = is_array($data['max_quantity']) ? $data['max_quantity'] : [];
            $type_category_product = is_array($data['type_category_product']) ? $data['type_category_product'] : [];
            $quantity_category = is_array($data['quantity_category']) ? $data['quantity_category'] : [];
            $array_config = [];
            foreach ($category_id as $key_category => $vl_category) {
                $min_quantities = isset($min_quantity[$vl_category]) ? $min_quantity[$vl_category] : [];
                $max_quantities = isset($max_quantity[$vl_category]) ? $max_quantity[$vl_category] : [];
                $type_category_products = isset($type_category_product[$vl_category]) ? $type_category_product[$vl_category] : [];
                $quantity_categories = isset($quantity_category[$vl_category]) ? $quantity_category[$vl_category] : [];
                $weight_steps = [];
                foreach ($min_quantities as $key => $value) {
                    $min_quantity_value = extract_number($value[0]);
                    $max_quantity_value = isset($max_quantities[$key][0]) ? extract_number($max_quantities[$key][0]) : '';
                    $type_category_products_value = isset($type_category_products[$key][0]) ? $type_category_products[$key][0] : '';
                    $quantity_categories_value = isset($quantity_categories[$key][0]) ? extract_number($quantity_categories[$key][0]) : '';
                    $weight_steps['all_category'] = $all_categories;
                    if ($all_categories == 0) {
                        $weight_steps['category_id'] = $vl_category;
                    }
                    $weight_steps['conditions'][] = array(
                        'min_quantity' => $min_quantity_value,
                        'max_quantity' => $max_quantity_value,
                        'type' => $type_category_products_value,
                        'discount' => $quantity_categories_value
                    );
                }

                $array_config[] = $weight_steps;
            }
            $config = $array_config;
        }

        if (!empty($data['manufacturer_id'])) {
            $all_manufacturer = isset($data['all_manufacturer']) ? $data['all_manufacturer'] : 0;
            $manufacturer_id = is_array($data['manufacturer_id']) ? array_unique($data['manufacturer_id']) : [];
            $min_quantity = is_array($data['min_quantity']) ? $data['min_quantity'] : [];
            $max_quantity = is_array($data['max_quantity']) ? $data['max_quantity'] : [];
            $type_category_product = is_array($data['type_manufacturer']) ? $data['type_manufacturer'] : [];
            $quantity_category = is_array($data['quantity_manufacturer']) ? $data['quantity_manufacturer'] : [];
            $array_config = [];
            foreach ($manufacturer_id as $key_category => $vl_category) {
                $min_quantities = isset($min_quantity[$vl_category]) ? $min_quantity[$vl_category] : [];
                $max_quantities = isset($max_quantity[$vl_category]) ? $max_quantity[$vl_category] : [];
                $type_category_products = isset($type_category_product[$vl_category]) ? $type_category_product[$vl_category] : [];
                $quantity_categories = isset($quantity_category[$vl_category]) ? $quantity_category[$vl_category] : [];
                $weight_steps = [];
                foreach ($min_quantities as $key => $value) {
                    $min_quantity_value = extract_number($value[0]);
                    $max_quantity_value = isset($max_quantities[$key][0]) ? extract_number($max_quantities[$key][0]) : '';
                    $type_category_products_value = isset($type_category_products[$key][0]) ? $type_category_products[$key][0] : '';
                    $quantity_categories_value = isset($quantity_categories[$key][0]) ? extract_number($quantity_categories[$key][0]) : '';
                    $weight_steps['all_manufacturer'] = $all_manufacturer;
                    if ($all_manufacturer == 0) {
                        $weight_steps['manufacturer_id'] = $vl_category;
                    }
                    $weight_steps['conditions'][] = array(
                        'min_quantity' => $min_quantity_value,
                        'max_quantity' => $max_quantity_value,
                        'type' => $type_category_products_value,
                        'discount' => $quantity_categories_value
                    );
                }

                $array_config[] = $weight_steps;
            }
            $config = $array_config;
        }

        if (!empty($data['product_id'])) {
            $all_product = isset($data['all_product']) ? $data['all_product'] : 0;
            $product_id = ($data['product_id']) ? ($data['product_id']) : [];
            $min_quantity = is_array($data['min_quantity']) ? $data['min_quantity'] : [];
            $max_quantity = is_array($data['max_quantity']) ? $data['max_quantity'] : [];
            $type_product = is_array($data['type_product']) ? $data['type_product'] : [];
            $quantity_product = is_array($data['quantity_product']) ? $data['quantity_product'] : [];
            $array_config = [];
            foreach ($product_id as $key => $value) {
                $min_quantities = isset($min_quantity[$key]) ? $min_quantity[$key] : [];
                $max_quantities = isset($max_quantity[$key]) ? $max_quantity[$key] : [];
                $type_products = isset($type_product[$key]) ? $type_product[$key] : [];
                $quantity_products = isset($quantity_product[$key]) ? $quantity_product[$key] : [];
                $weight_steps = [];
                $weight_steps['all_product'] = $all_product;
                if ($all_product) {
                    $weight_steps['product_id'] = '';
                    $weight_steps['product_version_ids'] = [];
                }else{
                    $weight_steps['product_id'] = $key;

                    if (strpos($value, ',') == false && strpos($value, '-') == false) {
                        $weight_steps['product_version_ids'] = [];
                    } else {
                        $value_ids = explode(',', $value);

                        $subString = $key.'-';
                        $value_ids = array_filter($value_ids, function ($val) use ($subString){
                            if (strpos($val, ''.$subString) !== false) {
                                return true;
                            } else {
                                return false;
                            }
                        });
                        $value_ids = array_values($value_ids);
                        $product_version_ids = str_replace($key . '-', '', $value_ids);
                        $weight_steps['product_version_ids'] = $product_version_ids;
                    }
                }


                foreach ($min_quantities as $key_quantity => $value_quantity) {
                    $min_quantity_value = extract_number($value_quantity[0]);
                    $max_quantity_value = isset($max_quantities[$key_quantity][0]) ? extract_number($max_quantities[$key_quantity][0]) : '';
                    $type_products_value = isset($type_products[$key_quantity][0]) ? $type_products[$key_quantity][0] : '';
                    $quantity_products_value = isset($quantity_products[$key_quantity][0]) ? extract_number($quantity_products[$key_quantity][0]) : '';

                    $weight_steps['conditions'][] = array(
                        'min_quantity' => $min_quantity_value,
                        'max_quantity' => $max_quantity_value,
                        'type' => $type_products_value,
                        'discount' => $quantity_products_value
                    );
                }

                $array_config[] = $weight_steps;
            }

            $config = $array_config;
        }

        return (!empty($config) ? $config : []);
    }

    protected function getList()
    {
        $status_discount = $this->getValueFromRequest('get', 'status_discount');
        $filter_name = $this->getValueFromRequest('get', 'filter_name');
        $discount_type_id = $this->getValueFromRequest('get', 'discount_type_id');
        $page = $this->getValueFromRequest('get', 'page', 1);
        $filter_data = $this->getValueFromRequest('get', 'filter_data');

        $url = '';
        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        // end default
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('discount/discount', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        $data['add'] = $this->url->link('discount/discount/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['delete'] = $this->url->link('discount/discount/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['avtivestatus'] = $this->url->link('discount/discount/avtivestatus', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['pause'] = $this->url->link('discount/discount/pause', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['list'] = $this->url->link('discount/discount', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['coupon'] = $this->url->link('discount/coupon', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['access_modify'] = $this->user->hasPermission('modify', 'discount/discount');

        $data['coupon_add'] = $this->url->link('discount/coupon/add', 'user_token=' . $this->session->data['user_token'], true);

        $data['updateStartDate'] = $this->replace_url($this->url->link('discount/discount/updateStartDate', ('user_token=' . $this->session->data['user_token'] . $url), true));
        $data['updateEndDate'] = $this->replace_url($this->url->link('discount/discount/updateEndDate', ('user_token=' . $this->session->data['user_token'] . $url), true));
        $data['loadOptionOne'] = $this->replace_url($this->url->link('discount/discount/loadoptionone', 'user_token=' . $this->session->data['user_token'] . $url, true));
        $data['discounts'] = array();

        if (!empty($filter_data)) {
            if (isset($this->request->get['status_discount'])) {
                $status_discount = ($this->request->get['status_discount']);
                if (is_array($status_discount)) {
                    if (count($status_discount) > 1) {
                        $status_discount = '';
                    } else {
                        $status_discount = (int)$status_discount[0];
                    }
                }
            }
            if (!empty($discount_type_id)) {
                if (count($discount_type_id) > 0) {
                    $discount_type_id = implode(',', ($discount_type_id));
                } else {
                    $discount_type_id = (int)$discount_type_id[0];
                }
            } else {
                $discount_type_id = '';
            }
        }

        $filter_data = array(
            'filter_data' => $filter_data,
            'filter_name' => $filter_name,
            'status_discount' => $status_discount,
            'discount_type_id' => $discount_type_id,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $discount_total = $this->model_discount_discount->getTotalDiscounts($filter_data);
        $results = $this->model_discount_discount->getDiscountsCustom($filter_data);
        foreach ($results as $result) {
            $status_discount = $this->getStatusDiscount($result['date_started'], $result['date_ended'], $result['status']);
            $data['discounts'][] = [
                'discount_id' => $result['discount_id'],
                'name' => $result['name'],
                'times_used' => $result['times_used'],
                'date_started' => $result['date_started'],
                'status_discount_text' => $status_discount['text'],
                'status_discount_value' => $status_discount['value'],
                'date_ended' => $result['date_ended'],
                'edit' => $this->url->link('discount/discount/edit', 'user_token=' . $this->session->data['user_token'] . '&discount_id=' . $result['discount_id'], true),
                'delete' => $this->url->link('discount/discount/delete', 'user_token=' . $this->session->data['user_token'] . '&discount_ids=' . $result['discount_id'], true),
                'pause' => $this->url->link('discount/discount/pause', 'user_token=' . $this->session->data['user_token'] . '&discount_id=' . $result['discount_id'], true),
                'continue' => $this->url->link('discount/discount/continue', 'user_token=' . $this->session->data['user_token'] . '&discount_id=' . $result['discount_id'], true),

            ];
        }
        $data['user_token'] = $this->session->data['user_token'];

        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $discount_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('discount/discount', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        $data['pagination'] = $pagination->render();

        if (isset($this->session->data['success']) && $this->session->data['success'] != 'text_success') {
            $data['success'] = $this->session->data['success'];

            $this->session->data['success'] = '';
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];

            $this->session->data['error'] = '';
            unset($this->session->data['error']);
        } else {
            $data['error_warning'] = '';
        }

        $data['results'] = sprintf($this->language->get('text_pagination'),
            ($discount_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0,
            ((($page - 1) * $this->config->get('config_limit_admin')) > ($discount_total - $this->config->get('config_limit_admin'))) ? $discount_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')),
            $discount_total,
            ceil($discount_total / $this->config->get('config_limit_admin')));

        $data['url_filter'] = $this->url->link('discount/discount', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['filter_name'] = $filter_name;
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('discount/discount_list_append', $data));
        } else {
            $this->response->setOutput($this->load->view('discount/discount_list', $data));
        }
    }

    protected function getForm()
    {
        $data = array();
        $this->load->model('catalog/category');
        $this->load->model('catalog/manufacturer');
        $this->load->model('catalog/product');
        $this->load->model('discount/discount_type');
        $this->load->model('user/user');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['text_form'] = !isset($this->request->get['discount_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('discount/discount', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $data['text_form'],
            'href' => '',
        );

        if (isset($this->request->get['discount_id'])){
            $data['discount_id'] = $this->request->get['discount_id'];
        }

        // get discount info
        $discount_product = [];
        $data['disable_update'] = false;

        if (isset($this->request->get['discount_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $discount_info = $this->model_discount_discount->getDiscountInfo($this->request->get['discount_id']);
            $discount_product = $this->model_discount_discount->getDiscountProduct($this->request->get['discount_id']);
        }

        $data['selectedPopup'] = json_encode($this->dataDiscountProduct($discount_product));

        if (isset($this->request->post['name'])) {
            $data['name'] = $this->request->post['name'];
        }else if(!empty($discount_info)){
            $data['name'] = $discount_info['name'];
        }else{
            $data['name'] = '';
        }

        $data['status'] = isset($discount_info['status']) ? $discount_info['status'] : false;
        $date_started = isset($discount_info['date_started']) ? $discount_info['date_started'] : false;
        $date_ended = isset($discount_info['date_ended']) ? $discount_info['date_ended'] : false;
        $status_discount = $this->getStatusDiscount($date_started, $date_ended, $data['status']);
        $data['status_discount'] = isset($status_discount['value']) ? $status_discount['value'] : '';
        $data['status_discount_text'] = isset($status_discount['text']) ? $status_discount['text'] : '';

        if (isset($this->request->post['description'])) {
            $data['description'] = $this->request->post['description'];
        }else if(!empty($discount_info)){
            $data['description'] = $discount_info['description'];
        }else{
            $data['description'] = '';
        }

        if (isset($this->request->post['start_at'])) {
            $data['time_start_at'] = $this->request->post['start_at'];
        }else if(!empty($discount_info)){
            $data['time_start_at'] = $discount_info['date_started'];
        }

        if (isset($this->request->post['end_at'])) {
            $data['time_end_at'] = $this->request->post['end_at'];
        }else if(!empty($discount_info)){
            $data['time_end_at'] = $discount_info['date_ended'];
        }

        $data['user_token'] = $this->session->data['user_token'];
        $data['get_versions_info'] = $this->url->link('discount/discount/getVersionsInfo', 'user_token=' . $this->session->data['user_token'] , true);
        $data['cancel'] = $this->url->link('discount/discount', 'user_token=' . $this->session->data['user_token'] , true);
        if (!isset($this->request->get['discount_id'])) {
            $data['action'] = $this->url->link('discount/discount/add', 'user_token=' . $this->session->data['user_token'], true);
        } else {
            $data['action'] = $this->url->link('discount/discount/edit', 'user_token=' . $this->session->data['user_token'] . '&discount_id=' . $this->request->get['discount_id'], true);
        }
        $data['href_check_discount_code_exist'] = $this->url->link('discount/discount/validateDiscountCodeExist', 'user_token=' . $this->session->data['user_token'], true);
        $data['auto_code'] = $this->url->link('discount/discount/autoGenerationCode', 'user_token=' . $this->session->data['user_token'], true);
        $data['icon_delete'] = 'view/image/custom/icons/trash.png';

        $data['access_modify'] = $this->user->hasPermission('modify', 'discount/discount');

        $data['listProductPopup'] = $this->url->link('discount/discount/getProductsPopupDiscount', 'user_token=' . $this->session->data['user_token'] , true);
        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $filter_name_category = isset($this->request->get['filter_name_category']) ? urldecode(html_entity_decode($this->request->get['filter_name_category'], ENT_QUOTES, 'UTF-8')) : '';

        $item_per_page_category = 5;
        $filter = [
            "filter_name" => $filter_name_category,
            "page" => $page,
            "start" => ($page - 1) * $item_per_page_category,
            "limit" => $item_per_page_category
        ];
        $data['categories'] = $this->model_catalog_category->getCategoriesForClassify($filter);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('discount/discount_form', $data));
    }

    public function getStatusDiscount($date_start, $date_end, $status) {
        $date_now = date("Y-m-d H:i:s");
        if ($date_now > $date_end) {
            $status = self::STATUS_DISCOUNT['status_ended'];
        }
        else if ($date_now < $date_start) {
            $status = self::STATUS_DISCOUNT['status_upcoming'];
        }
        else if ($status == 1) {
            $status = self::STATUS_DISCOUNT['status_happening'];
        }
        else {
            $status = self::STATUS_DISCOUNT['status_paused'];
        }
        return $status;
    }

    public function getDiscountTypeLazyLoad()
    {
        $json = array();

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $this->load->language('discount/discount');
        $this->load->model('discount/discount');
        $this->load->model('discount/discount_type');
        $this->load->model('user/user');

        $filter_data = array(
            'order' => 'ASC',
            'start' => ($page - 1) * 6,
            'limit' => 6
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }
        /* TODO using comment when apply multi lang for database */
        // $json['results'] = $this->model_discount_discount->getDiscountTypePaginator($filter_data);

        $user_id = $this->user->getId();
        $type_user = $this->model_user_user->getTypeUser($user_id);

        $json['results'] = [
            // product
            [
                'id' => ModelDiscountDiscountType::TYPE_PRODUCT,
                'text' => $this->language->get('txt_filter_product')
            ],
            // category
            [
                'id' => ModelDiscountDiscountType::TYPE_CATEGORY,
                'text' => $this->language->get('txt_filter_category')
            ],
            // manufacture
            [
                'id' => ModelDiscountDiscountType::TYPE_MANUFACTURER,
                'text' => $this->language->get('txt_filter_manufacture')
            ],
            // order
            [
                'id' => ModelDiscountDiscountType::TYPE_ORDER,
                'text' => $this->language->get('txt_filter_order')
            ]
        ];

        if ($type_user == 'Staff') {
            array_splice($json['results'], 3);
        }

        $count_store = $this->model_discount_discount->getTotalDiscountTypeFilter($filter_data);
        $json['count'] = $count_store;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getStoreLazyLoad()
    {
        $json = array();

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $this->load->model('setting/store');

        $filter_data = array(
            'order' => 'ASC',
            'start' => ($page - 1) * 6,
            'limit' => 6
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        $json['results'] = $this->model_setting_store->getStoresForUser($filter_data);

        $count_store = $this->model_setting_store->getTotalStoreForUser($filter_data);
        $json['count'] = $count_store;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getSourceLazyLoad()
    {
        $json = array();

        $data_array = $this->source_order_data_map;


        $json['results'] = $data_array;

        $json['count'] = 4;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    protected function validateForm()
    {
        return !$this->error;
    }

    public function validateDiscountCodeExist()
    {
        $this->load->language('discount/discount');
        $this->load->model('discount/discount');

        $discount_id = isset($this->request->post['discount_id']) ? $this->request->post['discount_id'] : '';

        $discount_code = isset($this->request->post['discount_code']) ? $this->request->post['discount_code'] : '';

        $json = [
            'valid' => true
        ];

        if ($discount_code){
            if ($this->model_discount_discount->validateCodeExist($discount_code, $discount_id)){
                $json['valid'] = false;
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    protected function validateDelete()
    {
        if (!$this->user->hasPermission('modify', 'catalog/product')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function view()
    {
    }

    public function avtivestatus()
    {
        $this->load->language('discount/discount');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('discount/discount');

        $discount_ids = $this->request->get['discount_ids'];

        $discount_ids = explode(',', $discount_ids);
        $discounts = [];
        if (isset($discount_ids)) {
            $count_fail = 0;
            $count_success = 0;
            $count = 0;
            foreach ($discount_ids as $discount_id) {
                $count++;
                $discount = $this->model_discount_discount->getDiscountById($discount_id);

                if ($discount['discount_status_id'] == 2) {
                    $count_fail++;
                    $discounts[] = array(
                        'id' => $discount['discount_id'],
                        'status' => $this->language->get('text_failure'),
                        'code' => $discount['code'],
                        'name' => $discount['name'],
                        'message' => $this->language->get('txt_note_active_one')
                    );
                }
            }
            if ($count == 1) {
                if ($count_fail == 1) {
                    $this->session->data['error'] = $this->language->get('txt_note_active_one');
                }
                if ($count_success == 1) {
                    $this->session->data['success'] = $this->language->get('txt_active_success');
                }
            } else {
                if ($count_success == 0) {
                    $this->session->data['error'] = $this->language->get('txt_note_active') . ' ' . $count_success
                        . '/' . $count . ' ' . $this->language->get('txt_note_discount_success') . ' '
                        . '<a class="text-yellow action-popup">' . $this->language->get('txt_note_view_detail') . '</a>';
                }
                else{
                    $message = $this->language->get('txt_note_active') . ' ' . $count_success
                        . '/' . $count . ' ' . $this->language->get('txt_note_discount_success');
                    if ($count_success != $count) {
                        $message .=' ' . '<a class="text-yellow action-popup">' . $this->language->get('txt_note_view_detail') . '</a>';
                    }
                    $this->session->data['success'] = $message;
                }
            }
        }
        $this->session->data['discounts'] = $discounts;
        $this->response->redirect($this->url->link('discount/discount', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function updateStartDate()
    {
        $this->load->model('discount/discount');
        $discount_id = (int)$this->request->get['discount_id'];
        $start_at = $this->request->get['start_at'];

        // validate
        $error = [];
        $error_messages = [];
        $discount = $this->model_discount_discount->getDiscountInfo($discount_id);
        $data['discount_id'] = isset($discount['discount_id']) ? $discount['discount_id'] : 0;
        $data['discount_type_id'] = isset($discount['discount_type_id']) ? $discount['discount_type_id'] : '';
        $data['config'] = isset($discount['config']) ? json_decode($discount['config'], true) : [];
        $data['start_at'] = $start_at;
        $data['end_at'] = isset($discount['end_at']) ? $discount['end_at'] : '';

        if (!empty($error) && isset($error['code']) && $error['code'] != self::$ERR_CODE_SUCCESS) {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode([
                'code' => 0,
                'message' => 'Lỗi: ' . implode(' - ', $error_messages) // TODO: language...
            ]));

            return;
        }

        // update
        $this->model_discount_discount->updateStartDate($discount_id, $start_at);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode([
            'code' => 1,
            'message' => 'updateStartDate successfully'
        ]));
    }

    public function updateEndDate()
    {
        $this->load->model('discount/discount');
        $discount_id = (int)$this->request->get['discount_id'];
        $end_at = $this->request->get['end_at'];

        // validate
        $error = [];
        $error_messages = [];
        $discount = $this->model_discount_discount->getDiscountInfo($discount_id);
        $data['discount_id'] = isset($discount['discount_id']) ? $discount['discount_id'] : 0;
        $data['discount_type_id'] = isset($discount['discount_type_id']) ? $discount['discount_type_id'] : '';
        $data['config'] = isset($discount['config']) ? json_decode($discount['config'], true) : [];
        $data['start_at'] = isset($discount['start_at']) ? $discount['start_at'] : '';
        $data['end_at'] = $end_at;

        if (!empty($error) && isset($error['code']) && $error['code'] != self::$ERR_CODE_SUCCESS) {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode([
                'code' => 0,
                'message' => 'Lỗi: ' . implode(' - ', $error_messages) // TODO: language...
            ]));

            return;
        }

        // update
        $this->model_discount_discount->updateEndDate($discount_id, $end_at);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode([
            'code' => 1,
            'message' => 'updateEndDate successfully'
        ]));
    }

    public function replace_url($url)
    {
        return str_replace("amp;", "", $url);
    }

    public function autoComplete()
    {
        $this->load->model('discount/discount');
        $filter_name = $this->request->get['filter_name'];
        $results = $this->model_discount_discount->getDiscountByName($filter_name);
        $discounts = array();
        foreach ($results as $result) {
            $discounts[] = array(
                'name' => $result['discount_name'],
                'code' => $result['discount_code'],
                'edit' => $this->url->link('discount/discount/edit', 'user_token=' . $this->session->data['user_token'] . '&discount_id=' . $result['discount_id'], true),
            );
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($discounts));
    }

    public function autoGenerationCode()
    {
        // count
        $this->load->model('discount/discount');
        $count = $this->model_discount_discount->getTotalDiscountsIncludeAllStatuses();

        // make code
        $prefix = 'KM';
        $code = $prefix . str_pad($count + 1, 6, "0", STR_PAD_LEFT);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($code));
    }

    public function getProductsLazyLoad()
    {
        $json = array();
        $this->load->language('discount/discount');
        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        $this->load->model('user/user');
        $filter_data['filter_name'] = isset($this->request->get['title']) ? $this->request->get['title'] : '';

        $user_id = $this->user->getId();

        if (!$this->user->canAccessAll()) {
            $user_store_ids = $this->model_user_user->getStoreIdsOffUser($user_id);
            $filter_data['user_store_id'] = implode(",", $user_store_ids);
            $filter_data['current_user_id'] = (int)$user_id;
        }

        $results = $this->model_catalog_product->getProducts($filter_data);
        foreach ($results as $key => $value) {
            if ($value['image'] != '') {
                $image = $this->model_tool_image->resize($value['image'], 100, 100);
            } else {
                $image = $this->model_tool_image->resize('no_image.png', 100, 100);
            }
            $children_array = [];
            if (isset($value['multi_versions']) && $value['multi_versions'] == 1) {
                $product_versions = $this->model_catalog_product->getProductVersions($value['product_id']);
                foreach ($product_versions as $children) {
                    $price_children = ($children['compare_price'] != 0 ? number_format($children['compare_price'], 0, '', ',') : number_format($children['price'], 0, '', ','));
                    $children_array[] = array(
                        'id' => $value['product_id'] . '-' . $children['product_version_id'],
                        'product_id' => $value['product_id'],
                        'text' => '
                        <img class="image_treeview" src="' . $image . '">
                        <div class="version_tree">' . str_replace(',', ' - ', $children['version']) . '
                            <p class="mb-1 mt-1 font-12">
                                <span>' . $this->language->get('txt_inventory') . ': ' . $children['quantity'] . '</span> 
                                <span class="ml-5">' . $this->language->get('txt_price') . ': ' . $price_children . '</span>
                            </p>
                        </div>
                        ',
                        'name' => $value['name'],
                        'version' => '' . str_replace(',', ' - ', $children['version']) . '',
                        'quantity' => $children['quantity'],
                        'image' => $image,
                        'price' => $price_children,
                    );
                }
            }

            $price = $value['compare_price'] != 0 ? number_format($value['compare_price'], 0, '', ',') : number_format($value['price'], 0, '', ',');
            $single_version_text = '
                        <img class="image_treeview" src="' . $image . '">'.$value['name'].
                        '
                            <p class="mb-1 mt-1 font-12">
                                <span>' . $this->language->get('txt_inventory') . ': ' . $value['quantity'] . '</span> 
                                <span class="ml-5">' . $this->language->get('txt_price') . ': ' . $price . '</span>
                            </p>
                        
                        ';
            $multi_version_text = '<img class="image_treeview" src="' . $image . '">' . $value['name'];
            $json[] = array(
                'id' => $value['product_id'],
                'text' => (count($children_array) > 0) ? $multi_version_text : $single_version_text,
                'name' => $value['name'],
                'version' => '',
                'checked' => false,
                'hasChildren' => false,
                'product_name' => $value['product_name'],
                'image' => $image,
                'weight' => number_format($value['weight'], 0, '', ''),
                'price' => ($value['compare_price'] != 0 ? number_format($value['compare_price'], 0, '', ',') : number_format($value['price'], 0, '', ',')),
                'quantity' => $value['quantity'],
                'sale_on_out_of_stock' => $value['sale_on_out_of_stock'],
                'status' => $value['status'],
                'children' => $children_array
            );
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getCategoryTreeViewLazyLoad()
    {
        $this->load->language('discount/discount');
        $this->load->model('catalog/category');

        $filter_data['filter_name'] = isset($this->request->get['title']) ? $this->request->get['title'] : '';
        $json = $this->model_catalog_category->getFullTreeCategories($filter_data);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getVersionsInfo()
    {
        $json = [];

        $product_id = isset($this->request->get['product_id']) ? $this->request->get['product_id'] : null;
        $version_ids = isset($this->request->get['version_ids']) ? $this->request->get['version_ids'] : [];
        if (!$product_id || !is_array($version_ids) || empty($version_ids)) {
            return;
        }

        $this->load->model('catalog/product');
        $json['product_name'] = $this->model_catalog_product->getProductNameById($product_id);
        $json['product_image'] = $this->model_catalog_product->getProductImageById($product_id);
        $json['versions'] = $this->model_catalog_product->getVersionsByVersionIds($version_ids);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getProductsLoadView()
    {
        $json = array();
        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        $filter_data = '';
        $results = $this->model_catalog_product->getProducts($filter_data);
        foreach ($results as $key => $value) {
            if ($value['image'] != '') {
                $image = $this->model_tool_image->resize($value['image'], 100, 100);
            } else {
                $image = $this->model_tool_image->resize('no_image.png', 100, 100);
            }
            $json[$value['product_id']] = array(
                'id' => $value['product_id'],
                'product_id' => $value['product_id'],
                'children_id' => 0,
                'name' => $value['name'],
                'text' =>  '',
                'version' =>  '',
                'quantity' => $value['quantity'],
                'image' => $image,
                'price' => ($value['compare_price'] != 0 ? number_format($value['compare_price'], 0, '', ',') : number_format($value['price'], 0, '', ',')),
            );

            if (isset($value['multi_versions']) && $value['multi_versions'] == 1) {
                $product_versions = $this->model_catalog_product->getProductVersions($value['product_id']);
                foreach ($product_versions as $children) {
                    $price_children = ($children['compare_price'] != 0 ? number_format($children['compare_price'], 0, '', ',') : number_format($children['price'], 0, '', ','));
                    $json[$value['product_id'] . '-' . $children['product_version_id']] = array(
                        'id' => $value['product_id'] . '-' . $children['product_version_id'],
                        'product_id' => $value['product_id'],
                        'children_id' => $value['product_id'],
                        'name' => $value['name'],
                        'text' => '' . str_replace(',', ' - ', $children['version']) . '',
                        'version' => $children['version'],
                        'quantity' => $children['quantity'],
                        'image' => $image,
                        'price' => $price_children,
                    );
                }
            }
        }

        return json_encode($json, JSON_HEX_APOS);
    }

    private function getFullCategoriesChildrenApplied($data)
    {
        /*//example data
        $data: [112 => [110,111], 115 => [113,114]]
        */
        $result = $data;
        $this->load->model('catalog/category');
        foreach ($data as $item) {
            if (!is_array($item)){
                continue;
            }
            foreach ($item as $scid){
                $result[$scid] = $this->model_catalog_category->getAllSubCategoryIdByParentId($scid);
                if (empty($result[$scid])){
                    continue;
                }
                $childs = $this->getFullCategoriesChildrenApplied([$scid => $result[$scid]]);

                foreach ($childs as $c_id => $child){
                    $result[$c_id] = $child;
                }
            }
        }

        return $result;
    }

    public function getProductsPopupDiscount()
    {
        $json = array();

        $this->load->model('discount/discount');
        $this->load->language('discount/discount');
        $this->load->model('tool/image');

        $page = isset($this->request->get['pageNumber']) ? $this->request->get['pageNumber'] : 1;

        $limit = 25;

        // display 25 product in select config theme is many, or setup other config
        $filter_data = array(
            'limit' => $limit,
            'start' => ($page - 1) * $limit,
        );

        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }
        if (isset($this->request->get['category_id'])) {
            $filter_data['category_id'] = trim($this->request->get['category_id']);
        }
        $json['results'] = [];
        $results = $this->model_discount_discount->getProductPopupDiscount($filter_data);
        foreach ($results as $key => $value) {
            $version_name = implode(' • ', explode(',', $value['version']));
            if ($version_name != '') {
                $version_name = $this->language->get('text_version') . $version_name;
            }

            if ($value['image'] != '') {
                $image = $this->model_tool_image->resize($value['image'], 100, 100);
            } else {
                $image = $this->model_tool_image->resize('no_image.png', 100, 100);
            }

            $json['results'][] = array(
                'id' => $value['id'],
                'product_id' => $value['product_id'],
                'product_version_id' => $value['product_version_id'],
                'version' => $version_name,
                'text' => $value['name'],
                'product_name' => $value['product_name'],
                'image' => $image,
                'price' => number_format($value['price'], 0, '', ','),
                'price_real' => (float)$value['price'],
                'quantity' => $value['quantity'],
                'sku' => $value['sku'],
                'turnOn' => true,
                "price_after_reduction" => '',
                "percent_reduce" => '',
            );
        }

        $count_product = $this->model_discount_discount->countProductPopupDiscount($filter_data);
        $json['total'] = $count_product;
        $json['currentPage'] = $page;
        $json['perPage'] = $limit;
        $json['totalPage'] = round($count_product/$limit);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function dataDiscountProduct($discount_product)
    {
        $result = [];

        $this->load->model('tool/image');
        if (!empty($discount_product)) {
            foreach ($discount_product as $key => $item) {
                $p_v_id = $item['product_id'];
                if ($item['product_version_id']) {
                    $p_v_id = $p_v_id . '-' . $item['product_version_id'];
                }

                $version_name = implode(' • ', explode(',', $item['version']));
                if ($version_name != '') {
                    $version_name = $this->language->get('text_version') . $version_name;
                }

                if ($item['image'] != '') {
                    $image = $this->model_tool_image->resize($item['image'], 100, 100);
                } else {
                    $image = $this->model_tool_image->resize('no_image.png', 100, 100);
                }

                $price = empty($item['multi_versions']) ? number_format((float)$item['p_compare_price'], 0, '', ',') : number_format((float)$item['pv_compare_price'], 0, '', ',');
                $price_real = empty($item['multi_versions']) ? (float)$item['p_compare_price'] : (float)$item['pv_compare_price'];
                $sku = empty($item['multi_versions']) ? $item['p_sku'] : $item['pv_sku'];
                $turn_on = $item['status'] == 1;

                $percent = round(($price_real - (float)$item['price_discount']) / $price_real * 100);

                if ((float)$item['price_discount'] == 0) {
                    $percent = 100;
                }

                $result[] = [
                    "id" => $p_v_id,
                    "product_id" => $item['product_id'],
                    "product_version_id" => $item['product_version_id'],
                    "version" => $version_name,
                    "product_name" => $item['name'],
                    "image" => $image,
                    "price" => $price,
                    "price_real" => $price_real,
                    "quantity" => (float)$item['pts_quantity'],
                    "sku" => $sku,
                    "turnOn" => $turn_on,
                    "price_after_reduction" => number_format((float)$item['price_discount'], 0, '', ','),
                    "percent_reduce" => $percent,
                ];
            }
        }

        return $result;
    }
}

?>