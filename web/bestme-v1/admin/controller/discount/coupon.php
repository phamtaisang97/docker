<?php

class ControllerDiscountCoupon extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('discount/discount');
        $this->document->setTitle($this->language->get('heading_title'));

        $this->getList();
    }

    public function getList()
    {
        $filter_name = $this->getValueFromRequest('get', 'filter_name');
        $page = $this->getValueFromRequest('get', 'page', 1);
        $filter_data = $this->getValueFromRequest('get', 'filter_data');

        $url = '';
        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->load->language('discount/discount');
        $data['footer'] = $this->load->controller('common/footer');
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['add'] = $this->url->link('discount/discount/add', 'user_token=' . $this->session->data['user_token'], true);
        $data['list'] = $this->url->link('discount/discount', 'user_token=' . $this->session->data['user_token'], true);
        $data['coupon'] = $this->url->link('discount/coupon', 'user_token=' . $this->session->data['user_token'], true);
        $this->document->setTitle($this->language->get('heading_title'));

        $this->document->setTitle($this->language->get('heading_title'));
        // add css,js only list product
        $this->document->addStyle('view/stylesheet/custom/product.css');

        $data['coupon_add'] = $this->url->link('discount/coupon/add', 'user_token=' . $this->session->data['user_token'], true);

        $filter = [
            'filter_name' => $filter_name,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        ];

        $this->load->model('discount/coupon');

        $total_coupons = $this->model_discount_coupon->getTotalCoupons($filter);
        $results = $this->model_discount_coupon->getListCoupons($filter);
        $coupons = [];
        foreach ($results as $result) {
            $coupons[] = [
                'name' => $result['name'],
                'code' => $result['code'],
                'type' => $result['type'],
                'discount' => $result['discount'],
                'uses_total' => $result['uses_total'],
                'date_start' => $result['date_start'],
                'date_end' => $result['date_end'],
                'edit' => $this->url->link('discount/coupon/edit', 'user_token=' . $this->session->data['user_token'] . '&coupon_id=' . $result['coupon_id'] . $url, true),
            ];
        }

        //var_dump($coupons);die;
        $data['coupons'] = $coupons;

        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $total_coupons;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('discount/discount', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        $data['pagination'] = $pagination->render();
        $data['results'] = sprintf($this->language->get('text_pagination'), ($total_coupons) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($total_coupons - $this->config->get('config_limit_admin'))) ? $total_coupons : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $total_coupons, ceil($total_coupons / $this->config->get('config_limit_admin')));

        $data['user_token'] = $this->session->data['user_token'];
        $data['url_filter'] = $this->url->link('discount/coupon', 'user_token=' . $this->session->data['user_token'] . $url, true);

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('discount/coupon_list_append', $data));
        } else {
            $this->response->setOutput($this->load->view('discount/coupon_list', $data));
        }
    }

    public function importFromExcelFile()
    {
        $this->load->language('discount/discount');
        $this->load->model('discount/coupon');

        $json = array();
        // Allowed file mime types
        $allowed = array(
            'application/vnd.ms-excel',          // xls
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' // xlsx
        );
        $file = $this->request->files['import-file'];
        $max_file_size = defined('MAX_FILE_SIZE_IMPORT_PRODUCTS') ? MAX_FILE_SIZE_IMPORT_PRODUCTS : 1048576;
        if (!isset($file['size']) || $file['size'] > $max_file_size) {
            $json['error'] = $this->language->get('error_filesize');
        }
        if (!in_array($file['type'], $allowed)) {
            $json['error'] = $this->language->get('error_filetype');
        }

        if (!$json) {
            try {
                // start set read data only
                $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($file['tmp_name']);
                $reader->setReadDataOnly(true);
                // end

                $spreadsheet = $reader->load($file['tmp_name']);
                $firstSheetIndex = $spreadsheet->getFirstSheetIndex();
                $sheetData = $spreadsheet->getSheet($firstSheetIndex)->toArray(null, true, true, true);
            } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
                $json['error'] = $this->language->get('error_unknown');
                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json));
            }
            $max_file_rows = defined('MAX_ROWS_IMPORT_PRODUCTS') ? MAX_ROWS_IMPORT_PRODUCTS : 501;
            if (count($sheetData) > $max_file_rows) {
                $json['error'] = $this->language->get('error_limit');
            } else {
                $header = array_shift($sheetData);
                $header = $this->mapHeaderFromFile($header); /// remove empty element
                if (!$header) {
                    $json['error'] = $this->language->get('error_file_incomplete');
                } else {
                    if (!array_key_exists('code', $header)) {
                        $json['error'] = $this->language->get('error_not_found_fields');
                    } else {
                        $dataCoupons = $this->mapDataFromFile($sheetData, $header);
                        if (array_key_exists('code', $dataCoupons)) { // check for parent
                            $row = array_key_exists('row', $dataCoupons) ? $dataCoupons['row'] : 'UNKNOW';
                            $json['error'] = $this->language->get('error_file_incomplete_in_row') . $row;
                        }

                        if (!$json) {
                            $this->model_discount_coupon->processImportExcel($dataCoupons);
                        }
                    }
                }
            }
        }

        if (!$json) {
            $json['success'] = $this->language->get('text_uploaded') . ' ' . count($dataCoupons) . $this->language->get('text_coupon_uploaded');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    protected function mapHeaderFromFile(array $header)
    {
        $arrMap = array(
            "code" => 'code',
            "allow_with_discount" => 'allow_with_discount',
            "limit_times" => 'limit_times',
            "date_start" => 'date_start',
            "date_end" => 'date_end',
            "type" => 'type',
            "discount" => 'discount',
            "apply_for" => 'apply_for',
            "applicable_type" => 'applicable_type',
            "applicable_type_value" => 'applicable_type_value',
            "order_value_from" => 'order_value_from',
            "id" => 'id',
            "how_to_apply" => 'how_to_apply',
            "subjects_of_application" => 'subjects_of_application',
            "sale_channel" => 'sale_channel'
        );

        $arrMap_2 = array(
            "code" => 'code',
            "allow_with_discount" => 'allow_with_discount',
            "limit_times" => 'limit_times',
            "date_start" => 'date_start',
            "date_end" => 'date_end',
            "type" => 'type',
            "discount" => 'discount',
            "apply_for" => 'apply_for',
            "applicable_type" => 'applicable_type',
            "applicable_type_value" => 'applicable_type_value',
            "order_value_from" => 'order_value_from',
            "id" => 'id',
            "how_to_apply" => 'how_to_apply',
            "subjects_of_application" => 'subjects_of_application',
            "sale_channel" => 'sale_channel'
        );

        $result = array();
        foreach ($header as $key => $value) {
            $value = trim($value);
            if (array_key_exists($value, $arrMap)) {
                $result[$arrMap[$value]] = $key;
            }
        }

        // for import different language
        if (count($result) < count($arrMap)) {
            foreach ($header as $key => $value) {
                $value = trim($value);
                if (array_key_exists($value, $arrMap_2)) {
                    $result[$arrMap_2[$value]] = $key;
                }
            }
        }

        $flag = 1;
        foreach ($arrMap as $k => $v) {
            if (!array_key_exists($v, $result)) {
                $flag = 0;
                break;
            }
        }

        // for import different language
        if (!$flag) {
            foreach ($arrMap_2 as $k => $v) {
                if (!array_key_exists($v, $result)) {
                    return false;
                }
            }
        }

        return $result;
    }

    protected function mapDataFromFile(array $data, array $header)
    {
        if (!is_array($data) || empty($data) || !is_array($header) || empty($header)) {
            return [];
        }

        $result = array();
        foreach ($data as $row => $info) {
            $current = $header;
            foreach ($current as $k => $v) {
                switch ($k) {
                    case 'order_value_from':
                    case 'discount':
                        $current[$k] = (float)$info[$v];
                        break;
                    case 'apply_for':
                        $apply_for =  isset(ModelDiscountCoupon::VALUE_COUPON_APPLY_FOR[$info[$v]]) ? ModelDiscountCoupon::VALUE_COUPON_APPLY_FOR[$info[$v]] : 1;
                        $current[$k] = $apply_for;
                        break;
                    case 'date_start':
                    case 'date_end':
                        $time = strtotime($info[$v]);
                        $newFormat = date('Y-m-d H:i:s', $time);
                        $current[$k] = $newFormat;
                        break;
                    case 'id':
                        $current_values = preg_split('/\r\n|\r|\n/', $info[$v]);
                        $current_values_trim_space = [];
                        foreach ($current_values as $c_val) {
                            if (trim($c_val) != '' && strlen($c_val) <= 100) {
                                $current_values_trim_space[] = trim($c_val);
                            }
                        }
                        $current[$k] = $current_values_trim_space;

                        break;
                    case 'how_to_apply':
                        $how_to_apply = ModelDiscountCoupon::VOUCHER_PRODUCT_HOW_TO_APPLY[$info[$v]] ?? 1;
                        $current[$k] = $how_to_apply;
                        break;
                    case 'sale_channel':
                        $sale_chanel = ModelDiscountCoupon::VOUCHER_SALE_CHANEL[$info[$v]] ?? 1;
                        $current[$k] = $sale_chanel;
                        break;
                    case 'type':
                        $type = ModelDiscountCoupon::VOUCHER_TYPE_LIST[$info[$v]] ?? 1;
                        $current[$k] = $type;
                        break;
                    default:
                        $current[$k] = $info[$v];
                }
            }

            $result[] = $current;
        }

        return $result;
    }

    public function add()
    {
        $this->load->language('discount/discount');
        $this->load->model('discount/coupon');
        $this->document->setTitle($this->language->get('heading_title_add_list'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validatePostData()) {
            $data = $this->request->post;

            $this->model_discount_coupon->addCoupon($data);

            $this->response->redirect($this->url->link('discount/coupon', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();
    }

    public function edit()
    {
        $this->load->language('discount/discount');
        $this->load->model('discount/coupon');
        $this->document->setTitle($this->language->get('heading_title_edit'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validatePostData()) {
            $data = $this->request->post;

            $this->model_discount_coupon->editCoupon($this->request->get['coupon_id'], $data);

            $this->response->redirect($this->url->link('discount/coupon', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();
    }

    protected function getForm()
    {
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['text_form'] = !isset($this->request->get['coupon_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('discount/coupon', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $data['text_form'],
            'href' => '',
        );

        if (isset($this->request->get['coupon_id'])){
            $data['coupon_id'] = $this->request->get['coupon_id'];
        }

        if (isset($this->request->get['coupon_id'])) {
            $coupon_info = $this->model_discount_coupon->getCouponInfo($this->request->get['coupon_id']);
            $coupon_product = $this->model_discount_coupon->getCouponProduct($this->request->get['coupon_id']);
        }

        if (!isset($this->request->get['coupon_id'])) {
            $data['action'] = $this->url->link('discount/coupon/add', 'user_token=' . $this->session->data['user_token'], true);
        } else {
            $data['action'] = $this->url->link('discount/coupon/edit', 'user_token=' . $this->session->data['user_token'] . '&coupon_id=' . $this->request->get['coupon_id'], true);
        }

        if (isset($this->request->post['name'])) {
            $data['name'] = $this->request->post['name'];
        } else if (!empty($coupon_info)) {
            $data['name'] = $coupon_info['name'];
        } else {
            $data['name'] = '';
        }

        if (isset($this->request->post['code'])) {
            $data['code'] = $this->request->post['code'];
        } else if (!empty($coupon_info)) {
            $data['code'] = $coupon_info['code'];
        } else {
            $data['code'] = '';
        }

        if (isset($this->request->post['type'])) {
            $data['type'] = $this->request->post['type'];
        } else if (!empty($coupon_info)) {
            $data['type'] = $coupon_info['type'];
        } else {
            $data['type'] = '';
        }

        if (isset($this->request->post['discount'])) {
            $data['discount'] = $this->request->post['discount'];
        } else if (!empty($coupon_info)) {
            $data['discount'] = $coupon_info['discount'];
        } else {
            $data['discount'] = '';
        }

        if (isset($this->request->post['uses_total'])) {
            $data['uses_total'] = $this->request->post['uses_total'];
        } else if (!empty($coupon_info)) {
            $data['uses_total'] = $coupon_info['uses_total'];
        } else {
            $data['uses_total'] = '';
        }

        if (isset($this->request->post['allow_with_discount'])) {
            $data['allow_with_discount'] = $this->request->post['allow_with_discount'];
        } else if (!empty($coupon_info)) {
            $data['allow_with_discount'] = $coupon_info['allow_with_discount'];
        } else {
            $data['allow_with_discount'] = '';
        }

        if (isset($this->request->post['limit_times'])) {
            $data['limit_times'] = $this->request->post['limit_times'];
        } else if (!empty($coupon_info)) {
            $data['limit_times'] = $coupon_info['limit_times'];
        } else {
            $data['limit_times'] = '';
        }

        if (isset($this->request->post['max_type_value'])) {
            $data['max_type_value'] = $this->request->post['max_type_value'];
        } else if (!empty($coupon_info)) {
            $data['max_type_value'] = $coupon_info['max_type_value'];
        } else {
            $data['max_type_value'] = '';
        }

        if (isset($this->request->post['apply_for'])) {
            $data['apply_for'] = $this->request->post['apply_for'];
        } else if (!empty($coupon_info)) {
            $data['apply_for'] = $coupon_info['apply_for'];
        } else {
            $data['apply_for'] = '';
        }

        if (isset($this->request->post['applicable_type'])) {
            $data['applicable_type'] = $this->request->post['applicable_type'];
        } else if (!empty($coupon_info)) {
            $data['applicable_type'] = $coupon_info['applicable_type'];
        } else {
            $data['applicable_type'] = '';
        }

        if (isset($this->request->post['applicable_type_value'])) {
            $data['applicable_type_value'] = $this->request->post['applicable_type_value'];
        } else if (!empty($coupon_info)) {
            $data['applicable_type_value'] = $coupon_info['applicable_type_value'];
        } else {
            $data['applicable_type_value'] = '';
        }

        if (isset($this->request->post['sale_channel'])) {
            $data['sale_channel'] = $this->request->post['sale_channel'];
        } else if (!empty($coupon_info)) {
            $data['sale_channel'] = $coupon_info['sale_channel'];
        } else {
            $data['sale_channel'] = '';
        }

        if (isset($this->request->post['order_value_from'])) {
            $data['order_value_from'] = $this->request->post['order_value_from'];
        } else if (!empty($coupon_info)) {
            $data['order_value_from'] = $coupon_info['order_value_from'];
        } else {
            $data['order_value_from'] = '';
        }

        if (isset($this->request->post['how_to_apply'])) {
            $data['how_to_apply'] = $this->request->post['how_to_apply'];
        } else if (!empty($coupon_info)) {
            $data['how_to_apply'] = $coupon_info['how_to_apply'];
        } else {
            $data['how_to_apply'] = '';
        }

        if (isset($this->request->post['subjects_of_application'])) {
            $data['subjects_of_application'] = $this->request->post['subjects_of_application'];
        } else if (!empty($coupon_info)) {
            $data['subjects_of_application'] = $coupon_info['subjects_of_application'];
        } else {
            $data['subjects_of_application'] = '';
        }

        if (isset($this->request->post['never_expired'])) {
            $data['never_expired'] = $this->request->post['never_expired'];
        } else if (!empty($coupon_info)) {
            $data['never_expired'] = $coupon_info['never_expired'];
        } else {
            $data['never_expired'] = '';
        }

        if (isset($this->request->post['date_start'])) {
            $data['date_start'] = $this->request->post['date_start'];
        } else if (!empty($coupon_info)) {
            $data['date_start'] = $coupon_info['date_start'];
        } else {
            $data['date_start'] = '';
        }

        if (isset($this->request->post['date_end'])) {
            $data['date_end'] = $this->request->post['date_end'];
        } else if (!empty($coupon_info)) {
            $data['date_end'] = $coupon_info['date_end'];
        } else {
            $data['date_end'] = '';
        }

        $data['selected_pv_ids'] = [];
        if (!empty($coupon_product)) {
            foreach ($coupon_product as &$item) {
                $item['pvid'] = $item['product_version_id'] != 0 ? $item['product_id'] . '-' . $item['product_version_id'] : $item['product_id'];
                $data['selected_pv_ids'][] = $item;
            }
        }

        $data['arr_apply_for'] = ModelDiscountCoupon::COUPON_APPLY_FOR;

        $data['user_token'] =  $this->session->data['user_token'];
        $data['href_check_coupon_code_exist'] = $this->url->link('discount/coupon/validateDiscountCodeExist', 'user_token=' . $this->session->data['user_token'], true);
        $data['auto_code'] = $this->url->link('discount/coupon/autoGenerationCode', 'user_token=' . $this->session->data['user_token'], true);
        $data['cancel'] = $this->url->link('discount/coupon', 'user_token=' . $this->session->data['user_token'], true);

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('discount/coupon_form', $data));
    }

    public function validateDiscountCodeExist()
    {
        $this->load->language('discount/discount');
        $this->load->model('discount/coupon');

        $json = [
            'valid' => true
        ];

        $coupon_id = isset($this->request->post['coupon_id']) ? $this->request->post['coupon_id'] : '';
        $coupon_code = isset($this->request->post['code']) ? $this->request->post['code'] : '';

        if ($coupon_code){
            if ($this->model_discount_coupon->validateCodeExist($coupon_code, $coupon_id)){
                $json['valid'] = false;
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function autoGenerationCode()
    {
        // count
        $this->load->model('discount/coupon');
        $count = $this->model_discount_coupon->getTotalCouponsIncludeAllStatuses();

        // make code
        $prefix = 'KM';
        $code = $prefix . str_pad($count + 1, 6, "0", STR_PAD_LEFT);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($code));
    }

    public function getProductsLazyLoad()
    {
        $json = array();
        $count_tag = 0;

        $this->load->language('discount/discount');
        $this->load->model('discount/coupon');
        $this->load->model('tool/image');

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        // display 6 product in select config theme is many, or setup other config
        $filter_data = array(
            'limit' => 6,
            'start' => ($page - 1) * 6,
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        if (!$this->user->hasPermission('modify', 'catalog/product')) {
            $json['count'] = $count_tag;
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));

            return;
        }

        $results = $this->model_discount_coupon->getProductByStore(0, $filter_data);
        foreach ($results as $key => $value) {
            $version_name = implode(' • ', explode(',', $value['version']));
            if ($version_name != '') {
                $version_name = $this->language->get('text_version') . $version_name;
            }

            if ($value['image'] != '') {
                $image = $this->model_tool_image->resize($value['image'], 100, 100);
            } else {
                $image = $this->model_tool_image->resize('no_image.png', 100, 100);
            }

            $json['results'][] = array(
                'id' => $value['id'],
                'product_id' => $value['product_id'],
                'product_version_id' => $value['product_version_id'],
                'version' => $version_name,
                'subname' => $version_name,
                'text' => $value['name'],
                'product_name' => $value['product_name'],
                'image' => $image,
                'weight' => number_format($value['weight'], 0, '', ''),
                'price' => number_format($value['price'], 0, '', ','),
                'quantity' => $value['quantity'],
                'sale_on_out_of_stock' => $value['sale_on_out_of_stock'],
                'status' => $value['status'],
                'sku' => $value['sku'],
                'cost_price' => (float)$value['cost_price']
            );
        }

        $count_tag = $this->model_discount_coupon->countProductByStore(0, $filter_data);
        $json['count'] = $count_tag;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function validatePostData()
    {
        if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 128)) {
            $this->error['warning'] = $this->language->get('txt_error_name');
        }

        if ((utf8_strlen($this->request->post['code']) < 1) || (utf8_strlen($this->request->post['code']) > 20)) {
            $this->error['warning'] = $this->language->get('txt_error_code');
        }

        $coupon_id = isset($this->request->get['coupon_id']) ? $this->request->get['coupon_id'] : '';

        if ($this->model_discount_coupon->validateCodeExist($this->request->post['code'], $coupon_id)){
            $this->error['warning'] = $this->language->get('error_code_exist');
        }

        return !$this->error;
    }
}