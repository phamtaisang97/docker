<?php

class ControllerAppStoreMyApp extends Controller
{
    use Open_Api;
    public function index()
    {
        /* Trả danh sách các App đã cài đặt cùng với các module/action cho các app */
        $this->load->language('appstore/my_app');
        $this->document->addStyle('view/stylesheet/custom/my_app.css');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('appstore/my_app');
        $this->load->model('setting/appstore_setting');
        $this->getList();
    }

    public function install()
    {
        $this->load->language('extension/extension/appstore');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $app_code = $this->request->post['app_code'];
            $this->load->model('appstore/my_app');
            // Làm lại hàm check install trường hợp đã dùng thử và hết hạn
            if (!$this->model_appstore_my_app->checkInstall($app_code)) {

                $this->load->language('extension/extension/appstore');

                $this->load->model('setting/extension');

                $this->load->model('setting/appstore_setting');

                $this->model_setting_extension->install('appstore', $app_code);

                $this->load->model('user/user_group');

                $this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'extension/appstore/' . $app_code);
                $this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'extension/appstore/' . $app_code);

                // Add app to my_app
                $used_time = $this->request->post['used_time'];
                $this->model_appstore_my_app->install($app_code, $used_time);

                // Call install method if it exsits
                $this->load->controller('extension/appstore/' . $app_code . '/install');
            }
            header('Content-Type: application/json');
            $data = json_encode([
                'code' => 201,
                'message' => 'Post install app success',
                'value' => $this->request->post['app_code']
            ]);

            $this->response->setOutput($data);
            return;
        } else {
            /* Nhận mã app_code từ CMS gửi về */

            /* Kiểm tra xem app đã cài chưa
               TH1: Đã cài thì redirect về thông tin App
               TH2: Chưa cài thì tiến hành cài đặt
            */
            $app_code = $this->request->get['app_code'];

            $this->load->model('appstore/my_app');

            if (!$this->model_appstore_my_app->checkInstall($app_code)) {

                $this->load->language('extension/extension/appstore');

                $this->load->model('setting/extension');

                $this->load->model('setting/appstore_setting');

                $this->model_setting_extension->install('appstore', $app_code);

                $this->load->model('user/user_group');

                $this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'extension/appstore/' . $app_code);
                $this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'extension/appstore/' . $app_code);

                // Add app to my_app
                $this->model_appstore_my_app->install($app_code);

                // Call install method if it exsits
                $this->load->controller('extension/appstore/' . $app_code . '/install');
            }
            $redirect = $this->url->link('app_store/my_app', 'user_token=' . $this->session->data['user_token'], true);
            $this->response->redirect($redirect);
        }
    }

    public function uninstall()
    {
        $this->load->model('setting/extension');

        $this->load->model('setting/appstore_setting');

        if ($this->validate()) {
            /* Bước 1: Xóa các cài đặt của app trong các bảng: oc_appstore, extension, module liên quan */

            $this->model_setting_extension->uninstall('appstore', $this->request->get['app_code']);

            $this->model_setting_appstore_setting->deleteModulesByCode($this->request->get['app_code']);

            // Call uninstall method if it exsits
            $this->load->controller('extension/appstore/' . $this->request->get['app_code'] . '/uninstall');

            /* Bước 2: Xóa/thay đổi status của app trong my_app */
            $this->load->controller('extension/appstore/' . $this->request->get['app_code'] . '/uninstall');

            $this->load->model('appstore/my_app');

            $this->model_appstore_my_app->uninstall($this->request->get['app_code']);

        }
        $redirect = $this->url->link('app_store/my_app', 'user_token=' . $this->session->data['user_token'], true);
        $this->response->redirect($redirect);
    }

    public function expiration()
    {
        $this->load->language('appstore/my_app');
        $this->document->setTitle($this->language->get('heading_title_expiration'));
        $app_code = $this->request->get['app_code'];
        $this->load->model('appstore/my_app');
        $data['app_info'] = $this->model_appstore_my_app->getInfo($app_code);

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('appstore/expiration', $data));

    }

    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'extension/extension/appstore')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    protected function getList()
    {
        $limit_paginate = 5;
        $page = $this->getValueFromRequest('get', 'page', 1);
        $filter_data = array();

        $url = '';
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }
        $filter_data = array(
            'start' => ($page - 1) * $limit_paginate,
            'limit' => $limit_paginate
        );
        $extension_total = $this->model_appstore_my_app->getTotalInstalledApp();

        $extensions = $this->model_appstore_my_app->getInstalledCustom($filter_data);
        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $extension_total;
        $pagination->page = $page;
        $pagination->limit = $limit_paginate;
        $pagination->url = $this->url->link('app_store/my_app', 'user_token=' . $this->session->data['user_token']  . '&page={page}', true);

        $data['pagination'] = $pagination->render();
        $data['url_filter'] = $this->url->link('app_store/my_app', 'user_token=' . $this->session->data['user_token'] , true);
        $data['extensions'] = array();
        // Create a new language container so we don't pollute the current one
        $language = new Language($this->config->get('config_language'));

        // Compatibility code for old extension folders
        //$files = glob(DIR_APPLICATION . 'controller/extension/appstore/*.php');
        foreach ($extensions as $extension) {
            $this->load->language('extension/appstore/' . $extension, 'extension');
            $info = $this->model_appstore_my_app->getInfo($extension);
            $expiration = $info['expiration_date'];
            $now_calculate = date('Y-m-d', strtotime('+3 days'));
            $expiration_status = false;
            if ((strtotime($expiration) < strtotime($now_calculate)) && isset($expiration)) {
                $expiration_status = true;
            }
            $link_detail = $this->url->link('extension/appstore/' . $extension . '/introduce', 'user_token=' . $this->session->data['user_token'], true);
            $link_edit = $this->url->link('extension/appstore/' . $extension, 'user_token=' . $this->session->data['user_token'], true);

            if ($info['status'] == 0) {
                $link_detail = $this->url->link('app_store/my_app/expiration', 'user_token=' . $this->session->data['user_token'] . '&app_code=' . $extension, true);
                $link_edit = $this->url->link('app_store/my_app/expiration', 'user_token=' . $this->session->data['user_token'] . '&app_code=' . $extension, true);
            }

            if ($extension == ModelAppstoreMyApp::APP_MAX_LEAD) {
                $link_detail = $this->url->link('custom/preference', 'user_token=' . $this->session->data['user_token'] . '&app_code=' . $extension, true);
                $link_edit = $link_detail;
            }

            if ($extension == ModelAppstoreMyApp::APP_GHN || $extension == ModelAppstoreMyApp::APP_VIETTEL_POST) {
                $link_detail = $this->url->link('settings/delivery', 'user_token=' . $this->session->data['user_token'] . '&app_code=' . $extension, true);
                $link_edit = $link_detail;
            }

            $data['extensions'][] = array(
                'name' => $info['app_name'],
                'path_logo' => $info['path_logo'],
                'expiration_date' => isset($info['expiration_date']) ? date("d/m/Y", strtotime($info['expiration_date'])) : $this->language->get('text_forever'),
                'expiration_status' => $expiration_status,
                'link_detail' => $link_detail,
                'link_edit' => $link_edit,
                'uninstall' => $this->url->link('app_store/my_app/uninstall', 'user_token=' . $this->session->data['user_token'] . '&app_code=' . $extension, true),
                'installed' => in_array($extension, $extensions),
                'removable' => true
            );
        }
        //v3.5.1
        $extensions_new = $this->getAppListOpenApi();
        if (is_array($extensions_new) && isset($extensions_new)){
            foreach ($extensions_new as $extension) {
                if (!isset($extension['app'])) {
                    continue;
                }
                $data['extensions'][] = array(
                    'name' => $extension['app']['name'],
                    'path_logo' => OPEN_API_BASE_URL_IMAGE_LOGO.$extension['app']['logo'],
                    'expiration_date' => isset($extension['end_date']) ? date("d/m/Y", strtotime($extension['end_date'])) : $this->language->get('text_forever'),
                    'expiration_status' => "expiration_status",
                    'link_detail' => $this->url->link('app_store/detail', 'user_token=' . $this->session->data['user_token'] .'&app_id='. $extension['app']['id'], true),
                    'link_edit' => $this->url->link('app_store/detail', 'user_token=' . $this->session->data['user_token'] .'&app_id='. $extension['app']['id'], true),
                    'uninstall' => $this->url->link('app_store/my_app/uninstall_new', 'user_token=' . $this->session->data['user_token'] . '&app_id=' . $extension['app']['id'], true),
                    'installed' => $this->url->link('app_store/detail', 'user_token=' . $this->session->data['user_token'] .'&app_id='. $extension['app']['id'], true),
                    'removable' => !defined('SOURCE_PRODUCTION_FOR_ADG') || SOURCE_PRODUCTION_FOR_ADG == false
                );
            }
        }
        $this->load->model('user/user');
        $data['email'] = $this->model_user_user->getEmailByUser($this->user->getId());
        $this->load->model('setting/setting');
        $data['shop_name'] = $this->model_setting_setting->getShopName();
        $app_redirect = $this->url->link('common/bestme_install_app', 'user_token=' . $this->session->data['user_token'], true);
        $app_redirect = urlencode($app_redirect);
        $data['themes_store_link'] = BESTME_SERVER . 'kho-ung-dung?app_redirect=' . $app_redirect . '&email=' . $data['email'] . '&shop_name=' . $data['shop_name'];

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('appstore/my_app', $data));
    }

    public function getValueFromRequest($method, $fieldRequest, $value = null)
    {
        $data = $this->request->$method;
        if (isset($data[$fieldRequest])) {
            return $data[$fieldRequest];
        }
        if ($value) {
            return $value;
        }
        return '';
    }

    public function getAppListOpenApi() {
        $baseUrl = OPEN_API_BASE_URL;
        $this->load->model('setting/setting');
        $shopname = $this->config->get('shop_name');
        $token = $this->getTokenApiKeyByShopName($shopname);
        $url = "{$baseUrl}/app-list?shop_name=$shopname&token=$token";
        $response = $this->call_api($url);
        return $response;
    }

    // uninstall app v3.5.1 open api
    public function uninstall_new() {
        $appid = $this->request->get['app_id'];
        $baseUrl = OPEN_API_BASE_URL;
        $this->load->model('setting/setting');
        $shopname = $this->config->get('shop_name');
        $token = $this->getTokenApiKeyByShopName($shopname);
        $url = "{$baseUrl}/uninstall?token=$token";
        $body = '{
                 "app_id": "'.$appid.'",
                 "shop_name": "'.$shopname.'"
            }';
        $this->call_api($url, $body, "POST");
        $redirect = $this->url->link('app_store/my_app', 'user_token=' . $this->session->data['user_token'], true);
        $this->response->redirect($redirect);
    }
}