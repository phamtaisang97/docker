<?php

class ControllerAppStoreDetail extends Controller
{
    use Open_Api;
    public function index()
    {
        $this->load->language('appstore/my_app');
        $this->document->addStyle('view/stylesheet/custom/my_app.css');
        $baseUrl = OPEN_API_BASE_URL;
        $this->load->model('setting/setting');
        $shopname = $this->model_setting_setting->getShopName();
        $appid = $this->request->get['app_id'];
        $token = $this->getTokenApiKeyByShopName($shopname);
        $url = "{$baseUrl}/app-detail?shop_name=$shopname&appid=$appid&token=$token";
        $data['app_detail']  = $this->call_api($url);
        $this-> document->setTitle($data['app_detail']['name']);
        $data['path_logo'] = OPEN_API_BASE_URL_IMAGE_LOGO.$data['app_detail']['logo'];

        // modify app url
        $data['app_detail']['url'] = $this->getAppUrl($data['app_detail'], $shopname);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('appstore/detail_new', $data));
    }

    /**
     * add hmac|shop parameters to app url
     *
     * @param $app_detail
     * @param $shopname
     * @return string
     */
    private function getAppUrl($app_detail, $shopname) {
        $hash_message = 'shop=' . $shopname;

        // calculate hmac
        $secret_key = $app_detail['secret_key'];
        $hash_algorithm = 'sha256';
        $hash_hmac_value = hash_hmac($hash_algorithm, $hash_message, $secret_key);

        $modify_parameters = 'hmac=' . $hash_hmac_value . '&shop=' . $shopname;

        return modifyUrlParameters($modify_parameters, $app_detail['url']);
    }
}