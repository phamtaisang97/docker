<?php

class ControllerThemeSectionTheme extends Controller
{
    use Theme_Config_Util;
    use Device_Util;
    const LIST_THEMES_SUPPORT_PRODUCT_VERSION_FONTS = ['food_grocerie', 'tech_sun_electro', 'fashion_flavor', 'furniture_furniter',
        'furniture_sfurniture', 'food_kafeviet', 'tech_egomall', 'food_dacsanviet', 'food_hanafood', 'car_parts_auto'];
    public function index()
    {
        $data = array();

        $this->load->language('theme/section_theme');

        if(isset($this->request->get['idx'])){
            $data['slide_idx'] = $this->request->get['idx'];
        }

        $this->document->setTitle($this->language->get('heading_title'));

        /* error */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        /* config with undo|redo|change api */
        $action = isset($this->request->get['action']) ? $this->request->get['action'] : 'config';
        if (in_array($action, ['config', 'undo', 'redo', 'change'])) {
            // backup request "key"
            $orig_key = $this->request->get['key'];

            $this->load->model('extension/module/theme_builder_config');

            $data['config'] = [];

            // color
            $this->request->get['key'] = ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_COLOR;
            $color_config = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $data['config']['color'] = is_array($color_config) ? $color_config : json_decode($color_config, true);

            // text
            $this->request->get['key'] = ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_TEXT;
            $text_config = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $data['config']['text'] = is_array($text_config) ? $text_config : json_decode($text_config, true);

            //v3.4.3-finetune
            $current_theme = $this->config->get('config_theme');
            if (in_array($current_theme, self::LIST_THEMES_SUPPORT_PRODUCT_VERSION_FONTS)) {
                $array_fonts = $data['config']['text']['all']['supported-fonts'];
                if (!in_array("Quicksand", $array_fonts)) {
                    array_push($array_fonts, "Quicksand");
                }
                if (!in_array("Montserrat", $array_fonts)) {
                    array_push($array_fonts, "Montserrat");
                }
                if (!in_array("KoHo", $array_fonts)) {
                    array_push($array_fonts, "KoHo");
                }
                if (!in_array("Newsreader", $array_fonts)) {
                    array_push($array_fonts, "Newsreader");
                }
                if (!in_array("Oswald", $array_fonts)) {
                    array_push($array_fonts, "Oswald");
                }
                if (!in_array("Playfair Display", $array_fonts)) {
                    array_push($array_fonts, "Playfair Display");
                }
                if (!in_array("Lora", $array_fonts)) {
                    array_push($array_fonts, "Lora");
                }
                if (!in_array("Inter", $array_fonts)) {
                    array_push($array_fonts, "Inter");
                }
                $data['config']['text']['all']['supported-fonts'] = $array_fonts;
            }
            // favicon
            $this->request->get['key'] = ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_FAVICON;
            $favicon_config = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $data['config']['favicon'] = is_array($favicon_config) ? $favicon_config : json_decode($favicon_config, true);

            // social
            $this->request->get['key'] = ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_SOCIAL;
            $social_config = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $social_config = is_array($social_config) ? $social_config : json_decode($social_config, true);
            $data['config']['social'] = [];
            foreach ($social_config as $sc) {
                $data['config']['social'][$sc['name']] = $sc;
            }

            // ecommerce
            $this->request->get['key'] = ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_ECOMMERCE;
            $ecommerce_config = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $ecommerce_config = is_array($ecommerce_config) ? $ecommerce_config : json_decode($ecommerce_config, true);
            $data['config']['ecommerce'] = [];
            foreach ($ecommerce_config as $ec) {
                $data['config']['ecommerce'][$ec['name']] = $ec;
            }

            // override css
            $this->request->get['key'] = ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_OVERRIDE_CSS;
            $override_css = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $override_css = is_array($override_css) ? $override_css : json_decode($override_css, true);
            $data['config']['override_css'] = $override_css;

            // restore request "key"
            $this->request->get['key'] = $orig_key;
        }

        $data['token'] = $this->session->data['user_token'];

        /* links for header */
        $data['href_exit'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_homepage'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $data['href_category'] = $this->url->link('section_category/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_sections', true);
        $data['href_product_detail'] = $this->url->link('section_product_detail/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_detail_sections', true);
        $data['href_blog'] = $this->url->link('section_blog/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog_sections', true);
        $data['href_contact'] = $this->url->link('section_contact/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_sections', true);

        // undo-redo page
        $data['href_change_color'] = $this->url->link('theme/color', 'user_token=' . $this->session->data['user_token'] . '&key=' . ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_COLOR . '&action=change', true);
        $data['href_change_text'] = $this->url->link('theme/text', 'user_token=' . $this->session->data['user_token'] . '&key=' . ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_TEXT . '&action=change', true);
        $data['href_change_favicon'] = $this->url->link('theme/favicon', 'user_token=' . $this->session->data['user_token'] . '&key=' . ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_FAVICON . '&action=change', true);
        $data['href_change_social'] = $this->url->link('theme/social', 'user_token=' . $this->session->data['user_token'] . '&key=' . ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_SOCIAL . '&action=change', true);
        $data['href_change_ecommerce'] = $this->url->link('theme/ecommerce', 'user_token=' . $this->session->data['user_token'] . '&key=' . ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_ECOMMERCE . '&action=change', true);
        $data['href_change_override_css'] = $this->url->link('theme/section_theme/override_css', 'user_token=' . $this->session->data['user_token'] . '&key=' . ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_OVERRIDE_CSS . '&action=change', true);
        $data['href_undo'] = $this->url->link('theme/color', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_color&action=undo', true);
        $data['href_redo'] = $this->url->link('theme/color', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_color&action=redo', true);

        $data['preview'] = $this->load->controller('theme/preview');
        $data['is_on_mobile'] = $this->isMobile();

        /* fix displaying selected page on header */
        $this->load->language('theme/preview');
        $data['selected_page'] = $this->getSelectedPageDueToRoute($this->request->get['route']);

        /* base */
        $data['href_exit'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_homepage'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $data['href_category'] = $this->url->link('section_category/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_sections', true);
        $data['href_product_detail'] = $this->url->link('section_product_detail/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_detail_sections', true);
        $data['href_blog'] = $this->url->link('section_blog/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog_sections', true);
        $data['href_contact'] = $this->url->link('section_contact/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_sections', true);

        $data['back_section'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);

        $this->response->setOutput($this->load->view('theme/section_theme', $data));
    }

    public function override_css()
    {
        $this->load->language('theme/section_theme');

        $this->document->setTitle($this->language->get('heading_title'));


        /* config with undo|redo|change api */
        $action = isset($this->request->get['action']) ? $this->request->get['action'] : 'config';
        if (in_array($action, ['config', 'undo', 'redo', 'change'])) {
            $data['config'] = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $data['config'] = is_array($data['config']) ? $data['config'] : json_decode($data['config'], true);
        }

        $data['token'] = $this->session->data['user_token'];
        // undo-redo page
        $data['href_change'] = $this->url->link('theme/override_css', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_override_css&action=change', true);
        $data['href_undo'] = $this->url->link('theme/override_css', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_override_css&action=undo', true);
        $data['href_redo'] = $this->url->link('theme/override_css', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_override_css&action=redo', true);

        return;
    }
}