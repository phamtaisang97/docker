<?php

class ControllerThemeText extends Controller
{
    use Theme_Config_Util;

    public function index()
    {
        $data = array();
        $this->load->language('theme/text');

        $this->document->setTitle($this->language->get('heading_title'));

        /* breadcrumbs */
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/attribute', 'user_token=' . $this->session->data['user_token'], true)
        );

        /* error */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        /* config */
        $data['heading_title'] = $this->language->get('heading_title');
        $data['heading_font'] = $this->language->get('heading_font');
        $data['text_heading_font'] = $this->language->get('text_heading_font');
        $data['text_heading_type'] = $this->language->get('text_heading_type');
        $data['text_heading_size'] = $this->language->get('text_heading_size');
        $data['body_font'] = $this->language->get('body_font');
        $data['text_body_font'] = $this->language->get('text_body_font');
        $data['text_body_type'] = $this->language->get('text_body_type');
        $data['text_body_size'] = $this->language->get('text_body_size');
        $data['back_section'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'], true);

        /* setup link undo|redo|change */
        // clear session
        // if (!isset($this->request->get['action']) && isset($this->session->data['theme_builder_config'][$this->getCurrentTheme()])) {
        //     unset($this->session->data['theme_builder_config'][$this->getCurrentTheme()]);
        // }

        $action = isset($this->request->get['action']) ? $this->request->get['action'] : 'config';
        if (in_array($action, ['config', 'undo', 'redo', 'change'])) {
            $data['config'] = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $data['config'] = is_array($data['config']) ? $data['config'] : json_decode($data['config'], true);
        }

        // undo-redo page
        $data['href_change'] = $this->url->link('theme/text', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_text&action=change', true);
        $data['href_undo'] = $this->url->link('theme/text', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_text&action=undo', true);
        $data['href_redo'] = $this->url->link('theme/text', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_text&action=redo', true);
        $data['href_config'] = $this->url->link('theme/text', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_text&action=config', true);

        /* fix displaying selected page on header */
        $this->load->language('theme/preview');
        $data['selected_page'] = $this->getSelectedPageDueToRoute($this->request->get['route']);

        /* base */
        $data['href_exit'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_homepage'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $data['href_category'] = $this->url->link('section_category/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_sections', true);
        $data['href_product_detail'] = $this->url->link('section_product_detail/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_detail_sections', true);
        $data['href_blog'] = $this->url->link('section_blog/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog_sections', true);
        $data['href_contact'] = $this->url->link('section_contact/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_sections', true);
        $data['preview'] = $this->load->controller('theme/preview');
        $data['back_section'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);

        $this->response->setOutput($this->load->view('theme/text', $data));
    }

    private function getCurrentTheme()
    {
        $current_theme = $this->config->get('config_theme');

        return isset($current_theme) ? $current_theme : 'default';
    }
}