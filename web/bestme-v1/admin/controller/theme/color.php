<?php

class ControllerThemeColor extends Controller
{
    use Theme_Config_Util;

    private $error = array();

    public function index()
    {
        $this->load->language('theme/color');

        $this->document->setTitle($this->language->get('heading_title'));

        /* breadcrumbs */
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/attribute', 'user_token=' . $this->session->data['user_token'], true)
        );

        /* error */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        /* lang - auto */
        // $data['color_background_title'] = $this->language->get('color_background_title');
        // $data['color_text_big_header'] = $this->language->get('color_text_big_header');
        // $data['color_text_content'] = $this->language->get('color_text_content');
        // $data['color_horizon_line'] = $this->language->get('color_horizon_line');
        // $data['color_button'] = $this->language->get('color_button');
        // $data['color_button_text'] = $this->language->get('color_button_text');
        // $data['color_extra'] = $this->language->get('color_extra');
        // $data['color_sale_card'] = $this->language->get('color_sale_card');

        /* config with undo|redo|change api */
        $action = isset($this->request->get['action']) ? $this->request->get['action'] : 'config';
        if (in_array($action, ['config', 'undo', 'redo', 'change'])) {
            $data['config'] = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $data['config'] = is_array($data['config']) ? $data['config'] : json_decode($data['config'], true);
        }

        $data['token'] = $this->session->data['user_token'];
        // undo-redo page
        $data['href_change'] = $this->url->link('theme/color', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_color&action=change', true);
        $data['href_undo'] = $this->url->link('theme/color', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_color&action=undo', true);
        $data['href_redo'] = $this->url->link('theme/color', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_color&action=redo', true);

        $data['preview'] = $this->load->controller('theme/preview');

        /* fix displaying selected page on header */
        $this->load->language('theme/preview');
        $data['selected_page'] = $this->getSelectedPageDueToRoute($this->request->get['route']);

        /* base */
        $data['href_exit'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_homepage'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $data['href_category'] = $this->url->link('section_category/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_sections', true);
        $data['href_product_detail'] = $this->url->link('section_product_detail/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_detail_sections', true);
        $data['href_blog'] = $this->url->link('section_blog/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog_sections', true);
        $data['href_contact'] = $this->url->link('section_contact/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_sections', true);
        $data['back_section'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);

        $this->response->setOutput($this->load->view('theme/color', $data));
    }
}