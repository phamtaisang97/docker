<?php

class ControllerCommonDashboard extends Controller
{
    use Device_Util;
    use Setting_Util_Trait;
    use Sync_Bestme_Data_To_Welcome_Util;

    const ON_BOARDING_LENGTH_STEP = 3;
    const ON_BOARDING_PAID_TYPE = 'optimal';
    const ON_BOARDING_VOUCHER_LENGTH = 8;

    public function index()
    {
        $this->load->language('common/dashboard');

        $this->load->model('sale/order');

        $this->load->model('catalog/product');

        $this->load->model('catalog/warehouse');

        $this->load->model('user/user');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['user_token'] = $this->session->data['user_token'];

        $data['is_on_mobile'] = $this->isMobile() ? 1 : 0;

        $data['is_admin'] = $this->user->isAdmin();
        $data['can_access_all'] = $this->user->canAccessAll();

        $user = $this->model_user_user->getUser($this->user->getId());
        $data['user'] = [
            'username' => $user['username'],
            'firstname' => $user['firstname'],
            'lastname' => $user['lastname']
        ];

        $data['total_product_out_of_stock'] = $this->getTotalProductOutOfStockForStoreId();
        $data['product_out_of_stock'] = $this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'] . '&amounttype[]=2_1', true);
        $data['order_processing_total'] = $this->model_sale_order->getTotalOrders(array('order_status' => array('7')));
        $data['order_processing'] = $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . '&filter_status%5B%5D=7', true);
        $data['order_shipping_total'] = $this->model_sale_order->getTotalOrders(array('order_status' => array('8')));
        $data['order_shipping'] = $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . '&filter_status%5B%5D=8', true);
        //nkt
        $data['left_currency'] = isset($this->session->data['currency']) ? $this->currency->getSymbolLeft($this->session->data['currency']) : '';
        $data['right_currency'] = isset($this->session->data['currency']) ? $this->currency->getSymbolRight($this->session->data['currency']) : '';
        $data['action_revenue'] = $this->url->link('report/overview/getRevenue', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_customer'] = $this->url->link('common/dashboard/getCustomer', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_order'] = $this->url->link('common/dashboard/getOrder', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_top_revenue_product'] = $this->url->link('common/dashboard/getTopRevenue', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_web_traffic'] = $this->url->link('common/dashboard/getWebTraffic', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_conversion_rate'] = $this->url->link('common/dashboard/getConversionRate', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_get_total_product_out_of_stock'] = $this->url->link('common/dashboard/getTotalProductOutOfStock', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_get_order_processing_total'] = $this->url->link('common/dashboard/getOrderProcessingTotal', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_get_order_shipping_total'] = $this->url->link('common/dashboard/getOrderShippingTotal', 'user_token=' . $this->session->data['user_token'], true);

        //check on boarding
        $config_on_boarding_complete = $this->config->get('config_onboarding_complete');
        $config_on_boarding_step_active = $this->config->get('config_onboarding_step_active');
        $data['on_boarding_step_active'] = empty($config_on_boarding_step_active) ? [] : explode(',', $config_on_boarding_step_active);
        $data['on_boarding_first_step'] = $this->getStepFirst($data['on_boarding_step_active'], 1);
        $data['on_boarding_length_step'] = self::ON_BOARDING_LENGTH_STEP;

        $data['onboarding_gitf_link'] = ONBOARDING_GITF_LINK;

        $data['create_product_url'] = $this->url->link('catalog/product/add', 'user_token=' . $this->session->data['user_token'], true);
        $data['interface_url'] = $this->url->link('custom/theme', 'user_token=' . $this->session->data['user_token'], true);
        $data['setting_domain_url'] = $this->url->link('online_store/domain', 'user_token=' . $this->session->data['user_token'], true);
        $data['packet_paid_url'] = $this->url->link('common/dashboard/updatePacketPaid', 'user_token=' . $this->session->data['user_token'], true);

        // TODO remove go live...
        //$data['reset_on_boarding_url'] = $this->url->link('common/dashboard/resetOnBoarding', 'user_token=' . $this->session->data['user_token'], true);

        $data['bestme_hotline'] = CONFIG_HOT_LINE;

        $data['voucher_code'] = $this->getVoucherCode();

        $data['config_onboarding_used_voucher'] = ($config_on_boarding_complete == 1) ? $this->config->get('config_onboarding_used_voucher') : 1;

        $data['config_on_boarding_complete'] = $config_on_boarding_complete;

        $data['is_paid_shop'] = $this->isPaidShop();

        //get user
        $this->load->model('user/user');
        $data['user'] = $this->model_user_user->getUser($this->user->getId());

        // get store, staff list
        $this->load->model('setting/store');
        $data['stores'] = $this->model_setting_store->getStores();

        $this->load->model('user/user');
        $data['staffs'] = $this->model_user_user->getStaffs();

        $data['start_date_paid'] = date('d/m/Y');
        $data['end_date_paid'] = date("d/m/Y", strtotime(" +2 months"));

        $data_1['menu_active'] = 'menu-dashboard';
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left', $data_1);
        $data['footer'] = $this->load->controller('common/footer');

        if (is_null($config_on_boarding_complete) || $config_on_boarding_complete == 1) {
            $this->response->setOutput($this->load->view('common/dashboard', $data));
        } else {
            $this->response->setOutput($this->load->view('common/onboarding', $data));
        }
    }

    public function getCustomer()
    {
        $this->load->model('report/order');
        if (isset($this->request->get['start']) && !empty($this->request->get['start'])) {
            $filter['start'] = $this->request->get['start'];
            $filter['end'] = $this->request->get['end'];
        }

        if (isset($this->request->get['user_id'])) {
            $user_id = preg_replace('/[^0-9]/', '', $this->request->get['user_id']);
            if ($user_id !== '') {
                $filter['user_id'] = $user_id;
            }
        }
        if (isset($this->request->get['store_id'])) {
            $store_id = preg_replace('/[^0-9]/', '', $this->request->get['store_id']);
            if ($store_id !== '') {
                $filter['store_id'] = $store_id;
            }
        }

        /* Handle count customer */
        $json_data = $this->model_report_order->getTotalCustomerOnDashBoard($filter);
        $this->response->setOutput(json_encode($json_data));
    }

    public function getOrder()
    {
        $this->load->model('report/order');
        if (isset($this->request->get['start']) && !empty($this->request->get['start'])) {
            $filter['start'] = $this->request->get['start'];
            $filter['end'] = $this->request->get['end'];
        }

        if (isset($this->request->get['user_id'])) {
            $user_id = preg_replace('/[^0-9]/', '', $this->request->get['user_id']);
            if ($user_id !== '') {
                $filter['user_id'] = $user_id;
            }
        }
        if (isset($this->request->get['store_id'])) {
            $store_id = preg_replace('/[^0-9]/', '', $this->request->get['store_id']);
            if ($store_id !== '') {
                $filter['store_id'] = $store_id;
            }
        }

        /* Get count order*/
        $json_data = $this->model_report_order->getTotalOrderOnDashBoard($filter);
        $this->response->setOutput(json_encode($json_data));
    }

    /* Get top revenue product */
    public function getTopRevenue()
    {
        $this->load->model('report/order_product');
        $this->load->model('tool/image');
        if (isset($this->request->get['start']) && !empty($this->request->get['start'])) {
            $filter['start'] = $this->request->get['start'];
            $filter['end'] = $this->request->get['end'];
        }

        if (isset($this->request->get['user_id'])) {
            $user_id = preg_replace('/[^0-9]/', '', $this->request->get['user_id']);
            if ($user_id !== '') {
                $filter['user_id'] = $user_id;
            }
        }
        if (isset($this->request->get['store_id'])) {
            $store_id = preg_replace('/[^0-9]/', '', $this->request->get['store_id']);
            if ($store_id !== '') {
                $filter['store_id'] = $store_id;
            }
        }

        /* Get count order*/
        $json_data = $this->model_report_order_product->getTopRevenueProduct($filter);
        foreach ($json_data as $key => $value) {
            if ($value['image']) {
                $value['image'] = $value['image'];
            } else {
                $value['image'] = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
            }
            $value['edit'] = $this->url->link('catalog/product/edit', 'user_token=' . $this->session->data['user_token'] . '&product_id=' . $value['product_id'], true);
            $json_data[$key] = $value;
        }
        $this->response->setOutput(json_encode($json_data));
    }

    public function getWebTraffic()
    {
        $this->load->model('report/web_traffic');
        if (isset($this->request->get['start']) && !empty($this->request->get['start'])) {
            $filter['start'] = $this->request->get['start'];
            $filter['end'] = $this->request->get['end'];
        }

        /* Get count order*/
        $json_data = $this->model_report_web_traffic->getTotalWebTraffic($filter);
        $this->response->setOutput(json_encode($json_data));
    }

    public function getConversionRate()
    {
        $this->load->model('report/web_traffic');
        if (isset($this->request->get['start']) && !empty($this->request->get['start'])) {
            $filter['start'] = $this->request->get['start'];
            $filter['end'] = $this->request->get['end'];
        }

        /* Get count order*/
        $json_data = $this->model_report_web_traffic->getConversionRate($filter);
        $this->response->setOutput(json_encode($json_data));
    }

    public function updatePacketPaid()
    {
        $config_on_boarding = $this->config->get('config_onboarding_complete');
        $config_on_boarding_step_active = $this->config->get('config_onboarding_step_active');
        $on_boarding_step_active = empty($config_on_boarding_step_active) ? [] : explode(',', $config_on_boarding_step_active);
        if (count($on_boarding_step_active) == self::ON_BOARDING_LENGTH_STEP && $config_on_boarding == 0) {
            $this->load->model('setting/setting');
            // update voucher status
            $this->model_setting_setting->editSettingValue('config', 'config_onboarding_used_voucher', 0);
            // update config on boarding
            $this->model_setting_setting->editSettingValue('config', 'config_onboarding_complete', 1);

            /* publish sync data */
            $shop_name = $this->model_setting_setting->getShopName();
            $onboarding_data = [
                'shop_domain' => $shop_name,
                'last_step' => empty($on_boarding_step_active) ? 0 : end($on_boarding_step_active),
                'steps' => 99
            ];
            $this->publishSyncDataOnboarding($onboarding_data);
        }
        $this->response->redirect($this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true));
    }

    // TODO remove go live
    public function resetOnBoarding()
    {
        $this->load->model('setting/setting');

        // update step active
        $this->model_setting_setting->editSettingValue('config', 'config_onboarding_step_active', '');

        // update config on boarding
        $this->model_setting_setting->editSettingValue('config', 'config_onboarding_complete', 0);

        // update voucher code
        $this->model_setting_setting->editSettingValue('config', 'config_onboarding_voucher', '');

        $this->response->redirect($this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function getTotalProductOutOfStock()
    {
        $store_id = '';
        if (isset($this->request->get['store_id'])) {
            $store_id = preg_replace('/[^0-9]/', '', $this->request->get['store_id']);
        }

        $json_data = $this->getTotalProductOutOfStockForStoreId($store_id);
        $this->response->setOutput(json_encode($json_data));
    }

    public function getOrderProcessingTotal(){
        $this->load->model('sale/order');
        $filter_data['order_status'] = ['7'];

        if (isset($this->request->get['user_id'])) {
            $user_id = preg_replace('/[^0-9]/', '', $this->request->get['user_id']);
            if ($user_id !== '') {
                $filter_data['user_id'] = $user_id;
            }
        }
        if (isset($this->request->get['store_id'])) {
            $store_id = preg_replace('/[^0-9]/', '', $this->request->get['store_id']);
            if ($store_id !== '') {
                $filter_data['store_id'] = $store_id;
            }
        }

        $json_data =  $this->model_sale_order->getTotalOrders($filter_data);
        $this->response->setOutput(json_encode($json_data));
    }

    public function getOrderShippingTotal(){
        $this->load->model('sale/order');
        $filter_data['order_status'] = ['8'];

        if (isset($this->request->get['user_id'])) {
            $user_id = preg_replace('/[^0-9]/', '', $this->request->get['user_id']);
            if ($user_id !== '') {
                $filter_data['user_id'] = $user_id;
            }
        }
        if (isset($this->request->get['store_id'])) {
            $store_id = preg_replace('/[^0-9]/', '', $this->request->get['store_id']);
            if ($store_id !== '') {
                $filter_data['store_id'] = $store_id;
            }
        }

        $json_data = $this->model_sale_order->getTotalOrders($filter_data);
        $this->response->setOutput(json_encode($json_data));
    }

    private function getStepFirst($list, $first)
    {
        if (!in_array($first, $list)) {
            return $first;
        }
        $first += 1;
        return $this->getStepFirst($list, $first);
    }

    /**
     * Generate code with checksum number
     * Format: |W|W|C|X|X|X|X|X|
     * - W|W: number of week of the year
     * - C: Checksum number, use to check valid of XXXXX
     * - X|X|X|X|X: random number
     *
     * @return string 8 digit
     */
    private function getVoucherCode()
    {
        // check if voucher already in db
        $this->load->model('setting/setting');
        $voucher = $this->model_setting_setting->getSettingValue('config_onboarding_voucher');
        if (empty($voucher)) {
            // create voucher if not exist
            $now = time();
            $week_of_year = date('W', $now);
            $rand_len = self::ON_BOARDING_VOUCHER_LENGTH - 3;
            $max = pow(10, $rand_len) - 1;
            $rand_num = mt_rand(0, $max);
            $num_tmp = sprintf('%0' . $rand_len . 'd', $rand_num);
            $checksum = $this->cal_checksum($num_tmp);
            $voucher = sprintf('%d%d%s', $week_of_year, $checksum, $num_tmp);

            // save db
            $this->model_setting_setting->editSettingValue('config', 'config_onboarding_voucher', $voucher);
        }

        return $voucher;
    }

    /**
     * Calculate checksum
     *
     * @param string $num
     * @return int
     */
    private function cal_checksum($num)
    {
        $odd_sum = 0;
        $even_sum = 0;
        for ($i = 0; $i < strlen($num); $i++) {
            if (($i + 1) % 2 == 0) {
                //even
                $even_sum += $num[$i];
            } else {
                //odd:
                $odd_sum += $num[$i];
            }
        }

        $sum = $odd_sum * 3 + $even_sum;

        //divide modulo:
        $checksum = 10 - $sum % 10;

        return $checksum == 10 ? 0 : $checksum;
    }

    /**
     * @param int|string $store_id empty for all stores
     * @return int
     */
    private function getTotalProductOutOfStockForStoreId($store_id = '')
    {
        $this->load->model('catalog/warehouse');
        return $this->model_catalog_warehouse->countProductOutOfStock($store_id);
    }
}