<?php

class ControllerCommonCustomColumnLeft extends Controller
{
    use Theme_Config_Util;
    use Setting_Util_Trait;

    const CONFIG_BESTME_POS_API_KEY = 'config_pos_api_key';
    const CONFIG_BESTME_NOVAONX_SOCIAL_API_KEY = 'config_novaonx_social_api_key';

    const DEMO_DATA_ACTION = ['load', 'delete'];

    public function index()
    {
        $this->load->model('setting/setting');
        if (isset($this->request->get['user_token']) && isset($this->session->data['user_token']) && ($this->request->get['user_token'] == $this->session->data['user_token'])) {
            $this->load->language('common/column_left');
            // Create a 3 level menu array
            // Level 2 can not have children
            $data['user_token'] = $this->session->data['user_token'];
            // overview
            $data['menus'][] = array(
                'id' => 'menu-dashboard',
                'icon' => 'view/image/custom/icons/house.svg',
                'name' => $this->language->get('text_dashboard'),
                'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true),
                'route' => ['common/dashboard'],
                'children' => array(),
                'show_on_mobile' => 1
            );

            $orders = array();
            if ($this->user->hasPermission('access', 'sale/order')) {
                $orders[] = array(
                    'name' => $this->language->get('text_order_list'),
                    'href' => $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['sale/order'],
                    'children' => array(),
                    'show_on_mobile' => 1
                );
            }

            if ($this->user->hasPermission('access', 'sale/return_receipt')) {
                $orders[] = array(
                    'name' => $this->language->get('text_return_receipt'),
                    'href' => $this->url->link('sale/return_receipt', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['sale/return_receipt'],
                    'children' => array(),
                    'show_on_mobile' => 1
                );
            }

            // order
            if ($this->user->hasPermission('access', 'sale/order')) {
                $data['menus'][] = array(
                    'id' => 'menu-catalog',
                    'icon' => 'view/image/custom/icons/list.svg',
                    'name' => $this->language->get('text_order'),
                    'href' => '#',
                    'route' => ['sale/order', 'sale/order/add', 'sale/return_receipt'],
                    'children' => $orders,
                    'show_on_mobile' => 1
                );
            }

            $products = array();
            if ($this->user->hasPermission('access', 'catalog/product')) {
                $products[] = array(
                    'name' => $this->language->get('text_product_list'),
                    'href' => $this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['catalog/product'],
                    'children' => array(),
                    'show_on_mobile' => 1
                );

                /*$products[] = array(
                    'name' => $this->language->get('text_product_add'),
                    'href' => $this->url->link('catalog/product/add', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['catalog/product/add'],
                    'children' => array(),
                    'show_on_mobile' => 1
                );*/

                /*$products[] = array(
                    'name' => $this->language->get('text_manage_warehouse'),
                    'href' => $this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['catalog/warehouse'],
                    'children' => array(),
                    'show_on_mobile' => 1
                );*/

                /*$products[] = array(
                    'name' => $this->language->get('text_list_store'),
                    'href' => $this->url->link('catalog/store', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['catalog/store'],
                    'children' => array(),
                    'show_on_mobile' => 1
                );*/

            if ($this->user->hasPermission('access', 'catalog/collection')) {
                $products[] = array(
                    'name' => $this->language->get('text_manage_collection'),
                    'href' => $this->url->link('catalog/collection', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['catalog/collection'],
                    'children' => array(),
                    'show_on_mobile' => 1
                );
            }

            if ($this->user->hasPermission('access', 'catalog/manufacturer')) {
                $products[] = array(
                    'name' => $this->language->get('text_manage_manufacturer'),
                    'href' => $this->url->link('catalog/manufacturer', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['catalog/manufacturer'],
                    'children' => array(),
                    'show_on_mobile' => 0
                );
            }

                /*$products[] = array(
                    'name' => $this->language->get('text_manage_store_receipt'),
                    'href' => $this->url->link('catalog/store_receipt', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['catalog/store_receipt'],
                    'children' => array(),
                    'show_on_mobile' => 0
                );*/
            }

            // product
            if ($this->user->hasPermission('access', 'catalog/product')) {
                $data['menus'][] = array(
                    'id' => 'menu-product',
                    'icon' => 'view/image/custom/icons/shape.svg',
                    'name' => $this->language->get('text_product'),
                    'href' => '#',
                    'route' => ['catalog/product', 'catalog/product/add'],
                    'children' => $products,
                    'show_on_mobile' => 1
                );
            }

            // ingredient
            if ($this->user->hasPermission('access', 'ingredient/ingredient')) {
                $data['menus'][] = array(
                    'id' => 'menu-ingredient',
                    'icon' => 'view/image/custom/icons/list.svg',
                    'name' => $this->language->get('text_menu_ingredient'),
                    'href' => $this->url->link('ingredient/ingredient', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['ingredient/ingredient'],
                    'children' => array(),
                    'show_on_mobile' => 1
                );
            }

            //warehouse
            $warehouse = array();
            if ($this->user->hasPermission('access', 'catalog/warehouse')) {
                $warehouse[] = array(
                    'name' => $this->language->get('text_manage_warehouse'),
                    'href' => $this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['catalog/warehouse'],
                    'children' => array(),
                    'show_on_mobile' => 1
                );
            }
            if ($this->user->hasPermission('access', 'catalog/store_receipt')) {
                $warehouse[] = array(
                    'name' => $this->language->get('text_import_goods'),
                    'href' => $this->url->link('catalog/store_receipt', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['catalog/store_receipt'],
                    'children' => array(),
                    'show_on_mobile' => 1
                );
            }
            if ($this->user->hasPermission('access', 'catalog/store_transfer_receipt')) {
                $warehouse[] = array(
                    'name' => $this->language->get('text_shipping'),
                    'href' => $this->url->link('catalog/store_transfer_receipt', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['catalog/store_transfer_receipt'],
                    'children' => array(),
                    'show_on_mobile' => 1
                );
            }
            if ($this->user->hasPermission('access', 'catalog/store_take_receipt')) {
                $warehouse[] = array(
                    'name' => $this->language->get('text_check_goods'),
                    'href' => $this->url->link('catalog/store_take_receipt', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['catalog/store_take_receipt'],
                    'children' => array(),
                    'show_on_mobile' => 1
                );
            }
            if ($this->user->hasPermission('access', 'catalog/cost_adjustment_receipt')) {
                $warehouse[] = array(
                    'name' => $this->language->get('text_cost_adjustment'),
                    'href' => $this->url->link('catalog/cost_adjustment_receipt', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['catalog/cost_adjustment_receipt'],
                    'children' => array(),
                    'show_on_mobile' => 1
                );
            }
            if ($this->user->hasPermission('access', 'catalog/store')) {
                $warehouse[] = array(
                    'name' => $this->language->get('text_list_store'),
                    'href' => $this->url->link('catalog/store', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['catalog/store'],
                    'children' => array(),
                    'show_on_mobile' => 1
                );
            }

            // warehouse
            if ($this->user->hasPermission('access', 'catalog/cost_adjustment_receipt') ||
                $this->user->hasPermission('access', 'catalog/store_take_receipt') ||
                $this->user->hasPermission('access', 'catalog/store_transfer_receipt') ||
                $this->user->hasPermission('access', 'catalog/store_receipt') ||
                $this->user->hasPermission('access', 'catalog/warehouse') ||
                $this->user->hasPermission('access', 'catalog/warehouse')
            ) {
                $data['menus'][] = array(
                    'id' => 'menu-warehouse',
                    'icon' => 'view/image/custom/icons/store.svg',
                    'name' => $this->language->get('text_warehouse'),
                    'href' => '#',
                    'route' => ['catalog/store', 'catalog/cost_adjustment_receipt', 'catalog/store_take_receipt', 'catalog/store_transfer_receipt', 'catalog/store_receipt', 'catalog/warehouse'],
                    'children' => $warehouse,
                    'show_on_mobile' => 1
                );
            }

            //customer
            $customer = array();
            if ($this->user->hasPermission('access', 'customer/customer')) {
                $customer[] = array(
                    'name' => $this->language->get('text_list_customers'),
                    'href' => $this->url->link('customer/customer', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['customer/customer'],
                    'children' => array(),
                    'show_on_mobile' => 1
                );
            }
            if($this->user->hasPermission('access', 'customer/customer_group')) {
                $customer[] = array(
                    'name' => $this->language->get('text_customer_group'),
                    'href' => $this->url->link('customer/customer_group', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['customer/customer_group'],
                    'children' => array(),
                    'show_on_mobile' => 1
                );
            }

            if ($this->user->hasPermission('access', 'customer/customer') || $this->user->hasPermission('access', 'customer/customer_group')) {
                $data['menus'][] = array(
                    'id' => 'menu-customer',
                    'icon' => 'view/image/custom/icons/person.svg',
                    'name' => $this->language->get('text_customer'),
                    'href' => '#',
                    'route' => ['customer/customer', 'customer/customer_group'],
                    'children' => $customer,
                    'show_on_mobile' => 1
                );
            }

            // cash flow
            if ($this->user->hasPermission('access', 'cash_flow/cash_flow')) {
                $cash_flow = [
                    [
                        'name' => $this->language->get('text_overview'),
                        'href' => $this->url->link('cash_flow/cash_flow', 'user_token=' . $this->session->data['user_token'], true),
                        'route' => ['cash_flow/cash_flow'],
                        'children' => array(),
                        'show_on_mobile' => 0
                    ],
                    [
                        'name' => $this->language->get('text_receipt_voucher'),
                        'href' => $this->url->link('cash_flow/receipt_voucher', 'user_token=' . $this->session->data['user_token'], true),
                        'route' => ['cash_flow/receipt_voucher'],
                        'children' => array(),
                        'show_on_mobile' => 0
                    ],
                    [
                        'name' => $this->language->get('text_payment_voucher'),
                        'href' => $this->url->link('cash_flow/payment_voucher', 'user_token=' . $this->session->data['user_token'], true),
                        'route' => ['cash_flow/payment_voucher'],
                        'children' => array(),
                        'show_on_mobile' => 0
                    ]
                ];

                $data['menus'][] = array(
                    'id' => 'menu-cash_flow',
                    'icon' => 'view/image/custom/icons/cash.svg',
                    'name' => $this->language->get('text_cash_flow'),
                    'href' => '#',
                    'route' => ['cash_flow/cash_flow'],
                    'children' => $cash_flow,
                    'show_on_mobile' => 0
                );
            }

            // report
            if ($this->user->hasPermission('access', 'report/overview')) {
                $report = [
                    [
                        'name' => $this->language->get('text_overview'),
                        'href' => $this->url->link('report/overview', 'user_token=' . $this->session->data['user_token'], true),
                        'route' => ['report/overview'],
                        'children' => array(),
                        'show_on_mobile' => 1
                    ],
                    [
                        'id' => "menu-report",
                        'name' => $this->language->get('text_sales_report'),
                        'href' => $this->url->link('report/product', 'user_token=' . $this->session->data['user_token'], true),
                        'route' => ['report/product'],
                        'children' => array(),
                        'show_on_mobile' => 1
                    ],
                    [
                        'name' => $this->language->get('text_financial_report'),
                        'href' => $this->url->link('report/financial', 'user_token=' . $this->session->data['user_token'], true),
                        'route' => ['report/financial'],
                        'children' => array(),
                        'show_on_mobile' => 1
                    ]
                ];

                $data['menus'][] = array(
                    'id' => 'menu-report',
                    'icon' => 'view/image/custom/icons/report.svg',
                    'name' => $this->language->get('text_reports'),
                    'href' => $this->url->link('report/overview', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['report/overview', 'report/product', 'report/financial'],
                    'children' => $report,
                    'show_on_mobile' => 1
                );
            }

            //// google_shopping
            if (!defined('SOURCE_PRODUCTION_FOR_ADG') || SOURCE_PRODUCTION_FOR_ADG == false) {
                if ($this->user->hasPermission('access', 'online_store/google_shopping')) {
                    $data['menus'][] = array(
                        'id' => 'menu-google-shopping',
                        'icon' => 'view/image/custom/icons/gg-shopping.svg',
                        'name' => $this->language->get('text_google_shopping'),
                        'href' => $this->url->link('online_store/google_shopping', 'user_token=' . $this->session->data['user_token'], true),
                        'route' => ['online_store/google_shopping'],
                        'children' => array(),
                        'show_on_mobile' => 0
                    );
                }
            }

            $discounts = array();
            if ($this->user->hasPermission('access', 'discount/discount')) {
                $discounts[] = array(
                    'name' => $this->language->get('text_flash_sale_list'),
                    'href' => $this->url->link('discount/flash_sale', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['discount/flash_sale'],
                    'children' => array(),
                    'show_on_mobile' => 0
                );

                $discounts[] = array(
                    'name' => $this->language->get('text_add_on_deal_list'),
                    'href' => $this->url->link('discount/add_on_deal', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['discount/flash_sale'],
                    'children' => array(),
                    'show_on_mobile' => 0
                );

                $discounts[] = array(
                    'name' => $this->language->get('text_discount_list'),
                    'href' => $this->url->link('discount/discount', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['discount/discount'],
                    'children' => array(),
                    'show_on_mobile' => 0
                );

                $discounts[] = array(
                    'name' => $this->language->get('text_discount_config'),
                    'href' => $this->url->link('discount/setting', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['discount/setting'],
                    'children' => array(),
                    'show_on_mobile' => 0
                );
            }

            //// discount
            if ($this->user->hasPermission('access', 'discount/discount')) {
                $data['menus'][] = array(
                    'id' => 'menu-discount',
                    'icon' => 'view/image/custom/icons/discount-new.svg',
                    'name' => $this->language->get('text_discount'),
                    'href' => '#',
                    'route' => ['discount/discount'],
                    'children' => $discounts,
                    'show_on_mobile' => 0
                );
            }

            // promotion TEMP DISABLED
            if (false && $this->user->hasPermission('access', '#')) {
                $data['menus'][] = array(
                    'id' => 'menu-promotion',
                    'icon' => 'view/image/custom/icons/discount.svg',
                    'name' => $this->language->get('text_promotion'),
                    'href' => '#',
                    'route' => '',
                    'children' => array(),
                    'show_on_mobile' => 0
                );
            }
            // my_app
            if ($this->user->hasPermission('access', 'app_store/my_app')) {
                $data['menus'][] = array(
                    'icon' => 'view/image/custom/icons/apps.svg',
                    'name' => $this->language->get('text_app_store'),
                    'href' => $this->url->link('app_store/my_app', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['app_store/my_app'],
                    'children' => array(),
                    'show_on_mobile' => 0
                );
            }
//            // application TEMP DISABLED
            if (false && $this->user->hasPermission('access', '#')) {
                $data['menus'][] = array(
                    'id' => 'menu-application',
                    'icon' => 'view/image/custom/icons/apps.svg',
                    'name' => $this->language->get('text_application'),
                    'href' => '#',
                    'route' => '',
                    'children' => array(),
                    'show_on_mobile' => 0
                );
            }

            $online_store = array();
            //// theme
            if ($this->user->hasPermission('access', 'custom/theme')) {
                $online_store[] = array(
                    'name' => $this->language->get('text_interface'),
                    'href' => $this->url->link('custom/theme', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['custom/theme'],
                    'children' => array(),
                    'show_on_mobile' => 1
                );
            }

            //// blog
            if ($this->user->hasPermission('access', 'blog/blog')) {
                $online_store[] = array(
                    'name' => $this->language->get('text_blog'),
                    'href' => $this->url->link('blog/blog', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['blog/blog'],
                    'children' => array(),
                    'show_on_mobile' => 1
                );
            }

            //// page
            if ($this->user->hasPermission('access', 'online_store/contents')) {
                $online_store[] = array(
                    'name' => $this->language->get('text_page'),
                    'href' => $this->url->link('online_store/contents', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['online_store/contents'],
                    'children' => array()
                );
            }

            //// group menu
            if ($this->user->hasPermission('access', 'custom/menu')) {
                $online_store[] = array(
                    // 'name' => $this->language->get('text_menu'),
                    'name' => $this->language->get('text_menu'),
                    'href' => $this->url->link('custom/menu', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['custom/menu'],
                    'children' => array()
                );
            }

            //// domain
            if ($this->user->hasPermission('access', 'online_store/domain')) {
                $online_store[] = array(
                    'name' => $this->language->get('text_domain'),
                    'href' => $this->url->link('online_store/domain', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['online_store/domain'],
                    'children' => array(),
                    'show_on_mobile' => 1
                );
            }

            //// config
            if ($this->user->hasPermission('access', 'custom/preference')) {
                $online_store[] = array(
                    'name' => $this->language->get('text_config'),
                    'href' => $this->url->link('custom/preference', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['custom/preference'],
                    'children' => array()
                );
            }

            //// image
            if ($this->user->hasPermission('access', 'common/filemanager')) {
                $online_store[] = array(
                    'name' => $this->language->get('text_image'),
                    'href' => $this->url->link('common/filemanager/imageList', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['common/filemanager/imageList'],
                    'children' => array()
                );
            }

            // Rated
            //// image
            if ($this->user->hasPermission('access', 'common/filemanager')) {
                if ($this->isSupportThemeFeature(self::$RATE_FORM)) {
                    $online_store[] = array(
                        'name' => $this->language->get('text_rate_website'),
                        'href' => $this->url->link('rate/rate', 'user_token=' . $this->session->data['user_token'], true),
                        'route' => ['rate/rate'],
                        'children' => array()
                    );
                }
            }

            // move to menu
            //// google_shopping
            /*if ($this->user->hasPermission('access', 'online_store/google_shopping')) {
                $online_store[] = array(
                    'name' => $this->language->get('text_google_shopping'),
                    'href' => $this->url->link('online_store/google_shopping', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['online_store/google_shopping'],
                    'children' => array()
                );
            }*/

            //// final: add store

            if ($this->isEnableModule(self::$MODULE_WEBSITE) == 1) {
                if ($online_store) {
                    $data['menus2'][] = array(
                        'id' => 'menu-online_store',
                        'icon' => 'view/image/custom/icons/shop.svg',
                        'name' => $this->language->get('text_online_store'),
                        'href' => '',
                        'route' => ['custom/theme', 'blog/blog', 'online_store/contents', 'custom/menu', 'custom/preference'],
                        'children' => $online_store,
                        'show_on_mobile' => 1
                    );
                }
            }

            // pos
            if ($this->user->hasPermission('access', 'sale_channel/pos_novaon')) {
                if ($this->model_setting_setting->getSettingValue('config_pos_status') == 1 &&
                    $this->isEnableModule(self::$MODULE_POS) == 1) {
                    $data['href_bestme_pos'] = BESTME_SERVER_POS;

                    $store_address = $this->config->get('config_address');
                    $store_telephone = $this->config->get('config_telephone');
                    $store_hotline = $this->config->get('config_hotline');

                    $shop_name = $this->model_setting_setting->getShopName();
                    $shop_name_display = !empty($this->config->get('config_name_display')) ? $this->config->get('config_name_display') : $this->model_setting_setting->getShopName();
                    $url_pos = sprintf(BESTME_POS_URL_TEMPLATE, $shop_name); // template e.g https://%s.bestme.asia/pos

                    $user_token = $this->session->data['user_token'];
                    $token = md5(implode([
                        $user_token,
                        $shop_name,
                        BESTME_POS_SECURE_KEY
                    ]));
                    $params = [
                        'user_token' => $user_token,
                        'shop_name' => $shop_name,
                        'shop_telephone' => $store_telephone,
                        'shop_address' => $store_address,
                        'shop_hotline' => $store_hotline,
                        'shop_name_display' => $shop_name_display,
                        'token' => $token
                    ];
                    $paramsStr = http_build_query($params);
                    $url_pos .= ((strpos($url_pos, '?') > -1) ? '&' : '?') . $paramsStr;

                    $data['menus2'][] = array(
                        'id' => 'menu-pos',
                        'icon' => 'view/image/theme/logo_pos_novaon.png',
                        'name' => $this->language->get('text_pos_novaon'),
                        'href' => $url_pos,
                        'route' => ['sale_channel/pos_novaon'],
                        'children' => array()
                    );
                }
            }

            // novaonx social
            if ($this->model_setting_setting->getSettingValue('config_novaonx_social_status') == 1 &&
                $this->isEnableModule(self::$MODULE_SOCIAL) == 1 &&
                $this->user->hasPermission('access', 'sale_channel/novaonx_social')
            ) {
                $data['menus2'][] = array(
                    'id' => 'menu-novaonx-social',
                    'icon' => 'view/image/theme/logo_novaonx_social.png',
                    'name' => $this->language->get('text_novaonx_social'),
                    'href' => $this->url->link('sale_channel/novaonx_social', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['sale_channel/novaonx_social'],
                    'children' => array()
                );
            };

            // shopee
            $shopee = [];
            if ($this->user->hasPermission('access', 'sale_channel/shopee')) {
                $shopee[] = array(
                    'name' => $this->language->get('text_shopee_products'),
                    'href' => $this->url->link('sale_channel/shopee', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['sale_channel/shopee'],
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'sale_channel/shopee')) {
                $shopee[] = array(
                    'name' => $this->language->get('text_upload_form'),
                    'href' => $this->url->link('sale_channel/shopee_upload', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['sale_channel/shopee_upload'],
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'sale_channel/shopee')) {
                $shopee[] = array(
                    'name' => $this->language->get('text_shopee_orders'),
                    'href' => $this->url->link('sale_channel/shopee/orders', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['sale_channel/shopee/orders'],
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'sale_channel/shopee')) {
                $shopee[] = array(
                    'name' => $this->language->get('text_config'),
                    'href' => $this->url->link('sale_channel/shopee/config', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['sale_channel/shopee/config'],
                    'children' => array()
                );
            }

            if ($this->isEnableModule(self::$MODULE_ECOM_PLATFORM) == 1) {
                if ($shopee) {
                    $data['shopee'][] = array(
                        'name' => $this->language->get('text_shopee'),
                        'icon' => 'view/image/custom/icons/shopee.svg',
                        'href' => $this->url->link('sale_channel/shopee/config', 'user_token=' . $this->session->data['user_token'], true),
                        'route' => ['sale_channel/shopee', 'sale_channel/shopee/orders', 'sale_channel/shopee/config', 'sale_channel/shopee_upload'],
                        'children' => $shopee
                    );
                }
            }

            // lazada
            $lazada = [];
            if ($this->user->hasPermission('access', 'sale_channel/lazada')) {
                $lazada[] = array(
                    'name' => $this->language->get('text_lazada_products'),
                    'href' => $this->url->link('sale_channel/lazada/products', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['sale_channel/shopee'],
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'sale_channel/lazada')) {
                $lazada[] = array(
                    'name' => $this->language->get('text_lazada_orders'),
                    'href' => $this->url->link('sale_channel/lazada/orders', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['sale_channel/shopee/orders'],
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'sale_channel/lazada')) {
                $lazada[] = array(
                    'name' => $this->language->get('text_config'),
                    'href' => $this->url->link('sale_channel/lazada/config', 'user_token=' . $this->session->data['user_token'], true),
                    'route' => ['sale_channel/shopee/config'],
                    'children' => array()
                );
            }

            if ($this->isEnableModule(self::$MODULE_ECOM_PLATFORM) == 1) {
                if ($lazada) {
                    $data['lazada'][] = array(
                        'name' => $this->language->get('text_lazada'),
                        'icon' => 'view/image/custom/icons/lazada.svg',
                        'href' => $this->url->link('sale_channel/lazada/config', 'user_token=' . $this->session->data['user_token'], true),
                        'route' => ['sale_channel/lazada/products', 'sale_channel/lazada/config'], //'sale_channel/lazada/orders',
                        'children' => $lazada
                    );
                }
            }

            // setting
            if (false && $this->user->hasPermission('access', '#')) {
                $data['menus'][] = array(
                    'id' => 'menu-setting',
                    'icon' => 'fa-cog',
                    'name' => $this->language->get('text_setting'),
                    'href' => '#',
                    'children' => array()
                );
            }

            // add menu from app_config if has
            // ...

            if (isset($this->request->get['route'])) {
                $data['page_route'] = $this->request->get['route'];
            }

            // TODO: remove $data['pos'], $data['config_url_pos'] ...
            $data['pos'] = $this->config->get('config_pos_status');
            $data['config_url_pos'] = $this->config->get('config_pos_url');

            $data['novaonx_social'] = $this->config->get('config_novaonx_social_status');
            $data['visible_module_ecom_platform'] = $this->isEnableModule(self::$MODULE_ECOM_PLATFORM);
            $data['visible_module_pos'] = $this->isEnableModule(self::$MODULE_POS);
            $data['visible_module_social'] = $this->isEnableModule(self::$MODULE_SOCIAL);

            // setting
            if ($this->user->hasPermission('access', 'settings/settings')) {
                $data['setting_links'] = $this->url->link('settings/settings', 'user_token=' . $this->session->data['user_token'], true);
            }
            $data['shopee_config_url'] = $this->url->link('sale_channel/shopee/config', 'user_token=' . $this->session->data['user_token'], true);
            $data['lazada_config_url'] = $this->url->link('sale_channel/lazada/config', 'user_token=' . $this->session->data['user_token'], true);
            $data['is_has_product_permission'] = $this->user->hasPermission('access', 'catalog/product');
            //get type user
            $this->load->model('user/user');
            $user_info = $this->model_user_user->getUser($this->user->getId());
            $data['user_type'] = isset($user_info['type']) ? $user_info['type'] : 'anonymous';

            $data['sync_state_href'] = $this->url->link('sale_channel/shopee/getSyncState&user_token=' . $this->session->data['user_token'], '', true);
            $data['sync_state_lazada_href'] = $this->url->link('sale_channel/lazada/getSyncState&user_token=' . $this->session->data['user_token'], '', true);

            /* sample data */
            $data['show_button_sample_data'] = !$this->isPaidShop() && $this->user->hasPermission('access', 'demo_data/demo_data');
            if ($data['show_button_sample_data']) {
                /* build query string from current get params */
                $data_request_get = $this->request->get;
                $query = '';
                foreach ($data_request_get as $key => $value) {
                    if ($key != 'route' && $key != 'user_token' && 'is_delete_demo_data' != $key) {
                        $query .= '&' . rawurlencode((string)$key) . '=' . rawurlencode((is_array($value) ? http_build_query($value) : (string)$value));
                    }
                }

                $demo_data_query = '&current_route=' . rawurlencode($this->request->get['route']) . '&current_query_param=' . rawurlencode($query);
                $data['href_use_sample_data'] = $this->url->link('demo_data/demo_data', 'user_token=' . $this->session->data['user_token'] . $demo_data_query, true);
                $data['href_delete_sample_data'] = $this->url->link('demo_data/demo_data/deleteDataDemo', 'user_token=' . $this->session->data['user_token'] . $demo_data_query, true);
            }

            $data['source_adg'] = (!defined('SOURCE_PRODUCTION_FOR_ADG') || SOURCE_PRODUCTION_FOR_ADG == false) ? 1 : 0;

            return $this->load->view('common/custom_column_left', $data);
        }
    }

    //Add permission channel
    public function addPermission()
    {
        $this->load->model('catalog/shopee');
        $this->load->language('common/column_left');
        $channel = $this->request->post['channel'];
        $json = array();
        if ($this->user->hasPermission('access', $channel) && $this->user->hasPermission('modify', $channel)) {
            $json['error'] = $this->language->get('text_error_permission');
        }
        $this->load->model('user/user_group');
        $user_group_id = $this->model_user_user_group->getUserGroupIdByUserId($this->user->getId());
        if (!$this->user->hasPermission('access', $channel)) {
            $this->model_user_user_group->addPermission($user_group_id, 'access', $channel);
            if ($channel == 'sale_channel/shopee') {
                $this->model_user_user_group->addPermission($user_group_id, 'access', 'sale_channel/shopee_upload');
            }
            $json['success'] = $this->language->get('text_add_shopee_success');
        }
        if (!$this->user->hasPermission('modify', $channel)) {
            $this->model_user_user_group->addPermission($user_group_id, 'modify', $channel);
            if ($channel == 'sale_channel/shopee') {
                $this->model_user_user_group->addPermission($user_group_id, 'modify', 'sale_channel/shopee_upload');
            }
            $json['success'] = $this->language->get('text_add_shopee_success');
        }
        $this->model_catalog_shopee->createTable();
        // shopee config
        $this->load->model('setting/setting');
        $config = [];
        $config['sync_name'] = 1;
        $config['sync_desc'] = 1;
        $config['sync_price'] = 1;
        $config['sync_quantity'] = 1;
        $config['sync_weight'] = 1;

        $config = json_encode($config);
        $this->model_setting_setting->addConfig('shopee_config', $config);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    //Remove permission channel
    public function removePermission()
    {
        $this->load->model('catalog/shopee');
        $this->load->language('common/column_left');
        $channel = $this->request->post['channel'];
        $json = array();
        if (!$this->user->hasPermission('access', $channel) && !$this->user->hasPermission('modify', $channel)) {
            $json['error'] = $this->language->get('text_remove_error_permission');
        }
        $this->load->model('user/user_group');
        $user_group_id = $this->model_user_user_group->getUserGroupIdByUserId($this->user->getId());
        if ($this->user->hasPermission('access', $channel)) {
            $this->model_user_user_group->removePermission($user_group_id, 'access', $channel);
            if ($channel == 'sale_channel/shopee') {
                $this->model_user_user_group->removePermission($user_group_id, 'access', 'sale_channel/shopee_upload');
            }
            $json['success'] = $this->language->get('text_remove_shopee_success');
        }
        if ($this->user->hasPermission('modify', $channel)) {
            $this->model_user_user_group->removePermission($user_group_id, 'modify', $channel);
            if ($channel == 'sale_channel/shopee') {
                $this->model_user_user_group->removePermission($user_group_id, 'modify', 'sale_channel/shopee_upload');
            }
            $json['success'] = $this->language->get('text_remove_shopee_success');
        }
//        $this->model_catalog_shopee->removeTable();

        // remove shopee config
        $this->load->model('setting/setting');
        $this->model_setting_setting->deleteSettingValue('config', 'shopee_config');

        // remove shop_id
        $this->model_setting_setting->deleteSettingValue('config', 'shopee_shop_id');

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function updatePos()
    {
        $json = array();
        $this->load->model('setting/setting');
        $data = $this->request->post;
        if ($data['pos'] == 1) {
            $this->model_setting_setting->editSettingValue('config', 'config_pos_status', 0);
            $this->model_setting_setting->deleteSettingValue('config', self::CONFIG_BESTME_POS_API_KEY);
        } else {
            $this->model_setting_setting->editSettingValue('config', 'config_pos_status', 1);
            $api_key = token(32);
            $this->model_setting_setting->editSettingValue($code = 'config', self::CONFIG_BESTME_POS_API_KEY, $api_key);
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function updatebtnNovaonXSocial()
    {
        $json = array();
        $this->load->model('setting/setting');
        $data = $this->request->post;
        if ($data['novaonx_social'] == 1) {
            $this->model_setting_setting->editSettingValue('config', 'config_novaonx_social_status', 0);
            $this->model_setting_setting->deleteSettingValue('config', self::CONFIG_BESTME_NOVAONX_SOCIAL_API_KEY);
        } else {
            $this->model_setting_setting->editSettingValue('config', 'config_novaonx_social_status', 1);
            $api_key = token(32);
            $this->model_setting_setting->editSettingValue($code = 'config', self::CONFIG_BESTME_NOVAONX_SOCIAL_API_KEY, $api_key);
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}