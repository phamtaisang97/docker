<?php
class ControllerCommonLogin extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('common/login');

		$this->document->setTitle($this->language->get('heading_title'));

		if ($this->user->isLogged() && isset($this->request->get['user_token']) && ($this->request->get['user_token'] == $this->session->data['user_token'])) {
			$this->response->redirect($this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true));
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->session->data['user_token'] = token(32);

			/* update last login */
            $this->load->model('user/user');
            $this->model_user_user->editLastLoggedIn($this->user->getId());

            if (isset($this->request->post['theme']) && $this->request->post['theme'] != false){
                $this->setUseTheme();

                $this->response->redirect($this->url->link('custom/theme', 'user_token=' . $this->session->data['user_token'], true));
            }
            // Kiểm tra nếu có param là app thì tiến hành cài đặt app
            if (isset($this->request->post['app_code']) && $this->request->post['app_code'] != false){
                $this->installApp();
                $this->response->redirect($this->url->link('app_store/my_app', 'user_token=' . $this->session->data['user_token'], true));
            }

			//update trial date config when login success
            if (is_null($this->config->get('config_packet_trial_start'))) {
                $this->load->model('setting/setting');
                $timezone = ini_get('date.timezone') ? ini_get('date.timezone') : date_default_timezone_get();
                $now = new DateTime('now', new DateTimeZone($timezone));
                if(defined('SOURCE_PRODUCTION_FOR_ADG') && SOURCE_PRODUCTION_FOR_ADG) {
                    $this->model_setting_setting->editSettingValue($code = 'config', 'config_packet_trial_start', $now->format("Y-m-d"), $store_id = 0);
                    $this->model_setting_setting->editSettingValue($code = 'config', 'config_packet_trial_period', defined('PACKET_TRIAL_PERIOD_DEFAULT') ? PACKET_TRIAL_PERIOD_DEFAULT : 0, $store_id = 0);

                    $this->model_setting_setting->editSettingValue($code = 'config', 'config_packet_capacity', 10, $store_id = 0);
                    $this->model_setting_setting->editSettingValue($code = 'config', 'config_packet_number_of_stores', null, $store_id = 0);
                    $this->model_setting_setting->editSettingValue($code = 'config', 'config_packet_unlimited_capacity', 0, $store_id = 0);
                    $this->model_setting_setting->editSettingValue($code = 'config', 'config_packet_number_of_staffs', null, $store_id = 0);
                    $this->model_setting_setting->editSettingValue($code = 'config', 'config_packet_unlimited_stores', 1, $store_id = 0);
                    $this->model_setting_setting->editSettingValue($code = 'config', 'config_packet_unlimited_staffs', 1, $store_id = 0);
                    $this->model_setting_setting->editSettingValue($code = 'config', 'config_packet_paid', 1, $store_id = 0);
                    $this->model_setting_setting->editSettingValue($code = 'config', 'config_packet_paid_type', "Gói nâng cao", $store_id = 0);
                    $this->model_setting_setting->editSettingValue($code = 'config', 'config_packet_paid_type_en', "Advance", $store_id = 0);
                    $this->model_setting_setting->editSettingValue($code = 'config', 'config_packet_paid_start', $now->format("Y-m-d"), $store_id = 0);
                    $date_end = Date('Y-m-d', strtotime('+36000 days'));
                    $this->model_setting_setting->editSettingValue($code = 'config', 'config_packet_paid_end', $date_end, $store_id = 0);
                }else{
                    $this->model_setting_setting->editSettingValue($code = 'config', 'config_packet_trial_start', $now->format("Y-m-d"), $store_id = 0);
                    $this->model_setting_setting->editSettingValue($code = 'config', 'config_packet_trial_period', defined('PACKET_TRIAL_PERIOD_DEFAULT') ? PACKET_TRIAL_PERIOD_DEFAULT : 0, $store_id = 0);

                    /* notice: NO NEED force update loaded config (auto redirect after logged in!) */
                }
            }

			if (isset($this->request->post['redirect']) && (strpos($this->request->post['redirect'], HTTP_SERVER) === 0 || strpos($this->request->post['redirect'], HTTPS_SERVER) === 0)) {
				$this->response->redirect($this->request->post['redirect'] . '&user_token=' . $this->session->data['user_token']);
			} else {
				$this->response->redirect($this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true));
			}
		}

		$data['header'] = $this->load->controller('common/header');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('common/login', $data));
	}

    private function setUseTheme(){
        /* call showtheme from here to avoid hack to use paid theme from ajax call */
        $this->load->controller('setting/setting/showtheme');

        $this->load->controller('setting/setting/usetheme');
    }
    private function installApp(){
        $this->load->controller('app_store/my_app/install');
    }
	protected function validate() {
		if (!isset($this->request->post['username']) || !isset($this->request->post['password']) || !isset($this->request->post['token'])
            || !isset($this->request->post['email']) || !isset($this->request->post['shop']) || !$this->user->loginFromCms(
		    $this->request->post['username'], html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8'),
                $this->request->post['token'], $this->request->post['email'] , $this->request->post['shop']
            )
        ) {
			$this->error['warning'] = $this->language->get('error_login');
		}

		return !$this->error;
	}
}
