<?php

class ControllerCommonBestmeInstallApp extends Controller
{
    private $error = array();

    public function index()
    {

        $json = array();
        $json['success'] = false;
        $json['code'] = 101;
        $json['msg'] = "Install App Error";

        if (isset($_GET['user_token'])){
            $this->load->model('user/user');
            $user_id = $this->model_user_user->checkTokenSession($_GET['user_token']);
            $app_code = $_GET['app_code'];
            if ($user_id){
                $this->session->data['user_id'] = $user_id;
                $this->registry->set('user', new Cart\User($this->registry));

                $this->load->model('appstore/my_app');

                if( !$this->model_appstore_my_app->checkInstall($app_code)){

                    $this->load->language('extension/extension/appstore');

                    $this->load->model('setting/extension');

                    $this->load->model('setting/appstore_setting');

                    $this->model_setting_extension->install('appstore', $app_code);

                    $this->load->model('user/user_group');

                    $this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'extension/appstore/' . $app_code);
                    $this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'extension/appstore/' . $app_code);

                    // Add app to my_app
                    $used_time = null;
                    if(isset($_GET['used_time'])){
                        $used_time = $_GET['used_time'];
                    }
                    $this->model_appstore_my_app->install($app_code,$used_time);

                    // Call install method if it exsits
                    $this->load->controller('extension/appstore/' . $app_code . '/install');

                }

                $json['success'] = true;
                $json['code'] = 200;
                $json['msg'] = 'Success';
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
