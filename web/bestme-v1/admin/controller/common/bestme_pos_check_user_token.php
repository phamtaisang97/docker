<?php

class ControllerCommonBestmePosCheckUserToken extends Controller
{
    private $error = array();

    public function index()
    {
        $json = array();
        $json['status'] = false;
        $json['msg'] = 'session timeout';

        if (isset($_GET['user_token'])){
            $this->load->model('user/user');
            $user_id = $this->model_user_user->checkTokenSession($_GET['user_token']);
            if ($user_id){
                $json['status'] = true;
                $json['msg'] = 'user_id: ' . $user_id;
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
