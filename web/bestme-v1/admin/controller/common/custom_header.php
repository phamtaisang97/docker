<?php

class ControllerCommonCustomHeader extends Controller {
    use Setting_Util_Trait;

    const DEMO_DATA_ACTION = ['load', 'delete'];

	public function index() {
        $this->load->controller('event/activity_tracking');

		$data['title'] = $this->document->getTitle();

		if ($this->request->server['HTTPS']) {
			$data['base'] = HTTPS_SERVER;
		} else {
			$data['base'] = HTTP_SERVER;
		}

		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		$this->load->language('common/header');

		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->user->getUserName());

        $data['config_packet_paid'] = $this->config->get('config_packet_paid');
        $data['config_packet_paid_type'] = $this->config->get('config_packet_paid_type');
        $data['config_packet_paid_type_name'] = $this->language->get($data['config_packet_paid_type'])
            ? $this->language->get($data['config_packet_paid_type'])
            : $data['config_packet_paid_type'];

        $data['packet_wait_for_pay'] = $this->getSettingPacketWaitForPayValue();
        $data['visible_module_website'] = $this->isEnableModule(self::$MODULE_WEBSITE);

		if (!isset($this->request->get['user_token']) || !isset($this->session->data['user_token']) || ($this->request->get['user_token'] != $this->session->data['user_token'])) {
			$data['logged'] = '';

			$data['home'] = $this->url->link('common/dashboard', '', true);
		} else {
			$data['logged'] = true;

			$data['home'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true);
            $data['logout'] = $this->url->link('common/logout', 'user_token=' . $this->session->data['user_token'], true);
            $data['profile'] = $this->url->link('common/profile', 'user_token=' . $this->session->data['user_token'], true);

            $this->load->model('user/user');

            $this->load->model('tool/image');

            $user_info = $this->model_user_user->getUser($this->user->getId());
            if (isset($user_info['type']) && $user_info['type'] == 'Admin') {
                $data['href_config_account'] = $this->url->link('settings/account', 'user_token=' . $this->session->data['user_token'], true);
            } elseif ($this->user->hasPermission('access', 'settings/settings')) {
                $data['href_config_account'] = $this->url->link('settings/account/editStaff', 'user_token=' . $this->session->data['user_token'] . '&id=' . $user_info['user_id'], true);
            }

            if ($user_info) {
				$data['firstname'] = $user_info['firstname'];
				$data['lastname'] = $user_info['lastname'];
				$data['username']  = $user_info['username'];
				$data['user_group'] = $user_info['user_group'];
				$data['email'] = $user_info['email'];

				if (!empty($user_info['image'])) {
					$data['image'] = $this->model_tool_image->resize($user_info['image'], 45, 45);
				} else {
					$data['image'] = $this->model_tool_image->resize('profile.png', 45, 45);
				}
			} else {
				$data['firstname'] = '';
				$data['lastname'] = '';
				$data['user_group'] = '';
				$data['image'] = '';
				$data['email'] = '';
			}

            $data['user_name'] = $data['firstname'].' '.$data['lastname'];
			// Online Stores
			$data['stores'] = array();

			$data['stores'][] = array(
				'name' => $this->config->get('config_name'),
				'href' => HTTP_CATALOG
			);

			$this->load->model('setting/store');

			$results = $this->model_setting_store->getStores();

			foreach ($results as $result) {
				$data['stores'][] = array(
					'name' => $result['name'],
					'href' => $result['url']
				);
			}

			$data['default_store'] = array(
                'name' => $this->config->get('config_name'),
                'href' => HTTP_CATALOG
            );
		}


        if (defined('CHANGE_PASSWORD_HREF') && defined('CHANGE_PASSWORD_HREF')){
            $data['href_change_password'] = $this->url->link('settings/account/edit', 'user_token=' . $this->session->data['user_token'], true);
        }

        $data['change_password'] = $this->language->get('change_password');

		/* build query string from current get params */
        $data_request_get = $this->request->get;
        $query = '';
        foreach ($data_request_get as $key => $value) {
            if ($key != 'route' && $key != 'user_token' && 'demo_data_action' != $key && 'is_delete_demo_data' != $key) {
                $query .= '&' . rawurlencode((string)$key) . '=' . rawurlencode((is_array($value) ? http_build_query($value) : (string)$value));
            }
        }

		/* support switch language */
        // very hardcode. TODO: get languages list from config and show to view...
        $data['href_change_language_vi'] = $this->url->link($this->request->get['route'], 'user_token=' . $this->session->data['user_token'] . $query . '&lang=vi-vn', true);
        $data['href_change_language_en'] = $this->url->link($this->request->get['route'], 'user_token=' . $this->session->data['user_token'] . $query . '&lang=en-gb', true);
        $data['href_current_url'] = $this->url->link($this->request->get['route'], 'user_token=' . $this->session->data['user_token'], true);
        $data['current_language'] = $this->config->get('config_language'); // vi-vn or en-gb...

        /* package */
        $data['config_packet_paid'] = $this->config->get('config_packet_paid');
        $data['config_packet_paid_type_name'] = 'vi-vn' == $data['current_language']
            ? $this->config->get('config_packet_paid_type')
            : $this->config->get('config_packet_paid_type_en');

        /* sample data */
        $data['show_button_sample_data'] = !$this->isPaidShop() && $this->user->hasPermission('access', 'demo_data/demo_data');
        if ($data['show_button_sample_data']) {
            $demo_data_query = '&current_route=' . rawurlencode($this->request->get['route']) . '&current_query_param=' . rawurlencode($query);
            $data['href_use_sample_data'] = $this->url->link('demo_data/demo_data', 'user_token=' . $this->session->data['user_token'] . $demo_data_query, true);
            $data['href_delete_sample_data'] = $this->url->link('demo_data/demo_data/deleteDataDemo', 'user_token=' . $this->session->data['user_token'] . $demo_data_query, true);
            $data['is_delete_demo_data'] = isset($this->request->get['is_delete_demo_data']) && '1' == $this->request->get['is_delete_demo_data'];
        }
        // show alert delete demo data when active shop
        if ($this->config->get('config_show_alert_demo_data_deleted')) {
            $data['is_delete_demo_data'] = 1;
            $this->load->model('setting/setting');
            $this->model_setting_setting->editSettingValue('config', 'config_show_alert_demo_data_deleted', 0);
        }

        $data['path_logo'] = PRODUCTION_BRAND_LOGO;
        $data['path_favicon'] = PRODUCTION_BRAND_FAVICON;
        $data['source_adg'] = (!defined('SOURCE_PRODUCTION_FOR_ADG') || SOURCE_PRODUCTION_FOR_ADG == false) ? 1 : 0;
		return $this->load->view('common/custom_header', $data);
	}
}