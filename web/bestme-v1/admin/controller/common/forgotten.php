<?php

class ControllerCommonForgotten extends Controller
{
    private $error = array();

    public function index()
    {
        $response = [
            'status' => false,
            'message' => 'Hãy đảm bảo tên cửa hàng và email của bạn đúng'
        ];
        $this->response->addHeader('Content-Type: application/json');
        if ($this->user->isLogged() && isset($this->request->get['user_token']) && ($this->request->get['user_token'] == $this->session->data['user_token'])) {
            $this->response->addHeader('Content-Type: application/json');
            $response = [
                'status' => true,
                'message' => 'Đã đăng nhập, có thể đến bảng điều khiển'
            ];
        }

        if (!$this->config->get('config_password')) {
            $this->response->addHeader('Content-Type: application/json');
            $response = [
                'status' => true,
                'message' => 'Already set password, please login'
            ];
        }

        $this->load->language('common/forgotten');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('user/user');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_user_user->editCode($this->request->post['email'], token(40), $this->request->post['shop'], $_POST['new_password']);

            $this->session->data['success'] = $this->language->get('text_success');

            $response = [
                'status' => true,
                'message' => 'Email xác nhận đổi mật khẩu đã được gửi tới email của bạn. Vui lòng kiểm tra email và thực hiện theo hướng dẫn.'
            ];
        }

        $this->response->setOutput(json_encode($response));
    }

    protected function validate()
    {
        if (!isset($this->request->post['email']) || !isset($this->request->post['new_password']) || !isset($this->request->post['shop'])) {
            $this->error['warning'] = $this->language->get('error_email');
        } elseif (!$this->model_user_user->getTotalUsersByEmail($this->request->post['email'])) {
            $this->error['warning'] = $this->language->get('error_email');
        }

        return !$this->error;
    }
}