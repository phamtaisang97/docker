<?php

class ControllerCommonReset extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('common/reset');

        if ($this->user->isLogged() && isset($this->request->get['user_token']) && ($this->request->get['user_token'] == $this->session->data['user_token'])) {
            $this->response->redirect($this->url->link('common/dashboard', '', true));
        }

        if (!$this->config->get('config_password')) {
            $data['text_notify'] = $this->language->get('error_disabled_forgotten');
        }

        if (isset($this->request->get['code'])) {
            $code = $this->request->get['code'];
        } else {
            $code = '';
        }

        $this->load->model('user/user');

        $user_info = $this->model_user_user->getUserByCode($code);

        $this->document->setTitle($this->language->get('heading_title'));

        if ($user_info) {
            if ($this->validate()) {
                /* set password_tmp value as password */
                $this->model_user_user->editPasswordActivationSuccess($user_info['user_id']);

                $data['text_notify'] = $this->language->get('text_success');
            } else {
                $data['text_notify'] = $this->language->get('text_success');
            }
        } else {
            $this->load->model('setting/setting');

            /* automatically disable forgotten if detected hacks */
            // $this->model_setting_setting->editSettingValue('config', 'config_password', '0');

            $data['text_notify'] = $this->language->get('error_invalid_activation_link');
        }

        $data['header'] = $this->load->controller('common/header');
        $data['footer'] = $this->load->controller('common/footer');
        $data['bestme_home_link'] = BESTME_SERVER;

        $this->response->setOutput($this->load->view('common/reset', $data));
    }

    protected function validate()
    {
        return !$this->error;
    }
}