<?php
class ControllerCommonLogout extends Controller {
	public function index() {

	    $session_id = $this->session->getID();

		$this->user->logout();

		unset($this->session->data['user_token']);
        $url = BESTME_SERVER . 'admin/login';
		$this->response->redirect($url);
	}
}