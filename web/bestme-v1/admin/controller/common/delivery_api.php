<?php

class ControllerCommonDeliveryApi extends Controller
{
    /**
     * login
     */
    public function login()
    {
        $this->load->language('common/delivery_api');
        $data = $this->request->post;
        try {
            $transport = new Transport($data['delivery_method'], $data, $this);
            $response = $transport->login($data);
            if ($response['status']) {
                $json = [
                    'status' => true,
                    'message' => $this->language->get('text_success')
                ];
            } else {
                $json = [
                    'status' => false,
                    'message' => $response['msg'],
                    'delivery_method' => $data['delivery_method']
                ];
            }
        } catch (Exception $e) {
            $json = [
                'status' => false,
                'message' => $this->language->get('error')
            ];
        }

        $this->responseJson($json);
    }

    /**
     * logout
     */
    public function logout()
    {
        $data = $this->request->get;
        if (isset($data['method'])) {
            $this->load->model('setting/setting');
            $transport_config = is_null($this->config->get('config_transport_active')) ? '' : $this->config->get('config_transport_active');
            $transport_list = explode(',', $transport_config);
            if (in_array($data['method'], $transport_list)) {
                $transport_list = array_diff($transport_list, [$data['method']]);
            }
            if ($transport_list) {
                $this->model_setting_setting->editSettingValue('config', 'config_transport_active', implode(',', $transport_list));
            } else {
                $this->model_setting_setting->deleteSettingValue('config', 'config_transport_active');
            }

            // TODO: create and use fn logout() in transport class instead of this...
            // like this:
            // transport = new Transport(...);
            // transport->logout();
            $this->model_setting_setting->deleteSettingValue('config', 'config_' . strtoupper($data['method']) . '_active');
            $this->model_setting_setting->deleteSettingValue('config', 'config_' . strtoupper($data['method']) . '_username');
            $this->model_setting_setting->deleteSettingValue('config', 'config_' . strtoupper($data['method']) . '_password');
            $this->model_setting_setting->deleteSettingValue('config', 'config_' . strtoupper($data['method']) . '_token');

            if ('GHN' == strtoupper($data['method'])) {
                $this->model_setting_setting->deleteSettingValue('config', 'config_' . strtoupper($data['method']) . '_hub');
                $this->model_setting_setting->deleteSettingValue('config', 'config_' . strtoupper($data['method']) . '_phone');
            }
        }

        $this->responseJson([]);
    }

    /**
     * getHubs
     */
    public function getHubs()
    {
        $data = $this->request->post;
        $html = '';
        try {
            $transport = new Transport($data['delivery_method'], $data, $this);
            $result = $transport->getConfiguredPickingWarehouse($data, true);
            $html .= $result;
        } catch (Exception $e) {

        }
        $this->response->addHeader('Content-Type: application/json');
        $json = [
            'html' => $html,
        ];

        $this->responseJson($json);
    }

    /**
     * updateHub
     */
    public function updateHub()
    {
        $this->load->language('common/delivery_api');
        $data = $this->request->get;
        if (isset($data['method'])) {
            $transport = new Transport($data['method'], $cre = [], $this);
            $result = $transport->setMapConfiguredPickingWarehouse($data);

            $json = [
                'status' => $result,
                'message' => $result ? $this->language->get('text_success') : $this->language->get('update_hub_error'),
            ];
        } else {
            $json = [
                'status' => false
            ];
        }

        $this->responseJson($json);
    }

    /**
     * getListServiceVariable
     */
    public function getListServiceVariable()
    {
        $this->load->language('common/delivery_api');
        $data = $this->request->post;
        $html = '';
        $json = array();
        try {
            if (!empty($data['transport_list_active'])) {
                $this->load->model('appstore/my_app');
                // TODO: debug + test. Remove if code works properly!...
                $transport_list = explode(',', $data['transport_list_active']);
                foreach ($transport_list as $key => $transport_name) {
                    $trans_app_code = $transport_name;
                    if ($transport_name == 'Ghn') {
                        $trans_app_code = ModelAppstoreMyApp::APP_GHN;
                    }

                    if ($transport_name == 'viettel_post') {
                        $trans_app_code = ModelAppstoreMyApp::APP_VIETTEL_POST;
                    }

                    if (!$this->model_appstore_my_app->checkAppActive($trans_app_code)) {
                        unset($transport_list[$key]);
                    };
                }

                foreach ($transport_list as $delivery_name) {
                    if (in_array($delivery_name, ['Ghn', 'viettel_post'])) {
                        $data['token'] = $this->config->get('config_' . strtoupper($delivery_name) . '_token');
                    } else {
                        $data['token'] = $this->config->get('config_' . $delivery_name . '_token');
                    }
                    $transport = new Transport($delivery_name, $data, $this);
                    if (isset($data['token']) && !empty($data['token'])) {
                        $result = $transport->getListService($data);
                    } else {
                        $result = '';
                    }

                    $html .= $result;
                }

                $json = [
                    'html' => $html,
                ];

                $this->responseJson($json);
            }
        } catch (Exception $e) {
            $json = [
                'status' => false,
                'message' => $this->language->get('error'),
            ];
        }

        $this->responseJson($json);
    }

    /**
     * createOrderDelivery
     */
    public function createOrderDelivery()
    {
        $this->load->language('common/delivery_api');
        $data = $this->request->post;
        $json = [
            'status' => false,
            'message' => $this->language->get('text_error_choose_service'),
        ];
        try {
            if (isset($data['transport_method']) && !empty($data['transport_method'])) {
                $data['token'] = $this->config->get('config_' . strtoupper($data['transport_method']) . '_token');
                $transport = new Transport($data['transport_method'], $data, $this);
                if (!$transport->getAdaptor() instanceof \Transport\Abstract_Transport) {
                    $this->responseJson($json);
                }

                $result = $transport->getAdaptor()->createOrderDelivery($data);
                if ($result['status']) {
                    $json = [
                        'status' => true,
                        'message' => 'success',
                    ];
                    //edit order
                    $data['order_status'] = ModelSaleOrder::ORDER_STATUS_ID_DELIVERING;
                    $data['is_create_order_delivery'] = true;
                    $this->load->model('sale/order');
                    $this->model_sale_order->editOrder($data['order_id'], $data);

                    if (isset($data['collection_amount']) && isset($data['order_id'])) {
                        $this->model_sale_order->updateCollectionAmount($data['order_id'], $data['collection_amount']);
                    }

                } else {
                    $json = [
                        'status' => false,
                        'message' => isset($result['msg']) ? $result['msg'] : $this->language->get('text_error_unknown'),
                    ];
                }
            }
        } catch (Exception $e) {
            $json = [
                'status' => false,
                'message' => $this->language->get('text_error_unknown'),
            ];
        }

        $this->responseJson($json);
    }

    /**
     * cancelOrderDelivery
     */
    public function cancelOrderDelivery()
    {
        $this->load->language('common/delivery_api');
        $data = $this->request->post;
        $json = [
            'status' => false,
            'message' => $this->language->get('text_error_choose_service'),
        ];
        try {
            if (isset($data['transport_method']) && !empty($data['transport_method'])) {
                $data['token'] = $this->config->get('config_' . strtoupper($data['transport_method']) . '_token');
                $transport = new Transport($data['transport_method'], $data, $this);
                $result = $transport->cancelOrder($data);
                if ($result['status']) {
                    $json = [
                        'status' => true,
                        'message' => 'success',
                    ];
                } else {
                    $json = [
                        'status' => false,
                        'message' => isset($result['msg']) ? $result['msg'] : $this->language->get('text_error_unknown'),
                    ];
                }
            }
        } catch (Exception $e) {
            $json = [
                'status' => false,
                'message' => $this->language->get('text_error_unknown'),
            ];
        }

        $this->responseJson($json);
    }
}