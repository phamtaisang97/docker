<?php


class ControllerCommonFileManager extends Controller
{
    const IMAGE_SERVER_UPLOAD_DEFAULT = 'bestme'; // bestme, cloudinary
    const SUPPORTED_IMAGE_SERVER_UPLOAD = [
        'bestme',
        'cloudinary',
    ];

    const LIST_IMAGE_LIMIT = 32;
    const MANAGER_LIST_IMAGE_LIMIT = 30;

    const DEFAULT_MAX_IMAGE = 7;

    const ROOT_FOLDER_ID = 0;

    private $error = array();

    public function index()
    {
        $this->load->model('custom/image');
        $this->load->model('tool/image');
        $this->load->language('common/filemanager');
        $single_select_param = '';
        if (isset($this->request->get['single']) && $this->request->get['single']) {
            $single_select_param = '&single=true';
            $data['single'] = $this->request->get['single'];
        }
        $store_id = $this->getStoreId();  /// dictory

        $filter = [];
        $filter['image_folder'] = isset($this->request->get['image_folder']) ? (int)$this->request->get['image_folder'] : 0;

        $filter['status'] = 1;
        $filter['page'] = $data['page'] = isset($this->request->get['page']) ? (int)$this->request->get['page'] : 1;
        $filter['start'] = ((int)$filter['page'] - 1) * self::LIST_IMAGE_LIMIT;
        $filter['limit'] = self::LIST_IMAGE_LIMIT;
        $filter['name'] = isset($this->request->get['filter_name']) ? $this->request->get['filter_name'] : '';

        // Make sure we have the correct directory
        if (isset($this->request->get['directory'])) {
            $directory = str_replace(' ', '', $this->request->get['directory']);
            $directory = strlen($directory) != 0 ? $store_id . '/' . $directory : $store_id;
        } else {
            $directory = $store_id;
        }

        $images = $this->model_custom_image->getImages($filter);
        $total = $this->model_custom_image->countImages($filter);
        // next_cursor = trang tiêp theo nếu vẫn còn ảnh
        $data['next_cursor'] = ($filter['start'] + $filter['limit']) < (int)$total ? $data['page'] + 1 : NULL;

        $this->load->model('custom/image_folder');
        $folders = $this->model_custom_image_folder->getFolders($filter['image_folder']);
        $full_folder = [];
        if (!empty($folders)) {
            foreach ($folders as $folder) {
                $folder['path'] = $this->url->link('common/filemanager', 'user_token=' . $this->session->data['user_token'] . '&image_folder=' . $folder['id'], true);
                array_push($full_folder, $folder);
            }
        }
        $data['folders'] = $full_folder;
        $data['image_folder'] = $filter['image_folder'];
        $image_folder = $this->model_custom_image_folder->getParent($filter['image_folder']);
        $data['folder_now'] = $image_folder['folder_now'];
        $data['folder_parent'] = $image_folder['folder_parent'];
        // Get total number of files and directories
        $data['images'] = array();
        foreach ($images as $image) {
            if (!is_array($image) || !array_key_exists('url', $image)) {
                continue;
            }
            //$name = str_split(basename($imageName), 14);
            $data['images'][] = array(
                'public_id' => $image['source_id'],
                'thumb' => $this->model_tool_image->resize($image['url'], 250, 250),
                'name' => $image['name'],
                'path' => $image['url'],
                'image_id' => $image['image_id']
            );
        }
        $data['user_token'] = $this->session->data['user_token'];

        if (isset($this->request->get['directory'])) {
            $data['directory'] = urlencode($this->request->get['directory']);
        } else {
            $data['directory'] = '';
        }

        if (isset($this->request->get['filter_name'])) {
            $data['filter_name'] = $this->request->get['filter_name'];
        } else {
            $data['filter_name'] = '';
        }

        // Return the target ID for the file manager to set the value
        if (isset($this->request->get['target'])) {
            $data['target'] = $this->request->get['target'];
        } else {
            $data['target'] = '';
        }

        // Return the thumbnail for the file manager to show a thumbnail
        if (isset($this->request->get['thumb'])) {
            $data['thumb'] = $this->request->get['thumb'];
        } else {
            $data['thumb'] = '';
        }

        // Refresh
        $url = '';

        if (isset($this->request->get['directory'])) {
            $url .= '&directory=' . urlencode($this->request->get['directory']);
        }

        if (isset($this->request->get['target'])) {
            $url .= '&target=' . $this->request->get['target'];
        }

        if (isset($this->request->get['thumb'])) {
            $url .= '&thumb=' . $this->request->get['thumb'];
        }
        if ($this->config->get('config_language') == 'vi-vn') {
            $data['placeholder_image'] = 'view/image/theme/upload-placeholder.svg';
        } else { //en-gb
            $data['placeholder_image'] = 'view/image/theme/en-upload-placeholder.svg';
        }

        if(isset($this->request->get['max_images'])) {
            $data['max_images'] = (int)$this->request->get['max_images'];
        } else {
            $data['max_images'] = self::DEFAULT_MAX_IMAGE;
        }
        $data['warning_for_num_of_images'] = str_replace('{max_images}', $data['max_images'], $this->language->get('warning_for_num_of_images'));

        $data['refresh'] = $this->url->link('common/filemanager', 'user_token=' . $this->session->data['user_token'] . $url . $single_select_param . '&image_folder=' . $data['image_folder'], true);
        $data['upload_href'] = $this->url->link('common/filemanager/upload', 'user_token=' . $this->session->data['user_token'], true);
        $data['filemanager'] = str_replace('&amp;', '&', $this->url->link('common/filemanager', 'user_token=' . $this->session->data['user_token'] . '&ckeditor=1', true));
        if ($filter['image_folder'] > 0) {
            $this->response->setOutput($this->load->view('common/filemanager_folder', $data));
        } else {
            if (isset($this->request->get['parent_back'])) {
                $this->response->setOutput($this->load->view('common/filemanager_folder', $data));
            } else {
                if (isset($this->request->get['ckeditor']) && $this->request->get['ckeditor'] == 1) {
                    $this->response->setOutput($this->load->view('common/filemanagerImage', $data));
                } else {
                    $this->response->setOutput($this->load->view('common/filemanager', $data));
                }
            }
        }
    }

    public function ajaxLoadMore()
    {
        $this->load->model('custom/image');
        $this->load->model('tool/image');
        $this->load->language('common/filemanager');
        $single_select_param = '';
        if (isset($this->request->get['single']) && $this->request->get['single']) {
            $single_select_param = '&single=true';
        }
        $store_id = $this->getStoreId();  /// dictory

        $filter = [];
        $filter['image_folder'] = isset($this->request->get['image_folder']) ? (int)$this->request->get['image_folder'] : 0;
        $filter['status'] = 1;
        $filter['page'] = $data['page'] = isset($this->request->get['page']) ? (int)$this->request->get['page'] : 1;
        $filter['start'] = ((int)$filter['page'] - 1) * self::LIST_IMAGE_LIMIT;
        $filter['limit'] = self::LIST_IMAGE_LIMIT;
        $filter['name'] = isset($this->request->get['filter_name']) ? $this->request->get['filter_name'] : '';

        // Make sure we have the correct directory
        if (isset($this->request->get['directory'])) {
            $directory = str_replace(' ', '', $this->request->get['directory']);
            $directory = strlen($directory) != 0 ? $store_id . '/' . $directory : $store_id;
        } else {
            $directory = $store_id;
        }

        $images = $this->model_custom_image->getImages($filter);
        $total = $this->model_custom_image->countImages($filter);
        // next_cursor = trang tiêp theo nếu vẫn còn ảnh
        $data['next_cursor'] = ($filter['start'] + $filter['limit']) < (int)$total ? $data['page'] + 1 : '';

        // Get total number of files and directories
        $data['images'] = array();
        foreach ($images as $image) {
            if (!is_array($image) || !array_key_exists('url', $image)) {
                continue;
            }
            //$name = str_split(basename($imageName), 14);
            $data['images'][] = array(
                'public_id' => $image['source_id'],
                'thumb' => $this->model_tool_image->resize($image['url'], 250, 250),
                'name' => $image['name'],
                'path' => $image['url'],
                'image_id' => $image['image_id']
            );
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($data));
    }

    public function upload()
    {
        $this->load->language('common/filemanager');
        $this->load->model('custom/image');

        $json = [];

        // Check user has permission
        if (!$this->user->hasPermission('modify', 'common/filemanager')) {
            $json['error'] = $this->language->get('error_permission');
        }

        // check if current parent folder is hidden
        if (isset($this->request->post['folder_path'])) {
            $this->load->model('custom/image_folder');
            if ($this->model_custom_image_folder->isHiddenFolder($this->request->post['folder_path'])) {
                $json['error'] = $this->language->get('text_failure');
            }
        }

        $store_id = $this->getStoreId();
        $success = false;

        // Kiểm tra giới hạn dung lượng của shop
        $config_packet_unlimited_capacity = $this->config->get('config_packet_unlimited_capacity');
        if (isset($config_packet_unlimited_capacity) && $config_packet_unlimited_capacity != '1') {
            $config_packet_capacity = $this->config->get('config_packet_capacity');
            if ($config_packet_capacity) {
                // Total size
                $total_size = $this->model_custom_image->getTotalSizeImages();
                if ($total_size >= $config_packet_capacity * 1e6) {
                    $json['error'] = $this->language->get('error_limit_capacity');
                }
            } else {
                $json['error'] = $this->language->get('error_limit_capacity');
            }
        }

        if (!$json) {
            // Check if multiple files are uploaded or just one
            $files = array();

            if (!empty($this->request->files['file']['name']) && is_array($this->request->files['file']['name'])) {
                foreach (array_keys($this->request->files['file']['name']) as $key) {
                    $files[] = array(
                        'name' => $this->request->files['file']['name'][$key],
                        'type' => $this->request->files['file']['type'][$key],
                        'tmp_name' => $this->request->files['file']['tmp_name'][$key],
                        'error' => $this->request->files['file']['error'][$key],
                        'size' => $this->request->files['file']['size'][$key]
                    );
                }
            }

            /*// check max image uploaded
            $total = $this->model_custom_image->countImages();
            $max_images_upload = defined('MAX_IMAGES_UPLOAD') ? MAX_IMAGES_UPLOAD : 1000;
            if ($total + count($files) > $max_images_upload){
                $json['error'] = $this->language->get('warning_maximum_upload_all_images');
            }*/

            /* limit upload file per time */
            if (count($files) > 7) {
                $json['error'] = $this->language->get('warning_maximum_upload_images');
            }

            /* do upload file to image server */
            if (!$json) {
                $this->load->model('custom/common');
                $image_manager = Image_Server::getImageServer($this->getConfigImageServerUpload());

                foreach ($files as $file) {
                    if (is_file($file['tmp_name'])) {
                        // Sanitize the filename

                        // Allowed file mime types
                        $allowed = array(
                            'image/jpeg',
                            'image/pjpeg',
                            'image/png',
                            'image/x-png',
                            //'image/gif',
                            'image/x-icon'
                        );

                        if (!in_array($file['type'], $allowed)) {
                            $json['error'] = $this->language->get('error_filetype');
                        }

                        if ($file['size'] > MAX_IMAGE_SIZE_UPLOAD) {
                            $json['error'] = $this->language->get('error_filesize') . '. Max size of image is ' . ((int)MAX_IMAGE_SIZE_UPLOAD / 1024) . 'KB';
                        }

                        // Return any upload error
                        if ($file['error'] != UPLOAD_ERR_OK) {
                            $json['error'] = $this->language->get('error_upload_' . $file['error']);
                        }
                    } else {
                        $this->log->write(sprintf('upload image got error: %s does not exist or is not a valid file', $file['tmp_name']));
                        $json['error'] = $this->language->get('error_upload_default') . ((int)MAX_IMAGE_SIZE_UPLOAD / 1024) . 'KB';
                    }

                    if (!$json) {
                        // upload
                        $fileNameWithoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $file['name']);
                        // trans to slug
                        $fileNameWithoutExt = $this->model_custom_common->createSlug($fileNameWithoutExt, '-');
                        $data = [
                            'file' => $file,
                            'image' => $file['tmp_name'],
                            'filename' => $fileNameWithoutExt,
                            'directory' => $store_id,
                            'shop_name' => $this->config->get('shop_name')
                        ];

                        $result = $image_manager->upload($data);
                        if (array_key_exists('url', $result)) {
                            $new_image = [];
                            $new_image['source_id'] = isset($result['source_id']) ? $result['source_id'] : '';
                            $new_image['url'] = $result['url'];
                            $new_image['name'] = isset($result['name']) ? $result['name'] : '';
                            $new_image['source'] = isset($result['source']) ? $result['source'] : '';
                            $new_image['size'] = (int)$file['size'] / 1024;
                            $new_image['folder_path'] = isset($this->request->post['folder_path']) ? $this->request->post['folder_path'] : 0;
                            $this->model_custom_image->addImage($new_image);
                            $success = true;
                        } else {
                            $this->log->write(sprintf('upload image got error: %s', (isset($result['message']) ? $result['message'] : 'UNKNOWN')));
                        }
                    }
                }
            }
        }

        if ($success) {
            $json['success'] = $this->language->get('text_uploaded');
        }

        if (!$json) {
            $json['error'] = $this->language->get('error_upload_default') . ((int)MAX_IMAGE_SIZE_UPLOAD / 1024) . 'KB';
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function delete()
    {
        $this->load->language('common/filemanager');
        $this->load->model('custom/image');

        $json = array();

        // Check user has permission
        if (!$this->user->hasPermission('modify', 'common/filemanager')) {
            $json['error'] = $this->language->get('error_permission');
        }

        if (isset($this->request->post['image_ids'])) {
            $image_ids = $this->request->post['image_ids'];
        } else {
            $image_ids = array();
        }

        $this->model_custom_image->disableImages($image_ids);

        $json['success'] = $this->language->get('text_delete');

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function getStoreId()
    {
        // TODO
        return SHOP_ID;
    }


    ////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////// Manager images /////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////

    public function imageList()
    {
        $this->load->language('common/filemanager');

        $this->document->setTitle($this->language->get('heading_title_image_manager'));

        $this->load->model('custom/image');
        $this->load->model('tool/image');

        $this->getList();
    }

    protected function getList()
    {
        /* message */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        // get error message from session
        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];

            $this->session->data['error'] = '';
            unset($this->session->data['error']);
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $url = '';
        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;

        $filter = [];
        $filter['image_folder'] = isset($this->request->get['image_folder']) ? (int)$this->request->get['image_folder'] : 0;

        $filter['page'] = isset($this->request->get['page']) ? (int)$this->request->get['page'] : 1;
        $filter['start'] = ((int)$filter['page'] - 1) * self::MANAGER_LIST_IMAGE_LIMIT;
        $filter['limit'] = self::MANAGER_LIST_IMAGE_LIMIT;
        $filter['name'] = isset($this->request->get['filter_name']) ? $this->request->get['filter_name'] : '';

        $filter_status = null;
        if (isset($this->request->get['filter_status'])) {
            $filter_status = ($this->request->get['filter_status']);
            if (count($filter_status) > 1) {
                $filter_status = '';
            } else {
                $filter_status = (int)$filter_status[0];
            }
            $url .= '&filter_status=' . $filter_status;
        }
        if ($filter['image_folder'] != 0) {
            $url .= '&image_folder=' . $filter['image_folder'];
        }
        $filter['status'] = $filter_status;

        // folder
        $this->load->model('custom/image_folder');
        $folders = $this->model_custom_image_folder->getFolders($filter['image_folder']);
        $full_folder = [];
        if (!empty($folders)) {
            foreach ($folders as $folder) {
                $folder['path'] = $this->url->link('common/filemanager/imageList', 'user_token=' . $this->session->data['user_token'] . '&image_folder=' . $folder['id'], true);
                array_push($full_folder, $folder);
            }
        }
        $data['folders'] = $full_folder;
        $data['folder_root'] = $this->model_custom_image_folder->getFolders(0);
        $data['image_folder'] = $filter['image_folder'];
        $image_folder = $this->model_custom_image_folder->getParent($filter['image_folder']);
        $data['folder_now'] = $image_folder['folder_now'];
        $data['url_parent_folder'] = $this->url->link('common/filemanager/imageList', 'user_token=' . $this->session->data['user_token'] . '&image_folder=' . $image_folder['folder_parent'], true);

        $image_total = $this->model_custom_image->countImages($filter);
        $images = $this->model_custom_image->getImages($filter);
        $data['images'] = array();
        foreach ($images as $image) {
            if (!is_array($image) || !array_key_exists('url', $image)) {
                continue;
            }
            //$name = str_split(basename($imageName), 14);
            $data['images'][] = array(
                'public_id' => $image['source_id'],
                'thumb' => $this->model_tool_image->resize($image['url'], 250, 250),
                'name' => $image['name'],
                'path' => $image['url'],
                'image_id' => $image['image_id'],
                'status' => $image['status']
            );
        }

        // pagination
        $data['results'] = sprintf($this->language->get('text_pagination'), ($image_total) ? (($page - 1) * self::MANAGER_LIST_IMAGE_LIMIT) + 1 : 0, ((($page - 1) * self::MANAGER_LIST_IMAGE_LIMIT) > ($image_total - self::MANAGER_LIST_IMAGE_LIMIT)) ? $image_total : ((($page - 1) * self::MANAGER_LIST_IMAGE_LIMIT) + self::MANAGER_LIST_IMAGE_LIMIT), $image_total, ceil($image_total / self::MANAGER_LIST_IMAGE_LIMIT));
        $pagination = new CustomPaginate();
        $pagination->total = $image_total;
        $pagination->page = $page;
        $pagination->limit = self::MANAGER_LIST_IMAGE_LIMIT;
        $pagination->url = $this->url->link('common/filemanager/imageList', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        $data['pagination'] = $pagination->render();

        $data['url_filter'] = $this->url->link('common/filemanager/imageList', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['loadOptionOne'] = $this->replace_url($this->url->link('common/filemanager/loadoptionone', 'user_token=' . $this->session->data['user_token'] . $url, true));
        $data['upload_href'] = $this->url->link('common/filemanager/upload', 'user_token=' . $this->session->data['user_token'], true);
        $data['url_show_images'] = $this->replace_url($this->url->link('common/filemanager/showImages', 'user_token=' . $this->session->data['user_token'] . $url, true));
        $data['url_hide_images'] = $this->replace_url($this->url->link('common/filemanager/hideImages', 'user_token=' . $this->session->data['user_token'] . $url, true));
        $data['url_delete_images'] = $this->replace_url($this->url->link('common/filemanager/deleteImages', 'user_token=' . $this->session->data['user_token'] . $url, true));
        $data['url_add_folder'] = $this->replace_url($this->url->link('common/filemanager/addFolder', 'user_token=' . $this->session->data['user_token'] . $url, true));
        $data['url_edit_folder'] = $this->replace_url($this->url->link('common/filemanager/editFolder', 'user_token=' . $this->session->data['user_token'] . $url, true));
        $data['url_move_folders'] = $this->replace_url($this->url->link('common/filemanager/moveToFolder', 'user_token=' . $this->session->data['user_token'] . $url, true));
        $data['url_lazy_load_folders'] = $this->replace_url($this->url->link('common/filemanager/getFoldersLazyLoad', 'user_token=' . $this->session->data['user_token'] . $url, true));

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');
        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('common/image_list_append', $data));
        } else {
            $this->response->setOutput($this->load->view('common/image_list', $data));
        }
    }

    public function renderListFolders() {
        $this->load->language('common/filemanager');
        $parent = $this->request->get['parent_folder'];
        $this->load->model('custom/image_folder');
        $folders = $this->model_custom_image_folder->getFolderActive($parent);
        $parent_info = $this->model_custom_image_folder->getParent($parent);
        $data['parent'] = $parent_info;
        $data['folders'] = $folders;
        $this->response->setOutput($this->load->view('common/image_folder_move_append', $data));
    }

    public function showImages()
    {
        $json = [
            'status' => false
        ];

        $this->load->language('common/filemanager');
        if (!$this->user->hasPermission('modify', 'common/filemanager')) {
            $json['msg'] = $this->language->get('error_permission');
            $this->responseJson($json);
            return;
        }

        $this->load->model('custom/image_folder');

        // check if current parent folder is hidden
        if ($this->model_custom_image_folder->isHiddenFolder($this->request->post['image_folder'])) {
            $json['msg'] = $this->language->get('text_failure');
            $this->responseJson($json);
            return;
        }

        try {
            $this->load->model('custom/image');
            // get all descendants of all folders and images
            $ids = $this->getAllDescendantFoldersImages();

            $this->model_custom_image_folder->enableFolders($ids['folders']);
            $this->model_custom_image->enableImages($ids['images']);

            $json = [
                'status' => true,
                'msg' => $this->language->get('txt_success_show')
            ];
            $this->session->data['success'] = $this->language->get('txt_success_show');
        } catch (Exception $e) {
            $json['msg'] = $this->language->get('text_failure');
            $this->session->data['error'] = $this->language->get('text_failure');
        }

        $this->responseJson($json);
    }

    public function hideImages()
    {
        $json = [
            'status' => false
        ];

        $this->load->language('common/filemanager');
        if (!$this->user->hasPermission('modify', 'common/filemanager')) {
            $json['msg'] = $this->language->get('error_permission');
            $this->responseJson($json);
            return;
        }

        $this->load->model('custom/image_folder');

        // check if current parent folder is hidden
        if ($this->model_custom_image_folder->isHiddenFolder($this->request->post['image_folder'])) {
            $json['msg'] = $this->language->get('text_failure');
            $this->responseJson($json);
            return;
        }

        try {
            $this->load->model('custom/image');
            // get all descendants of all folders and images
            $ids = $this->getAllDescendantFoldersImages();

            $this->model_custom_image_folder->disableFolders($ids['folders']);
            $this->model_custom_image->disableImages($ids['images']);

            $json = [
                'status' => true,
                'msg' => $this->language->get('txt_success_hide')
            ];
            $this->session->data['success'] = $this->language->get('txt_success_hide');
        } catch (Exception $e) {
            $json['msg'] = $this->language->get('text_failure');
            $this->session->data['error'] = $this->language->get('text_failure');
        }

        $this->responseJson($json);
    }

    public function deleteImages()
    {
        $this->load->language('common/filemanager');

        if (!$this->user->hasPermission('modify', 'common/filemanager')) {
            $json = [
                'status' => false,
                'msg' => $this->language->get('error_permission')
            ];
            $this->responseJson($json);
            return;
        }

        $this->load->model('custom/image');
        $this->load->model('custom/image_folder');

        $json = [
            'status' => true
        ];

        try {
            // get all descendants of all folders and images
            $ids = $this->getAllDescendantFoldersImages();

            // delete images
            $result = $this->model_custom_image->getSourceIds($ids['images']);
            if (!is_array($result)) {
                return;
            }
            foreach ($result as $item) {
                if (!array_key_exists('source', $item) || !$item['source']) {
                    continue;
                }
                $image_manager = Image_Server::getImageServer(strtolower($item['source']));
                $image_manager->deleteImages([$item]);
            }
            $this->model_custom_image->deleteImages($ids['images']);

            // delete folders
            $this->model_custom_image_folder->deleteFolders($ids['folders']);

            $this->session->data['success'] = $this->language->get('txt_success_delete');
        } catch (Exception $e) {
            $json = [
                'status' => false,
                'msg' => 'text_failure'
            ];
            $this->session->data['error'] = $this->language->get('text_failure');
        }

        $this->responseJson($json);
    }

    public function moveToFolder()
    {
        $json = [
            'status' => false
        ];

        $this->load->language('common/filemanager');
        if (!$this->user->hasPermission('modify', 'common/filemanager')) {
            $json['msg'] = $this->language->get('error_permission');
            $this->responseJson($json);
            return;
        }

        if (!isset($this->request->post['move_folder'])) {
            $json['msg'] = $this->language->get('txt_not_choose_move_folder');
            $this->responseJson($json);
            return;
        }

        $this->load->model('custom/image');
        $this->load->model('custom/image_folder');

        $folder_ids = isset($this->request->post['folder_ids']) ? $this->request->post['folder_ids'] : [];
        $image_ids = isset($this->request->post['image_ids']) ? $this->request->post['image_ids'] : [];
        $move_folder = $this->request->post['move_folder'];

        try {
            if (!empty($folder_ids)) {
                // check if folder name exists in move-to-folder
                $folder_names = $this->model_custom_image_folder->getFolderNamesFromIds($folder_ids);
                if ($this->model_custom_image_folder->checkFolderNamesExists($folder_names, $move_folder)) {
                    $json['msg'] = $this->language->get('warning_exists_folder_name');
                    $this->responseJson($json);
                    return;
                }

                $this->model_custom_image_folder->moveFolders($move_folder, $folder_ids);
            }

            if (!empty($image_ids)) {
                $this->model_custom_image->moveImages($move_folder, $image_ids);
            }

            $this->session->data['success'] = $this->language->get('txt_success_move');
            $json['status'] = true;
        } catch (Exception $e) {
            $this->session->data['error'] = $this->language->get('text_failure');
            $json['msg'] = $this->language->get('text_failure');
        }

        $this->responseJson($json);
    }

    private function replace_url($url)
    {
        return str_replace("amp;", "", $url);
    }

    public function loadoptionone()
    {
        $this->load->language('common/filemanager');
        $categoryID = $this->request->post['categoryID'];
        $end_select_html = '</select></div>';
        $show_html = '';

        if ($categoryID > 0) {
            if ($categoryID == 1) { // Trang thai hien thi
                $show_html .= '<div class="form-group"><select class="form-control select2 form-control-sm" data-placeholder="' . $this->language->get('filter_status_placeholder') . '" data-label="' . $this->language->get('filter_status') . '" id="status_image" name="status_image">';
                $show_html .= '<option value="">' . $this->language->get('filter_status') . '</option>';
                $show_html .= '<option value="1" data-value="' . $this->language->get('entry_status_publish') . '">' . $this->language->get('entry_status_publish') . '</option>';
                $show_html .= '<option value="0" data-value="' . $this->language->get('entry_status_unpublish') . '">' . $this->language->get('entry_status_unpublish') . '</option>';
                $show_html .= $end_select_html;
            }
        } else {
            $show_html .= '<style>#optionone,#optiontwo{display:none !important}</style>';
        }

        echo $show_html .= '';
    }

    private function getConfigImageServerUpload()
    {
        return defined('IMAGE_SERVER_UPLOAD') && in_array(IMAGE_SERVER_UPLOAD, self::SUPPORTED_IMAGE_SERVER_UPLOAD)
            ? IMAGE_SERVER_UPLOAD
            : self::IMAGE_SERVER_UPLOAD_DEFAULT; // fallback to default
    }

    public function addFolder()
    {
        $this->load->language('common/filemanager');
        $this->load->model('custom/image_folder');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateAddFolderForm()) {
            if ($this->model_custom_image_folder->isHiddenFolder($this->request->post['parent_id'])) {
                $this->session->data['error'] = $this->language->get('text_failure');
            } else {
                $this->model_custom_image_folder->addFolder($this->request->post);
                $this->session->data['success'] = $this->language->get('txt_success_add_folder');
            }
        }

        // add parent_id to url
        $parent_id = isset($this->request->post['parent_id']) ? (int)$this->request->post['parent_id'] : 0;
        $image_folder_query = $parent_id ? '&image_folder=' . $parent_id : '';

        $this->response->redirect($this->url->link('common/filemanager/imageList', 'user_token=' . $this->session->data['user_token'] . $image_folder_query, true));
    }

    public function editFolder()
    {
        $this->load->language('common/filemanager');
        $this->load->model('custom/image_folder');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateEditFolderForm()) {
            if ($this->model_custom_image_folder->isHiddenFolder($this->request->post['parent_id'])) {
                $this->session->data['error'] = $this->language->get('text_failure');
            } else {
                $this->model_custom_image_folder->editFolder($this->request->post);
                $this->session->data['success'] = $this->language->get('txt_success_edit_folder');
            }
        }

        // add parent_id to url
        $parent_id = isset($this->request->post['parent_id']) ? (int)$this->request->post['parent_id'] : 0;
        $image_folder_query = $parent_id ? '&image_folder=' . $parent_id : '';

        $this->response->redirect($this->url->link('common/filemanager/imageList', 'user_token=' . $this->session->data['user_token'] . $image_folder_query, true));
    }

    private function validateAddFolderForm()
    {
        if (!(isset($this->request->post['name']) && '' != trim($this->request->post['name']))) {
            $this->session->data['error'] = $this->language->get('warning_missing_folder_name');
        } elseif ($this->model_custom_image_folder->checkFolderNamesExists([$this->request->post['name']], $this->request->post['parent_id'])) {
            $this->session->data['error'] = $this->language->get('warning_exists_folder_name');
        }

        return !isset($this->session->data['error']) || !$this->session->data['error'];
    }

    private function validateEditFolderForm()
    {
        if (!(isset($this->request->post['name']) && '' != trim($this->request->post['name']))) {
            $this->session->data['error'] = $this->language->get('warning_missing_folder_name');
        } elseif ($this->model_custom_image_folder->checkFolderNamesExists([$this->request->post['name']], $this->request->post['parent_id'], $this->request->post['id'])) {
            $this->session->data['error'] = $this->language->get('warning_exists_folder_name');
        }

        return !isset($this->session->data['error']) || !$this->session->data['error'];
    }

    private function getAllDescendantFoldersImages()
    {
        $folder_ids = isset($this->request->post['folder_ids']) ? $this->request->post['folder_ids'] : [];
        $image_ids = isset($this->request->post['image_ids']) ? $this->request->post['image_ids'] : [];

        $all_folders = $folder_ids;
        foreach ($folder_ids as $folder_id) {
            $all_folders = array_merge($all_folders, $this->model_custom_image_folder->getDescendantFolders($folder_id));
        }

        // get all images in all folders
        $descendant_images = $this->model_custom_image->getImagesByFolders($all_folders);
        $all_images = array_merge($image_ids, $descendant_images);

        return [
            'folders' => $all_folders,
            'images' => $all_images
        ];
    }

    public function getFoldersLazyLoad()
    {
        $json = [
            'results' => [],
            'count' => 0
        ];
        $this->load->model('custom/image_folder');
        $page = isset($this->request->post['page']) ? $this->request->post['page'] : 1;
        $folder_ids = isset($this->request->post['folder_ids']) ? $this->request->post['folder_ids'] : [];
        //display 6 folder
        $filter_data = array(
            'limit' => 6,
            'start' => ($page - 1) * 6,
            'need_to_move_folders' => $folder_ids
        );

        $is_contain_root = true;
        if (empty($folder_ids)) {
            $filter_data['need_to_move_images'] = isset($this->request->post['image_ids']) ? $this->request->post['image_ids'] : [];
        } else {
            /* check if root folder not contain same name folders with need-to-move-folders */
            // get all need-to-move-folder-names from their ids
            $folder_names = $this->model_custom_image_folder->getFolderNamesFromIds($folder_ids);
            if ($this->model_custom_image_folder->checkFolderNamesExists($folder_names, self::ROOT_FOLDER_ID)) {
                $is_contain_root = false;
            }
        }
        $this->load->language('common/filemanager');
        if ($is_contain_root) {
            $json['results'][] = array(
                'text' => $this->language->get('root_folder_name'),
                'id' => self::ROOT_FOLDER_ID
            );
        }

        if (isset($this->request->post['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->post['filter_name']);
        }
        $results = $this->model_custom_image_folder->getFoldersPaginator($filter_data);

        foreach ($results as $value) {
            $json['results'][] = array(
                'text' => $value['name'],
                'id' => $value['id']
            );
        }
        $json['count'] = $this->model_custom_image_folder->getTotalFolders($filter_data);

        $this->responseJson($json);
    }
}