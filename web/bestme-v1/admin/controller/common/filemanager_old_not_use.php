<?php

require_once 'cloudinary_upload.php';

class ControllerCommonFileManager extends Controller
{

    public function index()
    {
        $this->load->language('common/filemanager');
        $single_select_param ='';
        if (isset($this->request->get['single']) && $this->request->get['single']){
            $single_select_param = '&single=true';
        }
        $cloudinary = new Cloudinary_Upload();
        $store_id = $this->getStoreId();  /// dictory

        if (isset($this->request->get['filter_name'])) {
            $filter_name = rtrim(str_replace(array('*', '/', '\\'), '', $this->request->get['filter_name']), '/');
        } else {
            $filter_name = '';
        }

        // Make sure we have the correct directory
        if (isset($this->request->get['directory'])) {
            $directory = str_replace(' ', '', $this->request->get['directory']);
            $directory = strlen($directory) != 0 ? $store_id . '/' . $directory : $store_id;
        } else {
            $directory = $store_id;
        }
        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $directories = array();
        $files = array();

        $data['images'] = array();
        if (strlen($filter_name) != 0) {
            //// rewirte after
            $images = $cloudinary->searchImages($filter_name . '*', $directory);
        } else {
            //$this->load->model('tool/image');
            $arrPath = explode('/', $directory);
            $rootPath = reset($arrPath);
            if ($rootPath == $store_id) {
                // Get directories
//                $directories = $cloudinary->getSubFolder($directory);
//                if ($directories == null) {
//                    $cloudinary->createEmptyFolder($directory);
//                    $directories = $cloudinary->getSubFolder($directory);
//                }
//                if (!$directories) {
//                    $directories = array();
//                }

                // Get files
                $files = $cloudinary->getImages($directory);
                $files = $files['images'];

                if (!$files) {
                    $files = array();
                }
            }

            // Merge directories and files
            $images = $files;
        }

        // Get total number of files and directories
        $image_total = count($images);

        // Split the array based on current page number and max number of items per page of 10
        $images = array_splice($images, ($page - 1) * 16, 16);

        foreach ($images as $image) {
            if (array_key_exists('path', $image) && array_key_exists('name', $image)) {
                $name = str_split(basename($image['name']), 14);
                $url = '';

                if (isset($this->request->get['target'])) {
                    $url .= '&target=' . $this->request->get['target'];
                }

                if (isset($this->request->get['thumb'])) {
                    $url .= '&thumb=' . $this->request->get['thumb'];
                }
                $path = explode('/', $image['path']);
                array_shift($path);
                $path = implode('/', $path);
                $data['images'][] = array(
                    'thumb' => '',
                    'name' => implode(' ', $name),
                    'type' => 'directory',
                    'path' => $path,
                    'href' => $this->url->link('common/filemanager', 'user_token=' . $this->session->data['user_token'] .$single_select_param. '&directory=' . $path . $url, true)
                );
            } elseif
            (array_key_exists('public_id', $image) && array_key_exists('format', $image) && array_key_exists('url', $image)) {
                $imageId = $imageName = explode('/', $image['public_id']);
                $imageName = is_array($imageName) ? end($imageName) : 'image_name';
                $image_id = end($imageId);
                $image_id = preg_replace('/[^A-Za-z0-9-]+/', '-', $image_id);

                $name = str_split(basename($imageName), 14);
                $data['images'][] = array(
                    'thumb' => $image['url'],//$this->model_tool_image->resize($image['url'], 100, 100), // $image['url']
                    'name' => implode(' ', $name),
                    'type' => 'image',
                    'path' => $image['url'], // $image['public_id']
                    'href' => $image['url'],
                    'image_id' => $image_id
                );
            }
        }
        $data['user_token'] = $this->session->data['user_token'];

        if (isset($this->request->get['directory'])) {
            $data['directory'] = urlencode($this->request->get['directory']);
        } else {
            $data['directory'] = '';
        }

        if (isset($this->request->get['filter_name'])) {
            $data['filter_name'] = $this->request->get['filter_name'];
        } else {
            $data['filter_name'] = '';
        }

        // Return the target ID for the file manager to set the value
        if (isset($this->request->get['target'])) {
            $data['target'] = $this->request->get['target'];
        } else {
            $data['target'] = '';
        }

        // Return the thumbnail for the file manager to show a thumbnail
        if (isset($this->request->get['thumb'])) {
            $data['thumb'] = $this->request->get['thumb'];
        } else {
            $data['thumb'] = '';
        }

        // Parent
        $url = '';

        if (isset($this->request->get['directory'])) {
            $pos = strrpos($this->request->get['directory'], '/');

            if ($pos) {
                $url .= '&directory=' . urlencode(substr($this->request->get['directory'], 0, $pos));
            }
        }

        if (isset($this->request->get['target'])) {
            $url .= '&target=' . $this->request->get['target'];
        }

        if (isset($this->request->get['thumb'])) {
            $url .= '&thumb=' . $this->request->get['thumb'];
        }

        $data['parent'] = $this->url->link('common/filemanager', 'user_token=' . $this->session->data['user_token'] . $url . $single_select_param, true);

        // Refresh
        $url = '';

        if (isset($this->request->get['directory'])) {
            $url .= '&directory=' . urlencode($this->request->get['directory']);
        }

        if (isset($this->request->get['target'])) {
            $url .= '&target=' . $this->request->get['target'];
        }

        if (isset($this->request->get['thumb'])) {
            $url .= '&thumb=' . $this->request->get['thumb'];
        }

        $data['refresh'] = $this->url->link('common/filemanager', 'user_token=' . $this->session->data['user_token'] . $url . $single_select_param, true);

        $url = '';

        if (isset($this->request->get['directory'])) {
            $url .= '&directory=' . urlencode(html_entity_decode($this->request->get['directory'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['target'])) {
            $url .= '&target=' . $this->request->get['target'];
        }

        if (isset($this->request->get['thumb'])) {
            $url .= '&thumb=' . $this->request->get['thumb'];
        }

        $pagination = new CustomPaginate();
        $pagination->total = $image_total;
        $pagination->page = $page;
        $pagination->limit = 16;
        $pagination->url = $this->url->link('common/filemanager', 'user_token=' . $this->session->data['user_token'] . $url . $single_select_param . '&page={page}', true);

        $data['pagination'] = $pagination->render();
        if (isset($this->request->get['single']) && $this->request->get['single']){
            $this->response->setOutput($this->load->view('common/filemanager_single', $data));
        }else{
            $this->response->setOutput($this->load->view('common/filemanager', $data));
        }
    }

    public function origin()
    {
        $this->load->language('common/filemanager');
        $cloudinary = new Cloudinary_Upload();
        $store_id = $this->getStoreId();  /// dictory

        if (isset($this->request->get['filter_name'])) {
            $filter_name = rtrim(str_replace(array('*', '/', '\\'), '', $this->request->get['filter_name']), '/');
        } else {
            $filter_name = '';
        }

        // Make sure we have the correct directory
        if (isset($this->request->get['directory'])) {
            $directory = str_replace(' ', '', $this->request->get['directory']);
            $directory = strlen($directory) != 0 ? $store_id . '/' . $directory : $store_id;
        } else {
            $directory = $store_id;
        }
        //var_dump($directory2);
        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $directories = array();
        $files = array();

        $data['images'] = array();
        if (strlen($filter_name) != 0) {
            //// rewirte after
            $images = $cloudinary->searchImages($filter_name . '*', $directory);
        } else {
            $this->load->model('tool/image');
            $arrPath = explode('/', $directory);
            $rootPath = reset($arrPath);
            if ($rootPath == $store_id) {
                // Get directories
                $directories = $cloudinary->getSubFolder($directory);
                if ($directories == null) {
                    $cloudinary->createEmptyFolder($directory);
                    $directories = $cloudinary->getSubFolder($directory);
                }
                if (!$directories) {
                    $directories = array();
                }

                // Get files
                $files = $cloudinary->getImages($directory);
                $files = $files['images'];

                if (!$files) {
                    $files = array();
                }
            }

            // Merge directories and files
            $images = array_merge($directories, $files);
        }


        // Get total number of files and directories
        $image_total = count($images);

        // Split the array based on current page number and max number of items per page of 10
        $images = array_splice($images, ($page - 1) * 16, 16);

        foreach ($images as $image) {

            if (array_key_exists('path', $image) && array_key_exists('name', $image)) {
                $name = str_split(basename($image['name']), 14);
                $url = '';

                if (isset($this->request->get['target'])) {
                    $url .= '&target=' . $this->request->get['target'];
                }

                if (isset($this->request->get['thumb'])) {
                    $url .= '&thumb=' . $this->request->get['thumb'];
                }
                $path = explode('/', $image['path']);
                array_shift($path);
                $path = implode('/', $path);
                $data['images'][] = array(
                    'thumb' => '',
                    'name' => implode(' ', $name),
                    'type' => 'directory',
                    'path' => $path,
                    'href' => $this->url->link('common/filemanager/origin', 'user_token=' . $this->session->data['user_token'] . '&directory=' . $path . $url, true)
                );
            } elseif
            (array_key_exists('public_id', $image) && array_key_exists('format', $image) && array_key_exists('url', $image)) {
                $imageName = explode('/', $image['public_id']);
                $imageName = is_array($imageName) ? end($imageName) : 'image_name';
                $name = str_split(basename($imageName), 14);
                $data['images'][] = array(
                    'thumb' => $image['url'], //$this->model_tool_image->resize($image['url'], 100, 100),
                    'name' => implode(' ', $name),
                    'type' => 'image',
                    'path' => $image['url'], // $image['public_id']
                    'href' => $image['url']
                );
            }
        }
        $data['user_token'] = $this->session->data['user_token'];

        if (isset($this->request->get['directory'])) {
            $data['directory'] = urlencode($this->request->get['directory']);
        } else {
            $data['directory'] = '';
        }

        if (isset($this->request->get['filter_name'])) {
            $data['filter_name'] = $this->request->get['filter_name'];
        } else {
            $data['filter_name'] = '';
        }

        // Return the target ID for the file manager to set the value
        if (isset($this->request->get['target'])) {
            $data['target'] = $this->request->get['target'];
        } else {
            $data['target'] = '';
        }

        // Return the thumbnail for the file manager to show a thumbnail
        if (isset($this->request->get['thumb'])) {
            $data['thumb'] = $this->request->get['thumb'];
        } else {
            $data['thumb'] = '';
        }

        // Parent
        $url = '';

        if (isset($this->request->get['directory'])) {
            $pos = strrpos($this->request->get['directory'], '/');

            if ($pos) {
                $url .= '&directory=' . urlencode(substr($this->request->get['directory'], 0, $pos));
            }
        }

        if (isset($this->request->get['target'])) {
            $url .= '&target=' . $this->request->get['target'];
        }

        if (isset($this->request->get['thumb'])) {
            $url .= '&thumb=' . $this->request->get['thumb'];
        }

        $data['parent'] = $this->url->link('common/filemanager/origin', 'user_token=' . $this->session->data['user_token'] . $url, true);

        // Refresh
        $url = '';

        if (isset($this->request->get['directory'])) {
            $url .= '&directory=' . urlencode($this->request->get['directory']);
        }

        if (isset($this->request->get['target'])) {
            $url .= '&target=' . $this->request->get['target'];
        }

        if (isset($this->request->get['thumb'])) {
            $url .= '&thumb=' . $this->request->get['thumb'];
        }

        $data['refresh'] = $this->url->link('common/filemanager/origin', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $url = '';

        if (isset($this->request->get['directory'])) {
            $url .= '&directory=' . urlencode(html_entity_decode($this->request->get['directory'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['target'])) {
            $url .= '&target=' . $this->request->get['target'];
        }

        if (isset($this->request->get['thumb'])) {
            $url .= '&thumb=' . $this->request->get['thumb'];
        }

        $pagination = new CustomPaginate();
        $pagination->total = $image_total;
        $pagination->page = $page;
        $pagination->limit = 16;
        $pagination->url = $this->url->link('common/filemanager/origin', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $this->response->setOutput($this->load->view('common/filemanager_origin', $data));
    }

    public function upload()
    {
        $this->load->language('common/filemanager');

        $json = array();

        // Check user has permission
        if (!$this->user->hasPermission('modify', 'common/filemanager')) {
            $json['error'] = $this->language->get('error_permission');
        }
        $cloudinary = new Cloudinary_Upload();
        $store_id = $this->getStoreId();  /// dictory

        // Make sure we have the correct directory
        if (isset($this->request->get['directory'])) {
            $directory = str_replace(' ', '', $this->request->get['directory']);
            $directory = strlen($directory) != 0 ? $store_id . '/' . $directory : $store_id;
        } else {
            $directory = $store_id;
        }

        // Check its a directory
        $arrPath = explode('/', $directory);
        $rootPath = reset($arrPath);
        if ($rootPath != $store_id) {
            $json['error'] = $this->language->get('error_directory');
        }

        if (!$json) {
            // Check if multiple files are uploaded or just one
            $files = array();

            if (!empty($this->request->files['file']['name']) && is_array($this->request->files['file']['name'])) {
                foreach (array_keys($this->request->files['file']['name']) as $key) {
                    $files[] = array(
                        'name' => $this->request->files['file']['name'][$key],
                        'type' => $this->request->files['file']['type'][$key],
                        'tmp_name' => $this->request->files['file']['tmp_name'][$key],
                        'error' => $this->request->files['file']['error'][$key],
                        'size' => $this->request->files['file']['size'][$key]
                    );
                }
            }

            foreach ($files as $file) {
                if (is_file($file['tmp_name'])) {
                    // Sanitize the filename
                    $filename = basename(html_entity_decode($file['name'], ENT_QUOTES, 'UTF-8'));

                    // Validate the filename length
                    if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 255)) {
                        $json['error'] = $this->language->get('error_filename');
                    }

                    // Allowed file extension types
                    $allowed = array(
                        'jpg',
                        'jpeg',
                        'gif',
                        'png',
                        'ico'
                    );
                    if (!in_array(utf8_strtolower(utf8_substr(strrchr($filename, '.'), 1)), $allowed)) {
                        $json['error'] = $this->language->get('error_filetype');
                    }

                    // Allowed file mime types
                    $allowed = array(
                        'image/jpeg',
                        'image/pjpeg',
                        'image/png',
                        'image/x-png',
                        'image/gif',
                        'image/x-icon'
                    );

                    if (!in_array($file['type'], $allowed)) {
                        $json['error'] = $this->language->get('error_filetype');
                    }

                    if ($file['size'] > MAX_IMAGE_SIZE_UPLOAD) {
                        $json['error'] = $this->language->get('error_filesize') . '. Max size of image is ' . ((int)MAX_IMAGE_SIZE_UPLOAD / 1024) . 'KB';
                    }

                    // Return any upload error
                    if ($file['error'] != UPLOAD_ERR_OK) {
                        $json['error'] = $this->language->get('error_upload_' . $file['error']);
                    }
                } else {
                    $json['error'] = $this->language->get('error_upload');
                }

                if (!$json) {
                    $fileNameWithoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $file['name']);
                    // trans to slug
                    $this->load->model('custom/common');
                    $fileNameWithoutExt = $this->model_custom_common->createSlug($fileNameWithoutExt, '-');

                    //upload
                    $cloudinary->upload($file['tmp_name'], $fileNameWithoutExt, $directory);
//                    $this->load->model('tool/image');
//                    $image = $this->model_tool_image->uploadThumbnailImage($file['tmp_name'], $file['name'], 600, 600);
//                    if ($image) {
//                        $cloudinary->uploadThumbnail($image, $fileNameWithoutExt, $directory, '600x600');
//                        unlink($image);
//                    }
                }
            }
        }

        if (!$json) {
            $json['success'] = $this->language->get('text_uploaded');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function folder()
    {
        $this->load->language('common/filemanager');

        $json = array();

        // Check user has permission
        if (!$this->user->hasPermission('modify', 'common/filemanager')) {
            $json['error'] = $this->language->get('error_permission');
        }

        $cloudinary = new Cloudinary_Upload();
        $store_id = $this->getStoreId();  /// dictory
        // Make sure we have the correct directory
        if (isset($this->request->get['directory'])) {
            $directory = str_replace(' ', '', $this->request->get['directory']);
            $directory = strlen($directory) != 0 ? $store_id . '/' . $directory : $store_id;
        } else {
            $directory = $store_id;
        }

        // Check its a directory
        $arrPath = explode('/', $directory);
        $rootPath = reset($arrPath);
        if ($rootPath != $store_id) {
            $json['error'] = $this->language->get('error_directory');
        }

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            // Sanitize the folder name
            $folder = basename(html_entity_decode($this->request->post['folder'], ENT_QUOTES, 'UTF-8'));
            $folder = str_replace(' ', '_', $folder);

            // Validate the filename length
            if ((utf8_strlen($folder) < 1) || (utf8_strlen($folder) > 255)) {
                $json['error'] = $this->language->get('error_folder');
            }

            // Check if directory already exists or not
            if (is_dir($directory . '/' . $folder)) {
                $json['error'] = $this->language->get('error_exists');
            }
        }

        if (!isset($json['error'])) {
            $cloudinary->createEmptyFolder($directory . '/' . $folder);

            $json['success'] = $this->language->get('text_directory');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function delete()
    {
        $this->load->language('common/filemanager');

        $json = array();

        // Check user has permission
        if (!$this->user->hasPermission('modify', 'common/filemanager')) {
            $json['error'] = $this->language->get('error_permission');
        }

        $cloudinary = new Cloudinary_Upload();
        $store_id = $this->getStoreId();  /// dictory

        if (isset($this->request->post['path'])) {
            $paths = $this->request->post['path'];
        } else {
            $paths = array();
        }

        $deleted = $cloudinary->deleteImages($paths);
        foreach ($deleted['deleted'] as $key => $value) {
            if ($value != 'deleted') {
                $cloudinary->deleteAllImagesByFolder($store_id . '/' . $key);
                $cloudinary->deleteEmptyFolder($store_id . '/' . $key);
            }
        }
        ob_clean();

        $json['success'] = $this->language->get('text_delete');

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function getStoreId()
    {
        // TODO
        return SHOP_ID;
    }
}