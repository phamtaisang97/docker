<?php
use App_Api\App_Config;
class ControllerCommonAppConfig extends Controller {
    public function setTheme($config = []){
        $this->load->language('common/app_config');
        $this->load->model('setting/extension');
        if ($this->request->server['REQUEST_METHOD'] == 'POST'){
            $app = $this->request->post['app'];
            $module_id = $this->request->post['module_id'];
            $config = new App_Config($this->registry);
            $config->addConfigTheme($module_id, $this->request->post);
            $redirect = $this->url->link('extension/appstore/'. $app,'user_token=' . $this->session->data['user_token'],true);
            $this->response->redirect($redirect);
        }
        $current_theme = $this->config->get('config_theme');

        $this->load->model('setting/setting');
        $theme_directory = $this->model_setting_setting->getSettingValue('theme_' . $current_theme . '_directory');

        $file_path = DIR_CATALOG . 'view/theme/' . $theme_directory . '/asset/default_config/default.json';
        $defaultConfig = null;
        if (file_exists($file_path) && is_readable($file_path)) {
            $default_config = file_get_contents($file_path);
            if ($default_config) {
                $defaultConfig = json_decode($default_config, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG);
            }
        }
        $page_config = null;
        if($defaultConfig['app_config_theme']){
            $page_config = $defaultConfig['app_config_theme'];
        }
        $data['page_config'] = $page_config;
        $data['current_theme'] = $current_theme;
        $data['app'] = $config['app'];
        $data['module_id'] = $config['module_id'];
        $data['action'] = $this->url->link('common/app_config/setTheme','user_token=' . $this->session->data['user_token'],true);
        return $this->load->view('common/app_config',$data);
    }
    public function getPositionOfPage(){

        $page = $this->request->get['page'];
        $current_theme = $this->config->get('config_theme');

        $this->load->model('setting/setting');
        $theme_directory = $this->model_setting_setting->getSettingValue('theme_' . $current_theme . '_directory');

        $file_path = DIR_CATALOG . 'view/theme/' . $theme_directory . '/asset/default_config/default.json';
        $defaultConfig = null;
        if (file_exists($file_path) && is_readable($file_path)) {
            $default_config = file_get_contents($file_path);
            if ($default_config) {
                $defaultConfig = json_decode($default_config, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG);
            }
        }
        $page_config = null;
        if($defaultConfig['app_config_theme']){
            $page_config = $defaultConfig['app_config_theme'];
        }
        $positions = null;
        foreach($page_config as $page_one){
            if($page_one['value'] == $page){
                $positions = $page_one['position'];
            }
        }
        $data['positions'] = $positions;
        $this->response->setOutput($this->load->view('common/get_position_of_page',$data));
    }
}
