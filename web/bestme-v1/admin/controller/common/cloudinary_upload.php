<?php
/**
 * Created by PhpStorm.
 * User: novaon888 - tiennd
 * Date: 10/24/2018
 * Time: 10:08 AM
 */


class CloudinaryUpload
{
    private $cloud_name;
    private $api_key;
    private $api_secret;

    /**
     * CloudinaryUpload constructor.
     * @param string $cloud_name
     * @param string $api_key
     * @param string $api_secret
     */
    public function __construct($cloud_name = CLOUD_NAME, $api_key = API_KEY, $api_secret = API_SECRET)
    {
        $this->cloud_name = $cloud_name;
        $this->api_key = $api_key;
        $this->api_secret = $api_secret;

        Cloudinary::config(array(
            "cloud_name" => $cloud_name,
            "api_key" => $api_key,
            "api_secret" => $api_secret
        ));
    }

    /**
     * @param string $image
     * @param string $filename
     * @param string $directory
     * @return mixed
     */
    public function upload($image, $filename, $directory)
    {
        $option = array(
            "folder" => $directory,
            "public_id" => $filename,
            "use_filename" => FALSE,
            "unique_filename" => TRUE,
            "overwrite" => false,
            "tags" => $directory,
            "context" => $filename
        );
        $result = \Cloudinary\Uploader::upload($image, $option);

        if (array_key_exists('existing', $result) && $result['existing']){
            $date = new DateTime();
            $time_stamp =  $date->getTimestamp();
            $filename = $filename . '(' . $time_stamp . ')';
            $option = array(
                "folder" => $directory,
                "public_id" => $filename,
                "use_filename" => FALSE,
                "unique_filename" => TRUE,
                "overwrite" => false,
                "tags" => $directory,
                "context" => $filename
            );
            $result = \Cloudinary\Uploader::upload($image, $option);
        }

        return $result;
    }

    /**
     * @param string $image
     * @param string $filename
     * @param string $directory
     * @param string $size
     * @return mixed
     */
    public function uploadThumbnail($image, $filename, $directory, $size)
    {
        $option = array(
            "folder" => $directory . '/' . $size,
            "public_id" => $filename,
            "use_filename" => FALSE,
            "unique_filename" => TRUE,
            "tags" => $directory . '/' . $size
        );
        $result = \Cloudinary\Uploader::upload($image, $option);

        return $result;
    }

    /**
     * @param $directory
     * @param int $limit
     * @param string $next_cursor
     * @return mixed
     */
    public function getImages($directory, $limit = 500, $next_cursor = null)  // limit max = 500
    {
        $api = new \Cloudinary\Api();
        if ($next_cursor != null) {
            $options = array("type" => "upload", "prefix" => $directory, "max_results" => $limit, "next_cursor" => $next_cursor);
        } else {
            $options = array("type" => "upload", "resource_type" => "image", "prefix" => $directory, "max_results" => $limit);
        }
        try {
            //$images = $api->resources($options);
            $images = $api->resources_by_tag($directory, $options);
        } catch (Exception $e) {
            return array('images' => null);
        }  // suggest: use tag to get images by current folder
        $result = array(
            'images' => $images['resources']
        );
        if (!empty($images) && array_key_exists("next_cursor", $images)) {
            $result['next_cursor'] = $images["next_cursor"];
        }

        return $result;
    }


    /**
     * @param $directory
     * @return mixed|null
     */
    public function getSubFolder($directory)
    {   // root_folder,sub_folder
        $api = new \Cloudinary\Api();
        try {
            $result = $api->subfolders($directory);
        } catch (Exception $e) {
            return null;
        }
        $folders = $result['folders'];
        foreach ($folders as $key => $value) {
            if ($value['name'] == '600x600') {
                unset($folders[$key]);
            }
        }

        return $folders;
    }

    public function getImageTag($public_id, $options)
    {  // eg: $options = ['alt' => 'test', 'width' => 100, 'height' => 150]
        return cl_image_tag($public_id, $options);
    }

    public function searchImages($expression, $directory)
    {
        $searchApi = new \Cloudinary\Search();
        $expression = $expression . ' AND resource_type:image AND tags = ' . $directory;
        $result = $searchApi->expression($expression)->with_field('context')->execute();

        $result = array_key_exists('resources', $result) ? $result['resources'] : [];
        foreach ($result as $key => $value) {
            if (strpos($value['public_id'], '/600x600/') !== false) {
                unset($result[$key]);
            }
        }
        
        return $result;
    }

    /**
     * @param $public_ids
     * @return array|\Cloudinary\Api\Response
     */
    public function deleteImages($public_ids)
    {
        $api = new \Cloudinary\Api();  /// add delete thumbnail
        try {
            $theme_ids = array_map(function ($url) {
                $url = preg_replace('/\\.[^.\\s]{3,4}$/', '', $url);
                $arr = explode('/', $url);
                $pos = array_search('upload', $arr) == 0 ? 0 : array_search('upload', $arr) + 2;
                $arr = array_slice($arr, $pos);
                array_splice($arr, count($arr) - 1, 0, '600x600');
                $public_id = implode('/', $arr);
                $public_id = urldecode($public_id);

                return $public_id;
            }, $public_ids);
            $api->delete_resources($theme_ids);

            $public_ids = array_map(function ($url) {
                $url = preg_replace('/\\.[^.\\s]{3,4}$/', '', $url);
                $arr = explode('/', $url);
                $pos = array_search('upload', $arr) == 0 ? 0 : array_search('upload', $arr) + 2;
                $arr = array_slice($arr, $pos);
                $public_id = implode('/', $arr);
                $public_id = urldecode($public_id);

                return $public_id;
            }, $public_ids);
            $result = $api->delete_resources($public_ids);
        } catch (Exception $e) {
            return [];
        }   // delete single images : \Cloudinary\Uploader::destroy('public_id');

        return $result;
    }

    /**
     * @param $directory
     * @return \Cloudinary\Api\Response
     * @throws \Cloudinary\Api\GeneralError
     */
    public function deleteAllImagesByFolder($directory)
    {
        $api = new \Cloudinary\Api();
        $result = $api->delete_resources_by_prefix($directory);

        return $result;
    }

    /**
     * @param $directory
     * @return \Cloudinary\Api\Response
     * @throws \Cloudinary\Api\GeneralError
     */
    public function deleteImagesByFolder($directory)
    {
        $api = new \Cloudinary\Api();
        $result = $api->delete_resources_by_tag($directory); // set tag = directory

        return $result;
    }

    /**
     * @return \Cloudinary\Api\Response
     * @throws \Cloudinary\Api\GeneralError
     */
    public function deleteAllResource()     //// careful
    {
//        $api = new \Cloudinary\Api();
//        $result = $api->delete_all_resources();
//
//        return $result;
    }

    /**
     * @param $directory
     * @return mixed
     */
    public function createEmptyFolder($directory)
    {
        $option = array(
            "folder" => $directory,
            'public_id' => 'profile',
            "use_filename" => FALSE,
            "unique_filename" => TRUE,
            "tags" => $directory
        );

        $image = DIR_IMAGE . '../profile.png';
        $result = \Cloudinary\Uploader::upload($image, $option);

        return $result;
    }

    /**
     * @param $directory
     * @return \Cloudinary\Api\Response
     * @throws \Cloudinary\Api\GeneralError
     */
    public function deleteEmptyFolder($directory)
    {
        $ch = curl_init('https://api.cloudinary.com/v1_1/' . $this->cloud_name . '/folders/' . $directory);
        curl_setopt($ch, CURLOPT_USERPWD, $this->api_key . ':' . $this->api_secret);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

        $result = curl_exec($ch);
        curl_close($ch);
        echo $result;
    }


    /**
     * @param $public_id
     * @param $version
     * @return string
     */
    public function verifySignatures($public_id, $version)
    {
        return Cloudinary::api_sign_request(array("public_id" => $public_id, "version" => $version), $this->api_secret);
    }
}