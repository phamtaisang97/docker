<?php

class ControllerCommonBestmeUseTheme extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('custom/theme');
        $json = array();
        $json['success'] = false;
        $json['code'] = 101;
        $json['msg'] = $this->language->get('error_change_theme');

        if (isset($_GET['user_token'])){
            $this->load->model('user/user');
            $user_id = $this->model_user_user->checkTokenSession($_GET['user_token']);
            if ($user_id){
                $this->session->data['user_id'] = $user_id;
                $this->registry->set('user', new Cart\User($this->registry));
                $this->request->post['theme'] = $_GET['theme'];
                $this->request->server['REQUEST_METHOD'] = 'POST';
                if ($this->load->controller('setting/setting/showtheme')){
                    $this->load->controller('setting/setting/usetheme');
                    $json['success'] = true;
                    $json['code'] = 200;
                    $json['msg'] = 'Success';
                }
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
