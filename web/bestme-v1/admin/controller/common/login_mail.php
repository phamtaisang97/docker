<?php
class ControllerCommonLoginMail extends Controller {
    private $error = array();

    public function index() {

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateLoginWithEmail()) {
            $this->session->data['user_token'] = token(32);

            /* update last login */
            $this->load->model('user/user');
            $this->model_user_user->editLastLoggedIn($this->user->getId());

            $this->response->redirect($this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true));
        }
    }

    protected function validateLoginWithEmail() {
        if($this->request->post['email'] && $this->request->post['token'] && $this->request->post['shop']  && $this->verifyTokenSso($this->request->post['email'], $this->request->post['token'])) {
            $this->user->loginByEmailWithoutPwd($this->request->post['email'], $this->request->post['token'], $this->request->post['shop']);
            return true;
        }else{
            return false;
        }
    }

    private function verifyTokenSso($email, $token) {
        try{
            $url = MIX_URL_SSO . "api/v1/sso/retrieve-user-profile";
            $client_id = MIX_CLIENT_ID_SSO_LOGIN;
            $client_secret = MIX_CLIENT_SECRET_SSO;
            $x_signature = sprintf('%s:%s', $client_id, $client_secret);
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                    "authorization: Bearer " . $token,
                    "x-hub-signature: " . $x_signature,
                )
            ));

            $response = curl_exec($curl);

            //Check for errors.
            if(curl_errno($curl)){
                $this->log->write('Cannot curl to SSO : ' . curl_error($curl) );
                return false;
            }
            // Close cURL session handle
            curl_close($curl);
            $response = json_decode($response,true);

            if (!isset($response['status']) || $response['status'] != 200) {
                $this->log->write('Cannot get user info from sso: ' . isset($response['status']) ? $response['status'] : 'UNKNOWN');
                return false;
            }else{
                $email_temp = $response['user']['email'];
                if($email != $email_temp ) {
                    $this->log->write('Email not correct !: ' . isset($response['status']) ? $response['status'] : 'UNKNOWN');
                    return false;
                }
            }
            return true;
        } catch(\Exception $e) {
            $this->log->write('Cannot get info from sso curl ' . $e->getMessage());
            return false;
        }
    }
}