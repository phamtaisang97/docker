<?php

class ControllerCommonFooter extends Controller
{
    use Setting_Util_Trait;

    public function index()
    {
        $this->load->language('common/footer');

        $config_packet_trial_start = $this->config->get('config_packet_trial_start');
        $config_packet_trial_period = $this->config->get('config_packet_trial_period');
        $is_trial_config_existing = !is_null($config_packet_trial_start) && !is_null($config_packet_trial_period);

        $config_packet_paid = $this->config->get('config_packet_paid');

        /* show trial if not paid and trial configs are set */
        $is_show_trial = ($config_packet_paid != 1 && $is_trial_config_existing) ? 1 : 0;
        $data['is_show_trial'] = $is_show_trial;

        if ($is_show_trial) {
            $date_trial_start = DateTime::createFromFormat('Y-m-d', $config_packet_trial_start);
            if ($date_trial_start instanceof DateTime) {
                $data['config_packet_trial_start'] = $config_packet_trial_start;
                $data['config_packet_trial_period'] = $config_packet_trial_period;

                $timezone = ini_get('date.timezone') ? ini_get('date.timezone') : date_default_timezone_get();
                $now = new DateTime('now', new DateTimeZone($timezone));
                $interval = $now->diff($date_trial_start)->format("%a");
                $data['trial_date'] = $config_packet_trial_period - $interval;
                if ($data['trial_date'] < 0) {
                    $data['trial_date'] = 0;
                }
                $data['trial_url'] = defined('BESTME_SERVER_PRICING') ? BESTME_SERVER_PRICING : '';
            } else {
                /* do not show trial dialog if any error... */
                $data['is_show_trial'] = $is_show_trial = 0;
            }
        }

        $data['packet_wait_for_pay'] =  $this->getSettingPacketWaitForPayValue();

        if ($this->user->isLogged() && isset($this->request->get['user_token']) && ($this->request->get['user_token'] == $this->session->data['user_token'])) {
            $data['text_version'] = sprintf($this->language->get('text_version'), VERSION);
        } else {
            $data['text_version'] = '';
        }

        return $this->load->view('common/footer', $data);
    }
}
