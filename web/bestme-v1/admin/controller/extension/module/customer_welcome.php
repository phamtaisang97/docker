<?php

/**
 * Class ControllerExtensionModuleCustomerWelcome
 * This is for installation from admin dashboard!!!
 */
class ControllerExtensionModuleCustomerWelcome extends Controller
{
    private $error = array();

    /**
     * Default call
     */
    public function index()
    {
        /* load language config */
        $this->load->language('extension/module/customer_welcome');

        /* set page title */
        $this->document->setTitle($this->language->get('heading_title'));

        /* load setting module for updating status of this module */
        $this->load->model('setting/setting');

        /* handle POST request*/
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->log->write('Saving customer_welcome setting...');

            $this->model_setting_setting->editSetting('module_customer_welcome', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->log->write('Saving customer_welcome setting... success!');

            /* redirect to market place, under Extensions > Extensions, where shows all modules */
            $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
        }

        /* show error if has */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        /* show breadcrumbs */
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/customer_welcome', 'user_token=' . $this->session->data['user_token'], true)
        );

        /* url of action edit/delete */
        $data['action'] = $this->url->link('extension/module/customer_welcome', 'user_token=' . $this->session->data['user_token'], true);

        $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

        /* set status of module if has or has not */
        if (isset($this->request->post['module_customer_welcome_status'])) {
            $data['module_customer_welcome_status'] = $this->request->post['module_customer_welcome_status'];
        } else {
            $data['module_customer_welcome_status'] = $this->config->get('module_customer_welcome_status');
        }

        /* default required layout for all module pages: header - column_left - {{ module content here }} - footer */
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        /* custom data */
        $data['custom_data_title'] = $this->language->get('custom_data_title');
        $data['custom_data_message'] = $this->language->get('custom_data_message');

        /* load data to view */
        $this->response->setOutput($this->load->view('extension/module/customer_welcome', $data));
    }

    /**
     * On install
     */
    public function install()
    {
        /* set default status to "enable" */
        $this->load->model('setting/setting');

        $setting = array();
        $setting['module_customer_welcome_status'] = 1;
        $this->model_setting_setting->editSetting('module_customer_welcome', $setting);

        /*
         * Register event on customer logged in
         * $this->model_setting_event->addEvent($code, $trigger, $action);
         * e.g: $this->model_setting_event->addEvent('my_theme', 'catalog/controller/catalog/product/before', 'my_theme/product');
         */
        $this->log->write('registering event customer_welcome...');

        $this->load->model('setting/event');

        $this->model_setting_event->addEvent('customer_welcome', 'catalog/model/account/customer/deleteLoginAttempts/after', 'event/customer_welcome/post_customer_login');

        /* init module table of this module */
        $this->load->model('extension/module/customer_welcome');
        $this->load->model('user/user_group');

        $this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'extension/module/customer_welcome');
        $this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'extension/module/customer_welcome');
        $this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'extension/module/customer_welcome');
        $this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'extension/module/customer_welcome');

        $this->model_extension_module_customer_welcome->install();

        $this->log->write('registering event customer_welcome...done!');
    }

    /**
     * On uninstall
     */
    public function uninstall()
    {
        /* remove setting of this module */
        $this->load->model('setting/setting');
        $this->model_setting_setting->deleteSetting('module_customer_welcome');

        /* drop module table of this module */
        $this->load->model('extension/module/customer_welcome');

        /** @var ModelExtensionModuleCustomerWelcome $this->model_extension_module_customer_welcome */
        $this->model_extension_module_customer_welcome->uninstall();

        /* remove registerd event */
        $this->log->write('un-registering event customer_welcome...');

        $this->load->model('setting/event');

        $this->model_setting_event->deleteEvent('customer_welcome');

        $this->log->write('un-registering event customer_welcome... done!');
    }

    /* validate form on POST */
    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'extension/module/customer_welcome')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }
}