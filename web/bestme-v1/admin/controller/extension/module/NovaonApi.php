<?php
class ControllerExtensionModuleNovaonApi extends Controller {
	private $error = array();
	
	const DEFAULT_MODULE_SETTINGS = [
		'name' => 'CustomApi',
		'message' => 'Api Credentials',
		'consumer_key' => 'GHNHTYDSTw567',
		'consumer_secret' => 'GHNDRTUYUKMVFYJKNBGFFGFHJJ7886GH;',
		'status' => 0 /* Enabled by default*/
	];

	public function index() {
		$this->load->model('extension/module/NovaonApi');
		$this->load->language('extension/module/NovaonApi');
		
		$this->document->setTitle($this->language->get('heading_title'));
			
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
			//$this->model_setting_module->editModule($this->request->get['module_id'], $this->request->post);
			$this->model_extension_module_NovaonApi->updateCredentials(1, $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}
		
		$data = array();

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/NovaonApi', 'user_token=' . $this->session->data['user_token'], true)
		);

		$api_details = $this->model_extension_module_NovaonApi->getApiDetails();
		if (isset($this->request->post['consumer_secret'])) {
			$data['consumer_secret'] = $this->request->post['consumer_secret'];
		} else {
			$data['consumer_secret'] = $api_details[0]['consumer_secret'];
		}
		
		if (isset($this->request->post['consumer_key'])) {
			$data['consumer_key'] = $this->request->post['consumer_key'];
		} else {
			$data['consumer_key'] = $api_details[0]['consumer_key'];
		}
		
		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} else {
			$data['status'] = $api_details[0]['status'];
		} 
		
		$data['action']['cancel'] = $this->url->link('marketplace/extension', 'user_token='.$this->session->data['user_token'].'&type=module');
		$data['action']['save'] = "";

		$data['error'] = $this->error;	
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		/* echo "<pre>";
		print_r($data);
		echo "</pre>"; */
		//print_r($data);
		$this->response->setOutput($this->load->view('extension/module/custom', $data));
		
	}



	public function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/NovaonApi')) {
			$this->error['permission'] = true;
			
			return false;
		}

		if (!isset($this->request->post['consumer_secret']) || !utf8_strlen($this->request->post['consumer_secret'])) {
			$this->error['consumer_secret'] = true;
		}
		
		if (!isset($this->request->post['consumer_key']) || !utf8_strlen($this->request->post['consumer_key'])) {
			$this->error['consumer_key'] = true;
		}
		return empty($this->error);
	}
	
	public function install() {
		$this->load->model('setting/setting');
		$this->load->model('setting/module');

		$this->model_setting_setting->editSetting('module_NovaonApi', ['module_NovaonApi_status'=>1]);
		//$this->model_setting_module->addModule('customApi', self::DEFAULT_MODULE_SETTINGS); 
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "novaon_api_credentials` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `consumer_key` varchar(255) NOT NULL,
		  `consumer_secret` varchar(255) NOT NULL,
		  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  `status` tinyint(1) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;");
		$this->db->query("INSERT INTO " . DB_PREFIX . "novaon_api_credentials 
		SET consumer_key='GHNHTYDSTw567',
		consumer_secret='GHNDRTUYUKMVFYJKNBGFFGFHJJ7886GH',
		create_date = CURRENT_TIMESTAMP, 
		status = 1");
		
		
	}
	
	public function uninstall() {
		$this->db->query("UPDATE " . DB_PREFIX . "novaon_api_credentials SET status = '0'");
		$this->load->model('setting/setting');
		$this->model_setting_setting->deleteSetting('module_NovaonApi');
	}
}
