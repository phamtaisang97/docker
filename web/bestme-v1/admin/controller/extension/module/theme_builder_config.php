<?php

class ControllerExtensionModuleThemeBuilderConfig extends Controller
{
    use Theme_Config_Util;

    private $error = array();

    public function index()
    {
        $this->load->language('extension/module/theme_builder_config');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('module_theme_builder_config', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/theme_builder_config', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['action'] = $this->url->link('extension/module/theme_builder_config', 'user_token=' . $this->session->data['user_token'], true);

        $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

        if (isset($this->request->post['module_theme_builder_config_status'])) {
            $data['module_theme_builder_config_status'] = $this->request->post['module_theme_builder_config_status'];
        } else {
            $data['module_theme_builder_config_status'] = $this->config->get('module_theme_builder_config_status');
        }

        $data['href_reset_config'] = $this->url->link('extension/module/theme_builder_config/reset', 'user_token=' . $this->session->data['user_token'], true);

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/theme_builder_config', $data));
    }

    /**
     * On install
     */
    public function install()
    {
        $this->log->write('Installing module theme_builder_config...');

        /* init module table of this module */
        $this->load->model('extension/module/theme_builder_config');
        $this->load->model('user/user_group');

        $this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'extension/module/theme_builder_config');
        $this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'extension/module/theme_builder_config');

        $this->model_extension_module_theme_builder_config->install();

        /* clear session data */
        $this->clearThemeBuilderConfigForCurrentSessionData();

        $this->log->write('Installing module theme_builder_config... done!');
    }

    /**
     * reset default
     */
    public function reset()
    {
        /* if update config */
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->load->model('extension/module/theme_builder_config');

            $this->model_extension_module_theme_builder_config->initAllDefaultConfigs();

            /* clear session data */
            $this->clearThemeBuilderConfigForCurrentSessionData();

            /* rebuild css */
            $this->rebuildCss(); // each theme has a default config, so rebuildCss work properly
            // also for preview too
            $this->rebuildCss($preview = true);

            $this->session->data['success'] = $this->language->get('text_success');
        }

        $this->response->redirect($this->url->link('extension/module/theme_builder_config', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
    }

    /**
     * init default for current theme
     */
    public function initAllDefaultConfigsForCurrentTheme()
    {
        /* if update config */
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->load->model('extension/module/theme_builder_config');

            /* avoid init default config if theme config already */
            // notice: include new theme name in call to avoid delayed from db where new config_theme value not yet updated successfully!
            $existed = $this->model_extension_module_theme_builder_config->isConfigExistedForCurrentTheme();
            if (!$existed) {
                $this->model_extension_module_theme_builder_config->initAllDefaultConfigs();

                /* clear session data */
                $this->clearThemeBuilderConfigForCurrentSessionData();

                /* rebuild css */
                $this->rebuildCss(); // each theme has a default config, so rebuildCss work properly...
            }
        }

        header('Content-Type: application/json');
        return json_encode([
            'code' => 200,
            'message' => 'OK'
        ]);
    }

    /**
     * On uninstall
     */
    public function uninstall()
    {
        $this->log->write('Uninstalling module theme_builder_config...');

        /* drop module table of this module */
        $this->load->model('extension/module/theme_builder_config');

        $this->model_extension_module_theme_builder_config->uninstall();

        /* clear session data */
        $this->clearThemeBuilderConfigForCurrentSessionData();

        $this->log->write('Uninstalling module theme_builder_config... done!');
    }

    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'extension/module/theme_builder_config')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    /**
     * GET:
     *   - param: key
     *
     * POST:
     *   - data: config array
     */
    public function config()
    {
        $this->load->model('extension/module/theme_builder_config');
        /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;

        /* if update config */
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $config_key = $this->request->get['key'];
            $theme_builder_config = $this->getThemeBuilderConfig($config_key);
            $config = $theme_builder_config['current'];

            // update attribute filters if config_key is 'config_section_category_filter'
            if (ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CATEGORY_FILTER == $config_key) {
                $this->load->model('catalog/attribute_filter');

                // get from session not request
                $attribute_filters_session = $this->load->controller('section_category/filter/getAttributeFilters');
                $attribute_filters = json_decode($attribute_filters_session['current'], true);
                $this->model_catalog_attribute_filter->updateAttributeFiltersStatus($attribute_filters);

                /* clear session data */
                unset($this->session->data['attribute_filters']);
            }

            if (null !== $config && !is_array($config)) {
                $config = json_decode($config, true);
                if (json_last_error() !== JSON_ERROR_NONE) {
                    header('Content-Type: application/json');
                    return json_encode([
                        'code' => 400,
                        'message' => 'error: invalid config'
                    ]);
                }
            }

            $data = [
                'store_id' => 0,
                'code' => 'config',
                'key' => $config_key,
                'config' => $config
            ];

            $this->applyThemeConfig($data);

            $this->session->data['success'] = 'success';

            /* clear session data */
            $this->clearThemeBuilderConfigForCurrentSessionData();

            header('Content-Type: application/json');
            $data = json_encode([
                'code' => 204,
                'message' => $this->session->data['success']
            ]);

            return $data;
        }

        if (!($this->request->server['REQUEST_METHOD'] == 'GET') || !$this->validate()) {
            throw new BadMethodCallException('Get config: Support method GET only');
        }

        /* do get config */
        $config_key = $this->request->get['key'];
        if (!$model_extension_module_theme_builder_config->isSupportKey($config_key)) {
            throw new InvalidArgumentException('Get config: Not support key ' . $config_key);
        }

        $theme_builder_config = $this->getThemeBuilderConfig($config_key);

        // update to current session data
        $this->updateThemeBuilderConfigForCurrentSessionData($config_key, $theme_builder_config['data'], $theme_builder_config['index']);

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        header('Content-Type: application/json');
        $data = is_array($theme_builder_config['current']) ? json_encode($theme_builder_config['current'], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG) : $theme_builder_config['current'];

        return $data;
    }

    public function change()
    {
        if (!($this->request->server['REQUEST_METHOD'] == 'POST') || !$this->validate() || !isset($this->request->post['theme_builder_config'])) {
            header('Content-Type: application/json');
            return json_encode([
                'code' => 400,
                'config' => '{}'
            ]);
        }

        /* do get config */
        $config_key = $this->request->get['key'];

        $this->load->model('extension/module/theme_builder_config');
        /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;

        if (!$model_extension_module_theme_builder_config->isSupportKey($config_key)) {
            throw new InvalidArgumentException();
        }

        $theme_builder_config = $this->getThemeBuilderConfig($config_key);

        $config = str_replace('&quot;', '"', $this->request->post['theme_builder_config']);
        $config = str_replace('&gt;', '>', $config);
        $config = str_replace('&lt;', '<', $config);
        // $this->request->post  system/library/request.php dòng 46

        $config = is_array($config) ? json_encode($config, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG) : $config;
        // $config = html_entity_decode($config); // TODO: fix to use this...

        $undo_redo = new \theme_config\Undo_Redo($theme_builder_config['data'], $theme_builder_config['index']);
        $undo_redo->add($config);
        // update to current session data
        $this->updateThemeBuilderConfigForCurrentSessionData($config_key, $undo_redo->allData(), $undo_redo->getIndex());
        // update config color
        $this->rebuildCss(true);

        header('Content-Type: application/json');
        $data = json_encode([
            'code' => 201,
            'index' => $undo_redo->getIndex(),
            'config' => $undo_redo->currentData()
        ]);

        return $data;
    }

    public function undo()
    {
        /* do get config */
        $config_key = $this->request->get['key'];
        $theme_builder_config = $this->getThemeBuilderConfig($config_key);

        $undo_redo = new \theme_config\Undo_Redo($theme_builder_config['data'], $theme_builder_config['index']);
        $undo_redo->undo();

        // update to current session data
        $this->updateThemeBuilderConfigForCurrentSessionData($config_key, $undo_redo->allData(), $undo_redo->getIndex());
        // update config color
        $this->rebuildCss(true);

        header('Content-Type: application/json');
        $data = json_encode([
            'code' => 201,
            'index' => $undo_redo->getIndex(),
            'config' => $undo_redo->currentData()
        ]);

        return $data;
    }

    public function redo()
    {
        /* do get config */
        $config_key = $this->request->get['key'];
        $theme_builder_config = $this->getThemeBuilderConfig($config_key);

        $undo_redo = new \theme_config\Undo_Redo($theme_builder_config['data'], $theme_builder_config['index']);
        $undo_redo->redo();

        // update to current session data
        $this->updateThemeBuilderConfigForCurrentSessionData($config_key, $undo_redo->allData(), $undo_redo->getIndex());
        // update config color
        $this->rebuildCss(true);

        header('Content-Type: application/json');
        $data = json_encode([
            'code' => 201,
            'index' => $undo_redo->getIndex(),
            'config' => $undo_redo->currentData()
        ]);

        return $data;
    }

    public function clearAllThemeBuilderConfigForCurrentSessionData()
    {
        unset($this->session->data['theme_builder_config'][$this->getCurrentTheme()]);
        unset($this->session->data['attribute_filters']);
    }

    private function applyThemeConfig(array $data)
    {
        $this->load->model('extension/module/theme_builder_config');
        /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;

        /* save to db */
        $result = $model_extension_module_theme_builder_config->updateConfig($data);

        /* apply to css */
        $result &= $this->rebuildCss();

        return $result;
    }

    /**
     * Rebuild Css
     * @param bool $preview
     * @return bool
     */
    private function rebuildCss($preview = false)
    {
        $result = true;

        $this->load->model('extension/module/theme_builder_config');
        /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;

        /* apply to css */
        // theme color
        if ($preview) {
            $override_css_version = token(32);
            $this->session->data['preview_builder_config_version'] = $override_css_version;
        } else {
            $this->load->model('setting/setting');
            $override_css_version = $this->model_setting_setting->increaseNumberConfig('builder_config_version');
        }

        $color = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_COLOR);
        $color = json_decode($color, true);
        if (is_array($color)) {
            $result &= $this->applyThemeColorConfig($color, $preview, $override_css_version);
        }

        // theme text
        $text = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_TEXT);
        $text = json_decode($text, true);
        if (is_array($text)) {
            $result &= $this->applyThemeTextConfig($text, $preview, $override_css_version);
        }

        // section slide_show
        $slide_show = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_SLIDESHOW);
        $slide_show = json_decode($slide_show, true);
        if (is_array($slide_show)) {
            $result &= $this->applySectionSlideShowConfig($slide_show, $preview, $override_css_version);
        }

        // override css
        $override_css = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_OVERRIDE_CSS);
        $override_css = json_decode($override_css, true);
        $override_css = isset($override_css['css']) ? $override_css['css'] : '';
        $result &= $this->applyThemeOverrideCssConfig($override_css, $preview, $override_css_version);

        return $result;
    }

    private function applyThemeColorConfig(array $color, $preview, $override_css_version)
    {
        // get template for css
        $css_template_file = DIR_CATALOG . 'view/theme/' . $this->getCurrentThemeDir() . '/asset/sass/override/color.css';
        $css_file_content = file_get_contents($css_template_file);

        $this->load->library('theme_config/theme_config');

        /** @var \theme_config\Theme_Config $themeConfig */
        $themeConfig = $this->theme_config;

        $themeConfig->applyThemeColorConfig($color, $css_file_content);

        // write to css

        if ($preview) {
            $css_dir = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override_preview/' . $override_css_version . '/';
            $css_dir_parrent = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override_preview/';
        } else {
            $css_dir = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override/' . $override_css_version . '/';
            $css_dir_parrent = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override/';
        }
        try {
            $this->removeOldOverrideDir($css_dir_parrent, $override_css_version);
            if (!is_dir($css_dir)) {
                mkdir($css_dir, 0755, true);
            }
            $css_file = $css_dir . 'color.css';
            //$this->log->write('$css_file_content: ' . $css_file_content);
            file_put_contents($css_file, $css_file_content);
        } catch (Exception $e) {
            $this->log->write('applyThemeColorConfig got error: ' . $e->getMessage());
        }

        return true;
    }

    private function applyThemeOverrideCssConfig($override_css, $preview, $override_css_version)
    {
        $this->load->library('theme_config/theme_config');

        if ($preview) {
            $css_dir = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override_preview/' . $override_css_version . '/';
            $css_dir_parrent = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override_preview/';
        } else {
            $css_dir = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override/' . $override_css_version . '/';
            $css_dir_parrent = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override/';
        }
        try {
            $this->removeOldOverrideDir($css_dir_parrent, $override_css_version);
            if (!is_dir($css_dir)) {
                mkdir($css_dir, 0755, true);
            }
            $css_file = $css_dir . 'custom_css.css';
            //$this->log->write('$css_file_content: ' . $css_file_content);
            file_put_contents($css_file, $override_css);
        } catch (Exception $e) {
            $this->log->write('applyThemeColorConfig got error: ' . $e->getMessage());
        }

        return true;
    }

    private function applyThemeTextConfig(array $text, $preview, $override_css_version)
    {
        // get template for css
        $css_template_file = DIR_CATALOG . 'view/theme/' . $this->getCurrentThemeDir() . '/asset/sass/override/text.css';
        $css_file_content = file_get_contents($css_template_file);

        $this->load->library('theme_config/theme_config');

        /** @var \theme_config\Theme_Config $themeConfig */
        $themeConfig = $this->theme_config;

        $themeConfig->applyThemeTextConfig($text, $css_file_content);

        // write to css

        if ($preview) {
            $css_dir = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override_preview/' . $override_css_version . '/';
            $css_dir_parrent = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override_preview/';
        } else {
            $css_dir = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override/' . $override_css_version . '/';
            $css_dir_parrent = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override/';
        }
        try {
            $this->removeOldOverrideDir($css_dir_parrent, $override_css_version);
            if (!is_dir($css_dir)) {
                mkdir($css_dir, 0755, true);
            }
            $css_file = $css_dir . 'text.css';
            //$this->log->write('$css_file_content: ' . $css_file_content);
            file_put_contents($css_file, $css_file_content);
        } catch (Exception $e) {
            $this->log->write('applyThemeTextConfig got error: ' . $e->getMessage());
        }

        return true;
    }

    private function applySectionSlideShowConfig(array $slide_show, $preview, $override_css_version)
    {
        // get template for css
        $css_template_file = DIR_CATALOG . 'view/theme/' . $this->getCurrentThemeDir() . '/asset/sass/override/slideshow.css';
        $css_file_content = file_get_contents($css_template_file);

        $this->load->library('theme_config/theme_config');

        /** @var \theme_config\Theme_Config $themeConfig */
        $themeConfig = $this->theme_config;

        $themeConfig->applySectionSlideshowConfig($slide_show, $css_file_content);

        // write to css

        if ($preview) {
            $css_dir = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override_preview/' . $override_css_version . '/';
            $css_dir_parrent = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override_preview/';
        } else {
            $css_dir = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override/' . $override_css_version . '/';
            $css_dir_parrent = DIR_ASSET . 'catalog/view/theme/' . $this->getCurrentThemeDir() . '/asset/css/override/';
        }
        try {
            $this->removeOldOverrideDir($css_dir_parrent, $override_css_version);
            if (!is_dir($css_dir)) {
                mkdir($css_dir, 0755, true);
            }
            $css_file = $css_dir . 'slideshow.css';
            //$this->log->write('$css_file_content: ' . $css_file_content);
            file_put_contents($css_file, $css_file_content);
        } catch (Exception $e) {
            $this->log->write('applySectionSlideShowConfig got error: ' . $e->getMessage());
        }

        return true;
    }

    private function removeOldOverrideDir($dir, $current_dir = null)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir") {
                        if ($object != $current_dir || $current_dir == null) {
                            $this->removeOldOverrideDir($dir . "/" . $object);
                            rmdir($dir . "/" . $object);
                        }
                    } else {
                        unlink($dir . "/" . $object);
                    }
                }
            }
            reset($objects);
        }
    }

    /**
     * get config from current session data or database
     * @param string $config_key
     * @param bool $from_db
     * @return array
     */
    private function getThemeBuilderConfig($config_key, $from_db = false)
    {
        $config_arr = null;
        $current_theme = $this->getCurrentTheme();

        if (!$from_db && isset($this->session->data['theme_builder_config'][$current_theme][$config_key])) {
            /* try from session */
            $current_config_index = $this->session->data['theme_builder_config'][$current_theme][$config_key]['index'];

            $config_arr = $this->session->data['theme_builder_config'][$current_theme][$config_key]['data'];
            $undo_redo_lib = new \theme_config\Undo_Redo($config_arr, $current_config_index);
            $existing_config = $undo_redo_lib->currentData();
        }

        /* get from db if force db, or try again in case of previous $config_arr empty */
        if ($from_db || empty($config_arr)) {
            $this->load->model('extension/module/theme_builder_config');
            /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
            $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;

            /* try from database */
            $customer_id = $this->customer->getId();
            $store_id = 0; // TODO: find...
            $existing_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id, $config_key);
            if ($this->isSupportThemeFeature(self::$RATE_FORM)) {
                if (!empty($existing_config) && $config_key == ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CUSTOMIZE_LAYOUT) {
                    $decode = json_decode($existing_config);
                    $default_customize_layout = isset($decode->default_customize_layout) ? $decode->default_customize_layout : [];
                    $exist = false;
                    foreach ($default_customize_layout as $layout) {
                        if ($layout->name == 'txt_block_rate') {
                            $exist = true;
                            break;
                        }
                    }

                    if (!$exist) {
                        $default_customize_layout[] = ["name" => "txt_block_rate", "file" => "rate-box"];
                        $decode->default_customize_layout = $default_customize_layout;
                    }
                    $existing_config = json_encode($decode);
                }
            }
            // if not existing => create default
            // this may never occur because we already init when installing module
            if (empty($existing_config)) {
                $existing_config = $model_extension_module_theme_builder_config->initDefaultConfig($store_id, $config_key);
            }

            // init current index
            $current_config_index = 0;

            // make array
            $existing_config = json_encode(json_decode($existing_config, true), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG);
            $config_arr = [$existing_config];
        }

        return [
            'index' => $current_config_index,
            'data' => $config_arr,
            'current' => $existing_config
        ];
    }

    /**
     * @param string $config_key
     * @param array $data
     * @param int $index
     */
    private function updateThemeBuilderConfigForCurrentSessionData($config_key, array $data, $index)
    {
        $this->session->data['theme_builder_config'][$this->getCurrentTheme()][$config_key] = [
            'index' => $index,
            'data' => $data
        ];
    }

    private function clearThemeBuilderConfigForCurrentSessionData()
    {
        $config_key = isset($this->request->get['key']) ? $this->request->get['key'] : '';
        if (!empty($config_key)
            && array_key_exists($config_key, $this->session->data['theme_builder_config'][$this->getCurrentTheme()])
        ) {
            unset($this->session->data['theme_builder_config'][$this->getCurrentTheme()][$config_key]);

            return;
        }

        unset($this->session->data['theme_builder_config'][$this->getCurrentTheme()]);
    }

    private function getCurrentTheme()
    {
        $current_theme = $this->config->get('config_theme');

        return isset($current_theme) ? $current_theme : 'default';
    }

    private function getCurrentThemeDir()
    {
        $current_theme = $this->config->get('config_theme');
        /*
		 * Support load theme deeper 1 level. This is for themes are under each Theme Partner
		 * e.g
		 * - old theme structure: view/theme/tech_sample_1
		 * - now support additional: view/theme/partner_apollo_group/tech_sample_2
		 */
        $this->load->model('setting/setting');
        $theme_directory = $this->model_setting_setting->getSettingValue('theme_' . $current_theme . '_directory');

        return isset($theme_directory) ? $theme_directory : 'default';
    }
}