<?php

/**
 * Class ControllerExtensionModuleCustomerWelcome
 * This is for installation from admin dashboard!!!
 */
class ControllerExtensionModuleRecommendation extends Controller
{
    private $error = array();

    /**
     * Default call
     */
    public function index()
    {
        /* load language config */
        $this->load->language('extension/module/recommendation');

        /* set page title */
        $this->document->setTitle($this->language->get('heading_title'));

        /* load setting module for updating status of this module */
        $this->load->model('setting/setting');

        /* handle POST request*/
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->log->write('Saving recommendation setting...');

            $this->model_setting_setting->editSetting('module_recommendation', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->log->write('Saving recommendation setting... success!');

            /* redirect to market place, under Extensions > Extensions, where shows all modules */
            $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
        }

        /* show error if has */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        /* show breadcrumbs */
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/recommendation', 'user_token=' . $this->session->data['user_token'], true)
        );

        /* url of action edit/delete */
        $data['action'] = $this->url->link('extension/module/recommendation', 'user_token=' . $this->session->data['user_token'], true);

        $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

        /* set status of module if has or has not */
        if (isset($this->request->post['module_recommendation_status'])) {
            $data['module_recommendation_status'] = $this->request->post['module_recommendation_status'];
        } else {
            $data['module_recommendation_status'] = $this->config->get('module_recommendation_status');
        }

        /* default required layout for all module pages: header - column_left - {{ module content here }} - footer */
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        /* custom data */
        $data['custom_data_title'] = $this->language->get('custom_data_title');
        $data['custom_data_message'] = $this->language->get('custom_data_message');

        /* event setting */
        $data['event_setting_title'] = $this->language->get('event_setting_title');
        $this->load->model('extension/module/recommendation');
        $eventSetting = $this->model_extension_module_recommendation->getEventSetting(1);
        $eventSetting = json_decode($eventSetting, true);
        $data['event_setting'] = $eventSetting;

        /* load data to view */
        $this->response->setOutput($this->load->view('extension/module/recommendation', $data));
    }

    /**
     * On install
     */
    public function install()
    {
        /* set default status to "enable" */
        $this->load->model('setting/setting');

        $setting = array();
        $setting['module_recommendation_status'] = 1;
        $this->model_setting_setting->editSetting('module_recommendation', $setting);

        /* init module table of this module */
        $this->load->model('extension/module/recommendation');
        $this->load->model('user/user_group');

        $this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'extension/module/recommendation');
        $this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'extension/module/recommendation');

        $this->model_extension_module_recommendation->install();

        // add setting
        $this->log->write('adding new event setting...');
        $this->model_extension_module_recommendation->saveEventSetting(
            [
                [
                    'event' => 'view',
                    'url' => 'http://sample.com/view'
                ],
                [
                    'event' => 'buy',
                    'url' => 'http://sample.com/buy'
                ],
                [
                    'event' => 'rate',
                    'url' => 'http://sample.com/rate'
                ]
            ]
        );

        /* done */
        $this->log->write('registering event recommendation...done!');
    }

    /**
     * On uninstall
     */
    public function uninstall()
    {
        /* remove setting of this module */
        $this->load->model('setting/setting');
        $this->model_setting_setting->deleteSetting('module_recommendation');

        /* drop module table of this module */
        $this->load->model('extension/module/recommendation');

        /* done */
        $this->log->write('un-registering event recommendation... done!');
    }

    /* validate form on POST */
    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'extension/module/recommendation')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }
}