<?php


use App_Api\App_Config;
use App_Api\App_Setting;


class ControllerExtensionAppstorePaymOnsVnpay extends Controller
{
    public static $VNPAY_REGISTER = 'https://vnpayment.vnpay.vn/Lien-he-Vnpayment.htm';
    public static $VNPAY_MERCHANT_LOGIN = 'https://merchant.vnpay.vn/Users/Login.htm';
    public static $VNPAY_GUIDE = 'https://docs.google.com/document/d/1p2J9qJU8T2L80TNgHFB7CuuaUhbM35yDSBkzNGvzvjk/edit';

    private $error = array();

    function index()
    {
        $this->load->language('extension/appstore/paym_ons_vnpay');
        $this->load->model('extension/appstore/paym_ons_vnpay');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $data['action'] = $this->url->link('extension/appstore/paym_ons_vnpay/saveCode', 'user_token=' . $this->session->data['user_token'], true);
        $data['vnp_web_code'] = '';
        $data['vnp_hash_secret'] = '';

        // account info
        $account = $this->model_extension_appstore_paym_ons_vnpay->getAccountPayment();
        if ($account->num_rows > 0) {
            $accountId = $account->row['id'];
            $data['vnp_web_code'] = $account->row['vnp_web_code'];
            $data['vnp_hash_secret'] = $account->row['vnp_hash_secret'];
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        }

        $data['href_register'] = defined('VNPAY_REGISTER') ? VNPAY_REGISTER : self::$VNPAY_REGISTER;
        $data['href_merchant_login'] = defined('VNPAY_MERCHANT_LOGIN') ? VNPAY_MERCHANT_LOGIN : self::$VNPAY_MERCHANT_LOGIN;
        $data['href_guide'] = defined('VNPAY_GUIDE') ? VNPAY_GUIDE : self::$VNPAY_GUIDE;

        $this->response->setOutput($this->load->view('extension/appstore/paym_ons_vnpay/paym_ons_vnpay', $data));
    }

    function introduce()
    {
        $this->load->language('extension/appstore/paym_ons_vnpay');
        $this->document->setTitle($this->language->get('heading_title_description'));

        // Load logo app
        $data['app_logo'] = "view/image/appstore/paym_ons_vnpay.png";

        // Link to detail app
        $data['link_to_app'] = $this->url->link('extension/appstore/paym_ons_vnpay', 'user_token=' . $this->session->data['user_token'], true);

        // Import default layout
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        // Return view of introduce page
        $this->response->setOutput($this->load->view('extension/appstore/paym_ons_vnpay/introduce', $data));
    }

    function saveCode()
    {
        $this->load->model('extension/appstore/paym_ons_vnpay');
        $data = $this->request->post;
        $accountPayment = [
            'vnp_web_code' => $data['vnp_web_code'],
            'vnp_hash_secret' => $data['vnp_hash_secret'],
        ];
        $account = $this->model_extension_appstore_paym_ons_vnpay->getAccountPayment();

        if ($account->num_rows > 0) {
            $accountId = $account->row['id'];
            // update
            $this->model_extension_appstore_paym_ons_vnpay->updateInfoAccountPayment($accountPayment, (int)$accountId);
        } else {
            // add
            $this->model_extension_appstore_paym_ons_vnpay->addInfoAccountPayment($accountPayment);
        }

        $this->session->data['success'] = true;

        $this->response->redirect($this->url->link('extension/appstore/paym_ons_vnpay', 'user_token=' . $this->session->data['user_token'], true));
    }

    function install()
    {
        // Create Table
        $this->load->model('extension/appstore/paym_ons_vnpay');
        $this->model_extension_appstore_paym_ons_vnpay->createTables();

        // Update app_name & Logo
        $data = [
            "app_name" => "VNPay",
            "path_logo" => "view/image/appstore/paym_ons_vnpay.png"
        ];

        $setting = new App_Setting($this->registry);
        $setting->updateInfo("paym_ons_vnpay", $data);
        $setting->addModule('paym_ons_vnpay', ['name' => 'paym_ons_vnpay_module_default', 'status' => 1]);
        $asset = [
            "css" => [
                "catalog/view/theme/default/template/extension/appstore/asset/css/paym_ons_vnpay.css"
            ],
            "js" => [
                "catalog/view/theme/default/template/extension/appstore/asset/js/paym_ons_vnpay.js"
            ]
        ];
        $setting->addAsset("paym_ons_vnpay", $asset);
    }

    function uninstall()
    {
        /* Gỡ ứng dụng */
        $config = new App_Config($this->registry);
        $config->unRegisterApp('paym_ons_vnpay');

        // DROP TABLE
        $this->load->model('extension/appstore/paym_ons_vnpay');
        $this->model_extension_appstore_paym_ons_vnpay->dropTables();
    }
}
