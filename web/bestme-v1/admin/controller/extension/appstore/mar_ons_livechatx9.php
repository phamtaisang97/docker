<?php

use App_Api\App_Config;
use App_Api\App_Setting;

/**
 * Created by PhpStorm.
 * User: Microsoft Windows
 * Date: 29/05/2020
 * Time: 11:00 AM
 */

class ControllerExtensionAppstoreMarOnsLivechatx9 extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('extension/appstore/mar_ons_livechatx9');
        $this->load->model('extension/appstore/mar_ons_livechatx9');

        $this->document->setTitle($this->language->get('heading_title'));

        if (isset($this->session->data['live_chat_enter_code_success'])) {
            $data['success'] = $this->session->data['live_chat_enter_code_success'];

            unset($this->session->data['live_chat_enter_code_success']);
        } else {
            $data['success'] = '';
        }

        $data['mar_ons_livechatx9_code'] = $this->config->get('online_store_config_mar_ons_livechatx9_code');
        $data['action'] = $this->url->link('extension/appstore/mar_ons_livechatx9/saveCode', 'user_token=' . $this->session->data['user_token'], true);

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/appstore/mar_ons_livechatx9/mar_ons_livechatx9', $data));
    }

    public function saveCode()
    {
        $this->load->model('extension/appstore/mar_ons_livechatx9');
        $this->load->language('extension/appstore/mar_ons_livechatx9');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_extension_appstore_mar_ons_livechatx9->editExistingSetting('online_store_config', $this->request->post);

            $this->session->data['live_chat_enter_code_success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('extension/appstore/mar_ons_livechatx9', 'user_token=' . $this->session->data['user_token'], true));
        }
    }

    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'extension/appstore/mar_ons_livechatx9')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function introduce(){
        $this->load->language('extension/appstore/mar_ons_livechatx9');
        $this->document->setTitle($this->language->get('heading_title_description'));

        // Load logo app
        $data['app_logo'] = "view/image/appstore/mar_ons_livechatx9.jpg";
        // Load name app
        $data['app_name'] = "OnCustomer livechat";
        // Link to detail app
        $data['link_to_app'] = $this->url->link('extension/appstore/mar_ons_livechatx9', 'user_token=' . $this->session->data['user_token'] , true);

        // Import default layoyt
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        // Return view of introduce page
        $this->response->setOutput($this->load->view('extension/appstore/mar_ons_livechatx9/introduce', $data));
    }

    public function install()
    {
        $this->load->model('extension/appstore/mar_ons_livechatx9');
        //$this->model_extension_appstore_app_demo->createTables();

        // Update app_name & Logo
        $data = [
            "app_name" => "OnCustomer livechat",
            "path_logo" => "view/image/appstore/mar_ons_livechatx9.jpg"
        ];

        $setting = new App_Setting($this->registry);
        $setting->updateInfo("mar_ons_livechatx9",$data);
        $setting->addModule('mar_ons_livechatx9', ['name' => 'mar_ons_livechatx9_module_1', 'status' => 1]);
        $asset = [
            "css" => [],
            "js" => [
                "catalog/view/theme/default/template/extension/appstore/asset/js/mar_ons_livechatx9.js"
            ]
        ];
        $setting->addAsset("mar_ons_livechatx9",$asset);
    }

    public function uninstall()
    {
        /* Gỡ ứng dụng */
        $config = new App_Config($this->registry);
        $config->unRegisterApp('mar_ons_livechatx9');

        $setting = new App_Setting($this->registry);
        $modules = $setting->getModulesByCode('mar_ons_livechatx9');
        foreach ($modules as $module){
            if (!isset($module['module_id'])){
                continue;
            }
            // Delete module
            $setting->deleteModule($module['module_id']);
        }
        $this->load->model('extension/appstore/mar_ons_livechatx9');
        $this->model_setting_setting->deleteSettingValue('online_store_config', 'online_store_config_mar_ons_livechatx9_code');

        /* DROP các bảng đã thêm */
        //$this->load->model('extension/appstore/mar_ons_livechatx9');
        //$this->model_extension_appstore_mar_ons_livechatx9->dropTables();
    }
}