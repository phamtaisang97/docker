<?php

use App_Api\App_Config;
use App_Api\App_Setting;

class ControllerExtensionAppstoreMarOnsOnfluencer extends Controller
{
    private $error = array();

    // temp not user key "config_onfluencer_novaon_api_key";
    // the Onfluencer app use open api key for common
    // TODO: separate key between apps that use open api too?...
    const CONFIG_ONFLUENCER_NOVAON_API_KEY = 'config_open_api_api_key';
    const ONFLUENCER_NOVAON_HOMEPAGE = 'https://onfluencer.net/';
    const ONFLUENCER_NOVAON_GUIDE = 'https://onfluencer.net/';

    public function index()
    {
        $this->load->model('appstore/my_app');
        if (!$this->model_appstore_my_app->checkAppActive(ModelAppstoreMyApp::APP_ONFLUENCER)) {
            $this->response->redirect($this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true));
        };
        $this->load->language('extension/appstore/mar_ons_onfluencer');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        /* breadcrumb */
        $data['breadcrumbs'] = array();

        /* message */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        /* model data */
        $api_key = $this->model_setting_setting->getSettingValue(self::CONFIG_ONFLUENCER_NOVAON_API_KEY);
        if (empty($api_key)) {
            // auto create api key if not has
            $api_key = token(32);
            $this->model_setting_setting->editSettingValue($code = 'config', self::CONFIG_ONFLUENCER_NOVAON_API_KEY, $api_key);
        }
        $data['api_key'] = $api_key;

        $data['action'] = $this->url->link('extension/appstore/mar_ons_onfluencer', 'user_token=' . $this->session->data['user_token'], true);

        /* Novaon onfluencer homepage */
        $data['href_onfluencer_novaon'] = self::ONFLUENCER_NOVAON_HOMEPAGE;
        $data['href_onfluencer_novaon_guide'] = self::ONFLUENCER_NOVAON_GUIDE;

        /* common */
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/appstore/mar_ons_onfluencer/mar_ons_onfluencer', $data));
    }

    function introduce()
    {
        $this->load->language('extension/appstore/mar_ons_onfluencer');
        $this->document->setTitle($this->language->get('heading_title_description'));

        // Load logo app
        $data['app_logo'] = "view/image/appstore/mar_ons_onfluencer.png";

        // Link to detail app
        $data['link_to_app'] = $this->url->link('extension/appstore/mar_ons_onfluencer', 'user_token=' . $this->session->data['user_token'], true);

        // Import default layout
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        // Return view of introduce page
        $this->response->setOutput($this->load->view('extension/appstore/mar_ons_onfluencer/introduce', $data));
    }

    function install()
    {
        // Update app_name & Logo
        $data = [
            "app_name" => "Onfluencer Marketing",
            "path_logo" => "view/image/appstore/mar_ons_onfluencer.png"
        ];

        $setting = new App_Setting($this->registry);
        $setting->updateInfo("mar_ons_onfluencer", $data);
    }

    function uninstall()
    {
        // uninstall app
        $config = new App_Config($this->registry);
        $config->unRegisterApp('mar_ons_onfluencer');
    }
}
