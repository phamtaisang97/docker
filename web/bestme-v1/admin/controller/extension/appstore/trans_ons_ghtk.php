<?php

// TODO: remove hardcode, use loader...
require_once 'controller/extension/appstore/trans_abstract.php';

use App_Api\App_Config;
use App_Api\App_Setting;

class ControllerExtensionAppstoreTransOnsGhtk extends ControllerExtensionAppstoreTransAbstract
{
    use Transport_Util_Trait;

    const APP_CODE = 'trans_ons_ghtk';

    /* error code ReturnURL */
    const ERR_CODE_SUCCESS = '00';

    const SHIPPING_TRANSPORT_STATUES = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12];
    const COMPLETE_TRANSPORT_STATUES = [11];
    const CANCELED_TRANSPORT_STATUES = [-1, 13, 20, 21];

    const TRANSPORT_STATUSES = [
        'ORDER_STATUS_ID_COMPLETED' => self::COMPLETE_TRANSPORT_STATUES,
        'ORDER_STATUS_ID_CANCELLED' => self::CANCELED_TRANSPORT_STATUES,
        'ORDER_STATUS_ID_DELIVERING' => self::SHIPPING_TRANSPORT_STATUES
    ];

    public static $GHTK_CREATE_ACCOUNT_URL = 'https://khachhang.giaohangtietkiem.vn/khach-hang/dang_nhap';
    public static $GHTK_LOGIN_URL = 'https://khachhang.giaohangtietkiem.vn/khach-hang/dang_nhap';
    public static $GHTK_ACCOUNT_INFO_URL = 'https://khachhang.giaohangtietkiem.vn/khach-hang/thong-tin-ca-nhan';
    public static $GHTK_GET_HUBS_URL = 'https://services.giaohangtietkiem.vn/services/shipment/list_pick_add';
    public static $GHTK_GUIDE_URL = 'https://docs.google.com/document/d/1FX-YKsBxbtRNhGRRT6aYCbnJJHytqoKNRCw8eFE6Bps/edit#';
    public static $GHTK_CREATE_ORDER_URL = 'https://services.giaohangtietkiem.vn/services/shipment/order';
    public static $GHTK_CANCEL_ORDER_URL = 'https://services.giaohangtietkiem.vn/services/shipment/cancel/';
    public static $GHTK_GET_ORDER_URL = 'https://services.giaohangtietkiem.vn/services/shipment/v2/';
    public static $GHTK_SHIPPING_SERVICES_ORDER_URL = 'https://services.giaohangtietkiem.vn/services/shipment/fee';

    public static $GHTK_STATUS_ORDER_CANCEL = -1;
    public static $GHTK_STATUS_ORDER_NO_RECEIVED = 1;
    public static $GHTK_STATUS_ORDER_RECEIVED = 2;

    /* common app */
    /**
     * @inheritDoc
     */
    function install()
    {
        // Create Table
        $this->load->model('extension/appstore/trans_ons_ghtk');
        $this->model_extension_appstore_trans_ons_ghtk->createTables();

        // Update app_name & Logo
        $data = [
            "app_name" => "Giao hàng tiết kiệm",
            "path_logo" => "view/image/appstore/trans_ons_ghtk.png"
        ];

        $setting = new App_Setting($this->registry);
        $setting->updateInfo("trans_ons_ghtk", $data);
        $setting->addModule('trans_ons_ghtk', ['name' => 'trans_ons_ghtk_module_default', 'status' => 1]);
        $asset = [
            "css" => [],
            "js" => []
        ];
        $setting->addAsset("trans_ons_ghtk", $asset);
    }

    /**
     * @inheritDoc
     */
    function uninstall()
    {
        /* Gỡ ứng dụng */
        $config = new App_Config($this->registry);
        $config->unRegisterApp('trans_ons_ghtk');

        // DROP TABLE
        $this->load->model('extension/appstore/trans_ons_ghtk');
        $this->model_extension_appstore_trans_ons_ghtk->dropTables();
    }

    /**
     * @inheritDoc
     */
    public function index($data = [])
    {
        $this->load->language('extension/appstore/trans_ons_ghtk');
        $this->load->model('extension/appstore/trans_ons_ghtk');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $data['app_code'] = self::APP_CODE;

        // login ghtk
        $data['delivery_api_login'] = $this->url->link('extension/appstore/trans_ons_ghtk/login', 'user_token=' . $this->session->data['user_token'], true);
        $data['account_ghtk'] = null;
        $data['ghtk_active'] = false;

        $account_ghtk = $this->model_extension_appstore_trans_ons_ghtk->getLastAccountGHTK();
        if (!empty($account_ghtk)) {
            $data['account_ghtk'] = $account_ghtk;
            $data['ghtk_active'] = true;
            $data['ghtk_remove_connect'] = $this->url->link('extension/appstore/trans_ons_ghtk/disConnect', 'user_token=' . $this->session->data['user_token'] . '&account_ghtk_id=' . $account_ghtk['id'], true);

            $data['address_shop'] = $this->config->get('config_address');
            $data['config_address_shop_link'] = $this->url->link('settings/general', 'user_token=' . $this->session->data['user_token'], true);
            $data['delivery_api_hubs'] = $this->url->link('extension/appstore/trans_ons_ghtk/getConfiguredPickingWarehouse', 'user_token=' . $this->session->data['user_token'], true);
            $data['delivery_api_update_hubs'] = $this->url->link('common/delivery_api/updateHub', 'user_token=' . $this->session->data['user_token'], true);
        }

        $data['href_ghtk_guide'] = defined('GHTK_GUIDE_URL') ? GHTK_GUIDE_URL : self::$GHTK_GUIDE_URL;
        $data['href_ghtk_create_account'] = defined('GHTK_CREATE_ACCOUNT_URL') ? GHTK_CREATE_ACCOUNT_URL : self::$GHTK_CREATE_ACCOUNT_URL;

        $this->response->setOutput($this->load->view('extension/appstore/trans_ons_ghtk/trans_ons_ghtk', $data));
    }

    /**
     * @inheritDoc
     */
    public function introduce()
    {
        $this->load->language('extension/appstore/trans_ons_ghtk');
        $this->document->setTitle($this->language->get('heading_title_description'));

        // Load logo app
        $data['app_logo'] = "view/image/appstore/trans_ons_ghtk.png";

        // Link to detail app
        $data['link_to_app'] = $this->url->link('extension/appstore/trans_ons_ghtk', 'user_token=' . $this->session->data['user_token'], true);

        // Import default layoyt
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        // Return view of introduce page
        $this->response->setOutput($this->load->view('extension/appstore/trans_ons_ghtk/introduce', $data));
    }

    /* transport */
    /**
     * @inheritDoc
     */
    public function login($data = [])
    {
        $this->load->model('extension/appstore/trans_ons_ghtk');
        $this->load->model('setting/setting');

        $json = [
            'status' => false,
            'message' => 'Lỗi đăng nhập',
            'data' => null
        ];

        $user_name = $this->request->post['username'];
        $password = $this->request->post['password'];

        $login_url = defined('GHTK_LOGIN_URL') ? GHTK_LOGIN_URL : self::$GHTK_LOGIN_URL;
        $curl = curl_init($login_url);

        curl_setopt_array($curl, array(
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_RETURNTRANSFER => true,
        ));

        $response = curl_exec($curl);

        $doc = new DOMDocument();
        $internalErrors = libxml_use_internal_errors(true);
        $doc->loadHTML($response);
        libxml_clear_errors();

        $login_form_el = $doc->getElementById('ShopLoginForm');
        $login_inputs = $login_form_el->getElementsByTagName('input');

        $data['token'] = $login_inputs[1]->getAttribute('value');
        $data['email'] = $user_name; //'0356725555'
        $data['password'] = $password; //'Quan@123456'

        curl_setopt_array($curl, array(
            CURLOPT_URL => $login_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_COOKIEJAR => 'cookie_ghtk',
            CURLOPT_COOKIEFILE => '../../cookie/cookie.txt',
            CURLOPT_POSTFIELDS => "data[_Token][key]=" . $data['token'] . "&data[Shop][email]=" . $data['email'] . "&data[Shop][password]=" . $data['password'],
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
        ));
        curl_exec($curl);

        // đăng nhập thành công
        $acc_info_url = defined('GHTK_ACCOUNT_INFO_URL') ? GHTK_ACCOUNT_INFO_URL : self::$GHTK_ACCOUNT_INFO_URL;
        curl_setopt_array($curl, array(
            CURLOPT_URL => $acc_info_url,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_POST => false,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_RETURNTRANSFER => true,
        ));

        $info_user = curl_exec($curl);
        curl_close($curl);
        if ($info_user == "") {
            $json = [
                'status' => false,
                'message' => 'Tài khoản hoặc mật khẩu không đúng.',
                'data' => null
            ];

            $this->responseJson($json);
            return;
        }
        $docInfo = new DOMDocument();
        $internalErrors = libxml_use_internal_errors(true);
        $docInfo->loadHTML($info_user);
        libxml_clear_errors();

        $tokenAPI = $docInfo->getElementById('ShopTokenApi');
        if (empty($tokenAPI)) {
            $json = [
                'status' => false,
                'message' => 'Không tìm thấy ShopTokenApi',
                'data' => null
            ];

            $this->responseJson($json);
            return;
        }

        $token = $tokenAPI->getAttribute('value');

        $account_ghtk = [
            'ghtk_username' => $user_name,
            'ghtk_token' => $token
        ];
        $account_ghtk_id = $this->model_extension_appstore_trans_ons_ghtk->addInfoAccountGHTK($account_ghtk);

        /*config info ghtk app*/
        $this->model_setting_setting->editSettingValue('config', 'config_' . self::APP_CODE . '_token', isset($token) ? $token : '');

        $json = [
            'status' => true,
            'message' => '',
            'data' => [
                'disconnect_link' => $this->url->link('extension/appstore/trans_ons_ghtk/disConnect', 'user_token=' . $this->session->data['user_token'] . 'account_ghtk_id=' . $account_ghtk_id, true)
            ]
        ];

        $this->responseJson($json);
    }

    /**
     * @inheritDoc
     */
    public function disConnect($data = [])
    {
        $this->load->model('extension/appstore/trans_ons_ghtk');
        $account_ghtk_id = $this->request->get['account_ghtk_id'];

        $this->model_extension_appstore_trans_ons_ghtk->deleteAccountGHTKById((int)$account_ghtk_id);

         /*remove hub ghtk*/
        $this->load->model('setting/setting');
        $this->model_setting_setting->deleteSettingValue('config', 'config_' . self::APP_CODE . '_hub');

         /*remove token ghtk*/
        $this->model_setting_setting->deleteSettingValue('config', 'config_' . self::APP_CODE . '_token');

        $json = [
            'status' => true,
            'message' => 'Thoát kết nối.',
            'data' => null
        ];

        $this->responseJson($json);
    }

    /**
     * @inheritDoc
     */
    public function getDisplayName()
    {
        $this->registry->load->language('extension/appstore/trans_ons_ghtk');
        return $this->registry->language->get('display_name');
    }

    /**
     * @inheritDoc
     */
    public function getConfiguredPickingWarehouse(array $data = [], $type_html = false)
    {
        $pick_warehouse_url = defined('GHTK_GET_HUBS_URL') ? GHTK_GET_HUBS_URL : self::$GHTK_GET_HUBS_URL;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $pick_warehouse_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => array(
                "Token: " . $this->getTokenGHTK(),
            ),
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);

        /*get config_trans_ons_ghtk_hub trong setting table*/
        $this->load->model('setting/setting');
        $ghtk_hub_id = $this->config->get('config_' . self::APP_CODE . '_hub');

        $option = '';
        foreach ($response->data as $item) {
            if ($ghtk_hub_id == $item->pick_address_id) {
                $option = $option . "<option value='" . $item->pick_address_id . "' selected>" . $item->address . "</option>";
            } else {
                $option = $option . "<option value='" . $item->pick_address_id . "'>" . $item->address . "</option>";
            }
        }

        $json = [
            'status' => true,
            'message' => '',
            'data' => $option
        ];

        $this->responseJson($json);
    }

    /**
     * @inheritdoc
     */
    public function setMapConfiguredPickingWarehouse($data = [])
    {
        // DO NOT ALLOW read/write directly from setting!
        // TODO: create new db for GHTK, or open api...

        $this->registry->load->model('setting/setting');
        $this->registry->model_setting_setting->editSettingValue('config', 'config_' . self::APP_CODE . '_hub', isset($data['value']) ? $data['value'] : '');

        /* active ghtk */
        /* xem lai co nen de day ko? */
        $this->registry->load->model('setting/setting');
        $transport_config = is_null($this->registry->config->get('config_transport_active')) ? '' : $this->registry->config->get('config_transport_active');
        $data['delivery_method'] = self::APP_CODE;
        if ($transport_config != '') {
            $transport_list = explode(',', $transport_config);
            if (!in_array($data['delivery_method'], $transport_list)) {
                array_push($transport_list, $data['delivery_method']);
            }
        } else {
            $transport_list = [$data['delivery_method']];
        }

        $this->registry->model_setting_setting->editSettingValue('config', 'config_transport_active', implode(',', $transport_list));

        return true;
    }

    /**
     * @inheritdoc
     */
    public function getMapConfiguredPickingWarehouse($data = [])
    {
        // TODO: MUST implement...
    }

    /**
     * @inheritDoc
     */
    public function getListService(array $data)
    {
        $this->registry->load->language('extension/appstore/trans_ons_ghtk');

        $toProvince = $this->getProvinceNameByBestmeProvinceCode($data['transport_shipping_province']);
        $toDistrictID = $this->getDistrictNameByBestmeDistrictCode($data['transport_shipping_district']);

        $pick_address_id = $this->registry->config->get('config_' . self::APP_CODE . '_hub');

        $transport = 'fly';
        /* shipment */
        $data = array(
            "pick_address_id" => (int)$pick_address_id,
            "province" => $toProvince, // dia chi nhan hang
            "district" => $toDistrictID, // dia chi nhan hang
            "address" => $data['transport_shipping_full_address'],
            "weight" => (int)$data['total_weight'],
            "value" => (int)$data['total_pay'],
            "transport" => ''
        );

        $listItem = '';

        /* transport: fly */
        $data['transport'] = 'fly';
        $listItem .= $this->getItemService($data);

        /* transport: road */
        $data['transport'] = 'road';
        $listItem .= $this->getItemService($data);

        /* end shipment */
        $html = '';
        $html .= '<h5 class="entry-content-title">' . $this->getDisplayName() . '</h5><ul>';
        $html .= $listItem;
        $html .= "</ul>";

        return $html;
    }

    /**
     * @inheritdoc
     */
    public function getPrice(array $data)
    {
        // TODO: implement...
    }

    /**
     * @inheritdoc
     */
    public function createOrderDelivery(array $data)
    {
        $this->registry->load->language('sale/order');
        $this->registry->load->language('extension/appstore/trans_ons_ghtk');

        $FromHubID = $this->registry->config->get('config_' . self::APP_CODE . '_hub');

        $data['pick_address_id'] = '';
        $data['pick_tel'] = '';
        $data['address'] = '';
        $data['pick_name'] = '';
        $response = $this->getPickingWarehouse($data, false);
        if ($response->success) {
            $configuredPickingWarehouse = isset($response->data) && is_array($response->data) ? $response->data : [];
            foreach ($configuredPickingWarehouse as $hub) {
                if ($hub->PickAddressId == $FromHubID) {
                    $pick_address = explode(",", $hub->Address);
                    $count_pick_addresses = count($pick_address);
                    $data['pick_address_id'] = $hub->PickAddressId;
                    $data['pick_tel'] = $hub->PickTel;
                    $data['address'] = $hub->Address;
                    $data['pick_name'] = $hub->PickName;
                    $data['pick_province'] = isset($pick_address[$count_pick_addresses - 1]) ? $pick_address[$count_pick_addresses - 1] : '';
                    $data['pick_district'] = isset($pick_address[$count_pick_addresses - 2]) ? $pick_address[$count_pick_addresses - 2] : '';
                    $data['pick_ward'] = isset($pick_address[$count_pick_addresses - 3]) ? $pick_address[$count_pick_addresses - 3] : '';
                }
            }
        }

        /*thong tin khach nhan hang*/
        $province = $this->getProvinceNameByBestmeProvinceCode($data['transport_shipping_province']);
        $district = $this->getDistrictNameByBestmeDistrictCode($data['transport_shipping_district']);
        $ward = $this->getWardNameByBestmeWardCode($data['transport_shipping_wards']);

        if (empty($data['transport_shipping_district']) || empty($data['transport_shipping_wards']) || empty($data['transport_shipping_province'])) {
            return array(
                "status" => false,
                "msg" => $this->registry->language->get('ghtk_transport_error_address')
            );
        }

        $products = [
            [
                'name' => 'Bestme',
                'weight' => (int)$data['total_weight'] / 1000, // unit: g => kg
                'quantity' => 1,
                'product_code' => 'Bestme_product_code'
            ]
        ];

        // add shop_name to order_id
        $this->registry->load->model('setting/setting');
        $shop_name = $this->registry->model_setting_setting->getShopName();
        $bestme_order_code = $shop_name . '.' . $data['shipping_bill']; // notice: use order_code instead of order_id

        $orderData = [
            /*thong tin noi lay hang*/
            "id" => $bestme_order_code,
            // could not use FromHubId to call api. Temp use pick_address instead. TODO: use id...
            "pick_address" => $data['address'],
            "pick_province" => $data['pick_province'],
            "pick_district" => $data['pick_district'],
            "pick_ward" => $data['pick_ward'],
            "pick_name" => $data['pick_name'],
            "pick_tel" => $data['pick_tel'],

            /*thong tin nguoi nhan hang*/
            "tel" => $data['transport_shipping_phone'],
            "name" => $data['transport_shipping_fullname'],
            "address" => $data['shipping_address'],
            "province" => $province,
            "district" => $district,
            "ward" => $ward,
            "hamlet" => "Khác",

            "is_freeship" => "1",
            "pick_money" => (int)$data['collection_amount'],
            "note" => $data['transport_note'],
            "transport" => $data['service_name'],
            "value" => (int)($data['total_pay'] - $data['shipping_fee'])
        ];

        $order = json_encode([
            "products" => $products,
            "order" => $orderData
        ]);

        $create_order_url = defined('GHTK_CREATE_ORDER_URL') ? GHTK_CREATE_ORDER_URL : self::$GHTK_CREATE_ORDER_URL;
        $curl = curl_init();

        $curlopt_httpheader = [
            "Content-Type: application/json",
            "Token: " . $this->getTokenGHTKInternal(),
            "Content-Length: " . strlen($order),
        ];

        // add Bestme-GHTK token refer if defined
        if (defined('TRANSPORT_GHTK_TOKEN') && !empty(TRANSPORT_GHTK_TOKEN)) {
            $curlopt_httpheader[] = "X-Refer-Token: " . TRANSPORT_GHTK_TOKEN;
        }

        curl_setopt_array($curl, [
            CURLOPT_URL => $create_order_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $order,
            CURLOPT_HTTPHEADER => $curlopt_httpheader,
        ]);

        $response = json_decode(curl_exec($curl));
        curl_close($curl);

        if ($response->success) {
            $order_code = $response->order->label;
            $this->registry->load->model('sale/order');
            $data['transport_order_code'] = $order_code;
            $this->registry->model_sale_order->updateTransportDeliveryApi($data['order_id'], $order_code, $data['transport_method'], $data['service_name'], ucfirst($data['service_name']), '', $response->order->status_id, $data['address']);

            return [
                'status' => true,
            ];
        }

        // TODO: json?...
        return [
            "status" => false,
            "msg" => $response->message
        ];
    }

    /**
     * @inheritdoc
     */
    public function getOrderInfo(array $data)
    {
        $get_order_url = defined('GHTK_GET_ORDER_URL') ? GHTK_GET_ORDER_URL : self::$GHTK_GET_ORDER_URL;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $get_order_url . $data['transport_order_code'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => array(
                "Token: " . $this->getTokenGHTKInternal(),
            ),
        ));

        $response = json_decode(curl_exec($curl), true);
        curl_close($curl);
        $response['code'] = 0;

        if (isset($response['success']) && $response['success']) {
            $response['code'] = 1;
            $this->registry->load->model('sale/order');
            $this->registry->model_sale_order->updateStatusTransportDeliveryApi($response['order']['label_id'], $response['order']['status']);
        }

        return $response;
    }

    /**
     * @inheritdoc
     */
    public function cancelOrder(array $data)
    {
        $cancel_order_url = defined('GHTK_CANCEL_ORDER_URL') ? GHTK_CANCEL_ORDER_URL : self::$GHTK_CANCEL_ORDER_URL;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $cancel_order_url . $data['transport_order_code'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => array(
                "Token: " . $this->getTokenGHTKInternal(),
            ),
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);


        if ($response->success) {
            $this->registry->load->model('sale/order');
            $this->registry->model_sale_order->updateStatusDeliveryApi($data['data_order_id'], self::$GHTK_STATUS_ORDER_CANCEL);

            return [
                'status' => true,
            ];
        }

        $this->registry->load->language('common/delivery_api');

        return [
            "status" => false,
            "msg" => $this->registry->language->get('token_error')
        ];
    }

    /**
     * @inheritdoc
     */
    public function isCancelableOrder($order_status_code)
    {
        return in_array($order_status_code, self::SHIPPING_TRANSPORT_STATUES) || in_array($order_status_code, self::COMPLETE_TRANSPORT_STATUES);
    }

    /**
     * @inheritdoc
     */
    public function getTransportStatusMessage($status_code)
    {
        $this->registry->load->language('extension/appstore/trans_ons_ghtk');
        return $this->registry->language->get('ghtk_code_' . $status_code);
    }

    /**
     * @inheritdoc
     */
    public function saveCode()
    {
        $this->load->model('extension/appstore/trans_ons_ghtk');
        $data = $this->request->post;
        $accountPayment = [
            'vnp_web_code' => $data['vnp_web_code'],
            'vnp_hash_secret' => $data['vnp_hash_secret'],
        ];
        $account = $this->model_extension_appstore_trans_ons_ghtk->getAccountPayment();

        if ($account->num_rows > 0) {
            $accountId = $account->row['id'];
            // update
            $this->model_extension_appstore_trans_ons_ghtk->updateInfoAccountPayment($accountPayment, (int)$accountId);
        } else {
            // add
            $this->model_extension_appstore_trans_ons_ghtk->addInfoAccountPayment($accountPayment);
        }

        $this->session->data['success'] = true;

        $this->response->redirect($this->url->link('extension/appstore/trans_ons_ghtk', 'user_token=' . $this->session->data['user_token'], true));
    }

    /**
     * @inheritdoc
     */
    public function getProvinceCodeByBestmeProvinceCode($bestme_province_code)
    {
        // TODO: run offline then add to map file instead off online running...

        /* get bestme provice name */
        $vna = new \Vietnam_Administrative();
        $bestme_province = $vna->getProvinceByCode($bestme_province_code);
        $bestme_province_name = mb_strtolower($bestme_province['name']);
        $bestme_province_name_with_type = mb_strtolower($bestme_province['name_with_type']);

        // build set to check. e.g:
        // Hà Nội, Tỉnh Hà Nội, Tinh Hà Nội, TP Hà Nội, TP. Hà Nội, Thành phố Hà nội, ...
        $bestme_province_name_for_check = [
            escapeVietnamese($bestme_province_name),
            escapeVietnamese("Tỉnh $bestme_province_name"),
            escapeVietnamese("Tinh $bestme_province_name"),
            escapeVietnamese("TP $bestme_province_name"),
            escapeVietnamese("TP. $bestme_province_name"),
            escapeVietnamese("Thành phố $bestme_province_name"),
            escapeVietnamese($bestme_province_name_with_type)
        ];

        /* try to get province code from ghn */
        try {
            $provinces_raw = file_get_contents(self::JSON_FILE_PROVINCE);
            $result = json_decode($provinces_raw, true);
            if (json_last_error() === JSON_ERROR_NONE) {
                foreach ($result as $province) {
                    if (!is_array($province) || !array_key_exists('ProvinceID', $province) || !array_key_exists('ProvinceName', $province)) {
                        continue;
                    }

                    // compare
                    $province_name = mb_strtolower($province['ProvinceName']);
                    if (in_array(escapeVietnamese($province_name), $bestme_province_name_for_check)) {
                        return $province['ProvinceID'];
                    }
                }
            }
        } catch (Exception $e) {
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getDistrictCodeByBestmeDistrictCode($bestme_district_code, $province_code)
    {
        // TODO: run offline then add to map file instead off online running...

        /* get bestme provice name */
        $vna = new \Vietnam_Administrative();
        $bestme_district = $vna->getDistrictByCode($bestme_district_code);
        $bestme_district_name = mb_strtolower($bestme_district['name']);
        $bestme_district_name_with_type = mb_strtolower($bestme_district['name_with_type']);
        $bestme_district_name_in_int = (int)$bestme_district_name;

        // build set to check. e.g:
        // Hà Nội, Tỉnh Hà Nội, Tinh Hà Nội, TP Hà Nội, TP. Hà Nội, Thành phố Hà nội, ...
        $bestme_district_name_for_check = [
            escapeVietnamese($bestme_district_name),
            escapeVietnamese("Quận $bestme_district_name"),
            escapeVietnamese("Q $bestme_district_name"),
            escapeVietnamese("Q. $bestme_district_name"),
            escapeVietnamese("Huyện $bestme_district_name"),
            escapeVietnamese("Huyện đảo $bestme_district_name"),
            escapeVietnamese("H $bestme_district_name"),
            escapeVietnamese("H. $bestme_district_name"),
            escapeVietnamese("Thành phố $bestme_district_name"),
            escapeVietnamese("TP. $bestme_district_name"),
            escapeVietnamese("TP $bestme_district_name"),
            escapeVietnamese("Thị xã $bestme_district_name"),
            escapeVietnamese("TX $bestme_district_name"),
            escapeVietnamese("TX. $bestme_district_name"),
            escapeVietnamese($bestme_district_name_with_type),
            // again with $bestme_ward is number. E.g 03 (Quận 03) instead of 3 (Quận 3)
            escapeVietnamese($bestme_district_name_in_int),
            escapeVietnamese("Quận $bestme_district_name_in_int"),
            escapeVietnamese("Q $bestme_district_name_in_int"),
            escapeVietnamese("Q. $bestme_district_name_in_int"),
            escapeVietnamese("Huyện $bestme_district_name_in_int"),
            escapeVietnamese("Huyện đảo $bestme_district_name_in_int"),
            escapeVietnamese("H $bestme_district_name_in_int"),
            escapeVietnamese("H. $bestme_district_name_in_int"),
            escapeVietnamese("Thành phố $bestme_district_name_in_int"),
            escapeVietnamese("TP. $bestme_district_name_in_int"),
            escapeVietnamese("TP $bestme_district_name_in_int"),
            escapeVietnamese("Thị xã $bestme_district_name_in_int"),
            escapeVietnamese("TX $bestme_district_name_in_int"),
            escapeVietnamese("TX. $bestme_district_name_in_int"),
        ];

        /* try to get province code from ghn */
        $districts = self::getDistrictsByProvinceCode($province_code);
        foreach ($districts as $district) {
            if (!is_array($district) || !array_key_exists('DistrictID', $district) || !array_key_exists('DistrictName', $district)) {
                continue;
            }

            // compare
            $district_name = mb_strtolower($district['DistrictName']);
            if (in_array(escapeVietnamese($district_name), $bestme_district_name_for_check)) {
                return $district['DistrictID'];
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getWardCodeByBestmeWardCode($bestmeWardCode, $districtCode, $token)
    {
        // TODO: run offline then add to map file instead off online running...

        /* get bestme provice name */
        $vna = new \Vietnam_Administrative();
        $bestme_ward = $vna->getWardByCode($bestmeWardCode);
        $bestme_ward_name = mb_strtolower($bestme_ward['name']);
        $bestme_ward_name_with_type = mb_strtolower($bestme_ward['name_with_type']);
        $bestme_ward_name_in_int = (int)$bestme_ward_name;

        // build set to check. e.g:
        // Hà Nội, Tỉnh Hà Nội, Tinh Hà Nội, TP Hà Nội, TP. Hà Nội, Thành phố Hà nội, ...
        $bestme_district_name_for_check = [
            escapeVietnamese($bestme_ward_name),
            escapeVietnamese("Quận $bestme_ward_name"),
            escapeVietnamese("Q $bestme_ward_name"),
            escapeVietnamese("Q. $bestme_ward_name"),
            escapeVietnamese("Huyện $bestme_ward_name"),
            escapeVietnamese("Huyện đảo $bestme_ward_name"),
            escapeVietnamese("H $bestme_ward_name"),
            escapeVietnamese("H. $bestme_ward_name"),
            escapeVietnamese("Thành phố $bestme_ward_name"),
            escapeVietnamese("TP. $bestme_ward_name"),
            escapeVietnamese("TP $bestme_ward_name"),
            escapeVietnamese("Thị xã $bestme_ward_name"),
            escapeVietnamese("Thị trấn $bestme_ward_name"),
            escapeVietnamese("Phường $bestme_ward_name"),
            escapeVietnamese("TX $bestme_ward_name"),
            escapeVietnamese("Xã $bestme_ward_name"),
            escapeVietnamese("TX. $bestme_ward_name"),
            escapeVietnamese($bestme_ward_name_with_type),
            // again with $bestme_ward is number. E.g 03 (Phường 03) instead of 3 (Phường 3)
            escapeVietnamese($bestme_ward_name_in_int),
            escapeVietnamese("Quận $bestme_ward_name_in_int"),
            escapeVietnamese("Q $bestme_ward_name_in_int"),
            escapeVietnamese("Q. $bestme_ward_name_in_int"),
            escapeVietnamese("Huyện $bestme_ward_name_in_int"),
            escapeVietnamese("Huyện đảo $bestme_ward_name_in_int"),
            escapeVietnamese("H $bestme_ward_name_in_int"),
            escapeVietnamese("H. $bestme_ward_name_in_int"),
            escapeVietnamese("Thành phố $bestme_ward_name_in_int"),
            escapeVietnamese("TP. $bestme_ward_name_in_int"),
            escapeVietnamese("TP $bestme_ward_name_in_int"),
            escapeVietnamese("Thị xã $bestme_ward_name_in_int"),
            escapeVietnamese("Thị trấn $bestme_ward_name_in_int"),
            escapeVietnamese("Phường $bestme_ward_name_in_int"),
            escapeVietnamese("TX $bestme_ward_name_in_int"),
            escapeVietnamese("Xã $bestme_ward_name_in_int"),
            escapeVietnamese("TX. $bestme_ward_name_in_int"),
        ];

        /* try to get province code from ghn */
        $districts = self::getWardsByDistrictCode($districtCode, $token);
        foreach ($districts as $district) {
            if (!is_array($district) || !array_key_exists('WardCode', $district) || !array_key_exists('WardName', $district)) {
                continue;
            }

            // compare
            $district_name = mb_strtolower($district['WardName']);
            if (in_array(escapeVietnamese($district_name), $bestme_district_name_for_check)) {
                return $district['WardCode'];
            }
        }

        return null;
    }

    /* === private functions === */

    /**
     * @param string|int $province_code
     * @return array
     */
    public static function getDistrictsByProvinceCode($province_code)
    {
        $result = [];

        try {
            $district_raw = file_get_contents(self::JSON_FILE_DISTRICT);
            $districts = json_decode($district_raw, true);
            if (json_last_error() === JSON_ERROR_NONE) {
                foreach ($districts as $district) {
                    if (!is_array($district) ||
                        !array_key_exists('ProvinceID', $district) ||
                        !array_key_exists('DistrictID', $district) ||
                        !array_key_exists('DistrictName', $district)
                    ) {
                        continue;
                    }

                    // filter by $province_code
                    if ($district['ProvinceID'] != $province_code) {
                        continue;
                    }

                    $result[$district['DistrictID']] = [
                        'DistrictID' => $district['DistrictID'],
                        'DistrictName' => $district['DistrictName']
                    ];
                }
            }
        } catch (Exception $e) {
        }

        return $result;
    }

    /**
     * @param string $district_code
     * @param string $token
     * @return mixed
     */
    public static function getWardsByDistrictCode($district_code, $token)
    {
        return $district_code;
    }

    /**
     *
     */
    public function checkStatusDelivery() {
        $this->registry->load->language('extension/appstore/trans_ons_ghtk');
        $resp = [
            'status' => true,
            'mes' => ''
        ];

        $token = $this->getToken();
        if (!isset($token) || empty($token)) {
            $resp = [
                'status' => false,
                'mes' => [
                    'app_code' => $this->registry->language->get(self::APP_CODE),
                    'mes' => $this->registry->language->get('not_login')
                ]
            ];

            return $resp;
        }

        $hub = $this->getHub();
        if (!isset($hub) || empty($hub)) {
            $resp = [
                'status' => false,
                'mes' => [
                    'app_code' => $this->registry->language->get(self::APP_CODE),
                    'mes' => $this->registry->language->get('not_map_address')
                ]
            ];

            return $resp;
        }

        return  $resp;
    }

    /**
     * @inheritdoc
     * @return mixed|void
     */
    public function isConnected()
    {
        $token = $this->getToken();
        return !is_null($token);
    }

    /**
     * @inheritdoc
     * @param array $data
     */
    public function parseWebhookResponse(array $data) {
        return [
            'order_status_id' => $this->getOrderStatus(self::TRANSPORT_STATUSES, $data['status_id']),
            'transport_order_code' => $data['label_id'],
            'transport_status' => $data['status_id']
        ];
    }

    /* === private functions */

    /**
     * getProvinceNameByBestmeProvinceCode
     *
     * @param $bestme_province_code
     * @return false|string|string[]|null
     */
    private function getProvinceNameByBestmeProvinceCode($bestme_province_code)
    {
        // TODO: run offline then add to map file instead off online running...

        /* get bestme provice name */
        $vna = new \Vietnam_Administrative();
        $bestme_province = $vna->getProvinceByCode($bestme_province_code);
        $bestme_province_name = mb_strtolower($bestme_province['name']);

        return $bestme_province_name;
    }

    /**
     * getDistrictNameByBestmeDistrictCode
     *
     * @param int|string $bestme_district_code
     * @param int|string $province_code
     * @return int|string|void|null
     */
    private function getDistrictNameByBestmeDistrictCode($bestme_district_code)
    {
        // TODO: run offline then add to map file instead off online running...

        /* get bestme provice name */
        $vna = new \Vietnam_Administrative();
        $bestme_district = $vna->getDistrictByCode($bestme_district_code);
        $bestme_district_name = mb_strtolower($bestme_district['name']);
        return $bestme_district_name;
    }

    /**
     * @param $bestmeWardCode
     * @param $districtCode
     * @param $token
     * @return mixed|null
     */
    private function getWardNameByBestmeWardCode($bestmeWardCode)
    {
        // TODO: run offline then add to map file instead off online running...

        /* get bestme provice name */
        $vna = new \Vietnam_Administrative();
        $bestme_ward = $vna->getWardByCode($bestmeWardCode);
        $bestme_ward_name = mb_strtolower($bestme_ward['name']);

        return $bestme_ward_name;
    }

    private function getItemService($data)
    {
        $get_shipping_service_url = defined('GHTK_SHIPPING_SERVICES_ORDER_URL') ? GHTK_SHIPPING_SERVICES_ORDER_URL : self::$GHTK_SHIPPING_SERVICES_ORDER_URL;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $get_shipping_service_url . '?' . http_build_query($data),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => array(
                "Token: " . $this->getTokenGHTKInternal(),
            ),
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);
        /* end shipment */
        $html = '';
        if ($response->success) {
            $html .= '<li class="select-shipping" data-service-name="' . $data['transport'] . '" data-delivery-method="' . self::APP_CODE . '" data-service_id="' . $response->fee->cost_id . '">' . $this->getDisplayName() . ' - ' . $data['transport'] . ' <span class="shipping-price">' . number_format($response->fee->fee, 0, '', ',') . 'đ' . '</span></li>';
        }

        return $html;
    }

    private function getPickingWarehouse(array $data, $type_html = false)
    {
        $pick_warehouse_url = defined('GHTK_GET_HUBS_URL') ? GHTK_GET_HUBS_URL : self::$GHTK_GET_HUBS_URL;
        $curl = curl_init($pick_warehouse_url);

        curl_setopt_array($curl, array(
            CURLOPT_URL => $pick_warehouse_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => array(
                "Token: " . $this->getTokenGHTKInternal(),
            ),
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);

        $result = new \stdClass();

        $result->code = $response->success ? 1 : 0;
        $result->success = $response->success;
        $result->msg = $response->message;
        $result->data = [];

        if ($response->success == 200) {
            foreach ($response->data as $shop) {
                $hub = new \stdClass();
                $hub->PickAddressId = $shop->pick_address_id;
                $hub->PickTel = $shop->pick_tel;
                $hub->Address = $shop->address;
                $hub->PickName = $shop->pick_name;

                $result->data[] = $hub;
            }
        }

        return $result;
    }

    private function getToken() {
        $this->registry->load->model('setting/setting');
        $token_ghtk = $this->registry->model_setting_setting->getSettingValue('config_' . self::APP_CODE . '_token');

        return $token_ghtk;
    }

    private function getHub() {
        $this->registry->load->model('setting/setting');
        $hub_ghtk = $this->registry->model_setting_setting->getSettingValue('config_' . self::APP_CODE . '_hub');

        return $hub_ghtk;
    }

    private function getTokenGHTK() {
        $this->load->model('setting/setting');
        $token_ghtk = $this->model_setting_setting->getSettingValue('config_' . self::APP_CODE . '_token');

        return $token_ghtk;
    }

    private function getTokenGHTKInternal() {
        $this->registry->load->model('setting/setting');
        $token_ghtk = $this->registry->model_setting_setting->getSettingValue('config_' . self::APP_CODE . '_token');

        return $token_ghtk;
    }
}
