<?php

abstract class ControllerExtensionAppstoreTransAbstract extends Controller
{
    /* common app */
    /**
     * install app
     *
     * @return mixed
     */
    public abstract function install();

    /**
     * uninstall app
     *
     * @return mixed
     */
    public abstract function uninstall();

    public abstract function index($data = []);

    public abstract function introduce();

    /* transport */
    public abstract function login($data = []);

    public abstract function disConnect($data = []);

    /**
     * @return string
     */
    public abstract function getDisplayName();

    /**
     * get addresses for picking order. e.g:
     * - So 1 ngo 2 Duy Tan CG HN
     * - So 3 ngo 4 Thai Thuy Thai Binh
     *
     * @param array $data
     * @param bool $type_html
     * @return mixed
     */
    public abstract function getConfiguredPickingWarehouse(array $data = [], $type_html = false);

    /**
     * @param array $data
     * @return bool
     */
    public abstract function setMapConfiguredPickingWarehouse($data = []);

    /**
     * @param array $data
     * @return mixed
     */
    public abstract function getMapConfiguredPickingWarehouse($data = []);

    /**
     * get list services with price. e.g:
     * - GHN - normal - 25.000vnd
     * - GHN - fast - 55.000vnd
     * - VTPost - super fast - 200.000vnd
     *
     * @param array $data
     * @return mixed
     */
    public abstract function getListService(array $data);

    public abstract function getPrice(array $data);

    public abstract function createOrderDelivery(array $data);

    public abstract function getOrderInfo(array $data);

    public abstract function cancelOrder(array $data);

    /**
     * @param mixed $order_status_code
     * @return bool
     */
    public abstract function isCancelableOrder($order_status_code);

    /**
     * @param string $status_code
     * @return mixed|string
     */
    public abstract function getTransportStatusMessage($status_code);

    /**
     * ?
     *
     * TODO: remove...
     *
     * @return mixed
     */
    public abstract function saveCode();

    /**
     * get Province Code By Bestme Province Code
     *
     * e.g Bestme Province code = 89 => "Ha Noi" Province => 3rd Province code = 24 ~ "Ha Noi"
     *
     * @param string|int $bestmeProvinceCode
     * @return string|int|null
     */
    public abstract function getProvinceCodeByBestmeProvinceCode($bestmeProvinceCode);

    /**
     * get District Code By Bestme District Code
     *
     * e.g
     * - Bestme District code = 89012, Province code = 89
     * => "Thanh Xuan" Province (find in 3rd Province code = 24 ~ "Ha Noi")
     * => 3rd District code = 1253 ~ "Thanh Xuan"
     *
     * @param string|int $bestmeDistrictCode
     * @param string|int $provinceCode MUST, for finding in small district set due to provinceCode => avoid wrong district name
     * @return string|int|null
     */
    public abstract function getDistrictCodeByBestmeDistrictCode($bestmeDistrictCode, $provinceCode);

    /**
     * get Ward Code By Bestme Ward Code
     *
     * e.g
     * - Bestme Ward code = 890125, District code = 1253
     * - => "Khuong Trung" Province (find in 3rd District code = 1253 ~ "Thanh Xuan")
     * - => 3rd Ward code = 2635 ~ "Khuong Trung"
     *
     * @param $bestmeWardCode
     * @param string|int $districtCode MUST, for finding in small ward set due to districtCode => avoid wrong ward name
     * @param $token
     * @return mixed
     */
    public abstract function getWardCodeByBestmeWardCode($bestmeWardCode, $districtCode, $token);

    /**
     * check if is app connected
     *
     * @return mixed
     */
    public abstract function isConnected();

    /**
     * parse webhook response data
     * @param array $data
     */
    public abstract function parseWebhookResponse(array $data);
}