<?php

use App_Api\App_Config;
use App_Api\App_Setting;

class ControllerExtensionAppstoreMarOnsChatbot extends Controller
{
    private $error = array();

    const CONFIG_CHATBOT_NOVAON_API_KEY = 'config_chatbot_novaon';
    public function index()
    {
        $this->load->model('appstore/my_app');
        if(!$this->model_appstore_my_app->checkAppActive(ModelAppstoreMyApp::APP_CHAT_BOT)){
            $this->response->redirect($this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true));
        };
        $this->load->language('extension/appstore/mar_ons_chatbot');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        /* temp not supported edit api key */
        if (false && ($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            try {
                $this->model_setting_setting->editSettingValue('config', self::CONFIG_CHATBOT_NOVAON_API_KEY, $this->request->post['api_key']);
                $this->session->data['success'] = $this->language->get('success_change_api_key');
            } catch (Exception $e) {
                $this->error['warning'] = $this->language->get('error_change_meta');
            }

            $this->response->redirect($this->url->link('extension/appstore/mar_ons_chatbot', 'user_token=' . $this->session->data['user_token'], true));
        }

        /* breadcrumb */
        $data['breadcrumbs'] = array();

        /* message */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        /* model data */
        $api_key = $this->model_setting_setting->getSettingValue(self::CONFIG_CHATBOT_NOVAON_API_KEY);
        if (empty($api_key)) {
            // auto create api key if not has
            $api_key = token(32);
            $this->model_setting_setting->editSettingValue($code = 'config', self::CONFIG_CHATBOT_NOVAON_API_KEY, $api_key);
        }
        $data['api_key'] = $api_key;

        $data['action'] = $this->url->link('online_store/google_shopping', 'user_token=' . $this->session->data['user_token'], true);

        /* Novaon chatbot homepage */
        $data['href_chatbot_novaon'] = CHATBOT_NOVAON_HOMEPAGE;
        $data['href_chatbot_novaon_guide'] = CHATBOT_NOVAON_GUIDE;

        /* common */
        $data['href_add_domain'] = $this->url->link('online_store/domain/add', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_delete_domain'] = $this->url->link('online_store/domain/delete', 'user_token=' . $this->session->data['user_token'], true);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');

        $this->response->setOutput($this->load->view('extension/appstore/mar_ons_chatbot/mar_ons_chatbot', $data));
    }

    private function validateForm()
    {
        if (!$this->user->hasPermission('modify', 'extension/appstore/mar_ons_chatbot')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        /* domain */
        if (!isset($this->request->post['api_key'])) {
            $this->error['api_key'] = $this->language->get('error_api_key');
        }

        return !$this->error;
    }

    function introduce()
    {
        $this->index();
    }

    function registerApp($data)
    {
        $config = new App_Config($this->registry);
        $config->registerApp($data);
    }

    function install()
    {
        $data = [
            "app_name"  => "NovaonX ChatBot",
            "path_logo" => "view/image/appstore/mar_ons_chatbot.jpg"
        ];

        $setting = new App_Setting($this->registry);
        $setting->updateInfo("mar_ons_chatbot", $data);
    }

    function uninstall()
    {
        $config = new App_Config($this->registry);
        $config->unRegisterApp('mar_ons_chatbot');
    }
}
