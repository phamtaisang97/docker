<?php


use App_Api\App_Config;
use App_Api\App_Setting;
use App_Api\Product_Field;
use App_Api\Query_App_Builder;
use App_Api\Table_Name;

class ControllerExtensionAppstoreAppPopup extends Controller
{
    private $error = array();

    function index()
    {
        $this->load->language('extension/appstore/app_popup');
        $this->document->setTitle($this->language->get('heading_title'));
        /* Hiển thị danh sách các module( Chương trình ) của app */
        $module_data = array();
        $setting = new App_Setting($this->registry);
        $modules = $setting->getModulesByCode('app_popup');

        foreach ($modules as $module) {
            if ($module['setting']) {
                $setting_info = json_decode($module['setting'], true);
            } else {
                $setting_info = array();
            }

            $module_data[] = array(
                'module_id' => $module['module_id'],
                'name' => $module['name'],
                'status' => (isset($setting_info['status']) && $setting_info['status']) ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
                'edit' => $this->url->link('extension/appstore/app_popup/module', 'user_token=' . $this->session->data['user_token'] . '&module_id=' . $module['module_id'], true),
                'delete' => $this->url->link('extension/appstore/app_popup/deleteModule', 'user_token=' . $this->session->data['user_token'] . '&module_id=' . $module['module_id'], true)
            );
        }
        $data['modules'] = $module_data;
        $data['action_add'] = $this->url->link('extension/appstore/app_popup/module', 'user_token=' . $this->session->data['user_token'], true);

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/appstore/app_popup/app_popup', $data));
    }

    function module()
    {
        $this->load->language('extension/appstore/app_popup');
        $setting = new App_Setting($this->registry);
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            if (!isset($this->request->get['module_id'])) {
                $setting->addModule('app_popup', $this->request->post);
                $this->session->data['success'] = "Add Module Success !";
                $this->response->redirect($this->url->link('extension/appstore/app_popup', 'user_token=' . $this->session->data['user_token'], true));
            } else {
                $setting->editModule($this->request->get['module_id'], $this->request->post);
            }
            $this->session->data['success'] = $this->language->get('text_success');
            $this->response->redirect($this->url->link('extension/appstore/app_popup', 'user_token=' . $this->session->data['user_token'], true));
        }
        if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $module_info = $setting->getModule($this->request->get['module_id']);
            $data['name'] = $module_info['name'];
            $data['status'] = $module_info['status'];

            $data['action'] = $this->url->link('extension/appstore/app_popup/module', 'user_token=' . $this->session->data['user_token'] .
                '&module_id=' . $this->request->get['module_id'], true);
            $config = [
                'app' => 'app_popup',
                'module_id' => $this->request->get['module_id']
            ];
            $data['app_config'] = $this->load->controller('common/app_config/setTheme', $config);
            $data['custom_header'] = $this->load->controller('common/custom_header');
            $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
            $data['footer'] = $this->load->controller('common/footer');
            $this->response->setOutput($this->load->view('extension/appstore/app_popup/edit_module', $data));
        }
    }

    function deleteModule()
    {
        // Khai báo thư viện
        $setting = new App_Setting($this->registry);
        // Delete module
        $setting->deleteModule($this->request->get['module_id']);
        $this->response->redirect($this->url->link('extension/appstore/app_popup', 'user_token=' . $this->session->data['user_token'], true));
    }

    function validate()
    {
        if (!$this->user->hasPermission('modify', 'extension/appstore/app_popup')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
            $this->error['name'] = $this->language->get('error_name');
        }

        return !$this->error;
    }

    function introduce()
    {
        $this->load->language('extension/appstore/app_popup');
        $this->document->setTitle($this->language->get('heading_title_description'));

        // Load logo app
        $data['app_logo'] = "view/image/appstore/app_popup.jpg";

        // Link to detail app
        $data['link_to_app'] = $this->url->link('extension/appstore/app_popup', 'user_token=' . $this->session->data['user_token'], true);

        // Import default layoyt
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        // Return view of introduce page
        $this->response->setOutput($this->load->view('extension/appstore/app_popup/introduce', $data));
    }

    function install()
    {
        // Update app_name & Logo
        $data = [
            "app_name" => "App Popup",
            "path_logo" => "view/image/appstore/app_popup.jpg"
        ];

        $setting = new App_Setting($this->registry);
        $setting->updateInfo("app_popup", $data);
        $asset = [
            "css" => [
                "catalog/view/theme/default/template/extension/appstore/asset/css/app_popup.css"
            ],
            "js" => [
                "catalog/view/theme/default/template/extension/appstore/asset/js/app_popup.js"
            ]
        ];
        $setting->addAsset("app_popup", $asset);
    }

    function uninstall()
    {
        /* Gỡ ứng dụng */
        $config = new App_Config($this->registry);
        $config->unRegisterApp('app_popup');
    }
}
