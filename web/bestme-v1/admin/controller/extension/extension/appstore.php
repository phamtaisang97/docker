<?php
class ControllerExtensionExtensionAppstore extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('extension/extension/appstore');

        $this->load->model('setting/extension');

        $this->load->model('setting/appstore_setting');

        $this->getList();
    }

    public function install() {
        $this->load->language('extension/extension/appstore');

        $this->load->model('setting/extension');

        $this->load->model('setting/appstore_setting');

        if ($this->validate()) {
            $this->model_setting_extension->install('appstore', $this->request->get['extension']);

            $this->load->model('user/user_group');

            $this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'extension/appstore/' . $this->request->get['extension']);
            $this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'extension/appstore/' . $this->request->get['extension']);

            // Call install method if it exsits
            $this->load->controller('extension/appstore/' . $this->request->get['extension'] . '/install');

            $this->session->data['success'] = $this->language->get('text_success');
        } else {
            $this->session->data['error'] = $this->error['warning'];
        }

        $this->getList();
    }

    public function uninstall() {
        $this->load->language('extension/extension/appstore');

        $this->load->model('setting/extension');

        $this->load->model('setting/appstore_setting');

        if ($this->validate()) {
            $this->model_setting_extension->uninstall('appstore', $this->request->get['extension']);

            $this->model_setting_appstore_setting->deleteModulesByCode($this->request->get['extension']);

            // Call uninstall method if it exsits
            $this->load->controller('extension/appstore/' . $this->request->get['extension'] . '/uninstall');

            $this->session->data['success'] = $this->language->get('text_success');
        }

        $this->getList();
    }

    public function add() {
        $this->load->language('extension/extension/appstore');

        $this->load->model('setting/extension');

        $this->load->model('setting/appstore_setting');

        if ($this->validate()) {
            $this->load->language('appstore' . '/' . $this->request->get['extension']);

            $this->model_setting_appstore_setting->addModule($this->request->get['extension'], $this->language->get('heading_title'));

            $this->session->data['success'] = $this->language->get('text_success');
        }

        $this->getList();
    }

    public function delete() {
        $this->load->language('extension/extension/appstore');

        $this->load->model('setting/extension');

        $this->load->model('setting/appstore_setting');

        if (isset($this->request->get['module_id']) && $this->validate()) {
            $this->model_setting_appstore_setting->deleteModule($this->request->get['module_id']);

            $this->session->data['success'] = $this->language->get('text_success');
        }

        //$this->getList();
        $redirect = $this->url->link('app_store/my_app','user_token=' . $this->session->data['user_token'],true);
        $this->response->redirect($redirect);
    }

    protected function getList() {
        $data['text_layout'] = sprintf($this->language->get('text_layout'), $this->url->link('design/layout', 'user_token=' . $this->session->data['user_token'], true));

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $extensions = $this->model_setting_extension->getInstalled('appstore');

        foreach ($extensions as $key => $value) {
            if (!is_file(DIR_APPLICATION . 'controller/extension/appstore/' . $value . '.php') && !is_file(DIR_APPLICATION . 'controller/appstore/' . $value . '.php')) {
                $this->model_setting_extension->uninstall('appstore', $value);

                unset($extensions[$key]);

                $this->model_setting_appstore_setting>deleteModulesByCode($value);
            }
        }

        $data['extensions'] = array();

        // Create a new language container so we don't pollute the current one
        $language = new Language($this->config->get('config_language'));

        // Compatibility code for old extension folders
        $files = glob(DIR_APPLICATION . 'controller/extension/appstore/*.php');

        if ($files) {
            foreach ($files as $file) {
                $extension = basename($file, '.php');

                $this->load->language('extension/appstore/' . $extension, 'extension');

                $module_data = array();

                $modules = $this->model_setting_appstore_setting->getModulesByCode($extension);
                foreach ($modules as $module) {
                    if ($module['setting']) {
                        $setting_info = json_decode($module['setting'], true);
                    } else {
                        $setting_info = array();
                    }

                    $module_data[] = array(
                        'module_id' => $module['module_id'],
                        'name'      => $module['name'],
                        'status'    => (isset($setting_info['status']) && $setting_info['status']) ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
                        'edit'      => $this->url->link('extension/appstore/' . $extension, 'user_token=' . $this->session->data['user_token'] . '&module_id=' . $module['module_id'], true),
                        'delete'    => $this->url->link('extension/extension/appstore/delete', 'user_token=' . $this->session->data['user_token'] . '&module_id=' . $module['module_id'], true)
                    );
                }

                $data['extensions'][] = array(
                    'name'      => $this->language->get('extension')->get('heading_title'),
                    'status'    => $this->config->get('module_' . $extension . '_status') ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
                    'module'    => $module_data,
                    'install'   => $this->url->link('extension/extension/appstore/install', 'user_token=' . $this->session->data['user_token'] . '&extension=' . $extension, true),
                    'uninstall' => $this->url->link('extension/extension/appstore/uninstall', 'user_token=' . $this->session->data['user_token'] . '&extension=' . $extension, true),
                    'installed' => in_array($extension, $extensions),
                    'edit'      => $this->url->link('extension/appstore/' . $extension, 'user_token=' . $this->session->data['user_token'], true)
                );
            }
        }

        $sort_order = array();

        foreach ($data['extensions'] as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $data['extensions']);

        $this->response->setOutput($this->load->view('extension/extension/appstore', $data));
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'extension/extension/appstore')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }
}
