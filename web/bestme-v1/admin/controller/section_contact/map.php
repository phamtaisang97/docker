<?php

class ControllerSectionContactMap extends Controller
{
    use Theme_Config_Util;

    public function index()
    {
        $data = array();

        $this->load->language('section/contact/map');

        $action = isset($this->request->get['action']) ? $this->request->get['action'] : 'config';
        if (in_array($action, ['config', 'undo', 'redo', 'change'])) {
            $data['config'] = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $data['config'] = is_array($data['config']) ? $data['config'] : json_decode($data['config'], true);
        }

        $data['href_map_display_address'] = 'https://www.google.com/maps/place/Ph%E1%BB%91+Duy+T%C3%A2n,+H%C3%A0+N%E1%BB%99i,+Vietnam/@21.0315501,105.7818414,16.75z/data=!4m5!3m4!1s0x3135ab4c76b12a3b:0x9a311c833456d5f0!8m2!3d21.0309274!4d105.7840373';

        // undo-redo page
        $data['href_change'] = $this->url->link('section_contact/map', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_map&action=change', true);
        $data['href_undo'] = $this->url->link('section_contact/map', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_map&action=undo', true);
        $data['href_redo'] = $this->url->link('section_contact/map', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_map&action=redo', true);

        /* fix displaying selected page on header */
        $this->load->language('theme/preview');
        $data['selected_page'] = $this->getSelectedPageDueToRoute($this->request->get['route']);

        /* base */
        $data['href_exit'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_homepage'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $data['href_category'] = $this->url->link('section_category/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_sections', true);
        $data['href_product_detail'] = $this->url->link('section_product_detail/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_detail_sections', true);
        $data['href_blog'] = $this->url->link('section_blog/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog_sections', true);
        $data['href_contact'] = $this->url->link('section_contact/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_sections', true);
        $data['preview'] = $this->load->controller('theme/preview');
        $data['href_section_theme'] = $this->load->controller('theme/section_theme');
        $data['back_section'] = $this->url->link('section_contact/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_sections', true);

        $this->response->setOutput($this->load->view('section/contact/map', $data));
    }
}