<?php

class ControllerSectionContactSections extends Controller
{
    use Theme_Config_Util;
    use Device_Util;

    public function index()
    {
        $data = array();

        $this->load->language('section/contact/sections');
        $data['banner_idx'] = 0;
        if(isset($this->request->get['idx'])){
            $data['banner_idx'] = $this->request->get['idx'];
        }
        // param url section
        $data['url_header'] = $this->url->link('section/header', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_header', true);
        $data['url_footer'] = $this->url->link('section/footer', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_footer', true);

        /* config with change api */
        $data['href_change'] = $this->url->link('section_contact/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_sections&action=change', true);

        /* config from db */
        $action = isset($this->request->get['action']) ? $this->request->get['action'] : 'config';
        if (in_array($action, ['config', 'undo', 'redo', 'change'])) {
            $data['config'] = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $data['config'] = is_array($data['config']) ? $data['config'] : json_decode($data['config'], true);
        }

        $data['href_section_theme'] = $this->url->link('theme/section_theme', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_section_theme', true);

        /* fix displaying selected page on header */
        $this->load->language('theme/preview');
        $data['selected_page'] = $this->getSelectedPageDueToRoute($this->request->get['route']);

        /* base */
        $data['href_exit'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_homepage'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $data['href_category'] = $this->url->link('section_category/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_sections', true);
        $data['href_product_detail'] = $this->url->link('section_product_detail/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_detail_sections', true);
        $data['href_blog'] = $this->url->link('section_blog/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog_sections', true);
        $data['href_contact'] = $this->url->link('section_contact/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_sections', true);
        $data['preview'] = $this->load->controller('theme/preview');
        $data['is_on_mobile'] = $this->isMobile();

        $this->response->setOutput($this->load->view('section/contact/sections', $data));
    }
}