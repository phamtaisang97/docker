<?php

class ControllerOnlineStoreDomain extends Controller
{
    use Sync_Bestme_Data_To_Welcome_Util;

    private $error = array();

    const CONFIG_DOMAINS = 'config_domains';
    const CONFIG_SHOP_NAME = 'shop_name';
    const CONFIG_SHOP_NAME_DEFAULT = 'cuahang';
    const CONFIG_MAX_DOMAIN = 15;
    const SHOP_NAME_SUFFIX = '.bestme.asia'; // already in admin config. TODO: remove...
    const ON_BOARDING_STEP = 3;

    public function index()
    {
        /*$result = dns_get_record("www.lixishop.com");
        var_dump($result);

        $result = dns_get_record("lixishop.com");
        var_dump($result);
        die;*/

        $this->load->language('online_store/domain');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        /* breadcrumb */
        $data['breadcrumbs'] = [];

        /* message */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        // on boarding step
        $config_on_boarding_step_active = $this->config->get('config_onboarding_step_active');
        $on_boarding_step_active = empty($config_on_boarding_step_active) ? [] : explode(',', $config_on_boarding_step_active);
        if (!is_null($config_on_boarding_step_active) && !in_array(self::ON_BOARDING_STEP, $on_boarding_step_active)) {
            array_push($on_boarding_step_active, self::ON_BOARDING_STEP);
            $this->model_setting_setting->editSettingValue('config', 'config_onboarding_step_active', implode(',', $on_boarding_step_active));
        }

        /* publish sync data */
        $shop_name = $this->model_setting_setting->getShopName();
        $onboarding_data = [
            'shop_domain' => $shop_name,
            'last_step' => self::ON_BOARDING_STEP,
            'steps' => is_array($on_boarding_step_active) ? count($on_boarding_step_active) : 0
        ];
        $this->publishSyncDataOnboarding($onboarding_data);

        $data['setting_domains'] = $this->getSettingDomainValue();
        $shop_name = $this->model_setting_setting->getSettingValue(self::CONFIG_SHOP_NAME);
        if ($shop_name) {
            $data['bestme_domain'] = $shop_name . SHOP_NAME_SUFFIX;
        } else {
            $data['bestme_domain'] = self::CONFIG_SHOP_NAME_DEFAULT . SHOP_NAME_SUFFIX;
        }
        $data['bestme_ip'] = defined('BESTME_IP') ? BESTME_IP : '';

        if (isset($this->session->data['domain_add_success'])) {
            $data['domain_add_success'] = $this->session->data['domain_add_success'];

            unset($this->session->data['domain_add_success']);
        }

        if (isset($this->session->data['domain_remove_success'])) {
            $data['domain_remove_success'] = $this->session->data['domain_remove_success'];

            unset($this->session->data['domain_remove_success']);
        }

        $data['txt_domain_auth_des_31'] = sprintf($this->language->get('txt_domain_auth_des_3'), $this->language->get('cname_record'), $this->language->get('cname_host_record'), $this->language->get('cname_type'), $data['bestme_domain']);
        $data['txt_domain_auth_des_32'] = sprintf($this->language->get('txt_domain_auth_des_3'), $this->language->get('a_record'), $this->language->get('a_host_record'), $this->language->get('a_type'), $data['bestme_ip']);
        $data['video_domain'] = defined('BESTME_DOMAIN_GUIDE') ? BESTME_DOMAIN_GUIDE : '';
        /* common */
        $data['href_add_domain'] = $this->url->link('online_store/domain/add', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_delete_domain'] = $this->url->link('online_store/domain/delete', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_redirect_change'] = $this->url->link('online_store/domain/changeRedirect', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_main_domain_change'] = $this->url->link('online_store/domain/changeMainDomain', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_main_domain_auth'] = $this->url->link('online_store/domain/domainAuth', 'user_token=' . $this->session->data['user_token'], true);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('online_store/domain', $data));
    }

    public function add()
    {
        $this->load->language('online_store/domain');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            /* get existing delivery config */
            $config_domains = $this->getSettingDomainValue();

            $domain = $this->mapDomainData();

            $domains = (isset($config_domains['domains']) && is_array($config_domains['domains'])) ? $config_domains['domains'] : [];
            $domains[] = $domain;
            $config_domains['domains'] = $domains;
            $this->model_setting_setting->editSettingValue('config', self::CONFIG_DOMAINS, $config_domains);

            $this->session->data['domain_add_success'] = $this->language->get('txt_save_domain_success');

            $this->response->redirect($this->url->link('online_store/domain', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->index();
    }

    public function delete()
    {
        $this->load->language('online_store/domain');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateDelete()) {
            /* get existing delivery config */
            $config_domains = $this->getSettingDomainValue();

            $domains = (isset($config_domains['domains']) && is_array($config_domains['domains'])) ? $config_domains['domains'] : [];
            foreach ($domains as $key => $domain) {
                if (!isset($domain['domain'])) {
                    continue;
                }
                if ($domain['domain'] == $this->request->post['domain']) {
                    unset($domains[$key]);
                }
            }
            unset($domain);
            $config_domains['domains'] = $domains;
            $this->model_setting_setting->editSettingValue('config', self::CONFIG_DOMAINS, $config_domains);

            // redis
            try {
                $this->domainPublish();
            } catch (\Exception $e) {
            }
            $this->session->data['domain_remove_success'] = $this->language->get('txt_remove_domain_success');

            $this->response->redirect($this->url->link('online_store/domain', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->index();
    }

    public function changeRedirect()
    {
        $this->load->language('online_store/domain');

        /* get existing delivery config */
        $config_domains = $this->getSettingDomainValue();

        $redirect = isset($config_domains['redirect_to_main_domain']) ? $config_domains['redirect_to_main_domain'] : 0;
        $main_domain = isset($config_domains['main_domain']) ? $config_domains['main_domain'] : '';
        if ($main_domain == null && !$redirect) {
            $this->load->model('setting/setting');

            $shop_name = $this->model_setting_setting->getSettingValue(self::CONFIG_SHOP_NAME);
            if ($shop_name) {
                $bestme_domain = $shop_name . SHOP_NAME_SUFFIX;
            } else {
                $bestme_domain = self::CONFIG_SHOP_NAME_DEFAULT . SHOP_NAME_SUFFIX;
            }
            $config_domains['main_domain'] = $bestme_domain;
        }
        $config_domains['redirect_to_main_domain'] = !$redirect;
        $this->model_setting_setting->editSettingValue('config', self::CONFIG_DOMAINS, $config_domains);

        $json = [
            'status' => true,
            'message' => $this->language->get('success_change_redirect'),
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function changeMainDomain()
    {
        $this->load->language('online_store/domain');
        $json = [
            'status' => false,
            'message' => $this->language->get('fail_change_main_domain'),
        ];
        if (isset($this->request->post['domain'])) {
            /* get existing delivery config */
            $config_domains = $this->getSettingDomainValue();

            $config_domains['main_domain'] = $this->request->post['domain'];
            $this->model_setting_setting->editSettingValue('config', self::CONFIG_DOMAINS, $config_domains);

            $json = [
                'status' => true,
                'message' => $this->language->get('success_change_main_domain'),
            ];
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function domainAuth()
    {
        $this->load->language('online_store/domain');
        $json = [
            'status' => false,
            'message' => $this->language->get('fail_domain_auth'),
        ];
        if (isset($this->request->post['domain'])) {
            $this->load->model('setting/setting');

            $shop_name = $this->model_setting_setting->getSettingValue(self::CONFIG_SHOP_NAME);
            if ($shop_name) {
                $bestme_domain = $shop_name . SHOP_NAME_SUFFIX;
            } else {
                $bestme_domain = self::CONFIG_SHOP_NAME_DEFAULT . SHOP_NAME_SUFFIX;
            }
            $bestme_ip = defined('BESTME_IP') ? BESTME_IP : '';
            $domain = parse_url($this->request->post['domain'], PHP_URL_HOST) != NULL ? parse_url($this->request->post['domain'], PHP_URL_HOST) : $this->request->post['domain'];
            $domain = str_replace('www.', '', $domain);
            $recordA = dns_get_record($domain, DNS_A);
            $flag_a = 0;
            foreach ($recordA as $item) {
                if (isset($item['type'])) {
                    if ($item['type'] == 'A') {
                        if (isset($item['ip'])) {
                            if ($item['ip'] == $bestme_ip) {
                                $flag_a = 1;
                            }
                        }
                    }
                }
            }

            $recordWWW = dns_get_record('www.' . $domain, DNS_CNAME);
            $flag_cname = 0;
            foreach ($recordWWW as $item) {
                if (isset($item['type'])) {
                    if ($item['type'] == 'CNAME') {
                        if (isset($item['target'])) {
                            if ($item['target'] == $bestme_domain) {
                                $flag_cname = 1;
                            }
                        }
                    }
                }
            }

            if ($flag_a && $flag_cname) {
                $auth = $this->auth($this->request->post['domain']);
                if ($auth) {
                    $json = [
                        'status' => true,
                        'message' => $this->language->get('success_domain_auth'),
                    ];
                    $this->session->data['domain_add_success'] = $this->language->get('success_domain_auth');
                    // redis
                    try {
                        $this->domainPublish();
                    } catch (\Exception $e) {
                    }
                }
            } else {
                if (!$flag_a && !$flag_cname) {
                    $mess = 'A Record và CNAME' . $this->language->get('fail_domain_alert_suffix');
                } else if (!$flag_a) {
                    $mess = 'A Record' . $this->language->get('fail_domain_alert_suffix');
                } else {
                    $mess = 'CNAME Record' . $this->language->get('fail_domain_alert_suffix');
                }

                $json = [
                    'status' => false,
                    'message' => $mess,
                ];
            }
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function validateForm()
    {
        if (!$this->user->hasPermission('modify', 'online_store/domain')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        /* domain */
        $pattern = "/^(?!:\/\/)(?!www.)([a-zA-Z0-9-_]+\.)*[a-zA-Z0-9][a-zA-Z0-9-_]+\.[a-zA-Z]{2,11}?$/";
        if (!isset($this->request->post['domain']) ||
            (utf8_strlen(trim($this->request->post['domain'])) < 1) ||
            (utf8_strlen(trim($this->request->post['domain'])) > 255) ||
            !preg_match($pattern, $this->request->post['domain'])
        ) {
            $this->error['domain'] = $this->language->get('error_domain');
            $this->error['warning'] = $this->language->get('error_domain');
        }

        $config_domains = $this->getSettingDomainValue();
        $domains = (isset($config_domains['domains']) && is_array($config_domains['domains'])) ? $config_domains['domains'] : [];
        if (count($domains) >= self::CONFIG_MAX_DOMAIN) {
            $this->error['domain'] = $this->language->get('error_domain_max');
            $this->error['warning'] = $this->language->get('error_domain_max');

            return !$this->error;
        }

        /* validate existing */
        // landing page domain
        $this->load->model('setting/setting');
        $shop_name = $this->model_setting_setting->getSettingValue(self::CONFIG_SHOP_NAME);
        if ($shop_name) {
            $bestme_domain = $shop_name . SHOP_NAME_SUFFIX;
        } else {
            $bestme_domain = self::CONFIG_SHOP_NAME_DEFAULT . SHOP_NAME_SUFFIX;
        }

        if (mb_strtolower($bestme_domain) == mb_strtolower($this->request->post['domain'])) {
            $this->error['domain'] = $this->language->get('error_exist');
            $this->error['warning'] = $this->language->get('error_exist');
        }

        // user manual domains
        foreach ($domains as $domain) {
            if (!isset($domain['domain'])) {
                continue;
            }

            if (mb_strtolower($domain['domain']) == mb_strtolower($this->request->post['domain'])) {
                $this->error['domain'] = $this->language->get('error_exist');
                $this->error['warning'] = $this->language->get('error_exist');
                break;
            }
        }

        return !$this->error;
    }

    private function validateDelete()
    {
        if (!$this->user->hasPermission('modify', 'online_store/domain')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        /* domain */
        $pattern = "/^(?!:\/\/)([a-zA-Z0-9-_]+\.)*[a-zA-Z0-9][a-zA-Z0-9-_]+\.[a-zA-Z]{2,11}?$/";
        if (!isset($this->request->post['domain']) ||
            (utf8_strlen(trim($this->request->post['domain'])) < 1) ||
            (utf8_strlen(trim($this->request->post['domain'])) > 255) ||
            !preg_match($pattern, $this->request->post['domain'])
        ) {
            $this->error['domain'] = $this->language->get('error_domain');
            $this->error['warning'] = $this->language->get('error_domain');
        }

        $config_domains = $this->getSettingDomainValue();
        $domains = (isset($config_domains['domains']) && is_array($config_domains['domains'])) ? $config_domains['domains'] : [];
        $flag = 0;
        foreach ($domains as $domain) {
            if (!isset($domain['domain'])) {
                continue;
            }
            if ($domain['domain'] == $this->request->post['domain']) {
                $flag = 1;
                if (isset($config_domains['main_domain']) && $domain['domain'] == $config_domains['main_domain']) {
                    $this->error['domain'] = $this->language->get('fail_remove_main_domain');
                    $this->error['warning'] = $this->language->get('fail_remove_main_domain');
                }
                break;
            }
        }

        if (!$flag) {
            $this->error['domain'] = $this->language->get('error_find_domain');
            $this->error['warning'] = $this->language->get('error_find_domain');
        }

        return !$this->error;
    }

    private function getSettingDomainValue()
    {
//        $result = [
//            'redirect_to_main_domain' => 0,
//            'domains' => [
//                [
//                    'domain' => 'https://abc.com',
//                    'status' => 1,  // 0 : chưa xác thực, 1: đã kết nối
//                ],
//                [
//                    'domain' => 'def.com',
//                    'status' => '0',  // 0 : chưa xác thực, 1: đã kết nối
//                ],
//            ]
//        ];
//
//        return $result;

        $this->load->model('setting/setting');
        $config_domains = $this->model_setting_setting->getSettingValue(self::CONFIG_DOMAINS);
        $config_domains = json_decode($config_domains, true);
        if (!is_array($config_domains)) {
            $config_domains = [];
        }

        return $config_domains;
    }

    private function mapDomainData()
    {
        $domain = [];
        $domain['domain'] = mb_strtolower($this->request->post['domain']);
        $domain['status'] = 0;

        return $domain;
    }

    private function auth($auth_domain)
    {
        $config_domains = $this->getSettingDomainValue();

        $domains = (isset($config_domains['domains']) && is_array($config_domains['domains'])) ? $config_domains['domains'] : [];
        $result = false;
        foreach ($domains as &$domain) {
            if (!isset($domain['domain'])) {
                continue;
            }
            if ($domain['domain'] == $auth_domain) {
                $domain['status'] = 2;
                $result = true;
            }
        }
        $config_domains['domains'] = $domains;
        $this->model_setting_setting->editSettingValue('config', self::CONFIG_DOMAINS, $config_domains);

        return $result;
    }

    private function redis_connect()
    {
        $redis = new \Redis();
        $redis->connect(MUL_REDIS_HOST, MUL_REDIS_PORT);
        $redis->select(MUL_REDIS_DB);
//        echo "Connection to server sucessfully";
//        //check whether server is running or not
//        echo "Server is running: ".$redis->ping();

        return $redis;
    }

    private function domainPublish()
    {
        $redis = $this->redis_connect();

        $this->load->model('setting/setting');
        $shop_name = $this->model_setting_setting->getSettingValue(self::CONFIG_SHOP_NAME);
        $config_domains = $this->getSettingDomainValue();
        $data = array();
        $data['subdomain'] = $shop_name;

        $domains = (isset($config_domains['domains']) && is_array($config_domains['domains'])) ? $config_domains['domains'] : [];
        $list = [];
        foreach ($domains as $domain) {
            if (!isset($domain['domain']) || !isset($domain['status'])) {
                continue;
            }
            if ($domain['status']) {
                $this_domain = parse_url($domain['domain'], PHP_URL_HOST) != NULL ? parse_url($domain['domain'], PHP_URL_HOST) : $domain['domain'];
                if (strpos($this_domain, 'www.') === 0) {
                    $this_domain = substr($this_domain, 4); // remove "www." at beginning
                }

                if (!in_array($this_domain, $list)) {
                    $list[] = $this_domain;
                    $list[] = 'www.' . $this_domain;
                }
            }
        }

        $data['list'] = $list;

        $data_json = json_encode($data);
        $redis->publish(MUL_REDIS_DOMAIN_PUBLISH_CHANEL, $data_json);

        $redis->close();
    }
}