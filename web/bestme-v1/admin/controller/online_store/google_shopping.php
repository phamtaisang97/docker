<?php

class ControllerOnlineStoreGoogleShopping extends Controller
{
    private $error = array();

    const CONFIG_GG_SHOPPING = 'config_gg_shopping';

    public function index()
    {
        $this->load->language('online_store/google_shopping');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');
        $this->load->model('online_store/google_shopping');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            try {
                $this->model_setting_setting->editSettingValue('config', self::CONFIG_GG_SHOPPING, $this->request->post['meta']);
                $this->session->data['success'] = $this->language->get('success_change_meta');
            } catch (Exception $e) {
                $this->error['warning'] = $this->language->get('error_change_meta');
            }

            $this->response->redirect($this->url->link('online_store/google_shopping', 'user_token=' . $this->session->data['user_token'], true));
        }

        /* breadcrumb */
        $data['breadcrumbs'] = array();

        /* message */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        /* model data */
        $data['meta'] = $this->model_setting_setting->getSettingValue(self::CONFIG_GG_SHOPPING);
        $google_shopping_keys = $this->model_online_store_google_shopping->getKeys();
        $data['site_key'] = isset($google_shopping_keys['site_key']) ? $google_shopping_keys['site_key'] : '';
        $data['secure_key'] = isset($google_shopping_keys['secure_key']) ? $google_shopping_keys['secure_key'] : '';

        $data['action'] = $this->url->link('online_store/google_shopping', 'user_token=' . $this->session->data['user_token'], true);

        /* autoads google shopping homepage */
        $data['autoads_gg_shopping'] = AUTOADS_GG_SHOPPING_SERVER;
        $data['autoads_gg_shopping_guide'] = AUTOADS_GG_SHOPPING_GUIDE;
        $data['video_google_shopping'] = BESTME_GOOGLE_SHOPPING_GUIDE;

        /* common */
        $data['href_add_domain'] = $this->url->link('online_store/domain/add', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_delete_domain'] = $this->url->link('online_store/domain/delete', 'user_token=' . $this->session->data['user_token'], true);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('online_store/google_shopping', $data));
    }

    private function validateForm()
    {
        if (!$this->user->hasPermission('modify', 'online_store/google_shopping')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        /* domain */
        if (!isset($this->request->post['meta'])) {
            $this->error['meta'] = $this->language->get('error_meta');
        }

        return !$this->error;
    }
}