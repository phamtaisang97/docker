<?php
/**
 * Created by PhpStorm.
 * User: novaon888
 * Date: 2/18/2019
 * Time: 2:19 PM
 */
use Image\Cloudinary_Manager;
class ControllerOnlineStoreContents extends Controller
{
    use Device_Util;
    private $error = array();

    public function index()
    {
        $this->load->language('online_store/contents');

        $this->document->setTitle($this->language->get('heading_title'));


        /* breadcrumb */
        $data['breadcrumbs'] = [];

        $this->load->model('online_store/contents');

        $url = '';
        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
            $url .= '&page=' . $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['filter_status'])) {
            $filter_status = $this->request->get['filter_status'];
            $data['filter_status'] = $filter_status;
        } else {
            $filter_status = '';
        }

        if (isset($this->request->get['filter_title'])) {
            $filter_title = $this->request->get['filter_title'];
            $data['filter_title'] = trim($filter_title);
        } else {
            $filter_title = '';
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];

            unset($this->session->data['error']);
        } /*else {
            $data['error_warning'] = '';
        }*/

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }
        $contents_filter = array(
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin'),
            'filter_status' => $filter_status,
            'filter_title' => trim($filter_title)
        );
        $contents = $this->model_online_store_contents->getContents($contents_filter);
        foreach ($contents as $content) {
            $data['contents'][] = array(
                'page_id' => $content['page_id'],
                'title' => $content['title'],
                'description' => $this->stripTagsAndShortenContentDescription($content['description']),
                'source' => $content['source'],
                'status' => $content['status'] ? $this->language->get('txt_filter_status_show') : $this->language->get('txt_filter_status_hide'),
                'date_added' => $content['date_added'],
                'edit' => $this->url->link('online_store/contents/edit', 'user_token=' . $this->session->data['user_token'] . '&page_id=' . $content['page_id'], true)
            );
        }
        $totalContents = $this->model_online_store_contents->getTotalContents($contents_filter);

        $pagination = new CustomPaginate();
        $pagination->total = $totalContents;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('online_store/contents', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($totalContents) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($totalContents - $this->config->get('config_limit_admin'))) ? $totalContents : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $totalContents, ceil($totalContents / $this->config->get('config_limit_admin')));
        $data['add_contents_link'] = $this->url->link('online_store/contents/add', 'user_token=' . $this->session->data['user_token'], true);
        $data['delete'] = $this->url->link('online_store/contents/delete', 'user_token=' . $this->session->data['user_token'], true);
        $data['change_status'] = $this->url->link('online_store/contents/ChangeStatuses', 'user_token=' . $this->session->data['user_token'], true);
        $data['video_content'] = defined('BESTME_CONTENT_GUIDE') ? BESTME_CONTENT_GUIDE : '';
        /* common */
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('online_store/contents_list_append', $data));
        } else {
            $this->response->setOutput($this->load->view('online_store/contents_list', $data));
        }
    }

    public function add()
    {
        $this->load->language('online_store/contents');

        $this->document->setTitle($this->language->get('add_heading_title'));

        /* breadcrumb */
        $data['breadcrumbs'] = [];

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_contents'),
            'href' => $this->url->link('online_store/contents', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_add_contents'),
            'href' => ''
        );

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['ckeditor_lang'] = $this->config->get('config_language') == 'vi-vn' ? 'vi' : 'en';
        $data['upload_image'] = str_replace('&amp;', '&', $this->url->link('online_store/contents/upload', 'user_token=' . $this->session->data['user_token'], true));
        $data['filemanager'] = str_replace('&amp;', '&', $this->url->link('common/filemanager', 'user_token=' . $this->session->data['user_token'] . '&ckeditor=1', true));
        if ($this->request->server['HTTPS']) {
            $data['base_url'] = HTTPS_SERVER;
        } else {
            $data['base_url'] = HTTP_SERVER;
        }

        $data['is_on_mobile'] = $this->isMobile();
        //$data['server_name'] = $_SERVER['SERVER_NAME'];
        $data['server_name'] = HTTP_CATALOG;
        $data['preview'] = HTTP_CATALOG . 'index.php?route=contents/page/preview';
        $data['cancel'] = $this->url->link('online_store/contents', 'user_token=' . $this->session->data['user_token'], true);
        $data['action'] = $this->url->link('online_store/contents/addContent', 'user_token=' . $this->session->data['user_token'], true);
        /* common */
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('online_store/contents_form', $data));
    }

    public function edit()
    {
        if ((isset($this->request->get['page_id']) && $this->request->get['page_id'] != null) || isset($this->request->post['page_id'])) {
            $page_id = isset($this->request->get['page_id']) ? $this->request->get['page_id'] : $this->request->post['page_id'];
            $this->load->language('online_store/contents');

            $this->document->setTitle($this->language->get('edit_heading_title'));

            $this->load->model('online_store/contents');

            if (isset($this->error['warning'])) {
                $data['error_warning'] = $this->error['warning'];
            } else {
                $data['error_warning'] = '';
            }
            $data['content_info'] = $this->model_online_store_contents->getContent($page_id);
            $content_alias = $this->model_online_store_contents->getAliasById($page_id);
            $data['content_alias'] = array_key_exists('keyword', $content_alias) ? $content_alias['keyword'] : '';
            $data['ckeditor_lang'] = $this->config->get('config_language') == 'vi-vn' ? 'vi' : 'en';
            $data['upload_image'] = str_replace('&amp;', '&', $this->url->link('online_store/contents/upload', 'user_token=' . $this->session->data['user_token'], true));
            $data['filemanager'] = str_replace('&amp;', '&', $this->url->link('common/filemanager', 'user_token=' . $this->session->data['user_token'] . '&ckeditor=1', true));
            if ($this->request->server['HTTPS']) {
                $data['base_url'] = HTTPS_SERVER;
            } else {
                $data['base_url'] = HTTP_SERVER;
            }
            if ($data['content_info']) {
                /* breadcrumb */
                $data['breadcrumbs'] = [];

                $data['breadcrumbs'][] = array(
                    'text' => $this->language->get('breadcrumb_contents'),
                    'href' => $this->url->link('online_store/contents', 'user_token=' . $this->session->data['user_token'], true)
                );

                $data['breadcrumbs'][] = array(
                    'text' => $this->language->get('breadcrumb_edit_contents'),
                    'href' => ''
                );

                $data['seo_preview_title'] = (isset($data['content_info']['seo_title']) && $data['content_info']['seo_title'] != null) ? $data['content_info']['seo_title']
                    : '';
//                    : $this->language->get('txt_seo_preview_title_demo');
                $data['seo_preview_description'] = (isset($data['content_info']['seo_description']) && $data['content_info']['seo_description'] != null) ? $data['content_info']['seo_description']
                    : '';
//                    : $this->language->get('txt_seo_preview_description_demo');
                $data['seo_preview_alias'] = $data['content_alias'] != null ? $data['content_alias'] : ''; // $this->language->get('txt_seo_preview_alias_demo')

                //$data['server_name'] = $_SERVER['SERVER_NAME'];
                $data['server_name'] = HTTP_CATALOG;
                $data['preview'] = HTTP_CATALOG . 'index.php?route=contents/page/preview';
                $data['cancel'] = $this->url->link('online_store/contents', 'user_token=' . $this->session->data['user_token'], true);
                $data['action'] = $this->url->link('online_store/contents/editContent', 'user_token=' . $this->session->data['user_token'], true);
                /* common */
                $data['custom_header'] = $this->load->controller('common/custom_header');
                $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
                $data['footer'] = $this->load->controller('common/footer');
                $data['is_on_mobile'] = $this->isMobile();
                $this->response->setOutput($this->load->view('online_store/contents_form', $data));
            } else {
                $this->response->redirect($this->url->link('online_store/contents', 'user_token=' . $this->session->data['user_token'], true));
            }
        } else {
            $this->response->redirect($this->url->link('online_store/contents', 'user_token=' . $this->session->data['user_token'], true));
        }
    }

    public function upload()
    {
        $json = array();
        $get_form = $this->request->post;

        if (isset($_FILES['mediaImages']['name'])) {
            $cloudinary = new Cloudinary_Manager();
            $store_id = SHOP_ID;  /// dictory
            $directory = SHOP_ID;
            if (is_file($_FILES['mediaImages']['tmp_name'])) {
                // Sanitize the filename
                $filename = basename(html_entity_decode($_FILES['mediaImages']['name'], ENT_QUOTES, 'UTF-8'));

                // Validate the filename length
                if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 255)) {
                    $json['error'] = $this->language->get('error_filename');
                }

                // Allowed file extension types
                $allowed = array(
                    'jpg',
                    'jpeg',
                    //'gif',
                    'png',
                    'ico'
                );
                if (!in_array(utf8_strtolower(utf8_substr(strrchr($filename, '.'), 1)), $allowed)) {
                    $json['error'] = $this->language->get('error_filetype');
                }

                // Allowed file mime types
                $allowed = array(
                    'image/jpeg',
                    'image/pjpeg',
                    'image/png',
                    'image/x-png',
                    //'image/gif',
                    'image/x-icon'
                );

                if (!in_array($_FILES['mediaImages']['type'], $allowed)) {
                    $json['error'] = $this->language->get('error_filetype');
                }

                if ($_FILES['mediaImages']['size'] > MAX_IMAGE_SIZE_UPLOAD) {
                    $json['error'] = $this->language->get('error_filesize') . '. Max size of image is ' . ((int)MAX_IMAGE_SIZE_UPLOAD / 1024) . 'KB';
                }

                // Return any upload error
                if ($_FILES['mediaImages']['error'] != UPLOAD_ERR_OK) {
                    $json['error'] = $this->language->get('error_upload_' . $_FILES['mediaImages']['error']);
                }
            } else {
                $json['error'] = $this->language->get('error_upload_default') . ((int)MAX_IMAGE_SIZE_UPLOAD / 1024) . 'KB';
            }

            if (!$json) {
                $fileNameWithoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $_FILES['mediaImages']['name']);
                // trans to slug
                $this->load->model('custom/common');
                $fileNameWithoutExt = $this->model_custom_common->createSlug($fileNameWithoutExt, '-');

                //upload
                $result = $cloudinary->upload($_FILES['mediaImages']['tmp_name'], $fileNameWithoutExt, $directory);
                if (array_key_exists('public_id', $result)) {
                    $function_number = 1;
                    echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($function_number, '" . $result['url'] . "', '');</script>";
                }
            }

        }
    }

    public function addContent()
    {
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $data = array();
            $data['title'] = isset($this->request->post['title']) ? $this->request->post['title'] : '';
            $data['description'] = isset($this->request->post['description']) ? $this->request->post['description'] : '';
            $data['seo_title'] = isset($this->request->post['seo_description']) ? $this->request->post['seo_description'] : '';
            $data['seo_description'] = isset($this->request->post['seo_description']) ? $this->request->post['seo_description'] : '';
            $data['alias'] = isset($this->request->post['alias']) ? $this->request->post['alias'] : '';
            $data['status'] = isset($this->request->post['status']) ? $this->request->post['status'] : '';
            if ($data['title'] != null) {
                $this->load->model('online_store/contents');
                $this->model_online_store_contents->addContent($data);
                $this->session->data['success'] = $this->language->get('text_success_add');
                $this->response->redirect($this->url->link('online_store/contents', 'user_token=' . $this->session->data['user_token'], true));
            } else {
                $this->add();
            }
        } else {
            $this->add();
        }
    }

    public function editContent()
    {
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && isset($this->request->post['page_id']) && $this->validateForm()) {
            $id = $this->request->post['page_id'];
            $data = array();
            $data['title'] = isset($this->request->post['title']) ? $this->request->post['title'] : '';
            $data['description'] = isset($this->request->post['description']) ? $this->request->post['description'] : '';
            $data['seo_title'] = isset($this->request->post['seo_title']) ? $this->request->post['seo_title'] : '';
            $data['seo_description'] = isset($this->request->post['seo_description']) ? $this->request->post['seo_description'] : '';
            $data['alias'] = isset($this->request->post['alias']) ? $this->request->post['alias'] : '';
            $data['status'] = isset($this->request->post['status']) ? $this->request->post['status'] : '';
            if ($data['title'] != null && $id != null) {
                $this->load->model('online_store/contents');
                $this->model_online_store_contents->editContent($id, $data);
                $this->session->data['success'] = $this->language->get('text_success_edit');
                $this->response->redirect($this->url->link('online_store/contents', 'user_token=' . $this->session->data['user_token'], true));
            } else {
                $this->edit();
            }
        } else {
            $this->edit();
        }
    }

    private function validateForm(){
        $this->load->language('online_store/contents');
        if (!$this->user->hasPermission('modify', 'online_store/contents')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }


        /* name */
        if (!isset($this->request->post['title']) ||
            (utf8_strlen(trim($this->request->post['title'])) < 1) ||
            (utf8_strlen(trim($this->request->post['title'])) > 256)
        ) {
            $this->error['name'] = $this->language->get('error_name');
            $this->error['warning'] = $this->language->get('error_name');
        }

        if ($this->request->post['source'] == "omihub") {
            $this->error['warning'] = $this->language->get('error_check_source');
        }

        return !$this->error;
    }

    public function delete()
    {
        $this->load->language('online_store/contents');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('online_store/contents');
        $this->response->addHeader('Content-Type: application/json');
        $json = [
            'status' => false,
            'message' => $this->language->get('text_delete_fail'),
        ];
        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $id) {
                $this->model_online_store_contents->deleteContent($id);
            }

            $json = [
                'status' => true,
                'message' => $this->language->get('text_delete_success'),
            ];
            $this->response->setOutput(json_encode($json));
            return;
        }
        $this->response->setOutput(json_encode($json));
    }

    public function ChangeStatuses()
    {
        $this->load->language('online_store/contents');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('online_store/contents');
        $this->response->addHeader('Content-Type: application/json');
        $json = [
            'status' => false,
            'message' => $this->language->get('text_change_status_fail'),
        ];
        if (isset($this->request->post['selected']) && isset($this->request->post['status']) && $this->validateDelete()) {
            $status = $this->request->post['status'];
            foreach ($this->request->post['selected'] as $id) {
                $this->model_online_store_contents->editStatusContent($id, $status);
            }

            $json = [
                'status' => true,
                'message' => $this->language->get('text_change_status_success'),
            ];
            $this->response->setOutput(json_encode($json));
            return;
        }
        $this->response->setOutput(json_encode($json));
    }

    protected function validateDelete()
    {
        if (!$this->user->hasPermission('modify', 'online_store/contents')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
//        foreach ($this->request->post['selected'] as $selected) {
//             $data_selected = $this->model_online_store_contents->getContent($selected);
//            if ($data_selected['source'] == "omihub") {
//                $this->error['warning'] = $this->language->get('error_check_source');
//            }
//        }
        return !$this->error;
    }

    private function stripTagsAndShortenContentDescription($description)
    {
        if (empty($description) || !is_string($description)) {
            return $description;
        }

        $description = strip_tags(html_entity_decode($description));

        $MAX_DESC_LENGTH = 250;
        if (strlen($description) <= $MAX_DESC_LENGTH) {
            return $description;
        }

        return substr($description, 0, $MAX_DESC_LENGTH) . '...';
    }
}