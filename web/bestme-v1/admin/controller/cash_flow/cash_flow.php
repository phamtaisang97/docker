<?php

class ControllerCashFlowCashFlow extends Controller
{
    public function index()
    {
        $this->load->language('cash_flow/cash_flow');

        $this->document->setTitle($this->language->get('heading_title'));
        //add script for page
        $this->document->addScript('view/javascript/custom/moment.js');
        $this->document->addScript('https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js');
        $this->document->addScript('view/javascript/custom/Chart.min.js');
        $this->document->addStyle('view/stylesheet/custom/daterangepicker.css');
        $this->document->addStyle('view/stylesheet/custom/cash_flow.css');

        // load url
        $data['action']                                       = $this->url->link('cash_flow/cash_flow', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_receipt_voucher_follow_type']           = $this->url->link('cash_flow/cash_flow/getCashInflowsByReceiptType', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_payment_voucher_follow_type']           = $this->url->link('cash_flow/cash_flow/getCashOutflowsByPaymentType', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_receipt_voucher_follow_payment_method'] = $this->url->link('cash_flow/cash_flow/getCashInflowsByPaymentMethod', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_payment_voucher_follow_payment_method'] = $this->url->link('cash_flow/cash_flow/getCashOutflowsByPaymentMethod', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_get_overview_data']                     = $this->url->link('cash_flow/cash_flow/getOverviewData', 'user_token=' . $this->session->data['user_token'], true);

        // load data
        $data['overview']     = $this->getOverviewData(false);
        $data['is_on_mobile'] = 0; // current not support mobile
        $data['breadcrumbs']  = [
            [
                'text' => $this->language->get('breadcrumb_cash_flow'),
                'href' => $this->url->link('cash_flow/cash_flow', 'user_token=' . $this->session->data['user_token'], true)
            ],
            [
                'text' => $this->language->get('breadcrumb_overview'),
                'href' => ''
            ]
        ];

        // load page components
        $data['custom_header']      = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer']             = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('cash_flow/cash_flow', $data));
    }

    public function getCashInflowsByReceiptType()
    {
        $this->load->model('cash_flow/cash_flow');

        $result = [];

        $filter = $this->getFilterData();

        /* check if there is a receipt voucher exists */
        $voucher_exists = $this->model_cash_flow_cash_flow->checkVoucherExistsInDateRange($filter['start'], $filter['end'], 'receipt');
        if ($voucher_exists) {
            /* get total cash inflow amount */
            $cash_inflow = $this->model_cash_flow_cash_flow->getCashFlowAmountInDateRange($filter['start'], $filter['end'], 'receipt');

            if (0 < $cash_inflow) {
                /* get cash inflow of each receipt voucher type */
                $cash_inflow_receipt_types = $this->model_cash_flow_cash_flow->getCashInflowsByReceiptType($filter);

                $result = $this->formatPercentNumbers($cash_inflow_receipt_types, $cash_inflow);
            }
        }

        $this->response->setOutput(json_encode($result));
    }

    public function getCashOutflowsByPaymentType()
    {
        $this->load->model('cash_flow/cash_flow');

        $result = [];

        $filter = $this->getFilterData();

        /* check if there is a payment voucher exists */
        $voucher_exists = $this->model_cash_flow_cash_flow->checkVoucherExistsInDateRange($filter['start'], $filter['end'], 'payment');
        if ($voucher_exists) {
            /* get total cash inflow amount */
            $cash_outflow = $this->model_cash_flow_cash_flow->getCashFlowAmountInDateRange($filter['start'], $filter['end'], 'payment');

            if (0 < $cash_outflow) {
                /* get cash inflow of each payment voucher type */
                $cash_outflow_payment_types = $this->model_cash_flow_cash_flow->getCashOutflowsByPaymentType($filter);

                $result = $this->formatPercentNumbers($cash_outflow_payment_types, $cash_outflow);
            }
        }

        $this->response->setOutput(json_encode($result));
    }

    public function getCashInflowsByPaymentMethod()
    {
        $this->load->model('cash_flow/cash_flow');

        $result = [];

        $filter = $this->getFilterData();

        /* check if there is a receipt voucher exists */
        $voucher_exists = $this->model_cash_flow_cash_flow->checkVoucherExistsInDateRange($filter['start'], $filter['end'], 'receipt');

        if ($voucher_exists) {
            /* get total cash inflow amount */
            $cash_flow = $this->model_cash_flow_cash_flow->getCashFlowAmountInDateRange($filter['start'], $filter['end'], 'receipt');

            if (0 < $cash_flow) {
                /* get cash inflow of each payment method */
                $cash_inflow_payment_methods = $this->model_cash_flow_cash_flow->getCashInflowsByPaymentMethod($filter);

                $result = $this->formatPercentNumbers($cash_inflow_payment_methods, $cash_flow);
            }
        }

        $this->response->setOutput(json_encode($result));
    }

    public function getCashOutflowsByPaymentMethod()
    {
        $this->load->model('cash_flow/cash_flow');

        $result = [];

        $filter = $this->getFilterData();

        /* check if there is a receipt voucher exists */
        $voucher_exists = $this->model_cash_flow_cash_flow->checkVoucherExistsInDateRange($filter['start'], $filter['end'], 'payment');

        if ($voucher_exists) {
            /* get total cash inflow amount */
            $cash_flow = $this->model_cash_flow_cash_flow->getCashFlowAmountInDateRange($filter['start'], $filter['end'], 'payment');

            if (0 < $cash_flow) {
                /* get cash outflow of each payment method */
                $cash_outflow_payment_methods = $this->model_cash_flow_cash_flow->getCashOutflowsByPaymentMethod($filter);

                $result = $this->formatPercentNumbers($cash_outflow_payment_methods, $cash_flow);
            }
        }

        $this->response->setOutput(json_encode($result));
    }

    public function getOverviewData($output_flag = true)
    {
        $this->load->model('cash_flow/cash_flow');

        $filter = $this->getFilterData();

        $result = [];
        /* calculate cash flow previous period */
        $previous_period_inflow    = $this->model_cash_flow_cash_flow->getCashFlowAmountInDateRange($filter['start'], $filter['end'], 'receipt', true);
        $previous_period_outflow   = $this->model_cash_flow_cash_flow->getCashFlowAmountInDateRange($filter['start'], $filter['end'], 'payment', true);
        $result['opening_balance'] = $previous_period_inflow - $previous_period_outflow;

        /* calculate cash flow current period */
        $result['cash_inflow']     = $this->model_cash_flow_cash_flow->getCashFlowAmountInDateRange($filter['start'], $filter['end'], 'receipt');
        $result['cash_outflow']    = $this->model_cash_flow_cash_flow->getCashFlowAmountInDateRange($filter['start'], $filter['end'], 'payment');
        $result['closing_balance'] = $result['opening_balance'] + $result['cash_inflow'] - $result['cash_outflow'];

        // format
        $result = array_map('number_format', $result);

        if ($output_flag) {
            $this->response->setOutput(json_encode($result));
        } else {
            return $result;
        }
    }

    /** add default filter data if not isset
     * @return array
     * @throws Exception
     */
    private function getFilterData()
    {
        $filter = [];

        if (isset($this->request->get['start']) && !empty($this->request->get['start'])) {
            $filter['start'] = $this->request->get['start'];
            $filter['end']   = $this->request->get['end'];
        } else {
            $now             = new DateTime();
            $filter['end']   = $now->format('Y-m-d H:i:s');
            $filter['start'] = $now->modify('first day of this month')->format('Y-m-d') . ' 00:00:00';
        }

        return $filter;
    }

    /**
     * @param $arr
     * @param $total
     * @return mixed
     */
    private function formatPercentNumbers($arr, $total)
    {
        if (0 < $total) {
            $total_percent = 0;
            foreach ($arr as $key => $item) {
                if ($key < count($arr) - 1) {
                    // calculate percent for all items but the last one
                    $percent = number_format($item['total_amount'] / $total * 100, 2);
                    $total_percent += $percent;
                    $arr[$key]['percent'] = (double)$percent;
                } else {
                    // calculate last item
                    $arr[$key]['percent'] = (double)number_format((100 - $total_percent), 2);
                }
            }
        }

        return $arr;
    }

    public function getUserStoreLazyLoad() {
        $json = array();

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $this->load->model('setting/store');

        $filter_data = array(
            'order' => 'ASC',
            'start' => ($page - 1) * 6,
            'limit' => 6,
        );

        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        if(!$this->user->isAdmin() && !$this->user->canAccessAll()) {
            $filter_data['store_ids'] =  $this->user->getUserStores();
        }

        $json['results'] = $this->model_setting_store->getUserStorePaginator($filter_data);

        $count_store = $this->model_setting_store->getTotalUserStoreFilter($filter_data);
        $json['count'] = $count_store;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getCustomerLazyLoad()
    {
        $json = array();

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $this->load->model('customer/customer');

        $filter_data = array(
            'order' => 'ASC',
            'start' => ($page - 1) * 6,
            'limit' => 6
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        if(!$this->user->canAccessAll()){
            $filter_data['current_user_id'] = $this->user->getId();
        }

        $results = $this->model_customer_customer->getCustomersForOrder($filter_data);

        foreach ($results as $result) {
            $name = strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'));
            $name = strlen($name) > 30 ? substr($name, 0, 30) . "..." : $name;
            $json['results'][] = array(
                'id' => $result['customer_id'],
                'text' => $name,
                'subname' => $result['telephone']
            );
        }
        $count_customer = $this->model_customer_customer->getTotalCustomersForOrder($filter_data);
        $json['count'] = $count_customer;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getManufacturerLazyLoad()
    {
        $json = array();

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $this->load->model('catalog/manufacturer');

        $filter_data = array(
            'order' => 'ASC',
            'start' => ($page - 1) * 6,
            'limit' => 6,
            'filter_status' => 1 // 1 : active
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        $json['results'] = $this->model_catalog_manufacturer->getManufacturersPaginator($filter_data);

        $count_manufacturer = $this->model_catalog_manufacturer->getTotalManufacturersFilter($filter_data);
        $json['count'] = $count_manufacturer;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}