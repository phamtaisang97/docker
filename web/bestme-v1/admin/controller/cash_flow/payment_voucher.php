<?php


class ControllerCashFlowPaymentVoucher extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('cash_flow/payment_voucher');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('cash_flow/payment_voucher');

        $this->getList();
    }

    public function add()
    {
        $this->load->language('cash_flow/payment_voucher');
        $this->document->setTitle($this->language->get('heading_title_add'));
        $this->load->model('cash_flow/payment_voucher');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_cash_flow_payment_voucher->addPaymentVoucher($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success_add');
            $this->response->redirect($this->url->link('cash_flow/payment_voucher', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();
    }

    public function edit()
    {
        $this->load->language('cash_flow/payment_voucher');
        $this->document->setTitle($this->language->get('heading_title_edit'));
        $this->load->model('cash_flow/payment_voucher');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateFormEdit()) {
            $this->model_cash_flow_payment_voucher->updatePaymentVoucherNote($this->request->post['payment_voucher_id'], $this->request->post['note']);

            $this->session->data['success'] = $this->language->get('text_success_update');
            $this->response->redirect($this->url->link('cash_flow/payment_voucher', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();
    }

    public function getList()
    {
        $this->document->addStyle('view/stylesheet/custom/order.css');

        $filter_status = $this->getValueFromRequest('get', 'filter_status');
        $filter_payment_voucher_type = $this->getValueFromRequest('get', 'filter_payment_voucher_type');
        $filter_cash_flow_method = $this->getValueFromRequest('get', 'filter_cash_flow_method');
        $filter_name = $this->getValueFromRequest('get', 'filter_name');
        $page = $this->getValueFromRequest('get', 'page', 1);
        $filter_data = $this->getValueFromRequest('get', 'filter_data');

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_cash_flow'),
            'href' => $this->url->link('cash_flow/cash_flow', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_payment_voucher'),
            'href' => ''
        );

        $data['add'] = $this->url->link('cash_flow/payment_voucher/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['list'] = $this->url->link('cash_flow/payment_voucher', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['cancel_url'] = $this->url->link('cash_flow/payment_voucher/cancel', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['payment_vouchers'] = array();

        $data['loadOptionOne'] = $this->url->link('cash_flow/payment_voucher/loadActionOne', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $filter_data = array(
            'filter_data' => $filter_data,
            'filter_name' => $filter_name,
            'filter_status' => $filter_status,
            'filter_payment_voucher_type' => $filter_payment_voucher_type,
            'filter_cash_flow_method' => $filter_cash_flow_method,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $PAYMENT_VOUCHER_STATUS = [
            // đã hủy
            ModelCashFlowPaymentVoucher::PAYMENT_VOUCHER_STATUS_ID_CANCEL => [
                'text' => $this->language->get('txt_cancel')
            ],
            // hoàn thành
            ModelCashFlowPaymentVoucher::PAYMENT_VOUCHER_STATUS_ID_COMPLETE => [
                'text' => $this->language->get('txt_complete')
            ]
        ];

        $CHANGEABLE_PAYMENT_VOUCHER_STATUS = [
            ModelCashFlowPaymentVoucher::PAYMENT_VOUCHER_STATUS_ID_CANCEL => [],
            ModelCashFlowPaymentVoucher::PAYMENT_VOUCHER_STATUS_ID_COMPLETE => [
                [
                    'status_id' => ModelCashFlowPaymentVoucher::PAYMENT_VOUCHER_STATUS_ID_CANCEL,
                    'status_description' => $this->language->get('txt_cancel')
                ]
            ]
        ];

        $store_payment_total = $this->model_cash_flow_payment_voucher->getTotalPaymentVouchers($filter_data);

        $results = $this->model_cash_flow_payment_voucher->getPaymentVouchers($filter_data);
        foreach ($results as $result) {
            $prepare_by = json_decode($result['object_info']);
            $data['payment_vouchers'][] = array(
                'payment_voucher_id' => $result['payment_voucher_id'],
                'payment_voucher_code' => $result['payment_voucher_code'],
                'type' => $result['type'],
                'status' => $result['status'],
                'status_text' => $PAYMENT_VOUCHER_STATUS[$result['status']]['text'],
                'amount' => number_format($result['amount'], 0, '', ','),
                'method' => $result['method'],
                'prepared_by' => isset($prepare_by->name) ? $prepare_by->name : '',
                'date_added' => $result['date_added'],
                'edit' => $this->url->link('cash_flow/payment_voucher/edit', 'user_token=' . $this->session->data['user_token'] . '&payment_voucher_id=' . $result['payment_voucher_id'] . $url, true),
                'changeable_order_statuses' => $CHANGEABLE_PAYMENT_VOUCHER_STATUS[$result['status']]
            );
        }
        $data['user_token'] = $this->session->data['user_token'];

        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $store_payment_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('cash_flow/payment_voucher', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        // Add jump to page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);

        if (isset($this->session->data['success']) && $this->session->data['success'] != 'text_success') {
            $data['success'] = $this->session->data['success'];

            $this->session->data['success'] = '';
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->session->data['payment_vouchers'])) {
            $data['payment_vouchers_action'] = json_encode($this->session->data['payment_vouchers']);
        }

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];

            $this->session->data['error'] = '';
            unset($this->session->data['error']);
        } else {
            $data['error_warning'] = '';
        }

        $data['results'] = sprintf($this->language->get('text_pagination'), ($store_payment_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($store_payment_total - $this->config->get('config_limit_admin'))) ? $store_payment_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $store_payment_total, ceil($store_payment_total / $this->config->get('config_limit_admin')));

        $data['url_filter'] = $this->url->link('cash_flow/payment_voucher', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['filter_name'] = $filter_name;
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        // permission modify receipt voucher
        $data['permission_modify'] = $this->user->hasPermission('modify', 'cash_flow/receipt_voucher');

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('cash_flow/payment_voucher_list_append', $data));
        } else {
            $this->response->setOutput($this->load->view('cash_flow/payment_voucher_list', $data));
        }
    }

    public function getForm()
    {
        $this->load->model('cash_flow/payment_voucher');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['validate'])) {
            $data['error_validate'] = $this->error['validate'];
        } else {
            $data['error_validate'] = '';
        }

        if (isset($this->session->data['success']) && $this->session->data['success'] != 'text_success') {
            $data['success'] = $this->session->data['success'];

            $this->session->data['success'] = '';
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data['text_form'] = !isset($this->request->get['payment_voucher_id']) ? $this->language->get('heading_title_add') : $this->language->get('heading_title_edit');
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('cash_flow/payment_voucher', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $data['text_form'],
            'href' => '',
        );

        if (isset($this->request->get['payment_voucher_id'])) {
            $data['payment_voucher_id'] = $this->request->get['payment_voucher_id'];
        }

        // get discount info
        if (isset($this->request->get['payment_voucher_id'])) {
            $payment_voucher = $this->model_cash_flow_payment_voucher->getPaymentVoucher($this->request->get['payment_voucher_id']);

            $data['status'] = $payment_voucher['status'];
            if ($data['status'] == 0) {
                $data['payment_voucher_is_canceled'] = 1;
            }

            if($payment_voucher['store_receipt_id']){
                $this->load->model('catalog/store_receipt');
                $store_receipt = $this->model_catalog_store_receipt->getStoreReceiptById($payment_voucher['store_receipt_id']);
                if(!empty($store_receipt)){
                    $data['store_receipt_code'] = $store_receipt['store_receipt_code'];
                    $data['link_to_store_receipt'] = $this->url->link('catalog/store_receipt/edit', 'user_token=' . $this->session->data['user_token'] . '&store_receipt_id=' . $payment_voucher['store_receipt_id'] , true);
                }
            }
            if($payment_voucher['return_receipt_code']) {
                $data['return_receipt_code'] = $payment_voucher['return_receipt_code'];
                $data['link_to_return_receipt'] = $this->url->link('sale/return_receipt/detail', 'user_token=' . $this->session->data['user_token'] . '&return_receipt_id=' . $payment_voucher['return_receipt_id'] , true);
            }
        }

        if (isset($this->request->post['store_id'])) {
            $data['store_id'] = $this->request->post['store_id'];
        } else if (!empty($payment_voucher)) {
            $data['store_id'] = $payment_voucher['store_id'];
            $data['store_name'] = $payment_voucher['store_name'];
        } else {
            $data['store_id'] = '';
        }

        if (isset($this->request->post['payment_type_id'])) {
            $data['payment_type_id'] = $this->request->post['payment_type_id'];
        } else if (!empty($payment_voucher)) {
            $data['payment_type_id'] = $payment_voucher['payment_voucher_type_id'];
        } else {
            $data['payment_type_id'] = '';
        }

        if (isset($this->request->post['payment_type_other'])) {
            $data['payment_type_other'] = $this->request->post['payment_type_other'];
        } else if (!empty($payment_voucher)) {
            $data['payment_type_other'] = $payment_voucher['payment_type_other'];
        } else {
            $data['payment_type_other'] = '';
        }

        if (isset($this->request->post['method_id'])) {
            $data['method_id'] = $_POST['method_id'];
        } else if (!empty($payment_voucher)) {
            $data['method_id'] = $payment_voucher['cash_flow_method_id'];
        } else {
            $data['method_id'] = '';
        }

        if (isset($this->request->post['object_id'])) {
            $data['object_id'] = $_POST['object_id'];
        } else if (!empty($payment_voucher)) {
            $data['object_id'] = $payment_voucher['cash_flow_object_id'];
            $data['object_info'] = json_decode($payment_voucher['object_info']);
        } else {
            $data['object_id'] = '';
        }

        if (isset($this->request->post['amount'])) {
            $data['amount'] = $_POST['amount'];
        } else if (!empty($payment_voucher)) {
            $data['amount'] = $payment_voucher['amount'];
        } else {
            $data['amount'] = '';
        }

        if (isset($this->request->post['note'])) {
            $data['note'] = $_POST['note'];
        } else if (!empty($payment_voucher)) {
            $data['note'] = $payment_voucher['note'];
        } else {
            $data['note'] = '';
        }

        if (isset($this->request->post['in_business_report'])) {
            $data['in_business_report'] = $_POST['in_business_report'];
        } else if (!empty($payment_voucher)) {
            $data['in_business_report'] = $payment_voucher['in_business_report_status'];
        } else {
            $data['in_business_report'] = '';
        }

        // manufacture form
        if (!isset($this->request->get['payment_voucher_id'])) {
            $data['action'] = $this->url->link('cash_flow/payment_voucher/add', 'user_token=' . $this->session->data['user_token'], true);
        } else {
            $data['action'] = $this->url->link('cash_flow/payment_voucher/edit', 'user_token=' . $this->session->data['user_token'] . '&payment_voucher_id=' . $this->request->get['payment_voucher_id'], true);
            $data['action_cancel_payment'] = $this->url->link('cash_flow/payment_voucher/cancel', 'user_token=' . $this->session->data['user_token'], true);
        }

        // permission modify payment voucher
        $data['permission_modify'] = $this->user->hasPermission('modify', 'cash_flow/payment_voucher');

        $data['cancel'] = $this->url->link('cash_flow/payment_voucher', 'user_token=' . $this->session->data['user_token'], true);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $data['user_token'] = $this->session->data['user_token'];

        $this->load->model('cash_flow/cash_flow');
        $this->load->model('cash_flow/payment_voucher');
        $this->load->model('user/user');

        $data['objects'] = $this->model_cash_flow_cash_flow->getAllObject();
        $data['methods'] = $this->model_cash_flow_cash_flow->getCashFlowMethods();
        $data['staffs'] = json_encode($this->model_user_user->getUsers([], ['user_id', 'firstname', 'lastname']));

        if(!empty($payment_voucher)) {
            $data['payment_voucher_types'] = $this->model_cash_flow_payment_voucher->getPaymentVoucherTypes(true);
        } else {
            $data['payment_voucher_types'] = $this->model_cash_flow_payment_voucher->getPaymentVoucherTypes(false);
        }

        $this->response->setOutput($this->load->view('cash_flow/payment_voucher_form/index', $data));
    }

    public function loadActionOne()
    {
        $this->load->language('cash_flow/payment_voucher');

        $categoryID = $this->request->post['categoryID'];
        $end_select_html = '</select></div>';
        $show_html = '';

        if ($categoryID > 0) {
            if ($categoryID == 1) { // payment voucher type
                $this->load->model('cash_flow/payment_voucher');

                $start_select_html = '<div class="form-group hierarchical-child" id="hierarchical-payment-voucher-type">
                                        <select class="form-control form-control-sm select2" 
                                        data-placeholder="' . $this->language->get('filter_payment_voucher_type_placeholder') . '" 
                                        data-label="' . $this->language->get('filter_payment_voucher_type') . '" 
                                        id="filter_payment_voucher_type" 
                                        name="filter_payment_voucher_type">';
                $end_select_html = '</select></div>';
                $payment_voucher_types = $this->model_cash_flow_payment_voucher->getPaymentVoucherTypes();
                $show_html .= $start_select_html . '<option value="" data-value="' . $this->language->get('filter_payment_voucher_type') . '">' . $this->language->get('filter_payment_voucher_type_placeholder') . '</option>';
                foreach ($payment_voucher_types as $key => $vl) {
                    $show_html .= '<option value="' . $vl['payment_voucher_type_id'] . '" data-value="' . $vl['name'] . '">' . $vl['name'] . '</option>';
                }
                $show_html .= $end_select_html;
            } elseif ($categoryID == 2) { // Status
                $show_html = '<div class="form-group hierarchical-child" id="hierarchical-status">
                                            <select class="form-control form-control-sm select2"
                                             data-placeholder="' . $this->language->get('filter_status_placeholder') . '" 
                                             data-label="' . $this->language->get('filter_status') . '" 
                                             id="filter_status" 
                                             name="filter_status">';
                $show_html .= '<option value="">' . $this->language->get('filter_status_placeholder') . '</option>';
                $show_html .= '<option value="1" data-value="' . $this->language->get('filter_status_complete') . '">' . $this->language->get('filter_status_complete') . '</option>';
                $show_html .= '<option value="0" data-value="' . $this->language->get('filter_status_cancel') . '">' . $this->language->get('filter_status_cancel') . '</option>';
                $show_html .= $end_select_html;
            } elseif ($categoryID == 3) { // method
                $this->load->model('cash_flow/cash_flow');

                $start_select_html = '<div class="form-group hierarchical-child" id="hierarchical-cash-flow-method">
                                        <select class="form-control form-control-sm select2" 
                                        data-placeholder="' . $this->language->get('filter_cash_flow_method_placeholder') . '" 
                                        data-label="' . $this->language->get('filter_cash_flow_method') . '" 
                                        id="filter_cash_flow_method" 
                                        name="filter_cash_flow_method">';
                $end_select_html = '</select></div>';
                $payment_voucher_types = $this->model_cash_flow_cash_flow->getCashFlowMethods();
                $show_html .= $start_select_html . '<option value="" data-value="' . $this->language->get('filter_cash_flow_method') . '">' . $this->language->get('filter_cash_flow_method_placeholder') . '</option>';
                foreach ($payment_voucher_types as $key => $vl) {
                    $show_html .= '<option value="' . $vl['cash_flow_method_id'] . '" data-value="' . $vl['name'] . '">' . $vl['name'] . '</option>';
                }
                $show_html .= $end_select_html;
            }
        } else {
            $show_html .= '<style>#optionone,#optiontwo{display:none !important}</style>';
        }

        echo $show_html .= '';
    }

    public function cancel()
    {
        $this->load->language('cash_flow/payment_voucher');
        $this->load->model('cash_flow/payment_voucher');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateFormCancelPaymentVoucher()) {
            try {
                $order = $this->model_cash_flow_payment_voucher->getPaymentVoucher($this->request->post['payment_voucher_id']);
                if (isset($order['payment_voucher_id'])) {
                    $this->request->post['status'] = ModelCashFlowPaymentVoucher::PAYMENT_VOUCHER_STATUS_ID_CANCEL;
                    $this->model_cash_flow_payment_voucher->updateStatus($this->request->post);

                    $this->session->data['success'] = $this->language->get('text_success_update');
                }
            } catch (Exception $e) {
                $this->session->data['error'] = $this->language->get('text_failure');
            }
        }

        $this->response->redirect($this->url->link('cash_flow/payment_voucher', 'user_token=' . $this->session->data['user_token'], true));
    }

    private function validateForm()
    {
        $this->load->language('catalog/store_payment');
        if (!$this->user->hasPermission('modify', 'cash_flow/payment_voucher')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        // Validate Amount
        if(!isset($this->request->post['amount']) || (isset($this->request->post['amount']) && $this->request->post['amount'] == '')) {
            $this->error['validate'][] = $this->language->get('error_amount_empty');
        }
        if(isset($this->request->post['amount']) && strlen($this->request->post['amount']) > 14) {
            $this->error['validate'][] = $this->language->get('error_amount_max_length');
        }

        // validate store
        if(!isset($this->request->post['store_id']) || (isset($this->request->post['store_id']) && $this->request->post['store_id'] === '')) {
            $this->error['validate'][] = $this->language->get('error_store_empty');
        }
        if(isset($this->request->post['store_id']) && !in_array($this->request->post['store_id'], $this->user->getUserStores())) {
            $this->error['validate'][] = $this->language->get('error_store_empty');
        }

        // validate payment type
        if(!isset($this->request->post['payment_type_id']) || (isset($this->request->post['payment_type_id']) && !$this->request->post['payment_type_id'])) {
            $this->error['validate'][] = $this->language->get('error_payment_type_empty');
        }

        // validate payment method
        if(!isset($this->request->post['method_id']) || (isset($this->request->post['method_id']) && !$this->request->post['method_id'])) {
            $this->error['validate'][] = $this->language->get('error_payment_method_empty');
        }

        // validate note
        if(isset($this->request->post['note']) && mb_strlen($this->request->post['note']) > 256) {
            $this->error['validate'][] = $this->language->get('error_note_max_length');
        }

        return !$this->error;
    }

    private function validateFormCancelPaymentVoucher()
    {
        if (!$this->user->hasPermission('modify', 'cash_flow/payment_voucher')) {
            $this->session->data['error'] = $this->language->get('error_permission');
            return false;
        }

        if (!isset($this->request->post['payment_voucher_id'])) {
            $this->session->data['error'] = $this->language->get('error_cancel_payment_voucher_id');
            return false;
        }

        return true;
    }

    private function validateFormEdit()
    {
        if (!$this->user->hasPermission('modify', 'cash_flow/payment_voucher')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!isset($this->request->post['payment_voucher_id'])) {
            $this->error['warning'] = $this->language->get('error_update_payment_voucher_id');
        }

        if(isset($this->request->post['note']) && mb_strlen($this->request->post['note']) > 256) {
            $this->error['validate'][] = $this->language->get('error_note_max_length');
        }

        return !$this->error;
    }
}