<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;

class ControllerReportStaff extends Controller
{
    use Device_Util;

    const LIMIT_DATA_PAGE = 30;

    public function index()
    {
        $this->document->addScript('view/javascript/custom/moment.js');
        $this->document->addScript('view/javascript/custom/daterangepicker.min.js');
        $this->document->addStyle('view/stylesheet/custom/daterangepicker.css');
        $this->document->addStyle('view/stylesheet/custom/report.css');
        $this->load->language('report/staff');
        $this->load->model('report/product');
        $this->load->model('setting/store');

        $data['breadcrumbs'] = [
            [
                'id' => 'menu-report',
                'text' => $this->language->get('text_report'),
                'href' => $this->url->link('report/overview', 'user_token=' . $this->session->data['user_token'], true)
            ],
            [
                'text' => $this->language->get('text_sales_report'),
                'href' => ''
            ]
        ];

        //get all store
        $stores             = $this->model_setting_store->getStores();
        $data['list_store'] = $stores;

        //report list
        $data['action']                  = $this->url->link('report/product', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_get_report_table'] = $this->url->link('report/staff/getTableAppend', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_export']           = $this->url->link('report/staff/export', 'user_token=' . $this->session->data['user_token'], true);

        // load default page
        $data['custom_header']      = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer']             = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('report/staff/report_by_staff', $data));
    }

    public function getTableAppend()
    {
        $this->load->language('report/staff');
        $this->load->model('report/staff');

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;

        // filter
        if (isset($this->request->get['start']) && !empty($this->request->get['start'])) {
            $filter['start'] = $this->request->get['start'];
            $filter['end']   = $this->request->get['end'];
        } else {
            $timezone   = ini_get('date.timezone') ? ini_get('date.timezone') : date_default_timezone_get();
            $now        = new DateTime('now', new DateTimeZone($timezone));
            $query_date = $now->format('Y-m-d');
            // First day of the month.
            $filter['start'] = date('Y-m-01', strtotime($query_date));
            // Last day of the month.
            $filter['end'] = date('Y-m-t', strtotime($query_date));
        }

        if (isset($this->request->get['store_id'])) {
            $filter['store_id'] = $this->request->get['store_id'];
        } else {
            $filter['store_id'] = -1;
        }

        //limit paginate
        $filter['start_limit'] = ($page - 1) * self::LIMIT_DATA_PAGE;
        $filter['end_limit']   = self::LIMIT_DATA_PAGE;

        $data['report_by_staffs'] = $this->model_report_staff->getReportByStaffs($filter);
        $total_record_report     = $this->model_report_staff->getTotalNumberReportByStaffs($filter);

        //limit paginate
        $filter['start_limit'] = ($page - 1) * self::LIMIT_DATA_PAGE;
        $filter['end_limit']   = self::LIMIT_DATA_PAGE;

        $url                       = '';
        $pagination                = new CustomPaginate();
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $pagination->total         = $total_record_report;
        $pagination->page          = $page;
        $pagination->limit         = self::LIMIT_DATA_PAGE;
        $pagination->url           = $this->url->link('report/staff/getTableAppend', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

        // Add jump to page
        $data['pagination'] = $pagination->render($option);
        $data['results']    = sprintf($this->language->get('text_pagination'), ($total_record_report) ? (($page - 1) * self::LIMIT_DATA_PAGE) + 1 : 0, ((($page - 1) * self::LIMIT_DATA_PAGE) > ($total_record_report - self::LIMIT_DATA_PAGE)) ? $total_record_report : ((($page - 1) * self::LIMIT_DATA_PAGE) + self::LIMIT_DATA_PAGE), $total_record_report, ceil($total_record_report / self::LIMIT_DATA_PAGE));

        // tinh tong
        $data['overview'] = $this->model_report_staff->getOverviewReport($filter);

        return $this->response->setOutput($this->load->view('report/staff/report_by_staff_append', $data));
    }

    public function export()
    {
        $this->load->language('report/staff');
        $this->load->model('report/staff');
        $this->load->model('setting/store');

        $data = [];
        // filter
        if (isset($this->request->get['start']) && !empty($this->request->get['start'])) {
            $filter['start'] = $this->request->get['start'];
            $filter['end']   = $this->request->get['end'];
        } else {
            $timezone   = ini_get('date.timezone') ? ini_get('date.timezone') : date_default_timezone_get();
            $now        = new DateTime('now', new DateTimeZone($timezone));
            $query_date = $now->format('Y-m-d');
            // First day of the month.
            $filter['start'] = date('Y-m-01', strtotime($query_date));
            // Last day of the month.
            $filter['end'] = date('Y-m-t', strtotime($query_date));
        }
        if (isset($this->request->get['store_id'])) {
            $filter['store_id'] = $this->request->get['store_id'];
        } else {
            $filter['store_id'] = -1;
        }

        $data['report_by_staffs'] = $this->model_report_staff->getReportByStaffs($filter);
        $data['overview'] = $this->model_report_staff->getOverviewReport($filter);

        $time_start = $filter['start'];
        $time_end = $filter['end'];
        if ($time_start === $time_end){
            $time = $time_start;
        }
        else{
            $time = $time_start." -> ".$time_end;
        }
        if ($filter['store_id'] < 0) {
            $store_name = "Tất cả cửa hàng";
        }
        else {
            $store_name = $this->model_setting_store->getStoreNameById($filter['store_id']);
        }
        $data['time'] = $time;
        $data['store_name'] = $store_name;

        return $this->response->setOutput($this->model_report_staff->export($data));
    }
}