<?php

class ControllerReportOverview extends Controller
{
    const REVENUE_ID = 1;
    const CUSTOMER_ID = 3;
    const ORDER_ID = 2;
    const BEST_SELL_ID = 4;
    use Device_Util;

    public function index()
    {
        $this->load->language('report/overview');
        $this->load->model('report/overview');

        $this->document->setTitle($this->language->get('heading_title'));
        //add script for page
        $this->document->addScript('view/javascript/custom/Chart.min.js');
        $this->document->addScript('view/javascript/custom/moment.js');
        $this->document->addScript('view/javascript/custom/daterangepicker.min.js');
        $this->document->addStyle('view/stylesheet/custom/daterangepicker.css');
        $this->document->addStyle('view/stylesheet/custom/report.css');


        $data['breadcrumbs'] = [
            [
                'text' => $this->language->get('breadcrumb_report'),
                'href' => $this->url->link('review/overview', 'user_token=' . $this->session->data['user_token'], true)
            ],
            [
                'text' => $this->language->get('breadcrumb_overview'),
                'href' => ''
            ]
        ];

        // actions
        $data['action_get_overview_data']            = $this->url->link('report/overview/getOverviewData', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_get_revenue_and_profit_data']  = $this->url->link('report/overview/getRevenueProfitLineChartData', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_get_revenue_from_source_data'] = $this->url->link('report/overview/getRevenueFromSourceChartData', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_get_profit_from_source_data']  = $this->url->link('report/overview/getProfitFromSourceChartData', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_get_top_customer_table']       = $this->url->link('report/overview/getTopCustomerTable', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_get_top_staff_table']          = $this->url->link('report/overview/getTopStaffTable', 'user_token=' . $this->session->data['user_token'], true);

        $data['stores'] = $this->model_report_overview->getAllStore();
        $data['is_admin'] = $this->user->isAdmin();

        // load default page
        $data['list_orer']          = $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], true);
        $data['list_customer']      = $this->url->link('customer/customer', 'user_token=' . $this->session->data['user_token'], true);
        $data['custom_header']      = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer']             = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('report/overview/index', $data));
    }

    public function getOverviewData()
    {
        $this->load->model('report/overview');

        $filter = $this->detectStartAndEndTime($this->request->get);
        $count_order    = $this->model_report_overview->getCountOrder($filter);
        $revenue        = $this->model_report_overview->getTotalOrder($filter);
        $sum_cost_price = $this->model_report_overview->getSumCostPrice($filter);

        $data = [
            'revenue' => $revenue,
            'count_order' => $count_order,
            'profit' => $revenue - $sum_cost_price,
            'revenue_per_order' => $count_order == 0 ? 0 : $revenue / $count_order
        ];

        $this->response->setOutput(json_encode($data));
    }

    public function getRevenueProfitLineChartData()
    {
        $this->load->model('report/overview');
        $filter = $this->detectStartAndEndTime($this->request->get, true);
        $revenue_data = $this->model_report_overview->getRevenueAndProfitChartData($filter);

        $this->response->setOutput(json_encode($revenue_data));
    }

    public function getTopCustomerTable()
    {
        $this->load->language('report/overview');
        $data = [];

        $this->load->model('report/overview');
        $filter = $this->detectStartAndEndTime($this->request->get);
        $data['customers'] = $this->model_report_overview->getTopBestSellCustomer($filter);

        $this->response->setOutput($this->load->view('report/overview/table_best_sell_customer_append', $data));
    }

    public function getTopStaffTable()
    {
        $this->load->language('report/overview');
        $data = [];

        $this->load->model('report/overview');
        $filter = $this->detectStartAndEndTime($this->request->get);
        $data['staffs'] = $this->model_report_overview->getTopBestSellStaff($filter);

        $this->response->setOutput($this->load->view('report/overview/table_best_sell_staff_append', $data));
    }

    public function getRevenueFromSourceChartData()
    {
        $this->load->model('report/overview');
        $filter = $this->detectStartAndEndTime($this->request->get);

        $data = $this->model_report_overview->getRevenueFromSource($filter);
        $this->response->setOutput(json_encode($data));
    }

    public function getProfitFromSourceChartData()
    {
        $this->load->model('report/overview');
        $filter = $this->detectStartAndEndTime($this->request->get);

        $data = $this->model_report_overview->getProfitFromSource($filter);
        $this->response->setOutput(json_encode($data));
    }

    public function getRevenue()
    {
        $this->load->model('report/overview');
        if (isset($this->request->get['start']) && !empty($this->request->get['start'])) {
            $filter['start'] = $this->request->get['start'];
            $filter['end'] = $this->request->get['end'];
        } else {
            $timezone = ini_get('date.timezone') ? ini_get('date.timezone') : date_default_timezone_get();
            $now = new DateTime('now', new DateTimeZone($timezone));
            $filter['end'] = $now->format('Y-m-d');
            $filter['start'] = $now->modify('- 30 day')->format('Y-m-d');
        }
        if (isset($this->request->get['user_id'])) {
            $user_id = preg_replace('/[^0-9]/', '', $this->request->get['user_id']);
            if ($user_id !== '') {
                $filter['user_id'] = $user_id;
            }
        }
        if (isset($this->request->get['store_id'])) {
            $store_id = preg_replace('/[^0-9]/', '', $this->request->get['store_id']);
            if ($store_id !== '') {
                $filter['store_id'] = $store_id;
            }
        }
        $json_data = $this->model_report_overview->getRevenue($filter, self::REVENUE_ID);
        $this->response->setOutput(json_encode($json_data));
    }

    private function detectStartAndEndTime($data, $for_line_chart = false) {
        if (!isset($data['start']) || $data['start'] == '') {
            $data['end']   = date('Y-m-d');
            $data['start'] = date('Y-m') . '-01';
        }

        if($data['start'] === $data['end'] && $for_line_chart) {
            $data['start'] = (new DateTime($data['start']))->modify('-1 day')->format('Y-m-d');
        }

        return $data;
    }
}