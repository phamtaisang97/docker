<?php
/**
 * Created by PhpStorm.
 * User: Microsoft Windows
 * Date: 09/11/2020
 * Time: 8:40 AM
 */

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Html as SpreadsheetReaderHtml;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ControllerReportFinancial extends Controller
{
    use Device_Util;

    public function index()
    {
        $this->load->language('report/financial');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_report'),
            'href' => $this->url->link('report/overview', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title_2'),
            'href' => '#'
        );
        //get all store
        $this->load->model('setting/store');
        $data['stores'] = $this->model_setting_store->getStores();

        $data['user_token'] = $this->session->data['user_token'];
        $data['url_filter'] = $this->url->link('report/financial/ajaxLoadReportData', 'user_token=' . $this->session->data['user_token'], true);

        // load default page
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('report/financial/index', $data));
    }

    public function ajaxLoadReportData()
    {
        $this->load->model('report/financial');
        $this->load->language('report/financial');
        $data = [];
        if ($this->validateData()){
            $current_period_filter['store_id'] = $previous_period_filter['store_id'] = $this->request->post['p_filter_store'];
            $data['check_compare_display'] = $check_date_range = isset($this->request->post['check_date_range']) ? $this->request->post['check_date_range'] : 1;

            // kỳ hiện tại
            $start =  new DateTime($this->request->post['start_date']);
            $end = new DateTime($this->request->post['end_date']);
            $end = $end->setTime(23, 59);

            $current_period_filter['start'] = $start->format('Y-m-d H:i:s');
            $current_period_filter['end'] = $end->format('Y-m-d H:i:s');
            $data['current'] = $this->getReportData($current_period_filter);

            // kỳ trước
            if ($check_date_range) {
                $start = $start->modify('- 1 month');
                $end = $end->modify('- 1 month');
                $previous_period_filter['start'] = $start->format('Y-m-d H:i:s');
                $previous_period_filter['end'] = $end->format('Y-m-d 23:59:00');
                $data['previous'] = $this->getReportData($previous_period_filter);

                $data['change'] = $this->getDataChangeRate($data['current'], $data['previous']);
            } else {
                $keys = array_keys($data['current']);
                $data['previous'] = array_fill_keys($keys, '--');
                $data['change'] = array_fill_keys($keys, '--');
            }
        }

        $this->response->setOutput($this->load->view('report/financial/index_append', $data));
    }

    public function validateData()
    {
        if (!$this->user->hasPermission('access', 'report/financial')) {
            return false;
        }

        if (!isset($this->request->post['start_date']) || !($this->request->post['start_date'])) {
            return false;
        }

        if (!isset($this->request->post['end_date']) || !($this->request->post['end_date'])) {
            return false;
        }

        return true;
    }

    private function getReportData($filter)
    {
        $data = [
            'sale_revenue' => 0,                // 1
            'trade_discount' => 0,              // 2.1
            'total_return' => 0,                // 2.2
            'revenue_deductions' => 0,          // 2
            'net_revenue' => 0,                 // 3
            'cost_ogs' => 0,                    // 4
            'gross_revenue' => 0,               // 5
            'delivery_fee_paid' => 0,           // 6.1
            'pay_salary_for_staff' => 0,        // 6.2
            'fee' => 0,                         // 6
            'other_fee' => 0,                   // 7
            'net_profit' => 0,                  // 8
            'manual_receipt_voucher' => 0,      // 9.1
            'return_fee' => 0,                  // 9.2
            'other_income' => 0,                // 9
            'eite' => 0,                        // 10
            'revenues' => 0,                    // 11

        ];
        $order_info = $this->model_report_financial->getOrderReport($filter);
        if (isset($order_info['total_amount']) && isset($order_info['disccount'])) {
            $data['sale_revenue'] = (float)$order_info['total_amount'] + (float)$order_info['disccount'];
            $data['trade_discount'] = (float)$order_info['disccount'];
        }
        $data['total_return'] = $this->model_report_financial->getOderReturnAmount($filter);
        $data['revenue_deductions'] = $data['total_return'] + $data['trade_discount'];
        $data['net_revenue'] = $data['sale_revenue'] - $data['total_return'] - $data['trade_discount'];
        $data['cost_ogs'] = $this->model_report_financial->GetCostOfGoodsSold($filter);
        $data['gross_revenue'] = $data['net_revenue'] - $data['cost_ogs'];
        $data['delivery_fee_paid'] = $this->model_report_financial->getDeliveryFeePaidToThePartner($filter);
        $order_shipping_fee = isset($order_info['shipping_fee']) ? (float)$order_info['shipping_fee'] : 0;
        $data['delivery_fee_paid'] += $order_shipping_fee;
        $data['pay_salary_for_staff'] = $this->model_report_financial->getAmountSalaryForStaff($filter);
        $data['fee'] = $data['delivery_fee_paid'] + $data['pay_salary_for_staff'];
        $data['other_fee'] = $this->model_report_financial->getOtherFee($filter);
        $data['net_profit'] = $data['gross_revenue'] - $data['fee'] - $data['other_fee'];
        $data['manual_receipt_voucher'] = $this->model_report_financial->getManualReceiptVoucher($filter);
        $data['return_fee'] = $this->model_report_financial->getReturnFee($filter);
        $data['other_income'] = $data['return_fee'] + $data['manual_receipt_voucher'];
        $data['eite'] = $this->model_report_financial->getEnterpriseIncomeTaxExpense($filter);
        $data['revenues'] = $data['net_profit'] + $data['other_income'] - $data['eite'];

        return $data;
    }

    private function getDataChangeRate($current, $previous)
    {
        if (!is_array($current) || !is_array($previous) || empty($current)) {
            return [];
        }
        $data = [];
        foreach ($current as $key => $item) {
            if ((float)$current[$key] == 0 && (float)$previous[$key] == 0){
                $data[$key] = 0;
            } else if ((!isset($previous[$key]) || (float)$previous[$key] == 0)) {
                if ((float)$current[$key] < 0){
                    $data[$key] = -100;
                }else{
                    $data[$key] = 100;
                }
            } else if ((float)$current[$key] == 0) {
                if ($previous[$key] > 0){
                    $data[$key] = -100;
                }else if($previous[$key] < 0){
                    $data[$key] = 100;
                }else{
                    $data[$key] = 0;
                }
            } else {
                $data[$key] = (($current[$key] - $previous[$key]) * 100) / $previous[$key];
                $data[$key] = round($data[$key]);
            }
        }

        return $data;
    }

    public function export()
    {
        $this->load->language('report/financial');
        $this->load->model('report/financial');

        $current_period_filter['store_id'] = $previous_period_filter['store_id'] = $this->request->post['p_filter_store'];
        $check_date_range = isset($this->request->post['check_date_range']) ? $this->request->post['check_date_range'] : 1;

        // kỳ hiện tại
        $start =  new DateTime($this->request->post['p_start']);
        $end = new DateTime($this->request->post['p_end']);
        $data['current_time'] = $start->format('d-m-Y') . ' : ' . $end->format('d-m-Y');
        $end = $end->setTime(23, 59);

        $current_period_filter['start'] = $start->format('Y-m-d H:i:s');
        $current_period_filter['end'] = $end->format('Y-m-d H:i:s');
        $data['current'] = $this->getReportData($current_period_filter);

        // kỳ trước
        if ($check_date_range) {
            $start = $start->modify('- 1 month');
            $end = $end->modify('- 1 month');
            $data['previous_time'] = $start->format('d-m-Y') . ' : ' . $end->format('d-m-Y');
            $previous_period_filter['start'] = $start->format('Y-m-d H:i:s');
            $previous_period_filter['end'] = $end->format('Y-m-d 23:59:00');
            $data['previous'] = $this->getReportData($previous_period_filter);

            $data['change'] = $this->getDataChangeRate($data['current'], $data['previous']);
        } else {
            $data['previous_time'] = '--';
            $keys = array_keys($data['current']);
            $data['previous'] = array_fill_keys($keys, '--');
            $data['change'] = array_fill_keys($keys, '--');
        }

        $store_name = $this->language->get('text_all_stores');

        if (isset($this->request->post['p_filter_store']) && $this->request->post['p_filter_store'] != '') {
            $this->load->model('setting/store');
            $store_name = $this->model_setting_store->getStoreNameById($this->request->post['p_filter_store']);
        }
        $data['store_name'] = $store_name;

        return $this->response->setOutput($this->model_report_financial->export($data));
    }
}