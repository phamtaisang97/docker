<?php
/**
 * Created by PhpStorm.
 * User: Microsoft Windows
 * Date: 11/11/2020
 * Time: 1:23 PM
 */

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Html as SpreadsheetReaderHtml;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ControllerReportStore extends Controller
{
    use Device_Util;

    const LIMIT_DATA_PAGE = 30;

    public function index()
    {
        $this->load->language('report/store');
        $this->load->model('report/store');
        $this->load->model('sale/return_receipt');
        $this->load->model('setting/store');

        $this->document->addScript('view/javascript/custom/moment.js');
        $this->document->addScript('view/javascript/custom/daterangepicker.min.js');
        $this->document->addStyle('view/stylesheet/custom/daterangepicker.css');
        $this->document->addStyle('view/stylesheet/custom/report.css');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_report'),
            'href' => $this->url->link('report/overview', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title_2'),
            'href' => '#'
        );

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;

        $filter_data = $this->getFilterData();

        $stores = $this->model_report_store->getDataByStore($filter_data);
        $store_ids = [];
        foreach ($stores as $store){
            if (!in_array($store['store_id'], $store_ids)){
                $store_ids[] = $store['store_id'];
            }
        }
        $list_store_in_return_receipt = $this->model_report_store->getListStoreReturnReceiptByDate($filter_data['start_time'], $filter_data['end_time']);
        foreach ($list_store_in_return_receipt as $store_id) {
            if (!in_array($store_id, $store_ids)) {
                $stores[] = array(
                    'store_id' => $store_id,
                    'count_order' => 0,
                    'amount' => 0,
                    'discount' => 0,
                    'shipping_fee' => 0,
                    'total_return' => 0
                );
            }
        }
        foreach ($stores as &$store) {
            if (!isset($store['store_id'])) {
                continue;
            }

            $return_receipts = $this->model_report_store->getReturnReceiptsDataByStore($store['store_id'], $filter_data['start_time'], $filter_data['end_time']);
            $store['count_order_return'] = isset($return_receipts['count_return']) ? (int)$return_receipts['count_return'] : 0;

            $store['discount'] = (float)$store['discount'];
            $store['shipping_fee'] = (float)$store['shipping_fee'];
            $store['amount'] = (float)$store['amount'] - $store['shipping_fee']; // due to `amount` previously included the `shipping_fee`
            $store['total_return'] = isset($return_receipts['total_return']) ? (float)$return_receipts['total_return'] : 0;
            $store['revenue'] = $store['amount'] - $store['discount'] + $store['shipping_fee'] - $store['total_return'];
            $store['store_name'] = $this->model_setting_store->getStoreNameById($store['store_id']);
            $store['link'] = $this->url->link('report/store/detail', 'user_token=' . $this->session->data['user_token'] . '&store_id=' . $store['store_id'], true);
        }
        unset($store);
        $data['report_by_stories'] = $stores;

        $allStores = $this->model_report_store->getDataAllStores($filter_data);
        $all_return_receipts = $this->model_report_store->getReturnReceiptsData($filter_data['start_time'], $filter_data['end_time']);
        $allStores['amount'] = isset($allStores['amount']) ? (float)$allStores['amount'] : 0;
        $allStores['discount'] = isset($allStores['discount']) ? (float)$allStores['discount'] : 0;
        $allStores['shipping_fee'] = isset($allStores['shipping_fee']) ? (float)$allStores['shipping_fee'] : 0;
        $allStores['amount'] = $allStores['amount'] - $allStores['shipping_fee']; // due to `amount` previously included the `shipping_fee`
        $allStores['total_return'] = isset($all_return_receipts['total_return']) ? (float)$all_return_receipts['total_return'] : 0;
        $allStores['count_order_return'] = isset($all_return_receipts['count_return']) ? (int)$all_return_receipts['count_return'] : 0;
        $allStores['revenue'] = $allStores['amount'] - $allStores['discount'] + $allStores['shipping_fee'] - $allStores['total_return'];
        $data['all_stores'] = $allStores;
        $total_rows = isset($allStores['count_store']) ? $allStores['count_store'] : 0;

        $data['user_token'] = $this->session->data['user_token'];
        $data['is_on_mobile'] = $this->isMobile();
        $data['url_filter'] = $this->url->link('report/store', 'user_token=' . $this->session->data['user_token'], true);

        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $total_rows;
        $pagination->page = $page;
        $pagination->limit = self::LIMIT_DATA_PAGE;
        $pagination->url = $this->url->link('report/order', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

        // Add jump to page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);
        $data['results'] = sprintf($this->language->get('text_pagination'), ($total_rows) ? (($page - 1) * self::LIMIT_DATA_PAGE) + 1 : 0, ((($page - 1) * self::LIMIT_DATA_PAGE) > ($total_rows - self::LIMIT_DATA_PAGE)) ? $total_rows : ((($page - 1) * self::LIMIT_DATA_PAGE) + self::LIMIT_DATA_PAGE), $total_rows, ceil($total_rows / self::LIMIT_DATA_PAGE));

        // load default page
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && !empty($this->request->get['filter_data'])) {
            $this->response->setOutput($this->load->view('report/store/index_append', $data));
        } else {
            $this->response->setOutput($this->load->view('report/store/index', $data));
        }
    }

    public function detail()
    {
        if (!isset($this->request->get['store_id']) || $this->request->get['store_id'] == ''){
            $this->response->redirect($this->url->link('report/store', 'user_token=' . $this->session->data['user_token'], true));
        }
        $this->load->language('report/store');
        $this->load->model('report/store');
        $this->load->model('report/order');
        $this->load->model('sale/return_receipt');
        $this->load->model('setting/store');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['back_link'] = $this->url->link('report/store', 'user_token=' . $this->session->data['user_token'], true);
        $data['store_name'] = $this->model_setting_store->getStoreNameById($this->request->get['store_id']);
        if ($data['store_name'] == '') {
            $this->response->redirect($this->url->link('report/store', 'user_token=' . $this->session->data['user_token'], true));
        }
        $data['store_id'] = $this->request->get['store_id'];

        $data['user_token'] = $this->session->data['user_token'];
        $data['is_on_mobile'] = $this->isMobile() ? 1 : 0;

        try {
            if (isset($this->request->get['start_time']) && !empty($this->request->get['start_time'])) {
                $start_time_req = $this->request->get['start_time'];
                $end_time_req = $this->request->get['end_time'];
            } else {
                $now = new DateTime('now');
                $start_time_req = date_create($now->format('Y-m-d'))->modify('first day of this month')->format('Y-m-d 00:00:00');
                $end_time_req = $now->format('Y-m-d 23:59:59');
            }

            $page = isset($this->request->get['page']) ? (int)$this->request->get['page'] : 1;

            $filter_data = [
                'store_id' => isset($this->request->get['store_id']) ? $this->request->get['store_id'] : "",
                'start_time' => $start_time_req,
                'end_time' => $end_time_req,
                'start' => ($page - 1) * self::LIMIT_DATA_PAGE, // $this->config->get('config_limit_admin')
                'limit' => self::LIMIT_DATA_PAGE,
                'page' => $page,
            ];

            $data['filter'] = $filter_data;

            // data store
            $stores = $this->model_setting_store->getStores();
            $data['stores'] = $stores;

            // get total (count) report rows by date range
            // $total_report_count = $this->model_report_order->getTotalReportByOrders($filter_data); // TODO: remove...
            $start_time_req_obj = date_create($start_time_req);
            if (!$start_time_req_obj instanceof DateTime) {
                throw new Exception('Invalid "start_time"');
            }

            $end_time_req_obj = date_create($end_time_req);
            if (!$end_time_req_obj instanceof DateTime) {
                throw new Exception('Invalid "end_time"');
            }

            $d_diff = date_diff($end_time_req_obj, $start_time_req_obj);
            if (!$d_diff instanceof DateInterval) {
                throw new Exception('Could not calculate date range from "start_time" to "end_time"');
            }

            $total_report_count = $d_diff->days + 1;
            $offset__ = ($page - 1) * self::LIMIT_DATA_PAGE;
            $len__ = ($page * self::LIMIT_DATA_PAGE > $total_report_count) ? ($total_report_count - $offset__) : self::LIMIT_DATA_PAGE;

            // paginate date range by hand
            $start_date = clone $start_time_req_obj;
            $start_date = $start_date->modify(sprintf('+ %s days', $offset__));
            $end_date = clone $start_date;
            $end_date = $end_date->modify(sprintf('+ %s days', $len__ - 1));

            /* init reports */
            $order_reports = [];
            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($start_date, $interval, $end_date);

            foreach ($period as $dt) {
                /** @var DateTime $dt */
                $r_date = $dt->format('Y-m-d');

                $order_reports[$r_date] = [
                    'total_amount' => 0,
                    'shipping_fee' => 0,
                    'discount' => 0,
                    'total_order' => 0,
                    'report_date' => $r_date,
                    'store_id' => 0,
                    'revenue' => 0,
                    'total_return' => 0, // later
                    'order_return' => 0, // later
                ];
            }

            // init for end_date
            $r_date = $end_date->format('Y-m-d');
            $order_reports[$r_date] = [
                'total_amount' => 0,
                'shipping_fee' => 0,
                'discount' => 0,
                'total_order' => 0,
                'report_date' => $r_date,
                'store_id' => 0,
                'revenue' => 0,
                'total_return' => 0, // later
                'order_return' => 0, // later
            ];

            /* get return receive report by date range */
            $order_return = $this->model_sale_return_receipt->totalOrderReturnByDateGroupByDate($start_date->format('Y-m-d 00:00:00'), $end_date->format("Y-m-d 23:59:59"), $filter_data['store_id']);
            foreach ($order_return as $ort) {
                if (!is_array($ort) ||
                    !array_key_exists('date_added', $ort) ||
                    !array_key_exists('order_return', $ort) ||
                    !array_key_exists('total_return', $ort)
                ) {
                    continue;
                }

                $order_reports[$ort['date_added']]['order_return'] = (int)$ort['order_return'];
                $order_reports[$ort['date_added']]['total_return'] = (float)$ort['total_return'];
            }

            // get report detail by date range
            $report_detail_filter = [
                'store_id' => isset($this->request->get['store_id']) ? $this->request->get['store_id'] : "",
                'start_time' => $start_date->format('Y-m-d 00:00:00'),
                'end_time' => $end_date->format('Y-m-d 23:59:59'),
                'start' => ($page - 1) * self::LIMIT_DATA_PAGE, // $this->config->get('config_limit_admin')
                'limit' => self::LIMIT_DATA_PAGE,
                'page' => $page,
            ];

            $order_reports_from_db = $this->model_report_order->getReportByOrders($report_detail_filter);
            foreach ($order_reports_from_db as $or) {
                if (!isset($order_reports[$or['report_date']])) {
                    continue;
                }

                $order_reports[$or['report_date']]['total_amount'] = (float)$or['total_amount'];
                $order_reports[$or['report_date']]['shipping_fee'] = (float)$or['shipping_fee'];
                $order_reports[$or['report_date']]['discount'] = (float)$or['discount'];
                $order_reports[$or['report_date']]['total_order'] = (int)$or['total_order'];
                $order_reports[$or['report_date']]['store_id'] = (float)$or['store_id'];
            }
            unset($or);

            // calculate "revenue" after got full reports with return receive and order report detail
            foreach ($order_reports as &$or) {
                $or['revenue'] = (float)$or['total_amount'] - (float)$or['discount'] + (float)$or['shipping_fee'] - (float)$or['total_return'];
            }
            unset($or);

            // return order report output
            $data['report_by_orders'] = $order_reports;

            // summary report for date range
            $data_total = $this->model_report_order->getDataTotalReportByOrders($filter_data);
            $data['action'] = $this->url->link('report/order', 'user_token=' . $this->session->data['user_token'], true);

            $data['total_all'] = [
                'total_order' => 0,
                'total_amount' => 0,
                'total_discount' => 0,
                'total_shipping_fee' => 0,
                'total_order_return' => 0,
                'total_return' => 0,
                'total_revenue' => 0,
            ];

            $result_order_return = $this->model_sale_return_receipt->totalOrderReturnAndTotalReturnByDate($start_time_req, $end_time_req, $filter_data['store_id']);
            $total_order_return = 0;
            $total_return = 0;

            if (is_array($result_order_return) &&
                array_key_exists('total_order_return', $result_order_return) &&
                array_key_exists('total_return', $result_order_return)
            ) {
                $total_order_return = $result_order_return['total_order_return'];
                $total_return = $result_order_return['total_return'];
            }

            $data['total_all']['total_order_return'] = (float)$total_order_return;
            $data['total_all']['total_return'] = (float)$total_return;

            foreach ($data_total as $item) {
                // data total
                $data['total_all']['total_order'] += (float)$item['total_order'];
                $data['total_all']['total_amount'] += (float)$item['total_amount'];
                $data['total_all']['total_discount'] += (float)$item['discount'];
                $data['total_all']['total_shipping_fee'] += (float)$item['shipping_fee'];
            }

            $data['total_all']['total_revenue'] = $data['total_all']['total_amount'] - $data['total_all']['total_discount'] + $data['total_all']['total_shipping_fee'] - $data['total_all']['total_return'];

            // paginate
            $url = '';

            $pagination = new CustomPaginate();
            $pagination->total = $total_report_count;
            $pagination->page = $page;
            $pagination->limit = self::LIMIT_DATA_PAGE; //$this->config->get('config_limit_admin');
            $pagination->url = $this->url->link('report/order', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

            // Add jump to page
            $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
            $data['pagination'] = $pagination->render($option);
            $data['results'] = sprintf($this->language->get('text_pagination'), ($total_report_count) ? (($page - 1) * self::LIMIT_DATA_PAGE) + 1 : 0, ((($page - 1) * self::LIMIT_DATA_PAGE) > ($total_report_count - self::LIMIT_DATA_PAGE)) ? $total_report_count : ((($page - 1) * self::LIMIT_DATA_PAGE) + self::LIMIT_DATA_PAGE), $total_report_count, ceil($total_report_count / self::LIMIT_DATA_PAGE));
        } catch (Exception $e) {
            $data['__error'] = $e->getMessage();
        }

        // load default page
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && !empty($this->request->get['filter_data'])) {
            return $this->response->setOutput($this->load->view('report/store/store_detail_append', $data));
        } else {
            $this->response->setOutput($this->load->view('report/store/store_detail', $data));
        }
    }

    public function all_store_export()
    {
        $this->load->language('report/store');
        $this->load->model('report/store');
        $this->load->model('sale/return_receipt');
        $this->load->model('setting/store');

        $filter = $this->getFilterData();
        unset($filter['start']);
        unset($filter['limit']);

        $stores = $this->model_report_store->getDataByStore($filter);
        foreach ($stores as &$store) {
            if (!isset($store['store_id'])) {
                continue;
            }

            $return_receipts = $this->model_report_store->getReturnReceiptsDataByStore($store['store_id'], $filter['start_time'], $filter['end_time']);
            $store['count_order_return'] = isset($return_receipts['count_return']) ? (int)$return_receipts['count_return'] : 0;

            $store['discount'] = (float)$store['discount'];
            $store['shipping_fee'] = (float)$store['shipping_fee'];
            $store['amount'] = (float)$store['amount'] - $store['shipping_fee']; // due to `amount` previously included the `shipping_fee`
            $store['total_return'] = isset($return_receipts['total_return']) ? (float)$return_receipts['total_return'] : 0;
            $store['revenue'] = $store['amount'] - $store['discount'] + $store['shipping_fee'] - $store['total_return'];
            $store['store_name'] = $this->model_setting_store->getStoreNameById($store['store_id']);
            $store['link'] = $this->url->link('report/store/detail', 'user_token=' . $this->session->data['user_token'] . '&store_id=' . $store['store_id'], true);
        }
        unset($store);
        $data['report_by_stories'] = $stores;

        $allStores = $this->model_report_store->getDataAllStores($filter);
        $all_return_receipts = $this->model_report_store->getReturnReceiptsData($filter['start_time'], $filter['end_time']);
        $allStores['amount'] = isset($allStores['amount']) ? (float)$allStores['amount'] : 0;
        $allStores['discount'] = isset($allStores['discount']) ? (float)$allStores['discount'] : 0;
        $allStores['shipping_fee'] = isset($allStores['shipping_fee']) ? (float)$allStores['shipping_fee'] : 0;
        $allStores['amount'] = $allStores['amount'] - $allStores['shipping_fee']; // due to `amount` previously included the `shipping_fee`
        $allStores['total_return'] = isset($all_return_receipts['total_return']) ? (float)$all_return_receipts['total_return'] : 0;
        $allStores['count_order_return'] = isset($all_return_receipts['count_return']) ? (int)$all_return_receipts['count_return'] : 0;
        $allStores['revenue'] = $allStores['amount'] - $allStores['discount'] + $allStores['shipping_fee'] - $allStores['total_return'];
        $data['all_stores'] = $allStores;
        $start = new DateTime($filter['start_time']);
        $end = new DateTime($filter['end_time']);
        $data['time'] =  $start->format('d-m-Y'). ' : ' . $end->format('d-m-Y');

        return $this->response->setOutput($this->model_report_store->allStoresExport($data));
    }

    private function getFilterData()
    {
        if (isset($this->request->get['start_time']) && !empty($this->request->get['start_time'])) {
            $start_time = $this->request->get['start_time'];
            $end_time = $this->request->get['end_time'];
        } else {
            $now = new DateTime('now');
            $start_time = date_create($now->format('Y-m-d'))->modify('first day of this month')->format('Y-m-d 00:00:00');
            $end_time = $now->format('Y-m-d H:i:s');
        }

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;

        $filter_data = [
            'start_time' => $start_time,
            'end_time' => $end_time,
            'start' => ($page - 1) * self::LIMIT_DATA_PAGE,
            'limit' => self::LIMIT_DATA_PAGE
        ];

        return $filter_data;
    }
}