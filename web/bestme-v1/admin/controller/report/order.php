<?php


class ControllerReportOrder extends Controller
{
    use Device_Util;

    const LIMIT_DATA_PAGE = 30;

    public function index()
    {
        $this->load->language('report/order');
        $this->load->model('report/order');
        $this->load->model('sale/return_receipt');
        $this->load->model('setting/store');

        $this->document->setTitle($this->language->get('heading_title'));

        // add script for page
        $this->document->addScript('view/javascript/custom/moment.js');
        $this->document->addScript('view/javascript/custom/daterangepicker.min.js');
        $this->document->addStyle('view/stylesheet/custom/daterangepicker.css');
        $this->document->addStyle('view/stylesheet/custom/report.css');

        $data = [];

        $data['breadcrumbs'] = [
            [
                'text' => $this->language->get('breadcrumb_report'),
                'href' => $this->url->link('review/overview', 'user_token=' . $this->session->data['user_token'], true)
            ],
            [
                'text' => $this->language->get('breadcrumb_report_order'),
                'href' => ''
            ]
        ];

        $data['user_token'] = $this->session->data['user_token'];
        $data['is_on_mobile'] = $this->isMobile() ? 1 : 0;

        try {
            if (isset($this->request->get['start_time']) && !empty($this->request->get['start_time'])) {
                $start_time_req = $this->request->get['start_time'];
                $end_time_req = $this->request->get['end_time'];
            } else {
                $now = new DateTime('now');
                $start_time_req = date_create($now->format('Y-m-d'))->modify('first day of this month')->format('Y-m-d 00:00:00');
                $end_time_req = $now->format('Y-m-d 23:59:59');
            }

            $page = isset($this->request->get['page']) ? (int)$this->request->get['page'] : 1;

            $filter_data = [
                'store_id' => isset($this->request->get['store_id']) ? $this->request->get['store_id'] : "",
                'start_time' => $start_time_req,
                'end_time' => $end_time_req,
                'start' => ($page - 1) * self::LIMIT_DATA_PAGE, // $this->config->get('config_limit_admin')
                'limit' => self::LIMIT_DATA_PAGE,
                'page' => $page,
            ];

            $data['filter'] = $filter_data;

            // data store
            $stores = $this->model_setting_store->getStores();
            $data['stores'] = $stores;

            // get total (count) report rows by date range
            // $total_report_count = $this->model_report_order->getTotalReportByOrders($filter_data); // TODO: remove...
            $start_time_req_obj = date_create($start_time_req);
            if (!$start_time_req_obj instanceof DateTime) {
                throw new Exception('Invalid "start_time"');
            }

            $end_time_req_obj = date_create($end_time_req);
            if (!$end_time_req_obj instanceof DateTime) {
                throw new Exception('Invalid "end_time"');
            }

            $d_diff = date_diff($end_time_req_obj, $start_time_req_obj);
            if (!$d_diff instanceof DateInterval) {
                throw new Exception('Could not calculate date range from "start_time" to "end_time"');
            }

            $total_report_count = $d_diff->days + 1;
            $offset__ = ($page - 1) * self::LIMIT_DATA_PAGE;
            $len__ = ($page * self::LIMIT_DATA_PAGE > $total_report_count) ? ($total_report_count - $offset__) : self::LIMIT_DATA_PAGE;

            // paginate date range by hand
            $start_date = clone $start_time_req_obj;
            $start_date = $start_date->modify(sprintf('+ %s days', $offset__));
            $end_date = clone $start_date;
            $end_date = $end_date->modify(sprintf('+ %s days', $len__ - 1));

            /* init reports */
            $order_reports = [];
            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($start_date, $interval, $end_date);

            foreach ($period as $dt) {
                /** @var DateTime $dt */
                $r_date = $dt->format('Y-m-d');

                $order_reports[$r_date] = [
                    'total_amount' => 0,
                    'shipping_fee' => 0,
                    'discount' => 0,
                    'total_order' => 0,
                    'report_date' => $r_date,
                    'store_id' => 0,
                    'revenue' => 0,
                    'total_return' => 0, // later
                    'order_return' => 0, // later
                ];
            }

            // init for end_date
            $r_date = $end_date->format('Y-m-d');
            $order_reports[$r_date] = [
                'total_amount' => 0,
                'shipping_fee' => 0,
                'discount' => 0,
                'total_order' => 0,
                'report_date' => $r_date,
                'store_id' => 0,
                'revenue' => 0,
                'total_return' => 0, // later
                'order_return' => 0, // later
            ];

            /* get return receive report by date range */
            $order_return = $this->model_sale_return_receipt->totalOrderReturnByDateGroupByDate($start_date->format('Y-m-d 00:00:00'), $end_date->format("Y-m-d 23:59:59"));
            foreach ($order_return as $ort) {
                if (!is_array($ort) ||
                    !array_key_exists('date_added', $ort) ||
                    !array_key_exists('order_return', $ort) ||
                    !array_key_exists('total_return', $ort)
                ) {
                    continue;
                }

                $order_reports[$ort['date_added']]['order_return'] = (int)$ort['order_return'];
                $order_reports[$ort['date_added']]['total_return'] = (float)$ort['total_return'];
            }

            // get report detail by date range
            $report_detail_filter = [
                'store_id' => isset($this->request->get['store_id']) ? $this->request->get['store_id'] : "",
                'start_time' => $start_date->format('Y-m-d 00:00:00'),
                'end_time' => $end_date->format('Y-m-d 23:59:59'),
                'start' => ($page - 1) * self::LIMIT_DATA_PAGE, // $this->config->get('config_limit_admin')
                'limit' => self::LIMIT_DATA_PAGE,
                'page' => $page,
            ];

            $order_reports_from_db = $this->model_report_order->getReportByOrders($report_detail_filter);
            foreach ($order_reports_from_db as $or) {
                if (!isset($order_reports[$or['report_date']])) {
                    continue;
                }

                $order_reports[$or['report_date']]['total_amount'] = (float)$or['total_amount'];
                $order_reports[$or['report_date']]['shipping_fee'] = (float)$or['shipping_fee'];
                $order_reports[$or['report_date']]['discount'] = (float)$or['discount'];
                $order_reports[$or['report_date']]['total_order'] = (int)$or['total_order'];
                $order_reports[$or['report_date']]['store_id'] = (float)$or['store_id'];
            }
            unset($or);

            // calculate "revenue" after got full reports with return receive and order report detail
            foreach ($order_reports as &$or) {
                $or['revenue'] = (float)$or['total_amount'] - (float)$or['discount'] + (float)$or['shipping_fee'] - (float)$or['total_return'];
            }
            unset($or);

            // return order report output
            $data['report_by_orders'] = $order_reports;

            // summary report for date range
            $data_total = $this->model_report_order->getDataTotalReportByOrders($filter_data);
            $data['action'] = $this->url->link('report/order', 'user_token=' . $this->session->data['user_token'], true);

            $data['total_all'] = [
                'total_order' => 0,
                'total_amount' => 0,
                'total_discount' => 0,
                'total_shipping_fee' => 0,
                'total_order_return' => 0,
                'total_return' => 0,
                'total_revenue' => 0,
            ];

            $result_order_return = $this->model_sale_return_receipt->totalOrderReturnAndTotalReturnByDate($start_time_req, $end_time_req);
            $total_order_return = 0;
            $total_return = 0;

            if (is_array($result_order_return) &&
                array_key_exists('total_order_return', $result_order_return) &&
                array_key_exists('total_return', $result_order_return)
            ) {
                $total_order_return = $result_order_return['total_order_return'];
                $total_return = $result_order_return['total_return'];
            }

            $data['total_all']['total_order_return'] = (float)$total_order_return;
            $data['total_all']['total_return'] = (float)$total_return;

            foreach ($data_total as $item) {
                // data total
                $data['total_all']['total_order'] += (float)$item['total_order'];
                $data['total_all']['total_amount'] += (float)$item['total_amount'];
                $data['total_all']['total_discount'] += (float)$item['discount'];
                $data['total_all']['total_shipping_fee'] += (float)$item['shipping_fee'];
            }

            $data['total_all']['total_revenue'] = $data['total_all']['total_amount'] - $data['total_all']['total_discount'] + $data['total_all']['total_shipping_fee'] - $data['total_all']['total_return'];

            // paginate
            $url = '';

            $pagination = new CustomPaginate();
            $pagination->total = $total_report_count;
            $pagination->page = $page;
            $pagination->limit = self::LIMIT_DATA_PAGE; //$this->config->get('config_limit_admin');
            $pagination->url = $this->url->link('report/order', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

            // Add jump to page
            $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
            $data['pagination'] = $pagination->render($option);
            $data['results'] = sprintf($this->language->get('text_pagination'), ($total_report_count) ? (($page - 1) * self::LIMIT_DATA_PAGE) + 1 : 0, ((($page - 1) * self::LIMIT_DATA_PAGE) > ($total_report_count - self::LIMIT_DATA_PAGE)) ? $total_report_count : ((($page - 1) * self::LIMIT_DATA_PAGE) + self::LIMIT_DATA_PAGE), $total_report_count, ceil($total_report_count / self::LIMIT_DATA_PAGE));
        } catch (Exception $e) {
            $data['__error'] = $e->getMessage();
        }

        // load default page
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && !empty($this->request->get['filter_data'])) {
            $this->response->setOutput($this->load->view('report/order/report_by_order_append', $data));
        } else {
            $this->response->setOutput($this->load->view('report/order/report_by_order', $data));
        }
    }

    public function export()
    {
        $this->load->language('report/order');

        $filter = [
            'start_time' => isset($this->request->post['start_time']) ? $this->request->post['start_time'] : '',
            'end_time' => isset($this->request->post['end_time']) ? $this->request->post['end_time'] : '',
            'store_id' => isset($this->request->post['store_id']) ? $this->request->post['store_id'] : '',
        ];

        $store_name = $this->language->get('text_all_store');

        if (isset($filter['store_id']) && $filter['store_id'] != '') {
            $this->load->model('setting/store');
            $store_name = $this->model_setting_store->getStoreNameById($filter['store_id']);
        }

        $this->load->model('report/order');
        $data = $this->model_report_order->dataExport($filter);

        $data['store_name'] = $store_name;
        $data['time'] = isset($this->request->post['display_excel']) ? $this->request->post['display_excel'] : '';

        return $this->response->setOutput($this->model_report_order->export($data));
    }
}