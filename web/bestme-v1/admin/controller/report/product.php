<?php

class ControllerReportProduct extends Controller
{
    use Device_Util;

    const LIMIT_DATA_PAGE = 30;

    public function index() {
        $this->document->addScript('view/javascript/custom/moment.js');
        $this->document->addScript('view/javascript/custom/daterangepicker.min.js');
        $this->document->addStyle('view/stylesheet/custom/daterangepicker.css');
        $this->document->addStyle('view/stylesheet/custom/report.css');
        $this->load->language('report/report');
        $data['is_on_mobile'] = $this->isMobile() ? 1 : 0;
        $page = $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $this->load->model('report/product');

        $data['breadcrumbs']  = [
            [
                'id' => 'menu-report',
                'text' => $this->language->get('text_report'),
                'href' => $this->url->link('report/overview', 'user_token=' . $this->session->data['user_token'], true)
            ],
            [
                'text' => $this->language->get('text_sales_report'),
                'href' => ''
            ]
        ];

        // filter
        if (isset($this->request->get['start']) && !empty($this->request->get['start'])) {
            $filter['start'] = $this->request->get['start'];
            $filter['end'] = $this->request->get['end'];
        } else {
            $timezone = ini_get('date.timezone') ? ini_get('date.timezone') : date_default_timezone_get();
            $now = new DateTime('now', new DateTimeZone($timezone));
            $query_date = $now->format('Y-m-d');
            // First day of the month.
            $filter['start'] = date('Y-m-01', strtotime($query_date));
            // Last day of the month.
            $filter['end'] = date('Y-m-t', strtotime($query_date));
        }

        if (isset($this->request->get['store_id'])) {
            $filter['store_id'] = $this->request->get['store_id'];
        }
        else{
            $filter['store_id'] = -1;
        }

        //limit paginate
        $filter['start_limit'] = ($page - 1) *self::LIMIT_DATA_PAGE;
        $filter['end_limit'] =self::LIMIT_DATA_PAGE;

        //get all store
        $this->load->model('setting/store');
        $stores = $this->model_setting_store->getStores();
        $data['list_store'] = $stores;

        //report list
        $results = $this->model_report_product->getReportByProducts($filter);
        $report_product_total = $this->model_report_product->getTotalNumberReportByProducts($filter);
        $data['action'] = $this->url->link('report/product', 'user_token=' . $this->session->data['user_token'], true);
        $return_amount_all = 0;
        $quantity_return_all = 0;
        foreach ($results as $result) {
            $quantity_return_all += isset($result['quantity_return']) ? (int)$result['quantity_return'] : 0;
            $return_amount_all += isset($result['return_amount']) ? (float)$result['return_amount'] : 0;
            $data['report_by_products'][] = array(
                'sku' => $result['sku'],
                'product_name' => $result['product_name'],
                'quantity' => $result['quantity'],
                'quantity_return' => $result['quantity_return'],
                'total_amount' => number_format($result['total_amount'], 0, '', ',').' ₫',
                'return_amount' => number_format($result['return_amount'], 0, '', ',').' ₫',
                'discount' => number_format($result['discount'], 0, '', ',').' ₫',
                'revenue' => number_format(($result['total_amount'] - $result['discount'] - $result['return_amount']), 0, '', ',').' ₫'
            );
        }

        // tinh tong
        $total_all = $this->model_report_product->getTotalReportByProducts($filter);
        $data['quantity_all'] = $total_all['quantity_all'];
        $data['quantity_return_all'] = $total_all['quantity_return_all'];
        $data['total_amount_all'] = number_format($total_all['total_amount_all'], 0, '', ',').' ₫';
        $data['return_amount_all'] = number_format($total_all['return_amount_all'], 0, '', ',').' ₫';
        $data['discount_all'] = number_format($total_all['discount_all'], 0, '', ',').' ₫';
        $data['revenue_all'] = number_format(($total_all['total_amount_all'] - $total_all['discount_all'] - $total_all['return_amount_all']), 0, '', ',').' ₫';

        // paginate
        $url = '';
        $pagination = new CustomPaginate();
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $pagination->total = $report_product_total;
        $pagination->page = $page;
        $pagination->limit =self::LIMIT_DATA_PAGE;
        $pagination->url = $this->url->link('report/product', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

        // Add jump to page
        $data['pagination'] = $pagination->render($option);
        $data['results'] = sprintf($this->language->get('text_pagination'), ($report_product_total) ? (($page - 1) *self::LIMIT_DATA_PAGE) + 1 : 0, ((($page - 1) *self::LIMIT_DATA_PAGE) > ($report_product_total -self::LIMIT_DATA_PAGE)) ? $report_product_total : ((($page - 1) *self::LIMIT_DATA_PAGE) +self::LIMIT_DATA_PAGE), $report_product_total, ceil($report_product_total /self::LIMIT_DATA_PAGE));

        //export_xls
        if (isset($this->request->get['export_xls']) && !empty($this->request->get['export_xls'])) {
            return $this->export($filter, $data);
        }
        //end export_xls

        // load default page
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if ((isset($this->request->get['start']) && !empty($this->request->get['start']))
            || isset($this->request->get['store_id'])
            || (isset($this->request->get['filter_data']) && !empty($this->request->get['filter_data']))) {
            return $this->response->setOutput($this->load->view('report/product/report_by_product_append', $data));
        } else {
            $this->response->setOutput($this->load->view('report/product/report_by_product', $data));
        }
    }

    public function export($filter, $data)
    {
        $this->load->model('setting/store');
        $this->load->language('report/report');
        $time_start = $filter['start'];
        $time_end = $filter['end'];
        if ($time_start === $time_end) {
            $time = $time_start;
        } else {
            $time = $time_start . " -> " . $time_end;
        }
        if ($filter['store_id'] < 0) {
            $store_name = $this->language->get('text_store_all');
        } else {
            $store_name = $this->model_setting_store->getStoreNameById($filter['store_id']);
        }
        $data['time'] = $time;
        $data['store_name'] = $store_name;

        return $this->response->setOutput($this->model_report_product->export($data));
    }
}