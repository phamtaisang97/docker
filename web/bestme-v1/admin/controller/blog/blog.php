<?php

class ControllerBlogBlog extends Controller
{
    use Device_Util;
    use Theme_Config_Util;

    private $error = array();

    private $approval_status_ctv = [
        [
            'value' => 'request_approval',
            'text' => 'Yêu cầu duyêt'
        ]
    ];

    private $approval_status_admin = [
        [
            'value' => 'request_approval',
            'text' => 'Yêu cầu duyêt'
        ],
        [
            'value' => 'content_approved',
            'text' => 'Content đã duyệt'
        ],
        [
            'value' => 'seo_approved',
            'text' => 'SEO đã duyệt'
        ],
        [
            'value' => 'fix_request',
            'text' => 'Yêu cầu sửa'
        ]
    ];

    /**
     * list blogs
     */
    public function index()
    {
        $this->load->language('blog/blog');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('blog/blog');
        $this->load->model('custom/tag');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_blog_blog->editBlog($this->request->get['blog_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('blog/blog', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getList();
    }

    /**
     * add blogs
     */
    public function add()
    {
        $this->load->language('blog/blog');

        $this->document->setTitle($this->language->get('heading_title_add'));

        $this->load->model('blog/blog');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $data['title'] = trim(isset($this->request->post['title'])) ? trim($this->request->post['title']) : '';
            $data['content'] = isset($this->request->post['content']) ? $this->request->post['content'] : '';
            $data['short_content'] = isset($this->request->post['short_content']) ? $this->request->post['short_content'] : '';
            $data['status'] = isset($this->request->post['status']) ? $this->request->post['status'] : 0;
            $data['in_home'] = isset($this->request->post['in_home']) ? $this->request->post['in_home'] : 0;
            $data['approval_status'] = isset($this->request->post['approval_status']) ? $this->request->post['approval_status'] : '';
            $data['image'] = isset($this->request->post['image']) ? $this->request->post['image'] : '';
            $data['alt'] = isset($this->request->post['alt']) ? $this->request->post['alt'] : '';
            $data['seo_name'] = trim(isset($this->request->post['seo_name'])) ? trim($this->request->post['seo_name']) : '';
            $data['body'] = isset($this->request->post['body']) ? $this->request->post['body'] : '';
            $data['alias'] = isset($this->request->post['alias']) ? $this->request->post['alias'] : '';
            $data['seo_keywords'] = isset($this->request->post['seo_keywords']) ? $this->request->post['seo_keywords'] : '';
            $data['author'] = isset($this->request->post['author']) ? $this->request->post['author'] : '';
            $data['blog_category'] = isset($this->request->post['blog_category']) ? $this->request->post['blog_category'] : '';
            $data['tag_list'] = isset($this->request->post['tag_list']) ? $this->request->post['tag_list'] : '';
            $data['type'] = isset($this->request->post['type']) ? $this->request->post['type'] : 'image';
            $data['video_url'] = isset($this->request->post['video_url']) ? $this->request->post['video_url'] : '';
            try {
                $this->model_blog_blog->addBlog($data);

                $this->session->data['success'] = $this->language->get('text_success_add');
                $this->session->data['success'] = $this->language->get('text_success');
            } catch (Exception $e) {
                $this->error['warning'] = $this->language->get('error_text_check_blog');
            }

            $this->response->redirect($this->url->link('blog/blog', 'user_token=' . $this->session->data['user_token'] . $url = '', true));
        }

        $this->getForm();
    }

    /**
     * edit blogs
     */
    public function edit()
    {
        $this->load->language('blog/blog');

        $this->document->setTitle($this->language->get('heading_title_edit'));

        $this->load->model('blog/blog');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $data['title'] = isset($this->request->post['title']) ? $this->request->post['title'] : '';
            $data['content'] = isset($this->request->post['content']) ? $this->request->post['content'] : '';
            $data['short_content'] = isset($this->request->post['short_content']) ? $this->request->post['short_content'] : '';
            $data['status'] = $this->request->post['status'];
            $data['in_home'] = $this->request->post['in_home'];
            $data['approval_status'] = isset($this->request->post['approval_status']) ? $this->request->post['approval_status'] : '';
            $data['image'] = isset($this->request->post['image']) ? $this->request->post['image'] : '';
            $data['alt'] = isset($this->request->post['alt']) ? $this->request->post['alt'] : '';
            $data['seo_name'] = isset($this->request->post['seo_name']) ? $this->request->post['seo_name'] : '';
            $data['body'] = isset($this->request->post['body']) ? $this->request->post['body'] : '';
            $data['alias'] = isset($this->request->post['alias']) ? $this->request->post['alias'] : '';
            $data['seo_keywords'] = isset($this->request->post['seo_keywords']) ? $this->request->post['seo_keywords'] : '';
            $data['author'] = isset($this->request->post['author']) ? $this->request->post['author'] : '';
            $data['blog_category'] = isset($this->request->post['blog_category']) ? $this->request->post['blog_category'] : '';
            $data['tag_list'] = isset($this->request->post['tag_list']) ? $this->request->post['tag_list'] : '';
            $data['type'] = isset($this->request->post['type']) ? $this->request->post['type'] : 'image';
            $data['video_url'] = isset($this->request->post['video_url']) ? $this->request->post['video_url'] : '';

            $this->model_blog_blog->editBlog($this->request->get['blog_id'], $data);
            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->session->data['success'] = $this->language->get('text_success_edit');
            $this->response->redirect($this->url->link('blog/blog', 'user_token=' . $this->session->data['user_token'] . $url, true));

        }

        $this->getForm();
    }

    /**
     * delete blog
     */
    public function delete()
    {
        $this->load->language('blog/blog');
        $this->load->model('blog/blog');
        $blogs = explode(',', $this->request->get['blog_id']);
        if (isset($blogs)) {
            foreach ($blogs as $blog_id) {
                $this->model_blog_blog->deleteBlog($blog_id);
            }
        }
        $this->session->data['success'] = $this->language->get('delete_success');
        $this->response->redirect($this->url->link('blog/blog', 'user_token=' . $this->session->data['user_token'], true));
    }

    /**
     * publish or un-publish blog
     */
    public function activeStatus()
    {
        $this->load->model('blog/blog');
        $blog_id = (int)$this->request->get['blog_id'];
        $status = $this->request->get['status'];
        $this->model_blog_blog->updateStatus($blog_id, $status);
    }

    public function approvalStatus()
    {
        $this->load->model('blog/blog');
        $blog_id = (int)$this->request->get['blog_id'];
        $approvalStatus = $this->request->get['approvalStatus'];
        $this->model_blog_blog->updateApprovalStatus($blog_id, $approvalStatus);
    }

    /**
     * replace escaped url
     *
     * @param $url
     * @return mixed
     */
    public function replace_url($url)
    {
        return str_replace("amp;", "", $url);
    }

    /**
     * publish or un-publish multiple blogs
     */
    public function updateStatusPublish($active = '')
    {
        $this->load->language('blog/blog');

        $this->load->model('blog/blog');

        $blogs = explode(',', $this->request->get['blog_id']);
        $active = $this->request->get['active'];
        if (isset($blogs)) {
            foreach ($blogs as $blog_id) {
                $this->model_blog_blog->updateStatus($blog_id, $active);
            }
        }
        $this->session->data['success'] = $this->language->get('change_success');
        $this->response->redirect($this->url->link('blog/blog', 'user_token=' . $this->session->data['user_token'], true));
    }

    /**
     * get list blogs with filter
     */
    private function getList()
    {
        /* current page */
        $filter_name = trim($this->getValueFromRequest('get', 'filter_name'));
        $page = $this->getValueFromRequest('get', 'page', 1);
        $filter_author = $this->getValueFromRequest('get', 'filter_author');
        $filter_category = $this->getValueFromRequest('get', 'filter_category');
        $filter_tag = $this->getValueFromRequest('get', 'filter_tag');

        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        /* breadcrumbs */
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('blog/blog', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        /* message */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        /* filter */
        $filter_blog = null;
        if (isset($this->request->get['filter_blog'])) {
            $filter_blog = (int)($this->request->get['filter_blog']);
            $url .= '&filter_blog=' . $filter_blog;
        }

        // TODO: use array, do not set '' for multiple statuses...
        $filter_status = null;
        if (isset($this->request->get['filter_status'])) {
            $filter_status = ($this->request->get['filter_status']);
            if (count($filter_status) > 1) {
                $filter_status = '';
            } else {
                $filter_status = (int)$filter_status[0];
            }
            $url .= '&filter_status=' . $filter_status;
        }

        $filter_approval_status = null;
        if (isset($this->request->get['filter_approval_status'])) {
            $filter_approval_status = $this->request->get['filter_approval_status'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $filter_data = array(
            'filter_name' => $filter_name,
            'filter_status' => $filter_status,
            'filter_author' => $filter_author,
            'filter_category' => $filter_category,
            'filter_tag' => $filter_tag,
            'filter_approval_status' => $filter_approval_status,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        /* selected fields in filter */
        $data['filter_blog'] = $filter_blog;
        $data['filter_status'] = $filter_status;
        $this->load->model('user/user');
        $current_user_id = $this->user->getId();
        $data['user'] = $this->model_user_user->getUser($current_user_id);
        $isAdmin = $this->user->isAdmin() ? true : false;
        $isIntern = $this->user->isIntern();
        if ($isAdmin || $isIntern) {
            $data['approval_status'] = $this->approval_status_admin;
        }
        else {
            $data['approval_status'] = $this->approval_status_ctv;
        }

        $this->load->model('tool/image');
        $results = $this->model_blog_blog->getAllBlogs($filter_data);

        foreach ($results as $result) {
            if ($current_user_id == $result['author'] || $isAdmin || $isIntern) {
                $access_modify = true;
            }
            else {
                $access_modify = false;
            }
            if ($result['image']) {
                $image = $result['image'];
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
            }
            $disable_update = 'omihub' == $result['source'];
            $data['blogs'][] = array(
                'blog_id' => $result['blog_id'],
                'title' => $result['title'],
                'image' => $image,
                'name_category' => $result['blog_category_title'],
                'author' => $disable_update ? $result['source_author_name'] : ($result['lastname'] . ' ' . $result['firstname']),
                'dateModified' => $result['date_modified'],
                'activeStatus' => $result['status'],
                'approval_status' => $result['approval_status'],
                'access_modify' => $access_modify,
                'edit' => $this->url->link('blog/blog/edit', 'user_token=' . $this->session->data['user_token'] . '&blog_id=' . $result['blog_id'] . $url, true),
                'disable_update' => $disable_update
            );
        }

        $data['user_token'] = $this->session->data['user_token'];
        $data['delete'] = $this->url->link('blog/blog/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $blog_total = $this->model_blog_blog->getTotalBlogs($filter_data);

        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $blog_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('blog/blog', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        // Add jump to page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);

        //get all tag
        $data['tag_all'] = $this->model_custom_tag->getAllTagValueProduct();
        $data['action_add_tag'] = $this->url->link('blog/blog/addTags', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_remove_tag'] = $this->url->link('blog/blog/removeTags', 'user_token=' . $this->session->data['user_token'], true);

        $data['results'] = sprintf($this->language->get('text_pagination'), ($blog_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($blog_total - $this->config->get('config_limit_admin'))) ? $blog_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $blog_total, ceil($blog_total / $this->config->get('config_limit_admin')));
        $data['url_filter'] = $this->url->link('blog/blog', 'user_token=' . $this->session->data['user_token'] . $url, true);
        /* href */
        $data['href_list_blog_category'] = $this->url->link('blog/category', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['href_add'] = $this->url->link('blog/blog/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['href_edit'] = $this->url->link('blog/blog', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['href_edit_quantity'] = $this->url->link('blog/blog/editQuantity', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['href_edit_sale_on_out_of_stock'] = $this->url->link('blog/blog/editSaleOnOutOfStock', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['avtivestatus'] = $this->replace_url($this->url->link('blog/blog/activeStatus', ('user_token=' . $this->session->data['user_token'] . $url), true));
        $data['approvalStatus'] = $this->replace_url($this->url->link('blog/blog/approvalStatus', ('user_token=' . $this->session->data['user_token'] . $url), true));
        $data['publish'] = $this->url->link('blog/blog/updateStatusPublish&active=1', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['unPublish'] = $this->url->link('blog/blog/updateStatusPublish&active=0', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['loadOptionOne'] = $this->replace_url($this->url->link('blog/blog/loadoptionone', 'user_token=' . $this->session->data['user_token'] . $url, true));
        $data['delete'] = $this->url->link('blog/blog/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('blog/blog_list_append', $data));
        } else {
            $this->response->setOutput($this->load->view('blog/blog_list', $data));
        }
    }


    public function loadoptionone()
    {
        $this->load->model('blog/blog');
        $this->load->model('custom/tag');
        $this->load->model('blog/category');
        $this->load->model('user/user');
        $this->load->language('blog/blog');
        $categoryID = $this->request->post['categoryID'];
        $start_select_html = '<div class="form-group"><select class="form-control select2 form-control-sm" data-placeholder="Chọn trạng thái" data-label="Trạng thái" id="status" name="status">';
        $end_select_html = '</select></div>';
        $show_html = '';

        if ($categoryID > 0) {
            if ($categoryID == 1) { // Trang thai hien thi
                if (!isset($this->request->post['flag'])) {
                    $show_html .= $start_select_html;
                    $show_html .= '<option value="" data-value="' . $this->language->get('product_all_status') . '">' . $this->language->get('product_all_status') . '</option>';
                    $show_html .= '<option value="1" data-value="' . $this->language->get('entry_status_publish') . '">' . $this->language->get('entry_status_publish') . '</option>';
                    $show_html .= '<option value="0" data-value="' . $this->language->get('entry_status_unpublish') . '">' . $this->language->get('entry_status_unpublish') . '</option>';
                } else {
                    $show_html .= '<div class="form-group"><select class="form-control select2 form-control-sm" 
                                        data-placeholder="' . $this->language->get('filter_status_placeholder') . '" data-label="Trạng thái" id="status" name="status">';
                    $show_html .= '<option value="">' . $this->language->get('filter_status_placeholder') . '</option>';
                    $show_html .= '<option value="1" data-value="' . $this->language->get('entry_status_publish') . '">' . $this->language->get('entry_status_publish') . '</option>';
                    $show_html .= '<option value="0" data-value="' . $this->language->get('entry_status_unpublish') . '">' . $this->language->get('entry_status_unpublish') . '</option>';
                }
                $show_html .= $end_select_html;
            } elseif ($categoryID == 2) { // tac gia
                $start_select_html = '<div class="form-group"><select class="form-control select2 form-control-sm" 
                                           data-placeholder="' . $this->language->get('filter_select_author') . '" data-label="Chọn tác giả" id="author" name="author[]">';
                $end_select_html = '</select></div>';
                $users = $this->model_user_user->getAuthorOfBlogs();
                $show_html .= $start_select_html . '<option value="" data-value="' . $this->language->get('filter_status_placeholder') . '">' . $this->language->get('filter_status_placeholder') . '</option>';
                foreach ($users as $key => $user) {
                    $show_html .= '<option value="' . $user['user_id'] . '" data-value="' . $user['lastname'] . ' ' . $user['firstname'] . '">' . $user['lastname'] . ' ' . $user['firstname'] . '</option>';
                }
                $show_html .= $end_select_html;
            } elseif ($categoryID == 3) { // Danh muc
                $start_select_html = '<div class="form-group"><select class="form-control select2 form-control-sm" data-placeholder="' . $this->language->get('filter_select_category') . '" data-label="Danh mục" id="category_blog" name="category_blog[]">';
                $end_select_html = '</select></div>';
                $categories = $this->model_blog_category->getAllCategories();
                $show_html .= $start_select_html . '<option value="" data-value="' . $this->language->get('filter_category') . '">' . $this->language->get('filter_category_placeholder') . '</option>';
                foreach ($categories as $key => $cate) {
                    $show_html .= '<option value="' . $cate['blog_category_id'] . '" data-value="' . $cate['title'] . '">' . $cate['title'] . '</option>';
                }
                $show_html .= $end_select_html;
            } elseif ($categoryID == 4) { // Duoc tag voi
                $start_select_html = '<div class="form-group"><input type="text" name="nametag" id="nametag" data-label="Tag" placeholder="' . $this->language->get('entry_tag_placeholder') . '" class="form-control" required="">';
                $end_select_html = '</div>';
//                $list_tags = $this->model_custom_tag->getAllTagValueProduct();
//                $show_html .= $start_select_html . '<option value="0" data-value="' . $this->language->get('filter_tag') . '">' . $this->language->get('filter_tag_placeholder') . '</option>';
//                foreach ($list_tags as $tag) {
//                    $show_html .= '<option value="' . $tag['tag_id'] . '" data-value="' . $tag['value'] . '">' . $tag['value'] . '</option>';
//                }
                $show_html .= $start_select_html . $end_select_html;
            }
            elseif ($categoryID == 5) { // Duoc tag voi
                $show_html .= '<div class="form-group"><select class="form-control select2 form-control-sm" 
                                        data-placeholder="' . $this->language->get('filter_approval_status_placeholder') . '" data-label="Trạng thái phê duyệt" id="approval_status" name="approval_status">';
                $show_html .= '<option value="null" data-value="null">Chọn</option>';
                $show_html .= '<option value="request_approval" data-value="Yêu cầu duyêt">Yêu cầu duyêt</option>';
                $show_html .= '<option value="content_approved" data-value="Content đã duyệt">Content đã duyệt</option>';
                $show_html .= '<option value="seo_approved" data-value="SEO đã duyệt">SEO đã duyệt</option>';
                $show_html .= '<option value="fix_request" data-value="Yêu cầu sửa">Yêu cầu sửa</option>';
                $show_html .= $start_select_html . $end_select_html;
            }
        } else {
            $show_html .= '<style>#optionone,#optiontwo{display:none !important}</style>';
        }

        echo $show_html .= '';
    }

    /**
     * get blog form for add/edit
     */
    private function getForm()
    {
        $data['text_form'] = !isset($this->request->get['blog_id']) ? $this->language->get('heading_title_add') : $this->language->get('heading_title_edit');
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['error_text_check'])) {
            $data['error_text_check'] = $this->error['error_text_check'];
        } else {
            $data['error_text_check'] = '';
        }

        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = array();
        }

        if (isset($this->error['keyword'])) {
            $data['error_keyword'] = $this->error['keyword'];
        } else {
            $data['error_keyword'] = '';
        }

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('blog/blog', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        if (isset($this->request->get['blog_id'])) {
            $data['breadcrumbs'][] = array(
                'text' => $data['text_form'],
                'href' => '',
            );
        } else {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_add'),
                'href' => ''
            );
        }

        if (!isset($this->request->get['blog_id'])) {
            $data['action'] = $this->url->link('blog/blog/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        } else {
            $data['action'] = $this->url->link('blog/blog/edit', 'user_token=' . $this->session->data['user_token'] . '&blog_id=' . $this->request->get['blog_id'] . $url, true);
            $data['blog_id'] = $this->request->get['blog_id'];
        }

        $data['cancel'] = $this->url->link('blog/blog', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $this->load->model('user/user');
        $current_user_id = $this->user->getId();
        $data['user'] = $this->model_user_user->getUser($current_user_id);

        $data['disable_update'] = false;
        $data['author_name'] = $data['user']['firstname'] . ' ' . $data['user']['lastname'];
        $data['access_modify'] = true;
        $isAdmin = $this->user->isAdmin() ? true : false;
        $isIntern = $this->user->isIntern();
        if (isset($this->request->get['blog_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $this->load->model('tool/image');
            $blog_info = $this->model_blog_blog->getBlog($this->request->get['blog_id']);
            $data['user'] = $this->model_user_user->getUser($blog_info['author']);
            $data['author_name'] = $data['user']['firstname'] . ' ' . $data['user']['lastname'];
            if ($current_user_id == $blog_info['author'] || $isAdmin || $isIntern) {
                $data['access_modify'] = true;
            }
            else {
                $data['access_modify'] = false;
            }
            // check source to disable update button
            $data['disable_update'] = 'omihub' == $blog_info['source'];
            // check source to get author name
            if ($data['disable_update']) {
                $data['author_name'] =  $blog_info['source_author_name'];
            }
        }

        if (isset($this->request->post['title'])) {
            $data['_title'] = $this->request->post['title'];
        } elseif (!empty($blog_info)) {
            $data['_title'] = $blog_info['title'];
        } else {
            $data['_title'] = '';
        }

        if (isset($this->request->post['content'])) {
            $data['content'] = $this->request->post['content'];
        } elseif (!empty($blog_info)) {
            $data['content'] = $blog_info['content'];
        } else {
            $data['content'] = '';
        }

        if (isset($this->request->post['short_content'])) {
            $data['short_content'] = $this->request->post['short_content'];
        } elseif (!empty($blog_info)) {
            $data['short_content'] = $blog_info['short_content'];
        } else {
            $data['short_content'] = '';
        }

        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($blog_info)) {
            $data['image'] = $blog_info['image'];
        } else {
            $data['image'] = '';
        }

        if (isset($this->request->post['type'])) {
            $data['type'] = $this->request->post['type'];
        } elseif (!empty($blog_info)) {
            $data['type'] = $blog_info['type'];
        } else {
            $data['type'] = 'image';
        }

        if (isset($this->request->post['video_url'])) {
            $data['video_url'] = $this->request->post['video_url'];
        } elseif (!empty($blog_info)) {
            $data['video_url'] = $blog_info['video_url'];
        } else {
            $data['video_url'] = '';
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($blog_info)) {
            $data['status'] = $blog_info['status'];
        } else {
            $data['status'] = true;
        }

        if (isset($this->request->post['in_home'])) {
            $data['in_home'] = $this->request->post['in_home'];
        } elseif (!empty($blog_info)) {
            $data['in_home'] = $blog_info['in_home'];
        } else {
            $data['in_home'] = true;
        }

        $this->load->model('user/user');
        $current_user_id = $this->user->getId();
        $data['user'] = $this->model_user_user->getUser($current_user_id);
        $isAdmin = $this->user->isAdmin() ? true : false;
        $isIntern = $this->user->isIntern();
        if ($isAdmin || $isIntern) {
            $data['approval_status'] = $this->approval_status_admin;
        }
        else {
            $data['approval_status'] = $this->approval_status_ctv;
        }

        $data['approval_status_detail'] = isset($blog_info['approval_status']) ? $blog_info['approval_status'] : '';

        if (isset($this->request->post['seo_name'])) {
            $data['meta_title'] = $this->request->post['seo_name'];
        } elseif (!empty($blog_info)) {
            $data['meta_title'] = $blog_info['meta_title'];
        } else {
            $data['meta_title'] = '';
        }

        if (isset($this->request->post['body'])) {
            $data['meta_description'] = $this->request->post['body'];
        } elseif (!empty($blog_info)) {
            $data['meta_description'] = $blog_info['meta_description'];
        } else {
            $data['meta_description'] = '';
        }

        if (isset($this->request->post['alias'])) {
            $data['alias'] = $this->request->post['alias'];
        } elseif (!empty($blog_info)) {
            $data['alias'] = $blog_info['alias'];
        } else {
            $data['alias'] = '';
        }

        if (isset($this->request->post['seo_keywords'])) {
            $data['seo_keywords'] = $this->request->post['seo_keywords'];
        } elseif (!empty($blog_info)) {
            $data['seo_keywords'] = $blog_info['seo_keywords'];
        } else {
            $data['seo_keywords'] = '';
        }

        $data['user_token'] = $this->session->data['user_token'];

        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();
        $this->load->model('setting/store');

        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($blog_info)) {
            $data['image'] = $blog_info['image'];
        } else {
            $data['image'] = '';
        }

        if (isset($this->request->post['alt'])) {
            $data['alt'] = $this->request->post['alt'];
        } elseif (!empty($blog_info)) {
            $data['alt'] = $blog_info['alt'];
        } else {
            $data['alt'] = '';
        }

        $this->load->model('blog/category');
        $data['cats'] = $this->model_blog_category->getAllCategories();

        if (!empty($blog_info)) {
            $data['category_selected'] = $this->model_blog_category->getCategoryByBlogId($blog_info['blog_id']);
        }

        // tag
        $tag_list = [];
        if (isset($this->request->post['tag_list'])) {
            $this->load->model('custom/tag');
            $tag_list_id = is_array($this->request->post['tag_list']) ? $this->request->post['tag_list'] : [];
            foreach ($tag_list_id as $tag_id) {
                $collection_info = $this->model_custom_tag->getTagById($tag_id);
                if ($collection_info) {
                    $tag_list[] = array(
                        'id' => $collection_info['tag_id'],
                        'title' => $collection_info['value']
                    );
                }
            }
        } else if (isset($this->request->get['blog_id'])) {
            $this->load->model('custom/tag');
            $tag_list = $this->model_custom_tag->getTagsByBlogId($this->request->get['blog_id']);
        }

        $data['tag_list'] = json_encode($tag_list);

        // Image
        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($blog_info)) {
            $data['image'] = $blog_info['image'];
        } else {
            $data['image'] = '';
        }

        $this->load->model('tool/image');

        if (isset($this->request->post['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
        } elseif (!empty($product_info)) {
            $data['thumb'] = $this->model_tool_image->resize($product_info['image'], 100, 100);
        } else {
            $data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        }

        $data['is_on_mobile'] = $this->isMobile();

        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

        if ($this->isSupportThemeFeature(self::$VIDEO_SLIDE)) {
            $data['blog_videos'] = true;
        }

        $data['ckeditor_lang'] = $this->config->get('config_language') == 'vi-vn' ? 'vi' : 'en';
        $data['upload_image'] = str_replace('&amp;', '&', $this->url->link('catalog/product/upload', 'user_token=' . $this->session->data['user_token'] . $url, true));
        $data['filemanager'] = str_replace('&amp;', '&', $this->url->link('common/filemanager', 'user_token=' . $this->session->data['user_token'] . '&ckeditor=1', true));

        $data['server_name'] = $_SERVER['SERVER_NAME'];
        $this->load->model('design/layout');
        $data['layouts'] = $this->model_design_layout->getLayouts();

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('blog/blog_form', $data));
    }

    /**
     * validate form add/edit
     *
     * @param string $action
     * @return bool
     */
    private function validateForm($action = 'edit')
    {
        if (!$this->user->hasPermission('modify', 'blog/blog')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ($action === 'editQuantity') {
            //
        }

        if (empty($this->request->post['seo_name'])) {
            $this->error['warning'] = $this->language->get('error_seo_name');
        }

        if (empty($this->request->post['body'])) {
            $this->error['warning'] = $this->language->get('error_seo_description');
        }

        if (empty($this->request->post['seo_keywords'])) {
            $this->error['warning'] = $this->language->get('error_seo_keywords');
        }

        if (isset($this->request->post['type']) && $this->request->post['type'] == 'image') {
            if (empty($this->request->post['image'])) {
                $this->error['warning'] = $this->language->get('error_image');
            }
            if (empty($this->request->post['alt'])) {
                $this->error['warning'] = $this->language->get('error_image_alt');
            }
        }

        // check exist keyword sync from brand
        $this->load->model('online_store/keyword_sync');
        $checkExistTitle = $this->model_online_store_keyword_sync->checkExistKeywordWithShop($this->request->post['title']);
        $checkExistContent = $this->model_online_store_keyword_sync->checkExistKeywordWithShop($this->request->post['content']);
        $checkExistShortContent = $this->model_online_store_keyword_sync->checkExistKeywordWithShop($this->request->post['short_content']);
        if ($checkExistTitle) {
            $this->error['warning'] = $this->language->get('error_keyword_check_title');
        }
        if ($checkExistContent) {
            $this->error['warning'] = $this->language->get('error_keyword_check_content');
        }
        if ($checkExistShortContent) {
            $this->error['warning'] = $this->language->get('error_keyword_check_short_content');
        }

        return !$this->error;
    }

    public function addTagFast()
    {
        $this->load->language('blog/blog');
        if (isset($this->request->post['text'])) {
            $this->load->model('custom/tag');
            $result = $this->model_custom_tag->addTagFast(trim($this->request->post['text']));
            if ($result) {
                return json_encode(array(
                    "status" => true,
                    "message" => $this->language->get('notice_add_tag_success'),
                ));
            }
        }
        return json_encode(array(
            "status" => false,
            "message" => $this->language->get('remove_tag_error'),
        ));
    }

    public function addTags()
    {
        $this->load->model('custom/tag');
        $this->load->language('blog/blog');

        $blog_ids = $this->request->post['product-id-select'];
        $tag_list = $this->request->post['tag_list'];
        if (!$tag_list) {
            $this->session->data['error'] = $this->language->get('notice_add_tag_error');
            $this->response->redirect($this->url->link('blog/blog', 'user_token=' . $this->session->data['user_token'], true));
        }
        $blog_ids = str_replace(array('[', ']', '"'), array('', '', ''), $blog_ids);
        $blog_ids = explode(',', $blog_ids);

        //add new tags return id
        try {
            $tag_adds = $this->model_custom_tag->insertTag($tag_list);
            foreach ($tag_list as $key => $tag) {
                if (in_array($tag, $tag_adds)) {
                    $tag_list[$key] = (string)array_search($tag, $tag_adds);
                }
            }

            $this->model_custom_tag->insertTagBlog($blog_ids, $tag_list);
            $this->session->data['success'] = $this->language->get('notice_add_tag_success');
            $this->response->redirect($this->url->link('blog/blog', 'user_token=' . $this->session->data['user_token'], true));
        } catch (Exception $e) {
            $this->session->data['error'] = $e->getMessage();
            $this->response->redirect($this->url->link('blog/blog', 'user_token=' . $this->session->data['user_token'], true));
        }
    }

    public function removeTags()
    {
        $this->load->model('custom/tag');
        $this->load->language('blog/blog');

        $blog_ids = $this->request->post['product-id-select'];
        $tag_list = $this->request->post['tag_list'];
        if (!$tag_list) {
            $this->session->data['error'] = $this->language->get('notice_add_tag_error');
            $this->response->redirect($this->url->link('blog/blog', 'user_token=' . $this->session->data['user_token'], true));
        }
        $blog_ids = str_replace(array('[', ']', '"'), array('', '', ''), $blog_ids);
        $blog_ids = explode(',', $blog_ids);
        try {
            $this->model_custom_tag->deleteTagBlog($blog_ids, $tag_list);
        } catch (Exception $e) {
            $this->session->data['error'] = $this->language->get('notice_remove_tag_try');
            $this->response->redirect($this->url->link('blog/blog', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->session->data['success'] = $this->language->get('notice_remove_tag_success');
        $this->response->redirect($this->url->link('blog/blog', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function getBlogLazyLoad()
    {
        $json = array();
        $this->load->model('blog/blog');
        $this->load->language('blog/blog');
        $this->load->model('tool/image');
        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        // display 6 product in select config theme is many, or setup other config
        $filter_data = array(
            'limit' => 6,
            'start' => ($page - 1) * 6,
        );

        if (isset($this->request->get['filter_title'])) {
            $filter_data['filter_title'] = trim($this->request->get['filter_title']);
        }
        $results = $this->model_blog_blog->getBlogPaginator($filter_data);

        foreach ($results as $key => $value) {
            if ($value['image'] != '') {
                $image = $this->model_tool_image->resize($value['image'], 100, 100);
            } else {
                $image = $this->model_tool_image->resize('no_image.png', 100, 100);
            }

            $json['results'][] = array(
                'id' => $value['blog_id'],
                'blog_id' => $value['blog_id'],
                'title' => $value['title'],
                'text' => $value['title'],
                'content' => $value['content'],
                'image' => $image,
            );
        }
        $count_tag = $this->model_blog_blog->countBlogPaginator($filter_data);
        $json['count'] = $count_tag;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
