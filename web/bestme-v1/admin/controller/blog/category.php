<?php

class ControllerBlogCategory extends Controller
{

    private $error = [];

    public function index()
    {
        $this->getList();
    }

    public function getBlogCategoryLazyLoad()
    {
        $json = array();
        $this->load->model('blog/category');
        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        //display 6 product in select config theme is many, or setup other config
        $filter_data = array(
            'limit' => 6,
            'start' => ($page - 1) * 6,
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }
        $results = $this->model_blog_category->getBlogCategoryPaginator($filter_data);
        foreach ($results as $key => $value) {
            $json['results'][] = array(
                'text' => $value['title'],
                'id' => $value['blog_category_id']
            );
        }
        $count_tag = $this->model_blog_category->countBlogCategory($filter_data);
        $json['count'] = $count_tag;
        //array_multisort($sort_order, SORT_ASC, $json);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function add()
    {
        $this->load->model('blog/category');
        $this->load->language('blog/category');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm($this->request->post)) {
            $data = $this->request->post;
            $category_id = $this->model_blog_category->createBlogCategory($data);
            if ($category_id && isset($data['menu']) && is_array($data['menu'])) {
                $this->load->model('custom/menu');
                $data_menu = [
                    'menu_name' => $data['title'],
                    'target_value' => $category_id,
                    'menu_type_id' => ModelCustomMenu::MENU_TYPE_BLOG
                ];
                foreach ($data['menu'] as $menu) {
                    $data_menu['parent_id'] = $menu;
                    $this->model_custom_menu->insertMenu($data_menu);
                }
            }
            $this->session->data['success'] = $this->language->get('text_success_add');
            $this->response->redirect($this->url->link('blog/category', 'user_token=' . $this->session->data['user_token'], true));
        }
        $this->getForm();
    }

    public function edit()
    {
        $this->load->model('blog/category');
        $this->load->language('blog/category');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm($this->request->post)) {
            $data = $this->request->post;
            $this->model_blog_category->editBlogCategory($data);
            // update category
            $old_category = $this->model_blog_category->getBlogCategoryByParent($this->request->post['blog_category_id']);

            $old_category_ids = array_map(function ($category) {
                return $category['blog_category_id'];
            }, $old_category);

            $remove_ids = array_diff($old_category_ids, $data['category']);
            $add_ids = array_diff($data['category'], $old_category_ids);

            if (is_array($add_ids)) {
                foreach ($add_ids as $item) {
                    $this->model_blog_category->updateParent($item, $this->request->post['blog_category_id']);
                }
            }

            if (is_array($remove_ids)) {
                foreach ($remove_ids as $item) {
                    $this->model_blog_category->removeCategory($item);
                }
            }


            $this->load->model('custom/menu');
            $old_menu = $this->model_custom_menu->getMenuParentIdByTarget(ModelCustomMenu::MENU_TYPE_BLOG, $this->request->get['blog_category_id']);
            $data['menu'] = isset($data['menu']) ? $data['menu'] : [];
            if (is_array($old_menu)) {
                foreach ($old_menu as $item) {
                    if (!isset($item['menu_id']) || !isset($item['parent_id'])) {
                        continue;
                    }

                    if (!isset($data['menu']) || !is_array($data['menu'])) {
                        continue;
                    }

                    if (in_array($item['parent_id'], $data['menu'])) {
                        $data_menu = [
                            'menu_id' => $item['menu_id'],
                            'menu_name' => $data['title'],
                            'target_value' => $this->request->get['blog_category_id'],
                            'menu_type_id' => ModelCustomMenu::MENU_TYPE_BLOG,
                            'parent_id' => $item['parent_id']
                        ];
                        $this->model_custom_menu->updateMenu($data_menu);

                        if (($key = array_search($item['menu_id'], $data['menu'])) !== false) {
                            unset($data['menu'][$key]);
                        }
                    } else {
                        $this->model_custom_menu->removeFromMenu($item['parent_id'], ModelCustomMenu::MENU_TYPE_BLOG, $this->request->get['blog_category_id']);
                    }
                }
            }
            // update menu collection
            if (isset($data['menu']) && $data['menu']) {
                $data_menu = [
                    'menu_name' => $data['title'],
                    'target_value' => $this->request->get['blog_category_id'],
                    'menu_type_id' => ModelCustomMenu::MENU_TYPE_BLOG
                ];
                foreach ($data['menu'] as $menu) {
                    if ($this->model_custom_menu->isExistMenu($menu, $this->request->get['blog_category_id'])) continue;
                    $data_menu['parent_id'] = $menu;
                    $this->model_custom_menu->insertMenu($data_menu);
                }
            }

            $this->session->data['success'] = $this->language->get('text_success_edit');
            $this->response->redirect($this->url->link('blog/category', 'user_token=' . $this->session->data['user_token'], true));
        }
        $this->getForm();
    }

    public function getList()
    {
        $this->load->language('blog/category');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('blog/category');

        $data = [];

        /* current page */
        $filter_name = trim($this->getValueFromRequest('get', 'filter_name'));
        $page = $this->getValueFromRequest('get', 'page', 1);

        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        /* breadcrumbs */
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_blog_title'),
            'href' => $this->url->link('blog/blog', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('blog/category', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        /* message */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        /* filter */
        $filter_blog = null;
        if (isset($this->request->get['filter_blog'])) {
            $filter_blog = (int)($this->request->get['filter_blog']);
            $url .= '&filter_blog=' . $filter_blog;
        }

        $filter_status = null;
        if (isset($this->request->get['filter_status'])) {
            $filter_status = ($this->request->get['filter_status']);
            if (count($filter_status) > 1) {
                $filter_status = '';
            } else {
                $filter_status = (int)$filter_status[0];
            }
            $url .= '&filter_status=' . $filter_status;
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $filter_data = array(
            'filter_name' => $filter_name,
            'filter_status' => $filter_status,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        /* selected fields in filter */
        $data['filter_blog'] = $filter_blog;
        $data['filter_status'] = $filter_status;

        $this->load->model('tool/image');
        $results = $this->model_blog_category->getAllCategories($filter_data);

        foreach ($results as $result) {
            $number_blog = $this->model_blog_category->getTotalBlogToCategorys($result['blog_category_id']);
            $data['blog_categorys'][] = array(
                'blog_category_id' => $result['blog_category_id'],
                'title' => $result['title'],
                'status' => $result['status'],
                'number_blog' => $number_blog,
                'edit' => $this->url->link('blog/category/edit', 'user_token=' . $this->session->data['user_token'] . '&blog_category_id=' . $result['blog_category_id'] . $url, true),
                'disable_update' => 'omihub' == $result['source']
            );
        }

        $data['user_token'] = $this->session->data['user_token'];
        $data['delete'] = $this->url->link('blog/category/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['action_add_blog_category'] = $this->url->link('blog/category/add', 'user_token=' . $this->session->data['user_token'], true);
        $blog_category_total = $this->model_blog_category->getTotalBlogCategories($filter_data);

        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $blog_category_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('blog/category', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        // Add jump to page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);

        $data['loadOptionOne'] = $this->replace_url($this->url->link('catalog/product/loadoptionone', 'user_token=' . $this->session->data['user_token'] . $url, true));
        $data['results'] = sprintf($this->language->get('text_pagination'), ($blog_category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($blog_category_total - $this->config->get('config_limit_admin'))) ? $blog_category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $blog_category_total, ceil($blog_category_total / $this->config->get('config_limit_admin')));
        $data['url_filter'] = $this->url->link('blog/category', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['avtivestatus'] = $this->replace_url($this->url->link('blog/category/activeStatus', ('user_token=' . $this->session->data['user_token'] . $url), true));
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('blog/category_list_append', $data));
        } else {
            $this->response->setOutput($this->load->view('blog/category_list', $data));
        }
    }

    public function getForm()
    {
        $this->load->language('blog/category');

        $this->document->setTitle($this->language->get('heading_title_add'));

        $this->load->model('blog/category');

        $data = [];

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumbs_setting_title'),
            'href' => $this->url->link('settings/settings', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('blog/category', 'user_token=' . $this->session->data['user_token'], true)
        );
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['category'] = [];
        $data['breadcrumb_last'] = $this->language->get('breadcrumbs_add_blog_category_title');

        $data['action'] = $this->url->link('blog/category/add', 'user_token=' . $this->session->data['user_token'], true);
        $data['cancel'] = $this->url->link('blog/category', 'user_token=' . $this->session->data['user_token'], true);

        $data['disable_update'] = false;
        if (isset($this->request->get['blog_category_id'])) {
            $this->load->model('custom/menu');
            $this->document->setTitle($this->language->get('heading_title_edit'));
            $data['category'] = $this->model_blog_category->getBlogCategory($this->request->get['blog_category_id']);
            $data['breadcrumb_last'] = $data['category']['title'];
            $data['menu_selected'] = json_encode($this->model_custom_menu->getMenuParentInfoByTarget(ModelCustomMenu::MENU_TYPE_BLOG, $this->request->get['blog_category_id']));
            $data['category_selected'] = json_encode($this->model_blog_category->getBlogCategoryByParent($this->request->get['blog_category_id']));
            $data['action'] = $this->url->link('blog/category/edit', 'user_token=' . $this->session->data['user_token'] . "&blog_category_id=" . $this->request->get['blog_category_id'], true);
            $data['action_delete'] = $this->url->link('blog/category/delete', 'user_token=' . $this->session->data['user_token'] . "&blog_category_id=" . $this->request->get['blog_category_id'], true);

            // check source to disable update button
            $data['disable_update'] = 'omihub' == $data['category']['source'];
        }

        if (isset($this->request->post['title'])) {
            $data['category']['title'] = $this->request->post['title'];
        }
        if (isset($this->request->post['seo_title'])) {
            $data['category']['meta_title'] = $this->request->post['seo_title'];
        }
        if (isset($this->request->post['seo_description'])) {
            $data['category']['meta_description'] = $this->request->post['seo_description'];
        }
        if (isset($this->request->post['alias'])) {
            $data['category']['alias'] = $this->request->post['alias'];
        }

        $data['breadcrumbs'][] = array(
            'text' => $data['breadcrumb_last']
        );

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($data['category'])) {
            $data['status'] = $data['category']['status'];
        } else {
            $data['status'] = 1;
        }

        $data['text_form'] = $this->language->get('blog_add_category_title');

        $data['server_name'] = $_SERVER['SERVER_NAME'];

        $data['user_token'] = $this->session->data['user_token'];

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('blog/category_form', $data));
    }

    public function getCategories()
    {
        $json = array();
        $this->load->model('blog/category');
        $results = $this->model_blog_category->getAllCategories(['get_bc_detail'=> true]);
        foreach ($results as $key => $value) {
            $json['results'][] = array(
                'text' => $value['title'],
                'id' => $value['blog_category_id']
            );
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function delete()
    {
        $this->load->language('blog/category');
        $this->load->model('blog/category');
        if ($this->request->get['blog_category_id'] && $this->validateDelete()) {
            $blog_categories = explode(',', $this->request->get['blog_category_id']);
            foreach ($blog_categories as $blog_category) {
                $this->model_blog_category->deleteCategory($blog_category);
            }

            $this->session->data['success'] = $this->language->get('delete_success');
            $this->response->redirect($this->url->link('blog/category', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getList();
    }

    /**
     * publish or un-publish blog
     */
    public function activeStatus()
    {
        $this->load->model('blog/category');
        $blog_category_id = (int)$this->request->get['blog_category_id'];
        $status = $this->request->get['status'];
        $this->model_blog_category->updateStatus($blog_category_id, $status);
    }

    /**
     * replace escaped url
     *
     * @param $url
     * @return mixed
     */
    public function replace_url($url)
    {
        return str_replace("amp;", "", $url);
    }

    private function validateForm($data)
    {
        $this->load->model('blog/category');

        if (!$this->user->hasPermission('modify', 'blog/category')) {
            $this->error['warning'][] = $this->language->get('error_permission');
        }

        if (!isset($data['title']) || empty($data['title'])) {
            $this->error['warning'][] = $this->language->get('error_name_empty');
        } elseif ((utf8_strlen($data['title']) < 1) || (utf8_strlen($data['title']) > 100)) {
            $this->error['warning'][] = $this->language->get('error_name_long');
        } else {
            $category_id = "";
            if (isset($data['blog_category_id'])) {
                $category_id = $data['blog_category_id'];
            }
            if ($this->model_blog_category->getBlogCategoryByName($data['title'], $category_id)) { //validate name
                $this->error['warning'][] = $this->language->get('error_name_exist');
            }
        }
        return !$this->error;
    }

    protected function validateDelete()
    {
        if (!$this->user->hasPermission('modify', 'blog/category')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!isset($this->request->get['blog_category_id'])) {
            return !$this->error;
        }

        $this->load->model('blog/category');

        $blog_categories = explode(',', $this->request->get['blog_category_id']);
        foreach ($blog_categories as $blog_category_id) {
            $count_blog_in_blog_cat = $this->model_blog_category->getTotalBlogToCategorys($blog_category_id);
            if ($count_blog_in_blog_cat > 0) {
                $this->error['warning'] = $this->language->get('error_category');
                break;
            }
        }

        return !$this->error;
    }
}