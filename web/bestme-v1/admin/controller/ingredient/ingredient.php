<?php
class ControllerIngredientIngredient extends Controller {
    private $error = array();

    public function index()
    {
        $this->load->language('ingredient/ingredient');
        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('ingredient/ingredient');

        $this->getList();
    }

    public function add()
    {
        $this->load->language('ingredient/ingredient');
        $this->document->setTitle($this->language->get('heading_title_add'));
        $this->load->model('ingredient/ingredient');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            try {
                $data = isset($this->request->post) ? $this->request->post : [];
                $this->model_ingredient_ingredient->addIngredient($data);

                $this->session->data['success'] = $this->language->get('text_success');
            } catch (Exception $e) {
                $this->error['warning'] = $e->getMessage();
            }

            $this->response->redirect($this->url->link('ingredient/ingredient', 'user_token=' . $this->session->data['user_token'] . $url = '', true));
        }

        $this->getForm();
    }

    public function edit()
    {
        $this->load->language('ingredient/ingredient');
        $this->document->setTitle($this->language->get('heading_title_add'));
        $this->load->model('ingredient/ingredient');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            try {
                $ingredient_id = !empty($this->request->get['ingredient_id']) ? $this->request->get['ingredient_id'] : 0;
                if (!$ingredient_id) {
                    $this->error['warning'] = $this->language->get('text_edit_not_found_id');
                }

                $data = isset($this->request->post) ? $this->request->post : [];
                $this->model_ingredient_ingredient->editIngredient($ingredient_id, $data);

                $this->session->data['success'] = $this->language->get('text_update_success');
            } catch (Exception $e) {
                $this->error['warning'] = $e->getMessage();
            }

            $this->response->redirect($this->url->link('ingredient/ingredient', 'user_token=' . $this->session->data['user_token'] . $url = '', true));
        }

        $this->getForm();
    }

    public function delete()
    {
        $this->load->language('ingredient/ingredient');

        if ($this->validateDelete()) {
            try {
                $this->document->setTitle($this->language->get('heading_title'));
                $this->load->model('ingredient/ingredient');

                $ingredient_ids = $this->request->get['ingredient_ids'];

                if (!empty($ingredient_ids)) {
                    $this->model_ingredient_ingredient->deleteIngredient($ingredient_ids);
                }

                $this->session->data['success'] = $this->language->get('delete_success');

            } catch (Exception $exception) {
                $this->error['warning'] = $exception->getMessage();
            }

            $this->response->redirect($this->url->link('ingredient/ingredient', 'user_token=' . $this->session->data['user_token'], true));
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $this->session->data['error'] = $data['error_warning'];
        $this->response->redirect($this->url->link('ingredient/ingredient', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function importExcel()
    {
        $this->load->language('ingredient/ingredient');
        $this->load->model('ingredient/ingredient');

        $json = array();
        // Allowed file mime types
        $allowed = array(
            'application/vnd.ms-excel',          // xls
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' // xlsx
        );
        $file = $this->request->files['import-file'];
        $max_file_size = defined('MAX_FILE_SIZE_IMPORT_PRODUCTS') ? MAX_FILE_SIZE_IMPORT_PRODUCTS : 1048576;
        if (!isset($file['size']) || $file['size'] > $max_file_size) {
            $json['error'] = $this->language->get('error_filesize');
        }
        if (!in_array($file['type'], $allowed)) {
            $json['error'] = $this->language->get('error_filetype');
        }

        if (!$json) {
            try {
                // start set read data only
                $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($file['tmp_name']);
                $reader->setReadDataOnly(true);
                // end

                $spreadsheet = $reader->load($file['tmp_name']);
                $firstSheetIndex = $spreadsheet->getFirstSheetIndex();
                $sheetData = $spreadsheet->getSheet($firstSheetIndex)->toArray(null, true, true, true);
            } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
                $json['error'] = $this->language->get('error_unknown');
                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));
            }
            $max_file_rows = defined('MAX_ROWS_IMPORT_PRODUCTS') ? MAX_ROWS_IMPORT_PRODUCTS : 501;
            if (count($sheetData) > $max_file_rows) {
                $json['error'] = $this->language->get('error_limit');
            } else {
                $header = array_shift($sheetData);
                $header = $this->mapHeaderFromFile($header); /// remove empty element
                if (!$header) {
                    $json['error'] = $this->language->get('error_file_incomplete');
                } else {
                    if (!array_key_exists('name', $header) || !array_key_exists('code', $header)) {
                        $json['error'] = $this->language->get('error_not_found_fields');
                    } else {
                        $dataIngredient = $this->mapDataFromFile($sheetData, $header);

                        if (!$json) {
                            $this->model_ingredient_ingredient->processImportExcel($dataIngredient);
                        }
                    }
                }
            }
        }

        if (!$json) {
            $json['success'] = $this->language->get('text_uploaded') . ' ' . count($dataIngredient) . $this->language->get('text_product_uploaded');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));
    }

    public function importExcelProductIngredient()
    {
        $this->load->language('ingredient/ingredient');
        $this->load->model('ingredient/ingredient');

        $json = array();
        // Allowed file mime types
        $allowed = array(
            'application/vnd.ms-excel',          // xls
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' // xlsx
        );
        $file = $this->request->files['import-file'];
        $max_file_size = defined('MAX_FILE_SIZE_IMPORT_PRODUCTS') ? MAX_FILE_SIZE_IMPORT_PRODUCTS : 1048576;
        if (!isset($file['size']) || $file['size'] > $max_file_size) {
            $json['error'] = $this->language->get('error_filesize');
        }
        if (!in_array($file['type'], $allowed)) {
            $json['error'] = $this->language->get('error_filetype');
        }

        if (!$json) {
            try {
                // start set read data only
                $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($file['tmp_name']);
                $reader->setReadDataOnly(true);
                // end

                $spreadsheet = $reader->load($file['tmp_name']);
                $firstSheetIndex = $spreadsheet->getFirstSheetIndex();
                $sheetData = $spreadsheet->getSheet($firstSheetIndex)->toArray(null, true, true, true);
            } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
                $json['error'] = $this->language->get('error_unknown');
                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));
            }
            $max_file_rows = defined('MAX_ROWS_IMPORT_PRODUCTS') ? MAX_ROWS_IMPORT_PRODUCTS : 501;
            if (count($sheetData) > $max_file_rows) {
                $json['error'] = $this->language->get('error_limit');
            } else {
                $header = array_shift($sheetData);
                $header = $this->mapHeaderFromFileProductIngredient($header); /// remove empty element
                if (!$header) {
                    $json['error'] = $this->language->get('error_file_incomplete');
                } else {
                    if (!array_key_exists('product_sku', $header) ||
                        !array_key_exists('ingredients', $header) ||
                        !array_key_exists('quantitative', $header)
                    ) {
                        $json['error'] = $this->language->get('error_not_found_fields');
                    } else {
                        $data = $this->mapDataFromFileProductIngredient($sheetData, $header);

                        if (!$json) {
                            $this->model_ingredient_ingredient->processImportExcelProductIngredient($data);
                        }
                    }
                }
            }
        }

        if (!$json) {
            $json['success'] = $this->language->get('text_uploaded') . ' ' . count($data) . $this->language->get('text_product_ingredient_uploaded');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));
    }

    private function getList()
    {
        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('ingredient/ingredient', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['href_add'] = $this->url->link('ingredient/ingredient/add', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $page = !empty($this->request->get['page']) ? $this->request->get['page'] : 1;

        $filter_data = [
            'filter_name' => !empty($this->request->get['filter_name']) ? trim($this->request->get['filter_name']) : '',
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        ];

        $ingredient_total = $this->model_ingredient_ingredient->getTotalIngredientsCustom($filter_data);
        $results = $this->model_ingredient_ingredient->getIngredientsCustom($filter_data);

        foreach ($results as $result) {
            $data['ingredients'][] = array(
                'ingredient_id' => $result['ingredient_id'],
                'name' => $result['name'],
                'code' => $result['code'],
                'updated_at' => $result['updated_at'],
                'edit' => $this->url->link('ingredient/ingredient/edit', 'user_token=' . $this->session->data['user_token'] . '&ingredient_id=' . $result['ingredient_id'] . $url, true),
                'disable_update' => false
            );
        }

        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $ingredient_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('ingredient/ingredient', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        // Add jump to page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);

        // clear notice
        if (isset($this->session->data['success']) && $this->session->data['success'] != 'text_success') {
            $data['success'] = $this->session->data['success'];

            $this->session->data['success'] = '';
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];

            $this->session->data['error'] = '';
            unset($this->session->data['error']);
        } else {
            $data['error_warning'] = '';
        }

        $data['delete'] = $this->url->link('ingredient/ingredient/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['results'] = sprintf($this->language->get('text_pagination'), ($ingredient_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($ingredient_total - $this->config->get('config_limit_admin'))) ? $ingredient_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $ingredient_total, ceil($ingredient_total / $this->config->get('config_limit_admin')));
        $data['url_filter'] = $this->url->link('ingredient/ingredient', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['user_token'] = $this->session->data['user_token'];
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        // result view page
        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('ingredient/ingredient_append', $data));
        } else {
            $this->response->setOutput($this->load->view('ingredient/ingredient', $data));
        }
    }

    private function getForm()
    {
        $data['text_form'] = !isset($this->request->get['ingredient_id']) ? $this->language->get('heading_title_add') : $this->language->get('heading_title_edit');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $url = '';

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('ingredient/ingredient', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        if (isset($this->request->get['ingredient_id'])) {
            $data['breadcrumbs'][] = array(
                'text' => $data['text_form'],
                'href' => '',
            );
        } else {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_add'),
                'href' => ''
            );
        }

        if (!isset($this->request->get['ingredient_id'])) {
            $data['action'] = $this->url->link('ingredient/ingredient/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        } else {
            $data['action'] = $this->url->link('ingredient/ingredient/edit', 'user_token=' . $this->session->data['user_token'] . '&ingredient_id=' . $this->request->get['ingredient_id'] . $url, true);
            $data['ingredient_id'] = $this->request->get['ingredient_id'];
        }

        $data['cancel'] = $this->url->link('ingredient/ingredient', 'user_token=' . $this->session->data['user_token'] . $url, true);

        if (!empty($this->request->get['ingredient_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $ingredient_info = $this->model_ingredient_ingredient->getIngredientById($this->request->get['ingredient_id']);
        }

        if (isset($this->request->post['name'])) {
            $data['name'] = $this->request->post['name'];
        } elseif (!empty($ingredient_info)) {
            $data['name'] = $ingredient_info['name'];
        } else {
            $data['name'] = '';
        }

        if (isset($this->request->post['code'])) {
            $data['code'] = $this->request->post['code'];
        } elseif (!empty($ingredient_info)) {
            $data['code'] = $ingredient_info['code'];
        } else {
            $data['code'] = '';
        }

        if (isset($this->request->post['active_ingredient'])) {
            $data['active_ingredient'] = $this->request->post['active_ingredient'];
        } elseif (!empty($ingredient_info)) {
            $data['active_ingredient'] = $ingredient_info['active_ingredient'];
        } else {
            $data['active_ingredient'] = '';
        }

        if (isset($this->request->post['unit'])) {
            $data['unit'] = $this->request->post['unit'];
        } elseif (!empty($ingredient_info)) {
            $data['unit'] = $ingredient_info['unit'];
        } else {
            $data['unit'] = '';
        }

        if (isset($this->request->post['basic_description'])) {
            $data['basic_description'] = $this->request->post['basic_description'];
        } elseif (!empty($ingredient_info)) {
            $data['basic_description'] = $ingredient_info['basic_description'];
        } else {
            $data['basic_description'] = '';
        }

        if (isset($this->request->post['note'])) {
            $data['note'] = $this->request->post['note'];
        } elseif (!empty($ingredient_info)) {
            $data['note'] = $ingredient_info['note'];
        } else {
            $data['note'] = '';
        }

        if (isset($this->request->post['overdose'])) {
            $data['overdose'] = $this->request->post['overdose'];
        } elseif (!empty($ingredient_info)) {
            $data['overdose'] = $ingredient_info['overdose'];
        } else {
            $data['overdose'] = '';
        }

        if (isset($this->request->post['course_certificates'])) {
            $data['course_certificates'] = $this->request->post['course_certificates'];
        } elseif (!empty($ingredient_info)) {
            $data['course_certificates'] = $ingredient_info['course_certificates'];
        } else {
            $data['course_certificates'] = '';
        }

        if (isset($this->request->post['detail_description'])) {
            $data['detail_description'] = $this->request->post['detail_description'];
        } elseif (!empty($ingredient_info)) {
            $data['detail_description'] = $ingredient_info['detail_description'];
        } else {
            $data['detail_description'] = '';
        }

        if (isset($this->request->post['us_man_19_30'])) {
            $data['us_man_19_30'] = $this->request->post['us_man_19_30'];
        } elseif (!empty($ingredient_info)) {
            $data['us_man_19_30'] = $ingredient_info['us_man_19_30'];
        } else {
            $data['us_man_19_30'] = '';
        }

        if (isset($this->request->post['us_man_31_50'])) {
            $data['us_man_31_50'] = $this->request->post['us_man_31_50'];
        } elseif (!empty($ingredient_info)) {
            $data['us_man_31_50'] = $ingredient_info['us_man_31_50'];
        } else {
            $data['us_man_31_50'] = '';
        }

        if (isset($this->request->post['us_man_51_70'])) {
            $data['us_man_51_70'] = $this->request->post['us_man_51_70'];
        } elseif (!empty($ingredient_info)) {
            $data['us_man_51_70'] = $ingredient_info['us_man_51_70'];
        } else {
            $data['us_man_51_70'] = '';
        }

        if (isset($this->request->post['us_man_gt_70'])) {
            $data['us_man_gt_70'] = $this->request->post['us_man_gt_70'];
        } elseif (!empty($ingredient_info)) {
            $data['us_man_gt_70'] = $ingredient_info['us_man_gt_70'];
        } else {
            $data['us_man_gt_70'] = '';
        }

        if (isset($this->request->post['us_women_19_30'])) {
            $data['us_women_19_30'] = $this->request->post['us_women_19_30'];
        } elseif (!empty($ingredient_info)) {
            $data['us_women_19_30'] = $ingredient_info['us_women_19_30'];
        } else {
            $data['us_women_19_30'] = '';
        }

        if (isset($this->request->post['us_women_31_50'])) {
            $data['us_women_31_50'] = $this->request->post['us_women_31_50'];
        } elseif (!empty($ingredient_info)) {
            $data['us_women_31_50'] = $ingredient_info['us_women_31_50'];
        } else {
            $data['us_women_31_50'] = '';
        }

        if (isset($this->request->post['us_women_51_70'])) {
            $data['us_women_51_70'] = $this->request->post['us_women_51_70'];
        } elseif (!empty($ingredient_info)) {
            $data['us_women_51_70'] = $ingredient_info['us_women_51_70'];
        } else {
            $data['us_women_51_70'] = '';
        }

        if (isset($this->request->post['us_women_gt_70'])) {
            $data['us_women_gt_70'] = $this->request->post['us_women_gt_70'];
        } elseif (!empty($ingredient_info)) {
            $data['us_women_gt_70'] = $ingredient_info['us_women_gt_70'];
        } else {
            $data['us_women_gt_70'] = '';
        }

        if (isset($this->request->post['vn_man_19_50'])) {
            $data['vn_man_19_50'] = $this->request->post['vn_man_19_50'];
        } elseif (!empty($ingredient_info)) {
            $data['vn_man_19_50'] = $ingredient_info['vn_man_19_50'];
        } else {
            $data['vn_man_19_50'] = '';
        }

        if (isset($this->request->post['vn_man_51_60'])) {
            $data['vn_man_51_60'] = $this->request->post['vn_man_51_60'];
        } elseif (!empty($ingredient_info)) {
            $data['vn_man_51_60'] = $ingredient_info['vn_man_51_60'];
        } else {
            $data['vn_man_51_60'] = '';
        }

        if (isset($this->request->post['vn_man_gt_60'])) {
            $data['vn_man_gt_60'] = $this->request->post['vn_man_gt_60'];
        } elseif (!empty($ingredient_info)) {
            $data['vn_man_gt_60'] = $ingredient_info['vn_man_gt_60'];
        } else {
            $data['vn_man_gt_60'] = '';
        }

        if (isset($this->request->post['vn_women_19_50'])) {
            $data['vn_women_19_50'] = $this->request->post['vn_women_19_50'];
        } elseif (!empty($ingredient_info)) {
            $data['vn_women_19_50'] = $ingredient_info['vn_women_19_50'];
        } else {
            $data['vn_women_19_50'] = '';
        }

        if (isset($this->request->post['vn_women_51_60'])) {
            $data['vn_women_51_60'] = $this->request->post['vn_women_51_60'];
        } elseif (!empty($ingredient_info)) {
            $data['vn_women_51_60'] = $ingredient_info['vn_women_51_60'];
        } else {
            $data['vn_women_51_60'] = '';
        }

        if (isset($this->request->post['vn_women_gt_60'])) {
            $data['vn_women_gt_60'] = $this->request->post['vn_women_gt_60'];
        } elseif (!empty($ingredient_info)) {
            $data['vn_women_gt_60'] = $ingredient_info['vn_women_gt_60'];
        } else {
            $data['vn_women_gt_60'] = '';
        }

        $data['ckeditor_lang'] = $this->config->get('config_language') == 'vi-vn' ? 'vi' : 'en';
        $data['upload_image'] = str_replace('&amp;', '&', $this->url->link('catalog/product/upload', 'user_token=' . $this->session->data['user_token'] . $url, true));
        $data['filemanager'] = str_replace('&amp;', '&', $this->url->link('common/filemanager', 'user_token=' . $this->session->data['user_token'] . '&ckeditor=1', true));

        $data['user_token'] = $this->session->data['user_token'];

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('ingredient/ingredient_form', $data));
    }

    /**
     * validate form add/edit
     *
     * @param string $action
     * @return bool
     */
    private function validateForm($action = 'edit')
    {
        if (!$this->user->hasPermission('modify', 'ingredient/ingredient')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    protected function mapDataFromFile(array $data, array $header)
    {
        if (!is_array($data) || empty($data) || !is_array($header) || empty($header)) {
            return [];
        }

        $result = array();
        foreach ($data as $row => $info) {
            if ($this->is_array_empty($info)) {
                continue;
            }

            $current = $header;
            foreach ($current as $k => $v) {
                $current[$k] = $info[$v];
            }

            $result[] = $current;
        }

        return $result;
    }

    protected function mapDataFromFileProductIngredient(array $data, array $header)
    {
        if (!is_array($data) || empty($data) || !is_array($header) || empty($header)) {
            return [];
        }

        $result = array();
        foreach ($data as $row => $info) {
            if ($this->is_array_empty($info)) {
                continue;
            }

            $current = $header;
            foreach ($current as $k => $v) {
                switch ($k) {
                    case 'ingredients':
                        $current_values = preg_split('/\r\n|\r|\n/', $info[$v]);
                        $ingredients = [];
                        foreach ($current_values as $c_val) {
                            $ingredients[] = trim($c_val);
                        }
                        $current[$k] = $ingredients;

                        break;
                    case 'quantitative':
                        $current_values = preg_split('/\r\n|\r|\n/', $info[$v]);
                        $quantitative = [];
                        foreach ($current_values as $c_val) {
                            $quantitative[] = trim($c_val);
                        }
                        $current[$k] = $quantitative;

                        break;
                    default:
                        $current[$k] = $info[$v];
                }
            }

            $result[] = $current;
        }

        return $result;
    }

    protected function mapHeaderFromFile(array $header)
    {
        $arrMap = array(
            $this->language->get('export_file_col_name') => 'name',
            $this->language->get('export_file_col_code') => 'code',
            $this->language->get('export_file_col_active_ingredient') => 'active_ingredient',
            $this->language->get('export_file_col_unit') => 'unit',
            $this->language->get('export_file_col_basic_description') => 'basic_description',
            $this->language->get('export_file_col_note') => 'note',
            $this->language->get('export_file_col_overdose') => 'overdose',
            $this->language->get('export_file_col_course_certificates') => 'course_certificates',
            $this->language->get('export_file_col_detail_description') => 'detail_description',
            $this->language->get('export_file_col_us_man_19_30') => 'us_man_19_30',
            $this->language->get('export_file_col_us_man_31_50') => 'us_man_31_50',
            $this->language->get('export_file_col_us_man_51_70') => 'us_man_51_70',
            $this->language->get('export_file_col_us_man_gt_70') => 'us_man_gt_70',
            $this->language->get('export_file_col_us_women_19_30') => 'us_women_19_30',
            $this->language->get('export_file_col_us_women_31_50') => 'us_women_31_50',
            $this->language->get('export_file_col_us_women_51_70') => 'us_women_51_70',
            $this->language->get('export_file_col_us_women_gt_70') => 'us_women_gt_70',
            $this->language->get('export_file_col_vn_man_19_50') => 'vn_man_19_50',
            $this->language->get('export_file_col_vn_man_51_60') => 'vn_man_51_60',
            $this->language->get('export_file_col_vn_man_gt_60') => 'vn_man_gt_60',
            $this->language->get('export_file_col_vn_women_19_50') => 'vn_women_19_50',
            $this->language->get('export_file_col_vn_women_51_60') => 'vn_women_51_60',
            $this->language->get('export_file_col_vn_women_gt_60') => 'vn_women_gt_60',
        );

        $arrMap_2 = array(
            $this->language->get('export_file_col_name') => 'name',
            $this->language->get('export_file_col_code') => 'code',
            $this->language->get('export_file_col_active_ingredient') => 'active_ingredient',
            $this->language->get('export_file_col_unit') => 'unit',
            $this->language->get('export_file_col_basic_description') => 'basic_description',
            $this->language->get('export_file_col_note') => 'note',
            $this->language->get('export_file_col_overdose') => 'overdose',
            $this->language->get('export_file_col_course_certificates') => 'course_certificates',
            $this->language->get('export_file_col_detail_description') => 'detail_description',
            $this->language->get('export_file_col_us_man_19_30') => 'us_man_19_30',
            $this->language->get('export_file_col_us_man_31_50') => 'us_man_31_50',
            $this->language->get('export_file_col_us_man_51_70') => 'us_man_51_70',
            $this->language->get('export_file_col_us_man_gt_70') => 'us_man_gt_70',
            $this->language->get('export_file_col_us_women_19_30') => 'us_women_19_30',
            $this->language->get('export_file_col_us_women_31_50') => 'us_women_31_50',
            $this->language->get('export_file_col_us_women_51_70') => 'us_women_51_70',
            $this->language->get('export_file_col_us_women_gt_70') => 'us_women_gt_70',
            $this->language->get('export_file_col_vn_man_19_50') => 'vn_man_19_50',
            $this->language->get('export_file_col_vn_man_51_60') => 'vn_man_51_60',
            $this->language->get('export_file_col_vn_man_gt_60') => 'vn_man_gt_60',
            $this->language->get('export_file_col_vn_women_19_50') => 'vn_women_19_50',
            $this->language->get('export_file_col_vn_women_51_60') => 'vn_women_51_60',
            $this->language->get('export_file_col_vn_women_gt_60') => 'vn_women_gt_60',
        );

        $result = array();
        foreach ($header as $key => $value) {
            $value = trim($value);
            if (array_key_exists($value, $arrMap)) {
                $result[$arrMap[$value]] = $key;
            }
        }

        // for import different language
        if (count($result) < count($arrMap)) {
            foreach ($header as $key => $value) {
                $value = trim($value);
                if (array_key_exists($value, $arrMap_2)) {
                    $result[$arrMap_2[$value]] = $key;
                }
            }
        }

        $flag = 1;
        foreach ($arrMap as $k => $v) {
            if (!array_key_exists($v, $result)) {
                $flag = 0;
                break;
            }
        }

        // for import different language
        if (!$flag) {
            foreach ($arrMap_2 as $k => $v) {
                if (!array_key_exists($v, $result)) {
                    return false;
                }
            }
        }

        return $result;
    }

    protected function mapHeaderFromFileProductIngredient(array $header)
    {
        $arrMap = array(
            $this->language->get('export_file_col_product_sku') => 'product_sku',
            $this->language->get('export_file_col_ingredients') => 'ingredients',
            $this->language->get('export_file_col_quantitative') => 'quantitative',
        );

        $arrMap_2 = array(
            $this->language->get('export_file_col_product_sku') => 'product_sku',
            $this->language->get('export_file_col_ingredients') => 'ingredients',
            $this->language->get('export_file_col_quantitative') => 'quantitative',
        );

        $result = array();
        foreach ($header as $key => $value) {
            $value = trim($value);
            if (array_key_exists($value, $arrMap)) {
                $result[$arrMap[$value]] = $key;
            }
        }

        // for import different language
        if (count($result) < count($arrMap)) {
            foreach ($header as $key => $value) {
                $value = trim($value);
                if (array_key_exists($value, $arrMap_2)) {
                    $result[$arrMap_2[$value]] = $key;
                }
            }
        }

        $flag = 1;
        foreach ($arrMap as $k => $v) {
            if (!array_key_exists($v, $result)) {
                $flag = 0;
                break;
            }
        }

        // for import different language
        if (!$flag) {
            foreach ($arrMap_2 as $k => $v) {
                if (!array_key_exists($v, $result)) {
                    return false;
                }
            }
        }

        return $result;
    }

    protected function validateDelete()
    {
        if (!$this->user->hasPermission('modify', 'ingredient/ingredient')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!isset($this->request->get['ingredient_ids'])) {
            $this->error['warning'] = $this->language->get('text_edit_not_found_id');
        }

        return !$this->error;
    }

    function is_array_empty($arr)
    {
        if (is_array($arr)) {
            foreach ($arr as $key => $value) {
                if (!empty($value) || $value != NULL || $value != "") {
                    return false;
                    break;//stop the process we have seen that at least 1 of the array has value so its not empty
                }
            }
            return true;
        }
    }
}