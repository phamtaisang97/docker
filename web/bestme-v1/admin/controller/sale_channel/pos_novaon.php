<?php


class ControllerSaleChannelPosNovaon extends Controller
{
    private $error = array();

    const CONFIG_POS_NOVAON_API_KEY = 'config_pos_status';

    public function index()
    {
        $this->load->language('sale_channel/pos_novaon');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');
    }

    private function validateForm()
    {
        if (!$this->user->hasPermission('modify', 'sale_channel/pos_novaon')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        /* domain */
        if (!isset($this->request->post['api_key'])) {
            $this->error['api_key'] = $this->language->get('error_api_key');
        }

        return !$this->error;
    }
}