<?php

class ControllerSaleChannelShopee extends Controller
{
    private $error = array();
    private $shopee_categories = [];
    const SHOPEE_CONFIG_KEY = 'shopee_shop_id';
    const SHOPEE_CONFIG = 'shopee_config';

    const DEFAULT_STORE_ID = 0;

    const SYNC_TYPE = 1;  // 0 : sync by sku, 1 by item_id  TODO use config
    // status for sync by item_id
    const STATUS_CREATE = 1;
    const STATUS_UPDATE = 2;

    // status for sync by sku
    const SHOPEE_STATUS_WITHOUT_SKU = 0;  // Sản phẩm 1 phiên bản thiếu SKU
    const SHOPEE_STATUS_CREATE = 1;  // Tạo mới sản phẩm
    const SHOPEE_STATUS_EXISTED = 2; // đồng bộ sản phẩm
    const SHOPEE_STATUS_DUPLICATE = 3; // trùng sku trên shopee
    const SHOPEE_STATUS_DUPLICATE_VERSION_PRODUCT = 4; // Có 2 sản phẩm có sku version trùng lặp với sản phẩm này

    // redis job key
    const CJ_JOB_KEY_DB_HOSTNAME = 'db_hostname';
    const CJ_JOB_KEY_DB_PORT = 'db_port';
    const CJ_JOB_KEY_DB_USERNAME = 'db_username';
    const CJ_JOB_KEY_DB_PASSWORD = 'db_password';
    const CJ_JOB_KEY_DB_DATABASE = 'db_database';
    const CJ_JOB_KEY_DB_PREFIX = 'db_prefix';
    const CJ_JOB_KEY_DB_DRIVER = 'db_driver';
    const CJ_JOB_KEY_SHOP_NAME = 'shop_name';
    const CJ_JOB_KEY_SHOPEE_ID = 'shopee_id';
    const CJ_JOB_KEY_TASK = 'task';
    const CJ_JOB_KEY_SYNC_NEXT_TIME = 'sync_next_time';
    const CJ_JOB_KEY_SYNC_INTERVAL = 'sync_interval';
    const CJ_JOB_KEY_SYNC_TYPE = 'sync_type'; // auto or manual
    // all tasks
    const CJ_JOB_TASK_GET_PRODUCTS = 'GET_PRODUCTS';
    const CJ_JOB_TASK_GET_ORDERS = 'GET_ORDERS';
    const CJ_UPDATE_JOB_TASK_GET_ORDERS_PREFIX = 'UPDATE_JOB_GET_ORDERS_';
    const CJ_UPDATE_JOB_KEY_TYPE = 'type';
    const CJ_UPDATE_JOB_TYPE_UPDATE = 'update';
    const CJ_UPDATE_JOB_TYPE_REMOVE = 'remove';
    const CJ_UPDATE_JOB_KEY_sync_interval = 'sync_interval';

    public function index()
    {
        $this->load->language('sale_channel/shopee');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->addStyle('view/stylesheet/custom/shopee.css');
        $this->load->model('catalog/shopee');
        $this->load->model('catalog/product');
        $this->getList();
    }

    public function sync()
    {
        ini_set('max_execution_time', 300);
        set_time_limit(300);
        $this->load->language('sale_channel/shopee');
        $json = [
            'status' => false,
            'msg' => $this->language->get('warning_sync_fail'),
            'create' => 0,
            'update' => 0
        ];
        $count_is_create = 0;
        $count_is_update = 0;
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateSync()) {
            $this->load->model('catalog/shopee');
            $this->load->model('catalog/product');
            $ids = $this->request->post['ids'];
            $shopee_id = isset($this->request->post['shopee_id']) ? (int)$this->request->post['shopee_id'] : 0;
            if ($ids == 'all') {
                if (!$shopee_id) {
                    $ids = [];
                } else {
                    $ids = $this->model_catalog_shopee->getIds($shopee_id);
                }
            }
            $type = isset($this->request->post['type']) ? $this->request->post['type'] : null; // create or update
            if (is_array($ids)) {
                $skues = $this->model_catalog_shopee->getSkues();
                $skues = array_count_values($skues);
                foreach ($ids as $id) {
                    if (trim($id) == '') {
                        continue;
                    }
                    $product = $this->model_catalog_shopee->getProductById($id);
                    if (empty($product)) {
                        continue;
                    }
                    if (self::SYNC_TYPE == 0) { // sync by sku
                        $status = $this->getStatus($product, $skues);
                        if ($status['sync_status_code'] == self::SHOPEE_STATUS_CREATE) {
                            if ($type == 'update') {
                                continue;
                            }
                            $this->createProduct($id);
                            $count_is_create++;
                            continue;
                        }
                        if ($status['sync_status_code'] == self::SHOPEE_STATUS_EXISTED) {
                            if (!array_key_exists('product_id_update', $status) || $status['product_id_update'] == null) {
                                continue;
                            }
                            if ($type == 'create') {
                                continue;
                            }
                            $this->updateProduct($id, $status['product_id_update']);
                            $count_is_update++;
                            continue;
                        }
                    } else { // sync by item_id
                        if (!isset($product['has_variation']) || !isset($product['item_id'])) {
                            continue;
                        }

                        if ($product['has_variation'] == 0) {
                            if ($product['sync_status']) {
                                $product_bestme_id = isset($product['bestme_id']) ? $product['bestme_id'] : 0;
                                $product_bestme_version_id = isset($product['bestme_product_version_id']) ? $product['bestme_product_version_id'] : 0;
                                $product_stock = isset($product['stock']) ? $product['stock'] : 0;
                                $product_price = isset($product['price']) ? $product['price'] : 0;
                                $product_original_price = isset($product['original_price']) ? $product['original_price'] : 0;

                                if ($product_bestme_id) {
                                    $this->model_catalog_shopee->updateProductHasLinkProductShopee((int)$shopee_id, $product_bestme_id, $product_bestme_version_id, (int)$product_stock, (float)$product_price, (float)$product_original_price);
                                }

                                continue;
                            }

                            $result = $this->createProduct($id);
                            if ($result) {
                                $count_is_create++;
                            }
                            continue;
                        } else {
                            if ($product['sync_status'] == null || $product['sync_status'] == 0) {
                                $result = $this->createProduct($id);
                                if ($result) {
                                    $count_is_create++;
                                }
                                continue;
                            } else {
                                $product_versions = $this->model_catalog_shopee->getProductVersionShopeeList($product['item_id'], ModelCatalogShopee::SYNC_STATUS_ALL);
                                foreach ($product_versions as $version) {
                                    if (isset($version['sync_status']) && $version['sync_status']) {
                                        $product_bestme_id = isset($version['bestme_product_id']) ? $version['bestme_product_id'] : 0;
                                        $product_bestme_version_id = isset($version['bestme_product_version_id']) ? $version['bestme_product_version_id'] : 0;
                                        $product_stock = isset($version['stock']) ? $version['stock'] : 0;
                                        $product_price = isset($version['price']) ? $version['price'] : 0;
                                        $product_original_price = isset($version['original_price']) ? $version['original_price'] : 0;

                                        if ($product_bestme_id) {
                                            $this->model_catalog_shopee->updateProductHasLinkProductShopee((int)$shopee_id, $product_bestme_id, $product_bestme_version_id, (int)$product_stock, (float)$product_price, (float)$product_original_price);
                                        }

                                        continue;
                                    }

                                    $result = $this->createProductVersion($version['id']);
                                    if ($result) {
                                        $count_is_create++;
                                    }
                                }
                                continue;
                            }
                        }

                        // TODO: remove...
                        //$count_is_create++;
                        //continue;
                    }
                }
            }

            if ($count_is_update > 0 || $count_is_create > 0) {
                $json['status'] = true;
                $json['msg'] = 'success!';
                $json['create'] = $count_is_create;
                $json['update'] = $count_is_update;
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function validateSync()
    {
        $this->load->language('sale_channel/shopee');
        if (!$this->user->hasPermission('modify', 'sale_channel/shopee')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        if (!isset($this->request->post['ids'])) {
            $this->error['warning'] = $this->language->get('warning_sync_fail');
        } else {
            if ($this->request->post['ids'] == 'all' && !isset($this->request->post['shopee_id'])) {
                $this->error['warning'] = $this->language->get('warning_sync_fail');
            }
        }

        return !$this->error;
    }

    private function updateProduct($shopee_product_id, $bestme_id)
    {
        $this->load->model('catalog/product');
        $this->load->model('catalog/shopee');
        $this->load->model('catalog/attribute');
        $this->load->model('custom/collection');
        $this->load->model('custom/tag');
        $product = $this->model_catalog_product->getProduct($bestme_id);
        if (empty($product)) {
            $this->createProduct($shopee_product_id);
            return;
        }
        $shopee_product = $this->model_catalog_shopee->getProductById($shopee_product_id);
        if (empty($shopee_product)) {
            return;
        }
        $shopee_id = isset($shopee_product['shopid']) ? (int)$shopee_product['shopid'] : 0;

        $product['promotion_price'] = $product['price'];
        $product['price'] = $product['compare_price'];
        $product['product_version'] = $shopee_product['has_variation'];
        $product['summary'] = $product['sub_description'];
        $product['seo_desciption'] = $product['seo_description'];
        $product['shopee_update_product'] = 'not_update';
        $product['status'] = $shopee_product['status'] == 'NORMAL' ? 1 : 0;
        if(!empty($product['source']) && $product['source'] != 'omihub') {
            $product['source'] = 'shopee';
        } else {
            $product['source'] = $product['source'];
        }
        if ($shopee_product['has_variation']) {
            $product['common_sku'] = $shopee_product['sku'];
        } else {
            $product['sku'] = $shopee_product['sku'];
        }
//        $product_attributes = $this->model_catalog_product->getProductAttributes($bestme_id);

//        // get old version info of product in bestme
//        foreach ($product_attributes as $key => $product_attribute) {
//            $attribute_info = $this->model_catalog_attribute->getAttribute($product_attribute['attribute_id']);
//            if ($attribute_info) {
//                $product['attribute_name'][$key] = $attribute_info['name'];
//                $product['attribute_values'][$key] = explode(",", $product_attribute['product_attribute_description']['text']);
//            }
//        }
//
//        $product_versions = $this->model_catalog_product->getProductVersions($bestme_id);
//        foreach ($product_versions as $key => $version) {
//            $product['product_version_names'][$key] = $version['version'];
//            $product['product_version_ids'][$key] = $version['product_version_id'];
//            $product['product_prices'][$key] = $version['compare_price'];
//            $product['product_promotion_prices'][$key] = $version['price'];
//            $product['product_quantities'][$key] = $version['quantity'];
//            $product['product_skus'][$key] = $version['sku'];
//            $product['product_barcodes'][$key] = $version['barcode'];
//        }

        // update info of all version in shopee
        $has_variation = isset($shopee_product['has_variation']) ? (int)$shopee_product['has_variation'] : 0;
        $version_info = $has_variation ? $this->mapVariations($shopee_id, $shopee_product['item_id'], $shopee_product['variations']) : [];
        $product = array_merge($product, $version_info);

        // update info for product_to_store
        $default_store_id = isset($product['$default_store_id']) ? (int)$product['$default_store_id'] : self::DEFAULT_STORE_ID;
        if (!empty($version_info)) {
            $product_to_store = $this->getProductToStoreForProductMultiversions($version_info, $default_store_id);
        } else {
            $product_to_store = $this->getProductToStoreForProductSingleVersion(0, $shopee_product['stock'], $default_store_id);
        }
        $product = array_merge($product, $product_to_store);

        $collection_list = $this->model_custom_collection->getCollectionsByProductId($bestme_id);
        $product['collection_list'] = array_map(function ($collection) {
            return $collection['id'];
        }, $collection_list);

        $tag_list = $this->model_custom_tag->getTagsByProductId($bestme_id);
        $product['tag_list'] = array_map(function ($tag) {
            return $tag['id'];
        }, $tag_list);

//        // current image product
//        $product_images = $this->model_catalog_product->getProductImages($bestme_id);
//        foreach ($product_images as $key => $image) {
//            $product['images'][$key] = $image['image'];
//            $product['image-alts'][$key] = $image['image_alt'];
//        }

        $product['images'] = json_decode($shopee_product['images']);

        // update data by config
        //$sync_fields = $this->config->get(self::SHOPEE_CONFIG);
        //$sync_fields = json_decode($sync_fields, true);
        //if ($sync_fields['sync_name']) {
        $product['name'] = $shopee_product['name'];
        //}
        //if ($sync_fields['sync_desc']) {
        $product['description'] = $shopee_product['description'];
        //}
        //if ($sync_fields['sync_price']) {
        $product['promotion_price'] = $shopee_product['price'];
        $product['price'] = $shopee_product['original_price'];;
        //}
        //if ($sync_fields['sync_quantity']) {
        $product['quantity'] = $shopee_product['stock'];
        //}
        //if ($sync_fields['sync_weight']) {
        $product['weight'] = $shopee_product['weight'];
        //}

        $this->model_catalog_product->editProduct($bestme_id, $product);
        // update bestme_id
        $this->model_catalog_shopee->updateBestmeId($shopee_product_id, $bestme_id);

        // update link if has version
        if ($shopee_product['has_variation']){
            $this->model_catalog_shopee->updateLinkProductHasVersion($bestme_id, $shopee_product);
        }
    }

    private function createProduct($shopee_product_id)
    {
        $this->load->model('catalog/shopee');
        $this->load->model('catalog/product');
        $shopee_product = $this->model_catalog_shopee->getProductById($shopee_product_id);
        if (empty($shopee_product)) {
            return false;
        }

        $product = $this->mapShopeeProductToBestmeProduct($shopee_product);

        $product['source'] = 'shopee';

        if (isset($product['product_version_names']) && (int)$product['product_version'] == 1) {
            if(is_array($product['product_version_names']) && (count($product['product_version_names']) != count(array_unique($product['product_version_names'])))) {
                return false;
            }
        }

        $bestme_id = $this->model_catalog_product->addProduct($product);
        // update bestme_id
        $this->model_catalog_shopee->updateBestmeId($shopee_product_id, $bestme_id);

        // update if has version
        if (isset($shopee_product['has_variation']) && $shopee_product['has_variation']) {
            $this->model_catalog_shopee->updateLinkProductHasVersion($bestme_id, $shopee_product);
        }

        return true;
    }

    private function createProductVersion($shopee_product_version_id)
    {
        $this->load->model('catalog/shopee');
        $this->load->model('catalog/product');
        $shopee_product_version = $this->model_catalog_shopee->getProductVersionById($shopee_product_version_id);
        if (empty($shopee_product_version)) {
            return false;
        }

        $product = $this->mapShopeeProductVersionToBestmeProduct($shopee_product_version);
        $product['source'] = 'shopee';
        $bestme_id = $this->model_catalog_product->addProduct($product);

        // update status for shopee_product when create version and link product to p_version shopee
        $this->model_catalog_shopee->updateBestmeProductIdForVersion($shopee_product_version_id, $bestme_id, $shopee_product_version['item_id']);

        return true;
    }

    private function mapShopeeProductToBestmeProduct(array $shopee_product)
    {
        $shopee_id = isset($shopee_product['shopid']) ? (int)$shopee_product['shopid'] : 0;
        if (!$shopee_id) {
            return [];
        }

        $product = [];
        $product['name'] = $shopee_product['name'];
        $product['description'] = $shopee_product['description'];
        $product['images'] = json_decode($shopee_product['images']);
        $product['product_version'] = $shopee_product['has_variation'];
        $product['price'] = $shopee_product['original_price'];
        $product['promotion_price'] = $shopee_product['price'];
        if ($shopee_product['has_variation']) {
            $product['common_sku'] = $shopee_product['sku'];
            $product['sku'] = '';
        } else {
            $product['sku'] = $shopee_product['sku'];
            $product['common_sku'] = '';
        }
        $product['quantity'] = $shopee_product['stock'];
        $product['weight'] = (integer)$shopee_product['weight'];
        $product['status'] = $shopee_product['status'] == 'NORMAL' ? 1 : 0; // default  = 1 display
        $product['category'][] = $this->getCategory($shopee_id, $shopee_product['category_id']);
        $product['barcode'] = '';
        $product['summary'] = '';
        $product['seo_title'] = '';
        $product['seo_desciption'] = '';
        $product['common_barcode'] = '';
        $product['common_compare_price'] = '';
        $product['common_price'] = '';
        $has_variation = isset($shopee_product['has_variation']) ? (int)$shopee_product['has_variation'] : 0;
        $version_info = $has_variation ? $this->mapVariations($shopee_id, $shopee_product['item_id'], $shopee_product['variations']) : [];
        $product = array_merge($product, $version_info);

        // update info for product_to_store
        if (!empty($version_info)) {
            $product_to_store = $this->getProductToStoreForProductMultiversions($version_info);
        } else {
            $product_to_store = $this->getProductToStoreForProductSingleVersion(0, $shopee_product['stock']);
        }
        $product = array_merge($product, $product_to_store);

        return $product;
    }

    private function mapShopeeProductVersionToBestmeProduct(array $shopee_product_version)
    {
        $this->load->model('catalog/shopee');
        if (!isset($shopee_product_version['item_id']) || !$shopee_product_version['item_id']) {
            return [];
        }

        $shopee_product = $this->model_catalog_shopee->getShopeeProductByItemId($shopee_product_version['item_id']);

        $shopee_id = isset($shopee_product['shopid']) ? (int)$shopee_product['shopid'] : 0;
        if (!$shopee_id){
            return [];
        }
        $product = [];
        $product['name'] = $shopee_product['name'] .', '. $shopee_product_version['version_name'];
        $product['description'] = $shopee_product['description'];
        $product['images'] = json_decode($shopee_product['images']);
        $product['product_version'] = 0; // 0 is one version.
        $product['price'] = $shopee_product_version['original_price'];
        $product['promotion_price'] = $shopee_product_version['price'];
        $product['sku'] = $shopee_product_version['sku'];
        $product['common_sku'] = '';
        $product['quantity'] = $shopee_product_version['stock'];
        $product['weight'] = (integer)$shopee_product_version['product_weight'];
        $product['status'] = $shopee_product_version['status'] == 'MODEL_NORMAL' ? 1 : 0; // default  = 1 display
        $product['category'][] = $this->getCategory($shopee_id, $shopee_product['category_id']);
        $product['barcode'] = '';
        $product['summary'] = '';
        $product['seo_title'] = '';
        $product['seo_desciption'] = '';
        $product['common_barcode'] = '';
        $product['common_compare_price'] = '';
        $product['common_price'] = '';

        // update info for product_to_store
        $product_to_store = $this->getProductToStoreForProductSingleVersion(0, $shopee_product_version['stock']);

        $product = array_merge($product, $product_to_store);

        return $product;
    }

    private function getProductToStoreForProductMultiversions($version_info, $default_store_id = self::DEFAULT_STORE_ID)
    {
        $result = [];
        if (empty($version_info)) {
            return $result;
        }

        if (!array_key_exists('product_version_names', $version_info) || !is_array($version_info['product_version_names'])
            || !array_key_exists('product_quantities', $version_info) || !is_array($version_info['product_quantities'])) {
            return $result;
        }

        foreach ($version_info['product_version_names'] as $key => $version_name) {
            $version_name = $this->replaceVersionValues($version_name);
            $result['stores'][$version_name][] = $default_store_id;
            $result['original_inventory'][$version_name][] = isset($version_info['product_quantities'][$key]) ? (int)$version_info['product_quantities'][$key] : 0;
            $result['cost_price'][$version_name][] = isset($version_info['product_prices'][$key]) ? (float)$version_info['product_prices'][$key] : 0;
        }

        return $result;
    }

    private function getProductToStoreForProductSingleVersion($cost_price, $quantity, $default_store_id = self::DEFAULT_STORE_ID)
    {
        $result['stores'][][] = $default_store_id;
        $result['original_inventory'][][] = (int)$quantity;
        $result['cost_price'][][] = (float)$cost_price;

        return $result;
    }

    private function replaceVersionValues($str)
    {
        return preg_replace('/[^0-9a-zA-Z\-_]+/', '_', $str) . '_' . md5($str);
    }

    private function getCategory($shopee_id, $category_id)
    {
        if (!$shopee_id) {
            return;
        }
        if (empty($this->shopee_categories)) {
            $shopee = Sale_Channel::getSaleChannel('shopee', ['shop_id' => (int)$shopee_id]);
            $shopee_categories = $shopee->getCategories([]);
            $shopee_categories = json_decode($shopee_categories, true);
            $this->shopee_categories = $shopee_categories;
        } else {
            $shopee_categories = $this->shopee_categories;
        }
        if (!array_key_exists('categories', $shopee_categories)) {
            return;
        }

        foreach ($shopee_categories['categories'] as $category) {
            if ($category['category_id'] == $category_id) {
                if (isset($category['parent_id'])) {
                    foreach ($shopee_categories['categories'] as $icategory) {
                        if ($icategory['category_id'] == $category['parent_id']) {
                            $category_name = $icategory['category_name'];
                            break;
                        }
                    }
                } else {
                    $category_name = $category['category_name'];
                }
                break;
            }
        }
        if (!isset($category_name)) {
            return;
        }

        $this->load->model('catalog/category');
        $filter_data['filter_name'] = trim($category_name);

        $results = $this->model_catalog_category->getCategoriesPaginator($filter_data);
        if (!empty($results)) {
            return reset($results)['id'];
        }

        return $this->model_catalog_category->addCategoryFast($category_name);
    }

    private function mapVariations($shopee_id, $item_id, $variations)
    {
        $result = [];
        if (!$shopee_id) {
            return $result;
        }
        $shopee = Sale_Channel::getSaleChannel('shopee', ['shop_id' => (int)$shopee_id]);
        $vartion = $shopee->GetVariations(['item_id' => (int)$item_id]);
        $vartion = json_decode($vartion, true);
        $variations = json_decode($variations, true);
        if (!array_key_exists('tier_variation', $vartion) || !is_array($vartion['tier_variation'])) {
            if (is_array($variations) && count($variations) > 0) {
                $this->load->language('sale_channel/shopee');
                $result['attribute_name'][] = $this->language->get('txt_classify');
                $attr_values = array();
                foreach ($variations as $ver) {
                    $attr_values[] = trim($ver['name']);
                }
                $result['attribute_values'][] = $attr_values;
            } else {
                return $result;
            }
        } else {
            foreach ($vartion['tier_variation'] as $key => $tier) {
                $result['attribute_name'][$key] = $tier['name'];
                $result['attribute_values'][$key] = $tier['options'];
            }
        }

        foreach ($variations as $key => $variation) {
            if (self::SYNC_TYPE == 0) { // sync by sku
                if ($variation['variation_sku'] == '') {
                    continue;
                }
            }

            $result['product_prices'][$key] = $variation['original_price'];
            $result['product_promotion_prices'][$key] = $variation['original_price'] > $variation['price'] ? $variation['price'] : "";
            $result['product_quantities'][$key] = $variation['stock'];
            $result['product_skus'][$key] = $variation['variation_sku'];
            $result['product_barcodes'][$key] = '';
            $result['product_display'][$key] = $variation['name'];
            if (!array_key_exists('tier_variation', $vartion) || !is_array($vartion['tier_variation'])) {
                $result['product_version_names'][$key] = $variation['name'];
            } else {
                $result['product_version_names'][$key] = $this->getProductVersionName($variation['variation_id'], $vartion, $result['attribute_values']);
            }
        }

        return $result;
    }

    private function getProductVersionName($variation_id, $vartion, $attr_values)
    {
        $version_name = '';
        if (!array_key_exists('variations', $vartion) || !is_array($vartion['variations'])) {
            return $version_name;
        }

        foreach ($vartion['variations'] as $variation) {
            if ($variation_id == $variation['variation_id']) {
                if (!array_key_exists('tier_index', $variation) || !is_array($variation['tier_index'])) {
                    return $version_name;
                }
                $tier_index = $variation['tier_index'];
                $name = [];
                foreach ($tier_index as $key => $index) {
                    $name[] = $attr_values[$key][$index];
                }
                $version_name = implode(',', $name);
                break;
            }
        }

        return $version_name;
    }

    public function config()
    {
        $this->load->language('sale_channel/shopee');
        $this->load->model('catalog/shopee');

        $this->document->setTitle($this->language->get('heading_title'));
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];
            $this->session->data['error'] = '';
            unset($this->session->data['error']);
        } else {
            $data['error_warning'] = '';
        }

        $data['video_shopee'] = BESTME_SHOPEE_GUIDE;
        //$shop_id = $this->config->get(self::SHOPEE_CONFIG_KEY);
        $data['sync_interval_test_value'] = defined('SHOPEE_SYNC_INTERVAL_TEST_VALUE') ? (int)SHOPEE_SYNC_INTERVAL_TEST_VALUE : '';

        $shops = $this->model_catalog_shopee->getShopeeShopList();
        $data['shops'] = $shops;

        $redirect_url = $this->url->link('sale_channel/shopee/updateShopId&user_token=' . $this->session->data['user_token'], '', true);
        $data['auth_url'] = \Sale_Channel\Shopee::getAuthLink($redirect_url);
        $data['save_time_config'] = $this->url->link('sale_channel/shopee/saveTimeSyncConfig', 'user_token=' . $this->session->data['user_token'], true);
        $data['disconnect_shop'] = $this->url->link('sale_channel/shopee/disconnectShopById', 'user_token=' . $this->session->data['user_token'], true);
        $data['shopee_href'] = $this->url->link('sale_channel/shopee/config', 'user_token=' . $this->session->data['user_token'], true);
        $data['save_shopee_shop_config'] = $this->url->link('sale_channel/shopee/saveShopeeShopConfig', 'user_token=' . $this->session->data['user_token'], true);
        /*else {
            $data['sync_data'] = json_decode($this->config->get(self::SHOPEE_CONFIG), true);
            $data['shopee_href'] = $this->url->link('sale_channel/shopee', 'user_token=' . $this->session->data['user_token'], true);
            $data['cancel_href'] = $this->url->link('sale_channel/shopee/config', 'user_token=' . $this->session->data['user_token'], true);
            $data['save_href'] = $this->url->link('sale_channel/shopee/doSaveConfig', 'user_token=' . $this->session->data['user_token'], true);
            $data['disconnect_href'] = $this->url->link('sale_channel/shopee/disconnectShopee', 'user_token=' . $this->session->data['user_token'], true);

            $this->response->setOutput($this->load->view('sale_channel/shopee_config', $data));
        }*/

        $this->response->setOutput($this->load->view('sale_channel/shopee_config', $data));
    }

    public function doSaveConfig()
    {
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateSyncConfig()) {
            $this->load->model('setting/setting');
            $config['sync_name'] = (int)$this->request->post['sync_name'];
            $config['sync_desc'] = (int)$this->request->post['sync_desc'];
            $config['sync_price'] = (int)$this->request->post['sync_price'];
            $config['sync_quantity'] = (int)$this->request->post['sync_quantity'];
            $config['sync_weight'] = (int)$this->request->post['sync_weight'];

            $config = json_encode($config);
            $this->model_setting_setting->addConfig(self::SHOPEE_CONFIG, $config);
            return;
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($this->error));
    }

    public function saveTimeSyncConfig()
    {
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateTimeSyncConfig()) {
            $this->load->model('catalog/shopee');
            $old_sync_interval = $this->model_catalog_shopee->getTimeIntervalById($this->request->post['id']);
            $this->model_catalog_shopee->updateTimeSyncById($this->request->post['id'], $this->request->post['time']);

            // update order job
            if ((int)$this->request->post['time'] == 0) {
                // remove job
                $this->updateOrderJob($this->request->post['id'], (int)$this->request->post['time'], self::CJ_UPDATE_JOB_TYPE_REMOVE);
            } else {
                if ($old_sync_interval == 0) {
                    //add new job
                    $this->loadShopeeOrders($this->request->post['id'], (int)$this->request->post['time'], null, 'auto');
                } else {
                    if ($this->checkJobExist($this->request->post['id'])) {
                        // update job
                        $this->updateOrderJob($this->request->post['id'], (int)$this->request->post['time'], self::CJ_UPDATE_JOB_TYPE_UPDATE);
                    } else {
                        //add new job
                        $this->loadShopeeOrders($this->request->post['id'], (int)$this->request->post['time'], null, 'auto');
                    }
                }
            }

            return;
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($this->error));
    }

    private function checkJobExist($shopee_id)
    {
        $result = false;
        $redis = null;

        try {
            if (!$shopee_id) {
                return $result;
            }

            $shop_name = $this->config->get('shop_name');

            $redis = $this->redisConnect();
            $jobs = $redis->lrange(SHOPEE_ORDER_JOB_REDIS_QUEUE, 0, -1);
            if (!is_array($jobs)) {
                return $result;
            }

            foreach ($jobs as $job) {
                $job = json_decode($job, true);
                if (!is_array($job) || !array_key_exists(self::CJ_JOB_KEY_SHOP_NAME, $job) || !array_key_exists(self::CJ_JOB_KEY_SHOPEE_ID, $job)) {
                    continue;
                }

                $sync_type = isset($job[self::CJ_JOB_KEY_SYNC_TYPE]) ? $job[self::CJ_JOB_KEY_SYNC_TYPE] : 'auto';
                if ($job[self::CJ_JOB_KEY_SHOP_NAME] == $shop_name && $job[self::CJ_JOB_KEY_SHOPEE_ID] == $shopee_id && $sync_type == 'auto') {
                    $result = true;
                    break;
                }
            }
        } catch (Exception $e) {
            // TODO
        }

        $this->redisClose($redis);

        return $result;
    }

    public function disconnectShopById()
    {
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateDisconnected()) {
            $this->load->model('catalog/shopee');

            $this->model_catalog_shopee->disconnectShopById($this->request->post['id']);
            // remove job
            $this->updateOrderJob($this->request->post['id'], 0, self::CJ_UPDATE_JOB_TYPE_REMOVE);
            return;
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($this->error));
    }

    public function saveShopeeShopConfig()
    {
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateSaveShopeeShopConfig()) {
            $this->load->model('catalog/shopee');

            $order_sync_range = isset($this->request->post['order_sync_range']) ? (int)$this->request->post['order_sync_range'] : 15;
            $allow_update_stock_product = isset($this->request->post['allow_update_stock_product']) ? (int)$this->request->post['allow_update_stock_product'] : 0;
            $allow_update_price_product = isset($this->request->post['allow_update_price_product']) ? (int)$this->request->post['allow_update_price_product'] : 0;

            $this->model_catalog_shopee->saveShopeeShopConfig($this->request->post['id'], $order_sync_range, $allow_update_stock_product, $allow_update_price_product);
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($this->error));
    }

    public function orders()
    {
        $this->load->language('sale_channel/shopee');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->addStyle('view/stylesheet/custom/shopee.css');
        $this->load->model('catalog/shopee');
        $this->load->model('catalog/product');
        $this->getListOrders();
    }

    private function validateSyncConfig()
    {
        $this->load->language('sale_channel/shopee');
        if (!$this->user->hasPermission('modify', 'sale_channel/shopee')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        if (!isset($this->request->post['sync_name']) || !isset($this->request->post['sync_desc']) || !isset($this->request->post['sync_price'])
            || !isset($this->request->post['sync_quantity']) || !isset($this->request->post['sync_weight'])) {
            $this->error['warning'] = $this->language->get('warning_validate_sync_config');
        }

        return !$this->error;
    }

    private function validateTimeSyncConfig()
    {
        $this->load->language('sale_channel/shopee');
        $this->load->model('catalog/shopee');
        if (!$this->user->hasPermission('modify', 'sale_channel/shopee')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!isset($this->request->post['id']) ||
            !isset($this->request->post['time'])) {
            $this->error['warning'] = $this->language->get('warning_validate_time_sync_config');
        }

        $shop = $this->model_catalog_shopee->checkExitsShopeeShop($this->request->post['id']);

        if (!isset($shop->row['shop_id'])) {
            $this->error['warning'] = $this->language->get('warning_validate_shoppe_shop_exits');
        }

        return !$this->error;
    }

    private function validateDisconnected()
    {
        $this->load->language('sale_channel/shopee');
        $this->load->model('catalog/shopee');
        if (!$this->user->hasPermission('modify', 'sale_channel/shopee')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!isset($this->request->post['id'])) {
            $this->error['warning'] = $this->language->get('warning_validate_time_sync_config');
        }

        $shop = $this->model_catalog_shopee->checkExitsShopeeShop($this->request->post['id']);

        if (!isset($shop->row['shop_id'])) {
            $this->error['warning'] = $this->language->get('warning_validate_shoppe_shop_exits');
        }

        return !$this->error;
    }

    private function validateSaveShopeeShopConfig()
    {
        $this->load->language('sale_channel/shopee');
        $this->load->model('catalog/shopee');
        if (!$this->user->hasPermission('modify', 'sale_channel/shopee')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!isset($this->request->post['id'])) {
            $this->error['warning'] = $this->language->get('warning_validate_time_sync_config');
        }

        $shop = $this->model_catalog_shopee->checkExitsShopeeShop($this->request->post['id']);

        if (!isset($shop->row['shop_id'])) {
            $this->error['warning'] = $this->language->get('warning_validate_shoppe_shop_exits');
        }

        return !$this->error;
    }

    public function updateShopId()
    {
        //$this->load->model('setting/setting');
        $this->load->model('catalog/shopee');
        $this->load->language('sale_channel/shopee');
        if (($this->request->server['REQUEST_METHOD'] == 'GET') && $this->validateShopId()) {
            //$this->model_setting_setting->addConfig(self::SHOPEE_CONFIG_KEY, $this->request->get['shop_id']);
            $shopee = Sale_Channel::getSaleChannel('shopee', ['shop_id' => (int)$this->request->get['shop_id']]);
            $shoinfo = $shopee->getShopInfo();
            $shoinfo = json_decode($shoinfo, true);

            if (!isset($shoinfo) || !$shoinfo || !array_key_exists('shop_id', $shoinfo)) {
                $this->session->data['error'] = $this->language->get('connection_failed');
                $this->response->redirect($this->url->link('sale_channel/shopee/config', 'user_token=' . $this->session->data['user_token'], true));
                return;
            }
            $shopIsConnected = $this->model_catalog_shopee->shopIsConnected($shoinfo['shop_id'], $shoinfo);

            $connectCheck = $this->model_catalog_shopee->checkExitsShopeeShop($shoinfo['shop_id']);

            if (isset($connectCheck->row['shop_id'])) {
                $this->model_catalog_shopee->addOrEditShopeeShop($shoinfo['shop_id'], $shoinfo);
            } else {
                $this->model_catalog_shopee->addShopeeShop($shoinfo['shop_id'], $shoinfo);
            }

            if (!$shopIsConnected) {
                // add new job
                $this->loadShopeeOrders($shoinfo['shop_id'], $this->model_catalog_shopee->getDefaultSyncInterval(), null, 'auto');
            }
            $this->response->redirect($this->url->link('sale_channel/shopee/config', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->config();
    }

    public function deleteShopId()
    {
        $this->load->model('setting/setting');
        if (($this->request->server['REQUEST_METHOD'] == 'GET') && $this->validateShopId()) {
            if ($this->request->get['shop_id'] == $this->config->get(self::SHOPEE_CONFIG_KEY)) {
                $this->model_setting_setting->deleteSettingValue('config', self::SHOPEE_CONFIG_KEY);
                $this->response->redirect($this->url->link('sale_channel/shopee/config', 'user_token=' . $this->session->data['user_token'], true));
            } else {
                $this->error['warning'] = $this->language->get('txt_disconnect_fail');
            }
        }

        $this->config();
    }

    private function validateShopId()
    {
        $this->load->language('sale_channel/shopee');
        if (!$this->user->hasPermission('modify', 'sale_channel/shopee')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        if (!isset($this->request->get['shop_id'])) {
            $this->error['warning'] = $this->language->get('warning_active_fail');
        }

        return !$this->error;
    }

    public function disconnectShopee()
    {
        $shop_id = $this->config->get(self::SHOPEE_CONFIG_KEY);
        if (!$shop_id) {
            $this->response->redirect($this->url->link('sale_channel/shopee/config', 'user_token=' . $this->session->data['user_token'], true));
        }
        $redirect_url = $this->url->link('sale_channel/shopee/deleteShopId&user_token=' . $this->session->data['user_token'], '', true);
        $auth_url = \Sale_Channel\Shopee::disconnect($redirect_url);

        $this->response->redirect($auth_url);
    }

    public function loadShopeeProducts()
    {
        if (!isset($this->request->get['shopee_id'])) {
            return;
        }

        /* push job to Redis. Notice: use shop_name to define unique job... */
        $redis = null;

        try {
            $shopee_id = (int)$this->request->get['shopee_id'];
            if (!$shopee_id) {
                return;
            }

            /* update process status */
            $shop_name = $this->config->get('shop_name');
            $this->updateProcessingStatusRunning($shop_name, $shopee_id);

            /* create job */
            $job = [
                self::CJ_JOB_KEY_DB_HOSTNAME => DB_HOSTNAME,
                self::CJ_JOB_KEY_DB_PORT => DB_PORT,
                self::CJ_JOB_KEY_DB_USERNAME => DB_USERNAME,
                self::CJ_JOB_KEY_DB_PASSWORD => DB_PASSWORD,
                self::CJ_JOB_KEY_DB_DATABASE => DB_DATABASE,
                self::CJ_JOB_KEY_DB_PREFIX => DB_PREFIX,
                self::CJ_JOB_KEY_DB_DRIVER => DB_DRIVER,
                self::CJ_JOB_KEY_SHOP_NAME => $shop_name,
                self::CJ_JOB_KEY_SHOPEE_ID => $shopee_id,
                self::CJ_JOB_KEY_TASK => self::CJ_JOB_TASK_GET_PRODUCTS
            ];

            $redis = $this->redisConnect();
            $redis->lPush(SHOPEE_JOB_REDIS_QUEUE, json_encode($job));
        } catch (Exception $e) {
            // could not connect to redis
            $this->log->write('[controller/sale_channel/shopee] loadShopeeProducts(): could not connect to redis. Error: ' . $e->getMessage());
        } finally {
            $this->redisClose($redis);
        }
    }

    public function getSyncState()
    {
        $result = [
            'count_product_shopee' => 0,
            'count_product_shopee_saved' => 0,
            'load_shopee_product_running' => 0
        ];

        if (!isset($this->request->get['shopee_id'])) {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($result));
            return;
        }

        $redis = null;

        try {
            $redis = $this->redisConnect();
            if (!$redis instanceof \Redis) {
                $result['_error'] = 'Connect to Redis failed!';

                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($result));

                return;
            }

            $shopee_id = (int)$this->request->get['shopee_id'];
            $shop_name = $this->config->get('shop_name');

            $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'load_shopee_product_running');
            $result['load_shopee_product_running'] = $redis->get($key);

            $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'count_product_shopee');
            $result['count_product_shopee'] = $redis->get($key);

            $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'count_product_shopee_saved');
            $result['count_product_shopee_saved'] = $redis->get($key);
        } catch (Exception $e) {
            // could not connect to redis
        } finally {
            $this->redisClose($redis);
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    protected function getList()
    {
        $page = $this->getValueFromRequest('get', 'page', 1);
        $filter_data = $this->getValueFromRequest('get', 'filter_data');
        $status_product = $this->getValueFromRequest('get', 'status_product');

        $url = '';
        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        if (isset($this->session->data['error'])) {
            $data['error'] = $this->session->data['error'];
            unset($this->session->data['error']);
        } else {
            $data['error'] = '';
        }
        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        // breadcrumb
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('sale_channel/shopee', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        $shopee_shops = $this->model_catalog_shopee->getShopeeShopList();
        $shop_selected = isset($this->request->get['shop_selected']) ? $this->request->get['shop_selected'] : (isset($shopee_shops[0]['shop_id']) ? $shopee_shops[0]['shop_id'] : null);
        $data['shop_selected'] = $shop_selected;

        if (!$shop_selected) {
            $this->response->redirect($this->url->link('sale_channel/shopee/config', 'user_token=' . $this->session->data['user_token'], true));
        }

        $last_sync_time_product = $this->model_catalog_shopee->getLastSyncTimeProductById($shop_selected);
        $data['last_sync_time_product'] = '--';

        if ($last_sync_time_product) {
            $data['last_sync_time_product'] = date("d/m/Y H:i:s", strtotime($last_sync_time_product));
        }

        foreach ($shopee_shops as &$shop) {
            $shop['url'] = $this->url->link('sale_channel/shopee', 'user_token=' . $this->session->data['user_token'] . '&shop_selected=' . $shop['shop_id'], true);
        }

        $data['shops'] = $shopee_shops;
        $data['listProduct'] = $this->url->link('sale_channel/shopee', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['loadOptionOne'] = $this->replace_url($this->url->link('sale_channel/shopee/loadoptionone', 'user_token=' . $this->session->data['user_token'] . $url, true));

        if (isset($this->request->get['status_product'])) {
            $status_product = ($this->request->get['status_product']);
            $status_product = count($status_product) > 1 ? '' : (int)$status_product[0];
        }

        $filter_name = isset($this->request->get['filter_name']) ? $this->request->get['filter_name'] : null;
        $status_sync = isset($this->request->get['status_sync']) ? $this->request->get['status_sync'] : ModelCatalogShopee::SYNC_STATUS_ALL;

        if (self::SYNC_TYPE == 0) { // sync by sku
            // filter data
            $filter_data = array(
                'filter_name' => $filter_name,
                'shop_id' => $shop_selected,
                'start' => ($page - 1) * $this->config->get('config_limit_admin'),
                'limit' => $this->config->get('config_limit_admin')
            );

            if (!isset($status_product) || $status_product == null) {
                $product_total = $this->model_catalog_shopee->getTotalProductsCustom($filter_data);
                // get product from oc_shopee_product
                $results = $this->model_catalog_shopee->getProductsCustom($filter_data);
                //Handle status
                //Get skues: parent and child
                $skues = $this->model_catalog_shopee->getSkues();
                $skues = array_count_values($skues);

                foreach ($results as &$item) {
                    $status = $this->getStatus($item, $skues);
                    $item = array_merge($item, $status);
                }

            } else {
                $list_product = $this->getProductByStatus($filter_data, $status_product);

                $product_total = $list_product['total'];
                $results = $list_product['products'];
            }
        } else { // sync by item_id
            $filter_data = array(
                'filter_name' => trim($filter_name),
                'shop_id' => $shop_selected,
                'status_product' => $status_product,
                'status_sync' => $status_sync,
                'start' => ($page - 1) * $this->config->get('config_limit_admin'),
                'limit' => $this->config->get('config_limit_admin'),
                'filter_data' => isset($this->request->get['filter_data']) ? $this->request->get['filter_data'] : false,
            );

            $product_total = $this->model_catalog_shopee->getTotalProductsFilterByStatus($filter_data);
            // get product from oc_shopee_product
            $results = $this->model_catalog_shopee->getProductsFilterByStatus($filter_data);

            foreach ($results as &$item) {
                if (!array_key_exists('bestme_id', $item)) {
                    continue;
                }

                if (!array_key_exists('item_id', $item) || !isset($item['item_id'])) {
                    continue;
                }

                if ($item['bestme_id'] != null) {
                    $item['sync_status_code'] = self::STATUS_UPDATE;
                    $item['sync_status_value'] = $this->language->get('entry_status_exist');
                } else {
                    $item['sync_status_code'] = self::STATUS_CREATE;
                }

                $versions = $this->model_catalog_shopee->getProductVersionShopeeList($item['item_id'], $status_sync);

                if ($versions && count($versions) > 0) {
                    $item['versions'] = $versions;
                } else if (!$item['has_variation']) {
                    $one_version = [
                        'id' => $item['id'].'_product',
                        'item_id' => $item['item_id'],
                        'version_name' => $item['name'],
                        'original_price' => $item['original_price'],
                        'price' => $item['price'],
                        'stock' => $item['stock'],
                        'bestme_product_id' => $item['bestme_id'],
                        'bestme_product_version_id' => $item['bestme_product_version_id'],
                        'sync_status' => $item['sync_status'],
                        'multi_version' => 1
                    ];

                    $item['versions'][] = $one_version;
                }

                if (isset($item['versions']) && is_array($item['versions'])) {
                    foreach ($item['versions'] as &$version) {
                        if (!isset($version['bestme_product_id']) || $version['bestme_product_id'] == null) {
                            continue;
                        }

                        $product_info = $this->model_catalog_shopee->getLinkProductInfoById($version['bestme_product_id'], $version['bestme_product_version_id']);

                        $version['product_name'] = isset($product_info['name']) ? $product_info['name'] : '';
                        $version['product_price'] = isset($product_info['price']) ? $product_info['price'] : '';
                        $version['product_quantity'] = isset($product_info['quantity']) ? $product_info['quantity'] : '';
                        $version['product_edit'] = isset($product_info['edit']) ? $product_info['edit'] : '';
                    }
                }
            }
        }

        $data['products'] = $results;
        if (self::SYNC_TYPE == 0) { // sync by sku
            $data['success_status_list'] = [self::SHOPEE_STATUS_CREATE, self::SHOPEE_STATUS_EXISTED];
            $data['error_status_list'] = [self::SHOPEE_STATUS_WITHOUT_SKU, self::SHOPEE_STATUS_DUPLICATE, self::SHOPEE_STATUS_DUPLICATE_VERSION_PRODUCT];
        } else {
            $data['success_status_list'] = [self::SHOPEE_STATUS_EXISTED];
            $data['error_status_list'] = [];
        }


        $data['filter'] = $filter_data;
        $data['user_token'] = $this->session->data['user_token'];

        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $product_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('sale_channel/shopee', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        // TODO: remove pagination old
        //$data['pagination'] = $pagination->render();
        //$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));

        // Add goto page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);
        $data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));


        $data['sync_href'] = $this->url->link('sale_channel/shopee/sync', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['url_filter'] = $this->url->link('sale_channel/shopee', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['sync_product_href'] = $this->url->link('sale_channel/shopee/loadShopeeProducts&user_token=' . $this->session->data['user_token'], '', true);
        $data['sync_state_href'] = $this->url->link('sale_channel/shopee/getSyncState&user_token=' . $this->session->data['user_token'], '', true);
        $data['create_product_href'] = $this->url->link('sale_channel/shopee/createProductShopee&user_token=' . $this->session->data['user_token'], '', true);
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('sale_channel/shopee_append', $data));
        } else {
            $this->response->setOutput($this->load->view('sale_channel/shopee', $data));
        }
    }

    private function getListOrders()
    {
        $page = $this->getValueFromRequest('get', 'page', 1);
        $filter_name = isset($this->request->get['filter_name']) ? $this->request->get['filter_name'] : null;

        if (isset($this->session->data['error'])) {
            $data['error'] = $this->session->data['error'];
            unset($this->session->data['error']);
        } else {
            $data['error'] = '';
        }
        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        // oc_shopee_shop_config
        $shopee_shops = $this->model_catalog_shopee->getShopeeShopList();
        $shop_selected = isset($this->request->get['shop_selected']) ? $this->request->get['shop_selected'] : (isset($shopee_shops[0]['shop_id']) ? $shopee_shops[0]['shop_id'] : null);
        $status_sync = isset($this->request->get['status_sync']) ? $this->request->get['status_sync'] : 99;

        if (!$shop_selected) {
            $this->response->redirect($this->url->link('sale_channel/shopee/config', 'user_token=' . $this->session->data['user_token'], true));
        }

        $last_sync_time_order = $this->model_catalog_shopee->getLastSyncTimeOrderById($shop_selected);
        $data['last_sync_time_order'] = '--';

        if ($last_sync_time_order) {
            $data['last_sync_time_order'] = date("d/m/Y H:i:s", strtotime($last_sync_time_order));
        }

        $data['shop_selected'] = $shop_selected;
        foreach ($shopee_shops as &$shop) {
            $shop['url'] = $this->url->link('sale_channel/shopee/orders', 'user_token=' . $this->session->data['user_token'] . '&shop_selected=' . $shop['shop_id'], true);
        }

        $url = '';
        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        // breadcrumb
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('sale_channel/shopee/orders', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        $data['user_token'] = $this->session->data['user_token'];

        $filter = array(
            'page' => $page,
            'code' => trim($filter_name),
            'shop_id' => $shop_selected,
            'status' => $status_sync,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $order_shopee = $this->model_catalog_shopee->getOrderSyncList($filter);

        foreach ($order_shopee as &$order) {
            if ($order['bestme_order_id']) {
                $order['url'] = $this->url->link('sale/order/detail', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . (int)$order['bestme_order_id'], true);
            } else {
                $order['url'] = 'javascript:;';
            }
        }

        $order_total = $this->model_catalog_shopee->getTotalOrderSyncList($filter);

        foreach ($order_shopee as &$order) {
            $order['order_status'] = $this->orderStatusDetail($order['order_status']);
            $order['sync_status_text'] = $this->orderSyncStatusDetail($order['sync_status']);
            $order['sync_status_error'] = $this->orderErrorSyncStatusDetail($order['sync_status']);
            $order['sync_status_solution'] = $this->orderSolutionSyncStatusDetail($order['sync_status']);
        }

        $data['orders'] = $order_shopee;
        $data['shops'] = $shopee_shops;
        $data['filter'] = $filter;
        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $order_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('sale_channel/shopee/orders', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        // TODO: remove pagination old
        //$data['pagination'] = $pagination->render();
        //$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));

        // Add goto page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);
        $data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));

        $data['sync_href'] = $this->url->link('sale_channel/shopee/syncOrders', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['url_filter'] = $this->url->link('sale_channel/shopee/orders', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['sync_order_href'] = $this->url->link('sale_channel/shopee/loadShopeeOrdersManual&user_token=' . $this->session->data['user_token'], '', true);
        $data['sync_state_href'] = $this->url->link('sale_channel/shopee/getSyncOrdersState&user_token=' . $this->session->data['user_token'], '', true);

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('sale_channel/shopee_order_list_append', $data));
        } else {
            $this->response->setOutput($this->load->view('sale_channel/shopee_order_list', $data));
        }
    }

    public function mappingProductBestme()
    {
        $json = [
            'status' => false,
            'message' => $this->language->get('json_not_permission_or_missing_data')
        ];
        $this->load->language('sale_channel/shopee');
        $this->load->model('catalog/shopee');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateMapping()) {
            $pvid = isset($this->request->post['mapping_id']) ? $this->request->post['mapping_id'] : 0;
            $id = isset($this->request->post['id']) ? $this->request->post['id'] : 0;

            $pvid = explode('-', $pvid);
            $product_id = isset($pvid[0]) ? $pvid[0] : 0;
            $product_version_id = (isset($pvid[1]) && $pvid[1]) ? $pvid[1] : 0;

            $this->model_catalog_shopee->mappingProductBestme($id, $product_id, $product_version_id);
            $product_info = $this->model_catalog_shopee->getLinkProductInfoById($product_id, $product_version_id);

            $json = [
                'status' => true,
                'product_name' => isset($product_info['name']) ? $product_info['name'] : '',
                'product_price' => isset($product_info['price']) ? number_format(str_replace(',', '', floatval($product_info['price']))) : '',
                'product_quantity' => isset($product_info['quantity']) ? $product_info['quantity'] : '',
                'product_edit' => isset($product_info['edit']) ? $product_info['edit'] : '',
                'message' => $this->language->get('json_connection_success')
            ];
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function cancelMappingVersion()
    {
        $json = [
            'status' => false,
            'message' => $this->language->get('json_not_permission_or_missing_data')
        ];
        $this->load->language('sale_channel/shopee');
        $this->load->model('catalog/shopee');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateCancelMapping()) {
            $id = isset($this->request->post['id']) ? $this->request->post['id'] : 0;
            $item_id = isset($this->request->post['item_id']) ? $this->request->post['item_id'] : 0;

            $this->model_catalog_shopee->cancelMappingVersion($id, $item_id);

            $json = [
                'status' => true,
                'message' => $this->language->get('json_cancel_connection_success')
            ];
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getProductsLazyLoad()
    {
        $json = array();
        $this->load->language('sale_channel/shopee');
        $this->load->model('catalog/shopee');
        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        // display 6 product in select config theme is many, or setup other config
        $filter_data = array(
            'limit' => 6,
            'start' => ($page - 1) * 6,
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        $shop_id = isset($this->request->get['shop_selected']) ? $this->request->get['shop_selected'] : '';
        if ($shop_id == '') {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }

        $filter_data['pv_ids'] = isset($this->request->get['pv_ids']) ? $this->request->get['pv_ids'] : [];
        $product_version_mapping = $this->model_catalog_shopee->getProductVersionMapping($shop_id);
        $product_mapping = $this->model_catalog_shopee->getProductMapping($shop_id);

        $mapping_ids = [];
        foreach ($product_version_mapping as $product) {
            $pvid = $product['bestme_product_id'];

            if (isset($product['bestme_product_version_id']) && $product['bestme_product_version_id']) {
                $pvid .= '-' . $product['bestme_product_version_id'];
            }

            $mapping_ids[] = $pvid;
        }

        foreach ($product_mapping as $product) {
            $pvid = $product['bestme_product_id'];

            if (isset($product['bestme_product_version_id']) && $product['bestme_product_version_id']) {
                $pvid .= '-' . $product['bestme_product_version_id'];
            }

            $mapping_ids[] = $pvid;
        }

        $filter_data['pv_ids'] = array_merge($filter_data['pv_ids'], $mapping_ids);
        $filter_data['pv_ids'] = array_unique($filter_data['pv_ids'], 0);

        $results = $this->model_catalog_shopee->getProductsForShopee($filter_data);

        foreach ($results as $key => $value) {
            $version_name = implode(' • ', explode(',', $value['version']));
            if ($version_name != '') {
                $version_name = $this->language->get('txt_version') . $version_name;
            }

            $json['results'][] = array(
                'id' => $value['id'],
                'product_id' => $value['product_id'],
                'product_version_id' => $value['product_version_id'],
                'version' => $version_name,
                'subname' => $version_name,
                'text' => $value['product_name'],
                'product_name' => $value['product_name'],
                'weight' => number_format($value['weight'], 0, '', ''),
                'price' => number_format($value['price'], 0, '', ','),
                'quantity' => $value['quantity'],
                'sale_on_out_of_stock' => $value['sale_on_out_of_stock'],
                'status' => $value['status'],
                'sku' => $value['sku'],
            );
        }

        $count_tag = $this->model_catalog_shopee->countProductAllContainDraft($filter_data);
        $json['count'] = $count_tag;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function cancelAllMapping()
    {
        $json = [
            'status' => false,
            'message' => $this->language->get('json_not_permission_or_missing_data')
        ];
        $this->load->language('sale_channel/shopee');
        $this->load->model('catalog/shopee');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateCancelAllMapping()) {
            $item_id = isset($this->request->post['item_id']) ? $this->request->post['item_id'] : 0;

            $this->model_catalog_shopee->cancelAllMapping($item_id);

            $json = [
                'status' => true,
                'message' => $this->language->get('json_cancel_connection_success')
            ];
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function createProductShopee()
    {
        $this->load->language('sale_channel/shopee');
        $json = [
            'code' => 200
        ];

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateCreateProduct()) {
            $this->load->model('catalog/shopee');

            $type = $this->request->post['type'];
            $id = $this->request->post['id'];

            if (strpos($id, '_product') !== false) {
                $id = str_replace('_product', '', $id);
            }

            if ($type == 'product') {
                $this->createProduct($id);
            } else if ($type == 'version') {
                $this->createProductVersion($id);
            }

            $this->session->data['success'] = $this->language->get('create_prod_shopee_success');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getSyncOrdersState()
    {
        $result = [
            'count_order_shopee' => 0,
            'count_order_shopee_saved' => 0,
            'load_shopee_order_running' => 0
        ];

        if (!isset($this->request->get['shopee_id'])) {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($result));
            return;
        }

        $redis = null;

        try {
            $redis = $this->redisConnect();
            if (!$redis instanceof \Redis) {
                $result['_error'] = 'Connect to Redis failed!';

                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($result));

                return;
            }

            $shopee_id = (int)$this->request->get['shopee_id'];
            $shop_name = $this->config->get('shop_name');

            $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'load_shopee_order_running');
            $result['load_shopee_order_running'] = $redis->get($key);

            $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'count_order_shopee');
            $result['count_order_shopee'] = $redis->get($key);

            $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'count_order_shopee_saved');
            $result['count_order_shopee_saved'] = $redis->get($key);
        } catch (Exception $e) {
            // could not connect to redis
        } finally {
            $this->redisClose($redis);
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    public function loadShopeeOrdersManual()
    {
        if (!isset($this->request->get['shopee_id'])) {
            return;
        }

        /* push job to Redis. Notice: use shop_name to define unique job... */
        $redis = null;

        try {
            $shopee_id = (int)$this->request->get['shopee_id'];
            if (!$shopee_id) {
                return;
            }
            $redis = $this->redisConnect();

            /* update process status */
            $shop_name = $this->config->get('shop_name');
            $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'load_shopee_order_running');
            if ((int)$redis->get($key) == 1) {
                return;
            };
            $this->updateSyncOrderProcessingStatusRunning($shop_name, $shopee_id);

            /* create job */
            $job = [
                self::CJ_JOB_KEY_DB_HOSTNAME => DB_HOSTNAME,
                self::CJ_JOB_KEY_DB_PORT => DB_PORT,
                self::CJ_JOB_KEY_DB_USERNAME => DB_USERNAME,
                self::CJ_JOB_KEY_DB_PASSWORD => DB_PASSWORD,
                self::CJ_JOB_KEY_DB_DATABASE => DB_DATABASE,
                self::CJ_JOB_KEY_DB_PREFIX => DB_PREFIX,
                self::CJ_JOB_KEY_DB_DRIVER => DB_DRIVER,
                self::CJ_JOB_KEY_SHOP_NAME => $shop_name,
                self::CJ_JOB_KEY_SHOPEE_ID => $shopee_id,
                self::CJ_JOB_KEY_SYNC_INTERVAL => 0,
                self::CJ_JOB_KEY_SYNC_NEXT_TIME => null,
                self::CJ_JOB_KEY_SYNC_TYPE => 'manual', // auto / manual
                self::CJ_JOB_KEY_TASK => self::CJ_JOB_TASK_GET_ORDERS
            ];

            $redis->lPush(SHOPEE_ORDER_JOB_REDIS_QUEUE, json_encode($job));
        } catch (Exception $e) {
            // could not connect to redis
            $this->log->write('[controller/sale_channel/shopee] loadShopeeOrders(): could not connect to redis. Error: ' . $e->getMessage());
        } finally {
            $this->redisClose($redis);
        }
    }

    public function syncOrders()
    {
        ini_set('max_execution_time', 300);
        set_time_limit(300);
        $this->load->language('sale_channel/shopee');
        $json = [
            'status' => false,
            'msg' => $this->language->get('warning_sync_fail'),
        ];
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateSyncOrders()) {
            $this->load->model('catalog/shopee');
            $id = $this->request->post['id'];
            $order_codes = [];
            if ($id == 'all') {
                $shopee_id = isset($this->request->post['shopee_id']) ? (int)$this->request->post['shopee_id'] : 0;
                if (!$shopee_id) {
                    $order_codes = [];
                } else {
                    $order_codes = $this->model_catalog_shopee->getOrderCodesByShop($shopee_id);
                }
            } else {
                $order_code = $this->model_catalog_shopee->getOrderCodeById((int)$id);
                if ($order_code) {
                    $order_codes[] = $order_code;
                }
            }
            $flag = false;
            if (is_array($order_codes)) {
                foreach ($order_codes as $code) {
                    if (!$code) {
                        continue;
                    }

                    $result = $this->model_catalog_shopee->syncOrderToAdmin($code);
                    if (!$flag && $result) {
                        $flag = $result;
                    }
                }
            }
            if ($flag) {
                $json['status'] = true;
                $json['msg'] = 'success!';
            }
        }

        if ($json['status']) {
            $this->session->data['success'] = $this->language->get('status_sync_success');
        } else {
            $this->session->data['error'] = $this->language->get('status_sync_error');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function validateSyncOrders()
    {
        $this->load->language('sale_channel/shopee');
        if (!$this->user->hasPermission('modify', 'sale_channel/shopee')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        if (!isset($this->request->post['id'])) {
            $this->error['warning'] = $this->language->get('warning_sync_fail');
        } else {
            if ($this->request->post['id'] == 'all' && !isset($this->request->post['shopee_id'])) {
                $this->error['warning'] = $this->language->get('warning_sync_fail');
            }
        }

        return !$this->error;
    }

    private function orderStatusDetail($status)
    {
        $this->load->language('sale_channel/shopee');
        $text = '';
        switch ($status) {
            case 'UNPAID':
                $text = $this->language->get('status_un_paid');
                break;
            case 'READY_TO_SHIP':
                $text = $this->language->get('status_to_ship');
                break;
            case 'COMPLETED':
                $text = $this->language->get('status_completed');
                break;
            case 'IN_CANCEL':
                $text = $this->language->get('status_cancelled');
                break;
            case 'CANCELLED':
                $text = $this->language->get('status_cancelled');
                break;
            case 'TO_RETURN':
                $text = $this->language->get('status_to_return');
                break;
            case 'TO_CONFIRM_RECEIVE':
                $text = $this->language->get('status_to_confirm_receive');
                break;
            case 'RETRY_SHIP':
                $text = $this->language->get('status_retry_ship');
                break;
            case 'SHIPPED':
                $text = $this->language->get('status_shipped');
                break;
        }

        return $text;
    }

    private function orderSyncStatusDetail($status)
    {
        $this->load->language('sale_channel/shopee');
        if ($status == ModelCatalogShopee::SYNC_STATUS_SYNC_SUCCESS) {
            $text = $this->language->get('status_sync_success');
        } elseif ($status == ModelCatalogShopee::SYNC_STATUS_ORDER_NOT_YET_SYNC) {
            $text = $this->language->get('text_status_not_yet_sync');
        } else {
            $text = $this->language->get('status_sync_error');
        }

        return $text;
    }

    private function orderErrorSyncStatusDetail($status)
    {
        $this->load->language('sale_channel/shopee');
        $text = '';
        switch ($status) {
            case ModelCatalogShopee::SYNC_STATUS_ERROR_ORDER_IS_EDITED:
                $text = $this->language->get('status_sync_error_order_has_been_process');
                break;
            case ModelCatalogShopee::SYNC_STATUS_ERROR_PRODUCT_ORDER_NOT_SYNC:
                $text = $this->language->get('status_sync_error_product_not_sync');
                break;
            case ModelCatalogShopee::SYNC_STATUS_ERROR_ORDER_SYNC_TIMEOUT:
                $text = $this->language->get('status_sync_error_sync_timeout');
                break;
            case ModelCatalogShopee::SYNC_STATUS_ERROR_PRODUCT_BESTMEEE_REMOVED:
                $text = $this->language->get('status_sync_error_product_deleted_on_shopee');
                break;
            case ModelCatalogShopee::SYNC_STATUS_ERROR_PRODUCT_OUT_OF_STOCK_ON_ADMIN:
                $text = $this->language->get('status_sync_error_product_out_of_stock_on_admin');
                break;
        }

        return $text;
    }

    private function orderSolutionSyncStatusDetail($status)
    {
        $this->load->language('sale_channel/shopee');
        $text = '';
        switch ($status) {
            case ModelCatalogShopee::SYNC_STATUS_ERROR_ORDER_IS_EDITED:
                $text = $this->language->get('status_sync_solution_order_has_been_process');
                break;
            case ModelCatalogShopee::SYNC_STATUS_ERROR_PRODUCT_ORDER_NOT_SYNC:
                $text = $this->language->get('status_sync_solution_product_not_sync');
                break;
            case ModelCatalogShopee::SYNC_STATUS_ERROR_ORDER_SYNC_TIMEOUT:
                $text = $this->language->get('status_sync_solution_sync_timeout');
                break;
            case ModelCatalogShopee::SYNC_STATUS_ERROR_PRODUCT_BESTMEEE_REMOVED:
                $text = $this->language->get('status_sync_solution_product_deleted_on_shopee');
                break;
            case ModelCatalogShopee::SYNC_STATUS_ERROR_PRODUCT_OUT_OF_STOCK_ON_ADMIN:
                $text = $this->language->get('status_sync_solution_product_out_of_stock_on_admin');
                break;
        }

        return $text;
    }

    private function loadShopeeOrders($shopee_id, $sync_interval, $sync_next_time, $sync_type)
    {
        /* push job to Redis. Notice: use shop_name to define unique job... */
        $redis = null;

        try {
            if (!$shopee_id) {
                return;
            }

            /* update process status */
            $shop_name = $this->config->get('shop_name');
            //$this->updateSyncOrderProcessingStatusRunning($shopee_id, $shop_name);

            /* create job */
            $job = [
                self::CJ_JOB_KEY_DB_HOSTNAME => DB_HOSTNAME,
                self::CJ_JOB_KEY_DB_PORT => DB_PORT,
                self::CJ_JOB_KEY_DB_USERNAME => DB_USERNAME,
                self::CJ_JOB_KEY_DB_PASSWORD => DB_PASSWORD,
                self::CJ_JOB_KEY_DB_DATABASE => DB_DATABASE,
                self::CJ_JOB_KEY_DB_PREFIX => DB_PREFIX,
                self::CJ_JOB_KEY_DB_DRIVER => DB_DRIVER,
                self::CJ_JOB_KEY_SHOP_NAME => $shop_name,
                self::CJ_JOB_KEY_SHOPEE_ID => $shopee_id,
                self::CJ_JOB_KEY_SYNC_INTERVAL => $sync_interval,
                self::CJ_JOB_KEY_SYNC_NEXT_TIME => $sync_next_time, // timestamp
                self::CJ_JOB_KEY_SYNC_TYPE => $sync_type, // auto / manual
                self::CJ_JOB_KEY_TASK => self::CJ_JOB_TASK_GET_ORDERS
            ];

            $redis = $this->redisConnect();
            $redis->lPush(SHOPEE_ORDER_JOB_REDIS_QUEUE, json_encode($job));
        } catch (Exception $e) {
            // could not connect to redis
            $this->log->write('[controller/sale_channel/shopee] loadShopeeOrders(): could not connect to redis. Error: ' . $e->getMessage());
        } finally {
            $this->redisClose($redis);
        }
    }

    private function updateOrderJob($shopee_id, $sync_interval, $update_type)
    {
        /* push job to Redis. Notice: use shop_name to define unique job... */
        $redis = null;

        try {
            if (!$shopee_id) {
                return;
            }
            $shop_name = $this->config->get('shop_name');

            $update_job = [
                self::CJ_UPDATE_JOB_KEY_TYPE => $update_type,
                self::CJ_UPDATE_JOB_KEY_sync_interval => $sync_interval
            ];

            $redis = $this->redisConnect();
            $key = self::CJ_UPDATE_JOB_TASK_GET_ORDERS_PREFIX . $shop_name . $shopee_id;
            $redis->set($key, json_encode($update_job));
        } catch (Exception $e) {
            // could not connect to redis
            $this->log->write('[controller/sale_channel/shopee] loadShopeeOrders(): could not connect to redis. Error: ' . $e->getMessage());
        } finally {
            $this->redisClose($redis);
        }
    }

    private function getProductByStatus($filter_data, $status_product)
    {
        $products = $this->model_catalog_shopee->getProducts($filter_data);

        $skues = $this->model_catalog_shopee->getSkues();
        $skues = array_count_values($skues);

        $list_product_by_status = [];
        foreach ($products as $item) {
            $status = $this->getStatus($item, $skues);
            if ($status['sync_status_code'] == $status_product) {
                $item = array_merge($item, $status);
                $list_product_by_status[] = $item;
            }
        }

        $result['products'] = array_slice($list_product_by_status, $filter_data['start'], $filter_data['limit']);
        $result['total'] = count($list_product_by_status);

        return $result;
    }


    private function getStatus($item, $skues)
    {
        $status = array(
            'sync_status_code' => null,
            'sync_status_value' => null
        );

        //  trùng sku sản phẩm trên shopee
        if ($item['sku'] != '' && $skues[$item['sku']] > 1) {
            $status['sync_status_code'] = self::SHOPEE_STATUS_DUPLICATE;
            $status['sync_status_value'] = $this->language->get('text_duplicate_sku') . $item['sku'];
            return $status;
        }

        if (!$item['has_variation']) {
            // sản phẩm 1 phiên bản

            // không có sku
            if ($item['sku'] == '') {
                $status['sync_status_code'] = self::SHOPEE_STATUS_WITHOUT_SKU;
                $status['sync_status_value'] = $this->language->get('missing_sku');

                return $status;
            }

            $product = $this->model_catalog_product->getProductOneVersionBySKU($item['sku']);
            if (empty($product)) {
                // trương hợp tạo mới
                $status['sync_status_code'] = self::SHOPEE_STATUS_CREATE;

                return $status;
            } else {
                // trường hợp đồng bộ
                $status['sync_status_code'] = self::SHOPEE_STATUS_EXISTED;
                $status['sync_status_value'] = $this->language->get('entry_status_exist_shopee');
                $status['product_id_update'] = $product['product_id'];

                return $status;
            }
        } else {
            // sản phẩm nhiều phiên bản
            $variations = json_decode($item['variations'], true);
            $variation_skues = [];
            foreach ($variations as $variation) {
                if (!array_key_exists('variation_sku', $variation) || $variation['variation_sku'] == '') {
                    continue;
                }

                $variation_skues[] = $variation['variation_sku'];
            }
            // trường hợp nhiều phiên bản nhưng các phiên bản không có sku hoặc ko có phiên bản
            if (empty($variation_skues)) {
                $status['sync_status_code'] = self::SHOPEE_STATUS_WITHOUT_SKU;
                $status['sync_status_value'] = $this->language->get('not_all_variation_product');

                return $status;
            }

            $products = $this->model_catalog_product->getProductMultiVersionByVersionSKUES($variation_skues);

            // trường hợp nhiều phiên bản nhưng không có sản phẩm nào trùng version sku
            if (empty($products)) {
                $status['sync_status_code'] = self::SHOPEE_STATUS_CREATE;
                $status['sync_status_value'] = '';

                return $status;
            }

            if (count($products) > 1) {
                $status['sync_status_code'] = self::SHOPEE_STATUS_DUPLICATE_VERSION_PRODUCT;
                $status['sync_status_value'] = $this->language->get('more_than_duplicate');

                return $status;
            }

            $status['sync_status_code'] = self::SHOPEE_STATUS_EXISTED;
            $status['sync_status_value'] = $this->language->get('entry_status_exist');
            $status['product_id_update'] = $products[0]['product_id'];

            return $status;
        }
    }

    public function replace_url($url)
    {
        return str_replace("amp;", "", $url);
    }

    //Get option status
    public function loadoptionone()
    {
        $this->load->language('sale_channel/shopee');
        $this->load->model('catalog/shopee');
        $categoryID = $this->request->post['categoryID'];
        $end_select_html = '</select></div>';
        $show_html = '';

        if ($categoryID > 0) {
            if ($categoryID == 1) { // Trang thai hien thi
                $show_html .= '<div class="form-group"><select class="form-control select2 form-control-sm" data-placeholder="' . $this->language->get('filter_status_placeholder') . '" data-label="' . $this->language->get('filter_status') . '" id="status_product" name="status_product">';
                $show_html .= '<option value="">' . $this->language->get('filter_status') . '</option>';
                if (self::SYNC_TYPE == 0) { // sync by sku
                    $show_html .= '<option value="0" data-value="' . $this->language->get('entry_status_without_sku') . '">' . $this->language->get('entry_status_without_sku') . '</option>';
                    $show_html .= '<option value="1" data-value="' . $this->language->get('entry_status_create') . '">' . $this->language->get('entry_status_create') . '</option>';
                    $show_html .= '<option value="2" data-value="' . $this->language->get('entry_status_exist') . '">' . $this->language->get('entry_status_exist') . '</option>';
                    $show_html .= '<option value="3" data-value="' . $this->language->get('entry_status_duplicate') . '">' . $this->language->get('entry_status_duplicate') . '</option>';
                    $show_html .= '<option value="4" data-value="' . $this->language->get('entry_status_duplicate_version_product') . '">' . $this->language->get('entry_status_duplicate_version_product') . '</option>';
                } else { // sync by item_id
                    $show_html .= '<option value="' . self::STATUS_CREATE . '" data-value="' . $this->language->get('text_action_create') . '">' . $this->language->get('text_action_create') . '</option>';
                    $show_html .= '<option value="' . self::STATUS_UPDATE . '" data-value="' . $this->language->get('text_action_update') . '">' . $this->language->get('text_action_update') . '</option>';
                }
            }
            $show_html .= $end_select_html;
        }
        echo $show_html .= '';
    }

    /**
     * @return null|Redis
     */
    private function redisConnect()
    {
        $redis = null;

        try {
            $redis = new \Redis();
            $redis->connect(MUL_REDIS_HOST, MUL_REDIS_PORT);
            $redis->select(MUL_REDIS_DB_SHOPEE);
        } catch (Exception $e) {
            // could not connect to redis
        }

        return $redis;
    }

    /**
     * @param Redis $redis
     */
    private function redisClose($redis)
    {
        if (!$redis instanceof \Redis) {
            return;
        }

        try {
            $redis->close();
        } catch (Exception $e) {
            // could not connect to redis
        }
    }

    private function updateProcessingStatusRunning($shop_name, $shopee_id)
    {
        $redis = $this->redisConnect();

        $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'load_shopee_product_running');
        $redis->set($key, 1, SHOPEE_PROCESS_INFO_TIMEOUT);

        $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'count_product_shopee');
        $redis->set($key, 0, SHOPEE_PROCESS_INFO_TIMEOUT);

        $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'count_product_shopee_saved');
        $redis->set($key, 0, SHOPEE_PROCESS_INFO_TIMEOUT);

        $this->redisClose($redis);
    }

    private function updateSyncOrderProcessingStatusRunning($shop_name, $shopee_id)
    {
        $redis = $this->redisConnect();

        $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'load_shopee_order_running');
        $redis->set($key, 1, SHOPEE_PROCESS_INFO_TIMEOUT);

        $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'count_order_shopee');
        $redis->set($key, 0, SHOPEE_PROCESS_INFO_TIMEOUT);

        $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'count_order_shopee_saved');
        $redis->set($key, 0, SHOPEE_PROCESS_INFO_TIMEOUT);

        $this->redisClose($redis);
    }

    private function validateMapping()
    {
        $this->load->language('sale_channel/shopee');
        if (!$this->user->hasPermission('modify', 'sale_channel/shopee')) {
            return false;
        }
        if (!isset($this->request->post['id'])) {
            return false;
        }

        if (!isset($this->request->post['mapping_id'])) {
            return false;
        }

        return true;
    }

    private function validateCancelMapping()
    {
        $this->load->language('sale_channel/shopee');
        if (!$this->user->hasPermission('modify', 'sale_channel/shopee')) {
            return false;
        }
        if (!isset($this->request->post['id'])) {
            return false;
        }

        if (!isset($this->request->post['item_id'])) {
            return false;
        }

        return true;
    }

    private function validateCancelAllMapping()
    {
        $this->load->language('sale_channel/shopee');
        if (!$this->user->hasPermission('modify', 'sale_channel/shopee')) {
            return false;
        }

        if (!isset($this->request->post['item_id'])) {
            return false;
        }

        return true;
    }

    private function validateCreateProduct()
    {
        $this->load->language('sale_channel/shopee');
        if (!$this->user->hasPermission('modify', 'sale_channel/shopee')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        if (!isset($this->request->post['type']) || !isset($this->request->post['id'])) {
            $this->error['warning'] = $this->language->get('warning_validate_create_prod');
        }

        return !$this->error;
    }
}