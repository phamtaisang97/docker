<?php


class ControllerSaleChannelNovaonxSocial extends Controller
{
    public function index()
    {
        $this->load->language('sale_channel/novaonx_social');
        $this->load->model('setting/setting');

        $this->document->setTitle($this->language->get('heading_title'));

        // old method connect to social. TODO: remove later...
        /*$this->load->model('user/user');
        $current_user_id = $this->user->getId();
        $email = $this->model_user_user->getEmailByUser($current_user_id);

        $redirect = REDIRECT_TO_NOVAON_X_SOCIAL;
        $redirect = str_replace('${CLIENT_ID}', MIX_CLIENT_ID_SSO, $redirect);
        $redirect = str_replace('${SHOP_ID}', SHOP_ID, $redirect);
        $redirect = str_replace('${EMAIL}', urlencode($email), $redirect);
        $data['redirect_novaon_x_social'] = $redirect;*/

        $redirect = GO_TO_NOVAON_X_SOCIAL; // . '?token=' . $this->session->data['user_token'];
        $data['redirect_novaon_x_social'] = $redirect;

        /* common */
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');

        $this->response->setOutput($this->load->view('sale_channel/novaonx_social', $data));
    }
}