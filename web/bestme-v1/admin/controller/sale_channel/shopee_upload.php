<?php

class ControllerSaleChannelShopeeUpload extends Controller
{
    private $error = array();
    // redis job key
    const CJ_JOB_KEY_DB_HOSTNAME = 'db_hostname';
    const CJ_JOB_KEY_DB_PORT = 'db_port';
    const CJ_JOB_KEY_DB_USERNAME = 'db_username';
    const CJ_JOB_KEY_DB_PASSWORD = 'db_password';
    const CJ_JOB_KEY_DB_DATABASE = 'db_database';
    const CJ_JOB_KEY_DB_PREFIX = 'db_prefix';
    const CJ_JOB_KEY_DB_DRIVER = 'db_driver';
    const CJ_JOB_KEY_SHOP_NAME = 'shop_name';
    const CJ_JOB_KEY_SHOPEE_ID = 'shopee_id';
    const CJ_JOB_KEY_SHOPEE_UPLOAD_PRODUCT_IDS = 'product_ids';
    const CJ_JOB_KEY_TASK = 'task';
    // all tasks
    const CJ_JOB_TASK_UPLOAD_PRODUCTS = 'UPLOAD_PRODUCTS';

    public function index()
    {
        $this->load->language('sale_channel/shopee_upload');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->addStyle('view/stylesheet/custom/shopee.css');
        $this->load->model('catalog/shopee_upload');
        $this->load->model('catalog/shopee');

        $this->getList();
    }

    protected function getList()
    {
        $has_shop = $this->model_catalog_shopee_upload->getFirstShopId();
        if (!$has_shop) {
            $this->response->redirect($this->url->link('sale_channel/shopee/config', 'user_token=' . $this->session->data['user_token'], true));
        }

        $data = [];

        if (isset($this->session->data['error'])) {
            $data['error'] = $this->session->data['error'];

            $this->session->data['error'] = '';
            unset($this->session->data['error']);
        } else {
            $data['error'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            $this->session->data['success'] = '';
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $data['status_tab'] = $status_tab = isset($this->request->get['status_tab']) ? $this->request->get['status_tab'] : 0;
        $filter_name = isset($this->request->get['filter_name']) ? trim($this->request->get['filter_name']) : '';

        $data['selected_all'] = isset($this->request->get['select_all']) ? $this->request->get['select_all'] : false;

        $logistics = $this->model_catalog_shopee_upload->getLogistics();
        $shopee_shops = $this->model_catalog_shopee->getShopeeShopList();

        if ($status_tab == 0 || $status_tab == 1) {
            $shop_selected = isset($this->request->get['shop_selected']) ? $this->request->get['shop_selected'] : null;
        }

        if ($status_tab == 2 || $status_tab == 3) {
            $shop_selected = isset($this->request->get['shop_selected']) ? $this->request->get['shop_selected'] : (isset($shopee_shops[0]['shop_id']) ? $shopee_shops[0]['shop_id'] : null);
        }

        $data['shop_selected'] = $shop_selected;

        foreach ($shopee_shops as &$shop) {
            $shop['url'] = $this->url->link('sale_channel/shopee_upload', 'user_token=' . $this->session->data['user_token'] . '&shop_selected=' . $shop['shop_id'] . '&status_tab=' . $status_tab, true);
        }

        $filter_data = [
            'status' => $status_tab,
            'name' => $filter_name,
            'shop_id' => $shop_selected,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        ];

        $products = $this->model_catalog_shopee_upload->getList($filter_data);
        $total_product = $this->model_catalog_shopee_upload->getTotalProduct($filter_data);
        foreach ($products as &$product) {
            $product['full_category'] = $this->model_catalog_shopee_upload->getFullCategoriesById($product['category_id']);
            $product['edit'] = $this->url->link('sale_channel/shopee_upload/edit', 'user_token=' . $this->session->data['user_token'] . '&product_id=' . $product['shopee_upload_product_id'] . '&status_tab='.$status_tab, true);
            $product['version'] = $this->model_catalog_shopee_upload->getMinMaxVersionPriceAndStockById($product['shopee_upload_product_id']);
        }
        //unset($product);

        $data['logistics'] = $logistics;
        $data['shops'] = $shopee_shops;
        $data['products'] = $products;
        $data['total_product'] = $total_product;

        $data['add_new'] = $this->url->link('sale_channel/shopee_upload/add', 'user_token=' . $this->session->data['user_token'], true);
        $data['edit_price'] = $this->url->link('sale_channel/shopee_upload/editPrice', 'user_token=' . $this->session->data['user_token'], true);
        $data['edit_price_version'] = $this->url->link('sale_channel/shopee_upload/editPriceVersion', 'user_token=' . $this->session->data['user_token'], true);
        $data['edit_stock'] = $this->url->link('sale_channel/shopee_upload/editStock', 'user_token=' . $this->session->data['user_token'], true);
        $data['edit_stock_version'] = $this->url->link('sale_channel/shopee_upload/editStockVersion', 'user_token=' . $this->session->data['user_token'], true);
        $data['map_brand'] = $this->url->link('sale_channel/shopee_upload/mapBrand', 'user_token=' . $this->session->data['user_token'], true);
        $data['map_multi_brand'] = $this->url->link('sale_channel/shopee_upload/mapMultiBrand', 'user_token=' . $this->session->data['user_token'], true);
        $data['attach_package_size'] = $this->url->link('sale_channel/shopee_upload/attachPackageSize', 'user_token=' . $this->session->data['user_token'], true);
        $data['shipping_unit_settings'] = $this->url->link('sale_channel/shopee_upload/shippingUnitSettings', 'user_token=' . $this->session->data['user_token'], true);
        $data['delete'] = $this->url->link('sale_channel/shopee_upload/delete', 'user_token=' . $this->session->data['user_token'], true);
        $data['selected_product_bestme'] = $this->url->link('sale_channel/shopee_upload/selectedProductBestme', 'user_token=' . $this->session->data['user_token'], true);

        $data['user_token'] = $this->session->data['user_token'];
        //link
        $data['not_enough_info'] = $this->url->link('sale_channel/shopee_upload', 'user_token=' . $this->session->data['user_token'] . '&status_tab=' . ModelCatalogShopeeUpload::STATUS_PRODUCT_NOT_ENOUGH_INFO, true);
        $data['ready_for_upload'] = $this->url->link('sale_channel/shopee_upload/readyForUpload', 'user_token=' . $this->session->data['user_token'] . '&status_tab=' . ModelCatalogShopeeUpload::STATUS_PRODUCT_READY_FOR_UPLOAD, true);
        $data['upload_failed'] = $this->url->link('sale_channel/shopee_upload/uploadFailed', 'user_token=' . $this->session->data['user_token'] . '&status_tab=' . ModelCatalogShopeeUpload::STATUS_PRODUCT_UPLOAD_FAILED, true);
        $data['upload_successfully'] = $this->url->link('sale_channel/shopee_upload/uploadSuccessfully', 'user_token=' . $this->session->data['user_token'] . '&status_tab=' . ModelCatalogShopeeUpload::STATUS_PRODUCT_UPLOAD_SUCCESS, true);


        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $total_product;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('sale_channel/shopee_upload', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        // Add jump to page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);

        $data['results'] = sprintf($this->language->get('text_pagination'), ($total_product) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($total_product - $this->config->get('config_limit_admin'))) ? $total_product : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $total_product, ceil($total_product / $this->config->get('config_limit_admin')));

        $data['url_filter'] = $this->url->link('sale_channel/shopee_upload', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            switch ($status_tab) {
                case ModelCatalogShopeeUpload::STATUS_PRODUCT_NOT_ENOUGH_INFO:
                    $this->response->setOutput($this->load->view('sale_channel/shopee_upload/not_enough_info/index_append', $data));
                    break;
                case ModelCatalogShopeeUpload::STATUS_PRODUCT_READY_FOR_UPLOAD:
                    $this->response->setOutput($this->load->view('sale_channel/shopee_upload/ready_for_upload/index_append', $data));
                    break;
                case ModelCatalogShopeeUpload::STATUS_PRODUCT_UPLOAD_FAILED:
                    $this->response->setOutput($this->load->view('sale_channel/shopee_upload/upload_failed/index_append', $data));
                    break;
                case ModelCatalogShopeeUpload::STATUS_PRODUCT_UPLOAD_SUCCESS:
                    $this->response->setOutput($this->load->view('sale_channel/shopee_upload/upload_successfully/index_append', $data));
                    break;
            }
        } else {
            switch ($status_tab) {
                case ModelCatalogShopeeUpload::STATUS_PRODUCT_NOT_ENOUGH_INFO:
                    $this->response->setOutput($this->load->view('sale_channel/shopee_upload/not_enough_info/index', $data));
                    break;
                case ModelCatalogShopeeUpload::STATUS_PRODUCT_READY_FOR_UPLOAD:
                    $this->response->setOutput($this->load->view('sale_channel/shopee_upload/ready_for_upload/index', $data));
                    break;
                case ModelCatalogShopeeUpload::STATUS_PRODUCT_UPLOAD_FAILED:
                    $this->response->setOutput($this->load->view('sale_channel/shopee_upload/upload_failed/index', $data));
                    break;
                case ModelCatalogShopeeUpload::STATUS_PRODUCT_UPLOAD_SUCCESS:
                    $this->response->setOutput($this->load->view('sale_channel/shopee_upload/upload_successfully/index', $data));
                    break;
            }
        }

    }

    public function readyForUpload()
    {
        $this->load->language('sale_channel/shopee_upload');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->addStyle('view/stylesheet/custom/shopee.css');
        $this->load->model('catalog/shopee_upload');
        $this->load->model('catalog/shopee');

        $this->getList();
    }

    public function uploadFailed()
    {
        $this->load->language('sale_channel/shopee_upload');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->addStyle('view/stylesheet/custom/shopee.css');
        $this->load->model('catalog/shopee_upload');
        $this->load->model('catalog/shopee');

        $this->getList();
    }

    public function uploadSuccessfully()
    {
        $this->load->language('sale_channel/shopee_upload');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->addStyle('view/stylesheet/custom/shopee.css');
        $this->load->model('catalog/shopee_upload');
        $this->load->model('catalog/shopee');

        $this->getList();
    }

    public function getProductVersionById()
    {
        $json = array();
        $this->load->model('catalog/shopee_upload');

        $product_id = isset($this->request->get['product_id']) ? $this->request->get['product_id'] : '';

        if ($product_id == '') {
            return;
        }

        $results = $this->model_catalog_shopee_upload->getProductVersionById($product_id);
        foreach ($results as $item) {
            $json['results'][] = [
                'version_id' => $item['shopee_upload_product_version_id'],
                'version_name' => $item['version_name'],
                'version_price' => number_format((float)$item['version_price']),
                'version_stock' => number_format((float)$item['version_stock']),
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function add()
    {
        $this->load->language('sale_channel/shopee_upload');
        $this->document->setTitle($this->language->get('heading_title_add'));
        $this->load->model('catalog/shopee_upload');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm($this->request->post)) {
            $this->model_catalog_shopee_upload->addShopeeProduct($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success_add');
            $this->response->redirect($this->url->link('sale_channel/shopee_upload', 'user_token=' . $this->session->data['user_token'].'&status_tab=1', true));
        }

        $this->getForm();
    }

    public function edit()
    {
        $this->load->language('sale_channel/shopee_upload');
        $this->document->setTitle($this->language->get('heading_title_edit'));
        $this->load->model('catalog/shopee_upload');
        $status_tab = isset($this->request->get['status_tab']) && $this->request->get['status_tab'] ? $this->request->get['status_tab'] : 0;

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateFormEdit()) {
            $this->model_catalog_shopee_upload->editShopeeProduct($this->request->post, $this->request->get['product_id']);

            $this->session->data['success'] = $this->language->get('text_success_update');
            $this->response->redirect($this->url->link('sale_channel/shopee_upload', 'user_token=' . $this->session->data['user_token'].'&status_tab='.$status_tab, true));
        }

        $this->getForm();
    }

    private function getForm()
    {
        $this->load->model('catalog/shopee_upload');

        $has_shop = $this->model_catalog_shopee_upload->getFirstShopId();
        if (!$has_shop) {
            $this->response->redirect($this->url->link('sale_channel/shopee/config', 'user_token=' . $this->session->data['user_token'], true));
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error) && !empty($this->error)) {
            $data['errors'] = $this->error;
        }

        if (isset($this->session->data['success']) && $this->session->data['success'] != 'text_success') {
            $data['success'] = $this->session->data['success'];

            $this->session->data['success'] = '';
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data['text_form'] = !isset($this->request->get['product_id']) ? $this->language->get('heading_title_add') : $this->language->get('heading_title_edit');
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('sale_channel/shopee_upload', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $data['text_form'],
            'href' => '',
        );

        if ($this->config->get('config_language') == 'vi-vn') {
            $data['placeholder_image'] = 'view/image/theme/upload-placeholder.svg';
        } else { //en-gb
            $data['placeholder_image'] = 'view/image/theme/en-upload-placeholder.svg';
        }

        if (isset($this->request->get['product_id']) && $this->request->get['product_id']) {
            $data['action'] = $this->url->link('sale_channel/shopee_upload/edit', 'user_token=' . $this->session->data['user_token'] . '&product_id=' . $this->request->get['product_id']. '&status_tab='.$this->request->get['status_tab'], true);
            $data['product'] = $this->model_catalog_shopee_upload->getProductById($this->request->get['product_id']);
            $data['product_images'] = $this->model_catalog_shopee_upload->getProductImages($this->request->get['product_id']);
            $data['logistics'] = $this->model_catalog_shopee_upload->getProductLogistics($this->request->get['product_id']);
            $data['product_versions'] = array_reverse($this->model_catalog_shopee_upload->getProductVersionById($this->request->get['product_id']));
            if ($data['product']['category_id']) {
                $data['category_name'] = $this->model_catalog_shopee_upload->getFullCategoriesById($data['product']['category_id']);
            }
            $data['version_attribute'] = json_decode($data['product']['version_attribute']);
        } else {
            $data['action'] = $this->url->link('sale_channel/shopee_upload/add', 'user_token=' . $this->session->data['user_token'], true);
            $data['logistics'] = $this->model_catalog_shopee_upload->getLogistics();
        }

        $data['cancel'] = $this->url->link('sale_channel/shopee_upload', 'user_token=' . $this->session->data['user_token'], true);

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $data['user_token'] = $this->session->data['user_token'];

        $this->response->setOutput($this->load->view('sale_channel/shopee_upload/product_form/index', $data));
    }

    private function validateForm($data)
    {
        if (!$this->user->hasPermission('access', 'sale_channel/shopee')) {
            $this->session->data['error'] = $this->language->get('error_permission');
            return false;
        }

        if (!isset($data['name']) || (isset($data['name']) && !$data['name'])) {
            $this->error['name'] = $this->language->get('error_name_required');
        } elseif (mb_strlen($data['name']) > 120 || mb_strlen($data['name']) < 10) {
            $this->error['name'] = $this->language->get('error_name_length');
        }


        if (!isset($data['description']) || (isset($data['description']) && !$data['description'])) {
            $this->error['description'] = $this->language->get('error_description_required');
        } elseif (mb_strlen($data['description']) < 100 || mb_strlen($data['description']) > 3000) {
            $this->error['description'] = $this->language->get('error_description_length');
        }

        if (!isset($data['images']) || (isset($data['images']) && empty($data['images']))) {
            $this->error['images'] = $this->language->get('error_image_required');
        }


        if (!isset($data['category_id']) || (isset($data['category_id']) && !$data['category_id'])) {
            $this->error['category_id'] = $this->language->get('error_category_required');
        }

        if (!isset($data['category_id']) || (isset($data['category_id']) && !$data['category_id'])) {
            $this->error['category_id'] = $this->language->get('error_category_required');
        }

        $data['price'] = (double)str_replace(',', '', $data['price']);
        if (!$this->isMultiVersion($data)) {
            if (!isset($data['price']) || (isset($data['price']) && !$data['price'])) {
                $this->error['price'] = $this->language->get('error_price_required');
            }
            if (!isset($data['quantity']) || (isset($data['quantity']) && !$data['quantity'])) {
                $this->error['quantity'] = $this->language->get('error_quantity_required');
            }
        } else {
            if (isset($data['product_version_names']) && count($data['product_version_names']) > 50) {
                $this->error['error_max_version'] = $this->language->get('error_max_version');
            }
            if (isset($data['product_skus']) && !empty($data['product_skus'])) {
                foreach ($data['product_skus'] as $product_skus) {
                    if (isset($product_skus) && $product_skus && mb_strlen($product_skus) > 20) {
                        $this->error['product_skus'] = $this->language->get('error_sku_length');
                    }
                    if (isset($product_skus) && $product_skus && !preg_match('/^[\\\sa-zA-Z0-9[)(_-]+$/', $product_skus)) {
                        $this->error['product_skus'] = $this->language->get('error_sku_special_char');
                    }
                }
            }

            if (isset($data['product_prices']) && !empty($data['product_prices'])) {
                foreach ($data['product_prices'] as $product_price) {
                    $product_price = (double)str_replace(',', '', $product_price);
                    if (!$product_price) {
                        $this->error['price'] = $this->language->get('error_price_required');
                        break;
                    }
                    if ((int)$product_price < 1000 || (int)$product_price > 120000000) {
                        $this->error['product_prices'] = $this->language->get('error_price_between');
                        break;
                    }
                }
            }

            if (isset($data['product_quantities']) && !empty($data['product_quantities'])) {
                foreach ($data['product_quantities'] as $product_quantity) {
                    if (!$product_quantity) {
                        $this->error['quantity'] = $this->language->get('error_quantity_required');
                        break;
                    }
                    if ((int)$product_quantity < 1 || (int)$product_quantity > 999998) {
                        $this->error['product_quantities'] = $this->language->get('error_quantity_between');
                        break;
                    }
                }
            }
        }

        if (isset($data['price']) && $data['price'] && ((int)$data['price'] < 1000 || (int)$data['price'] > 120000000)) {
            $this->error['price'] = $this->language->get('error_price_between');
        }
        if (isset($data['quantity']) && $data['quantity'] && ((int)$data['quantity'] < 1 || (int)$data['quantity'] > 999998)) {
            $this->error['quantity'] = $this->language->get('error_quantity_between');
        }
        if (isset($data['sku']) && $data['sku'] && mb_strlen($data['sku']) > 20) {
            $this->error['sku'] = $this->language->get('error_sku_length');
        }
        if (isset($data['sku']) && $data['sku'] && !preg_match('/^[\\\sa-zA-Z0-9[)(_-]+$/', $data['sku'])) {
            $this->error['sku'] = $this->language->get('error_sku_special_char');
        }

        if (isset($data['attribute_name']) && !empty($data['attribute_name'])) {
            $attribute_names = $data['attribute_name'];
            foreach ($attribute_names as $attribute_name) {
                if (mb_strlen($attribute_name) > 14) {
                    $this->error['attribute_name'] = $this->language->get('error_attribute_length');
                }
            }
        }

        if (isset($data['attribute_values']) && !empty($data['attribute_values'])) {
            $attribute_values = $data['attribute_values'];
            foreach ($attribute_values as $attribute_value) {
                if (count($attribute_value) > 20) {
                    $this->error['max_attribute_version'] = $this->language->get('error_version_attr_value_max_size');
                    break;
                }
                foreach ($attribute_value as $value) {
                    if (mb_strlen($value) > 20) {
                        $this->error['attribute_length'] = $this->language->get('error_version_attr_value_length');
                    }
                }
            }
        }


        if (!isset($data['weight']) || (isset($data['weight']) && !$data['weight'])) {
            $this->error['weight'] = $this->language->get('error_weight_required');
        } elseif ((int)$data['weight'] < 10 || (int)$data['weight'] > 1000000) {
            $this->error['weight'] = $this->language->get('error_weight_between');
        }

        if (!isset($data['length']) || (isset($data['length']) && !$data['length'])) {
            $this->error['length'] = $this->language->get('error_length_required');
        } elseif ((int)$data['length'] < 1 || (int)$data['length'] > 1000000) {
            $this->error['length'] = $this->language->get('error_length_between');
        }

        if (!isset($data['width']) || (isset($data['width']) && !$data['width'])) {
            $this->error['width'] = $this->language->get('error_width_required');
        } elseif ((int)$data['width'] < 1 || (int)$data['width'] > 1000000) {
            $this->error['width'] = $this->language->get('error_width_between');
        }

        if (!isset($data['height']) || (isset($data['height']) && !$data['height'])) {
            $this->error['height'] = $this->language->get('error_height_required');
        } elseif ((int)$data['height'] < 1 || (int)$data['height'] > 1000000) {
            $this->error['height'] = $this->language->get('error_height_between');
        }

        if (!isset($data['logistics']) || (isset($data['logistics']) && empty($data['logistics']))) {
            $this->error['logistics'] = $this->language->get('error_logistic_required');
        }

        return !$this->error;
    }

    private function isMultiVersion($data)
    {
        if (isset($data['attribute_name']) && !empty($data['attribute_name']) &&
            isset($data['attribute_values']) && !empty($data['attribute_values'])) {
            return true;
        }
        return false;
    }

    private function validateFormEdit()
    {
        if (!$this->validateForm($this->request->post)) {
            return false;
        }
        if (!isset($this->request->get['product_id']) || (isset($this->request->get['product_id']) && !$this->request->get['product_id'])) {
            $this->error['product_id'] = $this->language->get('error_product_id_not_found');
        }

        return !$this->error;
    }

    public function getCategories()
    {
        $this->load->model('catalog/shopee_upload');
        $results = $this->model_catalog_shopee_upload->getCategories();
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($results));
    }

    public function getLogistics()
    {
        $this->load->model('catalog/shopee_upload');
        $results = $this->model_catalog_shopee_upload->getLogistics();
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($results));
    }

    public function getShopeeAttribute()
    {
        $this->load->model('catalog/shopee_upload');
        $shop_id = $this->model_catalog_shopee_upload->getFirstShopId();
        $language_code = $this->getCurrentLanguageId();

        $shopee = Sale_Channel::getSaleChannel('shopee', ['shop_id' => (int)$shop_id]);
        $attributes = json_decode($shopee->getAttribute(['language' => $language_code, 'category_id' => (int)$this->request->get['category_id']]), true)['attributes'];

        $product_attrs = $this->model_catalog_shopee_upload->getProductAttrs($this->request->get['product_id']);

        $product_attrs_column = array_column($product_attrs, 'attribute_id');

        foreach ($attributes as &$attribute) {
            $key = array_search($attribute['attribute_id'], $product_attrs_column);
            if ($key > -1) {
                $attribute['value'] = htmlspecialchars_decode($product_attrs[$key]['value']);
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($attributes));
    }

    public function getProductsLazyLoad()
    {
        $json = array();
        $this->load->model('catalog/shopee_upload');
        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        // display 6 product in select config theme is many, or setup other config
        $filter_data = array(
            'limit' => 6,
            'start' => ($page - 1) * 6,
        );

        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        $count_product = $this->model_catalog_shopee_upload->countAllProduct($filter_data);
        $results = $this->model_catalog_shopee_upload->getProductPaginator($filter_data);

        foreach ($results as $key => $value) {
            $json['results'][] = array(
                'id' => $value['product_id'],
                'product_id' => $value['product_id'],
                'text' => $value['name'],
                'multi_versions' => $value['multi_versions'],
                'edit' => $this->url->link('catalog/product/edit', 'user_token=' . $this->session->data['user_token'] . '&product_id=' . $value['product_id'], true),
                'price' => number_format($value['compare_price'], 0, '', ','),
                'price_version' => $this->model_catalog_shopee_upload->getMinMaxVersionPriceProductBestmeById($value['product_id']),
                'quantity' => $this->model_catalog_shopee_upload->getAllQuantityOfProductById($value['product_id']),
                'linked' => $this->model_catalog_shopee_upload->checkProductMapWithShopee($value['product_id']),
                'disabled' => $this->model_catalog_shopee_upload->checkProductDoSelect($value['product_id']),
            );
        }

        $json['count'] = $count_product;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function editPrice()
    {
        $this->load->language('sale_channel/shopee_upload');
        $this->document->setTitle($this->language->get('heading_title_edit'));
        $this->load->model('catalog/shopee_upload');

        $product_id = isset($this->request->post['product_id']) ? $this->request->post['product_id'] : '';
        $status = isset($this->request->post['status']) ? $this->request->post['status'] : 0;
        $price = isset($this->request->post['price']) ? (float)str_replace(',', '', $this->request->post['price']) : '';

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateEditPrice()) {
            $this->model_catalog_shopee_upload->editPrice($product_id, $price);

            $this->response->redirect($this->url->link('sale_channel/shopee_upload', 'user_token=' . $this->session->data['user_token'] . '&status_tab=' . $status, true));
        }

        $this->getForm();
    }

    public function editPriceVersion()
    {
        $this->load->language('sale_channel/shopee_upload');
        $this->document->setTitle($this->language->get('heading_title_edit'));
        $this->load->model('catalog/shopee_upload');

        $arr_product_version_id = isset($this->request->post['product_version_id']) && is_array($this->request->post['product_version_id']) ? $this->request->post['product_version_id'] : [];
        $arr_product_version_price = isset($this->request->post['price_version']) && is_array($this->request->post['price_version']) ? $this->request->post['price_version'] : [];
        $status = isset($this->request->post['status']) ? $this->request->post['status'] : 0;
        $product_id = isset($this->request->post['product_id']) ? $this->request->post['product_id'] : '';

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateEditPriceVersion()) {
            foreach ($arr_product_version_id as $key => $pv_id) {
                $this->model_catalog_shopee_upload->editPriceVersion($product_id, $pv_id, (float)str_replace(',', '', $arr_product_version_price[$key]));
            }

            $this->response->redirect($this->url->link('sale_channel/shopee_upload', 'user_token=' . $this->session->data['user_token'] . '&status_tab=' . $status, true));
        }

        $this->getForm();
    }

    public function editStock()
    {
        $this->load->language('sale_channel/shopee_upload');
        $this->document->setTitle($this->language->get('heading_title_edit'));
        $this->load->model('catalog/shopee_upload');

        $product_id = isset($this->request->post['product_id']) ? $this->request->post['product_id'] : '';
        $price = isset($this->request->post['stock']) ? (float)str_replace(',', '', $this->request->post['stock']) : '';
        $status = isset($this->request->post['status']) ? $this->request->post['status'] : 0;

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateEditStock()) {
            $this->model_catalog_shopee_upload->editStock($product_id, $price);

            $this->response->redirect($this->url->link('sale_channel/shopee_upload', 'user_token=' . $this->session->data['user_token'] . '&status_tab=' . $status, true));
        }

        $this->getForm();
    }

    public function editStockVersion()
    {
        $this->load->language('sale_channel/shopee_upload');
        $this->document->setTitle($this->language->get('heading_title_edit'));
        $this->load->model('catalog/shopee_upload');

        $arr_product_version_id = isset($this->request->post['product_version_id']) && is_array($this->request->post['product_version_id']) ? $this->request->post['product_version_id'] : [];
        $arr_product_version_stock = isset($this->request->post['stock_version']) && is_array($this->request->post['stock_version']) ? $this->request->post['stock_version'] : [];
        $status = isset($this->request->post['status']) ? $this->request->post['status'] : 0;
        $product_id = isset($this->request->post['product_id']) ? $this->request->post['product_id'] : '';

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateEditStockVersion()) {
            foreach ($arr_product_version_id as $key => $pv_id) {
                $this->model_catalog_shopee_upload->editStockVersion($product_id, $pv_id, (float)str_replace(',', '', $arr_product_version_stock[$key]));
            }

            $this->response->redirect($this->url->link('sale_channel/shopee_upload', 'user_token=' . $this->session->data['user_token'] . '&status_tab=' . $status, true));
        }

        $this->getForm();
    }

    public function mapBrand()
    {
        $this->load->language('sale_channel/shopee_upload');
        $this->document->setTitle($this->language->get('heading_title_edit'));
        $this->load->model('catalog/shopee_upload');

        $product_id = isset($this->request->post['product_id']) ? $this->request->post['product_id'] : '';
        $category_id = isset($this->request->post['category_id']) ? $this->request->post['category_id'] : '';
        $status = isset($this->request->post['status']) ? $this->request->post['status'] : 0;

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validatemapBrand()) {
            $this->model_catalog_shopee_upload->editBrand($product_id, $category_id);

            $this->session->data['success'] = $this->language->get('text_assign_category_success');
        }

        $this->response->redirect($this->url->link('sale_channel/shopee_upload', 'user_token=' . $this->session->data['user_token'] . '&status_tab=' . $status, true));
    }

    public function mapMultiBrand()
    {
        $this->load->language('sale_channel/shopee_upload');
        $this->document->setTitle($this->language->get('heading_title_edit'));
        $this->load->model('catalog/shopee_upload');

        $product_ids = isset($this->request->get['product_ids']) ? $this->request->get['product_ids'] : '';
        $category_id = isset($this->request->get['category_id']) ? $this->request->get['category_id'] : '';
        $status = isset($this->request->get['status']) ? $this->request->get['status'] : 0;

        $data = ['status' => $status];

        if ($this->validatemapMultiBrand()) {
            if ($product_ids == 'all') {
                $this->model_catalog_shopee_upload->setBrand($data, $category_id);
            } else {
                $this->model_catalog_shopee_upload->updateBrand($product_ids, $category_id);
            }

            $this->session->data['success'] = $this->language->get('text_assign_category_success');
        }

        $this->response->redirect($this->url->link('sale_channel/shopee_upload', 'user_token=' . $this->session->data['user_token'] . '&status_tab=' . $status, true));
    }

    public function attachPackageSize()
    {
        $this->load->language('sale_channel/shopee_upload');
        $this->document->setTitle($this->language->get('heading_title_edit'));
        $this->load->model('catalog/shopee_upload');

        $product_ids = isset($this->request->post['product_ids']) ? $this->request->post['product_ids'] : '';
        $weight = isset($this->request->post['weight']) ? (float)str_replace(',', '', $this->request->post['weight']) : 10;
        $length = isset($this->request->post['length']) ? (float)str_replace(',', '', $this->request->post['length']) : 1;
        $width = isset($this->request->post['width']) ? (float)str_replace(',', '', $this->request->post['width']) : 1;
        $height = isset($this->request->post['height']) ? (float)str_replace(',', '', $this->request->post['height']) : 1;
        $status = isset($this->request->post['status']) ? $this->request->post['status'] : '';

        $data = ['status' => $status];


        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateAttachPackageSize()) {
            if ($product_ids == 'all') {
                $this->model_catalog_shopee_upload->setPackageSize($data, $weight, $length, $width, $height);
            } else {
                $this->model_catalog_shopee_upload->updatePackageSize($product_ids, $weight, $length, $width, $height);
            }

            $this->session->data['success'] = $this->language->get('text_shipping_unit_setting_success');
        }

        $this->response->redirect($this->url->link('sale_channel/shopee_upload', 'user_token=' . $this->session->data['user_token'] . '&status_tab=' . $status, true));
    }

    public function shippingUnitSettings()
    {
        $this->load->language('sale_channel/shopee_upload');
        $this->document->setTitle($this->language->get('heading_title_edit'));
        $this->load->model('catalog/shopee_upload');

        $product_ids = isset($this->request->post['product_ids']) ? $this->request->post['product_ids'] : '';
        $logistic_ids = isset($this->request->post['logistic_ids']) ? $this->request->post['logistic_ids'] : '';
        $status = isset($this->request->post['status']) ? $this->request->post['status'] : '';

        $arr_product_ids = explode(',', $product_ids);
        $arr_logistic_ids = explode(',', $logistic_ids);

        if (!is_array($arr_product_ids) || !is_array($arr_logistic_ids) || $status == '') {
            return;
        }

        $data = ['status' => $status];

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateShippingUnitSettings()) {
            if ($product_ids == 'all') {
                $this->model_catalog_shopee_upload->allProductLogistics($data, $arr_logistic_ids);
            } else {
                foreach ($arr_product_ids as $product_id) {
                    $this->model_catalog_shopee_upload->insertLogistics($product_id, $arr_logistic_ids);
                }
            }

            $this->session->data['success'] = $this->language->get('text_unit_shipping_is_installed_success');
        }

        $this->response->redirect($this->url->link('sale_channel/shopee_upload', 'user_token=' . $this->session->data['user_token'] . '&status_tab=' . $status, true));
    }

    public function delete()
    {
        $this->load->language('sale_channel/shopee_upload');
        $this->document->setTitle($this->language->get('heading_title_edit'));
        $this->load->model('catalog/shopee_upload');

        $product_ids = isset($this->request->get['product_ids']) ? $this->request->get['product_ids'] : '';
        $status = isset($this->request->get['status']) ? $this->request->get['status'] : '';

        $data = ['status' => $status];

        if ($this->validateDeleteProduct()) {
            if ($product_ids == 'all') {
                $this->model_catalog_shopee_upload->deleteProduct($data);
            } else {
                $arr_p_id_pass = [];
                $product_ids = explode(',', $product_ids);
                foreach ($product_ids as $p_id) {
                    if (!$this->model_catalog_shopee_upload->checkProductInProcessUpload($p_id)) {
                        $arr_p_id_pass[] = $p_id;
                    }
                }

                $arr_p_id_pass = implode(',', $arr_p_id_pass);
                $this->model_catalog_shopee_upload->deleteProductByIds($arr_p_id_pass);
            }

            $this->session->data['success'] = $this->language->get('text_success_delete');
        }

        $this->response->redirect($this->url->link('sale_channel/shopee_upload', 'user_token=' . $this->session->data['user_token'] . '&status_tab=' . $status, true));
    }

    public function selectedProductBestme()
    {
        $this->load->language('sale_channel/shopee_upload');
        $this->document->setTitle($this->language->get('heading_title_edit'));
        $this->load->model('catalog/shopee_upload');

        $json = [];
        $product_ids = isset($this->request->get['product_ids']) ? $this->request->get['product_ids'] : '';
        $status = isset($this->request->get['status']) ? $this->request->get['status'] : '';

        $arr_product_ids = explode(',', $product_ids);

        if ($this->validateSelectedProductBestme()) {
            if ($product_ids == 'all') {
                $results = $this->model_catalog_shopee_upload->getProductPaginator([]);

                foreach ($results as $key => $value) {
                    if (!isset($value['product_id'])) {
                        continue;
                    }

                    $do_not_select = $this->model_catalog_shopee_upload->checkProductDoSelect($value['product_id']);
                    if ($do_not_select) {
                        continue;
                    }

                    $data_product = $this->getProductBestme($value['product_id']);
                    $shopee_upload_product_id = $this->model_catalog_shopee_upload->getShopeeUploadProductIdByBestmeProductId($value['product_id']);

                    if ($shopee_upload_product_id) {
                        $this->model_catalog_shopee_upload->editShopeeProduct($data_product, $shopee_upload_product_id, true);
                    } else {
                        $this->model_catalog_shopee_upload->addShopeeProduct($data_product);
                    }
                }
            } else {
                foreach ($arr_product_ids as $product_id) {
                    if (!$product_id) {
                        continue;
                    }

                    $data_product = $this->getProductBestme($product_id);
                    $shopee_upload_product_id = $this->model_catalog_shopee_upload->getShopeeUploadProductIdByBestmeProductId($product_id);

                    if ($shopee_upload_product_id) {
                        $this->model_catalog_shopee_upload->editShopeeProduct($data_product, $shopee_upload_product_id, true);
                    } else {
                        $this->model_catalog_shopee_upload->addShopeeProduct($data_product);
                    }
                }
            }

            $this->session->data['success'] = $this->language->get('text_success_add');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getProductBestme($product_id)
    {
        $this->load->model('catalog/product');

        $quanity = 0;
        // Images
        $images = [];

        // Attributes
        $product_attributes = [];

        // Version
        $product_version_name = [];
        $product_prices = [];
        $product_quantities = [];
        $product_skus = [];

        $product_info = $this->model_catalog_product->getProduct($product_id);
        if (isset($product_info['multi_versions']) && $product_info['multi_versions'] == 0) {
            // $quanity = $this->model_catalog_product->getQuantityForProductByStore($product_info['default_store_id'], $product_id);
            $quanity = $this->model_catalog_shopee_upload->getAllQuantityOfProductById($product_id);
        }

        // Images
        if (isset($product_info['image']) && !empty($product_info['image'])) {
            $images[] = $product_info['image'];
        }

        $product_images = $this->model_catalog_product->getProductImages($product_id);
        foreach ($product_images as $image) {
            if (isset($image['image']) && !empty($image['image'])) {
                $images[] = $image['image'];
            }
        }

        // has version
        if (isset($product_info['multi_versions']) && $product_info['multi_versions']) {
            // Attributes
            $this->load->model('catalog/attribute');
            $product_attributes_sql = $this->model_catalog_product->getProductAttributes($product_id);

            foreach ($product_attributes_sql as $product_attribute) {
                $attribute_info = $this->model_catalog_attribute->getAttribute($product_attribute['attribute_id']);
                if ($attribute_info) {
                    $product_attributes[] = array(
                        'name' => $attribute_info['name'],
                        'values' => explode(",", $product_attribute['product_attribute_description']['text'])
                    );
                }
            }

            $product_attributes = ($this->checkProductAttributes($product_attributes)) ? $product_attributes : [];

            if (!empty($product_attributes)) {
                // Version
                $product_versions = $this->model_catalog_product->getProductVersions($product_id);
                foreach ($product_versions as $version) {
                    $version['product_to_stores'] = $this->model_catalog_product->getProductToStores($product_id, $version['product_version_id']);
                    $version['quantity_total'] = 0;
                    foreach ($version['product_to_stores'] as $key => &$item) {
                        if ($item['product_version_id'] == $version['product_version_id']) {
                            $version['quantity_total'] += $item['quantity'];
                            $item['product_on_delivery'] = $this->model_catalog_product->getProductDelivery($item['store_id'], $item['product_id'], $item['product_version_id']);
                        }

                        unset($version['product_to_stores'][$key]);
                        array_unshift($version['product_to_stores'], $item);
                    }
                    unset($item);

                    $product_version_name[] = $version['version'];
                    $product_prices[] = $version['compare_price'];
                    $product_quantities[] = $version['quantity_total'];
                    $product_skus[] = $version['sku'];
                }
            }
        }

        $product = [
            'bestme_product_id' => isset($product_id) ? $product_id : '',
            'name' => isset($product_info['name']) ? $product_info['name'] : '',
            'weight' => isset($product_info['weight']) ? (float)$product_info['weight'] : 0,
            'length' => isset($product_info['length']) ? (float)$product_info['length'] : 0,
            'width' => isset($product_info['width']) ? (float)$product_info['width'] : 0,
            'height' => isset($product_info['height']) ? (float)$product_info['height'] : 0,
            'images' => $images,
            'description' => isset($product_info['description']) ? strip_tags(html_entity_decode($product_info['description'])) : '',
            'price' => isset($product_info['compare_price']) ? (float)$product_info['compare_price'] : 0,
            'quantity' => $quanity,
            'sku' => isset($product_info['sku']) ? $product_info['sku'] : '',
            'product_attributes' => (is_array($product_attributes) && count($product_attributes) > 0) ? json_encode($product_attributes) : '',
            'product_version_names' => is_array($product_version_name) ? $product_version_name : [],
            'product_prices' => is_array($product_prices) ? $product_prices : [],
            'product_quantities' => is_array($product_quantities) ? $product_quantities : [],
            'product_skus' => is_array($product_skus) ? $product_skus : [],
        ];

        return $product;
    }

    private function checkProductAttributes(array $attributes)
    {
        if (!is_array($attributes)) {
            return false;
        }

        if (count($attributes) > 2 || count($attributes) == 0) {
            return false;
        }

        $arr1 = isset($attributes[0]['values']) ? $attributes[0]['values'] : [];
        $arr2 = isset($attributes[1]['values']) ? $attributes[1]['values'] : [];

        if (count($arr1) > 20 || count($arr2) > 20) {
            return false;
        }

        $count = count($arr1) * count($arr2);

        if ($count > 50) {
            return false;
        }

        return true;
    }

    private function validateEditPrice()
    {
        if (!$this->user->hasPermission('access', 'sale_channel/shopee')) {
            $this->session->data['error'] = $this->language->get('error_permission');
            return false;
        }

        if (!isset($this->request->post['product_id'])) {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        $product_id = isset($this->request->post['product_id']) ? $this->request->post['product_id'] : '';

        if ($product_id == '') {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        return true;
    }

    public function tetUp()
    {
        var_dump($_POST);die;
        /* push job to Redis. Notice: use shop_name to define unique job... */
        $redis = null;

        try {

            /* update process status */
            $shop_name = $this->config->get('shop_name');
            //$this->updateSyncOrderProcessingStatusRunning($shopee_id, $shop_name);

            /* create job */
            $job = [
                self::CJ_JOB_KEY_DB_HOSTNAME => DB_HOSTNAME,
                self::CJ_JOB_KEY_DB_PORT => DB_PORT,
                self::CJ_JOB_KEY_DB_USERNAME => DB_USERNAME,
                self::CJ_JOB_KEY_DB_PASSWORD => DB_PASSWORD,
                self::CJ_JOB_KEY_DB_DATABASE => DB_DATABASE,
                self::CJ_JOB_KEY_DB_PREFIX => DB_PREFIX,
                self::CJ_JOB_KEY_DB_DRIVER => DB_DRIVER,
                self::CJ_JOB_KEY_SHOP_NAME => $shop_name,
                self::CJ_JOB_KEY_SHOPEE_ID => 205134,
                'product_ids' => [1, 2, 3],
                self::CJ_JOB_KEY_TASK => 'UPLOAD_PRODUCTS'
            ];

            $redis = $this->redisConnect();
            $redis->lPush(SHOPEE_UPLOAD_JOB_REDIS_QUEUE, json_encode($job));
        } catch (Exception $e) {
            // could not connect to redis
            $this->log->write('[controller/sale_channel/shopee] loadShopeeOrders(): could not connect to redis. Error: ' . $e->getMessage());
        } finally {
            $this->redisClose($redis);
        }
    }

    public function uploadProducts()
    {
        if (!isset($this->request->post['shops']) || !isset($this->request->post['ids'])) {
            return;
        }

        /* push job to Redis. Notice: use shop_name to define unique job... */
        $redis = null;

        try {
            $shops = $this->request->post['shops'];
            if (!is_array($shops) || empty($shops)) {
                return;
            }
            $ids = $this->request->post['ids'];
            if (!$ids){
                return;
            }
            $this->load->model('catalog/shopee_upload');

            $status = $this->request->post['status'];
            if ($ids == 'all') {
                if (!isset($this->request->post['status'])){
                    return;
                }
                $ids = $this->model_catalog_shopee_upload->getProductIdsByStatus($status);
            }

            //

            /* update process status */
            $shop_name = $this->config->get('shop_name');
            $redis = $this->redisConnect();
            foreach ($shops as $shop){
                $this->model_catalog_shopee_upload->updateProductInProcess($shop, $ids, $status);
                $this->updateProcessingStatusRunning($shop_name, $shop);

                /* create job */
                $job = [
                    self::CJ_JOB_KEY_DB_HOSTNAME => DB_HOSTNAME,
                    self::CJ_JOB_KEY_DB_PORT => DB_PORT,
                    self::CJ_JOB_KEY_DB_USERNAME => DB_USERNAME,
                    self::CJ_JOB_KEY_DB_PASSWORD => DB_PASSWORD,
                    self::CJ_JOB_KEY_DB_DATABASE => DB_DATABASE,
                    self::CJ_JOB_KEY_DB_PREFIX => DB_PREFIX,
                    self::CJ_JOB_KEY_DB_DRIVER => DB_DRIVER,
                    self::CJ_JOB_KEY_SHOP_NAME => $shop_name,
                    self::CJ_JOB_KEY_SHOPEE_ID => $shop,
                    self::CJ_JOB_KEY_SHOPEE_UPLOAD_PRODUCT_IDS => $ids,
                    self::CJ_JOB_KEY_TASK => self::CJ_JOB_TASK_UPLOAD_PRODUCTS
                ];

                $redis->lPush(SHOPEE_UPLOAD_JOB_REDIS_QUEUE, json_encode($job));
            }
        } catch (Exception $e) {
            // could not connect to redis
            $this->log->write('[controller/sale_channel/shopee] loadShopeeProducts(): could not connect to redis. Error: ' . $e->getMessage());
        } finally {
            $this->redisClose($redis);
        }
    }

    private function validateEditPriceVersion()
    {
        if (!$this->user->hasPermission('access', 'sale_channel/shopee')) {
            $this->session->data['error'] = $this->language->get('error_permission');
            return false;
        }

        if (!isset($this->request->post['product_id'])) {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        $product_id = isset($this->request->post['product_id']) ? $this->request->post['product_id'] : '';

        if ($product_id == '') {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        if (!isset($this->request->post['product_version_id'])) {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        if (!isset($this->request->post['price_version'])) {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        return true;
    }

    private function validateEditStock()
    {
        if (!$this->user->hasPermission('access', 'sale_channel/shopee')) {
            $this->session->data['error'] = $this->language->get('error_permission');
            return false;
        }

        if (!isset($this->request->post['product_id'])) {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        $product_id = isset($this->request->post['product_id']) ? $this->request->post['product_id'] : '';

        if ($product_id == '') {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        return true;
    }

    private function validateEditStockVersion()
    {
        if (!$this->user->hasPermission('access', 'sale_channel/shopee')) {
            $this->session->data['error'] = $this->language->get('error_permission');
            return false;
        }

        if (!isset($this->request->post['product_id'])) {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        $product_id = isset($this->request->post['product_id']) ? $this->request->post['product_id'] : '';

        if ($product_id == '') {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        if (!isset($this->request->post['product_version_id'])) {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        if (!isset($this->request->post['stock_version'])) {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        return true;
    }

    private function validateMapBrand()
    {
        if (!$this->user->hasPermission('access', 'sale_channel/shopee')) {
            $this->session->data['error'] = $this->language->get('error_permission');
            return false;
        }

        if (!isset($this->request->post['product_id'])) {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        $product_id = isset($this->request->post['product_id']) ? $this->request->post['product_id'] : '';

        if ($product_id == '') {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        if (!isset($this->request->post['category_id'])) {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        return true;
    }

    private function validateMapMultiBrand()
    {
        if (!$this->user->hasPermission('access', 'sale_channel/shopee')) {
            $this->session->data['error'] = $this->language->get('error_permission');
            return false;
        }

        if (!isset($this->request->get['product_ids'])) {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        $product_ids = isset($this->request->get['product_ids']) ? $this->request->get['product_ids'] : '';

        if ($product_ids == '') {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        if (!isset($this->request->get['category_id'])) {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        return true;
    }

    private function validateAttachPackageSize()
    {
        if (!$this->user->hasPermission('access', 'sale_channel/shopee')) {
            $this->session->data['error'] = $this->language->get('error_permission');
            return false;
        }

        if (!isset($this->request->post['product_ids'])) {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        return true;
    }

    private function validateShippingUnitSettings()
    {
        if (!$this->user->hasPermission('access', 'sale_channel/shopee')) {
            $this->session->data['error'] = $this->language->get('error_permission');
            return false;
        }

        if (!isset($this->request->post['product_ids'])) {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        return true;
    }

    private function validateDeleteProduct()
    {
        if (!$this->user->hasPermission('access', 'sale_channel/shopee')) {
            $this->session->data['error'] = $this->language->get('error_permission');
            return false;
        }

        if (!isset($this->request->get['product_ids'])) {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        return true;
    }

    private function validateSelectedProductBestme()
    {
        if (!$this->user->hasPermission('access', 'sale_channel/shopee')) {
            $this->session->data['error'] = $this->language->get('error_permission');
            return false;
        }

        if (!isset($this->request->get['product_ids'])) {
            $this->session->data['error'] = $this->language->get('error_not_enough_data');
            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    private function getCurrentLanguageId()
    {
        $this->load->model('localisation/language');
        $language = $this->model_localisation_language->getLanguageByCode($this->config->get('config_language'));

        return isset($language['language_id']) && $language['language_id'] == 2 ? 'vi' : 'en'; // default 'vn'
    }

    /**
     * @return null|Redis
     */
    private function redisConnect()
    {
        $redis = null;

        try {
            $redis = new \Redis();
            $redis->connect(MUL_REDIS_HOST, MUL_REDIS_PORT);
            $redis->select(MUL_REDIS_DB_SHOPEE);
        } catch (Exception $e) {
            // could not connect to redis
        }

        return $redis;
    }

    /**
     * @param Redis $redis
     */
    private function redisClose($redis)
    {
        if (!$redis instanceof \Redis) {
            return;
        }

        try {
            $redis->close();
        } catch (Exception $e) {
            // could not connect to redis
        }
    }

    private function updateProcessingStatusRunning($shop_name, $shopee_id)
    {
        $redis = $this->redisConnect();

        $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'load_shopee_upload_product_running');
        $redis->set($key, 1, SHOPEE_PROCESS_INFO_TIMEOUT);

        $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'count_product_upload_shopee');
        $redis->set($key, 0, SHOPEE_PROCESS_INFO_TIMEOUT);

        $key = sprintf('%s%s_%s_%s', SHOPEE_PROCESS_INFO_PREFIX, $shop_name, $shopee_id, 'count_product_shopee_uploaded');
        $redis->set($key, 0, SHOPEE_PROCESS_INFO_TIMEOUT);

        $this->redisClose($redis);
    }
}