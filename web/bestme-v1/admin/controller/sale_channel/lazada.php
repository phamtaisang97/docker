<?php

class ControllerSaleChannelLazada extends Controller
{
    private $error = array();

    // redis job key
    const CJ_JOB_KEY_DB_HOSTNAME = 'db_hostname';
    const CJ_JOB_KEY_DB_PORT = 'db_port';
    const CJ_JOB_KEY_DB_USERNAME = 'db_username';
    const CJ_JOB_KEY_DB_PASSWORD = 'db_password';
    const CJ_JOB_KEY_DB_DATABASE = 'db_database';
    const CJ_JOB_KEY_DB_PREFIX = 'db_prefix';
    const CJ_JOB_KEY_DB_DRIVER = 'db_driver';
    const CJ_JOB_KEY_SHOP_NAME = 'shop_name';
    const CJ_JOB_KEY_SHOP_ID = 'shop_id'; // id in table config
    const CJ_JOB_KEY_ACCESS_TOKEN = 'access_token';
    const CJ_JOB_KEY_TASK = 'task';
    // all tasks
    const CJ_JOB_TASK_GET_PRODUCTS = 'GET_PRODUCTS';
    const CJ_JOB_TASK_GET_ORDERS = 'GET_ORDERS';
    const CJ_JOB_KEY_SYNC_NEXT_TIME = 'sync_next_time';
    const CJ_JOB_KEY_SYNC_INTERVAL  = 'sync_interval';
    const CJ_JOB_KEY_SYNC_TYPE  = 'sync_type'; // auto or manual
    const CJ_UPDATE_JOB_TASK_GET_ORDERS_PREFIX = 'UPDATE_LAZADA_JOB_GET_ORDERS_';
    const CJ_UPDATE_JOB_KEY_TYPE = 'type';
    const CJ_UPDATE_JOB_TYPE_UPDATE = 'update';
    const CJ_UPDATE_JOB_TYPE_REMOVE = 'remove';
    const CJ_UPDATE_JOB_KEY_sync_interval = 'sync_interval';

    public function products()
    {
        $this->load->language('sale_channel/lazada');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->addStyle('view/stylesheet/custom/shopee.css');
        $this->load->model('catalog/lazada');
        $this->load->model('catalog/product');
        $this->getList();
    }

    public function config()
    {
        $this->load->language('sale_channel/lazada');
        $this->load->model('catalog/lazada');

        $this->document->setTitle($this->language->get('heading_title'));
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];
            $this->session->data['error'] = '';
            unset($this->session->data['error']);
        } else {
            $data['error_warning'] = '';
        }

        $redirect_url = $this->url->link('sale_channel/lazada/updateShopConfig&user_token=' . $this->session->data['user_token'], '', true);
        $data['auth_url'] = \Sale_Channel\Lazada::getAuthLink($redirect_url);

        $data['sync_interval_test_value'] = defined('LAZADA_SYNC_INTERVAL_TEST_VALUE') ? (int)LAZADA_SYNC_INTERVAL_TEST_VALUE : '';
        $shops = $this->model_catalog_lazada->getListLazadaShops();
        $data['shops'] = $shops;

        $data['disconnect_shop'] = $this->url->link('sale_channel/lazada/disconnectShopLazada', 'user_token=' . $this->session->data['user_token'], true);
        $data['save_time_config'] = $this->url->link('sale_channel/lazada/saveTimeSyncConfig', 'user_token=' . $this->session->data['user_token'], true);
        $data['save_lazada_shop_config'] = $this->url->link('sale_channel/lazada/saveLazadaShopConfig', 'user_token=' . $this->session->data['user_token'], true);

        $this->response->setOutput($this->load->view('sale_channel/lazada_config', $data));
    }

    public function saveLazadaShopConfig()
    {
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateSaveLazadaShopConfig()) {
            $this->load->model('catalog/lazada');

            $order_sync_range = isset($this->request->post['order_sync_range']) ? (int)$this->request->post['order_sync_range'] : 15;
            $allow_update_stock_product = isset($this->request->post['allow_update_stock_product']) ? (int)$this->request->post['allow_update_stock_product'] : 0;
            $allow_update_price_product = isset($this->request->post['allow_update_price_product']) ? (int)$this->request->post['allow_update_price_product'] : 0;

            $this->model_catalog_lazada->saveLazadaShopConfig($this->request->post['id'], $order_sync_range, $allow_update_stock_product, $allow_update_price_product);
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($this->error));
    }

    public function orders()
    {
        $this->load->language('sale_channel/lazada');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->addStyle('view/stylesheet/custom/shopee.css');
        $this->load->model('catalog/lazada');
        $this->load->model('catalog/product');
        $this->getListOrders();
    }

    public function updateShopConfig()
    {
        if ($this->validateShopConfig()){
            $code = $this->request->get['code'];
            $accesstoken = \Sale_Channel\Lazada::getAccessToken($code);
            $accesstoken = json_decode($accesstoken, true);
            if (!array_key_exists('access_token', $accesstoken) || !array_key_exists('refresh_token', $accesstoken)){
                $this->session->data['error'] = $this->language->get('warning_connect_shop_fails');
            }
            $data['access_token'] = $accesstoken['access_token'];
            $expires_in = gmdate("Y-m-d H:i:s", (int)$accesstoken['expires_in'] + time());
            $data['expires_in'] = $expires_in;
            $data['refresh_token'] = $accesstoken['refresh_token'];
            $refresh_expires_in = gmdate("Y-m-d H:i:s", (int)$accesstoken['refresh_expires_in'] + time());
            $data['refresh_expires_in'] = $refresh_expires_in;
            $data['connected'] = 1;
            $data['country'] = isset($accesstoken['country_user_info'][0]['country']) ? $accesstoken['country_user_info'][0]['country'] : 'vn';

            $lazada = Sale_Channel::getSaleChannel('lazada', ['access_token' => $data['access_token']]);
            $shopInfo = $lazada->getShopInfo();
            $shopInfo = json_decode($shopInfo, true);
            $shopInfo = isset($shopInfo['data']) ? $shopInfo['data'] : [];
            $data['seller_id']= isset($shopInfo['seller_id']) ? $shopInfo['seller_id'] : '';
            $data['shop_name']= isset($shopInfo['name']) ? $shopInfo['name'] : '';

            $this->load->model('catalog/lazada');
            $shopIsConnected = $this->model_catalog_lazada->shopIsConnected($data);
            $shop_id = $this->model_catalog_lazada->updateLazadaShopConfig($data);
            if (!$shopIsConnected && $shop_id){
                // add new job
                $this->loadLazadaOrders($shop_id, $this->model_catalog_lazada->getDefaultSyncInterval(), null, 'auto');
            }

            $this->getLazadaCategoryTree($lazada);

            $this->session->data['success'] = $this->language->get('create_shop_lazada_success');
            $this->response->redirect($this->url->link('sale_channel/lazada/config', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->config();
    }

    private function getLazadaCategoryTree($lazada)
    {
        $data = $lazada->getCategoryTree();
        $data = json_decode($data, true);
        if (is_array($data) && array_key_exists('data', $data)){
            if (!is_array($data['data']) || empty($data['data'])) {
                return;
            }
            $this->model_catalog_lazada->truncateLazadaCategoryTree();
            $this->model_catalog_lazada->updateLazadaCategoryTree($data['data']);
            if ($this->model_catalog_lazada->countCategoryDescription() == 0){
                $file_path = DIR_SYSTEM . 'library/sale_channel/lazada_category_tree_vn.json';
                if (file_exists($file_path)){
                    $names = file_get_contents($file_path);
                    $names = json_decode($names, true);
                    if (!is_array($names)){
                        return;
                    }
                    foreach ($names as $name){
                        if (!array_key_exists('id', $name) || !array_key_exists('name', $name)){
                            continue;
                        }

                        $this->model_catalog_lazada->addCategoryDescription($name['id'], $name['name']);
                    }
                }
            }
        }
    }

    public function disconnectShopLazada()
    {
        $this->load->language('sale_channel/lazada');
        $this->load->model('catalog/lazada');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateShopId()) {
            $id = isset($this->request->post['id']) ? $this->request->post['id'] : '';

            if (!$id) {
                $this->session->data['error'] = $this->language->get('warning_validate_sync_config');
            }

            $this->model_catalog_lazada->disconnectShopLazadaById($id);
            // remove job
            $this->updateOrderJob($id, 0, self::CJ_UPDATE_JOB_TYPE_REMOVE);

            $this->session->data['success'] = $this->language->get('text_disconnect_success');
        }

        $json['code'] = 200;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function validateShopId()
    {
        $this->load->language('sale_channel/lazada');
        if (!$this->user->hasPermission('modify', 'sale_channel/lazada')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        if (!isset($this->request->post['id'])) {
            $this->error['warning'] = $this->language->get('warning_validate_sync_config');
        }

        return !$this->error;
    }

    private function validateShopConfig()
    {
        $this->load->language('sale_channel/lazada');
        if (!$this->user->hasPermission('modify', 'sale_channel/lazada')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        if (!isset($this->request->get['code'])) {
            $this->error['warning'] = $this->language->get('warning_validate_sync_config');
        }

        return !$this->error;
    }

    protected function getList()
    {
        $page = $this->getValueFromRequest('get', 'page', 1);
        $sync_status = isset($this->request->get['status_sync']) ? trim($this->request->get['status_sync']) : ModelCatalogLazada::SYNC_STATUS_ALL;

        $url = '';
        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        if (isset($this->session->data['error'])) {
            $data['error'] = $this->session->data['error'];
            unset($this->session->data['error']);
        } else {
            $data['error'] = '';
        }
        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        // breadcrumb
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('sale_channel/lazada/products', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );
        $data['user_token'] = $this->session->data['user_token'];

        $lazada_shops = $this->model_catalog_lazada->getListLazadaShops();
        $shop_selected = isset($this->request->get['shop_selected']) ? $this->request->get['shop_selected'] : (isset($lazada_shops[0]['id']) ? $lazada_shops[0]['id'] : null);
        $data['shop_selected'] = $shop_selected;

        if (!$shop_selected) {
            $this->response->redirect($this->url->link('sale_channel/lazada/config', 'user_token=' . $this->session->data['user_token'], true));
        }

        $last_sync_time_product = $this->model_catalog_lazada->getLastSyncTimeProductById($shop_selected);
        $data['last_sync_time_product'] = '--';

        if ($last_sync_time_product) {
            $data['last_sync_time_product'] = date("d/m/Y H:i:s", strtotime($last_sync_time_product));
        }

        foreach ($lazada_shops as &$shop){
            $shop['url'] = $this->url->link('sale_channel/lazada/products', 'user_token=' . $this->session->data['user_token'] . '&shop_selected=' . $shop['id'], true);
        }

        // filter
        $filter = [
            'filter_data' => isset($this->request->get['filter_data']) ? $this->request->get['filter_data'] : 0,
            'filter_name' => isset($this->request->get['filter_name']) ? trim($this->request->get['filter_name']) : '',
            'shop_id' => $shop_selected,
            'sync_status' => $sync_status,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        ];

        $products_lazada = $this->model_catalog_lazada->getProductLazadaList($filter);

        //$bestme_products = $this->model_catalog_lazada->getProductsForLazada();

        foreach ($products_lazada as &$product) {
            if (!isset($product['item_id']) || !isset($product['sync_status'])) {
                continue;
            }

            $product['versions'] = $this->model_catalog_lazada->getProductVersionLazadaList($product['item_id'], $sync_status);

            foreach ($product['versions'] as &$version) {
                if (!isset($version['bestme_product_id']) || !isset($version['bestme_product_version_id'])) {
                    continue;
                }

                $product_info = $this->model_catalog_lazada->getProductInfoById($version['bestme_product_id'], $version['bestme_product_version_id']);

                $version['product_name'] = isset($product_info['name']) ? $product_info['name'] : '';
                $version['product_price'] = isset($product_info['price']) ? $product_info['price'] : '';
                $version['product_quantity'] = isset($product_info['quantity']) ? $product_info['quantity'] : '';
                $version['product_edit'] = isset($product_info['edit']) ? $product_info['edit'] : '';
            }
        }

        $product_total = $this->model_catalog_lazada->getTotalProductLazadaList($filter);

        // shop expired is show model view
        $data['shop_expired'] = $this->model_catalog_lazada->shopLazadaExpired($shop_selected);

        $data['shops'] = $lazada_shops;
        $data['products'] = $products_lazada;
        //$data['bestme_products'] = $bestme_products;
        $data['filter'] = $filter;

        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $product_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('sale_channel/lazada/products', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

        // Add goto page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);
        $data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));

        // sync href
        $data['sync_state_href'] = $this->url->link('sale_channel/lazada/getSyncState&user_token=' . $this->session->data['user_token'], '', true);
        $data['sync_product_href'] = $this->url->link('sale_channel/lazada/loadLazadaProducts&user_token=' . $this->session->data['user_token'], '', true);

        $data['create_product_href'] = $this->url->link('sale_channel/lazada/createProduct&user_token=' . $this->session->data['user_token'], '', true);
        $data['sync_all_product_href'] = $this->url->link('sale_channel/lazada/sync&user_token=' . $this->session->data['user_token'], '', true);

        // data load header
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');

        $data['url_filter'] = $this->url->link('sale_channel/lazada/products', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['go_to_config'] = $this->url->link('sale_channel/lazada/config', 'user_token=' . $this->session->data['user_token'], true);
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('sale_channel/lazada_products_append', $data));
        } else {
            $this->response->setOutput($this->load->view('sale_channel/lazada_products', $data));
        }
    }

    protected function getListOrders()
    {
        $page = $this->getValueFromRequest('get', 'page', 1);
        $sync_status = isset($this->request->get['status_sync']) ? trim($this->request->get['status_sync']) : ModelCatalogLazada::SYNC_STATUS_ALL;

        $url = '';
        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        if (isset($this->session->data['error'])) {
            $data['error'] = $this->session->data['error'];
            unset($this->session->data['error']);
        } else {
            $data['error'] = '';
        }
        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        // breadcrumb
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('sale_channel/lazada/orders', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );
        $data['user_token'] = $this->session->data['user_token'];

        $lazada_shops = $this->model_catalog_lazada->getListLazadaShops();
        $shop_selected = (isset($this->request->get['shop_selected']) && $this->request->get['shop_selected'] != '') ? $this->request->get['shop_selected'] : (isset($lazada_shops[0]['id']) ? $lazada_shops[0]['id'] : null);
        $data['shop_selected'] = $shop_selected;

        if (!$shop_selected) {
            $this->response->redirect($this->url->link('sale_channel/lazada/config', 'user_token=' . $this->session->data['user_token'], true));
        }

        $last_sync_time_order = $this->model_catalog_lazada->getLastSyncTimeOrderById($shop_selected);
        $data['last_sync_time_order'] = '--';

        if ($last_sync_time_order) {
            $data['last_sync_time_order'] = date("d/m/Y H:i:s", strtotime($last_sync_time_order));
        }

        foreach ($lazada_shops as &$shop){
            $shop['url'] = $this->url->link('sale_channel/lazada/orders', 'user_token=' . $this->session->data['user_token'] . '&shop_selected=' . $shop['id'], true);
        }

        // filter
        $filter = [
            'filter_data' => isset($this->request->get['filter_data']) ? $this->request->get['filter_data'] : 0,
            'filter_name' => isset($this->request->get['filter_name']) ? trim($this->request->get['filter_name']) : '',
            'shop_id' => $shop_selected,
            'sync_status' => $sync_status,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        ];

        $order_lazada = $this->model_catalog_lazada->getOrderSyncList($filter);

        foreach ($order_lazada as &$order){
            if ($order['bestme_order_id']) {
                $order['url'] = $this->url->link('sale/order/detail', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . (int)$order['bestme_order_id'], true);
            } else {
                $order['url'] = 'javascript:;';
            }
        }

        $order_total = $this->model_catalog_lazada->getTotalOrderSyncList($filter);

        foreach ($order_lazada as &$order) {
            $order['order_status'] = $this->orderStatusLazadaDetail($order['order_status']);
            $order['sync_status_text'] = $this->orderSyncStatusDetail($order['sync_status']);
            $order['sync_status_error'] = $this->orderErrorSyncStatusDetail($order['sync_status']);
            $order['sync_status_solution'] = $this->orderSolutionSyncStatusDetail($order['sync_status']);
        }

        // shop expired is show model view
        $data['shop_expired'] = $this->model_catalog_lazada->shopLazadaExpired($shop_selected);

        $data['orders'] = $order_lazada;
        $data['shops'] = $lazada_shops;
        $data['filter'] = $filter;

        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $order_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('sale_channel/lazada/orders', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

        // Add goto page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);
        $data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));

        // sync href
        //$data['sync_state_href'] = $this->url->link('sale_channel/lazada/getSyncState&user_token=' . $this->session->data['user_token'], '', true);
        //$data['sync_product_href'] = $this->url->link('sale_channel/lazada/loadLazadaProducts&user_token=' . $this->session->data['user_token'], '', true);

        //$data['create_product_href'] = $this->url->link('sale_channel/lazada/createProduct&user_token=' . $this->session->data['user_token'], '', true);

        // data load header
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['sync_order_href'] = $this->url->link('sale_channel/lazada/loadLazadaOrdersManual&user_token=' . $this->session->data['user_token'], '', true);
        $data['sync_state_href'] = $this->url->link('sale_channel/lazada/getSyncOrdersState&user_token=' . $this->session->data['user_token'], '', true);
        $data['sync_href'] = $this->url->link('sale_channel/lazada/syncOrders', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['url_filter'] = $this->url->link('sale_channel/lazada/orders', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['go_to_config'] = $this->url->link('sale_channel/lazada/config', 'user_token=' . $this->session->data['user_token'], true);
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('sale_channel/lazada_orders_append', $data));
        } else {
            $this->response->setOutput($this->load->view('sale_channel/lazada_orders', $data));
        }
    }

    public function syncOrders()
    {
        ini_set('max_execution_time', 300);
        set_time_limit(300);
        $this->load->language('sale_channel/lazada');
        $json = [
            'status' => false,
            'msg' => $this->language->get('warning_sync_fail'),
        ];
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateSyncOrders()) {
            $this->load->model('catalog/lazada');
            $id = $this->request->post['id'];
            $order_ids = [];
            if ($id == 'all') {
                $shop_id = isset($this->request->post['shop_id']) ? (int)$this->request->post['shop_id'] : 0;
                if (!$shop_id) {
                    $order_ids = [];
                } else {
                    $order_ids = $this->model_catalog_lazada->getOrderIdByShop($shop_id);
                }
            } else {
                $order_id = $this->model_catalog_lazada->getOrderIdById((int)$id);
                if ($order_id) {
                    $order_ids[] = $order_id;
                }
            }
            $flag = false;
            if (is_array($order_ids)) {
                foreach ($order_ids as $order_id) {
                    if (!$order_id){
                        continue;
                    }

                    $result = $this->model_catalog_lazada->syncOrderToAdmin($order_id);
                    if (!$flag && $result) {
                        $flag = $result;
                    }
                }
            }
            if ($flag){
                $json['status'] = true;
                $json['msg'] = 'success!';
            }
        }

        if($json['status']){
            $this->session->data['success'] = $this->language->get('status_sync_success');
        }else{
            $this->session->data['error'] = $this->language->get('status_sync_error');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function validateSyncOrders()
    {
        $this->load->language('sale_channel/lazada');
        if (!$this->user->hasPermission('modify', 'sale_channel/lazada')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        if (!isset($this->request->post['id'])) {
            $this->error['warning'] = $this->language->get('warning_sync_fail');
        }else{
            if ($this->request->post['id'] == 'all' && !isset($this->request->post['shop_id'])){
                $this->error['warning'] = $this->language->get('warning_sync_fail');
            }
        }

        return !$this->error;
    }

    public function sync()
    {
        ini_set('max_execution_time', 300);
        set_time_limit(300);
        $this->load->language('sale_channel/lazada');
        $json = [
            'status' => false,
            'msg' => $this->language->get('warning_sync_fail'),
            'create' => 0,
            'update' => 0
        ];
        $count_is_create = 0;
        $count_is_update = 0;
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateSync()) {
            $this->load->model('catalog/lazada');
            $this->load->model('catalog/product');

            $ids = $this->request->post['ids'];

            $shop_id = isset($this->request->post['shop_id']) ? (int)$this->request->post['shop_id'] : 0;
            if ($ids == 'all') {
                if (!$shop_id) {
                    $ids = [];
                } else {
                    $ids = $this->model_catalog_lazada->getIds($shop_id);
                }
            }

            if (is_array($ids)) {
                foreach ($ids as $id) {
                    if (trim($id) == '') {
                        continue;
                    }

                    $product_info = $this->model_catalog_lazada->getProductById($id);
                    if (!is_array($product_info) || empty($product_info) || !array_key_exists('item_id', $product_info)) {
                        $this->session->data['error'] = $this->language->get('warning_validate_not_exits_prod');
                        break;
                    }

                    $versions_info = $this->model_catalog_lazada->getProductVersionByItemId($product_info['item_id']);
                    if (empty($versions_info)){
                        $this->session->data['error'] = $this->language->get('warning_validate_info_prod_not_complete');
                        break;
                    }

                    $product_has_link_to_admin = (isset($product_info['sync_status']) && $product_info['sync_status']) ? true : false;

                    if ($product_has_link_to_admin) {
                        foreach ($versions_info as $version) {
                            if (isset($version['bestme_product_id']) && !empty($version['bestme_product_id'])) {
                                $product_bestme_id = $version['bestme_product_id'];
                                $product_bestme_version_id = isset($version['bestme_product_version_id']) ? $version['bestme_product_version_id'] : 0;
                                $product_stock = isset($version['quantity']) ? $version['quantity'] : 0;
                                $product_price = isset($version['price']) ? $version['price'] : 0;
                                $product_original_price = isset($version['price']) ? $version['price'] : 0;

                                $this->model_catalog_lazada->updateProductHasLinkProductLazada((int)$shop_id, $product_bestme_id, $product_bestme_version_id, (int)$product_stock, (float)$product_price, (float)$product_original_price);

                                continue;
                            } else {
                                $this->createProductSingleVersion($version['id']);
                                $count_is_create++;
                                continue;
                            }
                        }
                    } else {
                        $this->createProductMultiVersion($id);
                        $count_is_create++;
                        continue;
                    }
                }
            }

            if ($count_is_update > 0 || $count_is_create > 0) {
                $json['status'] = true;
                $json['msg'] = 'success!';
                $json['create'] = $count_is_create;
                $json['update'] = $count_is_update;
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function validateSync()
    {
        $this->load->language('sale_channel/shopee');
        if (!$this->user->hasPermission('modify', 'sale_channel/shopee')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        if (!isset($this->request->post['ids'])) {
            $this->error['warning'] = $this->language->get('warning_sync_fail');
        } else {
            if ($this->request->post['ids'] == 'all' && !isset($this->request->post['shop_id'])) {
                $this->error['warning'] = $this->language->get('warning_sync_fail');
            }
        }

        return !$this->error;
    }

    public function getProductsLazyLoad()
    {
        $json = array();
        $this->load->language('sale_channel/lazada');
        $this->load->model('catalog/lazada');
        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        // display 6 product in select config theme is many, or setup other config
        $filter_data = array(
            'limit' => 6,
            'start' => ($page - 1) * 6,
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        $shop_id = isset($this->request->get['shop_selected']) ? $this->request->get['shop_selected'] : '';
        if ($shop_id == '') {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }

        $filter_data['pv_ids'] = isset($this->request->get['pv_ids']) ? $this->request->get['pv_ids'] : [];
        $product_mapping = $this->model_catalog_lazada->getProductMapping($shop_id);

        $mapping_ids = [];
        foreach ($product_mapping as $product) {
            $pvid = $product['bestme_product_id'];

            if (isset($product['bestme_product_version_id']) && $product['bestme_product_version_id']) {
                $pvid .= '-' . $product['bestme_product_version_id'];
            }

            $mapping_ids[] = $pvid;
        }

        $filter_data['pv_ids'] = array_merge($filter_data['pv_ids'], $mapping_ids);
        $filter_data['pv_ids'] = array_unique($filter_data['pv_ids'], 0);

        $results = $this->model_catalog_lazada->getProductsForLazada($filter_data);

        foreach ($results as $key => $value) {
            $version_name = implode(' • ', explode(',', $value['version']));
            if ($version_name != '') {
                $version_name = $this->language->get('txt_version') . $version_name;
            }

            $json['results'][] = array(
                'id' => $value['id'],
                'product_id' => $value['product_id'],
                'product_version_id' => $value['product_version_id'],
                'version' => $version_name,
                'subname' => $version_name,
                'text' => $value['product_name'],
                'product_name' => $value['product_name'],
                'weight' => number_format($value['weight'], 0, '', ''),
                'price' => number_format($value['price'], 0, '', ','),
                'quantity' => $value['quantity'],
                'sale_on_out_of_stock' => $value['sale_on_out_of_stock'],
                'status' => $value['status'],
                'sku' => $value['sku'],
            );
        }

        $count_tag = $this->model_catalog_lazada->countProductAllContainDraft($filter_data);
        $json['count'] = $count_tag;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function mappingProductBestme()
    {
        $json = [
            'status' => false,
            'message' => $this->language->get('json_not_permission_or_missing_data')
        ];
        $this->load->language('sale_channel/lazada');
        $this->load->model('catalog/lazada');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateMapping()) {
            $pvid = isset($this->request->post['mapping_id']) ? $this->request->post['mapping_id'] : 0;
            $id = isset($this->request->post['id']) ? $this->request->post['id'] : 0;

            $pvid = explode('-', $pvid);
            $product_id = isset($pvid[0]) ? $pvid[0] : 0;
            $product_version_id = (isset($pvid[1]) && $pvid[1]) ? $pvid[1] : 0;

            $this->model_catalog_lazada->mappingProductBestme($id, $product_id, $product_version_id);
            $product_info = $this->model_catalog_lazada->getProductInfoById($product_id, $product_version_id);

            $json = [
                'status' => true,
                'product_name' => isset($product_info['name']) ? $product_info['name'] : '',
                'product_price' => isset($product_info['price']) ? number_format(str_replace(',', '', floatval($product_info['price']))) : '',
                'product_quantity' => isset($product_info['quantity']) ? $product_info['quantity'] : '',
                'product_edit' => isset($product_info['edit']) ? $product_info['edit'] : '',
                'message' => $this->language->get('json_connection_success')
            ];
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function cancelMappingVersion()
    {
        $json = [
            'status' => false,
            'message' => $this->language->get('json_not_permission_or_missing_data')
        ];
        $this->load->language('sale_channel/lazada');
        $this->load->model('catalog/lazada');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateCancelMapping()) {
            $id = isset($this->request->post['id']) ? $this->request->post['id'] : 0;
            $item_id = isset($this->request->post['item_id']) ? $this->request->post['item_id'] : 0;

            $this->model_catalog_lazada->cancelMappingVersion($id, $item_id);

            $json = [
                'status' => true,
                'message' => $this->language->get('json_cancel_connection_success')
            ];
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function cancelAllMapping()
    {
        $json = [
            'status' => false,
            'message' => $this->language->get('json_not_permission_or_missing_data')
        ];
        $this->load->language('sale_channel/lazada');
        $this->load->model('catalog/lazada');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateCancelAllMapping()) {
            $item_id = isset($this->request->post['item_id']) ? $this->request->post['item_id'] : 0;

            $this->model_catalog_lazada->cancelAllMapping($item_id);

            $json = [
                'status' => true,
                'message' => $this->language->get('json_cancel_connection_success')
            ];
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function createProduct()
    {
        $this->load->language('sale_channel/lazada');
        $json = [
            'code' => 200
        ];

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateCreateProduct()) {
            $this->load->model('catalog/lazada');

            $type = $this->request->post['type'];
            $id = $this->request->post['id'];
            if ($type == 'product') {
                $this->createProductMultiVersion($id);
            } else if ($type == 'version') {
                $this->createProductSingleVersion($id);
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function validateCreateProduct()
    {
        $this->load->language('sale_channel/lazada');
        if (!$this->user->hasPermission('modify', 'sale_channel/lazada')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        if (!isset($this->request->post['type']) || !isset($this->request->post['id'])) {
            $this->error['warning'] = $this->language->get('warning_validate_create_prod');
        }

        return !$this->error;
    }

    private function createProductMultiVersion($id)
    {
        $product_info = $this->model_catalog_lazada->getProductById($id);
        if (!is_array($product_info) || empty($product_info) || !array_key_exists('item_id', $product_info)) {
            $this->session->data['error'] = $this->language->get('warning_validate_not_exits_prod');
            return;
        }

        $versions_info = $this->model_catalog_lazada->getProductVersionByItemId($product_info['item_id']);
        if (empty($versions_info)){
            $this->session->data['error'] = $this->language->get('warning_validate_info_prod_not_complete');
            return;
        }

        $attributes = $this->mapAttribute($product_info['primary_category'], $versions_info);
        if (!array_key_exists('attribute_name', $attributes) || empty($attributes['attribute_name'])){
            $first_version_id = isset($versions_info[0]['id']) ? $versions_info[0]['id'] : 0;
            if (!$first_version_id){
                $this->session->data['error'] = $this->language->get('warning_validate_info_prod_not_complete');
                return;
            }
            $this->createProductSingleVersion($first_version_id);
            return;
        }

        $product = array();
        $product['name'] = $product_info['name'];
        $product['description'] = $product_info['description'];
        $product['summary'] = $product_info['short_description'];
        $images = [];
        foreach ($versions_info as $version) {
            if (!isset($version['images'])) {
                continue;
            }
            $version_image = json_decode($version['images'], true);
            if (!is_array($version_image)) {
                continue;
            }
            $images = array_merge($images, $version_image);
            if (count($images) > 6) {
                break;
            }
        }
        $product_weight = isset($versions_info[0]['product_weight']) ? (float)$versions_info[0]['product_weight'] : 0;
        $images = array_slice($images, 0, 6);
        $product['images'] = $images;
        $product['product_version'] = 1; // create multi version
        $product['price'] = '';
        $product['promotion_price'] = '';
        $product['sku'] = '';
        $product['common_sku'] = '';
        $product['quantity'] = '';
        $product['weight'] = $product['common_weight'] = (int)($product_weight * 1000);
        $product['status'] = 1;
        $product['barcode'] = '';
        $product['seo_title'] = '';
        $product['seo_desciption'] = '';
        $product['common_barcode'] = '';
        $product['common_compare_price'] = '';
        $product['common_price'] = '';
        $product['category'][] = $this->getCategory($product_info['primary_category']);
        $versions = $this->mapVersion($versions_info);
        $product = array_merge($product, $versions);
        $product = array_merge($product, $attributes);
        $product_to_store = $this->getProductToStoreForProductMultiversions($versions);
        $product = array_merge($product, $product_to_store);

        $this->load->model('catalog/product');
        $bestme_id = $this->model_catalog_product->addProduct($product);

        // map version
        $admin_product_version = $this->model_catalog_product->getProductVersions($bestme_id);
        foreach ($versions_info as $version) {
            if (!is_array($version) || !array_key_exists('version_name', $version)) {
                continue;
            }

            foreach ($admin_product_version as $admin_version) {
                if (!is_array($admin_version) || !array_key_exists('version', $admin_version)) {
                    continue;
                }

                if ($version['version_name'] == $admin_version['version']) {
                    $this->model_catalog_lazada->mappingProductBestme($version['id'], $bestme_id, $admin_version['product_version_id']);
                }
            }
        }

        $this->session->data['success'] = $this->language->get('create_prod_lazada_success');
    }

    private function mapVersion($versions)
    {
        $result = [];

        foreach ($versions as $key => $version){
            if (!is_array($version) || !array_key_exists('version_name', $version)) {
                continue;
            }
            $result['product_prices'][$key] = (float)$version['price'];
            $result['product_promotion_prices'][$key] = '';
            $result['product_quantities'][$key] = (int)$version['quantity'];
            $result['product_skus'][$key] = $version['seller_sku'];
            $result['product_barcodes'][$key] = '';
            if ($version['status'] == 'active'){
                $result['product_display'][$key] = $version['version_name'];
            }
            $result['product_version_names'][$key] = $version['version_name'];
        }

        return $result;
    }

    private function mapAttribute($category_id, $versions)
    {
        $result = [];

        $categoryAttr = $this->model_catalog_lazada->getCategoryAttributes($category_id);
        $k = 0;
        foreach ($categoryAttr as $attribute){
            if (!array_key_exists('is_sale_prop', $attribute) || !array_key_exists('name', $attribute) || $attribute['is_sale_prop'] == 0){
                continue;
            }

            $result['attribute_name'][] = $attribute['name'];

            $arr_values = array();
            foreach ($versions as $version){
                if (!is_array($version) || !array_key_exists('version_name', $version) || !$version['version_name']){
                    continue;
                }
                $version_name = $version['version_name'];
                $version_name = explode(',', $version_name);
                $value = isset($version_name[$k]) ? trim($version_name[$k]) : '';
                if ($value && !in_array($value, $arr_values)) {
                    $arr_values[] = $value;
                }
            }

            $result['attribute_values'][] = $arr_values;
            $k++;
        }

        return $result;
    }

    private function getProductToStoreForProductMultiversions($version_info, $default_store_id = 0)
    {
        $result = [];
        if (empty($version_info)) {
            return $result;
        }

        if (!array_key_exists('product_version_names', $version_info) || !is_array($version_info['product_version_names'])
            || !array_key_exists('product_quantities', $version_info) || !is_array($version_info['product_quantities'])) {
            return $result;
        }

        foreach ($version_info['product_version_names'] as $key => $version_name) {
            $version_name = $this->replaceVersionValues($version_name);
            $result['stores'][$version_name][] = $default_store_id;
            $result['original_inventory'][$version_name][] = isset($version_info['product_quantities'][$key]) ? (int)$version_info['product_quantities'][$key] : 0;
            $result['cost_price'][$version_name][] = 0;
        }

        return $result;
    }

    private function replaceVersionValues($str) {
        return preg_replace('/[^0-9a-zA-Z\-_]+/', '_', $str) . '_' . md5($str);
    }

    private function createProductSingleVersion($id)
    {
        $version_info = $this->model_catalog_lazada->getProductVersionByVersionId($id);
        if (!is_array($version_info) || empty($version_info) || !array_key_exists('item_id', $version_info)) {
            $this->session->data['error'] = $this->language->get('warning_validate_not_exits_prod');
            return;
        }
        $product_info = $this->model_catalog_lazada->getProductByItemId($version_info['item_id']);
        if (!is_array($product_info) || empty($product_info) || !array_key_exists('primary_category', $product_info)) {
            $this->session->data['error'] = $this->language->get('warning_validate_not_exits_prod');
            return;
        }

        $product= array();
        $product['name'] = $product_info['name'];
        if ($version_info['version_name']){
            $product['name'] .= '-' . $version_info['version_name'];
        }
        $product['description'] = $product_info['description'];
        $product['summary'] = $product_info['short_description'];
        $product['images'] = json_decode($version_info['images'], true);
        $product['product_version'] = 0; // create single version
        $product['price'] = (float)$version_info['price'];
        $product['promotion_price'] = '';
        $product['sku'] = $version_info['seller_sku'];
        $product['common_sku'] = '';
        $product['quantity'] = (int)$version_info['quantity'];
        $product['weight'] = (int)((float)$version_info['product_weight'] * 1000);
        $product['status'] = $version_info['status'] == 'active' ? 1 : 0;
        $product['barcode'] = '';
        $product['seo_title'] = '';
        $product['seo_desciption'] = '';
        $product['common_barcode'] = '';
        $product['common_compare_price'] = '';
        $product['common_price'] = '';
        $product['stores'][][] = 0; // default store_id
        $product['original_inventory'][][] = (int)$version_info['quantity'];
        $product['cost_price'][][] = 0;
        $product['category'][] = $this->getCategory($product_info['primary_category']);

        $this->load->model('catalog/product');
        $bestme_id = $this->model_catalog_product->addProduct($product);

        $this->model_catalog_lazada->mappingProductBestme($id, $bestme_id, 0);
        $this->session->data['success'] = $this->language->get('create_prod_lazada_success');
    }

    private function getCategory($lazada_category_id)
    {
        $this->load->model('catalog/lazada');
        $this->load->model('catalog/category');
        $category_name = $this->model_catalog_lazada->getCategoryNameById($lazada_category_id);
        if (!$category_name) {
            return null;
        }
        $filter_data['filter_name'] = trim($category_name);

        $results = $this->model_catalog_category->getCategoriesPaginator($filter_data);
        if (!empty($results)) {
            return reset($results)['id'];
        }

        return $this->model_catalog_category->addCategoryFast($category_name);
    }

    public function getSyncState()
    {
        $result = [
            'count_product_lazada' => 0,
            'count_product_lazada_saved' => 0,
            'load_lazada_product_running' => 0
        ];

        if (!isset($this->request->get['shop_id'])) {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($result));
            return;
        }

        $redis = null;

        try {
            $redis = $this->redisConnect();
            if (!$redis instanceof \Redis) {
                $result['_error'] = 'Connect to Redis failed!';

                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($result));

                return;
            }

            $shop_id = (int)$this->request->get['shop_id'];
            $shop_name = $this->config->get('shop_name');

            $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'load_lazada_product_running');
            $result['load_lazada_product_running'] = $redis->get($key);

            $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'count_product_lazada');
            $result['count_product_lazada'] = $redis->get($key);

            $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'count_product_lazada_saved');
            $result['count_product_lazada_saved'] = $redis->get($key);
        } catch (Exception $e) {
            // could not connect to redis
        } finally {
            $this->redisClose($redis);
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    public function loadLazadaProducts()
    {
        if (!isset($this->request->get['shop_id'])) {
            return;
        }

        /* push job to Redis. Notice: use shop_name to define unique job... */
        $redis = null;

        try {
            $shop_id = (int)$this->request->get['shop_id'];
            if (!$shop_id) {
                return;
            }
            $this->load->model('catalog/lazada');
            $access_token = $this->model_catalog_lazada->getAccessTokenById($shop_id);
            if (!$access_token){
                return;
            }

            /* update process status */
            $shop_name = $this->config->get('shop_name');
            $this->updateProcessingStatusRunning($shop_name, $shop_id);

            /* create job */
            $job = [
                self::CJ_JOB_KEY_DB_HOSTNAME => DB_HOSTNAME,
                self::CJ_JOB_KEY_DB_PORT => DB_PORT,
                self::CJ_JOB_KEY_DB_USERNAME => DB_USERNAME,
                self::CJ_JOB_KEY_DB_PASSWORD => DB_PASSWORD,
                self::CJ_JOB_KEY_DB_DATABASE => DB_DATABASE,
                self::CJ_JOB_KEY_DB_PREFIX => DB_PREFIX,
                self::CJ_JOB_KEY_DB_DRIVER => DB_DRIVER,
                self::CJ_JOB_KEY_SHOP_NAME => $shop_name,
                self::CJ_JOB_KEY_SHOP_ID => $shop_id,
                self::CJ_JOB_KEY_ACCESS_TOKEN => $access_token,
                self::CJ_JOB_KEY_TASK => self::CJ_JOB_TASK_GET_PRODUCTS
            ];

            $redis = $this->redisConnect();
            $redis->lPush(LAZADA_PRODUCT_JOB_REDIS_QUEUE, json_encode($job));
        } catch (Exception $e) {
            // could not connect to redis
            $this->log->write('[controller/sale_channel/lazada] loadLazadaProducts(): could not connect to redis. Error: ' . $e->getMessage());
        } finally {
            $this->redisClose($redis);
        }
    }

    public function loadLazadaOrdersManual()
    {
        if (!isset($this->request->get['shop_id'])) {
            return;
        }

        /* push job to Redis. Notice: use shop_name to define unique job... */
        $redis = null;

        try {
            $shop_id = (int)$this->request->get['shop_id'];
            if (!$shop_id) {
                return;
            }

            $this->load->model('catalog/lazada');
            $access_token = $this->model_catalog_lazada->getAccessTokenById($shop_id);
            if (!$access_token){
                return;
            }
            $redis = $this->redisConnect();

            /* update process status */
            $shop_name = $this->config->get('shop_name');
            $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'load_lazada_order_running');
            if((int)$redis->get($key) == 1){
                return;
            };
            $this->updateSyncOrderProcessingStatusRunning($shop_name, $shop_id);

            /* create job */
            $job = [
                self::CJ_JOB_KEY_DB_HOSTNAME => DB_HOSTNAME,
                self::CJ_JOB_KEY_DB_PORT => DB_PORT,
                self::CJ_JOB_KEY_DB_USERNAME => DB_USERNAME,
                self::CJ_JOB_KEY_DB_PASSWORD => DB_PASSWORD,
                self::CJ_JOB_KEY_DB_DATABASE => DB_DATABASE,
                self::CJ_JOB_KEY_DB_PREFIX => DB_PREFIX,
                self::CJ_JOB_KEY_DB_DRIVER => DB_DRIVER,
                self::CJ_JOB_KEY_SHOP_NAME => $shop_name,
                self::CJ_JOB_KEY_SHOP_ID => $shop_id,
                self::CJ_JOB_KEY_ACCESS_TOKEN => $access_token,
                self::CJ_JOB_KEY_SYNC_INTERVAL => 0,
                self::CJ_JOB_KEY_SYNC_NEXT_TIME => null,
                self::CJ_JOB_KEY_SYNC_TYPE => 'manual', // auto / manual
                self::CJ_JOB_KEY_TASK => self::CJ_JOB_TASK_GET_ORDERS
            ];

            $redis->lPush(LAZADA_ORDER_JOB_REDIS_QUEUE, json_encode($job));
        } catch (Exception $e) {
            // could not connect to redis
            $this->log->write('[controller/sale_channel/lazada] loadLazadaOrdersManual(): could not connect to redis. Error: ' . $e->getMessage());
        } finally {
            $this->redisClose($redis);
        }
    }

    private function loadLazadaOrders($shop_id, $sync_interval, $sync_next_time, $sync_type)
    {
        /* push job to Redis. Notice: use shop_name to define unique job... */
        $redis = null;

        try {
            if (!$shop_id) {
                return;
            }
            $this->load->model('catalog/lazada');
            $access_token = $this->model_catalog_lazada->getAccessTokenById($shop_id);
            if (!$access_token){
                return;
            }

            /* update process status */
            $shop_name = $this->config->get('shop_name');
            //$this->updateSyncOrderProcessingStatusRunning($shopee_id, $shop_name);

            /* create job */
            $job = [
                self::CJ_JOB_KEY_DB_HOSTNAME => DB_HOSTNAME,
                self::CJ_JOB_KEY_DB_PORT => DB_PORT,
                self::CJ_JOB_KEY_DB_USERNAME => DB_USERNAME,
                self::CJ_JOB_KEY_DB_PASSWORD => DB_PASSWORD,
                self::CJ_JOB_KEY_DB_DATABASE => DB_DATABASE,
                self::CJ_JOB_KEY_DB_PREFIX => DB_PREFIX,
                self::CJ_JOB_KEY_DB_DRIVER => DB_DRIVER,
                self::CJ_JOB_KEY_SHOP_NAME => $shop_name,
                self::CJ_JOB_KEY_SHOP_ID => $shop_id,
                self::CJ_JOB_KEY_ACCESS_TOKEN => $access_token,
                self::CJ_JOB_KEY_SYNC_INTERVAL => $sync_interval,
                self::CJ_JOB_KEY_SYNC_NEXT_TIME => $sync_next_time,
                self::CJ_JOB_KEY_SYNC_TYPE => $sync_type, // auto / manual
                self::CJ_JOB_KEY_TASK => self::CJ_JOB_TASK_GET_ORDERS
            ];

            $redis = $this->redisConnect();
            $redis->lPush(LAZADA_ORDER_JOB_REDIS_QUEUE, json_encode($job));
        } catch (Exception $e) {
            // could not connect to redis
            $this->log->write('[controller/sale_channel/lazada] loadLazadaOrders(): could not connect to redis. Error: ' . $e->getMessage());
        } finally {
            $this->redisClose($redis);
        }
    }

    private function updateOrderJob($shop_id, $sync_interval, $update_type)
    {
        /* push job to Redis. Notice: use shop_name to define unique job... */
        $redis = null;

        try {
            if (!$shop_id) {
                return;
            }
            $shop_name = $this->config->get('shop_name');

            $update_job = [
                self::CJ_UPDATE_JOB_KEY_TYPE => $update_type,
                self::CJ_UPDATE_JOB_KEY_sync_interval => $sync_interval
            ];

            $redis = $this->redisConnect();
            $key = self::CJ_UPDATE_JOB_TASK_GET_ORDERS_PREFIX . $shop_name . $shop_id;
            $redis->set($key, json_encode($update_job));
        } catch (Exception $e) {
            // could not connect to redis
            $this->log->write('[controller/sale_channel/shopee] loadShopeeOrders(): could not connect to redis. Error: ' . $e->getMessage());
        } finally {
            $this->redisClose($redis);
        }
    }

    public function getSyncOrdersState()
    {
        $result = [
            'count_order_lazada' => 0,
            'count_order_lazada_saved' => 0,
            'load_lazada_order_running' => 0
        ];

        if (!isset($this->request->get['shop_id'])) {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($result));
            return;
        }

        $redis = null;

        try {
            $redis = $this->redisConnect();
            if (!$redis instanceof \Redis) {
                $result['_error'] = 'Connect to Redis failed!';

                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($result));

                return;
            }

            $shop_id = (int)$this->request->get['shop_id'];
            $shop_name = $this->config->get('shop_name');

            $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'load_lazada_order_running');
            $result['load_lazada_order_running'] = $redis->get($key);

            $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'count_order_lazada');
            $result['count_order_lazada'] = $redis->get($key);

            $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'count_order_lazada_saved');
            $result['count_order_lazada_saved'] = $redis->get($key);
        } catch (Exception $e) {
            // could not connect to redis
        } finally {
            $this->redisClose($redis);
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    public function saveTimeSyncConfig()
    {
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateTimeSyncConfig()) {
            $this->load->model('catalog/lazada');
            $old_sync_interval = $this->model_catalog_lazada->getTimeIntervalById($this->request->post['id']);
            $this->model_catalog_lazada->updateTimeSyncById($this->request->post['id'], $this->request->post['time']);
            // update order job
            if ((int)$this->request->post['time'] == 0) {
                // remove job
                $this->updateOrderJob($this->request->post['id'], (int)$this->request->post['time'], self::CJ_UPDATE_JOB_TYPE_REMOVE);
            } else {
                if ($old_sync_interval == 0) {
                    //add new job
                    $this->loadLazadaOrders($this->request->post['id'], (int)$this->request->post['time'], null, 'auto');
                } else {
                    if ($this->checkJobExist($this->request->post['id'])) {
                        // update job
                        $this->updateOrderJob($this->request->post['id'], (int)$this->request->post['time'], self::CJ_UPDATE_JOB_TYPE_UPDATE);
                    } else {
                        //add new job
                        $this->loadLazadaOrders($this->request->post['id'], (int)$this->request->post['time'], null, 'auto');
                    }
                }
            }

            $this->session->data['success'] = $this->language->get('text_save_time_config_success');

            return;
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($this->error));
    }

    private function checkJobExist($shop_id)
    {
        $result = false;
        $redis = null;

        try {
            if (!$shop_id) {
                return $result;
            }

            $shop_name = $this->config->get('shop_name');

            $redis = $this->redisConnect();
            $jobs = $redis->lrange(LAZADA_ORDER_JOB_REDIS_QUEUE, 0, -1);
            if (!is_array($jobs)) {
                return $result;
            }

            foreach ($jobs as $job) {

                $job = json_decode($job, true);
                if (!is_array($job) || !array_key_exists(self::CJ_JOB_KEY_SHOP_NAME, $job) || !array_key_exists(self::CJ_JOB_KEY_SHOP_ID, $job)) {
                    continue;
                }

                $sync_type = isset($job[self::CJ_JOB_KEY_SYNC_TYPE]) ? $job[self::CJ_JOB_KEY_SYNC_TYPE] : 'auto';
                if ($job[self::CJ_JOB_KEY_SHOP_NAME] == $shop_name && $job[self::CJ_JOB_KEY_SHOP_ID] == $shop_id && $sync_type == 'auto') {
                    $result = true;
                    break;
                }
            }
        } catch (Exception $e) {
            // TODO
        }

        $this->redisClose($redis);

        return $result;
    }

    private function validateTimeSyncConfig()
    {
        $this->load->language('sale_channel/lazada');
        $this->load->model('catalog/lazada');
        if (!$this->user->hasPermission('modify', 'sale_channel/lazada')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!isset($this->request->post['id']) ||
            !isset($this->request->post['time'])) {
            $this->error['warning'] = $this->language->get('warning_validate_time_sync_config');
        }

        $shop = $this->model_catalog_lazada->checkExitsLazadaShop($this->request->post['id']);

        if (!isset($shop->row['id'])) {
            $this->error['warning'] = $this->language->get('warning_validate_lazada_shop_exits');
        }

        return !$this->error;
    }

    private function validateMapping()
    {
        $this->load->language('sale_channel/lazada');
        if (!$this->user->hasPermission('modify', 'sale_channel/lazada')) {
            return false;
        }
        if (!isset($this->request->post['id'])) {
            return false;
        }

        if (!isset($this->request->post['mapping_id'])) {
            return false;
        }

        return true;
    }

    private function validateCancelMapping()
    {
        $this->load->language('sale_channel/lazada');
        if (!$this->user->hasPermission('modify', 'sale_channel/lazada')) {
            return false;
        }
        if (!isset($this->request->post['id'])) {
            return false;
        }

        if (!isset($this->request->post['item_id'])) {
            return false;
        }

        return true;
    }

    private function validateCancelAllMapping()
    {
        $this->load->language('sale_channel/lazada');
        if (!$this->user->hasPermission('modify', 'sale_channel/lazada')) {
            return false;
        }

        if (!isset($this->request->post['item_id'])) {
            return false;
        }

        return true;
    }

    private function updateProcessingStatusRunning($shop_name, $shop_id)
    {
        $redis = $this->redisConnect();

        $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'load_lazada_product_running');
        $redis->set($key, 1, LAZADA_PROCESS_INFO_TIMEOUT);

        $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'count_product_lazada');
        $redis->set($key, 0, LAZADA_PROCESS_INFO_TIMEOUT);

        $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'count_product_lazada_saved');
        $redis->set($key, 0, LAZADA_PROCESS_INFO_TIMEOUT);

        $this->redisClose($redis);
    }

    private function updateSyncOrderProcessingStatusRunning($shop_name, $shop_id)
    {
        $redis = $this->redisConnect();

        $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'load_lazada_order_running');
        $redis->set($key, 1, LAZADA_PROCESS_INFO_TIMEOUT);

        $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'count_order_lazada');
        $redis->set($key, 0, LAZADA_PROCESS_INFO_TIMEOUT);

        $key = sprintf('%s%s_%s_%s', LAZADA_PROCESS_INFO_PREFIX, $shop_name, $shop_id, 'count_order_lazada_saved');
        $redis->set($key, 0, LAZADA_PROCESS_INFO_TIMEOUT);

        $this->redisClose($redis);
    }

    /**
     * @return null|Redis
     */
    private function redisConnect()
    {
        $redis = null;

        try {
            $redis = new \Redis();
            $redis->connect(MUL_REDIS_HOST, MUL_REDIS_PORT);
            $redis->select(MUL_REDIS_DB_LAZADA);
        } catch (Exception $e) {
            // could not connect to redis
        }

        return $redis;
    }

    /**
     * @param Redis $redis
     */
    private function redisClose($redis)
    {
        if (!$redis instanceof \Redis) {
            return;
        }

        try {
            $redis->close();
        } catch (Exception $e) {
            // could not connect to redis
        }
    }

    private function orderStatusLazadaDetail($status)
    {
        // status: pending, ready_to_ship, shipped, delivered, canceled, returned, failed : giao hàng thất bại

        $this->load->language('sale_channel/lazada');
        $text = '';
        switch ($status) {
            case 'pending':
                $text = $this->language->get('status_lazada_pending');
                break;
            case 'canceled':
                $text = $this->language->get('status_lazada_canceled');
                break;
            case 'ready_to_ship':
                $text = $this->language->get('status_lazada_ready_to_ship');
                break;
            case 'shipped':
                $text = $this->language->get('status_lazada_shipped');
                break;
            case 'delivered':
                $text = $this->language->get('status_lazada_delivered');
                break;
            case 'failed':
                $text = $this->language->get('status_lazada_failed');
                break;
            case 'returned':
                $text = $this->language->get('status_lazada_returned');
                break;
        }

        return $text;
    }

    private function orderSyncStatusDetail($status)
    {
        $this->load->language('sale_channel/lazada');
        if ($status == ModelCatalogLazada::SYNC_STATUS_SYNC_SUCCESS) {
            $text = $this->language->get('status_sync_success');
        } elseif ($status == ModelCatalogLazada::SYNC_STATUS_ORDER_NOT_YET_SYNC) {
            $text = $this->language->get('text_status_not_yet_sync');
        } else {
            $text = $this->language->get('status_sync_error');
        }

        return $text;
    }

    private function orderErrorSyncStatusDetail($status)
    {
        $this->load->language('sale_channel/lazada');
        $text = '';
        switch ($status) {
            case ModelCatalogLazada::SYNC_STATUS_PRODUCT_NOT_MAPPING:
                $text = $this->language->get('status_sync_error_product_not_mapping');
                break;
            case ModelCatalogLazada::SYNC_STATUS_ORDER_IS_EDITED:
                $text = $this->language->get('status_sync_error_order_has_been_process');
                break;
            case ModelCatalogLazada::SYNC_STATUS_SHOP_EXPIRED:
                $text = $this->language->get('status_sync_error_shop_expired');
                break;
            case ModelCatalogLazada::SYNC_STATUS_SYNC_TIME_OUT:
                $text = $this->language->get('status_sync_error_sync_timeout');
                break;
            case ModelCatalogLazada::SYNC_STATUS_PRODUCT_MAPPING_OUT_OF_STOCK:
                $text = $this->language->get('status_sync_error_product_out_of_stock_on_admin');
                break;
        }

        return $text;
    }

    private function orderSolutionSyncStatusDetail($status)
    {
        $this->load->language('sale_channel/lazada');
        $text = '';
        switch ($status) {
            case ModelCatalogLazada::SYNC_STATUS_PRODUCT_NOT_MAPPING:
                $text = $this->language->get('status_sync_solution_product_not_mapping');
                break;
            case ModelCatalogLazada::SYNC_STATUS_ORDER_IS_EDITED:
                $text = $this->language->get('status_sync_solution_order_has_been_process');
                break;
            case ModelCatalogLazada::SYNC_STATUS_SHOP_EXPIRED:
                $text = $this->language->get('status_sync_error_shop_expired');
                break;
            case ModelCatalogLazada::SYNC_STATUS_SYNC_TIME_OUT:
                $text = $this->language->get('status_sync_solution_sync_timeout');
                break;
            case ModelCatalogLazada::SYNC_STATUS_PRODUCT_MAPPING_OUT_OF_STOCK:
                $text = $this->language->get('status_sync_solution_product_out_of_stock_on_admin');
                break;
        }

        return $text;
    }

    private function validateSaveLazadaShopConfig()
    {
        $this->load->language('sale_channel/lazada');
        $this->load->model('catalog/lazada');
        if (!$this->user->hasPermission('modify', 'sale_channel/lazada')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!isset($this->request->post['id'])) {
            $this->error['warning'] = $this->language->get('warning_validate_time_sync_config');
        }

        $shop = $this->model_catalog_lazada->checkExitsLazadaShop($this->request->post['id']);

        if (!isset($shop->row['id'])) {
            $this->error['warning'] = $this->language->get('warning_validate_lazada_shop_exits');
        }

        return !$this->error;
    }
}