<?php

use theme_config\Undo_Redo;

class ControllerSectionCategoryFilter extends Controller
{
    use Theme_Config_Util;

    public function index()
    {
        $data = array();

        $this->load->language('section/category/filter');

        /* config with undo|redo|change api */
        $action = isset($this->request->get['action']) ? $this->request->get['action'] : 'config';
        if (in_array($action, ['config', 'undo', 'redo', 'change'])) {
            // undo, redo attribute filters
            switch ($action) {
                case 'config':
                    // get all attribute filters
                    $attribute_filters = $this->getAttributeFilters();
                    $attributes = json_decode($attribute_filters['current'], true);
                    foreach ($attributes as $attribute_key => $attribute) {
                        // format attribute value
                        $attributes[$attribute_key]['value'] = str_replace(',', ', ', $attribute['value']);
                    }
                    break;

                case 'undo':
                    $attributes = $this->undoAttributeFilters();
                    break;

                case 'redo':
                    $attributes = $this->redoAttributeFilters();
                    break;

                default:
                    $attributes = [];
            }
            $data['attributes'] = $attributes;

            $data['config'] = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $data['config'] = is_array($data['config']) ? $data['config'] : json_decode($data['config'], true);
            if (isset($data['config']['display'])) {
                if (!isset($data['config']['display']['tag'])) {
                    $data['config']['display']['tag']['visible'] = '1';
                }

                // v2.10.2 - Tunning: Custom filters by product attributes
                if (!isset($data['config']['display']['attribute'])) {
                    $data['config']['display']['attribute']['visible'] = '1';
                }
            }
        }

        // undo-redo page
        $data['href_change'] = $this->url->link('section_category/filter', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_filter&action=change', true);
        $data['href_undo'] = $this->url->link('section_category/filter', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_filter&action=undo', true);
        $data['href_redo'] = $this->url->link('section_category/filter', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_filter&action=redo', true);

        $data['href_section_theme'] = $this->url->link('theme/section_theme', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_section_theme', true);

        $data['preview'] = $this->load->controller('theme/preview');

        /* fix displaying selected page on header */
        $this->load->language('theme/preview');
        $data['selected_page'] = $this->getSelectedPageDueToRoute($this->request->get['route']);

        /* base */
        $data['href_exit'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_homepage'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $data['href_category'] = $this->url->link('section_category/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_sections', true);
        $data['href_product_detail'] = $this->url->link('section_product_detail/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_detail_sections', true);
        $data['href_blog'] = $this->url->link('section_blog/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog_sections', true);
        $data['href_contact'] = $this->url->link('section_contact/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_sections', true);
        $data['back_section'] = $this->url->link('section_category/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_sections', true);
        $data['url_change_attribute_filter_status'] = $this->url->link('section_category/filter/changeAttributeFilterStatus', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_filter', true);

        $this->response->setOutput($this->load->view('section/category/filter', $data));
    }

    public function getAttributeFilters() {
        if (empty($this->session->data['attribute_filters'])) {
            // get from db
            $this->load->model('catalog/attribute_filter');
            $existing_attribute_filters = $this->model_catalog_attribute_filter->getAttributeFilters();

            // re-init current index
            $current_index = 0;

            // make array
            $existing_attribute_filters = json_encode($existing_attribute_filters, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG);
            $attribute_filters_arr = [$existing_attribute_filters];

            // update session data
            $this->updateCurrentAttributeFiltersSessionData($attribute_filters_arr, $current_index);
        } else {
            // get from session
            $current_index = $this->session->data['attribute_filters']['index'];
            $attribute_filters_arr = $this->session->data['attribute_filters']['data'];
            $undo_redo_lib = new Undo_Redo($attribute_filters_arr, $current_index);
            $existing_attribute_filters = $undo_redo_lib->currentData();
        }

        return [
            'index' => $current_index,
            'data' => $attribute_filters_arr,
            'current' => $existing_attribute_filters
        ];
    }

    public function changeAttributeFilterStatus() {
        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            // get current attribute filter data from session
            $attribute_filters_session = $this->getAttributeFilters();
            $current_attribute_filters = json_decode($attribute_filters_session['current'], true);

            // update attribute filters status with request data
            if (isset($this->request->post['statuses']) && !empty($this->request->post['statuses'])) {
                foreach ($current_attribute_filters as $key => $current_attribute_filter) {
                    $status = in_array($current_attribute_filter['attribute_filter_id'], $this->request->post['statuses']) ? 1 : 0;
                    $current_attribute_filters[$key]['status'] = $status;
                }
            }
            $new_attribute_filters = json_encode($current_attribute_filters, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG);

            $undo_redo = new Undo_Redo($attribute_filters_session['data'], $attribute_filters_session['index']);
            $undo_redo->add($new_attribute_filters);

            // update to current session data
            $this->updateCurrentAttributeFiltersSessionData($undo_redo->allData(), $undo_redo->getIndex());
        }

        $this->response->redirect($this->url->link('section_category/filter', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_filter', true));
    }

    private function updateCurrentAttributeFiltersSessionData($data, $index) {
        $this->session->data['attribute_filters'] = [
            'index' => $index,
            'data' => $data
        ];
    }

    private function undoAttributeFilters() {
        // get all attribute filters
        $attribute_filters = $this->getAttributeFilters();

        $undo_redo = new Undo_Redo($attribute_filters['data'], $attribute_filters['index']);
        $undo_redo->undo();

        // update to current session data
        $this->updateCurrentAttributeFiltersSessionData($undo_redo->allData(), $undo_redo->getIndex());

        return [
            'index' => $undo_redo->getIndex(),
            'data' => $undo_redo->allData(),
            'current' => $undo_redo->currentData()
        ];
    }

    private function redoAttributeFilters() {
        // get all attribute filters
        $attribute_filters = $this->getAttributeFilters();

        $undo_redo = new Undo_Redo($attribute_filters['data'], $attribute_filters['index']);
        $undo_redo->redo();

        // update to current session data
        $this->updateCurrentAttributeFiltersSessionData($undo_redo->allData(), $undo_redo->getIndex());

        return [
            'index' => $undo_redo->getIndex(),
            'data' => $undo_redo->allData(),
            'current' => $undo_redo->currentData()
        ];
    }
}