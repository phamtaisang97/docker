<?php

class ControllerSectionCategoryProductCategory extends Controller
{
    use Theme_Config_Util;

    public function index()
    {
        $data = array();

        $this->load->language('section/category/product_category');

        $this->load->model('custom/common');
        $data['product_category'] = $this->model_custom_common->getListGroupMenu();

        /* config with undo|redo|change api */
        $action = isset($this->request->get['action']) ? $this->request->get['action'] : 'config';
        if (in_array($action, ['config', 'undo', 'redo', 'change'])) {
            $data['config'] = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $data['config'] = is_array($data['config']) ? $data['config'] : json_decode($data['config'], true);
        }

        // undo-redo page
        $data['href_change'] = $this->url->link('section_category/product_category', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_product_category&action=change', true);
        $data['href_undo'] = $this->url->link('section_category/product_category', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_product_category&action=undo', true);
        $data['href_redo'] = $this->url->link('section_category/product_category', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_product_category&action=redo', true);
        $data['menu_edit_link'] = $this->url->link('custom/menu', 'user_token=' . $this->session->data['user_token'], true);

        $data['href_section_theme'] = $this->url->link('theme/section_theme', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_section_theme', true);

        $data['preview'] = $this->load->controller('theme/preview');

        /* fix displaying selected page on header */
        $this->load->language('theme/preview');
        $data['selected_page'] = $this->getSelectedPageDueToRoute($this->request->get['route']);

        /* base */
        $data['href_exit'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_homepage'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $data['href_category'] = $this->url->link('section_category/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_sections', true);
        $data['href_product_detail'] = $this->url->link('section_product_detail/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_detail_sections', true);
        $data['href_blog'] = $this->url->link('section_blog/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog_sections', true);
        $data['href_contact'] = $this->url->link('section_contact/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_sections', true);
        $data['back_section'] = $this->url->link('section_category/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_sections', true);

        $this->response->setOutput($this->load->view('section/category/product_category', $data));
    }
}