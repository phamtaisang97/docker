<?php
class ControllerStartupStartup extends Controller {
	public function index() {
		// Settings
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '0'");

		foreach ($query->rows as $setting) {
			if (!$setting['serialized']) {
				$this->config->set($setting['key'], $setting['value']);
			} else {
				$this->config->set($setting['key'], json_decode($setting['value'], true));
			}
		}

		// Theme
		$this->config->set('template_cache', $this->config->get('developer_theme'));

		// Language
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "language` WHERE code = '" . $this->db->escape($this->config->get('config_admin_language')) . "'");

		if ($query->num_rows) {
			$this->config->set('config_language_id', $query->row['language_id']);
		}

        /* hard code. Current not support multi lang for data. TODO: if need multi language, MUST design db and data correctly, also product design MUST be clear... */
        $this->config->set('config_language_id', '2');

		// Language. note: code fix for f5 case
        // hardcode. TODO: get from db...
        $this->load->model('setting/setting');
        $SUPPORTED_LANGUAGES = [
            'vi-vn',
            'en-gb'
        ];
        if (isset($this->request->get['lang'])
            && in_array($this->request->get['lang'], $SUPPORTED_LANGUAGES)
            && $this->request->get['lang'] !== $this->config->get('config_language')
        ) {
            $this->model_setting_setting->editSettingValue($code = 'config', $key = 'config_language', $value = $this->request->get['lang'], $store_id = 0);
            $this->model_setting_setting->editSettingValue($code = 'config', $key = 'config_admin_language', $value = $this->request->get['lang'], $store_id = 0);

            /* notice: force update loaded config (that is already saved saved to system/config.php before) */
            $this->config->set('config_language', $this->request->get['lang']);
            $this->config->set('config_admin_language', $this->request->get['lang']);

            /* return current language */
            $data['current_language'] = $this->config->get('config_language');
        }

		$language = new Language($this->config->get('config_admin_language'));
		$language->load($this->config->get('config_admin_language'));
		$this->registry->set('language', $language);

		// Customer
		$this->registry->set('customer', new Cart\Customer($this->registry));

		// Currency
		$this->registry->set('currency', new Cart\Currency($this->registry));

		// Tax
		$this->registry->set('tax', new Cart\Tax($this->registry));

		if ($this->config->get('config_tax_default') == 'shipping') {
			$this->tax->setShippingAddress($this->config->get('config_country_id'), $this->config->get('config_zone_id'));
		}

		if ($this->config->get('config_tax_default') == 'payment') {
			$this->tax->setPaymentAddress($this->config->get('config_country_id'), $this->config->get('config_zone_id'));
		}

		$this->tax->setStoreAddress($this->config->get('config_country_id'), $this->config->get('config_zone_id'));

		// Weight
		$this->registry->set('weight', new Cart\Weight($this->registry));

		// Length
		$this->registry->set('length', new Cart\Length($this->registry));

		// Cart
		$this->registry->set('cart', new Cart\Cart($this->registry));

		// Encryption
		$this->registry->set('encryption', new Encryption($this->config->get('config_encryption')));

		// OpenBay Pro
		$this->registry->set('openbay', new Openbay($this->registry));

		// migrate
        $migration = new Migration($this->db, $this->log);
        $migration->migrate();

        // Hàm expire app
        $expiration = new Expiration($this->db, $this->log);
        $expiration->expiration("app");

        // delete demo data after active
        $this->deleteDemoData();
	}

    private function deleteDemoData()
    {
        try {
            $config_packet_paid = $this->config->get('config_packet_paid');
            $config_use_demo_data = $this->config->get('config_use_demo_data');
            if ($config_packet_paid && $config_use_demo_data) {
                $this->load->model('demo_data/demo_data');
                $this->model_demo_data_demo_data->removeAllDataDemo();
                $this->db->query("UPDATE `" . DB_PREFIX . "setting` SET `value` = 0 WHERE `key` = 'config_use_demo_data'");
                $this->db->query("INSERT INTO `" . DB_PREFIX . "setting` (`store_id`, `code`, `key`, `value`, `serialized`) 
                                                SELECT '0', 'config', 'config_show_alert_demo_data_deleted', 1, 0 FROM DUAL 
                                                    WHERE NOT EXISTS (SELECT 1 FROM `" . DB_PREFIX . "setting` WHERE `key` = 'config_show_alert_demo_data_deleted')");
            }
        } catch (Exception $e) {
        }
    }
}