<?php

class ControllerStartupPermission extends Controller
{
    use Setting_Util_Trait;

    public function index()
    {
        if (isset($this->request->get['route'])) {
            $route = '';

            $part = explode('/', $this->request->get['route']);

            if (isset($part[0])) {
                $route .= $part[0];
            }

            if (isset($part[1])) {
                $route .= '/' . $part[1];
            }

            // If a 3rd part is found we need to check if its under one of the extension folders.
            $extension = array(
                'extension/appstore',
                'extension/dashboard',
                'extension/analytics',
                'extension/captcha',
                'extension/extension',
                'extension/feed',
                'extension/fraud',
                'extension/module',
                'extension/payment',
                'extension/shipping',
                'extension/theme',
                'extension/total',
                'extension/report',
                'extension/openbay'
            );

            if (isset($part[2]) && in_array($route, $extension)) {
                $route .= '/' . $part[2];
            }

            // We want to ingore some pages from having its permission checked.
            $ignore = array(
                'common/dashboard',
                'common/login',
                'common/login_check',
                'common/login_mail',
                'common/bestme_use_theme',
                'common/bestme_install_app',
                'common/bestme_pos_check_user_token',
                'common/logout',
                'common/forgotten',
                'common/reset',
                'error/not_found',
                'error/permission',
                'super/login',
                //TODO auto ingore common/filemanager had add when add product
                'common/filemanager',
                'localisation/country',
                'common/delivery_api',
            );

            if (!in_array($route, $ignore) && !$this->user->hasPermission('access', $route)) {
                // return new Action('error/permission');
                return new Action('common/dashboard');
            }

            /* module visible */
            $ROUTE_TO_MODULE_MAPPING = [
                // shopee
                'sale_channel/shopee' => self::$MODULE_ECOM_PLATFORM, // already for sub-routes as /orders and /config
                'sale_channel/shopee_upload' => self::$MODULE_ECOM_PLATFORM,
                // laz
                'sale_channel/lazada' => self::$MODULE_ECOM_PLATFORM,
                // social
                'sale_channel/novaonx_social' => self::$MODULE_SOCIAL,
                // website
                'custom/theme' => self::$MODULE_WEBSITE,
                'blog/blog' => self::$MODULE_WEBSITE,
                'online_store/contents' => self::$MODULE_WEBSITE,
                'custom/menu' => self::$MODULE_WEBSITE,
                'online_store/domain' => self::$MODULE_WEBSITE,
                'custom/preference' => self::$MODULE_WEBSITE,
                'common/filemanager' => self::$MODULE_WEBSITE,
            ];
            if (array_key_exists($route, $ROUTE_TO_MODULE_MAPPING) && !$this->isEnableModule($ROUTE_TO_MODULE_MAPPING[$route])) {
                // return new Action('error/permission');
                return new Action('common/dashboard');
            }
            return $this->checkPermissionBySource($route);
        }
    }

    private function checkUserCanAccess($type_id, $model, $callback, $user_id_text = 'user_create_id') {
        if ((isset($this->request->get[$type_id]) && $this->request->get[$type_id]) ||
            (isset($this->request->post[$type_id]) && $this->request->post[$type_id])
        ) {
            $this->load->model($model);
            $ids = isset($this->request->get[$type_id]) ? $this->request->get[$type_id] : $this->request->post[$type_id];
            $model_registry = 'model_'. str_replace('/', '_', $model);

            if(is_array($ids)) {
                foreach ($ids as $id) {
                    $product = $this->$model_registry->$callback($id);
                    if(!$product[$user_id_text] === $this->user->getId()) {
                        return false;
                    }
                }
            } else {
                $product = $this->$model_registry->$callback($ids);
                return $product[$user_id_text] === $this->user->getId();
            }
        }
        return true;
    }

    private function checkPermissionBySource($route)
    {
        if ($this->user->isAdmin() ||
            $this->user->canAccessAll()
        ) {
            return;
        }


        // check receipt voucher belong to user
        if(!$this->checkUserCanAccess('receipt_voucher_id', 'cash_flow/receipt_voucher', 'getReceiptVoucher')) {
            return new Action('common/dashboard');
        }

        // check payment voucher belong to user
        if(!$this->checkUserCanAccess('payment_voucher_id', 'cash_flow/payment_voucher', 'getPaymentVoucher')) {
            return new Action('common/dashboard');
        }

        // check product belong to user
        if(!$this->checkUserCanAccess('product_id', 'catalog/product', 'getProduct')) {
            return new Action('common/dashboard');
        }

        // check store receipt belong to user
        if(!$this->checkUserCanAccess('store_receipt_id', 'catalog/store_receipt', 'getStoreReceiptById')) {
            return new Action('common/dashboard');
        }

        // check store transfer receipt belong to user
        if(!$this->checkUserCanAccess('store_transfer_receipt_id', 'catalog/store_transfer_receipt', 'getStoreTransferReceiptById')) {
            return new Action('common/dashboard');
        }

        // check store take receipt belong to user
        if(!$this->checkUserCanAccess('store_take_receipt_id', 'catalog/store_take_receipt', 'getStoreTakeReceiptById')) {
            return new Action('common/dashboard');
        }

        // check cost adjustment receipt belong to user
        if(!$this->checkUserCanAccess('cost_adjustment_receipt_id', 'catalog/cost_adjustment_receipt', 'getReceiptById')) {
            return new Action('common/dashboard');
        }

        // check customer belong to user
        if(!$this->checkUserCanAccess('customer_id', 'customer/customer', 'getCustomer')) {
            return new Action('common/dashboard');
        }

        // check customer group belong to user
        if(!$this->checkUserCanAccess('customer_group_id', 'customer/customer_group', 'getCustomerGroup')) {
            return new Action('common/dashboard');
        }

        return true;
    }
}
