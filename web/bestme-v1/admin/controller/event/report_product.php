<?php

class ControllerEventReportProduct extends Controller
{

    /**
     * @param $route
     * @param $args
     * @param $output
     * @throws Exception
     * Create product report after create order
     */
    public function afterCreateOrder(&$route, &$args, &$output)
    {
        if (empty($output) || !isset($args[0]['products']) || !isset($args[0]['quantities']) || !isset($args[0]['prices'])) {
            return;
        }
        $this->load->model('sale/order');
        $this->load->model('report/order_product');
        $dataOrder = $this->model_sale_order->getOrder($output);

        // order discount is divided by product
        $total_before_discount = 0;
        foreach ($args[0]['products'] as $key => $product) {
            $price = (int)str_replace(',', '', $args[0]['prices'][$key]);
            $total_before_discount += $args[0]['quantities'][$key] * $price;
        }
        foreach ($args[0]['products'] as $key => $product) {
            $product_discount = (int)str_replace(',', '', $args[0]['product-discount'][$key]);
            $price = (int)str_replace(',', '', $args[0]['prices'][$key]);
            if($total_before_discount == 0) {
                $args[0]['product-discount'][$key] = 0;
            } else {
                $percent_price = $args[0]['quantities'][$key] * $price / $total_before_discount;
                $args[0]['product-discount'][$key] = $product_discount + $percent_price * (int)$dataOrder['discount'];
            }
        }

        if ($dataOrder && $dataOrder['order_status'] == ModelSaleOrder::ORDER_STATUS_ID_PROCESSING) {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $reportTime = new DateTime();
            $reportTime->setTime($reportTime->format('H'), 0, 0);
            foreach ($args[0]['products'] as $key => $product) {
                $ids = explode('-', $product);
                $productId = array_shift($ids);
                $productVersionId = array_shift($ids);
                $args[0]['quantities'][$key] = str_replace(',', '', $args[0]['quantities'][$key]);
                $args[0]['prices'][$key] = str_replace(',', '', $args[0]['prices'][$key]);
                $dataReportOrderProduct = [
                    'report_time' => $reportTime->format('Y-m-d H:i:s'),
                    'report_date' => $reportTime->format('Y-m-d'),
                    'order_id' => $output,
                    'product_id' => $productId,
                    'product_version_id' => $productVersionId,
                    'quantity' => $args[0]['quantities'][$key],
                    'price' => $args[0]['prices'][$key],
                    'discount' => $args[0]['product-discount'][$key],
                    'total_amount' => $args[0]['quantities'][$key] * $args[0]['prices'][$key],
                ];
                $this->model_report_order_product->saveOrderProductReport($dataReportOrderProduct);
            }
        }
    }

    /**
     * @param $route
     * @param $args
     * @param $output
     * @throws Exception
     * Update product report when change order in order list
     */
    public function editOrderInOrderList(&$route, &$args, &$output)
    {
        if(!is_array($args) || !isset($args[0]) || !isset($args[1])) {
            return;
        }
        $this->load->model('sale/order');
        $this->load->model('report/order_product');
        if(is_array($args[0])) {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $reportTime = new DateTime();
            $reportTime->setTime($reportTime->format('H'), 0, 0);
            $products = $this->model_sale_order->getOrderProducts($args[1]);

            foreach ($products as $key => $product) {
                $dataReportOrderProduct = [
                    'order_id' => $args[1],
                    'product_id' => $product['product_id'],
                    'product_version_id' => $product['product_version_id'],
                    'quantity' => $product['quantity'],
                    'price' => $product['price'],
                    'total_amount' => $product['quantity'] * $product['price'],
                    'discount' => $product['discount'],
                ];
                $this->model_report_order_product->updateOrderProductReport($dataReportOrderProduct);
            }
        }
    }

    /**
     * @param $route
     * @param $args
     * @param $output
     * @throws Exception
     * Update when change status in order list
     */
    public function updateOrderStatusInOrderList(&$route, &$args, &$output)
    {
        if (!is_array($args) || !isset($args[0]['order_id']) || !isset($args[1])) {
            return;
        }
        $this->load->model('sale/order');
        $this->load->model('report/order_product');
        if ($args[0]['order_status_id'] == ModelSaleOrder::ORDER_STATUS_ID_PROCESSING && $args[1] == ModelSaleOrder::ORDER_STATUS_ID_DRAFT) {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $reportTime = new DateTime();
            $reportTime->setTime($reportTime->format('H'), 0, 0);
            $products = $this->model_sale_order->getOrderProducts($args[0]['order_id']);
            foreach ($products as $product) {
                $dataReportOrderProduct = [
                    'report_time' => $reportTime->format('Y-m-d H:i:s'),
                    'report_date' => $reportTime->format('Y-m-d'),
                    'order_id' => $args[0]['order_id'],
                    'product_id' => $product['product_id'],
                    'product_version_id' => (int)$product['product_version_id'],
                    'quantity' => $product['quantity'],
                    'price' => $product['price'],
                    'total_amount' => $product['total'],
                ];
                $this->model_report_order_product->saveOrderProductReport($dataReportOrderProduct);
            }
        }
        if ($args[0]['order_status_id'] == ModelSaleOrder::ORDER_STATUS_ID_CANCELLED){
            $this->model_report_order_product->deleteProductInOrderReport($args[0]['order_id']);
        }
    }

    /**
     * @param $route
     * @param $args
     * @param $output
     * @throws Exception
     * Update or create or delete report product when edit order in detail
     */
    public function editOrderInOrderDetail(&$route, &$args, &$output)
    {
        if (empty($output) || !isset($args[1]['old_order_status_id'])) {
            return;
        }
        $this->load->model('sale/order');
        $this->load->model('report/order_product');
        $dataOrder = $this->model_sale_order->getOrder($output);
        if ($dataOrder) {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $reportTime = new DateTime();
            $reportTime->setTime($reportTime->format('H'), 0, 0);
            $products = $this->model_sale_order->getOrderProducts($output);

            // order discount is divided by product
            $total_before_discount = 0;
            foreach ($products as $key => $product) {
                $total_before_discount += $product['quantity'] * $product['price'];
            }
            foreach ($products as $key => &$product) {
                if($total_before_discount == 0) {
                    $product['discount_with_order_discount_be_divided'] = 0;
                } else {
                    $percent_price = $product['quantity'] * $product['price'] / $total_before_discount;
                    $product['discount_with_order_discount_be_divided'] = $product['discount'] + $percent_price * (int)$dataOrder['discount'];
                }
            }

            if ($dataOrder['order_status'] != ModelSaleOrder::ORDER_STATUS_ID_DRAFT && $args[1]['old_order_status_id'] == ModelSaleOrder::ORDER_STATUS_ID_DRAFT) {
                foreach ($products as $product) {
                    $dataReportOrderProduct = [
                        'report_time' => $reportTime->format('Y-m-d H:i:s'),
                        'report_date' => $reportTime->format('Y-m-d'),
                        'order_id' => $output,
                        'product_id' => $product['product_id'],
                        'product_version_id' => (int)$product['product_version_id'],
                        'quantity' => $product['quantity'],
                        'price' => $product['price'],
                        'total_amount' => $product['total'],
                        'discount' => $product['discount_with_order_discount_be_divided'],
                    ];
                    $this->model_report_order_product->saveOrderProductReport($dataReportOrderProduct);
                }
            } else if ($dataOrder['order_status'] == ModelSaleOrder::ORDER_STATUS_ID_CANCELLED) {
                $this->model_report_order_product->deleteProductInOrderReport($output);
            } else {
                foreach ($products as $product) {
                    $dataReportOrderProduct = [
                        'order_id' => $output,
                        'product_id' => $product['product_id'],
                        'product_version_id' => (int)$product['product_version_id'],
                        'quantity' => $product['quantity'],
                        'price' => $product['price'],
                        'total_amount' => $product['quantity'] * $product['price'],
                        'discount' => $product['discount_with_order_discount_be_divided'],
                    ];
                    $this->model_report_order_product->updateOrderProductReport($dataReportOrderProduct);
                }
            }
        }
    }

    public function updateReportProductWhenEditProduct(&$route, &$args, &$output)
    {
        $product_id = isset($args[0]) ? $args[0] : '';
        $data_product = isset($args[1]) ? $args[1] : '';
        $this->load->model('report/order_product');
        if (isset($data_product['product_version_names']) && $data_product['product_version'] == 1) {
            foreach ($data_product['product_version_ids'] as $k => $product_version_id) {
                $price = ((float)str_replace(',', '', $data_product['product_promotion_prices'][$k]) == 0) ?
                    (float)str_replace(',', '', $data_product['product_prices'][$k]) :
                    (float)str_replace(',', '', $data_product['product_promotion_prices'][$k]);
                $this->model_report_order_product->updateReportProductPrice($product_id, $product_version_id, $price);
            }
        } else {
            $price = ((float)str_replace(',', '', $data_product['promotion_price']) == 0) ?
                (float)str_replace(',', '', $data_product['price']) :
                (float)str_replace(',', '', $data_product['promotion_price']);
            $this->model_report_order_product->updateReportProductPrice($product_id, 0, $price);
        }
    }

}