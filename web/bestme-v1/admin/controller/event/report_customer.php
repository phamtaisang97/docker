<?php

class ControllerEventReportCustomer extends Controller
{
    /**
     * @param $route
     * @param $args
     * @param $output
     * Update report customer when create customer
     */
    public function createCustomer(&$route, &$args, &$output)
    {
        if (empty($output)) {
            return;
        }
        $this->load->model('report/customer');
        $this->model_report_customer->createCustomerReport($output);
    }

    public function deleteCustomer(&$route, &$args, &$output)
    {
        if (!isset($args[0]) || empty($args[0])) {
            return;
        }
        $this->load->model('report/customer');
        $this->model_report_customer->deleteCustomerReport($args[0]);
    }
}