<?php

class ControllerEventActivityTracking extends Controller
{
    use Activity_Tracking_Util_Trait;

    // TODO: use ModelOrder::$ORDER_COMPLETE_STATUS...
    const ORDER_COMPLETE_STATUS = 9;

    public function index()
    {
        try {
            $this->load->model('setting/setting');
            $data = [
                'path' => $this->request->get['route'],
                'method' => $_SERVER['REQUEST_METHOD'],
                'shop_name' => $this->config->get('shop_name'),
                'domain' => $_SERVER['HTTP_HOST'],
                'time' => date("Y-m-d H:i:s"),
                'packet_paid' => $this->config->get('config_packet_paid')
            ];

            if (isset($_SERVER['HTTP_REFERER'])) {
                $url = $this->detectRoute($_SERVER['HTTP_REFERER']);
                $data['from'] = $url;
            }

            $this->pushDataToRedis('ACTIVITY_TRACKING', $data);
        } catch (Exception $ex) {
            return;
        }
    }

    public function trackingOrderComplete()
    {
        if (isset($this->request->post['order_status_id']) && $this->request->post['order_status_id'] == self::ORDER_COMPLETE_STATUS) {
            $this->saveToRedis('count_order_complete');
        }
    }

    public function trackingLogin()
    {
        $this->saveToRedis('login');
    }

    /**
     * run when admin change status of an order
     * listen: admin/model/sale/order_history/addHistory/after
     */
    public function trackingTransferOrderStatus()
    {
        $this->saveToRedis('count_order_status_transfer');
    }

    /**
     * event?...
     */
    public function trackingNewProduct()
    {
        $this->saveToRedis('count_product');
    }

    public function trackingNewProductCategory()
    {
        $this->saveToRedis('count_product_category');
    }

    public function trackingDeleteProductCategory()
    {
        $this->saveToRedis('count_product_category');
    }

    public function trackingProductPrice()
    {
        $this->load->model('activity_tracking/product');
        $this->saveToRedis('avg_product_price', $this->model_activity_tracking_product->getAvgProductPrice());
        $this->saveToRedis('rate_product_have_cost_price', $this->model_activity_tracking_product->getRateProductHaveCostPrice());
    }

    public function trackingStoreReceipt()
    {
        $this->saveToRedis('count_store_receipt');
    }

    public function trackingStoreTransferReceipt()
    {
        $this->saveToRedis('count_store_transfer_receipt');
    }

    public function trackingStoreTakeReceipt()
    {
        $this->saveToRedis('count_store_take_receipt');
    }

    public function trackingCostAdjustmentReceipt()
    {
        $this->saveToRedis('count_cost_adjustment_receipt');
    }

    /*
     * run when admin create an discount application
     * lister: admin/model/discount/discount/addDiscount/after
     */
    public function trackingCountDiscount()
    {
        $this->saveToRedis('count_discount');
    }

    /*
     * run when admin create an order
     * lister: catalog/model/sale/order/addOrderCommon/after
     */
    public function trackingCountOrder()
    {
        $this->saveToRedis('count_order');
    }

    /*
     * run when admin create connect shoppe account
     * lister: admin/model/catalog/shopee/addShopeeShop/after
     */
    public function trackingCountShopee()
    {
        $this->saveToRedis('count_shopee');
    }

    /*
     * run when admin create product shoppe
     * lister: aadmin/model/catalog/product/addProduct/after
     */
    public function trackingCountShopeeProduct(&$route, &$args, &$output)
    {
        try {
            if (!is_array($args) || !isset($args[0])) {
                return;
            }
            if (!isset($args[0]['product-source'])) {
                return;
            } else {
                if ($args[0]['product-source'] == 'shopee') {
                    $this->saveToRedis('count_shopee_product');
                }
            }
        } catch (Exception $ex) {
            return;
        }
    }

    /**
     * @param $url
     * @return mixed|null
     */
    private function detectRoute($url)
    {
        try {
            if (!$url || !isset(explode('?', $url)[1])) return null;

            $url_param_string = explode('?', $url)[1];
            $params = explode('&', $url_param_string);
            foreach ($params as $param_string) {
                $key = explode('=', $param_string)[0];
                $value = explode('=', $param_string)[1];
                if ($key === 'route') {
                    return $value;
                }
            }
            return null;
        } catch (Exception $ex) {
            return null;
        }
    }
}