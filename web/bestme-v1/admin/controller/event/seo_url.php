<?php

class ControllerEventSeoUrl extends Controller
{
    private function updateSitemapLastmod($type) {
        $this->load->model('setting/setting');
        $lastmod = date('Y-m-d');
        $this->model_setting_setting->editSettingValue('config', "sitemap_{$type}_lastmod", $lastmod);

        // ping google when sitemap is changed
        $url = "https://www.google.com/ping?sitemap=" . HTTPS_SERVER ."sitemap_index.xml";
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));

        curl_exec($curl);
        curl_close($curl);
    }

    private function updateFirstLevelProductCategory($product_id) {
        // get first-level categories of this product
        $this->load->model('catalog/product');
        $categories = $this->model_catalog_product->getCategoriesAndParents($product_id);
        $DB_PREFIX = DB_PREFIX;
        $language_id = $this->config->get('config_language_id');
        foreach ($categories as $category) {
            // only accept first-level categories
            if (0 != $category['parent_id']) {
                continue;
            }

            // get category seo url
            $alias = 'category_id=' . $category['id'];
            $sql = "SELECT `keyword` FROM `{$DB_PREFIX}seo_url` WHERE `language_id` = '{$language_id}' AND `query` = '" . $alias . "' AND `table_name` = 'category' LIMIT 1";
            $query = $this->db->query($sql);

            if ($query->row) {
                // update sitemap lastmod
                $this->updateSitemapLastmod($query->row['keyword']);
            }
        }
    }

    private function updateFirstLevelBlogCategory($blog_id) {
        // get first-level blog categories of this blog
        $this->load->model('blog/blog');
        $categories = $this->model_blog_blog->getCategoriesAndParents($blog_id);
        $DB_PREFIX = DB_PREFIX;
        $language_id = $this->config->get('config_language_id');
        foreach ($categories as $category) {
            // only accept first-level categories
            if (0 != $category['parent_id']) {
                continue;
            }

            // get category seo url
            $alias = 'blog_category_id=' . $category['id'];
            $sql = "SELECT `keyword` FROM `{$DB_PREFIX}seo_url` WHERE `language_id` = '{$language_id}' AND `query` = '" . $alias . "' AND `table_name` = 'blog_category' LIMIT 1";
            $query = $this->db->query($sql);

            if ($query->row) {
                // update sitemap lastmod
                $this->updateSitemapLastmod($query->row['keyword']);
            }
        }
    }

    // admin/model/catalog/collection/addCollection/after
    public function afterAddCollection(&$route, &$args, &$output)
    {
        if (empty($output)) {
            return;
        }

        // update sitemap lastmod
        $this->updateSitemapLastmod('collections');
    }

    // admin/model/catalog/collection/editCollection/before
    public function beforeEditCollection(&$route, &$args)
    {
        if (!is_array($args) || count($args) > 2) {
            return;
        }
        $collection_id = $args[0];
        $data = $args[1];

        // generate seo url
        $this->load->model('custom/common');
        $tmp_alias = (isset($data['alias']) && $data['alias'] != '') ? $data['alias'] : ((isset($data['title']) && $data['title'] != '') ? $data['title'] : '');
        $slug = $this->model_custom_common->createSlug($tmp_alias, '-');

        $language_id = $this->config->get('config_language_id');
        $alias = 'collection=' . $collection_id;
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT `keyword` FROM `{$DB_PREFIX}seo_url` WHERE `language_id` = '{$language_id}' AND `query` = '" . $alias . "' AND `table_name` = 'collection' LIMIT 1";
        $query = $this->db->query($sql);

        if (!empty($query->row) && $query->row['keyword'] != $slug) {
            // update sitemap lastmod
            $this->updateSitemapLastmod('collections');
        }
    }

    // admin/model/catalog/collection/deleteCollection/after
    public function afterDeleteCollection(&$route, &$args, &$output)
    {
        if (!is_array($args) || count($args) != 1 || !is_numeric($args[0])) {
            return;
        }

        // update sitemap lastmod
        $this->updateSitemapLastmod('collections');
    }

    // admin/model/catalog/category/addCategory/after
    public function afterAddCategory(&$route, &$args, &$output)
    {
        if (empty($output)) {
            return;
        }

        // update sitemap lastmod
        $this->updateSitemapLastmod('categories');
    }

    // admin/model/catalog/category/editCategory/before
    public function beforeEditCategory(&$route, &$args)
    {
        if (!is_array($args) || count($args) > 2) {
            return;
        }
        $category_id = $args[0];
        $data = $args[1];

        // generate seo url
        $this->load->model('custom/common');
        $slug = $this->model_custom_common->createSlug($data['name'], '-');

        $language_id = $this->config->get('config_language_id');
        $alias = 'category_id=' . $category_id;
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT `keyword` FROM `{$DB_PREFIX}seo_url` WHERE `language_id` = '{$language_id}' AND `query` = '" . $alias . "' AND `table_name` = 'category' LIMIT 1";
        $query = $this->db->query($sql);

        if (!empty($query->row) && $query->row['keyword'] != $slug) {
            // update sitemap lastmod
            $this->updateSitemapLastmod('categories');
        }
    }

    // admin/model/catalog/category/deleteCategory/after
    public function afterDeleteCategory(&$route, &$args, &$output)
    {
        if (!$output) {
            return;
        }

        // update sitemap lastmod
        $this->updateSitemapLastmod('categories');
    }

    // admin/model/blog/category/beforeEditBlogCategory/before
    public function beforeEditBlogCategory(&$route, &$args)
    {
        if (!is_array($args) || count($args) > 1) {
            return;
        }
        $data = $args[0];
        $blog_category_id = $data['blog_category_id'];

        // generate seo url
        $this->load->model('custom/common');
        $slug = $this->model_custom_common->createSlug($data['name'], '-');

        $language_id = $this->config->get('config_language_id');
        $alias = 'blog_category_id=' . $blog_category_id;
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT `keyword` FROM `{$DB_PREFIX}seo_url` WHERE `language_id` = '{$language_id}' AND `query` = '" . $alias . "' AND `table_name` = 'blog_category' LIMIT 1";
        $query = $this->db->query($sql);

        if (!empty($query->row) && $query->row['keyword'] != $slug) {
            // update sitemap lastmod
            $this->updateSitemapLastmod('blog_categories');
        }
    }

    // admin/model/catalog/product/addProduct/after
    public function afterAddProduct(&$route, &$args, &$output)
    {
        if (empty($output)) {
            return;
        }

        // update sitemap lastmod
        $this->updateFirstLevelProductCategory($output);
    }

    // admin/model/catalog/product/editProduct/after
    public function afterEditProduct(&$route, &$args)
    {
        if (!is_array($args) || count($args) > 2) {
            return;
        }
        $product_id = $args[0];

        // generate seo url
        $should_change_sitemap = $this->session->data['product_' . $product_id . '_change_sitemap'];

        if ($should_change_sitemap) {
            // delete session
            $this->session->data['product_' . $product_id . '_change_sitemap'] = null;

            $this->updateFirstLevelProductCategory($product_id);
        }
    }

    // admin/model/catalog/product/deleteProduct/before
    public function beforeDeleteProduct(&$route, &$args)
    {
        if (!is_array($args) || count($args) > 2 || !is_numeric($args[0])) {
            return;
        }

        $product_id = $args[0];

        // update sitemap lastmod
        $this->updateFirstLevelProductCategory($product_id);
    }

    // admin/model/blog/blog/addBlog/after
    public function afterAddBlog(&$route, &$args, &$output)
    {
        if (empty($output)) {
            return;
        }

        // update sitemap lastmod
        $this->updateFirstLevelBlogCategory($output);
    }

    // admin/model/blog/blog/editBlog/after
    public function afterEditBlog(&$route, &$args)
    {
        if (!is_array($args) || count($args) > 2) {
            return;
        }
        $blog_id = $args[0];

        // generate seo url
        $should_change_sitemap = $this->session->data['blog_' . $blog_id . '_change_sitemap'];

        if ($should_change_sitemap) {
            // delete session
            $this->session->data['blog_' . $blog_id . '_change_sitemap'] = null;

            $this->updateFirstLevelBlogCategory($blog_id);
        }
    }

    // admin/model/blog/blog/deleteBlog/before
    public function beforeDeleteBlog(&$route, &$args)
    {
        if (!is_array($args) || count($args) > 2 || !is_numeric($args[0])) {
            return;
        }

        $blog_id = $args[0];

        // update sitemap lastmod
        $this->updateFirstLevelBlogCategory($blog_id);
    }

    // admin/model/catalog/manufacturer/addManufacturer/after
    public function afterAddManufacturer(&$route, &$args, &$output)
    {
        if (empty($output)) {
            return;
        }

        // update sitemap lastmod
        $this->updateSitemapLastmod('manufacturers');
    }

    // admin/model/catalog/manufacturer/editManufacturer/before
    public function beforeEditManufacturer(&$route, &$args)
    {
        if (!is_array($args) || count($args) > 2) {
            return;
        }
        $manufacturer_id = $args[0];
        $data = $args[1];

        // generate seo url
        $this->load->model('custom/common');
        $slug = $this->model_custom_common->createSlug($data['name'], '-');

        $language_id = $this->config->get('config_language_id');
        $alias = 'manufacturer_id=' . $manufacturer_id;
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT `keyword` FROM `{$DB_PREFIX}seo_url` WHERE `language_id` = '{$language_id}' AND `query` = '" . $alias . "' AND `table_name` = 'manufacturer' LIMIT 1";
        $query = $this->db->query($sql);

        if (!empty($query->row) && $query->row['keyword'] != $slug) {
            // update sitemap lastmod
            $this->updateSitemapLastmod('manufacturers');
        }
    }
}