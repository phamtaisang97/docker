<?php

class ControllerEventStatistics extends Controller
{
    use Async_Callback_Util;

    const COMMENT_ADD_ORDER = 'tạo';
    const COMMENT_UPDATE_ORDER = 'cập nhật';
    const COMMENT_CHANGE_STATUS = 'trạng thái';

    // model/catalog/review/removeReview/after
    public function removeReview(&$route, &$args, &$output)
    {
        $this->load->model('setting/statistics');

        $this->model_report_statistics->addValue('review', 1);
    }

    // model/sale/return/removeReturn/after
    public function removeReturn(&$route, &$args, &$output)
    {
        $this->load->model('setting/statistics');

        $this->model_report_statistics->addValue('return', 1);
    }

    // admin/model/sale/order/addOrder/after
    public function createOrder(&$route, &$args, &$output)
    {
        $this->load->model('sale/order');
        $this->load->model('sale/order_history');
        $order = $this->model_sale_order->getOrderById($output);
        if (empty($order)) {
            return;
        }

        $data = [
            'user_name' => $this->user->getUserName(),
            'user_id' => $this->user->getId(),
            'order_id' => $output,
            'order_status_id' => $order['order_status_id'],
            'notify' => 1,
            'comment' => ModelSaleOrderHistory::COMMENT_ADD_ORDER,
            'status' => 1
        ];

        /* create order history */
        $this->createOrderHistory($data);

        if ($order['order_status_id'] != ModelSaleOrder::ORDER_STATUS_ID_DRAFT) {
            $this->updateOrdersReport($order, $order['order_status_id']);
        }

        if ($order['order_status_id'] == ModelSaleOrder::ORDER_STATUS_ID_PROCESSING) {
            $this->model_sale_order->updateProductOrderChangeStatus($output);
        }
    }

    // admin/model/sale/order/updateProductOrder/after
    public function editOrder(&$route, &$args, &$output)
    {
        if (!is_array($args) || count($args) < 2) {
            return;
        }

        $this->load->model('sale/order');
        $this->load->model('sale/order_history');
        $order_id = $args[1];
        $order = $this->model_sale_order->getOrderById($order_id);
        if (empty($order)) {
            return;
        }

        $data = [
            'user_name' => $this->user->getUserName(),
            'user_id' => $this->user->getId(),
            'order_id' => $order_id,
            'order_status_id' => $order['order_status_id'],
            'notify' => 1,
            'comment' => ModelSaleOrderHistory::COMMENT_UPDATE_ORDER,
            'status' => 2
        ];

        /* create order history */
        $this->createOrderHistory($data);

        /* callback S2S for order change */
        $this->callbackChatbotNovaonOnOrderChanged($order_id, $order);
    }

    // admin/model/sale/order/editOrder/after
    public function editOrderDetail(&$route, &$args, &$output)
    {
        if (!is_array($args) || count($args) < 1) {
            return;
        }

        $this->load->model('sale/order_history');
        $this->load->model('sale/order');
        $order_id = $args[0];
        $order = $this->model_sale_order->getOrderById($order_id);

        if (!isset($args[1]['old_order_status_id'])) {
            return;
        }

        $old_order_status_id = $args[1]['old_order_status_id'];

        if (empty($order)) {
            return;
        }

        $data = [
            'user_name' => $this->user->getUserName(),
            'user_id' => $this->user->getId(),
            'order_id' => $order_id,
            'order_status_id' => $order['order_status_id'],
            'notify' => 1,
            'comment' => ModelSaleOrderHistory::COMMENT_UPDATE_ORDER,
            'status' => 2
        ];
        /* create order history */
        $this->createOrderHistory($data);

        /* update orders report ONLY changed from draft to other */
        if ($old_order_status_id == ModelSaleOrder::ORDER_STATUS_ID_DRAFT &&
            $args[1]['order_status'] !== ModelSaleOrder::ORDER_STATUS_ID_DRAFT
        ) {
            $this->updateOrdersReport($order, $args[1]['order_status']);
        }

        /* update revenue report */
        $this->updateRevenueReport($order, $old_order_status_id);

        /* update product quantity */
        // admin/model/sale/order.php dòng 140 có cộng lại quantity
        if ($args[1]['order_status'] == ModelSaleOrder::ORDER_STATUS_ID_PROCESSING || $old_order_status_id == ModelSaleOrder::ORDER_STATUS_ID_PROCESSING) {
            $this->model_sale_order->updateProductOrderChangeStatus($order_id);
            // $this->model_sale_order->updateOrderPaymentStatus($order_id, 1);
        }
        if (($old_order_status_id == ModelSaleOrder::ORDER_STATUS_ID_COMPLETED || $old_order_status_id == ModelSaleOrder::ORDER_STATUS_ID_PROCESSING || $old_order_status_id == ModelSaleOrder::ORDER_STATUS_ID_DELIVERING) && $args[1]['order_status'] == ModelSaleOrder::ORDER_STATUS_ID_CANCELLED) {
            $this->model_sale_order->updateProductOrderChangeStatus($order_id, false);
        }
        if ($order['order_status_id'] != $old_order_status_id) {
            $this->model_sale_order->updatePreviousOrderStatusId($order_id, $old_order_status_id);
        }

        /* callback S2S for order change */
        $this->callbackChatbotNovaonOnOrderChanged($order_id, $order);
    }

    //admin/model/sale/order/editOrder/before
    public function updateStatusOrderDetail(&$route, &$args)
    {
        if (!is_array($args) || count($args) < 2 ||
            !isset($args[1]['order_status'])
        ) {
            return;
        }
        $this->load->model('sale/order_history');
        $this->load->model('sale/order');
        $order_id = $args[0];
        $order = $this->model_sale_order->getOrderById($order_id);

        if (empty($order)) {
            return;
        }

        if ($order['order_status_id'] != $args[1]['order_status']) {
            $data = [
                'user_name' => $this->user->getUserName(),
                'user_id' => $this->user->getId(),
                'order_id' => $order_id,
                'order_status_id' => $args[1]['order_status'],
                'notify' => 1,
                'comment' => ModelSaleOrderHistory::COMMENT_CHANGE_STATUS,
                'status' => 3
            ];

            /* create order history */
            $this->createOrderHistory($data);
        }

        /* callback S2S for order change */
        $this->callbackChatbotNovaonOnOrderChanged($order_id, $order);
    }

    // admin/model/sale/order/updateStatus/after
    public function changeStatus(&$route, &$args, &$output)
    {
        if (!is_array($args) || count($args) < 2 ||
            !isset($args[0]['order_id']) || !isset($args[0]['order_status_id'])
        ) {
            return;
        }
        $this->load->model('sale/order_history');
        $this->load->model('sale/order');

        $old_order_status_id = $args[1];
        $order_id = $args[0]['order_id'];

        $order = $this->model_sale_order->getOrderById($order_id);
        if (empty($order)) {
            return;
        }

        $data = [
            'user_name' => $this->user->getUserName(),
            'user_id' => $this->user->getId(),
            'order_id' => $order_id,
            'order_status_id' => $args[0]['order_status_id'],
            'notify' => 1,
            'comment' => ModelSaleOrderHistory::COMMENT_CHANGE_STATUS,
            'status' => 3
        ];

        /* create order history */
        $this->createOrderHistory($data);

        /* update revenue report */
        $this->updateRevenueReport($order, $old_order_status_id);

        /* update orders report ONLY changed from draft to other */
        if ($old_order_status_id == ModelSaleOrder::ORDER_STATUS_ID_DRAFT &&
            $args[0]['order_status_id'] !== ModelSaleOrder::ORDER_STATUS_ID_DRAFT
        ) {
            $this->updateOrdersReport($order, $args[0]['order_status_id']);
        }

        /* update product quantity */
        // được gọi sau event chỉ thay đổi trạng thái đơn nên chỉ update quanitty khi status = 7
        if ($args[0]['order_status_id'] == ModelSaleOrder::ORDER_STATUS_ID_PROCESSING) {
            $this->model_sale_order->updateProductOrderChangeStatus($order_id);
            // $this->model_sale_order->updateOrderPaymentStatus($order_id, 1);
        }
        if (($old_order_status_id == ModelSaleOrder::ORDER_STATUS_ID_COMPLETED || $old_order_status_id == ModelSaleOrder::ORDER_STATUS_ID_PROCESSING || $old_order_status_id == ModelSaleOrder::ORDER_STATUS_ID_DELIVERING) && $args[0]['order_status_id'] == ModelSaleOrder::ORDER_STATUS_ID_CANCELLED) {
            $this->model_sale_order->updateProductOrderChangeStatus($order_id, false);
        }
        $this->model_sale_order->updatePreviousOrderStatusId($order_id, $old_order_status_id);

        /* callback S2S for order change */
        $this->callbackChatbotNovaonOnOrderChanged($order_id, $order);
    }

    // admin/model/sale/order/deleteOrderById/after
    public function deleteOrder(&$route, &$args, &$output)
    {
        if (!is_array($args) || count($args) < 1 ||
            !isset($args[0]['order_id'])
        ) {
            return;
        }
        $this->load->model('sale/order_history');
        $this->load->model('sale/order');

        $order = $this->model_sale_order->getOrderById($args[0]);
        if (empty($order)) {
            return;
        }
        // dummy order data
        $order = [
            'order_id' => $args[0]['order_id'],
            'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_DELETED
        ];

        /* update revenue report */
        $this->updateRevenueReport($order);

        /* update orders report */
        $this->updateOrdersReport($order, $order['order_status_id']);
    }
    ///
    ///
    private function createOrderHistory($data)
    {
        $this->load->model('sale/order_history');

        $this->model_sale_order_history->addHistory($data);
    }

    private function updateRevenueReport(array $order, $old_order_status_id = '')
    {
        if (!isset($order['order_status_id'])) {
            return;
        }

        switch ($order['order_status_id']) {
            case ModelSaleOrder::ORDER_STATUS_ID_COMPLETED:
                $this->doUpdateRevenueReport($order, true);

                break;

            case ModelSaleOrder::ORDER_STATUS_ID_CANCELLED:
                if ($old_order_status_id == ModelSaleOrder::ORDER_STATUS_ID_COMPLETED) {
                    $this->doUpdateRevenueReport($order, false);
                }

                break;
        }
    }

    private function doUpdateRevenueReport(array $order, $increase = true)
    {
        $this->load->model('report/overview');

        try {
            $report_time = date_create_from_format('Y-m-d H:i:s', $order['date_added']);
            $report_time->setTime($report_time->format('H'), 0, 0);
        } catch (Exception $e) {
            return -1;
        }

        $data = [
            'report_time' => $report_time->format('Y-m-d H:i:s'),
            'm_value' => $order['total'],
            'report_date' => $report_time->format('Y-m-d'),
        ];

        return $this->model_report_overview->increaseOrDecreaseRevenue($data, $increase);
    }

    private function updateOrdersReport(array $order, $orderStatusId)
    {
        switch ($orderStatusId) {
            case ModelSaleOrder::ORDER_STATUS_ID_PROCESSING:
            case ModelSaleOrder::ORDER_STATUS_ID_DELIVERING:
            case ModelSaleOrder::ORDER_STATUS_ID_COMPLETED:
                $this->doUpdateOrdersReport($order, true);

                break;

            case ModelSaleOrder::ORDER_STATUS_ID_CANCELLED:
            case ModelSaleOrder::ORDER_STATUS_ID_DELETED:
                // $this->doUpdateOrdersReport($order, false);

                break;
        }
    }

    private function doUpdateOrdersReport(array $order, $increase = true)
    {
        $this->load->model('report/overview');
        try {
            $report_time = date_create_from_format('Y-m-d H:i:s', $order['date_added']);
            $report_time->setTime($report_time->format('H'), 0, 0);
        } catch (Exception $e) {
            return -1;
        }

        $data = [
            'report_time' => $report_time->format('Y-m-d H:i:s'),
            'm_value' => 1,
            'report_date' => $report_time->format('Y-m-d'),
        ];

        return $this->model_report_overview->increaseOrDecreaseOrdersReport($data, $increase);
    }

    /**
     * @param int $order_id
     * @param array $order empty if need to query order by id
     * @throws Exception
     */
    private function callbackChatbotNovaonOnOrderChanged($order_id, $order = [])
    {
        try {
            // get order if not give
            $this->load->model('sale/order');
            if (!is_array($order) || empty($order)) {
                $order = $this->model_sale_order->getOrderById($order_id);
                if (!is_array($order) || empty($order)) {
                    return;
                }
            }

            /* allow order status changed to DELIVERING OR COMPLETE OR CANCELLED */
            $prev_order_status_id = isset($order['previous_order_status_id']) ? (int)$order['previous_order_status_id'] : -99;
            $order_status_id = isset($order['order_status_id']) ? (int)$order['order_status_id'] : -99;
            $need_call_back = false;
            if ($prev_order_status_id != ModelSaleOrder::ORDER_STATUS_ID_DELIVERING &&
                $order_status_id == ModelSaleOrder::ORDER_STATUS_ID_DELIVERING
            ) {
                $need_call_back = true;
            }

            if ($prev_order_status_id != ModelSaleOrder::ORDER_STATUS_ID_COMPLETED &&
                $order_status_id == ModelSaleOrder::ORDER_STATUS_ID_COMPLETED
            ) {
                $need_call_back = true;
            }

            if ($prev_order_status_id != ModelSaleOrder::ORDER_STATUS_ID_CANCELLED &&
                $order_status_id == ModelSaleOrder::ORDER_STATUS_ID_CANCELLED
            ) {
                $need_call_back = true;
            }

            if (!$need_call_back) {
                return;
            }

            // do getting order detail
            $data_filter = [];
            $data_filter['order_ids'] = $order_id;

            $this->load->model('api/chatbox_novaon_v2');
            $orders = $this->model_api_chatbox_novaon_v2->getOrdersListData($data_filter);
            $order_data = [];
            if (isset($orders['records']) && is_array($orders['records']) && count($orders['records']) > 0) {
                $order_data = $orders['records'][0];
            }

            /* skip not found order */
            if (!is_array($order_data) || empty($order_data)) {
                return;
            }

            $this->load->model('setting/setting');
            $shop_name = $this->model_setting_setting->getShopName();

            $data = [
                'order_id' => $order_id,
                'shop_id' => SHOP_ID,
                'shop_name' => $shop_name,
                'data' => $order_data,
            ];

            $this->queueAsyncChatbotNovaonV2OrderNotify($data);
        } catch (Exception $e) {
            // log error...
        }
    }
}