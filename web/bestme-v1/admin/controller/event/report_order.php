<?php

class ControllerEventReportOrder extends Controller
{

    use sync_bestme_data_to_welcome_util;

    /**
     * @param $route
     * @param $args
     * @param $output
     * @throws Exception
     * Create order report after create order
     */
    public function afterCreateOrder(&$route, &$args, &$output)
    {
        if (empty($output)) {
            return;
        }
        $this->load->model('sale/order');
        $this->load->model('report/order');
        $data_order = $this->model_sale_order->getOrder($output);
        $data_order_product = $this->model_sale_order->getOrderProducts($output);
        $total_product_discount = array_sum(array_column($data_order_product, 'discount'));

        if ($data_order && $data_order['order_status'] == ModelSaleOrder::ORDER_STATUS_ID_PROCESSING) {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $report_time = new DateTime();
            $report_time->setTime($report_time->format('H'), 0, 0);
            $data_order_report = [
                'report_time' => $report_time->format('Y-m-d H:i:s'),
                'report_date' => $report_time->format('Y-m-d'),
                'order_id' => $output,
                'customer_id' => $data_order['customer_id'],
                'total_amount' => $data_order['total'],
                'order_status' => $data_order['order_status'],
                'source' => $data_order['source'],
                'shipping_fee' => $data_order['shipping_fee'],
                'discount' => $data_order['discount'] + $total_product_discount,
            ];
            $this->model_report_order->saveOrderReport($data_order_report);
            if (!in_array($data_order['source'], ['shopee', 'lazada'])){
                // sync shop order to welcome
                $data_order_report = [
                    'type' => 'add',
                    'new_status' => $data_order['order_status'],
                    'old_status' => null,
                    'new_value' => $data_order['total'],
                    'old_value' => null,
                    'time' => $report_time->format('Y-m-d H:i:s')
                ];
                $this->publishOrder($data_order_report);
            }
        }
    }


    /**
     * @param $route
     * @param $args
     * @param $output
     * listen: admin/model/sale/return_receipt/addReturnReceipt/after
     */
    public function afterCreateReturnReceipt(&$route, &$args, &$output)
    {
        $this->load->model('report/order');
        $this->load->model('report/product');

        //update in report_product table
        $this->model_report_product->updateProductReport($args[0], $args[1]);

        // update total return in report_order table
        $this->model_report_order->updateTotalReturnOrderReport($args[0], $output['total_return']);
    }

    /**
     * @param $route
     * @param $args
     * @param $output
     * @throws Exception
     * Update order report when change order in order list
     */
    public function editOrderInOrderList(&$route, &$args, &$output)
    {
        if (!is_array($args) || !isset($args[0])) {
            return;
        }
        $this->load->model('sale/order');
        $this->load->model('report/order');
        $dataOrder = $this->model_sale_order->getOrder($args[0]);
        if($dataOrder) {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $reportTime = new DateTime();
            $reportTime->setTime($reportTime->format('H'), 0, 0);
            $dataOrderReport = [
                'report_time' => $reportTime->format('Y-m-d H:i:s'),
                'report_date' => $reportTime->format('Y-m-d'),
                'order_id' => $args[0],
                'customer_id' => $dataOrder['customer_id'],
                'total_amount' => $dataOrder['total'],
                'order_status' => $dataOrder['order_status'],
                'shipping_fee' => $dataOrder['shipping_fee'],
                'discount' => $dataOrder['discount'],
            ];
            $report = $this->model_report_order->getReportByOrderId($dataOrderReport['order_id']);
            $old_status = isset($report['order_status']) ? $report['order_status'] : null;
            $old_value = isset($report['total_amount']) ? $report['total_amount'] : null;
            $time = isset($report['report_time']) ? $report['report_time'] : $reportTime->format('Y-m-d H:i:s');

            $this->model_report_order->updateOrderReport($dataOrderReport);
            if (!in_array($dataOrder['source'], ['shopee', 'lazada'])){
                // sync shop order to welcome
                $dataOrderReport = [
                    'type' => 'edit',
                    'new_status' => $dataOrder['order_status'],
                    'old_status' => $old_status,
                    'new_value' => $dataOrder['total'],
                    'old_value' => $old_value,
                    'time' => $time
                ];
                $this->publishOrder($dataOrderReport);
            }
        }
    }

    /**
     * @param $route
     * @param $args
     * @param $output
     * @throws Exception
     * Update when change status in order list
     */
    public function updateOrderStatusInOrderList(&$route, &$args, &$output)
    {
        if (!is_array($args) || !isset($args[0]['order_id']) || !isset($args[1])) {
            return;
        }
        $this->load->model('sale/order');
        $this->load->model('report/order');
        $dataOrder = $this->model_sale_order->getOrder($args[0]['order_id']);
        $data_order_product = $this->model_sale_order->getOrderProducts($args[0]['order_id']);
        $total_product_discount = array_sum(array_column($data_order_product, 'discount'));

        if ($dataOrder) {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $reportTime = new DateTime();
            $reportTime->setTime($reportTime->format('H'), 0, 0);
            $dataOrderReport = [
                'report_time' => $reportTime->format('Y-m-d H:i:s'),
                'report_date' => $reportTime->format('Y-m-d'),
                'order_id' => $args[0]['order_id'],
                'customer_id' => $dataOrder['customer_id'],
                'total_amount' => $dataOrder['total'],
                'order_status' => $dataOrder['order_status'],
                'shipping_fee' => $dataOrder['shipping_fee'],
                'discount' => $dataOrder['discount'] + $total_product_discount,
                'source' => $dataOrder['source'],
            ];

            if ($args[1] == ModelSaleOrder::ORDER_STATUS_ID_DRAFT && $args[0]['order_status_id'] == ModelSaleOrder::ORDER_STATUS_ID_PROCESSING) {
                $this->model_report_order->saveOrderReport($dataOrderReport);
                if (!in_array($dataOrder['source'], ['shopee', 'lazada'])){
                    // sync shop order to welcome
                    $dataOrderReport = [
                        'type' => 'add',
                        'new_status' => $dataOrder['order_status'],
                        'old_status' => null,
                        'new_value' => $dataOrder['total'],
                        'old_value' => null,
                        'time' => $reportTime->format('Y-m-d H:i:s')
                    ];
                    $this->publishOrder($dataOrderReport);
                }
            } else {
                $report = $this->model_report_order->getReportByOrderId($dataOrderReport['order_id']);
                $old_status = isset($report['order_status']) ? $report['order_status'] : null;
                $old_value = isset($report['total_amount']) ? $report['total_amount'] : null;
                $time = isset($report['report_time']) ? $report['report_time'] : $reportTime->format('Y-m-d H:i:s');

                $this->model_report_order->updateStatusOrderReport($dataOrderReport);
                if (!in_array($dataOrder['source'], ['shopee', 'lazada'])){
                    // sync shop order to welcome
                    $dataOrderReport = [
                        'type' => 'edit',
                        'new_status' => $dataOrder['order_status'],
                        'old_status' => $old_status,
                        'new_value' => $dataOrder['total'],
                        'old_value' => $old_value,
                        'time' => $time
                    ];
                    $this->publishOrder($dataOrderReport);
                }
            }
        }
    }

    /**
     * @param $route
     * @param $args
     * @param $output
     * @throws Exception
     * Update or create report order when edit order in detail
     */
    public function editOrderInOrderDetail(&$route, &$args, &$output)
    {
        if (empty($output) || !isset($args[1]['old_order_status_id'])) {
            return;
        }
        $this->load->model('sale/order');
        $this->load->model('report/order');
        $data_order = $this->model_sale_order->getOrder($output);
        $data_order_product = $this->model_sale_order->getOrderProducts($output);
        $total_product_discount = array_sum(array_column($data_order_product, 'discount'));

        if ($data_order) {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $report_time = new DateTime();
            $report_time->setTime($report_time->format('H'), 0, 0);
            $data_order_report = [
                'report_time' => $report_time->format('Y-m-d H:i:s'),
                'report_date' => $report_time->format('Y-m-d'),
                'order_id' => $output,
                'customer_id' => $data_order['customer_id'],
                'total_amount' => $data_order['total'],
                'order_status' => $data_order['order_status'],
                'shipping_fee' => $data_order['shipping_fee'],
                'discount' => $data_order['discount'] + $total_product_discount,
                'source' => $data_order['source'],
            ];
            if ($data_order['order_status'] != ModelSaleOrder::ORDER_STATUS_ID_DRAFT && $args[1]['old_order_status_id'] == ModelSaleOrder::ORDER_STATUS_ID_DRAFT) {
                $this->model_report_order->saveOrderReport($data_order_report);
                if (!in_array($data_order['source'], ['shopee', 'lazada'])){
                    // sync shop order to welcome
                    $data_order_report = [
                        'type' => 'add',
                        'new_status' => $data_order['order_status'],
                        'old_status' => null,
                        'new_value' => $data_order['total'],
                        'old_value' => null,
                        'time' => $report_time->format('Y-m-d H:i:s')
                    ];
                    $this->publishOrder($data_order_report);
                }
            } else {
                $report = $this->model_report_order->getReportByOrderId($data_order_report['order_id']);
                $old_status = isset($report['order_status']) ? $report['order_status'] : null;
                $old_value = isset($report['total_amount']) ? $report['total_amount'] : null;
                $time = isset($report['report_time']) ? $report['report_time'] : $report_time->format('Y-m-d H:i:s');

                $this->model_report_order->saveCASOrderReport($data_order_report);
                if (!in_array($data_order['source'], ['shopee', 'lazada'])){
                    // sync shop order to welcome
                    $data_order_report = [
                        'type' => 'edit',
                        'new_status' => $data_order['order_status'],
                        'old_status' => $old_status,
                        'new_value' => $data_order['total'],
                        'old_value' => $old_value,
                        'time' => $time
                    ];
                    $this->publishOrder($data_order_report);
                }
            }
        }
    }

    private function publishOrder($data)
    {
        $this->load->model('setting/setting');

        $data['shop_name'] = $this->model_setting_setting->getShopName();

        $this->publishSyncShopOrder($data);
    }
}