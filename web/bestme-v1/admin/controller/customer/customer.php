<?php

class ControllerCustomerCustomer extends Controller
{
    use Theme_Config_Util;
    private $error = array();

    public function index()
    {
        $this->load->language('customer/customer');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('customer/customer');

        $this->getList();
    }

    public function add()
    {
        $this->load->language('customer/customer');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('customer/customer');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            //if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $this->model_customer_customer->addCustomer($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('customer/customer', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();
    }

    public function edit()
    {
        $this->load->language('customer/customer');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('customer/customer');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
//		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $this->model_customer_customer->editCustomer($this->request->get['customer_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('customer/customer', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();
    }

    public function delete()
    {
        $this->load->language('customer/customer');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('customer/customer');
        $this->response->addHeader('Content-Type: application/json');
        $json = [
            'status' => false,
            'message' => $this->language->get('text_delete_fail'),
        ];
        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $customer_id) {
                $this->model_customer_customer->deleteCustomer($customer_id);
            }

            $json = [
                'status' => true,
                'message' => $this->language->get('text_delete_success'),
            ];
            $this->response->setOutput(json_encode($json));
            return;
        }
        $this->response->setOutput(json_encode($json));
    }

    public function unlock()
    {
        $this->load->language('customer/customer');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('customer/customer');

        if (isset($this->request->get['email']) && $this->validateUnlock()) {
            $this->model_customer_customer->deleteLoginAttempts($this->request->get['email']);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_email'])) {
                $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_customer_group_id'])) {
                $url .= '&filter_customer_group_id=' . $this->request->get['filter_customer_group_id'];
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['filter_ip'])) {
                $url .= '&filter_ip=' . $this->request->get['filter_ip'];
            }

            if (isset($this->request->get['filter_date_added'])) {
                $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('customer/customer', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getList();
    }

    protected function getList()
    {
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else if (isset($this->session->data['warning'])) {
            $data['error_warning'] = $this->session->data['warning'];

            unset($this->session->data['warning']);
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->get['filter_name'])) {
            $filter_name = $this->request->get['filter_name'];
        } else {
            $filter_name = '';
        }

        if (isset($this->request->get['customer_group'])) {
            $filter_customer_group_id = $this->request->get['customer_group'];
            if (!empty($filter_customer_group_id)) {
                if (count($filter_customer_group_id) > 0) {
                    $filter_customer_group_id = implode(',', ($filter_customer_group_id));
                } else {
                    $filter_customer_group_id = (int)$filter_customer_group_id[0];
                }
            } else {
                $filter_customer_group_id = '';
            }
        } else {
            $filter_customer_group_id = '';
        }

        if (isset($this->request->get['status_customer'])) {
            $filter_status = $this->request->get['status_customer'];
            if (!empty($filter_status)) {
                if (count($filter_status) > 0) {
                    $filter_status = implode(',', ($filter_status));
                } else {
                    $filter_status = (int)$filter_status[0];
                }
            } else {
                $filter_status = '';
            }
        } else {
            $filter_status = '';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data['access_modify'] = $this->user->hasPermission('modify', 'customer/customer');
        $data['add'] = $this->url->link('customer/customer/add', 'user_token=' . $this->session->data['user_token'], true);
        $data['block_href'] = $this->url->link('customer/customer/blockCustomer', 'user_token=' . $this->session->data['user_token'], true);
        $data['unblock_href'] = $this->url->link('customer/customer/unbBlockCustomer', 'user_token=' . $this->session->data['user_token'], true);
        $data['delete_href'] = $this->url->link('customer/customer/deleteCustomer', 'user_token=' . $this->session->data['user_token'], true);

        $data['customers'] = array();

        $filter_data = array(
            'filter_name' => trim($filter_name),
            'filter_customer_group_id' => $filter_customer_group_id,
            'filter_status' => $filter_status,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );
        $this->load->model('user/user');
        $admin_user = $this->model_user_user->getUser($this->user->getId());
        if ($this->user->canAccessAll() || (isset($admin_user['type']) && $admin_user['type'] == 'Admin')) {

        } else {
            $filter_data['user_create_id'] = $this->user->getId();
        }
        $customer_total = $this->model_customer_customer->getTotalCustomers($filter_data);

        $results = $this->model_customer_customer->getCustomers($filter_data);
        foreach ($results as $result) {
            $data['customers'][] = array(
                'customer_id' => $result['customer_id'],
                'code' => $result['code'],
                'full_name' => $result['name'],
                'group_name' => $result['group_name'],
                'email' => $result['email'],
                'telephone' => $result['telephone'],
                'count_order' => $result['count_order'],
                'order_total' => isset($result['order_total']) ? number_format($result['order_total'], 0, '', ',') : '',
                'status' => $result['status'],
                'detail' => $this->url->link('customer/customer/detail', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $result['customer_id'], true),
                'order_lastest_mobile' => date('d/m/Y', strtotime($result['order_lastest'])),
                'edit' => $this->url->link('customer/customer/edit', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $result['customer_id'], true),
            );
        }
        //var_dump($data['customers']);die();
        $data['user_token'] = $this->session->data['user_token'];

        $pagination = new CustomPaginate();
        $pagination->total = $customer_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('customer/customer', 'user_token=' . $this->session->data['user_token'] . '&page={page}', true);

        // Add jump to page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);

        $data['results'] = sprintf($this->language->get('text_pagination'), ($customer_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($customer_total - $this->config->get('config_limit_admin'))) ? $customer_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $customer_total, ceil($customer_total / $this->config->get('config_limit_admin')));

        $data['loadOptionOne'] = $this->url->link('customer/customer/loadoptionone', 'user_token=' . $this->session->data['user_token'], true);
        $data['url_filter'] = $this->url->link('customer/customer', 'user_token=' . $this->session->data['user_token'], true);
        $data['link_down'] = $this->config->get('config_language') == 'vi-vn' ? SAMPLE_CUSTOMER_LIST_UPLOAD : SAMPLE_CUSTOMER_LIST_UPLOAD_EN;
        $data['action'] = $this->url->link('customer/customer', 'user_token=' . $this->session->data['user_token'], true);
        $data['link_list_email_subscribers'] = $this->url->link('customer/customer/listEmailSubscribers', 'user_token=' . $this->session->data['user_token'], true);
        if ($this->isSupportThemeFeature(self::$EMAIL_SUBSCRIBERS)) {
            $data['email_subscribers'] = true;
        }
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('customer/customer_list_append', $data));
        } else {
            $this->response->setOutput($this->load->view('customer/customer_list', $data));
        }
    }

    public function detail()
    {
        $this->load->language('customer/customer');
        $data = [];

        $this->document->setTitle($this->language->get('text_detail_customer'));

        $data['user_token'] = $this->session->data['user_token'];
        $this->load->model('customer/customer');
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else if (isset($this->session->data['warning'])) {
            $data['error_warning'] = $this->session->data['warning'];

            unset($this->session->data['warning']);
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->get['customer_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $customer_info = $this->model_customer_customer->getCustomer($this->request->get['customer_id']);
        } else {
            $this->response->redirect($this->url->link('customer/customer', 'user_token=' . $this->session->data['user_token'], true));
        }

        if (isset($customer_info['customer_group_id']) && $customer_info['customer_group_id']) {
            $this->load->model('customer/customer_group');
            $customer_info['customer_group_name'] = $this->model_customer_customer_group->getCustomerGroupNameById($customer_info['customer_group_id']);
        }

        if (isset($customer_info['staff_in_charge']) && $customer_info['staff_in_charge']) {
            $this->load->model('user/user');
            $user_info = $this->model_user_user->getUser($customer_info['staff_in_charge']);
            if (array_key_exists('firstname', $user_info) && array_key_exists('lastname', $user_info)) {
                $customer_info['staff_in_charge'] = $user_info['lastname'] . $user_info['firstname'];
            }
        }

        $data['info'] = $customer_info;
        $data['access_modify'] = $this->user->hasPermission('modify', 'customer/customer');
        $data['href_product_list'] = $this->url->link('customer/customer', 'user_token=' . $this->session->data['user_token'], true);
        $data['edit'] = $this->url->link('customer/customer/edit', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $this->request->get['customer_id'], true);
        $data['address_href'] = $this->url->link('customer/customer/customerAddress', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $this->request->get['customer_id'], true);
        $data['history_href'] = $this->url->link('customer/customer/history', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $this->request->get['customer_id'], true);

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('customer/customer_detail', $data));
    }

    public function customerAddress()
    {
        $this->load->language('customer/customer');
        $this->load->model('customer/customer');

        $this->document->setTitle($this->language->get('text_address_book'));

        $data = [];

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else if (isset($this->session->data['warning'])) {
            $data['error_warning'] = $this->session->data['warning'];

            unset($this->session->data['warning']);
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $customer_id = isset($this->request->get['customer_id']) ? $this->request->get['customer_id'] : '';

        $data['access_modify'] = $this->user->hasPermission('modify', 'customer/customer');

        $data['address_books'] = $this->model_customer_customer->getAddressBook($customer_id);
        $data['max_address'] = (is_array($data['address_books']) && count($data['address_books']) >= 10) ? false : true;

        $data['user_token'] = $this->session->data['user_token'];

        $data['href_customer_list'] = $this->url->link('customer/customer', 'user_token=' . $this->session->data['user_token'], true);
        $data['detail_href'] = $this->url->link('customer/customer/detail', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $this->request->get['customer_id'], true);
        $data['history_href'] = $this->url->link('customer/customer/history', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $this->request->get['customer_id'], true);
        $data['add_new_address'] = $this->url->link('customer/customer/addNewAddress', 'user_token=' . $this->session->data['user_token']. '&customer_id=' . $this->request->get['customer_id'], true);
        $data['edit_address'] = $this->url->link('customer/customer/editAddress', 'user_token=' . $this->session->data['user_token']. '&customer_id=' . $this->request->get['customer_id'], true);

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('customer/customer_address', $data));
    }


    public function addNewAddress()
    {
        $this->load->language('customer/customer');
        $this->load->model('customer/customer');

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $this->model_customer_customer->addNewAddressBook($this->request->post, $this->request->get['customer_id']);
            $this->session->data['success'] = $this->language->get('text_form_add_address_success');
        }

        $this->response->redirect($this->url->link('customer/customer/customerAddress', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $this->request->get['customer_id'], true));
    }

    public function editAddress()
    {
        $this->load->language('customer/customer');
        $this->load->model('customer/customer');

        if (!isset($this->request->post['address_id'])) {
            $this->session->data['warning'] = $this->language->get('text_error_not_found_address');
            $this->response->redirect($this->url->link('customer/customer/customerAddress', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $this->request->get['customer_id'], true));
        }

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $this->model_customer_customer->editAddressBook($this->request->post, $this->request->get['customer_id']);
            $this->session->data['success'] = $this->language->get('text_form_edit_address_success');
        }

        $this->response->redirect($this->url->link('customer/customer/customerAddress', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $this->request->get['customer_id'], true));
    }

    protected function getForm()
    {
        $data['text_form'] = !isset($this->request->get['customer_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

        $data['user_token'] = $this->session->data['user_token'];

        if (isset($this->request->get['customer_id'])) {
            $data['customer_id'] = $this->request->get['customer_id'];
        } else {
            $data['customer_id'] = 0;
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else if (isset($this->session->data['warning'])) {
            $data['error_warning'] = $this->session->data['warning'];

            unset($this->session->data['warning']);
        } else {
            $data['error_warning'] = '';
        }

        // todo: remove
//		if (isset($this->error['firstname'])) {
//			$data['error_firstname'] = $this->error['firstname'];
//		} else {
//			$data['error_firstname'] = '';
//		}
//
//		if (isset($this->error['lastname'])) {
//			$data['error_lastname'] = $this->error['lastname'];
//		} else {
//			$data['error_lastname'] = '';
//		}
//
//		if (isset($this->error['full_name'])) {
//			$data['error_full_name'] = $this->error['full_name'];
//		} else {
//			$data['error_full_name'] = '';
//		}
//
//		if (isset($this->error['email'])) {
//			$data['error_email'] = $this->error['email'];
//		} else {
//			$data['error_email'] = '';
//		}
//
//		if (isset($this->error['telephone'])) {
//			$data['error_telephone'] = $this->error['telephone'];
//		} else {
//			$data['error_telephone'] = '';
//		}
//
//		if (isset($this->error['cheque'])) {
//			$data['error_cheque'] = $this->error['cheque'];
//		} else {
//			$data['error_cheque'] = '';
//		}
//
//		if (isset($this->error['paypal'])) {
//			$data['error_paypal'] = $this->error['paypal'];
//		} else {
//			$data['error_paypal'] = '';
//		}
//
//		if (isset($this->error['bank_account_name'])) {
//			$data['error_bank_account_name'] = $this->error['bank_account_name'];
//		} else {
//			$data['error_bank_account_name'] = '';
//		}
//
//		if (isset($this->error['bank_account_number'])) {
//			$data['error_bank_account_number'] = $this->error['bank_account_number'];
//		} else {
//			$data['error_bank_account_number'] = '';
//		}
//
//		if (isset($this->error['password'])) {
//			$data['error_password'] = $this->error['password'];
//		} else {
//			$data['error_password'] = '';
//		}
//
//		if (isset($this->error['confirm'])) {
//			$data['error_confirm'] = $this->error['confirm'];
//		} else {
//			$data['error_confirm'] = '';
//		}
//
//		if (isset($this->error['custom_field'])) {
//			$data['error_custom_field'] = $this->error['custom_field'];
//		} else {
//			$data['error_custom_field'] = array();
//		}
//
//		if (isset($this->error['address'])) {
//			$data['error_address'] = $this->error['address'];
//		} else {
//			$data['error_address'] = array();
//		}

        $url = '';

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('customer/customer', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        if (!isset($this->request->get['customer_id'])) {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title_add')
            );
        } else {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_edit')
            );
        }


        if (!isset($this->request->get['customer_id'])) {
            $data['action'] = $this->url->link('customer/customer/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        } else {
            $data['action'] = $this->url->link('customer/customer/edit', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $this->request->get['customer_id'] . $url, true);
        }

        $data['cancel'] = $this->url->link('customer/customer', 'user_token=' . $this->session->data['user_token'] . $url, true);

        if (isset($this->request->get['customer_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $customer_info = $this->model_customer_customer->getCustomer($this->request->get['customer_id']);
        }

        $this->load->model('customer/customer_group');

        $data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();

        if (isset($this->request->post['customer_group_id'])) {
            $data['customer_group_id'] = $this->request->post['customer_group_id'];
        } elseif (!empty($customer_info)) {
            $data['customer_group_id'] = $customer_info['customer_group_id'];
        } else {
            $data['customer_group_id'] = null;//$this->config->get('config_customer_group_id');
        }

        if (isset($this->request->post['email'])) {
            $data['email'] = $this->request->post['email'];
        } elseif (!empty($customer_info)) {
            $data['email'] = $customer_info['email'];
        } else {
            $data['email'] = '';
        }

        if (isset($this->request->post['telephone'])) {
            $data['telephone'] = $this->request->post['telephone'];
        } elseif (!empty($customer_info)) {
            $data['telephone'] = $customer_info['telephone'];
        } else {
            $data['telephone'] = '';
        }

        if (isset($this->request->post['full_name'])) {
            $data['full_name'] = $this->request->post['full_name'];
        } elseif (!empty($customer_info)) {
            $data['full_name'] = (isset($customer_info['full_name']) && $customer_info['full_name'] != '') ? $customer_info['full_name'] : $customer_info['lastname'] . ' ' . $customer_info['firstname'];
        } else {
            $data['full_name'] = '';
        }

        if (isset($this->request->post['birthday'])) {
            $data['birthday'] = $this->request->post['birthday'];
        } elseif (!empty($customer_info)) {
            $data['birthday'] = $customer_info['birthday'];
        } else {
            $data['birthday'] = '';
        }

        if (isset($this->request->post['sex'])) {
            $data['sex'] = $this->request->post['sex'];
        } elseif (!empty($customer_info)) {
            $data['sex'] = $customer_info['sex'];
        } else {
            $data['sex'] = 1;
        }

        if (isset($this->request->post['website'])) {
            $data['website'] = $this->request->post['website'];
        } elseif (!empty($customer_info)) {
            $data['website'] = $customer_info['website'];
        } else {
            $data['website'] = '';
        }

        if (isset($this->request->post['tax_code'])) {
            $data['tax_code'] = $this->request->post['tax_code'];
        } elseif (!empty($customer_info)) {
            $data['tax_code'] = $customer_info['tax_code'];
        } else {
            $data['tax_code'] = '';
        }

        if (isset($this->request->post['staff_in_charge'])) {
            $data['staff_in_charge'] = $this->request->post['staff_in_charge'];
        } elseif (!empty($customer_info)) {
            $data['staff_in_charge'] = $customer_info['staff_in_charge'];
        } else {
            $data['staff_in_charge'] = '';
        }

        // Custom Fields
        $this->load->model('customer/custom_field');

        $data['custom_fields'] = array();

        $filter_data = array(
            'sort' => 'cf.sort_order',
            'order' => 'ASC'
        );

        $custom_fields = $this->model_customer_custom_field->getCustomFields($filter_data);

        foreach ($custom_fields as $custom_field) {
            $data['custom_fields'][] = array(
                'custom_field_id' => $custom_field['custom_field_id'],
                'custom_field_value' => $this->model_customer_custom_field->getCustomFieldValues($custom_field['custom_field_id']),
                'name' => $custom_field['name'],
                'value' => $custom_field['value'],
                'type' => $custom_field['type'],
                'location' => $custom_field['location'],
                'sort_order' => $custom_field['sort_order']
            );
        }

        if (isset($this->request->post['custom_field'])) {
            $data['account_custom_field'] = $this->request->post['custom_field'];
        } elseif (!empty($customer_info)) {
            $data['account_custom_field'] = json_decode($customer_info['custom_field'], true);
        } else {
            $data['account_custom_field'] = array();
        }

        if (isset($this->request->post['newsletter'])) {
            $data['newsletter'] = $this->request->post['newsletter'];
        } elseif (!empty($customer_info)) {
            $data['newsletter'] = $customer_info['newsletter'];
        } else {
            $data['newsletter'] = '';
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($customer_info)) {
            $data['status'] = $customer_info['status'];
        } else {
            $data['status'] = true;
        }

        if (isset($this->request->post['safe'])) {
            $data['safe'] = $this->request->post['safe'];
        } elseif (!empty($customer_info)) {
            $data['safe'] = $customer_info['safe'];
        } else {
            $data['safe'] = 0;
        }

        if (isset($this->request->post['password'])) {
            $data['password'] = $this->request->post['password'];
        } else {
            $data['password'] = '';
        }

        if (isset($this->request->post['confirm'])) {
            $data['confirm'] = $this->request->post['confirm'];
        } else {
            $data['confirm'] = '';
        }

        $this->load->model('localisation/country');

        $data['countries'] = $this->model_localisation_country->getCountries();

        if (isset($this->request->post['note'])) {
            $data['note'] = $this->request->post['note'];
        } elseif (!empty($customer_info)) {
            $data['note'] = $customer_info['note'];
        } else {
            $data['note'] = '';
        }

        if (isset($this->request->post['customer_source'])) {
            $data['customer_source'] = $this->request->post['customer_source'];
        } elseif (!empty($customer_info)) {
            $data['customer_source'] = $customer_info['customer_source'];
        } else {
            $data['customer_source'] = '';
        }

        if (isset($this->request->post['address_id'])) {
            $data['address_id'] = $this->request->post['address_id'];
        } elseif (!empty($customer_info)) {
            $data['address_id'] = $customer_info['address_id'];
        } else {
            $data['address_id'] = '';
        }

        $addressDefault = $this->model_customer_customer->getAddressDefaultCustomer($data['address_id']);

        if (isset($this->request->post['city'])) {
            $data['city'] = $this->request->post['city'];
        } elseif (!empty($addressDefault)) {
            $data['city'] = $addressDefault['city'];
        } else {
            $data['city'] = '';
        }

        if (isset($this->request->post['district'])) {
            $data['district'] = $this->request->post['district'];
        } elseif (!empty($addressDefault)) {
            $data['district'] = $addressDefault['district'];
        } else {
            $data['district'] = '';
        }

        if (isset($this->request->post['wards'])) {
            $data['wards'] = $this->request->post['wards'];
        } elseif (!empty($addressDefault)) {
            $data['wards'] = $addressDefault['wards'];
        } else {
            $data['wards'] = '';
        }

        if (!empty($addressDefault)) {
            $data['firstname'] = $addressDefault['firstname'];
        } else {
            $data['firstname'] = '';
        }

        if (!empty($addressDefault)) {
            $data['lastname'] = $addressDefault['lastname'];
        } else {
            $data['lastname'] = '';
        }

        if (!empty($addressDefault)) {
            $data['telephone_address_default'] = $addressDefault['phone'];
        } else {
            $data['telephone_address_default'] = '';
        }

        $this->load->model('localisation/vietnam_administrative');
        $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($data['city']);
        $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($data['district']);
        $ward = $this->model_localisation_vietnam_administrative->getWardByCode($data['wards']);

        $data['city'] = [
            'code' => $province['code'],
            'name' => $province['name']
        ];

        $data['district'] = [
            'code' => $district['code'],
            'name' => $district['name']
        ];

        $data['wards'] = [
            'code' => $ward['code'],
            'name' => $ward['name']
        ];

        if (isset($this->request->post['address'])) {
            $data['address'] = $this->request->post['address'];
        } elseif (!empty($addressDefault)) {
            $data['address'] = $addressDefault['address'];
        } else {
            $data['address'] = '';
        }

//		todo: remove
//		if(!empty($data['address_id'])){
//		    foreach ($data['extra_address'] as $k => $addresse){
//		        if($addresse['address_id'] == $data['address_id']) {
//                    $data['first_address'] = $data['extra_address'][$k];
//                    unset($data['extra_address'][$k]);
//                    break;
//                }
//            }
//        }

        // Affliate
//		if (isset($this->request->get['customer_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
//			$affiliate_info = $this->model_customer_customer->getAffiliate($this->request->get['customer_id']);
//		}
//
//		if (isset($this->request->post['affiliate'])) {
//			$data['affiliate'] = $this->request->post['affiliate'];
//		} elseif (!empty($affiliate_info)) {
//			$data['affiliate'] = $affiliate_info['status'];
//		} else {
//			$data['affiliate'] = '';
//		}
//
//		if (isset($this->request->post['company'])) {
//			$data['company'] = $this->request->post['company'];
//		} elseif (!empty($affiliate_info)) {
//			$data['company'] = $affiliate_info['company'];
//		} else {
//			$data['company'] = '';
//		}
//
////		todo: remove
////		if (isset($this->request->post['website'])) {
////			$data['website'] = $this->request->post['website'];
////		} elseif (!empty($affiliate_info)) {
////			$data['website'] = $affiliate_info['website'];
////		} else {
////			$data['website'] = '';
////		}
//
//		if (isset($this->request->post['tracking'])) {
//			$data['tracking'] = $this->request->post['tracking'];
//		} elseif (!empty($affiliate_info)) {
//			$data['tracking'] = $affiliate_info['tracking'];
//		} else {
//			$data['tracking'] = '';
//		}
//
//		if (isset($this->request->post['commission'])) {
//			$data['commission'] = $this->request->post['commission'];
//		} elseif (!empty($affiliate_info)) {
//			$data['commission'] = $affiliate_info['commission'];
//		} else {
//			$data['commission'] = $this->config->get('config_affiliate_commission');
//		}
//
//		if (isset($this->request->post['tax'])) {
//			$data['tax'] = $this->request->post['tax'];
//		} elseif (!empty($affiliate_info)) {
//			$data['tax'] = $affiliate_info['tax'];
//		} else {
//			$data['tax'] = '';
//		}
//
//		if (isset($this->request->post['payment'])) {
//			$data['payment'] = $this->request->post['payment'];
//		} elseif (!empty($affiliate_info)) {
//			$data['payment'] = $affiliate_info['payment'];
//		} else {
//			$data['payment'] = 'cheque';
//		}
//
//		if (isset($this->request->post['cheque'])) {
//			$data['cheque'] = $this->request->post['cheque'];
//		} elseif (!empty($affiliate_info)) {
//			$data['cheque'] = $affiliate_info['cheque'];
//		} else {
//			$data['cheque'] = '';
//		}
//
//		if (isset($this->request->post['paypal'])) {
//			$data['paypal'] = $this->request->post['paypal'];
//		} elseif (!empty($affiliate_info)) {
//			$data['paypal'] = $affiliate_info['paypal'];
//		} else {
//			$data['paypal'] = '';
//		}
//
//		if (isset($this->request->post['bank_name'])) {
//			$data['bank_name'] = $this->request->post['bank_name'];
//		} elseif (!empty($affiliate_info)) {
//			$data['bank_name'] = $affiliate_info['bank_name'];
//		} else {
//			$data['bank_name'] = '';
//		}
//
//		if (isset($this->request->post['bank_branch_number'])) {
//			$data['bank_branch_number'] = $this->request->post['bank_branch_number'];
//		} elseif (!empty($affiliate_info)) {
//			$data['bank_branch_number'] = $affiliate_info['bank_branch_number'];
//		} else {
//			$data['bank_branch_number'] = '';
//		}
//
//		if (isset($this->request->post['bank_swift_code'])) {
//			$data['bank_swift_code'] = $this->request->post['bank_swift_code'];
//		} elseif (!empty($affiliate_info)) {
//			$data['bank_swift_code'] = $affiliate_info['bank_swift_code'];
//		} else {
//			$data['bank_swift_code'] = '';
//		}
//
//		if (isset($this->request->post['bank_account_name'])) {
//			$data['bank_account_name'] = $this->request->post['bank_account_name'];
//		} elseif (!empty($affiliate_info)) {
//			$data['bank_account_name'] = $affiliate_info['bank_account_name'];
//		} else {
//			$data['bank_account_name'] = '';
//		}
//
//		if (isset($this->request->post['bank_account_number'])) {
//			$data['bank_account_number'] = $this->request->post['bank_account_number'];
//		} elseif (!empty($affiliate_info)) {
//			$data['bank_account_number'] = $affiliate_info['bank_account_number'];
//		} else {
//			$data['bank_account_number'] = '';
//		}
//
//		if (isset($this->request->post['custom_field'])) {
//			$data['affiliate_custom_field'] = $this->request->post['custom_field'];
//		} elseif (!empty($affiliate_info)) {
//			$data['affiliate_custom_field'] = json_decode($affiliate_info['custom_field'], true);
//		} else {
//			$data['affiliate_custom_field'] = array();
//		}

        $this->load->model('user/user');
        $users = $this->model_user_user->getUsers();

        $data['staffs'] = $users;

        //text error
        $data['error_input_null'] = $this->language->get('error_input_null');
        $data['error_type_mail'] = $this->language->get('error_type_mail');
        $data['error_max_length_255'] = $this->language->get('error_max_length_255');
        $data['error_max_length_15'] = $this->language->get('error_max_length_15');
        $data['error_type_phone'] = $this->language->get('error_type_phone');
        $data['error_max_length_10'] = $this->language->get('error_max_length_10');
        $data['error_min_length_8'] = $this->language->get('error_min_length_8');

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('customer/customer_form', $data));
    }

    public function import()
    {
        $this->load->language('customer/customer');
        $this->load->model('customer/customer');

        $json = array();
        // Allowed file mime types
        $allowed = array(
            'application/vnd.ms-excel',          // xls
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' // xlsx
        );
        $file = $this->request->files['import-file'];

        $max_file_size = defined('MAX_FILE_SIZE_IMPORT_CUSTOMER') ? MAX_FILE_SIZE_IMPORT_CUSTOMER : 2097152; // 2097152 ~2MB
        if (!isset($file['size']) || $file['size'] > $max_file_size) {
            $json['error'] = $this->language->get('text_warning_file_size') . ((float)$max_file_size / 1048576) . 'MB!';
        }
        if (!in_array($file['type'], $allowed)) {
            $json['error'] = $this->language->get('error_filetype');
        }
        $dataCustomer = [];

        if (!$json) {
            try {
                // start set read data only
                $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($file['tmp_name']);
                $reader->setReadDataOnly(true);
                // end

                $spreadsheet = $reader->load($file['tmp_name']);
                $firstSheetIndex = $spreadsheet->getFirstSheetIndex();
                $sheetData = $spreadsheet->getSheet($firstSheetIndex)->toArray(null, true, true, true);
            } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
                $json['error'] = $this->language->get('error_upload');
                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json));
            }
            $header = array_shift($sheetData);
            $header = $this->mapHeaderFromFile($header); /// remove empty element
            if (!$header) {
                $json['error'] = $this->language->get('error_file_incomplete');
            } else {
                $dataCustomer = $this->mapDataCustomerFromFile($sheetData, $header);
                if (array_key_exists('status', $dataCustomer) && !$dataCustomer['status']) {
                    $row = array_key_exists('row', $dataCustomer) ? $dataCustomer['row'] : 'UNKNOW';
                    if (isset($dataCustomer['error_msg'])) {
                        $json['error'] = $dataCustomer['error_msg'] . $this->language->get('error_file_error_columns') . $row;
                    } else {
                        $json['error'] = $this->language->get('error_file_missing_columns') . $row;
                    }
                }

                if (!$json) {
                    $this->model_customer_customer->importMultiCustomer($dataCustomer);
                }
            }
        }

        if (!$json) {
            $json['success'] = $this->language->get('txt_success') . ' ' . count($dataCustomer) . $this->language->get('txt_success_suffix');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    protected function mapHeaderFromFile(array $header)
    {
        $arrMap = array(
            //'mã_khách_hàng' => 'code',
            'họ_và_tên' => 'full_name',
            'số_điện_thoại' => 'phone',
            'thông_tin_địa_chỉ' => 'addresses',
            'trạng_thái' => 'status',
            'email' => 'email',
            'nhóm_khách_hàng' => 'group',
            'ngày_sinh' => 'birthday',
            'giới_tính' => 'sex',
            'website' => 'website',
            'mã_số_thuế' => 'tax_code',
            'nhân_viên_phụ_trách' => 'staff_in_charge',
            'ghi_chú' => 'note'
        );

        $result = array();
        foreach ($header as $key => $value) {
            $value = trim($value);
            if (array_key_exists($value, $arrMap)) {
                $result[$arrMap[$value]] = $key;
            }
        }

        foreach ($arrMap as $k => $v) {
            if (!array_key_exists($v, $result)) {
                return false;
            }
        }

        return $result;
    }

    protected function mapDataCustomerFromFile(array $data, array $header)
    {
        if (!is_array($data) || empty($data) || !is_array($header) || empty($header) || !array_key_exists('phone', $header)) {
            return [];
        }

        $result = array();
        foreach ($data as $row => $info) {
            if ($this->is_array_empty($info)) {
                continue;
            }
            if (trim($info[$header['full_name']]) == '' || trim($info[$header['phone']]) == '' || trim($info[$header['addresses']]) == '') {
                return [
                    'status' => false,
                    'row' => $row + 2
                ];
            }
            $current = $header;
            foreach ($current as $k => $v) {
                switch ($k) {
                    case 'status':
                        $current[$k] = trim($info[$v]) == 'Đang hoạt động' ? 1 : 0;
                        break;
                    case 'addresses':
                        $current_values = preg_split('/\r\n|\r|\n/', $info[$v]);
                        $current_values_trim_space = [];
                        if (count($current_values) > 10) {
                            return [
                                'status' => false,
                                'error_msg' => 'thông_tin_địa_chỉ ' . $this->language->get('warning_max_10_address'),
                                'row' => $row + 2
                            ];
                        }
                        foreach ($current_values as $c_val) {
                            if (trim($c_val) != '') {
                                if (count(explode(';', $c_val)) < 7) {
                                    return [
                                        'status' => false,
                                        'error_msg' => 'thông_tin_địa_chỉ ' . $this->language->get('warning_invalid'),
                                        'row' => $row + 2
                                    ];
                                } else {
                                    $addr_val = explode(';', $c_val);
                                    $add_full_name = trim($addr_val[0]);
                                    $add_phone = trim($addr_val[1]);
                                    $add_address = trim($addr_val[2]);
                                    $add_province = trim($addr_val[5]);
                                    if (!$add_full_name || !$add_phone || !$add_province || !$add_address || !preg_match('/^0[0-9-+]+$|^84[0-9-+]+$|^\+84[0-9-+]+$/', trim($add_phone))) {
                                        return [
                                            'status' => false,
                                            'error_msg' => 'thông_tin_địa_chỉ ' . $this->language->get('warning_invalid'),
                                            'row' => $row + 2
                                        ];
                                    }
                                }
                                $current_values_trim_space[] = trim($c_val);
                            }
                        }
                        $current[$k] = $current_values_trim_space;
                        break;
                    case 'note':
                        if (strlen($info[$v]) > 255) {
                            return [
                                'status' => false,
                                'error_msg' => 'ghi_chú ' . $this->language->get('warning_max_length_255'),
                                'row' => $row + 2
                            ];
                        }
                        $current[$k] = trim($info[$v]);
                        break;
                    case 'website':
                        if (strlen($info[$v]) > 50) {
                            return [
                                'status' => false,
                                'error_msg' => 'website ' . $this->language->get('warning_max_length_50'),
                                'row' => $row + 2
                            ];
                        }
                        $current[$k] = trim($info[$v]);
                        break;
                    case 'group':
                        if (strlen($info[$v]) > 50) {
                            return [
                                'status' => false,
                                'error_msg' => 'nhóm_khách_hàng ' . $this->language->get('warning_max_length_50'),
                                'row' => $row + 2
                            ];
                        }
                        $current[$k] = trim($info[$v]);
                        break;
                    case 'phone':
                        if ((strlen(trim($info[$v])) > 0 && !preg_match('/^0[0-9-+]+$|^84[0-9-+]+$|^\+84[0-9-+]+$/', trim($info[$v]))) || strlen(trim($info[$v])) > 15) {
                            return [
                                'status' => false,
                                'error_msg' => 'số_điện_thoại ' . $this->language->get('warning_invalid'),
                                'row' => $row + 2
                            ];
                        }
                        $current[$k] = trim($info[$v]);
                        break;
                    case 'tax_code':
                        if ((strlen(trim($info[$v])) > 0 && !preg_match('/^[0-9\-]+$/', trim($info[$v]))) || strlen(trim($info[$v])) > 15) {
                            return [
                                'status' => false,
                                'error_msg' => 'mã_số_thuế ' . $this->language->get('warning_invalid'),
                                'row' => $row + 2
                            ];
                        }
                        $current[$k] = trim($info[$v]);
                        break;
                    case 'email':
                        if ((strlen(trim($info[$v])) > 0 && !preg_match('/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', trim($info[$v]))) || strlen(trim($info[$v])) > 50) {
                            return [
                                'status' => false,
                                'error_msg' => 'email ' . $this->language->get('warning_invalid'),
                                'row' => $row + 2
                            ];
                        }
                        $current[$k] = trim($info[$v]);
                        break;
                    case 'birthday':
                        if (strlen(trim($info[$v])) > 0) {
                            if (is_numeric($info[$v])) {
                                $date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp(trim($info[$v]));
                                $birthday = date("Y-m-d", $date);
                            } else {
                                $date = str_replace('/', '-', trim($info[$v]));
                                if (strtotime($date) === false) {
                                    return [
                                        'status' => false,
                                        'error_msg' => 'ngày_sinh ' . $this->language->get('warning_invalid'),
                                        'row' => $row + 2
                                    ];
                                }
                                $birthday = date("Y-m-d", strtotime($date));
                            }
                            $now = date("Y-m-d");
                            if ($birthday > $now) {
                                return [
                                    'status' => false,
                                    'error_msg' => 'ngày_sinh ' . $this->language->get('warning_max_date'),
                                    'row' => $row + 2
                                ];
                            }
                        }
                        $current[$k] = trim($info[$v]);
                        break;
                    default:
                        $current[$k] = $info[$v];
                }
            }
            $ch = $this->validateRequireFields($current);
            if (!$ch) {
                return [
                    'status' => false,
                    'row' => $row + 2
                ];
            }

            $result[] = $current;
        }

        return $result;
    }

    private function validateRequireFields($data)
    {
        if (!array_key_exists('phone', $data) || trim($data['phone']) == '') {
            return false;
        }

        return true;
    }

    public function export()
    {
        $this->load->language('customer/customer');

        $this->response->addheader('Pragma: public');
        $this->response->addheader('Expires: 0');
        $this->response->addheader('Content-Description: File Transfer');
        $this->response->addheader('Content-Type: application/octet-stream');
        $this->response->addheader('Content-Disposition: attachment; filename="' . 'customer' . '_' . date('Y-m-d_H-i-s', time()) . '.xls"');
        $this->response->addheader('Content-Transfer-Encoding: binary');

        $this->load->model('customer/customer');

        $this->response->setOutput($this->model_customer_customer->export());
    }

    protected function validateForm()
    {
        if (!$this->user->hasPermission('modify', 'customer/customer')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

//		if ((utf8_strlen($this->request->post['firstname']) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 255)) {
//			$this->error['firstname'] = $this->language->get('error_firstname');
//		}
//
//		if ((utf8_strlen($this->request->post['lastname']) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 255)) {
//			$this->error['lastname'] = $this->language->get('error_lastname');
//		}

        if ((utf8_strlen($this->request->post['full_name']) < 1) || (utf8_strlen(trim($this->request->post['full_name'])) > 50)) {
            $this->load->language('customer/customer');
            $this->error['warning'] = $this->language->get('error_full_name');
        }

        if ((utf8_strlen($this->request->post['email']) > 255) ||
            (!filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL) &&
                !empty(trim($this->request->post['email'])))) {
            $this->error['email'] = $this->language->get('error_email');
            $this->error['warning'] = $this->language->get('error_email');
        }

        if ((utf8_strlen($this->request->post['telephone']) < 8) ||
            (utf8_strlen(trim($this->request->post['telephone'])) > 15)) {
            $this->error['telephone'] = $this->language->get('error_telephone');
            $this->error['warning'] = $this->language->get('error_phone');
        }

        $customer_info = $this->model_customer_customer->getCustomerByTelephone($this->request->post['telephone']);

        if (!isset($this->request->get['customer_id'])) {
            if ($customer_info) {
                $this->error['warning'] = $this->language->get('error_phone_exists');
            }
        } else {
            if ($customer_info && ($this->request->get['customer_id'] != $customer_info['customer_id'])) {
                $this->error['warning'] = $this->language->get('error_phone_exists');
            }
        }
        // check email
        $customer_info = !empty(trim($this->request->post['email'])) ? $this->model_customer_customer->getCustomerByEmail($this->request->post['email']) : null;

        if (!isset($this->request->get['customer_id'])) {
            if ($customer_info) {
                $this->error['warning'] = $this->language->get('error_exists');
            }
        } else {
            if ($customer_info && ($this->request->get['customer_id'] != $customer_info['customer_id'])) {
                $this->error['warning'] = $this->language->get('error_exists');
            }
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

    protected function validateDelete()
    {
        if (!$this->user->hasPermission('modify', 'customer/customer')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    protected function validateUnlock()
    {
        if (!$this->user->hasPermission('modify', 'customer/customer')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function blockCustomer()
    {
        $this->load->language('customer/customer');
        $json = array();
        if (!$this->user->hasPermission('modify', 'customer/customer')) {
            $json['warning'] = $this->language->get('error_permission');
            $this->session->data['warning'] = $this->language->get('txt_not_success');
        } else {
            if (!isset($this->request->post['selected']) || !is_array($this->request->post['selected']) || empty($this->request->post['selected'])) {
                $this->session->data['warning'] = $this->language->get('txt_not_success');
            } else {
                $this->load->model('customer/customer');
                $this->model_customer_customer->blockCustomers($this->request->post['selected']);
                $this->session->data['success'] = $this->language->get('txt_block_success');
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function unbBlockCustomer()
    {
        $this->load->language('customer/customer');
        $json = array();
        $this->load->model('customer/customer');
        if (!$this->user->hasPermission('modify', 'customer/customer')) {
            $json['warning'] = $this->language->get('error_permission');
            $this->session->data['warning'] = $this->language->get('txt_not_success');
        } else {
            if (!isset($this->request->post['selected']) || !is_array($this->request->post['selected']) || empty($this->request->post['selected'])) {
                $this->session->data['warning'] = $this->language->get('txt_not_success');
            } else {
                $this->load->model('customer/customer');
                $this->model_customer_customer->unBlockCustomers($this->request->post['selected']);
                $this->session->data['success'] = $this->language->get('txt_unblock_success');
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }

    public function deleteCustomer()
    {
        $this->load->language('customer/customer');
        $json = array();
        $this->load->model('customer/customer');
        if (!$this->user->hasPermission('modify', 'customer/customer')) {
            $json['warning'] = $this->language->get('error_permission');
            $this->session->data['warning'] = $this->language->get('txt_not_success');
        } else {
            if (!isset($this->request->post['selected']) || !is_array($this->request->post['selected']) || empty($this->request->post['selected'])) {
                $this->session->data['warning'] = $this->language->get('txt_not_success');
            } else {
                $this->load->model('customer/customer');
                if ($this->model_customer_customer->checkCustomersHasOrder($this->request->post['selected'])) {
                    $this->session->data['warning'] = $this->language->get('txt_customer_have_order');
                } else {
                    $this->model_customer_customer->deleteCustomers($this->request->post['selected']);
                    $this->session->data['success'] = $this->language->get('txt_delete_success');
                }
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function loadoptionone()
    {
        $this->load->language('customer/customer');
        $this->load->model('customer/customer_group');
        $categoryID = $this->request->post['categoryID'];
        $end_select_html = '</select></div>';
        $show_html = '';

        if ($categoryID > 0) {
            if ($categoryID == 1) { // Status
                $show_html = '<div class="form-group hierarchical-child" id="hierarchical-status">
                                            <select class="form-control form-control-sm select2"
                                             data-placeholder="' . $this->language->get('text_choose_status') . '" 
                                             data-label="' . $this->language->get('text_filter_status') . '" 
                                             id="filter_status" 
                                             name="filter_status">';
                $show_html .= '<option value="">' . $this->language->get('text_choose_status') . '</option>';
                $show_html .= '<option value="0" data-value="' . $this->language->get('text_customer_block') . '">' . $this->language->get('text_customer_block') . '</option>';
                $show_html .= '<option value="1" data-value="' . $this->language->get('text_customer_active') . '">' . $this->language->get('text_customer_active') . '</option>';
                $show_html .= $end_select_html;
            } elseif ($categoryID == 2) { // customer group
                $start_select_html = '<div class="form-group hierarchical-child" id="hierarchical-customer-group">
                                        <select class="form-control form-control-sm select2" 
                                        data-placeholder="' . $this->language->get('text_chose_group_customer') . '" 
                                        data-label="' . $this->language->get('text_filter_group') . '" 
                                        id="filter_customer_group" 
                                        name="filter_customer_group">';
                $end_select_html = '</select></div>';
                $getListGroup = $this->model_customer_customer_group->getCustomerGroups(['limit' => 100, 'start' => 0]);
                $show_html .= $start_select_html . '<option value="" data-value="">' . $this->language->get('text_chose_group_customer') . '</option>';
                foreach ($getListGroup as $key => $vl) {
                    $show_html .= '<option value="' . $vl['customer_group_id'] . '" data-value="' . $vl['name'] . '">' . $vl['name'] . '</option>';
                }
                $show_html .= $end_select_html;
            }
        } else {
            $show_html .= '<style>#optionone,#optiontwo{display:none !important}</style>';
        }

        echo $show_html .= '';
    }

    public function login()
    {
        if (isset($this->request->get['customer_id'])) {
            $customer_id = $this->request->get['customer_id'];
        } else {
            $customer_id = 0;
        }

        $this->load->model('customer/customer');

        $customer_info = $this->model_customer_customer->getCustomer($customer_id);

        if ($customer_info) {
            // Create token to login with
            $token = token(64);

            $this->model_customer_customer->editToken($customer_id, $token);

            if (isset($this->request->get['store_id'])) {
                $store_id = $this->request->get['store_id'];
            } else {
                $store_id = 0;
            }

            $this->load->model('setting/store');

            $store_info = $this->model_setting_store->getStore($store_id);

            if ($store_info) {
                $this->response->redirect($store_info['url'] . 'index.php?route=account/login&token=' . $token);
            } else {
                $this->response->redirect(HTTP_CATALOG . 'index.php?route=account/login&token=' . $token);
            }
        } else {
            $this->load->language('error/not_found');

            $this->document->setTitle($this->language->get('heading_title'));

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('error/not_found', 'user_token=' . $this->session->data['user_token'], true)
            );

            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('error/not_found', $data));
        }
    }

    public function history()
    {
        $this->load->language('customer/customer');

        $this->load->model('customer/customer');

        $this->document->setTitle($this->language->get('text_history_title'));

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data['histories'] = array();

        $filter_data = array(
            'customer_id' => $this->request->get['customer_id'],
            'start'                    => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit'                    => $this->config->get('config_limit_admin')
        );

		$results = $this->model_customer_customer->getHistories($filter_data);

		foreach ($results as $result) {
			$data['histories'][] = array(
                'order_url' => $this->url->link('sale/order/detail', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . (int)$result['order_id'], true),
				'order_code' => $result['order_code'],
				'date_added' =>  $result['date_added'],
				'total'      => $result['total'],
                'status'     => $this->convertStatusOrder($result['order_status_id']),
                'return_receipt'     => $result['return_receipt']
			);
		}

		$history_total = $this->model_customer_customer->getTotalHistories($this->request->get['customer_id']);
		$pagination = new CustomPaginate();
		$pagination->total = $history_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('customer/customer/history', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $this->request->get['customer_id'] . '&page={page}', true);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['href_product_list'] = $this->url->link('customer/customer', 'user_token=' . $this->session->data['user_token'], true);
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
		$data['pagination'] = $pagination->render($option);
        $data['address_href'] = $this->url->link('customer/customer/customerAddress', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $this->request->get['customer_id'], true);
        $data['detail_href'] = $this->url->link('customer/customer/detail', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $this->request->get['customer_id'], true);
        $data['results'] = sprintf($this->language->get('text_pagination'), ($history_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($history_total - $this->config->get('config_limit_admin'))) ? $history_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $history_total, ceil($history_total / $this->config->get('config_limit_admin')));
		$this->response->setOutput($this->load->view('customer/customer_history', $data));
        if(isset($this->request->get['filter_data']) && $this->request->get['filter_data']){
            $this->response->setOutput($this->load->view('customer/customer_history_append', $data));
        }else{
            $this->response->setOutput($this->load->view('customer/customer_history', $data));
        }
	}

    public function convertStatusOrder($order_id)
    {
        $order_status_text = '';
        $this->load->language('sale/order');
        $this->load->model('sale/order');
        switch ($order_id) {
            case ModelSaleOrder::ORDER_STATUS_ID_PROCESSING :
                $order_status_text = $this->language->get('order_status_processing');
                break;
            case ModelSaleOrder::ORDER_STATUS_ID_DELIVERING :
                $order_status_text = $this->language->get('order_status_delivering');
                break;
            case ModelSaleOrder::ORDER_STATUS_ID_COMPLETED :
                $order_status_text = $this->language->get('order_status_complete');
                break;
            case ModelSaleOrder::ORDER_STATUS_ID_CANCELLED :
                $order_status_text = $this->language->get('order_status_canceled');
                break;
            case ModelSaleOrder::ORDER_STATUS_ID_DRAFT :
                $order_status_text = $this->language->get('order_status_draft');
                break;
        }
        return $order_status_text;
    }

    public function addHistory()
    {
        $this->load->language('customer/customer');

        $json = array();

        if (!$this->user->hasPermission('modify', 'customer/customer')) {
            $json['error'] = $this->language->get('error_permission');
        } else {
            $this->load->model('customer/customer');

            $this->model_customer_customer->addHistory($this->request->get['customer_id'], $this->request->post['comment']);

            $json['success'] = $this->language->get('text_success');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function transaction()
    {
        $this->load->language('customer/customer');

        $this->load->model('customer/customer');

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data['transactions'] = array();

        $results = $this->model_customer_customer->getTransactions($this->request->get['customer_id'], ($page - 1) * 10, 10);

        foreach ($results as $result) {
            $data['transactions'][] = array(
                'amount' => $this->currency->format($result['amount'], $this->config->get('config_currency')),
                'description' => $result['description'],
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
            );
        }

        $data['balance'] = $this->currency->format($this->model_customer_customer->getTransactionTotal($this->request->get['customer_id']), $this->config->get('config_currency'));

        $transaction_total = $this->model_customer_customer->getTotalTransactions($this->request->get['customer_id']);

        $pagination = new Pagination();
        $pagination->total = $transaction_total;
        $pagination->page = $page;
        $pagination->limit = 10;
        $pagination->url = $this->url->link('customer/customer/transaction', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $this->request->get['customer_id'] . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($transaction_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($transaction_total - 10)) ? $transaction_total : ((($page - 1) * 10) + 10), $transaction_total, ceil($transaction_total / 10));

        $this->response->setOutput($this->load->view('customer/customer_transaction', $data));
    }

    public function addTransaction()
    {
        $this->load->language('customer/customer');

        $json = array();

        if (!$this->user->hasPermission('modify', 'customer/customer')) {
            $json['error'] = $this->language->get('error_permission');
        } else {
            $this->load->model('customer/customer');

            $this->model_customer_customer->addTransaction($this->request->get['customer_id'], $this->request->post['description'], $this->request->post['amount']);

            $json['success'] = $this->language->get('text_success');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function reward()
    {
        $this->load->language('customer/customer');

        $this->load->model('customer/customer');

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data['rewards'] = array();

        $results = $this->model_customer_customer->getRewards($this->request->get['customer_id'], ($page - 1) * 10, 10);

        foreach ($results as $result) {
            $data['rewards'][] = array(
                'points' => $result['points'],
                'description' => $result['description'],
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
            );
        }

        $data['balance'] = $this->model_customer_customer->getRewardTotal($this->request->get['customer_id']);

        $reward_total = $this->model_customer_customer->getTotalRewards($this->request->get['customer_id']);

        $pagination = new Pagination();
        $pagination->total = $reward_total;
        $pagination->page = $page;
        $pagination->limit = 10;
        $pagination->url = $this->url->link('customer/customer/reward', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $this->request->get['customer_id'] . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($reward_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($reward_total - 10)) ? $reward_total : ((($page - 1) * 10) + 10), $reward_total, ceil($reward_total / 10));

        $this->response->setOutput($this->load->view('customer/customer_reward', $data));
    }

    public function addReward()
    {
        $this->load->language('customer/customer');

        $json = array();

        if (!$this->user->hasPermission('modify', 'customer/customer')) {
            $json['error'] = $this->language->get('error_permission');
        } else {
            $this->load->model('customer/customer');

            $this->model_customer_customer->addReward($this->request->get['customer_id'], $this->request->post['description'], $this->request->post['points']);

            $json['success'] = $this->language->get('text_success');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function ip()
    {
        $this->load->language('customer/customer');

        $this->load->model('customer/customer');

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data['ips'] = array();

        $results = $this->model_customer_customer->getIps($this->request->get['customer_id'], ($page - 1) * 10, 10);

        foreach ($results as $result) {
            $data['ips'][] = array(
                'ip' => $result['ip'],
                'total' => $this->model_customer_customer->getTotalCustomersByIp($result['ip']),
                'date_added' => date('d/m/y', strtotime($result['date_added'])),
                'filter_ip' => $this->url->link('customer/customer', 'user_token=' . $this->session->data['user_token'] . '&filter_ip=' . $result['ip'], true)
            );
        }

        $ip_total = $this->model_customer_customer->getTotalIps($this->request->get['customer_id']);

        $pagination = new Pagination();
        $pagination->total = $ip_total;
        $pagination->page = $page;
        $pagination->limit = 10;
        $pagination->url = $this->url->link('customer/customer/ip', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $this->request->get['customer_id'] . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($ip_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($ip_total - 10)) ? $ip_total : ((($page - 1) * 10) + 10), $ip_total, ceil($ip_total / 10));

        $this->response->setOutput($this->load->view('customer/customer_ip', $data));
    }

    public function autocomplete()
    {
        $json = array();

        if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_email'])) {
            if (isset($this->request->get['filter_name'])) {
                $filter_name = $this->request->get['filter_name'];
            } else {
                $filter_name = '';
            }

            if (isset($this->request->get['filter_email'])) {
                $filter_email = $this->request->get['filter_email'];
            } else {
                $filter_email = '';
            }

            if (isset($this->request->get['filter_affiliate'])) {
                $filter_affiliate = $this->request->get['filter_affiliate'];
            } else {
                $filter_affiliate = '';
            }

            $this->load->model('customer/customer');

            $filter_data = array(
                'filter_name' => $filter_name,
                'filter_email' => $filter_email,
                'filter_affiliate' => $filter_affiliate,
                'start' => 0,
                'limit' => 5
            );

            $results = $this->model_customer_customer->getCustomers($filter_data);

            foreach ($results as $result) {
                $json[] = array(
                    'customer_id' => $result['customer_id'],
                    'customer_group_id' => $result['customer_group_id'],
                    'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
                    'customer_group' => $result['customer_group'],
                    'firstname' => $result['firstname'],
                    'lastname' => $result['lastname'],
                    'email' => $result['email'],
                    'telephone' => $result['telephone'],
                    'custom_field' => json_decode($result['custom_field'], true),
                    'address' => $this->model_customer_customer->getAddresses($result['customer_id'])
                );
            }
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function autocompleteEmail()
    {
        $json = array();

        if (isset($this->request->get['filter_email'])) {
            $filter_email = $this->request->get['filter_email'];
        } else {
            $filter_email = '';
        }

        $this->load->model('customer/customer');

        $filter_data = array(
            'filter_email' => $filter_email,
            'start' => 0,
            'limit' => 5
        );

        $results = $this->model_customer_customer->getCustomers($filter_data);

        foreach ($results as $result) {
            $json[] = array(
                'customer_id' => $result['customer_id'],
                'email' => $result['email'],
            );
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function customfield()
    {
        $json = array();

        $this->load->model('customer/custom_field');

        // Customer Group
        if (isset($this->request->get['customer_group_id'])) {
            $customer_group_id = $this->request->get['customer_group_id'];
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        $custom_fields = $this->model_customer_custom_field->getCustomFields(array('filter_customer_group_id' => $customer_group_id));

        foreach ($custom_fields as $custom_field) {
            $json[] = array(
                'custom_field_id' => $custom_field['custom_field_id'],
                'required' => empty($custom_field['required']) || $custom_field['required'] == 0 ? false : true
            );
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function address()
    {
        $json = array();

        if (!empty($this->request->get['address_id'])) {
            $this->load->model('customer/customer');

            $json = $this->model_customer_customer->getAddress($this->request->get['address_id']);
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getGroupCustomers()
    {
        $json = array();

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $this->load->model('customer/customer_group');

        $filter_data = array(
            'order' => 'ASC',
            'start' => ($page - 1) * 6,
            'limit' => 6
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        $json['results'] = $this->model_customer_customer_group->getCustomerGroups($filter_data);

        foreach ($json['results'] as &$result) {
            $result['id'] = $result['customer_group_id'];
            $result['text'] = $result['name'];
        }

        $count_store = $this->model_customer_customer_group->getTotalCustomerGroups($filter_data);
        $json['count'] = $count_store;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getAddressJson()
    {
        $this->load->model('customer/customer');
        $this->load->model('localisation/vietnam_administrative');
        $json = [];
        if (isset($this->request->post['address_id']) && isset($this->request->post['customer_id'])) {
            $result = $this->model_customer_customer->getAddressDefaultCustomer($this->request->post['address_id']);
            $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($result['city']);
            $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($result['district']);
            $ward = $this->model_localisation_vietnam_administrative->getWardByCode($result['wards']);

            $json['province'] = [
                'code' => $result['city'],
                'name' => isset($province['name']) ? $province['name'] : '',
            ];
            $json['district'] = [
                'code' => $result['district'],
                'name' => isset($district['name']) ? $district['name'] : '',
            ];
            $json['ward'] = [
                'code' => $result['wards'],
                'name' => isset($ward['name']) ? $ward['name'] : '',
            ];
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function deleteAddress()
    {
        $this->load->model('customer/customer');
        $this->load->language('customer/customer');
        $json = [];
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $this->model_customer_customer->deleteAddress($this->request->post['address_id'], $this->request->post['customer_id']);
            $this->session->data['success'] = $this->language->get('text_form_delete_address_success');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function setDefaultAddress()
    {
        $this->load->model('customer/customer');
        $json = [];
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $this->model_customer_customer->setDefaultAddress($this->request->post['address_id'], $this->request->post['customer_id']);
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function listEmailSubscribers()
    {
        $this->load->language('customer/customer');

        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('customer/customer');
        $data['email_customers'] = array();
        $data['link_list_customer'] = $this->url->link('customer/customer/index', 'user_token=' . $this->session->data['user_token'], true);
        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $filter_data = array(
            'start'=> ($page - 1) * $this->config->get('config_limit_admin'),
            'limit'=> $this->config->get('config_limit_admin')
        );
        $customer_total = $this->model_customer_customer->getTotalEmailsSubscribers($filter_data);
        $results = $this->model_customer_customer->getEmailsSubscribers($filter_data);
        foreach ($results as $result) {
            $data['email_customers'][] = array(
                'email' => $result['email'],
                'date_added' => $result['date_added'],
                'delete' => $this->url->link('customer/customer/deleteEmailSubscribers', 'user_token=' . $this->session->data['user_token'] . '&email_id=' . $result['email_id'], true),
            );
        }
        $data['user_token'] = $this->session->data['user_token'];
        $pagination = new CustomPaginate();
        $pagination->total = $customer_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('customer/customer', 'user_token=' . $this->session->data['user_token'] . '&page={page}', true);

        // Add jump to page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);
        $data['results'] = sprintf($this->language->get('text_pagination'), ($customer_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($customer_total - $this->config->get('config_limit_admin'))) ? $customer_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $customer_total, ceil($customer_total / $this->config->get('config_limit_admin')));
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('customer/customer_list_append_email_subscribers', $data));
        } else {
            $this->response->setOutput($this->load->view('customer/customer_list_email_subscribers', $data));
        }
    }

    public function deleteEmailSubscribers() {
        $this->load->model('customer/customer');
        $this->model_customer_customer->deleteCustomers($this->request->post['selected']);
        $this->response->addHeader('Content-Type: application/json');
        $json = [
            'status' => true,
            'message' => $this->language->get('text_delete_success'),
        ];
        $this->response->setOutput(json_encode($json));
    }

    function is_array_empty($arr)
    {
        if (is_array($arr)) {
            foreach ($arr as $key => $value) {
                if (!empty($value) || $value != NULL || $value != "") {
                    return false;
                    break;//stop the process we have seen that at least 1 of the array has value so its not empty
                }
            }
            return true;
        }
    }
}