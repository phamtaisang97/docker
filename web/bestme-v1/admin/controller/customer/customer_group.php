<?php
class ControllerCustomerCustomerGroup extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('customer/customer_group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('customer/customer_group');

		$this->getList();
	}

	public function add() {
		$this->load->language('customer/customer_group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('customer/customer_group');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
		    $this->model_customer_customer_group->addCustomerGroup($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
		}
        $this->response->redirect($this->url->link('customer/customer_group', 'user_token=' . $this->session->data['user_token']));
	}

	public function edit() {
		$this->load->language('customer/customer_group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('customer/customer_group');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$this->model_customer_customer_group->editCustomerGroup($this->request->post['customer_group_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_edit_success');;

			$this->response->redirect($this->url->link('customer/customer_group', 'user_token=' . $this->session->data['user_token']));
		}
	}

	public function delete() {
		$this->load->language('customer/customer_group');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('customer/customer_group');
		if (isset($this->request->post)) {
            $this->model_customer_customer_group->deleteCustomerGroup($this->request->post['customer_group_id']);
            $this->session->data['success'] = $this->language->get('text_del_success');
            $this->response->redirect($this->url->link('customer/customer_group', 'user_token=' . $this->session->data['user_token']));
        }
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'cgd.name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('customer/customer_group', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		$data['customer_groups'] = array();

        //search list
        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }
        $filter_name = '';
        if (isset($this->request->get['filter_name'])) {
            $filter_name = trim($this->request->get['filter_name']);
        }
		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'filter_name' => $filter_name,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
        if(!$this->user->canAccessAll()){
            $filter_data['user_create_id'] = $this->user->getId();
        }

		$customer_group_total = $this->model_customer_customer_group->getTotalCustomerGroups($filter_data);

		$results = $this->model_customer_customer_group->getCustomerGroups($filter_data);
		foreach ($results as $result) {
			$data['customer_groups'][] = array(
				'customer_group_id'   => $result['customer_group_id'],
				'name'                => $result['name'],
				'sort_order'          => $result['sort_order'],
				'customer_group_code' => $result['customer_group_code'],
				'description'         => $result['description'],
				'amoutCustomer'         => $this->model_customer_customer_group->getCountCustomerGroup($result['customer_group_id']),
				'edit'                => $this->url->link('customer/customer_group/edit', 'user_token=' . $this->session->data['user_token']. '&customer_group_id=' . $result['customer_group_id'], true),
				'delete'                => $this->url->link('customer/customer_group/delete', '&user_token=' . $this->session->data['user_token'], true),
			);
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('customer/customer_group', 'user_token=' . $this->session->data['user_token'] . '&sort=cgd.name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('customer/customer_group', 'user_token=' . $this->session->data['user_token'] . '&sort=cg.sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new CustomPaginate();
		$pagination->total = $customer_group_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('customer/customer_group', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

        // Add jump to page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
		$data['pagination'] = $pagination->render($option);

        $data['access_modify'] = $this->user->hasPermission('modify', 'customer/customer_group');

		$data['results'] = sprintf($this->language->get('text_pagination'), ($customer_group_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($customer_group_total - $this->config->get('config_limit_admin'))) ? $customer_group_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $customer_group_total, ceil($customer_group_total / $this->config->get('config_limit_admin')));

		$data['action'] = $this->url->link('customer/customer_group/add', 'user_token=' . $this->session->data['user_token'], true);
        $data['url_filter'] = $this->url->link('customer/customer_group', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['sort'] = $sort;
		$data['order'] = $order;
        $data['user_token'] = $this->session->data['user_token'];
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if(isset($this->request->get['filter_data']) && $this->request->get['filter_data']){
            $this->response->setOutput($this->load->view('customer/customer_group_list_append', $data));
        }else{
            $this->response->setOutput($this->load->view('customer/customer_group_list', $data));
        }
	}

	protected function getForm() {
		$data['text_form'] = !isset($this->request->get['customer_group_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('customer/customer_group', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		$data['cancel'] = $this->url->link('customer/customer_group', 'user_token=' . $this->session->data['user_token'] . $url, true);

		if (isset($this->request->get['customer_group_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$customer_group_info = $this->model_customer_customer_group->getCustomerGroup($this->request->get['customer_group_id']);
		}

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['customer_group_description'])) {
			$data['customer_group_description'] = $this->request->post['customer_group_description'];
		} elseif (isset($this->request->get['customer_group_id'])) {
			$data['customer_group_description'] = $this->model_customer_customer_group->getCustomerGroupDescriptions($this->request->get['customer_group_id']);
		} else {
			$data['customer_group_description'] = array();
		}

		if (isset($this->request->post['approval'])) {
			$data['approval'] = $this->request->post['approval'];
		} elseif (!empty($customer_group_info)) {
			$data['approval'] = $customer_group_info['approval'];
		} else {
			$data['approval'] = '';
		}

		if (isset($this->request->post['sort_order'])) {
			$data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($customer_group_info)) {
			$data['sort_order'] = $customer_group_info['sort_order'];
		} else {
			$data['sort_order'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('customer/customer_group_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'customer/customer_group')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if(isset($this->request->post['name_group'])){
			if ((utf8_strlen($this->request->post['name_group']) < 1) || (utf8_strlen($this->request->post['name_group']) > 50)) {
                $json = [
                    'error' => true,
                    'message' => $this->language->get('error_name')
                ];
                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json));
			}
		}
		return true;
	}

    public function validateGroupNameExist()
    {
        $this->load->language('customer/customer_group');

        $this->load->model('customer/customer_group');

        $group_id = isset($this->request->post['group_id']) ? $this->request->post['group_id'] : '';

        $group_name = isset($this->request->post['name_group']) ? $this->request->post['name_group'] : '';

        $json = [
            'valid' => true
        ];

        if ($group_name){
            if ($this->model_customer_customer_group->validateNameExist($group_id, $group_name)){
                $json['valid'] = false;
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'customer/customer_group')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('setting/store');
		$this->load->model('customer/customer');

		/*foreach ($this->request->post['selected'] as $customer_group_id) {
			if ($this->config->get('config_customer_group_id') == $customer_group_id) {
				$this->error['warning'] = $this->language->get('error_default');
			}

			$store_total = $this->model_setting_store->getTotalStoresByCustomerGroupId($customer_group_id);

			if ($store_total) {
				$this->error['warning'] = sprintf($this->language->get('error_store'), $store_total);
			}

			$customer_total = $this->model_customer_customer->getTotalCustomersByCustomerGroupId($customer_group_id);

			if ($customer_total) {
				$this->error['warning'] = sprintf($this->language->get('error_customer'), $customer_total);
			}
		}*/

		return !$this->error;
	}
}