<?php

class ControllerSettingsGeneral extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('settings/general');
        $this->load->model('localisation/vietnam_administrative');
        $this->load->model('localisation/country');
        $this->load->model('localisation/time_zone');
        $this->load->model('localisation/currency');
        $this->load->model('localisation/weight_class');
        $this->load->model('setting/setting');

        $this->document->setTitle($this->language->get('heading_title'));

        $data = [];

        $data['provinces'] = $this->model_localisation_vietnam_administrative->getProvinces();
        $data['countries'] = $this->model_localisation_country->getCountries();
        $data['timezones'] = $this->model_localisation_time_zone->getTimeZones();
        $data['currencies'] = $this->model_localisation_currency->getCurrencies();
        $data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();
        // hardcode: only gram. TODO: rollback?...
        $data['weight_classes'] = array_filter($data['weight_classes'], function ($weight_class) {
            return is_array($weight_class) && isset($weight_class['weight_class_id']) && $weight_class['weight_class_id'] == 2; // gram only
        });

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $keys = [
            'config_name',
            'config_home_name',
            'config_home_des',
            'config_email_admin',
            'config_email_marketing',
            'config_hotline',
            'config_eco_name',
            'config_tax_code',
            'config_telephone',
            'config_address',
            'config_city_id',
            'config_country_code',
            'config_time_zone',
            'config_weight_class_id',
            'config_currency',
            'config_id_prefix',
            'config_id_suffix',
            'config_name_display',
            'config_image_qr_code'
        ];

        $settings = $this->model_setting_setting->getMultiSettingValue($keys);
        if (is_array($settings)) {
            foreach ($settings as $setting) {
                if (!isset($setting['key']) || !isset($setting['value'])) {
                    continue;
                }
                $data['settings'][$setting['key']] = $setting['value'];
            }
        }
        $data['settings'] = $this->setDefault($data['settings']);

        $data['action'] = $this->url->link('settings/general/edit', 'user_token=' . $this->session->data['user_token'], true);
        $data['general_setting_link'] = $this->url->link('settings/general', 'user_token=' . $this->session->data['user_token'], true);
        $data['settings_link'] = $this->url->link('settings/settings', 'user_token=' . $this->session->data['user_token'], true);

        $data['ckeditor_lang'] = $this->config->get('config_language') == 'vi-vn' ? 'vi' : 'en';
        $data['upload_image'] = str_replace('&amp;', '&', $this->url->link('catalog/product/upload', 'user_token=' . $this->session->data['user_token'], true));
        $data['filemanager'] = str_replace('&amp;', '&', $this->url->link('common/filemanager', 'user_token=' . $this->session->data['user_token'] . '&ckeditor=1', true));

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        //text error
        $data['error_input_null'] = $this->language->get('error_input_null');
        $data['error_type_mail'] = $this->language->get('error_type_mail');
        $data['error_max_length_255'] = $this->language->get('error_max_length_255');
        $data['error_max_length_15'] = $this->language->get('error_max_length_15');
        $data['error_type_phone'] = $this->language->get('error_type_phone');
        $data['error_max_length_10'] = $this->language->get('error_max_length_10');
        $data['error_min_length_8'] = $this->language->get('error_min_length_8');

        $this->response->setOutput($this->load->view('settings/general_settings', $data));
    }

    public function edit()
    {
        // shop_name : config; config_name_display                  NEW
        // home_name : config; config_home_name                     NEW
        // home_desc : config; config_home_des                      NEW
        // email_admin : config; config_email_admin                 NEW
        // email_marketing : config; config_email_marketing         NEW
        // eco_name : config; config_eco_name                       NEW
        // tax_code : config; config_tax_code                       NEW
        // phone : config; config_telephone
        // address : config; config_address
        // city_id : config; config_city_id                         NEW
        // country : config; config_country_code                    NEW
        // time_zone : config; config_time_zone                     NEW
        // weight_unit : config; config_weight_class_id
        // currency : config; config_currency
        // id_prefix : config; config_id_prefix                     NEW
        // id_suffix : config; config_id_suffix                     NEW

        // image_qr_code : config; config_image_qr_code             NEW

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $data = $this->request->post;
            $data = $this->dataMap($data);
            $this->load->language('settings/general');
            $this->load->model('setting/setting');
            $this->model_setting_setting->updateMultiSettingValue($data);
            $this->session->data['success'] = $this->language->get('text_success_update');
            $this->response->redirect($this->url->link('settings/settings', 'user_token=' . $this->session->data['user_token'], true));
        } else {
            $this->index();
        }
    }

    private function dataMap($data)
    {
        $result['config_name_display'] = isset($data['shop_name']) ? $data['shop_name'] : '';
        $result['config_home_name'] = isset($data['home_name']) ? $data['home_name'] : '';
        $result['config_home_des'] = isset($data['home_desc']) ? $data['home_desc'] : '';
        $result['config_email_admin'] = isset($data['email_admin']) ? $data['email_admin'] : '';
        $result['config_email_marketing'] = isset($data['email_marketing']) ? $data['email_marketing'] : '';
        $result['config_hotline'] = isset($data['hotline']) ? $data['hotline'] : '';
        $result['config_eco_name'] = isset($data['eco_name']) ? $data['eco_name'] : '';
        $result['config_tax_code'] = isset($data['tax_code']) ? $data['tax_code'] : '';
        $result['config_telephone'] = isset($data['phone']) ? $data['phone'] : '';
        $result['config_address'] = isset($data['address']) ? $data['address'] : '';
        $result['config_city_id'] = isset($data['city']) ? $data['city'] : '';
        $result['config_country_code'] = isset($data['country']) ? $data['country'] : '';
        $result['config_time_zone'] = isset($data['time_zone']) ? $data['time_zone'] : '+07:00';
        $result['config_weight_class_id'] = isset($data['weight_unit']) ? $data['weight_unit'] : '2'; // 1: kilogram, 2: gram
        $result['config_currency'] = isset($data['currency']) ? $data['currency'] : 'VND';
        $result['config_id_prefix'] = isset($data['id_prefix']) ? $data['id_prefix'] : '';
        $result['config_id_suffix'] = isset($data['id_suffix']) ? $data['id_suffix'] : '';

        $result['config_image_qr_code'] = isset($data['image_qr_code']) ? $data['image_qr_code'] : '';

        return $result;
    }

    private function setDefault($data)
    {
        if (!isset($data['config_city_id'])) {
            $data['config_city_id'] = '';
        }

        if (!isset($data['config_country_code'])) {
            $data['config_country_code'] = "";
        }

        if (!isset($data['config_time_zone'])) {
            $data['config_time_zone'] = "+07:00";
        }

        if (!isset($data['config_weight_class_id'])) {
            $data['config_weight_class_id'] = '2';
        }

        if (!isset($data['config_currency'])) {
            $data['config_currency'] = 'VND';
        }

        if (!isset($data['config_name_display'])) {
            $data['config_name_display'] = isset($data['config_name']) ? $data['config_name'] : '';
        }

        if (!isset($data['config_image_qr_code']) || empty($data['config_image_qr_code'])) {
            $data['config_image_qr_code'] = 'view/image/theme/upload-placeholder2.svg';
        }

        return $data;
    }

    private function validateForm()
    {
        $this->load->language('settings/general');
        if (!$this->user->hasPermission('modify', 'settings/general')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        /* shop_name */
        if (!isset($this->request->post['shop_name']) ||
            (utf8_strlen(trim($this->request->post['shop_name'])) < 1) ||
            (utf8_strlen(trim($this->request->post['shop_name'])) > 50)
        ) {
            $this->error['name'] = $this->language->get('error_name');
            $this->error['warning'] = $this->language->get('error_name');
        }

        /* home_name */
        if (!isset($this->request->post['home_name']) ||
            (utf8_strlen(trim($this->request->post['home_name'])) < 1) ||
            (utf8_strlen(trim($this->request->post['home_name'])) > 255)
        ) {
            $this->error['name'] = $this->language->get('error_title');
            $this->error['warning'] = $this->language->get('error_title');
        }

        /* hotline */
        if (utf8_strlen(trim($this->request->post['hotline'])) > 15 || utf8_strlen(trim($this->request->post['hotline'])) != 0) {
            if (!preg_match("/^([0-9]+)$/", $this->request->post['hotline'])) {
                $this->error['hotline'] = $this->language->get('error_hotline');
                $this->error['warning'] = $this->language->get('error_hotline');
            }
        }

        return !$this->error;
    }
}