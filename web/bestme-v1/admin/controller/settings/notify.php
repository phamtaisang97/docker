<?php

class ControllerSettingsNotify extends Controller
{
    private $error = array();
    
    public function index()
    {
        $this->load->language('settings/notify');
        $this->load->model('setting/setting');

        $this->document->setTitle($this->language->get('heading_title'));

        $data = [];

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $keys = array(
            'config_order_success_text',
            'config_register_success_text'
        );

        $settings = $this->model_setting_setting->getMultiSettingValue($keys);
        if (is_array($settings)) {
            foreach ($settings as $setting) {
                if (!isset($setting['key']) || !isset($setting['value'])) {
                    continue;
                }
                $data['settings'][$setting['key']] = $setting['value'];
            }
        }

        $data['action'] = $this->url->link('settings/notify/edit', 'user_token=' . $this->session->data['user_token'], true);
        $data['settings_link'] = $this->url->link('settings/settings', 'user_token=' . $this->session->data['user_token'], true);

        $data['ckeditor_lang'] = $this->config->get('config_language') == 'vi-vn' ? 'vi' : 'en';
        $data['upload_image'] = str_replace('&amp;', '&', $this->url->link('catalog/product/upload', 'user_token=' . $this->session->data['user_token'], true));

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('settings/notify', $data));
    }

    public function edit()
    {
        // config_order_success_text : config; config_order_success_text        NEW
        // config_register_success_text : config; config_register_success_text  NEW

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $data = $this->request->post;
            $data = $this->dataMap($data);
            $this->load->language('settings/notify');
            $this->load->model('setting/setting');
            $this->model_setting_setting->updateMultiSettingValue($data);
            $this->session->data['success'] = $this->language->get('text_update_success');
            $this->response->redirect($this->url->link('settings/notify', 'user_token=' . $this->session->data['user_token'] , true));
        }else{
            $this->index();
        }
    }

    private function dataMap($data)
    {
        $result['config_order_success_text'] = isset($data['order_success_text']) ? $data['order_success_text'] : '';
        $result['config_register_success_text'] = isset($data['register_success_text']) ? $data['register_success_text'] : '';

        return $result;
    }

    private function validateForm()
    {
        $this->load->language('settings/notify');
        if (!$this->user->hasPermission('modify', 'settings/general')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }
}