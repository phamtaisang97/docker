<?php

class ControllerSettingsUserGroup extends Controller
{
    private $error = [];

    public function index()
    {
        $this->load->language('settings/user_group');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('user/user_group');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $permission = $this->buildPermission();
            $user_group = array(
                'name' => $this->request->post['name'],
                'permission' => $permission,
                'access_all' => isset($this->request->post['block_access_all']) ? 0 : 1,
            );

            $this->load->model('user/user_group');
            $this->model_user_user_group->addUserGroup($user_group);

            $this->session->data['success'] = $this->language->get('text_success_add');
            $this->response->redirect($this->url->link('settings/account', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();
    }

    public function edit()
    {
        $this->load->language('settings/user_group');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('user/user_group');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $permission = $this->buildPermission();
            $user_group = array(
                'name' => $this->request->post['name'],
                'permission' => $permission,
                'access_all' => isset($this->request->post['block_access_all']) ? 0 : 1,
            );

            $this->load->model('user/user_group');

            $this->model_user_user_group->editUserGroup($this->request->post['user_group_id'] ,$user_group);

            $this->session->data['success'] = $this->language->get('text_success_edit');
            $this->response->redirect($this->url->link('settings/account', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();
    }

    private function buildPermission()
    {
        $permissionsConfig = $this->getPermissionConfig();

        $access = [];
        $modifies = [];
        $permissions_data = $this->request->post['permission'];

        foreach ($permissions_data as $key => $permission_data) {
            if ($permission_data === '1') {
                $access = array_merge($access, $permissionsConfig[$key]['router']);
                continue;
            }
            if ($permission_data === '2') {
                $modifies = array_merge($modifies, $permissionsConfig[$key]['router']);
                $access = array_merge($access, $permissionsConfig[$key]['router']);
                continue;
            }
        }

        return [
            'access' => $access,
            'modify' => $modifies,
        ];
    }

    private function getForm()
    {
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('user/user');
        $data = [];

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['validate'])) {
            $data['error_validate'] = $this->error['validate'];
        } else {
            $data['error_validate'] = '';
        }

        /* breadcrumb */
        $data['breadcrumbs'] = [];

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_setting'),
            'href' => $this->url->link('settings/settings', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_account_setting'),
            'href' => $this->url->link('settings/account', 'user_token=' . $this->session->data['user_token'], true)
        );

        if (isset($this->request->get['user_group_id']) && $this->request->get['user_group_id']) {
            $user_group = $this->model_user_user_group->getUserGroup($this->request->get['user_group_id']);
            $data['permission_name'] = $user_group['name'];
            $data['block_access_all'] = !$user_group['access_all'];
            $data['permissions'] = $this->getUserPermission($user_group);
            $data['action'] = $this->url->link('settings/user_group/edit', 'user_token=' . $this->session->data['user_token'], true);
            $data['user_group_id'] = $this->request->get['user_group_id'];

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('breadcrumb_edit_group'),
                'href' => ''
            );
            $data['title'] = $this->language->get('heading_edit');
        } else {
            $data['permissions'] = $this->getPermissionConfig();
            $data['action'] = $this->url->link('settings/user_group', 'user_token=' . $this->session->data['user_token'], true);

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('breadcrumb_add_group'),
                'href' => ''
            );
            $data['title'] = $this->language->get('heading_add');
        }

        $data['action_cancel'] = $this->url->link('settings/account', 'user_token=' . $this->session->data['user_token'], true);

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $data['user_token'] = $this->session->data['user_token'];


        $this->response->setOutput($this->load->view('setting/user_group', $data));
    }

    public function delete()
    {
        $this->load->model('user/user_group');
        $this->load->language('settings/user_group');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && isset($this->request->post['user_group_id'])) {
            $count_user = $this->model_user_user_group->getCountUserInGroup($this->request->post['user_group_id']);
            if($count_user == 0 & $this->request->post['user_group_id'] != 1 & $this->request->post['user_group_id'] != 10) {
                $this->model_user_user_group->deleteUserGroup($this->request->post['user_group_id']);
                $this->session->data['success'] = $this->language->get('text_success_delete');
            } else {
                $this->session->data['error'] = $this->language->get('text_error_delete');
            }
        }
        $this->response->redirect($this->url->link('settings/account', 'user_token=' . $this->session->data['user_token'], true));
    }
    private function getUserPermission($user_group)
    {
        $permissions = $this->getPermissionConfig();
        foreach ($permissions as $key => &$permission) {
            // check if all permission are in access and modifies rule of group
            if (count(array_intersect($permission['router'], $user_group['permission']['access'])) === count($permission['router']) &&
                count(array_intersect($permission['router'], $user_group['permission']['modify'])) === count($permission['router'])
            ) {
                $permission['value'] = 2;
            } elseif (count(array_intersect($permission['router'], $user_group['permission']['access'])) === count($permission['router'])) {
                // check if all permission are in access rule of group
                $permission['value'] = 1;
            } else {
                $permission['value'] = 0;
            }
        }
        return $permissions;
    }

    private function validateForm() {
        $this->load->language('settings/user_groups');
        $this->load->model('user/user_group');

        if (!$this->user->hasPermission('modify', 'settings/user_group')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if(!isset($this->request->post['name']) || (isset($this->request->post['isset']) && $this->request->post['amount'] == '')) {
            $this->error['validate'][] = $this->language->get('error_name_empty');
        }

        if(isset($this->request->post['name']) && strlen($this->request->post['name']) > 50) {
            $this->error['validate'][] = $this->language->get('error_name_length');
        }

        $user_group = $this->model_user_user_group->getUserGroupByName($this->request->post['name'], isset($this->request->post['user_group_id']) ? $this->request->post['user_group_id'] : null);
        if(!empty($user_group)) {
            $this->error['validate'][] = $this->language->get('error_name_unique');
        }

        return !$this->error;
    }

    private function getPermissionConfig()
    {
        $this->load->language('settings/user_group');

        return [
            'order' => ['router' => ['sale/order'], 'name' =>  $this->language->get('text_order')],
            'return_receipt' => ['router' => ['sale/return_receipt'], 'name' => $this->language->get('text_return_receipt')],
            'product' => [
                'router' => [
                    'catalog/product',
                    'catalog/warehouse'
                ],
                'name' => $this->language->get('text_product')
            ],
            'collection' => ['router' => ['catalog/collection'], 'name' => $this->language->get('text_collection')],
            'manufacturer' => ['router' => ['catalog/manufacturer'], 'name' => $this->language->get('text_manufacturer')],
            'store_receipt' => ['router' => ['catalog/store_receipt'], 'name' => $this->language->get('text_store_receipt')],
            'store_transfer_receipt' => ['router' => ['catalog/store_transfer_receipt'], 'name' => $this->language->get('text_store_transfer_receipt')],
            'store_take_receipt' => ['router' => ['catalog/store_take_receipt'], 'name' => $this->language->get('text_store_take_receipt')],
            'cost_adjustment_receipt' => ['router' => ['catalog/cost_adjustment_receipt'], 'name' => $this->language->get('text_cost_adjustment_receipt')],
            'list_store' => ['router' => ['catalog/store'], 'name' => $this->language->get('text_list_store')],
            'customer' => ['router' => ['customer/customer'], 'name' => $this->language->get('text_customer')],
            'customer_group' => ['router' => ['customer/customer_group'], 'name' => $this->language->get('text_customer_group')],
            'blog' => ['router' => ['blog/blog'], 'name' => $this->language->get('text_blog')],
            'ingredient' => ['router' => ['ingredient/ingredient'], 'name' => $this->language->get('text_ingredient')],
            'cash_flow' => [
                'router' => ['cash_flow/cash_flow', 'cash_flow/receipt_voucher', 'cash_flow/payment_voucher',],
                'name' => $this->language->get('text_cash_flow')
            ],
            'report' => [
                'router' => ['report/overview', 'report/order', 'report/store', 'report/product', 'report/staff', 'report/financial',],
                'name' => $this->language->get('text_report')],
            'discount' => [
                'router' => ['discount/discount', 'discount/setting', 'discount/coupon'],
                'name' => $this->language->get('text_discount')
            ],
            'website' => [
                'router' => [
                    'section/sections',
                    'custom/theme',
                    'blog/blog',
                    'blog/category',
                    'online_store/contents',
                    'custom/menu',
                    'online_store/domain',
                    'custom/preference',
                    'common/filemanager',
                    'extension/module/theme_builder_config',
                    'rate/rate'
                ],
                'name' => $this->language->get('text_website'),
                'disable_access' => true,
            ],
            'pos' => ['router' => ['sale_channel/pos_novaon'],
                'name' => $this->language->get('text_pos'),
                'disable_access' => true,
            ],
            'shopee' => [
                'router' => ['sale_channel/shopee', 'sale_channel/shopee_upload',],
                'name' => $this->language->get('text_shopee'),
                'disable_access' => true,
                'disable_modify' => true,
            ],

            'lazada' => [
                'router' => [
                    'sale_channel/lazada',
                ],
                'name' => $this->language->get('text_lazada'),
                'disable_access' => true,
                'disable_modify' => true,
            ],
            'social' => [
                'router' => ['sale_channel/novaonx_social'],
                'name' => $this->language->get('text_novaonx_social'),
                'disable_access' => true,
                'disable_modify' => true,
            ],
            'setting' => [
                'router' => [
                    'setting/setting',
                    'settings/account',
                    'settings/classify',
                    'settings/delivery',
                    'settings/general',
                    'settings/payment',
                    'settings/notify',
                    'settings/settings',
                ],
                'name' => $this->language->get('text_setting'),
                'disable_access' => true,
            ],
        ];
    }
}