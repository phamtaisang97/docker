<?php

class ControllerSettingsClassify extends Controller
{
    const CONF_KEY_IMPORT_EXCEL_CATEGORY_STATUS = 'config_import_excel_category_status';

    public function index()
    {
        $this->load->language('settings/classify');

        $this->document->addScript('view/javascript/pagination/table-sortable-category.js');
        $this->document->addScript('view/javascript/pagination/table-sortable-manufacture.js');
        $this->document->addScript('view/javascript/pagination/table-sortable-tag.js');
        $this->document->addScript('view/javascript/pagination/table-sortable-store.js');
        $this->document->addStyle('view/stylesheet/custom/table-sortable.min.css');
        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/category');
        $this->load->model('catalog/manufacturer');
        $this->load->model('custom/tag');
        $this->load->model('setting/store');

        /* breadcrumb */
        $data['breadcrumbs'] = [];

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_setting'),
            'href' => $this->url->link('settings/settings', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => ''
        );

        $data['add_category_link'] = $this->url->link('settings/classify/addCategory', 'user_token=' . $this->session->data['user_token'], true);
        $data['edit_category_link'] = $this->url->link('settings/classify/editCategory', 'user_token=' . $this->session->data['user_token'], true);
        $data['delete_category_link'] = $this->url->link('settings/classify/deleteCategory', 'user_token=' . $this->session->data['user_token'], true);
        $data['get_child_category'] = $this->url->link('settings/classify/getChildCategory', 'user_token=' . $this->session->data['user_token'], true);
        $data['update_category_sort_link'] = $this->url->link('settings/classify/updateCategorySort', 'user_token=' . $this->session->data['user_token'], true);
        $data['avtivestatus'] = $this->url->link('settings/classify/updateStatusCategory', 'user_token=' . $this->session->data['user_token'], true);

        $data['add_category_title'] = $this->url->link('settings/classify/addCategoryTitle', 'user_token=' . $this->session->data['user_token'], true);

        $data['add_manufacture_link'] = $this->url->link('settings/classify/addManufacture', 'user_token=' . $this->session->data['user_token'], true);
        $data['edit_manufacture_link'] = $this->url->link('settings/classify/editManufacture', 'user_token=' . $this->session->data['user_token'], true);
        $data['delete_manufacture_link'] = $this->url->link('settings/classify/deleteManufacture', 'user_token=' . $this->session->data['user_token'], true);

        $data['add_tag_link'] = $this->url->link('settings/classify/addTag', 'user_token=' . $this->session->data['user_token'], true);
        $data['edit_tag_link'] = $this->url->link('settings/classify/editTag', 'user_token=' . $this->session->data['user_token'], true);
        $data['delete_tag_link'] = $this->url->link('settings/classify/deleteTag', 'user_token=' . $this->session->data['user_token'], true);

        $data['add_store_link'] = $this->url->link('settings/classify/addStore', 'user_token=' . $this->session->data['user_token'], true);
        $data['edit_store_link'] = $this->url->link('settings/classify/editStore', 'user_token=' . $this->session->data['user_token'], true);
        $data['delete_store_link'] = $this->url->link('settings/classify/deleteStore', 'user_token=' . $this->session->data['user_token'], true);

        // WHY comment? TODO: remove...
        /*$categories = $this->model_catalog_category->getCategories(['sort' => "c1.date_modified", 'order' => "DESC"]);
        foreach ($categories as &$category) {
            $result = $this->model_catalog_category->getProductInCategory($category['category_id']);
            $category['count_product'] = $result['total'];
        }
        unset($category);*/

        // for categories
        $url = '';
        if (isset($this->request->get['filter_name_category'])) {
            $url .= '&filter_name_category=' . urlencode(html_entity_decode($this->request->get['filter_name_category'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $filter_name_category = isset($this->request->get['filter_name_category']) ? urldecode(html_entity_decode($this->request->get['filter_name_category'], ENT_QUOTES, 'UTF-8')) : '';

        $item_per_page_category = 5;
        $filter = [
            "filter_name" => $filter_name_category,
            "page" => $page,
            "start" => ($page - 1) * $item_per_page_category,
            "limit" => $item_per_page_category
        ];

        $data['url_filter'] = $this->url->link('settings/classify', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $categories = $this->model_catalog_category->getCategoriesForClassify($filter);
        $categories_check = $this->model_catalog_category->getCategoriesCheck();

        // category 3 levels , TODO update for multiple levels
        /*foreach ($categories as &$category) {
            $category['count_product'] = $this->model_catalog_category->getTotalProductByCategoryId($category['id']);

            if (!empty($category['children'])) {
                foreach ($category['children'] as &$child) {
                    $child['count_product'] = $this->model_catalog_category->getTotalProductByCategoryId($child['id']);
                    if (!empty($child['children'])) {
                        foreach ($child['children'] as &$subChild) {
                            $subChild['count_product'] = $this->model_catalog_category->getTotalProductByCategoryId($subChild['id']);
                        }
                    }
                }
            }
            unset($child);
        }
        unset($category);*/
        $data['categories'] = $this->addCountProducts($categories);
        $data['categories_check'] = json_encode($categories_check, JSON_HEX_APOS);

        $categories_total = $this->model_catalog_category->countCategoriesParent($filter);
        $data['results'] = sprintf($this->language->get('text_pagination'), ($categories_total) ? (($page - 1) * $item_per_page_category) + 1 : 0, ((($page - 1) * $item_per_page_category) > ($categories_total - $item_per_page_category)) ? $categories_total : ((($page - 1) * $item_per_page_category) + $item_per_page_category), $categories_total, ceil($categories_total / $item_per_page_category));

        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $categories_total;
        $pagination->page = $page;
        $pagination->limit = $item_per_page_category;
        $pagination->url = $this->url->link('settings/classify', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        $data['pagination'] = $pagination->render();
        // end categories

        $manufactures = $this->model_catalog_manufacturer->getManufacturers(array('sort' => 'manufacturer_id', 'order' => 'DESC'));
        foreach ($manufactures as &$manufacture) {
            $result = $this->model_catalog_manufacturer->getProductInManufacture($manufacture['manufacturer_id']);
            $manufacture['count_product'] = $result['total'];
        }
        unset($manufacture);
        $data['manufactures'] = json_encode($manufactures, JSON_HEX_APOS);

        $tags = $this->model_custom_tag->getTagPaginator(array('sort' => 'tag_id'));
        $data['tags'] = json_encode($tags, JSON_HEX_APOS);

        $stores = $this->model_setting_store->getStores();
        $data['stores'] = json_encode($stores, JSON_HEX_APOS);

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];
            unset($this->session->data['error']);
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        /* check setting enable import category from excel */
        $this->load->model('setting/setting');
        $config_import_excel_category = $this->model_setting_setting->getSettingValue(self::CONF_KEY_IMPORT_EXCEL_CATEGORY_STATUS);
        $data['enabled_import_excel_category'] = $config_import_excel_category == 1 ? 1 : 0;

        $data['href_import_excel_category_sample'] = $this->config->get('config_language') == 'vi-vn'
            ? SAMPLE_CATEGORY_UPLOAD
            : SAMPLE_CATEGORY_UPLOAD_EN;

        $data['user_token'] = $this->session->data['user_token'];

        /* category title */
        $keys = array(
            'config_category_title'
        );
        $data['category_title'] = $this->model_setting_setting->getMultiSettingValue($keys);

        /* common */
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data'] && isset($this->request->get['fitler_type']) && $this->request->get['fitler_type'] == 'category') {
            $this->response->setOutput($this->load->view('settings/classify_category_append', $data));
        } else {
            $this->response->setOutput($this->load->view('settings/classify', $data));
        }
    }

    public function getChildCategory()
    {
        $this->load->language('settings/classify');
        $this->load->model('catalog/category');

        $json = $this->model_catalog_category->getChildCategory($this->request->get['category_id']);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function addCategory()
    {
        $this->load->language('settings/classify');
        $this->load->model('catalog/category');

        $validate = true;
        $data = $this->request->post;
        if (isset($this->request->post['noindex'])) {
            $data['noindex'] = 1;
        }
        else {
            $data['noindex'] = 0;
        }

        if (isset($this->request->post['nofollow'])) {
            $data['nofollow'] = 1;
        }
        else {
            $data['nofollow'] = 0;
        }

        if (isset($this->request->post['noarchive'])) {
            $data['noarchive'] = 1;
        }
        else {
            $data['noarchive'] = 0;
        }

        if (isset($this->request->post['noimageindex'])) {
            $data['noimageindex'] = 1;
        }
        else {
            $data['noimageindex'] = 0;
        }

        if (isset($this->request->post['nosnippet'])) {
            $data['nosnippet'] = 1;
        }
        else {
            $data['nosnippet'] = 0;
        }

        if (isset($this->request->post['name'])) {
            $data['name'] = htmlspecialchars_decode($data['name']);
            $data['category_description'] = $this->request->post['name'];
            $data['parent_id'] = 0;
        }

        if (!isset($this->request->post['name']) || $this->request->post['name'] == '') {
            $this->session->data['error'] = $this->language->get('error_name_category_empty');
            $validate = false;
        } elseif ($this->model_catalog_category->getCategoryByName($this->request->post)) {
            $this->session->data['error'] = $this->language->get('error_name_category_isset');
            $validate = false;
        }

        if ($validate) {
            $this->model_catalog_category->addCategory($data);
            $this->session->data['success'] = $this->language->get('add_category_success');
        }

        $this->response->redirect($this->url->link('settings/classify', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function updateCategorySort()
    {
        $this->load->model('catalog/category');

        try {
            $this->model_catalog_category->updateCategorySort($this->request->post['id'], $this->request->post['sort_order']);
            $this->response->setOutput(json_encode(['code' => 1, 'message' => 'success']));
        } catch (Exception $e) {
            $this->response->setOutput(json_encode(['code' => -1, 'message' => 'error']));
        }
    }

    public function editCategory()
    {
        $this->load->language('settings/classify');
        $this->load->model('catalog/category');

        $validate = true;

        $data = $this->request->post;

        if (isset($this->request->post['noindex'])) {
            $data['noindex'] = 1;
        }
        else {
            $data['noindex'] = 0;
        }

        if (isset($this->request->post['nofollow'])) {
            $data['nofollow'] = 1;
        }
        else {
            $data['nofollow'] = 0;
        }

        if (isset($this->request->post['noarchive'])) {
            $data['noarchive'] = 1;
        }
        else {
            $data['noarchive'] = 0;
        }

        if (isset($this->request->post['noimageindex'])) {
            $data['noimageindex'] = 1;
        }
        else {
            $data['noimageindex'] = 0;
        }

        if (isset($this->request->post['nosnippet'])) {
            $data['nosnippet'] = 1;
        }
        else {
            $data['nosnippet'] = 0;
        }

        if (isset($this->request->post['name'])) {
            $data['name'] = htmlspecialchars_decode($data['name']);
            $data['category_description'] = $this->request->post['name'];
        }

        $data['old_name'] = isset($data['old_name']) ? htmlspecialchars_decode($data['old_name']) : '';

        /* validate related discount */
        $related_discounts_for_category = [];
        if (is_array($related_discounts_for_category) && !empty($related_discounts_for_category)) {
            $this->session->data['error'] = $this->language->get('edit_category_error_has_discount');
            $validate = false;
        }

        if ($validate) {
            if (!isset($this->request->post['name']) || $this->request->post['name'] == '') {
                $this->session->data['error'] = $this->language->get('error_name_category_empty');
                $validate = false;
            }
        }

        if ($validate) {
            if ($this->model_catalog_category->getCategoryByName($this->request->post)) {
                $this->session->data['error'] = $this->language->get('error_name_category_isset');
                $validate = false;
            }
        }

        if ($validate) {
            $this->model_catalog_category->editCategory($this->request->post['id'], $data);
            $this->session->data['success'] = $this->language->get('edit_category_success');
        }

        $this->response->redirect($this->url->link('settings/classify', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function deleteCategory()
    {
        $this->load->language('settings/classify');
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');

        try {
            $error = '';

            /* validate related discount */
            $related_discounts_for_category = [];
            if (is_array($related_discounts_for_category) && !empty($related_discounts_for_category)) {
                $error = $this->language->get('delete_category_error_has_discount');
            }

            /* validate related products */
            if (!$error) {
                if (count($this->model_catalog_product->getProductsByCategoryId($this->request->post['id']))) {
                    $error = $this->language->get('delete_category_error');
                }
            }

            if ($error) {
                $this->session->data['error'] = $error;
            } else {
                $this->model_catalog_category->deleteCategory($this->request->post['id']);
                $this->session->data['success'] = $this->language->get('delete_category_success');
            }
        } catch (Exception $e) {
            $this->session->data['error'] = $this->language->get('delete_category_error');
        }

        $this->response->redirect($this->url->link('settings/classify', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function addCategoryTitle()
    {
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $data = $this->request->post;
            $data = $this->dataMap($data);
            $this->load->language('settings/classify');
            $this->load->model('setting/setting');
            $this->model_setting_setting->updateMultiSettingValue($data);
            $this->session->data['success'] = $this->language->get('text_success_update');
            $this->response->redirect($this->url->link('settings/classify', 'user_token=' . $this->session->data['user_token'], true));
        } else {
            $this->index();
        }
    }

    private function dataMap($data)
    {
        $result['config_category_title'] = isset($data['category_title']) ? $data['category_title'] : '';

        return $result;
    }

    private function validateForm()
    {
        $this->load->language('settings/classify');
        if (utf8_strlen(trim($this->request->post['category_title'])) > 200) {
            $this->session->data['error'] = $this->language->get('error_category_title');
            $this->session->data['error'] = $this->language->get('error_category_title');

            return false;
        }

        return !$this->error;
    }

    public function addManufacture()
    {
        $this->load->language('settings/classify');
        $this->load->model('catalog/manufacturer');

        $validate = true;

        if (!isset($this->request->post['name']) || $this->request->post['name'] == '') {
            $this->session->data['error'] = $this->language->get('error_name_manufacture_empty');
            $validate = false;
        } elseif ($this->model_catalog_manufacturer->getManufactureByName($this->request->post)) {
            $this->session->data['error'] = $this->language->get('error_name_manufacture_isset');
            $validate = false;
        }

        if ($validate) {
            $this->model_catalog_manufacturer->addManufacturerFast($this->request->post['name']);
            $this->session->data['success'] = $this->language->get('add_manufacture_success');
        }

        $this->response->redirect($this->url->link('settings/classify', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function editManufacture()
    {
        $this->load->language('settings/classify');
        $this->load->model('catalog/manufacturer');

        $validate = true;

        /* validate related discount */
        $related_discounts_for_category = [];
        if (is_array($related_discounts_for_category) && !empty($related_discounts_for_category)) {
            $this->session->data['error'] = $this->language->get('edit_manufacture_error_has_discount');
            $validate = false;
        }

        if ($validate) {
            if (!isset($this->request->post['name']) || $this->request->post['name'] == '') {
                $this->session->data['error'] = $this->language->get('error_name_manufacture_empty');
                $validate = false;
            }
        }

        if ($validate) {
            if ($this->model_catalog_manufacturer->getManufactureByName($this->request->post)) {
                $this->session->data['error'] = $this->language->get('error_name_manufacture_isset');
                $validate = false;
            }
        }

        if ($validate) {
            $this->model_catalog_manufacturer->editManufactureName($this->request->post);
            $this->session->data['success'] = $this->language->get('edit_manufacture_success');
        }

        $this->response->redirect($this->url->link('settings/classify', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function deleteManufacture()
    {
        $this->load->language('settings/classify');
        $this->load->model('catalog/manufacturer');
        $this->load->model('catalog/product');

        try {
            $error = '';

            /* validate related discount */
            $related_discounts_for_category = [];
            if (is_array($related_discounts_for_category) && !empty($related_discounts_for_category)) {
                $error = $this->language->get('delete_manufacture_error_has_discount');
            }

            /* validate related products */
            if (!$error) {
                if ($this->model_catalog_product->getTotalProductsByManufacturerId($this->request->post['id'])) {
                    $error = $this->language->get('delete_manufacture_error');
                }
            }

            if ($error) {
                $this->session->data['error'] = $error;
            } else {
                $this->model_catalog_manufacturer->deleteManufacturer($this->request->post['id']);
                $this->session->data['success'] = $this->language->get('delete_manufacture_success');
            }
        } catch (Exception $e) {
            $this->session->data['error'] = $this->language->get('delete_manufacture_error');
        }

        $this->response->redirect($this->url->link('settings/classify', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function addTag()
    {
        $this->load->language('settings/classify');
        $this->load->model('custom/tag');

        $validate = true;

        if (!isset($this->request->post['name']) || $this->request->post['name'] == '') {
            $this->session->data['error'] = $this->language->get('error_name_tag_empty');
            $validate = false;
        } elseif ($this->model_custom_tag->getTagByName($this->request->post)) {
            $this->session->data['error'] = $this->language->get('error_name_tag_isset');
            $validate = false;
        }

        if ($validate) {
            $this->model_custom_tag->addTagFast($this->request->post['name']);
            $this->session->data['success'] = $this->language->get('add_tag_success');
        }

        $this->response->redirect($this->url->link('settings/classify', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function editTag()
    {
        $this->load->language('settings/classify');
        $this->load->model('custom/tag');

        $validate = true;

        if (!isset($this->request->post['name']) || $this->request->post['name'] == '') {
            $this->session->data['error'] = $this->language->get('error_name_tag_empty');
            $validate = false;
        } elseif ($this->model_custom_tag->getTagByName($this->request->post)) {
            $this->session->data['error'] = $this->language->get('error_name_tag_isset');
            $validate = false;
        }

        if ($validate) {
            $this->model_custom_tag->editTagName($this->request->post);
            $this->session->data['success'] = $this->language->get('edit_tag_success');
        }

        $this->response->redirect($this->url->link('settings/classify', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function deleteTag()
    {
        $this->load->language('settings/classify');
        $this->load->model('custom/tag');
        try {
            $this->model_custom_tag->deleteTagAll($this->request->post['id']);
            $this->session->data['success'] = $this->language->get('delete_tag_success');
        } catch (Exception $e) {
            $this->session->data['error'] = $this->language->get('delete_tag_error');
        }

        $this->response->redirect($this->url->link('settings/classify', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function addStore()
    {
        $this->load->language('settings/classify');
        $this->load->model('setting/store');

        $validate = true;

        if (!isset($this->request->post['name']) || $this->request->post['name'] == '') {
            $this->session->data['error'] = $this->language->get('error_name_store_empty');
            $validate = false;
        }

        if ($validate) {
            $data = array(
                'config_name' => $this->request->post['name'],
                'config_url' => '',
                'config_ssl' => ''
            );
            $this->model_setting_store->addStore($data);
            $this->session->data['success'] = $this->language->get('add_store_success');
        }

        $this->response->redirect($this->url->link('settings/classify', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function editStore()
    {
        $this->load->language('settings/classify');
        $this->load->model('setting/store');

        $validate = true;

        if (!isset($this->request->post['name']) || $this->request->post['name'] == '') {
            $this->session->data['error'] = $this->language->get('error_name_store_empty');
            $validate = false;
        }

        if ($validate) {
            $data = array(
                'config_name' => $this->request->post['name'],
                'config_url' => '',
                'config_ssl' => ''
            );
            $this->model_setting_store->editStore($this->request->post['id'], $data);
            $this->session->data['success'] = $this->language->get('edit_store_success');
        }

        $this->response->redirect($this->url->link('settings/classify', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function deleteStore()
    {
        $this->load->language('settings/classify');
        $this->load->model('setting/store');

        try {
            $this->model_setting_store->deleteStore($this->request->post['id']);
            $this->session->data['success'] = $this->language->get('delete_store_success');
        } catch (Exception $e) {
            $this->session->data['error'] = $this->language->get('delete_store_error');
        }

        $this->response->redirect($this->url->link('settings/classify', 'user_token=' . $this->session->data['user_token'], true));
    }

    /**
     * importFromExcelFile
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function importFromExcelFile()
    {
        $this->load->language('settings/classify');
        $this->load->model('catalog/category');

        $result = [];

        /* validate file size */
        $file = $this->request->files['import-file'];
        $max_file_size = defined('MAX_FILE_SIZE_IMPORT_CATEGORIES') ? MAX_FILE_SIZE_IMPORT_CATEGORIES : 1048576;
        if (!isset($file['size']) || $file['size'] > $max_file_size) {
            $result['error'] = $this->language->get('error_filesize');
            $this->responseJson($result);

            return;
        }

        /* validate file mime type */
        $allowed = [
            'application/vnd.ms-excel', // xls
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' // xlsx
        ];

        if (!in_array($file['type'], $allowed)) {
            $result['error'] = $this->language->get('error_filetype');
            $this->responseJson($result);

            return;
        }

        /* read sheet data */
        try {
            // start set read data only
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($file['tmp_name']);
            $reader->setReadDataOnly(true);
            // end

            $spreadsheet = $reader->load($file['tmp_name']);
            $firstSheetIndex = $spreadsheet->getFirstSheetIndex();
            $sheetData = $spreadsheet->getSheet($firstSheetIndex)->toArray(null, true, true, true);
        } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
            $result['error'] = $this->language->get('error_unknown');
            $this->responseJson($result);

            return;
        }

        /* validate max rows */
        $max_file_rows = defined('MAX_ROWS_IMPORT_CATEGORIES') ? MAX_ROWS_IMPORT_CATEGORIES : 501;
        if (count($sheetData) > $max_file_rows) {
            $result['error'] = $this->language->get('error_limit');
            $this->responseJson($result);

            return;
        }

        /* validate header */
        $header = array_shift($sheetData);
        $header = $this->mapHeaderFromFile($header); /// remove empty element
        if (!$header) {
            $result['error'] = $this->language->get('error_file_incomplete');
            $this->responseJson($result);

            return;
        }

        /* validate header */
        if (!array_key_exists('level_1', $header) || !array_key_exists('level_2', $header) || !array_key_exists('level_3', $header)) {
            $result['error'] = $this->language->get('error_not_found_fields');
            $this->responseJson($result);

            return;
        }

        /* map sheet data to category model data */
        $dataCategory = $this->mapDataCategoryFromFile($sheetData, $header);
        if (array_key_exists('status', $dataCategory) && !$dataCategory['status']) { // check for parent
            $row = array_key_exists('row', $dataCategory) ? $dataCategory['row'] : 'UNKNOWN';
            $result['error'] = $this->language->get($dataCategory['error']) . $row;
            $this->responseJson($result);

            return;
        }

        /* import categories to db */
        $last_id_level_1 = 0;
        $last_id_level_2 = 0;
        foreach ($dataCategory as $category) {
            $c_data = [
                "parent_id" => 0,
                "status" => $category['status'],
                "category_description" => $category['name'],
                "name" => $category['name'],
                "image" => $category['image'],
                "sub_categories" => [],
            ];

            // get parent_id that be saved before
            switch ($category['level']) {
                case 2:
                    $c_data['parent_id'] = $last_id_level_1;
                    break;

                case 3:
                    $c_data['parent_id'] = $last_id_level_2;
                    break;

                default:
                    $c_data['parent_id'] = 0;
                    break;
            }

            $category_id = $this->model_catalog_category->addCategory($c_data);
            if (empty($category_id) || $category_id <= 0) {
                $result['error'] = $this->language->get('error_unknown');
                $this->responseJson($result);

                return;
            }

            // update last_id variables
            if (1 == $category['level']) {
                $last_id_level_1 = $category_id;
            } else {
                $last_id_level_2 = $category_id;
            }
        }

        $result['success'] = $this->language->get('text_uploaded') . ' ' . count($dataCategory) . $this->language->get('text_product_uploaded');

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    public function getProductsByCategoryId()
    {
        $this->load->language('settings/classify');
        $this->load->model('catalog/category');

        $json = $this->model_catalog_category->getProductsByCategoryId($this->request->get['category_id']);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function updateStatusCategory()
    {
        $this->load->model('catalog/category');
        $category_id = (int)$this->request->get['category_id'];
        $status = (isset($this->request->get['status']) && $this->request->get['status'] == 'true') ? 1: 0;
        $this->model_catalog_category->updateStatusCategory($category_id, $status);
    }

    private function mapHeaderFromFile(array $header)
    {
        $arrMap = array(
            $this->language->get('export_file_col_level_1') => 'level_1',
            $this->language->get('export_file_col_level_2') => 'level_2',
            $this->language->get('export_file_col_level_3') => 'level_3',
            $this->language->get('export_file_col_status') => 'status',
            $this->language->get('export_file_col_image_url') => 'image_url'
        );

        $arrMap_2 = array(
            $this->language->get('export_file_col_level_1_2') => 'level_1',
            $this->language->get('export_file_col_level_2_2') => 'level_2',
            $this->language->get('export_file_col_level_3_2') => 'level_3',
            $this->language->get('export_file_col_status_2') => 'status',
            $this->language->get('export_file_col_image_url_2') => 'image_url'
        );

        $result = array();
        foreach ($header as $key => $value) {
            $value = trim($value);
            if (array_key_exists($value, $arrMap)) {
                $result[$arrMap[$value]] = $key;
            }
        }

        // for import different language
        if (count($result) < count($arrMap)) {
            foreach ($header as $key => $value) {
                $value = trim($value);
                if (array_key_exists($value, $arrMap_2)) {
                    $result[$arrMap_2[$value]] = $key;
                }
            }
        }

        $flag = 1;
        foreach ($arrMap as $k => $v) {
            if (!array_key_exists($v, $result)) {
                $flag = 0;
                break;
            }
        }

        // for import different language
        if (!$flag) {
            foreach ($arrMap_2 as $k => $v) {
                if (!array_key_exists($v, $result)) {
                    return false;
                }
            }
        }

        return $result;
    }

    /**
     * @param array $data
     * @param array $header
     * @return array format as
     *
     * [
     *     "level_1": "",
     *     "level_2": "",
     *     "level_3": "",
     *     "status": "",
     *     "image_url": "",
     *     "level": "",
     *     "name": "",
     *     "image": "",
     * ]
     */
    private function mapDataCategoryFromFile(array $data, array $header)
    {
        if (!is_array($data) || empty($data) || !is_array($header) || empty($header)) {
            return [];
        }

        $this->load->model('catalog/category');

        $result = [];
        $level_checking = 3;
        $row = 1;
        foreach ($data as $row => $info) {
            if ($this->isArrayEmpty($info)) {
                continue;
            }

            $current = $header;
            // validate if levels format is invalid: have more than 1 level in 1 row
            $array_levels = [
                $info[$header['level_1']],
                $info[$header['level_2']],
                $info[$header['level_3']]
            ];

            if (1 != count(array_filter($array_levels))) {
                return [
                    'status' => false,
                    'error' => 'error_file_wrong_level_format_in_row',
                    'row' => $row + 2
                ];
            }

            foreach ($current as $k => $v) {
                switch ($k) {
                    case 'level_1':
                        if ('' != trim($info[$v])) {
                            $current['level'] = 1;
                            $current['name'] = $info[$v];
                        }
                        break;

                    case 'level_2':
                        if ('' != trim($info[$v])) {
                            $current['level'] = 2;
                            $current['name'] = $info[$v];
                        }
                        break;

                    case 'level_3':
                        if ('' != trim($info[$v])) {
                            $current['level'] = 3;
                            $current['name'] = $info[$v];
                        }
                        break;

                    case 'image_url':
                        $current['image'] = $info[$v];
                        break;
                }
                $current[$k] = $info[$v];
            }

            // validate if levels format is invalid: wrong level
            switch ($level_checking) {
                case 1: // level must be 1,2 ( < 3 )
                    if (2 < $current['level']) {
                        return [
                            'status' => false,
                            'error' => 'error_file_wrong_level_format_in_row',
                            'row' => $row + 2
                        ];
                    }
                    break;

                default: // $level_checking = 0|2 => all cases are valid
                    break;
            }

            // update level checking
            $level_checking = $current['level'];

            $ch = $this->validateRequireFields($current);
            if (!$ch) {
                return [
                    'status' => false,
                    'error' => 'error_file_incomplete_in_row',
                    'row' => $row + 2
                ];
            }

            // check if category name already exists
            if ($this->model_catalog_category->getCategoryByName($current)) {
                return [
                    'status' => false,
                    'error' => 'error_file_category_exist_in_row',
                    'row' => $row + 2
                ];
            }

            $result[] = $current;
        }

        // check if there are duplicated category names in uploaded file content
        $result_column = array_column($result, 'name');
        $result_unique = array_unique($result_column);
        if (count($result_unique) < count($result)) {
            return [
                'status' => false,
                'error' => 'error_file_category_exist_in_row',
                'row' => $row + 2
            ];
        }

        return $result;
    }

    private function isArrayEmpty($arr)
    {
        if (!is_array($arr)) {
            return false;
        }

        foreach ($arr as $key => $value) {
            if (!empty($value) || $value != NULL || $value != "") {
                return false;

                // no need break here? TODO: remove...
                break;//stop the process we have seen that at least 1 of the array has value so its not empty
            }
        }

        return true;
    }

    private function validateRequireFields($data)
    {
        if (!array_key_exists('name', $data) || trim($data['name']) == '') {
            return false;
        }

        if (!array_key_exists('status', $data) || !in_array(trim($data['status']), array(1, 0, '1', '0'), true)) {  // category product must be 0 or 1
            return false;
        }

        if (array_key_exists('image_url', $data) && !empty($data['image_url'])) {
            $preg_check = preg_match("/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/", trim($data['image_url']));
            if (false === $preg_check || 0 === $preg_check) {
                return false;
            }
        }

        return true;
    }

    function getTotalProductByCategoryId($id)
    {
        return $this->model_catalog_category->getTotalProductByCategoryId($id);
    }

    function addCountProducts($categories)
    {
        if (empty($categories) || !is_array($categories)) {
            return [];
        }

        foreach ($categories as $key => $category) {
            $category['count_product'] = $this->getTotalProductByCategoryId($category['id']);
            $category['children'] = $this->addCountProducts($category['children']);

            $categories[$key] = $category;
        }

        return $categories;
    }
}
