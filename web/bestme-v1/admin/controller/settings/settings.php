<?php

class ControllerSettingsSettings extends Controller
{
    public function index()
    {
        $this->load->language('settings/settings');

        $this->document->setTitle($this->language->get('heading_title'));

        $data = [];

        $data['general_setting_link'] = $this->url->link('settings/general', 'user_token=' . $this->session->data['user_token'], true);
        $data['account_setting_link'] = $this->url->link('settings/account', 'user_token=' . $this->session->data['user_token'], true);
        $data['payment_setting_link'] = $this->url->link('settings/payment', 'user_token=' . $this->session->data['user_token'], true);
        $data['delivery_setting_link'] = $this->url->link('settings/delivery', 'user_token=' . $this->session->data['user_token'], true);
        $data['classify_setting_link'] = $this->url->link('settings/classify', 'user_token=' . $this->session->data['user_token'], true);
        $data['notify_setting_link'] = $this->url->link('settings/notify', 'user_token=' . $this->session->data['user_token'], true);

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];

            unset($this->session->data['error']);
        } /*else {
            $data['error_warning'] = '';
        }*/

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }
        $this->response->setOutput($this->load->view('settings/settings', $data));
    }
}