<?php

class ControllerSettingsPayment extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('settings/payment');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        $this->getList();
    }

    public function add()
    {
        $this->load->language('settings/payment');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm('add')) {
            /* get existing payment config */
            $payment_methods = $this->model_setting_setting->getSettingValue('payment_methods');
            $payment_methods = json_decode($payment_methods, true);
            if (!is_array($payment_methods)) {
                $payment_methods = [];
            }

            /* check if payment method existed */
            $is_existed = false;
            foreach ($payment_methods as $payment_method) {
                if (isset($payment_method['payment_id']) && $payment_method['payment_id'] == $this->request->post['payment_id']) {
                    $is_existed = true;
                    break;
                }
            }

            /* add if not existed */
            if (!$is_existed) {
                $payment_methods[] = $this->request->post;
                $this->model_setting_setting->editSettingValue('payment', 'payment_methods', $payment_methods);

                $this->session->data['success'] = $this->language->get('text_success');

                $this->response->redirect($this->url->link('settings/payment', 'user_token=' . $this->session->data['user_token'], true));
            }
        }

        $this->getForm();
    }

    public function edit()
    {
        $this->load->language('settings/payment');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm('edit')) {
            /* get existing payment config */
            $payment_methods = $this->model_setting_setting->getSettingValue('payment_methods');
            $payment_methods = json_decode($payment_methods, true);
            if (!is_array($payment_methods)) {
                $payment_methods = [];
            }

            /* check if payment method existed */
            $is_existed = false;
            foreach ($payment_methods as &$payment_method) {
                if (isset($payment_method['payment_id']) && $payment_method['payment_id'] == $this->request->post['payment_id']) {
                    $payment_method = $this->request->post;
                    $is_existed = true;
                    break;
                }
            }
            unset($payment_method);

            /* update if existed */
            if ($is_existed) {
                if (!$this->statusValid($payment_methods)) {
                    $this->error['warning'] = $this->language->get('error_require_and_active');
                } else {
                    $this->model_setting_setting->editSettingValue('payment', 'payment_methods', $payment_methods);

                    $this->session->data['success'] = $this->language->get('text_success');

                    $this->response->redirect($this->url->link('settings/payment', 'user_token=' . $this->session->data['user_token'], true));
                }
            }
        }

        $this->getForm();
    }

    public function editStatus()
    {
        $this->load->language('settings/payment');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm('edit')) {
            /* get existing payment config */
            $payment_methods = $this->model_setting_setting->getSettingValue('payment_methods');
            $payment_methods = json_decode($payment_methods, true);
            if (!is_array($payment_methods)) {
                $payment_methods = [];
            }

            /* check if payment method existed */
            $is_existed = false;
            foreach ($payment_methods as &$payment_method) {
                if (isset($payment_method['payment_id']) && $payment_method['payment_id'] == $this->request->post['payment_id']) {
                    $payment_method = $this->request->post;
                    $is_existed = true;
                    break;
                }
            }
            unset($payment_method);

            /* update if existed */
            if ($is_existed) {
                if (!$this->statusValid($payment_methods)) {
                    $this->error['warning'] = $this->language->get('error_require_and_active');
                } else {
                    $this->model_setting_setting->editSettingValue('payment', 'payment_methods', $payment_methods);

                    $this->session->data['success'] = $this->language->get('text_success');

                    $this->response->redirect($this->url->link('settings/payment', 'user_token=' . $this->session->data['user_token'], true));
                }
            }
        }

        $this->getForm();
    }

    public function delete()
    {
        $this->load->language('settings/payment');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateDelete()) {
            /* get existing payment config */
            $payment_methods = $this->model_setting_setting->getSettingValue('payment_methods');
            $payment_methods = json_decode($payment_methods, true);
            if (!is_array($payment_methods)) {
                $payment_methods = [];
            }

            /* check if payment method existed */
            $is_existed = false;
            foreach ($payment_methods as $idx => $payment_method) {
                if (isset($payment_method['payment_id']) && $payment_method['payment_id'] == $this->request->post['payment_id']) {
                    array_splice($payment_methods, $idx, 1);
                    $is_existed = true;
                    break;
                }
            }

            /* update if existed */
            if ($is_existed) {
                if (!$this->statusValid($payment_methods)) {
                    $this->error['warning'] = $this->language->get('error_require_and_active');
                } else {
                    $this->model_setting_setting->editSettingValue('payment', 'payment_methods', array_values($payment_methods));

                    $this->session->data['success'] = $this->language->get('text_success');

                    $this->response->redirect($this->url->link('settings/payment', 'user_token=' . $this->session->data['user_token'], true));
                }
            }
        }

        $this->getList();
    }

    public function getList()
    {
        $data = [];

        /* breadcrumb */
        $data['breadcrumbs'] = [];

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_setting'),
            'href' => $this->url->link('settings/settings', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_payment_setting'),
            'href' => ''
        );

        /* message */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        /* payment methods */
        $data['payment_methods'] = $this->getPaymentMethods();

        /* mark as listing */
        $data['current_action'] = 'list';

        /* href */
        $data['href_add_new_payment'] = $this->url->link('settings/payment/add', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_edit_payment'] = $this->url->link('settings/payment/edit', 'user_token=' . $this->session->data['user_token'] . '&payment_id=', true);
        $data['href_delete_payment'] = $this->url->link('settings/payment/delete', 'user_token=' . $this->session->data['user_token'] . '&payment_id=', true);

        /* common */
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('settings/payment', $data));
    }

    protected function getForm()
    {
        $data = [];

        /* breadcrumb */
        $data['breadcrumbs'] = [];

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_setting'),
            'href' => $this->url->link('settings/settings', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_payment_setting'),
            'href' => ''
        );

        /* message */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        /* payment methods */
        $data['payment_methods'] = $this->getPaymentMethods();

        /* href */
        $data['href_add_new_payment'] = $this->url->link('settings/payment/add', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_edit_payment'] = $this->url->link('settings/payment/edit', 'user_token=' . $this->session->data['user_token'] . '&payment_id=', true);
        $data['href_delete_payment'] = $this->url->link('settings/payment/delete', 'user_token=' . $this->session->data['user_token'] . '&payment_id=', true);

        if (isset($this->request->get['payment_id'])) {
            $data['payment_id'] = $this->request->get['payment_id'];
        }

        /* mark as listing */
        if (!isset($this->request->get['payment_id'])) {
            $data['current_action'] = 'add';
        } else {
            $data['current_action'] = 'edit';
        }

        /* form fields */
        if (!isset($this->request->get['payment_id'])) {
            if (isset($this->request->post['payment_id'])) {
                $data['payment_id'] = $this->request->post['payment_id'];
            } else {
                $data['payment_id'] = '';
            }
        }

        if (isset($this->request->post['name'])) {
            $data['name'] = $this->request->post['name'];
        } else {
            $data['name'] = '';
        }

        if (isset($this->request->post['guide'])) {
            $data['guide'] = $this->request->post['guide'];
        } else {
            $data['guide'] = '';
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } else {
            $data['status'] = '';
        }

        /* href */
        $data['href_cancel'] = $this->url->link('settings/payment', 'user_token=' . $this->session->data['user_token'], true);

        /* common */
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('settings/payment', $data));
    }

    protected function validateForm($action)
    {
        if (!$this->user->hasPermission('modify', 'setting/setting')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        /* payment_id */
        if (!isset($this->request->post['payment_id'])) {
            $this->error['payment_id'] = $this->language->get('error_payment_id');
            $this->error['warning'] = $this->language->get('error_payment_id');
        }

        /* name */
        if (!isset($this->request->post['name']) ||
            (utf8_strlen(trim($this->request->post['name'])) < 1)
        ) {
            $this->error['name'] = $this->language->get('error_name_min');
            $this->error['warning'] = $this->language->get('error_name_min');
        }

        if (utf8_strlen(trim($this->request->post['name'])) > 255) {
            $this->error['name'] = $this->language->get('error_name_max');
            $this->error['warning'] = $this->language->get('error_name_max');
        }

        /* guide */
        if (isset($this->request->post['guide']) &&
            (utf8_strlen($this->request->post['guide']) > 1000)
        ) {
            $this->error['guide'] = $this->language->get('error_guide');
            $this->error['warning'] = $this->language->get('error_guide');
        }

        /* status */
        if (!isset($this->request->post['status']) ||
            !in_array($this->request->post['status'], [0, '0', 1, '1'])
        ) {
            $this->error['guide'] = $this->language->get('error_guide');
            $this->error['warning'] = $this->language->get('error_guide');
        }

        /* validate if existed due to action */
        // get existing payment config
        $payment_methods = $this->model_setting_setting->getSettingValue('payment_methods');
        $payment_methods = json_decode($payment_methods, true);
        if (!is_array($payment_methods)) {
            $payment_methods = [];
        }

        switch ($action) {
            case 'add':
                // check if name existed
                $is_existed = false;
                foreach ($payment_methods as $payment_method) {
                    if (isset($payment_method['name']) && $payment_method['name'] == $this->request->post['name']) {
                        $is_existed = true;
                        break;
                    }
                }

                if ($is_existed) {
                    $this->error['name'] = $this->language->get('error_payment_isset');
                    $this->error['warning'] = $this->language->get('error_payment_isset');
                }

                break;

            case 'edit':
                // check if payment_id existed
                $is_existed = false;
                foreach ($payment_methods as $payment_method) {
                    if (isset($payment_method['name']) && $payment_method['name'] == $this->request->post['name'] && $this->request->post['payment_id'] != $payment_method['payment_id']) {
                        $is_existed = true;
                        break;
                    }
                }
                if ($is_existed) {
                    $this->error['name'] = $this->language->get('error_payment_isset');
                    $this->error['warning'] = $this->language->get('error_payment_isset');
                }

                break;
        }

        return !$this->error;
    }

    protected function validateDelete()
    {
        if (!$this->user->hasPermission('modify', 'settings/payment')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        $this->load->model('setting/setting');

        // TODO: validate...

        return !$this->error;
    }

    private function statusValid($payment_methods){
        $statusValid = false;
        foreach ($payment_methods as $method) {
            if (!isset($method['status'])) {
                continue;
            }
            if ($method['status'] == 1) {
                $statusValid = true;
                break;
            }
        }

        return $statusValid;
    }

    /**
     * @return array format as
     *
     * [
     *     [
     *         'payment_id' => 1,
     *         'name' => 'Thanh toán khi nhận hàng (COD)',
     *         'guide' => 'Hướng dẫn Thanh toán khi nhận hàng (COD): khi nhận hàng vui lòng thanh toán toàn bộ cho nhân viên giao hàng',
     *         'status' => 1
     *     ],
     *     ...
     * ]
     *
     */
    protected function getPaymentMethods()
    {
        /* get existing payment config */
        $payment_methods = $this->model_setting_setting->getSettingValue('payment_methods');
        $payment_methods = json_decode($payment_methods, true);
        if (!is_array($payment_methods)) {
            $payment_methods = [];
        }

        return $payment_methods;
    }
}