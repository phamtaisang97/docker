<?php

class ControllerSettingsAccount extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('settings/account');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('user/user');
        $data = [];

        /* breadcrumb */
        $data['breadcrumbs'] = [];

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_setting'),
            'href' => $this->url->link('settings/settings', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_account_setting'),
            'href' => ''
        );

        $admin_user = $this->model_user_user->getUser($this->user->getId());
        if (!$admin_user || !isset($admin_user['type']) || $admin_user['type'] !== 'Admin') {
            /* show edit user form if not admin user */
            $this->response->redirect($this->url->link('settings/account/editStaff', 'user_token=' . $this->session->data['user_token'] . '&id=' . $this->user->getId(), true));
        }

        /* make admin user data */
        $this->load->model('tool/image');
        if (is_file(DIR_IMAGE . $admin_user['image']) || $this->model_tool_image->imageIsExist($admin_user['image'])) {
            $image = $this->model_tool_image->resize($admin_user['image'], 45, 45);
        } else {
            $image = 'view/image/custom/icons/avatar.svg';
        }

        $last_logged_in = sprintf($this->language->get('staff_account_last_login'), '--:-- --', '--/--/----');
        try {
            $last_logged_in_obj = date_create_from_format('Y-m-d H:i:s', isset($admin_user['last_logged_in']) ? $admin_user['last_logged_in'] : '');
            if ($last_logged_in_obj instanceof DateTime) {
                $last_logged_in = sprintf($this->language->get('staff_account_last_login'), $last_logged_in_obj->format('g:i A'), $last_logged_in_obj->format('d/m/Y'));
            }
        } catch (Exception $e) {
        }

        $data['admin_user'] = [
            'username' => $admin_user['username'],
            'firstname' => $admin_user['firstname'],
            'lastname' => $admin_user['lastname'],
            'avatar' => $image,
            'last_login' => $last_logged_in
        ];

        /* make staff users data */
        $users = $this->model_user_user->getUsers();

        $data['staff_users'] = [];
        $this->load->model('user/user_group');
        foreach ($users as $user) {
            if ($user['type'] !== 'Staff') {
                continue;
            }

//            if ($this->checkIsFullPermission(json_decode($user['permission']))) {
//                $permission = $this->language->get('staff_account_permission_full');
//            } else {
//                $permission = $this->language->get('staff_account_permission_limit');
//            }
            $user_group =  $this->model_user_user_group->getUserGroup($user['user_group_id']);
            $last_logged_in = sprintf($this->language->get('staff_account_last_login'), '--:-- --', '--/--/----');
            try {
                $last_logged_in_obj = date_create_from_format('Y-m-d H:i:s', isset($user['last_logged_in']) ? $user['last_logged_in'] : '');
                if ($last_logged_in_obj instanceof DateTime) {
                    $last_logged_in = sprintf($this->language->get('staff_account_last_login'), $last_logged_in_obj->format('g:i A'), $last_logged_in_obj->format('d/m/Y'));
                }
            } catch (Exception $e) {
            }

            $data['staff_users'][] = array(
                'id' => $user['user_id'],
                'avatar' => $user['image'] != '' ? $user['image'] : 'view/image/custom/icons/avatar.svg',
                'username' => $user['username'],
                'firstname' => $user['firstname'],
                'lastname' => $user['lastname'],
                'last_login' => $last_logged_in,
                'user_group_name' => $user_group ? $user_group['name'] : '',
                'href_edit_account' => $this->url->link('settings/account/editStaff', 'user_token=' . $this->session->data['user_token'] . '&id=' . $user['user_id'], true)
            );
        }
        $list_groups_staffs = $this->model_user_user_group->getUserGroupWithCountStaff();
        $data['number_group_staff'] = !empty($list_groups_staffs) ? count($list_groups_staffs) : 0;
        $data['list_groups_staffs'] = $list_groups_staffs;
        /* href */
        $data['href_add_user_group'] = $this->url->link('settings/user_group', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_edit_user_group'] = $this->url->link('settings/user_group/edit', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_admin_account_setting'] = $this->url->link('settings/account/edit', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_add_account'] = $this->url->link('settings/account/addStaff', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_kill_all_sessions'] = $this->url->link('settings/account/killAllSessions', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_delete_user_group'] = $this->url->link('settings/user_group/delete', 'user_token=' . $this->session->data['user_token'], true);

        //config packet
        $data['config_packet_paid_start'] = $this->formatOutputDate($this->config->get('config_packet_paid_start'));
        $data['config_packet_paid_type'] = $this->config->get('config_packet_paid_type');
        $data['config_packet_paid_end'] = $this->formatOutputDate($this->config->get('config_packet_paid_end'));
        $data['config_shop_disabled'] = $this->config->get('config_shop_disabled');
        $data['bestme_pricing'] = BESTME_SERVER_PRICING;

        $data['config_packet_unlimited_capacity'] = $this->config->get('config_packet_unlimited_capacity');
        $data['config_packet_unlimited_stores'] = $this->config->get('config_packet_unlimited_stores');
        $data['config_packet_unlimited_staffs'] = $this->config->get('config_packet_unlimited_staffs');

        $data['config_packet_capacity'] = $this->config->get('config_packet_capacity');
        $data['config_packet_number_of_stores'] = $this->config->get('config_packet_number_of_stores');
        $data['config_packet_number_of_staffs'] = $this->config->get('config_packet_number_of_staffs');

        if($data['config_packet_unlimited_capacity'] != '1') {
            $this->load->model('custom/image');
            $total_image_size = $this->model_custom_image->getTotalSizeImages();
            $data['total_image_size'] = round($total_image_size/ 1e6,3);
        }

        $data['count_staff'] = count($data['staff_users']);
        $this->load->model('setting/store');
        $stores = $this->model_setting_store->getStores();
        $data['count_store'] = count($stores);

        if ($data['config_packet_paid_type']) {
            $data['config_packet_paid_name'] = $this->language->get($data['config_packet_paid_type']);
        }
        //set trial package
        $config_packet_paid = $this->config->get('config_packet_paid');
        if ($config_packet_paid != 1) {
            $data['config_packet_paid_name'] = $this->language->get('trial');
            $data['config_packet_paid_start'] = $this->formatOutputDate($this->config->get('config_packet_trial_start'));
            $config_packet_trial_period = $this->config->get('config_packet_trial_period');
            $data['config_packet_paid_end'] = $this->formatOutputDate($this->config->get('config_packet_trial_start') . ' + ' . $config_packet_trial_period . ' days');
        }

        /* common */
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];

            unset($this->session->data['error']);
        } /*else {
            $data['error_warning'] = '';
        }*/

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }
        $this->response->setOutput($this->load->view('settings/account', $data));
    }


    public function killAllSessions()
    {
        $this->load->language('settings/account');

        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $json = [
                'code' => 400,
                'message' => $this->language->get('staff_account_kill_all_sessions_failed') . ' (Required: POST method)'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }

        $this->load->model('user/user');

        /* check if admin user */
        $admin_user = $this->model_user_user->getUser($this->user->getId());
        if (!$admin_user || !isset($admin_user['type']) || $admin_user['type'] !== 'Admin') {
            $json = [
                'code' => 400,
                'message' => $this->language->get('staff_account_kill_all_sessions_failed')
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }

        /* do kill all sessions */
        $users = $this->model_user_user->getUsers();

        // except admin user
        $users = array_filter($users, function ($user) {
            return isset($user['user_id']) && $user['user_id'] != $this->user->getId();
        });

        $result = $this->session->killSessionsByUsers($users);

        if ($result) {
            $json = [
                'code' => 200,
                'message' => $this->language->get('staff_account_kill_all_sessions_success')
            ];
        } else {
            $json = [
                'code' => 400,
                'message' => $this->language->get('staff_account_kill_all_sessions_failed')
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function killUserSessions()
    {
        $this->load->language('settings/account');

        if (($this->request->server['REQUEST_METHOD'] !== 'POST') || !isset($this->request->post['user_id'])) {
            $json = [
                'code' => 400,
                'message' => $this->language->get('staff_account_kill_all_sessions_failed') . ' (Required: POST method)'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }

        $users = array(
            [
                'user_id' => $this->request->post['user_id']
            ]
        );

        $result = $this->session->killSessionsByUsers($users);

        if ($result) {
            $json = [
                'code' => 200,
                'message' => $this->language->get('staff_account_kill_all_sessions_success')
            ];
        } else {
            $json = [
                'code' => 400,
                'message' => $this->language->get('staff_account_kill_all_sessions_failed')
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function edit()
    {
        $this->load->language('settings/account_edit');

        $this->document->setTitle($this->language->get('heading_title'));

        $data = [];

        /* breadcrumb */
        $data['breadcrumbs'] = [];

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_setting'),
            'href' => $this->url->link('settings/settings', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_account_setting'),
            'href' => $this->url->link('settings/account', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_admin_account_setting'),
            'href' => ''
        );

        /* skip button */
        $data['href_breadcrumb_setting'] = $this->url->link('settings/account', 'user_token=' . $this->session->data['user_token'], true);

        $this->load->model('user/user');

        $this->load->model('tool/image');

        $user_info = $this->model_user_user->getUser($this->user->getId());

        if ($user_info) {
            $data['user_id'] = $user_info['user_id'];
            $data['user_group_id'] = $user_info['user_group_id'];
            $data['firstname'] = $user_info['firstname'];
            $data['lastname'] = $user_info['lastname'];
            $data['username'] = $user_info['username'];
            $data['email'] = $user_info['email'];
            $data['telephone'] = $user_info['telephone'];
            $data['info'] = $user_info['info'];
            $data['user_group'] = $user_info['user_group'];
            $data['type'] = $user_info['type'];
            $data['last_logged_in'] = isset($user_info['last_logged_in']) ? $user_info['last_logged_in'] : '';

            if (is_file(DIR_IMAGE . $user_info['image']) || $this->model_tool_image->imageIsExist($user_info['image'])) {
                $data['image'] = $this->model_tool_image->resize($user_info['image'], 45, 45);
            } else {
                $data['image'] = 'view/image/custom/icons/avatar.svg';
            }
        }
        // TODO: data...
        $data['action'] = $this->url->link('settings/account/editAdmin', 'user_token=' . $this->session->data['user_token'], true);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');
        /* text validate */
        $data['error_empty'] = $this->language->get('error_empty');
        $data['error_input_max'] = $this->language->get('error_input_max');
        $data['error_input_telephone'] = $this->language->get('error_input_telephone');
        $data['error_input_max_min_password'] = $this->language->get('error_input_max_min_password');
        $data['error_number'] = $this->language->get('error_number');

        $this->response->setOutput($this->load->view('settings/account_edit', $data));
    }

    public function editAdmin()
    {
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && isset($this->request->post['user_id'])) {
            $user = array(
                'user_group_id' => $this->request->post['user_group_id'],
                'username' => $this->request->post['username'],
                'firstname' => $this->request->post['firstname'],
                'lastname' => $this->request->post['lastname'],
                'email' => $this->request->post['email'],
                'telephone' => $this->request->post['telephone'],
                'image' => $this->request->post['avatar'],
                'status' => 1,
                'type' => 'Admin',
                'permission' => '',
                'info' => $this->request->post['info']
            );
            if ($this->request->post['password']) {
                $user['password'] = $this->request->post['password'];
            }
            $this->load->model('user/user');
            $this->load->language('settings/account');
            $this->model_user_user->editUser($this->request->post['user_id'], $user);
            $this->session->data['success'] = $this->language->get('txt_success_edit_admin');
//            if ($this->request->post['password']) {
//                $this->killUserSessions();
//                $this->user->logout();
//
//                unset($this->session->data['user_token']);
//
//                $this->response->redirect($this->url->link('common/login', '', true));
//            }
            $current_user_info = $this->model_user_user->getUser($this->user->getId());
            if (isset($current_user_info['type']) && $current_user_info['type'] == 'Admin') {
                $this->response->redirect($this->url->link('settings/account', 'user_token=' . $this->session->data['user_token'], true));
            } else {
                $this->response->redirect($this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true));
            }
        } else {
            $this->edit();
        }
    }

    public function checkPass()
    {
        $this->load->language('settings/account');
        $this->response->addHeader('Content-Type: application/json');
        $json = [
            'status' => false,
            'message' => $this->language->get('txt_invalid_password'),
        ];
        if (isset($this->request->post['password'])) {
            $this->load->model('user/user');
            $user_info = $this->model_user_user->getUser($this->user->getId());
            if (isset($user_info['password']) && isset($user_info['salt'])) {
                $password = sha1($this->request->post['password']);
                $password = sha1($user_info['salt'] . $password);
                $password = sha1($user_info['salt'] . $password);
                if ($password == $user_info['password']) {
                    $json = [
                        'status' => false,
                        'message' => $this->language->get('txt_invalid_permission'),
                    ];
                    if (isset($user_info['type']) && ($user_info['type'] == 'Admin' || $user_info['type'] == 'Staff')) {
                        $json = [
                            'status' => true,
                            'message' => '',
                        ];
                    }
                }
            }
        }
        if (isset($this->request->post['action_validate']) && $this->request->post['action_validate'] == 'create_new') {
            // Kiểm tra xem shop đã thêm quá giới hạn nhân viên chưa
            if (isset($this->request->post['action_validate']) && $this->request->post['action_validate'] == 'create_new') {
                $config_packet_unlimited_staffs = $this->config->get('config_packet_unlimited_staffs');
                if (isset($config_packet_unlimited_staffs) && $config_packet_unlimited_staffs != '1') {
                    $config_packet_number_of_staffs = (int)$this->config->get('config_packet_number_of_staffs');
                    $this->load->model('user/user');
                    $count_staff = $this->model_user_user->getTotalStaff();
                    if ($count_staff >= $config_packet_number_of_staffs) {
                        $json = [
                            'status' => false,
                            'message' => $this->language->get('txt_invalid_max_staff'),
                        ];
                    }
                }
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    public function validateLimitedStaff() {
        $this->load->language('settings/account');
        $this->response->addHeader('Content-Type: application/json');
        $json = [
            'status' => true,
            'message' => '',
        ];
        $config_packet_unlimited_staffs = $this->config->get('config_packet_unlimited_staffs');
        if (isset($config_packet_unlimited_staffs) && $config_packet_unlimited_staffs != '1') {
            $config_packet_number_of_staffs = (int)$this->config->get('config_packet_number_of_staffs');
            $this->load->model('user/user');
            $count_staff = $this->model_user_user->getTotalStaff();
            if ($count_staff >= $config_packet_number_of_staffs) {
                $json = [
                    'status' => false,
                    'message' => $this->language->get('txt_invalid_max_staff'),
                ];
            }else{
                $json = [
                    'status' => true,
                    'message' => '',
                    'ratio_staff' => (int)($count_staff/$config_packet_number_of_staffs * 100)
                ];
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    public function addStaff()
    {
        $this->load->language('settings/account_staff_form');

        $this->document->setTitle($this->language->get('heading_title'));

        $data = [];

        $this->load->model('user/user');
        $current_user_info = $this->model_user_user->getUser($this->user->getId());
        $data['current_user_type'] = isset($current_user_info['type']) ? $current_user_info['type'] : '';

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        /* breadcrumb */
        $data['breadcrumbs'] = [];

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_setting'),
            'href' => $this->url->link('settings/settings', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_account_setting'),
            'href' => $this->url->link('settings/account', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_account_setting_add_staff'),
            'href' => ''
        );

        // support when create fail
        $user = [];
        if (isset($this->request->post['avatar'])) {
            $user['image'] = $this->request->post['avatar'];
        }
        if (isset($this->request->post['firstname'])) {
            $user['firstname'] = $this->request->post['firstname'];
        }
        if (isset($this->request->post['lastname'])) {
            $user['lastname'] = $this->request->post['lastname'];
        }
        if (isset($this->request->post['telephone'])) {
            $user['telephone'] = $this->request->post['telephone'];
        }
        if (isset($this->request->post['email'])) {
            $user['email'] = $this->request->post['email'];
        }
        if (isset($this->request->post['info'])) {
            $user['info'] = $this->request->post['info'];
        }
        $data['user_info'] = $user;
        $data['type_action'] = 'create_new_staff';

        // data store
        $this->load->model('setting/store');
        $stores = $this->model_setting_store->getStores();
        $data['stores'] = $stores;
        $data['user_token'] = $this->session->data['user_token'];

        /* skip button */
        $data['href_breadcrumb_setting'] = $this->url->link('settings/account', 'user_token=' . $this->session->data['user_token'], true);

        $data['action'] = $this->url->link('settings/account/staffAdd', 'user_token=' . $this->session->data['user_token'], true);
        $data['settings_link'] = $this->url->link('settings/account', 'user_token=' . $this->session->data['user_token'], true);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('settings/account_staff_form', $data));
    }

    public function editStaff()
    {
        if (isset($this->request->get['id'])) {
            $this->load->model('user/user');
            $user = $this->model_user_user->getUser($this->request->get['id']);
            if (!$user) {
                $this->response->redirect($this->url->link('settings/account', 'user_token=' . $this->session->data['user_token'], true));
            }

            if (isset($this->error['warning'])) {
                $data['error_warning'] = $this->error['warning'];
            } else {
                $data['error_warning'] = '';
            }

            if (isset($this->session->data['success'])) {
                $data['success'] = $this->session->data['success'];

                unset($this->session->data['success']);
            } else {
                $data['success'] = '';
            }

            $data['user_token'] = $this->session->data['user_token'];
            $data['user_id'] = $this->request->get['id'];
            $data['user_info'] = $user;
            $data['permission'] = isset($user['permission']) ? json_decode($user['permission']) : [];
            if(isset($user['user_group_id'])) {
                $this->load->model('user/user_group');
                $data['user_groups'] = json_encode([0 =>
                    $this->model_user_user_group->getDetailUserGroup($user['user_group_id'])]);

                $data['user_stores'] = json_encode($this->model_user_user->getUserStore($this->request->get['id']));
            }

            $current_user_info = $this->model_user_user->getUser($this->user->getId());
            $data['current_user_type'] = isset($current_user_info['type']) ? $current_user_info['type'] : '';

            $this->load->language('settings/account_staff_form');
            $this->load->language('settings/account');

            $this->document->setTitle($this->language->get('edit_heading_title'));

            $data['action'] = $this->url->link('settings/account/staffEdit', 'user_token=' . $this->session->data['user_token'], true);
            $data['settings_link'] = $this->url->link('settings/account', 'user_token=' . $this->session->data['user_token'], true);
            $data['custom_header'] = $this->load->controller('common/custom_header');
            $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
            $data['footer'] = $this->load->controller('common/footer');
            $data['href_kill_user_sessions'] = $this->url->link('settings/account/killUserSessions', 'user_token=' . $this->session->data['user_token'], true);
            $data['href_delete_user'] = $this->url->link('settings/account/staffDelete', 'user_token=' . $this->session->data['user_token'] . '&id=' . $this->request->get['id'], true);

            $data['error_empty'] = $this->language->get('error_empty');
            $data['error_input_max'] = $this->language->get('error_input_max');
            $data['error_input_max_min_telephone'] = $this->language->get('error_input_max_min_telephone');
            $data['error_input_max_min_password'] = $this->language->get('error_input_max_min_password');
            $data['error_number'] = $this->language->get('error_number');
            $data['error_mail_address'] = $this->language->get('error_mail_address');
            $data['type_action'] = 'edit_staff';
            $this->response->setOutput($this->load->view('settings/account_staff_form', $data));
        } else {
            // notice: should not use this, may be loop infinitive if query param "id" not set...
            $this->response->redirect($this->url->link('settings/account', 'user_token=' . $this->session->data['user_token'], true));
        }
    }

    public function staffAdd()
    {
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateStaffForm()) {
//            $permission = $this->buildPermission();
//            $permissionRaw = $this->buildPermissionRaw();
//            $user_group = array(
//                'name' => $this->request->post['telephone'],
//                'permission' => $permission
//            );
//            $this->load->model('user/user_group');
//            $user_group_id = $this->model_user_user_group->addUserGroup($user_group);
            // Auto genarate password
            $password = $this->generateRandomString();
            $user_group_id = $this->request->post['user_group'];
            if ($user_group_id) {
                $user = array(
                    'user_group_id' => $user_group_id,
                    'username' => $this->request->post['email'],
                    'password' => $password,
                    'firstname' => $this->request->post['firstname'],
                    'lastname' => $this->request->post['lastname'],
                    'email' => $this->request->post['email'],
                    'telephone' => $this->request->post['telephone'],
                    'image' => $this->request->post['avatar'],
                    'status' => 1,
                    'type' => 'Staff',
                    'permission' => [],
                    'info' => $this->request->post['info']
                );

                $this->load->language('settings/account');
                $this->load->model('user/user');
                $user_id = $this->model_user_user->addUser($user);
                if ($user_id) {
                    $this->publishCustomerStaff($user);
                    // send email to staff
                    $this->sendMailToStaff($user);
                }
                // Save user_store
                $stores = $this->request->post['store_staff'];
                if(isset($stores)) {
                    foreach ($stores as $store) {
                        $user_store = array(
                            'user_id' => $user_id,
                            'store_id' => $store
                        );
                        $this->model_user_user->addUserStore($user_store);
                    }
                }
                $this->session->data['success'] = $this->language->get('txt_success_add');
            }

            $this->response->redirect($this->url->link('settings/account', 'user_token=' . $this->session->data['user_token'], true));
        } else {
            $this->addStaff();
        }
    }

    private function generateRandomString($length = 10) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }
    public function staffEdit()
    {
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && isset($this->request->post['user_id'])) {
            $this->load->model('user/user');
            $this->load->model('user/user_group');
            $this->load->language('settings/account');
            $current_user_info = $this->model_user_user->getUser($this->user->getId());
            if ($current_user_info['type'] == 'Admin') {

            } else {
                $user = $this->model_user_user->getUser($this->request->post['user_id']);
                if (!$user) {
                    $this->response->redirect($this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true));
                }
                $permissionRaw = $user['permission'];
            }

            $user = array(
                'user_group_id' => $this->request->post['user_group'],
                'username' => $this->request->post['email'],
                'firstname' => $this->request->post['firstname'],
                'lastname' => $this->request->post['lastname'],
                'email' => $this->request->post['email'],
                'telephone' => $this->request->post['telephone'],
                'image' => $this->request->post['avatar'],
                'status' => 1,
                'type' => 'Staff',
                'permission' => [],
                'info' => $this->request->post['info']
            );
//            if (!is_null($this->request->post['password'])) {
//                $user['password'] = $this->request->post['password'];
//            }
            $this->model_user_user->editUser($this->request->post['user_id'], $user);
            // Save user_store
            $stores = $this->request->post['store_staff'];
            if(isset($stores)) {
                $this->model_user_user->editUserStore($this->request->post['user_id'],$stores);
            }
            $this->session->data['success'] = $this->language->get('txt_success_edit');

            if (isset($current_user_info['type']) && ($current_user_info['type'] == 'Staff' || $current_user_info['type'] == 'Admin')) {
                $this->response->redirect($this->url->link('settings/account', 'user_token=' . $this->session->data['user_token'], true));
            } else {
                $this->response->redirect($this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true));
            }
        } else {
            $this->editStaff();
        }
    }

    public function staffDelete()
    {
        if (isset($this->request->get['id'])) {
            $this->load->model('user/user_group');
            $this->load->model('user/user');
            $this->load->language('settings/account');

            $user = $this->model_user_user->getUser($this->request->get['id']);
            if ($user) {
                // $user_group_id = $user['user_group_id'];
                $this->model_user_user->deleteUser($this->request->get['id']);
                // if (!is_null($user_group_id)) {
                    // $this->model_user_user_group->deleteUserGroup($user_group_id);
                // }
                $this->session->data['success'] = $this->language->get('txt_success_delete');
                $this->publishCustomerStaff($user, 'delete');
            }
        }

        $this->response->redirect($this->url->link('settings/account', 'user_token=' . $this->session->data['user_token'], true));
    }

    protected function validateStaffForm()
    {
        if (!$this->user->hasPermission('modify', 'settings/account')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        $this->load->language('settings/account_staff_form');
        $this->load->model('user/user');

        if ((utf8_strlen($this->request->post['firstname']) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
            $this->error['firstname'] = $this->language->get('error_firstname');
        }

        if ((utf8_strlen($this->request->post['lastname']) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
            $this->error['lastname'] = $this->language->get('error_lastname');
        }

        if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
            $this->error['email'] = $this->language->get('error_email');
            $this->error['warning'] = $this->language->get('error_email');
        }

        if ((utf8_strlen($this->request->post['telephone']) != 10)) {
            $this->error['telephone'] = $this->language->get('error_telephone');
            $this->error['warning'] = $this->language->get('error_phone');
        }

        $user_info = $this->model_user_user->getUserByEmail($this->request->post['email']);
        if (!isset($this->request->get['user_id'])) {
            if ($user_info) {
                $this->error['warning'] = $this->language->get('error_exists');
            }
        } else {
            if ($user_info && ($this->request->get['user_id'] != $user_info['user_id'])) {
                $this->error['warning'] = $this->language->get('error_exists');
            }
        }

        // Kiểm tra xem shop đã vượt giới hạn nhân viên chưa thì thông báo và không cho tạo tiếp

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

    private function buildPermission()
    {
        $permission = array();
        if (isset($this->request->post['home_permission']) && $this->request->post['home_permission'] == 'on') {
            $home_persmission = [
                'common/dashboard',
                'common/filemanager'
            ];
            $permission = array_merge($permission, $home_persmission);
        }

        if (isset($this->request->post['order_permission']) && $this->request->post['order_permission'] == 'on') {
            $order_permission = [
                'sale/order',
                'sale/return_receipt',
                'common/filemanager'
            ];
            $permission = array_merge($permission, $order_permission);
        }

        if (isset($this->request->post['product_permission']) && $this->request->post['product_permission'] == 'on') {
            $product_permission = [
                'catalog/product',
                'catalog/category',
                'catalog/collection',
                'catalog/manufacturer',
                'catalog/warehouse',
                'common/filemanager'
            ];
            $permission = array_merge($permission, $product_permission);
        }

        if (isset($this->request->post['customer_permission']) && $this->request->post['customer_permission'] == 'on') {
            $customer_permission = [
                'customer/custom_field',
                'customer/customer',
                'customer/customer_approval',
                'customer/customer_group',
                'common/filemanager'
            ];
            $permission = array_merge($permission, $customer_permission);
        }

        if (isset($this->request->post['setting_permission']) && $this->request->post['setting_permission'] == 'on') {
            $setting_permission = [
                'settings/account',
                'settings/classify',
                'settings/delivery',
                'settings/general',
                'settings/payment',
                'settings/notify',
                'settings/settings',
                'setting/setting',
                'common/filemanager'
            ];
            $permission = array_merge($permission, $setting_permission);
        }

        if (isset($this->request->post['interface_permission']) && $this->request->post['interface_permission'] == 'on') {
            $interface_permission = [
                // theme builder extension
                'extension/module/theme_builder_config',
                // theme builder start point
                'custom/theme',
                // section setting in theme builder
                'section/banner',
                'section/best_sales_product',
                'section/best_views_product',
                'section/blog',
                'section/detail_product',
                'section/footer',
                'section/header',
                'section/hot_product',
                'section/list_product',
                'section/new_product',
                'section/partner',
                'theme/common/preview',
                'section/sections',
                'section/slideshow',
                'section_blog/blog_category',
                'section_blog/blog_list',
                'section_blog/sections',
                'section_blog/latest_blog',
                'section_category/banner',
                'section_category/filter',
                'section_category/product_category',
                'section_category/product_list',
                'section_category/sections',
                'section_contact/contact',
                'section_contact/form',
                'section_contact/map',
                'section_contact/sections',
                'section_product_detail/related_product',
                'section_product_detail/sections',
                // theme (general) setting in theme builder
                'theme/color',
                'theme/favicon',
                'theme/preview',
                'theme/section_theme',
                'theme/social',
                'theme/text',
                // image manager (for setting favicon, banner, ...)
                'common/filemanager'
            ];
            $permission = array_merge($permission, $interface_permission);
        }

        if (isset($this->request->post['menu_permission']) && $this->request->post['menu_permission'] == 'on') {
            $menu_permission = [
                'custom/menu',
                'custom/group_menu', // old. TODO: remove...
                'custom/menu_item', // old. TODO: remove...
                'common/filemanager'
            ];
            $permission = array_merge($permission, $menu_permission);
        }

        if (isset($this->request->post['content_permission']) && $this->request->post['content_permission'] == 'on') {
            $content_permission = [
                'online_store/contents',
                'blog/blog'
            ];
            $permission = array_merge($permission, $content_permission);
        }

        if (isset($this->request->post['domain_permission']) && $this->request->post['domain_permission'] == 'on') {
            $domain_permission = [
                'online_store/domain',
                'common/filemanager'
            ];
            $permission = array_merge($permission, $domain_permission);
        }

        return array(
            'access' => $permission,
            'modify' => $permission
        );
    }

    private function buildPermissionRaw()
    {
        $permission = array();
        if (isset($this->request->post['allPermission']) && $this->request->post['allPermission'] == 'on') {
            $permission[] = 'allPermission';
        }
        if (isset($this->request->post['home_permission']) && $this->request->post['home_permission'] == 'on') {
            $permission[] = 'home_permission';
        }
        if (isset($this->request->post['order_permission']) && $this->request->post['order_permission'] == 'on') {
            $permission[] = 'order_permission';
        }
        if (isset($this->request->post['product_permission']) && $this->request->post['product_permission'] == 'on') {
            $permission[] = 'product_permission';
        }
        if (isset($this->request->post['customer_permission']) && $this->request->post['customer_permission'] == 'on') {
            $permission[] = 'customer_permission';
        }
        if (isset($this->request->post['setting_permission']) && $this->request->post['setting_permission'] == 'on') {
            $permission[] = 'setting_permission';
        }
        if (isset($this->request->post['interface_permission']) && $this->request->post['interface_permission'] == 'on') {
            $permission[] = 'interface_permission';
        }
        if (isset($this->request->post['menu_permission']) && $this->request->post['menu_permission'] == 'on') {
            $permission[] = 'menu_permission';
        }
        if (isset($this->request->post['content_permission']) && $this->request->post['content_permission'] == 'on') {
            $permission[] = 'content_permission';
        }
        if (isset($this->request->post['domain_permission']) && $this->request->post['domain_permission'] == 'on') {
            $permission[] = 'domain_permission';
        }

        return $permission;
    }

    private function checkIsFullPermission($permission)
    {
        if (!is_array($permission)) {
            return false;
        }
        if (!in_array('home_permission', $permission) || !in_array('order_permission', $permission) || !in_array('product_permission', $permission)
            || !in_array('customer_permission', $permission) || !in_array('setting_permission', $permission) || !in_array('interface_permission', $permission)
            || !in_array('menu_permission', $permission) || !in_array('content_permission', $permission) || !in_array('domain_permission', $permission)) {
            return false;
        }

        return true;
    }

    private function formatOutputDate($date_string)
    {
        $date = strtotime($date_string);
        if (!is_integer($date) || $date <= 0) {
            return '-';
        }

        return date('d/m/Y', $date);
    }

    private function publishCustomerStaff($user, $action = 'add')
    {
        $this->load->model('setting/setting');
        $redis = $this->redis_connect();
        $data['email'] = $user['email'];
        $data['firstname'] = $user['firstname'];
        $data['lastname'] = $user['lastname'];
        $data['shopname'] = $this->model_setting_setting->getShopName();
        $data['phone'] = $user['telephone'];
        $data['action'] = $action;
        $data_json = json_encode($data);

        $redis->publish(MUL_REDIS_CUSTOMER_STAFF_PUBLISH_CHANEL, $data_json);
        $redis->close();
    }

    private function redis_connect()
    {
        $redis = new \Redis();
        $redis->connect(MUL_REDIS_HOST, MUL_REDIS_PORT);
        $redis->select(MUL_REDIS_DB);
//        echo "Connection to server sucessfully";
//        //check whether server is running or not
//        echo "Server is running: ".$redis->ping();

        return $redis;
    }

    public function getUserStoreLazyLoad() {
        $json = array();

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $this->load->model('setting/store');

        $filter_data = array(
            'order' => 'ASC',
            'start' => ($page - 1) * 6,
            'limit' => 6,
        );

        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        $filter_data['store_ids'] =  $this->user->getUserStores();

        $json['results'] = $this->model_setting_store->getUserStorePaginator($filter_data);

        $count_store = $this->model_setting_store->getTotalUserStoreFilter($filter_data);
        $json['count'] = $count_store;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function sendMailToStaff($data) {
        $this->load->language('mail/create_staff');
        $data['text_subject'] = sprintf($this->language->get('text_subject'), $data['firstname'] . ' ' . $data['lastname']);
        $data['text_greeting'] = $this->language->get('text_greeting');
        $data['text_end'] = $this->language->get('text_end');

        $mail = new Mail($this->config->get('config_mail_engine'));
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = MAIL_SMTP_HOST_NAME;
        $mail->smtp_username = MAIL_SMTP_USERNAME;
        $mail->smtp_password = html_entity_decode(MAIL_SMTP_PASSWORD, ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = MAIL_SMTP_PORT;
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($data['email']);
        $mail->setFrom($this->config->get('config_mail_smtp_username'));
        $mail->setSender(html_entity_decode($this->config->get('config_mail_smtp_username'), ENT_QUOTES, 'UTF-8'));
        $mail->setSubject('Thông báo tạo tài khoản nhân viên thành công');
        $mail->setHtml($this->load->view('mail/create_staff', $data));
        try {
            $mail->send();
        } catch (Exception $e) {
            $this->log->write('on staff created successfully sends mail got error: ' . $e->getMessage());
        }
    }
}