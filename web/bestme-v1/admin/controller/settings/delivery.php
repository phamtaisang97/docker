<?php

include_once DIR_APPLICATION . 'controller/common/delivery_api' . '.php';

class ControllerSettingsDelivery extends Controller
{
    private $error = array();
    public $deliveryApi;

    public function __construct($registry)
    {
        $this->deliveryApi = new ControllerCommonDeliveryApi($registry);
        parent::__construct($registry);
    }

    public function index()
    {
        $this->load->language('settings/delivery');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        $this->getList();
    }

    public function add()
    {
        $this->load->language('settings/delivery');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm('add')) {
            /* get existing delivery config */
            $delivery_methods = $this->model_setting_setting->getSettingValue('delivery_methods');
            $delivery_methods = json_decode($delivery_methods, true);
            if (!is_array($delivery_methods)) {
                $delivery_methods = [];
            }

            $delivery_method = $this->mapDelivery();
            $delivery_method['id'] = $this->getNextId($delivery_methods);

            /* add if not existed */
            $delivery_methods[] = $delivery_method;
            $this->model_setting_setting->editSettingValue('delivery', 'delivery_methods', $delivery_methods);

            $this->session->data['success'] = $this->language->get('text_success_add_delivery');

            $this->response->redirect($this->url->link('settings/delivery', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();
    }

    public function edit()
    {
        $this->load->language('settings/delivery');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm('edit')) {
            /* get existing delivery config */
            $delivery_methods = $this->getDeliveryMethods();

            /* check if delivery method existed */
            $is_existed = false;
            foreach ($delivery_methods as &$delivery_method) {
                if (isset($delivery_method['id']) && $delivery_method['id'] == $this->request->post['method-id']) {
                    $delivery_method = $this->mapDelivery();
                    $is_existed = true;
                    break;
                }
            }
            unset($delivery_method);

            /* update if existed */
            if ($is_existed) {
                $this->model_setting_setting->editSettingValue('delivery', 'delivery_methods', $delivery_methods);

                $this->session->data['success'] = $this->language->get('text_success');

                $this->response->redirect($this->url->link('settings/delivery', 'user_token=' . $this->session->data['user_token'], true));
            }
        }

        $this->getForm();
    }

    public function editStatus()
    {
        $this->load->language('settings/delivery');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');
        if (($this->request->server['REQUEST_METHOD'] == 'GET') && $this->validateEditStatus()) {
            /* get existing delivery config */
            $delivery_methods = $this->getDeliveryMethods();

            /* check if delivery method existed */
            $is_existed = false;
            foreach ($delivery_methods as &$delivery_method) {
                if (isset($delivery_method['id']) && $delivery_method['id'] == $this->request->get['delivery_id']) {
                    $status = isset($delivery_method['status']) ? $delivery_method['status']: '0';
                    if ($status == 0 || $status == '0') {
                        $delivery_method['status'] = 1;
                    } else {
                        $delivery_method['status'] = 0;
                    }
                    $is_existed = true;
                    break;
                }
            }
            unset($delivery_method);

            /* update if existed */
            if ($is_existed) {
                if (!$this->statusValid($delivery_methods)) {
                    $this->error['warning'] = $this->language->get('error_require_and_active');
                } else {
                    $this->model_setting_setting->editSettingValue('delivery', 'delivery_methods', $delivery_methods);

                    $this->session->data['success'] = $this->language->get('text_success_edit_status');

                    $this->response->redirect($this->url->link('settings/delivery', 'user_token=' . $this->session->data['user_token'], true));
                }
            }
        }

        $this->getList();
    }

    public function delete()
    {
        $this->load->language('settings/delivery');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');
        if (($this->request->server['REQUEST_METHOD'] == 'GET') && $this->validateDelete()) {
            /* get existing delivery config */
            $delivery_methods = $this->getDeliveryMethods();
            /* check if delivery method existed */
            $is_existed = false;
            foreach ($delivery_methods as $idx => $delivery_method) {
                if (isset($delivery_method['id']) && $delivery_method['id'] == $this->request->get['delivery_id']) {
                    array_splice($delivery_methods, $idx, 1);
                    $is_existed = true;
                    break;
                }
            }

            /* update if existed */
            if ($is_existed) {
                if (!$this->statusValid($delivery_methods)) {
                    $this->error['warning'] = $this->language->get('error_require_and_active');
                }else{
                    $this->model_setting_setting->editSettingValue('delivery', 'delivery_methods', array_values($delivery_methods));

                    $this->session->data['success'] = $this->language->get('text_success_delete');

                    $this->response->redirect($this->url->link('settings/delivery', 'user_token=' . $this->session->data['user_token'], true));
                }
            }
        }

        $this->getList();
    }

    public function getList()
    {
        $data = [];

        /* breadcrumb */
        $data['breadcrumbs'] = [];

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_setting'),
            'href' => $this->url->link('settings/settings', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_delivery_setting'),
            'href' => ''
        );

        /* message */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        /* delivery methods */
        $data['delivery_methods'] = $this->getDeliveryMethods();

        $this->load->model('localisation/vietnam_administrative');
        $provinces = $this->model_localisation_vietnam_administrative->getProvinces();
        $data['supported_provinces'] = array_map(function ($province) {
            $province_name = isset($province['name']) ? $province['name'] : '';
            $province_name = str_replace('Thành phố ', '', $province_name);
            $province_name = str_replace('Tỉnh ', '', $province_name);
            return $province_name;
        }, $provinces);

        /* mark as listing */
        $data['current_action'] = 'list';

        $this->load->model('appstore/my_app');
        /* delivery method 3th ghn*/
        if($this->model_appstore_my_app->checkAppActive(ModelAppstoreMyApp::APP_GHN)){
            $data['ghn_active'] = is_null($this->config->get('config_GHN_active')) ? false : true;
            $data['ghn_user_name'] = $this->config->get('config_GHN_username');
            $data['ghn_token'] = $this->config->get('config_GHN_token');
            $data['href_ghn_guide'] = TRANSPORT_GHN_GUIDE;
            $data['app_ghn_is_actived'] = true;
        };

        if($this->model_appstore_my_app->checkAppActive(ModelAppstoreMyApp::APP_VIETTEL_POST)){
            $data['viettel_post_token'] = $this->config->get('config_VIETTEL_POST_token');
            $data['vietpost_active'] = $this->config->get('config_VIETTEL_POST_active');
            $data['vietpost_user_name'] = $this->config->get('config_VIETTEL_POST_username');
            $data['href_viettelpost_guide'] = TRANSPORT_VIETTELPOST_GUIDE;
            $data['app_vtpost_is_actived'] = true;
        };

        $data['delivery_api_login'] = $this->url->link('common/delivery_api/login', 'user_token=' . $this->session->data['user_token'], true);
        $data['delivery_api_logout'] = $this->url->link('common/delivery_api/logout', 'user_token=' . $this->session->data['user_token'], true);
        $data['delivery_api_hubs'] = $this->url->link('common/delivery_api/getHubs', 'user_token=' . $this->session->data['user_token'], true);
        $data['address_shop'] = $this->config->get('config_address');
        $data['config_address_shop_link'] = $this->url->link('settings/general', 'user_token=' . $this->session->data['user_token'], true);
        $data['delivery_api_update_hubs'] = $this->url->link('common/delivery_api/updateHub', 'user_token=' . $this->session->data['user_token'], true);

        /* href */
        $data['href_add_new_delivery'] = $this->url->link('settings/delivery/add', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_edit_delivery'] = $this->url->link('settings/delivery/edit', 'user_token=' . $this->session->data['user_token'] . '&delivery_id=', true);
        $data['href_delete_delivery'] = $this->url->link('settings/delivery/delete', 'user_token=' . $this->session->data['user_token'] . '&delivery_id=', true);
        $data['href_change_status_delivery'] = $this->url->link('settings/delivery/editStatus', 'user_token=' . $this->session->data['user_token'] . '&delivery_id=', true);
        // TODO: rename to getOtp() instead of specific Ghn...
        $data['ghn_api_get_otp_url'] = $this->url->link('settings/delivery/getGhnOtp', 'user_token=' . $this->session->data['user_token'], true);

        /* common */
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');
        /* text error */
        $data['error_provinces'] = $this->language->get('error_provinces');
        $data['error_step_price'] = $this->language->get('error_step_price');
        $data['error_weight'] = $this->language->get('error_weight');
        $data['error_weight_from_to'] = $this->language->get('error_weight_from_to');

        $this->response->setOutput($this->load->view('settings/delivery', $data));
    }

    /**
     * get GHN OTP (Affiliate)
     *
     * TODO: rename to getOtp() instead of specific Ghn...
     */
    public function getGhnOtp() {
        $this->load->language('common/delivery_api');
        $json = [
            'status' => false,
            'message' => $this->language->get('text_error_choose_service')
        ];
        $transport_method = 'ghn';
        try {
            $transport = new Transport($transport_method, [], $this);
            if (!$transport->getAdaptor() instanceof \Transport\Abstract_Transport) {
                $this->responseJson($json);
            }

            $result = $transport->getAdaptor()->getOtp();
            if ($result) {
                $json = [
                    'status' => true,
                    'message' => $this->language->get('text_success')
                ];
            } else {
                $json = [
                    'status' => false,
                    'message' => isset($result['msg']) ? $result['msg'] : $this->language->get('text_error_unknown')
                ];
            }
        } catch (Exception $e) {
            $json = [
                'status' => false,
                'message' => $this->language->get('text_error_unknown')
            ];
        }

        $this->responseJson($json);
    }

    protected function getForm()
    {
        $data = [];

        /* breadcrumb */
        $data['breadcrumbs'] = [];

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_setting'),
            'href' => $this->url->link('settings/settings', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_delivery_setting'),
            'href' => ''
        );

        /* message */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        /* delivery methods */
        $data['delivery_methods'] = $this->getDeliveryMethods();

        $this->load->model('localisation/vietnam_administrative');
        $provinces = $this->model_localisation_vietnam_administrative->getProvinces();
        $data['supported_provinces'] = array_map(function ($province) {
            $province_name = isset($province['name']) ? $province['name'] : '';
            $province_name = str_replace('Thành phố ', '', $province_name);
            $province_name = str_replace('Tỉnh ', '', $province_name);
            return $province_name;
        }, $provinces);

        /* href */
        $data['href_add_new_delivery'] = $this->url->link('settings/delivery/add', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_edit_delivery'] = $this->url->link('settings/delivery/edit', 'user_token=' . $this->session->data['user_token'] . '&delivery_id=', true);
        $data['href_delete_delivery'] = $this->url->link('settings/delivery/delete', 'user_token=' . $this->session->data['user_token'] . '&delivery_id=', true);

        if (isset($this->request->get['delivery_id'])) {
            $data['delivery_id'] = $this->request->get['delivery_id'];
        }

        /* mark as listing */
        if (!isset($this->request->get['delivery_id'])) {
            $data['current_action'] = 'add';
        } else {
            $data['current_action'] = 'edit';
        }

        /* form fields */
        if (!isset($this->request->get['delivery_id'])) {
            if (isset($this->request->post['delivery_id'])) {
                $data['delivery_id'] = $this->request->post['delivery_id'];
            } else {
                $data['delivery_id'] = '';
            }
        }

        if (isset($this->request->post['name'])) {
            $data['name'] = $this->request->post['name'];
        } else {
            $data['name'] = '';
        }

        if (isset($this->request->post['guide'])) {
            $data['guide'] = $this->request->post['guide'];
        } else {
            $data['guide'] = '';
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } else {
            $data['status'] = '';
        }

        /* href */
        $data['href_cancel'] = $this->url->link('settings/delivery', 'user_token=' . $this->session->data['user_token'], true);

        /* common */
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('settings/delivery', $data));
    }

    protected function validateForm($action)
    {
        if (!$this->user->hasPermission('modify', 'setting/setting')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        /* delivery_id */
        if (!isset($this->request->post['method-id'])) {
            $this->error['delivery_id'] = $this->language->get('error_delivery_id');
            $this->error['warning'] = $this->language->get('error_delivery_id');
        }

        if (!isset($this->request->post['method-name']) ||
            (utf8_strlen(trim($this->request->post['method-name'])) < 1) ||
            (utf8_strlen(trim($this->request->post['method-name'])) > 100)
        ) {
            $this->error['name'] = $this->language->get('error_name');
            $this->error['warning'] = $this->language->get('error_name');
        }

        $method_id = $this->request->post['method-id'];
        // check if editing default delivery method
        if ($method_id == -1) {
            return !$this->error;
        }

        // else, validate user defined delivery method
        /* province */
        if (!isset($this->request->post['region-provinces'])) {
            $this->error['warning'] = $this->language->get('error_provinces');
        }

        /* validate if existed due to action */
        // get existing delivery config
        $delivery_methods = $this->getDeliveryMethods();
        if (!is_array($delivery_methods)) {
            $delivery_methods = [];
        }

        // check existed
        $check_name = true;
        $delivery_is_existed = false;
        foreach ($delivery_methods as $delivery_method) {
            if (isset($delivery_method['name']) && $delivery_method['name'] == $this->request->post['method-name']) {
                $check_name = false;
            }
            if (isset($delivery_method['id']) && $delivery_method['id'] == $this->request->post['method-id']) {
                if ($delivery_method['name'] == $this->request->post['method-name']) {
                    $check_name = true;  // not edit name
                }
                $delivery_is_existed = true;
            }
        }
        switch ($action) {
            case 'add':
                if (!$check_name) {
                    $this->error['name'] = $this->language->get('error_name');
                    $this->error['warning'] = $this->language->get('error_name');
                }

                break;

            case 'edit':
                if (!$check_name) {
                    $this->error['name'] = $this->language->get('error_name');
                    $this->error['warning'] = $this->language->get('error_name');
                }
                if (!$delivery_is_existed) {
                    $this->error['delivery_id'] = $this->language->get('error_delivery_id');
                    $this->error['warning'] = $this->language->get('error_delivery_id');
                }

                break;
        }

        return !$this->error;
    }

    protected function validateDelete()
    {
        if (!$this->user->hasPermission('modify', 'settings/delivery')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        /* delivery_id */
        if (!isset($this->request->get['delivery_id'])) {
            $this->error['delivery_id'] = $this->language->get('error_delivery_id');
            $this->error['warning'] = $this->language->get('error_delivery_id');
        }

        return !$this->error;
    }

    protected function validateEditStatus()
    {
        if (!$this->user->hasPermission('modify', 'settings/delivery')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        /* delivery_id */
        if (!isset($this->request->get['delivery_id'])) {
            $this->error['delivery_id'] = $this->language->get('error_delivery_id');
            $this->error['warning'] = $this->language->get('error_delivery_id');
        }

        return !$this->error;
    }

    /**
     * @return array format as
     *
     * [
     *     [
     *         'delivery_id' => 1,
     *         'name' => 'Thanh toán khi nhận hàng (COD)',
     *         'guide' => 'Hướng dẫn Thanh toán khi nhận hàng (COD): khi nhận hàng vui lòng thanh toán toàn bộ cho nhân viên giao hàng',
     *         'status' => 1
     *     ],
     *     ...
     * ]
     *
     */
    protected function getDeliveryMethods()
    {
        /*
         * get existing delivery config. Sample data:
         *
         * [
         *     'id' => 1,
         *     'name' => 'Viettel Post',
         *     'status' => 1,
         *     'fee_regions' => [
         *         [
         *             'id' => '1',
         *             'name' => 'Khu vực Miền Bắc',
         *             'provinces' => [
         *                 'Hà Nội', 'Quảng Ninh', 'Thái Nguyên', 'Hưng Yên', 'Hải Phòng', 'Hà Nam'
         *             ],
         *             'weight_steps' => [
         *                 [
         *                     'type' => 'FROM',
         *                     'from' => 100,
         *                     'to' => 200,
         *                     'price' => 20000
         *                 ],
         *                 [
         *                     'type' => 'ABOVE',
         *                     'from' => 200,
         *                     'to' => '',
         *                     'price' => 50000
         *                 ]
         *             ]
         *         ]
         *     ],
         *     'fee_cod' => [
         *         'fixed' => [
         *             'status' => 1,
         *             'value' => 35000
         *         ],
         *         'dynamic' => [
         *             'status' => 0,
         *             'value' => 10.5
         *         ]
         *     ]
         * ]
         */
        $delivery_methods = $this->model_setting_setting->getSettingValue('delivery_methods');
        $delivery_methods = json_decode($delivery_methods, true);
        if (!is_array($delivery_methods)) {
            $delivery_methods = [];
        }

        return $delivery_methods;
    }

    private function getNextId($delivery_methods)
    {
        $max_current_id = 0;
        if (is_array($delivery_methods)) {
            foreach ($delivery_methods as $delivery_method) {
                if (isset($delivery_method['id'])) {
                    if ((int)$delivery_method['id'] > $max_current_id) {
                        $max_current_id = (int)$delivery_method['id'];
                    }
                }
            }
        }

        return $max_current_id + 1;
    }

    private function mapDelivery()
    {
        $delivery_id = $this->request->post['method-id'];
        $new_delivery_method = array();
        // for mapping default delivery method
        if ($delivery_id == -1) {
            $new_delivery_method['id'] = $delivery_id;
            $new_delivery_method['name'] = $this->request->post['method-name'];
            $new_delivery_method['status'] = '1';
            $new_delivery_method['fee_region'] = $this->request->post['method_fee_default'];

            return $new_delivery_method;
        }

        // else, mapping user defined delivery method
        $new_delivery_method['id'] = $delivery_id;
        $new_delivery_method['name'] = $this->request->post['method-name'];
        $config_for_step = isset($this->request->post['config_for_step']) ? $this->request->post['config_for_step'] : '';
        $new_delivery_method['status'] = isset($this->request->post['method-status']) ? $this->request->post['method-status'] : '0';
        $new_delivery_method['fee_region'] = isset($this->request->post['method_fee_region']) ? $this->request->post['method_fee_region'] : 0;
        // fee_regions
        $fee_regions = array();
        if (isset($this->request->post['region-name'][$delivery_id])) {
            $regionNames = is_array($this->request->post['region-name'][$delivery_id]) ? $this->request->post['region-name'][$delivery_id] : [];
            $provinces = isset($this->request->post['region-provinces'][$delivery_id]) ? $this->request->post['region-provinces'][$delivery_id] : [];
            $config_for_steps = isset($this->request->post['config_for_step'][$delivery_id]) ? $this->request->post['config_for_step'][$delivery_id] : [];
            $weight_step_prices = isset($this->request->post['weight-step-price'][$delivery_id]) ? $this->request->post['weight-step-price'][$delivery_id] : [];
            $weight_step_froms = isset($this->request->post['weight-step-from'][$delivery_id]) ? $this->request->post['weight-step-from'][$delivery_id] : [];
            $weight_step_tos = isset($this->request->post['weight-step-to'][$delivery_id]) ? $this->request->post['weight-step-to'][$delivery_id] : [];

            $price_step_prices = isset($this->request->post['price-step-price'][$delivery_id]) ? $this->request->post['price-step-price'][$delivery_id] : [];
            $price_step_froms = isset($this->request->post['price-step-from'][$delivery_id]) ? $this->request->post['price-step-from'][$delivery_id] : [];
            $price_step_tos = isset($this->request->post['price-step-to'][$delivery_id]) ? $this->request->post['price-step-to'][$delivery_id] : [];

            foreach ($regionNames as $region_id => $regionName) {
                $province = isset($provinces[$region_id]) ? explode(',', $provinces[$region_id]) : [];
                $config_for_step = isset($config_for_steps[$region_id]) ? $config_for_steps[$region_id] : 'weight';
                //weight_steps
                $froms = isset($weight_step_froms[$region_id]) ? $weight_step_froms[$region_id] : [];
                $tos = isset($weight_step_tos[$region_id]) ? $weight_step_tos[$region_id] : [];
                $prices = isset($weight_step_prices[$region_id]) ? $weight_step_prices[$region_id] : [];
                $weight_steps = array();
                foreach ($prices as $key => $value) {
                    $from = isset($froms[$key]) ? trim($froms[$key]) : '';
                    $to = isset($tos[$key]) ? trim($tos[$key]) : '';
                    if ($from == '' && $to == '') {
                        continue;
                    } else if ($from != '' && $to == '') {
                        $type = $this->language->get('delivery_methods_form_fee_region_weight_step_upper_type');
                    } else {
                        $type = $this->language->get('delivery_methods_form_fee_region_weight_step_from_type');
                    }
                    $weight_steps[] = array(
                        'type' => $type,
                        'from' => $from,
                        'to' => $to,
                        'price' => $value
                    );
                }

                $price_froms = isset($price_step_froms[$region_id]) ? $price_step_froms[$region_id] : [];
                $price_tos = isset($price_step_tos[$region_id]) ? $price_step_tos[$region_id] : [];
                $price_prices = isset($price_step_prices[$region_id]) ? $price_step_prices[$region_id] : [];
                $price_price_steps = array();
                foreach ($price_prices as $key => $value) {
                    $from = isset($price_froms[$key]) ? trim($price_froms[$key]) : '';
                    $to = isset($price_tos[$key]) ? trim($price_tos[$key]) : '';
                    if ($from == '' && $to == '') {
                        continue;
                    } else if ($from != '' && $to == '') {
                        $type = $this->language->get('delivery_methods_form_fee_region_weight_step_upper_type');
                    } else {
                        $type = $this->language->get('delivery_methods_form_fee_region_weight_step_from_type');
                    }
                    $price_price_steps[] = array(
                        'type' => $type,
                        'from' => $from,
                        'to' => $to,
                        'price' => $value
                    );
                }

                $fee_regions[] = array(
                    'id' => $region_id,
                    'name' => $regionName,
                    'provinces' => $province,
                    'steps' => $config_for_step,
                    'weight_steps' => $weight_steps,
                    'price_steps' => $price_price_steps
                );
            }
            $new_delivery_method['fee_regions'] = $fee_regions;
        } else {
            $new_delivery_method['fee_regions'] = [];
        }
        // fee_cod
        $payment_type = isset($this->request->post['payment-type'][$delivery_id]) ? $this->request->post['payment-type'][$delivery_id] : 'fixed';
        $payment_value = isset($this->request->post['payment-value'][$delivery_id]) ? $this->request->post['payment-value'][$delivery_id] : '';
        if ($payment_type == 'fixed') {
            $payment_fixed_value = $payment_value;
            $payment_dynamic_value = '';
        } else {
            $payment_fixed_value = '';
            $payment_dynamic_value = $payment_value;
        }
        $fee_cod = array(
            'fixed' => array(
                'status' => $payment_type == 'fixed' ? '1' : '0',
                'value' => $payment_fixed_value
            ),
            'dynamic' => array(
                'status' => $payment_type == 'dynamic' ? '1' : '0',
                'value' => $payment_dynamic_value
            )
        );
        $new_delivery_method['fee_cod'] = $fee_cod;
        return $new_delivery_method;
    }

    private function statusValid($delivery_methods)
    {
        $statusValid = false;
        foreach ($delivery_methods as $method) {
            if (!isset($method['status'])) {
                continue;
            }
            if ($method['status'] == 1) {
                $statusValid = true;
            }
        }

        return $statusValid;
    }
}