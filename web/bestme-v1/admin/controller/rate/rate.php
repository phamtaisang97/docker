<?php

class ControllerRateRate extends Controller
{
    use Device_Util;

    private $error = array();

    /**
     * list rates
     */
    public function index()
    {
        $this->load->language('rate/rate');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('rate/rate');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_rate_rate->editRate($this->request->get['rate_id'], $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('rate/rate', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getList();
    }

    /**
     * add rates
     */
    public function add()
    {
        $this->load->language('rate/rate');

        $this->document->setTitle($this->language->get('heading_title_add'));

        $this->load->model('rate/rate');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $data['customer_name'] = trim(isset($this->request->post['customer_name'])) ? trim($this->request->post['customer_name']) : '';
            $data['sub_info'] = isset($this->request->post['sub_info']) ? $this->request->post['sub_info'] : '';
            $data['content'] = isset($this->request->post['content']) ? $this->request->post['content'] : '';
            $data['status'] = isset($this->request->post['status']) ? $this->request->post['status'] : 0;
            $data['image'] = isset($this->request->post['image']) ? $this->request->post['image'] : '';
            $data['alt'] = isset($this->request->post['alt']) ? $this->request->post['alt'] : '';
            try {
                $this->model_rate_rate->addRate($data);
                $this->session->data['success'] = $this->language->get('text_success_add');
                $this->session->data['success'] = $this->language->get('text_success');
            } catch (Exception $e) {
                $this->error['warning'] = $this->language->get('error_text_check_rate');
            }

            $this->response->redirect($this->url->link('rate/rate', 'user_token=' . $this->session->data['user_token'] . $url = '', true));
        }

        $this->getForm();
    }

    /**
     * edit rates
     */
    public function edit()
    {
        $this->load->language('rate/rate');

        $this->document->setTitle($this->language->get('heading_title_edit'));

        $this->load->model('rate/rate');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $data['customer_name'] = trim(isset($this->request->post['customer_name'])) ? trim($this->request->post['customer_name']) : '';
            $data['sub_info'] = isset($this->request->post['sub_info']) ? $this->request->post['sub_info'] : '';
            $data['content'] = isset($this->request->post['content']) ? $this->request->post['content'] : '';
            $data['status'] = isset($this->request->post['status']) ? $this->request->post['status'] : 0;
            $data['image'] = isset($this->request->post['image']) ? $this->request->post['image'] : '';
            $data['alt'] = isset($this->request->post['alt']) ? $this->request->post['alt'] : '';
            $this->model_rate_rate->editRate($this->request->get['rate_id'], $data);
            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->session->data['success'] = $this->language->get('text_success_edit');
            $this->response->redirect($this->url->link('rate/rate', 'user_token=' . $this->session->data['user_token'] . $url, true));

        }

        $this->getForm();
    }

    /**
     * delete rate
     */
    public function delete()
    {
        $this->load->language('rate/rate');
        $this->load->model('rate/rate');
        $rates = explode(',', $this->request->get['rate_id']);
        if (isset($rates)) {
            foreach ($rates as $rate_id) {
                $this->model_rate_rate->deleteRate($rate_id);
            }
        }
        $this->session->data['success'] = $this->language->get('delete_success');
        $this->response->redirect($this->url->link('rate/rate', 'user_token=' . $this->session->data['user_token'], true));
    }

    /**
     * publish or un-publish rate
     */
    public function activeStatus()
    {
        $this->load->model('rate/rate');
        $rate_id = (int)$this->request->get['rate_id'];
        $status = $this->request->get['status'];
        $this->model_rate_rate->updateStatus($rate_id, $status);
    }

    /**
     * replace escaped url
     *
     * @param $url
     * @return mixed
     */
    public function replace_url($url)
    {
        return str_replace("amp;", "", $url);
    }

    /**
     * publish or un-publish multiple rates
     */
    public function updateStatusPublish($active = '')
    {
        $this->load->language('rate/rate');

        $this->load->model('rate/rate');

        $rates = explode(',', $this->request->get['rate_id']);
        $active = $this->request->get['active'];
        if (isset($rates)) {
            foreach ($rates as $rate_id) {
                $this->model_rate_rate->updateStatus($rate_id, $active);
            }
        }
        $this->session->data['success'] = $this->language->get('change_success');
        $this->response->redirect($this->url->link('rate/rate', 'user_token=' . $this->session->data['user_token'], true));
    }

    /**
     * get list rates with filter
     */
    private function getList()
    {
        /* current page */
        $filter_name = trim($this->getValueFromRequest('get', 'filter_name'));
        $page = $this->getValueFromRequest('get', 'page', 1);
        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        /* breadcrumbs */
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('rate/rate', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        /* message */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        /* filter */
        $filter_rate = null;
        if (isset($this->request->get['filter_rate'])) {
            $filter_rate = (int)($this->request->get['filter_rate']);
            $url .= '&filter_rate=' . $filter_rate;
        }

        // TODO: use array, do not set '' for multiple statuses...
        $filter_status = null;
        if (isset($this->request->get['filter_status'])) {
            $filter_status = ($this->request->get['filter_status']);
            if (count($filter_status) > 1) {
                $filter_status = '';
            } else {
                $filter_status = (int)$filter_status[0];
            }
            $url .= '&filter_status=' . $filter_status;
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $filter_data = array(
            'filter_name' => $filter_name,
            'filter_status' => $filter_status,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        /* selected fields in filter */
        $data['filter_rate'] = $filter_rate;
        $data['filter_status'] = $filter_status;

        $this->load->model('tool/image');
        $results = $this->model_rate_rate->getAllRates($filter_data);
        foreach ($results as $result) {
            if ($result['image']) {
                $image = $result['image'];
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
            }
            $data['rates'][] = array(
                'rate_id' => $result['rate_id'],
                'customer_name' => $result['customer_name'],
                'sub_info' => $result['sub_info'],
                'content' => $result['content'],
                'image' => $image,
                'dateModified' => $result['date_modified'],
                'activeStatus' => $result['status'],
                'edit' => $this->url->link('rate/rate/edit', 'user_token=' . $this->session->data['user_token'] . '&rate_id=' . $result['rate_id'] . $url, true),
            );
        }

        $data['user_token'] = $this->session->data['user_token'];
        $data['delete'] = $this->url->link('rate/rate/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $rate_total = $this->model_rate_rate->getTotalRates($filter_data);

        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $rate_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('rate/rate', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        // Add jump to page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);

        //get all tag
//        $data['tag_all'] = $this->model_custom_tag->getAllTagValueProduct();
        $data['action_add_tag'] = $this->url->link('rate/rate/addTags', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_remove_tag'] = $this->url->link('rate/rate/removeTags', 'user_token=' . $this->session->data['user_token'], true);

        $data['results'] = sprintf($this->language->get('text_pagination'), ($rate_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($rate_total - $this->config->get('config_limit_admin'))) ? $rate_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $rate_total, ceil($rate_total / $this->config->get('config_limit_admin')));
        $data['url_filter'] = $this->url->link('rate/rate', 'user_token=' . $this->session->data['user_token'] . $url, true);
        /* href */
        $data['href_list_rate_category'] = $this->url->link('rate/category', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['href_add'] = $this->url->link('rate/rate/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['href_edit'] = $this->url->link('rate/rate', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['href_edit_quantity'] = $this->url->link('rate/rate/editQuantity', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['href_edit_sale_on_out_of_stock'] = $this->url->link('rate/rate/editSaleOnOutOfStock', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['avtivestatus'] = $this->replace_url($this->url->link('rate/rate/activeStatus', ('user_token=' . $this->session->data['user_token'] . $url), true));
        $data['publish'] = $this->url->link('rate/rate/updateStatusPublish&active=1', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['unPublish'] = $this->url->link('rate/rate/updateStatusPublish&active=0', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['loadOptionOne'] = $this->replace_url($this->url->link('rate/rate/loadoptionone', 'user_token=' . $this->session->data['user_token'] . $url, true));
        $data['delete'] = $this->url->link('rate/rate/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('rate/rate_list_append', $data));
        } else {
            $this->response->setOutput($this->load->view('rate/rate_list', $data));
        }
    }

    /**
     * get rate form for add/edit
     */
    private function getForm()
    {
        $data['text_form'] = !isset($this->request->get['rate_id']) ? $this->language->get('heading_title_add') : $this->language->get('heading_title_edit');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['error_text_check'])) {
            $data['error_text_check'] = $this->error['error_text_check'];
        } else {
            $data['error_text_check'] = '';
        }

        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = array();
        }

        if (isset($this->error['keyword'])) {
            $data['error_keyword'] = $this->error['keyword'];
        } else {
            $data['error_keyword'] = '';
        }

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('rate/rate', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        if (isset($this->request->get['rate_id'])) {
            $data['breadcrumbs'][] = array(
                'text' => $data['text_form'],
                'href' => '',
            );
        } else {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_add'),
                'href' => ''
            );
        }

        if (!isset($this->request->get['rate_id'])) {
            $data['action'] = $this->url->link('rate/rate/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        } else {
            $data['action'] = $this->url->link('rate/rate/edit', 'user_token=' . $this->session->data['user_token'] . '&rate_id=' . $this->request->get['rate_id'] . $url, true);
            $data['rate_id'] = $this->request->get['rate_id'];
        }

        $data['cancel'] = $this->url->link('rate/rate', 'user_token=' . $this->session->data['user_token'] . $url, true);

        if (isset($this->request->get['rate_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $this->load->model('tool/image');
            $rate_info = $this->model_rate_rate->getRate($this->request->get['rate_id']);
        }

        if (isset($this->request->post['customer_name'])) {
            $data['customer_name'] = $this->request->post['customer_name'];
        } elseif (!empty($rate_info)) {
            $data['customer_name'] = $rate_info['customer_name'];
        } else {
            $data['customer_name'] = '';
        }

        if (isset($this->request->post['content'])) {
            $data['content'] = $this->request->post['content'];
        } elseif (!empty($rate_info)) {
            $data['content'] = $rate_info['content'];
        } else {
            $data['content'] = '';
        }

        if (isset($this->request->post['sub_info'])) {
            $data['sub_info'] = $this->request->post['sub_info'];
        } elseif (!empty($rate_info)) {
            $data['sub_info'] = $rate_info['sub_info'];
        } else {
            $data['sub_info'] = '';
        }

        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($rate_info)) {
            $data['image'] = $rate_info['image'];
        } else {
            $data['image'] = '';
        }

        if (isset($this->request->post['alt'])) {
            $data['alt'] = $this->request->post['alt'];
        } elseif (!empty($rate_info)) {
            $data['alt'] = $rate_info['alt'];
        } else {
            $data['alt'] = '';
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($rate_info)) {
            $data['status'] = $rate_info['status'];
        } else {
            $data['status'] = true;
        }

        $data['user_token'] = $this->session->data['user_token'];

        // Image
        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($rate_info)) {
            $data['image'] = $rate_info['image'];
        } else {
            $data['image'] = '';
        }

        $this->load->model('tool/image');

        if (isset($this->request->post['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
        } elseif (!empty($product_info)) {
            $data['thumb'] = $this->model_tool_image->resize($product_info['image'], 100, 100);
        } else {
            $data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        }

        $data['is_on_mobile'] = $this->isMobile();

        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

        $data['ckeditor_lang'] = $this->config->get('config_language') == 'vi-vn' ? 'vi' : 'en';
        $data['upload_image'] = str_replace('&amp;', '&', $this->url->link('catalog/product/upload', 'user_token=' . $this->session->data['user_token'] . $url, true));
        $data['filemanager'] = str_replace('&amp;', '&', $this->url->link('common/filemanager', 'user_token=' . $this->session->data['user_token'] . '&ckeditor=1', true));

        $data['server_name'] = $_SERVER['SERVER_NAME'];
        $this->load->model('design/layout');
        $data['layouts'] = $this->model_design_layout->getLayouts();

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('rate/rate_form', $data));
    }

    /**
     * validate form add/edit
     *
     * @param string $action
     * @return bool
     */
    private function validateForm($action = 'edit')
    {
        if (!$this->user->hasPermission('modify', 'rate/rate')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ($action === 'editQuantity') {
            //
        }

        return !$this->error;
    }

}
