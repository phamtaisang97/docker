<?php

class ControllerDemoDataDemoData extends Controller
{
    use AUTO_CREATE_RECEIPT_VOUCHER_UTIL_TRAIT;
    use Setting_Util_Trait;

    const ALPHABET = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

    const ORDER_STATUS_APPLY = 'apply';
    const ORDER_STATUS_DRAFT = 'draft';
    const NUMBER_OF_MANUFACTURERS = 5;
    const NUMBER_OF_IN_TRANSACTION_MANUFACTURERS = 3;
    const NUMBER_OF_RANDOM_RECEIPT_VOUCHERS = 5;
    const NUMBER_OF_RANDOM_PAYMENT_VOUCHERS = 5;

    const NUMBER_OF_STORES = 4;
    const NUMBER_OF_RANDOM_PRODUCT_TO_STORES = 4;

    const GHN_SHIPPING_METHOD = 'Ghn';
    const VTPOST_SHIPPING_METHOD = 'viettel_post';
    const GHTK_SHIPPING_METHOD = 'trans_ons_ghtk';

    const STORE_RECEIPT_STATUS_APPLY = 'apply';
    const STORE_RECEIPT_STATUS_DRAFT = 'draft';

    const STORE_TRANSFER_RECEIPT_DRAFT = 0;
    const STORE_TRANSFER_RECEIPT_DELIVERED = 1;
    const STORE_TRANSFER_RECEIPT_RECEIVED = 2;
    const STORE_TRANSFER_RECEIPT_CANCEL = 9;
    const STORE_DEFAULT = 0;

    /**
     * Load demo data
     */
    public function index()
    {
        try {
            if (!$this->isPaidShop()) {
                $this->load->model('sale/order');
                $this->load->model('customer/customer');
                $this->load->model('catalog/product');
                $this->load->model('catalog/manufacturer');
                $this->load->model('demo_data/demo_data');

                $this->model_demo_data_demo_data->removeAllDataDemo();

                /* product, category, collection */
                $this->loadThemeDemoData();

                /* create Customer and customer group */
                $this->createCustomerDemo();

                /* get customer and product to generate Order data */
                $customers = $this->model_demo_data_demo_data->getCustomers();
                $products = $this->model_demo_data_demo_data->getProducts(['start' => 1, 'limit' => 20]);

                /* create Bank Transfer payment method */
                $this->model_demo_data_demo_data->createPaymentMethod();

                /* Order */
                if($products && count($products) > 0) {
                    $this->createOrderDemo($products, $customers);
                }

                /* Return Receipt */
                $this->createReturnReceiptDemo();

                /* manufacturer */
                $this->createDemoManufacturer();

                /* cash flow */
                $this->createDemoReceiptVoucher();
                $this->createDemoPaymentVoucher();

                // Create store
                $this->createDemoStore();
                // Store_receipt
                $this->createStoreReceipt($products);
                // Store_transfer_receipt
                $this->createStoreTransferReceipt($products);

                // Store_take_receipt
                $this->createStoreTakeReceipt($products);

                // Update product to store
                $this->removeNotExistsProductToStore();
                $this->updateRandomProductToStore();
            }
        } catch (Exception $e) {
            // return error message
        }

        // redirect back to current page
        $this->redirectToPreviousPage();
    }

    /**
     * Delete all data demo
     */
    public function deleteDataDemo()
    {
        try {
            if (!$this->isPaidShop()) {
                $this->load->model('demo_data/demo_data');
                // delete all demo data except product, category, collection
                $this->model_demo_data_demo_data->removeAllDataDemo();
                // delete theme demo data
                $this->deleteThemeDemoData();
            }
        } catch (Exception $e) {
            // return error message
        }

        // redirect back to current page
        $this->redirectToPreviousPage(true);
    }

    /**
     * redirect to previous page
     * @param false $is_delete
     */
    private function redirectToPreviousPage($is_delete = false) {
        /* build query string from current get params */
        // default: dashboard page
        $route = isset($this->request->get['current_route']) && $this->request->get['current_route'] ? rawurldecode($this->request->get['current_route']) : 'common/dashboard';

        $query = 'user_token=' . $this->session->data['user_token'];
        if (isset($this->request->get['current_query_param']) && $this->request->get['current_query_param']) {
            $current_query_param = str_replace('&amp;', '&', $this->request->get['current_query_param']);
            $query .= rawurldecode($current_query_param);
        }

        if ($is_delete) {
            $query .= '&is_delete_demo_data=1';
        }

        $this->response->redirect($this->url->link($route, $query, true));
    }

    /**
     * Create customer group
     */
    private function createCustomerGroupDemo()
    {
        $this->load->model('customer/customer_group');
        for ($i = 1; $i <= 5; $i++) {
            $this->model_customer_customer_group->addCustomerGroup([
                'name_group' => 'Nhóm khách hàng ' . self::ALPHABET[$i],
                'description' => 'Mô tả cho nhóm khách hàng ' . self::ALPHABET[$i],
            ]);
        }
    }

    /**
     * Create customer and add customer to group
     */
    private function createCustomerDemo()
    {
        $this->load->model('customer/customer');
        $this->load->model('localisation/vietnam_administrative');
        $this->load->model('customer/customer_group');

        $this->createCustomerGroupDemo();
        $customers_groups = $this->model_customer_customer_group->getCustomerGroups([]);

        $group_rand      = array_rand($customers_groups, 4);
        $group_to_insert = $group_rand;
        for ($i = 0; $i < 4; $i++) {
            $group_to_insert[] = array_rand($group_rand, 1);
        }

        $group_to_insert[] = null;
        $group_to_insert[] = null;

        for ($i = 0; $i < 10; $i++) {
            $group_key     = $group_to_insert[$i];
            $provinces     = $this->model_localisation_vietnam_administrative->getProvinces();
            $province_code = $provinces[array_rand($provinces, 1)]['code'];
            $district      = $this->model_localisation_vietnam_administrative->getDistrictsByProvinceCode($province_code);
            $district_code = $district[array_rand($district, 1)]['code'];
            $ward          = $this->model_localisation_vietnam_administrative->getWardsByDistrictCode($district_code);
            $ward_code     = $ward[array_rand($ward, 1)]['code'];

            $customer_data = [
                'full_name' => 'Khach Hang ' . self::ALPHABET[$i],
                'telephone' => '09669' . rand(10000, 99999),
                'email' => 'khachang' . strtolower(self::ALPHABET[$i]) . '@gmail.com',
                'customer_group_id' => $group_key !== null ? $customers_groups[$group_key]['customer_group_id'] : null,
                'address_id' => '',
                'birthday' => rand(1970, 2000) . '-' . rand(01, 12) . '-' . rand(01, 28),
                'full_name_for_default_address' => '',
                'telephone_for_default_address' => '',
                'city' => $province_code,
                'district' => $district_code,
                'wards' => $ward_code,
                'address' => 'Số nhà ' . rand(10, 200),
                'website' => 'bestme.asia',
                'tax_code' => '',
                'staff_in_charge' => '2',
                'sex' => rand(0, 1),
                'note' => '',
            ];
            $this->model_customer_customer->addCustomer($customer_data);
        }
    }


    /*
     * Create Order
     */
    private function createOrderDemo($products, $customers)
    {
        $pickup_address = $this->generateShippingAddress();
        // order draft
        $orders_requirement = array_fill(0, 5, ['status' => ModelSaleOrder::ORDER_STATUS_ID_DRAFT, 'payment_status' => 0]);
        // order in processing
        $orders_requirement = array_merge($orders_requirement, array_fill(0, 3, ['status' => ModelSaleOrder::ORDER_STATUS_ID_PROCESSING, 'payment_status' => 0]));
        $orders_requirement = array_merge($orders_requirement, array_fill(0, 2, ['status' => ModelSaleOrder::ORDER_STATUS_ID_PROCESSING, 'payment_status' => 1]));
        // order is delivering
        $orders_requirement   = array_merge($orders_requirement, array_fill(0, 2, ['status' => ModelSaleOrder::ORDER_STATUS_ID_DELIVERING, 'payment_status' => 0]));
        $orders_requirement[] = ['status' => ModelSaleOrder::ORDER_STATUS_ID_DELIVERING, 'shipping_method' => self::GHN_SHIPPING_METHOD, 'payment_status' => 1];
        $orders_requirement[] = ['status' => ModelSaleOrder::ORDER_STATUS_ID_DELIVERING, 'shipping_method' => self::VTPOST_SHIPPING_METHOD, 'payment_status' => 1];
        $orders_requirement[] = ['status' => ModelSaleOrder::ORDER_STATUS_ID_DELIVERING, 'shipping_method' => self::GHTK_SHIPPING_METHOD, 'payment_status' => 1];
        // order is completed
        $orders_requirement = array_merge($orders_requirement, array_fill(0, 5, ['status' => ModelSaleOrder::ORDER_STATUS_ID_COMPLETED, 'payment_status' => 1, 'payment_method' => ModelDemoDataDemoData::VNPAY_METHOD]));
        $orders_requirement = array_merge($orders_requirement, array_fill(0, 3, ['status' => ModelSaleOrder::ORDER_STATUS_ID_COMPLETED, 'payment_status' => 1, 'payment_method' => ModelDemoDataDemoData::BANK_TRANSFER_METHOD]));
        $orders_requirement = array_merge($orders_requirement, array_fill(0, 2, ['status' => ModelSaleOrder::ORDER_STATUS_ID_COMPLETED, 'payment_status' => 1, 'payment_method' => ModelDemoDataDemoData::COD_METHOD]));
        // order is canceled
        $orders_requirement = array_merge($orders_requirement, array_fill(0, 5, ['status' => ModelSaleOrder::ORDER_STATUS_ID_CANCELLED]));

        foreach ($orders_requirement as $key => &$order_requirement) {
            $order_data = $this->generateOrderData($products, $customers, $order_requirement, $key);
            $order_id = $this->model_sale_order->addOrder($order_data);
            $order_requirement['order_id'] = $order_id;

            if ($order_requirement['status'] != ModelSaleOrder::ORDER_STATUS_ID_PROCESSING && $order_requirement['status'] != ModelSaleOrder::ORDER_STATUS_ID_DRAFT) {
                $this->model_sale_order->updateStatus([
                    'order_id' => $order_id,
                    'order_status_id' => $order_requirement['status'],
                    'order_source' => 'Quản trị',
                ], ModelSaleOrder::ORDER_STATUS_ID_PROCESSING);
            }

            if ($order_requirement['status'] == ModelSaleOrder::ORDER_STATUS_ID_DELIVERING) {
                if(isset($order_requirement['shipping_method']) && $order_requirement['shipping_method']) {
                    $this->model_sale_order->updateTransportDeliveryApi($order_id, $this->generateRandomString(9), $order_requirement['shipping_method'], 'Chuẩn', '50000', '20', 'ready_to_pick', $pickup_address);
                } else {
                    $this->model_sale_order->updateStatus([
                        'order_id' => $order_id,
                        'order_status_id' => $order_requirement['status'],
                        'order_source' => 'Quản trị',
                    ], ModelSaleOrder::ORDER_STATUS_ID_DELIVERING);
                }
            }

            if($order_requirement['status'] == ModelSaleOrder::ORDER_STATUS_ID_COMPLETED) {
                $this->addReceiptVoucherLinkedOrder($order_id, $order_data);
            }
        }
        $this->model_demo_data_demo_data->updateOrdersAdded($orders_requirement);
    }

    private function generateShippingAddress()
    {
        $provinces = $this->model_localisation_vietnam_administrative->getProvinces();
        $province = $provinces[array_rand($provinces, 1)];
        $districts = $this->model_localisation_vietnam_administrative->getDistrictsByProvinceCode($province['code']);
        $district = $districts[array_rand($districts, 1)];
        $ward = $this->model_localisation_vietnam_administrative->getWardsByDistrictCode($district['code']);
        $ward = $ward[array_rand($ward, 1)];

        return ($ward['name'] . ', ' . $district['name'] . ', ' . $province['name']);
    }

    private function addReceiptVoucherLinkedOrder($order_id, $data_order)
    {
        if (isset($data_order['customer_full_name'])) {
            $name = extract_name($data_order['customer_full_name']);
            $data_order['customer_firstname'] = $name[0];
            $data_order['customer_lastname'] = $name[1];
        } else {
            $data_order['customer_firstname'] = "";
            $data_order['customer_lastname'] = "";
        }

        $customer_info = json_encode(['id' => null, 'name' => $data_order['customer_lastname'] . ' ' . $data_order['customer_firstname'] ]);
        $data_order['customer_info'] = $customer_info;

        $this->autoUpdateFromOrder($order_id, $data_order);
    }

    /**
     * @param int $length
     * @return string
     * Generate random string (for payment code, shipping code)
     */
    private function generateRandomString($length = 10)
    {
        $characters       = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString     = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    private function createReturnReceiptDemo()
    {
        $this->load->model('sale/order');
        $this->load->model('demo_data/demo_data');
        $this->load->model('sale/return_receipt');

        $MAX_PART_REFUND = 2;
        $order_ids = $this->model_demo_data_demo_data->getOrderCanReturn();
        foreach ($order_ids as $key => $order_id) {
            if (!isset($order_id['order_id']) || !$order_id['order_id']){
                continue;
            }
            $order_id = $order_id['order_id'];

            $count = $key + 1;
            $return_data = [];
            $order_products = $this->model_sale_order->getOrderProducts($order_id);

            if ($count <= $MAX_PART_REFUND) {
                $order_products = array_map(function ($item){
                    $item['quantity'] = rand(1, (int)$item['quantity']);
                    return $item;
                }, $order_products);
                if (count($order_products) > 1){
                    $max_rand = count($order_products) - 1 > 0 ? count($order_products) - 1 : 1;
                    $order_products = array_slice($order_products, rand(1, $max_rand));
                }
            }

            $return_data['product_id'] = array_map(function ($order_product) {
                return $order_product['product_id'];
            }, $order_products);

            $return_data['product_version_id'] = array_map(function ($order_product) {
                return $order_product['product_version_id'];
            }, $order_products);

            $return_data['product_return_quantity'] = array_map(function ($order_product) {
                return $order_product['quantity'];
            }, $order_products);

            $return_data['return_prices'] = array_map(function ($order_product) {
                return (float)$order_product['price'] - (float)$order_product['discount'];
            }, $order_products);

            $return_data['return_fee'] = '0';
            $return_data['note'] = 'demo data';
            $return_data['submit_action'] = 'apply';

            $this->model_sale_return_receipt->addReturnReceipt($order_id, $return_data);
        }
    }

    /*
     * Generate random order data to create order
     */
    private function generateOrderData($products, $customers, $order_requirement, $order_number)
    {
        // random product
        $count_product   = rand(1, count($products) > 5 ? 5 : count($products));
        $product_price    = [];
        $quantities       = [];
        $product_ids      = [];
        $product_discount = [];
        $weight           = 0;
        $total_pay        = 0;
        $product_index    = $count_product == 1 ? [array_rand($products, $count_product)] : array_rand($products, $count_product);
        $customer         = $customers[array_rand($customers, 1)];
        //end random_product

        foreach ($product_index as $index) {
            $price = $products[$index]['price'] != 0 ? $products[$index]['price'] : $products[$index]['compare_price'];
            $quantity       = rand(1, 3);           // random quantity

            $product_price[]    = $price;
            $quantities[]       = $quantity;
            if($products[$index]['multi_versions']  == 1) {
                $product_ids[]      = $products[$index]['product_id'] .'-'. $products[$index]['product_version_id'];
            } else {
                $product_ids[]      = $products[$index]['product_id'];
            }
            $product_discount[] = '';
            $weight             += (int)$products[$index]['weight'];
            $total_pay          += (int)$price * (int)$quantity;
        }
        $submit_action = $order_requirement['status'] === ModelSaleOrder::ORDER_STATUS_ID_DRAFT ? self::ORDER_STATUS_DRAFT : self::ORDER_STATUS_APPLY;

        $payment_methods = [ModelDemoDataDemoData::COD_METHOD, ModelDemoDataDemoData::BANK_TRANSFER_METHOD, ModelDemoDataDemoData::VNPAY_METHOD];
        $payment_status = isset($order_requirement['payment_status']) ? $order_requirement['payment_status'] : rand(0, 1);
        if(isset($order_requirement['payment_method'])) {
            $payment_method_to_create = $order_requirement['payment_method'];
            $payment_status = 1;
        } elseif ($payment_status == 1) {
            $payment_method_to_create = $payment_methods[array_rand($payment_methods, 1)];
        } else {
            $payment_method_to_create = ModelDemoDataDemoData::COD_METHOD;
        }

        return [
            'submit_action' => $submit_action,
            'selected_discount' => '&quot;{\&quot;selected_discounts\&quot;:[{\&quot;product_id\&quot;:\&quot;order\&quot;,\&quot;discounts\&quot;:\&quot;\&quot;}]}&quot;',
            'products' => $product_ids,
            'pids' => $product_ids,
            'prices' => $product_price,
            'quantities' => $quantities,
            'total_weight' => $weight,
            'shipping_fee' => '0',
            'shipping_method' => '',
            'total_pay' => $total_pay,
            'order_source' => 'shop',
            'product-discount' => $product_discount,
            'payment_method' => $payment_method_to_create,
            'old_order_status_id' => '',
            'payment_status' => $payment_status,
            'user_id' => '1',
            'customer' => $customer['customer_id'],
            'customer_group_id' => '',
            'shipping_address_id' => '13',
            'shipping_fullname' => $customer['full_name'],
            'shipping_phone' => $customer['phone'],
            'shipping_address' => $customer['address'],
            'shipping_wards' => $customer['wards'],
            'shipping_district' => $customer['district'],
            'shipping_province' => $customer['city'],
            'shipping_bill' => '',
            'note' => '',
            'delivery' => '-3',
            'customer_full_name' => $customer['full_name'],
            'customer_phone' => $customer['telephone'],
            'customer_email' => $customer['email'],
            'customer_province' => '',
            'customer_address' => '',
            'delivery_fullname' => $customer['full_name'],
            'delivery_phone' => $customer['phone'],
            'delivery_province' => $customer['city'],
            'delivery_district' => $customer['district'],
            'delivery_wards' => $customer['city'],
            'delivery_address' => $customer['address'],
        ];
    }

    /**
     * create NUMBER_OF_MANUFACTURERS demo manufacturers
     * includes NUMBER_OF_IN_TRANSACTION_MANUFACTURERS manufacturers
     */
    private function createDemoManufacturer()
    {
        $this->load->model('catalog/manufacturer');

        // default value
        $data = [
            'name' => 'Default manufacturer name',
            'telephone' => 'Default manufacturer telephone',
            'email' => null,
            'tax_code' => null,
            'address' => null,
            'province' => null
        ];

        $in_transaction_manufacturers = 0;
        $manufacturer_transaction_statuses = [
            ModelCatalogManufacturer::TYPE_IN_TRANSACTION,
            ModelCatalogManufacturer::TYPE_STOP_TRANSACTION
        ];
        $max_stop_transaction_manufacturers = self::NUMBER_OF_MANUFACTURERS - self::NUMBER_OF_IN_TRANSACTION_MANUFACTURERS;

        for ($i = 0; $i < self::NUMBER_OF_MANUFACTURERS; $i++) {
            // status
            $stop_transaction_manufacturers = $i - $in_transaction_manufacturers;
            if ($in_transaction_manufacturers >= self::NUMBER_OF_IN_TRANSACTION_MANUFACTURERS) {
                $data['status'] = ModelCatalogManufacturer::TYPE_STOP_TRANSACTION;
            } elseif ($stop_transaction_manufacturers >= $max_stop_transaction_manufacturers) {
                $data['status'] = ModelCatalogManufacturer::TYPE_IN_TRANSACTION;
            } else {
                $data['status'] = $manufacturer_transaction_statuses[array_rand($manufacturer_transaction_statuses)];
            }

            if ($data['status'] == ModelCatalogManufacturer::TYPE_IN_TRANSACTION) {
                ++$in_transaction_manufacturers;
            }

            // other information
            $data['name'] = $this->getRandomText();
            $data['telephone'] = rand(0, 1e10);

            // create manufacturer
            $this->model_catalog_manufacturer->addManufacturer($data);
        }
    }

    /**
     * use loadThemeDemoData from setting/setting controller
     */
    private function loadThemeDemoData()
    {
        // delete theme demo data first
        $this->deleteThemeDemoData();

        $current_theme = $this->config->get('config_theme');
        $this->load->controller('setting/setting/loadThemeDemoData', $current_theme);
    }

    /**
     * delete product demo data
     * use deleteThemeDemoData from setting/setting controller
     */
    private function deleteThemeDemoData()
    {
        $current_theme = $this->config->get('config_theme');
        $this->load->controller('setting/setting/deleteThemeDemoData', $current_theme);
    }

    /**
     * create NUMBER_OF_RANDOM_RECEIPT_VOUCHERS receipt vouchers
     * @return bool
     */
    private function createDemoReceiptVoucher() {
        $this->load->model('cash_flow/receipt_voucher');
        $receipt_voucher_types = $this->model_cash_flow_receipt_voucher->getReceiptVoucherTypes(false);
        if (empty($receipt_voucher_types)) {
            return false;
        }

        $this->load->model('cash_flow/cash_flow');
        $this->load->model('setting/store');

        $object_ids = [
            ModelCashFlowReceiptVoucher::OBJECT_CUSTOMER => null,
            ModelCashFlowReceiptVoucher::OBJECT_STAFF => null,
            ModelCashFlowReceiptVoucher::OBJECT_MANUFACTURER => null,
            ModelCashFlowReceiptVoucher::OBJECT_SHIPPING_PARTNER => null,
            ModelCashFlowReceiptVoucher::OBJECT_OTHER => null
        ];
        for ($i = 0; $i < self::NUMBER_OF_RANDOM_RECEIPT_VOUCHERS; $i++) {
            $object_id = array_rand($object_ids);
            $object_info = $this->createObjectInfoFromObjectId($object_id);
            $data = [
                'object_id' => $object_id,
                'object_info' => $object_info,
                'in_business_report' => rand(0, 1),
                'amount' => rand(0, 1e6),
                'receipt_type_id' => $this->getRandomArrayElement($receipt_voucher_types)['receipt_voucher_type_id'],
                'store_id' => $this->model_setting_store->getRandomStoreId(),
                'method_id' => $this->model_cash_flow_cash_flow->getRandomCashFlowMethodId(),
                'receipt_type_other' => $this->getRandomText(),
                'note' => $this->getRandomText()
            ];

            $this->model_cash_flow_receipt_voucher->addReceiptVoucher($data);
        }

        return true;
    }

    /**
     * create NUMBER_OF_RANDOM_PAYMENT_VOUCHERS receipt vouchers
     * @return bool
     */
    private function createDemoPaymentVoucher() {
        $this->load->model('cash_flow/payment_voucher');
        $payment_voucher_types = $this->model_cash_flow_payment_voucher->getPaymentVoucherTypes(false);
        if (empty($payment_voucher_types)) {
            return false;
        }

        $this->load->model('cash_flow/cash_flow');
        $this->load->model('setting/store');

        $object_ids = [
            ModelCashFlowPaymentVoucher::OBJECT_CUSTOMER => null,
            ModelCashFlowPaymentVoucher::OBJECT_STAFF => null,
            ModelCashFlowPaymentVoucher::OBJECT_MANUFACTURER => null,
            ModelCashFlowPaymentVoucher::OBJECT_SHIPPING_PARTNER => null,
            ModelCashFlowPaymentVoucher::OBJECT_OTHER => null
        ];
        for ($i = 0; $i < self::NUMBER_OF_RANDOM_PAYMENT_VOUCHERS; $i++) {
            $object_id = array_rand($object_ids);
            $object_info = $this->createObjectInfoFromObjectId($object_id, 'payment');
            $data = [
                'object_id' => $object_id,
                'object_info' => $object_info,
                'in_business_report' => rand(0, 1),
                'amount' => rand(0, 1e6),
                'payment_type_id' => $this->getRandomArrayElement($payment_voucher_types)['payment_voucher_type_id'],
                'store_id' => $this->model_setting_store->getRandomStoreId(),
                'method_id' => $this->model_cash_flow_cash_flow->getRandomCashFlowMethodId(),
                'payment_type_other' => $this->getRandomText(),
                'note' => $this->getRandomText()
            ];

            $this->model_cash_flow_payment_voucher->addPaymentVoucher($data);
        }

        return true;
    }

    /**
     * create object info base on object id
     * @param $object_id
     * @param string $type
     * @return array|null[]|string[]
     */
    private function createObjectInfoFromObjectId($object_id, $type = 'receipt') {
        $model = 'receipt' == $type ? 'ModelCashFlowReceiptVoucher' : 'ModelCashFlowPaymentVoucher';
        switch ($object_id) {
            case $model::OBJECT_CUSTOMER:
                $this->load->model('customer/customer');
                return ['customer' => $this->model_customer_customer->getRandomCustomerId()];
            case $model::OBJECT_STAFF:
                $this->load->model('user/user');
                return ['staff' => $this->model_user_user->getRandomUserId()];
            case $model::OBJECT_MANUFACTURER:
                $this->load->model('catalog/manufacturer');
                return ['manufacturer' => $this->model_catalog_manufacturer->getRandomManufacturerId()];
            case $model::OBJECT_SHIPPING_PARTNER:
                return ['shipping-partner' => $this->getRandomArrayElement($model::SHIPPING_PARTNERS)];
            case $model::OBJECT_OTHER:
                return ['other' => $this->getRandomText()];
            default:
                return [];
        }
    }

    /**
     * get a random element from given array
     * return null if array is empty
     * @param $array
     * @return null|mixed
     */
    private function getRandomArrayElement($array) {
        return empty($array) ? null : $array[array_rand($array)];
    }

    /**
     * get a random string
     * @return string
     */
    private function getRandomText() {
        return self::ALPHABET[array_rand(self::ALPHABET)];
    }

    /**
     * create NUMBER_OF_STORES demo stores
     */
    private function createDemoStore()
    {
        $this->load->model('setting/store');

        for ($i = 0; $i < self::NUMBER_OF_STORES; $i++) {
            $data = array(
                'config_name' => 'Kho ' . self::ALPHABET[$i],
                'config_url' => '',
                'config_ssl' => ''
            );
            $this->model_setting_store->addStore($data);
        }
    }

    private function createStoreReceipt($products)
    {
        $this->load->model('catalog/store_receipt');
        $this->load->model('cash_flow/payment_voucher');

        $store_receipt_requirements = array_fill(0, 5, ['submit_type' => "apply"]);
        $store_receipt_requirements = array_merge($store_receipt_requirements, array_fill(0, 3, ['submit_type' => "draft"]));
        $store_receipt_requirements = array_merge($store_receipt_requirements, array_fill(0, 2, ['submit_type' => "apply"]));

        foreach ($store_receipt_requirements as $store_receipt_requirement) {
            $store_receipt_id = $this->model_catalog_store_receipt->addStoreReceipt($this->generateStoreReceiptData($products, $store_receipt_requirement));

            // Auto add payment_voucher
            $this->model_cash_flow_payment_voucher->autoCreateOrUpdatePaymentVoucher($store_receipt_id, $this->generateStoreReceiptData($products, $store_receipt_requirement));
        }
    }

    private function generateStoreReceiptData($products, $store_receipt_requirement)
    {
        $this->load->model('setting/store');
        $this->load->model('catalog/manufacturer');
        $this->load->model('catalog/product');

        $receipt_store = $this->model_setting_store->getRandomStoreId();
        $receipt_manufacturer = $this->model_catalog_manufacturer->getRandomManufacturerId();

        // random product
        $count_product    = rand(1, 5);
        $product_version_id  = [];
        $product_quantity    = [];
        $product_cost_price  = [];
        $product_other_fee   = [];
        $product_discount    = [];
        $into_money    = [];
        $product_index    = $count_product == 1 ? [array_rand($products, $count_product)] : array_rand($products, $count_product);

        foreach ($product_index as $k => $index) {
            if (!isset($products[$index]) || empty($products[$index])) {
                continue;
            }

            $product_version_id[]       = $products[$index]['product_id'];
            $product_quantity[]         = rand(1, 3);        // random quantity
            $product_cost_price[]       = $this->model_catalog_product->getCostPriceForProductByStore($products[$index]['default_store_id'], $products[$index]['product_id']);
            $product_other_fee[]        = "";
            $product_discount[]         = "";
            $into_money[]               = (float)($product_cost_price[$k]) * $product_quantity[$k];
        }

        $receipt_total = array_sum($into_money);
        $receipt_discount = 0;
        $receipt_fee = 0;
        $total_to_pay = $receipt_total - $receipt_discount + $receipt_fee;
        $total_paid = 0;
        $receipt_owed = $total_to_pay - $total_paid;
        $submit_type = (isset($store_receipt_requirement['submit_type']) && $store_receipt_requirement['submit_type'] == self::STORE_RECEIPT_STATUS_APPLY) ? self::STORE_RECEIPT_STATUS_APPLY : self::STORE_RECEIPT_STATUS_DRAFT;

        return [
            "fee_config" => "",
            "type_criteria_allocation" => "",
            "receipt_store" => $receipt_store,
            "receipt_manufacturer" => $receipt_manufacturer,
            "product_version_id" => $product_version_id,
            "product_quantity" => $product_quantity,
            "product_cost_price" => $product_cost_price,
            "product_other_fee" => $product_other_fee,
            "product_discount" => $product_discount,
            "into_money" => $into_money,
            "receipt_total" => $receipt_total,
            "receipt_discount" => $receipt_discount,
            "receipt_fee" => $receipt_fee,
            "total_to_pay" => $total_to_pay,
            "total_paid" => $total_paid,
            "receipt_owed" => $receipt_owed,
            "note" => "",
            "submit_type" => $submit_type,
        ];
    }

    private function createStoreTransferReceipt($products)
    {
        $this->load->model('catalog/store_transfer_receipt');

        $store_transfer_receipt_requirements = array_fill(0, 2, ['status' => self::STORE_TRANSFER_RECEIPT_DELIVERED]);
        $store_transfer_receipt_requirements = array_merge($store_transfer_receipt_requirements, array_fill(0, 2, ['status' => self::STORE_TRANSFER_RECEIPT_RECEIVED]));
        $store_transfer_receipt_requirements = array_merge($store_transfer_receipt_requirements, array_fill(0, 1, ['status' => self::STORE_TRANSFER_RECEIPT_CANCEL]));

        foreach ($store_transfer_receipt_requirements as $store_transfer_receipt_requirement) {
            $this->model_catalog_store_transfer_receipt->addStoreTransferReceipt($this->generateStoreTransferReceiptData($products, $store_transfer_receipt_requirement));
        }
    }

    private function generateStoreTransferReceiptData($products, $store_transfer_receipt_requirement)
    {
        $this->load->model('setting/store');
        $this->load->model('catalog/product');

        $export_store = self::STORE_DEFAULT;
        $import_store = $this->model_setting_store->getRandomStoreIdNotDefaultStoreId();

        // random product
        $count_product    = rand(1, 5);
        $product_version_id = [];
        $product_pre_quantity = [];
        $total_value = [];
        $product_quantity = [];
        $product_cost_price = [];
        $product_index = $count_product == 1 ? [array_rand($products, $count_product)] : array_rand($products, $count_product);

        foreach ($product_index as $k => $index) {
            if (!isset($products[$index]) || empty($products[$index])) {
                continue;
            }

            $product_version_id[]       = $products[$index]['product_id'];
            $product_pre_quantity[]     = $this->model_catalog_product->getQuantityForProductByStore(self::STORE_DEFAULT, $products[$index]['product_id']);
            $product_cost_price[]       = $this->model_catalog_product->getCostPriceForProductByStore(self::STORE_DEFAULT, $products[$index]['product_id']);
            $product_quantity[]         = rand(1, 3); // random quantity
            $total_value[]              = (float)$product_cost_price[$k] * $product_quantity[$k];
        }

        return [
            "type_submit" => $store_transfer_receipt_requirement['status'],
            "export_store" => $export_store,
            "import_store" => $import_store,
            "common_quantity_transfer" => "",
            "product_version_id" => $product_version_id,
            "product_pre_quantity" => $product_pre_quantity,
            "total_value" => $total_value,
            "product_quantity" => $product_quantity,
            "product_cost_price" => $product_cost_price,
            "submit_action" => 1
        ];
    }

    private function createStoreTakeReceipt($products)
    {
        $this->load->model('catalog/store_take_receipt');

        $store_take_receipt_requirements = array_fill(0, 2, ['type_submit' => "apply"]);
        $store_take_receipt_requirements = array_merge($store_take_receipt_requirements, array_fill(0, 2, ['type_submit' => "draft"]));
        $store_take_receipt_requirements = array_merge($store_take_receipt_requirements, array_fill(0, 1, ['type_submit' => "cancel"]));

        foreach ($store_take_receipt_requirements as $store_take_receipt_requirement) {
            $this->model_catalog_store_take_receipt->addStoreTakeReceipt($this->generateStoreTakeReceiptData($products, $store_take_receipt_requirement));
        }
    }

    private function generateStoreTakeReceiptData($products, $store_take_receipt_requirement)
    {
        $this->load->model('catalog/product');

        $take_receipt_store = self::STORE_DEFAULT;
        // random product
        $count_product    = rand(1, 5);
        $product_version_id = [];
        $product_pre_quantity = [];
        $product_quantity = [];
        $product_cost_price = [];
        $differences_value = [];
        $reason = [];
        $product_index = $count_product == 1 ? [array_rand($products, $count_product)] : array_rand($products, $count_product);

        foreach ($product_index as $k => $index) {
            if (!isset($products[$index]) || empty($products[$index])) {
                continue;
            }

            $product_version_id[]       = $products[$index]['product_id'];
            $product_pre_quantity[]     = $this->model_catalog_product->getQuantityForProductByStore(self::STORE_DEFAULT, $products[$index]['product_id']);
            $product_quantity[]         = $this->model_catalog_product->getQuantityForProductByStore(self::STORE_DEFAULT, $products[$index]['product_id']);
            $product_cost_price[]       = $this->model_catalog_product->getCostPriceForProductByStore(self::STORE_DEFAULT, $products[$index]['product_id']);
            $differences_value[]        = (float)$product_cost_price[$k] * ((int)$product_quantity[$k] - (int)$product_pre_quantity[$k]);
            $reason[]                   = "";
        }

        $data = [
            'demo_data' => true,
            'type_submit' => $store_take_receipt_requirement['type_submit'],
            'take_receipt_store' => $take_receipt_store,
            'common_quantity_transfer' => "",
            'product_version_id' => $product_version_id,
            'product_pre_quantity' => $product_pre_quantity,
            'product_quantity' => $product_quantity,
            'differences_value' => $differences_value,
            'reason' => $reason,
            'submit_action' => $store_take_receipt_requirement['type_submit']
        ];

        if ($store_take_receipt_requirement['type_submit'] == "apply") {
            $data['select_all'] = 1;
        }

        return $data;
    }

    /**
     * remove product to store records which store is not exists
     */
    private function removeNotExistsProductToStore() {
        $this->load->model('demo_data/demo_data');
        $this->model_demo_data_demo_data->removeNotExistsProductToStore();
    }

    /**
     * update random products store
     */
    private function updateRandomProductToStore() {
        $this->load->model('setting/store');
        $this->load->model('demo_data/demo_data');

        $product_to_stores = $this->model_demo_data_demo_data->getRandomProductToStore(self::NUMBER_OF_RANDOM_PRODUCT_TO_STORES);
        $stores = $this->model_setting_store->getStores();
        if (!empty($stores)) {
            foreach ($product_to_stores as $key => $product_to_store) {
                $random_store = $this->getRandomArrayElement($stores);
                $this->model_demo_data_demo_data->updateProductStore($product_to_store, $random_store['store_id']);
            }
        }
    }
}