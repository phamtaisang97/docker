<?php

class ControllerSectionBlog extends Controller
{
    use Theme_Config_Util;
    use Device_Util;

    const DEFAULT_GRID_VALUE = [
        'grid_quantity' => 3,
        'grid_row' => 1,
        'grid_mobile_quantity' => 1,
        'grid_mobile_row' => 1
    ];
    const CHANGEABLE_BLOGS_PER_ROW = [2, 3, 4];
    const CHANGEABLE_ROWS = [1, 2, 3, 4];
    const CHANGEABLE_BLOGS_PER_ROW_ON_MOBILE = [1, 2];
    const CHANGEABLE_ROWS_ON_MOBILE = [1, 2, 3, 4];

    public function index()
    {
        $data = array();

        $this->load->language('section/blog');

        $data['section_idx'] = 0;
        if(isset($this->request->get['idx'])){
            $data['section_idx'] = $this->request->get['idx'];
        }

        /* undo-redo page */
        $data['href_change'] = $this->url->link('section/blog', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog&action=change', true);
        $data['href_undo'] = $this->url->link('section/blog', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog&action=undo', true);
        $data['href_redo'] = $this->url->link('section/blog', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog&action=redo', true);

        /* config with undo|redo|change api */
        $action = isset($this->request->get['action']) ? $this->request->get['action'] : 'config';
        if (in_array($action, ['config', 'undo', 'redo', 'change'])) {
            $data['config'] = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $data['config'] = is_array($data['config']) ? $data['config'] : json_decode($data['config'], true);

            /* v2.11.2 : grid config */
            $data['config']['setting']['title'] = isset($data['config']['setting']['title']) ? $data['config']['setting']['title'] : 'Blogs';

            $data['config']['display']['grid']['quantity'] = isset($data['config']['display']['grid']['quantity']) && in_array($data['config']['display']['grid']['quantity'], self::CHANGEABLE_BLOGS_PER_ROW) ? $data['config']['display']['grid']['quantity'] : self::DEFAULT_GRID_VALUE['grid_quantity'];
            $data['config']['display']['grid']['row'] = isset($data['config']['display']['grid']['row']) && in_array($data['config']['display']['grid']['row'], self::CHANGEABLE_ROWS) ? $data['config']['display']['grid']['row'] : self::DEFAULT_GRID_VALUE['grid_row'];
            $data['config']['display']['grid_mobile']['quantity'] = isset($data['config']['display']['grid_mobile']['quantity']) && in_array($data['config']['display']['grid_mobile']['quantity'], self::CHANGEABLE_BLOGS_PER_ROW_ON_MOBILE) ? $data['config']['display']['grid_mobile']['quantity'] : self::DEFAULT_GRID_VALUE['grid_mobile_quantity'];
            $data['config']['display']['grid_mobile']['row'] = isset($data['config']['display']['grid_mobile']['row']) && in_array($data['config']['display']['grid_mobile']['row'], self::CHANGEABLE_ROWS_ON_MOBILE) ? $data['config']['display']['grid_mobile']['row'] : self::DEFAULT_GRID_VALUE['grid_mobile_row'];

            $data['changeable_blogs_per_row'] = self::CHANGEABLE_BLOGS_PER_ROW;
            $data['changeable_rows'] = self::CHANGEABLE_ROWS;
            $data['changeable_blogs_per_row_on_mobile'] = self::CHANGEABLE_BLOGS_PER_ROW_ON_MOBILE;
            $data['changeable_rows_on_mobile'] = self::CHANGEABLE_ROWS_ON_MOBILE;
        }

        if (isset($data['current_config']['setting']['collection_id']) && trim($data['current_config']['setting']['collection_id']) != '') {
            $data['href_edit_collection'] = $this->url->link('catalog/collection/edit', 'user_token=' . $this->session->data['user_token'] . '&collection_id=' . $data['current_config']['setting']['collection_id'], true);
        } else {
            $data['href_edit_collection'] = $this->url->link('catalog/collection', 'user_token=' . $this->session->data['user_token'], true);
        }

        $data['selected_page'] = $this->getSelectedPageDueToRoute($this->request->get['route']);
        $data['token'] = $this->session->data['user_token'];

        /* base */
        $data['href_exit'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_homepage'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $data['href_category'] = $this->url->link('section_category/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_sections', true);
        $data['href_product_detail'] = $this->url->link('section_product_detail/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_detail_sections', true);
        $data['href_blog'] = $this->url->link('section_blog/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog_sections', true);
        $data['href_contact'] = $this->url->link('section_contact/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_sections', true);
        $data['back_section'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $data['preview'] = $this->load->controller('theme/preview');
        $data['is_on_mobile'] = $this->isMobile();

        $this->response->setOutput($this->load->view('section/blog', $data));
    }
}