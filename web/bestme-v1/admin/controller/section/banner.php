<?php

class ControllerSectionBanner extends Controller
{
    use Theme_Config_Util;
    use Device_Util;

    public function index()
    {
        $data = array();

        $this->load->language('section/banner');
        $data['banner_idx'] = 0;
        if(isset($this->request->get['idx'])){
            $data['banner_idx'] = $this->request->get['idx'];
        }
        /* config with undo|redo|change api */
        $action = isset($this->request->get['action']) ? $this->request->get['action'] : 'config';
        if (in_array($action, ['config', 'undo', 'redo', 'change'])) {
            $data['config'] = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $data['config'] = is_array($data['config']) ? $data['config'] : json_decode($data['config'], true);
        }

        // undo-redo page
        $data['href_change'] = $this->url->link('section/banner', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_banner&action=change', true);
        $data['href_undo'] = $this->url->link('section/banner', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_banner&action=undo', true);
        $data['href_redo'] = $this->url->link('section/banner', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_banner&action=redo', true);

        $data['href_edit_list_product'] = $this->url->link('category/category', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_section_theme'] = $this->url->link('theme/section_theme', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_section_theme', true);

        /* fix displaying selected page on header */
        $this->load->language('theme/preview');
        $data['selected_page'] = $this->getSelectedPageDueToRoute($this->request->get['route']);

        // get image size suggestion
        $this->load->model('extension/module/theme_builder_config');
        $image_size_suggestion = $this->model_extension_module_theme_builder_config->getImageSizeSuggestionConfig();
                // banner 1
        $banner1_size = (isset($image_size_suggestion['home']['banner_1']) && is_array($image_size_suggestion['home']['banner_1'])) ? $image_size_suggestion['home']['banner_1'] : [];
        $data['banner_size_width'][0] = array_shift($banner1_size);
        $data['banner_size_height'][0] = array_shift($banner1_size);
                // banner 2
        $banner2_size = (isset($image_size_suggestion['home']['banner_2']) && is_array($image_size_suggestion['home']['banner_2'])) ? $image_size_suggestion['home']['banner_2'] : [];
        $data['banner_size_width'][1] = array_shift($banner2_size);
        $data['banner_size_height'][1] = array_shift($banner2_size);
                // banner 3
        $banner3_size = (isset($image_size_suggestion['home']['banner_3']) && is_array($image_size_suggestion['home']['banner_3'])) ? $image_size_suggestion['home']['banner_3'] : [];
        $data['banner_size_width'][2] = array_shift($banner3_size);
        $data['banner_size_height'][2] = array_shift($banner3_size);
        $data['video_banner'] = BESTME_BANNER_GUIDE;
        /* base */
        $data['href_exit'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_homepage'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $data['href_category'] = $this->url->link('section_category/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_sections', true);
        $data['href_product_detail'] = $this->url->link('section_product_detail/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_detail_sections', true);
        $data['href_blog'] = $this->url->link('section_blog/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog_sections', true);
        $data['href_contact'] = $this->url->link('section_contact/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_sections', true);
        $data['preview'] = $this->load->controller('theme/preview');
        $data['is_on_mobile'] = $this->isMobile();

        $data['back_section'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $this->response->setOutput($this->load->view('section/banner', $data));
    }
}