<?php
class ControllerSectionListProduct extends Controller{
    use Theme_Config_Util;

    public function index(){
        $data = array();

        $this->load->language('section/list_product');

        // $data['list_product'] = [
        //     ['id' => 11, 'name' => 'Danh sách menu 12'],
        //     ['id' => 12, 'name' => 'Danh sách menu 22'],
        //     ['id' => 13, 'name' => 'Danh sách menu 33'],
        // ];
        $this->load->model('custom/menu');
        $list_product = $this->model_custom_menu->getAllMenu();
        $data['list_product'] = $list_product;
        /* config with undo|redo|change api */
        $action = isset($this->request->get['action']) ? $this->request->get['action'] : 'config';
        if (in_array($action, ['config', 'undo', 'redo', 'change'])) {
            $data['config'] = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $data['config'] = is_array($data['config']) ? $data['config'] : json_decode($data['config'], true);
        }

        // undo-redo page
        $data['href_change'] = $this->url->link('section/list_product', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_list_product&action=change', true);
        $data['href_undo'] = $this->url->link('section/list_product', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_list_product&action=undo', true);
        $data['href_redo'] = $this->url->link('section/list_product', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_list_product&action=redo', true);

        $data['href_edit_list_product'] = $this->url->link('category/category', 'user_token=' . $this->session->data['user_token'], true);

        $data['preview'] = $this->load->controller('theme/preview');

        $data['href_edit_collection'] = $this->url->link('catalog/collection', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_section_theme'] = $this->url->link('theme/section_theme', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_section_theme', true);

        /* fix displaying selected page on header */
        $this->load->language('theme/preview');
        $data['selected_page'] = $this->getSelectedPageDueToRoute($this->request->get['route']);

        /* base */
        $data['href_exit'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_homepage'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $data['href_category'] = $this->url->link('section_category/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_sections', true);
        $data['href_product_detail'] = $this->url->link('section_product_detail/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_detail_sections', true);
        $data['href_blog'] = $this->url->link('section_blog/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog_sections', true);
        $data['href_contact'] = $this->url->link('section_contact/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_sections', true);
        $data['back_section'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);

        $this->response->setOutput($this->load->view('section/list_product', $data));
    }
}