<?php

class ControllerSectionFooter extends Controller
{
    use Theme_Config_Util;
    use Device_Util;

    public function index()
    {
        $data = array();

        $this->load->language('section/footer');
        $data['footer_idx'] = 0;
        if(isset($this->request->get['idx'])){
            $data['footer_idx'] = $this->request->get['idx'];
        }
        $this->load->model('custom/menu');
        $list_menu = $this->model_custom_menu->getAllMenu();
        $data['collections'] = $list_menu;
        $data['quick_links'] = $list_menu;

        /* config with undo|redo|change api */
        $action = isset($this->request->get['action']) ? $this->request->get['action'] : 'config';
        if (in_array($action, ['config', 'undo', 'redo', 'change'])) {
            $data['config'] = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $data['config'] = is_array($data['config']) ? $data['config'] : json_decode($data['config'], true, 512, JSON_UNESCAPED_UNICODE|JSON_HEX_APOS);

            // v2.8.3 tuning: support add contact more:
            // support contact_more
            if (!isset($data['config']['contact_more'])) {
                $data['config']['contact_more'] = [];
            }

            if (!isset($data['config']['subscribe'])) {
                $data['config']['subscribe']['shopee_visible'] = 1;
                $data['config']['subscribe']['tiki_visible'] = 1;
                $data['config']['subscribe']['lazada_visible'] = 1;
                $data['config']['subscribe']['sendo_visible'] = 1;
            }
        }

        // undo-redo page
        $data['href_change'] = $this->url->link('section/footer', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_footer&action=change', true);
        $data['href_undo'] = $this->url->link('section/footer', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_footer&action=undo', true);
        $data['href_redo'] = $this->url->link('section/footer', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_footer&action=redo', true);

        $data['href_edit_list_product'] = $this->url->link('category/category', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_section_theme'] = $this->url->link('theme/section_theme', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_section_theme', true);

        $data['preview'] = $this->load->controller('theme/preview');
        $data['is_on_mobile'] = $this->isMobile();

        // menu edit/add link
        // collection
        if (isset($data['config']['collection']['menu_id']) && trim($data['config']['collection']['menu_id']) != '' && $this->checkMenuInList($data['config']['collection']['menu_id'], $data['collections'])){
            $data['href_edit_menu_collection'] = $this->url->link('custom/menu/edit', 'user_token=' . $this->session->data['user_token'] . '&menu_popup=true&menu_id=' . trim($data['config']['collection']['menu_id']), true);
            $data['txt_menu_edit_collection'] = $this->language->get('txt_menu_edit');
        }else{
            $data['href_edit_menu_collection'] = $this->url->link('custom/menu/add', 'user_token=' . $this->session->data['user_token'] . '&menu_popup=true', true);
            $data['txt_menu_edit_collection'] = $this->language->get('txt_menu_add');
        }
        // quick-links
        if (isset($data['config']['quick-links']['menu_id']) && trim($data['config']['quick-links']['menu_id']) != '' && $this->checkMenuInList($data['config']['quick-links']['menu_id'], $data['quick_links'])){
            $data['href_edit_menu_quick_links'] = $this->url->link('custom/menu/edit', 'user_token=' . $this->session->data['user_token'] . '&menu_popup=true&menu_id=' . trim($data['config']['quick-links']['menu_id']), true);
            $data['txt_menu_edit_quick_links'] = $this->language->get('txt_menu_edit');
        }else{
            $data['href_edit_menu_quick_links'] = $this->url->link('custom/menu/add', 'user_token=' . $this->session->data['user_token'] . '&menu_popup=true', true);
            $data['txt_menu_edit_quick_links'] = $this->language->get('txt_menu_add');
        }

        /* fix displaying selected page on header */
        $this->load->language('theme/preview');
        $data['selected_page'] = $this->getSelectedPageDueToRoute($this->request->get['route']);

        /* base */
        $data['href_exit'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_homepage'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $data['href_category'] = $this->url->link('section_category/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_sections', true);
        $data['href_product_detail'] = $this->url->link('section_product_detail/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_detail_sections', true);
        $data['href_blog'] = $this->url->link('section_blog/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog_sections', true);
        $data['href_contact'] = $this->url->link('section_contact/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_sections', true);
        $data['back_section'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);

        $this->response->setOutput($this->load->view('section/footer', $data));
    }

    private function checkMenuInList($menu_id, $list)
    {
        if (!is_array($list)) {
            return false;
        }
        foreach ($list as $item){
            if (!array_key_exists('menu_id',$item)){
                continue;
            }
            if ($item['menu_id'] == $menu_id){
                return true;
            }
        }

        return false;
    }
}