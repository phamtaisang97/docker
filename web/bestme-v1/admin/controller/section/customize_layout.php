<?php

class ControllerSectionCustomizeLayout extends Controller
{
    use Theme_Config_Util;
    use Device_Util;
    use Sync_Bestme_Data_To_Welcome_Util;

    public function index()
    {
        $data = array();

        $data['blog_idx'] = 0;
        if (isset($this->request->get['idx'])) {
            $data['blog_idx'] = $this->request->get['idx'];
        }

        $this->load->language('section/customize_layout');

        // param url section
        $data['url_header'] = $this->url->link('section/header', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_header', true);
        /* config with change api */
        $data['href_change'] = $this->url->link('section/customize_layout', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_customize_layout&action=change', true);

        /* config from db */
        $action = isset($this->request->get['action']) ? $this->request->get['action'] : 'config';
        if (in_array($action, ['config', 'change', 'undo', 'redo'])) {
            $data['config'] = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $data['config'] = is_array($data['config']) ? $data['config'] : json_decode($data['config'], true);

            // product groups
            if ($action != 'change') { // do dữ liệu được lưu vào session được lấy từ phương thức POST do đó phải tách xuống bên dưới
                $orig_key = $this->request->get['key'];
                $this->request->get['key'] = ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CUSTOMIZE_LAYOUT;
                $customize_layout = $this->load->controller('extension/module/theme_builder_config/' . $action);
                $data['config_section_customize_layout'] = is_array($customize_layout) ? $customize_layout : json_decode($customize_layout, true);
                // restore request "key"
                $this->request->get['key'] = $orig_key;
                // end product groups
            }
        }

        $this->load->model('setting/setting');
        $this->load->model('extension/module/theme_builder_config');
        /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;

        $customize_layout = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CUSTOMIZE_LAYOUT);
        $customize_layout = json_decode($customize_layout, true);

        // get default theme config from json file
        $current_theme = $this->config->get('config_theme');

        $this->load->model('setting/setting');
        $theme_directory = $this->model_setting_setting->getSettingValue('theme_' . $current_theme . '_directory');

        $file_path = DIR_CATALOG . 'view/theme/' . $theme_directory . '/asset/default_config/default.json';
        $home_layout_blocks = [];
        if (file_exists($file_path) && is_readable($file_path)) {
            $default_config = file_get_contents($file_path);
            if ($default_config) {
                $defaultConfig = json_decode($default_config, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG);
                if ($defaultConfig) {
                    $is_change_version = isset($defaultConfig['version']) && ((isset($customize_layout['version']) && $defaultConfig['version'] > $customize_layout['version']) || !isset($customize_layout['version']));
                    if ($is_change_version) {
                        // update version save in db
                        $data['config']['version'] = $defaultConfig['version'];
                    }

                    // if db has no data or default config change version => get config from default config
                    if (!$customize_layout || $is_change_version) {
                        if ($defaultConfig['default_customize_layout']) {
                            $home_layout_blocks = $defaultConfig['default_customize_layout'];
                        }
                    } else {
                        $home_layout_blocks = $customize_layout['default_customize_layout'];
                    }
                }
            }
        }

        $customize_layout_translate = [];
        foreach ($home_layout_blocks as $home_layout_block) {
            $home_layout_block['txt_name'] = $this->language->get($home_layout_block['name']);
            $customize_layout_translate[] = $home_layout_block;
        }
        $data['sections_layout'] = $customize_layout_translate;

        $data['preview'] = $this->load->controller('theme/preview');
        $data['href_preview'] =  $this->getPreviewPageUrlDueToRoute($this->request->get['route']) . "&key=" . token(32) .'&config=';
        $data['href_back'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $data['is_on_mobile'] = $this->isMobile();

        $this->response->setOutput($this->load->view('section/customize_layout', $data));
    }
}