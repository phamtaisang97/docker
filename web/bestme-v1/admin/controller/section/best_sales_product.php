<?php
class ControllerSectionBestSalesProduct extends Controller{
    use Theme_Config_Util;
    use Device_Util;

    const CHANGEABLE_ROWS = [1, 2, 3, 4];
    const CHANGEABLE_ROWS_ON_MOBILE = [1, 2];

    public function index(){
        $data = array();

        $this->load->language('section/best_sales_product');
        $data['product_idx'] = 0;
        if(isset($this->request->get['idx'])){
            $data['product_idx'] = $this->request->get['idx'];
        }
        $this->load->model('catalog/collection');
        $this->load->model('catalog/category');
        $data['collections'] = $this->model_catalog_collection->getAllcollection();
        $data['categories'] = $this->model_catalog_category->getAllCategories();
        /* config with undo|redo|change api */
        $action = isset($this->request->get['action']) ? $this->request->get['action'] : 'config';
        if (in_array($action, ['config', 'undo', 'redo', 'change'])) {
            $data['config'] = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $data['config'] = is_array($data['config']) ? $data['config'] : json_decode($data['config'], true);
            // v2.7.2 tuning: support setting auto play and loop. format:
            // => migrate: add new key to config
            if (!isset($data['config']['setting']) || !is_array($data['config']['setting'])) {
                // v2.7.2 tuning: support setting resource_type category. format:
                // => migrate: add new key to config
                if (!isset($data['config']['setting']['resource_type'])) {
                    $data['config']['setting']['resource_type'] = Product_Util_Trait::$PRODUCT_BLOCK_RESOURCE_TYPE_COLLECTION;
                }

                if (!isset($data['config']['setting']['category_id'])) {
                    $data['config']['setting']['category_id'] = 1;
                }
            }

            /* v2.8.3.1: config quantity per row */
            $data['changeable_rows'] = self::CHANGEABLE_ROWS;
            $data['changeable_rows_on_mobile'] = self::CHANGEABLE_ROWS_ON_MOBILE;

            // set first changeable row value as default if row config is not exists
            $data['config']['display']['grid']['row'] = isset($data['config']['display']['grid']['row']) ? $data['config']['display']['grid']['row'] : self::CHANGEABLE_ROWS[0];
            $data['config']['display']['grid_mobile']['row'] = isset($data['config']['display']['grid_mobile']['row']) ? $data['config']['display']['grid_mobile']['row'] : self::CHANGEABLE_ROWS_ON_MOBILE[0];

            /* v2.10.2 : config autoplay , loop */
            if (!isset($data['config']['setting']['autoplay'])) {
                $data['config']['setting']['autoplay'] = 1;
            }

            if (!isset($data['config']['setting']['autoplay_time'])) {
                $data['config']['setting']['autoplay_time'] = 5;
            }

            if (!isset($data['config']['setting']['loop'])) {
                $data['config']['setting']['loop'] = 1;
            }
        }

        // undo-redo page
        $data['href_change'] = $this->url->link('section/best_sales_product', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_best_sales_product&action=change', true);
        $data['href_undo'] = $this->url->link('section/best_sales_product', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_best_sales_product&action=undo', true);
        $data['href_redo'] = $this->url->link('section/best_sales_product', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_best_sales_product&action=redo', true);

        $data['preview'] = $this->load->controller('theme/preview');
        $data['is_on_mobile'] = $this->isMobile();

        if (isset($data['config']['setting']['collection_id']) && trim($data['config']['setting']['collection_id']) != ''){
            $data['href_edit_collection'] = $this->url->link('catalog/collection/edit', 'user_token=' . $this->session->data['user_token'] . '&collection_id='. $data['config']['setting']['collection_id'], true);
        }else{
            $data['href_edit_collection'] = $this->url->link('catalog/collection', 'user_token=' . $this->session->data['user_token'], true);
        }

        $data['href_section_theme'] = $this->url->link('theme/section_theme', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_section_theme', true);

        /* fix displaying selected page on header */
        $this->load->language('theme/preview');
        $data['selected_page'] = $this->getSelectedPageDueToRoute($this->request->get['route']);

        /* base */
        $data['href_exit'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_homepage'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $data['href_category'] = $this->url->link('section_category/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_sections', true);
        $data['href_product_detail'] = $this->url->link('section_product_detail/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_detail_sections', true);
        $data['href_blog'] = $this->url->link('section_blog/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog_sections', true);
        $data['href_contact'] = $this->url->link('section_contact/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_sections', true);
        $data['back_section'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);

        $this->response->setOutput($this->load->view('section/best_sales_product', $data));
    }
}