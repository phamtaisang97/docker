<?php

class ControllerSectionSlideshow extends Controller
{
    use Theme_Config_Util;
    use Device_Util;

    public function index()
    {
        $data = array();
        $this->load->language('section/slideshow');
        $data['slide_idx'] = 999;
        if(isset($this->request->get['idx'])){
            $data['slide_idx'] = $this->request->get['idx'];
        }
        /* config with undo|redo|change api */
        $action = isset($this->request->get['action']) ? $this->request->get['action'] : 'config';
        if (in_array($action, ['config', 'undo', 'redo', 'change'])) {
            $data['config'] = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $data['config'] = is_array($data['config']) ? $data['config'] : json_decode($data['config'], true);
        }

        // undo-redo page
        $data['href_change'] = $this->url->link('section/slideshow', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_slideshow&action=change', true);
        $data['href_undo'] = $this->url->link('section/slideshow', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_slideshow&action=undo', true);
        $data['href_redo'] = $this->url->link('section/slideshow', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_slideshow&action=redo', true);

        $data['href_section_theme'] = $this->url->link('theme/section_theme', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_section_theme', true);

        $data['preview'] = $this->load->controller('theme/preview');
        $data['is_on_mobile'] = $this->isMobile();

        /* fix displaying selected page on header */
        $this->load->language('theme/preview');
        $data['selected_page'] = $this->getSelectedPageDueToRoute($this->request->get['route']);

        // get image size suggestion
        $this->load->model('extension/module/theme_builder_config');
        $image_size_suggestion = $this->model_extension_module_theme_builder_config->getImageSizeSuggestionConfig();
        $slide_show_size = (isset($image_size_suggestion['home']['slide_show']) && is_array($image_size_suggestion['home']['slide_show'])) ? $image_size_suggestion['home']['slide_show'] : [];
        $data['slide_show_size_width'] = array_shift($slide_show_size);
        $data['slide_show_size_height'] = array_shift($slide_show_size);

        /* base */
        $data['href_exit'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_homepage'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $data['href_category'] = $this->url->link('section_category/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_sections', true);
        $data['href_product_detail'] = $this->url->link('section_product_detail/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_detail_sections', true);
        $data['href_blog'] = $this->url->link('section_blog/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog_sections', true);
        $data['href_contact'] = $this->url->link('section_contact/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_sections', true);
        $data['back_section'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);

        $data['video_slide'] = BESTME_SLIDE_GUIDE;
        // temp: add ids to config
        if (!empty($data['config']['display'])) {
            foreach ($data['config']['display'] as &$value) {
                $value['id'] = rand(0, 99999999); // random_int in php7 only!
            }
        } else {
            $display = array('display' => array());
            $data['config'] = $data['config'] + $display;
        }
        if ($this->isSupportThemeFeature(self::$VIDEO_SLIDE)) {
            $data['slide_videos'] = true;

            // Add default value for type and video-url
            foreach ($data['config']['display'] as &$item) {
                if (!isset($item['type'])) {
                    $item['type'] = 'image';
                }
                if (!isset($item['video-url'])) {
                    $item['video-url'] = '';
                }
            }
            unset($item);
        }

        unset($value);

        $this->response->setOutput($this->load->view('section/slideshow', $data));
    }
}