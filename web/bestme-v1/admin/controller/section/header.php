<?php

class ControllerSectionHeader extends Controller
{
    use Theme_Config_Util;
    use Device_Util;

    public function index()
    {
        $data = array();

        $this->load->language('section/header');
        $data['header_idx'] = 0;
        if(isset($this->request->get['idx'])){
            $data['header_idx'] = $this->request->get['idx'];
        }

        $this->load->model('custom/menu');
        $list_menu = $this->model_custom_menu->getAllMenu();
        // $data['list_product'] = [
        //     ['id' => 11, 'name' => 'Danh sách menu 12'],
        //     ['id' => 12, 'name' => 'Danh sách menu 22'],
        //     ['id' => 13, 'name' => 'Danh sách menu 33'],
        // ];
        $data['list_menu'] = $list_menu;
        /* config with undo|redo|change api */
        $action = isset($this->request->get['action']) ? $this->request->get['action'] : 'config';
        if (in_array($action, ['config', 'undo', 'redo', 'change'])) {
            $data['config'] = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $data['config'] = is_array($data['config']) ? $data['config'] : json_decode($data['config'], true);
            if (!isset($data['config']['logo']['alt'])) {
                $data['config']['logo']['alt'] = "alt";
            }
        }
        // undo-redo page
        $data['href_change'] = $this->url->link('section/header', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_header&action=change', true);
        $data['href_undo'] = $this->url->link('section/header', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_header&action=undo', true);
        $data['href_redo'] = $this->url->link('section/header', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_header&action=redo', true);

        $data['href_section_theme'] = $this->url->link('theme/section_theme', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_section_theme', true);

        $data['preview'] = $this->load->controller('theme/preview');
        $data['is_on_mobile'] = $this->isMobile();

        // get image size suggestion
        $this->load->model('extension/module/theme_builder_config');
        $image_size_suggestion = $this->model_extension_module_theme_builder_config->getImageSizeSuggestionConfig();
        $logo_size = (array_key_exists('logo', $image_size_suggestion) && is_array($image_size_suggestion['logo'])) ? $image_size_suggestion['logo'] : [];
        $data['logo_size_width'] = array_shift($logo_size);
        $data['logo_size_height'] = array_shift($logo_size);

        // menu edit/add link
        if (isset($data['config']['menu']['display-list']['id']) && trim($data['config']['menu']['display-list']['id']) != '' && $this->checkMenuInList($data['config']['menu']['display-list']['id'], $data['list_menu'])){
            $data['href_edit_menu'] = $this->url->link('custom/menu/edit', 'user_token=' . $this->session->data['user_token'] . '&menu_popup=true&menu_id=' . trim($data['config']['menu']['display-list']['id']), true);
            $data['txt_menu_edit'] = $this->language->get('txt_menu_edit');
        }else{
            $data['href_edit_menu'] = $this->url->link('custom/menu/add', 'user_token=' . $this->session->data['user_token'] . '&menu_popup=true', true);
            $data['txt_menu_edit'] = $this->language->get('txt_menu_add');
        }

        /* fix displaying selected page on header */
        $this->load->language('theme/preview');
        $data['selected_page'] = $this->getSelectedPageDueToRoute($this->request->get['route']);

        /* base */
        $data['href_exit'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_homepage'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $data['href_category'] = $this->url->link('section_category/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_sections', true);
        $data['href_product_detail'] = $this->url->link('section_product_detail/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_detail_sections', true);
        $data['href_blog'] = $this->url->link('section_blog/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog_sections', true);
        $data['href_contact'] = $this->url->link('section_contact/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_sections', true);
        $data['back_section'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);

        $this->response->setOutput($this->load->view('section/header', $data));
    }

    private function checkMenuInList($menu_id, $list)
    {
        if (!is_array($list)) {
            return false;
        }
        foreach ($list as $item){
            if (!array_key_exists('menu_id',$item)){
                continue;
            }
            if ($item['menu_id'] == $menu_id){
                return true;
            }
        }

        return false;
    }
}