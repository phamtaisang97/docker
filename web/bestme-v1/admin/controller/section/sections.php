<?php

class ControllerSectionSections extends Controller
{
    use Theme_Config_Util;
    use Device_Util;
    use Sync_Bestme_Data_To_Welcome_Util;

    const ON_BOARDING_STEP = 2;

    const CHANGEABLE_ROWS = [1, 2, 3, 4];
    const CHANGEABLE_ROWS_ON_MOBILE = [1, 2];

    public function index()
    {
        $data = array();

        $data['blog_idx'] = 0;
        if(isset($this->request->get['idx'])){
            $data['blog_idx'] = $this->request->get['idx'];
        }

        $this->load->language('section/sections');

        // param url section
        $data['url_header'] = $this->url->link('section/header', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_header', true);
        $data['url_slideshow'] = $this->url->link('section/slideshow', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_slideshow', true);
        $data['url_list_product'] = $this->url->link('section/list_product', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_list_product', true);
        $data['url_hot_product'] = $this->url->link('section/hot_product', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_hot_product', true);
        $data['url_best_sales_product'] = $this->url->link('section/best_sales_product', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_best_sales_product', true);
        $data['url_best_views_product'] = $this->url->link('section/best_views_product', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_best_views_product', true);
        $data['url_new_product'] = $this->url->link('section/new_product', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_new_product', true);
        $data['url_detail_product'] = $this->url->link('section/detail_product', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_detail_product', true);
        $data['url_content_customize'] = $this->url->link('section/content_customize', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_content_customize', true);
        $data['url_banner'] = $this->url->link('section/banner', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_banner', true);
        $data['url_partner'] = $this->url->link('section/partner', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_partner', true);
        $data['url_blog'] = $this->url->link('section/blog', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog', true);
        $data['url_footer'] = $this->url->link('section/footer', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_footer', true);

        // tabs
        $data['href_tab_section'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections#nav-home', true);
        $data['href_tab_theme'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections#nav-profile', true);

        $data['href_section_theme'] = $this->url->link('theme/section_theme', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_section_theme', true);

        /* config with change api */
        $data['href_change'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections&action=change', true);

        /* config from db */
        $action = isset($this->request->get['action']) ? $this->request->get['action'] : 'config';
        if (in_array($action, ['config', 'change', 'undo', 'redo'])) {
            $data['config'] = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $data['config'] = is_array($data['config']) ? $data['config'] : json_decode($data['config'], true);
            // v2.11.2 tuning
            if (!isset($data['config']['section']['rate']) || !is_array($data['config']['section']['rate'])) {
                $data['config']['section']['rate'] = [
                    'name' => 'rate',
                    'text' => 'Đánh giá website',
                    'visible' => 1
                ];
            }
            // product groups
            if ($action != 'change'){ // do dữ liệu được lưu vào session được lấy từ phương thức POST do đó phải tách xuống bên dưới
                $orig_key = $this->request->get['key'];
                $this->request->get['key'] = ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_PRODUCT_GROUPS;
                $product_groups = $this->load->controller('extension/module/theme_builder_config/' . $action);
                $data['config_product_groups'] = is_array($product_groups) ? $product_groups : json_decode($product_groups, true);
                // restore request "key"
                $this->request->get['key'] = $orig_key;
                // end product groups
            }
        }
        $data['href_product_group'] = $this->url->link('section/sections/product_groups', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_groups', true);
        $data['href_product_group_change'] = $this->url->link('section/sections/product_groups', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_groups&action=change', true);

        /* fix displaying selected page on header */
        $this->load->language('theme/preview');
        $data['selected_page'] = $this->getSelectedPageDueToRoute($this->request->get['route']);

        $this->load->model('setting/setting');

        // on boarding step
        $config_on_boarding_step_active = $this->config->get('config_onboarding_step_active');
        $on_boarding_step_active = empty($config_on_boarding_step_active) ? [] : explode(',', $config_on_boarding_step_active);
        if (!is_null($config_on_boarding_step_active) && !in_array(self::ON_BOARDING_STEP, $on_boarding_step_active)) {
            array_push($on_boarding_step_active, self::ON_BOARDING_STEP);
            $this->model_setting_setting->editSettingValue('config', 'config_onboarding_step_active', implode(',', $on_boarding_step_active));
        }

        /* publish sync data */
        $shop_name = $this->model_setting_setting->getShopName();
        $onboarding_data = [
            'shop_domain' => $shop_name,
            'last_step' => self::ON_BOARDING_STEP,
            'steps' => is_array($on_boarding_step_active) ? count($on_boarding_step_active) : 0
        ];
        $this->publishSyncDataOnboarding($onboarding_data);

        /* base */
        $data['href_exit'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_homepage'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $data['href_category'] = $this->url->link('section_category/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_sections', true);
        $data['href_product_detail'] = $this->url->link('section_product_detail/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_detail_sections', true);
        $data['href_blog'] = $this->url->link('section_blog/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog_sections', true);
        $data['href_contact'] = $this->url->link('section_contact/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_sections', true);
        $data['preview'] = $this->load->controller('theme/preview');
        $data['is_on_mobile'] = $this->isMobile();

        /* customize the layout */
        if ($this->isSupportThemeFeature(self::$CUSTOMIZE_LAYOUT)) {
            $data['href_customize_layout'] = $this->url->link('section/customize_layout', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_customize_layout', true);
        }

        /* rate form */
        if ($this->isSupportThemeFeature(self::$RATE_FORM)) {
            $data['url_rate'] = $this->url->link('section/rate', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_rate', true);
        }

        /* customize blogs grid */
        $data['is_support_blogs_grid'] = $this->isSupportThemeFeature(self::$CUSTOMIZE_BLOG_GRID);

        $this->response->setOutput($this->load->view('section/sections', $data));
    }

    public function product_groups()
    {

        $data = array();

        $this->load->language('section/hot_product');

        $data['product_idx'] = 0;
        if(isset($this->request->get['idx'])){
            $data['product_idx'] = $this->request->get['idx'];
        }
        $this->load->model('catalog/collection');
        $this->load->model('catalog/category');
        $data['collections'] = $this->model_catalog_collection->getAllcollection();
        $data['categories'] = $this->model_catalog_category->getAllCategories();

        /* undo-redo page */
        $data['href_change'] = $this->url->link('section/sections/product_groups', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_groups&action=change', true);
        $data['href_undo'] = $this->url->link('section/sections/product_groups', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_groups&action=undo', true);
        $data['href_redo'] = $this->url->link('section/sections/product_groups', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_groups&action=redo', true);

        /* config with undo|redo|change api */
        $action = isset($this->request->get['action']) ? $this->request->get['action'] : 'config';
        if (in_array($action, ['config', 'undo', 'redo', 'change'])) {
            $data['config'] = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $data['config'] = is_array($data['config']) ? $data['config'] : json_decode($data['config'], true);
            if (isset($this->request->get['group_id'])){
                $group_id = $this->request->get['group_id'];
                $data['group_id'] = $group_id;

                // v2.7.2 tuning: support setting resource_type category. format:
                // => migrate: add new key to config
                foreach ($data['config']['list'] as &$setting_config){
                    if (!isset($setting_config['setting']['resource_type'])) {
                        $setting_config['setting']['resource_type'] = Product_Util_Trait::$PRODUCT_BLOCK_RESOURCE_TYPE_COLLECTION;
                    }

                    if (!isset($setting_config['setting']['category_id'])) {
                        $setting_config['setting']['category_id'] = 1;
                    }
                }
                unset($setting_config);

                if (array_key_exists($group_id, $data['config']['list'])) {
                    $data['current_config'] = $data['config']['list'][$group_id];

                    /* v2.8.3.1: config quantity per row */
                    $data['changeable_rows'] = self::CHANGEABLE_ROWS;
                    $data['changeable_rows_on_mobile'] = self::CHANGEABLE_ROWS_ON_MOBILE;

                    // set first changeable row value as default if row config is not exists
                    $data['current_config']['display']['grid']['row'] = isset($data['current_config']['display']['grid']['row']) ? $data['current_config']['display']['grid']['row'] : self::CHANGEABLE_ROWS[0];
                    $data['current_config']['display']['grid_mobile']['row'] = isset($data['current_config']['display']['grid_mobile']['row']) ? $data['current_config']['display']['grid_mobile']['row'] : self::CHANGEABLE_ROWS_ON_MOBILE[0];

                    /* v2.10.2 : config autoplay , loop */
                    if (!isset($data['current_config']['setting']['autoplay'])) {
                        $data['current_config']['setting']['autoplay'] = 1;
                    }

                    if (!isset($data['current_config']['setting']['autoplay_time'])) {
                        $data['current_config']['setting']['autoplay_time'] = 5;
                    }

                    if (!isset($data['current_config']['setting']['loop'])) {
                        $data['current_config']['setting']['loop'] = 1;
                    }
                } else {
                    $data['current_config'] = [];
                }
            }
        }

        if (isset($data['current_config']['setting']['collection_id']) && trim($data['current_config']['setting']['collection_id']) != '') {
            $data['href_edit_collection'] = $this->url->link('catalog/collection/edit', 'user_token=' . $this->session->data['user_token'] . '&collection_id=' . $data['current_config']['setting']['collection_id'], true);
        } else {
            $data['href_edit_collection'] = $this->url->link('catalog/collection', 'user_token=' . $this->session->data['user_token'], true);
        }

        $data['selected_page'] = $this->getSelectedPageDueToRoute($this->request->get['route']);
        $data['token'] = $this->session->data['user_token'];

        /* base */
        $data['href_exit'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_homepage'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $data['href_category'] = $this->url->link('section_category/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_sections', true);
        $data['href_product_detail'] = $this->url->link('section_product_detail/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_detail_sections', true);
        $data['href_blog'] = $this->url->link('section_blog/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog_sections', true);
        $data['href_contact'] = $this->url->link('section_contact/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_sections', true);
        $data['back_section'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $data['preview'] = $this->load->controller('theme/preview');
        $data['is_on_mobile'] = $this->isMobile();

        $this->response->setOutput($this->load->view('section/product_groups', $data));
    }
}