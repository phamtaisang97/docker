<?php

class ControllerSectionContentCustomize extends Controller
{
    use Theme_Config_Util;
    use Device_Util;

    public function index()
    {
        $data = array();
        $data['slide_idx'] = 0;
        if(isset($this->request->get['idx'])){
            $data['slide_idx'] = $this->request->get['idx'];
        }

        $this->load->language('section/content_customize');
        /* config with undo|redo|change api */
        $action = isset($this->request->get['action']) ? $this->request->get['action'] : 'config';
        if (in_array($action, ['config', 'undo', 'redo', 'change'])) {
            $data['config'] = $this->load->controller('extension/module/theme_builder_config/' . $action);
            $data['config'] = is_array($data['config']) ? $data['config'] : json_decode($data['config'], true);
        }

        // undo-redo page
        $data['href_change'] = $this->url->link('section/content_customize', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_content_customize&action=change', true);
        $data['href_undo'] = $this->url->link('section/content_customize', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_content_customize&action=undo', true);
        $data['href_redo'] = $this->url->link('section/content_customize', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_content_customize&action=redo', true);

        /* fix displaying selected page on header */
        $this->load->language('theme/preview');
        $data['selected_page'] = $this->getSelectedPageDueToRoute($this->request->get['route']);

        $data['ckeditor_lang'] = $this->config->get('config_language') == 'vi-vn' ? 'vi' : 'en';
        $data['upload_image'] = str_replace('&amp;', '&', $this->url->link('catalog/product/upload', 'user_token=' . $this->session->data['user_token'] , true));
        $data['filemanager'] = str_replace('&amp;', '&', $this->url->link('common/filemanager', 'user_token=' . $this->session->data['user_token'] . '&ckeditor=1', true));
        if ($this->request->server['HTTPS']) {
            $data['base_url'] = HTTPS_SERVER;
        } else {
            $data['base_url'] = HTTP_SERVER;
        }

        /* base */
        $data['href_exit'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_homepage'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $data['href_category'] = $this->url->link('section_category/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_category_sections', true);
        $data['href_product_detail'] = $this->url->link('section_product_detail/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_product_detail_sections', true);
        $data['href_blog'] = $this->url->link('section_blog/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_blog_sections', true);
        $data['href_contact'] = $this->url->link('section_contact/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_contact_sections', true);
        $data['preview'] = $this->load->controller('theme/preview');
        $data['is_on_mobile'] = $this->isMobile();

        $data['back_section'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);

        $data['href_section_theme'] = $this->url->link('theme/section_theme', 'user_token=' . $this->session->data['user_token'] . '&key=config_theme_section_theme', true);

        $this->response->setOutput($this->load->view('section/content_customize', $data));
    }
}