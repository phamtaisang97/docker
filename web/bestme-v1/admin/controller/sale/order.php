<?php

class ControllerSaleOrder extends Controller
{
    use Order_Util_Trait;
    use RabbitMq_Trait;

    use AUTO_CREATE_RECEIPT_VOUCHER_UTIL_TRAIT;

    private $error = array();

    public $status_quick_view = [6, 7];

    public $ghn = 'ghn';

    public $vietpost = 'viettel_post';

    const APP_CODE_PAYMENT_VNPAY = 'paym_ons_vnpay';

    public function index()
    {
        $this->load->language('sale/order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/order');

        $this->document->addStyle('view/stylesheet/custom/order.css');

        $this->getList();
    }

    public function add()
    {
        $this->load->language('sale/order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/order');
        $this->load->model('discount/discount');
        $this->load->model('catalog/category');
        $this->load->model('catalog/manufacturer');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm($this->request->post)) {
            if (isset($this->request->post['selected_discount'])) {
                $select_discount_obj = htmlspecialchars_decode($this->request->post['selected_discount']);
            } else {
                $select_discount_obj = '';
            }

            // TODO: refactor submit data, DO NOT use this json_decode(json_decode(...
            $select_discount = json_decode(json_decode($select_discount_obj));
            $list_discounts = [];
            if (!empty($select_discount) && !empty($select_discount->selected_discounts)) {
                foreach ($select_discount->selected_discounts as $select) {
                    if (strlen($select->discounts)) {
                        $discounts = explode(',', $select->discounts);
                        $list_discounts = array_merge($list_discounts, $discounts);
                    }
                }
            }
            $list_discounts = array_unique($list_discounts);
            $order_id = $this->model_sale_order->addOrder($this->request->post);

            $check = $this->request->post;
            // Add or remove receipt_voucher
            $data_order = $this->request->post;

            if (isset($data_order['customer_full_name'])) {
                $name = extract_name($data_order['customer_full_name']);
                $data_order['customer_firstname'] = $name[0];
                $data_order['customer_lastname'] = $name[1];
            } else {
                $data_order['customer_firstname'] = "";
                $data_order['customer_lastname'] = "";
            }

            $customer_info = json_encode(['id' => null, 'name' => $data_order['customer_lastname'] . ' ' . $data_order['customer_firstname'] ]);
            $data_order['customer_info'] = $customer_info;
            $data_order['user_create_id'] = $this->user->getId();
            $receipt_voucher = $this->autoUpdateFromOrder($order_id, $data_order);

            $this->model_discount_discount->addOrderDiscount($order_id, $list_discounts);
            $this->session->data['success'] = $this->language->get('text_success_add');

            $url = '';

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_model'])) {
                $url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_price'])) {
                $url .= '&filter_price=' . $this->request->get['filter_price'];
            }

            if (isset($this->request->get['filter_quantity'])) {
                $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getForm();
    }

    public function detail()
    {
        $this->load->language('sale/order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/order');
        $this->load->model('catalog/category');
        $this->load->model('catalog/manufacturer');
        $this->load->model('discount/discount');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm($this->request->post, $this->request->get['order_id'])) {
            $this->model_sale_order->editOrder($this->request->get['order_id'], $this->request->post);

            // Add or remove receipt_voucher
            $data_order = $this->request->post;

            if (isset($data_order['customer_full_name'])) {
                $name = extract_name($data_order['customer_full_name']);
                $data_order['customer_firstname'] = $name[0];
                $data_order['customer_lastname'] = $name[1];
            } else {
                $data_order['customer_firstname'] = "";
                $data_order['customer_lastname'] = "";
            }

            $customer_info = json_encode(['id' => null, 'name' => $data_order['customer_lastname'] . ' ' . $data_order['customer_firstname'] ]);
            $data_order['customer_info'] = $customer_info;
            $data_order['user_create_id'] = $this->user->getId();
            $receipt_voucher = $this->autoUpdateFromOrder($this->request->get['order_id'], $data_order);

            // update order_to_discount
            if (isset($this->request->post['selected_discount'])) {
                $select_discount_obj = htmlspecialchars_decode($this->request->post['selected_discount']);
            } else {
                $select_discount_obj = '';
            }

            // TODO: refactor submit data, DO NOT use this json_decode(json_decode(...
            $select_discount = json_decode(json_decode($select_discount_obj));
            $list_discounts = [];
            if (!empty($select_discount) && !empty($select_discount->selected_discounts)) {
                foreach ($select_discount->selected_discounts as $select) {
                    if (strlen($select->discounts)) {
                        $discounts = explode(',', $select->discounts);
                        $list_discounts = array_merge($list_discounts, $discounts);
                    }
                }
            }
            $list_discounts = array_unique($list_discounts);
            $this->model_discount_discount->addOrderDiscount($this->request->get['order_id'], $list_discounts);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_model'])) {
                $url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_price'])) {
                $url .= '&filter_price=' . $this->request->get['filter_price'];
            }

            if (isset($this->request->get['filter_quantity'])) {
                $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->post['duplicate']) && $this->request->post['duplicate']) {
                $this->response->redirect($this->url->link('sale/order/duplicate', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $this->request->get['order_id'] . $url, true));
            } else {
                $this->response->redirect($this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . $url, true));
            }
        }

        $this->getForm();
    }

    public function duplicate()
    {
        $this->load->language('sale/order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/order');
        $this->load->model('catalog/category');
        $this->load->model('catalog/manufacturer');

        $this->document->addStyle('view/stylesheet/custom/order.css');
        unset($this->session->data['success']);

        $data['text_form'] = $this->language->get('text_add');

        // error message
        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];
            unset($this->session->data['error']);
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $url = '';

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $data['text_form'],
            'href' => '',
        );
        $EDITABLE_ORDER_STATUS = [
            // create order
            'create' => ['product_permission', 'customer_permission', 'note_permission', 'tag_permission'],
            // draft
            ModelSaleOrder::ORDER_STATUS_ID_DRAFT => ['product_permission', 'customer_permission', 'note_permission', 'tag_permission'],
            // processing
            ModelSaleOrder::ORDER_STATUS_ID_PROCESSING => ['product_permission', 'customer_permission', 'note_permission', 'tag_permission'],
            // delivering
            ModelSaleOrder::ORDER_STATUS_ID_DELIVERING => ['note_permission', 'tag_permission'],
            // complete
            ModelSaleOrder::ORDER_STATUS_ID_COMPLETED => ['note_permission', 'tag_permission'],
            // cancelled
            ModelSaleOrder::ORDER_STATUS_ID_CANCELLED => ['note_permission', 'tag_permission'],
        ];

        $data['cancel'] = $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['product_link_prefix'] = $this->url->link('catalog/product/edit', 'user_token=' . $this->session->data['user_token'] . '&product_id=' . $url, true);
        $data['update_delivery_methods_html_url'] = $this->url->link('sale/order/getDeliveryMethodsHtml', 'user_token=' . $this->session->data['user_token'], true);

        $data['user_token'] = $this->session->data['user_token'];

        if (isset($this->request->get['order_id'])) {
            $order_info = $this->model_sale_order->getOrder($this->request->get['order_id']);
        }
        $data['action'] = $this->url->link('sale/order/add', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['create_order_user_id'] = $this->user->getId();
        $data['date_added'] = $this->language->get('text_order_create_now');
        $data['editable_order_statuses'] = $EDITABLE_ORDER_STATUS['create'];

        if (!empty($order_info)) {
            $data['customer_id'] = $order_info['customer_id'];
            $data['firstname'] = $order_info['firstname'];
            $data['lastname'] = $order_info['lastname'];
            $data['fullname'] = $order_info['fullname'];
            $data['email'] = $order_info['email'];
            $data['telephone'] = $order_info['telephone'];

            $data['payment_firstname'] = $order_info['payment_firstname'];
            $data['payment_lastname'] = $order_info['payment_lastname'];
            $data['payment_company'] = $order_info['payment_company'];
            $data['payment_address_1'] = $order_info['payment_address_1'];
            $data['payment_address_2'] = $order_info['payment_address_2'];
            $data['payment_city'] = $order_info['payment_city'];
            $data['payment_postcode'] = $order_info['payment_postcode'];
            $data['payment_country_id'] = $order_info['payment_country_id'];
            $data['payment_zone_id'] = $order_info['payment_zone_id'];
            $data['payment_custom_field'] = $order_info['payment_custom_field'];
            $data['payment_method'] = $order_info['payment_method'];
            $data['payment_code'] = $order_info['payment_code'];

            $data['shipping_firstname'] = $order_info['shipping_firstname'];
            $data['shipping_lastname'] = $order_info['shipping_lastname'];
            $data['shipping_company'] = $order_info['shipping_company'];
            $data['shipping_address_1'] = $order_info['shipping_address_1'];
            $data['shipping_address_2'] = $order_info['shipping_address_2'];
            $data['shipping_city'] = $order_info['shipping_city'];
            $data['shipping_postcode'] = $order_info['shipping_postcode'];
            $data['shipping_country_id'] = $order_info['shipping_country_id'];
            $data['shipping_zone_id'] = $order_info['shipping_zone_id'];
            $data['shipping_custom_field'] = $order_info['shipping_custom_field'];
            $data['shipping_method'] = $order_info['shipping_method'];
            $data['shipping_code'] = $order_info['shipping_code'];
            $data['shipping_fee'] = $order_info['shipping_fee'];
            $data['shipping_method'] = $order_info['shipping_method'];
            $data['shipping_province_code'] = $order_info['shipping_province_code'];
            $data['shipping_district_code'] = $order_info['shipping_district_code'];
            $data['shipping_ward_code'] = $order_info['shipping_ward_code'];
            $data['order_status_id'] = $order_info['order_status_id'];
            $data['amount'] = $order_info['total'] - $order_info['shipping_fee'];
            $data['discount'] = isset($order_info['discount']) ? $order_info['discount'] : 0;
            $data['total'] = $order_info['total'];
            $data['create_order_user_id'] = $order_info['user_id'];

            $data['customer_info'] = $this->model_sale_order->getAddress($order_info['customer_id']);

            // Products
            $data['order_products'] = $this->model_sale_order->getInfoOrderProducts($this->request->get['order_id']);

            //tag
            $this->load->model('custom/tag');
            $data['tags'] = $this->model_custom_tag->getTagsByOrderId($this->request->get['order_id']);

            $data['order_status_id'] = $order_info['order_status_id'];
            $data['order_status_id_name'] = $this->model_sale_order->getOrderStatusName($order_info['order_status_id']);
            $data['comment'] = $order_info['comment'];
            $data['currency_code'] = $order_info['currency_code'];

            $this->load->model('localisation/vietnam_administrative');
            $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($data['shipping_province_code']);
            $data['shipping_province_name'] = $province['name'];
            $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($data['shipping_district_code']);
            $data['shipping_district_name'] = $district['name'];
            $ward = $this->model_localisation_vietnam_administrative->getWardByCode($data['shipping_ward_code']);
            $data['shipping_ward_name'] = $ward['name'];

            $data['full_shipping_address'] = convertAddressCustomer($data['shipping_address_1'], $province['name_with_type'], $district['name_with_type'], $ward['name_with_type'], ', ');

        }

        $this->load->model('user/user');
        $user = $this->model_user_user->getUser($data['create_order_user_id']);
        if ($user) {
            $data['create_order_user_name'] = $user['lastname'] . ' ' . $user['firstname'];
        }

        // order history
        if (isset($this->request->get['order_id'])) {
            $this->load->model('sale/order_history');
            $data['list_history'] = $this->model_sale_order_history->getHistory($this->request->get['order_id']);
        }

        $list_status = $this->model_sale_order->getListStatus();
        $list_order_payment = $this->model_sale_order->getListPayment();
        $list_order_shipment = $this->model_sale_order->getListShipment();

        // province
        $this->load->model('localisation/vietnam_administrative');
        $data['provinces'] = $this->model_localisation_vietnam_administrative->getProvinces();

        // setting delivery methods
        $this->load->model('setting/setting');
        $delivery_methods = $this->model_setting_setting->getSettingValue('delivery_methods');
        $delivery_methods = json_decode($delivery_methods, true);
        $delivery_methods = is_array($delivery_methods) ? $delivery_methods : [];
        foreach ($delivery_methods as $k => $delivery_method) {
            if ($delivery_method['status'] != '1') {
                continue;
            }
            $data['delivery_methods'][$k]['id'] = $delivery_method['id'];
            $data['delivery_methods'][$k]['name'] = $delivery_method['name'];
        }

        // payment method
        $payment_methods = $this->model_setting_setting->getSettingValue('payment_methods');
        $payment_methods = json_decode($payment_methods, true);
        $payment_methods = is_array($payment_methods) ? $payment_methods : [];
        $data['current_payment_method_name'] = '';
        // add vnpay payment method
        $this->load->model('appstore/my_app');
        if ($this->model_appstore_my_app->checkAppActive(self::APP_CODE_PAYMENT_VNPAY)) {
            $payment_methods[] = [
                'payment_id' => self::APP_CODE_PAYMENT_VNPAY,
                'name' => $this->language->get('text_vnpay_payment'),
                'guide' => '',
                'status' => 1
            ];
        } else if (isset($data['payment_method']) && $data['payment_method'] == self::APP_CODE_PAYMENT_VNPAY) {
            $data['current_payment_method_name'] = $this->language->get('text_vnpay_payment');
        }
        if (isset($data['order_source']) && $data['order_source'] == 'shopee') {
            $data['current_payment_method_name'] = isset($data['payment_method']) ? $data['payment_method'] : '';
        }
        foreach ($payment_methods as $k => $payment_method) {
            if ($payment_method['status'] != '1') {
                continue;
            }
            if (isset($data['payment_method']) && $payment_method['payment_id'] == $data['payment_method']) {
                $data['current_payment_method_name'] = $payment_method['name'];
            }
            $data['payment_methods'][$k]['payment_id'] = $payment_method['payment_id'];
            $data['payment_methods'][$k]['name'] = $payment_method['name'];
        }
        if (isset($data['payment_method']) && $data['payment_method'] == self::APP_CODE_PAYMENT_VNPAY && $data['payment_status'] == 1) {
            $data['not_change_payment_status'] = true;
        }

        // discount
        $discountCombineSetting = $this->model_setting_setting->getSettingValue('discount_combine');
        $data['check_discount_combine'] = $discountCombineSetting ? 1 : 0;

        $discountAutoSetting = $this->model_setting_setting->getSettingValue('discount_auto_apply');
        $data['check_discount_auto'] = $discountAutoSetting ? 1 : 0;

        $data['can_modify'] = $this->user->hasPermission('modify', 'sale/order') ? 1 : 0;
        $data['list_status'] = $list_status;
        $data['list_order_payment'] = $list_order_payment;
        $data['list_order_shipment'] = $list_order_shipment;
        $data['order_cancel_url'] = $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('sale/order_form', $data));
    }

    public function copy($order_id = null)
    {
        $this->load->language('sale/order');
        $this->load->model('sale/order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->response->addHeader('Content-Type: application/json');
        $json = [
            'status' => false,
            'message' => 'thất bại',
        ];

        if ($order_id) {
            $result = $this->model_sale_order->copy($order_id);
            if (!$result) {
                return false;
            }
            return true;
        }

        if (isset($this->request->post['order_id'])) {
            $order_id = $this->getValueFromRequest('post', 'order_id');
            $result = $this->model_sale_order->copy($order_id);
            if (!$result) {
                $json = [
                    'status' => false,
                    'message' => $this->language->get('not_copy')
                ];
                $this->response->setOutput(json_encode($json));
                return;
            }

            $json = [
                'status' => true,
                'message' => $this->language->get('success')
            ];
            $this->response->setOutput(json_encode($json));
            return;

        }

        $this->response->setOutput(json_encode($json));
    }

    public function copyAllCustom()
    {
        $this->load->language('sale/order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/order');
        $this->response->addHeader('Content-Type: application/json');
        $json = [
            'status' => false,
            'message' => $this->language->get('failure'),
        ];

        if (isset($this->request->post['selected'])) {
            foreach ($this->request->post['selected'] as $key => $value) {
                $result = $this->copy($value);
                if (!$result) {
                    $this->response->setOutput(json_encode($json));
                }
            }

            $json = [
                'status' => true,
                'message' => $this->language->get('success')
            ];

            $this->response->setOutput(json_encode($json));

            return;
        }

        $this->response->setOutput(json_encode($json));
    }

    public function edit()
    {
        $this->load->language('sale/order');
        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/order');
        $this->load->model('catalog/category');
        $this->load->model('catalog/manufacturer');

        $this->getEditForm();
    }

    public function editCustom()
    {
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $data = $this->request->post;
            $order_id = $this->getValueFromRequest('get', 'order_id');
            $this->load->model('sale/order');
            $this->model_sale_order->editOrderCustom($data, $order_id);

            // TODO: update order_to_discount...

            $this->response->redirect($this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], true));
        }
    }

    public function delete()
    {
        $this->load->language('sale/order');
        $this->load->model('sale/order');
        if (isset($this->request->get['order_id'])) {
            $order = $this->model_sale_order->getOrder($this->request->get['order_id']);
            if (!$order) {
                $this->session->data['error'] = $this->language->get('error_notfound_order');
            }
            if ($order['order_status_id'] != ModelSaleOrder::ORDER_STATUS_ID_DRAFT) {
                $this->session->data['error'] = $this->language->get('error_order_delete_s_6');
            }
            $this->model_sale_order->deleteOrderById($this->request->get['order_id']);
            $this->session->data['success'] = $this->language->get('text_delete_order_success');
        }
        $this->response->redirect($this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function destroy()
    {
        $this->response->addHeader('Content-Type: application/json');
        $json = [
            'status' => false,
            'message' => 'Fail',
        ];
        $orderId = $this->getValueFromRequest('post', 'order_id');
        if (isset($orderId)) {
            $this->load->model('sale/order');
            $this->model_sale_order->deleteOrderById($orderId);
            $json = [
                'status' => true,
                'message' => 'Success',
            ];
            $this->response->setOutput(json_encode($json));

            return;
        }

        $this->response->setOutput(json_encode($json));
    }

    protected function getList()
    {
        /* filter params */
        $order_status = $this->getValueFromRequest('get', 'filter_status');
        $data['order_status_id'] = $order_status;

        $order_payment = $this->getValueFromRequest('get', 'filter_payment_status');
        $data['order_payment'] = $order_payment;

        $order_phone = $this->getValueFromRequest('get', 'filter_phone');
        $order_source = $this->getValueFromRequest('get', 'filter_source');
        $order_discount = $this->getValueFromRequest('get', 'filter_discount');
        $order_tag = $this->getValueFromRequest('get', 'filter_tag');
        $order_tag = $order_tag ? array_map('trim', $order_tag) : '';
        $order_utm = $this->getValueFromRequest('get', 'filter_utm');
        $order_utm = $order_utm ? array_map('trim', $order_utm) : '';

        $filter_staff = $this->getValueFromRequest('get', 'filter_staff');

        // support params from redirect
        $order_tag_string = $this->getValueFromRequest('get', 'tag', '');
        $order_tag_string = empty(trim($order_tag_string)) ? [] : explode(',', $order_tag_string);
        $order_tag_string = $order_tag_string ? array_map('trim', $order_tag_string) : [];
        if (is_array($order_tag)) {
            $order_tag = array_merge($order_tag, $order_tag_string);
            $order_tag = array_values(array_unique($order_tag));
        } else {
            $order_tag = $order_tag_string;
        }
        $data['order_tag'] = $order_tag;
        $data['order_utm'] = $order_utm;

        $order_updated_time_type = $this->getValueFromRequest('get', 'filter_updated_time_type');
        $data['order_updated_time_type'] = $order_updated_time_type;

        $order_updated_time_value = $this->getValueFromRequest('get', 'filter_updated_time_value');
        $data['order_updated_time_value'] = $order_updated_time_value;

        $order_total_type = $this->getValueFromRequest('get', 'filter_total_type');
        $data['order_total_type'] = $order_total_type;

        $order_total_value = $this->getValueFromRequest('get', 'filter_total_value');
        $data['order_total_value'] = $order_total_value;

        /* search params */
        $order_search_text = $this->getValueFromRequest('get', 'order_search_text');
        $data['order_search_text'] = $order_search_text;

        $filter_name = $this->getValueFromRequest('get', 'filter_name');
        $data['filter_name'] = $filter_name;

        $order = $this->getValueFromRequest('get', 'order', 'DESC');
        $page = $this->getValueFromRequest('get', 'page', 1);
        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['order_search_text'])) {
            $url .= '&order_search_text=' . urlencode(html_entity_decode($this->request->get['order_search_text'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['order_status_id'])) {
            $url .= '&order_status_id=' . $this->request->get['order_status_id'];
        }
        if (isset($this->request->get['order_payment'])) {
            $url .= '&order_payment=' . $this->request->get['order_payment'];
        }
        if (isset($this->request->get['order_transfer'])) {
            $url .= '&order_transfer=' . $this->request->get['order_transfer'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        /* breadcrumbs */
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        /* links */
        $data['add'] = $this->url->link('sale/order/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        // $data['delete'] = str_replace('&amp;', '&', $this->url->link('sale/order/delete', 'user_token=' . $this->session->data['user_token'] . $url, true));
        $data['copy'] = $this->url->link('sale/order/copyAllCustom', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['action_in_stock_product'] = $this->url->link('sale/order/invoice', 'user_token=' . $this->session->data['user_token'], true);
        $data['orders'] = array();

        $filter_data = array(
            'order_status' => $order_status,
            'order_payment' => $order_payment,
            'order_tag' => $order_tag,
            'order_utm' => $order_utm,
            'order_updated_time_type' => $order_updated_time_type,
            'order_updated_time_value' => $order_updated_time_value,
            'order_total_type' => $order_total_type,
            'order_total_value' => $order_total_value,
            'order_search_text' => $order_search_text,
            'order' => $order,
            'order_code' => trim($filter_name),
            'order_phone' => $order_phone,
            'order_source' => $order_source,
            'order_discount' => $order_discount,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $filterStaff= null;
        $filter_staff = is_array($filter_staff) ? $filter_staff : [];
        if (!$this->user->canAccessAll()){
            $filter_staff[] = (int)$this->user->getId();
        }

        if (!empty($filter_staff)) {
            if (count($filter_staff) > 0) {
                $filterStaff = implode(',', ($filter_staff));
            } else {
                $filterStaff = (int)$filter_staff[0];
            }
        } else {
            $filterStaff = '';
        }
        $filter_data['filter_staff'] = $filterStaff;

        $order_total = $this->model_sale_order->getTotalOrders($filter_data);

        $CHANGEABLE_ORDER_STATUS = [
            // draft
            ModelSaleOrder::ORDER_STATUS_ID_DRAFT => [
                [
                    'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_PROCESSING,
                    'order_status_description' => $this->language->get('order_status_processing')
                ]
            ],
            // processing
            ModelSaleOrder::ORDER_STATUS_ID_PROCESSING => [
                [
                    'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_DELIVERING,
                    'order_status_description' => $this->language->get('order_status_delivering')
                ],
                [
                    'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_COMPLETED,
                    'order_status_description' => $this->language->get('order_status_complete')
                ],
                [
                    'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_CANCELLED,
                    'order_status_description' => $this->language->get('order_status_canceled')
                ]
            ],
            // delivering
            ModelSaleOrder::ORDER_STATUS_ID_DELIVERING => [
                [
                    'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_COMPLETED,
                    'order_status_description' => $this->language->get('order_status_complete')
                ],
                [
                    'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_CANCELLED,
                    'order_status_description' => $this->language->get('order_status_canceled')
                ]
            ],
            // complete
            ModelSaleOrder::ORDER_STATUS_ID_COMPLETED => [
                [
                    'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_CANCELLED,
                    'order_status_description' => $this->language->get('order_status_canceled')
                ]
            ],
            // cancelled
            ModelSaleOrder::ORDER_STATUS_ID_CANCELLED => []
        ];

        $results = $this->model_sale_order->getOrders($filter_data);

        $this->load->model('localisation/vietnam_administrative');
        $this->load->model('user/user');
        foreach ($results as $result) {
            $orderStatus = $this->model_sale_order->getNameStatusOrder($result['order_status_id']);
            $language_status = $this->convertStatusOrder($result['order_status_id']);
            $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($result['shipping_province_code']);
            $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($result['shipping_district_code']);
            $ward = $this->model_localisation_vietnam_administrative->getWardByCode($result['shipping_ward_code']);
            $data['orders'][] = array(
                'order_id' => $result['order_id_result'],
                'order_code' => $result['order_code'],
                'order_url' => $this->url->link('sale/order/detail', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . (int)$result['order_id_result'], true),
                'fullname' => $result['customer_full_name'],
                'order_full_name' => $result['order_full_name'],
                'customer_url' => $this->url->link('customer/customer/detail', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . (int)$result['customer_id'], true),
                'order_status_id' => $result['order_status_id'],
                'order_status_description' => $language_status,
                'changable_order_statuses' => $CHANGEABLE_ORDER_STATUS[$result['order_status_id']],
                'payment_status' => $result['payment_status'],
                'discount' => isset($result['discount']) ? number_format($result['discount'], 0, '', ',') : 0,
                'total' => number_format($result['total'], 0, '', ','),
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                'date_added_order' => convertDate($result['date_added_order']),
                'date_modified' => convertDate($result['date_modified']),
                'date_modified_mobile' => date('d/m/Y', strtotime($result['date_modified'])),
                'view' => $this->url->link('sale/order/view', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $result['order_id_result'] . $url, true),
                'edit' => $this->url->link('sale/order/edit', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $result['order_id_result'] . $url, true),
                'destroy' => $this->url->link('sale/order/destroy', 'user_token=' . $this->session->data['user_token'] . $url, true),
                'copy' => $this->url->link('sale/order/copy', 'user_token=' . $this->session->data['user_token'] . $url, true),
                'products' => $this->model_sale_order->getProductsDetailByOrder($result['order_id_result'], $result['source']),
                'telephone' => $result['telephone_order'],
                'address' => $result['source'] == 'shopee' ? $result['shipping_address_1'] :
                    convertAddressOrderList(isset($result['shipping_address_1']) ? $result['shipping_address_1'] : '',
                        isset($province['name']) ? $province['name'] : '',
                        isset($district['name']) ? $district['name'] : '',
                        isset($ward['name']) ? $ward['name'] : ''),
                'comment' => $result['comment'],
                'customer_id' => $result['customer_id'],
                'print_r' => $this->url->link('sale/order/invoice', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $result['order_id_result'] . $url, true),
                'delete_order' => $this->url->link('sale/order/delete', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $result['order_id_result'] . $url, true),
                'quick_view_edit' => in_array($result['order_status_id'], $this->status_quick_view) ? true : false,
                'previous_order_status_id' => $result['previous_order_status_id'],
                'order_source' => $this->language->get($result['source']),
                'changeable_manual_update_status' => 0 == $result['manual_update_status'] && !is_null($result['transport_order_code']),
                'user_created' => $this->model_user_user->getFullNameByUserId($result['user_id'])
            );

            // Update payment status for order status complete
            // TODO: why?... move to other place...
            if ($result['order_status_id'] == 9 && $result['payment_status'] == 0) {
                $this->model_sale_order->updateOrderPaymentStatus($result['order_id_result'], 1);
            }
        }

        $data['is_admin'] = $this->user->isAdmin() ? 1 : 0;
        $data['can_modify'] = $this->user->hasPermission('modify', 'sale/order') ? 1 : 0;

        $data['list_order_status'] = $this->model_sale_order->getListStatus();
        $data['list_order_payment'] = $this->model_sale_order->getListPayment();
        $data['list_order_transfer'] = $this->model_sale_order->getListShipment();
        $this->load->model('discount/discount');

        $list_discount= []; //$this->model_discount_discount->getDiscounts();

        $data['list_discount'] = $list_discount;

        $data['url_quick_update'] = $this->url->link('sale/order/updateOrderQuickView', 'user_token=' . $this->session->data['user_token'] . $url, true);

        // get provinces
        $this->load->model('localisation/vietnam_administrative');
        $data['provinces'] = $this->model_localisation_vietnam_administrative->getProvinces();

        $data['user_token'] = $this->session->data['user_token'];

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];

            unset($this->session->data['error']);
        } /*else {
            $data['error_warning'] = '';
        }*/

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }
        $url = '';
        $data['product_link_prefix'] = $this->url->link('catalog/product/edit', 'user_token=' . $this->session->data['user_token'] . '&product_id=' . $url, true);
        if (isset($this->request->get['order_search_text'])) {
            $url .= '&order_search_text=' . urlencode(html_entity_decode($this->request->get['order_search_text'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['order_status_id'])) {
            $url .= 'order_status_id=' . $this->request->get['order_status_id'];
        }

        if (isset($this->request->get['order_payment'])) {
            $url .= '&order_payment=' . $this->request->get['order_payment'];
        }

        if (isset($this->request->get['order_transfer'])) {
            $url .= '&order_transfer=' . $this->request->get['order_transfer'];
        }

        $pagination = new CustomPaginate();
        $pagination->total = $order_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

        // Add jump to page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        //get all tag
        $this->load->model('custom/tag');
        $data['tag_all'] = $this->model_custom_tag->getAllTagValueProduct();
        $data['action_add_tag'] = $this->url->link('sale/order/addTags', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_remove_tag'] = $this->url->link('sale/order/removeTags', 'user_token=' . $this->session->data['user_token'], true);

        $data['results'] = sprintf($this->language->get('text_pagination'),
            ($order_total)
                ? (($page - 1) * $this->config->get('config_limit_admin')) + 1
                : 0,
            ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin')))
                ? $order_total
                : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')),
            $order_total,
            ceil($order_total / $this->config->get('config_limit_admin'))
        );

        // for auto building selected filter in view
        if (isset($this->request->get['filter_status'])) {
            $data['filter_order_status'] = $this->request->get['filter_status'];
        }

        $data['url_filter'] = $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['url_load_user_optionone'] = $this->url->link('sale/order/loadUseroptionone', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $data['export'] = $this->url->link('sale/order/export', 'user_token=' . $this->session->data['user_token'], true);
        $data['update_order_status_url'] = $this->url->link('sale/order/updateStatus', 'user_token=' . $this->session->data['user_token'], true);
        $data['cancel_order_url'] = $this->url->link('sale/order/cancelOrder', 'user_token=' . $this->session->data['user_token'], true);
        $data['get_order_products_url'] = $this->url->link('sale/order/getOrderProducts', 'user_token=' . $this->session->data['user_token'], true);

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            // for ajax loading, not reload page on each filter/search
            $this->response->setOutput($this->load->view('sale/order_list_append', $data));
        } else {
            $this->response->setOutput($this->load->view('sale/order_list', $data));
        }
    }

    public function view()
    {
        $this->load->language('sale/order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/order');
        $this->load->model('catalog/category');
        $this->load->model('catalog/manufacturer');

        $orderId = $this->getValueFromRequest('get', 'order_id');
        $order = $this->model_sale_order->getOrderById($orderId);

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], true)
        );

        $list_status = $this->model_sale_order->getListStatus();
        $list_order_payment = $this->model_sale_order->getListPayment();
        $list_order_shipment = $this->model_sale_order->getListShipment();

        $data['order_detail'] = $order;
        $data['list_product'] = $this->getListProductForEditOrderForm($orderId);
        $data['list_categories'] = $this->model_catalog_category->getCategories();
        $data['list_manufactures'] = $this->model_catalog_manufacturer->getManufacturers();
        $data['list_status'] = $list_status;
        $data['list_order_payment'] = $list_order_payment;
        $data['list_order_shipment'] = $list_order_shipment;

        $data['order_cancel_url'] = $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], true);

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $this->response->setOutput($this->load->view('sale/order_view', $data));
    }

    public function getForm()
    {
        $this->document->addStyle('view/stylesheet/custom/order.css');

        $data['text_form'] = !isset($this->request->get['order_id']) ? $this->language->get('text_add') : $this->language->get('text_order_detail');
        //override document heading title
        $this->document->setTitle($data['text_form']);

        $url = '';

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $data['text_form'],
            'href' => '',
        );

        $CHANGEABLE_ORDER_STATUS = [
            // draft
            ModelSaleOrder::ORDER_STATUS_ID_DRAFT => [
                [
                    'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_PROCESSING,
                    'order_status_description' => $this->language->get('order_status_processing')
                ]
            ],
            // processing
            ModelSaleOrder::ORDER_STATUS_ID_PROCESSING => [
                [
                    'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_DELIVERING,
                    'order_status_description' => $this->language->get('order_status_delivering')
                ],
                [
                    'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_COMPLETED,
                    'order_status_description' => $this->language->get('order_status_complete')
                ],
                [
                    'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_CANCELLED,
                    'order_status_description' => $this->language->get('order_status_canceled')
                ]
            ],
            // delivering
            ModelSaleOrder::ORDER_STATUS_ID_DELIVERING => [
                [
                    'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_COMPLETED,
                    'order_status_description' => $this->language->get('order_status_complete')
                ],
                [
                    'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_CANCELLED,
                    'order_status_description' => $this->language->get('order_status_canceled')
                ]
            ],
            // complete
            ModelSaleOrder::ORDER_STATUS_ID_COMPLETED => [
                [
                    'order_status_id' => ModelSaleOrder::ORDER_STATUS_ID_CANCELLED,
                    'order_status_description' => $this->language->get('order_status_canceled')
                ]
            ],
            // cancelled
            ModelSaleOrder::ORDER_STATUS_ID_CANCELLED => []
        ];

        $EDITABLE_ORDER_STATUS = [
            // create order
            'create' => ['product_permission', 'customer_permission', 'note_permission', 'tag_permission'],
            // draft
            ModelSaleOrder::ORDER_STATUS_ID_DRAFT => ['product_permission', 'customer_permission', 'note_permission', 'tag_permission'],
            // processing
            ModelSaleOrder::ORDER_STATUS_ID_PROCESSING => ['product_permission', 'customer_permission', 'note_permission', 'tag_permission'],
            // delivering
            ModelSaleOrder::ORDER_STATUS_ID_DELIVERING => ['note_permission', 'tag_permission'],
            // complete
            ModelSaleOrder::ORDER_STATUS_ID_COMPLETED => ['note_permission', 'tag_permission'],
            // cancelled
            ModelSaleOrder::ORDER_STATUS_ID_CANCELLED => ['note_permission', 'tag_permission'],
        ];

        // get order_status_id_draft to disable order_payment_status_paid
        $data['order_status_id_draft'] = ModelSaleOrder::ORDER_STATUS_ID_DRAFT;

        $data['cancel'] = $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['product_link_prefix'] = $this->url->link('catalog/product/edit', 'user_token=' . $this->session->data['user_token'] . '&product_id=' . $url, true);
        $data['cancel_order_url'] = $this->url->link('sale/order/cancelOrder', 'user_token=' . $this->session->data['user_token'], true);
        $data['update_delivery_methods_html_url'] = $this->url->link('sale/order/getDeliveryMethodsHtml', 'user_token=' . $this->session->data['user_token'], true);

        $data['user_token'] = $this->session->data['user_token'];

        if (isset($this->request->get['order_id'])) {
            // check permission view
            if (!$this->checkPermission($this->request->get['order_id'])){
                $this->response->redirect($this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . $url, true));
            }

            $data['order_id'] = $this->request->get['order_id'];
            $data['action'] = $this->url->link('sale/order/detail', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $this->request->get['order_id'] . $url, true);
            $data['delete_order_url'] = $this->url->link('sale/order/delete', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $this->request->get['order_id'], true);
            $data['print_order_url'] = $this->url->link('sale/order/invoice', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $this->request->get['order_id'], true);
            $data['print_bill_of_lading'] = $this->url->link('sale/order/billOfLading', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $this->request->get['order_id'], true);
            $data['duplicate'] = $this->url->link('sale/order/duplicate', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $this->request->get['order_id'] . $url, true);
            $order_info = $this->model_sale_order->getOrder($this->request->get['order_id']);
            $order_utm = $this->model_sale_order->getOrderUtm($this->request->get['order_id']);
            if (empty($order_info)) {
                $this->session->data['error'] = $this->language->get('error_notfound_order');
                $this->response->redirect($this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . $url, true));
            }
        } else {
            $data['action'] = $this->url->link('sale/order/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        }
        $data['create_order_user_id'] = $this->user->getId();
        $data['date_added'] = '';
        $data['editable_order_statuses'] = $EDITABLE_ORDER_STATUS['create'];

        $this->load->model('customer/customer_group');
        $data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();

        if (!empty($order_info)) {
            $data['customer_url'] = $this->url->link('customer/customer/detail', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $order_info['customer_id'], true);
            $data['text_form'] = $this->language->get('heading_title') . ' ' . $order_info['order_code'];
            $data['customer_id'] = $order_info['customer_id'];
            $data['firstname'] = $order_info['firstname'];
            $data['lastname'] = $order_info['lastname'];
            $data['fullname'] = $order_info['fullname'];
            $data['email'] = $order_info['email'];
            $data['telephone'] = $order_info['telephone'];

            $data['transport_order_code'] = $order_info['transport_order_code'];
            $data['transport_name'] = $order_info['transport_name'];
            $data['transport_service_name'] = $order_info['transport_service_name'];
            $data['transport_display_name'] = ''; // Notice: only has value if use one transport (GHN, VTPOST, GHTK, ...)

            /* build transport_order_detail */
            // notice: some transport partners support api to get order info, and some do not support
            // so, build "transport_order_detail" from db for not supported case
            $transport_order_detail_from_db = [];
            if (!empty($data['transport_order_code'])) {
                $transport = new Transport($data['transport_name'], $data, $this);
                $is_validate_transport = $transport->getAdaptor() instanceof \Transport\Abstract_Transport;
                if ($is_validate_transport) {
                    $data['transport_display_name'] = $transport->getAdaptor()->getDisplayName();
                } else {
                    $data['transport_display_name'] = '';
                }

                $data['transport_order_active'] = true;
                try {
                    $data_for_api = [];
                    $data_for_api = array_merge($data_for_api, $transport_order_detail_from_db);
                    $data_for_api['token'] = $this->config->get('config_' . strtoupper($data['transport_name']) . '_token');
                    $data_for_api['transport_order_code'] = $data['transport_order_code'];
                    $data['transport_order_detail']['PickWarehouseName'] = $order_info['transport_current_warehouse'];
                    $data['transport_order_detail']['OrderCode'] = $data['transport_order_code'];
                    $data['transport_order_detail']['CurrentStatus'] = $order_info['transport_status'];

                    // always get transport status code from transport adaptor
                    $data['transport_order_detail']['CurrentStatusLanguage'] = $transport->getAdaptor()->getTransportStatusMessage($order_info['transport_status']);

                    /* No need always update order info on editing. TODO: remove... */
                    /*$response = $transport->getAdaptor()->getOrderInfo($data_for_api);
                    if ($response['code'] == 1 && strtolower($data['transport_name']) == $this->ghn) {
                        $data['transport_order_detail']['CurrentStatus'] = $response['data']['CurrentStatus'];
                        $this->model_sale_order->updateStatusDeliveryApi($data['order_id'], $response['data']['CurrentStatus']);
                        $data['transport_order_detail']['CurrentStatusLanguage'] = $this->language->get(strtolower($data['transport_name']) . '_' . $response['data']['CurrentStatus']);
                    }*/

                    // is cancelable order
                    /*if (!$transport->getAdaptor() instanceof \Transport\App_Trans) {
                        $data['transport_order_detail']['Cancelable'] = $data['transport_order_detail']['CurrentStatus'] == 'ReadyToPick' ||
                        $data['transport_order_detail']['CurrentStatus'] == 'ready_to_pick' ||
                        $data['transport_order_detail']['CurrentStatus'] == 'Picking' ||
                        $data['transport_order_detail']['CurrentStatus'] == 'picking' ||
                        $data['transport_order_detail']['CurrentStatus'] == 'approved' ? 1 : 0;
                    } else {
                        $data['transport_order_detail']['Cancelable'] = $transport->getAdaptor()->isCancelableOrder($data['transport_order_detail']['CurrentStatus']) ? 1 : 0;
                    }*/

                    $data['transport_order_detail']['Cancelable'] = 1;
                    if ($is_validate_transport) {
                        $data['transport_order_detail']['Cancelable'] = $transport->getAdaptor()->isCancelableOrder($data['transport_order_detail']['CurrentStatus']) ? 1 : 0;
                    }
                } catch (Exception $e) {
                }
            } else {
                $data['transport_order_active'] = false;
            }

            $data['payment_firstname'] = $order_info['payment_firstname'];
            $data['payment_lastname'] = $order_info['payment_lastname'];
            $data['payment_company'] = $order_info['payment_company'];
            $data['payment_address_1'] = $order_info['payment_address_1'];
            $data['payment_address_2'] = $order_info['payment_address_2'];
            $data['payment_city'] = $order_info['payment_city'];
            $data['payment_postcode'] = $order_info['payment_postcode'];
            $data['payment_country_id'] = $order_info['payment_country_id'];
            $data['payment_zone_id'] = $order_info['payment_zone_id'];
            $data['payment_custom_field'] = $order_info['payment_custom_field'];
            $data['payment_method'] = $order_info['payment_method'];
            $data['payment_code'] = $order_info['payment_code'];
            $data['payment_status'] = $order_info['payment_status'];

            $data['shipping_firstname'] = $order_info['shipping_firstname'];
            $data['shipping_lastname'] = $order_info['shipping_lastname'];
            $data['shipping_company'] = $order_info['shipping_company'];
            $data['shipping_address_1'] = $order_info['shipping_address_1'];
            $data['shipping_address_2'] = $order_info['shipping_address_2'];
            $data['shipping_city'] = $order_info['shipping_city'];
            $data['shipping_postcode'] = $order_info['shipping_postcode'];
            $data['shipping_country_id'] = $order_info['shipping_country_id'];
            $data['shipping_zone_id'] = $order_info['shipping_zone_id'];
            $data['shipping_custom_field'] = $order_info['shipping_custom_field'];
            $data['shipping_method'] = $order_info['shipping_method'];
            $data['shipping_method_value'] = $order_info['shipping_method_value'];
            $data['shipping_code'] = $order_info['shipping_code'];
            $data['shipping_fee'] = $order_info['shipping_fee'];
            $data['shipping_method'] = $order_info['shipping_method'];
            $data['shipping_province_code'] = $order_info['shipping_province_code'];
            $data['shipping_district_code'] = $order_info['shipping_district_code'];
            $data['shipping_ward_code'] = $order_info['shipping_ward_code'];
            $data['order_status_id'] = $order_info['order_status_id'];
            $data['previous_order_status_id'] = $order_info['previous_order_status_id'];
            $data['amount'] = $order_info['total'] - $order_info['shipping_fee'];
            $data['discount'] = isset($order_info['discount']) ? $order_info['discount'] : 0;
            $data['total'] = $order_info['total'];
            $data['collection_amount'] = $order_info['collection_amount'];
            $data['create_order_user_id'] = $order_info['user_id'];
            $data['date_added'] = convertDate($order_info['date_added']);
            $data['order_code'] = $order_info['order_code'];
            $data['transport_status'] = $order_info['transport_status'];
            $data['order_source'] = $order_info['source'];
            $data['manual_update_status'] = $order_info['manual_update_status'];

            $data['customer_info'] = $this->model_sale_order->getAddress($order_info['customer_id']);

            $this->load->model('customer/customer');
            $data['customer_info_only'] = $this->model_customer_customer->getCustomer($order_info['customer_id']);

            // Products
            $data['order_products'] = $this->model_sale_order->getInfoOrderProducts($this->request->get['order_id']);
            $data['order_weight'] = 0;
            $data['shopee_amount'] = 0;
            $data['total_into_money'] = 0;
            foreach ($data['order_products'] as $product) {
                $data['order_weight'] += $product['weight'] * $product['quantity'];
                $data['shopee_amount'] += (int)$product['quantity'] * (float)$product['price'];
                $data['total_into_money'] += (int)$product['quantity'] * (float)$product['price'];
            }

            //tag
            $this->load->model('custom/tag');
            $data['tags'] = $this->model_custom_tag->getTagsByOrderId($this->request->get['order_id']);

            // status
            $data['order_status_id'] = $order_info['order_status_id'];
            $data['changable_order_statuses'] = $CHANGEABLE_ORDER_STATUS[$data['order_status_id']];
            $data['editable_order_statuses'] = $EDITABLE_ORDER_STATUS[$data['order_status_id']];

            $data['order_status_id_name'] = $this->convertStatusOrder($order_info['order_status_id']);


            $data['comment'] = $order_info['comment'];
            $data['currency_code'] = $order_info['currency_code'];

            $this->load->model('localisation/vietnam_administrative');
            $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($data['shipping_province_code']);
            $data['shipping_province_name'] = $province['name'];
            $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($data['shipping_district_code']);
            $data['shipping_district_name'] = isset($district['name']) ? $district['name'] : '';
            $ward = $this->model_localisation_vietnam_administrative->getWardByCode($data['shipping_ward_code']);
            $data['shipping_ward_name'] = isset($ward['name']) ? $ward['name'] : '';

            $addrArr = array();
            if (trim($data['shipping_address_1']) != '') {
                $addrArr[] = $data['shipping_address_1'];
            }
            if (!empty($ward['name']) && trim($ward['name']) != '') {
                $addrArr[] = $ward['name_with_type'];
            }
            if (!empty($district['name']) && trim($district['name']) != '') {
                $addrArr[] = $district['name_with_type'];
            }
            if (trim($province['name']) != '') {
                $addrArr[] = $province['name_with_type'];
            }

            $data['full_shipping_address'] = implode(', ', $addrArr);
            // shopee
            if (isset($data['order_source']) && $data['order_source'] == 'shopee') {
                $data['full_shipping_address'] = $data['shipping_address_1'];
            }

            $data['source'] = $this->language->get($order_info['source']);


            // shopee
            if ($data['order_source'] == 'shopee') {
                $data['shopee_shop_name'] = $this->model_sale_order->getShopeeShopName($data['order_code']);
            }

            // lazada
            if ($data['order_source'] == 'lazada'){
                $data['shopee_shop_name'] = $this->model_sale_order->getLazadaShopName($data['order_code']);
            }
        }

        $this->load->model('user/user');
        $user = $this->model_user_user->getUser($data['create_order_user_id']);
        if ($user) {
            $data['create_order_user_name'] = $user['lastname'] . ' ' . $user['firstname'];
        }

        // order history
        if (isset($this->request->get['order_id'])) {
            $this->load->model('sale/order_history');
            $data['list_history'] = $this->model_sale_order_history->getHistory($this->request->get['order_id']);
        }

        /*$this->load->model('localisation/currency');

        $data['currencies'] = $this->model_localisation_currency->getCurrencies();

//        $data['voucher_min'] = $this->config->get('config_voucher_min');

//        $this->load->model('sale/voucher_theme');
//
//        $data['voucher_themes'] = $this->model_sale_voucher_theme->getVoucherThemes();

        // API login
        $data['catalog'] = $this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG;

        // API login
        $this->load->model('user/api');

        $api_info = $this->model_user_api->getApi($this->config->get('config_api_id'));

        if ($api_info && $this->user->hasPermission('modify', 'sale/order')) {
            $session = new Session($this->config->get('session_engine'), $this->registry);

            $session->start();

            $this->model_user_api->deleteApiSessionBySessonId($session->getId());

            $this->model_user_api->addApiSession($api_info['api_id'], $session->getId(), $this->request->server['REMOTE_ADDR']);

            $session->data['api_id'] = $api_info['api_id'];

            $data['api_token'] = $session->getId();
        } else {
            $data['api_token'] = '';
        }*/
        $list_status = $this->model_sale_order->getListStatus();
        $list_order_payment = $this->model_sale_order->getListPayment();
        $list_order_shipment = $this->model_sale_order->getListShipment();

        // province
        $this->load->model('localisation/vietnam_administrative');
        $data['provinces'] = $this->model_localisation_vietnam_administrative->getProvinces();

        // setting delivery methods
        $this->load->model('setting/setting');
        $delivery_methods = $this->model_setting_setting->getSettingValue('delivery_methods');
        $delivery_methods = json_decode($delivery_methods, true);
        $delivery_methods = is_array($delivery_methods) ? $delivery_methods : [];
        // get default delivery method (id = -1)
        $delivery_methods = array_filter($delivery_methods, function ($dm) {
            return -1 == $dm['id'] && '1' == $dm['status'];
        });
        $satisfied_delivery_methods = empty($order_info) ? [] : $this->getSatisfiedDeliveryMethods(
            isset($data['customer_info']['city']) ? $data['customer_info']['city'] : '',
            $data['order_weight'], $data['total']);
        $delivery_methods = array_merge($delivery_methods, $satisfied_delivery_methods);
        foreach ($delivery_methods as $k => $delivery_method) {
            $data['delivery_methods'][$k]['id'] = $delivery_method['id'];
            $data['delivery_methods'][$k]['name'] = $delivery_method['name'];
        }

        $payment_methods = $this->model_setting_setting->getSettingValue('payment_methods');
        $payment_methods = json_decode($payment_methods, true);
        $payment_methods = is_array($payment_methods) ? $payment_methods : [];
        $data['current_payment_method_name'] = '';
        // add vnpay payment method
        $this->load->model('appstore/my_app');
        if ($this->model_appstore_my_app->checkAppActive(self::APP_CODE_PAYMENT_VNPAY)) {
            $payment_methods[] = [
                'payment_id' => self::APP_CODE_PAYMENT_VNPAY,
                'name' => $this->language->get('text_vnpay_payment'),
                'guide' => '',
                'status' => 1
            ];
        } else if (isset($data['payment_method']) && $data['payment_method'] == self::APP_CODE_PAYMENT_VNPAY) {
            $data['current_payment_method_name'] = $this->language->get('text_vnpay_payment');
        }
        if (isset($data['order_source']) && ($data['order_source'] == 'shopee' || $data['order_source'] == 'lazada')){
            $data['current_payment_method_name'] = isset($data['payment_method']) ? $data['payment_method'] : '';
        }
        foreach ($payment_methods as $k => $payment_method) {
            if ($payment_method['status'] != '1') {
                continue;
            }
            if (isset($data['payment_method']) && $payment_method['payment_id'] == $data['payment_method']) {
                $data['current_payment_method_name'] = $payment_method['name'];
            }
            $data['payment_methods'][$k]['payment_id'] = $payment_method['payment_id'];
            $data['payment_methods'][$k]['name'] = $payment_method['name'];
        }
        if (isset($data['payment_method']) && $data['payment_method'] == self::APP_CODE_PAYMENT_VNPAY && $data['payment_status'] == 1) {
            $data['not_change_payment_status'] = true;
        }

        // error message
        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];
            unset($this->session->data['error']);
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        // delivery api
        /* check has at least one delivery (transport) app active */
        $delivery_api_active = false;
        // delivery app has app_name starts with *trans_*
        // get all apps from db app_name starts with *trans_*
        $filter_data = ['app_code_start_with' => 'trans_'];
        $trans_app_codes = $this->model_appstore_my_app->getInstalledCustom($filter_data);

        // check is active or not
        foreach ($trans_app_codes as $trans_app_code) {
            // individual check built in deliveries such as ghn, vtpost
            if ($trans_app_code == \Transport\Ghn::APP_CODE) {
                $delivery_api_active = !is_null($this->config->get('config_GHN_active'));
                if ($delivery_api_active) {
                    break;
                }
                continue;
            }

            if ($trans_app_code == \Transport\Viettel_post::APP_CODE) {
                $delivery_api_active = !is_null($this->config->get('config_VIETTEL_POST_active'));
                if ($delivery_api_active) {
                    break;
                }
                continue;
            }

            $trans_app_ins = new Transport($trans_app_code, $data, $this);
            if (!$trans_app_ins->getAdaptor() instanceof \Transport\Abstract_Transport) {
                continue;
            }

            if ($trans_app_ins->isConnected()) {
                $delivery_api_active = true;
                break;
            }
        }

        $data['can_modify'] = $this->user->hasPermission('modify', 'sale/order') ? 1 : 0;
        $data['delivery_api_active'] = $delivery_api_active;

        $data['address_shop'] = $this->config->get('config_address');
        $data['ghn_district_hub'] = $this->config->get('config_GHN_hub');
        $data['url_delivery_api_service_available'] = $this->url->link('common/delivery_api/getListServiceVariable', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['delivery_api_list_active'] = $this->config->get('config_transport_active');
        $data['url_delivery_create_order'] = $this->url->link('common/delivery_api/createOrderDelivery', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['url_delivery_cancel_order'] = $this->url->link('common/delivery_api/cancelOrderDelivery', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['new_tag_prefix'] = ModelSaleOrder::ORDER_TAG_NEW_PREFIX;
        $data['list_status'] = $list_status;
        $data['list_order_payment'] = $list_order_payment;
        $data['list_order_shipment'] = $list_order_shipment;
        $data['order_cancel_url'] = $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . $url, true);

        // show error connect delivery
        if (!empty($data['delivery_api_list_active'])) {
            $this->load->model('appstore/my_app');
            // TODO: debug + test. Remove if code works properly!...
            $transport_list = explode(',', $data['delivery_api_list_active']);
            foreach ($transport_list as $key => $transport_name) {
                $trans_app_code = $transport_name;
                if ($transport_name == 'Ghn') {
                    $trans_app_code = ModelAppstoreMyApp::APP_GHN;
                }

                if ($transport_name == 'viettel_post') {
                    $trans_app_code = ModelAppstoreMyApp::APP_VIETTEL_POST;
                }

                if (!$this->model_appstore_my_app->checkAppActive($trans_app_code)) {
                    unset($transport_list[$key]);
                };
            }

            $deliveryAppError = [];
            foreach ($transport_list as $delivery_name) {
                if (!in_array($delivery_name, ['Ghn', 'viettel_post'])) {
                    $transport = new Transport($delivery_name, $data, $this);
                    $result = $transport->checkStatusApp();

                    if (!$result['status']) {
                        array_push($deliveryAppError, $result['mes']);
                    }
                }
            }

            $data['delivery_error'] = $deliveryAppError;
        }



        // discount
        $discountCombineSetting = $this->model_setting_setting->getSettingValue('discount_combine');
        $data['check_discount_combine'] = $discountCombineSetting ? 1 : 0;

        $discountAutoSetting = $this->model_setting_setting->getSettingValue('discount_auto_apply');
        $data['check_discount_auto'] = $discountAutoSetting ? 1 : 0;

        $data['label_inactive_img'] = 'view/image/custom/icons/label_inactive.png';
        $data['label_active_img'] = 'view/image/custom/icons/discount.gif';
        $data['gift_inactive_img'] = 'view/image/custom/icons/gift.png';
        $data['gift_active_img'] = 'view/image/custom/icons/gift.gif';

        $data['list_product'] = [];
        if (isset($this->request->get['order_id'])) {
            $data['list_product'] = $this->getListProductForEditOrderForm($this->request->get['order_id']);
        }

        /*
         * build selected_discounts for edit form
         * [
         *     <$prod_and_ver_id> => [ <$discount_id>, ... ],
         *     "order" => [ <$discount_id>, ... ]
         * ]
         */
        $data['selected_discounts'] = [];
        if (isset($this->request->get['order_id'])) {
            // Get exist receipt_voucher
            $this->load->model('cash_flow/receipt_voucher');
            $receipt_voucher = $this->model_cash_flow_receipt_voucher->getReceiptVoucherByOrder($this->request->get['order_id']);
            if ($receipt_voucher) {
                $data['receipt_voucher_code'] = $receipt_voucher['receipt_voucher_code'];
                $data['link_receipt_voucher'] = $this->url->link('cash_flow/receipt_voucher/edit', 'user_token=' . $this->session->data['user_token'] . '&receipt_voucher_id=' . $receipt_voucher['receipt_voucher_id'], true);
            }
        }

        $data['list_categories'] = $this->model_catalog_category->getCategories();
        $data['list_manufactures'] = $this->model_catalog_manufacturer->getManufacturers();

        if (isset($this->request->get['order_id'])) {
            $order_voucher = $this->model_sale_order->getOrderVoucherByOrderId($this->request->get['order_id']);

            if (!empty($order_voucher)) {
                $data['discount_by_voucher_code'] = isset($order_voucher['amount']) ? (float)$order_voucher['amount'] : 0;
                $voucherData = $this->model_sale_order->getVoucherById($order_voucher['voucher_id']);

                $data['voucher_data'] = [
                    'code' => $voucherData['code'],
                    'discount' => (float)$voucherData['discount'],
                    'detail' => $this->url->link('discount/coupon/edit', 'user_token=' . $this->session->data['user_token'] . '&coupon_id=' . $voucherData['coupon_id'], true),
                    'apply_for' => isset(ModelSaleOrder::COUPON_APPLY_FOR[$voucherData['apply_for']]) ? ModelSaleOrder::COUPON_APPLY_FOR[$voucherData['apply_for']] : ''
                ];
            }
        }

        $data['order_utm'] = !empty($order_utm) ? $order_utm : [];

        // common layout
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('sale/order_form', $data));
    }

    public function manualUpdateStatus()
    {
        $this->load->model('sale/order');
        $order_id = $this->request->post['order_id'];
        $json = $this->model_sale_order->getManualUpdateStatusById($order_id);
        $this->responseJson($json);
    }

    public function getDeliveryMethodsHtml() {
        $result = [
            'html' => '',
            'is_checked' => false
        ];

        if (isset($this->request->get['shipping_method_value']) && !empty($this->request->get['shipping_method_value'])) {
            // setting delivery methods
            $this->load->model('setting/setting');
            $delivery_methods = $this->model_setting_setting->getSettingValue('delivery_methods');
            $delivery_methods = json_decode($delivery_methods, true);
            $delivery_methods = is_array($delivery_methods) ? $delivery_methods : [];
            // get default delivery method (id = -1)
            $delivery_methods = array_filter($delivery_methods, function ($dm) {
                return -1 == $dm['id'] && '1' == $dm['status'];
            });

            $order_weight = $this->request->get['order_weight'];
            $order_amount = $this->request->get['order_amount'];
            $satisfied_delivery_methods = $this->getSatisfiedDeliveryMethods($this->request->get['city'], $order_weight, $order_amount);
            $delivery_methods = array_merge($delivery_methods, $satisfied_delivery_methods);

            $shipping_method_value = $this->request->get['shipping_method_value'];

            foreach ($delivery_methods as $k => $delivery_method) {
                if($shipping_method_value == $delivery_method['id']) {
                    $result['is_checked'] = true;
                }
                $result['html'] .= '<div class="custom-control custom-radio form-group">
                        <input type="radio" id="delivery'. $k .'" '. ($shipping_method_value == $delivery_method['id'] ? 'checked' : '') .' data-method-name="'. $delivery_method['name'] .'" name="delivery" value="'. $delivery_method['id'] .'" class="transport_id custom-control-input">
                        <label class="custom-control-label font-weight-normal" for="delivery'. $k .'">'. $delivery_method['name'] .'</label>
                    </div>';
            }
        }

        $this->response->setOutput(json_encode($result));
    }

    public function info()
    {
        $this->load->model('sale/order');

        if (isset($this->request->get['order_id'])) {
            $order_id = $this->request->get['order_id'];
        } else {
            $order_id = 0;
        }

        $order_info = $this->model_sale_order->getOrder($order_id);

        if ($order_info) {
            $this->load->language('sale/order');

            $this->document->setTitle($this->language->get('heading_title'));

            $data['text_ip_add'] = sprintf($this->language->get('text_ip_add'), $this->request->server['REMOTE_ADDR']);
            $data['text_order'] = sprintf($this->language->get('text_order'), $this->request->get['order_id']);

            $url = '';

            if (isset($this->request->get['filter_order_id'])) {
                $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
            }

            if (isset($this->request->get['filter_customer'])) {
                $url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_order_status'])) {
                $url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
            }

            if (isset($this->request->get['filter_order_status_id'])) {
                $url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
            }

            if (isset($this->request->get['filter_total'])) {
                $url .= '&filter_total=' . $this->request->get['filter_total'];
            }

            if (isset($this->request->get['filter_date_added'])) {
                $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
            }

            if (isset($this->request->get['filter_date_modified'])) {
                $url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . $url, true)
            );

            $data['shipping'] = $this->url->link('sale/order/shipping', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . (int)$this->request->get['order_id'], true);
            $data['invoice'] = $this->url->link('sale/order/invoice', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . (int)$this->request->get['order_id'], true);
            $data['edit'] = $this->url->link('sale/order/edit', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . (int)$this->request->get['order_id'], true);
            $data['cancel'] = $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . $url, true);

            $data['user_token'] = $this->session->data['user_token'];

            $data['order_id'] = $this->request->get['order_id'];

            $data['store_id'] = $order_info['store_id'];
            $data['store_name'] = $order_info['store_name'];

            if ($order_info['store_id'] == 0) {
                $data['store_url'] = $this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG;
            } else {
                $data['store_url'] = $order_info['store_url'];
            }

            if ($order_info['invoice_no']) {
                $data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
            } else {
                $data['invoice_no'] = '';
            }

            $data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));

            $data['firstname'] = $order_info['firstname'];
            $data['lastname'] = $order_info['lastname'];

            if ($order_info['customer_id']) {
                $data['customer'] = $this->url->link('customer/customer/edit', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $order_info['customer_id'], true);
            } else {
                $data['customer'] = '';
            }

            $this->load->model('customer/customer_group');

            $customer_group_info = $this->model_customer_customer_group->getCustomerGroup($order_info['customer_group_id']);

            if ($customer_group_info) {
                $data['customer_group'] = $customer_group_info['name'];
            } else {
                $data['customer_group'] = '';
            }

            $data['email'] = $order_info['email'];
            $data['telephone'] = $order_info['telephone'];

            $data['shipping_method'] = $order_info['shipping_method'];
            $data['payment_method'] = $order_info['payment_method'];

            // Payment Address
            if ($order_info['payment_address_format']) {
                $format = $order_info['payment_address_format'];
            } else {
                $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
            }

            $find = array(
                '{firstname}',
                '{lastname}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{postcode}',
                '{zone}',
                '{zone_code}',
                '{country}'
            );

            $replace = array(
                'firstname' => $order_info['payment_firstname'],
                'lastname' => $order_info['payment_lastname'],
                'company' => $order_info['payment_company'],
                'address_1' => $order_info['payment_address_1'],
                'address_2' => $order_info['payment_address_2'],
                'city' => $order_info['payment_city'],
                'postcode' => $order_info['payment_postcode'],
                'zone' => $order_info['payment_zone'],
                'zone_code' => $order_info['payment_zone_code'],
                'country' => $order_info['payment_country']
            );

            $data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            // Shipping Address
            if ($order_info['shipping_address_format']) {
                $format = $order_info['shipping_address_format'];
            } else {
                $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
            }

            $find = array(
                '{firstname}',
                '{lastname}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{postcode}',
                '{zone}',
                '{zone_code}',
                '{country}'
            );

            $replace = array(
                'firstname' => $order_info['shipping_firstname'],
                'lastname' => $order_info['shipping_lastname'],
                'company' => $order_info['shipping_company'],
                'address_1' => $order_info['shipping_address_1'],
                'address_2' => $order_info['shipping_address_2'],
                'city' => $order_info['shipping_city'],
                'postcode' => $order_info['shipping_postcode'],
                'zone' => $order_info['shipping_zone'],
                'zone_code' => $order_info['shipping_zone_code'],
                'country' => $order_info['shipping_country']
            );

            $data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            // Uploaded files
            $this->load->model('tool/upload');

            $data['products'] = array();

            $products = $this->model_sale_order->getOrderProducts($this->request->get['order_id']);

            foreach ($products as $product) {
                $option_data = array();

                $options = $this->model_sale_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);

                foreach ($options as $option) {
                    if ($option['type'] != 'file') {
                        $option_data[] = array(
                            'name' => $option['name'],
                            'value' => $option['value'],
                            'type' => $option['type']
                        );
                    } else {
                        $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                        if ($upload_info) {
                            $option_data[] = array(
                                'name' => $option['name'],
                                'value' => $upload_info['name'],
                                'type' => $option['type'],
                                'href' => $this->url->link('tool/upload/download', 'user_token=' . $this->session->data['user_token'] . '&code=' . $upload_info['code'], true)
                            );
                        }
                    }
                }

                $data['products'][] = array(
                    'order_product_id' => $product['order_product_id'],
                    'product_id' => $product['product_id'],
                    'name' => $product['name'],
                    'model' => $product['model'],
                    'option' => $option_data,
                    'quantity' => $product['quantity'],
                    'price' => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                    'total' => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
                    'href' => $this->url->link('catalog/product/edit', 'user_token=' . $this->session->data['user_token'] . '&product_id=' . $product['product_id'], true)
                );
            }

            $data['vouchers'] = array();

            $vouchers = $this->model_sale_order->getOrderVouchers($this->request->get['order_id']);

            foreach ($vouchers as $voucher) {
                $data['vouchers'][] = array(
                    'description' => $voucher['description'],
                    'amount' => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
                    'href' => $this->url->link('sale/voucher/edit', 'user_token=' . $this->session->data['user_token'] . '&voucher_id=' . $voucher['voucher_id'], true)
                );
            }

            $data['totals'] = array();

            $totals = $this->model_sale_order->getOrderTotals($this->request->get['order_id']);

            foreach ($totals as $total) {
                $data['totals'][] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value'])
                );
            }

            $data['comment'] = nl2br($order_info['comment']);

            $this->load->model('customer/customer');

            $data['reward'] = $order_info['reward'];

            $data['reward_total'] = $this->model_customer_customer->getTotalCustomerRewardsByOrderId($this->request->get['order_id']);

            $data['affiliate_firstname'] = $order_info['affiliate_firstname'];
            $data['affiliate_lastname'] = $order_info['affiliate_lastname'];

            if ($order_info['affiliate_id']) {
                $data['affiliate'] = $this->url->link('customer/customer/edit', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $order_info['affiliate_id'], true);
            } else {
                $data['affiliate'] = '';
            }

            $data['commission'] = $this->currency->format($order_info['commission'], $order_info['currency_code'], $order_info['currency_value']);

            $this->load->model('customer/customer');

            $data['commission_total'] = $this->model_customer_customer->getTotalTransactionsByOrderId($this->request->get['order_id']);

            $this->load->model('localisation/order_status');

            $order_status_info = $this->model_localisation_order_status->getOrderStatus($order_info['order_status_id']);

            if ($order_status_info) {
                $data['order_status_id'] = $order_status_info['name'];
            } else {
                $data['order_status_id'] = '';
            }

            $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

            $data['order_status_id'] = $order_info['order_status_id'];

            $data['account_custom_field'] = $order_info['custom_field'];

            // Custom Fields
            $this->load->model('customer/custom_field');

            $data['account_custom_fields'] = array();

            $filter_data = array(
                'sort' => 'cf.sort_order',
                'order' => 'ASC'
            );

            $custom_fields = $this->model_customer_custom_field->getCustomFields($filter_data);

            foreach ($custom_fields as $custom_field) {
                if ($custom_field['location'] == 'account' && isset($order_info['custom_field'][$custom_field['custom_field_id']])) {
                    if ($custom_field['type'] == 'select' || $custom_field['type'] == 'radio') {
                        $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($order_info['custom_field'][$custom_field['custom_field_id']]);

                        if ($custom_field_value_info) {
                            $data['account_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $custom_field_value_info['name']
                            );
                        }
                    }

                    if ($custom_field['type'] == 'checkbox' && is_array($order_info['custom_field'][$custom_field['custom_field_id']])) {
                        foreach ($order_info['custom_field'][$custom_field['custom_field_id']] as $custom_field_value_id) {
                            $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($custom_field_value_id);

                            if ($custom_field_value_info) {
                                $data['account_custom_fields'][] = array(
                                    'name' => $custom_field['name'],
                                    'value' => $custom_field_value_info['name']
                                );
                            }
                        }
                    }

                    if ($custom_field['type'] == 'text' || $custom_field['type'] == 'textarea' || $custom_field['type'] == 'file' || $custom_field['type'] == 'date' || $custom_field['type'] == 'datetime' || $custom_field['type'] == 'time') {
                        $data['account_custom_fields'][] = array(
                            'name' => $custom_field['name'],
                            'value' => $order_info['custom_field'][$custom_field['custom_field_id']]
                        );
                    }

                    if ($custom_field['type'] == 'file') {
                        $upload_info = $this->model_tool_upload->getUploadByCode($order_info['custom_field'][$custom_field['custom_field_id']]);

                        if ($upload_info) {
                            $data['account_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $upload_info['name']
                            );
                        }
                    }
                }
            }

            // Custom fields
            $data['payment_custom_fields'] = array();

            foreach ($custom_fields as $custom_field) {
                if ($custom_field['location'] == 'address' && isset($order_info['payment_custom_field'][$custom_field['custom_field_id']])) {
                    if ($custom_field['type'] == 'select' || $custom_field['type'] == 'radio') {
                        $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($order_info['payment_custom_field'][$custom_field['custom_field_id']]);

                        if ($custom_field_value_info) {
                            $data['payment_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $custom_field_value_info['name'],
                                'sort_order' => $custom_field['sort_order']
                            );
                        }
                    }

                    if ($custom_field['type'] == 'checkbox' && is_array($order_info['payment_custom_field'][$custom_field['custom_field_id']])) {
                        foreach ($order_info['payment_custom_field'][$custom_field['custom_field_id']] as $custom_field_value_id) {
                            $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($custom_field_value_id);

                            if ($custom_field_value_info) {
                                $data['payment_custom_fields'][] = array(
                                    'name' => $custom_field['name'],
                                    'value' => $custom_field_value_info['name'],
                                    'sort_order' => $custom_field['sort_order']
                                );
                            }
                        }
                    }

                    if ($custom_field['type'] == 'text' || $custom_field['type'] == 'textarea' || $custom_field['type'] == 'file' || $custom_field['type'] == 'date' || $custom_field['type'] == 'datetime' || $custom_field['type'] == 'time') {
                        $data['payment_custom_fields'][] = array(
                            'name' => $custom_field['name'],
                            'value' => $order_info['payment_custom_field'][$custom_field['custom_field_id']],
                            'sort_order' => $custom_field['sort_order']
                        );
                    }

                    if ($custom_field['type'] == 'file') {
                        $upload_info = $this->model_tool_upload->getUploadByCode($order_info['payment_custom_field'][$custom_field['custom_field_id']]);

                        if ($upload_info) {
                            $data['payment_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $upload_info['name'],
                                'sort_order' => $custom_field['sort_order']
                            );
                        }
                    }
                }
            }

            // Shipping
            $data['shipping_custom_fields'] = array();

            foreach ($custom_fields as $custom_field) {
                if ($custom_field['location'] == 'address' && isset($order_info['shipping_custom_field'][$custom_field['custom_field_id']])) {
                    if ($custom_field['type'] == 'select' || $custom_field['type'] == 'radio') {
                        $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($order_info['shipping_custom_field'][$custom_field['custom_field_id']]);

                        if ($custom_field_value_info) {
                            $data['shipping_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $custom_field_value_info['name'],
                                'sort_order' => $custom_field['sort_order']
                            );
                        }
                    }

                    if ($custom_field['type'] == 'checkbox' && is_array($order_info['shipping_custom_field'][$custom_field['custom_field_id']])) {
                        foreach ($order_info['shipping_custom_field'][$custom_field['custom_field_id']] as $custom_field_value_id) {
                            $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($custom_field_value_id);

                            if ($custom_field_value_info) {
                                $data['shipping_custom_fields'][] = array(
                                    'name' => $custom_field['name'],
                                    'value' => $custom_field_value_info['name'],
                                    'sort_order' => $custom_field['sort_order']
                                );
                            }
                        }
                    }

                    if ($custom_field['type'] == 'text' || $custom_field['type'] == 'textarea' || $custom_field['type'] == 'file' || $custom_field['type'] == 'date' || $custom_field['type'] == 'datetime' || $custom_field['type'] == 'time') {
                        $data['shipping_custom_fields'][] = array(
                            'name' => $custom_field['name'],
                            'value' => $order_info['shipping_custom_field'][$custom_field['custom_field_id']],
                            'sort_order' => $custom_field['sort_order']
                        );
                    }

                    if ($custom_field['type'] == 'file') {
                        $upload_info = $this->model_tool_upload->getUploadByCode($order_info['shipping_custom_field'][$custom_field['custom_field_id']]);

                        if ($upload_info) {
                            $data['shipping_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $upload_info['name'],
                                'sort_order' => $custom_field['sort_order']
                            );
                        }
                    }
                }
            }

            $data['ip'] = $order_info['ip'];
            $data['forwarded_ip'] = $order_info['forwarded_ip'];
            $data['user_agent'] = $order_info['user_agent'];
            $data['accept_language'] = $order_info['accept_language'];

            // Additional Tabs
            $data['tabs'] = array();

            if ($this->user->hasPermission('access', 'extension/payment/' . $order_info['payment_code'])) {
                if (is_file(DIR_CATALOG . 'controller/extension/payment/' . $order_info['payment_code'] . '.php')) {
                    $content = $this->load->controller('extension/payment/' . $order_info['payment_code'] . '/order');
                } else {
                    $content = '';
                }

                if ($content) {
                    $this->load->language('extension/payment/' . $order_info['payment_code']);

                    $data['tabs'][] = array(
                        'code' => $order_info['payment_code'],
                        'title' => $this->language->get('heading_title'),
                        'content' => $content
                    );
                }
            }

            $this->load->model('setting/extension');

            $extensions = $this->model_setting_extension->getInstalled('fraud');

            foreach ($extensions as $extension) {
                if ($this->config->get('fraud_' . $extension . '_status')) {
                    $this->load->language('extension/fraud/' . $extension, 'extension');

                    $content = $this->load->controller('extension/fraud/' . $extension . '/order');

                    if ($content) {
                        $data['tabs'][] = array(
                            'code' => $extension,
                            'title' => $this->language->get('extension')->get('heading_title'),
                            'content' => $content
                        );
                    }
                }
            }

            // The URL we send API requests to
            $data['catalog'] = $this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG;

            // API login
            $this->load->model('user/api');

            $api_info = $this->model_user_api->getApi($this->config->get('config_api_id'));

            if ($api_info && $this->user->hasPermission('modify', 'sale/order')) {
                $session = new Session($this->config->get('session_engine'), $this->registry);

                $session->start();

                $this->model_user_api->deleteApiSessionBySessonId($session->getId());

                $this->model_user_api->addApiSession($api_info['api_id'], $session->getId(), $this->request->server['REMOTE_ADDR']);

                $session->data['api_id'] = $api_info['api_id'];

                $data['api_token'] = $session->getId();
            } else {
                $data['api_token'] = '';
            }

            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('sale/order_info', $data));
        } else {
            return new Action('error/not_found');
        }
    }

    public function getProductPrice()
    {
        $this->load->model('sale/order');
        $product_and_ver_id = $this->request->get['product_and_ver_id'];

        $product_id = explode('-', $product_and_ver_id)[0];
        $product_version_id = isset(explode('-', $product_and_ver_id)[1]) ? explode('-', $product_and_ver_id)[1] : null;

        $product_price = $this->model_sale_order->getProductPrice($product_id, $product_version_id);

        $data = [
            'price' => number_format($product_price['price'], 0, '', ','),
            'compare_price' => number_format($product_price['compare_price'], 0, '', ','),
        ];
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($data));
    }

    public function getProductsLazyLoad()
    {
        $json = array();

        if (!$this->user->hasPermission('access', 'catalog/product')) {
            $json['count'] = 0;
            $json['results'] = [];
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        
        $this->load->model('sale/order');
        $this->load->language('sale/order');
        $this->load->model('tool/image');
        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        // display 6 product in select config theme is many, or setup other config
        $filter_data = array(
            'limit' => 6,
            'start' => ($page - 1) * 6,
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        if(!$this->user->canAccessAll()){
            $filter_data['current_user_id'] = $this->user->getId();
        }

        $results = $this->model_sale_order->getProductPaginator($filter_data);
        foreach ($results as $key => $value) {
            $version_name = implode(' • ', explode(',', $value['version']));
            if ($version_name != '') {
                $version_name = $version_name;
            }

            if ($value['image'] != '') {
                $image = $this->model_tool_image->resize($value['image'], 100, 100);
            } else {
                $image = $this->model_tool_image->resize('no_image.png', 100, 100);
            }

            $json['results'][] = array(
                'id' => $value['id'],
                'product_id' => $value['product_id'],
                'product_version_id' => $value['product_version_id'],
                'version' => $version_name,
                'subname' => $version_name,
                'text' => $value['name'],
                'product_name' => $value['product_name'],
                'image' => $image,
                'weight' => number_format($value['weight'], 0, '', ''),
                'price' => number_format($value['price'], 0, '', ','),
                'compare_price' => number_format($value['compare_price'], 0, '', ','),
                'quantity' => $value['quantity'],
                'sale_on_out_of_stock' => $value['sale_on_out_of_stock'],
                'status' => $value['status'],
                'sku' => $value['sku']
            );
        }

        $count_tag = $this->model_sale_order->countProductAll($filter_data);
        $json['count'] = $count_tag;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getTagLazyLoad()
    {
        $json = array();
        $this->load->model('custom/tag');
        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        //display 6 product in select config theme is many, or setup other config
        $filter_data = array(
            'limit' => 6,
            'start' => ($page - 1) * 6,
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = $this->request->get['filter_name'];
        }
        $results = $this->model_custom_tag->getTagPaginator($filter_data);
        foreach ($results as $key => $value) {
            $json['results'][] = array(
                'text' => $value['value'],
                'id' => $value['tag_id']
            );
        }
        $count_tag = $this->model_custom_tag->countTag($filter_data);
        $json['count'] = $count_tag;
        //array_multisort($sort_order, SORT_ASC, $json);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getCustomerLazyLoad()
    {
        $json = array();

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $this->load->model('customer/customer');

        $filter_data = array(
            'order' => 'ASC',
            'start' => ($page - 1) * 6,
            'limit' => 6
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        if(!$this->user->canAccessAll()){
            $filter_data['current_user_id'] = $this->user->getId();
        }

        $results = $this->model_customer_customer->getCustomersForOrder($filter_data);

        foreach ($results as $result) {
            $name = strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'));
            $name = strlen($name) > 30 ? substr($name, 0, 30) . "..." : $name;
            $json['results'][] = array(
                'id' => $result['customer_id'],
                'text' => $name,
                'subname' => $result['telephone']
            );
        }
        $count_customer = $this->model_customer_customer->getTotalCustomersForOrder($filter_data);
        $json['count'] = $count_customer;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getCustomerInfoAjax()
    {
        $this->load->language('sale/order');
        $json = array(
            'success' => false,
            'message' => $this->language->get('error_customer_warning')
        );

        if (!isset($this->request->get['customer_id'])) {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        $customer_id = $this->request->get['customer_id'];
        $this->load->model('customer/customer');
        $result = $this->model_customer_customer->getCustomer($customer_id);
        if (!$result) {
            $json['message'] = $this->language->get('error_customer_info');
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        $json['success'] = true;
        $json['message'] = $this->language->get('success_order_customer_info');
        $json['data']['customer_id'] = $customer_id;
        $json['data']['first_name'] = $result['firstname'];
        $json['data']['last_name'] = $result['lastname'];
        $json['data']['email'] = $result['email'];
        $json['data']['telephone'] = $result['telephone'];
        $json['data']['customer_group_id'] = $result['customer_group_id'];
        $address_id = $result['address_id'];
        $address = $this->model_customer_customer->getAddress($address_id);
        if ($address) {
            $this->load->model('localisation/vietnam_administrative');
            $city = $this->model_localisation_vietnam_administrative->getProvinceByCode($address['city']);
            $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($address['district']);
            $wards = $this->model_localisation_vietnam_administrative->getWardByCode($address['wards']);
            $json['data']['address_id'] = $address['address_id'];
            $json['data']['first_name'] = $address['firstname'];
            $json['data']['last_name'] = $address['lastname'];
            $json['data']['address'] = $address['address'];
            $json['data']['city'] = isset($city['code']) ? $city['code'] : '';
            $json['data']['city_text'] = isset($city['name']) ? $city['name'] : '';
            $json['data']['district'] = isset($district['code']) ? $district['code'] : '';
            $json['data']['district_text'] = isset($district['name']) ? $district['name'] : '';
            $json['data']['wards'] = isset($wards['code']) ? $wards['code'] : '';
            $json['data']['wards_text'] = isset($wards['name']) ? $wards['name'] : '';
            $json['data']['telephone'] = $address['phone'];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function ajaxValidateEmailCustomer()
    {
        $this->response->addHeader('Content-Type: application/json');
        $this->load->language('sale/order');
        $json = [
            'status' => false,
            'message' => 'false',
        ];
        $status = true;
        if (!$this->request->post['email'] || !$this->request->post['phone']) {
            $this->response->setOutput(json_encode($json));
        }
        $this->load->model('sale/order');

        if (isset($this->request->post['email']) && $this->request->post['email'] != '') {
            $result = $this->model_sale_order->checkCustomerEmailExist($this->request->post['email']);
            if ($result) {
                $json['message'] = $this->language->get('error_email_exist');
                $status = false;
            }
        }

        $this->load->model('customer/customer');
        if ($this->model_customer_customer->getCustomerByTelephone($this->request->post['phone'])) {
            $json['message'] = $this->language->get('error_phone_exist');
            $status = false;
        }
        if ($status) {
            $json = [
                'status' => true,
                'message' => 'success',
            ];
        }
        $this->response->setOutput(json_encode($json));
    }

    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'sale/order')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function createInvoiceNo()
    {
        $this->load->language('sale/order');

        $json = array();

        if (!$this->user->hasPermission('modify', 'sale/order')) {
            $json['error'] = $this->language->get('error_permission');
        } elseif (isset($this->request->get['order_id'])) {
            if (isset($this->request->get['order_id'])) {
                $order_id = $this->request->get['order_id'];
            } else {
                $order_id = 0;
            }

            $this->load->model('sale/order');

            $invoice_no = $this->model_sale_order->createInvoiceNo($order_id);

            if ($invoice_no) {
                $json['invoice_no'] = $invoice_no;
            } else {
                $json['error'] = $this->language->get('error_action');
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function addReward()
    {
        $this->load->language('sale/order');

        $json = array();

        if (!$this->user->hasPermission('modify', 'sale/order')) {
            $json['error'] = $this->language->get('error_permission');
        } else {
            if (isset($this->request->get['order_id'])) {
                $order_id = $this->request->get['order_id'];
            } else {
                $order_id = 0;
            }

            $this->load->model('sale/order');

            $order_info = $this->model_sale_order->getOrder($order_id);

            if ($order_info && $order_info['customer_id'] && ($order_info['reward'] > 0)) {
                $this->load->model('customer/customer');

                $reward_total = $this->model_customer_customer->getTotalCustomerRewardsByOrderId($order_id);

                if (!$reward_total) {
                    $this->model_customer_customer->addReward($order_info['customer_id'], $this->language->get('text_order_id') . ' #' . $order_id, $order_info['reward'], $order_id);
                }
            }

            $json['success'] = $this->language->get('text_reward_added');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function removeReward()
    {
        $this->load->language('sale/order');

        $json = array();

        if (!$this->user->hasPermission('modify', 'sale/order')) {
            $json['error'] = $this->language->get('error_permission');
        } else {
            if (isset($this->request->get['order_id'])) {
                $order_id = $this->request->get['order_id'];
            } else {
                $order_id = 0;
            }

            $this->load->model('sale/order');

            $order_info = $this->model_sale_order->getOrder($order_id);

            if ($order_info) {
                $this->load->model('customer/customer');

                $this->model_customer_customer->deleteReward($order_id);
            }

            $json['success'] = $this->language->get('text_reward_removed');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function addCommission()
    {
        $this->load->language('sale/order');

        $json = array();

        if (!$this->user->hasPermission('modify', 'sale/order')) {
            $json['error'] = $this->language->get('error_permission');
        } else {
            if (isset($this->request->get['order_id'])) {
                $order_id = $this->request->get['order_id'];
            } else {
                $order_id = 0;
            }

            $this->load->model('sale/order');

            $order_info = $this->model_sale_order->getOrder($order_id);

            if ($order_info) {
                $this->load->model('customer/customer');

                $affiliate_total = $this->model_customer_customer->getTotalTransactionsByOrderId($order_id);

                if (!$affiliate_total) {
                    $this->model_customer_customer->addTransaction($order_info['affiliate_id'], $this->language->get('text_order_id') . ' #' . $order_id, $order_info['commission'], $order_id);
                }
            }

            $json['success'] = $this->language->get('text_commission_added');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function removeCommission()
    {
        $this->load->language('sale/order');

        $json = array();

        if (!$this->user->hasPermission('modify', 'sale/order')) {
            $json['error'] = $this->language->get('error_permission');
        } else {
            if (isset($this->request->get['order_id'])) {
                $order_id = $this->request->get['order_id'];
            } else {
                $order_id = 0;
            }

            $this->load->model('sale/order');

            $order_info = $this->model_sale_order->getOrder($order_id);

            if ($order_info) {
                $this->load->model('customer/customer');

                $this->model_customer_customer->deleteTransactionByOrderId($order_id);
            }

            $json['success'] = $this->language->get('text_commission_removed');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function history()
    {
        $this->load->language('sale/order');

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data['histories'] = array();

        $this->load->model('sale/order');

        $results = $this->model_sale_order->getOrderHistories($this->request->get['order_id'], ($page - 1) * 10, 10);

        foreach ($results as $result) {
            $data['histories'][] = array(
                'notify' => $result['notify'] ? $this->language->get('text_yes') : $this->language->get('text_no'),
                'status' => $result['status'],
                'comment' => nl2br($result['comment']),
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
            );
        }

        $history_total = $this->model_sale_order->getTotalOrderHistories($this->request->get['order_id']);

        $pagination = new Pagination();
        $pagination->total = $history_total;
        $pagination->page = $page;
        $pagination->limit = 10;
        $pagination->url = $this->url->link('sale/order/history', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $this->request->get['order_id'] . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'),
            ($history_total) ? (($page - 1) * 10) + 1 : 0,
            ((($page - 1) * 10) > ($history_total - 10)) ? $history_total : ((($page - 1) * 10) + 10),
            $history_total,
            ceil($history_total / 10)
        );

        $this->response->setOutput($this->load->view('sale/order_history', $data));
    }

    public function invoice()
    {
        $this->load->language('sale/order');
        $barcodeGenerator = new Picqer\Barcode\BarcodeGeneratorSVG();

        $data['title'] = $this->language->get('text_invoice');

        if ($this->request->server['HTTPS']) {
            $data['base'] = HTTPS_SERVER;
        } else {
            $data['base'] = HTTP_SERVER;
        }

        $data['direction'] = $this->language->get('direction');
        $data['lang'] = $this->language->get('code');

        $this->load->model('sale/order');

        $this->load->model('setting/setting');

        $this->load->model('customer/customer');

        $this->load->model('localisation/vietnam_administrative');

        $data['orders'] = array();

        $orders = array();

        if (isset($this->request->post['selected'])) {
            $orders = $this->request->post['selected'];
        } elseif (isset($this->request->get['order_id'])) {
            $orders[] = $this->request->get['order_id'];
        }

        foreach ($orders as $order_id) {
            $order_infos = $this->model_sale_order->getOrder_print($order_id);
            if ($order_infos) {
                foreach ($order_infos as $order_info) {
                    $store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);
                    if ($store_info) {
                        $store_address = $store_info['config_address'];
                        $store_email = $store_info['config_email'];
                        $store_telephone = $store_info['config_telephone'];
                        $store_fax = $store_info['config_fax'];
                        // $store_name = $store_info['config_name_display'];
                    } else {
                        $store_address = $this->config->get('config_address');
                        $store_email = $this->config->get('config_email');
                        $store_telephone = $this->config->get('config_telephone');
                        $store_fax = $this->config->get('config_fax');
                        // $store_name = $this->config->get('config_name_display');
                    }

                    if ($order_info['invoice_no']) {
                        $invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
                    } else {
                        $invoice_no = '';
                    }

                    if ($order_info['payment_address_format']) {
                        $format = $order_info['payment_address_format'];
                    } else {
                        $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
                    }

                    $find = array(
                        '{firstname}',
                        '{lastname}',
                        '{company}',
                        '{address_1}',
                        '{address_2}',
                        '{city}',
                        '{postcode}',
                        '{zone}',
                        '{zone_code}',
                        '{country}'
                    );

                    $replace = array(
                        'firstname' => $order_info['payment_firstname'],
                        'lastname' => $order_info['payment_lastname'],
                        'company' => $order_info['payment_company'],
                        'address_1' => $order_info['payment_address_1'],
                        'address_2' => $order_info['payment_address_2'],
                        'city' => $order_info['payment_city'],
                        'postcode' => $order_info['payment_postcode'],
                        'zone' => $order_info['payment_zone'],
                        'zone_code' => $order_info['payment_zone_code'],
                        'country' => $order_info['payment_country']
                    );

                    $payment_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

                    if ($order_info['shipping_address_format']) {
                        $format = $order_info['shipping_address_format'];
                    } else {
                        $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
                    }

                    $find = array(
                        '{firstname}',
                        '{lastname}',
                        '{company}',
                        '{address_1}',
                        '{address_2}',
                        '{city}',
                        '{postcode}',
                        '{zone}',
                        '{zone_code}',
                        '{country}'
                    );

                    $replace = array(
                        'firstname' => $order_info['shipping_firstname'],
                        'lastname' => $order_info['shipping_lastname'],
                        'company' => $order_info['shipping_company'],
                        'address_1' => $order_info['shipping_address_1'],
                        'address_2' => $order_info['shipping_address_2'],
                        'city' => $order_info['shipping_city'],
                        'postcode' => $order_info['shipping_postcode'],
                        'zone' => $order_info['shipping_zone'],
                        'zone_code' => $order_info['shipping_zone_code'],
                        'country' => $order_info['shipping_country']
                    );

                    $shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

                    $this->load->model('tool/upload');

                    $product_data = array();

                    $products = $this->model_sale_order->getOrderProductVersion($order_info['order_id']);

                    $shopee_total = 0;
                    foreach ($products as $product) {
                        $option_data = array();

                        $options = $this->model_sale_order->getOrderOptions($order_info['order_id'], $product['order_product_id']);

                        foreach ($options as $option) {
                            if ($option['type'] != 'file') {
                                $value = $option['value'];
                            } else {
                                $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                                if ($upload_info) {
                                    $value = $upload_info['name'];
                                } else {
                                    $value = '';
                                }
                            }

                            $option_data[] = array(
                                'name' => $option['name'],
                                'value' => $value
                            );
                        }
                        $shopee_total += (float)$product['total'];
                        $product_data[] = array(
                            'name' => $product['name'],
                            'option' => $option_data,
                            'quantity' => $product['quantity'],
                            'sku' => $product['sku_version'],
                            'version' => str_replace(',', ' / ', $product['version']),
                            'price' => $product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0),
                            'discount' => $product['discount'] + ($this->config->get('config_tax') ? $product['tax'] : 0),
                            'total' => $product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0)
                        );
                    }

                    $voucher_data = array();

                    $vouchers = $this->model_sale_order->getOrderVouchers($order_id);

                    foreach ($vouchers as $voucher) {
                        $voucher_data[] = array(
                            'description' => $voucher['description'],
                            'amount' => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
                        );
                    }

                    $total_data = array();

                    $totals = $this->model_sale_order->getOrderTotals($order_id);

                    foreach ($totals as $total) {
                        $total_data[] = array(
                            'title' => $total['title'],
                            'text' => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value'])
                        );
                    }

                    $customer = $this->model_customer_customer->getCustomer($order_info['customer_id']);
                    $customer_address = $this->model_customer_customer->getAddressByCustomerId($order_info['customer_id']);
                    $this->load->model('localisation/vietnam_administrative');
                    $province_shop = $this->model_localisation_vietnam_administrative->getProvinceByCode($this->config->get('config_city_id'));
                    $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($customer_address['city']);
                    $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($customer_address['district']);
                    $ward = $this->model_localisation_vietnam_administrative->getWardByCode($customer_address['wards']);
                    // order from domain
                    if (array_key_exists('from_domain', $order_info) && trim($order_info['from_domain']) != '') {
                        $from_domain = $order_info['from_domain'];
                    } else {
                        $setting_domains = $this->getSettingDomainValue();
                        if (array_key_exists('main_domain', $setting_domains)) {
                            $from_domain = is_string($setting_domains['main_domain']) ? $setting_domains['main_domain'] : '';
                        } else {
                            $from_domain = $_SERVER['SERVER_NAME'];
                        }
                    }

                    $total_shipping_custom = $order_info['total'] - $order_info['shipping_fee'];
                    if (isset($order_info['source']) && (($order_info['source'] == 'shopee') || ($order_info['source'] == 'lazada'))) {
                        $total_shipping_custom = $shopee_total;
                    }

                    if (isset($order_info['transport_name']) && !empty($order_info['transport_name'])) {
                        $transport = new Transport($order_info['transport_name'], $data, $this);
                        if ($transport->getAdaptor() instanceof \Transport\Abstract_Transport) {
                            $transport_display_name = $transport->getAdaptor()->getDisplayName();
                        } else {
                            $transport_display_name = '';
                        }
                    }

                    if (isset($this->request->get['order_id'])) {
                        $order_voucher = $this->model_sale_order->getOrderVoucherByOrderId($order_id);

                        if (!empty($order_voucher)) {
                            $data['discount_by_voucher_code'] = (float)$order_voucher['amount'];
                        }
                    }

                    $data['orders'][] = array(
                        'order_id' => $order_id,
                        'invoice_no' => $invoice_no,
                        'date_added' => date($this->language->get('date_format_short'), strtotime($order_info['date_added'])),
                        'store_name' => !empty($this->config->get('config_name_display')) ? $this->config->get('config_name_display') : $this->config->get('config_name'),
                        'store_url' => rtrim($order_info['store_url'], '/'),
                        'store_address' => nl2br($store_address) . (!empty($province_shop['name']) ? ', ' . $province_shop['name'] : ''),
                        'store_email' => $store_email,
                        'store_telephone' => $store_telephone,
                        'store_fax' => $store_fax,
                        'email' => $order_info['email'],
                        'telephone' => $order_info['telephone'],
                        'shipping_address' => $shipping_address,
                        'shipping_method' => $order_info['shipping_method'],
                        'payment_address' => $payment_address,
                        'payment_method' => $order_info['payment_method'],
                        'product' => $product_data,
                        'voucher' => $voucher_data,
                        'discount' => isset($order_info['discount']) ? $order_info['discount'] : 0,
                        'total' => $total_data,
                        'comment' => nl2br($order_info['comment']),
                        'order_code' => $order_info['order_code'],
                        'customer_full_name' => (isset($customer['lastname']) ? $customer['lastname'] : '') . ' ' . (isset($customer['firstname']) ? $customer['firstname'] : ''),
                        'customer_email' => (isset($customer['email']) ? $customer['email'] : ''),
                        'customer_telephone' => (isset($customer['telephone']) ? $customer['telephone'] : ''),
                        'order_full_name' => $order_info['fullname'],
                        'address_shipping_total' => convertAddressPrint($order_info['address_shipping_total']),
                        'shipping_free' => $order_info['shipping_fee'],
                        'total_shipping_custom' => $total_shipping_custom,
                        'shopee_discount' => $total_shipping_custom + $order_info['shipping_fee'] - $order_info['total'],
                        'total_custom' => $order_info['total'],
                        'customer_address_full' => $customer_address['address'] . (!empty($ward['name']) ? ', ' . $ward['name'] : '') . (!empty($district['name']) ? ', ' . $district['name'] : '') . (!empty($province['name']) ? ', ' . $province['name'] : ''),
                        'website' => $from_domain,
                        'source' => $order_info['source'],
                        'transport_name' => isset($transport_display_name) ? $transport_display_name : '',
                        'transport_order_code' => $order_info['transport_order_code'],
                        'bar_code' => $barcodeGenerator->getBarcode($order_info['transport_order_code'], $barcodeGenerator::TYPE_CODE_128),
                        'config_hotline' => $this->config->get('config_hotline'),
                        'config_image_qr_code' => $this->model_setting_setting->getSettingValue('config_image_qr_code'),
                        'discount_by_voucher_code' => isset($data['discount_by_voucher_code']) ? $data['discount_by_voucher_code'] : 0
                        //'store_name' => $store_name, duplicate. TODO: check why or remove?
                    );
                }
            }
        }

        $this->response->setOutput($this->load->view('sale/order_invoice', $data));
    }

    public function shipping()
    {
        $this->load->language('sale/order');

        $data['title'] = $this->language->get('text_shipping');

        if ($this->request->server['HTTPS']) {
            $data['base'] = HTTPS_SERVER;
        } else {
            $data['base'] = HTTP_SERVER;
        }

        $data['direction'] = $this->language->get('direction');
        $data['lang'] = $this->language->get('code');

        $this->load->model('sale/order');

        $this->load->model('catalog/product');

        $this->load->model('setting/setting');

        $data['orders'] = array();

        $orders = array();

        if (isset($this->request->post['selected'])) {
            $orders = $this->request->post['selected'];
        } elseif (isset($this->request->get['order_id'])) {
            $orders[] = $this->request->get['order_id'];
        }

        foreach ($orders as $order_id) {
            $order_info = $this->model_sale_order->getOrder($order_id);

            // Make sure there is a shipping method
            if ($order_info && $order_info['shipping_code']) {
                $store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

                if ($store_info) {
                    $store_address = $store_info['config_address'];
                    $store_email = $store_info['config_email'];
                    $store_telephone = $store_info['config_telephone'];
                } else {
                    $store_address = $this->config->get('config_address');
                    $store_email = $this->config->get('config_email');
                    $store_telephone = $this->config->get('config_telephone');
                }

                if ($order_info['invoice_no']) {
                    $invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
                } else {
                    $invoice_no = '';
                }

                if ($order_info['shipping_address_format']) {
                    $format = $order_info['shipping_address_format'];
                } else {
                    $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
                }

                $find = array(
                    '{firstname}',
                    '{lastname}',
                    '{company}',
                    '{address_1}',
                    '{address_2}',
                    '{city}',
                    '{postcode}',
                    '{zone}',
                    '{zone_code}',
                    '{country}'
                );

                $replace = array(
                    'firstname' => $order_info['shipping_firstname'],
                    'lastname' => $order_info['shipping_lastname'],
                    'company' => $order_info['shipping_company'],
                    'address_1' => $order_info['shipping_address_1'],
                    'address_2' => $order_info['shipping_address_2'],
                    'city' => $order_info['shipping_city'],
                    'postcode' => $order_info['shipping_postcode'],
                    'zone' => $order_info['shipping_zone'],
                    'zone_code' => $order_info['shipping_zone_code'],
                    'country' => $order_info['shipping_country']
                );

                $shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

                $this->load->model('tool/upload');

                $product_data = array();

                $products = $this->model_sale_order->getOrderProducts($order_id);

                foreach ($products as $product) {
                    $option_weight = '';

                    $product_info = $this->model_catalog_product->getProduct($product['product_id']);

                    if ($product_info) {
                        $option_data = array();

                        $options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);

                        foreach ($options as $option) {
                            if ($option['type'] != 'file') {
                                $value = $option['value'];
                            } else {
                                $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                                if ($upload_info) {
                                    $value = $upload_info['name'];
                                } else {
                                    $value = '';
                                }
                            }

                            $option_data[] = array(
                                'name' => $option['name'],
                                'value' => $value
                            );

                            $product_option_value_info = $this->model_catalog_product->getProductOptionValue($product['product_id'], $option['product_option_value_id']);

                            if ($product_option_value_info) {
                                if ($product_option_value_info['weight_prefix'] == '+') {
                                    $option_weight += $product_option_value_info['weight'];
                                } elseif ($product_option_value_info['weight_prefix'] == '-') {
                                    $option_weight -= $product_option_value_info['weight'];
                                }
                            }
                        }

                        $product_data[] = array(
                            'name' => $product_info['name'],
                            'model' => $product_info['model'],
                            'option' => $option_data,
                            'quantity' => $product['quantity'],
                            'location' => $product_info['location'],
                            'sku' => $product_info['sku'],
                            'upc' => $product_info['upc'],
                            'ean' => $product_info['ean'],
                            'jan' => $product_info['jan'],
                            'isbn' => $product_info['isbn'],
                            'mpn' => $product_info['mpn'],
                            'weight' => $this->weight->format(($product_info['weight'] + (float)$option_weight) * $product['quantity'], $product_info['weight_class_id'], $this->language->get('decimal_point'), $this->language->get('thousand_point'))
                        );
                    }
                }

                $data['orders'][] = array(
                    'order_id' => $order_id,
                    'invoice_no' => $invoice_no,
                    'date_added' => date($this->language->get('date_format_short'), strtotime($order_info['date_added'])),
                    'store_name' => $order_info['store_name'],
                    'store_url' => rtrim($order_info['store_url'], '/'),
                    'store_address' => nl2br($store_address),
                    'store_email' => $store_email,
                    'store_telephone' => $store_telephone,
                    'email' => $order_info['email'],
                    'telephone' => $order_info['telephone'],
                    'shipping_address' => $shipping_address,
                    'shipping_method' => $order_info['shipping_method'],
                    'product' => $product_data,
                    'comment' => nl2br($order_info['comment'])
                );
            }
        }

        $this->response->setOutput($this->load->view('sale/order_shipping', $data));
    }

    public function billOfLading()
    {
        $this->load->language('sale/order');
        $barcodeGenerator = new Picqer\Barcode\BarcodeGeneratorSVG();

        $data['title'] = $this->language->get('text_invoice');

        if ($this->request->server['HTTPS']) {
            $data['base'] = HTTPS_SERVER;
        } else {
            $data['base'] = HTTP_SERVER;
        }

        $data['direction'] = $this->language->get('direction');
        $data['lang'] = $this->language->get('code');

        $this->load->model('sale/order');

        $this->load->model('setting/setting');

        $this->load->model('customer/customer');

        $this->load->model('localisation/vietnam_administrative');

        $data['orders'] = array();

        $orders = array();

        if (isset($this->request->post['selected'])) {
            $orders = $this->request->post['selected'];
        } elseif (isset($this->request->get['order_id'])) {
            $orders[] = $this->request->get['order_id'];
        }

        foreach ($orders as $order_id) {
            $order_infos = $this->model_sale_order->getOrder_print($order_id);
            if ($order_infos) {
                foreach ($order_infos as $order_info) {
                    $store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);
                    if ($store_info) {
                        $store_address = $store_info['config_address'];
                        $store_email = $store_info['config_email'];
                        $store_telephone = $store_info['config_telephone'];
                        $store_fax = $store_info['config_fax'];
                        // $store_name = $store_info['config_name_display'];
                    } else {
                        $store_address = $this->config->get('config_address');
                        $store_email = $this->config->get('config_email');
                        $store_telephone = $this->config->get('config_telephone');
                        $store_fax = $this->config->get('config_fax');
                        // $store_name = $this->config->get('config_name_display');
                    }

                    if ($order_info['invoice_no']) {
                        $invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
                    } else {
                        $invoice_no = '';
                    }

                    if ($order_info['payment_address_format']) {
                        $format = $order_info['payment_address_format'];
                    } else {
                        $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
                    }

                    $find = array(
                        '{firstname}',
                        '{lastname}',
                        '{company}',
                        '{address_1}',
                        '{address_2}',
                        '{city}',
                        '{postcode}',
                        '{zone}',
                        '{zone_code}',
                        '{country}'
                    );

                    $replace = array(
                        'firstname' => $order_info['payment_firstname'],
                        'lastname' => $order_info['payment_lastname'],
                        'company' => $order_info['payment_company'],
                        'address_1' => $order_info['payment_address_1'],
                        'address_2' => $order_info['payment_address_2'],
                        'city' => $order_info['payment_city'],
                        'postcode' => $order_info['payment_postcode'],
                        'zone' => $order_info['payment_zone'],
                        'zone_code' => $order_info['payment_zone_code'],
                        'country' => $order_info['payment_country']
                    );

                    $payment_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

                    if ($order_info['shipping_address_format']) {
                        $format = $order_info['shipping_address_format'];
                    } else {
                        $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
                    }

                    $find = array(
                        '{firstname}',
                        '{lastname}',
                        '{company}',
                        '{address_1}',
                        '{address_2}',
                        '{city}',
                        '{postcode}',
                        '{zone}',
                        '{zone_code}',
                        '{country}'
                    );

                    $replace = array(
                        'firstname' => $order_info['shipping_firstname'],
                        'lastname' => $order_info['shipping_lastname'],
                        'company' => $order_info['shipping_company'],
                        'address_1' => $order_info['shipping_address_1'],
                        'address_2' => $order_info['shipping_address_2'],
                        'city' => $order_info['shipping_city'],
                        'postcode' => $order_info['shipping_postcode'],
                        'zone' => $order_info['shipping_zone'],
                        'zone_code' => $order_info['shipping_zone_code'],
                        'country' => $order_info['shipping_country']
                    );

                    $shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

                    $this->load->model('tool/upload');

                    $product_data = array();

                    $products = $this->model_sale_order->getOrderProductVersion($order_info['order_id']);

                    $shopee_total = 0;
                    foreach ($products as $product) {
                        $option_data = array();

                        $options = $this->model_sale_order->getOrderOptions($order_info['order_id'], $product['order_product_id']);

                        foreach ($options as $option) {
                            if ($option['type'] != 'file') {
                                $value = $option['value'];
                            } else {
                                $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                                if ($upload_info) {
                                    $value = $upload_info['name'];
                                } else {
                                    $value = '';
                                }
                            }

                            $option_data[] = array(
                                'name' => $option['name'],
                                'value' => $value
                            );
                        }
                        $shopee_total += (float)$product['total'];
                        $product_data[] = array(
                            'name' => $product['name'],
                            'option' => $option_data,
                            'quantity' => $product['quantity'],
                            'sku' => $product['sku_version'],
                            'version' => str_replace(',', ' / ', $product['version']),
                            'price' => $product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0),
                            'total' => $product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0)
                        );
                    }

                    $voucher_data = array();

                    $vouchers = $this->model_sale_order->getOrderVouchers($order_id);

                    foreach ($vouchers as $voucher) {
                        $voucher_data[] = array(
                            'description' => $voucher['description'],
                            'amount' => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
                        );
                    }

                    $total_data = array();

                    $totals = $this->model_sale_order->getOrderTotals($order_id);

                    foreach ($totals as $total) {
                        $total_data[] = array(
                            'title' => $total['title'],
                            'text' => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value'])
                        );
                    }

                    $customer = $this->model_customer_customer->getCustomer($order_info['customer_id']);
                    $customer_address = $this->model_customer_customer->getAddressByCustomerId($order_info['customer_id']);
                    $this->load->model('localisation/vietnam_administrative');
                    $province_shop = $this->model_localisation_vietnam_administrative->getProvinceByCode($this->config->get('config_city_id'));
                    $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($customer_address['city']);
                    $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($customer_address['district']);
                    $ward = $this->model_localisation_vietnam_administrative->getWardByCode($customer_address['wards']);
                    // order from domain
                    if (array_key_exists('from_domain', $order_info) && trim($order_info['from_domain']) != '') {
                        $from_domain = $order_info['from_domain'];
                    } else {
                        $setting_domains = $this->getSettingDomainValue();
                        if (array_key_exists('main_domain', $setting_domains)) {
                            $from_domain = is_string($setting_domains['main_domain']) ? $setting_domains['main_domain'] : '';
                        } else {
                            $from_domain = $_SERVER['SERVER_NAME'];
                        }
                    }

                    $total_shipping_custom = $order_info['total'] - $order_info['shipping_fee'];
                    if (isset($order_info['source']) && (($order_info['source'] == 'shopee') || ($order_info['source'] == 'lazada'))) {
                        $total_shipping_custom = $shopee_total;
                    }

                    if (isset($order_info['transport_name']) && !empty($order_info['transport_name'])) {
                        $transport = new Transport($order_info['transport_name'], $data, $this);
                        if ($transport->getAdaptor() instanceof \Transport\Abstract_Transport) {
                            $transport_display_name = $transport->getAdaptor()->getDisplayName();
                        } else {
                            $transport_display_name = '';
                        }
                    }

                    // todo: get logo of shop, now not use
                    // $this->load->model('extension/module/theme_builder_config');
                    // $this->request->get['key'] = ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HEADER;
                    // $config_header = $this->load->controller('extension/module/theme_builder_config/' . 'config');

                    // $arr_config_header = is_array($config_header) ? $config_header : json_decode($config_header, true);
                    // $logo_shop = isset($arr_config_header['logo']['url']) ? $arr_config_header['logo']['url'] : '';

                    $data['orders'][] = array(
                        'order_id' => $order_id,
                        'invoice_no' => $invoice_no,
                        'date_added' => date($this->language->get('date_format_short'), strtotime($order_info['date_added'])),
                        'store_name' => !empty($this->config->get('config_name_display')) ? $this->config->get('config_name_display') : $this->config->get('config_name'),
                        'store_url' => rtrim($order_info['store_url'], '/'),
                        'store_address' => nl2br($store_address) . (!empty($province_shop['name']) ? ', ' . $province_shop['name'] : ''),
                        'store_email' => $store_email,
                        'store_telephone' => $store_telephone,
                        'store_fax' => $store_fax,
                        'email' => $order_info['email'],
                        'telephone' => $order_info['telephone'],
                        'shipping_address' => $shipping_address,
                        'shipping_method' => $order_info['shipping_method'],
                        'payment_address' => $payment_address,
                        'payment_method' => $order_info['payment_method'],
                        'product' => $product_data,
                        'voucher' => $voucher_data,
                        'discount' => isset($order_info['discount']) ? $order_info['discount'] : 0,
                        'total' => $total_data,
                        'comment' => nl2br($order_info['comment']),
                        'order_code' => $order_info['order_code'],
                        'customer_full_name' => (isset($customer['lastname']) ? $customer['lastname'] : '') . ' ' . (isset($customer['firstname']) ? $customer['firstname'] : ''),
                        'customer_email' => (isset($customer['email']) ? $customer['email'] : ''),
                        'customer_telephone' => (isset($customer['telephone']) ? $customer['telephone'] : ''),
                        'order_full_name' => $order_info['fullname'],
                        'address_shipping_total' => convertAddressPrint($order_info['address_shipping_total']),
                        'shipping_free' => $order_info['shipping_fee'],
                        'total_shipping_custom' => $total_shipping_custom,
                        'shopee_discount' => $total_shipping_custom + $order_info['shipping_fee'] - $order_info['total'],
                        'total_custom' => $order_info['total'],
                        'customer_address_full' => $customer_address['address'] . (!empty($ward['name']) ? ', ' . $ward['name'] : '') . (!empty($district['name']) ? ', ' . $district['name'] : '') . (!empty($province['name']) ? ', ' . $province['name'] : ''),
                        'website' => $from_domain,
                        'source' => $order_info['source'],
                        'transport_name' => isset($transport_display_name) ? $transport_display_name : '',
                        'transport_order_code' => $order_info['transport_order_code'],
                        'bar_code' => $barcodeGenerator->getBarcode($order_info['transport_order_code'], $barcodeGenerator::TYPE_CODE_128),
                        'collection_amount' => (isset($order_info['transport_name']) && !empty($order_info['transport_name'])) ? number_format((float)$order_info['collection_amount']) : '',
                        'config_hotline' => $this->config->get('config_hotline')
                    );
                }
            }
        }

        //var_dump($data);die;
        $this->response->setOutput($this->load->view('sale/order_bill_of_lading', $data));
    }

    public function validateCustom()
    {
        // $this->load->language('api/customer');
        $this->response->addHeader('Content-Type: application/json');
        $json = array();
        $data = $this->request->post;
        // var_dump($data);die;
        //validate du lieu: fullname, firstname, lastname, phone, email
        if ($errorFullname = $this->validateFullname($data)) {
            $json['error']['fullname'] = $errorFullname;
        }

        if ($errorFirstname = $this->validateFirstname($data)) {
            $json['error']['firstname'] = $errorFirstname;
        }

        if ($errorLastname = $this->validateLastname($data)) {
            $json['error']['lastname'] = $errorLastname;
        }

        if ($errorEmail = $this->validateEmail($data)) {
            $json['error']['email'] = $errorEmail;
        }
        if ($errorPhone = $this->validatePhone($data)) {
            $json['error']['telephone'] = $errorPhone;
        }
        $this->response->setOutput(json_encode($json));
        return;
    }

    public function validateFullname($data)
    {
        $fullname = utf8_strlen(trim($data['fullname']));
        if ($fullname < 1) {
            $result = $this->language->get('error_fullname');
            return $result;
        }
        return null;
    }

    public function validateFirstname($data)
    {
        if ((utf8_strlen(trim($data['firstname'])) < 1) || (utf8_strlen(trim($data['firstname'])) > 32)) {
            $result = $this->language->get('error_firstname');
            return $result;
        }

        return null;
    }

    public function validateLastname($data)
    {
        if ((utf8_strlen(trim($data['lastname'])) < 1) || (utf8_strlen(trim($data['lastname'])) > 32)) {
            $result = $this->language->get('error_lastname');
            return $result;
        }

        return null;
    }

    public function validateEmail($data)
    {
        if ((utf8_strlen($this->request->post['email']) > 96) || (!filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL))) {
            $result = $this->language->get('error_email');
            return $result;
        }

        return null;
    }

    public function validatePhone($data)
    {
        if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
            $json['error']['telephone'] = $this->language->get('error_telephone');
        }
    }

    public function addCustom()
    {
        $url = '';
        $this->load->language('sale/order');
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $data = $this->request->post;
            // validate data product add order
            if (!$this->validateProductOrder($data)) {
                $this->response->redirect($this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . $url, true));
            }
            // var_dump($data);die;
            $this->load->model('sale/order');
            //lưu vào bảng order
            $orderId = $this->model_sale_order->addOrderCustom($data);
            //save order_product table
            $this->model_sale_order->addOrderProduct($data, $orderId);
            $this->response->redirect($this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }
    }

    public function getEditForm()
    {
        $data['text_form'] = !isset($this->request->get['order_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

        $url = '';
        $orderId = $this->getValueFromRequest('get', 'order_id');
        $order = $this->model_sale_order->getOrderById($orderId);

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );
        $data['cancel'] = $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['user_token'] = $this->session->data['user_token'];

        $order_info = $this->model_sale_order->getOrder($orderId);
        $data['order_detail'] = $order;
        $data['list_product'] = $this->getListProductForEditOrderForm($orderId);
        $data['list_categories'] = $this->model_catalog_category->getCategories();
        $data['list_manufactures'] = $this->model_catalog_manufacturer->getManufacturers();
        $data['order_id'] = $orderId;
        $data['store_id'] = $order_info['store_id'];
        $data['store_url'] = $this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG;

        $data['customer'] = $order_info['customer'];
        $data['customer_id'] = $order_info['customer_id'];
        $data['customer_group_id'] = $order_info['customer_group_id'];
        $data['firstname'] = $order_info['firstname'];
        $data['lastname'] = $order_info['lastname'];
        $data['email'] = $order_info['email'];
        $data['telephone'] = $order_info['telephone'];

        $this->load->model('customer/customer');

        // Products
        $data['order_products'] = array();

        $products = $this->model_sale_order->getOrderProducts($orderId);

        foreach ($products as $product) {
            $data['order_products'][] = array(
                'product_id' => $product['product_id'],
                'name' => $product['name'],
                'model' => $product['model'],
                'option' => $this->model_sale_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']),
                'quantity' => $product['quantity'],
                'price' => round($product['price']),
                'discount' => $product['discount'],
                'total' => $product['total'],
                'reward' => $product['reward']
            );
        }
        // API login
        $data['catalog'] = $this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG;

        // API login
        $this->load->model('user/api');

        $api_info = $this->model_user_api->getApi($this->config->get('config_api_id'));

        if ($api_info && $this->user->hasPermission('modify', 'sale/order')) {
            $session = new Session($this->config->get('session_engine'), $this->registry);

            $session->start();

            $this->model_user_api->deleteApiSessionBySessonId($session->getId());

            $this->model_user_api->addApiSession($api_info['api_id'], $session->getId(), $this->request->server['REMOTE_ADDR']);

            $session->data['api_id'] = $api_info['api_id'];

            $data['api_token'] = $session->getId();
        } else {
            $data['api_token'] = '';
        }

        $list_status = $this->model_sale_order->getListStatus();
        $list_status_except_draft = $this->model_sale_order->getListStatusExceptDraft();

        $list_order_payment = $this->model_sale_order->getListPayment();
        $list_order_shipment = $this->model_sale_order->getListShipment();

        if ($order['order_status_id'] == 1) {
            $data['list_status'] = $list_status;
        } else {
            $data['list_status'] = $list_status_except_draft;
        }

        $data['is_enable_edit'] = ($order['order_status_id'] == 6) ? 1 : 0;
        $data['can_modify'] = $this->user->hasPermission('modify', 'sale/order') ? 1 : 0;

        $data['list_order_payment'] = $list_order_payment;
        $data['list_order_shipment'] = $list_order_shipment;

        $data['order_cancel_url'] = $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['order_submit_url'] = $this->url->link('sale/order/editCustom', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $orderId . $url, true);

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('sale/order_form_edit', $data));
    }

    private function validateForm($data, $order_id = null)
    {
        if (!$this->user->hasPermission('modify', 'sale/order')) {
            $this->session->data['error'] = $this->language->get('error_permission');
            return false;
        }

        $this->load->language('sale/order');

        /* validate product out of stock on action save draft or save apply on creating new order */
        $ACTIONS_NEED_VALIDATE_OUT_OF_STOCK = [
            'apply',
            'draft',
            'save-draft',
        ];
        if (isset($data['submit_action']) && in_array($data['submit_action'], $ACTIONS_NEED_VALIDATE_OUT_OF_STOCK)) {
            if (!isset($data['products'])) {
                $this->session->data['error'] = $this->language->get('order_product_data_not_empty');
                return false;
            }

            $this->load->model('catalog/product');
            foreach ($data['products'] as $key => $product_id) {
                $pvid = explode('-', $product_id);
                $product_id = $pvid[0];
                $product_version_id = isset($pvid[1]) ? $pvid[1] : 0;

                $productDetail = $this->model_catalog_product->getProductForOrder($product_id, $product_version_id);

                if (!$productDetail) {
                    $this->session->data['error'] = $this->language->get('product_not_empty');
                    return false;
                }

                $check_quantity = $productDetail['store_quantity'];
                if ($order_id) {
                    $new_order_quantity = isset($data['quantities'][$key]) ? (int)$data['quantities'][$key] : 0;
                    $check_quantity += $this->model_catalog_product->getProductQuantityForOrder($order_id, $product_id, $product_version_id);
                    $check_quantity -= $new_order_quantity;
                    if ($productDetail['p_status'] == 0 || ($check_quantity < 0 && $productDetail['sale_on_out_of_stock'] == 0)) {
                        $this->session->data['error'] = $this->language->get('lang_out_of_stock');
                        return false;
                    }
                } else {
                    if ($productDetail['p_status'] == 0 || ($check_quantity <= 0 && $productDetail['sale_on_out_of_stock'] == 0)) {
                        $this->session->data['error'] = $this->language->get('lang_out_of_stock');
                        return false;
                    }
                }
            }
        }

        /* validate product on change status from draft to processing */
        if (isset($data['old_order_status_id']) &&
            isset($data['order_status']) &&
            $data['old_order_status_id'] == ModelSaleOrder::ORDER_STATUS_ID_DRAFT &&
            $data['order_status'] == ModelSaleOrder::ORDER_STATUS_ID_PROCESSING
        ) {
            if (!isset($data['products'])) {
                $this->session->data['error'] = $this->language->get('order_product_data_not_empty');
                return false;
            }

            $this->load->model('catalog/product');
            foreach ($data['products'] as $product_id) {
                if (!$this->model_catalog_product->getProduct($product_id)) {
                    $this->session->data['error'] = $this->language->get('product_not_empty');
                    return false;
                }
            }
        }

        /* validate product out of stock on editing order */
        $STATUSES_NEED_VALIDATE_OUT_OF_STOCK_ON_EDITING = [
            ModelSaleOrder::ORDER_STATUS_ID_DRAFT,
            ModelSaleOrder::ORDER_STATUS_ID_PROCESSING
        ];
        if (isset($data['old_order_status_id']) &&
            isset($data['order_status']) &&
            in_array($data['old_order_status_id'], $STATUSES_NEED_VALIDATE_OUT_OF_STOCK_ON_EDITING)
        ) {
            if (!isset($data['products'])) {
                $this->session->data['error'] = $this->language->get('order_product_data_not_empty');

                return false;
            }

            // DUPLICATED code. TODO: extract function...
            $this->load->model('catalog/product');
            foreach ($data['products'] as $key => $product_id) {
                $pvid = explode('-', $product_id);
                $product_id = $pvid[0];
                $product_version_id = isset($pvid[1]) ? $pvid[1] : 0;

                $productDetail = $this->model_catalog_product->getProductForOrder($product_id, $product_version_id);

//                if (!$productDetail) {
//                    $this->session->data['error'] = $this->language->get('product_not_empty');
//                    return false;
//                }

//                $check_quantity = $productDetail['store_quantity'];
//                if ($order_id) {
//                    $new_order_quantity = isset($data['quantities'][$key]) ? (int)$data['quantities'][$key] : 0;
//                    $check_quantity += $this->model_catalog_product->getProductQuantityForOrder($order_id, $product_id, $product_version_id);
//                    $check_quantity -= $new_order_quantity;
//                    if ($productDetail['p_status'] == 0 || ($check_quantity < 0 && $productDetail['sale_on_out_of_stock'] == 0)) {
//                        $this->session->data['error'] = $this->language->get('lang_out_of_stock');
//                        return false;
//                    }
//                } else {
//                    if ($productDetail['p_status'] == 0 || ($check_quantity <= 0 && $productDetail['sale_on_out_of_stock'] == 0)) {
//                        $this->session->data['error'] = $this->language->get('lang_out_of_stock');
//                        return false;
//                    }
//                }
            }
            // End - DUPLICATED code
        }

        return true;
    }

    private function validateProductOrder($data)
    {
        if (!isset($data['product-choose']) || empty($data['product-choose'])) {
            $this->error['warning'] = $this->language->get('error_not_add_product');
            return false;
        }
        if (!isset($data['product_quantity']) || in_array(0, $data['product_quantity'])) {
            $this->error['warning'] = $this->language->get('error_not_add_product_min');
            return false;
        }
        return true;
    }

    public function addTags()
    {
        $this->load->model('custom/tag');
        $this->load->language('sale/order');

        $order_ids = $this->request->post['order-id-select'];
        $tag_list = $this->request->post['tag_list'];
        if (!$tag_list) {
            $this->session->data['error'] = $this->language->get('notice_add_tag_error');
            $this->response->redirect($this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], true));
        }
        $order_ids = str_replace(array('[', ']', '"'), array('', '', ''), $order_ids);
        $order_ids = explode(',', $order_ids);
        //add new tags return id
        try {
            $tag_adds = $this->model_custom_tag->insertTag($tag_list);
            foreach ($tag_list as $key => $tag) {
                if (in_array($tag, $tag_adds)) {
                    $tag_list[$key] = (string)array_search($tag, $tag_adds);
                }
            }

            $this->model_custom_tag->insertTagOrder($order_ids, $tag_list);
            $this->session->data['success'] = $this->language->get('notice_add_tag_success');
            $this->response->redirect($this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], true));
        } catch (Exception $e) {
            $this->session->data['error'] = $e->getMessage();
            $this->response->redirect($this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], true));
        }
    }

    public function removeTags()
    {
        $this->load->model('custom/tag');
        $this->load->language('sale/order');

        $order_ids = $this->request->post['order-id-select'];
        $tag_list = $this->request->post['tag_list'];
        if (!$tag_list) {
            $this->session->data['error'] = $this->language->get('notice_add_tag_error');
            $this->response->redirect($this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], true));
        }
        $order_ids = str_replace(array('[', ']', '"'), array('', '', ''), $order_ids);
        $order_ids = explode(',', $order_ids);
        try {
            $this->model_custom_tag->deleteTagOrder($order_ids, $tag_list);
        } catch (Exception $e) {
            $this->session->data['error'] = $e->getMessage();
            $this->response->redirect($this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->session->data['success'] = $this->language->get('notice_remove_tag_success');
        $this->response->redirect($this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function export()
    {
        $this->load->language('catalog/product');

        $this->response->addheader('Pragma: public');
        $this->response->addheader('Expires: 0');
        $this->response->addheader('Content-Description: File Transfer');
        $this->response->addheader('Content-Type: application/octet-stream');
        $this->response->addheader('Content-Disposition: attachment; filename="' . 'product' . '_' . date('Y-m-d_H-i-s', time()) . '.xls"');
        $this->response->addheader('Content-Transfer-Encoding: binary');

        $this->load->model('sale/order');

        $this->response->setOutput($this->model_sale_order->export());
    }

    public function getOrderJson()
    {
        $this->load->model('sale/order');
        $this->load->model('localisation/vietnam_administrative');
        $json = [];
        if ($this->request->post['order_id']) {
            $result = $this->model_sale_order->getOrder($this->request->post['order_id']);
            $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($result['shipping_province_code']);
            $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($result['shipping_district_code']);
            $ward = $this->model_localisation_vietnam_administrative->getWardByCode($result['shipping_ward_code']);
            $json['customer_name'] = $result['fullname'];
            $json['phone'] = $result['telephone'];
            $json['address'] = $result['shipping_address_1'];
            $json['province'] = [
                'code' => $result['shipping_province_code'],
                'name' => isset($province['name']) ? $province['name'] : '',
            ];
            $json['district'] = [
                'code' => $result['shipping_district_code'],
                'name' => isset($district['name']) ? $district['name'] : '',
            ];
            $json['ward'] = [
                'code' => $result['shipping_ward_code'],
                'name' => isset($ward['name']) ? $ward['name'] : '',
            ];
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getTransportFee($transport = '', $city = '', $mass = '', $total_order = '')
    {

        $this->load->model('localisation/vietnam_administrative');
        $transport = $this->request->get['transport_id']; // ID Phuong thuc van chuyen
        $city_code = $this->request->get['city'];         // Van chuyen den thanh pho (VD: An Giang)
        $cityArr = $this->model_localisation_vietnam_administrative->getProvinceByCode($city_code);
        $city = '';
        if (isset($cityArr['name'])) {
            $city = $cityArr['name'];
        }
        $mass = $this->request->get['mass'];               // Khoi luong san pham
        $total_order = $this->request->get['totalamount']; // Tong gia tri don hang

        $this->load->model('sale/order');
        $this->load->model('setting/setting');
        $this->load->language('sale/order');
        $delivery_methods = $this->model_setting_setting->getSettingValue('delivery_methods');
        $delivery_methods = json_decode($delivery_methods, true);

        $delivery_methods = is_array($delivery_methods) ? $delivery_methods : [];
        $value_fee_cod = 0;
        $fee_price = 0;
        $name_transport = '';
        foreach ($delivery_methods as $key => $vl) {
            $default_fee_region = isset($vl['fee_region']) ? $vl['fee_region'] : 0;

            if ($transport == $vl['id']) {
                $name_transport = $vl['name'];
                if (isset($vl['fee_regions'])) {
                    foreach ($vl['fee_regions'] as $value) {
                        if (in_array(trim($city), $value['provinces'])) {
                            if (isset($value['steps']) && $value['steps'] == 'price') {
                                foreach ($value['price_steps'] as $vl_price) {
                                    if (convertWeight($vl_price['from']) <= $total_order && convertWeight($vl_price['to']) >= $total_order) {
                                        $fee_price = str_replace(',', '', $vl_price['price']); // Gia thuoc tinh thanh
                                        break;
                                    } else {
                                        $fee_price = str_replace(',', '', $default_fee_region);  // Gia mac dinh khi khong thuoc tinh thanh
                                    }
                                }
                            } else {
                                foreach ($value['weight_steps'] as $vl_weight) {
                                    if (convertWeight($vl_weight['from']) <= $mass && convertWeight($vl_weight['to']) >= $mass) {
                                        $fee_price = str_replace(',', '', $vl_weight['price']); // Gia thuoc tinh thanh
                                        break;
                                    } else {
                                        $fee_price = str_replace(',', '', $default_fee_region);  // Gia mac dinh khi khong thuoc tinh thanh
                                    }
                                }
                            }

                            break;
                        } else {
                            $fee_price = str_replace(',', '', $default_fee_region);  // Gia mac dinh khi khong thuoc tinh thanh
                        }
                    }
                    foreach ($vl['fee_cod'] as $key_cod => $fee_cod) {
                        $arr_fee_status = ($vl['fee_cod'][$key_cod]);
                        if ($arr_fee_status['status'] == 1) {
                            $arr_fee_status = str_replace(',', '', $arr_fee_status['value']);
                            if ($key_cod === 'dynamic') {
                                if ($total_order != 0) {
                                    $value_fee_cod = ($arr_fee_status / 100) * $total_order;
                                } else {
                                    $value_fee_cod = 0;
                                }
                            } elseif ($key_cod === 'fixed') {
                                $value_fee_cod = $arr_fee_status;
                            }
                        }
                    }
                } else {
                    $name_transport = $vl['name'];
                    $value_fee_cod = str_replace(',', '', $vl['fee_region']);
                }
            }
        }

        $json['fee_order'] = [
            'name' => $name_transport,
            'fee' => $fee_price + $value_fee_cod,
            'error' => '',
        ];
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function updateStatus()
    {
        $this->load->language('sale/order');

        $this->load->model('sale/order');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateFormUpdateStatus()) {
            $order = $this->model_sale_order->getOrder($this->request->post['order_id']);
            if (isset($order['order_status_id'])) {
                if ($this->request->post['order_status_id'] == 9) {

                    $orderDetail = $this->model_sale_order->getOrderById($this->request->post['order_id']);

                    if($orderDetail['payment_status'] != 1){
                        $customer_info = json_encode(['id' => isset($orderDetail['customer_id']) ? $orderDetail['customer_id'] : null ,
                            'name' => isset($orderDetail['fullname']) ? $orderDetail['fullname'] : null ]
                        );
                        $orderDetail['customer_info'] = $customer_info;
                        $orderDetail['payment_status'] = 1;
                        $orderDetail['total_pay'] = $orderDetail['total'];
                        $orderDetail['user_create_id'] = $this->user->getId();
                        $receipt_voucher = $this->autoUpdateFromOrder($this->request->post['order_id'], $orderDetail);
                    }
                    $this->model_sale_order->updateOrderPaymentStatus($this->request->post['order_id'], 1);
                } elseif ($this->request->post['order_status_id'] == 10) {
                    $this->model_sale_order->updateStatus($this->request->post, $order['order_status_id']);

                    // remove receipt voucher
                    $remove_receipt = $this->autoRemoveFromOrder($this->request->post['order_id']);
                    $orderProducts = $this->model_sale_order->getOrderProductsDetail($this->request->post['order_id']);
                    foreach ($orderProducts as &$orderProduct) {
                        $orderProduct['total'] = number_format($orderProduct['total'], 0, '', ',');
                    }
                    unset($orderProduct);

                    $order = $this->model_sale_order->getOrderById($this->request->post['order_id']);
                    $this->sendEmailOnCancelOrder($orderProducts, $order);

                    // cancel order has delivery
                    if (!empty($order['transport_name']) && !empty($order['transport_order_code'])) {
                        $dataCancel = [
                            'data_order_id' => $order['order_id'],
                            'transport_method' => $order['transport_name'],
                            'transport_order_code' => $order['transport_order_code'],
                        ];
                        try {
                            if (isset($dataCancel['transport_method']) && !empty($dataCancel['transport_method'])) {
                                $dataCancel['token'] = $this->config->get('config_' . strtoupper($dataCancel['transport_method']) . '_token');
                                $transport = new Transport($dataCancel['transport_method'], $dataCancel, $this);
                                $transport->cancelOrder($dataCancel);
                            }
                        } catch (Exception $e) {
                           // do something
                        }
                    }
                }

                $this->model_sale_order->updateStatus($this->request->post, $order['order_status_id']);

                $this->session->data['success'] = $this->language->get('text_success');
            }
        }

        $this->response->redirect($this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], true));
    }

    private function validateFormUpdateStatus()
    {
        if (!$this->user->hasPermission('modify', 'sale/order')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!isset($this->request->post['order_id'])) {
            $this->error['warning'] = $this->language->get('error_update_order_status_order_id');
        }

        if (!isset($this->request->post['order_status_id'])) {
            $this->error['warning'] = $this->language->get('error_update_order_status_order_status_id');
        }

        /* validate if change from draft to processing, or from processing to delivering */
        $this->load->model('sale/order');
        $this->load->language('sale/order');

        $STATUSES_NEED_VALIDATE_OUT_OF_STOCK_ON_EDITING = [];

        $current_order = $this->model_sale_order->getOrder($this->request->post['order_id']);
        if ($current_order['order_status_id'] == ModelSaleOrder::ORDER_STATUS_ID_DRAFT) {
            $STATUSES_NEED_VALIDATE_OUT_OF_STOCK_ON_EDITING = [
                ModelSaleOrder::ORDER_STATUS_ID_PROCESSING,
                ModelSaleOrder::ORDER_STATUS_ID_DELIVERING,
                ModelSaleOrder::ORDER_STATUS_ID_COMPLETED,
                ModelSaleOrder::ORDER_STATUS_ID_CANCELLED,
            ];
        }

        if ($current_order['order_status_id'] == ModelSaleOrder::ORDER_STATUS_ID_PROCESSING) {
            $STATUSES_NEED_VALIDATE_OUT_OF_STOCK_ON_EDITING = [
                ModelSaleOrder::ORDER_STATUS_ID_DELIVERING,
                ModelSaleOrder::ORDER_STATUS_ID_COMPLETED,
                ModelSaleOrder::ORDER_STATUS_ID_CANCELLED,
            ];
        }

        // TODO: $STATUSES_NEED_VALIDATE_OUT_OF_STOCK_ON_EDITING missing check from processing to complete|cancelled...
        if (isset($this->request->post['order_status_id']) &&
            in_array($this->request->post['order_status_id'], $STATUSES_NEED_VALIDATE_OUT_OF_STOCK_ON_EDITING)
        ) {
            // check delivery address
            if (!$current_order['shipping_province_code'] || !$current_order['telephone'] || !$current_order['shipping_address_1']) {
                $this->error['warning'] = $this->language->get('warning_shipping');
                $this->session->data['error'] = $this->language->get('warning_shipping');
            } else {
                $this->load->model('catalog/product');
                $orders = $this->model_sale_order->getOrderProducts($this->request->post['order_id']);
                if (!$orders) {
                    $this->error['warning'] = $this->language->get('order_product_data_not_empty');
                    $this->session->data['error'] = $this->language->get('order_product_data_not_empty');
                }

                foreach ($orders as $order) {
                    $product = $this->model_catalog_product->getProduct($order['product_id']);
                    if (!$product || empty($product) || !isset($product['product_id'])) {
                        $this->error['warning'] = $this->language->get('order_product_data_not_empty');
                        $this->session->data['error'] = $this->language->get('order_product_data_not_empty');

                        continue;
                    }

                    if (empty($order['product_version_id'])) {
                        if ($product['status'] == 0) {
                            $this->error['warning'] = $this->language->get('lang_out_of_stock');
                            $this->session->data['error'] = $this->language->get('lang_out_of_stock');
                        } elseif (!$this->model_catalog_product->isStock($order['product_id'])) {
                            $this->error['warning'] = $this->language->get('lang_out_of_stock');
                            $this->session->data['error'] = $this->language->get('lang_out_of_stock');
                        }

                        if ($product['multi_versions'] == 1) {
                            $this->error['warning'] = $this->language->get('warning_product_changed');
                            $this->session->data['error'] = $this->language->get('warning_product_changed');
                        }
                    } else {
                        $product_version = $this->model_catalog_product->getProductVersionByProductIdAndVersionId($order['product_id'], $order['product_version_id']);
                        if (!$product_version) {
                            $this->error['warning'] = $this->language->get('lang_out_of_stock');
                            $this->session->data['error'] = $this->language->get('lang_out_of_stock');
                        }

                        if ($product_version['status'] == 0 || $product['status'] == 0) {
                            $this->error['warning'] = $this->language->get('lang_out_of_stock');
                            $this->session->data['error'] = $this->language->get('lang_out_of_stock');
                        }
//                        elseif (!$this->model_catalog_product->isStock($order['product_id'], $order['product_version_id'])) {
//                            $this->error['warning'] = $this->language->get('lang_out_of_stock');
//                            $this->session->data['error'] = $this->language->get('lang_out_of_stock');
//                        } elseif ($product_version['deleted'] == 1) {
//                            $this->error['warning'] = $this->language->get('product_not_empty');
//                            $this->session->data['error'] = $this->language->get('product_not_empty');
//                        }
                    }
                }
            }
        }

        return !$this->error;
    }

    public function updateInfoCustomer()
    {
        $this->load->language('sale/order');
        try {
            $this->load->model('sale/order');
            $this->load->model('localisation/vietnam_administrative');

            $this->model_sale_order->updateInfoCustomer($this->request->post);
            $data = $this->model_sale_order->getOrder($this->request->post['order_id']);

            $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($data['shipping_province_code']);
            $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($data['shipping_district_code']);
            $ward = $this->model_localisation_vietnam_administrative->getWardByCode($data['shipping_ward_code']);

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode([
                'status' => true,
                'phone' => $data['telephone'],
                'fullname' => $data['fullname'],
                'address' => convertAddressOrderList($data['shipping_address_1'], $province['name'], $district['name'], $ward['name']),
                'message' => $this->language->get('success_order_customer_info'),
            ]));
        } catch (Exception $e) {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode([
                'status' => false,
                'message' => $this->language->get('error_order_customer_info'),
            ]));
        }
    }

    public function updateOrderQuickView()
    {
        $this->load->language('sale/order');
        $this->load->model('custom/tag');
        if (isset($this->request->post['order_id'])) {
            $this->load->model('sale/order');
            $order_id = $this->request->post['order_id'];
            $products = isset($this->request->post['product']) ? $this->request->post['product'] : '';
            $this->model_sale_order->updateProductOrder($products, $order_id);
            $this->model_sale_order->updateOrderComment($order_id, $this->request->post['order_comment']);
            $this->model_sale_order->updateColumnTotalOrder($order_id);
        }
        if (isset($this->request->post['tags'])) {
            $tag_list = $this->request->post['tags'];
            $order_id = $this->request->post['order_id'];
            //add new tags return id
            try {
                $tag_adds = $this->model_custom_tag->insertTag($tag_list);
                foreach ($tag_list as $key => $tag) {
                    if (in_array($tag, $tag_adds)) {
                        $tag_list[$key] = (string)array_search($tag, $tag_adds);
                    }
                }

                $this->model_custom_tag->insertTagOnlyOrder($order_id, $tag_list);
                $this->session->data['success'] = $this->language->get('notice_add_tag_success');
                $this->response->redirect($this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], true));
            } catch (Exception $e) {
                $this->session->data['error'] = $e->getMessage();
                $this->response->redirect($this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], true));
            }
        } else if (isset($this->request->post['order_id'])) {
            $this->model_custom_tag->deleteTag($this->request->post['order_id']);
        }
        $this->session->data['success'] = $this->language->get('success_order_customer_info');
        $this->response->redirect($this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function cancelOrder()
    {
        $this->load->language('sale/order');

        $this->load->model('sale/order');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateFormCancelOrder()) {
            $order = $this->model_sale_order->getOrder($this->request->post['order_id']);
            if (isset($order['order_status_id'])) {
                $this->request->post['order_status_id'] = 10;
                $this->model_sale_order->updateStatus($this->request->post, $order['order_status_id']);

                // remove receipt voucher
                $remove_receipt = $this->autoRemoveFromOrder($this->request->post['order_id']);
                if (isset($this->request->post['send_email']) && $this->request->post['send_email'] == 1) {
                    $orderProducts = $this->model_sale_order->getOrderProductsDetail($this->request->post['order_id']);
                    foreach ($orderProducts as &$orderProduct) {
                        $orderProduct['total'] = number_format($orderProduct['total'], 0, '', ',');
                    }
                    unset($orderProduct);

                    $order = $this->model_sale_order->getOrderById($this->request->post['order_id']);
                    $this->sendEmailOnCancelOrder($orderProducts, $order);
                }

                // cancel order has delivery
                if (!empty($order['transport_name']) && !empty($order['transport_order_code'])) {
                    $dataCancel = [
                        'data_order_id' => $order['order_id'],
                        'transport_method' => $order['transport_name'],
                        'transport_order_code' => $order['transport_order_code'],
                    ];
                    try {
                        if (isset($dataCancel['transport_method']) && !empty($dataCancel['transport_method'])) {
                            $dataCancel['token'] = $this->config->get('config_' . strtoupper($dataCancel['transport_method']) . '_token');
                            $transport = new Transport($dataCancel['transport_method'], $dataCancel, $this);
                            $transport->cancelOrder($dataCancel);
                        }
                    } catch (Exception $e) {
                        // do something
                    }
                }

                $this->session->data['success'] = $this->language->get('text_success');
            }
        }

        $this->response->redirect($this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], true));
    }

    private function validateFormCancelOrder()
    {
        if (!$this->user->hasPermission('modify', 'sale/order')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!isset($this->request->post['order_id'])) {
            $this->error['warning'] = $this->language->get('error_cancel_order_order_id');
        }

        if (!isset($this->request->post['send_email'])) {
            $this->error['warning'] = $this->language->get('error_cancel_order_send_email');
        }

        if (isset($this->request->post['order_cancel_comment']) && strlen(trim($this->request->post['order_cancel_comment'])) > 300) {
            $this->error['warning'] = sprintf($this->language->get('error_cancel_order_comment'), 300);
        }

        return !$this->error;
    }

    public function getOrderProducts()
    {
        if (!isset($this->request->get['order_id'])) {
            return [];
        }

        $this->load->model('sale/order');
        $this->load->model('tool/image');

        $order = $this->model_sale_order->getOrderById($this->request->get['order_id']);
        if (empty($order)) {
            return [];
        }

        $orderProducts = $this->model_sale_order->getOrderProductsDetail($this->request->get['order_id']);
        foreach ($orderProducts as &$orderProduct) {
            $orderProduct['price'] = number_format($orderProduct['price'], 0, '', ',');
            $orderProduct['total'] = number_format($orderProduct['total'], 0, '', ',');
            if ($orderProduct['image'] != '') {
                $orderProduct['image'] = $this->model_tool_image->resize($orderProduct['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
            } else {
                $orderProduct['image'] = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
            }
        }
        unset($orderProduct);

        $result = [
            'order_id' => $order['order_id'],
            'order_code' => $order['order_code'],
            'total' => number_format($order['total'], 0, '', ','),
            'order_products' => $orderProducts,
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    public function sendEmailOnCancelOrder($orderProducts, $order)
    {
        if (empty($orderProducts) || !is_array($orderProducts) || !isset($orderProducts[0]['order_code'])) {
            return;
        }

        $this->load->model('customer/customer');
        $customer = $this->model_customer_customer->getCustomer($order['customer_id']);
        if (empty($customer) || !isset($customer['email']) || trim($customer['email']) == '') {
            return;
        }

        $this->load->language('mail/order_cancel');

        $data['text_greeting'] = sprintf($this->language->get('text_greeting'), $orderProducts[0]['order_code']);

        $data['text_order_code'] = sprintf($this->language->get('text_order_code'), $orderProducts[0]['order_code']);

        $products = [];
        foreach ($orderProducts as $orderProduct) {
            $products[] = sprintf($this->language->get('text_order_product'),
                isset($orderProduct['name']) ? $orderProduct['name'] : '?',
                isset($orderProduct['quantity']) ? $orderProduct['quantity'] : '?',
                isset($orderProduct['price']) ? number_format($orderProduct['price'], 0, '', ',') : '?',
                isset($orderProduct['total']) ? $orderProduct['total'] : '?'
            );
        }
        $data['text_order_products'] = sprintf($this->language->get('text_order_products'), implode("\n", $products));

        $data['text_order_total'] = sprintf($this->language->get('text_order_total'), number_format($order['total'], 0, '', ','));

        $data['text_wish'] = $this->language->get('text_wish');
        $data['text_end'] = $this->language->get('text_end');

        $mail = new Mail($this->config->get('config_mail_engine'));
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = MAIL_SMTP_HOST_NAME;
        $mail->smtp_username = MAIL_SMTP_USERNAME;
        $mail->smtp_password = html_entity_decode(MAIL_SMTP_PASSWORD, ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = MAIL_SMTP_PORT;
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($customer['email']);
        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        $mail->setSubject(html_entity_decode(sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8'));
        $mail->setText($this->load->view('mail/order_cancel', $data));
        $mail->send();
    }

    public function getTagsAjax()
    {
        $json = [];
        if ($this->request->post['order_id']) {
            $this->load->model('custom/tag');
            $json = $this->model_custom_tag->getTagsByOrderId($this->request->post['order_id']);
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function convertStatusOrder($order_id)
    {
        $order_status_text = '';
        $this->load->language('sale/order');
        switch ($order_id) {
            case ModelSaleOrder::ORDER_STATUS_ID_PROCESSING :
                $order_status_text = $this->language->get('order_status_processing');
                break;
            case ModelSaleOrder::ORDER_STATUS_ID_DELIVERING :
                $order_status_text = $this->language->get('order_status_delivering');
                break;
            case ModelSaleOrder::ORDER_STATUS_ID_COMPLETED :
                $order_status_text = $this->language->get('order_status_complete');
                break;
            case ModelSaleOrder::ORDER_STATUS_ID_CANCELLED :
                $order_status_text = $this->language->get('order_status_canceled');
                break;
            case ModelSaleOrder::ORDER_STATUS_ID_DRAFT :
                $order_status_text = $this->language->get('order_status_draft');
                break;
        }
        return $order_status_text;
    }

    /**
     * for ajax
     *
     * @return array
     */
    public function getSatisfiedDiscountsAjaxForProduct()
    {
        if (!isset($this->request->get['data'])) {
            return [];
        }

        $products_info = ($this->request->get['data']);
        $json = [];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * for ajax
     *
     * @return int
     */
    public function calcDiscountAjaxForProduct()
    {
        if (!isset($this->request->get['data'])) {
            return [];
        }

        $products_info = ($this->request->get['data']);
        $json = [];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * for ajax
     *
     * @return int
     */
    public function calcDiscountAjaxForOrder()
    {
        if (!isset($this->request->get['total'])) {
            return [];
        }

        $total = $this->request->get['total'];
        $selected_discounts = (isset($this->request->get['discounts']) && is_array($this->request->get['discounts'])) ? $this->request->get['discounts'] : [];
        $result = [];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    public function loadUseroptionone()
    {
        $this->load->language('sale/order');
        $this->load->model('user/user');
        $show_html = '';

        $getListStaff = $this->model_user_user->getUsers();
        $show_html .=  '<option value="" data-value="' . $this->language->get('filter_staff') . '">' . $this->language->get('filter_staff_placeholder') . '</option>';
        foreach ($getListStaff as $key => $vl) {
            $show_html .= '<option value="' . $vl['user_id'] . '" data-value="' . $vl['lastname'] . ' ' . $vl['firstname'] . '">' . $vl['lastname'] . ' ' . $vl['firstname'] . '</option>';
        }

        echo $show_html .= '';
    }

    private function getSettingDomainValue()
    {
        $this->load->model('setting/setting');
        $config_domains = $this->model_setting_setting->getSettingValue('config_domains');
        $config_domains = json_decode($config_domains, true);
        if (!is_array($config_domains)) {
            $config_domains = [];
        }

        return $config_domains;
    }

    /**
     * getListProductForEditOrderForm
     *
     * @param $order_id
     * @return array Notice: the result must be same as got from getProductsLazyLoad()
     */
    private function getListProductForEditOrderForm($order_id)
    {
        $this->load->model('sale/order');
        $info_order_products = $this->model_sale_order->getInfoOrderProducts($order_id);

        $result = [];
        foreach ($info_order_products as $od) {
            if (!isset($od['product_id']) || !isset($od['name'])) {
                continue;
            }

            $is_multi_version = isset($od['product_version_id']);

            $version_name = implode(' • ', explode(',', $od['version']));
            if ($version_name != '') {
                $version_name = $this->language->get('text_version') . $version_name;
            }

            if ($od['image'] != '') {
                $image = $this->model_tool_image->resize($od['image'], 100, 100);
            } else {
                $image = $this->model_tool_image->resize('no_image.png', 100, 100);
            }

            $result[] = [
                'id' => $is_multi_version ? $od['pv_id'] : $od['product_id'],
                'product_id' => $od['product_id'],
                'product_version_id' => isset($od['product_version_id']) ? $od['product_version_id'] : null,
                'version' => $version_name,
                'subname' => $version_name,
                'text' => $od['name'],
                'product_name' => $od['name'],
                'image' => $image,
                'weight' => number_format($od['weight'], 0, '', ''),
                'price' => number_format($od['price'], 0, '', ','),
                'quantity' => $is_multi_version ? $od['ppv_quantity'] : $od['quantity'],
                'sale_on_out_of_stock' => $od['p_sale_on_out_of_stock'],
                'status' => $is_multi_version ? $od['ppv_status'] : $od['p_status']
            ];
        }

        return $result;
    }

    private function checkPermission($order_id)
    {
        if ($this->user->canAccessAll()) {
            return true;
        }

        $this->load->model('sale/order');

        return $this->model_sale_order->checkOrderPermission($order_id);
    }
}
