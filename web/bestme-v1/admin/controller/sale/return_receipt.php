<?php

/**
 * Created by PhpStorm.
 * User: Microsoft Windows
 * Date: 19/10/2020
 * Time: 9:24 AM
 */
class ControllerSaleReturnReceipt extends Controller
{
    private $error = array();
    private $source_allow_return = ['pos', 'shop', 'admin', 'chatbot', 'social'];
    const ORDER_STATUS_COMPLETE = 9;

    public function index()
    {
        $this->load->language('sale/return_receipt');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->getList();

    }

    public function getList()
    {
        $this->load->model('sale/order');
        $this->load->model('sale/return_receipt');

        $data = array();

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => ''
        );

        if (isset($this->session->data['success']) && $this->session->data['success'] != 'text_success') {
            $data['success'] = $this->session->data['success'];

            $this->session->data['success'] = '';
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $filter_name = $this->getValueFromRequest('get', 'filter_name');
        $page = $this->getValueFromRequest('get', 'page', 1);

        $filter_data = array(
            'filter_name' => $filter_name,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );
        if (isset($this->request->get['start_date']) && !empty($this->request->get['start_date'])) {
            $filter_data['start_date'] = $this->request->get['start_date'];
            $filter_data['end_date'] = $this->request->get['end_date'];
        }

        if (!$this->user->canAccessAll()){
            $filter_data['current_user_id'] = (int)$this->user->getId();
        }

        $return_receipt_total = $this->model_sale_return_receipt->getTotalReturnReceipts($filter_data);

        $results = $this->model_sale_return_receipt->getReturnReceipts($filter_data);// paginate
        foreach ($results as &$result){
            if (!isset($result['return_receipt_id']) || !isset($result['order_id']) || !$result['order_id']) {
                continue;
            }
            $result['view'] = $this->url->link('sale/return_receipt/detail', 'user_token=' . $this->session->data['user_token'] . '&return_receipt_id=' . $result['return_receipt_id'], true);
            $result['order_link'] = $this->url->link('sale/order/detail', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $result['order_id'], true);
        }
        $data['return_receipt'] = $results;

        $url = '';
        $pagination = new CustomPaginate();
        $pagination->total = $return_receipt_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/store_receipt', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        // Add jump to page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);
        $data['results'] = sprintf($this->language->get('text_pagination'), ($return_receipt_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($return_receipt_total - $this->config->get('config_limit_admin'))) ? $return_receipt_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $return_receipt_total, ceil($return_receipt_total / $this->config->get('config_limit_admin')));

        if(isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];
            $this->session->data['error'] = '';
            unset($this->session->data['error']);
        }

        $data['can_modify'] = $this->user->hasPermission('modify', 'sale/return_receipt') ? 1 : 0;
        $data['loadOptionOne'] = $this->url->link('sale/return_receipt/loadoptionone', 'user_token=' . $this->session->data['user_token'], true);
        $data['url_filter'] = $this->url->link('sale/return_receipt', 'user_token=' . $this->session->data['user_token'], true);
        $data['url_filter_popup'] = $this->url->link('sale/return_receipt/listOrderForPopUP', 'user_token=' . $this->session->data['user_token'], true);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('sale/return_receipt_list_append', $data));
        } else {
            $this->response->setOutput($this->load->view('sale/return_receipt_list', $data));
        }
    }

    public function listOrderForPopUP()
    {
        $this->load->language('sale/return_receipt');
        $this->load->model('sale/order');
        $data = array();

        $filter_name = $this->getValueFromRequest('get', 'filter_name');
        $order_source = $this->getValueFromRequest('get', 'order_source');
        $page = $this->getValueFromRequest('get', 'page', 1);
        $limit = 10;

        $filter_data = array(
            'order_code' => $filter_name,
            'order_source' => $order_source,
            'start' => ($page - 1) * $limit,
            'limit' => $limit
        );

        if(!$this->user->canAccessAll()){
            $filter_data['current_user_id'] = $this->user->getId();
        }

        if (isset($this->request->get['start_date']) && !empty($this->request->get['end_date'])) {
            $filter_data['time_from'] = $this->request->get['start_date'];
            $filter_data['time_to'] = $this->request->get['end_date'];
        }

        $order_total = $this->model_sale_order->getOrdersListTotal($filter_data);

        $results = $this->model_sale_order->getOrdersListData($filter_data);// paginate
        foreach ($results as &$result){
            if (!isset($result['order_code']) || !isset($result['order_id']) || !$result['order_id']) {
                continue;
            }
            $result['source'] = $this->language->get($result['source']);
            $result['order_full_name'] = isset($result['order_full_name']) ? $result['order_full_name'] : '';
            $result['customer_name'] = (isset($result['customer_full_name']) && $result['customer_full_name']) ? $result['customer_full_name'] : $result['order_full_name'];
            $result['return'] = $this->url->link('sale/return_receipt/add', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $result['order_id'], true);
            $result['order_link'] = $this->url->link('sale/order/detail', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $result['order_id'], true);
        }
        $data['order_list'] = $results;

        $url = '';
        $pagination = new CustomPaginate();
        $pagination->total = $order_total;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->url = $this->url->link('catalog/store_receipt', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        // Add jump to page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render(); // $data['pagination'] = $pagination->render($option);
        $data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($order_total - $limit)) ? $order_total : ((($page - 1) * $limit) + $limit), $order_total, ceil($order_total / $limit));

        $this->response->setOutput($this->load->view('sale/return_receipt_order_popup', $data));
    }

    public function detail()
    {
        return $this->getForm();
    }

    public function add()
    {
        $this->load->language('sale/return_receipt');
        $this->load->model('sale/order');
        $this->load->model('sale/return_receipt');
        $this->load->model('user/user');

        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            if (!$this->user->hasPermission('modify', 'sale/return_receipt')) {
                $this->response->redirect($this->url->link('sale/return_receipt', 'user_token=' . $this->session->data['user_token'] . $url='', true));
            }
            $this->model_sale_return_receipt->addReturnReceipt($this->request->get['order_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success_add');
            $this->response->redirect($this->url->link('sale/return_receipt', 'user_token=' . $this->session->data['user_token'], true));
        }

        return $this->getForm();
    }

    public function getForm()
    {
        $this->load->model('sale/order');
        $this->load->model('sale/return_receipt');
        $this->load->model('user/user');
        $this->load->model('setting/store');

        $this->load->language('sale/return_receipt');

        $this->document->addStyle('view/stylesheet/custom/order.css');

        $data['text_form'] = !isset($this->request->get['order_id']) ? $this->language->get('text_detail') : $this->language->get('text_create_receipt');
        //override document heading title
        $this->document->setTitle($data['text_form']);

        $url = '';

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('sale/return_receipt', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $data['text_form'],
            'href' => '',
        );

        $stores = array_reverse($this->model_setting_store->getStoreForUserLogin());
        $data['stores'] = $stores;

        if(isset($this->request->get['return_receipt_id']) && $this->request->get['return_receipt_id']) {
            $return_receipt = $this->model_sale_return_receipt->getReceiptById($this->request->get['return_receipt_id']);
            // check Permission staff
            if (!$this->user->canAccessAll()){
                if (isset($return_receipt['user_create_id']) && $this->user->getId() != $return_receipt['user_create_id']){
                    $this->response->redirect($this->url->link('sale/return_receipt', 'user_token=' . $this->session->data['user_token'] . $url='', true));
                }
            }

            if(empty($return_receipt)) {
                $this->session->data['error'] = $this->language->get('error_return_receipt_not_found');
                $this->response->redirect($this->url->link('sale/return_receipt', 'user_token=' . $this->session->data['user_token'], true));
            }

            $data['order_info'] = $this->model_sale_order->getOrder($return_receipt['order_id']);
            $products = $this->model_sale_return_receipt->getProductDetailInReceipt($return_receipt['order_id'], $this->request->get['return_receipt_id']);
            $data['order_info']['date_added'] = convertDate($data['order_info']['date_added']);
            $data['return_receipt'] = $return_receipt;
            $data['store_id'] = isset($return_receipt['store_id']) ? $return_receipt['store_id'] : 0;
        } else {
            $data['order_info'] = $this->model_sale_order->getOrder($this->request->get['order_id']);

            $order_not_complete = $data['order_info']['order_status_id'] != self::ORDER_STATUS_COMPLETE;
            $source_order_not_allowed = !in_array($data['order_info']['source'], $this->source_allow_return);

            if(!$data['order_info'] || $order_not_complete || $source_order_not_allowed) {
                $this->session->data['error'] = $this->language->get('error_order_not_found');
                $this->response->redirect($this->url->link('sale/return_receipt', 'user_token=' . $this->session->data['user_token'], true));
            }

            $order_products = $this->model_sale_order->getProductsDetailByOrder($this->request->get['order_id']);
            $returned_products = $this->model_sale_return_receipt->getReturnProductByQuantityOrderId($this->request->get['order_id']);

            $data['order_info']['date_added'] = convertDate($data['order_info']['date_added']);

            if($data['order_info']['discount']) {
                $total_before_discount = $data['order_info']['discount'] + $data['order_info']['total'];
                foreach ($order_products as &$product) {
                    $rate_discount = $product['total'] / $total_before_discount;
                    $product['return_price'] = ($product['total'] - $data['order_info']['discount'] * ($rate_discount)) / $product['quantity'];
                    $product['return_quantity'] = 0;
                }
            }

            $products = $this->getProductsNotReturned($returned_products, $order_products);

            if(empty($products)) {
                $this->session->data['error'] = $this->language->get('error_all_product_is_returned');
                $this->response->redirect($this->url->link('sale/return_receipt', 'user_token=' . $this->session->data['user_token'], true));
            }

            // store id from pos
            $data['store_id'] = isset($this->request->get['store_id']) ? $this->request->get['store_id'] : 0;
        }

        $data['order_link'] = $this->url->link('sale/order/detail', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $data['order_info']['order_id'], true);
        $data['product_link_prefix'] = $this->url->link('catalog/product/edit', 'user_token=' . $this->session->data['user_token'] . '&product_id=', true);
        $data['cancel'] = $this->url->link('sale/return_receipt', 'user_token=' . $this->session->data['user_token'] . '&product_id=', true);

        $data['return_receipt_id'] = isset($this->request->get['return_receipt_id']) ? $this->request->get['return_receipt_id'] : '';

        $user = $this->model_user_user->getUser($data['order_info']['user_id']);
        $data['user_create_order'] = $user['lastname'] . ' ' . $user['firstname'];
        $data['source'] = $this->language->get($data['order_info']['source']);
        $data['products'] = $products;

        // common layout
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('sale/return_form/index', $data));
    }

    private function getProductsNotReturned($returned_products, $order_products) {
        foreach ($order_products as $key => &$order_product) {
            foreach ($returned_products as $returned_product) {
                if($returned_product['product_id'] == $order_product['product_id'] && $returned_product['product_version_id'] == $order_product['product_version_id']) {
                    $order_product['quantity'] = (int)$order_product['quantity'] - (int)$returned_product['quantity'];
                }
                if($order_product['quantity'] <= 0) {
                    unset($order_products[$key]);
                }
            }
        }

        return $order_products;
    }

    public function loadoptionone()
    {
        $this->load->language('catalog/store_take_receipt');
        $categoryID = $this->request->post['categoryID'];
        $end_select_html = '</select></div>';
        $show_html = '';

        if ($categoryID > 0) {
            if ($categoryID == 1){ // taked_date
                $show_html .= '<div class="form-group hierarchical-child" id="hierarchical-date">
                                            <select class="form-control form-control-sm select2"
                                             data-placeholder="' . $this->language->get('filter_created_at_placeholder') . '" 
                                             data-label="Khoảng thời gian trả hàng" 
                                             id="filter_created_at_type" 
                                             name="filter_created_at_type">';
                $show_html .= '<option value="">' . $this->language->get('filter_created_at_placeholder') . '</option>';
                $show_html .= '<option value="today" data-value="' . $this->language->get('filter_created_at_today') . '">' . $this->language->get('filter_created_at_today') . '</option>';
                $show_html .= '<option value="week" data-value="' . $this->language->get('filter_created_at_this_week') . '">' . $this->language->get('filter_created_at_this_week') . '</option>';
                $show_html .= '<option value="month" data-value="' . $this->language->get('filter_created_at_this_month') . '">' . $this->language->get('filter_created_at_this_month') . '</option>';
                $show_html .= '<option value="option" data-value="' . $this->language->get('filter_created_at_option') . '">' . $this->language->get('filter_created_at_option') . '</option>';
                $show_html .= '</select>';
                $show_html .= '<div class="input-group-icon mt-3 w-100 d-none filter_created_at_value">
                            <i class="bg-secondary icon icon-calendar ml-n3" style=" top: -50px;"></i>
                            <input type="text" name="filter_created_at_from"
                                   class="form-control date-picker date-picker-store-receipt-filter mt-3"
                                   placeholder="' . $this->language->get('filter_select_date_from') . '" data-label="' . $this->language->get('filter_txt_date') . '"
                                   data-value="">
                            <i class="bg-secondary icon icon-calendar ml-n3" style=" top: 50px;"></i>
                            <input type="text" name="filter_created_at_to"
                                   class="form-control date-picker date-picker-store-receipt-filter mt-3"
                                   placeholder="' . $this->language->get('filter_select_date_to') . '" data-label="' . $this->language->get('filter_txt_date') . '"
                                   data-value="">
                          </div>';
            }
        } else {
            $show_html .= '<style>#optionone,#optiontwo{display:none !important}</style>';
        }

        echo $show_html .= '';
    }
}
