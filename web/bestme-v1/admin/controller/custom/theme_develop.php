<?php

class ControllerCustomThemeDevelop extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('custom/theme_develop');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        $this->document->addScript('view/javascript/pagination/pagination.js');
        $this->document->addStyle('view/stylesheet/custom/pagination.css');

        $this->getList();
    }

    protected function getList()
    {
        $data = [];
        $data['user_token'] = $this->session->data['user_token'];

        $shop_name = $this->model_setting_setting->getSettingValue('shop_name');
        $partner_directory = str_replace('-', '_', 'partner_' . $shop_name);
        $catalog_root_dir = DIR_CATALOG . 'view/theme/';
        $partner_root_dir = $catalog_root_dir . $partner_directory;
        $in_use_theme_code = $this->config->get('config_theme');
        $data['theme_in_use'] = $in_use_theme_code;
        $scandir_check = (is_dir($partner_root_dir));

        if ($scandir_check == false) { // kiểm tra ton tại thư mục partner
            $folder_path = $catalog_root_dir;
            $partner_new_name = "partner_" . $shop_name;
            $folder_path .= $partner_new_name . '/';
            if (!is_dir($folder_path)) {
                if (!mkdir($folder_path, 0755, true)) {
                    // do something...
                    $data['error_warning'] = $this->language->get('error_partner_theme_dir_not_found');
                }
            }

            $data['themes'][] = '';
        } else {
            $all_folder = array_diff(scandir($partner_root_dir . "/", 1), [".", ".."]);
            $total_folder = count(glob("" . $partner_root_dir . "/*", GLOB_ONLYDIR));
            if ($total_folder > 0) {
                foreach ($all_folder as $folder) {
                    // skip if not dir or hidden dir
                    $_folder_dir = $partner_root_dir . '/' . $folder;
                    if (!is_dir($_folder_dir) || strpos($folder, '.') === 0) {
                        continue;
                    }

                    $data['themes'][] = [
                        'name' => $folder,
                        'code' => $folder,
                        'in_use' => $folder === $in_use_theme_code,
                        'image' => $this->getThemeImage('partner_' . $shop_name, $folder),
                        'delete_link' => $this->url->link('custom/theme_develop/deleteTheme', 'theme=' . $folder . '&user_token=' . $this->session->data['user_token'] . '', true),
                        'download_link' => $this->url->link('custom/theme_develop/downloadTheme', 'theme=' . $folder . '&user_token=' . $this->session->data['user_token'] . '', true),
                    ];
                }
            } else {
                $data['themes'][] = '';
            }
        }

        $data['themes_json'] = json_encode($data['themes']);
        //$data['in_use_theme_image'] = $this->getThemeImage($in_use_theme_code);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['tool_file_manage'] = defined('THEME_TOOL_FILE_MANAGER_URL') ? THEME_TOOL_FILE_MANAGER_URL : 'https://bestme.vn/theme-tool/file-manager';

        $this->response->setOutput($this->load->view('custom/develop_list', $data));
    }

    private function getThemeImage($partner_theme_dir = '', $theme)
    {
        if ($this->request->server['HTTPS']) {
            $server = HTTPS_CATALOG;
        } else {
            $server = HTTP_CATALOG;
        }

        if (is_file(DIR_CATALOG . 'view/theme/' . $partner_theme_dir . '/' . $theme . '/image/' . $theme . '.png')) {
            return $server . 'catalog/view/theme/' . $partner_theme_dir . '/' . $theme . '/image/' . $theme . '.png';
        } else {
            return $server . 'image/no_image.png';
        }
    }

    public function uploadTheme()
    {
        $this->load->language('custom/theme');

        $json = array();

        // Check user has permission
        if (!$this->user->hasPermission('modify', 'custom/theme') && $this->model_user_user->getUserByDevelop($this->user->getId()) != 1) {
            $json['error'] = $this->language->get('error_permission');
        }

        // Check if there is a install zip already there
        $files = glob(DIR_UPLOAD . '*.tmp');

        foreach ($files as $file) {
            if (is_file($file) && (filectime($file) < (time() - 5))) {
                unlink($file);
            }

            if (is_file($file)) {
                $json['error'] = $this->language->get('error_upload_theme');

                break;
            }
        }

        // Check for any install directories
        $directories = glob(DIR_UPLOAD . 'tmp-*');

        foreach ($directories as $directory) {
            if (is_dir($directory) && (filectime($directory) < (time() - 5))) {
                // Get a list of files ready to upload
                $files = array();

                $path = array($directory);

                while (count($path) != 0) {
                    $next = array_shift($path);

                    // We have to use scandir function because glob will not pick up dot files.
                    foreach (array_diff(scandir($next), array('.', '..')) as $file) {
                        $file = $next . '/' . $file;

                        if (is_dir($file)) {
                            $path[] = $file;
                        }

                        $files[] = $file;
                    }
                }

                rsort($files);

                foreach ($files as $file) {
                    if (is_file($file)) {
                        unlink($file);
                    } elseif (is_dir($file)) {
                        rmdir($file);
                    }
                }

                rmdir($directory);
            }

            if (is_dir($directory)) {
                $json['error'] = $this->language->get('error_upload_theme');

                break;
            }
        }

        if (isset($this->request->files['file']['name'])) {
            if (substr($this->request->files['file']['name'], -4) != '.zip') {
                $json['error'] = $this->language->get('error_upload_theme_type');
            }

            if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
                $json['error'] = $this->language->get('error_upload_theme');
            }
        } else {
            $json['error'] = $this->language->get('error_upload_theme');
        }
        if (!$json) {
            $this->load->model('setting/setting');
            $shop_name = $this->model_setting_setting->getSettingValue('shop_name');
            $partner_directory = str_replace('-', '_', 'partner_' . $shop_name);
            $file = DIR_UPLOAD . $this->request->files['file']['name'];
            move_uploaded_file($this->request->files['file']['tmp_name'], $file);
            if (is_file($file)) {
                $folder_theme = DIR_UPLOAD . 'temp_theme/' . $partner_directory;
                $folder_last = DIR_CATALOG . 'view/theme/' . $partner_directory;
                $zip = new ZipArchive;
                $res = $zip->open($file);
                if ($res === TRUE && !file_exists($folder_theme . '/' . $zip->getNameIndex(0))) {
                    $zip->extractTo($folder_theme);
                    //check folder isset
                    $flag = true;

                    $theme_tool_validate_config = unserialize(THEME_TOOL_VALIDATE_CONFIG);

                    foreach ($theme_tool_validate_config as $ver => $validate) {
                        // TODO: validate version...
                        if (!isset($validate['required']) || !is_array($validate['required'])) {
                            continue;
                        }

                        foreach ($validate['required'] as $file_require) {
                            if (strpos($file_require, '$$THEME_NAME$$') === false) {
                                $file_require = str_replace('$$THEME_NAME$$', $zip->getNameIndex(0), $file_require);
                            } else {
                                $file_require = str_replace('$$THEME_NAME$$', str_replace('/', '', $zip->getNameIndex(0)), $file_require);
                            }
                            if (!file_exists($folder_theme . '/' . $zip->getNameIndex(0) . $file_require)) {
                                $flag = false;
                                break;
                            }
                        }

                        if (!$flag) break;
                    }

                    if ($flag && !file_exists($folder_last . '/' . $zip->getNameIndex(0))) {
                        $zip->extractTo($folder_last);
                        $json['text'] = $this->language->get('upload_theme_success');
                    } else {
                        $json['error'] = $this->language->get('error_upload_theme_file_require');
                        if (file_exists($folder_last . '/' . $zip->getNameIndex(0))) {
                            $json['error'] = $this->language->get('file_theme_isset');
                        }
                    }

                    $zip->close();
                    unlink($file);
                    $this->deleteDirectory($folder_theme);
                } else {
                    $json['error'] = $this->language->get('error_upload_theme_or_file_isset');
                }
            } else {
                $json['error'] = $this->language->get('upload_theme_file_not_isset');
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function deleteDirectory($dirname)
    {
        if (is_dir($dirname)) {
            $dir_handle = opendir($dirname);
        }

        if (!$dir_handle) {
            return false;
        }

        while ($file = readdir($dir_handle)) {
            if ($file != "." && $file != "..") {
                if (!is_dir($dirname . "/" . $file)) {
                    unlink($dirname . "/" . $file);
                } else {
                    $this->deleteDirectory($dirname . '/' . $file);
                }
            }
        }

        closedir($dir_handle);
        rmdir($dirname);

        return true;
    }

    public function downloadTheme()
    {
        $theme = $this->request->get['theme'];

        // Get real path for theme folder under partner theme folder
        $shop_name = $this->model_setting_setting->getSettingValue('shop_name');
        $partner_directory = str_replace('-', '_', 'partner_' . $shop_name);
        $catalog_root_dir = DIR_CATALOG . 'view/theme/';
        $partner_root_dir = $catalog_root_dir . $partner_directory . '/' . $theme;
        if (!is_dir($partner_root_dir)) {
            return false; // TODO: correct ??
        }

        $zip_name = sprintf('%s.zip', $theme);
        $zip_dir = sprintf('%s%s', DIR_DOWNLOAD, time());
        if (!is_dir($zip_dir)) {
            if (!mkdir($zip_dir, 0755, true)) {
                return false; // TODO: correct  ??
            }
        }
        $zip_filepath = sprintf('%s%s/%s', DIR_DOWNLOAD, time(), $zip_name);
        $total_folder = count(glob("" . $partner_root_dir . "/*", GLOB_ONLYDIR));

        if ($total_folder <= 0) {
            return false; // TODO: correct  ??
        }

        ob_start();

        // Initialize archive object
        $zip = new ZipArchive();
        $zip->open($zip_filepath, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        // Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($partner_root_dir), RecursiveIteratorIterator::LEAVES_ONLY);

        // Add files/dirs to zip
        foreach ($files as $name => $file) {
            // Skip directories (they would be added automatically)
            if (!$file->isDir()) {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($partner_root_dir) + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }
        }

        // Zip archive will be created only after closing object
        $zip->close();

        // stream download data
        header('Content-Type: application/zip');
        header("Content-Disposition: attachment; filename = $zip_name");
        header('Content-Length: ' . filesize($zip_filepath));
        // header("Location: $zip_filepath");

        while (ob_get_level()) {
            ob_end_clean();
        }
        readfile($zip_filepath);

        $this->response->setOutput($this->load->view('custom/develop_list', []));
    }

    public function deleteTheme()
    {
        // only support for partner
        $this->load->model('setting/extension');
        $this->load->language('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'GET') && $this->validatePartnerTheme()) {
            $code = $this->request->get['theme'];
            $this->model_setting_extension->uninstall('theme', $code);

            $themeDirectory = $this->getPartnerThemeDirectory($this->request->get['theme']);
            $this->deleteDirectory(DIR_CATALOG . 'view/theme/' . $themeDirectory);

            $this->response->redirect($this->url->link('custom/theme_develop', 'user_token=' . $this->session->data['user_token'], true));
        }

        return false;
    }

    private function validatePartnerTheme()
    {
        // only support for partner
        if (!$this->user->hasPermission('modify', 'setting/setting')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        $this->load->model('user/user');
        $isDev = $this->model_user_user->getUserByDevelop($this->user->getId());
        if ($isDev) {
            $themeDirectory = $this->getPartnerThemeDirectory($this->request->get['theme']);
            if (is_dir(DIR_CATALOG . 'view/theme/' . $themeDirectory)) {
                return true;
            }
        }

        return false;
    }

    private function getPartnerThemeDirectory($theme)
    {
        $this->load->model('setting/setting');
        $shop_name = $this->model_setting_setting->getSettingValue('shop_name');
        // note: partner directory is built from shop_name with - gets replaced by _ as opencart style!
        $partner_directory = str_replace('-', '_', 'partner_' . $shop_name);
        $partner_directory_theme = $partner_directory . '/' . $theme;

        return $partner_directory_theme;
    }
}
