<?php

class ControllerCustomPreference extends Controller
{
    public $FILE_ROBOTS = 'robots.txt';
    public $FILE_SITE_MAP = 'sitemap.xml';

    public function index()
    {
        $this->load->language('custom/preference');
        $this->load->model('appstore/my_app');

        $this->document->setTitle($this->language->get('heading_title'));

        $data = [];

        $data['action'] = $this->url->link('custom/preference', 'user_token=' . $this->session->data['user_token'], true);

        $this->load->model('setting/setting');

        // Note: use model_setting_setting->getSetting() did not worked! Use config->get() instead
        // e.g: skip using: $this->model_setting_setting->getSetting('xxx');
        $data['online_store_config_homepage_title'] = $this->config->get('online_store_config_homepage_title');
        $data['online_store_config_homepage_description'] = $this->config->get('online_store_config_homepage_description');

        // gtm code
        $data['online_store_config_gtm_code_header'] = $this->config->get('online_store_config_gtm_code_header');
        $data['online_store_config_gtm_code_body'] = $this->config->get('online_store_config_gtm_code_body');

        // gg analytics code
        $data['online_store_config_google_code'] = $this->config->get('online_store_config_google_code');

        // gg adwords conversion tracking
        $data['online_store_config_google_aw_code'] = $this->config->get('online_store_config_google_aw_code');
        $data['online_store_config_google_aw_code_cart'] = $this->config->get('online_store_config_google_aw_code_cart');
        $data['online_store_config_google_aw_code_order'] = $this->config->get('online_store_config_google_aw_code_order');
        $data['online_store_config_google_aw_code_order_success'] = $this->config->get('online_store_config_google_aw_code_order_success');

        // maxlead code
        if($this->model_appstore_my_app->checkAppActive(ModelAppstoreMyApp::APP_MAX_LEAD)){
            $data['online_store_config_maxlead_code'] = $this->config->get('online_store_config_maxlead_code');
            $data['maxlead_is_actived'] = true;
        };

        // facebook pixel code
        $data['online_store_config_facebook_pixel'] = $this->config->get('online_store_config_facebook_pixel');

        $online_store_config_facebook_pixel_cart = $this->config->get('online_store_config_facebook_pixel_cart');
        if (empty($online_store_config_facebook_pixel_cart)) {
            // init default
            $online_store_config_facebook_pixel_cart = '
                <!-- Facebook Pixel Tracking Code -->
                <script>
                  fbq(\'track\', \'AddToCart\');
                </script>
                <!-- End Facebook Pixel Tracking Code -->';
        }
        $data['online_store_config_facebook_pixel_cart'] = $online_store_config_facebook_pixel_cart;

        $online_store_config_facebook_pixel_order = $this->config->get('online_store_config_facebook_pixel_order');
        if (empty($online_store_config_facebook_pixel_order)) {
            // init default
            $online_store_config_facebook_pixel_order = '
                <!-- Facebook Pixel Tracking Code -->
                <script>
                  fbq(\'track\', \'AddPaymentInfo\');
                </script>
                <!-- End Facebook Pixel Tracking Code -->';
        }
        $data['online_store_config_facebook_pixel_order'] = $online_store_config_facebook_pixel_order;

        $online_store_config_facebook_pixel_order_success = $this->config->get('online_store_config_facebook_pixel_order_success');
        if (empty($online_store_config_facebook_pixel_order_success)) {
            // init default
            $online_store_config_facebook_pixel_order_success = '
                <!-- Facebook Pixel Tracking Code -->
                <script>
                  fbq(\'track\', \'Purchase\');
                </script>
                <!-- End Facebook Pixel Tracking Code -->';
        }
        $data['online_store_config_facebook_pixel_order_success'] = $online_store_config_facebook_pixel_order_success;

        // sitemap and robots feature
        // notice: DO NOT use physical file for sitemap.xml and robots.txt, due to multi shops technology
        // => use db setting instead
        $data['online_store_config_site_map_type'] = $this->config->get('online_store_config_site_map_type');
        $data['online_store_config_sitemap'] = $this->config->get('online_store_config_sitemap');
        $data['online_store_config_robots'] = $this->config->get('online_store_config_robots');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editExistingSetting('online_store_config', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('custom/preference', 'user_token=' . $this->session->data['user_token'], true));
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        $data['video_facebook_pixel'] = BESTME_FACEBOOK_PIXEL_GUIDE;
        $data['video_google_analytics'] = BESTME_GOOGLE_ANALYTICS_GUIDE;

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('custom/preference', $data));
    }

    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'custom/preference')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }
}