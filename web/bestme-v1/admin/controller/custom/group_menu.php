<?php

class ControllerCustomGroupMenu extends Controller {
	public function index() {
		$this->load->language('catalog/product');

		$this->load->language('catalog/category');
		$this->load->language('custom/menu');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('custom/group_menu');

		$this->getList();
	}

	public function getList()
	{
		$data = [];
		$url = '';
		$list_menu = $this->model_custom_group_menu->getListMenuCustom();
		foreach ($list_menu as $key => $value) {
			$data['list_menu'][] = array(
				'group_menu_id' => $value['group_menu_id'],
				'name' => $value['name'],
				'edit' => $this->url->link('custom/group_menu/edit', 'user_token=' . $this->session->data['user_token'] . '&group_menu_id=' . $value['group_menu_id'] . $url, true),
				'child' => $value['child'],

			);
		}
		$data['add'] = $this->url->link('custom/group_menu/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['custom_header'] = $this->load->controller('common/custom_header');
		$data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('custom_menu/menu', $data));
	}

	public function edit()
	{
		$this->load->language('custom/menu');
		if (($this->request->server['REQUEST_METHOD'] == 'POST')){
			$data = $this->request->post;
			$group_menu_id = $this->getValueFromRequest('get', 'group_menu_id');
			$this->update($group_menu_id, $data);
		}

		$group_menu_id = $this->getValueFromRequest('get', 'group_menu_id');

        $this->load->model('custom/common');
        $data['seo_url_text'] = $this->model_custom_common->getSlugUrlForGroupMenu($group_menu_id);

		$this->load->model('custom/group_menu');
		$group_menu = $this->model_custom_group_menu->getGroupMenuById($group_menu_id);
		$data['group_menu']['name'] = $group_menu['name'];

		$this->load->model('catalog/category');
        $listCategories = $this->model_catalog_category->getCategories();
        $data['list_categories'] = [];
        foreach ($listCategories as $value) {
            $name = $this->removeChar($value, 'name', '>');
            $data['list_categories'][] = array(
                'category_id' => $value['category_id'],
                'category_name'        =>  str_replace(' ', '', $name)
            );
        }
        $data['json']= json_encode($data['list_categories']);

        $this->load->model('custom/type');
        $listType = $this->model_custom_type->getAllType();

        $data['list_type'] = $listType;
        $data['jsonType'] = json_encode($listType);
        
        $data['json']= json_encode($data['list_categories']);

        $this->load->model('custom/collection');
        $data['collection_list'] = json_encode($this->model_custom_collection->getCollectionPaginator());
        $data['collections'] = $this->model_custom_collection->getCollectionPaginator();

        $this->load->model('custom/relation_table');
        $childMenu = $this->model_custom_relation_table->getAllByMainId('group_menu', $group_menu_id);

        $data['childMenu'] = $childMenu;

        $data['group_menu_id_data'] = $group_menu_id;
        $data['user_token'] = $this->session->data['user_token'];
		$data['action'] = $this->url->link('custom/group_menu/edit', 'user_token=' . $this->session->data['user_token'] . '&group_menu_id=' . $this->request->get['group_menu_id'], true);

		$data['menu_index'] = $this->url->link('custom/group_menu', 'user_token=' . $this->session->data['user_token'], true);

		$data['menu_destroy'] = $this->url->link('custom/group_menu/destroy', 'user_token=' . $this->session->data['user_token'] . '&group_menu_id=' . $this->request->get['group_menu_id'], true);

		$data['custom_header'] = $this->load->controller('common/custom_header');
		$data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $data['menu_cancel_url'] = $this->url->link('custom/group_menu', 'user_token=' . $this->session->data['user_token'], true);

        $this->response->setOutput($this->load->view('custom_menu/menu_edit', $data));
	}

	public function update($group_menu_id, $data)
	{
		$url = '';
		$this->load->model('custom/group_menu');
		$this->load->model('custom/menu_item');
		$this->load->model('custom/relation_table');
		$menu_item_old_id =  $this->model_custom_relation_table->getFieldByMainId('group_menu', $group_menu_id, 'id');
		//lay danh sach cac menu_item old duoc post
		if (!isset($data['menu_item_name_old'])) {
			foreach ($menu_item_old_id as $key => $value) {
				$this->model_custom_relation_table->deleteById($value);
			}
		} else {
			$menu_item_old_post = array_keys($data['menu_item_name_old']);
			$menu_item_delete = array_diff($menu_item_old_id, $menu_item_old_post);
			foreach ($menu_item_delete as $key => $value) {
				$this->model_custom_relation_table->deleteById($value);
			}

			foreach ($menu_item_old_post as $relation_id) {
				$menu_item_id = $this->model_custom_relation_table->getFieldById($relation_id, 'child_id');
				$this->model_custom_menu_item->edit($menu_item_id, $data);
			}
		}

		$this->load->model('custom/group_menu');
		$this->model_custom_group_menu->edit($group_menu_id, $data);

		$this->response->redirect($this->url->link('custom/group_menu', 'user_token=' . $this->session->data['user_token'] . $url, true));
	}

	public function destroy()
	{
		$this->response->addHeader('Content-Type: application/json');
		$json = array('message' => '', 'status' => false);
		$group_menu_id = $this->getValueFromRequest('get', 'group_menu_id');
		if ($group_menu_id == '') {
			$this->response->setOutput(json_encode($json));
			return ;
		}

        if (isset($this->request->post)) {
            $this->load->model('custom/group_menu');
            $this->model_custom_group_menu->deleteById($group_menu_id);
            $json = array('message' => 'Success', 'status' => true);
            $this->response->setOutput(json_encode($json));
            return;
        }
	}

	public function add()
	{
		$this->load->language('custom/menu');
		if (($this->request->server['REQUEST_METHOD'] == 'POST')){
			// $group_menu_id = $this->getValueFromRequest('get', 'group_menu_id');
			$data = $this->request->post;
			$this->load->model('custom/group_menu');
			$this->model_custom_group_menu->add($data);

			$this->response->redirect($this->url->link('custom/group_menu', 'user_token=' . $this->session->data['user_token'], true));
		}

		$data = array();
		// $group_menu_id = $this->getValueFromRequest('get', 'group_menu_id');

		// $this->load->model('custom/group_menu');
		// $group_menu = $this->model_custom_group_menu->getGroupMenuById($group_menu_id);
		// $data['group_menu']['name'] = $group_menu['name'];

		// get category
		$this->load->model('catalog/category');
        $listCategories = $this->model_catalog_category->getCategories();
        $data['list_categories'] = [];
        foreach ($listCategories as $value) {
            $name = $this->removeChar($value, 'name', '>');
            $data['list_categories'][] = array(
                'category_id' => $value['category_id'],
                'category_name'        =>  str_replace(' ', '', $name)
            );
        }
        $data['json']= json_encode($data['list_categories']);

        // get all type
        $this->load->model('custom/type');
        $listType = $this->model_custom_type->getAllType();

        $data['list_type'] = $listType;
        $data['jsonType'] = json_encode($listType);

        $data['user_token'] = $this->session->data['user_token'];
		$data['action'] = $this->url->link('custom/group_menu/add', 'user_token=' . $this->session->data['user_token'], true);
		$data['user_token'] = $this->session->data['user_token'];
		$data['menu_index'] = $this->url->link('custom/group_menu', 'user_token=' . $this->session->data['user_token'], true);
		$data['custom_header'] = $this->load->controller('common/custom_header');
		$data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

		$data['menu_cancel_url'] = $this->url->link('custom/group_menu', 'user_token=' . $this->session->data['user_token'], true);

		$this->response->setOutput($this->load->view('custom_menu/menu_add', $data));
	}
}