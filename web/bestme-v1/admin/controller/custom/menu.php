<?php

class ControllerCustomMenu extends Controller
{
    public function index()
    {
        $this->load->language('catalog/product');

        $this->load->language('catalog/category');
        $this->load->language('custom/menu');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('custom/menu');

        $this->getList();
    }

    public function getList()
    {
        $data = array();
        $list_menu = $this->model_custom_menu->getAllMenu();

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];
            unset($this->session->data['error']);
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data['list_menu'] = $this->mappingMenu($list_menu, $this->getMenuConfig());
        $data['video_menu'] = defined('BESTME_MENU_GUIDE') ? BESTME_MENU_GUIDE : '';

        $data['add'] = $this->url->link('custom/menu/add', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_reorder_menu_item'] = $this->url->link('custom/menu/reOrderMenuItem', 'user_token=' . $this->session->data['user_token'], true);

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('custom/menu', $data));
    }

    private function mappingMenu($data, $menu_configs = [])
    {
        if (!is_array($data)) {
            return [];
        }
        $list_menu = array();
        foreach ($data as $key => $value) {
            $attach_list = [];
            foreach ($menu_configs as $menu_config) {
                if ($menu_config['id'] == $value['menu_id']) {
                    $attach_list[] = $menu_config['name'];
                }
            }
            $list_menu[] = array(
                'menu_id' => $value['menu_id'],
                'name' => $value['name'],
                'edit' => $this->url->link('custom/menu/edit', 'user_token=' . $this->session->data['user_token'] . '&menu_id=' . $value['menu_id'], true),
                'delete' => $this->url->link('custom/menu/delete', 'user_token=' . $this->session->data['user_token'] . '&menu_id=' . $value['menu_id'], true),
                'list_items' => implode(', ', array_map(function ($m) {
                    return $m['name'];
                }, $value['items'])),
                'attach' => implode(', ', $attach_list),
                'items' => $this->mappingMenu($value['items']),
            );
        }
        return $list_menu;
    }

    public function add()
    {
        $this->load->language('custom/menu');
        $this->load->model('custom/menu');
        $this->document->setTitle($this->language->get('heading_title_add'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateMenuForm()) {
            $menu_id = $this->model_custom_menu->add($this->request->post);
            $this->updateMenuPosition($menu_id);
            if (isset($this->request->post['type_popup']) && $this->request->post['type_popup']){
                // support for add menu in builder config
                header('Location: ' . $_SERVER["HTTP_REFERER"] );
                exit;
            }else {
                $this->response->redirect($this->url->link('custom/menu', 'user_token=' . $this->session->data['user_token'], true));
            }
        }

        $this->getForm();
    }

    public function edit()
    {
        $this->load->language('custom/menu');
        $this->load->model('custom/menu');
        $this->load->model('custom/collection');
        $this->load->model('extension/module/theme_builder_config');

        $this->document->setTitle($this->language->get('heading_title_edit'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateMenuEditForm()) {
            $menu_id = $this->request->post['menu_id'];
            $this->updateMenuPosition($menu_id);
            $this->model_custom_menu->edit($menu_id, $this->request->post);

            if (isset($this->request->post['type_popup']) && $this->request->post['type_popup']){
                // support for edit menu in builder config
                header('Location: ' . $_SERVER["HTTP_REFERER"] );
                exit;
            }else {
                $this->session->data['success'] = $this->language->get('edit_success');
                $this->response->redirect($this->url->link('custom/menu', 'user_token=' . $this->session->data['user_token'], true));
            }
        }

        $this->getForm();
    }

    public function reOrderMenuItem()
    {
        $this->load->language('custom/menu');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateMenuReorder()) {
            $this->load->model('custom/menu');
            $menu_id = $this->request->post['menu_id'];
            $new_menu_order = $this->request->post['menu_item_ids'];
            $this->model_custom_menu->reOrderMenuItem($menu_id, $new_menu_order);

            $this->session->data['success'] = $this->language->get('edit_menu_order_success');

            $json = [
                'code' => 1,
                'message' => 'Re-order menu items successfully!',
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));

            return;
        }

        $json = [
            'code' => 100,
            'message' => 'Re-order menu items failed! Error: 100',
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    protected function getForm()
    {
        $data = array();
        if (isset($this->request->get['menu_id'])) {
            $this->load->model('catalog/category');
            $this->load->model('catalog/product');
            $this->load->model('custom/collection');
            $this->load->model('blog/category');

            $listCategories = $this->model_catalog_category->getCategories();
            $categories = array();
            foreach ($listCategories as $value) {
                $name = $this->removeChar($value, 'name', '>');
                $categories[$value['category_id']] = $name;
            }

            $data['menu_id'] = $this->request->get['menu_id'];
            $data['menu'] = $this->model_custom_menu->getMenuById($data['menu_id']);
            if (is_array($data['menu'])) {
                foreach ($data['menu'] as $k => $menu) {
                    if (isset($menu['menu_type_id']) && $menu['menu_type_id'] == ModelCustomMenu::MENU_TYPE_PRODUCT) {
                        if (isset($menu['target_value']) && $menu['target_value']) {
                            $data['menu'][$k]['type_name'] = $this->model_catalog_product->getProductNameById($menu['target_value']);
                        }
                    }

                    if (isset($menu['menu_type_id']) && $menu['menu_type_id'] == ModelCustomMenu::MENU_TYPE_CATEGORY) {
                        if (isset($menu['target_value']) && $menu['target_value']) {
                            $data['menu'][$k]['type_name'] = isset($categories[$menu['target_value']]) ? $categories[$menu['target_value']] : '';
                        }
                    }

                    if (isset($menu['menu_type_id']) && $menu['menu_type_id'] == ModelCustomMenu::MENU_TYPE_COLLECTION) {
                        if (isset($menu['target_value']) && $menu['target_value']) {
                            $data['menu'][$k]['type_name'] = $this->model_custom_collection->getCollectionTitleById($menu['target_value']);
                        }
                    }

                    if (isset($menu['menu_type_id']) && $menu['menu_type_id'] == ModelCustomMenu::MENU_TYPE_BLOG) {
                        if (isset($menu['target_value']) && $menu['target_value']) {
                            $title = $this->model_blog_category->getBlogCategoryNameById($menu['target_value']);
                            $data['menu'][$k]['type_name'] = isset($title['title']) ? $title['title'] : '';
                        }
                    }
                }
            }
            $data['menu_name'] = $this->model_custom_menu->getMenuNameById($data['menu_id']);

            $this->load->model('design/seo_url');
            $data['seo_url_text'] = $this->model_design_seo_url->getSeoUrlByQuery('menu_id=' . $data['menu_id']);
        }

        // get all type
        $listType = $this->model_custom_menu->getMenuTypes();

        $data['is_parent'] = false;
        $parent_menu = $this->model_custom_menu->getAllParentMenu();
        foreach ($parent_menu as $menu) {
            if (isset($data['menu_id']) && $menu['menu_id'] == $data['menu_id']) {
                $data['is_parent'] = true;
                break;
            }
        }
        if (!isset($this->request->get['menu_id'])) {
            $data['is_parent'] = true;
        }

        $data['list_type'] = $listType;
        $data['jsonType'] = json_encode($listType);

        $list_built_in_pages = array();
        $list_built_in_pages[$this->language->get('txt_page_home')]                 = '?route=common/home';
        $list_built_in_pages[$this->language->get('txt_page_product')]              = '?route=common/shop';
        $list_built_in_pages[$this->language->get('txt_page_contact')]              = '?route=contact/contact';
        $list_built_in_pages[$this->language->get('txt_page_login')]                = '?route=account/login';
        $list_built_in_pages[$this->language->get('txt_page_register')]             = '?route=account/register';
        $list_built_in_pages[$this->language->get('txt_page_checkout_cart')]        = '?route=checkout/my_orders';
        $list_built_in_pages[$this->language->get('text_page_account_profile')]     = '?route=checkout/profile';
        // $list_built_in_pages[$this->language->get('txt_page_policy')]               = '?route=common/home#';
        // $list_built_in_pages[$this->language->get('txt_page_about')]                = '?route=about/about';
        $list_built_in_pages[$this->language->get('txt_page_blog')]                 = '?route=blog/blog';

        $data['list_built_in_page_array'] = $list_built_in_pages;
        $data['list_built_in_page'] = json_encode($list_built_in_pages);

        $data['menu_type_category'] = ModelCustomMenu::MENU_TYPE_CATEGORY;
        $data['menu_type_collection'] = ModelCustomMenu::MENU_TYPE_COLLECTION;
        $data['menu_type_product'] = ModelCustomMenu::MENU_TYPE_PRODUCT;
        $data['menu_type_url'] = ModelCustomMenu::MENU_TYPE_URL;
        $data['menu_type_built_in_page'] = ModelCustomMenu::MENU_TYPE_BUILT_IN_PAGE;
        $data['menu_type_blog'] = ModelCustomMenu::MENU_TYPE_BLOG;

        $data['user_token'] = $this->session->data['user_token'];
        if (isset($this->request->get['menu_id'])) {
            $data['action'] = $this->url->link('custom/menu/edit', 'user_token=' . $this->session->data['user_token'], true);
        } else {
            $data['action'] = $this->url->link('custom/menu/add', 'user_token=' . $this->session->data['user_token'], true);
        }
        $data['user_token'] = $this->session->data['user_token'];
        $data['menu_index'] = $this->url->link('custom/menu', 'user_token=' . $this->session->data['user_token'], true);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $data['menu_cancel_url'] = $this->url->link('custom/menu', 'user_token=' . $this->session->data['user_token'], true);

        $data['menu_configs'] = $this->getMenuConfig();

        if (isset($this->request->get['menu_popup']) && $this->request->get['menu_popup']) {
            $this->response->setOutput($this->load->view('custom/menu_form_popup', $data));
        }else {
            $this->response->setOutput($this->load->view('custom/menu_form', $data));
        }
    }

    public function destroy()
    {
        $this->response->addHeader('Content-Type: application/json');
        $json = array('message' => 'Failed!', 'status' => false);
        if (isset($this->request->get['menu_id'])) {
            $menu_id = $this->request->get['menu_id'];
        } else {
            $menu_id = '';
        }
        if ($menu_id == '') {
            $this->response->setOutput(json_encode($json));
            return;
        }

        $this->load->model('custom/menu');
        $this->model_custom_menu->deleteMenuById($menu_id);
        $json = array('message' => 'Success!', 'status' => true);
        $this->response->setOutput(json_encode($json));
        return;
    }

    public function delete()
    {
        $this->load->language('custom/menu');
        if (isset($this->request->get['menu_id'])) {
            $menu_id = $this->request->get['menu_id'];
        } else {
            $menu_id = '';
        }
        if ($menu_id == '') {
            $this->session->data['error'] = $this->language->get('delete_error');
            $this->response->redirect($this->url->link('custom/menu', 'user_token=' . $this->session->data['user_token'] , true));
        }

        $this->load->model('custom/menu');
        $this->model_custom_menu->deleteMenuById($menu_id);
        $this->session->data['success'] = $this->language->get('delete_success');
        $this->response->redirect($this->url->link('custom/menu', 'user_token=' . $this->session->data['user_token'] , true));
    }

    private function validateMenuForm()
    {
        if (!$this->user->hasPermission('modify', 'custom/menu')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    private function validateMenuEditForm()
    {
        if (!$this->user->hasPermission('modify', 'custom/menu')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!isset($this->request->post['menu_id'])){
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

    private function validateMenuReorder()
    {
        if (!$this->user->hasPermission('modify', 'custom/menu')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!isset($this->request->post['menu_id'])){
            $this->error['warning'] = $this->language->get('edit_menu_order_failed_missing_menu_id');
        }

        if (!isset($this->request->post['menu_item_ids']) || !is_array($this->request->post['menu_item_ids'])){
            $this->error['warning'] = $this->language->get('edit_menu_order_failed_invalid_menu_ids');
        }

        return !$this->error;
    }

    private function getMenuConfig()
    {
        $this->load->language('custom/menu');
        $this->load->model('extension/module/theme_builder_config');
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;
        $menu_header = json_decode($model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HEADER, 'config', false), true);
        $menu_footer = json_decode($model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_FOOTER, 'config', false), true);
        $menu[] = [
            'type' => 'attach_header',
            'id' => isset($menu_header['menu']['display-list']['id']) ? $menu_header['menu']['display-list']['id'] : '',
            'name' => $this->language->get('attach_header')
        ];
        $menu[] = [
            'type' => 'attach_collection',
            'id' => isset($menu_footer['collection']['menu_id']) ? $menu_footer['collection']['menu_id'] : '',
            'name' => $this->language->get('attach_footer_collection')
        ];
        $menu[] = [
            'type' => 'attach_quick_links',
            'id' => isset($menu_footer['quick-links']['menu_id']) ? $menu_footer['quick-links']['menu_id'] : '',
            'name' => $this->language->get('attach_footer_quick_link')
        ];
        return $menu;
    }

    /**
     * TODO: correct name "updateMenuPosition()" => "updateMenuReferences()"
     * @param $menu_id
     */
    private function updateMenuPosition ($menu_id)
    {
        $this->load->model('extension/module/theme_builder_config');
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;
        $menu_header = json_decode($model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HEADER, 'config', false), true);
        $menu_footer = json_decode($model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_FOOTER, 'config', false), true);

        if (isset($menu_header['menu']['display-list']['id']) && $menu_header['menu']['display-list']['id'] == $menu_id) {
            if (!isset($this->request->post['attach_header'])) {
                $menu_header['menu']['display-list']['id'] = '';
                $model_extension_module_theme_builder_config->updateConfig([
                    'store_id' => 0,
                    'code' => 'config',
                    'key' => ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HEADER,
                    'config' => $menu_header,
                ]);
            }
        }

        if (isset($menu_header['menu']['display-list']['id']) && $menu_header['menu']['display-list']['id'] != $menu_id) {
            if (isset($this->request->post['attach_header'])) {
                $menu_header['menu']['display-list']['id'] = $menu_id;
                $model_extension_module_theme_builder_config->updateConfig([
                    'store_id' => 0,
                    'code' => 'config',
                    'key' => ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HEADER,
                    'config' => $menu_header,
                ]);
            }
        }

        if (isset($menu_header['menu']['display-list']['id']) && $menu_footer['collection']['menu_id'] == $menu_id) {
            if (!isset($this->request->post['attach_collection'])) {
                $menu_footer['collection']['menu_id'] = '';
                $model_extension_module_theme_builder_config->updateConfig([
                    'store_id' => 0,
                    'code' => 'config',
                    'key' =>  ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_FOOTER,
                    'config' => $menu_footer,
                ]);
            }
        }

        if (isset($menu_header['menu']['display-list']['id']) && $menu_footer['collection']['menu_id'] != $menu_id) {
            if (isset($this->request->post['attach_collection'])) {
                $menu_footer['collection']['menu_id'] = $menu_id;
                $model_extension_module_theme_builder_config->updateConfig([
                    'store_id' => 0,
                    'code' => 'config',
                    'key' => ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_FOOTER,
                    'config' => $menu_footer,
                ]);
            }
        }

        if (isset($menu_header['menu']['display-list']['id']) && $menu_footer['quick-links']['menu_id'] == $menu_id) {
            if (!isset($this->request->post['attach_quick_links'])) {
                $menu_footer['quick-links']['menu_id'] = '';
                $model_extension_module_theme_builder_config->updateConfig([
                    'store_id' => 0,
                    'code' => 'config',
                    'key' =>  ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_FOOTER,
                    'config' => $menu_footer,
                ]);
            }
        }

        if (isset($menu_header['menu']['display-list']['id']) && $menu_footer['quick-links']['menu_id'] != $menu_id) {
            if (isset($this->request->post['attach_quick_links'])) {
                $menu_footer['quick-links']['menu_id'] = $menu_id;
                $model_extension_module_theme_builder_config->updateConfig([
                    'store_id' => 0,
                    'code' => 'config',
                    'key' => ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_FOOTER,
                    'config' => $menu_footer,
                ]);
            }
        }
    }
}