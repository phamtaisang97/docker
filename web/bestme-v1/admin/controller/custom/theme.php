<?php

class ControllerCustomTheme extends Controller
{
    use Device_Util;
    public function index()
    {
        $this->load->language('custom/theme');

        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->addScript('view/javascript/pagination/pagination.js');
        $this->document->addStyle('view/stylesheet/custom/pagination.css');

        $data = [];

        $data['user_token'] = $this->session->data['user_token'];

        /* used theme */
        $in_use_theme_code = $this->config->get('config_theme');

        /* installed themes. format:
         * [
         *     [
         *         'id' => 1,
         *         'name' => 'Novaon - công nghệ'
         *     ],
         *     [
         *         'id' => 1,
         *         'name' => 'Novaon - thời trang mẹ và bé'
         *     ]
         * ];
         */
        $data['themes'] = [];

        $this->load->model('setting/extension');

        $extensions = $this->model_setting_extension->getInstalled('theme');

        foreach ($extensions as $code) {
            if (!$this->checkThemeExist($code)) {
                continue;
            }
            $this->load->language('extension/theme/' . $code, 'extension');

            $data['themes'][] = [
                'id' => 1,
                'name' => $this->language->get('extension')->get('heading_title'),
                'code' => $code,
                'in_use' => $code === $in_use_theme_code,
                'image' => $this->getThemeImage($code),
            ];
            if ($code === $in_use_theme_code) {
                $data['theme_active'] = [
                    'id' => count($data['themes']) - 1,
                    'name' => $this->language->get('extension')->get('heading_title'),
                    'code' => $code,
                    'in_use' => $code === $in_use_theme_code,
                    'image' => $this->getThemeImage($code),
                ];
            }
        }
        $data['has_permission_setting'] = $this->user->hasPermission('access', 'setting/setting');

        $data['themes_json'] = json_encode($data['themes']);
        $data['href_theme_builder'] = $this->url->link('section/sections', 'user_token=' . $this->session->data['user_token'] . '&key=config_section_sections', true);
        $this->load->model('user/user');
        $data['theme_develop'] = $this->model_user_user->getUserByDevelop($this->user->getId());
        $data['email'] = $this->model_user_user->getEmailByUser($this->user->getId());
        $this->load->model('setting/setting');
        $data['shop_name'] = $this->model_setting_setting->getShopName();
        $data['tool_file_manage'] = defined('THEME_TOOL_FILE_MANAGER_URL') ? THEME_TOOL_FILE_MANAGER_URL : 'https://bestme.vn/theme-tool/file-manager';
        $data['list_theme_develop'] = $this->url->link('custom/theme_develop', 'user_token=' . $this->session->data['user_token'] . '', true);
        $data['video_interface'] = defined('BESTME_INTERFACE_GUIDE') ? BESTME_INTERFACE_GUIDE : '';

        $data['in_use_theme_image'] = $this->getThemeImage($in_use_theme_code);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $theme_redirect = $this->url->link('common/bestme_use_theme', 'user_token=' . $this->session->data['user_token'], true);
        $theme_redirect = urlencode($theme_redirect);
        $data['themes_store_link'] = BESTME_SERVER . 'kho-giao-dien?theme_redirect=' . $theme_redirect. '&email=' . $data['email']. '&shop_name=' . $data['shop_name'];
        $data['footer'] = $this->load->controller('common/footer');
        $data['is_on_mobile'] = $this->isMobile();
        $this->response->setOutput($this->load->view('custom/theme', $data));
    }

    private function getThemeImage($theme)
    {
        if ($this->request->server['HTTPS']) {
            $server = HTTPS_CATALOG;
        } else {
            $server = HTTP_CATALOG;
        }

        if (is_file(DIR_CATALOG . 'view/theme/' . $theme . '/image/' . $theme . '.png')) {
            return $server . 'catalog/view/theme/' . $theme . '/image/' . $theme . '.png';
        } else {
            return $server . 'image/no_image.png';
        }
    }

    private function checkThemeExist($theme)
    {  // check theme is not dev and exist
        if (is_dir(DIR_CATALOG . 'view/theme/' . $theme)) {
            return true;
        }

        return false;
    }

}