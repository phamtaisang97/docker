<?php

class ControllerCustomMenuItem extends Controller {
	public function edit()
	{
		if (($this->request->server['REQUEST_METHOD'] == 'POST')){
			$data = $this->request->post;
			$menu_item_id = $this->getValueFromRequest('get', 'menu_item_id');
			$this->update($menu_item_id,$data);
		}

		$menu_item_id = $this->getValueFromRequest('get', 'menu_item_id');
		$data = array();
		$this->load->language('custom/menu');
		$url = '';
		$this->load->model('custom/menu_item');
		$this->load->model('custom/relation_table');

		$relation_id = $this->getValueFromRequest('get', 'relation_id');

		$group_menu_id = $this->model_custom_relation_table->getFieldById($relation_id, 'main_id');

		$this->load->model('custom/group_menu');
		$this->load->model('custom/menu_item');
		$group_menu = $this->model_custom_group_menu->getGroupMenuById($group_menu_id);
		$data['name_of_menu_item'] = $this->model_custom_menu_item->getNameRecordById($menu_item_id, 'menu_item');
		$data['group_menu']['name'] = $group_menu['name'];

		$this->load->model('catalog/category');
        $listCategories = $this->model_catalog_category->getCategories();
        foreach ($listCategories as $value) {
            $name = $this->removeChar($value, 'name', '>');
            $data['list_categories'][] = array(
                'category_id' => $value['category_id'],
                'category_name'        =>  str_replace(' ', '', $name)
            );
        }
        $data['json']= json_encode($data['list_categories']);

        $this->load->model('custom/type');
        $listType = $this->model_custom_type->getAllType();
        $data['list_type'] = $listType;
        $data['jsonType'] = json_encode($listType);
        
        $data['json']= json_encode($data['list_categories']);

        $this->load->model('custom/relation_table');
        $this->load->model('custom/common');
        $childMenuItem = $this->model_custom_relation_table->getAllByMainId('menu_item', $menu_item_id, 0);

        $data['childMenuItem'] = $childMenuItem;
        $data['menu_item_id'] = $menu_item_id;
        $data['seo_url_text'] = $this->model_custom_common->getSlugUrlForMenuItem($menu_item_id);

        $data['group_menu_id_data'] = $group_menu_id;
        $data['user_token'] = $this->session->data['user_token'];
		$data['action'] = $this->url->link('custom/menu_item/edit', 'user_token=' . $this->session->data['user_token'] . '&menu_item_id=' . $menu_item_id .'&relation_id='.$relation_id.$url, true);
		$data['menu_index'] = $this->url->link('custom/group_menu', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['custom_header'] = $this->load->controller('common/custom_header');
		$data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $data['menu_cancel_url'] = $this->url->link('custom/group_menu', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $this->response->setOutput($this->load->view('custom_menu/menu_item_edit', $data));
	}

	public function update($menu_item_id, $data)
	{
		$url = '';
		$relation_id = $this->getValueFromRequest('get', 'relation_id');
		$this->load->model('custom/menu_item');
		$name = '';
		if (isset($data['menu_item_name'])) {
			$name = $data['menu_item_name'];
		}
		$this->model_custom_menu_item->updateMenuItemDes($menu_item_id, $name);
		$this->model_custom_menu_item->editChild($menu_item_id,$data);

		$this->response->redirect($this->url->link('custom/group_menu', 'user_token=' . $this->session->data['user_token'] . $url, true));
	}

	public function destroy()
	{
		$this->response->addHeader('Content-Type: application/json');
		$json = array('message' => '', 'status' => false);
		$menu_item_id = $this->getValueFromRequest('get', 'menu_item_id');
		if ($menu_item_id == '') {
			$this->response->setOutput(json_encode($json));
			return ;
		}
        if (isset($this->request->post)) {
            $this->load->model('custom/menu_item');
            $this->model_custom_menu_item->deleteById($menu_item_id);
            $json = array('message' => 'Success', 'status' => true);
            $this->response->setOutput(json_encode($json));
            return;
        }
	}
}