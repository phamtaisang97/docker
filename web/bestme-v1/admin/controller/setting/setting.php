<?php

class ControllerSettingSetting extends Controller
{
    private $error = array();
    private $demo_data_path = DIR_SYSTEM . 'library/theme_config/demo_data/';
    private $theme_parent_directory = 'special_theme/';

    public function index()
    {
        $this->load->language('setting/setting');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('config', $this->request->post);

            if ($this->config->get('config_currency_auto')) {
                $this->load->model('localisation/currency');
                $this->model_localisation_currency->refresh();
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('setting/store', 'user_token=' . $this->session->data['user_token'], true));
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = '';
        }

        if (isset($this->error['owner'])) {
            $data['error_owner'] = $this->error['owner'];
        } else {
            $data['error_owner'] = '';
        }

        if (isset($this->error['address'])) {
            $data['error_address'] = $this->error['address'];
        } else {
            $data['error_address'] = '';
        }

        if (isset($this->error['email'])) {
            $data['error_email'] = $this->error['email'];
        } else {
            $data['error_email'] = '';
        }

        if (isset($this->error['telephone'])) {
            $data['error_telephone'] = $this->error['telephone'];
        } else {
            $data['error_telephone'] = '';
        }

        if (isset($this->error['meta_title'])) {
            $data['error_meta_title'] = $this->error['meta_title'];
        } else {
            $data['error_meta_title'] = '';
        }

        if (isset($this->error['country'])) {
            $data['error_country'] = $this->error['country'];
        } else {
            $data['error_country'] = '';
        }

        if (isset($this->error['zone'])) {
            $data['error_zone'] = $this->error['zone'];
        } else {
            $data['error_zone'] = '';
        }

        if (isset($this->error['customer_group_display'])) {
            $data['error_customer_group_display'] = $this->error['customer_group_display'];
        } else {
            $data['error_customer_group_display'] = '';
        }

        if (isset($this->error['login_attempts'])) {
            $data['error_login_attempts'] = $this->error['login_attempts'];
        } else {
            $data['error_login_attempts'] = '';
        }

        if (isset($this->error['voucher_min'])) {
            $data['error_voucher_min'] = $this->error['voucher_min'];
        } else {
            $data['error_voucher_min'] = '';
        }

        if (isset($this->error['voucher_max'])) {
            $data['error_voucher_max'] = $this->error['voucher_max'];
        } else {
            $data['error_voucher_max'] = '';
        }

        if (isset($this->error['processing_status'])) {
            $data['error_processing_status'] = $this->error['processing_status'];
        } else {
            $data['error_processing_status'] = '';
        }

        if (isset($this->error['complete_status'])) {
            $data['error_complete_status'] = $this->error['complete_status'];
        } else {
            $data['error_complete_status'] = '';
        }

        if (isset($this->error['log'])) {
            $data['error_log'] = $this->error['log'];
        } else {
            $data['error_log'] = '';
        }

        if (isset($this->error['limit_admin'])) {
            $data['error_limit_admin'] = $this->error['limit_admin'];
        } else {
            $data['error_limit_admin'] = '';
        }

        if (isset($this->error['encryption'])) {
            $data['error_encryption'] = $this->error['encryption'];
        } else {
            $data['error_encryption'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_stores'),
            'href' => $this->url->link('setting/store', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('setting/setting', 'user_token=' . $this->session->data['user_token'], true)
        );

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data['action'] = $this->url->link('setting/setting', 'user_token=' . $this->session->data['user_token'], true);

        $data['cancel'] = $this->url->link('setting/store', 'user_token=' . $this->session->data['user_token'], true);

        $data['user_token'] = $this->session->data['user_token'];

        if (isset($this->request->post['config_meta_title'])) {
            $data['config_meta_title'] = $this->request->post['config_meta_title'];
        } else {
            $data['config_meta_title'] = $this->config->get('config_meta_title');
        }

        if (isset($this->request->post['config_meta_description'])) {
            $data['config_meta_description'] = $this->request->post['config_meta_description'];
        } else {
            $data['config_meta_description'] = $this->config->get('config_meta_description');
        }

        if (isset($this->request->post['config_meta_keyword'])) {
            $data['config_meta_keyword'] = $this->request->post['config_meta_keyword'];
        } else {
            $data['config_meta_keyword'] = $this->config->get('config_meta_keyword');
        }

        if (isset($this->request->post['config_theme'])) {
            $data['config_theme'] = $this->request->post['config_theme'];
        } else {
            $data['config_theme'] = $this->config->get('config_theme');
        }

        if ($this->request->server['HTTPS']) {
            $data['store_url'] = HTTPS_CATALOG;
        } else {
            $data['store_url'] = HTTP_CATALOG;
        }

        $data['themes'] = array();

        $this->load->model('setting/extension');

        $extensions = $this->model_setting_extension->getInstalled('theme');

        foreach ($extensions as $code) {
            $this->load->language('extension/theme/' . $code, 'extension');

            $data['themes'][] = array(
                'text' => $this->language->get('extension')->get('heading_title'),
                'value' => $code
            );
        }

        if (isset($this->request->post['config_layout_id'])) {
            $data['config_layout_id'] = $this->request->post['config_layout_id'];
        } else {
            $data['config_layout_id'] = $this->config->get('config_layout_id');
        }

        $this->load->model('design/layout');

        $data['layouts'] = $this->model_design_layout->getLayouts();

        if (isset($this->request->post['config_name'])) {
            $data['config_name'] = $this->request->post['config_name'];
        } else {
            $data['config_name'] = $this->config->get('config_name');
        }

        if (isset($this->request->post['config_owner'])) {
            $data['config_owner'] = $this->request->post['config_owner'];
        } else {
            $data['config_owner'] = $this->config->get('config_owner');
        }

        if (isset($this->request->post['config_address'])) {
            $data['config_address'] = $this->request->post['config_address'];
        } else {
            $data['config_address'] = $this->config->get('config_address');
        }

        if (isset($this->request->post['config_geocode'])) {
            $data['config_geocode'] = $this->request->post['config_geocode'];
        } else {
            $data['config_geocode'] = $this->config->get('config_geocode');
        }

        if (isset($this->request->post['config_email'])) {
            $data['config_email'] = $this->request->post['config_email'];
        } else {
            $data['config_email'] = $this->config->get('config_email');
        }

        if (isset($this->request->post['config_telephone'])) {
            $data['config_telephone'] = $this->request->post['config_telephone'];
        } else {
            $data['config_telephone'] = $this->config->get('config_telephone');
        }

        if (isset($this->request->post['config_fax'])) {
            $data['config_fax'] = $this->request->post['config_fax'];
        } else {
            $data['config_fax'] = $this->config->get('config_fax');
        }

        if (isset($this->request->post['config_image'])) {
            $data['config_image'] = $this->request->post['config_image'];
        } else {
            $data['config_image'] = $this->config->get('config_image');
        }

        $this->load->model('tool/image');

        if (isset($this->request->post['config_image']) && (is_file(DIR_IMAGE . $this->request->post['config_image']) || $this->model_tool_image->imageIsExist($this->request->post['config_image']))) {
            $data['thumb'] = $this->model_tool_image->resize($this->request->post['config_image'], 100, 100);
        } elseif ($this->config->get('config_image') && (is_file(DIR_IMAGE . $this->config->get('config_image')) || $this->model_tool_image->imageIsExist($this->config->get('config_image')))) {
            $data['thumb'] = $this->model_tool_image->resize($this->config->get('config_image'), 100, 100);
        } else {
            $data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        }

        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

        if (isset($this->request->post['config_open'])) {
            $data['config_open'] = $this->request->post['config_open'];
        } else {
            $data['config_open'] = $this->config->get('config_open');
        }

        if (isset($this->request->post['config_comment'])) {
            $data['config_comment'] = $this->request->post['config_comment'];
        } else {
            $data['config_comment'] = $this->config->get('config_comment');
        }

        $this->load->model('localisation/location');

        $data['locations'] = $this->model_localisation_location->getLocations();

        if (isset($this->request->post['config_location'])) {
            $data['config_location'] = $this->request->post['config_location'];
        } elseif ($this->config->get('config_location')) {
            $data['config_location'] = $this->config->get('config_location');
        } else {
            $data['config_location'] = array();
        }

        if (isset($this->request->post['config_country_id'])) {
            $data['config_country_id'] = $this->request->post['config_country_id'];
        } else {
            $data['config_country_id'] = $this->config->get('config_country_id');
        }

        $this->load->model('localisation/country');

        $data['countries'] = $this->model_localisation_country->getCountries();

        if (isset($this->request->post['config_zone_id'])) {
            $data['config_zone_id'] = $this->request->post['config_zone_id'];
        } else {
            $data['config_zone_id'] = $this->config->get('config_zone_id');
        }

        if (isset($this->request->post['config_language'])) {
            $data['config_language'] = $this->request->post['config_language'];
        } else {
            $data['config_language'] = $this->config->get('config_language');
        }

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        if (isset($this->request->post['config_admin_language'])) {
            $data['config_admin_language'] = $this->request->post['config_admin_language'];
        } else {
            $data['config_admin_language'] = $this->config->get('config_admin_language');
        }

        if (isset($this->request->post['config_currency'])) {
            $data['config_currency'] = $this->request->post['config_currency'];
        } else {
            $data['config_currency'] = $this->config->get('config_currency');
        }

        if (isset($this->request->post['config_currency_auto'])) {
            $data['config_currency_auto'] = $this->request->post['config_currency_auto'];
        } else {
            $data['config_currency_auto'] = $this->config->get('config_currency_auto');
        }

        $this->load->model('localisation/currency');

        $data['currencies'] = $this->model_localisation_currency->getCurrencies();

        if (isset($this->request->post['config_length_class_id'])) {
            $data['config_length_class_id'] = $this->request->post['config_length_class_id'];
        } else {
            $data['config_length_class_id'] = $this->config->get('config_length_class_id');
        }

        $this->load->model('localisation/length_class');

        $data['length_classes'] = $this->model_localisation_length_class->getLengthClasses();

        if (isset($this->request->post['config_weight_class_id'])) {
            $data['config_weight_class_id'] = $this->request->post['config_weight_class_id'];
        } else {
            $data['config_weight_class_id'] = $this->config->get('config_weight_class_id');
        }

        $this->load->model('localisation/weight_class');

        $data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();

        if (isset($this->request->post['config_limit_admin'])) {
            $data['config_limit_admin'] = $this->request->post['config_limit_admin'];
        } else {
            $data['config_limit_admin'] = $this->config->get('config_limit_admin');
        }

        if (isset($this->request->post['config_product_count'])) {
            $data['config_product_count'] = $this->request->post['config_product_count'];
        } else {
            $data['config_product_count'] = $this->config->get('config_product_count');
        }

        if (isset($this->request->post['config_review_status'])) {
            $data['config_review_status'] = $this->request->post['config_review_status'];
        } else {
            $data['config_review_status'] = $this->config->get('config_review_status');
        }

        if (isset($this->request->post['config_review_guest'])) {
            $data['config_review_guest'] = $this->request->post['config_review_guest'];
        } else {
            $data['config_review_guest'] = $this->config->get('config_review_guest');
        }

        if (isset($this->request->post['config_voucher_min'])) {
            $data['config_voucher_min'] = $this->request->post['config_voucher_min'];
        } else {
            $data['config_voucher_min'] = $this->config->get('config_voucher_min');
        }

        if (isset($this->request->post['config_voucher_max'])) {
            $data['config_voucher_max'] = $this->request->post['config_voucher_max'];
        } else {
            $data['config_voucher_max'] = $this->config->get('config_voucher_max');
        }

        if (isset($this->request->post['config_tax'])) {
            $data['config_tax'] = $this->request->post['config_tax'];
        } else {
            $data['config_tax'] = $this->config->get('config_tax');
        }

        if (isset($this->request->post['config_tax_default'])) {
            $data['config_tax_default'] = $this->request->post['config_tax_default'];
        } else {
            $data['config_tax_default'] = $this->config->get('config_tax_default');
        }

        if (isset($this->request->post['config_tax_customer'])) {
            $data['config_tax_customer'] = $this->request->post['config_tax_customer'];
        } else {
            $data['config_tax_customer'] = $this->config->get('config_tax_customer');
        }

        if (isset($this->request->post['config_customer_online'])) {
            $data['config_customer_online'] = $this->request->post['config_customer_online'];
        } else {
            $data['config_customer_online'] = $this->config->get('config_customer_online');
        }

        if (isset($this->request->post['config_customer_activity'])) {
            $data['config_customer_activity'] = $this->request->post['config_customer_activity'];
        } else {
            $data['config_customer_activity'] = $this->config->get('config_customer_activity');
        }

        if (isset($this->request->post['config_customer_search'])) {
            $data['config_customer_search'] = $this->request->post['config_customer_search'];
        } else {
            $data['config_customer_search'] = $this->config->get('config_customer_search');
        }

        if (isset($this->request->post['config_customer_group_id'])) {
            $data['config_customer_group_id'] = $this->request->post['config_customer_group_id'];
        } else {
            $data['config_customer_group_id'] = $this->config->get('config_customer_group_id');
        }

        $this->load->model('customer/customer_group');

        $data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();

        if (isset($this->request->post['config_customer_group_display'])) {
            $data['config_customer_group_display'] = $this->request->post['config_customer_group_display'];
        } elseif ($this->config->get('config_customer_group_display')) {
            $data['config_customer_group_display'] = $this->config->get('config_customer_group_display');
        } else {
            $data['config_customer_group_display'] = array();
        }

        if (isset($this->request->post['config_customer_price'])) {
            $data['config_customer_price'] = $this->request->post['config_customer_price'];
        } else {
            $data['config_customer_price'] = $this->config->get('config_customer_price');
        }

        if (isset($this->request->post['config_login_attempts'])) {
            $data['config_login_attempts'] = $this->request->post['config_login_attempts'];
        } elseif ($this->config->has('config_login_attempts')) {
            $data['config_login_attempts'] = $this->config->get('config_login_attempts');
        } else {
            $data['config_login_attempts'] = 5;
        }

        if (isset($this->request->post['config_account_id'])) {
            $data['config_account_id'] = $this->request->post['config_account_id'];
        } else {
            $data['config_account_id'] = $this->config->get('config_account_id');
        }

        $this->load->model('catalog/information');

        $data['informations'] = $this->model_catalog_information->getInformations();

        if (isset($this->request->post['config_cart_weight'])) {
            $data['config_cart_weight'] = $this->request->post['config_cart_weight'];
        } else {
            $data['config_cart_weight'] = $this->config->get('config_cart_weight');
        }

        if (isset($this->request->post['config_checkout_guest'])) {
            $data['config_checkout_guest'] = $this->request->post['config_checkout_guest'];
        } else {
            $data['config_checkout_guest'] = $this->config->get('config_checkout_guest');
        }

        if (isset($this->request->post['config_checkout_id'])) {
            $data['config_checkout_id'] = $this->request->post['config_checkout_id'];
        } else {
            $data['config_checkout_id'] = $this->config->get('config_checkout_id');
        }

        if (isset($this->request->post['config_invoice_prefix'])) {
            $data['config_invoice_prefix'] = $this->request->post['config_invoice_prefix'];
        } elseif ($this->config->get('config_invoice_prefix')) {
            $data['config_invoice_prefix'] = $this->config->get('config_invoice_prefix');
        } else {
            $data['config_invoice_prefix'] = 'INV-' . date('Y') . '-00';
        }

        if (isset($this->request->post['config_order_status_id'])) {
            $data['config_order_status_id'] = $this->request->post['config_order_status_id'];
        } else {
            $data['config_order_status_id'] = $this->config->get('config_order_status_id');
        }

        if (isset($this->request->post['config_processing_status'])) {
            $data['config_processing_status'] = $this->request->post['config_processing_status'];
        } elseif ($this->config->get('config_processing_status')) {
            $data['config_processing_status'] = $this->config->get('config_processing_status');
        } else {
            $data['config_processing_status'] = array();
        }

        if (isset($this->request->post['config_complete_status'])) {
            $data['config_complete_status'] = $this->request->post['config_complete_status'];
        } elseif ($this->config->get('config_complete_status')) {
            $data['config_complete_status'] = $this->config->get('config_complete_status');
        } else {
            $data['config_complete_status'] = array();
        }

        if (isset($this->request->post['config_fraud_status_id'])) {
            $data['config_fraud_status_id'] = $this->request->post['config_fraud_status_id'];
        } else {
            $data['config_fraud_status_id'] = $this->config->get('config_fraud_status_id');
        }

        $this->load->model('localisation/order_status');

        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        if (isset($this->request->post['config_api_id'])) {
            $data['config_api_id'] = $this->request->post['config_api_id'];
        } else {
            $data['config_api_id'] = $this->config->get('config_api_id');
        }

        $this->load->model('user/api');

        $data['apis'] = $this->model_user_api->getApis();

        if (isset($this->request->post['config_stock_display'])) {
            $data['config_stock_display'] = $this->request->post['config_stock_display'];
        } else {
            $data['config_stock_display'] = $this->config->get('config_stock_display');
        }

        if (isset($this->request->post['config_stock_warning'])) {
            $data['config_stock_warning'] = $this->request->post['config_stock_warning'];
        } else {
            $data['config_stock_warning'] = $this->config->get('config_stock_warning');
        }

        if (isset($this->request->post['config_stock_checkout'])) {
            $data['config_stock_checkout'] = $this->request->post['config_stock_checkout'];
        } else {
            $data['config_stock_checkout'] = $this->config->get('config_stock_checkout');
        }

        if (isset($this->request->post['config_affiliate_group_id'])) {
            $data['config_affiliate_group_id'] = $this->request->post['config_affiliate_group_id'];
        } else {
            $data['config_affiliate_group_id'] = $this->config->get('config_affiliate_group_id');
        }

        if (isset($this->request->post['config_affiliate_approval'])) {
            $data['config_affiliate_approval'] = $this->request->post['config_affiliate_approval'];
        } elseif ($this->config->has('config_affiliate_approval')) {
            $data['config_affiliate_approval'] = $this->config->get('config_affiliate_approval');
        } else {
            $data['config_affiliate_approval'] = '';
        }

        if (isset($this->request->post['config_affiliate_auto'])) {
            $data['config_affiliate_auto'] = $this->request->post['config_affiliate_auto'];
        } elseif ($this->config->has('config_affiliate_auto')) {
            $data['config_affiliate_auto'] = $this->config->get('config_affiliate_auto');
        } else {
            $data['config_affiliate_auto'] = '';
        }

        if (isset($this->request->post['config_affiliate_commission'])) {
            $data['config_affiliate_commission'] = $this->request->post['config_affiliate_commission'];
        } elseif ($this->config->has('config_affiliate_commission')) {
            $data['config_affiliate_commission'] = $this->config->get('config_affiliate_commission');
        } else {
            $data['config_affiliate_commission'] = '5.00';
        }

        if (isset($this->request->post['config_affiliate_id'])) {
            $data['config_affiliate_id'] = $this->request->post['config_affiliate_id'];
        } else {
            $data['config_affiliate_id'] = $this->config->get('config_affiliate_id');
        }

        if (isset($this->request->post['config_return_id'])) {
            $data['config_return_id'] = $this->request->post['config_return_id'];
        } else {
            $data['config_return_id'] = $this->config->get('config_return_id');
        }

        if (isset($this->request->post['config_return_status_id'])) {
            $data['config_return_status_id'] = $this->request->post['config_return_status_id'];
        } else {
            $data['config_return_status_id'] = $this->config->get('config_return_status_id');
        }

        $this->load->model('localisation/return_status');

        $data['return_statuses'] = $this->model_localisation_return_status->getReturnStatuses();

        if (isset($this->request->post['config_captcha'])) {
            $data['config_captcha'] = $this->request->post['config_captcha'];
        } else {
            $data['config_captcha'] = $this->config->get('config_captcha');
        }

        $this->load->model('setting/extension');

        $data['captchas'] = array();

        // Get a list of installed captchas
        $extensions = $this->model_setting_extension->getInstalled('captcha');

        foreach ($extensions as $code) {
            $this->load->language('extension/captcha/' . $code, 'extension');

            if ($this->config->get('captcha_' . $code . '_status')) {
                $data['captchas'][] = array(
                    'text' => $this->language->get('extension')->get('heading_title'),
                    'value' => $code
                );
            }
        }

        if (isset($this->request->post['config_captcha_page'])) {
            $data['config_captcha_page'] = $this->request->post['config_captcha_page'];
        } elseif ($this->config->has('config_captcha_page')) {
            $data['config_captcha_page'] = $this->config->get('config_captcha_page');
        } else {
            $data['config_captcha_page'] = array();
        }

        $data['captcha_pages'] = array();

        $data['captcha_pages'][] = array(
            'text' => $this->language->get('text_register'),
            'value' => 'register'
        );

        $data['captcha_pages'][] = array(
            'text' => $this->language->get('text_guest'),
            'value' => 'guest'
        );

        $data['captcha_pages'][] = array(
            'text' => $this->language->get('text_review'),
            'value' => 'review'
        );

        $data['captcha_pages'][] = array(
            'text' => $this->language->get('text_return'),
            'value' => 'return'
        );

        $data['captcha_pages'][] = array(
            'text' => $this->language->get('text_contact'),
            'value' => 'contact'
        );

        if (isset($this->request->post['config_logo'])) {
            $data['config_logo'] = $this->request->post['config_logo'];
        } else {
            $data['config_logo'] = $this->config->get('config_logo');
        }

        if (isset($this->request->post['config_logo']) && (is_file(DIR_IMAGE . $this->request->post['config_logo']) || $this->model_tool_image->imageIsExist($this->request->post['config_logo']))) {
            $data['logo'] = $this->model_tool_image->resize($this->request->post['config_logo'], 100, 100);
        } elseif ($this->config->get('config_logo') && (is_file(DIR_IMAGE . $this->config->get('config_logo')) || $this->model_tool_image->imageIsExist($this->config->get('config_logo')))) {
            $data['logo'] = $this->model_tool_image->resize($this->config->get('config_logo'), 100, 100);
        } else {
            $data['logo'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        }

        if (isset($this->request->post['config_icon'])) {
            $data['config_icon'] = $this->request->post['config_icon'];
        } else {
            $data['config_icon'] = $this->config->get('config_icon');
        }

        if (isset($this->request->post['config_icon']) && (is_file(DIR_IMAGE . $this->request->post['config_icon']) || $this->model_tool_image->imageIsExist($this->request->post['config_icon']))) {
            $data['icon'] = $this->model_tool_image->resize($this->request->post['config_icon'], 100, 100);
        } elseif ($this->config->get('config_icon') && (is_file(DIR_IMAGE . $this->config->get('config_icon')) || $this->model_tool_image->imageIsExist($this->config->get('config_icon')))) {
            $data['icon'] = $this->model_tool_image->resize($this->config->get('config_icon'), 100, 100);
        } else {
            $data['icon'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        }

        if (isset($this->request->post['config_mail_engine'])) {
            $data['config_mail_engine'] = $this->request->post['config_mail_engine'];
        } else {
            $data['config_mail_engine'] = $this->config->get('config_mail_engine');
        }

        if (isset($this->request->post['config_mail_parameter'])) {
            $data['config_mail_parameter'] = $this->request->post['config_mail_parameter'];
        } else {
            $data['config_mail_parameter'] = $this->config->get('config_mail_parameter');
        }

        if (isset($this->request->post['config_mail_smtp_hostname'])) {
            $data['config_mail_smtp_hostname'] = $this->request->post['config_mail_smtp_hostname'];
        } else {
            $data['config_mail_smtp_hostname'] = $this->config->get('config_mail_smtp_hostname');
        }

        if (isset($this->request->post['config_mail_smtp_username'])) {
            $data['config_mail_smtp_username'] = $this->request->post['config_mail_smtp_username'];
        } else {
            $data['config_mail_smtp_username'] = MAIL_SMTP_USERNAME;
        }

        if (isset($this->request->post['config_mail_smtp_password'])) {
            $data['config_mail_smtp_password'] = $this->request->post['config_mail_smtp_password'];
        } else {
            $data['config_mail_smtp_password'] = MAIL_SMTP_PASSWORD;
        }

        if (isset($this->request->post['config_mail_smtp_port'])) {
            $data['config_mail_smtp_port'] = $this->request->post['config_mail_smtp_port'];
        } elseif ($this->config->has('config_mail_smtp_port')) {
            $data['config_mail_smtp_port'] = $this->config->get('config_mail_smtp_port');
        } else {
            $data['config_mail_smtp_port'] = 25;
        }

        if (isset($this->request->post['config_mail_smtp_timeout'])) {
            $data['config_mail_smtp_timeout'] = $this->request->post['config_mail_smtp_timeout'];
        } elseif ($this->config->has('config_mail_smtp_timeout')) {
            $data['config_mail_smtp_timeout'] = $this->config->get('config_mail_smtp_timeout');
        } else {
            $data['config_mail_smtp_timeout'] = 5;
        }

        if (isset($this->request->post['config_mail_alert'])) {
            $data['config_mail_alert'] = $this->request->post['config_mail_alert'];
        } elseif ($this->config->has('config_mail_alert')) {
            $data['config_mail_alert'] = $this->config->get('config_mail_alert');
        } else {
            $data['config_mail_alert'] = array();
        }

        $data['mail_alerts'] = array();

        $data['mail_alerts'][] = array(
            'text' => $this->language->get('text_mail_account'),
            'value' => 'account'
        );

        $data['mail_alerts'][] = array(
            'text' => $this->language->get('text_mail_affiliate'),
            'value' => 'affiliate'
        );

        $data['mail_alerts'][] = array(
            'text' => $this->language->get('text_mail_order'),
            'value' => 'order'
        );

        $data['mail_alerts'][] = array(
            'text' => $this->language->get('text_mail_review'),
            'value' => 'review'
        );

        if (isset($this->request->post['config_mail_alert_email'])) {
            $data['config_mail_alert_email'] = $this->request->post['config_mail_alert_email'];
        } else {
            $data['config_mail_alert_email'] = $this->config->get('config_mail_alert_email');
        }

        if (isset($this->request->post['config_secure'])) {
            $data['config_secure'] = $this->request->post['config_secure'];
        } else {
            $data['config_secure'] = $this->config->get('config_secure');
        }

        if (isset($this->request->post['config_shared'])) {
            $data['config_shared'] = $this->request->post['config_shared'];
        } else {
            $data['config_shared'] = $this->config->get('config_shared');
        }

        if (isset($this->request->post['config_robots'])) {
            $data['config_robots'] = $this->request->post['config_robots'];
        } else {
            $data['config_robots'] = $this->config->get('config_robots');
        }

        if (isset($this->request->post['config_seo_url'])) {
            $data['config_seo_url'] = $this->request->post['config_seo_url'];
        } else {
            $data['config_seo_url'] = $this->config->get('config_seo_url');
        }

        if (isset($this->request->post['config_file_max_size'])) {
            $data['config_file_max_size'] = $this->request->post['config_file_max_size'];
        } elseif ($this->config->get('config_file_max_size')) {
            $data['config_file_max_size'] = $this->config->get('config_file_max_size');
        } else {
            $data['config_file_max_size'] = 300000;
        }

        if (isset($this->request->post['config_file_ext_allowed'])) {
            $data['config_file_ext_allowed'] = $this->request->post['config_file_ext_allowed'];
        } else {
            $data['config_file_ext_allowed'] = $this->config->get('config_file_ext_allowed');
        }

        if (isset($this->request->post['config_file_mime_allowed'])) {
            $data['config_file_mime_allowed'] = $this->request->post['config_file_mime_allowed'];
        } else {
            $data['config_file_mime_allowed'] = $this->config->get('config_file_mime_allowed');
        }

        if (isset($this->request->post['config_maintenance'])) {
            $data['config_maintenance'] = $this->request->post['config_maintenance'];
        } else {
            $data['config_maintenance'] = $this->config->get('config_maintenance');
        }

        if (isset($this->request->post['config_password'])) {
            $data['config_password'] = $this->request->post['config_password'];
        } else {
            $data['config_password'] = $this->config->get('config_password');
        }

        if (isset($this->request->post['config_encryption'])) {
            $data['config_encryption'] = $this->request->post['config_encryption'];
        } else {
            $data['config_encryption'] = $this->config->get('config_encryption');
        }

        if (isset($this->request->post['config_compression'])) {
            $data['config_compression'] = $this->request->post['config_compression'];
        } else {
            $data['config_compression'] = $this->config->get('config_compression');
        }

        if (isset($this->request->post['config_error_display'])) {
            $data['config_error_display'] = $this->request->post['config_error_display'];
        } else {
            $data['config_error_display'] = $this->config->get('config_error_display');
        }

        if (isset($this->request->post['config_error_log'])) {
            $data['config_error_log'] = $this->request->post['config_error_log'];
        } else {
            $data['config_error_log'] = $this->config->get('config_error_log');
        }

        if (isset($this->request->post['config_error_filename'])) {
            $data['config_error_filename'] = $this->request->post['config_error_filename'];
        } else {
            $data['config_error_filename'] = $this->config->get('config_error_filename');
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('setting/setting', $data));
    }

    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'setting/setting')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->request->post['config_meta_title']) {
            $this->error['meta_title'] = $this->language->get('error_meta_title');
        }

        if (!$this->request->post['config_name']) {
            $this->error['name'] = $this->language->get('error_name');
        }

        if ((utf8_strlen($this->request->post['config_owner']) < 3) || (utf8_strlen($this->request->post['config_owner']) > 64)) {
            $this->error['owner'] = $this->language->get('error_owner');
        }

        if ((utf8_strlen($this->request->post['config_address']) < 3) || (utf8_strlen($this->request->post['config_address']) > 256)) {
            $this->error['address'] = $this->language->get('error_address');
        }

        if ((utf8_strlen($this->request->post['config_email']) > 96) || !filter_var($this->request->post['config_email'], FILTER_VALIDATE_EMAIL)) {
            $this->error['email'] = $this->language->get('error_email');
        }

        if ((utf8_strlen($this->request->post['config_telephone']) < 3) || (utf8_strlen($this->request->post['config_telephone']) > 32)) {
            $this->error['telephone'] = $this->language->get('error_telephone');
        }

        if (!empty($this->request->post['config_customer_group_display']) && !in_array($this->request->post['config_customer_group_id'], $this->request->post['config_customer_group_display'])) {
            $this->error['customer_group_display'] = $this->language->get('error_customer_group_display');
        }

        if (!$this->request->post['config_limit_admin']) {
            $this->error['limit_admin'] = $this->language->get('error_limit');
        }

        if ($this->request->post['config_login_attempts'] < 1) {
            $this->error['login_attempts'] = $this->language->get('error_login_attempts');
        }

        if (!$this->request->post['config_voucher_min']) {
            $this->error['voucher_min'] = $this->language->get('error_voucher_min');
        }

        if (!$this->request->post['config_voucher_max']) {
            $this->error['voucher_max'] = $this->language->get('error_voucher_max');
        }

        if (!isset($this->request->post['config_processing_status'])) {
            $this->error['processing_status'] = $this->language->get('error_processing_status');
        }

        if (!isset($this->request->post['config_complete_status'])) {
            $this->error['complete_status'] = $this->language->get('error_complete_status');
        }

        if (!$this->request->post['config_error_filename']) {
            $this->error['log'] = $this->language->get('error_log_required');
        } elseif (preg_match('/\.\.[\/\\\]?/', $this->request->post['config_error_filename'])) {
            $this->error['log'] = $this->language->get('error_log_invalid');
        } elseif (substr($this->request->post['config_error_filename'], strrpos($this->request->post['config_error_filename'], '.')) != '.log') {
            $this->error['log'] = $this->language->get('error_log_extension');
        }

        if ((utf8_strlen($this->request->post['config_encryption']) < 32) || (utf8_strlen($this->request->post['config_encryption']) > 1024)) {
            $this->error['encryption'] = $this->language->get('error_encryption');
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

    public function theme()
    {
        if ($this->request->server['HTTPS']) {
            $server = HTTPS_CATALOG;
        } else {
            $server = HTTP_CATALOG;
        }

        // This is only here for compatibility with old themes.
        if ($this->request->get['theme'] == 'theme_default') {
            $theme = $this->config->get('theme_default_directory');
        } else {
            $theme = basename($this->request->get['theme']);
        }

        if (is_file(DIR_CATALOG . 'view/theme/' . $theme . '/image/' . $theme . '.png')) {
            $this->response->setOutput($server . 'catalog/view/theme/' . $theme . '/image/' . $theme . '.png');
        } else {
            $this->response->setOutput($server . 'image/no_image.png');
        }
    }

    public function usetheme()
    {
        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateUseTheme()) {
            /* call showtheme from login ONLY to avoid hack to use paid theme from ajax call */
            //$this->showtheme();

            /* if theme partner: allow */
            if ($this->validatePartnerTheme()) {
                $this->showtheme();
            }

            /* get old theme for deleting demo data */
            $old_theme = $this->config->get('config_theme');

            /* update current theme */
            $this->model_setting_setting->editSettingValue($code = 'config', $key = 'config_theme', $value = $this->request->post['theme'], $store_id = 0);

            // notice: force update loaded config (that is already saved saved to system/config.php before)
            $this->config->set('config_theme', $this->request->post['theme']);

            /* load demo data for theme */
            $this->loadThemeDemoData($this->request->post['theme'], $old_theme);

            /* init default config for just selected theme */
            $init_default_result = $this->load->controller('extension/module/theme_builder_config/initAllDefaultConfigsForCurrentTheme');
            $init_default_result = json_decode($init_default_result, true);
            if (!is_array($init_default_result) || !isset($init_default_result['code']) || $init_default_result['code'] !== 200) {
                // some error occurred. TODO: handle this...
            }

            header('Content-Type: application/json');
            $data = json_encode([
                'code' => 201,
                'message' => 'Post use theme success',
                'value' => $this->request->post['theme'],
                'image' => $this->getThemeImage($this->request->post['theme']),
            ]);

            //clear cookie viewed_products v3.4.3-Finetune
            if (isset($_COOKIE['_viewed_products'])) {
                unset($_COOKIE['_viewed_products']);
                setcookie('_viewed_products', null, -1, '/');
            }

            $this->response->setOutput($data);
            return;
        }

        /* on fail */
        header('Content-Type: application/json');
        $data = json_encode([
            'code' => 400,
            'message' => 'Post use theme failed'
        ]);

        $this->response->setOutput($data);
    }

    private function validateUseTheme()
    {
        if (!$this->user->hasPermission('modify', 'setting/setting')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!isset($this->request->post['theme'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

    public function showtheme()
    {
        $this->load->model('setting/setting');
        $this->load->model('setting/extension');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateShowTheme()) {
            $code = $this->request->post['theme'];
            $this->model_setting_extension->install('theme', $code);
            $theme_directoty = $code;
            if ($this->validatePartnerTheme()) {
                $theme_directoty = $this->getPartnerThemeDirectory($code);
            }

            $themeSettingValue = array(
                'theme_' . $code . '_image_wishlist_height' => '47',
                'theme_' . $code . '_image_wishlist_width' => '47',
                'theme_' . $code . '_image_compare_height' => '90',
                'theme_' . $code . '_image_compare_width' => '90',
                'theme_' . $code . '_image_related_height' => '80',
                'theme_' . $code . '_image_related_width' => '80',
                'theme_' . $code . '_image_additional_height' => '74',
                'theme_' . $code . '_image_additional_width' => '74',
                'theme_' . $code . '_image_product_height' => '228',
                'theme_' . $code . '_image_product_width' => '228',
                'theme_' . $code . '_image_popup_height' => '500',
                'theme_' . $code . '_image_popup_width' => '500',
                'theme_' . $code . '_image_thumb_height' => '228',
                'theme_' . $code . '_image_thumb_width' => '228',
                'theme_' . $code . '_image_category_height' => '80',
                'theme_' . $code . '_image_category_width' => '80',
                'theme_' . $code . '_product_description_length' => '100',
                'theme_' . $code . '_product_limit' => '15',
                'theme_' . $code . '_status' => '1',
                'theme_' . $code . '_directory' => $theme_directoty,
                'theme_' . $code . '_image_cart_width' => '47',
                'theme_' . $code . '_image_cart_height' => '47',
                'theme_' . $code . '_image_location_width' => '268',
                'theme_' . $code . '_image_location_height' => '50'
            );

            $this->model_setting_setting->updateThemeSettings('theme_' . $code, $themeSettingValue);

            return true;
        }

        return false;
    }

    private function validateShowTheme()
    {
        if (!$this->user->hasPermission('modify', 'setting/setting')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!isset($this->request->post['theme'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

    private function getPartnerThemeDirectory($theme)
    {
        $this->load->model('setting/setting');
        $shop_name = $this->model_setting_setting->getSettingValue('shop_name');
        // note: partner directory is built from shop_name with - gets replaced by _ as opencart style!
        $partner_directory = str_replace('-', '_', 'partner_' . $shop_name);
        $partner_directory_theme = $partner_directory . '/' . $theme;

        return $partner_directory_theme;
    }

    private function validatePartnerTheme()
    {
        // only support for partner
        if (!$this->user->hasPermission('modify', 'setting/setting')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        $this->load->model('user/user');
        $isDev = $this->model_user_user->getUserByDevelop($this->user->getId());
        if ($isDev) {
            $themeDirectory = $this->getPartnerThemeDirectory($this->request->post['theme']);
            if (is_dir(DIR_CATALOG . 'view/theme/' . $themeDirectory)) {
                return true;
            }
        }

        return false;
    }

    public function actionThemeDemoData()
    {
        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateActionThemeDemoData()) {
            $theme = $this->request->post['theme'];
            $action = $this->request->post['action'];
            $result = false;

            try {
                switch ($action) {
                    case 'delete':
                        $result = $this->deleteThemeDemoData($theme);
                        break;

                    case 'load':
                        $result = $this->loadThemeDemoData($theme, $old_theme = $theme); // theme not changed
                        break;

                    // TODO: move to theme_develop...
                    case 'deleteTheme':
                        $result = $this->deleteTheme();
                        break;
                }
            } catch (Exception $e) {
                $result = false;
            }

            header('Content-Type: application/json');
            $data = $result
                ? json_encode([
                    'code' => 201,
                    'message' => 'Post actionThemeDemoData success'
                ])
                : json_encode([
                    'code' => 400,
                    'message' => 'Post actionThemeDemoData failed'
                ]);

            $this->response->setOutput($data);
            return;
        }

        /* on fail */
        header('Content-Type: application/json');
        $data = json_encode([
            'code' => 400,
            'message' => 'Post actionThemeDemoData failed'
        ]);

        $this->response->setOutput($data);
    }

    private function getThemeImage($theme)
    {
        if ($this->request->server['HTTPS']) {
            $server = HTTPS_CATALOG;
        } else {
            $server = HTTP_CATALOG;
        }

        if (is_file(DIR_CATALOG . 'view/theme/' . $theme . '/image/' . $theme . '.png')) {
            return $server . 'catalog/view/theme/' . $theme . '/image/' . $theme . '.png';
        } else {
            return $server . 'image/no_image.png';
        }
    }

    private function validateActionThemeDemoData()
    {
        if (!isset($this->request->post['theme'])) {
            $this->error['theme'] = 'Missing theme code';
        }

        if (!isset($this->request->post['action']) || !in_array($this->request->post['action'], ['delete', 'load', 'deleteTheme', 'downloadTheme'])) {
            $this->error['action'] = 'Missing action or not supported (supported: "delete", "load", "Download)';
        }

        return !$this->error;
    }

    /**
     * delete theme demo data by theme (code)
     *
     * @param string $theme
     * @return bool
     */
    public function deleteThemeDemoData($theme)
    {
        /* update theme parent directory base on theme */
        $this->updateThemeParentDirectory($theme);

        try {
            /* delete products */
            $this->deleteThemeDemoDataForProduct();

            /* delete categories, collections */
            $file_config = $this->demo_data_path . $this->theme_parent_directory . $theme . '/demo_data_config.json';
            if (file_exists($file_config)) {
                // try if existed special theme: get categories by special shop theme config
                $config = file_get_contents($file_config);
                $config = json_decode($config, true);

                $this->deleteThemeDemoDataForCategory($config['categories']);
                $this->deleteThemeDemoDataForCollection($config['collections']);
            }

            // remove seo
            // auto deleted when deleting product, category, collection. TODO: remove...
            /*$seos = $this->model_catalog_product->getDataByTableName('seo_url');
            foreach ($seos as $seo) {
                $this->model_design_seo_url->deleteSeoUrl($seo['data_id']);
            }*/

            /* delete blog */
            $this->deleteThemeDemoDataForBlog();

            // remove table data demo. TODO: remove...
            try {
                $this->db->query('TRUNCATE TABLE ' . DB_PREFIX . 'demo_data');
            } catch (Exception $e) {
                // table "demo_data" may not be existed. Skipped!
            }

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    private function deleteThemeDemoDataForProduct()
    {
        $this->load->model('catalog/product');
        $this->model_catalog_product->deleteDemoProducts();
    }

    private function deleteThemeDemoDataForCategory($categories_remove)
    {
        $this->load->model('catalog/category');

        // remove category
        foreach ($categories_remove as $category_id) {
            $this->model_catalog_category->deleteCategory($category_id);
        }
    }

    private function deleteThemeDemoDataForCollection($collections_remove)
    {
        $this->load->model('catalog/collection');

        // remove collection
        foreach ($collections_remove as $collection_id) {
            // only delete collection not contains product
            if (!$this->model_catalog_collection->hasProducts($collection_id)) {
                $this->model_catalog_collection->deleteCollection($collection_id);
            }
        }
    }

    private function deleteThemeDemoDataForBlog()
    {
        $this->load->model('blog/blog');
        $this->model_blog_blog->deleteDemoBlogs();
    }

    /**
     * load theme demo data
     *
     * @param string $theme new theme (code) to change
     * @param string $old_theme for deleting old demo data belongs to old theme
     * @return bool
     */
    public function loadThemeDemoData($theme, $old_theme = '')
    {
        /* to load method from another controller: use only first parameter */
        if ($old_theme) {
            /* delete theme demo data */
            if (!$this->deleteThemeDemoData($old_theme)) {
                return false;
            }
        }

        $this->updateThemeParentDirectory($theme);
        if ('theme_group/' == $this->theme_parent_directory) {
            $theme = explode('_', $theme)[0];

            // default theme group tech if not found
            if (!in_array($theme, [
                'beauty',
                'fashion',
                'furniture',
            ])) {
                $theme = 'tech';
            }
        }

        try {
            /* hard delete deleted products whose id coincides new demo data */
            $file_config = $this->demo_data_path . $this->theme_parent_directory . $theme . '/demo_data_config.json';
            if (file_exists($file_config)) {
                // try if existed special theme: get categories by special shop theme config
                $config = file_get_contents($file_config);
                $config = json_decode($config, true);

                $this->hardDeleteDeletedProduct($config['products']);
            }

            /* load product, category, collection */
            $this->loadThemDemoDataForProduct($theme);
            $this->loadThemDemoDataForCategory($theme);
            $this->loadThemDemoDataForCollection($theme);

            /* load seo url */
            $this->loadThemDemoDataForSeoUrl($theme);

            /* load blog */
            $this->loadThemDemoDataForBlog($theme);

            /* load theme builder config */
            $this->loadThemDemoDataForThemeBuilderConfig($theme);

            $this->log->write(sprintf('switch theme from "%s" to "%s" success!', $old_theme, $theme));

            return true;
        } catch (Exception $e) {
            $this->log->write(sprintf('switch theme from "%s" to "%s" got error: %s', $old_theme, $theme, $e->getMessage()));
            return false;
        }
    }

    /**
     * update theme parent directory
     *
     * @param string $theme theme (code) to check
     * @return void
     */
    private function updateThemeParentDirectory($theme)
    {
        /* check if theme is special or not */
        $file_config = $this->demo_data_path . 'special_theme/' . $theme . '/demo_data_config.json';

        if (file_exists($file_config)) {
            $this->theme_parent_directory = 'special_theme/';
        } else {
            $this->theme_parent_directory = 'theme_group/';
        }
    }

    /**
     * hard delete deleted products whose id coincides new demo data
     * @param $product_ids
     */
    private function hardDeleteDeletedProduct($product_ids) {
        $this->load->model('catalog/product');
        $this->model_catalog_product->hardDeleteDeletedProducts($product_ids);
    }

    private function loadThemDemoDataForProduct($theme)
    {
        /* insert demo products */
        $file_name_sql = $this->demo_data_path . $this->theme_parent_directory . $theme . '/product.sql';
        executeSqlFileDueToShopDBPrefix($file_name_sql, $this->db);
    }

    private function loadThemDemoDataForCategory($theme)
    {
        /* insert demo categories */
        $file_name_sql = $this->demo_data_path . $this->theme_parent_directory . $theme . '/category.sql';
        executeSqlFileDueToShopDBPrefix($file_name_sql, $this->db);
    }

    private function loadThemDemoDataForCollection($theme)
    {
        /* insert demo collections */
        $file_name_sql = $this->demo_data_path . $this->theme_parent_directory . $theme . '/collection.sql';
        executeSqlFileDueToShopDBPrefix($file_name_sql, $this->db);
    }

    private function loadThemDemoDataForSeoUrl($theme)
    {
        /* insert demo seo urls */
        $file_name_sql = $this->demo_data_path . $this->theme_parent_directory . $theme . '/seo_url.sql';
        executeSqlFileDueToShopDBPrefix($file_name_sql, $this->db);
    }

    private function loadThemDemoDataForBlog($theme)
    {
        /* insert demo blogs */
        $file_name_sql = $this->demo_data_path . $this->theme_parent_directory . $theme . '/blog.sql';
        executeSqlFileDueToShopDBPrefix($file_name_sql, $this->db);
    }

    private function loadThemDemoDataForThemeBuilderConfig($theme)
    {
        /* check if existing theme builder config */
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT EXISTS (SELECT 1 FROM `{$DB_PREFIX}theme_builder_config` 
                WHERE `store_id` = '0'
                  AND `config_theme` = '{$theme}'
                  AND `code` = 'config') result";
        $query = $this->db->query($sql);
        $exists = $query->row['result'];
        if (!$exists) {
            /* insert theme builder configs */
            $file_name_sql = $this->demo_data_path . $this->theme_parent_directory . $theme . '/theme_builder_config.sql';
            executeSqlFileDueToShopDBPrefix($file_name_sql, $this->db);
        }
    }
}
