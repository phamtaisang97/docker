<?php

class ControllerCatalogCategory extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('catalog/product');

        $this->load->language('catalog/category');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/category');

        $this->getList();
    }

    public function add()
    {
        $this->load->language('catalog/product');

        $this->load->language('catalog/category');

        $this->document->setTitle($this->language->get('heading_title_add'));

        $this->load->model('catalog/category');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_category->addCategory($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/category', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getForm();
    }

    public function edit()
    {
        $this->load->language('catalog/product');
        $this->load->language('catalog/category');

        $this->document->setTitle($this->language->get('heading_title_edit'));

        $this->load->model('catalog/category');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            // var_dump($this->request->post);die;
            $this->model_catalog_category->editCategory($this->request->get['category_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/category', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getForm();
    }

    public function delete()
    {
        $this->load->language('catalog/category');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/category');
        $this->response->addHeader('Content-Type: application/json');
        $json = [
            'status' => false,
            'message' => $this->language->get('category_delete_error')
        ];
        try {
            if (isset($this->request->post['selected']) && $this->validateDelete()) {
                foreach ($this->request->post['selected'] as $category_id) {
                    $this->model_catalog_category->deleteCategory($category_id);
                }
                $json = [
                    'status' => true,
                    'message' => $this->language->get('category_delete_success')
                ];
                $this->response->setOutput(json_encode($json));
            } else {
                $this->response->setOutput(json_encode($json));
            }
        } catch (Exception $e) {
            $this->response->setOutput(json_encode($json));
        }

        if (isset($this->error['warning'])) {
            $json = [
                'status' => false,
                'message' => $this->error['warning']
            ];
        }
        $this->response->setOutput(json_encode($json));
    }

    public function destroy()
    {

        $this->response->addHeader('Content-Type: application/json');
        $json = [
            'status' => false,
            'message' => 'thất bại',
        ];
        if (isset($this->request->post['category_id'])) {
            $category_id = $this->request->post['category_id'];
            $this->load->model('catalog/category');
            $result = $this->model_catalog_category->deleteCategory($category_id);
            // if (!$result) {
            // 	$this->response->setOutput(json_encode(false));
            // 	return;
            // }
            $json = [
                'status' => true,
                'message' => 'thanh cong'
            ];
            $this->response->setOutput(json_encode($json));
            return;

        }
        $this->response->setOutput(json_encode($json));
    }

    public function repair()
    {
        $this->load->language('catalog/category');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/category');

        if ($this->validateRepair()) {
            $this->model_catalog_category->repairCategories();

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/category', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getList();
    }

    protected function getList()
    {
        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'name';
        }
        $filter_status = $this->getValueFromRequest('get', 'filter_status');
        $filter_name = $this->getValueFromRequest('get', 'filter_name');
        // var_dump($filter_name);die;
        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }
        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }
        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . $this->request->get['filter_name'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/category', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        $data['add'] = $this->url->link('catalog/category/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['delete'] = $this->url->link('catalog/category/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['repair'] = $this->url->link('catalog/category/repair', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['product'] = $this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['categories'] = array();

        $filter_data = array(
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin'),
            'filter_status' => $filter_status,
            'filter_name' => $filter_name,
        );

        $renderCategories = $this->model_catalog_category->renderCategories($this->session->data['user_token'], $url, $filter_data);
        $data['categories'] = $renderCategories['html'];
        $category_total = $renderCategories['total'];

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }
        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }
        $data['sort_name'] = $this->url->link('catalog/category', 'user_token=' . $this->session->data['user_token'] . '&sort=name' . $url, true);
        $data['sort_sort_order'] = $this->url->link('catalog/category', 'user_token=' . $this->session->data['user_token'] . '&sort=sort_order' . $url, true);

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }
        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }
        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . $this->request->get['filter_name'];
        }
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }
        // $list_category_main = $this->model_catalog_category->getMainCategory();

        $pagination = new CustomPaginate();
        $pagination->total = $category_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/category', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

        // Add jump to page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);

        $data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;
        // $data['list_category_main'] = $list_category_main;
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('catalog/category_list_append', $data));
        } else {
            $this->response->setOutput($this->load->view('catalog/category_list', $data));

        }
    }

    protected function getForm()
    {
        $data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = array();
        }

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('category_heading'),
            'href' => $this->url->link('catalog/category', 'user_token=' . $this->session->data['user_token'], true)
        );
        if (isset($this->request->get['category_id'])) {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('category_edit'),
                'href' => '',
            );
        } else {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('category_add'),
                'href' => ''
            );
        }

        $this->load->model('catalog/category');
        $listCategories = $this->model_catalog_category->getCategories();
        $data['list_categories'] = [];
        $current_category_id = -1;
        if (isset($this->request->get['category_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $current_category_id = $this->request->get['category_id'];
        }
        foreach ($listCategories as $value) {
            if ($value['category_id'] == $current_category_id) {
                continue;
            }
            $name = $this->removeChar($value, 'name', '>');
            $data['list_categories'][] = array(
                'category_id' => $value['category_id'],
                'name' => str_replace(' ', '', $name)
            );
        }
//        $data['list_categories'] = array_merge(['' => 'Danh mục cha'], $data['list_categories']);

        $this->load->model('catalog/product');

        if (isset($this->request->post['product_category'])) {
            $products = $this->request->post['product_category'];
        } elseif (isset($this->request->get['category_id'])) {
            $products = $this->model_catalog_product->getProductsByCategoryId($this->request->get['category_id']);
        } else {
            $products = array();
        }

        foreach ($products as $product) {
            if ($product) {
                $data['product_categories'][] = array(
                    'product_id' => $product['product_id'],
                    'name' => $product['name'],
                    'image' => $product['image']
                );
            }
        }

        $this->load->model('custom/common');
        $listMenu = $this->model_custom_common->getListMenu();
        $data['listMenu'] = $listMenu;
        $category_id = $this->getValueFromRequest('get', 'category_id');
        $data['category_menus'] = $this->model_custom_common->getCategoryMenu($category_id);

        $data['cancel'] = $this->url->link('catalog/category', 'user_token=' . $this->session->data['user_token'], true);

        if (!isset($this->request->get['category_id'])) {
            $data['action'] = $this->url->link('catalog/category/add', 'user_token=' . $this->session->data['user_token'], true);
        } else {
            $data['action'] = $this->url->link('catalog/category/edit', 'user_token=' . $this->session->data['user_token'] . '&category_id=' . $this->request->get['category_id'], true);
        }

        $data['user_token'] = $this->session->data['user_token'];

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        if (isset($this->request->post['category_description'])) {
            $data['category_description'] = $this->request->post['category_description'];
        } elseif (isset($this->request->get['category_id'])) {
            $data['category_description'] = $this->model_catalog_category->getCategoryDescriptions($this->request->get['category_id']);
        } else {
            $data['category_description'] = array();
        }

        if (isset($this->request->post['parent_id'])) {
            $data['parent_id'] = $this->request->post['parent_id'];
        } elseif (!empty($category_info)) {
            $data['parent_id'] = $category_info['parent_id'];
        } else {
            $data['parent_id'] = 0;
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($category_info)) {
            $data['status'] = $category_info['status'];
        } else {
            $data['status'] = true;
        }

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/category_form', $data));
    }

    protected function validateForm()
    {
        if (!$this->user->hasPermission('modify', 'catalog/category')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        // foreach ($this->request->post['category_description'] as $language_id => $value) {
        // 	if ((utf8_strlen($value['name']) < 1) || (utf8_strlen($value['name']) > 255)) {
        // 		$this->error['name'][$language_id] = $this->language->get('error_name');
        // 	}

        // 	// if ((utf8_strlen($value['meta_title']) < 1) || (utf8_strlen($value['meta_title']) > 255)) {
        // 	// 	$this->error['meta_title'][$language_id] = $this->language->get('error_meta_title');
        // 	// }
        // }

        // if (isset($this->request->get['category_id']) && $this->request->post['parent_id']) {
        // 	$results = $this->model_catalog_category->getCategoryPath($this->request->post['parent_id']);

        // foreach ($results as $result) {
        // 	if ($result['path_id'] == $this->request->get['category_id']) {
        // 		$this->error['parent'] = $this->language->get('error_parent');

        // 		break;
        // 	}
        // }
        // }

        // if ($this->request->post['category_seo_url']) {
        // 	$this->load->model('design/seo_url');

        // 	foreach ($this->request->post['category_seo_url'] as $store_id => $language) {
        // 		foreach ($language as $language_id => $keyword) {
        // 			if (!empty($keyword)) {
        // 				if (count(array_keys($language, $keyword)) > 1) {
        // 					$this->error['keyword'][$store_id][$language_id] = $this->language->get('error_unique');
        // 				}

        // 				$seo_urls = $this->model_design_seo_url->getSeoUrlsByKeyword($keyword);

        // 				foreach ($seo_urls as $seo_url) {
        // 					if (($seo_url['store_id'] == $store_id) && (!isset($this->request->get['category_id']) || ($seo_url['query'] != 'category_id=' . $this->request->get['category_id']))) {
        // 						$this->error['keyword'][$store_id][$language_id] = $this->language->get('error_keyword');

        // 						break;
        // 					}
        // 				}
        // 			}
        // 		}
        // 	}
        // }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

    protected function validateDelete()
    {
        if (!$this->user->hasPermission('modify', 'catalog/category')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        $this->load->model('catalog/product');

        foreach ($this->request->post['selected'] as $category_id) {
            $product_total = count($this->model_catalog_product->getProductsByCategoryId($category_id));

            if ($product_total) {
                $this->error['warning'] = sprintf($this->language->get('error_category'), $product_total);
            }
        }

        return !$this->error;
    }

    protected function validateRepair()
    {
        if (!$this->user->hasPermission('modify', 'catalog/category')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function autocomplete()
    {
        $json = array();

        if (isset($this->request->get['filter_name'])) {
            $this->load->model('catalog/category');

            $filter_data = array(
                'filter_name' => $this->request->get['filter_name'],
                'sort' => 'name',
                'order' => 'ASC',
                'start' => 0,
                'limit' => 5
            );

            $results = $this->model_catalog_category->getCategories($filter_data);

            foreach ($results as $result) {
                $json[] = array(
                    'category_id' => $result['category_id'],
                    'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                );
            }
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function autocompleteProduct()
    {
        $json = array();
        if (isset($this->request->get['filter_name'])) {
            $this->load->model('catalog/product');
            $this->load->model('tool/image');
            $filter_data = ['filter_name' => $this->request->get['filter_name']];
            $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
            $filter_data = array(
                'filter_name' => $this->request->get['filter_name'],
                'sort' => 'name',
                'order' => 'ASC',
                'start' => 0,
                'limit' => 6,
                'start' => ($page - 1) * 6,
            );
            $listProduct = $this->model_catalog_product->getProducts($filter_data);
            foreach ($listProduct as $key => $value) {
                $json[] = array(
                    'product_id' => $value['product_id'],
                    'name' => strip_tags(html_entity_decode($value['name'], ENT_QUOTES, 'UTF-8')),
                    'image' => $value['image'] ? $value['image'] : $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height')),
                    'date_added' => $value['date_added'],
                    'date_modified' => $value['date_modified']
                );
            }
            $sort_order = array();
            foreach ($json as $key => $value) {
                $sort_order[$key] = $value['name'];
            }
        }

        array_multisort($sort_order, SORT_ASC, $json);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function autocompleteProducOrdert()
    {
        $json = array();
        if (isset($this->request->get['filter_name'])) {
            $this->load->model('catalog/product');
            $this->load->model('tool/image');
            $filter_data = ['filter_name' => $this->request->get['filter_name']];
            $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
            //display 6 product in select config theme is many, or setup other config
            $filter_data = array(
                'filter_name' => $this->request->get['filter_name'],
                'sort' => 'name',
                'order' => 'ASC',
                'start' => 0,
                'limit' => 6,
                'start' => ($page - 1) * 6,
            );
            $listProduct = $this->model_catalog_product->getProducts($filter_data);
            foreach ($listProduct as $key => $value) {
                $json[] = array(
                    'product_id' => $value['product_id'],
                    'name' => strip_tags(html_entity_decode($value['name'], ENT_QUOTES, 'UTF-8')),
                    'image' => $value['image'] ? $value['image'] : $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height')),
                    'price' => round($value['price']),
                    'quantity' => $value['quantity'],
                );
            }
            $sort_order = array();
            foreach ($json as $key => $value) {
                $sort_order[$key] = $value['name'];
            }
        }

        array_multisort($sort_order, SORT_ASC, $json);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function addCategoryFast() {
        $this->load->language('catalog/category');
        if (isset($this->request->post['text'])) {
            $this->load->model('catalog/category');
            $result = $this->model_catalog_category->addCategoryFast($this->request->post['text']);
            if ($result) {
                return json_encode(array(
                    "status" => true,
                    "message" => $this->language->get('category_add_success'),
                ));
            }
        }
        return json_encode(array(
            "status" => false,
            "message" => $this->language->get('category_add_error'),
        ));
    }

    public function deleteProductsCategory()
    {
        $this->load->language('catalog/category');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/category');
        $this->response->addHeader('Content-Type: application/json');
        $json = [
            'status' => false,
            'message' => $this->language->get('category_delete_product_error')
        ];
        try {
            if (isset($this->request->post['category_id'])) {
                $product_id = isset($this->request->post['product_id']) && $this->request->post['product_id'] > 0 ? $this->request->post['product_id'] : null;
                $this->model_catalog_category->deleteProductsCategory($this->request->post['category_id'], $product_id);
                $json = [
                    'status' => true,
                    'message' => $this->language->get('category_delete_product_success')
                ];
                $this->response->setOutput(json_encode($json));
            } else {
                $this->response->setOutput(json_encode($json));
            }
        } catch (Exception $e) {
            $this->response->setOutput(json_encode($json));
        }

        if (isset($this->error['warning'])) {
            $json = [
                'status' => false,
                'message' => $this->error['warning']
            ];
        }
        $this->response->setOutput(json_encode($json));
    }

    public function addProductCategory()
    {
        $this->load->language('catalog/category');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/category');
        $this->response->addHeader('Content-Type: application/json');
        $json = [
            'status' => false,
            'message' => $this->language->get('category_add_product_error')
        ];
        try {
            if (isset($this->request->post['category_id']) && isset($this->request->post['product_id'])) {
                $this->model_catalog_category->addProductCategory($this->request->post['category_id'], $this->request->post['product_id']);
                $json = [
                    'status' => true,
                    'message' => $this->language->get('category_add_product_success')
                ];
                $this->response->setOutput(json_encode($json));
            } else {
                $this->response->setOutput(json_encode($json));
            }
        } catch (Exception $e) {
            $this->response->setOutput(json_encode($json));
        }

        if (isset($this->error['warning'])) {
            $json = [
                'status' => false,
                'message' => $this->error['warning']
            ];
        }
        $this->response->setOutput(json_encode($json));
    }
}
