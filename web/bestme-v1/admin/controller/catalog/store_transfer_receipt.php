<?php


class ControllerCatalogStoreTransferReceipt extends Controller
{
    use Device_Util;
    private $error = array();

    public function index()
    {
        $this->load->language('catalog/store_transfer_receipt');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/store_transfer_receipt');

        $this->getList();
    }

    public function add()
    {
        $this->load->language('catalog/store_transfer_receipt');
        $this->document->setTitle($this->language->get('heading_title_add'));
        $this->load->model('catalog/store_transfer_receipt');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_store_transfer_receipt->addStoreTransferReceipt($this->request->post);

            if (isset($this->request->post['type_submit']) && $this->request->post['type_submit'] == ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_DRAFT) {
                $this->session->data['success'] = $this->language->get('text_success_draft');
            } else {
                $this->session->data['success'] = $this->language->get('text_success_add');
            }
            $this->response->redirect($this->url->link('catalog/store_transfer_receipt', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();

    }

    public function edit()
    {
        $this->load->language('catalog/store_transfer_receipt');
        $this->document->setTitle($this->language->get('heading_title_edit'));
        $this->load->model('catalog/store_transfer_receipt');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_store_transfer_receipt->editStoreTransferReceipt($this->request->get['store_transfer_receipt_id'], $this->request->post);

            if (isset($this->request->post['type_submit']) && $this->request->post['type_submit'] == ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_TRANSFERED) {
                $this->session->data['success'] = $this->language->get('text_success_add');
            } else if (isset($this->request->post['type_submit']) && $this->request->post['type_submit'] == ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_RECIVED){
                $this->session->data['success'] = $this->language->get('text_success_edit');
            } else if (isset($this->request->post['type_submit']) && $this->request->post['type_submit'] == ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_CANCELED) {
                $this->session->data['success'] = $this->language->get('text_success_cancel');
            }

            $this->response->redirect($this->url->link('catalog/store_transfer_receipt', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();
    }

    public function getList()
    {
        $this->load->model('setting/store');
        $filter_status = $this->getValueFromRequest('get', 'filter_status');
        $filter_name = $this->getValueFromRequest('get', 'filter_name');
        $filter_store_export = $this->getValueFromRequest('get', 'filter_store_export');
        $filter_store_import = $this->getValueFromRequest('get', 'filter_store_import');
        $page = $this->getValueFromRequest('get', 'page', 1);
        $filter_data = $this->getValueFromRequest('get', 'filter_data');

        $url = '';
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data = array();

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => ''
        );
        $url = '';
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['add'] = $this->url->link('catalog/store_transfer_receipt/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['access_modify'] = $this->user->hasPermission('modify', 'catalog/store_transfer_receipt');

        if (!empty($filter_data)) {
            if (!empty($filter_status)) {
                if (count($filter_status) > 0) {
                    $filter_status = implode(',', ($filter_status));
                } else {
                    $filter_status = (int)$filter_status[0];
                }
            } else {
                $filter_status = '';
            }
            if (!empty($filter_store_export)) {
                if (count($filter_store_export) > 0) {
                    $filter_store_export = implode(',', ($filter_store_export));
                } else {
                    $filter_store_export = (int)$filter_store_export[0];
                }
            } else {
                $filter_store_export = '';
            }
            if (!empty($filter_store_import)) {
                if (count($filter_store_import) > 0) {
                    $filter_store_import = implode(',', ($filter_store_import));
                } else {
                    $filter_store_import = (int)$filter_store_import[0];
                }
            } else {
                $filter_store_import = '';
            }
        }

        $data['loadOptionOne'] = $this->url->link('catalog/store_transfer_receipt/loadoptionone', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $filter_data = array(
            'filter_data' => $filter_data,
            'filter_name' => $filter_name,
            'filter_status' => $filter_status,
            'filter_store_export' => $filter_store_export,
            'filter_store_import' => $filter_store_import,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );
        if (isset($this->request->get['start_export']) && !empty($this->request->get['start_export'])) {
            $filter_data['start_export'] = $this->request->get['start_export'];
            $filter_data['end_export'] = $this->request->get['end_export'];
        }
        if (isset($this->request->get['start_import']) && !empty($this->request->get['start_import'])) {
            $filter_data['start_import'] = $this->request->get['start_import'];
            $filter_data['end_import'] = $this->request->get['end_import'];
        }

        $this->load->model('user/user');
        $user_id = $this->user->getId();

        if (!$this->user->canAccessAll()) {
            $user_store_ids = $this->model_user_user->getStoreIdsOffUser($user_id);
            $filter_data['user_store_id'] = implode(",", $user_store_ids);
            $filter_data['current_user_id'] = (int)$user_id;
        }

        $store_transfer_receipt_total = $this->model_catalog_store_transfer_receipt->getTotalStoreTransferReceipts($filter_data);

        $results = $this->model_catalog_store_transfer_receipt->getStoreTransferReceiptsCustom($filter_data);

        $CHANGEABLE_STORE_TRANSFER_RECEIPT_STATUS = [
            // draft
            ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_DRAFT => [
                [
                    'receipt_status_id' => ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_TRANSFERED,
                    'receipt_status_description' => $this->language->get('store_transfer_receipt_status_transfered')
                ],
                [
                    'receipt_status_id' => ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_CANCELED,
                    'receipt_status_description' => $this->language->get('txt_btn_cancel')
                ]
            ],
            // transfered
            ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_TRANSFERED => [
                [
                    'receipt_status_id' => ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_RECIVED,
                    'receipt_status_description' => $this->language->get('store_transfer_receipt_status_recived')
                ],
                [
                    'receipt_status_id' => ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_CANCELED,
                    'receipt_status_description' => $this->language->get('txt_btn_cancel')
                ]
            ],
            // received
            ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_RECIVED => [],
            // cancelled
            ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_CANCELED => [],
        ];

        $data['store_transfer_receipts'] = array();
        foreach ($results as $result) {
            $description_status = $this->convertStatusReceipt($result['status']);

            $data['store_transfer_receipts'][] = array(
                'store_transfer_receipt_id'             => $result['store_transfer_receipt_id'],
                'code'                                  => $result['code'],
                'export_store'                          => $this->model_setting_store->getStoreNameById($result['export_store_id']),
                'import_store'                          => $this->model_setting_store->getStoreNameById($result['import_store_id']),
                'status'                                => $result['status'],
                'description_status'                    => $description_status,
                'store_transfer_receipt_status'         => $CHANGEABLE_STORE_TRANSFER_RECEIPT_STATUS[$result['status']],
                'date_exported'                         => $result['date_exported'],
                'date_imported'                         => $result['date_imported'],
                'total'                                 => number_format($result['total'], 0, '', ','),
                'edit'                                  => $this->url->link('catalog/store_transfer_receipt/edit', 'user_token=' . $this->session->data['user_token'] . '&store_transfer_receipt_id=' . $result['store_transfer_receipt_id'], $url, true)
            );
        }

        $data['user_token'] = $this->session->data['user_token'];
        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $store_transfer_receipt_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/store_receipt', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        // Add jump to page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);
        $data['results'] = sprintf($this->language->get('text_pagination'), ($store_transfer_receipt_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($store_transfer_receipt_total - $this->config->get('config_limit_admin'))) ? $store_transfer_receipt_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $store_transfer_receipt_total, ceil($store_transfer_receipt_total / $this->config->get('config_limit_admin')));

        if (isset($this->session->data['success']) && $this->session->data['success'] != 'text_success') {
            $data['success'] = $this->session->data['success'];

            $this->session->data['success'] = '';
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->session->data['store_transfer_receipts'])) {
            $data['store_transfer_receipts_action'] = json_encode($this->session->data['store_transfer_receipts']);
        }

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];

            $this->session->data['error'] = '';
            unset($this->session->data['error']);
        } else {
            $data['error_warning'] = '';
        }

        // for auto building selected filter in view
        if (isset($this->request->get['filter_status'])) {
            $data['filter_store_transfer_receipt_status'] = $this->request->get['filter_status'];
        }
        $data['url_filter'] = $this->url->link('catalog/store_transfer_receipt', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['update_receipt_status_url'] = $this->url->link('catalog/store_transfer_receipt/updateStatus', 'user_token=' . $this->session->data['user_token'], true);

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $data['is_on_mobile'] = $this->isMobile();
        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('catalog/store_transfer_receipt_append', $data));
        } else {
            $this->response->setOutput($this->load->view('catalog/store_transfer_receipt', $data));
        }
    }

    public function updateStatus()
    {
        $this->load->language('catalog/store_transfer_receipt');

        $this->load->model('catalog/store_transfer_receipt');

        $receiptId = $this->request->post['receipt_id'];
        $receiptStatusId = $this->request->post['receipt_status_id'];

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateFormUpdateStatus()) {
            $this->model_catalog_store_transfer_receipt->updateReceiptStatus($receiptId, $receiptStatusId);

            if (isset($this->request->post['receipt_status_id']) && $this->request->post['receipt_status_id'] == ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_TRANSFERED) {
                $this->session->data['success'] = $this->language->get('text_success_add');
            } else if (isset($this->request->post['receipt_status_id']) && $this->request->post['receipt_status_id'] == ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_RECIVED){
                $this->session->data['success'] = $this->language->get('text_success_edit');
            } else if (isset($this->request->post['receipt_status_id']) && $this->request->post['receipt_status_id'] == ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_CANCELED) {
                $this->session->data['success'] = $this->language->get('text_success_cancel');
            }
        }

        $this->getList();
    }

    public function loadoptionone()
    {
        $this->load->language('catalog/store_transfer_receipt');
        $this->load->model('setting/store');
        $categoryID = $this->request->post['categoryID'];
        $end_select_html = '</select></div>';
        $show_html = '';

        if ($categoryID > 0) {
            if ($categoryID == 1) { // store_export
                $start_select_html = '<div class="form-group hierarchical-child" id="hierarchical-store-export">
                                        <select class="form-control form-control-sm select2" 
                                        data-placeholder="' . $this->language->get('filter_store_export_placeholder') . '" 
                                        data-label="' . $this->language->get('filter_store_export_placeholder') . '" 
                                        id="filter_store_export" 
                                        name="filter_store_export">';
                $end_select_html = '</select></div>';
                $getListStore = $this->model_setting_store->getStoresForUser([]);
                $show_html .= $start_select_html . '<option value="" data-value="' . $this->language->get('filter_store_export') . '">' . $this->language->get('filter_store_export_placeholder') . '</option>';
                foreach ($getListStore as $key => $vl) {
                    $show_html .= '<option value="' . $vl['id'] . '" data-value="' . $vl['text'] . '">' . $vl['text'] . '</option>';
                }
                $show_html .= $end_select_html;
            } elseif ($categoryID == 2) { // store_import
                $start_select_html = '<div class="form-group hierarchical-child" id="hierarchical-store-import">
                                        <select class="form-control form-control-sm select2" 
                                        data-placeholder="' . $this->language->get('filter_store_import_placeholder') . '" 
                                        data-label="' . $this->language->get('filter_store_import_placeholder') . '" 
                                        id="filter_store_import" 
                                        name="filter_store_import">';
                $end_select_html = '</select></div>';
                $getListStore = $this->model_setting_store->getStores();
                $show_html .= $start_select_html . '<option value="" data-value="' . $this->language->get('filter_store_import') . '">' . $this->language->get('filter_store_import_placeholder') . '</option>';
                foreach ($getListStore as $key => $vl) {
                    $show_html .= '<option value="' . $vl['store_id'] . '" data-value="' . $vl['name'] . '">' . $vl['name'] . '</option>';
                }
                $show_html .= $end_select_html;
            }elseif ($categoryID == 3) { // Status
                $show_html = '<div class="form-group hierarchical-child" id="hierarchical-status">
                                            <select class="form-control form-control-sm select2"
                                             data-placeholder="' . $this->language->get('filter_status_placeholder') . '" 
                                             data-label="' . $this->language->get('filter_status') . '" 
                                             id="filter_status" 
                                             name="filter_status">';
                $show_html .= '<option value="">' . $this->language->get('filter_status_placeholder') . '</option>';
                $show_html .= '<option value="0" data-value="' . $this->language->get('filter_status_draft') . '">' . $this->language->get('filter_status_draft') . '</option>';
                $show_html .= '<option value="1" data-value="' . $this->language->get('filter_status_transfered') . '">' . $this->language->get('filter_status_transfered') . '</option>';
                $show_html .= '<option value="2" data-value="' . $this->language->get('filter_status_recived') . '">' . $this->language->get('filter_status_recived') . '</option>';
                $show_html .= '<option value="9" data-value="' . $this->language->get('filter_status_canceled') . '">' . $this->language->get('filter_status_canceled') . '</option>';
                $show_html .= $end_select_html;
            }elseif ($categoryID == 4){ // exported_date
                $show_html .= '<div class="form-group hierarchical-child" id="hierarchical-date">
                                            <select class="form-control form-control-sm select2"
                                             data-placeholder="' . $this->language->get('filter_created_at_placeholder') . '" 
                                             data-label="' . $this->language->get('filter_date_range_export') . '" 
                                             id="filter_exported_at_type" 
                                             name="filter_exported_at_type">';
                $show_html .= '<option value="">' . $this->language->get('filter_created_at_placeholder') . '</option>';
                $show_html .= '<option value="today" data-value="' . $this->language->get('filter_created_at_today') . '">' . $this->language->get('filter_created_at_today') . '</option>';
                $show_html .= '<option value="week" data-value="' . $this->language->get('filter_created_at_this_week') . '">' . $this->language->get('filter_created_at_this_week') . '</option>';
                $show_html .= '<option value="month" data-value="' . $this->language->get('filter_created_at_this_month') . '">' . $this->language->get('filter_created_at_this_month') . '</option>';
                $show_html .= '<option value="option" data-value="' . $this->language->get('filter_created_at_option') . '">' . $this->language->get('filter_created_at_option') . '</option>';
                $show_html .= '</select>';
                $show_html .= '<div class="input-group-icon mt-3 w-100 d-none filter_exported_at_value">
                            <i class="bg-secondary icon icon-calendar ml-n3" style=" top: -50px;"></i>
                            <input type="text" name="filter_exported_at_from"
                                   class="form-control date-picker date-picker-store-receipt-filter mt-3"
                                   placeholder="' . $this->language->get('filter_select_date_from') . '" data-label="' . $this->language->get('filter_txt_date') . '"
                                   data-value="">
                            <i class="bg-secondary icon icon-calendar ml-n3" style=" top: 50px;"></i>
                            <input type="text" name="filter_exported_at_to"
                                   class="form-control date-picker date-picker-store-receipt-filter mt-3"
                                   placeholder="' . $this->language->get('filter_select_date_to') . '" data-label="' . $this->language->get('filter_txt_date') . '"
                                   data-value="">
                          </div>';
            }elseif ($categoryID == 5){ // Imported_date
                $show_html .= '<div class="form-group hierarchical-child" id="hierarchical-date">
                                            <select class="form-control form-control-sm select2"
                                             data-placeholder="' . $this->language->get('filter_imported_date_placeholder') . '" 
                                             data-label="' . $this->language->get('filter_date_range_import') . '" 
                                             id="filter_imported_at_type" 
                                             name="filter_imported_at_type">';
                $show_html .= '<option value="">' . $this->language->get('filter_created_at_placeholder') . '</option>';
                $show_html .= '<option value="today" data-value="' . $this->language->get('filter_created_at_today') . '">' . $this->language->get('filter_created_at_today') . '</option>';
                $show_html .= '<option value="week" data-value="' . $this->language->get('filter_created_at_this_week') . '">' . $this->language->get('filter_created_at_this_week') . '</option>';
                $show_html .= '<option value="month" data-value="' . $this->language->get('filter_created_at_this_month') . '">' . $this->language->get('filter_created_at_this_month') . '</option>';
                $show_html .= '<option value="option" data-value="' . $this->language->get('filter_created_at_option') . '">' . $this->language->get('filter_created_at_option') . '</option>';
                $show_html .= '</select>';
                $show_html .= '<div class="input-group-icon mt-3 w-100 d-none filter_imported_at_value">
                            <i class="bg-secondary icon icon-calendar ml-n3" style=" top: -50px;"></i>
                            <input type="text" name="filter_imported_at_from"
                                   class="form-control date-picker date-picker-store-receipt-filter mt-3"
                                   placeholder="' . $this->language->get('filter_select_date_from') . '" data-label="' . $this->language->get('filter_txt_date') . '"
                                   data-value="">
                            <i class="bg-secondary icon icon-calendar ml-n3" style=" top: 50px;"></i>
                            <input type="text" name="filter_imported_at_to"
                                   class="form-control date-picker date-picker-store-receipt-filter mt-3"
                                   placeholder="' . $this->language->get('filter_select_date_to') . '" data-label="' . $this->language->get('filter_txt_date') . '"
                                   data-value="">
                          </div>';
            }
        } else {
            $show_html .= '<style>#optionone,#optiontwo{display:none !important}</style>';
        }

        echo $show_html .= '';
    }

    public function convertStatusReceipt($staus)
    {
        $description_status = '';
        $this->load->language('catalog/store_transfer_receipt');
        switch ($staus) {
            case ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_DRAFT :
                $description_status = $this->language->get('store_transfer_receipt_status_draft');
                break;
            case ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_TRANSFERED :
                $description_status = $this->language->get('store_transfer_receipt_status_transfered');
                break;
            case ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_RECIVED :
                $description_status = $this->language->get('store_transfer_receipt_status_recived');
                break;
            case ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_CANCELED :
                $description_status = $this->language->get('store_transfer_receipt_status_canceled');
                break;
        }
        return $description_status;
    }

    public function getForm()
    {
        $data['is_edit'] = isset($this->request->get['store_transfer_receipt_id']) ? 1 : 0;

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];

            $this->session->data['error'] = '';
            unset($this->session->data['error']);
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success']) && $this->session->data['success'] != 'text_success') {
            $data['success'] = $this->session->data['success'];

            $this->session->data['success'] = '';
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data['text_form'] = !isset($this->request->get['store_transfer_receipt_id']) ? $this->language->get('heading_title_add') : $this->language->get('heading_title_edit');
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_store'),
            'href' => $this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('breadcrumb_transfer'),
            'href' => $this->url->link('catalog/store_transfer_receipt', 'user_token=' . $this->session->data['user_token'], true)
        );


        $this->load->model('catalog/store_transfer_receipt');
        $this->load->model('catalog/product');

        // data store
        $this->load->model('setting/store');
        $stores = $this->model_setting_store->getStores();
        $data['stores'] = $stores;

        $data['store_transfer_receipt'] = array();
        $data['store_transfer_receipt_to_product'] = array();
        $current_user_id = $this->user->getId();

        if (!isset($this->request->get['store_transfer_receipt_id'])) {
            $data['action'] = $this->url->link('catalog/store_transfer_receipt/add', 'user_token=' . $this->session->data['user_token'], true);

            if (isset($this->request->post['export_store'])) {
                $data['store_transfer_receipt']['export_store_id'] = $this->request->post['export_store'];
            }
            if (isset($this->request->post['import_store'])) {
                $data['store_transfer_receipt']['import_store_id'] = $this->request->post['import_store'];
            }
            if (isset($this->request->post['common_quantity_transfer'])) {
                $data['store_transfer_receipt']['common_quantity_transfer'] = (float)$this->request->post['common_quantity_transfer'];
            }
            if (isset($this->request->post['total_value'])) {
                $total_value = (isset($this->request->post['total_value']) && is_array($this->request->post['total_value'])) ? $this->request->post['total_value'] : [];
                $total = 0;
                foreach ($total_value as $value) {
                    $total += (float)$value;
                }

                $data['store_transfer_receipt']['total'] = (float)$total;
            }

            $data['user_create_id'] = isset($current_user_id) ? $current_user_id : 1;

            if (isset($this->request->post['product_version_id'])) {
                $product_version_ids = is_array($this->request->post['product_version_id']) ? $this->request->post['product_version_id'] : [];
                foreach ($product_version_ids as $key => $pvid) {
                    $arr_pvid = explode('-', $pvid);
                    $product_id = isset($arr_pvid[0]) ? $arr_pvid[0] : '';
                    $version_id = isset($arr_pvid[1]) ? $arr_pvid[1] : '';

                    if (!$product_id){
                        continue;
                    }

                    $product_name = $this->model_catalog_product->getProductNameById($product_id);
                    $product_sku = $this->model_catalog_product->getProductSKUById($product_id);

                    if ($version_id) {
                        $product_version_info = $this->model_catalog_product->getProductVersionByProductIdAndVersionId($product_id, $version_id);
                        if (isset($product_version_info['sku']) && $product_version_info['sku']) {
                            $product_sku = $product_version_info['sku'];
                        }

                        if (isset($product_version_info['version']) && $product_version_info['version']) {
                            $product_name .= '(' . $this->language->get('text_version') . $product_version_info['version'] . ')';
                        }

                        if (!is_array($product_version_info) || !array_key_exists('deleted', $product_version_info)) {
                            $product_deleted = 1;
                        } else {
                            $product_deleted = $product_version_info['deleted'];
                        }
                    } else {
                        if ($this->model_catalog_product->checkProductSingleVersionExist($product_id) > 0) {
                            $product_deleted = 0;
                        } else {
                            $product_deleted = 1;
                        }
                    }

                    $current_qty = 0;
                    if (isset($this->request->post['export_store'])) {
                        $result = $this->model_catalog_store_transfer_receipt->getProductInProductToStore($this->request->post['export_store'], $product_id, $version_id);
                        $current_qty = isset($result['quantity']) ? $result['quantity'] : (isset($this->request->post['product_pre_quantity'][$key]) ? extract_number($this->request->post['product_pre_quantity'][$key]) : 0);
                    }

                    $data['store_transfer_receipt_to_product'][] = array(
                        'product_id'                => $product_id,
                        'product_version_id'        => $version_id,
                        'quantity'                  => isset($this->request->post['product_quantity'][$key]) ? extract_number($this->request->post['product_quantity'][$key]) : 0,
                        'current_quantity'          => $current_qty,
                        'price'                     => isset($this->request->post['product_cost_price'][$key]) ? extract_number($this->request->post['product_cost_price'][$key]) : 0,
                        'sku'                       => $product_sku,
                        'selected_id'               => $pvid,
                        'deleted'                   => $product_deleted,
                        'name'                      => $product_name,
                        'total_value'               => isset($this->request->post['product_cost_price'][$key]) ? extract_number($this->request->post['product_cost_price'][$key]) : 0,
                    );
                }
            }

        } else {
            $data['action'] = $this->url->link('catalog/store_transfer_receipt/edit', 'user_token=' . $this->session->data['user_token'] . '&store_transfer_receipt_id=' . $this->request->get['store_transfer_receipt_id'], true);
            $data['store_transfer_receipt_id'] = $this->request->get['store_transfer_receipt_id'];

            // data store_transfer_receipt
            $store_transfer_receipt =  $this->model_catalog_store_transfer_receipt->getStoreTransferReceiptById($this->request->get['store_transfer_receipt_id']);

            $data['store_transfer_receipt'] = $store_transfer_receipt;

            // data product
            $store_transfer_receipt_to_product =  $this->model_catalog_store_transfer_receipt->getStoreTransferReceiptToProductById($this->request->get['store_transfer_receipt_id']);
            $total = 0;
            foreach ($store_transfer_receipt_to_product as &$product) {
                $pvid = $product['product_id'];
                $product_name = $this->model_catalog_product->getProductNameById($product['product_id']);
                $product['sku'] = $this->model_catalog_product->getProductSKUById($product['product_id']);
                $product['selected_id'] = $product['product_id'];
                $product['price']  = (float)$product['price']; //number_format($product['price'], 0, '', ',');

                if ($product['product_version_id']){
                    $pvid .= '-'.$product['product_version_id'];
                    $product_version_info = $this->model_catalog_product->getProductVersionByProductIdAndVersionId($product['product_id'], $product['product_version_id']);
                    if (isset($product_version_info['sku']) && $product_version_info['sku']){
                        $product['sku'] = $product_version_info['sku'];
                    }

                    if (isset($product_version_info['version']) && $product_version_info['version']){
                        $product_name .= '(' . $this->language->get('text_version') . $product_version_info['version'] . ')';
                    }

                    if (!is_array($product_version_info) || !array_key_exists('deleted', $product_version_info)){
                        $product['deleted'] = 1;
                    }else{
                        $product['deleted'] = $product_version_info['deleted'];
                    }
                }else{
                    if ($this->model_catalog_product->checkProductSingleVersionExist($product['product_id']) > 0) {
                        $product['deleted'] = 0;
                    } else {
                        $product['deleted'] = 1;
                    }
                }
                $product['name'] = $product_name;
                $product['selected_id'] = $pvid;
                $product['total_value'] = (float)$product['price'] * $product['quantity'];
                if (isset($store_transfer_receipt['status']) &&
                    $store_transfer_receipt['status'] == ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_DRAFT) {
                    $result = $this->model_catalog_store_transfer_receipt->getProductInProductToStore($store_transfer_receipt['export_store_id'], $product['product_id'], $product['product_version_id']);
                    $product['current_quantity'] =  isset($result['quantity']) ? $result['quantity'] : $product['current_quantity'];
                    $product['price'] =  isset($result['cost_price']) ? (float)$result['cost_price'] : (float)$product['price'];
                    $product['total_value'] = $product['price'] * $product['quantity'];
                    $total += $product['total_value'];
                }
            }
            unset($pvid);
            $data['store_transfer_receipt_to_product'] = $store_transfer_receipt_to_product;
            if (isset($store_transfer_receipt['status']) &&
                $store_transfer_receipt['status'] == ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_DRAFT) {
                $data['store_transfer_receipt']['total'] = $total;
            }

            if(isset($store_transfer_receipt['user_create_id'])){
                $data['user_create_id'] = $store_transfer_receipt['user_create_id'];
            } else {
                $data['user_create_id'] = isset($current_user_id) ? $current_user_id : 1;
            }
        }

        $data['access_modify'] = $this->user->hasPermission('modify', 'catalog/store_transfer_receipt');

        $data['cancel'] = $this->url->link('catalog/store_transfer_receipt', 'user_token=' . $this->session->data['user_token'], true);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $data['user_token'] = $this->session->data['user_token'];

        $this->response->setOutput($this->load->view('catalog/store_transfer_receipt_form', $data));
    }

    public function getStoreLazyLoad()
    {
        $json = array();

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $this->load->model('setting/store');

        $filter_data = array(
            'order' => 'ASC',
            'start' => ($page - 1) * 6,
            'limit' => 6
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        $filter_data['store_ids'] = isset($this->request->get['store_ids']) ? $this->request->get['store_ids'] : [];

        if (isset($this->request->get['name_select']) &&
            !empty($this->request->get['name_select']) &&
            $this->request->get['name_select'] == "import_store") {
            $json['results'] = $this->model_setting_store->getStorePaginator($filter_data);

            $count_store = $this->model_setting_store->getTotalStoreFilter($filter_data);
            $json['count'] = $count_store;
        } else {
            $json['results'] = $this->model_setting_store->getStoresForUser($filter_data);

            $count_store = $this->model_setting_store->getTotalStoreForUser($filter_data);
            $json['count'] = $count_store;
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getProductsLazyLoad()
    {
        $json = array();
        $count_tag = 0;

        $this->load->language('catalog/store_receipt');
        $this->load->model('catalog/store_transfer_receipt');
        $this->load->model('tool/image');

        $store_id = isset($this->request->get['store']) ? $this->request->get['store'] : '';
        if ($store_id == '') {
            return;
        }
        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        // display 6 product in select config theme is many, or setup other config
        $filter_data = array(
            'limit' => 6,
            'start' => ($page - 1) * 6,
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        $user_id = $this->user->getId();
        if (!$this->user->canAccessAll()) {
            $filter_data['current_user_id'] = (int)$user_id;
        }

        if (!$this->user->hasPermission('modify', 'catalog/product')) {
            $json['count'] = $count_tag;
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));

            return;
        }

        $results = $this->model_catalog_store_transfer_receipt->getProductByStore($store_id, $filter_data);
        foreach ($results as $key => $value) {
            $version_name = implode(' • ', explode(',', $value['version']));
            if ($version_name != '') {
                $version_name = $this->language->get('text_version') . $version_name;
            }

            if ($value['image'] != '') {
                $image = $this->model_tool_image->resize($value['image'], 100, 100);
            } else {
                $image = $this->model_tool_image->resize('no_image.png', 100, 100);
            }

            $json['results'][] = array(
                'id' => $value['id'],
                'product_id' => $value['product_id'],
                'product_version_id' => $value['product_version_id'],
                'version' => $version_name,
                'subname' => $version_name,
                'text' => $value['name'],
                'product_name' => $value['product_name'],
                'image' => $image,
                'weight' => number_format($value['weight'], 0, '', ''),
                'price' => number_format($value['price'], 0, '', ','),
                'quantity' => $value['quantity'],
                'sale_on_out_of_stock' => $value['sale_on_out_of_stock'],
                'status' => $value['status'],
                'sku' => $value['sku'],
                'cost_price' => (float)$value['cost_price']
            );
        }

        $count_tag = $this->model_catalog_store_transfer_receipt->countProductByStore($store_id, $filter_data);
        $json['count'] = $count_tag;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function validateForm()
    {
        $this->load->language('catalog/store_transfer_receipt');
        $this->load->model('catalog/store_transfer_receipt');
        $this->load->model('catalog/product');
        if (!$this->user->hasPermission('modify', 'catalog/store_transfer_receipt')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!isset($this->request->post['export_store']) ||
            !isset($this->request->post['import_store']) ||
            !isset($this->request->post['product_version_id']) ||
            !isset($this->request->post['product_quantity']) || !isset($this->request->post['type_submit']) ) {
            $this->error['warning'] = $this->language->get('error_store_transfer_receipt_input_data_is_invalid');
        }

        if ($this->request->post['type_submit'] == ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_CANCELED) {
            return !$this->error;
        }

        if (isset($this->request->post['product_version_id'])) {
            $product_version_ids = is_array($this->request->post['product_version_id']) ? $this->request->post['product_version_id'] : [];
            foreach ($product_version_ids as $key => $pvid) {
                $arr_pvid = explode('-', $pvid);
                $product_id = isset($arr_pvid[0]) ? $arr_pvid[0] : '';
                $version_id = isset($arr_pvid[1]) ? $arr_pvid[1] : 0;

                if (!$product_id){
                    continue;
                }

                $product_trans_quantity = isset($this->request->post['product_quantity'][$key]) ? extract_number($this->request->post['product_quantity'][$key]) : 0;
                $product_current_quantity_trans = isset($this->request->post['current_quantity_trans'][$key]) ? extract_number($this->request->post['current_quantity_trans'][$key]) : 0;
                $result = $this->model_catalog_store_transfer_receipt->getProductInProductToStore($this->request->post['export_store'], $product_id, $version_id);

                if (isset($result['quantity'])) {
                    $qty = (int)$result['quantity'] + (int)$product_current_quantity_trans;

                    if ((int)$qty < (int)$product_trans_quantity) {
                        $this->error['warning'] = $this->language->get('error_store_transfer_receipt_product_more_quantity');
                    }
                }

                if ($result['product_version_id']) {
                    $product_version_info = $this->model_catalog_product->getProductVersionByProductIdAndVersionId($result['product_id'], $result['product_version_id']);
                    if (!is_array($product_version_info) || !array_key_exists('deleted', $product_version_info) || $product_version_info['deleted'] == 1){
                        $this->error['warning'] = $this->language->get('error_store_transfer_receipt_product_is_deleted');
                        $this->session->data['error'] = $this->language->get('error_store_transfer_receipt_product_is_deleted');
                    }
                } else {
                    if ($this->model_catalog_product->checkProductSingleVersionExist($result['product_id']) == 0 &&
                        $this->request->post['receipt_status_id'] != ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_CANCELED) {
                        $this->error['warning'] = $this->language->get('error_store_transfer_receipt_product_is_deleted');
                        $this->session->data['error'] = $this->language->get('error_store_transfer_receipt_product_is_deleted');
                    }
                }
            }
        }

        return !$this->error;
    }

    private function validateFormUpdateStatus()
    {
        $this->load->language('catalog/store_transfer_receipt');
        $this->load->model('catalog/store_transfer_receipt');
        $this->load->model('catalog/product');
        if (!$this->user->hasPermission('modify', 'catalog/store_transfer_receipt')) {
            $this->error['warning'] = $this->language->get('error_permission');
            $this->session->data['error'] = $this->language->get('error_permission');
        }

        if (!isset($this->request->post['receipt_id'])) {
            $this->error['warning'] = $this->language->get('error_update_receipt_status_receipt_id');
            $this->session->data['error'] = $this->language->get('error_update_receipt_status_receipt_id');
        }

        if (!isset($this->request->post['receipt_status_id'])) {
            $this->error['warning'] = $this->language->get('error_update_receipt_status_receipt_status_id');
            $this->session->data['error'] = $this->language->get('error_update_receipt_status_receipt_status_id');
        }

        $store_transfer_receipt = $this->model_catalog_store_transfer_receipt->getStoreTransferReceiptById($this->request->post['receipt_id']);
        $products = $this->model_catalog_store_transfer_receipt->getStoreTransferReceiptToProductById($this->request->post['receipt_id']);

        if (!isset($store_transfer_receipt)) {
            $this->error['warning'] = $this->language->get('error_store_transfer_receipt_not_exist');
            $this->session->data['error'] = $this->language->get('error_store_transfer_receipt_not_exist');
        }

        foreach ($products as $product) {
            $result = $this->model_catalog_store_transfer_receipt->getProductInProductToStore($store_transfer_receipt['export_store_id'], $product['product_id'], $product['product_version_id']);

            if (isset($result['quantity'])) {
                $qty = (int)$result['quantity'] + (int)$product['quantity'];

                if (((int)$qty < (int)$product['quantity']) &&
                    $this->request->post['receipt_status_id'] != ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_CANCELED) {
                    $this->error['warning'] = $this->language->get('error_store_transfer_receipt_product_more_quantity');
                    $this->session->data['error'] = $this->language->get('error_store_transfer_receipt_product_more_quantity');
                }
            }

            if ($product['product_version_id']) {
                $product_version_info = $this->model_catalog_product->getProductVersionByProductIdAndVersionId($product['product_id'], $product['product_version_id']);
                if (!is_array($product_version_info) || !array_key_exists('deleted', $product_version_info) || $product_version_info['deleted'] == 1){
                    $this->error['warning'] = $this->language->get('error_store_transfer_receipt_product_is_deleted');
                    $this->session->data['error'] = $this->language->get('error_store_transfer_receipt_product_is_deleted');
                }
            } else {
                if ($this->model_catalog_product->checkProductSingleVersionExist($product['product_id']) == 0 &&
                    $this->request->post['receipt_status_id'] != ModelCatalogStoreTransferReceipt::STORE_TRANSFER_RECEIPT_STATUS_ID_CANCELED) {
                    $this->error['warning'] = $this->language->get('error_store_transfer_receipt_product_is_deleted');
                    $this->session->data['error'] = $this->language->get('error_store_transfer_receipt_product_is_deleted');
                }
            }
        }

        return !$this->error;
    }
}