<?php

class Controllercatalogcollection extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('catalog/collection');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/collection');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_collection->editcollection($this->request->get['collection_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/collection', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getList();
    }

    public function add()
    {
        $this->load->language('catalog/collection');

        $this->document->setTitle($this->language->get('heading_title_add'));

        $this->load->model('catalog/collection');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $data['menu'] = isset($this->request->post['menu']) ? $this->request->post['menu'] : '';
            $data['title'] = trim(isset($this->request->post['title'])) ? trim($this->request->post['title']) : '';
            $data['description'] = isset($this->request->post['description']) ? $this->request->post['description'] : '';
            $data['status'] = isset($this->request->post['status']) ?$this->request->post['status'] : 0;
            $data['image'] = isset($this->request->post['image']) ? $this->request->post['image'] : '';
            $data['seo_name'] = trim(isset($this->request->post['seo_name'])) ? trim($this->request->post['seo_name']) : '';
            $data['body'] = isset($this->request->post['body']) ? $this->request->post['body'] : '';
            $data['alias'] = isset($this->request->post['alias']) ? $this->request->post['alias'] : '';
            $data['collection_list'] = isset($this->request->post['hidearray']) ? $this->request->post['hidearray'] : '';
            $data['type_select'] = isset($this->request->post['type_select']) ? $this->request->post['type_select'] : '';
            $data['cat_list'] = isset($this->request->post['cat_list']) ? $this->request->post['cat_list'] : '';
            if ($data['type_select'] == 1) {
                $data['cat_list'] = isset($this->request->post['collection_list']) ? $this->request->post['collection_list'] : '';
                $data['collection_list'] = isset($this->request->post['collection_list_hidden']) ? $this->request->post['collection_list_hidden'] : '';
            }

            if (isset($this->request->post['noindex'])) {
                $data['noindex'] = 1;
            }
            else {
                $data['noindex'] = 0;
            }

            if (isset($this->request->post['nofollow'])) {
                $data['nofollow'] = 1;
            }
            else {
                $data['nofollow'] = 0;
            }

            if (isset($this->request->post['noarchive'])) {
                $data['noarchive'] = 1;
            }
            else {
                $data['noarchive'] = 0;
            }

            if (isset($this->request->post['noimageindex'])) {
                $data['noimageindex'] = 1;
            }
            else {
                $data['noimageindex'] = 0;
            }

            if (isset($this->request->post['nosnippet'])) {
                $data['nosnippet'] = 1;
            }
            else {
                $data['nosnippet'] = 0;
            }

            $check_colletion = $this->model_catalog_collection->check_collection($data['title']);
            if($check_colletion == 0){
                if(isset($data['collection_list']) && $data['collection_list'] != ''){
                    $collection_id = $this->model_catalog_collection->addCollection($data);
                    //update data to menu
                    if ($collection_id && $data['menu']) {
                        $this->load->model('custom/menu');
                        $data_menu = [
                            'menu_name' => $data['title'],
                            'target_value' => $collection_id,
                            'menu_type_id' => ModelCustomMenu::MENU_TYPE_COLLECTION
                        ];
                        foreach ($data['menu'] as $menu) {
                            $data_menu['parent_id'] = $menu;
                            $this->model_custom_menu->insertMenu($data_menu);
                        }
                    }
                    $this->session->data['success'] = $this->language->get('text_success');
                    $this->response->redirect($this->url->link('catalog/collection', 'user_token=' . $this->session->data['user_token'] . $url='', true));
                }
                else{
                    $this->error['warning'] = $this->language->get('error_product');
                }
            }
            else{
                $this->error['error_text_check'] = $this->language->get('error_text_check_colletion');
            }
        }

        $this->getForm();

    }
    public function edit()
    {
        $this->load->language('catalog/collection');

        $this->document->setTitle($this->language->get('heading_title_edit'));

        $this->load->model('catalog/collection');
        // var_dump($this->request->server['REQUEST_METHOD']);die;
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $data['menu'] = isset($this->request->post['menu']) ? $this->request->post['menu'] : [];
            $data['title'] = isset($this->request->post['title']) ? $this->request->post['title'] : '';
            $data['description'] = isset($this->request->post['description']) ? $this->request->post['description'] : '';
            $data['status'] = $this->request->post['status'];
            $data['image'] = isset($this->request->post['image']) ? $this->request->post['image'] : '';
            $data['seo_name'] = isset($this->request->post['seo_name']) ? $this->request->post['seo_name'] : '';
            $data['body'] = isset($this->request->post['body']) ? $this->request->post['body'] : '';
            $data['hidearray'] = isset($this->request->post['hidearray']) ? $this->request->post['hidearray'] : '';
            $data['alias'] = isset($this->request->post['alias']) ? $this->request->post['alias'] : '';
            $data['cat_list'] = isset($this->request->post['cat_list']) ? $this->request->post['cat_list'] : '';
            $data['type_select'] = isset($this->request->post['type_select']) ? $this->request->post['type_select'] : '';
            if (isset($this->request->post['noindex'])) {
                $data['noindex'] = 1;
            }
            else {
                $data['noindex'] = 0;
            }

            if (isset($this->request->post['nofollow'])) {
                $data['nofollow'] = 1;
            }
            else {
                $data['nofollow'] = 0;
            }

            if (isset($this->request->post['noarchive'])) {
                $data['noarchive'] = 1;
            }
            else {
                $data['noarchive'] = 0;
            }

            if (isset($this->request->post['noimageindex'])) {
                $data['noimageindex'] = 1;
            }
            else {
                $data['noimageindex'] = 0;
            }

            if (isset($this->request->post['nosnippet'])) {
                $data['nosnippet'] = 1;
            }
            else {
                $data['nosnippet'] = 0;
            }
            if ($data['type_select'] == 1) {
                $data['cat_list'] = isset($this->request->post['collection_list']) ? $this->request->post['collection_list'] : '';
                $data['hidearray'] = isset($this->request->post['collection_list_hidden']) ? $this->request->post['collection_list_hidden'] : '';
            }

            // remove or edit menu collection
            $this->load->model('custom/menu');
            $old_menu = $this->model_custom_menu->getMenuParentIdByTarget(ModelCustomMenu::MENU_TYPE_COLLECTION, $this->request->get['collection_id']);
            if (is_array($old_menu)) {
                foreach ($old_menu as $item) {
                    if (!isset($item['menu_id']) || !isset($item['parent_id'])) {
                        continue;
                    }
                    if (!is_array($data['menu'])) {
                        continue;
                    }
                    if (in_array($item['parent_id'], $data['menu'])) {
                        $data_menu = [
                            'menu_id' => $item['menu_id'],
                            'menu_name' => $data['title'],
                            'target_value' => $this->request->get['collection_id'],
                            'menu_type_id' => ModelCustomMenu::MENU_TYPE_COLLECTION,
                            'parent_id' => $item['parent_id']
                        ];
                        $this->model_custom_menu->updateMenu($data_menu);

                        if (($key = array_search($item['menu_id'], $data['menu'])) !== false) {
                            unset($data['menu'][$key]);
                        }
                    } else {
                        $this->model_custom_menu->removeFromMenu($item['parent_id'], ModelCustomMenu::MENU_TYPE_COLLECTION, $this->request->get['collection_id']);
                    }
                }
            }
            // update menu collection
            if ($data['menu']) {
                $data_menu = [
                    'menu_name' => $data['title'],
                    'target_value' => $this->request->get['collection_id'],
                    'menu_type_id' => ModelCustomMenu::MENU_TYPE_COLLECTION
                ];
                foreach ($data['menu'] as $menu) {
                    if ($this->model_custom_menu->isExistMenu($menu, $this->request->get['collection_id'])) continue;
                    $data_menu['parent_id'] = $menu;
                    $this->model_custom_menu->insertMenu($data_menu);
                }
            }
            // end menu collection

            $this->model_catalog_collection->editCollection($this->request->get['collection_id'], $data);
            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->session->data['success'] = $this->language->get('text_success_edit');
            $this->response->redirect($this->url->link('catalog/collection', 'user_token=' . $this->session->data['user_token'] . $url, true));
            // var_dump(444);die;

        }

        $this->getForm();
    }


    public function getCollectionLazyLoad() {
        $json = array();

        if (!$this->user->hasPermission('access', 'catalog/product')) {
            $json['count'] = 0;
            $json['results'] = [];
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }

        $this->load->model('catalog/collection');
        $this->load->model('custom/collection');
        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        //display 6 product in select config theme is many, or setup other config
        $filter_data = array(
            'limit' => 6,
            'start' => ($page - 1) * 6,
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        if(!$this->user->canAccessAll()){
            $filter_data['current_user_id'] = $this->user->getId();
        }

        $results = $this->model_catalog_collection->getCollectionPaginator($filter_data);
        $this->load->model('tool/image');
        foreach ($results as $key => $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
            }
            $json['results'][] = array(
                'id' => $result['product_id'],
                'text' => $result['name'],
                'image' => $image,
                'url' => $this->url->link('catalog/product/edit', 'user_token=' . $this->session->data['user_token'] . '&product_id=' .$result['product_id'], true),
                'price' => $result['price'],
                'date_added' => $result['date_added'],
            );
        }


        $count_tag = $this->model_catalog_collection->countProductAll($filter_data);
        $json['count'] = $count_tag;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getCollectionListLazyLoad() {
        $json = array();
        $this->load->model('catalog/collection');
        $this->load->model('custom/collection');
        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        //display 6 product in select config theme is many, or setup other config
        $filter_data = array(
            'limit' => 6,
            'start' => ($page - 1) * 6,
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }
        $filter_data['type'] = 1;
        $results = $this->model_catalog_collection->getListCollection($filter_data);
        $this->load->model('tool/image');
        foreach ($results as $key => $result) {
            if ($result['image']) {
                $image = $result['image'];
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
            }
            $json['results'][] = array(
                'id' => $result['collection_id'],
                'text' => $result['title'],
                'image' => $image,
                'url' => $this->url->link('catalog/collection/edit', 'user_token=' . $this->session->data['user_token'] . '&collection_id=' .$result['collection_id'], true)
            );
        }


        $count_collection = $this->model_catalog_collection->getTotalCollections($filter_data);
        $json['count'] = $count_collection;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getAddList() {
        $json = array();
        $this->load->model('catalog/collection');
        $id_get = $this->request->get['id'];
        $extension =  explode(",", $id_get);
        $extension = end($extension);
        $results = $this->model_catalog_collection->getAddListProduct($id_get,'');
        foreach ($results as $key => $value) {
            $json[] = array(
                'id' => $value['product_id'],
                'text' => $value['name'],
                'image' => $value['image'],
            );
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    protected function getList()
    {
        /* current page */

        $filter_name = trim($this->getValueFromRequest('get', 'filter_name'));
        $filter_staff = $this->getValueFromRequest('get', 'filter_staff');
        $page = $this->getValueFromRequest('get', 'page', 1);

        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        /* breadcrumbs */
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/collection', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        /* message */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        /* filter */
        $data['loadOptionOne'] = $this->replace_url($this->url->link('catalog/collection/loadoptionone', 'user_token=' . $this->session->data['user_token'] . $url, true));

        $filter_category = null;
        if (isset($this->request->get['filter_category'])) {
            $filter_category = (int)($this->request->get['filter_category']);
            $url .= '&filter_category=' . $filter_category;
        }

        $filter_collection = null;
        if (isset($this->request->get['filter_collection'])) {
            $filter_collection = (int)($this->request->get['filter_collection']);
            $url .= $filter_collection;
        }

        $filter_status = null;
        if (isset($this->request->get['filter_status'])) {
            $filter_status = ($this->request->get['filter_status']);
            if (count($filter_status) > 1) {
                $filter_status = '';
            } else {
                $filter_status = (int)$filter_status[0];
            }
            $url .= '&filter_status=' . $filter_status;
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $filterStaff= null;
        $filter_staff = is_array($filter_staff) ? $filter_staff : [];
        if (!$this->user->canAccessAll()){
            $filter_staff[] = (int)$this->user->getId();
        }
        if (!empty($filter_staff)) {
            if (count($filter_staff) > 0) {
                $filterStaff = implode(',', ($filter_staff));
            } else {
                $filterStaff = (int)$filter_staff[0];
            }
        } else {
            $filterStaff = '';
        }

        $filter_data = array(
            'filter_name' => $filter_name,
            'filter_status' => $filter_status,
            'filter_staff' => $filterStaff,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        /* selected fields in filter */
        $data['filter_category'] = $filter_category;
        $data['filter_collection'] = $filter_collection;
        $data['filter_status'] = $filter_status;
        $data['is_admin'] = $this->user->isAdmin() ? 1 : 0;
        $data['current_user_id'] = (int)$this->user->getId();
        $data['can_modify'] = $this->user->hasPermission('modify', 'catalog/collection') ? 1 : 0;

        $this->load->model('tool/image');
        $results = $this->model_catalog_collection->getAllcollection($filter_data);

        foreach ($results as $result) {
            if ($result['image']) {
                $image = $result['image'];
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
            }
            if (isset($result['type']) && $result['type'] == 1) {
                $amoutProduct =$this->model_catalog_collection->getCountCollectionCollections($result['collection_id']);
            } else {
                $amoutProduct =$this->model_catalog_collection->getCountProductCollections($result['collection_id']);
            }
            $data['collections'][] = array(
                'collection_id' => $result['collection_id'],
                'title' => $result['title'],
                'image' => $image,
                'type' => isset($result['type']) ? $result['type'] : '',
                'amoutProduct' => $amoutProduct,
                'activeStatus' => $result['status'],
                'user_name' => $result['fullname'],
                'edit' => $this->url->link('catalog/collection/edit', 'user_token=' . $this->session->data['user_token'] . '&collection_id=' . $result['collection_id'] . $url, true),
            );
        }
        $data['user_token'] = $this->session->data['user_token'];
        $data['delete'] = $this->url->link('catalog/collection/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $collection_total = $this->model_catalog_collection->getTotalCollections($filter_data);

        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $collection_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/collection', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        // Add jump to page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);

        $data['results'] = sprintf($this->language->get('text_pagination'), ($collection_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($collection_total - $this->config->get('config_limit_admin'))) ? $collection_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $collection_total, ceil($collection_total / $this->config->get('config_limit_admin')));
        $data['url_filter'] = $this->url->link('catalog/collection', 'user_token=' . $this->session->data['user_token'] . $url, true);
        /* href */
        $data['href_add'] = $this->url->link('catalog/collection/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['href_edit'] = $this->url->link('catalog/collection', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['href_edit_quantity'] = $this->url->link('catalog/collection/editQuantity', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['href_edit_sale_on_out_of_stock'] = $this->url->link('catalog/collection/editSaleOnOutOfStock', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['avtivestatus'] = $this->replace_url($this->url->link('catalog/collection/avtivestatus',('user_token='.$this->session->data['user_token'].$url), true));
        $data['showProduct'] = $this->url->link('catalog/collection/ShowHideCollection&active=1', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['hideProduct'] = $this->url->link('catalog/collection/ShowHideCollection&active=0', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['delete'] = $this->url->link('catalog/collection/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('catalog/collection_list_append', $data));
        } else {
            $this->response->setOutput($this->load->view('catalog/collection_list', $data));
        }

    }

    protected function getForm()
    {
        $data['text_form'] = !isset($this->request->get['collection_id']) ? $this->language->get('heading_title_add') : $this->language->get('heading_title_edit');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        if (isset($this->error['error_text_check'])) {
            $data['error_text_check'] = $this->error['error_text_check'];
        } else {
            $data['error_text_check'] = '';
        }


        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = array();
        }

        if (isset($this->error['keyword'])) {
            $data['error_keyword'] = $this->error['keyword'];
        } else {
            $data['error_keyword'] = '';
        }

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/collection', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );
        if (isset($this->request->get['collection_id'])) {
            $data['breadcrumbs'][] = array(
                'text' => $data['text_form'],
                'href' => '',
            );
        } else {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_add'),
                'href' => ''
            );
        }
        if (!isset($this->request->get['collection_id'])) {
            $data['action'] = $this->url->link('catalog/collection/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        } else {
            $data['action'] = $this->url->link('catalog/collection/edit', 'user_token=' . $this->session->data['user_token'] . '&collection_id=' . $this->request->get['collection_id'] . $url, true);
            $data['collection_id']=$this->request->get['collection_id'] ;
        }

        $data['cancel'] = $this->url->link('catalog/collection', 'user_token=' . $this->session->data['user_token'] . $url, true);

        if (isset($this->request->get['collection_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $this->load->model('tool/image');
            $collection_info = $this->model_catalog_collection->getCollection($this->request->get['collection_id']);

            // check Permission staff
            if (!$this->user->canAccessAll()){
                if (isset($collection_info['user_create_id']) && $this->user->getId() != $collection_info['user_create_id']){
                    $this->response->redirect($this->url->link('catalog/collection', 'user_token=' . $this->session->data['user_token'] . $url='', true));
                }
            }

            $data['product_collection'] = $this->model_catalog_collection->getProductCollections('','',$this->request->get['collection_id']);
            $product_selected = [];
            foreach ($data['product_collection'] as $product) {
                if ($product['image']) {
                    $image = $this->model_tool_image->resize($product['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
                }
                array_push($product_selected, [
                    'id' => $product['product_id'],
                    'title' => $product['name'],
                    'text' => $product['name'],
                    'image' => $image,
                    'price' => $product['price'],
                    'date_added' => $product['date_added'],
                ]);
            }
            $data['product_selected'] = json_encode($product_selected, JSON_HEX_APOS);

            $data['collection_collection'] = $this->model_catalog_collection->getCollectionCollections('','',$this->request->get['collection_id']);
            $collection_selected = [];
            foreach ($data['collection_collection'] as &$collection) {
                if ($collection['image']) {
                    $image = $collection['image'];
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
                }
                $collection['image'] = $image;
                array_push($collection_selected, [
                    'id' => $collection['collection_id'],
                    'title' => $collection['title'],
                    'text' => $collection['title'],
                    'image' => $image,
                ]);
            }
            $data['collection_selected'] = json_encode($collection_selected, JSON_HEX_APOS);

            $str_productID = '';
            foreach ($data['product_collection'] as $key => $vl){
                $str_productID .= $vl['product_id'].',';
                if ($vl['image']) {
                    $image = $this->model_tool_image->resize($vl['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
                }
                $data['product_collection'][$key]['image'] = $image;
            }
            $data['product_edit'] = ','.substr($str_productID,0,-1);
        }

        if (isset($this->request->post['title'])) {
            $data['_title'] = $this->request->post['title'];
        } elseif (!empty($collection_info)) {
            $data['_title'] = $collection_info['title'];
        } else {
            $data['_title'] = '';
        }

        if (isset($this->request->post['description'])) {
            $data['description'] = $this->request->post['description'];
        } elseif (!empty($collection_info)) {
            $data['description'] = $collection_info['description'];
        } else {
            $data['description'] = '';
        }

        if (isset($this->request->post['cat_list'])) {
            $data['cat_list'] = $this->request->post['cat_list'];
        } elseif (!empty($collection_info)) {
            $data['cat_list'] = $collection_info['product_type_sort'];
        } else {
            $data['cat_list'] = '';
        }

        if (isset($this->request->post['type_select'])) {
            $data['type_select'] = $this->request->post['type_select'];
        } elseif (!empty($collection_info)) {
            $data['type_select'] = $collection_info['type'];
        } else {
            $data['type_select'] = '';
        }

        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($collection_info)) {
            $data['image'] = $collection_info['image'];
        } else {
            $data['image'] = '';
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($collection_info)) {
            $data['status'] = $collection_info['status'];
        }else{
            $data['status'] = true;
        }

        if (isset($this->request->post['seo_name'])) {
            $data['meta_title'] = $this->request->post['seo_name'];
        } elseif (!empty($collection_info)) {
            $data['meta_title'] = $collection_info['meta_title'];
        } else {
            $data['meta_title'] = '';
        }
        if (isset($this->request->post['body'])) {
            $data['meta_description'] = $this->request->post['body'];
        } elseif (!empty($collection_info)) {
            $data['meta_description'] = $collection_info['meta_description'];
        } else {
            $data['meta_description'] = '';
        }
        if (isset($this->request->post['alias'])) {
            $data['alias'] = $this->request->post['alias'];
        } elseif (!empty($collection_info)) {
            $data['alias'] = $collection_info['alias'];
        } else {
            $data['alias'] = '';
        }

        if (isset($this->request->post['noindex'])) {
            $data['noindex'] = $this->request->post['noindex'];
        } elseif (!empty($collection_info)) {
            $data['noindex'] = $collection_info['noindex'];
        } else {
            $data['noindex'] = 0;
        }

        if (isset($this->request->post['nofollow'])) {
            $data['nofollow'] = $this->request->post['nofollow'];
        } elseif (!empty($collection_info)) {
            $data['nofollow'] = $collection_info['nofollow'];
        } else {
            $data['nofollow'] = 0;
        }

        if (isset($this->request->post['noarchive'])) {
            $data['noarchive'] = $this->request->post['noarchive'];
        } elseif (!empty($collection_info)) {
            $data['noarchive'] = $collection_info['noarchive'];
        } else {
            $data['noarchive'] = 0;
        }

        if (isset($this->request->post['noimageindex'])) {
            $data['noimageindex'] = $this->request->post['noimageindex'];
        } elseif (!empty($collection_info)) {
            $data['noimageindex'] = $collection_info['noimageindex'];
        } else {
            $data['noimageindex'] = 0;
        }

        if (isset($this->request->post['nosnippet'])) {
            $data['nosnippet'] = $this->request->post['nosnippet'];
        } elseif (!empty($collection_info)) {
            $data['nosnippet'] = $collection_info['nosnippet'];
        } else {
            $data['nosnippet'] = 0;
        }

        $data['user_token'] = $this->session->data['user_token'];

        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();
        $this->load->model('setting/store');

        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($collection_info)) {
            $data['image'] = $collection_info['image'];
        } else {
            $data['image'] = '';
        }

        // tag collection
        if (isset($this->request->get['collection_id'])) {
            //$this->load->model('custom/collection');
            //$data['collection_list'] = json_encode($this->model_custom_collection->getCollectionsByProductId($this->request->get['product_id']));
        }

        // menu collection
        if (isset($this->request->get['collection_id'])) {
            $this->load->model('custom/menu');
            $data['menu_selected'] = json_encode($this->model_custom_menu->getMenuParentInfoByTarget(ModelCustomMenu::MENU_TYPE_COLLECTION, $this->request->get['collection_id']), JSON_HEX_APOS);
        }

        // Image
        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($collection_info)) {
            $data['image'] = $collection_info['image'];
        } else {
            $data['image'] = '';
        }

        $this->load->model('tool/image');

        if (isset($this->request->post['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
        } elseif (!empty($product_info)) {
            $data['thumb'] = $this->model_tool_image->resize($product_info['image'], 100, 100);
        } else {
            $data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        }

        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);


        if(isset($data['product_collection'])){
            $data['product_collection'] = $data['product_collection'];
        }
        else{$data['product_collection'] = '';}

        $data['can_modify'] = $this->user->hasPermission('modify', 'catalog/collection') ? 1 : 0;
        $data['ckeditor_lang'] = $this->config->get('config_language') == 'vi-vn' ? 'vi' : 'en';
        $data['upload_image'] = str_replace('&amp;', '&', $this->url->link('catalog/product/upload', 'user_token=' . $this->session->data['user_token'] . $url, true));
        $data['filemanager'] = str_replace('&amp;', '&', $this->url->link('common/filemanager', 'user_token=' . $this->session->data['user_token'] . '&ckeditor=1', true));

        $data['server_name'] = $_SERVER['SERVER_NAME'];
        $data['video_collection'] = defined('BESTME_COLLECTION_GUIDE') ? BESTME_COLLECTION_GUIDE : '';
        $this->load->model('design/layout');
        $data['layouts'] = $this->model_design_layout->getLayouts();

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/collection_form', $data));
    }

    public function getAllProductSort(){
        $arr_product = $this->request->post['arr_product'];
        $value = $this->request->post['value'];
        $json = array();
        $this->load->model('catalog/collection');
        $results = $this->model_catalog_collection->getAddListProduct($arr_product,$value);
        foreach ($results as $key => $value) {
            $json[] = array(
                'id' => $value['product_id'],
                'text' => $value['name'],
                'image' => $value['image'],
            );
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }
    public  function getProductCollection($type = '',$val = '',$collection_id = '')
    {
        $this->load->model('catalog/collection');
        $type = (!empty($this->request->get['type']) ? $this->request->get['type'] : '');
        $html = '';
        $collection_id = (!empty($this->request->get['collection_id']) ? $this->request->get['collection_id'] : '');
        if(isset($collection_id) != 'undefined'){
            if($type == 'sort'){
                $value = (!empty($this->request->get['val']) ? $this->request->get['val'] : '');
                $data = $this->model_catalog_collection->getProductCollections('',$value,$collection_id);
                foreach ($data as $key => $value){
                    $html .= '<tr data-id="'.$value['product_id'].'" id="message_'.$value['product_id'].'">
                        <td width="50" class="font-weight-bold">'.($key+1).'</td>
                        <td width="85">
                              <img src="'.$value['image'].'">
                        </td>
                        <td><a href="javascript:;">'.$value['name'].'</a></td>
                        <td width="70">
                            <a class="d-inline-block sortable-button" href="javascript:void()"  onclick="getProductCollection(\'order\',this.value,'.$value['collection_id'].','.$value['product_collection_id'].')"><i class="fas fa-expand-arrows-alt text-pink mr-2 font-14"></i></a>
                            <a href="javascript:;" class="d-inline-block" onclick="callaction('.$value['product_collection_id'].')"><i class="icon icon-close font-12 bg-pink"></i></a>
                            <input type="hidden" name="id_product" value="'.$value['product_id'].'">
                        </td>
                    </tr>';
                }
                $html .= '';
            }
            elseif($type == 'order'){
                $collection_product_id = (!empty($this->request->get['p_collection']) ? $this->request->get['p_collection'] : '');
                $collection_id = $this->request->get['collection_id'];
                $this->model_catalog_collection->updateSortOrder($collection_product_id);
                $data = $this->model_catalog_collection->getProductCollections('','',$collection_id);
                foreach ($data as $key => $value){
                    $html .= '<tr data-id="'.$value['product_id'].'" id="message_'.$value['product_id'].'">
                        <td width="50" class="font-weight-bold">'.($key+1).'</td>
                        <td width="85">
                              <img src="'.$value['image'].'">
                        </td>
                        <td><a href="javascript:;">'.$value['name'].'</a></td>
                        <td width="70">
                            <a class="d-inline-block sortable-button" href="javascript:void()"  onclick="getProductCollection(\'order\',this.value,'.$value['collection_id'].','.$value['product_collection_id'].')"><i class="fas fa-expand-arrows-alt text-pink mr-2 font-14"></i></a>
                            <a href="javascript:;" class="d-inline-block" onclick="callaction('.$value['product_collection_id'].')"><i class="icon icon-close font-12 bg-pink"></i></a>
                            <input type="hidden" name="id_product" value="'.$value['product_id'].'">
                        </td>
                    </tr>';
                }
                $html .= '';
            }
        }

        $html .= '';
        echo $html;
    }

    public function getMenuLazyLoad()
    {
        $json = array();
        $this->load->model('custom/menu');
        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        //display 6 product in select config theme is many, or setup other config
        $filter_data = array(
            'limit' => 6,
            'start' => ($page - 1) * 6,
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }
        $results = $this->model_custom_menu->getListMenuPaginator($filter_data);

        foreach ($results['data'] as $key => $value) {
            $json['results'][] = array(
                'text' => $value['name'],
                'id' => $value['menu_id'],
                'level' => $value['level']
            );
        }
        $count_tag = isset($results['count']) ? $results['count'] : 0;
        $json['count'] = $count_tag;
        //array_multisort($sort_order, SORT_ASC, $json);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function loadoptionone()
    {
        $this->load->language('catalog/collection');
        $this->load->model('user/user');
        $categoryID = $this->request->post['categoryID'];
        $start_select_html = '<div class="form-group"><select class="form-control select2 form-control-sm" data-placeholder="' . $this->language->get('filter_status_placeholder') . '" data-label="' . $this->language->get('filter_status') . '" id="status_product" name="status_product">';
        $end_select_html = '</select></div>';
        $show_html = '';

        if ($categoryID > 0) {
            if ($categoryID == 1) { // Trang thai hien thi
                if (!isset($this->request->post['flag'])) {
                    $show_html .= $start_select_html;
                    $show_html .= '<option value="" data-value="' . $this->language->get('product_all_status') . '">' . $this->language->get('product_all_status') . '</option>';
                    $show_html .= '<option value="1" data-value="' . $this->language->get('entry_status_publish') . '">' . $this->language->get('entry_status_publish') . '</option>';
                    $show_html .= '<option value="0" data-value="' . $this->language->get('entry_status_unpublish') . '">' . $this->language->get('entry_status_unpublish') . '</option>';
                } else {
                    $show_html .= '<div class="form-group"><select class="form-control select2 form-control-sm" data-placeholder="' . $this->language->get('filter_status_placeholder') . '" data-label="' . $this->language->get('filter_status') . '" id="status_product" name="status_product">';
                    $show_html .= '<option value="">' . $this->language->get('filter_status') . '</option>';
                    $show_html .= '<option value="1" data-value="' . $this->language->get('entry_status_publish') . '">' . $this->language->get('entry_status_publish') . '</option>';
                    $show_html .= '<option value="0" data-value="' . $this->language->get('entry_status_unpublish') . '">' . $this->language->get('entry_status_unpublish') . '</option>';
                }
                $show_html .= $end_select_html;
            } elseif ($categoryID == 2) { // Nhân viên
                $start_select_html = '<div class="form-group"><select class="form-control select2 form-control-sm" data-placeholder="' . $this->language->get('filter_staff_placeholder') . '" data-label="' . $this->language->get('filter_staff') . '" id="staff" name="staff[]">';
                $end_select_html = '</select></div>';
                $getListStaff = $this->model_user_user->getUsers();
                $show_html .= $start_select_html . '<option value="" data-value="' . $this->language->get('filter_staff') . '">' . $this->language->get('filter_staff_placeholder') . '</option>';
                foreach ($getListStaff as $key => $vl) {
                    $show_html .= '<option value="' . $vl['user_id'] . '" data-value="' . $vl['lastname'] . ' ' . $vl['firstname'] . '">' . $vl['lastname'] . ' ' . $vl['firstname'] . '</option>';
                }
                $show_html .= $end_select_html;
            }
        } else {
            $show_html .= '<style>#optionone,#optiontwo{display:none !important}</style>';
        }

        echo $show_html .= '';
    }

    protected function validateForm($action = 'edit')
    {
        if (!$this->user->hasPermission('modify', 'catalog/collection')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        if ($action === 'editQuantity') {
            if (!isset($this->request->post['quantity']) || (int)($this->request->post['quantity']) < 0 || (int)($this->request->post['quantity']) > (10e11 - 1)) {
                $this->error['quantity'] = $this->language->get('error_quantity');
                $this->error['warning'] = $this->language->get('error_quantity');
            }
        }

        return !$this->error;
    }
    public function avtivestatus(){
        if (!$this->user->hasPermission('modify', 'catalog/collection')) {
            return;
        }
        $this->load->model('catalog/collection');
        $collection_id = (int)$this->request->get['collection_id'];
        $status = $this->request->get['status'];
        $this->model_catalog_collection->updateStatus($collection_id,$status);
    }
    public function replace_url($url){
        return str_replace("amp;","",$url);
    }
    public function ShowHideCollection($active = '')
    {
        if (!$this->user->hasPermission('modify', 'catalog/collection')) {
            return;
        }
        $this->load->language('catalog/collection');

        $this->load->model('catalog/collection');

        $collections = explode(',',$this->request->get['collection_id']);
        $active = $this->request->get['active'];
        if (isset($collections)) {
            foreach ($collections as $collection_id) {
                $this->model_catalog_collection->updateStatus($collection_id,$active);
            }
        }
        $this->session->data['success'] = $this->language->get('change_success');
        $this->response->redirect($this->url->link('catalog/collection', 'user_token=' . $this->session->data['user_token'] , true));


    }
    public function delete()
    {
        if (!$this->user->hasPermission('modify', 'catalog/collection')) {
            return;
        }
        $this->load->language('catalog/collection');
        $this->load->model('catalog/collection');
        $collections = explode(',',$this->request->get['collection_id']);
        if (isset($collections)) {
            foreach ($collections as $collection_id) {
                $this->model_catalog_collection->deleteCollection($collection_id);
            }
        }
        $this->session->data['success'] = $this->language->get('delete_success');
        $this->response->redirect($this->url->link('catalog/collection', 'user_token=' . $this->session->data['user_token'] , true));
    }
    public function callaction(){
        if (!$this->user->hasPermission('modify', 'catalog/collection')) {
            return;
        }
        $this->load->model('catalog/collection');
        $product_collection_id = $this->request->post['message_id'];
        $this->model_catalog_collection->deleteProductCollection($product_collection_id);
    }
}
