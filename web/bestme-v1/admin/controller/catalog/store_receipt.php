<?php
/**
 * Created by PhpStorm.
 * User: Microsoft Windows
 * Date: 12/03/2020
 * Time: 8:56 AM
 */

class ControllerCatalogStoreReceipt extends Controller
{
    use Device_Util;
    private $error = array();

    public function index()
    {
        $this->load->language('catalog/store_receipt');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/store_receipt');
        $this->load->model('catalog/manufacturer');

        $this->getList();
    }

    public function add()
    {
        $this->load->language('catalog/store_receipt');
        $this->document->setTitle($this->language->get('heading_title_add'));
        $this->load->model('catalog/store_receipt');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->request->post['fee_config'] = $_POST['fee_config'];
            $store_receipt_id = $this->model_catalog_store_receipt->addStoreReceipt($this->request->post);

            // Auto add payment_voucher
            $this->load->model('cash_flow/payment_voucher');
            $this->model_cash_flow_payment_voucher->autoCreateOrUpdatePaymentVoucher($store_receipt_id, $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success_add');
            $this->response->redirect($this->url->link('catalog/store_receipt', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();
    }

    public function edit()
    {
        $this->load->language('catalog/store_receipt');
        $this->document->setTitle($this->language->get('heading_title_edit'));
        $this->load->model('catalog/store_receipt');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->request->post['fee_config'] = $_POST['fee_config'];
            $this->model_catalog_store_receipt->editStoreReceipt($this->request->get['store_receipt_id'], $this->request->post);

            // Auto update payment_voucher
            $this->load->model('cash_flow/payment_voucher');
            $this->model_cash_flow_payment_voucher->autoCreateOrUpdatePaymentVoucher($this->request->get['store_receipt_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success_edit');
            $this->response->redirect($this->url->link('catalog/store_receipt', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();
    }

    public function getList()
    {
        $filter_status = $this->getValueFromRequest('get', 'filter_status');
        $filter_manufacturer = $this->getValueFromRequest('get', 'filter_manufacturer');
        $filter_name = $this->getValueFromRequest('get', 'filter_name');
        $page = $this->getValueFromRequest('get', 'page', 1);
        $filter_data = $this->getValueFromRequest('get', 'filter_data');

        $url = '';

        $data['is_on_mobile'] = $this->isMobile();

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/store_receipt', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        $data['add'] = $this->url->link('catalog/store_receipt/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['delete'] = $this->url->link('catalog/store_receipt/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['receive'] = $this->url->link('catalog/store_receipt/receive', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['list'] = $this->url->link('catalog/store_receipt', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['access_modify'] = $this->user->hasPermission('modify', 'catalog/store_receipt');

        $data['store_receipts'] = array();

        if (!empty($filter_data)) {
            if (isset($this->request->get['filter_status'])) {
                $filter_status = ($this->request->get['filter_status']);
                if (is_array($filter_status)) {
                    if (count($filter_status) > 1) {
                        $filter_status = '';
                    } else {
                        $filter_status = (int)$filter_status[0];
                    }
                }
            }
        }
        $data['loadOptionOne'] = $this->url->link('catalog/store_receipt/loadoptionone', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $filter_data = array(
            'filter_data' => $filter_data,
            'filter_name' => $filter_name,
            'filter_status' => $filter_status,
            'filter_manufacturer' => $filter_manufacturer,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        if (isset($this->request->get['start_time']) && !empty($this->request->get['start_time'])) {
            $filter_data['start_time'] = $this->request->get['start_time'];
            $filter_data['end_time'] = $this->request->get['end_time'];
        }

        $this->load->model('user/user');
        $user_id = $this->user->getId();

        if (!$this->user->canAccessAll()) {
            $user_store_ids = $this->model_user_user->getStoreIdsOffUser($user_id);
            $filter_data['user_store_id'] = implode(",", $user_store_ids);
            $filter_data['current_user_id'] = (int)$user_id;
        }

        $STORE_RECEIPT_STATUS = [
            // hàng đang về
            0 => [
                'text' => $this->language->get('txt_delivering')
            ],
            // đã nhập hàng
            1 => [
                'text' => $this->language->get('txt_receipted')
            ]
        ];

        $store_receipt_total = $this->model_catalog_store_receipt->getTotalStoreReceipts($filter_data);

        $results = $this->model_catalog_store_receipt->getStoreReceiptsCustom($filter_data);
        foreach ($results as $result) {
            $data['store_receipts'][] = array(
                'store_receipt_id'             => $result['store_receipt_id'],
                'store_receipt_code'           => $result['store_receipt_code'],
                'manufacturer'                 => $result['manufacturer'],
                'branch'                       => $result['branch'],
                'status'                       => $result['status'],
                'user_create_id'               => $result['user_create_id'],
                'text'                         => $STORE_RECEIPT_STATUS[$result['status']]['text'],
                'total_to_pay'                 => number_format($result['total_to_pay'], 0, '', ','),
                'owed'                         => number_format($result['owed'], 0, '', ','),
                'created_at'                   => $result['created_at'],
                'edit'            => $this->url->link('catalog/store_receipt/edit', 'user_token=' . $this->session->data['user_token'] . '&store_receipt_id=' . $result['store_receipt_id'] . $url, true)
            );
        }
        $data['user_token'] = $this->session->data['user_token'];

        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $store_receipt_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/store_receipt', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        // Add jump to page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);

        if (isset($this->session->data['success']) && $this->session->data['success'] != 'text_success') {
            $data['success'] = $this->session->data['success'];

            $this->session->data['success'] = '';
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->session->data['store_receipts'])) {
            $data['store_receipts_action'] = json_encode($this->session->data['store_receipts']);
        }

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];

            $this->session->data['error'] = '';
            unset($this->session->data['error']);
        } else {
            $data['error_warning'] = '';
        }

        $data['results'] = sprintf($this->language->get('text_pagination'), ($store_receipt_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($store_receipt_total - $this->config->get('config_limit_admin'))) ? $store_receipt_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $store_receipt_total, ceil($store_receipt_total / $this->config->get('config_limit_admin')));

        // for auto building selected filter in view
        if (isset($this->request->get['filter_status'])) {
            $data['filter_store_receipt_status'] = $this->request->get['filter_status'];
        }
        $data['url_filter'] = $this->url->link('catalog/store_receipt', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['filter_name'] = $filter_name;
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('catalog/store_receipt_list_append', $data));
        } else {
            $this->response->setOutput($this->load->view('catalog/store_receipt_list', $data));
        }

    }

    public function getForm()
    {
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success']) && $this->session->data['success'] != 'text_success') {
            $data['success'] = $this->session->data['success'];

            $this->session->data['success'] = '';
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data['text_form'] = !isset($this->request->get['store_receipt_id']) ? $this->language->get('heading_title_add') : $this->language->get('heading_title_edit');
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/store_receipt', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $data['text_form'],
            'href' => '',
        );

        if (isset($this->request->get['store_receipt_id'])){
            $data['store_receipt_id'] = $this->request->get['store_receipt_id'];
        }

        // get discount info
        if (isset($this->request->get['store_receipt_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $store_receipt_info = $this->model_catalog_store_receipt->getStoreReceiptById($this->request->get['store_receipt_id']);
            $data['status'] = isset($store_receipt_info['status']) ? $store_receipt_info['status'] : 99;
            if ($data['status'] == 99){
                $data['store_receipt_is_deleted'] = 1;
            }
            $store_receipt_products = $this->model_catalog_store_receipt->getStoreReceiptProductsByReceiptId($this->request->get['store_receipt_id']);
            $this->load->model('catalog/product');
            foreach ($store_receipt_products as &$product){
                $product_name = $this->model_catalog_product->getProductNameById($product['product_id']);
                $product['sku'] = $this->model_catalog_product->getProductSKUById($product['product_id']);
                $pvid = $product['product_id'];
                if ($product['product_version_id']){
                    $pvid .= '-'.$product['product_version_id'];
                    $product_version_info = $this->model_catalog_product->getProductVersionByProductIdAndVersionId($product['product_id'], $product['product_version_id']);
                    if (isset($product_version_info['sku']) && $product_version_info['sku']){
                        $product['sku'] = $product_version_info['sku'];
                    }
                    if (isset($product_version_info['version']) && $product_version_info['version']){
                        $product_name .= '(' . $this->language->get('text_version') . $product_version_info['version'] . ')';
                    }
                }
                $product['name'] = $product_name;
                $product['pvid'] = $pvid;
                $product['cost_price'] = (float)$product['cost_price'];
                $product['fee'] = (float)$product['fee'];
                $product['discount'] = (float)$product['discount'];
                $product['total'] = (float)$product['total'];
            }

            $data['store_receipt_products'] = $store_receipt_products;

            // Get exist payment_voucher
            $this->load->model('cash_flow/payment_voucher');
            $payment_voucher = $this->model_cash_flow_payment_voucher->getReceiptVoucherByStoreReceipt($this->request->get['store_receipt_id']);
            if ($payment_voucher) {
                $data['payment_voucher_code'] = $payment_voucher['payment_voucher_code'];
                $data['link_payment_voucher'] = $this->url->link('cash_flow/payment_voucher/edit', 'user_token=' . $this->session->data['user_token'] . '&payment_voucher_id=' . $payment_voucher['payment_voucher_id'] , true);
            }
        }

        $this->load->model('setting/store');
        if (isset($this->request->post['receipt_store'])) {
            $receipt_store_selected[] = [
                'id' => $this->request->post['receipt_store'],
                'title' => $this->model_setting_store->getStoreNameById($this->request->post['receipt_store'])
            ];
            $data['receipt_store_selected'] = json_encode($receipt_store_selected);
        }else if(!empty($store_receipt_info)){
            $receipt_store_selected[] =[
                'id' => $store_receipt_info['store_id'],
                'title' => $this->model_setting_store->getStoreNameById($store_receipt_info['store_id'])
            ];
            $data['receipt_store_selected'] = json_encode($receipt_store_selected);
        }

        $this->load->model('catalog/manufacturer');
        if (isset($this->request->post['receipt_manufacturer'])) {
            $receipt_manufacturer_selected[] = [
                'id' => $this->request->post['receipt_manufacturer'],
                'title' => $this->model_catalog_manufacturer->getManufacturerNameById($this->request->post['receipt_manufacturer'])
            ];
            $data['receipt_manufacturer_selected'] = json_encode($receipt_manufacturer_selected);
        }else if(!empty($store_receipt_info)){
            $receipt_manufacturer_selected[] = [
                'id' => $store_receipt_info['manufacturer_id'],
                'title' => $this->model_catalog_manufacturer->getManufacturerNameById($store_receipt_info['manufacturer_id'])
            ];
            $data['receipt_manufacturer_selected'] = json_encode($receipt_manufacturer_selected);
        }

        if(!empty($store_receipt_info)){
            $data['receipt_total'] = (float)$store_receipt_info['total'];
        }else{
            $data['receipt_total'] = 0;
        }

        if (isset($this->request->post['receipt_discount'])) {
            $data['receipt_discount'] = extract_number($this->request->post['receipt_discount']);
        }else if(!empty($store_receipt_info)){
            $data['receipt_discount'] = (float)$store_receipt_info['discount'];
        }else{
            $data['receipt_discount'] = 0;
        }

        if (isset($this->request->post['type_criteria_allocation'])) {
            $data['allocation_criteria_type'] = $this->request->post['type_criteria_allocation'];
        }else if(!empty($store_receipt_info)){
            $data['allocation_criteria_type'] = $store_receipt_info['allocation_criteria_type'];
        }

        if (isset($this->request->post['receipt_fee'])) {
            $data['receipt_fee'] = extract_number($this->request->post['receipt_fee']);
        }else if(!empty($store_receipt_info)){
            $data['receipt_fee'] = (float)$store_receipt_info['allocation_criteria_total'];
        }else{
            $data['receipt_fee'] = 0;
        }

        if (isset($this->request->post['fee_config'])) {
            $data['allocation_criteria_fees'] = $_POST['fee_config'];
        }else if(!empty($store_receipt_info)){
            $data['allocation_criteria_fees'] = $store_receipt_info['allocation_criteria_fees'];
        }

        if(!empty($store_receipt_info)){
            $data['total_to_pay'] = (float)$store_receipt_info['total_to_pay'];
        }else{
            $data['total_to_pay'] = 0;
        }

        if (isset($this->request->post['total_paid'])) {
            $data['total_paid'] = extract_number($this->request->post['total_paid']);
        }else if(!empty($store_receipt_info)){
            $data['total_paid'] = (float)$store_receipt_info['total_paid'];
        }else{
            $data['total_paid'] = 0;
        }

        if(!empty($store_receipt_info)){
            $data['receipt_owed'] = (float)$store_receipt_info['owed'];
        }else{
            $data['receipt_owed'] = 0;
        }

        if (isset($this->request->post['note'])) {
            $data['note'] = $this->request->post['note'];
        }else if(!empty($store_receipt_info)){
            $data['note'] = $store_receipt_info['comment'];
        }else{
            $data['note'] = '';
        }

        if(!empty($store_receipt_info)){
            $data['status'] = $store_receipt_info['status'];
        }

        $current_user_id = $this->user->getId();
        if(!empty($store_receipt_info)){
            $data['user_create_id'] = $store_receipt_info['user_create_id'];
        } else {
            $data['user_create_id'] = isset($current_user_id) ? $current_user_id : 1;
        }

        $data['current_user_id'] = isset($current_user_id) ? $current_user_id : 1;
        $data['access_modify'] = $this->user->hasPermission('modify', 'catalog/store_receipt');

        // manufacture form
        // province
        $this->load->model('localisation/vietnam_administrative');
        $data['provinces'] = $this->model_localisation_vietnam_administrative->getProvinces();

        if (!isset($this->request->get['store_receipt_id'])) {
            $data['action'] = $this->url->link('catalog/store_receipt/add', 'user_token=' . $this->session->data['user_token'], true);
        } else {
            $data['action'] = $this->url->link('catalog/store_receipt/edit', 'user_token=' . $this->session->data['user_token'] . '&store_receipt_id=' . $this->request->get['store_receipt_id'], true);
        }
        $data['cancel'] = $this->url->link('catalog/store_receipt', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_check_manufacturer_name_exist'] = $this->url->link('catalog/store_receipt/validateManufacturerNameExist', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_check_manufacturer_tax_code_exist'] = $this->url->link('catalog/store_receipt/validateManufacturerTaxCodeExist', 'user_token=' . $this->session->data['user_token'], true);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $data['user_token'] = $this->session->data['user_token'];

        $this->response->setOutput($this->load->view('catalog/store_receipt_form', $data));
    }

    public function createManufacturer()
    {
        $json = [
            'status' => false,
        ];

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $this->load->model('catalog/manufacturer');
            $manufacturer_id = $this->model_catalog_manufacturer->addManufacturer($this->request->post);
            if ($manufacturer_id){
                $json = [
                    'status' => true,
                    'manufacture_id' => $manufacturer_id,
                    'name' => isset($this->request->post['name']) ? $this->request->post['name'] : ''
                ];
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function createProduct()
    {
        $json = [
            'status' => false,
            'msg' => 'Tạo sản phẩm ko thành công'
        ];
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateProductForm()) {
            $this->load->model('catalog/store_receipt');
            $result = $this->model_catalog_store_receipt->addProductDraft($this->request->post);
            if ($result){
                $json['status'] = true;
                $json['msg'] = 'Tạo sản phẩm thành công';
                $json['data'] = $result;
            }
        } else {
            if (isset($this->error['warning'])) {
                $json['msg'] = $this->error['warning'];
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function validateProductForm()
    {
        $this->load->language('catalog/store_receipt');
        $this->load->model('catalog/product');
        if (!$this->user->hasPermission('modify', 'catalog/product')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        /* validate form */
        if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 100)) {
            $this->error['name'][0] = $this->language->get('error_name');
            $this->error['warning'] = $this->language->get('error_name');
        }

        if ($this->model_catalog_product->searchProductByNameContainDraft($this->request->post['name'], '') != false) {
            $this->error['warning'] = $this->language->get('error_name_exist');
        }

        if (isset($this->request->post['sku']) && !empty($this->request->post['sku']) && $this->model_catalog_product->searchProductAndVersionBySKU($this->request->post['sku'], '') == false) {
            $this->error['warning'] = $this->language->get('error_sku_exist');
        }

        if (isset($this->request->post['skus'])) {
            $product_skus = $this->request->post['skus'];
            foreach ($product_skus as $product_sku) {
                if (!empty($product_sku) && $this->model_catalog_product->searchProductAndVersionBySKU($product_sku, '') == false) {
                    $this->error['warning'] = $this->language->get('error_sku_exist');
                    break;
                }
            }

        }

        return !$this->error;
    }

    public function getStoreLazyLoad()
    {
        $json = array();

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $this->load->model('setting/store');

        $filter_data = array(
            'order' => 'ASC',
            'start' => ($page - 1) * 6,
            'limit' => 6
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        $json['results'] = $this->model_setting_store->getStoresForUser($filter_data);

        $count_store = $this->model_setting_store->getTotalStoreForUser($filter_data);
        $json['count'] = $count_store;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getManufacturerLazyLoad()
    {
        $json = array();

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $this->load->model('catalog/manufacturer');

        $filter_data = array(
            'order' => 'ASC',
            'start' => ($page - 1) * 6,
            'limit' => 6,
            'filter_status' => 1 // 1: active
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        $json['results'] = $this->model_catalog_manufacturer->getManufacturersPaginator($filter_data);

        $count_manufacturer = $this->model_catalog_manufacturer->getTotalManufacturersFilter($filter_data);
        $json['count'] = $count_manufacturer;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }


    public function getProductsLazyLoad()
    {
        $json = array();
        $count_tag = 0;

        $this->load->language('catalog/store_receipt');
        $this->load->model('catalog/store_receipt');
        $this->load->model('tool/image');
        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        // display 6 product in select config theme is many, or setup other config
        $filter_data = array(
            'limit' => 6,
            'start' => ($page - 1) * 6,
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        $this->load->model('user/user');
        $user_id = $this->user->getId();

        if (!$this->user->canAccessAll()) {
            $user_store_ids = $this->model_user_user->getStoreIdsOffUser($user_id);
            $filter_data['user_store_id'] = implode(",", $user_store_ids);
            $filter_data['current_user_id'] = (int)$user_id;
        }

        if (!$this->user->hasPermission('modify', 'catalog/product')) {
            $json['count'] = $count_tag;
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));

            return;
        }

        $results = $this->model_catalog_store_receipt->getProductContainDraftPaginator($filter_data);

        foreach ($results as $key => $value) {
            $version_name = implode(' • ', explode(',', $value['version']));
            if ($version_name != '') {
                $version_name = $this->language->get('text_version') . $version_name;
            }

            if ($value['image'] != '') {
                $image = $this->model_tool_image->resize($value['image'], 100, 100);
            } else {
                $image = $this->model_tool_image->resize('no_image.png', 100, 100);
            }

            $json['results'][] = array(
                'id' => $value['id'],
                'product_id' => $value['product_id'],
                'product_version_id' => $value['product_version_id'],
                'version' => $version_name,
                'subname' => $version_name,
                'text' => $value['name'],
                'product_name' => $value['product_name'],
                'image' => $image,
                'weight' => number_format($value['weight'], 0, '', ''),
                'price' => number_format($value['price'], 0, '', ','),
                'quantity' => $value['quantity'],
                'sale_on_out_of_stock' => $value['sale_on_out_of_stock'],
                'status' => $value['status'],
                'sku' => $value['sku'],
            );
        }

        $count_tag = $this->model_catalog_store_receipt->countProductAllContainDraft($filter_data);

        $json['count'] = $count_tag;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));
    }

    private function validateForm()
    {
        $this->load->language('catalog/store_receipt');
        if (!$this->user->hasPermission('modify', 'catalog/store_receipt')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function validateManufacturerNameExist()
    {
        $this->load->language('catalog/manufacturer');
        $this->load->model('catalog/manufacturer');

        $manufacturer_id = isset($this->request->post['manufacturer_id']) ? $this->request->post['manufacturer_id'] : '';
        $name = isset($this->request->post['name']) ? $this->request->post['name'] : '';

        $json = [
            'valid' => true
        ];

        if ($name){
            if ($this->model_catalog_manufacturer->validateNameManufacturerExist($name, $manufacturer_id)){
                $json['valid'] = false;
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function validateManufacturerTaxCodeExist()
    {
        $this->load->language('catalog/manufacturer');
        $this->load->model('catalog/manufacturer');

        $manufacturer_id = isset($this->request->post['manufacturer_id']) ? $this->request->post['manufacturer_id'] : '';
        $tax_code = isset($this->request->post['tax_code']) ? $this->request->post['tax_code'] : '';

        $json = [
            'valid' => true
        ];

        if ($tax_code){
            if ($this->model_catalog_manufacturer->validateTaxCodeManufacturerExist($tax_code, $manufacturer_id)){
                $json['valid'] = false;
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function changeStatus($status)
    {
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('catalog/store_receipt');

        $store_receipt_ids = $this->request->get['store_receipt_ids'];
        $store_receipt_ids = explode(',', $store_receipt_ids);
        $store_receipts = [];
        if (isset($store_receipt_ids)) {
            $count_fail = 0;
            $count_success = 0;
            $count = 0;
            foreach ($store_receipt_ids as $store_receipt_id) {
                $count++;
                $store_receipt = $this->model_catalog_store_receipt->getStoreReceipt($store_receipt_id);

                if (!is_array($store_receipt) || empty($store_receipt) || !array_key_exists('status', $store_receipt)) {
                    continue;
                }
                if ($status == 1) {
                    if ($store_receipt['status'] == 0) {
                        $this->model_catalog_store_receipt->approveStoreReceipt($store_receipt_id);
                        $store_receipts[] = array(
                            'id' => $store_receipt['store_receipt_id'],
                            'status' => $this->language->get('text_success'),
                            'code' => $store_receipt['store_receipt_code'],
                            'message' => ''
                        );
                        $count_success++;
                    } else {
                        $count_fail++;
                        $store_receipts[] = array(
                            'id' => $store_receipt['store_receipt_id'],
                            'status' => $this->language->get('text_failure'),
                            'code' => $store_receipt['store_receipt_code'],
                            'message' => $this->language->get('txt_failure_receipt')
                        );
                    }
                } elseif ($status == 99) {
                    if ($store_receipt['status'] == 1){
                        $this->model_catalog_store_receipt->removeStoreReceipt($store_receipt_id, $status);
                    }else{
                        $this->model_catalog_store_receipt->updateStoreReceiptStatus($store_receipt_id, $status);
                    }

                    // Auto remove payment_voucher
                    $this->load->model('cash_flow/payment_voucher');
                    $this->model_cash_flow_payment_voucher->autoRemoveVoucherFromOrder($store_receipt_id);

                    $store_receipts[] = array(
                        'id' => $store_receipt['store_receipt_id'],
                        'status' => $this->language->get('text_success'),
                        'code' => $store_receipt['store_receipt_code'],
                        'message' => ''
                    );
                    $count_success++;
                }
                else{
                    $this->model_catalog_store_receipt->deliverStoreReceipt($store_receipt_id);
                }
            }
            if ($count == 1) {
                if ($count_fail == 1) {
                    $this->session->data['error'] = $this->language->get('txt_failure_receipt');
                }
                if ($count_success == 1) {
                    if ($status == 1) {
                        $this->session->data['success'] = $this->language->get('txt_success_receipt') . '!';
                    } else {
                        $this->session->data['success'] = $this->language->get('txt_success_cancel') . '!';
                    }
                }
            } else {
                $message = $this->language->get('txt_success_receipt') . ' ' . $count_success
                    . '/' . $count . ' ' . '<a class="text-yellow action-popup">' . $this->language->get('txt_note_view_detail') . '</a>';
                $cancel_message = $this->language->get('txt_success_cancel') . ' ' . $count_success
                    . '/' . $count . ' ' . '<a class="text-yellow action-popup">' . $this->language->get('txt_note_view_detail') . '</a>';
                if ($count_success == 0) {
                    $this->session->data['error'] = $message;
                } else {
                    if ($status == 1) {
                        $this->session->data['success'] = $message;
                    } else {
                        $this->session->data['success'] = $cancel_message;
                    }
                }
            }
        }
        $this->session->data['store_receipts'] = $store_receipts;
        $this->response->redirect($this->url->link('catalog/store_receipt', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function receive()
    {
        $this->load->language('catalog/store_receipt');
        $this->changeStatus(1);
    }

    public function delete()
    {
        $this->load->language('catalog/store_receipt');
        $this->changeStatus(99);
    }

    public function loadoptionone()
    {
        $this->load->language('catalog/store_receipt');
        $this->load->model('catalog/manufacturer');
        $categoryID = $this->request->post['categoryID'];
        $end_select_html = '</select></div>';
        $show_html = '';

        if ($categoryID > 0) {
            if ($categoryID == 1) { // Created at
                $show_html .= '<div class="form-group hierarchical-child" id="hierarchical-date">
                                            <select class="form-control form-control-sm select2"
                                             data-placeholder="' . $this->language->get('filter_created_at_placeholder') . '" 
                                             data-label="' . $this->language->get('filter_date_range') . '" 
                                             id="filter_created_at_type" 
                                             name="filter_created_at_type">';
                $show_html .= '<option value="">' . $this->language->get('filter_created_at_placeholder') . '</option>';
                $show_html .= '<option value="today" data-value="' . $this->language->get('filter_created_at_today') . '">' . $this->language->get('filter_created_at_today') . '</option>';
                $show_html .= '<option value="week" data-value="' . $this->language->get('filter_created_at_this_week') . '">' . $this->language->get('filter_created_at_this_week') . '</option>';
                $show_html .= '<option value="month" data-value="' . $this->language->get('filter_created_at_this_month') . '">' . $this->language->get('filter_created_at_this_month') . '</option>';
                $show_html .= '<option value="option" data-value="' . $this->language->get('filter_created_at_option') . '">' . $this->language->get('filter_created_at_option') . '</option>';
                $show_html .= '</select>';
                $show_html .= '<div class="input-group-icon mt-3 w-100 d-none filter_created_at_value">
                            <i class="bg-secondary icon icon-calendar ml-n3" style=" top: -50px;"></i>
                            <input type="text" name="filter_created_at_from"
                                   class="form-control date-picker date-picker-store-receipt-filter mt-3"
                                   placeholder="' . $this->language->get('filter_select_date_from') . '" data-label="' . $this->language->get('filter_txt_date') . '"
                                   data-value="">
                            <i class="bg-secondary icon icon-calendar ml-n3" style=" top: 50px;"></i>
                            <input type="text" name="filter_created_at_to"
                                   class="form-control date-picker date-picker-store-receipt-filter mt-3"
                                   placeholder="' . $this->language->get('filter_select_date_to') . '" data-label="' . $this->language->get('filter_txt_date') . '"
                                   data-value="">
                          </div>';
            } elseif ($categoryID == 2) { // Manufacturer
                $start_select_html = '<div class="form-group hierarchical-child" id="hierarchical-manufacturer">
                                        <select class="form-control form-control-sm select2" 
                                        data-placeholder="' . $this->language->get('filter_manufacturer_placeholder') . '" 
                                        data-label="' . $this->language->get('filter_manufacturer_placeholder') . '" 
                                        id="filter_manufacturer" 
                                        name="filter_manufacturer">';
                $end_select_html = '</select></div>';
                $getListManufacturer = $this->model_catalog_manufacturer->getManufacturers();
                $show_html .= $start_select_html . '<option value="" data-value="' . $this->language->get('filter_manufacture') . '">' . $this->language->get('filter_manufacturer_placeholder') . '</option>';
                foreach ($getListManufacturer as $key => $vl) {
                    $show_html .= '<option value="' . $vl['manufacturer_id'] . '" data-value="' . $vl['name'] . '">' . $vl['name'] . '</option>';
                }
                $show_html .= $end_select_html;
            }elseif ($categoryID == 3) { // Status
                $show_html = '<div class="form-group hierarchical-child" id="hierarchical-status">
                                            <select class="form-control form-control-sm select2"
                                             data-placeholder="' . $this->language->get('filter_status_placeholder') . '" 
                                             data-label="' . $this->language->get('filter_status') . '" 
                                             id="filter_status" 
                                             name="filter_status">';
                $show_html .= '<option value="">' . $this->language->get('filter_status_placeholder') . '</option>';
                $show_html .= '<option value="0" data-value="' . $this->language->get('filter_status_delivering') . '">' . $this->language->get('filter_status_delivering') . '</option>';
                $show_html .= '<option value="1" data-value="' . $this->language->get('filter_status_receipted') . '">' . $this->language->get('filter_status_receipted') . '</option>';
                $show_html .= $end_select_html;
            }
        } else {
            $show_html .= '<style>#optionone,#optiontwo{display:none !important}</style>';
        }

        echo $show_html .= '';
    }
}