<?php
class ControllerCatalogManufacturer extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/manufacturer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/manufacturer');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/manufacturer');

		$this->document->setTitle($this->language->get('text_add'));

		$this->load->model('catalog/manufacturer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
		    if (empty($this->request->post['source'])) {
                $this->request->post['source'] = 'admin';
            }

			$this->model_catalog_manufacturer->addManufacturer($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/manufacturer', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/manufacturer');

		$this->document->setTitle($this->language->get('text_edit'));

		$this->load->model('catalog/manufacturer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            if (empty($this->request->post['source'])) {
                $this->request->post['source'] = 'admin';
            }

			$this->model_catalog_manufacturer->editManufacturer($this->request->get['manufacturer_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/manufacturer', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

    private function changeStatus($status, $text_action_one, $text_action)
    {
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('catalog/manufacturer');

        $manufacturer_ids = $this->request->get['manufacturer_ids'];
        $manufacturer_ids = explode(',', $manufacturer_ids);
        $manufacturers = [];
        if (isset($manufacturer_ids)) {
            $count_fail = 0;
            $count_success = 0;
            $count = 0;
            foreach ($manufacturer_ids as $manufacturer_id) {
                $count++;
                $manufacturer = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);
                if ($manufacturer['status'] == $status) {
                    $count_fail++;
                    $manufacturers[] = array(
                        'id' => $manufacturer['manufacturer_id'],
                        'status' => $this->language->get('text_failure'),
                        'code' => $manufacturer['manufacturer_code'],
                        'message' => $this->language->get('txt_note_manufacture').' '.$text_action_one
                    );
                }
                else{
                    switch ($status) {
                        case 2:
                            $this->model_catalog_manufacturer->pauseManufacturer($manufacturer_id);
                            break;
                        case 99:
                            $this->model_catalog_manufacturer->deleteManufacturer($manufacturer_id);
                            break;
                        default:
                            $this->model_catalog_manufacturer->activeManufacturer($manufacturer_id);
                    }
                    $manufacturers[] = array(
                        'id' => $manufacturer['manufacturer_id'],
                        'status' => $this->language->get('text_success'),
                        'code' => $manufacturer['manufacturer_code'],
                        'message' => ''
                    );
                    $count_success++;
                }
            }
            if ($status != 99) {
                if ($count == 1) {
                    if ($count_fail == 1) {
                        $this->session->data['error'] = $this->language->get('txt_note_manufacture_one').' '.$text_action_one;
                    }
                    if ($count_success == 1) {
                        $this->session->data['success'] = $text_action.' '.$this->language->get('txt_action_one_success');
                    }
                } else {
                    if ($count_success == 0) {
                        $this->session->data['error'] = $text_action . ' ' . $this->language->get('txt_success_lower') . ' ' . $count_success
                            . '/' . $count . ' ' . $this->language->get('txt_manufacture_lower') . ' '
                            . '<a class="text-yellow action-popup">' . $this->language->get('txt_note_view_detail') . '</a>';
                    }
                    else{
                        $message = $text_action . ' ' . $this->language->get('txt_success_lower') . ' ' . $count_success . '/' . $count . ' ';
                        if ($count_success != $count) {
                            $message .=' ' . '<a class="text-yellow action-popup">' . $this->language->get('txt_note_view_detail') . '</a>';
                        }
                        $this->session->data['success'] = $message;
                    }
                }
            }
            else{
                $this->session->data['success'] = $text_action.' '.$this->language->get('txt_manufacture_lower').' '.$this->language->get('txt_success_lower').'!';
            }
        }
        $this->session->data['manufacturers'] = $manufacturers;
        $this->response->redirect($this->url->link('catalog/manufacturer', 'user_token=' . $this->session->data['user_token'], true));
	}

    public function pause()
    {
        $this->load->language('catalog/manufacturer');
        $text_action_one = $this->language->get('txt_note_manufacture_pause');
        $text_action = $this->language->get('txt_note_pause');
        $this->changeStatus(2, $text_action_one, $text_action);
	}

    public function active()
    {
        $this->load->language('catalog/manufacturer');
        $text_action_one = $this->language->get('txt_note_manufacture_active');
        $text_action = $this->language->get('txt_note_active');
        $this->changeStatus(1, $text_action_one, $text_action);
	}
	
	public function delete() {
        $this->load->language('catalog/manufacturer');
        $text_action_one = $this->language->get('txt_manufacture_lower');
        $text_action = $this->language->get('txt_note_delete');
        $this->changeStatus(99, $text_action_one, $text_action);
	}

	public function deleteJson()
    {
        $this->load->language('catalog/manufacturer');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/manufacturer');

        $this->response->addHeader('Content-Type: application/json');
        $json = [
            'status' => false,
            'message' => $this->language->get('delete_error')
        ];
        try {
            if (isset($this->request->post['selected']) && $this->validateDelete()) {
                foreach ($this->request->post['selected'] as $manufacturer_id) {
                    $this->model_catalog_manufacturer->deleteManufacturer($manufacturer_id);
                }
                $json = [
                    'status' => true,
                    'message' => $this->language->get('delete_success')
                ];
                $this->response->setOutput(json_encode($json));
            } else {
                $this->response->setOutput(json_encode($json));
            }
        } catch (Exception $e) {
            $this->response->setOutput(json_encode($json));
        }

        if (isset($this->error['warning'])) {
            $json = [
                'status' => false,
                'message' => $this->error['warning']
            ];
        }

        $this->response->setOutput(json_encode($json));
    }

	protected function getList() {

        $filter_staff = $this->getValueFromRequest('get', 'filter_staff');
        $manufacturer_status = $this->getValueFromRequest('get', 'manufacturer_status');
        $filter_name = $this->getValueFromRequest('get', 'filter_name');
        $page = $this->getValueFromRequest('get', 'page', 1);
        $filter_data = $this->getValueFromRequest('get', 'filter_data');

		$url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/manufacturer', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

        $data['access_modify'] = $this->user->hasPermission('modify', 'catalog/manufacturer');
        $data['is_admin'] = $this->user->isAdmin() ? 1 : 0;
		$data['add'] = $this->url->link('catalog/manufacturer/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/manufacturer/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['active'] = $this->url->link('catalog/manufacturer/active', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['pause'] = $this->url->link('catalog/manufacturer/pause', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['list'] = $this->url->link('catalog/manufacturer', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['loadOptionOne'] = $this->replace_url($this->url->link('catalog/manufacturer/loadoptionone', 'user_token=' . $this->session->data['user_token'] . $url, true));
		$data['manufacturers'] = array();

        if (!empty($filter_data)) {
            if (isset($this->request->get['manufacturer_status'])) {
                $manufacturer_status = ($this->request->get['manufacturer_status']);
                if (is_array($manufacturer_status)) {
                    if (count($manufacturer_status) > 1) {
                        $manufacturer_status = '';
                    } else {
                        $manufacturer_status = (int)$manufacturer_status[0];
                    }
                }
            }
        }

        $filterStaff= null;
        $filter_staff = is_array($filter_staff) ? $filter_staff : [];
        if (!$this->user->canAccessAll()){
            $filter_staff[] = (int)$this->user->getId();
        }
        if (!empty($filter_staff)) {
            if (count($filter_staff) > 0) {
                $filterStaff = implode(',', ($filter_staff));
            } else {
                $filterStaff = (int)$filter_staff[0];
            }
        } else {
            $filterStaff = '';
        }

        $filter_data = array(
            'filter_data' => $filter_data,
            'filter_name' => $filter_name,
            'filter_staff' => $filterStaff,
            'manufacturer_status' => $manufacturer_status,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );
        if (!$this->user->canAccessAll()) {
            $filter_data['user_create_id'] = $this->user->getId();
        }
        $FILTER_TRANSACTION_TYPE = [
            // in transaction
            [
                'filter_type_id' => ModelCatalogManufacturer::TYPE_IN_TRANSACTION,
                'filter_type_description' => $this->language->get('text_in_transaction')
            ],
            // stop transaction
            [
                'filter_type_id' => ModelCatalogManufacturer::TYPE_STOP_TRANSACTION,
                'filter_type_description' => $this->language->get('text_stop_transaction')
            ]
        ];

        $MANUFACTURER_STATUS = [
            // in transaction
            1 => [
                'text' => $this->language->get('text_in_transaction')
            ],
            // stop transaction
            2 => [
                'text' => $this->language->get('text_stop_transaction')
            ],
            // delete transaction
            99 => [
                'text' => $this->language->get('text_delete_transaction')
            ]
        ];

        $data['filter_transaction_type'] = $FILTER_TRANSACTION_TYPE;

		$manufacturer_total = $this->model_catalog_manufacturer->getTotalManufacturers($filter_data);

		$results = $this->model_catalog_manufacturer->getManufacturersCustom($filter_data);
		foreach ($results as $result) {
			$data['manufacturers'][] = array(
				'manufacturer_id'              => $result['manufacturer_id'],
				'source'                       => $result['source'],
				'manufacturer_code'            => $result['manufacturer_code'],
				'name'                         => $result['name'],
				'telephone'                    => $result['telephone'],
				'user_name'                    => $result['fullname'],
				'address'                      => $result['address'],
				'status'                       => $result['status'],
                'text'                         => $MANUFACTURER_STATUS[$result['status']]['text'],
				'edit'            => $this->url->link('catalog/manufacturer/edit', 'user_token=' . $this->session->data['user_token'] . '&manufacturer_id=' . $result['manufacturer_id'] . $url, true)
			);
		}
        $data['user_token'] = $this->session->data['user_token'];
        
		// paginate
		$url = '';

		$pagination = new CustomPaginate();
		$pagination->total = $manufacturer_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/manufacturer', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        // Add jump to page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);
		
        if (isset($this->session->data['success']) && $this->session->data['success'] != 'text_success') {
            $data['success'] = $this->session->data['success'];

            $this->session->data['success'] = '';
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->session->data['manufacturers'])) {
            $data['manufacturers_action'] = json_encode($this->session->data['manufacturers']);
        }

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];

            $this->session->data['error'] = '';
            unset($this->session->data['error']);
        } else {
            $data['error_warning'] = '';
        }

        $data['results'] = sprintf($this->language->get('text_pagination'), ($manufacturer_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($manufacturer_total - $this->config->get('config_limit_admin'))) ? $manufacturer_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $manufacturer_total, ceil($manufacturer_total / $this->config->get('config_limit_admin')));
        $data['url_filter'] = $this->url->link('catalog/manufacturer', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['filter_name'] = $filter_name;
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('catalog/manufacturer_list_append', $data));
        } else {
            $this->response->setOutput($this->load->view('catalog/manufacturer_list', $data));
        }
	}

	protected function getForm() {
		$data['text_form'] = !isset($this->request->get['manufacturer_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/manufacturer', 'user_token=' . $this->session->data['user_token'], true)
		);

        if (isset($this->request->get['manufacturer_id'])) {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_edit'),
                'href' => '',
            );
        } else {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_add'),
                'href' => ''
            );
        }

        $data['access_modify'] = $this->user->hasPermission('modify', 'catalog/manufacturer');

		if (!isset($this->request->get['manufacturer_id'])) {
			$data['action'] = $this->url->link('catalog/manufacturer/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/manufacturer/edit', 'user_token=' . $this->session->data['user_token'] . '&manufacturer_id=' . $this->request->get['manufacturer_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/manufacturer', 'user_token=' . $this->session->data['user_token'] . $url, true);

		if (isset($this->request->get['manufacturer_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($this->request->get['manufacturer_id']);
		}

        if (!empty($manufacturer_info)) {
            $data['manufacturer'] = $manufacturer_info;
        } else {
            $data['manufacturer'] = [];
        }

		$data['user_token'] = $this->session->data['user_token'];

		$this->load->model('setting/store');

		$data['stores'] = array();
		
		$data['stores'][] = array(
			'store_id' => 0,
			'name'     => $this->language->get('text_default')
		);
		
		$stores = $this->model_setting_store->getStores();

		foreach ($stores as $store) {
			$data['stores'][] = array(
				'store_id' => $store['store_id'],
				'name'     => $store['name']
			);
		}

		if (isset($this->request->post['manufacturer_store'])) {
			$data['manufacturer_store'] = $this->request->post['manufacturer_store'];
		} elseif (isset($this->request->get['manufacturer_id'])) {
			$data['manufacturer_store'] = $this->model_catalog_manufacturer->getManufacturerStores($this->request->get['manufacturer_id']);
		} else {
			$data['manufacturer_store'] = array(0);
		}

		if (isset($this->request->post['image'])) {
			$data['image'] = $this->request->post['image'];
		} elseif (!empty($manufacturer_info)) {
			$data['image'] = $manufacturer_info['image'];
		} else {
			$data['image'] = '';
		}

		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && (is_file(DIR_IMAGE . $this->request->post['image']) || $this->model_tool_image->imageIsExist($this->request->post['image']))) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($manufacturer_info) && (is_file(DIR_IMAGE . $manufacturer_info['image']) || $this->model_tool_image->imageIsExist($manufacturer_info['image']))) {
			$data['thumb'] = $this->model_tool_image->resize($manufacturer_info['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		if (isset($this->request->post['sort_order'])) {
			$data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($manufacturer_info)) {
			$data['sort_order'] = $manufacturer_info['sort_order'];
		} else {
			$data['sort_order'] = '';
		}

        if (isset($this->request->post['source'])) {
            $data['source'] = $this->request->post['source'];
        } elseif (!empty($manufacturer_info)) {
            $data['source'] = $manufacturer_info['source'];
        } else {
            $data['source'] = '';
        }

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();
		
		if (isset($this->request->post['manufacturer_seo_url'])) {
			$data['manufacturer_seo_url'] = $this->request->post['manufacturer_seo_url'];
		} elseif (isset($this->request->get['manufacturer_id'])) {
			$data['manufacturer_seo_url'] = $this->model_catalog_manufacturer->getManufacturerSeoUrls($this->request->get['manufacturer_id']);
		} else {
			$data['manufacturer_seo_url'] = array();
		}

        if (isset($this->request->get['manufacturer_id'])){
            $data['manufacturer_id'] = $this->request->get['manufacturer_id'];
        }

        // province
        $this->load->model('localisation/vietnam_administrative');
        $data['provinces'] = $this->model_localisation_vietnam_administrative->getProvinces();
        // distict
        if (isset($data['manufacturer']['district'])) {
            $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($data['manufacturer']['district']);
            $data['district_name'] = $district['name'];
        }
        $data['href_check_manufacturer_name_exist'] = $this->url->link('catalog/manufacturer/validateManufacturerNameExist', 'user_token=' . $this->session->data['user_token'], true);
        $data['href_check_manufacturer_tax_code_exist'] = $this->url->link('catalog/manufacturer/validateManufacturerTaxCodeExist', 'user_token=' . $this->session->data['user_token'], true);
		$data['custom_header'] = $this->load->controller('common/custom_header');
		$data['custom_column_left'] = $this->load->controller('common/custom_column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/manufacturer_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/manufacturer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		if (isset($this->request->post['manufacturer_seo_url']) && $this->request->post['manufacturer_seo_url']) {
			$this->load->model('design/seo_url');
			
			foreach ($this->request->post['manufacturer_seo_url'] as $store_id => $language) {
				foreach ($language as $language_id => $keyword) {
					if (!empty($keyword)) {
						if (count(array_keys($language, $keyword)) > 1) {
							$this->error['keyword'][$store_id][$language_id] = $this->language->get('error_unique');
						}							
						
						$seo_urls = $this->model_design_seo_url->getSeoUrlsByKeyword($keyword);
						
						foreach ($seo_urls as $seo_url) {
							if (($seo_url['store_id'] == $store_id) && (!isset($this->request->get['manufacturer_id']) || (($seo_url['query'] != 'manufacturer_id=' . $this->request->get['manufacturer_id'])))) {
								$this->error['keyword'][$store_id][$language_id] = $this->language->get('error_keyword');
							}
						}
					}
				}
			}
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/manufacturer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/product');

		foreach ($this->request->post['selected'] as $manufacturer_id) {
			$product_total = $this->model_catalog_product->getTotalProductsByManufacturerId($manufacturer_id);

			if ($product_total) {
				$this->error['warning'] = sprintf($this->language->get('error_product'), $product_total);
			}
		}

		return !$this->error;
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/manufacturer');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_catalog_manufacturer->getManufacturers($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'manufacturer_id' => $result['manufacturer_id'],
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function addManufacturerFast() {
        $this->load->language('catalog/manufacturer');
        if (isset($this->request->post['text'])) {
            $this->load->model('catalog/manufacturer');
            $result = $this->model_catalog_manufacturer->addManufacturerFast($this->request->post['text']);
            if ($result) {
                return json_encode(array(
                    "status" => true,
                    "message" => $this->language->get('add_success'),
                ));
            }
        }
	    return json_encode(array(
	        "status" => false,
            "message" => $this->language->get('add_error'),
        ));
    }

    public function validateManufacturerNameExist()
    {
        $this->load->language('catalog/manufacturer');
        $this->load->model('catalog/manufacturer');

        $manufacturer_id = isset($this->request->post['manufacturer_id']) ? $this->request->post['manufacturer_id'] : '';
        $name = isset($this->request->post['name']) ? $this->request->post['name'] : '';

        $json = [
            'valid' => true
        ];

        if ($name){
            if ($this->model_catalog_manufacturer->validateNameManufacturerExist($name, $manufacturer_id)){
                $json['valid'] = false;
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function validateManufacturerTaxCodeExist()
    {
        $this->load->language('catalog/manufacturer');
        $this->load->model('catalog/manufacturer');

        $manufacturer_id = isset($this->request->post['manufacturer_id']) ? $this->request->post['manufacturer_id'] : '';
        $tax_code = isset($this->request->post['tax_code']) ? $this->request->post['tax_code'] : '';

        $json = [
            'valid' => true
        ];

        if ($tax_code){
            if ($this->model_catalog_manufacturer->validateTaxCodeManufacturerExist($tax_code, $manufacturer_id)){
                $json['valid'] = false;
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function loadoptionone()
    {
        $this->load->language('catalog/manufacturer');

        $this->load->model('catalog/manufacturer');
        $this->load->model('user/user');
        $categoryID = $this->request->post['categoryID'];
        $start_select_html = '<div class="form-group"><select class="form-control select2 form-control-sm" data-placeholder="' . $this->language->get('filter_status_placeholder') . '" data-label="' . $this->language->get('filter_status') . '" id="status_product" name="status_product">';
        $end_select_html = '</select></div>';
        $show_html = '';

        $FILTER_TRANSACTION_TYPE = [
            // in transaction
            [
                'filter_type_id' => ModelCatalogManufacturer::TYPE_IN_TRANSACTION,
                'filter_type_description' => $this->language->get('text_in_transaction')
            ],
            // stop transaction
            [
                'filter_type_id' => ModelCatalogManufacturer::TYPE_STOP_TRANSACTION,
                'filter_type_description' => $this->language->get('text_stop_transaction')
            ]
        ];

        if ($categoryID > 0) {
            if ($categoryID == 1) { // Trang thai hien thi
//                if (!isset($this->request->post['flag'])) {
//                    $show_html .= $start_select_html;
//                    $show_html .= '<option value="" data-value="' . $this->language->get('product_all_status') . '">' . $this->language->get('product_all_status') . '</option>';
//                    $show_html .= '<option value="1" data-value="' . $this->language->get('entry_status_publish') . '">' . $this->language->get('entry_status_publish') . '</option>';
//                    $show_html .= '<option value="0" data-value="' . $this->language->get('entry_status_unpublish') . '">' . $this->language->get('entry_status_unpublish') . '</option>';
//                } else {
//
//                }
                $show_html .= '<div class="form-group"><select class="form-control select2 form-control-sm" data-placeholder="' . $this->language->get('text_filter_method') . '" data-label="' . $this->language->get('filter_status') . '" id="discount_type_id" name="discount_type_id">';
                $show_html .= '<option value="">' . $this->language->get('choose_filter ') . '</option>';
                $show_html .= '<option value="'. ModelCatalogManufacturer::TYPE_IN_TRANSACTION .  '" data-value="' . $this->language->get('text_in_transaction') . '">' . $this->language->get('text_in_transaction') . '</option>';
                $show_html .= '<option value="'. ModelCatalogManufacturer::TYPE_STOP_TRANSACTION .'" data-value="' . $this->language->get('text_stop_transaction') . '">' . $this->language->get('text_stop_transaction') . '</option>';
                $show_html .= $end_select_html;
            } elseif ($categoryID == 2) { // Nhân viên
                $start_select_html = '<div class="form-group"><select class="form-control select2 form-control-sm" data-placeholder="' . $this->language->get('filter_staff_placeholder') . '" data-label="' . $this->language->get('filter_staff') . '" id="staff" name="staff[]">';
                $end_select_html = '</select></div>';
                $getListStaff = $this->model_user_user->getUsers();
                $show_html .= $start_select_html . '<option value="" data-value="' . $this->language->get('filter_staff') . '">' . $this->language->get('filter_staff_placeholder') . '</option>';
                foreach ($getListStaff as $key => $vl) {
                    $show_html .= '<option value="' . $vl['user_id'] . '" data-value="' . $vl['lastname'] . ' ' . $vl['firstname'] . '">' . $vl['lastname'] . ' ' . $vl['firstname'] . '</option>';
                }
                $show_html .= $end_select_html;
            }
        } else {
            $show_html .= '<style>#optionone,#optiontwo{display:none !important}</style>';
        }

        echo $show_html .= '';
    }

    public function replace_url($url)
    {
        return str_replace("amp;", "", $url);
    }
}