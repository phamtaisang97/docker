<?php

use Image\Cloudinary_Manager;

class ControllerCatalogProduct extends Controller
{
    use Device_Util;
    use Sync_Bestme_Data_To_Welcome_Util;
    use Theme_Config_Util;

    private $error = array();
    const ON_BOARDING_STEP = 1;

    public function index()
    {
        $this->load->language('catalog/product');

        $this->document->setTitle($this->language->get('heading_title'));
        // add css,js only list product
        $this->document->addStyle('view/stylesheet/custom/product.css');

        $this->load->model('catalog/product');

        $this->load->model('custom/tag');

        $this->getList();
    }

    public function add()
    {
        $this->load->language('catalog/product');

        $this->document->setTitle($this->language->get('heading_title_add'));

        $this->load->model('catalog/product');
        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            if (empty($this->request->post['source'])) {
                $this->request->post['source'] = 'admin';
            }

            // set seo name by title
            if (!$this->request->post['seo_title']) {
                $this->request->post['seo_title'] = $this->request->post['name'];
            }

            // get first p of first description tab content to set seo description
            if (!$this->request->post['seo_desciption']) {
                if (isset($this->request->post['description_tab'][0])) {
                    $html_content = str_get_html(html_entity_decode($this->request->post['description_tab'][0], ENT_QUOTES, 'UTF-8'));
                    $first_p = $html_content->find('p', 0);
                    $this->request->post['seo_desciption'] = $first_p->plaintext ?? $this->request->post['name'];
                } else {
                    $this->request->post['seo_desciption'] = $this->request->post['name'];
                }
            }

            if (isset($this->request->post['noindex'])) {
                $this->request->post['noindex'] = 1;
            }
            else {
                $this->request->post['noindex'] = 0;
            }

            if (isset($this->request->post['nofollow'])) {
                $this->request->post['nofollow'] = 1;
            }
            else {
                $this->request->post['nofollow'] = 0;
            }

            if (isset($this->request->post['noarchive'])) {
                $this->request->post['noarchive'] = 1;
            }
            else {
                $this->request->post['noarchive'] = 0;
            }

            if (isset($this->request->post['noimageindex'])) {
                $this->request->post['noimageindex'] = 1;
            }
            else {
                $this->request->post['noimageindex'] = 0;
            }

            if (isset($this->request->post['nosnippet'])) {
                $this->request->post['nosnippet'] = 1;
            }
            else {
                $this->request->post['nosnippet'] = 0;
            }

            $this->model_catalog_product->addProduct($this->request->post);
            // on boarding step
            $config_on_boarding_step_active = $this->config->get('config_onboarding_step_active');
            $on_boarding_step_active = empty($config_on_boarding_step_active) ? [] : explode(',', $config_on_boarding_step_active);
            if (!is_null($config_on_boarding_step_active) && !in_array(self::ON_BOARDING_STEP, $on_boarding_step_active)) {
                array_push($on_boarding_step_active, self::ON_BOARDING_STEP);
                $this->model_setting_setting->editSettingValue('config', 'config_onboarding_step_active', implode(',', $on_boarding_step_active));
            }

            /* publish sync data */
            $shop_name = $this->model_setting_setting->getShopName();
            $onboarding_data = [
                'shop_domain' => $shop_name,
                'last_step' => self::ON_BOARDING_STEP,
                'steps' => is_array($on_boarding_step_active) ? count($on_boarding_step_active) : 0
            ];
            $this->publishSyncDataOnboarding($onboarding_data);

            $this->session->data['success'] = $this->language->get('text_success_add');

            $url = '';

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_model'])) {
                $url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_price'])) {
                $url .= '&filter_price=' . $this->request->get['filter_price'];
            }

            if (isset($this->request->get['filter_quantity'])) {
                $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

//            if (isset($this->request->get['page'])) {
//                $url .= '&page=' . $this->request->get['page'];
//            }

            $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getForm();
    }

    public function destroy()
    {
        $this->response->addHeader('Content-Type: application/json');
        $this->load->model('catalog/product');
        $json = [
            'status' => false,
            'message' => 'thất bại',
        ];
        if (isset($this->request->post['product_id'])) {
            $product_id = $this->request->post['product_id'];
            $this->model_catalog_product->deleteProduct($product_id);
            $json = [
                'status' => true,
                'message' => 'thanh cong'
            ];
            $this->response->setOutput(json_encode($json));
            return;

        }
        $this->response->setOutput(json_encode($json));
    }

    /**
     * edit product
     *
     * Duplicated code with function add()
     * TODO: re-use code (common function, extract function, ...), do not duplicate code...
     */
    public function edit()
    {
        $this->load->language('catalog/product');

        $this->document->setTitle($this->language->get('heading_title_edit'));

        $this->load->model('catalog/product');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            if (empty($this->request->post['source'])) {
                $this->request->post['source'] = 'admin';
            }

            if (isset($this->request->post['noindex'])) {
                $this->request->post['noindex'] = 1;
            }
            else {
                $this->request->post['noindex'] = 0;
            }

            if (isset($this->request->post['nofollow'])) {
                $this->request->post['nofollow'] = 1;
            }
            else {
                $this->request->post['nofollow'] = 0;
            }

            if (isset($this->request->post['noarchive'])) {
                $this->request->post['noarchive'] = 1;
            }
            else {
                $this->request->post['noarchive'] = 0;
            }

            if (isset($this->request->post['noimageindex'])) {
                $this->request->post['noimageindex'] = 1;
            }
            else {
                $this->request->post['noimageindex'] = 0;
            }

            if (isset($this->request->post['nosnippet'])) {
                $this->request->post['nosnippet'] = 1;
            }
            else {
                $this->request->post['nosnippet'] = 0;
            }

            $this->model_catalog_product->editProduct($this->request->get['product_id'], $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success_edit');

            $url = '';

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_model'])) {
                $url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_price'])) {
                $url .= '&filter_price=' . $this->request->get['filter_price'];
            }

            if (isset($this->request->get['filter_quantity'])) {
                $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getForm();
    }

    public function delete()
    {
        $this->load->language('catalog/product');

        if ($this->validateDelete()) {
            $this->document->setTitle($this->language->get('heading_title'));
            $this->load->model('catalog/product');

            $product_ids = $this->request->get['product_ids'];

            $product_ids = explode(',', $product_ids);
            if (isset($product_ids)) {
                foreach ($product_ids as $product_id) {
                    $infoProduct = $this->model_catalog_product->getProduct($product_id);
                    if (!empty($infoProduct['source']) && $infoProduct['source'] == 'omihub') {
                        continue;
                    }

                    $this->model_catalog_product->deleteProduct($product_id);
                }
            }

            $this->session->data['success'] = $this->language->get('delete_success');
            $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true));
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $this->session->data['error'] = $data['error_warning'];
        $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function copy()
    {
        $this->load->language('catalog/product');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/product');
        $json = [
            'status' => false,
            'message' => 'thất bại',
        ];
        $this->response->addHeader('Content-Type: application/json');

        // if (isset($this->request->post['selected']) && $this->validateCopy()) {
        //     foreach ($this->request->post['selected'] as $product_id) {
        //         $this->model_catalog_product->copyProduct($product_id);
        //     }

        //     $this->session->data['success'] = $this->language->get('text_success');

        //     $url = '';

        //     if (isset($this->request->get['filter_name'])) {
        //         $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        //     }

        //     if (isset($this->request->get['filter_model'])) {
        //         $url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
        //     }

        //     if (isset($this->request->get['filter_price'])) {
        //         $url .= '&filter_price=' . $this->request->get['filter_price'];
        //     }

        //     if (isset($this->request->get['filter_quantity'])) {
        //         $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
        //     }

        //     if (isset($this->request->get['filter_status'])) {
        //         $url .= '&filter_status=' . $this->request->get['filter_status'];
        //     }

        //     if (isset($this->request->get['sort'])) {
        //         $url .= '&sort=' . $this->request->get['sort'];
        //     }

        //     if (isset($this->request->get['order'])) {
        //         $url .= '&order=' . $this->request->get['order'];
        //     }

        //     if (isset($this->request->get['page'])) {
        //         $url .= '&page=' . $this->request->get['page'];
        //     }

        //     $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'] . $url, true));
        // }
        if (isset($this->request->post['product_id'])) {

            $product_id = $this->request->post['product_id'];
            $this->model_catalog_product->copyProduct($product_id);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_model'])) {
                $url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_price'])) {
                $url .= '&filter_price=' . $this->request->get['filter_price'];
            }

            if (isset($this->request->get['filter_quantity'])) {
                $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $json = [
                'status' => true,
                'message' => 'thanh cong'
            ];
            $this->response->setOutput(json_encode($json));
            return;
            // $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }
        $this->response->setOutput(json_encode($json));
        // $this->getList();
    }

    protected function getList()
    {
        //tung

        $category_product = $this->getValueFromRequest('get', 'category_product');
        $collection = $this->getValueFromRequest('get', 'collection');
        $status_product = $this->getValueFromRequest('get', 'status_product');
        $filter_name = $this->getValueFromRequest('get', 'filter_name');
        $manufacturer = $this->getValueFromRequest('get', 'manufacturer');
        $nametag = $this->getValueFromRequest('get', 'nametag');
        $amounttype = $this->getValueFromRequest('get', 'amounttype');
        $textamount = $this->getValueFromRequest('get', 'textamount');
        $page = $this->getValueFromRequest('get', 'page', 1);
        $filter_data = $this->getValueFromRequest('get', 'filter_data');
        $filter_staff = $this->getValueFromRequest('get', 'filter_staff');

        $url = '';
        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        // end default
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );
        $data['access_modify'] = $this->user->hasPermission('modify', 'catalog/product');
        $data['add'] = $this->url->link('catalog/product/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['copy'] = $this->url->link('catalog/product/copy', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['delete'] = $this->url->link('catalog/product/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['listProduct'] = $this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['is_admin'] = $this->user->isAdmin() ? 1 : 0;


        $data['categories'] = $this->url->link('catalog/category', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['avtivestatus'] = $this->replace_url($this->url->link('catalog/product/avtivestatus', ('user_token=' . $this->session->data['user_token'] . $url), true));
        $data['showProduct'] = $this->url->link('catalog/product/ShowHideProduct&active=1', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['hideProduct'] = $this->url->link('catalog/product/ShowHideProduct&active=0', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['loadOptionOne'] = $this->replace_url($this->url->link('catalog/product/loadoptionone', 'user_token=' . $this->session->data['user_token'] . $url, true));
        $data['products'] = array();

        if (!empty($filter_data)) {
            if (!empty($category_product)) {
                if (count($category_product) > 0) {
                    $category_product = implode(',', ($category_product));
                } else {
                    $category_product = (int)$category_product[0];
                }
            } else {
                $category_product = '';
            }

            if (isset($this->request->get['status_product'])) {
                $status_product = ($this->request->get['status_product']);
                if (count($status_product) > 1) {
                    $status_product = '';
                } else {
                    $status_product = (int)$status_product[0];
                }
            }

            if (!empty($collection)) {
                if (count($collection) > 0) {
                    $collection = implode(',', ($collection));
                } else {
                    $collection = (int)$collection[0];
                }
            } else {
                $collection = '';
            }

            if (!empty($manufacturer)) {
                if (count($manufacturer) > 0) {
                    $manufacturer = implode(',', ($manufacturer));
                } else {
                    $manufacturer = (int)$manufacturer[0];
                }
            } else {
                $manufacturer = '';
            }

            if (!empty($nametag)) {
                if (count($nametag) > 0) {
                    $nametag = implode('|', ($nametag));
                } else {
                    $nametag = (int)$nametag[0];
                }
            } else {
                $nametag = '';
            }

        }
        $filterStaff= null;
        $filter_staff = is_array($filter_staff) ? $filter_staff : [];

        if (!empty($filter_staff)) {
            if (count($filter_staff) > 0) {
                $filterStaff = implode(',', ($filter_staff));
            } else {
                $filterStaff = (int)$filter_staff[0];
            }
        } else {
            $filterStaff = '';
        }
        $filter_data = array(
            'filter_data' => $filter_data,
            'category_product' => $category_product,
            'filter_name' => $filter_name,
            'filter_staff' => $filterStaff,
            'status_product' => $status_product,
            'collection' => $collection,
            'manufacturer' => $manufacturer,
            'nametag' => $nametag,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        if(!$this->user->canAccessAll()){
            $filter_data['user_create_id'] = $this->user->getId();
        }
        //unset($_SESSION['category_product']);
        $product_total = $this->model_catalog_product->getTotalProductsCustom($category_product, $filter_data);
        $data['product_total'] = $product_total;
        $data['export_limited_rows'] = MAX_ROWS_EXPORT_PRODUCTS;
        $data['product_parts_count'] = $data['product_total'] > $data['export_limited_rows'] && 0 < $data['export_limited_rows'] ? ceil($data['product_total'] / $data['export_limited_rows']) : 0;
        $results = $this->model_catalog_product->getProductsCustom($category_product, $filter_data);
        $this->load->model('catalog/category');
        $listCategories = $this->model_catalog_category->getCategories();
        foreach ($listCategories as $value) {
            $name = $this->removeChar($value, 'name', '>');
            $data['list_categories'][] = array(
                'category_id' => $value['category_id'],
                //'name' => str_replace(' ', '', $name)
                'name' => $name
            );
        }

        $this->load->model('tool/image');
        $this->load->model('catalog/manufacturer');
        foreach ($results as $result) {

            if ($result['image'] != '') {
                $image = $this->model_tool_image->resize($result['product_image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
            }
            $category_names = $this->model_catalog_product->getProductCategoryName($result['product_id']);
            $category_name_product = is_array($category_names) ? implode(',', $category_names) : '';
            $name_cate = (!empty($category_name_product) ? $category_name_product : '');
            $array_manufacturer = $this->model_catalog_manufacturer->getManufacturer($result['manufacturer_id']);
            $name_manufacturer = (isset($array_manufacturer['name']) ? $array_manufacturer['name'] : '');

            $quantity = $this->model_catalog_product->countProductInStoreDefault($result['product_id']);
            if (isset($result['multi_versions']) && $result['multi_versions'] == 1) {
                $count_version = $this->model_catalog_product->getCountVersionProduct($result['product_id']);
                $html_version = $this->language->get('product_of_list') . ' ' . $count_version . ' ' . $this->language->get('product_of_version');
            } else {
                $html_version = '';
            }

            $categories_selected = $this->model_catalog_product->getProductCategories($result['product_id'], false);
            $manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($result['manufacturer_id']);
            $data['products'][] = array(
                'product_id' => $result['product_id'],
                'name' => $result['product_name'],
                'model' => $result['model'],
                'image' => $image,
                'html_version' => $html_version,
                'price' => $this->currency->format($result['price'], $this->config->get('config_currency')),
                'compare_price' => $this->currency->format($result['compare_price'], $this->config->get('config_currency')),
                'quantity' => $quantity,
                'sku' => $result['sku'],
                'status' => $result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
                'date_modified' => $result['date_modified'],
                'edit' => $this->url->link('catalog/product/edit', 'user_token=' . $this->session->data['user_token'] . '&product_id=' . $result['product_id'] . $url, true),
                // 'destroy'       => $this->url->link('catalog/product/destroy', 'user_token=' . $this->session->data['user_token'] . $url, true),
                'view' => $this->url->link('catalog/product/view', 'user_token=' . $this->session->data['user_token'] . '&product_id=' . $result['product_id'] . $url, true),
                'nameCategory' => (isset($name_cate) ? $name_cate : ''),
                'manufacturer' => $name_manufacturer,
                'activeStatus' => $result['status'],
                'user_name' => $result['fullname'],
                'categories_selected' => json_encode($categories_selected),
                'manufacturer_selected' => json_encode($manufacturer_info),
                'source' => $result['source'],
                'disable_update' => 'omihub' != $result['source']
            );
        }
        $data['user_token'] = $this->session->data['user_token'];
        $data['destroy'] = $this->url->link('catalog/product/destroy', 'user_token=' . $this->session->data['user_token'] . $url, true);

        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $product_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        // Add jump to page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);

        //get all tag
        $data['tag_all'] = $this->model_custom_tag->getAllTagValueProduct();
        $data['action_add_tag'] = $this->url->link('catalog/product/addTags', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_remove_tag'] = $this->url->link('catalog/product/removeTags', 'user_token=' . $this->session->data['user_token'], true);

        if (isset($this->session->data['success']) && $this->session->data['success'] != 'text_success') {
            $data['success'] = $this->session->data['success'];

            $this->session->data['success'] = '';
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];

            $this->session->data['error'] = '';
            unset($this->session->data['error']);
        } else {
            $data['error_warning'] = '';
        }

        // list for check exist manufactures
        $manufactures_pause_transaction = $this->model_catalog_manufacturer->getManufacturersPauseTransaction();
        $data['manufactures_pause_transaction'] = json_encode($manufactures_pause_transaction, JSON_HEX_APOS);

        //end get all tag
        $data['action_add_collection'] = $this->url->link('catalog/product/addCollections', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_remove_collection'] = $this->url->link('catalog/product/removeCollections', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_add_category'] = $this->url->link('catalog/product/addCategory', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_add_manufacturer'] = $this->url->link('catalog/product/addProductManufacturer', 'user_token=' . $this->session->data['user_token'], true);
        //get url status
        $data['action_in_stock_product'] = $this->url->link('catalog/product/changeInStockProduct', 'user_token=' . $this->session->data['user_token'], true);
        $data['action_out_stock_product'] = $this->url->link('catalog/product/changeOutStockProduct', 'user_token=' . $this->session->data['user_token'], true);
        //end

        $data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));

        $data['testfileLink'] = $this->url->link('catalog/product/importFromExcelFile', 'user_token=' . $this->session->data['user_token'], true);

        $data['url_filter'] = $this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['filter_name'] = $filter_name;
        //$data['category_id'] = $category_id;
        //$data['filter_status'] = $filter_status;
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $data['link_down'] = $this->config->get('config_language') == 'vi-vn' ? SAMPLE_PRODUCT_LIST_UPLOAD : SAMPLE_PRODUCT_LIST_UPLOAD_EN;
        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('catalog/product_list_append', $data));
        } else {
            $this->response->setOutput($this->load->view('catalog/product_list', $data));
        }
    }

    protected function getForm()
    {
        $data['text_form'] = !isset($this->request->get['product_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

        $data['is_edit'] = isset($this->request->get['product_id']) ? 1 : 0;

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = array();
        }

        if (isset($this->error['has_discount'])) {
            $data['error_has_discount'] = $this->error['has_discount'];
        } else {
            $data['error_has_discount'] = array();
        }

        if (isset($this->error['meta_title'])) {
            $data['error_meta_title'] = $this->error['meta_title'];
        } else {
            $data['error_meta_title'] = array();
        }

        if (isset($this->error['model'])) {
            $data['error_model'] = $this->error['model'];
        } else {
            $data['error_model'] = '';
        }

        if (isset($this->error['keyword'])) {
            $data['error_keyword'] = $this->error['keyword'];
        } else {
            $data['error_keyword'] = '';
        }

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_model'])) {
            $url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_price'])) {
            $url .= '&filter_price=' . $this->request->get['filter_price'];
        }

        if (isset($this->request->get['filter_quantity'])) {
            $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );
        if (isset($this->request->get['product_id'])) {
            $data['breadcrumbs'][] = array(
                'text' => $data['text_form'],
                'href' => '',
            );
        } else {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_add'),
                'href' => ''
            );
        }
        if (!isset($this->request->get['product_id'])) {
            $data['action'] = $this->url->link('catalog/product/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        } else {
            $data['action'] = $this->url->link('catalog/product/edit', 'user_token=' . $this->session->data['user_token'] . '&product_id=' . $this->request->get['product_id'], true);
            $data['product_id'] = $this->request->get['product_id'];
        }

        $data['cancel'] = $this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'] . $url, true);

        if (isset($this->request->get['product_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);
            if (empty($product_info)) {
                $data['product_deleted'] = true;
            }
        }

        $data['new_tag_prefix'] = ModelCatalogProduct::ORDER_TAG_NEW_PREFIX;
        $data['user_token'] = $this->session->data['user_token'];

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();
        if (isset($this->request->post['product_description'])) {
            $data['product_description'] = $this->request->post['product_description'];
        } elseif (isset($this->request->get['product_id'])) {
            $data['product_description'] = $this->model_catalog_product->getProductDescriptions($this->request->get['product_id']);
        } else {
            $data['product_description'] = array();
        }

        if (isset($this->request->post['name'])) {
            $data['product_description']['name'] = $this->request->post['name'];
        }

        if (isset($this->request->post['description'])) {
            $data['product_description']['description'] = $this->request->post['description'];
        }

        if (isset($this->request->post['summary'])) {
            $data['product_description']['sub_description'] = $this->request->post['summary'];
        }

        if (isset($this->request->post['seo_title'])) {
            $data['product_description']['seo_title'] = $this->request->post['seo_title'];
        }

        // keywords
        if (isset($this->request->post['meta_keyword'])) {
            $data['product_description']['meta_keyword'] = $this->request->post['meta_keyword'];
        }

        if (isset($this->request->post['seo_desciption'])) {
            $data['product_description']['seo_description'] = $this->request->post['seo_desciption'];
        }

        if (isset($this->request->post['noindex'])) {
            $data['product_description']['noindex'] = $this->request->post['noindex'];
        }

        if (isset($this->request->post['nofollow'])) {
            $data['product_description']['nofollow'] = $this->request->post['nofollow'];
        }

        if (isset($this->request->post['noarchive'])) {
            $data['product_description']['noarchive'] = $this->request->post['noarchive'];
        }

        if (isset($this->request->post['noimageindex'])) {
            $data['product_description']['noimageindex'] = $this->request->post['noimageindex'];
        }

        if (isset($this->request->post['nosnippet'])) {
            $data['product_description']['nosnippet'] = $this->request->post['nosnippet'];
        }

        // description tab
        if (isset($this->request->get['product_id'])) {
            $data['product_description_tabs'] = $this->model_catalog_product->getProductDescriptionTab($this->request->get['product_id']);
        }

        if (isset($this->request->post['model'])) {
            $data['model'] = $this->request->post['model'];
        } elseif (!empty($product_info)) {
            $data['model'] = $product_info['model'];
        } else {
            $data['model'] = '';
        }

        if (isset($this->request->post['sku'])) {
            $data['sku'] = $this->request->post['sku'];
        } elseif (!empty($product_info)) {
            $data['sku'] = $product_info['sku'];
        } else {
            $data['sku'] = '';
        }
        if (isset($this->request->post['barcode'])) {
            $data['barcode'] = $this->request->post['barcode'];
        } elseif (!empty($product_info)) {
            $data['barcode'] = $product_info['barcode'];
        } else {
            $data['barcode'] = '';
        }

        if (isset($this->request->post['source'])) {
            $data['source'] = $this->request->post['source'];
        } elseif (!empty($product_info)) {
            $data['source'] = $product_info['source'];
        } else {
            $data['source'] = '';
        }

        if (isset($this->request->post['common_barcode'])) {
            $data['common_barcode'] = $this->request->post['common_barcode'];
        } elseif (!empty($product_info)) {
            $data['common_barcode'] = $product_info['common_barcode'];
        } else {
            $data['common_barcode'] = '';
        }

        if (isset($this->request->post['common_sku'])) {
            $data['common_sku'] = $this->request->post['common_sku'];
        } elseif (!empty($product_info)) {
            $data['common_sku'] = $product_info['common_sku'];
        } else {
            $data['common_sku'] = '';
        }

        if (isset($this->request->post['common_price'])) {
            $data['common_price'] = (int)str_replace(',', '', $this->request->post['common_price']);
        } elseif (!empty($product_info)) {
            $data['common_price'] = (int)str_replace(',', '', $product_info['common_price']);
        } else {
            $data['common_price'] = 0;
        }

        if (isset($this->request->post['common_cost_price'])) {
            $data['common_cost_price'] = (int)str_replace(',', '', $this->request->post['common_cost_price']);
        } elseif (!empty($product_info)) {
            $data['common_cost_price'] = (int)str_replace(',', '', $product_info['common_cost_price']);
        } else {
            $data['common_cost_price'] = 0;
        }

        if (isset($this->request->post['common_compare_price'])) {
            $data['common_compare_price'] = (int)str_replace(',', '', $this->request->post['common_compare_price']);
        } elseif (!empty($product_info)) {
            $data['common_compare_price'] = (int)str_replace(',', '', $product_info['common_compare_price']);
        } else {
            $data['common_compare_price'] = '';
        }

        if (isset($this->request->post['upc'])) {
            $data['upc'] = $this->request->post['upc'];
        } elseif (!empty($product_info)) {
            $data['upc'] = $product_info['upc'];
        } else {
            $data['upc'] = '';
        }

        if (isset($this->request->post['location'])) {
            $data['location'] = $this->request->post['location'];
        } elseif (!empty($product_info)) {
            $data['location'] = $product_info['location'];
        } else {
            $data['location'] = '';
        }

        if (isset($this->request->post['user_create_id'])) {
            $data['user_create_id'] = $this->request->post['user_create_id'];
        } elseif (!empty($product_info)) {
            $data['user_create_id'] = $product_info['user_create_id'];
        } else {
            $data['user_create_id'] = $this->user->getId();
        }

        $this->load->model('setting/store');

        $data['stores'] = [];
        $stores = $this->model_setting_store->getStoresForUser([]);
        foreach ($stores as $store) {
            $data['stores'][] = array(
                'store_id' => $store['id'],
                'name' => $store['text']
            );
        }

        if (isset($this->request->post['product_store'])) {
            $data['product_store'] = $this->request->post['product_store'];
        } elseif (isset($this->request->get['product_id'])) {
            $data['product_store'] = $this->model_catalog_product->getProductStores($this->request->get['product_id']);
        } else {
            $data['product_store'] = array(0);
        }

        if (isset($this->request->post['shipping'])) {
            $data['shipping'] = $this->request->post['shipping'];
        } elseif (!empty($product_info)) {
            $data['shipping'] = $product_info['shipping'];
        } else {
            $data['shipping'] = 1;
        }

        if (isset($this->request->post['promotion_price'])) {
            $data['price'] = (int)str_replace(',', '', $this->request->post['promotion_price']);
        } elseif (!empty($product_info)) {
            $data['price'] = (int)str_replace(',', '', $product_info['price']);
        } else {
            $data['price'] = '';
        }
        if (isset($this->request->post['price'])) {
            $data['compare_price'] = (int)str_replace(',', '', $this->request->post['price']);
        } elseif (!empty($product_info)) {
            $data['compare_price'] = (int)str_replace(',', '', $product_info['compare_price']);
        } else {
            $data['compare_price'] = '';
        }
        //unit price and compare price
        if (isset($this->request->post['price_unit'])) {
            $data['price_unit'] = $this->request->post['price_unit'];
        } elseif (!empty($product_info)) {
            $data['price_unit'] = $product_info['price_currency_id'];
        } else {
            $data['price_unit'] = '';
        }
        if (isset($this->request->post['compare_price_unit'])) {
            $data['compare_price_unit'] = $this->request->post['compare_price_unit'];
        } elseif (!empty($product_info)) {
            $data['compare_price_unit'] = $product_info['c_price_currency_id'];
        } else {
            $data['compare_price_unit'] = '';
        }

        $this->load->model('catalog/recurring');

        $data['recurrings'] = $this->model_catalog_recurring->getRecurrings();

        if (isset($this->request->post['product_recurrings'])) {
            $data['product_recurrings'] = $this->request->post['product_recurrings'];
        } elseif (!empty($product_info)) {
            $data['product_recurrings'] = $this->model_catalog_product->getRecurrings($product_info['product_id']);
        } else {
            $data['product_recurrings'] = array();
        }

        $this->load->model('localisation/tax_class');

        $data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

        if (isset($this->request->post['tax_class_id'])) {
            $data['tax_class_id'] = $this->request->post['tax_class_id'];
        } elseif (!empty($product_info)) {
            $data['tax_class_id'] = $product_info['tax_class_id'];
        } else {
            $data['tax_class_id'] = 0;
        }

        if (isset($this->request->post['date_available'])) {
            $data['date_available'] = $this->request->post['date_available'];
        } elseif (!empty($product_info)) {
            $data['date_available'] = ($product_info['date_available'] != '0000-00-00') ? $product_info['date_available'] : '';
        } else {
            $data['date_available'] = date('Y-m-d');
        }

        if (isset($this->request->post['quantity'])) {
            $data['quantity'] = $this->request->post['quantity'];
        } elseif (!empty($product_info)) {
            $data['quantity'] = $product_info['quantity'];
        } else {
            $data['quantity'] = 1;
        }

        if (isset($this->request->post['product_version'])) {
            $data['multi_versions'] = $this->request->post['product_version'];
        } elseif (!empty($product_info)) {
            $data['multi_versions'] = $product_info['multi_versions'];
        } else {
            $data['multi_versions'] = 0;
        }

        // default_store_id
        if (isset($this->request->post['store_default'])) {
            $data['default_store_id'] = $this->request->post['store_default'];
        } elseif (!empty($product_info)) {
            $data['default_store_id'] = $product_info['default_store_id'];
        } else {
            $data['default_store_id'] = 0;
        }

        // product to store
        if (isset($this->request->post['stores'])) {
            $data['product_to_stores'] = $this->request->post['stores'];
        } elseif (isset($this->request->get['product_id'])) {
            $data['product_to_stores'] = $this->model_catalog_product->getProductToStores($this->request->get['product_id']);
            foreach($data['product_to_stores'] as $key => $store){
                if ($store['store_id'] == $data['default_store_id']){
                    unset($data['product_to_stores'][$key]);
                    array_unshift($data['product_to_stores'], $store);
                }
            }

        } else {
            $data['product_to_stores'] = [];
        }

        foreach ($data['product_to_stores'] as &$store){
            if (!isset($store['store_id']) || !isset($store['product_id'])) {
                continue;
            }

            $store['product_on_delivery'] = $this->model_catalog_product->getProductDelivery($store['store_id'], $store['product_id']);
        }
        unset($store);

        $this->load->model('localisation/stock_status');

        $data['stock_statuses'] = $this->model_localisation_stock_status->getStockStatuses();

        if (isset($this->request->post['stock_status_id'])) {
            $data['stock_status_id'] = $this->request->post['stock_status_id'];
        } elseif (!empty($product_info)) {
            $data['stock_status_id'] = $product_info['stock_status_id'];
        } else {
            $data['stock_status_id'] = 0;
        }

        if (isset($this->request->post['sale_on_out_of_stock'])) {
            $data['sale_on_out_of_stock'] = $this->request->post['sale_on_out_of_stock'];
        } elseif (!empty($product_info)) {
            $data['sale_on_out_of_stock'] = $product_info['sale_on_out_of_stock'];
        } else {
            $data['sale_on_out_of_stock'] = true;
        }

        // seo preview
        $data['server_name'] = $_SERVER['SERVER_NAME'];
        if (isset($this->request->post['alias'])) {
            $data['product_alias'] = $this->request->post['alias'];
        } else if (!empty($product_info)) {
            $product_alias = $this->model_catalog_product->getAliasById($product_info['product_id']);
            $data['product_alias'] = array_key_exists('keyword', $product_alias) ? $product_alias['keyword'] : '';
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($product_info)) {
            $data['status'] = $product_info['status'];
        } else {
            $data['status'] = true;
        }

        if (isset($this->request->post['show_gg_product_feed'])) {
            $data['show_gg_product_feed'] = $this->request->post['show_gg_product_feed'];
        } elseif (!empty($product_info)) {
            $data['show_gg_product_feed'] = $product_info['show_gg_product_feed'];
        } else {
            $data['show_gg_product_feed'] = true;
        }

        if (isset($this->request->post['weight'])) {
            $data['weight'] = (float)$this->request->post['weight'];
        } elseif (!empty($product_info)) {
            $data['weight'] = (float)$product_info['weight'];
        } else {
            $data['weight'] = '';
        }
        $this->load->model('localisation/weight_class');

        $data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();
        if (isset($this->request->post['weight_class_id'])) {
            $data['weight_class_id'] = $this->request->post['weight_class_id'];
        } elseif (!empty($product_info)) {
            $data['weight_class_id'] = $product_info['weight_class_id'];
        } else {
            $data['weight_class_id'] = $this->config->get('config_weight_class_id');
        }
        $data['weight_list'] = $this->model_localisation_weight_class->getWeightList();
        if (isset($this->request->post['length'])) {
            $data['length'] = $this->request->post['length'];
        } elseif (!empty($product_info)) {
            $data['length'] = $product_info['length'];
        } else {
            $data['length'] = '';
        }

        if (isset($this->request->post['width'])) {
            $data['width'] = $this->request->post['width'];
        } elseif (!empty($product_info)) {
            $data['width'] = $product_info['width'];
        } else {
            $data['width'] = '';
        }

        if (isset($this->request->post['height'])) {
            $data['height'] = $this->request->post['height'];
        } elseif (!empty($product_info)) {
            $data['height'] = $product_info['height'];
        } else {
            $data['height'] = '';
        }

        if (isset($this->request->post['channel'])) {
            $data['channel'] = $this->request->post['channel'];
        } elseif (!empty($product_info)) {
            $data['channel'] = $product_info['channel'];
        } else {
            $data['channel'] = 0;
        }

        $this->load->model('localisation/length_class');

        $data['length_classes'] = $this->model_localisation_length_class->getLengthClasses();

        if (isset($this->request->post['length_class_id'])) {
            $data['length_class_id'] = $this->request->post['length_class_id'];
        } elseif (!empty($product_info)) {
            $data['length_class_id'] = $product_info['length_class_id'];
        } else {
            $data['length_class_id'] = $this->config->get('config_length_class_id');
        }

        $this->load->model('catalog/manufacturer');

        if (isset($this->request->post['manufacturer'])) {
            $data['manufacturer_id'] = $this->request->post['manufacturer'];
            $manufacturer_selected = $this->model_catalog_manufacturer->getManufacturer($data['manufacturer_id']);
            if ($manufacturer_selected) {
                $data['manufacturer_selected'] = json_encode([0 => array(
                    'id' => $manufacturer_selected['manufacturer_id'],
                    'title' => $manufacturer_selected['name'],
                )]);
            }
        } elseif (!empty($product_info)) {
            $data['manufacturer_id'] = $product_info['manufacturer_id'];
            $manufacturer_selected = $this->model_catalog_manufacturer->getManufacturer($product_info['manufacturer_id']);
            if ($manufacturer_selected) {
                $data['manufacturer_selected'] = json_encode([0 => array(
                    'id' => $manufacturer_selected['manufacturer_id'],
                    'title' => $manufacturer_selected['name'],
                )]);
            }
        } else {
            $data['manufacturer_id'] = 0;
        }
        // if (isset($this->request->post['manufacturer'])) {
        //     $data['manufacturer'] = $this->request->post['manufacturer'];
        // } elseif (!empty($product_info)) {
        //     $data['manufacturer'] = $product_info['manufacturer_id'];
        // } else {
        //     $data['manufacturer'] = '';
        // }

        if (isset($this->request->post['manufacturer'])) {
            $data['manufacturer'] = $this->request->post['manufacturer'];
        } elseif (!empty($product_info)) {
            $manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($product_info['manufacturer_id']);

            if ($manufacturer_info) {
                $data['manufacturer'] = $manufacturer_info['name'];
            } else {
                $data['manufacturer'] = '';
            }
        } else {
            $data['manufacturer'] = '';
        }

        // list for check exist
        $manufactures_pause_transaction = $this->model_catalog_manufacturer->getManufacturersPauseTransaction();
        $data['manufactures_pause_transaction'] = json_encode($manufactures_pause_transaction, JSON_HEX_APOS);

        // Categories
        $this->load->model('catalog/category');
        $categories_selected = [];

        if (isset($this->request->post['category'])) {
            $categories = $this->request->post['category'];
            $data['categories_selected'] = array();
            foreach ($categories as $category_id) {
                $category_info = $this->model_catalog_category->getCategory($category_id);

                if ($category_info) {
                    $categories_selected[] = array(
                        'id' => $category_info['category_id'],
                        'title' => $category_info['name']
                    );
                }
            }

        } elseif (isset($this->request->get['product_id'])) {
            $categories_selected = $this->model_catalog_product->getProductCategories($this->request->get['product_id'], false);
        }

        $data['categories_selected'] = json_encode($categories_selected);

        //store selected
        $this->load->model('setting/store');
        $store_selected = [];
        if (isset($this->request->post['store_list'])) {
            $stores = $this->request->post['store_list'];
            foreach ($stores as $store) {
                $store_info = $this->model_setting_store->getStore($store);
                if ($store_info) {
                    $store_selected[] = array(
                        'id' => $store_info['store_id'],
                        'title' => $store_info['name']
                    );
                }
            }

        } elseif (isset($this->request->get['product_id'])) {
            $store_selected = $this->model_catalog_product->getStoreByProductId($this->request->get['product_id']);
        }  else {
            // default store 0
            $store_info = $this->model_setting_store->getStore(0);
            if ($store_info) {
                $store_selected[] = array(
                    'id' => $store_info['store_id'],
                    'title' => $store_info['name']
                );
            }
        }
        $data['store_selected'] = json_encode($store_selected);

        // Filters
        $this->load->model('catalog/filter');
        if (isset($this->request->post['product_filter'])) {
            $filters = $this->request->post['product_filter'];
        } elseif (isset($this->request->get['product_id'])) {
            $filters = $this->model_catalog_product->getProductFilters($this->request->get['product_id']);
        } else {
            $filters = array();
        }
        $listFilter = $this->model_catalog_filter->getFilters();
        foreach ($listFilter as $value) {
            $name = $this->removeChar($value, 'name', '>');
            $data['list_filter'][] = array(
                'filter_id' => $value['filter_id'],
                //'name' => str_replace(' ', '', $name)
                'name' => $name
            );
        }
        $listManufacture = $this->model_catalog_manufacturer->getManufacturers();
        foreach ($listManufacture as $value) {
            $data['list_manufacturer'][] = array(
                'manufacturer_id' => $value['manufacturer_id'],
                'name' => $value['name']
            );
        }

        $data['product_filters'] = array();
        foreach ($filters as $filter_id) {
            $filter_info = $this->model_catalog_filter->getFilter($filter_id);

            if ($filter_info) {
                $data['product_filters'][] = array(
                    'filter_id' => $filter_info['filter_id'],
                    'name' => $filter_info['group'] . ' &gt; ' . $filter_info['name']
                );
                $data['product_filters_list_id'][] = $filter_info['filter_id'];
            }
        }
        // Attributes
        $this->load->model('catalog/attribute');

        $data['product_attributes'] = array();
        if (isset($this->request->post['attribute_name']) && isset($this->request->post['attribute_values'])) {
            $product_attribute_names = is_array($this->request->post['attribute_name']) ? $this->request->post['attribute_name'] : [];
            $product_attribute_values = is_array($this->request->post['attribute_values']) ? $this->request->post['attribute_values'] : [];
            foreach ($product_attribute_names as $k => $product_attribute_name) {
                $data['product_attributes'][] = array(
                    'name' => $product_attribute_name,
                    'detail' => isset($product_attribute_values[$k]) ? $product_attribute_values[$k] : []
                );
            }
        } elseif (isset($this->request->get['product_id'])) {
            $product_attributes = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);
            foreach ($product_attributes as $product_attribute) {
                $attribute_info = $this->model_catalog_attribute->getAttribute($product_attribute['attribute_id']);

                if ($attribute_info) {
                    $data['product_attributes'][] = array(
                        'attribute_id' => $product_attribute['attribute_id'],
                        'name' => $attribute_info['name'],
                        'detail' => explode(",", $product_attribute['product_attribute_description']['text'])
                    );
                }
            }
        }


        if (isset($this->request->post['product_version'])) {
            $data['product_versions'] = array();
            $post_product_displays = isset($this->request->post['product_display']) ? $this->request->post['product_display'] : [];
            $post_product_version_names = isset($this->request->post['product_version_names']) ? $this->request->post['product_version_names'] : [];
            $post_product_prices = isset($this->request->post['product_prices']) ? $this->request->post['product_prices'] : [];
            $post_product_promotion_prices = isset($this->request->post['product_promotion_prices']) ? $this->request->post['product_promotion_prices'] : [];
            $post_product_skus = isset($this->request->post['product_skus']) ? $this->request->post['product_skus'] : [];
            $post_product_cost_prices = isset($this->request->post['version_cost_price']) ? $this->request->post['version_cost_price'] : [];
            $post_product_barcodes = isset($this->request->post['product_barcodes']) ? $this->request->post['product_barcodes'] : [];
            $post_product_quantities = isset($this->request->post['product_quantities']) ? $this->request->post['product_quantities'] : [];
            $post_product_version_images = isset($this->request->post['product_version_images']) ? $this->request->post['product_version_images'] : [];
            $post_product_version_image_alts = isset($this->request->post['product_version_image_alts']) ? $this->request->post['product_version_image_alts'] : [];
            foreach ($post_product_version_names as $k => $post_product_version_name) {
                $data['product_versions'][] = array(
                    'version' => $post_product_version_name,
                    'price' => isset($post_product_promotion_prices[$k]) ? $post_product_promotion_prices[$k] : 0,
                    'compare_price' => isset($post_product_prices[$k]) ? $post_product_prices[$k] : 0,
                    'sku' => isset($post_product_skus[$k]) ? $post_product_skus[$k] : '',
                    'barcode' => isset($post_product_barcodes[$k]) ? $post_product_barcodes[$k] : '',
                    'quantity' => isset($post_product_quantities[$k]) ? $post_product_quantities[$k] : 0,
                    'version_cost_price' => isset($post_product_cost_prices[$k]) ? $post_product_cost_prices[$k] : 0,
                    'status' => in_array($post_product_version_name, $post_product_displays) ? 1 : 0,
                    'image' => isset($post_product_version_images[$k]) ? $post_product_version_images[$k] : null,
                    'image_alt' => isset($post_product_version_image_alts[$k]) ? $post_product_version_image_alts[$k] : null
                );
            }
        } elseif (isset($this->request->get['product_id'])) {
            $product_versions = $this->model_catalog_product->getProductVersions($this->request->get['product_id']);
            foreach ($product_versions as &$version) {
                $version['version'] = implode(' • ', explode(',', $version['version']));
                $version['version_for_store'] = $this->replaceVersionValues($version['version']);

                $version['price'] = (int)str_replace(',', '', $version['price']);
                $version['compare_price'] = (int)str_replace(',', '', $version['compare_price']);
                $version['product_to_stores'] = $this->model_catalog_product->getProductToStores($this->request->get['product_id'], $version['product_version_id']);
                $version['quantity_total'] = 0;
                foreach ($version['product_to_stores'] as $key => &$item){
                    if ($item['product_version_id'] == $version['product_version_id']){
                        $version['quantity_total'] += $item['quantity'];
                        $item['product_on_delivery'] = $this->model_catalog_product->getProductDelivery($item['store_id'], $item['product_id'], $item['product_version_id']);
                    }

                    if ($item['store_id'] == $data['default_store_id']){
                        unset($version['product_to_stores'][$key]);
                        array_unshift($version['product_to_stores'], $item);
                    }
                }
                unset($item);
            }
            $data['product_versions'] = $product_versions;

        } else {
            $data['product_versions'] = array();
        }


        // Options
        $this->load->model('catalog/option');

        if (isset($this->request->post['product_option'])) {
            $product_options = $this->request->post['product_option'];
        } elseif (isset($this->request->get['product_id'])) {
            $product_options = $this->model_catalog_product->getProductOptions($this->request->get['product_id']);
        } else {
            $product_options = array();
        }

        $data['product_options'] = array();

        foreach ($product_options as $product_option) {
            $product_option_value_data = array();

            if (isset($product_option['product_option_value'])) {
                foreach ($product_option['product_option_value'] as $product_option_value) {
                    $product_option_value_data[] = array(
                        'product_option_value_id' => $product_option_value['product_option_value_id'],
                        'option_value_id' => $product_option_value['option_value_id'],
                        'quantity' => $product_option_value['quantity'],
                        'subtract' => $product_option_value['subtract'],
                        'price' => $product_option_value['price'],
                        'price_prefix' => $product_option_value['price_prefix'],
                        'points' => $product_option_value['points'],
                        'points_prefix' => $product_option_value['points_prefix'],
                        'weight' => $product_option_value['weight'],
                        'weight_prefix' => $product_option_value['weight_prefix']
                    );
                }
            }

            $data['product_options'][] = array(
                'product_option_id' => $product_option['product_option_id'],
                'product_option_value' => $product_option_value_data,
                'option_id' => $product_option['option_id'],
                'name' => $product_option['name'],
                'type' => $product_option['type'],
                'value' => isset($product_option['value']) ? $product_option['value'] : '',
                'required' => $product_option['required']
            );
        }

        $data['option_values'] = array();

        foreach ($data['product_options'] as $product_option) {
            if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
                if (!isset($data['option_values'][$product_option['option_id']])) {
                    $data['option_values'][$product_option['option_id']] = $this->model_catalog_option->getOptionValues($product_option['option_id']);
                }
            }
        }

        $this->load->model('customer/customer_group');

        $data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();

        if (isset($this->request->post['product_discount'])) {
            $product_discounts = $this->request->post['product_discount'];
        } elseif (isset($this->request->get['product_id'])) {
            $product_discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);
        } else {
            $product_discounts = array();
        }

        $data['product_discounts'] = array();

        foreach ($product_discounts as $product_discount) {
            $data['product_discounts'][] = array(
                'customer_group_id' => $product_discount['customer_group_id'],
                'quantity' => $product_discount['quantity'],
                'priority' => $product_discount['priority'],
                'price' => $product_discount['price'],
                'date_start' => ($product_discount['date_start'] != '0000-00-00') ? $product_discount['date_start'] : '',
                'date_end' => ($product_discount['date_end'] != '0000-00-00') ? $product_discount['date_end'] : ''
            );
        }

        if (isset($this->request->post['product_special'])) {
            $product_specials = $this->request->post['product_special'];
        } elseif (isset($this->request->get['product_id'])) {
            $product_specials = $this->model_catalog_product->getProductSpecials($this->request->get['product_id']);
        } else {
            $product_specials = array();
        }

        $data['product_specials'] = array();

        foreach ($product_specials as $product_special) {
            $data['product_specials'][] = array(
                'customer_group_id' => $product_special['customer_group_id'],
                'priority' => $product_special['priority'],
                'price' => $product_special['price'],
                'date_start' => ($product_special['date_start'] != '0000-00-00') ? $product_special['date_start'] : '',
                'date_end' => ($product_special['date_end'] != '0000-00-00') ? $product_special['date_end'] : ''
            );
        }
//        // tag collection
//        if (isset($this->request->get['product_id'])) {
//            $this->load->model('custom/tag');
//            $this->load->model('custom/collection');
//            $data['tag_list'] = json_encode($this->model_custom_tag->getTagsByProductId($this->request->get['product_id']));
//            $data['collection_list'] = json_encode($this->model_custom_collection->getCollectionsByProductId($this->request->get['product_id']));
//        }

        // collection
        $collection_list = [];
        if (isset($this->request->post['collection_list'])) {
            $this->load->model('catalog/collection');
            $collection_list_id = is_array($this->request->post['collection_list']) ? $this->request->post['collection_list'] : [];
            foreach ($collection_list_id as $collection_id) {
                $collection_info = $this->model_catalog_collection->getCollection($collection_id);
                if ($collection_info) {
                    $collection_list[] = array(
                        'id' => $collection_info['collection_id'],
                        'title' => $collection_info['title']
                    );
                }
            }
        } else if (isset($this->request->get['product_id'])) {
            $this->load->model('custom/collection');
            $collection_list = $this->model_custom_collection->getCollectionsByProductId($this->request->get['product_id']);
        }

        $data['collection_list_empty'] = false;
        if ($collection_list) {
            $data['collection_list_empty'] = true;
        }
        $data['collection_list'] = json_encode($collection_list);
        // tag
        $tag_list = [];
        if (isset($this->request->post['tag_list'])) {
            $this->load->model('custom/tag');
            $tag_list_id = is_array($this->request->post['tag_list']) ? $this->request->post['tag_list'] : [];
            foreach ($tag_list_id as $tag_id) {
                $collection_info = $this->model_custom_tag->getTagById($tag_id);
                if ($collection_info) {
                    $tag_list[] = array(
                        'id' => $collection_info['tag_id'],
                        'title' => $collection_info['value']
                    );
                }
            }
        } else if (isset($this->request->get['product_id'])) {
            $this->load->model('custom/tag');
            $tag_list = $this->model_custom_tag->getTagsByProductId($this->request->get['product_id']);
        }

        $data['tag_list'] = json_encode($tag_list);

        // Image
        $this->load->model('tool/image');

        if (isset($this->request->post['images'])) {
            $data['thumb'] = isset($this->request->post['images'][0]) ? $this->request->post['images'][0] : '';
            if (isset($this->request->post['image-alts'])) {
                $data['thumb_alt'] = isset($this->request->post['image-alts'][0]) ? $this->request->post['image-alts'][0] : '';
            }
        } elseif (!empty($product_info)) {
            $data['thumb'] = $product_info['image'];
            $data['thumb_alt'] = $product_info['image_alt'];
        } else {
            $data['thumb'] = '';
        }

        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 500, 500);

        // Images
        if (isset($this->request->post['images'])) {
            $post_images = is_array($this->request->post['images']) ? $this->request->post['images'] : [];
            array_shift($post_images);
            $post_image_alts = (isset($this->request->post['image-alts']) && is_array($this->request->post['image-alts'])) ? $this->request->post['image-alts'] : [];
            array_shift($post_image_alts);
            $product_images = array();
            foreach ($post_images as $k => $post_image) {
                $product_images[] = array(
                    'image' => $post_image,
                    'image_alt' => isset($post_image_alts[$k]) ? $post_image_alts[$k] : '',
                    'sort_order' => $k
                );
            }
        } elseif (isset($this->request->get['product_id'])) {
            $product_images = $this->model_catalog_product->getProductImages($this->request->get['product_id']);
        } else {
            $product_images = array();
        }

        $data['product_images'] = array();
        foreach ($product_images as $product_image) {
            if (is_file(DIR_IMAGE . $product_image['image']) || $this->model_tool_image->imageIsExist($product_image['image'])) {
                $image = $product_image['image'];
                $thumb = $product_image['image'];
            } else {
                $image = '';
                $thumb = 'no_image.png';
            }

            $data['product_images'][] = array(
                'image' => $image,
                'thumb' => $thumb,
                'image_alt' => isset($product_image['image_alt']) ? $product_image['image_alt'] : '',
                'sort_order' => $product_image['sort_order']
            );
        }
        $data['count_product_images'] = count($data['product_images']);
        // Downloads
        $this->load->model('catalog/download');

        if (isset($this->request->post['product_download'])) {
            $product_downloads = $this->request->post['product_download'];
        } elseif (isset($this->request->get['product_id'])) {
            $product_downloads = $this->model_catalog_product->getProductDownloads($this->request->get['product_id']);
        } else {
            $product_downloads = array();
        }

        $data['product_downloads'] = array();

        foreach ($product_downloads as $download_id) {
            $download_info = $this->model_catalog_download->getDownload($download_id);

            if ($download_info) {
                $data['product_downloads'][] = array(
                    'download_id' => $download_info['download_id'],
                    'name' => $download_info['name']
                );
            }
        }

        if (isset($this->request->post['product_related'])) {
            $products = $this->request->post['product_related'];
        } elseif (isset($this->request->get['product_id'])) {
            $products = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);
        } else {
            $products = array();
        }

        $data['product_relateds'] = array();

        foreach ($products as $product_id) {
            $related_info = $this->model_catalog_product->getProduct($product_id);

            if ($related_info) {
                $data['product_relateds'][] = array(
                    'product_id' => $related_info['product_id'],
                    'name' => $related_info['name']
                );
            }
        }

        if (isset($this->request->post['points'])) {
            $data['points'] = $this->request->post['points'];
        } elseif (!empty($product_info)) {
            $data['points'] = $product_info['points'];
        } else {
            $data['points'] = '';
        }

        if (isset($this->request->post['product_reward'])) {
            $data['product_reward'] = $this->request->post['product_reward'];
        } elseif (isset($this->request->get['product_id'])) {
            $data['product_reward'] = $this->model_catalog_product->getProductRewards($this->request->get['product_id']);
        } else {
            $data['product_reward'] = array();
        }

        if (isset($this->request->post['product_seo_url'])) {
            $data['product_seo_url'] = $this->request->post['product_seo_url'];
        } elseif (isset($this->request->get['product_id'])) {
            $data['product_seo_url'] = $this->model_catalog_product->getProductSeoUrls($this->request->get['product_id']);
        } else {
            $data['product_seo_url'] = array();
        }

        if (isset($this->request->post['product_layout'])) {
            $data['product_layout'] = $this->request->post['product_layout'];
        } elseif (isset($this->request->get['product_id'])) {
            $data['product_layout'] = $this->model_catalog_product->getProductLayouts($this->request->get['product_id']);
        } else {
            $data['product_layout'] = array();
        }

        $data['ckeditor_lang'] = $this->config->get('config_language') == 'vi-vn' ? 'vi' : 'en';

        $data['upload_image'] = str_replace('&amp;', '&', $this->url->link('catalog/product/upload', 'user_token=' . $this->session->data['user_token'] . $url, true));
        $data['filemanager'] = str_replace('&amp;', '&', $this->url->link('common/filemanager', 'user_token=' . $this->session->data['user_token'] . '&ckeditor=1', true));

        $this->load->model('localisation/currency');
        $data['currencies'] = $this->model_localisation_currency->getCurrencies();

        $this->load->model('design/layout');
        if ($this->request->server['HTTPS']) {
            $data['base_url'] = HTTPS_SERVER;
        } else {
            $data['base_url'] = HTTP_SERVER;
        }

        $data['product_ingredient'] = [];

        if (!empty($this->request->get['product_id'])) {
            $this->load->model('ingredient/ingredient');
            $product_ingredient = $this->model_ingredient_ingredient->getIngredientOfProductByProductId($this->request->get['product_id']);
            $data['product_ingredient'] = $product_ingredient;
        }

        if ($this->config->get('config_language') == 'vi-vn') {
            $data['placeholder_image'] = 'view/image/theme/upload-placeholder.svg';
        } else {//en-gb
            $data['placeholder_image'] = 'view/image/theme/en-upload-placeholder.svg';
        }
        $data['access_modify'] = $this->user->hasPermission('modify', 'catalog/product');
        $data['video_upload_product'] = defined('BESTME_UPLOAD_PRODUCT_GUIDE') ? BESTME_UPLOAD_PRODUCT_GUIDE : '';
        $data['layouts'] = $this->model_design_layout->getLayouts();
        $data['is_on_mobile'] = $this->isMobile();
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if ($this->isSupportThemeFeature(self::$TAB_DESCRIPTION)) {
            $data['product_description_tab'] = true;
        }
        $data['product_version_image_enabled'] = $this->isSupportThemeFeature(self::$PRODUCT_VERSION_IMAGE);

        $this->response->setOutput($this->load->view('catalog/product_form', $data));
    }

    public function upload()
    {
        return;
        $json = array();
        $get_form = $this->request->get;
        if (isset($_FILES['upload']['name'])) {
            $cloudinary = new Cloudinary_Manager();
            $store_id = SHOP_ID;  /// dictory
            $directory = SHOP_ID;
            if (is_file($_FILES['upload']['tmp_name'])) {
                // Sanitize the filename
                $filename = basename(html_entity_decode($_FILES['upload']['name'], ENT_QUOTES, 'UTF-8'));

                // Validate the filename length
                if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 255)) {
                    $json['error'] = $this->language->get('error_filename');
                }

                // Allowed file extension types
                $allowed = array(
                    'jpg',
                    'jpeg',
                    //'gif',
                    'png',
                    'ico'
                );
                if (!in_array(utf8_strtolower(utf8_substr(strrchr($filename, '.'), 1)), $allowed)) {
                    $json['error'] = $this->language->get('error_filetype');
                }

                // Allowed file mime types
                $allowed = array(
                    'image/jpeg',
                    'image/pjpeg',
                    'image/png',
                    'image/x-png',
                    //'image/gif',
                    'image/x-icon'
                );

                if (!in_array($_FILES['upload']['type'], $allowed)) {
                    $json['error'] = $this->language->get('error_filetype');
                }

                if ($_FILES['upload']['size'] > MAX_IMAGE_SIZE_UPLOAD) {
                    $json['error'] = $this->language->get('error_filesize') . '. Max size of image is ' . ((int)MAX_IMAGE_SIZE_UPLOAD / 1024) . 'KB';
                }

                // Return any upload error
                if ($_FILES['upload']['error'] != UPLOAD_ERR_OK) {
                    $json['error'] = $this->language->get('error_upload_' . $_FILES['upload']['error']);
                }
            } else {
                $json['error'] = $this->language->get('error_upload_default') . ((int)MAX_IMAGE_SIZE_UPLOAD / 1024) . 'KB';
            }

            if (!$json) {
                $fileNameWithoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $_FILES['upload']['name']);
                // trans to slug
                $this->load->model('custom/common');
                $fileNameWithoutExt = $this->model_custom_common->createSlug($fileNameWithoutExt, '-');

                //upload
                $result = $cloudinary->upload($_FILES['upload']['tmp_name'], $fileNameWithoutExt, $directory);
                if (array_key_exists('public_id', $result)) {
                    $function_number = $get_form['CKEditorFuncNum'];
                    echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($function_number, '" . $result['url'] . "', '');</script>";
                }
            }

        }
    }

    protected function validateForm()
    {
        $this->load->model('catalog/product');
        if (!$this->user->hasPermission('modify', 'catalog/product')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        $product_id = isset($this->request->get['product_id']) ? $this->request->get['product_id'] : '';

        /* validate form */
        if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 100)) {
            $this->error['name'][0] = $this->language->get('error_name');
            $this->error['warning'] = $this->language->get('error_name');
        }

        if ($this->model_catalog_product->checkExistingProductByName($this->request->post['name'], $product_id) != false) {
            $this->error['warning'] = $this->language->get('error_name_exist');
        }

        if (isset($this->request->post['sku']) && !empty($this->request->post['sku']) && $this->model_catalog_product->searchProductAndVersionBySKU($this->request->post['sku'], $product_id) == false) {
            $this->error['warning'] = $this->language->get('error_sku_exist');
        }

        if (isset($this->request->post['product_skus'])) {
            $product_skus = $this->request->post['product_skus'];
            foreach ($product_skus as $product_sku) {
                if (!empty($product_sku) && $this->model_catalog_product->searchProductAndVersionBySKU($product_sku, $product_id) == false) {
                    $this->error['warning'] = $this->language->get('error_sku_exist');
                    break;
                }
            }

        }

        /*if (empty($this->request->post['seo_title'])) {
            $this->error['warning'] = $this->language->get('error_empty_seo_title');
        }

        if (empty($this->request->post['seo_desciption'])) {
            $this->error['warning'] = $this->language->get('error_empty_seo_desciption');
        }*/

        if (empty($this->request->post['meta_keyword'])) {
            $this->error['warning'] = $this->language->get('error_empty_meta_keyword');
        }

        if (empty($this->request->post['images'])) {
            $this->error['warning'] = $this->language->get('error_empty_image');
        }

        if (empty($this->request->post['image-alts']) && !empty($this->request->post['images'])) {
            $this->error['warning'] = $this->language->get('error_empty_image_alt');
        }

        if (isset($this->request->post['image-alts']) && !empty($this->request->post['image-alts'])) {
            $product_image_alts = $this->request->post['image-alts'];
            foreach ($product_image_alts as $image_alt) {
                if (empty($image_alt)) {
                    $this->error['warning'] = $this->language->get('error_empty_image_alt');
                    break;
                }
            }
        }

        // check exist keyword sync from brand
        /*$this->load->model('online_store/keyword_sync');
        $checkExistName = $this->model_online_store_keyword_sync->checkExistKeywordWithShop($this->request->post['name']);
        $checkExistDescription = $this->model_online_store_keyword_sync->checkExistKeywordWithShop($this->request->post['description']);
        $checkExistSummary = $this->model_online_store_keyword_sync->checkExistKeywordWithShop($this->request->post['summary']);
        if ($checkExistName) {
            $this->error['warning'] = $this->language->get('error_keyword_check_name');
        }
        if ($checkExistDescription) {
            $this->error['warning'] = $this->language->get('error_keyword_check_description');
        }
        if ($checkExistSummary) {
            $this->error['warning'] = $this->language->get('error_keyword_check_summary');
        }*/

        return !$this->error;
    }

    public function ajaxValidateForm()
    {
        $this->load->language('catalog/product');
        $this->load->model('catalog/product');
        if (!$this->user->hasPermission('modify', 'catalog/product')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        $product_id = isset($this->request->get['product_id']) ? $this->request->get['product_id'] : '';

        /* validate form */
        if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 100)) {
            $this->error['name'][0] = $this->language->get('error_name');
            $this->error['warning'] = $this->language->get('error_name');
        }

        if ($this->model_catalog_product->checkExistingProductByName($this->request->post['name'], $product_id) != false) {
            $this->error['warning'] = $this->language->get('error_name_exist');
        }

        if (isset($this->request->post['sku']) &&
            !empty($this->request->post['sku']) &&
            $this->model_catalog_product->searchProductAndVersionBySKU($this->request->post['sku'], $product_id) == false
        ) {
            $this->error['warning'] = $this->language->get('error_sku_exist');
        }

        if (isset($this->request->post['product_skus'])) {
            $product_skus = $this->request->post['product_skus'];
            foreach ($product_skus as $product_sku) {
                if (!empty($product_sku) && $this->model_catalog_product->searchProductAndVersionBySKU($product_sku, $product_id) == false) {
                    $this->error['warning'] = $this->language->get('error_sku_exist');
                    break;
                }
            }

        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($this->error));
    }

    protected function validateDelete()
    {
        if (!$this->user->hasPermission('modify', 'catalog/product')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    protected function validateCopy()
    {
        if (!$this->user->hasPermission('modify', 'catalog/product')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function autocomplete()
    {
        $json = array();

        if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_model'])) {
            $this->load->model('catalog/product');
            $this->load->model('catalog/option');

            if (isset($this->request->get['filter_name'])) {
                $filter_name = $this->request->get['filter_name'];
            } else {
                $filter_name = '';
            }

            if (isset($this->request->get['filter_model'])) {
                $filter_model = $this->request->get['filter_model'];
            } else {
                $filter_model = '';
            }

            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = 5;
            }

            $filter_data = array(
                'filter_name' => $filter_name,
                'filter_model' => $filter_model,
                'start' => 0,
                'limit' => $limit
            );

            $results = $this->model_catalog_product->getProducts($filter_data);

            foreach ($results as $result) {
                $option_data = array();

                $product_options = $this->model_catalog_product->getProductOptions($result['product_id']);

                foreach ($product_options as $product_option) {
                    $option_info = $this->model_catalog_option->getOption($product_option['option_id']);

                    if ($option_info) {
                        $product_option_value_data = array();

                        foreach ($product_option['product_option_value'] as $product_option_value) {
                            $option_value_info = $this->model_catalog_option->getOptionValue($product_option_value['option_value_id']);

                            if ($option_value_info) {
                                $product_option_value_data[] = array(
                                    'product_option_value_id' => $product_option_value['product_option_value_id'],
                                    'option_value_id' => $product_option_value['option_value_id'],
                                    'name' => $option_value_info['name'],
                                    'price' => (float)$product_option_value['price'] ? $this->currency->format($product_option_value['price'], $this->config->get('config_currency')) : false,
                                    'price_prefix' => $product_option_value['price_prefix']
                                );
                            }
                        }

                        $option_data[] = array(
                            'product_option_id' => $product_option['product_option_id'],
                            'product_option_value' => $product_option_value_data,
                            'option_id' => $product_option['option_id'],
                            'name' => $option_info['name'],
                            'type' => $option_info['type'],
                            'value' => $product_option['value'],
                            'required' => $product_option['required']
                        );
                    }
                }

                $json[] = array(
                    'product_id' => $result['product_id'],
                    'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
                    'model' => $result['model'],
                    'option' => $option_data,
                    'price' => $result['price']
                );
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function view()
    {
        if (!isset($this->request->get['product_id']) || $this->request->get['product_id'] == null) {
            $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true));
        }
        $this->load->language('catalog/product');

        $this->document->setTitle($this->language->get('heading_title_add'));

        $this->load->model('catalog/product');

        $data['text_form'] = !isset($this->request->get['product_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = array();
        }

        if (isset($this->error['meta_title'])) {
            $data['error_meta_title'] = $this->error['meta_title'];
        } else {
            $data['error_meta_title'] = array();
        }

        if (isset($this->error['model'])) {
            $data['error_model'] = $this->error['model'];
        } else {
            $data['error_model'] = '';
        }

        if (isset($this->error['keyword'])) {
            $data['error_keyword'] = $this->error['keyword'];
        } else {
            $data['error_keyword'] = '';
        }

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_model'])) {
            $url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_price'])) {
            $url .= '&filter_price=' . $this->request->get['filter_price'];
        }

        if (isset($this->request->get['filter_quantity'])) {
            $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );
        if (isset($this->request->get['product_id'])) {
            $data['breadcrumbs'][] = array(
                'text' => $data['text_form'],
                'href' => '',
            );
        } else {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_add'),
                'href' => ''
            );
        }
        if (!isset($this->request->get['product_id'])) {
            $data['action'] = $this->url->link('catalog/product/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        } else {
            $data['action'] = $this->url->link('catalog/product/edit', 'user_token=' . $this->session->data['user_token'] . '&product_id=' . $this->request->get['product_id'] . $url, true);
        }

        $data['cancel'] = $this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'] . $url, true);

        if (isset($this->request->get['product_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);
        }

        $data['user_token'] = $this->session->data['user_token'];

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();
        if (isset($this->request->post['product_description'])) {
            $data['product_description'] = $this->request->post['product_description'];
        } elseif (isset($this->request->get['product_id'])) {
            $data['product_description'] = $this->model_catalog_product->getProductDescriptions($this->request->get['product_id']);
        } else {
            $data['product_description'] = array();
        }
        if (isset($this->request->post['model'])) {
            $data['model'] = $this->request->post['model'];
        } elseif (!empty($product_info)) {
            $data['model'] = $product_info['model'];
        } else {
            $data['model'] = '';
        }

        if (isset($this->request->post['sku'])) {
            $data['sku'] = $this->request->post['sku'];
        } elseif (!empty($product_info)) {
            $data['sku'] = $product_info['sku'];
        } else {
            $data['sku'] = '';
        }
        if (isset($this->request->post['barcode'])) {
            $data['barcode'] = $this->request->post['barcode'];
        } elseif (!empty($product_info)) {
            $data['barcode'] = $product_info['barcode'];
        } else {
            $data['barcode'] = '';
        }

        if (isset($this->request->post['upc'])) {
            $data['upc'] = $this->request->post['upc'];
        } elseif (!empty($product_info)) {
            $data['upc'] = $product_info['upc'];
        } else {
            $data['upc'] = '';
        }

        if (isset($this->request->post['location'])) {
            $data['location'] = $this->request->post['location'];
        } elseif (!empty($product_info)) {
            $data['location'] = $product_info['location'];
        } else {
            $data['location'] = '';
        }
        $this->load->model('setting/store');

        $data['stores'] = array();

        $data['stores'][] = array(
            'store_id' => 0,
            'name' => $this->language->get('text_default')
        );

        $stores = $this->model_setting_store->getStores();

        foreach ($stores as $store) {
            $data['stores'][] = array(
                'store_id' => $store['store_id'],
                'name' => $store['name']
            );
        }

        if (isset($this->request->post['product_store'])) {
            $data['product_store'] = $this->request->post['product_store'];
        } elseif (isset($this->request->get['product_id'])) {
            $data['product_store'] = $this->model_catalog_product->getProductStores($this->request->get['product_id']);
        } else {
            $data['product_store'] = array(0);
        }

        if (isset($this->request->post['shipping'])) {
            $data['shipping'] = $this->request->post['shipping'];
        } elseif (!empty($product_info)) {
            $data['shipping'] = $product_info['shipping'];
        } else {
            $data['shipping'] = 1;
        }

        if (isset($this->request->post['promotion_price'])) {
            $data['price'] = $this->request->post['promotion_price'];
        } elseif (!empty($product_info)) {
            $data['price'] = $product_info['price'];
        } else {
            $data['price'] = '';
        }
        if (isset($this->request->post['price'])) {
            $data['compare_price'] = $this->request->post['price'];
        } elseif (!empty($product_info)) {
            $data['compare_price'] = $product_info['compare_price'];
        } else {
            $data['compare_price'] = '';
        }
        //unit price and compare price
        if (isset($this->request->post['price_unit'])) {
            $data['price_unit'] = $this->request->post['price_unit'];
        } elseif (!empty($product_info)) {
            $data['price_unit'] = $product_info['price_currency_id'];
        } else {
            $data['price_unit'] = '';
        }
        if (isset($this->request->post['compare_price_unit'])) {
            $data['compare_price_unit'] = $this->request->post['compare_price_unit'];
        } elseif (!empty($product_info)) {
            $data['compare_price_unit'] = $product_info['c_price_currency_id'];
        } else {
            $data['compare_price_unit'] = '';
        }

        $this->load->model('catalog/recurring');

        $data['recurrings'] = $this->model_catalog_recurring->getRecurrings();

        if (isset($this->request->post['product_recurrings'])) {
            $data['product_recurrings'] = $this->request->post['product_recurrings'];
        } elseif (!empty($product_info)) {
            $data['product_recurrings'] = $this->model_catalog_product->getRecurrings($product_info['product_id']);
        } else {
            $data['product_recurrings'] = array();
        }

        $this->load->model('localisation/tax_class');

        $data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

        if (isset($this->request->post['tax_class_id'])) {
            $data['tax_class_id'] = $this->request->post['tax_class_id'];
        } elseif (!empty($product_info)) {
            $data['tax_class_id'] = $product_info['tax_class_id'];
        } else {
            $data['tax_class_id'] = 0;
        }

        if (isset($this->request->post['date_available'])) {
            $data['date_available'] = $this->request->post['date_available'];
        } elseif (!empty($product_info)) {
            $data['date_available'] = ($product_info['date_available'] != '0000-00-00') ? $product_info['date_available'] : '';
        } else {
            $data['date_available'] = date('Y-m-d');
        }

        if (isset($this->request->post['quantity'])) {
            $data['quantity'] = $this->request->post['quantity'];
        } elseif (!empty($product_info)) {
            $data['quantity'] = $product_info['quantity'];
        } else {
            $data['quantity'] = 1;
        }

        if (isset($this->request->post['product_version'])) {
            $data['multi_versions'] = $this->request->post['product_version'];
        } elseif (!empty($product_info)) {
            $data['multi_versions'] = $product_info['multi_versions'];
        } else {
            $data['multi_versions'] = 0;
        }

        $this->load->model('localisation/stock_status');

        $data['stock_statuses'] = $this->model_localisation_stock_status->getStockStatuses();

        if (isset($this->request->post['stock_status_id'])) {
            $data['stock_status_id'] = $this->request->post['stock_status_id'];
        } elseif (!empty($product_info)) {
            $data['stock_status_id'] = $product_info['stock_status_id'];
        } else {
            $data['stock_status_id'] = 0;
        }

        if (isset($this->request->post['sale_on_out_of_stock'])) {
            $data['sale_on_out_of_stock'] = $this->request->post['sale_on_out_of_stock'];
        } elseif (!empty($product_info)) {
            $data['sale_on_out_of_stock'] = $product_info['sale_on_out_of_stock'];
        } else {
            $data['sale_on_out_of_stock'] = true;
        }

        // seo preview
        $data['server_name'] = $_SERVER['SERVER_NAME'];
        $product_alias = $this->model_catalog_product->getAliasById($product_info['product_id']);
        $data['product_alias'] = array_key_exists('keyword', $product_alias) ? $product_alias['keyword'] : '';

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($product_info)) {
            $data['status'] = $product_info['status'];
        } else {
            $data['status'] = true;
        }
        if (isset($this->request->post['weight'])) {
            $data['weight'] = $this->request->post['weight'];
        } elseif (!empty($product_info)) {
            $data['weight'] = $product_info['weight'];
        } else {
            $data['weight'] = '';
        }
        $this->load->model('localisation/weight_class');

        $data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();
        if (isset($this->request->post['weight_class_id'])) {
            $data['weight_class_id'] = $this->request->post['weight_class_id'];
        } elseif (!empty($product_info)) {
            $data['weight_class_id'] = $product_info['weight_class_id'];
        } else {
            $data['weight_class_id'] = $this->config->get('config_weight_class_id');
        }
        $data['weight_list'] = $this->model_localisation_weight_class->getWeightList();
        if (isset($this->request->post['length'])) {
            $data['length'] = $this->request->post['length'];
        } elseif (!empty($product_info)) {
            $data['length'] = $product_info['length'];
        } else {
            $data['length'] = '';
        }

        if (isset($this->request->post['width'])) {
            $data['width'] = $this->request->post['width'];
        } elseif (!empty($product_info)) {
            $data['width'] = $product_info['width'];
        } else {
            $data['width'] = '';
        }

        if (isset($this->request->post['height'])) {
            $data['height'] = $this->request->post['height'];
        } elseif (!empty($product_info)) {
            $data['height'] = $product_info['height'];
        } else {
            $data['height'] = '';
        }

        $this->load->model('localisation/length_class');

        $data['length_classes'] = $this->model_localisation_length_class->getLengthClasses();

        if (isset($this->request->post['length_class_id'])) {
            $data['length_class_id'] = $this->request->post['length_class_id'];
        } elseif (!empty($product_info)) {
            $data['length_class_id'] = $product_info['length_class_id'];
        } else {
            $data['length_class_id'] = $this->config->get('config_length_class_id');
        }

        $this->load->model('catalog/manufacturer');

        if (isset($this->request->post['manufacturer_id'])) {
            $data['manufacturer_id'] = $this->request->post['manufacturer_id'];
        } elseif (!empty($product_info)) {
            $data['manufacturer_id'] = $product_info['manufacturer_id'];
            $manufacturer_selected = $this->model_catalog_manufacturer->getManufacturer($product_info['manufacturer_id']);
            if ($manufacturer_selected) {
                $data['manufacturer_selected'] = json_encode([0 => array(
                    'id' => $manufacturer_selected['manufacturer_id'],
                    'title' => $manufacturer_selected['name'],
                )]);
            }
        } else {
            $data['manufacturer_id'] = 0;
        }

        if (isset($this->request->post['manufacturer'])) {
            $data['manufacturer'] = $this->request->post['manufacturer'];
        } elseif (!empty($product_info)) {
            $manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($product_info['manufacturer_id']);

            if ($manufacturer_info) {
                $data['manufacturer'] = $manufacturer_info['name'];
            } else {
                $data['manufacturer'] = '';
            }
        } else {
            $data['manufacturer'] = '';
        }

        // Categories
        $this->load->model('catalog/category');

        if (isset($this->request->post['product_category'])) {
            $categories = $this->request->post['product_category'];
        } elseif (isset($this->request->get['product_id'])) {
            $categories = $this->model_catalog_product->getProductCategories($this->request->get['product_id']);
            $data['categories_selected'] = json_encode($this->model_catalog_product->getProductCategories($this->request->get['product_id'], false));
        } else {
            $categories = array();
        }

        $listCategories = $this->model_catalog_category->getCategories();
        foreach ($listCategories as $value) {
            $name = $this->removeChar($value, 'name', '>');
            $data['list_categories'][] = array(
                'category_id' => $value['category_id'],
                //'name' => str_replace(' ', '', $name)
                'name' => $name
            );
        }
        $data['product_categories'] = array();

        foreach ($categories as $category_id) {
            $category_info = $this->model_catalog_category->getCategory($category_id);

            if ($category_info) {
                $data['product_categories'][] = array(
                    'category_id' => $category_info['category_id'],
                    'name' => ($category_info['path']) ? $category_info['path'] . ' &gt; ' . $category_info['name'] : $category_info['name']
                );
                $data['product_categories_list_id'][] = $category_info['category_id'];
            }
        }
        // Filters
        $this->load->model('catalog/filter');
        if (isset($this->request->post['product_filter'])) {
            $filters = $this->request->post['product_filter'];
        } elseif (isset($this->request->get['product_id'])) {
            $filters = $this->model_catalog_product->getProductFilters($this->request->get['product_id']);
        } else {
            $filters = array();
        }
        $listFilter = $this->model_catalog_filter->getFilters();
        foreach ($listFilter as $value) {
            $name = $this->removeChar($value, 'name', '>');
            $data['list_filter'][] = array(
                'filter_id' => $value['filter_id'],
                //'name' => str_replace(' ', '', $name)
                'name' => $name
            );
        }
        $listManufacture = $this->model_catalog_manufacturer->getManufacturers();
        foreach ($listManufacture as $value) {
            $data['list_manufacturer'][] = array(
                'manufacturer_id' => $value['manufacturer_id'],
                'name' => $value['name']
            );
        }

        $data['product_filters'] = array();
        foreach ($filters as $filter_id) {
            $filter_info = $this->model_catalog_filter->getFilter($filter_id);

            if ($filter_info) {
                $data['product_filters'][] = array(
                    'filter_id' => $filter_info['filter_id'],
                    'name' => $filter_info['group'] . ' &gt; ' . $filter_info['name']
                );
                $data['product_filters_list_id'][] = $filter_info['filter_id'];
            }
        }
        // Attributes
        $this->load->model('catalog/attribute');

        if (isset($this->request->post['product_attribute'])) {
            $product_attributes = $this->request->post['product_attribute'];
        } elseif (isset($this->request->get['product_id'])) {
            $product_attributes = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);
        } else {
            $product_attributes = array();
        }

        $data['product_attributes'] = array();

        foreach ($product_attributes as $product_attribute) {
            $attribute_info = $this->model_catalog_attribute->getAttribute($product_attribute['attribute_id']);

            if ($attribute_info) {
                $data['product_attributes'][] = array(
                    'attribute_id' => $product_attribute['attribute_id'],
                    'name' => $attribute_info['name'],
                    'detail' => explode(",", $product_attribute['product_attribute_description']['text'])
                );
            }
        }

        if (isset($this->request->post['product_versions'])) {
            $data['product_versions'] = array();  // after
        } elseif (isset($this->request->get['product_id'])) {
            $data['product_versions'] = $this->model_catalog_product->getProductVersions($this->request->get['product_id']);
        } else {
            $data['product_versions'] = array();
        }
        // Options
        $this->load->model('catalog/option');

        if (isset($this->request->post['product_option'])) {
            $product_options = $this->request->post['product_option'];
        } elseif (isset($this->request->get['product_id'])) {
            $product_options = $this->model_catalog_product->getProductOptions($this->request->get['product_id']);
        } else {
            $product_options = array();
        }

        $data['product_options'] = array();

        foreach ($product_options as $product_option) {
            $product_option_value_data = array();

            if (isset($product_option['product_option_value'])) {
                foreach ($product_option['product_option_value'] as $product_option_value) {
                    $product_option_value_data[] = array(
                        'product_option_value_id' => $product_option_value['product_option_value_id'],
                        'option_value_id' => $product_option_value['option_value_id'],
                        'quantity' => $product_option_value['quantity'],
                        'subtract' => $product_option_value['subtract'],
                        'price' => $product_option_value['price'],
                        'price_prefix' => $product_option_value['price_prefix'],
                        'points' => $product_option_value['points'],
                        'points_prefix' => $product_option_value['points_prefix'],
                        'weight' => $product_option_value['weight'],
                        'weight_prefix' => $product_option_value['weight_prefix']
                    );
                }
            }

            $data['product_options'][] = array(
                'product_option_id' => $product_option['product_option_id'],
                'product_option_value' => $product_option_value_data,
                'option_id' => $product_option['option_id'],
                'name' => $product_option['name'],
                'type' => $product_option['type'],
                'value' => isset($product_option['value']) ? $product_option['value'] : '',
                'required' => $product_option['required']
            );
        }

        $data['option_values'] = array();

        foreach ($data['product_options'] as $product_option) {
            if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
                if (!isset($data['option_values'][$product_option['option_id']])) {
                    $data['option_values'][$product_option['option_id']] = $this->model_catalog_option->getOptionValues($product_option['option_id']);
                }
            }
        }

        $this->load->model('customer/customer_group');

        $data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();

        if (isset($this->request->post['product_discount'])) {
            $product_discounts = $this->request->post['product_discount'];
        } elseif (isset($this->request->get['product_id'])) {
            $product_discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);
        } else {
            $product_discounts = array();
        }

        $data['product_discounts'] = array();

        foreach ($product_discounts as $product_discount) {
            $data['product_discounts'][] = array(
                'customer_group_id' => $product_discount['customer_group_id'],
                'quantity' => $product_discount['quantity'],
                'priority' => $product_discount['priority'],
                'price' => $product_discount['price'],
                'date_start' => ($product_discount['date_start'] != '0000-00-00') ? $product_discount['date_start'] : '',
                'date_end' => ($product_discount['date_end'] != '0000-00-00') ? $product_discount['date_end'] : ''
            );
        }

        if (isset($this->request->post['product_special'])) {
            $product_specials = $this->request->post['product_special'];
        } elseif (isset($this->request->get['product_id'])) {
            $product_specials = $this->model_catalog_product->getProductSpecials($this->request->get['product_id']);
        } else {
            $product_specials = array();
        }

        $data['product_specials'] = array();

        foreach ($product_specials as $product_special) {
            $data['product_specials'][] = array(
                'customer_group_id' => $product_special['customer_group_id'],
                'priority' => $product_special['priority'],
                'price' => $product_special['price'],
                'date_start' => ($product_special['date_start'] != '0000-00-00') ? $product_special['date_start'] : '',
                'date_end' => ($product_special['date_end'] != '0000-00-00') ? $product_special['date_end'] : ''
            );
        }
        // tag collection
        if (isset($this->request->get['product_id'])) {
            $this->load->model('custom/tag');
            $this->load->model('custom/collection');
            $data['tag_list'] = json_encode($this->model_custom_tag->getTagsByProductId($this->request->get['product_id']));
            $data['collection_list'] = json_encode($this->model_custom_collection->getCollectionsByProductId($this->request->get['product_id']));
        }

        // Image
        $this->load->model('tool/image');

        if (isset($this->request->post['images'])) {
            $data['thumb'] = isset($this->request->post['images'][0]) ? $this->request->post['images'][0] : '';
        } elseif (!empty($product_info)) {
            $data['thumb'] = $product_info['image'];
            $data['thumb_alt'] = $product_info['image_alt'];
        } else {
            $data['thumb'] = '';
        }

        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 500, 500);

        // Images
        if (isset($this->request->post['product_image'])) {
            $product_images = $this->request->post['product_image'];
        } elseif (isset($this->request->get['product_id'])) {
            $product_images = $this->model_catalog_product->getProductImages($this->request->get['product_id']);
        } else {
            $product_images = array();
        }

        $data['product_images'] = array();
        foreach ($product_images as $product_image) {
            if (is_file(DIR_IMAGE . $product_image['image']) || $this->model_tool_image->imageIsExist($product_image['image'])) {
                $image = $product_image['image'];
                $thumb = $product_image['image'];
            } else {
                $image = '';
                $thumb = 'no_image.png';
            }

            $data['product_images'][] = array(
                'image' => $image,
                'thumb' => $this->model_tool_image->resize($thumb, 500, 500),
                'image_alt' => isset($product_image['image_alt']) ? $product_image['image_alt'] : '',
                'sort_order' => $product_image['sort_order']
            );
        }
        $data['count_product_images'] = count($data['product_images']);
        // Downloads
        $this->load->model('catalog/download');

        if (isset($this->request->post['product_download'])) {
            $product_downloads = $this->request->post['product_download'];
        } elseif (isset($this->request->get['product_id'])) {
            $product_downloads = $this->model_catalog_product->getProductDownloads($this->request->get['product_id']);
        } else {
            $product_downloads = array();
        }

        $data['product_downloads'] = array();

        foreach ($product_downloads as $download_id) {
            $download_info = $this->model_catalog_download->getDownload($download_id);

            if ($download_info) {
                $data['product_downloads'][] = array(
                    'download_id' => $download_info['download_id'],
                    'name' => $download_info['name']
                );
            }
        }

        if (isset($this->request->post['product_related'])) {
            $products = $this->request->post['product_related'];
        } elseif (isset($this->request->get['product_id'])) {
            $products = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);
        } else {
            $products = array();
        }

        $data['product_relateds'] = array();

        foreach ($products as $product_id) {
            $related_info = $this->model_catalog_product->getProduct($product_id);

            if ($related_info) {
                $data['product_relateds'][] = array(
                    'product_id' => $related_info['product_id'],
                    'name' => $related_info['name']
                );
            }
        }

        if (isset($this->request->post['points'])) {
            $data['points'] = $this->request->post['points'];
        } elseif (!empty($product_info)) {
            $data['points'] = $product_info['points'];
        } else {
            $data['points'] = '';
        }

        if (isset($this->request->post['product_reward'])) {
            $data['product_reward'] = $this->request->post['product_reward'];
        } elseif (isset($this->request->get['product_id'])) {
            $data['product_reward'] = $this->model_catalog_product->getProductRewards($this->request->get['product_id']);
        } else {
            $data['product_reward'] = array();
        }

        if (isset($this->request->post['product_seo_url'])) {
            $data['product_seo_url'] = $this->request->post['product_seo_url'];
        } elseif (isset($this->request->get['product_id'])) {
            $data['product_seo_url'] = $this->model_catalog_product->getProductSeoUrls($this->request->get['product_id']);
        } else {
            $data['product_seo_url'] = array();
        }

        if (isset($this->request->post['product_layout'])) {
            $data['product_layout'] = $this->request->post['product_layout'];
        } elseif (isset($this->request->get['product_id'])) {
            $data['product_layout'] = $this->model_catalog_product->getProductLayouts($this->request->get['product_id']);
        } else {
            $data['product_layout'] = array();
        }
        $this->load->model('localisation/currency');
        $data['currencies'] = $this->model_localisation_currency->getCurrencies();

        $data['controller_view'] = true;

        $this->load->model('design/layout');

        $data['layouts'] = $this->model_design_layout->getLayouts();

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');

        $this->response->setOutput($this->load->view('catalog/product_view', $data));
    }

    public function importFromExcelFile()
    {
        $this->load->language('catalog/product');
        $this->load->model('catalog/product');

        $json = array();
        // Allowed file mime types
        $allowed = array(
            'application/vnd.ms-excel',          // xls
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' // xlsx
        );
        $file = $this->request->files['import-file'];
        $max_file_size = defined('MAX_FILE_SIZE_IMPORT_PRODUCTS') ? MAX_FILE_SIZE_IMPORT_PRODUCTS : 1048576;
        if (!isset($file['size']) || $file['size'] > $max_file_size) {
            $json['error'] = $this->language->get('error_filesize');
        }
        if (!in_array($file['type'], $allowed)) {
            $json['error'] = $this->language->get('error_filetype');
        }

        if (!$json) {
            try {
                // start set read data only
                $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($file['tmp_name']);
                $reader->setReadDataOnly(true);
                // end

                $spreadsheet = $reader->load($file['tmp_name']);
                $firstSheetIndex = $spreadsheet->getFirstSheetIndex();
                $sheetData = $spreadsheet->getSheet($firstSheetIndex)->toArray(null, true, true, true);
            } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
                $json['error'] = $this->language->get('error_unknown');
                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json));
            }
            $max_file_rows = defined('MAX_ROWS_IMPORT_PRODUCTS') ? MAX_ROWS_IMPORT_PRODUCTS : 501;
            if (count($sheetData) > $max_file_rows) {
                $json['error'] = $this->language->get('error_limit');
            } else {
                $header = array_shift($sheetData);
                $header = $this->mapHeaderFromFile($header); /// remove empty element
                if (!$header) {
                    $json['error'] = $this->language->get('error_file_incomplete');
                } else {
                    if (!array_key_exists('name', $header) || !array_key_exists('description', $header)) {
                        $json['error'] = $this->language->get('error_not_found_fields');
                    } else {
                        $dataProduct = $this->mapDataProductFromFile($sheetData, $header);
                        if (array_key_exists('status', $dataProduct) && !$dataProduct['status']) { // check for parent
                            $row = array_key_exists('row', $dataProduct) ? $dataProduct['row'] : 'UNKNOW';
                            $json['error'] = $this->language->get('error_file_incomplete_in_row') . $row;
                        }
                        $dataProduct = $this->mapDataProductVersionFromFile($sheetData, $header, $dataProduct);
                        if (array_key_exists('status', $dataProduct) && !$dataProduct['status']) { /// check for version
                            $row = array_key_exists('row', $dataProduct) ? $dataProduct['row'] : 'UNKNOW';
                            $json['error'] = $this->language->get('error_file_incomplete_in_row') . $row;
                        }
                        if (!$json) {
                            $this->model_catalog_product->processImportExcel($dataProduct);
                        }
                    }
                }
            }
        }

        if (!$json) {
            $json['success'] = $this->language->get('text_uploaded') . ' ' . count($dataProduct) . $this->language->get('text_product_uploaded');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function export()
    {
        $this->load->language('catalog/product');

        $this->response->addheader('Pragma: public');
        $this->response->addheader('Expires: 0');
        $this->response->addheader('Content-Description: File Transfer');
        $this->response->addheader('Content-Type: application/octet-stream');
        $this->response->addheader('Content-Disposition: attachment; filename="' . 'product' . '_' . date('Y-m-d_H-i-s', time()) . '.xls"');
        $this->response->addheader('Content-Transfer-Encoding: binary');

        $this->load->model('catalog/product');

        $product_filter_data = isset($this->request->post['product_part'])
            ? [
                'start' => ($this->request->post['product_part'] - 1 ) * MAX_ROWS_EXPORT_PRODUCTS, // limit sql 0-base
                'limit' => MAX_ROWS_EXPORT_PRODUCTS
            ]
            : [];

        $this->response->setOutput($this->model_catalog_product->export($product_filter_data));
    }

    protected function mapHeaderFromFile(array $header)
    {
        $arrMap = array(
            $this->language->get('export_file_col_link') => 'alias',
            $this->language->get('export_file_col_name') => 'name',
            $this->language->get('export_file_col_description') => 'description',
            $this->language->get('export_file_col_sub_description') => 'sub_description',
            $this->language->get('export_file_col_sku') => 'sku',
            $this->language->get('export_file_col_barcode') => 'barcode',
            $this->language->get('export_file_col_status') => 'status',
            $this->language->get('export_file_col_quantity') => 'quantity',
            $this->language->get('export_file_col_manufacture') => 'manufacturer',
            $this->language->get('export_file_col_type') => 'categories', // i.e product category
            //$this->language->get('export_file_col_category') => 'categories', // google category, temp not used
            //$this->language->get('export_file_col_type') => 'product_filter', // not used?
            $this->language->get('export_file_col_weight') => 'weight',
            $this->language->get('export_file_col_attributes') => 'attributes',
            $this->language->get('export_file_col_price') => 'promotion_price',
            $this->language->get('export_file_col_compare_price') => 'price',
            $this->language->get('export_file_col_cost_price') => 'cost_price',
            $this->language->get('export_file_col_collections') => 'collections',
            $this->language->get('export_file_col_image') => 'thumb',
            $this->language->get('export_file_col_additional_image_urls') => 'images',
            $this->language->get('export_file_col_image_alts') => 'image-alts',
            $this->language->get('export_file_col_original_product') => 'parent_sku',
            $this->language->get('export_file_col_multi_versions') => 'product_version',
            $this->language->get('export_file_col_general_information') => 'product_tabs_general_information',
            $this->language->get('export_file_col_ingredients') => 'product_tabs_ingredients',
            $this->language->get('export_file_col_main_uses') => 'product_tabs_main_uses',
            $this->language->get('export_file_col_user_manual') => 'product_tabs_user_manual',
            $this->language->get('export_file_col_meta_title') => 'seo_title',
            $this->language->get('export_file_col_meta_keyword') => 'meta_keyword',
            //$this->language->get('export_file_col_tags') => 'product_tags',
        );

        $arrMap_2 = array(
            $this->language->get('export_file_col_link') => 'alias',
            $this->language->get('export_file_col_name_2') => 'name',
            $this->language->get('export_file_col_description_2') => 'description',
            $this->language->get('export_file_col_sub_description_2') => 'sub_description',
            $this->language->get('export_file_col_sku_2') => 'sku',
            $this->language->get('export_file_col_barcode_2') => 'barcode',
            $this->language->get('export_file_col_status_2') => 'status',
            $this->language->get('export_file_col_quantity_2') => 'quantity',
            $this->language->get('export_file_col_manufacture_2') => 'manufacturer',
            $this->language->get('export_file_col_type_2') => 'categories', // i.e product category
            //$this->language->get('export_file_col_category_2') => 'categories', // google category, temp not used
            //$this->language->get('export_file_col_type_2') => 'product_filter', // not used?
            $this->language->get('export_file_col_weight_2') => 'weight',
            $this->language->get('export_file_col_attributes_2') => 'attributes',
            $this->language->get('export_file_col_price_2') => 'promotion_price',
            $this->language->get('export_file_col_compare_price_2') => 'price',
            $this->language->get('export_file_col_cost_price_2') => 'cost_price',
            $this->language->get('export_file_col_collections_2') => 'collections',
            $this->language->get('export_file_col_image_2') => 'thumb',
            $this->language->get('export_file_col_additional_image_urls_2') => 'images',
            $this->language->get('export_file_col_image_alts_2') => 'image-alts',
            $this->language->get('export_file_col_original_product_2') => 'parent_sku',
            $this->language->get('export_file_col_multi_versions_2') => 'product_version',
            $this->language->get('export_file_col_general_information') => 'product_tabs_general_information',
            $this->language->get('export_file_col_ingredients') => 'product_tabs_ingredients',
            $this->language->get('export_file_col_main_uses') => 'product_tabs_main_uses',
            $this->language->get('export_file_col_user_manual') => 'product_tabs_user_manual',
            $this->language->get('export_file_col_meta_title') => 'seo_title',
            $this->language->get('export_file_col_meta_keyword') => 'meta_keyword',
            //$this->language->get('export_file_col_tags') => 'product_tags',
        );

        $result = array();
        foreach ($header as $key => $value) {
            $value = trim($value);
            if (array_key_exists($value, $arrMap)) {
                $result[$arrMap[$value]] = $key;
            }
        }

        // for import different language
        if (count($result) < count($arrMap)) {
            foreach ($header as $key => $value) {
                $value = trim($value);
                if (array_key_exists($value, $arrMap_2)) {
                    $result[$arrMap_2[$value]] = $key;
                }
            }
        }
        //remove column sub_description to check :  column sub_description not require
        unset($arrMap[$this->language->get('export_file_col_sub_description')]);
        unset($arrMap_2[$this->language->get('export_file_col_sub_description_2')]);
        unset($arrMap[$this->language->get('export_file_col_cost_price')]);
        unset($arrMap_2[$this->language->get('export_file_col_cost_price_2')]);
        unset($arrMap[$this->language->get('export_file_col_collections')]);
        unset($arrMap_2[$this->language->get('export_file_col_collections_2')]);
        unset($arrMap_2[$this->language->get('export_file_col_collections_2')]);
        // end remove column sub_description to check

        $flag = 1;
        foreach ($arrMap as $k => $v) {
            if (!array_key_exists($v, $result)) {
                $flag = 0;
                break;
            }
        }

        // for import different language
        if (!$flag) {
            foreach ($arrMap_2 as $k => $v) {
                if (!array_key_exists($v, $result)) {
                    return false;
                }
            }
        }

        return $result;
    }

    protected function mapDataProductFromFile(array $data, array $header)
    {
        if (!is_array($data) || empty($data) || !is_array($header) || empty($header) || !array_key_exists('parent_sku', $header)) {
            return [];
        }

        $result = array();
        foreach ($data as $row => $info) {
            if ($this->is_array_empty($info)) {
                continue;
            }
            if (!is_array($info) || $info[$header['name']] == null) {
                if ((trim($info[$header['product_version']]) == '' && trim($info[$header['parent_sku']]) == '') || trim($info[$header['product_version']]) != '') {
                    return [
                        'status' => false,
                        'row' => $row + 2
                    ];
                }
                continue;
            }
            if ($info[$header['parent_sku']] != '') {
                continue;
            }
            $current = $header;
            foreach ($current as $k => $v) {
                switch ($k) {
                    case 'status':
                        $current[$k] = (trim($info[$v]) == $this->language->get('entry_status_publish') || ord(trim($info[$v])) == ord($this->language->get('entry_status_publish'))) ? 1 : 0; // temporary use ord() to compare string2 with different types of encoding : check first char
                        break;
                    case 'attributes':
                        $attributes = preg_split('/\r\n|\r|\n/', $info[$v]);
                        $current[$k] = array();
                        $attr = array();
                        foreach ($attributes as $attribute) {
                            $attribute = explode(':', $attribute);
                            if (!is_array($attribute) || count($attribute) < 2) {
                                continue;
                            }
                            $attr['attribute_id'] = '';
                            $attr['attribute_name'] = array_shift($attribute);

                            $attribute_description = trim(array_shift($attribute));
                            // trim all attribute values
                            $attribute_description = preg_replace('(\s+,)', ',', $attribute_description);
                            $attribute_description = preg_replace('(,\s+)', ',', $attribute_description);
                            $attr['attribute_description'] = $attribute_description;
                            $current[$k][] = $attr;
                        }
                        break;
                    case 'images':
                    case 'image-alts':
                    case 'categories':
                    case 'product_filter':
                        $current_values = preg_split('/\r\n|\r|\n/', $info[$v]);
                        $current_values_trim_space = [];
                        foreach ($current_values as $c_val){
                            if (trim($c_val) != '' || 'image-alts' == $k){
                                $current_values_trim_space[] = trim($c_val);
                            }
                        }
                        $current[$k] = $current_values_trim_space;

                        break;
                    case 'collections':
                        $current_values = preg_split('/\r\n|\r|\n/', $info[$v]);
                        $current_values_trim_space = [];
                        foreach ($current_values as $c_val) {
                            if (trim($c_val) != '' && strlen($c_val) <= 100) {
                                $current_values_trim_space[] = trim($c_val);
                            }
                        }
                        $current[$k] = $current_values_trim_space;

                        break;
                    case 'price':
                    case 'promotion_price':
                        $current[$k] = (float)(str_replace(',', '', $info[$v]));
                        $current['price_unit'] = $current['compare_price_unit'] = 2; // VND
                        break;
                    case 'cost_price':
                        $c_cost_price = (float)(str_replace(',', '', $info[$v]));
                        if (strlen($c_cost_price) > 11) {
                            $current[$k] = 0;
                            break;
                        }
                        //$c_cost_price = (float)(str_replace(',', '', $c_cost_price));
                        if ($c_cost_price < 0) {
                            $current[$k] = 0;
                        } else {
                            $current[$k] = $c_cost_price;
                        }
                        break;
                    case 'weight':
                        $current[$k] = preg_replace("/[^0-9\.]/", '', $info[$v]);
                        $current['weight_unit'] = 1; // kg
                        break;
                    case 'product_version':
                        $current[$k] = trim($info[$v]) == '' ? 0 : 1;
                        $current['multi_versions'] = $info[$v];
                        break;
                    default:
                        $current[$k] = $info[$v];
                }
            }
            $ch = $this->validateRequireFields($current);
            if (!$ch) {
                return [
                    'status' => false,
                    'row' => $row + 2
                ];
            }

            $result[] = $current;
        }

        return $result;
    }

    private function validateRequireFields($data)
    {
        if (!array_key_exists('name', $data) || trim($data['name']) == '') {
            return false;
        }

        if (array_key_exists('multi_versions', $data) && trim($data['multi_versions']) == '' &&
            (!array_key_exists('price', $data) || $data['price'] < 0) || ($data['promotion_price'] > $data['price'] && $data['price'] > 0)) {  // product multi version has price == null is true
            return false;
        }

        if (!array_key_exists('weight', $data) || trim($data['weight']) == '') {
            return false;
        }
        if (count($data['images']) + 1 != count($data['image-alts']) && count($data['images']) < count($data['image-alts'])) {
            return false;
        }

        return true;
    }

    protected function mapDataProductVersionFromFile(array $data, array $header, array $dataProduct)
    {
        if (!is_array($data) || empty($data) || !is_array($header) || empty($header) || !array_key_exists('parent_sku', $header)) {
            return [];
        }

        $result = array();
        foreach ($data as $row => $info) {
            if (!is_array($info)) {
                continue;
            }

            if ($this->is_array_empty($info)) {
                continue;
            }

            $current = $header;
            if ($info[$current['parent_sku']] == '') {
                continue;
            } else {
                $flag = 0;
                foreach ($dataProduct as $k => $product) {
                    if ($info[$header['parent_sku']] == $product['multi_versions']) {
                        $flag = 1;
                        $current['parent_sku'] = $k;
                    }
                }
                if (!$flag) {
                    continue;
                }
            }

            foreach ($current as $k => $v) {
                switch ($k) {
                    case 'status':
                        $current[$k] = trim($info[$v]) == $this->language->get('entry_status_unpublish') ? 0 : 1;
                        break;
                    case 'images':
                    case 'image-alts':
                        $images_alt = trim($info[$v]);
                        // trim all attribute values
                        $images_alt = preg_replace('(\s+,)', ',', $images_alt);
                        $images_alt = preg_replace('(,\s+)', ',', $images_alt);
                        $current[$k] = $images_alt;
                        break;
                    case 'categories':
                    case 'product_filter':
                    case 'collections':
                        /*$current[$k] = preg_split('/\r\n|\r|\n/', $info[$v]);*/
                        $current[$k] = '';
                        break;
                    case 'price':
                    case 'promotion_price':
                        $current[$k] = (float)(str_replace(',', '', $info[$v]));
                        $current['price_unit'] = $current['compare_price_unit'] = 2; // VND
                        break;
                    case 'cost_price':
                        $c_cost_price = (float)(str_replace(',', '', $info[$v]));
                        /*if (strlen($c_cost_price) > 11) {
                            $current[$k] = 0;
                            break;
                        }
                        /* $c_cost_price = (float)(str_replace(',', '', $c_cost_price)); */
                        if ($c_cost_price < 0) {
                            $current[$k] = 0;
                        } else {
                            $current[$k] = $c_cost_price;
                        }
                        break;
                    case 'weight':
                        /*$current[$k] = preg_replace("/[^0-9\.]/", '', $info[$v]);
                        $current['weight_unit'] = 1; // kg*/
                        $current[$k] = '';
                        break;
                    case 'parent_sku':
                        break;
                    case 'attributes':
                        $attribute_description = trim($info[$v]);
                        // trim all attribute values
                        $attribute_description = preg_replace('(\s+,)', ',', $attribute_description);
                        $attribute_description = preg_replace('(,\s+)', ',', $attribute_description);
                        $current[$k] = $attribute_description;
                        break;
                    default:
                        $current[$k] = $info[$v];
                }
            }
            if (!array_key_exists('price', $current) || $current['price'] < 0 || ($current['promotion_price'] > $current['price'] && $current['price'] > 0)) {
                return [
                    'status' => false,
                    'row' => $row + 2
                ];
            }


            $result[] = $current;
        }
        foreach ($result as $item) {
            $dataProduct[$item['parent_sku']]['versions'][] = $item;
        }

        return $dataProduct;
    }

    public function getTagLazyLoad()
    {
        $json = array();
        $this->load->model('custom/tag');
        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        //display 6 product in select config theme is many, or setup other config
        $filter_data = array(
            'limit' => 6,
            'start' => ($page - 1) * 6,
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }
        $results = $this->model_custom_tag->getTagPaginator($filter_data);
        foreach ($results as $key => $value) {
            $json['results'][] = array(
                'text' => $value['value'],
                'id' => $value['tag_id']
            );
        }
        $count_tag = $this->model_custom_tag->countTag($filter_data);
        $json['count'] = $count_tag;
        //array_multisort($sort_order, SORT_ASC, $json);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getIngredientLazyLoad()
    {
        $json = array();
        $this->load->model('ingredient/ingredient');
        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        //display 6 product in select config theme is many, or setup other config
        $filter_data = array(
            'limit' => 6,
            'start' => ($page - 1) * 6,
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }
        $results = $this->model_ingredient_ingredient->getIngredientsCustom($filter_data);
        foreach ($results as $key => $value) {
            $json['results'][] = array(
                'text' => $value['code'],
                'name' => $value['name'],
                'unit' => $value['unit'],
                'id' => $value['ingredient_id']
            );
        }
        $count_tag = $this->model_ingredient_ingredient->getTotalIngredientsCustom([]);
        $json['count'] = $count_tag;
        //array_multisort($sort_order, SORT_ASC, $json);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getCollectionLazyLoad()
    {
        $json = array();
        $this->load->model('custom/collection');
        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        //display 6 product in select config theme is many, or setup other config
        $filter_data = array(
            'limit' => 6,
            'start' => ($page - 1) * 6,
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }
        if (isset($this->request->get['filter_type'])) {
            $filter_data['filter_type'] = trim($this->request->get['filter_type']);
        }
        $results = $this->model_custom_collection->getCollectionPaginator($filter_data);
        foreach ($results as $key => $value) {
            $json['results'][] = array(
                'text' => $value['title'],
                'id' => $value['collection_id']
            );
        }
        $count_tag = $this->model_custom_collection->countCollection($filter_data);
        $json['count'] = $count_tag;
        //array_multisort($sort_order, SORT_ASC, $json);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getCategoryLazyLoad()
    {
        $start = microtime(true);
        //$this->log->write('getCategoryLazyLoad(): start = ' . $start);

        $json = array();

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $this->load->model('catalog/category');

        $filter_data = array(
            'order' => 'ASC',
            'start' => ($page - 1) * 6,
            'limit' => 6
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        $result = $this->model_catalog_category->getLevelCategories($filter_data);

        $count_category = count($result);

        $result = array_slice($result, $filter_data['start'], $filter_data['limit']);
        $json['results'] = $result;

        $end = microtime(true);
        //$this->log->write('getCategoryLazyLoad(): end = ' . $end . '. Excution time = ' . ($end - $start));

        $json['count'] = $count_category;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getCategoryLazyLoadClassify()
    {
        $start = microtime(true);
        //$this->log->write('getCategoryLazyLoad(): start = ' . $start);

        $json = array();

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $this->load->model('catalog/category');

        $filter_data = array(
            'order' => 'ASC',
            'start' => ($page - 1) * 6,
            'limit' => 6
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        if (isset($this->request->get['current_id'])) {
            $filter_data['current_id'] = trim($this->request->get['current_id']);
        }

        $filter_data['categories_selected'] = isset($this->request->get['categories_selected']) ? $this->request->get['categories_selected'] : [];

        $json['results'] = $this->model_catalog_category->getCategoriesPaginatorClassify($filter_data);

        foreach ($json['results'] as &$result) {
            $result['id'] = $result['name'];
            $result['text'] = $result['name'];
        }

        $count_category = $this->model_catalog_category->getTotalCategoriesNoCategoriesCustom($filter_data);

        $end = microtime(true);
        //$this->log->write('getCategoryLazyLoad(): end = ' . $end . '. Excution time = ' . ($end - $start));

        $json['count'] = $count_category;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getManufacturerLazyLoad()
    {
        $json = array();

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $this->load->model('catalog/manufacturer');

        $filter_data = array(
            'order' => 'ASC',
            'start' => ($page - 1) * 6,
            'limit' => 6,
            'filter_status' => 1 // 1 : active
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        $json['results'] = $this->model_catalog_manufacturer->getManufacturersPaginator($filter_data);

        $count_manufacturer = $this->model_catalog_manufacturer->getTotalManufacturersFilter($filter_data);
        $json['count'] = $count_manufacturer;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getStoreLazyLoad()
    {
        $json = array();

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $this->load->model('setting/store');

        $filter_data = array(
            'order' => 'ASC',
            'start' => ($page - 1) * 6,
            'limit' => 6
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        $filter_data['store_ids'] = isset($this->request->get['store_ids']) ? $this->request->get['store_ids'] : [];

        $json['results'] = $this->model_setting_store->getStorePaginator($filter_data);

        $count_store = $this->model_setting_store->getTotalStoreFilter($filter_data);
        $json['count'] = $count_store;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getStoreLazyLoadForUser()
    {
        $json = array();

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $this->load->model('setting/store');

        $filter_data = array(
            'order' => 'ASC',
            'start' => ($page - 1) * 6,
            'limit' => 6
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        $filter_data['store_ids'] = isset($this->request->get['store_ids']) ? $this->request->get['store_ids'] : [];

        $json['results'] = $this->model_setting_store->getStoresForUser($filter_data);

        $count_store = $this->model_setting_store->getTotalStoreForUser($filter_data);
        $json['count'] = $count_store;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function addTags()
    {
        $this->load->model('custom/tag');
        $this->load->language('catalog/product');

        $product_ids = $this->request->post['product-id-select'];
        $tag_list = $this->request->post['tag_list'];
        if (!$tag_list) {
            $this->session->data['error'] = $this->language->get('notice_add_tag_error');
            $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true));
        }
        $product_ids = str_replace(array('[', ']', '"'), array('', '', ''), $product_ids);
        $product_ids = explode(',', $product_ids);

        //add new tags return id
        try {
            $tag_adds = $this->model_custom_tag->insertTag($tag_list);
            foreach ($tag_list as $key => $tag) {
                if (in_array($tag, $tag_adds)) {
                    $tag_list[$key] = (string)array_search($tag, $tag_adds);
                }
            }

            $this->model_custom_tag->insertTagProduct($product_ids, $tag_list);
            $this->session->data['success'] = $this->language->get('notice_add_tag_success');
            $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true));
        } catch (Exception $e) {
            $this->session->data['error'] = $e->getMessage();
            $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true));
        }
    }

    public function addTagFast()
    {
        $this->load->language('catalog/product');
        if (isset($this->request->post['text'])) {
            $this->load->model('custom/tag');
            $result = $this->model_custom_tag->addTagFast($this->request->post['text']);
            if ($result) {
                return json_encode(array(
                    "status" => true,
                    "message" => $this->language->get('add_tag_success'),
                ));
            }
        }
        return json_encode(array(
            "status" => false,
            "message" => $this->language->get('remove_tag_error'),
        ));
    }

    public function removeTags()
    {
        $this->load->model('custom/tag');
        $this->load->language('catalog/product');

        $product_ids = $this->request->post['product-id-select'];
        $tag_list = $this->request->post['tag_list'];
        if (!$tag_list) {
            $this->session->data['error'] = $this->language->get('notice_add_tag_error');
            $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true));
        }
        $product_ids = str_replace(array('[', ']', '"'), array('', '', ''), $product_ids);
        $product_ids = explode(',', $product_ids);
        try {
            $this->model_custom_tag->deleteTagProduct($product_ids, $tag_list);
        } catch (Exception $e) {
            $this->session->data['error'] = $this->language->get('notice_remove_tag_try');
            $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->session->data['success'] = $this->language->get('notice_remove_tag_success');
        $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function addCollections()
    {
        $this->load->model('custom/collection');
        $this->load->language('catalog/product');

        $product_ids = $this->request->post['product-id-select'];
        $collection_list = $this->request->post['collection_list'];
        if (!$collection_list) {
            $this->session->data['error'] = $this->language->get('notice_add_collection_error');
            $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true));
        }
        $product_ids = str_replace(array('[', ']', '"'), array('', '', ''), $product_ids);
        $product_ids = explode(',', $product_ids);

        try {
            $this->model_custom_collection->insertCollectionProduct($product_ids, $collection_list);
        } catch (Exception $e) {
            $this->session->data['error'] = $e->getMessage();
            $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true));
        }
        $this->session->data['success'] = $this->language->get('notice_add_collection_success');
        $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function removeCollections()
    {
        $this->load->model('custom/collection');
        $this->load->language('catalog/product');

        $product_ids = $this->request->post['product-id-select'];
        $collection_list = $this->request->post['collection_list'];
        if (!$collection_list) {
            $this->session->data['error'] = $this->language->get('notice_add_collection_error');
            $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true));
        }
        $product_ids = str_replace(array('[', ']', '"'), array('', '', ''), $product_ids);
        $product_ids = explode(',', $product_ids);
        try {
            $this->model_custom_collection->deleteCollectionProduct($product_ids, $collection_list);
        } catch (Exception $e) {
            $this->session->data['error'] = $e->getMessage();
            $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->session->data['success'] = $this->language->get('notice_remove_collection_success');
        $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function addCategory()
    {
        $this->load->model('catalog/product');
        $this->load->language('catalog/product');
        if (isset($this->request->post['product_to_category'])) {
            $product_id  = $this->request->post['product_to_category'];
            if (trim($product_id) != '') {
                $category_ids = (isset($this->request->post['category']) && is_array($this->request->post['category'])) ? $this->request->post['category'] : [];
                $this->model_catalog_product->addToCategoryQuickly($category_ids, $product_id);
            }
        }
        $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function addProductManufacturer()
    {
        $this->load->model('catalog/product');
        $this->load->language('catalog/product');
        $this->load->model('catalog/manufacturer');
        $data = $this->request->post;
        $data['manufacturer'] = isset($data['manufacturer']) ? $data['manufacturer'] : '';
        if (isset($data['product_to_manufacturer'])) {
            if (strpos($data['manufacturer'], 'add_new_') !== false) {
                $this->load->model('catalog/manufacturer');
                $data['manufacturer'] = $this->model_catalog_manufacturer->addManufacturerFast(str_replace('add_new_', '', $data['manufacturer']));
            }
            $this->model_catalog_manufacturer->updateProductManufacture($data['product_to_manufacturer'], $data['manufacturer']);
        }

        $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function changeInStockProduct()
    {
        $this->load->model('catalog/product');
        $this->load->language('catalog/product');
        $product_ids = $this->request->get['product_ids'];
        //$product_ids = explode(',', $product_ids);
        $result = $this->model_catalog_product->setStock($product_ids, 1);
        if ($result) {
            $this->session->data['success'] = $this->language->get('change_status_success');
            $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true));
        }
        $this->session->data['error'] = $this->language->get('change_status_error');
        $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function changeOutStockProduct()
    {
        $this->load->model('catalog/product');
        $this->load->language('catalog/product');
        $product_ids = $this->request->get['product_ids'];
        //$product_ids = explode(',', $product_ids);
        $result = $this->model_catalog_product->setStock($product_ids, 0);
        if ($result) {
            $this->session->data['success'] = $this->language->get('change_status_success');
            $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true));
        }
        $this->session->data['error'] = $this->language->get('change_status_error');
        $this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function avtivestatus()
    {
        $this->load->model('catalog/product');
        $product_id = (int)$this->request->get['product_id'];
        $status = $this->request->get['status'];
        $status = ($status == 'true' ? '1' : '0');
        $this->model_catalog_product->updateStatus($product_id, $status);
    }

    public function replace_url($url)
    {
        return str_replace("amp;", "", $url);
    }

    public function loadoptionone()
    {
        $this->load->language('catalog/product');
        $this->load->model('catalog/manufacturer');
        $this->load->model('catalog/product');
        $this->load->model('catalog/collection');
        $this->load->model('custom/tag');
        $this->load->model('user/user');
        $categoryID = $this->request->post['categoryID'];
        $start_select_html = '<div class="form-group"><select class="form-control select2 form-control-sm" data-placeholder="' . $this->language->get('filter_status_placeholder') . '" data-label="' . $this->language->get('filter_status') . '" id="status_product" name="status_product">';
        $end_select_html = '</select></div>';
        $show_html = '';

        if ($categoryID > 0) {
            if ($categoryID == 1) { // Trang thai hien thi
                if (!isset($this->request->post['flag'])) {
                    $show_html .= $start_select_html;
                    $show_html .= '<option value="" data-value="' . $this->language->get('product_all_status') . '">' . $this->language->get('product_all_status') . '</option>';
                    $show_html .= '<option value="1" data-value="' . $this->language->get('entry_status_publish') . '">' . $this->language->get('entry_status_publish') . '</option>';
                    $show_html .= '<option value="0" data-value="' . $this->language->get('entry_status_unpublish') . '">' . $this->language->get('entry_status_unpublish') . '</option>';
                } else {
                    $show_html .= '<div class="form-group"><select class="form-control select2 form-control-sm" data-placeholder="' . $this->language->get('filter_status_placeholder') . '" data-label="' . $this->language->get('filter_status') . '" id="status_product" name="status_product">';
                    $show_html .= '<option value="">' . $this->language->get('filter_status') . '</option>';
                    $show_html .= '<option value="1" data-value="' . $this->language->get('entry_status_publish') . '">' . $this->language->get('entry_status_publish') . '</option>';
                    $show_html .= '<option value="0" data-value="' . $this->language->get('entry_status_unpublish') . '">' . $this->language->get('entry_status_unpublish') . '</option>';
                }
                $show_html .= $end_select_html;
            } elseif ($categoryID == 2) { // Nha cung cap
                $start_select_html = '<div class="form-group"><select class="form-control select2 form-control-sm" data-placeholder="' . $this->language->get('filter_manufacture_placeholder') . '" data-label="' . $this->language->get('filter_manufacture') . '" id="manufacturer" name="manufacturer[]">';
                $end_select_html = '</select></div>';
                $getListManufacturer = $this->model_catalog_manufacturer->getManufacturers();
                $show_html .= $start_select_html . '<option value="" data-value="' . $this->language->get('filter_manufacture') . '">' . $this->language->get('filter_manufacture_placeholder') . '</option>';
                foreach ($getListManufacturer as $key => $vl) {
                    $show_html .= '<option value="' . $vl['manufacturer_id'] . '" data-value="' . $vl['name'] . '">' . $vl['name'] . '</option>';
                }
                $show_html .= $end_select_html;
            } elseif ($categoryID == 3) { // Loai san pham
                $start_select_html = '<div class="form-group"><select class="form-control select2 form-control-sm" data-placeholder="' . $this->language->get('filter_category_placeholder') . '" data-label="' . $this->language->get('filter_category') . '" id="category_product" name="category_product[]">';
                $end_select_html = '</select></div>';
                $category_name = $this->model_catalog_product->getAllCategoryName();
                $show_html .= $start_select_html . '<option value="" data-value="' . $this->language->get('filter_category') . '">' . $this->language->get('filter_category_placeholder') . '</option>';
                foreach ($category_name as $key => $vl) {
                    $show_html .= '<option value="' . $vl['id'] . '" data-value="' . $vl['text'] . '" data-level="' . $vl['level'] . '">' . $vl['text'] . '</option>';
                }
                $show_html .= $end_select_html;
            } elseif ($categoryID == 4) { // Duoc tag voi
                $start_select_html = '<div class="form-group"><input type="text" name="nametag" id="nametag" data-label="Tag" placeholder="' . $this->language->get('filter_tag_placeholder') . '" class="form-control" required="">';
                $end_select_html = '</div>';
//                $list_tags = $this->model_custom_tag->getAllTagValueProduct();
//                $show_html .= $start_select_html . '<option value="0" data-value="' . $this->language->get('filter_tag') . '">' . $this->language->get('filter_tag_placeholder') . '</option>';
//                foreach ($list_tags as $tag) {
//                    $show_html .= '<option value="' . $tag['tag_id'] . '" data-value="' . $tag['value'] . '">' . $tag['value'] . '</option>';
//                }
                $show_html .= $start_select_html . $end_select_html;
            } elseif ($categoryID == 5) { // Bo suu tap
                $start_select_html = '<div class="form-group hierarchical-child"><select class=" form-control select2 form-control-sm"  data-placeholder="' . $this->language->get('filter_collection_placeholder') . '" data-label="' . $this->language->get('filter_collection') . '" id="collection" name="collection[]">';
                $end_select_html = '</select></div>';

                $category_coll = $this->model_catalog_collection->getAllcollection_filter();
                $show_html .= $start_select_html . '<option value="" data-value="' . $this->language->get('filter_collection') . '">' . $this->language->get('filter_collection_placeholder') . '</option>';
                foreach ($category_coll as $key => $vl) {
                    $show_html .= '<option value="' . $vl['collection_id'] . '" data-value="' . $vl['title'] . '">' . $vl['title'] . '</option>';
                }
                $show_html .= $end_select_html;
            } elseif ($categoryID == 6) { // So luong
                $start_select_html = '<div class="form-group hierarchical-child" id="hierarchical-product-quantity"><select class="form-control form-control-sm select2 amountoption" data-placeholder="' . $this->language->get('filter_length_product_placeholder') . '" data-label="' . $this->language->get('filter_length_product') . '" id="amounttype" name="amounttype">';
                $end_select_html = '</select></div>
                    <script>jQuery(document).ready(function($){$(".amountoption").change(function(){if($(this).val()==0){$("#textamount").hide()}else{$("#textamount").show()}})});</script>';
                $show_html .= $start_select_html . '<option value="">' . $this->language->get('filter_length_product_placeholder') . '</option>';
                $show_html .= '<option value="1" data-value="' . $this->language->get('filter_length_product_greater') . '">' . $this->language->get('filter_length_product_greater') . '</option>';
                $show_html .= '<option value="2" data-value="' . $this->language->get('filter_length_product_smaller') . '">' . $this->language->get('filter_length_product_smaller') . '</option>';
                $show_html .= '<option value="3" data-value="' . $this->language->get('filter_length_product_equal') . '">' . $this->language->get('filter_length_product_equal') . '</option>';
                $show_html .= '<input type="number" name="textamount" id="textamount" class="form-control mt-3" placeholder="' . $this->language->get('filter_length_product_input_placeholder') . '" data-label="" required style="display:none;">';
                $show_html .= $end_select_html;
            } elseif ($categoryID == 7) { // Nhân viên
                $start_select_html = '<div class="form-group hierarchical-child"><select class="form-control select2 form-control-sm" data-placeholder="' . $this->language->get('filter_staff_placeholder') . '" data-label="' . $this->language->get('filter_staff') . '" id="staff" name="staff[]">';
                $end_select_html = '</select></div>';
                $getListStaff = $this->model_user_user->getUsers();
                $show_html .= $start_select_html . '<option value="" data-value="' . $this->language->get('filter_staff') . '">' . $this->language->get('filter_staff_placeholder') . '</option>';
                foreach ($getListStaff as $key => $vl) {
                    $show_html .= '<option value="' . $vl['user_id'] . '" data-value="' . $vl['lastname'] . ' ' . $vl['firstname'] . '">' . $vl['lastname'] . ' ' . $vl['firstname'] . '</option>';
                }
                $show_html .= $end_select_html;
            }
        } else {
            $show_html .= '<style>#optionone,#optiontwo{display:none !important}</style>';
        }

        echo $show_html .= '';
    }

    public function loadoptiontwo()
    {
        $this->load->language('catalog/product');
        $this->load->model('catalog/manufacturer');
        $this->load->model('catalog/product');
        echo $type = $this->request->post['type'];
        //$show_html .= '<input type="text" name="" id="textamount" placeholder="Nhập số lượng" style="display:none;" class="form-control">';
    }

    public function ShowHideProduct($active = '')
    {
        $this->load->language('catalog/product');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('catalog/product');
        $this->response->addHeader('Content-Type: application/json');
        $json = [
            'status' => false,
            'message' => 'thất bại',
        ];
        if (isset($this->request->post['selected'])) {
            $active = $this->request->get['active'];
            foreach ($this->request->post['selected'] as $product_id) {
                $this->model_catalog_product->updateStatus($product_id, $active);
            }
            $json = [
                'status' => true,
                'message' => 'thanh cong'
            ];
            $this->response->setOutput(json_encode($json));
            return;
        }
        $this->response->setOutput(json_encode($json));
    }

    function is_array_empty($arr)
    {
        if (is_array($arr)) {
            foreach ($arr as $key => $value) {
                if (!empty($value) || $value != NULL || $value != "") {
                    return false;
                    break;//stop the process we have seen that at least 1 of the array has value so its not empty
                }
            }
            return true;
        }
    }

    function replaceVersionValues($str) {
        // unescape version value due to version may contains special char that was escaped
        // e.g BLACK & WHITE => BLACK &apm; WHITE => BLACK__apm__WHITE instead of our expect is BLACK___WHITE
        $str = html_entity_decode($str);

        return preg_replace('/[^0-9a-zA-Z\-_]+/', '_', $str) . '_' . md5($str);
    }
}

