<?php

class ControllerCatalogStore extends Controller
{

    public function index()
    {
        $this->load->language('catalog/store');
        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/store');
        $this->load->model('user/user');

        /* breadcrumb */
        $data['breadcrumbs'] = [];

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('tab_product'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => ''
        );

        $data['add_store_link'] = $this->url->link('catalog/store/addStore', 'user_token=' . $this->session->data['user_token'], true);
        $data['edit_store_link'] = $this->url->link('catalog/store/editStore', 'user_token=' . $this->session->data['user_token'], true);
        $data['delete_store_link'] = $this->url->link('catalog/store/deleteStore', 'user_token=' . $this->session->data['user_token'], true);

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $filter_name = isset($this->request->get['filter_name']) ? $this->request->get['filter_name'] : null;

        $filter_data = array(
            'filter_name' => trim($filter_name),
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin'),
            'filter_data' => isset($this->request->get['filter_data']) ? $this->request->get['filter_data'] : false,
        );

        $filter_data['store_list'] = 1;

        $stores = $this->model_setting_store->getStoresForUser($filter_data);
        $total_store = $this->model_setting_store->getTotalStoreForUser($filter_data);
        foreach ($stores as &$st) {
            if (!isset($st['id'])) {
                continue;
            }

            $st['product_quantity'] = $this->model_setting_store->getTotalQuantityProductOfStoreByStoreId($st['id']);
            $st['user_store'] = $this->model_setting_store->getTotalStaffsOfStoreByStoreId($st['id']);
            $st['store_receipt'] = $this->model_setting_store->getTotalStoreReceiptGoodsDeliveringOfStoreByStoreId($st['id']);
            $st['store_transfer_receipt'] = $this->model_setting_store->getTotalStoreTransReceiptDraftAndTransferredOfStoreByStoreId($st['id']);
        }
        unset($st);

        $data['stores'] = $stores;
        $data['stores_check'] = json_encode($stores, JSON_HEX_APOS);
        $current_user_id = $this->user->getId();
        $type = $this->model_user_user->getTypeUser($current_user_id);
        $data['type_user'] = $type;

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];
            unset($this->session->data['error']);
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $total_store;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');;
        $pagination->url = $this->url->link('catalog/store', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

        // Add goto page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);
        $data['results'] = sprintf($this->language->get('text_pagination'), ($total_store) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($total_store - $this->config->get('config_limit_admin'))) ? $total_store : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $total_store, ceil($total_store / $this->config->get('config_limit_admin')));
        //var_dump($data);die;

        /* common */
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('catalog/store_append', $data));
        } else {
            $this->response->setOutput($this->load->view('catalog/store', $data));
        }
    }


    public function addStore()
    {
        $this->load->language('settings/classify');
        $this->load->model('setting/store');

        $validate = true;

        if (!isset($this->request->post['name']) || $this->request->post['name'] == '') {
            $this->session->data['error'] = $this->language->get('error_name_store_empty');
            $validate = false;
        }

        if ($validate) {
            $data = array(
                'config_name' => $this->request->post['name'],
                'config_url' => '',
                'config_ssl' => ''
            );
            $this->model_setting_store->addStore($data);
            $this->session->data['success'] = $this->language->get('add_store_success');
        }

        $this->response->redirect($this->url->link('catalog/store', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function editStore()
    {
        $this->load->language('settings/classify');
        $this->load->model('setting/store');

        $validate = true;

        if (!isset($this->request->post['name']) || $this->request->post['name'] == '') {
            $this->session->data['error'] = $this->language->get('error_name_store_empty');
            $validate = false;
        }

        if ($validate) {
            $data = array(
                'config_name' => $this->request->post['name'],
                'config_url' => '',
                'config_ssl' => ''
            );
            $this->model_setting_store->editStore($this->request->post['id'], $data);
            $this->session->data['success'] = $this->language->get('edit_store_success');
        }

        $this->response->redirect($this->url->link('catalog/store', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function deleteStore()
    {
        $this->load->language('catalog/store');
        $this->load->model('setting/store');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateFormDelete()) {
            try {
                $this->model_setting_store->deleteStore($this->request->post['id']);
                $this->session->data['success'] = $this->language->get('delete_store_success');
            } catch (Exception $e) {
                $this->session->data['error'] = $this->language->get('delete_store_error');
            }
        }

        $this->response->redirect($this->url->link('catalog/store', 'user_token=' . $this->session->data['user_token'], true));
    }

    public function validateMaxStore()
    {
        $this->load->language('catalog/store');
        $this->response->addHeader('Content-Type: application/json');
        $json = [
            'status' => true,
            'message' => '',
        ];
        $config_packet_unlimited_stores = $this->config->get('config_packet_unlimited_stores');
        if (isset($config_packet_unlimited_stores) && $config_packet_unlimited_stores != '1') {
            $config_packet_number_of_stores = $this->config->get('config_packet_number_of_stores');
            if (isset($config_packet_number_of_stores)) {
                $this->load->model('setting/store');
                $count_stores = count($this->model_setting_store->getStores());
                if ($count_stores >= (int)$config_packet_number_of_stores) {
                    $json = [
                        'status' => false,
                        'message' => $this->language->get('error_limit_store'),
                    ];
                } else {
                    $json = [
                        'status' => true,
                        'message' => '',
                        'ratio_size' => (int)($count_stores / $config_packet_number_of_stores * 100)
                    ];
                }
            }
        }

        $this->response->setOutput(json_encode($json));
    }

    private function validateFormDelete()
    {
        $this->load->model('setting/store');

        // check permission
        if (!$this->user->hasPermission('modify', 'catalog/store')) {
            $this->session->data['error'] = $this->language->get('error_permission');

            return false;
        }

        // check param
        if (!isset($this->request->post['id']) || $this->request->post['id'] == '') {
            $this->session->data['error'] = $this->language->get('text_no_result');

            return false;
        }

        $store_id = $this->request->post['id'];

        // check product
        if ($this->model_setting_store->getTotalQuantityProductOfStoreByStoreId($store_id)) {
            $this->session->data['error'] = $this->language->get('error_not_delete');

            return false;
        }

        // check staff_store
        if ($this->model_setting_store->getTotalStaffsOfStoreByStoreId($store_id)) {
            $this->session->data['error'] = $this->language->get('error_not_delete');

            return false;
        }

        // check store_receipt
        if ($this->model_setting_store->getTotalStoreReceiptGoodsDeliveringOfStoreByStoreId($store_id)) {
            $this->session->data['error'] = $this->language->get('error_not_delete');

            return false;
        }

        // check store_transfer_receipt
        if ($this->model_setting_store->getTotalStoreTransReceiptDraftAndTransferredOfStoreByStoreId($store_id)) {
            $this->session->data['error'] = $this->language->get('error_not_delete');

            return false;
        }

        return true;
    }
}