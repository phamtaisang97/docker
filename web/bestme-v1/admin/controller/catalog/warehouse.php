<?php

class ControllerCatalogWarehouse extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('catalog/warehouse');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/warehouse');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_warehouse->editWarehouse($this->request->get['warehouse_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getList();
    }

    public function editQuantity()
    {
        $this->load->language('catalog/warehouse');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/warehouse');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm('editQuantity')) {
            if (isset($this->request->post['warehouse_ids']) && isset($this->request->post['quantity']) && isset($this->request->post['quantity_method'])) {
                // update multiple
                foreach (explode(',', $this->request->post['warehouse_ids']) as $warehouse_id) {
                    $pv_id = explode('_', $warehouse_id);
                    $product_id = $pv_id[0];
                    $product_version_id = isset($pv_id[1]) ? (int)$pv_id[1] : 0;
                    $this->model_catalog_warehouse->editProductDefaultStoreQuantity($product_id, $product_version_id, $this->request->post['quantity'], $this->request->post['quantity_method']);
                }

                $this->model_catalog_warehouse->deleteProductCaches();
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getList();
    }

    public function editSaleOnOutOfStock()
    {
        $this->load->language('catalog/warehouse');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/warehouse');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            if (isset($this->request->post['warehouse_id'])) {
                // update one

                // support for basic mode
                $pv_id = explode('_', $this->request->post['warehouse_id']);
                $product_id = $pv_id[0];
                $this->model_catalog_warehouse->editWarehouseSaleOnOutOfStock($product_id, $this->request->post);
            } else if (isset($this->request->post['warehouse_ids'])){
                // update multiple
                $warehouse_ids = $this->request->post['warehouse_ids'];

                // support for basic mode
                $warehouse_ids = explode(',', $warehouse_ids);
                $product_ids = array_map(function ($warehouse_id) {
                    $pv_id = explode('_', $warehouse_id);
                    $product_id = $pv_id[0];
                    return $product_id;
                }, $warehouse_ids);
                $product_ids = implode(',', $product_ids);

                $this->model_catalog_warehouse->editWarehousesSaleOnOutOfStock($product_ids, $this->request->post);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getList();
    }

    protected function getList()
    {
        /* current page */
        $page = $this->getValueFromRequest('get', 'page', 1);

        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        /* breadcrumbs */
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        /* message */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        /* filter */
        $filter_status = '';
        if (isset($this->request->get['filter_status'])) {
            $filter_status = ($this->request->get['filter_status']);
            if (count($filter_status) > 1) {
                $filter_status = '';
            } else {
                $filter_status = (int)$filter_status[0];
            }
        }

        $filter_manufacturer = '';
        if (isset($this->request->get['filter_manufacturer'])) {
            if (count($this->request->get['filter_manufacturer']) > 0) {
                $filter_manufacturer = implode(',', ($this->request->get['filter_manufacturer']));
            } else {
                $filter_manufacturer = (int)$this->request->get['filter_manufacturer'][0];
            }
        }

        $filter_category = '';
        if (isset($this->request->get['filter_category'])) {
            if (count($this->request->get['filter_category']) > 0) {
                $filter_category = implode(',', ($this->request->get['filter_category']));
            } else {
                $filter_category = (int)$this->request->get['filter_category'][0];
            }
        }

        $filter_tag = '';
        if (isset($this->request->get['tags'])) {
            $filter_tag = $this->request->get['tags'];
            if (!empty($filter_tag)) {
                if(count($filter_tag) > 0){
                    $filter_tag = implode('|',($filter_tag));
                }
                else{
                    $filter_tag = (int)$filter_tag[0];
                }
            }
            else{$filter_tag = '';}
        }

        $filter_collection = '';
        if (isset($this->request->get['filter_collection'])) {
            if (count($this->request->get['filter_collection']) > 0) {
                $filter_collection = implode(',', ($this->request->get['filter_collection']));
            } else {
                $filter_collection = (int)$this->request->get['filter_collection'][0];
            }
        }

        $filter_amount = null;
        if (isset($this->request->get['amounttype'])) {
            $filter_amount = $this->request->get['amounttype'];
        }

        $filter_store = 'default';
        $data['default_store'] = 'default';
        $data['default_store_name'] = $this->language->get('txt_default_store');

        if (isset($this->request->get['filter_store'])) {
            $filter_store = $this->request->get['filter_store'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $filter_name = $this->getValueFromRequest('get', 'filter_name');
        $fil_data = $this->getValueFromRequest('get', 'filter_data');

        $data['loadOptionOne'] = $this->url->link('catalog/product/loadoptionone', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $filter_data = array(
            'filter_status' => $filter_status,
            'filter_data' => $fil_data,
            'filter_manufacturer' => $filter_manufacturer,
            'filter_category' => $filter_category,
            'filter_tag' => $filter_tag,
            'filter_collection' => $filter_collection,
            'filter_name' => $filter_name,
            'filter_amount' => $filter_amount,
            'filter_store' => $filter_store,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $this->load->model('user/user');
        $user_id = $this->user->getId();

        if (!$this->user->canAccessAll()) {
            $user_store_ids = $this->model_user_user->getStoreIdsOffUser($user_id);
            $filter_data['user_store_id'] = implode(",", $user_store_ids);
            $filter_data['current_user_id'] = (int)$user_id;
        }

        $data['filter_data'] = $filter_data;

        /* selected fields in filter */
        $data['filter_category'] = $filter_category;
        $data['filter_status'] = $filter_status;
        /*
         * get product_warehouse, format:
         *
         * [
         *   [
         *      'warehouse_id' => 1,
         *      'product_id' => 1,
         *      'image' => 'img/product_1.jpg',
         *      'name' => 'Bút chì STAEDTLER Đen - 12x36',
         *      'sku' => 'IBK-A860',
         *      'price' => '2.500.000 ₫',
         *      'price_version' => '2.500.000 ₫',
         *      'sale_on_out_of_stock' => 1,
         *      'quantity' => '70',
         *      ...
         *   ],
         *   ...
         * ]
         */
        $total = 0;
        try {
            $data['product_warehouses'] = $this->model_catalog_warehouse->getWarehouses($filter_data);
            $this->load->model('tool/image');
            foreach ($data['product_warehouses'] as $key => $product) {
                if (!$product['image']) {
                    $data['product_warehouses'][$key]['image'] = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
                }

                // for basic mode
                if (array_key_exists('price_version', $product)) {
                    $data['product_warehouses'][$key]['price_version'] = number_format($product['price_version'], 0, '', ',');
                }

                $pv_id = $product['product_id'];
                if ($product['product_version_id']){
                    $pv_id = $pv_id . '_' . $product['product_version_id'];
                }
                $data['product_warehouses'][$key]['warehouse_id'] = $pv_id;

                $qty = $this->model_catalog_warehouse->getQtyGoodsAreComing($product, $filter_store);
                $qty = is_null($qty) ? 0 : $qty;
                $data['product_warehouses'][$key]['arrival'] = $qty;

                $info = $this->model_catalog_warehouse->getInfoFromProductToStore($product, $filter_store);
                $quantity = 0;
                $min_cost_price = 0;
                $max_cost_price = 0;
                if (!empty($info)) {
                    $min_cost_price = $max_cost_price = (int)$info[0]['cost_price'];
                    foreach ($info as $item) {
                        $quantity += (int)$item['quantity'];

                        $cost_price = (int)$item['cost_price'];
                        if ($min_cost_price > $cost_price) {
                            $min_cost_price = $cost_price;
                        }
                        if ($max_cost_price < $cost_price) {
                            $max_cost_price = $cost_price;
                        }
                    }
                }
                $data['product_warehouses'][$key]['quantity'] = $quantity;
                $data['product_warehouses'][$key]['cost_price'] = $min_cost_price == $max_cost_price ? number_format($min_cost_price, 0, '', ',') . '&#8363;' : number_format($min_cost_price, 0, '', ',') . '&#8363;' . ' ~ ' . number_format($max_cost_price, 0, '', ',') . '&#8363;';
            }
        } catch (Exception $e) {
            $data['product_warehouses'] = [];
        }

        $data['total_value'] = $this->model_catalog_warehouse->getTotalValueStores($filter_store);
        $data['total_quantity'] = $this->model_catalog_warehouse->getTotalQuantityStores($filter_store);

        /* list categories */
        $this->load->model('catalog/category');
        $listCategories = $this->model_catalog_category->getCategories();
        foreach ($listCategories as $value) {
            $name = $this->removeChar($value, 'name', '>');
            $data['list_categories'][] = array(
                'category_id' => $value['category_id'],
                'name' => str_replace(' ', '', $name)
            );
        }

        $data['user_token'] = $this->session->data['user_token'];
        $data['delete'] = $this->url->link('catalog/warehouse/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $warehouse_total = $this->model_catalog_warehouse->getTotalWarehouses($filter_data);

        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $warehouse_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        // Add jump to page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);

        $data['results'] = sprintf($this->language->get('text_pagination'), ($warehouse_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($warehouse_total - $this->config->get('config_limit_admin'))) ? $warehouse_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $warehouse_total, ceil($warehouse_total / $this->config->get('config_limit_admin')));
        if (isset($this->request->get['amounttype'])) {

            $data['amounttype'] = $this->request->get['amounttype'];
        }

        $data['inventory_management_mode'] = $this->config->get('config_inventory_mode') ? $this->config->get('config_inventory_mode') : 0;
        $data['url_filter'] = $this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'] . $url, true);
        /* href */
        $data['href_list'] = $this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['href_edit'] = $this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['href_edit_quantity'] = $this->url->link('catalog/warehouse/editQuantity', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['href_edit_sale_on_out_of_stock'] = $this->url->link('catalog/warehouse/editSaleOnOutOfStock', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['href_product_detail'] = $this->url->link('catalog/product/edit', 'user_token=' . $this->session->data['user_token'] . '&product_id=', true);

        $data['access_modify'] = $this->user->hasPermission('modify', 'catalog/warehouse');

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if ($data['inventory_management_mode'] == 0){
            if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
                $this->response->setOutput($this->load->view('catalog/warehouse_list_append', $data));
            } else {
                $this->response->setOutput($this->load->view('catalog/warehouse_list', $data));
            }
        } else{
            if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
                $this->response->setOutput($this->load->view('catalog/warehouse_list_basic_append', $data));
            } else {
                $this->response->setOutput($this->load->view('catalog/warehouse_list_basic', $data));
            }
        }
    }

    public function getStoreLazyLoad()
    {
        $json = array();

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $this->load->model('setting/store');
        $this->load->language('catalog/warehouse');

        $filter_data = array(
            'order' => 'ASC',
            'start' => ($page - 1) * 6,
            'limit' => 6
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        $json['results'] = $this->model_setting_store->getStoresForUser($filter_data);

        if ($page == 1) {
            array_unshift($json['results'], [
                'id' => 'default',
                'text' => $this->language->get('txt_default_store')
            ]);

            array_unshift($json['results'], [
                'id' => 'all',
                'text' => $this->language->get('txt_all_store')
            ]);
        }

        $count_store = $this->model_setting_store->getTotalStoreForUser($filter_data);
        $json['count'] = $count_store;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * AJAX getTotalValueStores
     *
     * $_GET param: filter_store
     */
    public function getTotalValuesForStore()
    {
        $this->load->model('catalog/warehouse');

        $filter_store = isset($this->request->get['filter_store']) ? $this->request->get['filter_store'] : 'default';
        $total_value = $this->model_catalog_warehouse->getTotalValueStores($filter_store);
        $json['total_value'] = (float)$total_value;

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * AJAX getTotalQuantityForStore
     *
     * $_GET param: filter_store
     */
    public function getTotalQuantityForStore()
    {
        $this->load->model('catalog/warehouse');

        $filter_store = isset($this->request->get['filter_store']) ? $this->request->get['filter_store'] : 'default';
        $total_quantity = $this->model_catalog_warehouse->getTotalQuantityStores($filter_store);
        $json['total_quantity'] = (int)$total_quantity;

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function switchInventoryMode()
    {
        $json['msg'] = 'Fail!';
        if (isset($this->request->post['current_mode'])){
            $this->load->model('setting/setting');
            $current_mode = (int)$this->request->post['current_mode'];
            $new_mode = 0;
            if ($current_mode == 0){
                $new_mode = 1;
            }
            $this->model_setting_setting->editSettingValue('config', 'config_inventory_mode', $new_mode);
            $json['msg'] = 'Success!';
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    protected function getForm()
    {
        $data['text_form'] = !isset($this->request->get['warehouse_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

        /* message */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = array();
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        if (isset($this->request->get['warehouse_id'])) {
            $data['breadcrumbs'][] = array(
                'text' => $data['text_form'],
                'href' => '',
            );
        } else {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_add'),
                'href' => ''
            );
        }

        if (!isset($this->request->get['warehouse_id'])) {
            $data['action'] = $this->url->link('catalog/warehouse/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        } else {
            $data['action'] = $this->url->link('catalog/warehouse/edit', 'user_token=' . $this->session->data['user_token'] . '&warehouse_id=' . $this->request->get['warehouse_id'] . $url, true);
        }

        $data['cancel'] = $this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'] . $url, true);

        if (isset($this->request->get['warehouse_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $warehouse_info = $this->model_catalog_warehouse->getProduct($this->request->get['warehouse_id']);
        }

        $data['user_token'] = $this->session->data['user_token'];

        $this->load->model('localisation/language');

        if (isset($this->request->post['location'])) {
            $data['location'] = $this->request->post['location'];
        } elseif (!empty($warehouse_info)) {
            $data['location'] = $warehouse_info['location'];
        } else {
            $data['location'] = '';
        }

        $this->load->model('localisation/currency');
        $data['currencies'] = $this->model_localisation_currency->getCurrencies();

        $this->load->model('design/layout');

        $data['layouts'] = $this->model_design_layout->getLayouts();

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');

        $this->response->setOutput($this->load->view('catalog/warehouse_form', $data));
    }

    protected function validateForm($action = 'edit')
    {
        if (!$this->user->hasPermission('modify', 'catalog/warehouse')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        if ($action === 'editQuantity') {
            /* notice: remove "(int)($this->request->post['quantity']) < 0": allow quantity negative in warehouse */
            if (!isset($this->request->post['quantity']) || (int)($this->request->post['quantity']) > (10e11 - 1)) {
                $this->error['quantity'] = $this->language->get('error_quantity');
                $this->error['warning'] = $this->language->get('error_quantity');
            }
            if (isset($this->request->post['quantity']) && $this->request->post['quantity'] == "") {
                $this->error['warning'] = $this->language->get('error_quantity');
            }
        }

        return !$this->error;
    }
}
