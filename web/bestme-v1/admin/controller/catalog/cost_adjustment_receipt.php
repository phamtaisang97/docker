<?php


Class ControllerCatalogCostAdjustmentReceipt extends Controller
{
    use Device_Util;
    private $error = array();

    public function index()
    {
        $this->load->language('catalog/cost_adjustment_receipt');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/cost_adjustment_receipt');

        $this->getList();
    }

    public function getList()
    {
        $this->load->model('setting/store');
        $filter_status = $this->getValueFromRequest('get', 'filter_status');
        $filter_name = $this->getValueFromRequest('get', 'filter_name');
        $filter_store = $this->getValueFromRequest('get', 'filter_store');
        $page = $this->getValueFromRequest('get', 'page', 1);
        $filter_data = $this->getValueFromRequest('get', 'filter_data');


        $url = '';
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data = array();

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => ''
        );
        $url = '';
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['add'] = $this->url->link('catalog/cost_adjustment_receipt/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['access_modify'] = $this->user->hasPermission('modify', 'catalog/cost_adjustment_receipt');

        if (!empty($filter_data)) {
            if (!empty($filter_status)) {
                if (count($filter_status) > 0) {
                    $filter_status = implode(',', ($filter_status));
                } else {
                    $filter_status = (int)$filter_status[0];
                }
            } else {
                $filter_status = '';
            }
            if (!empty($filter_store)) {
                if (count($filter_store) > 0) {
                    $filter_store = implode(',', ($filter_store));
                } else {
                    $filter_store = (int)$filter_store[0];
                }
            } else {
                $filter_store = '';
            }
        }

        $data['loadOptionOne'] = $this->url->link('catalog/cost_adjustment_receipt/loadoptionone', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $filter_data = array(
            'filter_data' => $filter_data,
            'filter_name' => $filter_name,
            'filter_status' => $filter_status,
            'filter_store' => $filter_store,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        if (isset($this->request->get['start_date']) && !empty($this->request->get['start_date'])) {
            $filter_data['start_date'] = $this->request->get['start_date'];
            $filter_data['end_date'] = $this->request->get['end_date'];
        }

        $this->load->model('user/user');
        $user_id = $this->user->getId();

        if (!$this->user->canAccessAll()) {
            $user_store_ids = $this->model_user_user->getStoreIdsOffUser($user_id);
            $filter_data['user_store_id'] = implode(",", $user_store_ids);
            $filter_data['current_user_id'] = (int)$user_id;
        }

        $cost_adjustment_receipt_total = $this->model_catalog_cost_adjustment_receipt->getTotalCostAdjustmentReceipt($filter_data);

        $results = $this->model_catalog_cost_adjustment_receipt->getCostAdjustmentReceiptCustom($filter_data);

        $data['cost_adjustment_receipts'] = array();
        foreach ($results as $result) {
            $description_status = $this->convertStatusReceipt($result['status']);

            $data['cost_adjustment_receipts'][] = array(
                'cost_adjustment_receipt_id' => $result['cost_adjustment_receipt_id'],
                'code' => $result['cost_adjustment_receipt_code'],
                'store_name' => $this->model_setting_store->getStoreNameById($result['store_id']),
                'status' => $result['status'],
                'description_status' => $description_status,
                'date_added' => $result['date_added'],
                'date_modified' => $result['date_modified'],
                'edit' => $this->url->link('catalog/cost_adjustment_receipt/edit', 'user_token=' . $this->session->data['user_token'] . '&cost_adjustment_receipt_id=' . $result['cost_adjustment_receipt_id'], $url, true)
            );
        }

        $data['user_token'] = $this->session->data['user_token'];
        // paginate
        $url = '';

        $pagination = new CustomPaginate();
        $pagination->total = $cost_adjustment_receipt_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/store_receipt', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        // Add jump to page
        $option['text_go_to_page'] = sprintf($this->language->get('text_go_to_page'));
        $data['pagination'] = $pagination->render($option);
        $data['results'] = sprintf($this->language->get('text_pagination'), ($cost_adjustment_receipt_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($cost_adjustment_receipt_total - $this->config->get('config_limit_admin'))) ? $cost_adjustment_receipt_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $cost_adjustment_receipt_total, ceil($cost_adjustment_receipt_total / $this->config->get('config_limit_admin')));

        if (isset($this->session->data['success']) && $this->session->data['success'] != 'text_success') {
            $data['success'] = $this->session->data['success'];

            $this->session->data['success'] = '';
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->session->data['cost_adjustment_receipts'])) {
            $data['cost_adjustment_receipts_action'] = json_encode($this->session->data['cost_adjustment_receipts']);
        }

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];

            $this->session->data['error'] = '';
            unset($this->session->data['error']);
        } else {
            $data['error_warning'] = '';
        }

        // for auto building selected filter in view
        if (isset($this->request->get['filter_status'])) {
            $data['filter_cost_adjustment_receipt_status'] = $this->request->get['filter_status'];
        }
        $data['url_filter'] = $this->url->link('catalog/cost_adjustment_receipt', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $data['is_on_mobile'] = $this->isMobile();
        if (isset($this->request->get['filter_data']) && $this->request->get['filter_data']) {
            $this->response->setOutput($this->load->view('catalog/cost_adjustment_receipt_append', $data));
        } else {
            $this->response->setOutput($this->load->view('catalog/cost_adjustment_receipt', $data));
        }
    }

    public function convertStatusReceipt($staus)
    {
        $description_status = '';
        $this->load->language('catalog/cost_adjustment_receipt');
        switch ($staus) {
            case ModelCatalogCostAdjustmentReceipt::RECEIPT_DRAPT :
                $description_status = $this->language->get('cost_adjustment_receipt_status_draft');
                break;
            case ModelCatalogCostAdjustmentReceipt::RECEIPT_APPLY :
                $description_status = $this->language->get('cost_adjustment_receipt_status_apply');
                break;
            case ModelCatalogCostAdjustmentReceipt::RECEIPT_CANCEL :
                $description_status = $this->language->get('cost_adjustment_receipt_status_canceled');
                break;
        }
        return $description_status;
    }

    public function loadoptionone()
    {
        $this->load->language('catalog/cost_adjustment_receipt');
        $this->load->model('setting/store');
        $categoryID = $this->request->post['categoryID'];
        $end_select_html = '</select></div>';
        $show_html = '';

        if ($categoryID > 0) {
            if ($categoryID == 1) { // store
                $start_select_html = '<div class="form-group hierarchical-child" id="hierarchical-store-take">
                                        <select class="form-control form-control-sm select2" 
                                        data-placeholder="' . $this->language->get('filter_cost_adjustment_placeholder') . '" 
                                        data-label="' . $this->language->get('filter_cost_adjustment_placeholder') . '" 
                                        id="filter_cost_adjustment" 
                                        name="filter_cost_adjustment">';
                $end_select_html = '</select></div>';
                $getListStore = $this->model_setting_store->getStoresForUser([]);
                $show_html .= $start_select_html . '<option value="" data-value="' . $this->language->get('filter_cost_adjustment') . '">' . $this->language->get('filter_cost_adjustment_placeholder') . '</option>';
                foreach ($getListStore as $key => $vl) {
                    $show_html .= '<option value="' . $vl['id'] . '" data-value="' . $vl['text'] . '">' . $vl['text'] . '</option>';
                }
                $show_html .= $end_select_html;
            } elseif ($categoryID == 2) { // Status
                $show_html = '<div class="form-group hierarchical-child" id="hierarchical-status">
                                            <select class="form-control form-control-sm select2"
                                             data-placeholder="' . $this->language->get('filter_status_placeholder') . '" 
                                             data-label="' . $this->language->get('filter_status') . '" 
                                             id="filter_status" 
                                             name="filter_status">';
                $show_html .= '<option value="">' . $this->language->get('filter_status_placeholder') . '</option>';
                $show_html .= '<option value="0" data-value="' . $this->language->get('filter_status_draft') . '">' . $this->language->get('filter_status_draft') . '</option>';
                $show_html .= '<option value="1" data-value="' . $this->language->get('filter_status_apply') . '">' . $this->language->get('filter_status_apply') . '</option>';
                $show_html .= '<option value="9" data-value="' . $this->language->get('filter_status_canceled') . '">' . $this->language->get('filter_status_canceled') . '</option>';
                $show_html .= $end_select_html;
            } elseif ($categoryID == 3){ // added_date
                $show_html .= '<div class="form-group hierarchical-child" id="hierarchical-date">
                                            <select class="form-control form-control-sm select2"
                                             data-placeholder="' . $this->language->get('filter_created_at_placeholder') . '" 
                                             data-label="' . $this->language->get('filter_date_range') . '" 
                                             id="filter_created_at_type" 
                                             name="filter_created_at_type">';
                $show_html .= '<option value="">' . $this->language->get('filter_created_at_placeholder') . '</option>';
                $show_html .= '<option value="today" data-value="' . $this->language->get('filter_created_at_today') . '">' . $this->language->get('filter_created_at_today') . '</option>';
                $show_html .= '<option value="week" data-value="' . $this->language->get('filter_created_at_this_week') . '">' . $this->language->get('filter_created_at_this_week') . '</option>';
                $show_html .= '<option value="month" data-value="' . $this->language->get('filter_created_at_this_month') . '">' . $this->language->get('filter_created_at_this_month') . '</option>';
                $show_html .= '<option value="option" data-value="' . $this->language->get('filter_created_at_option') . '">' . $this->language->get('filter_created_at_option') . '</option>';
                $show_html .= '</select>';
                $show_html .= '<div class="input-group-icon mt-3 w-100 d-none filter_created_at_value">
                            <i class="bg-secondary icon icon-calendar ml-n3" style=" top: -50px;"></i>
                            <input type="text" name="filter_created_at_from"
                                   class="form-control date-picker date-picker-store-receipt-filter mt-3"
                                   placeholder="' . $this->language->get('filter_select_date_from') . '" data-label="' . $this->language->get('filter_txt_date') . '"
                                   data-value="">
                            <i class="bg-secondary icon icon-calendar ml-n3" style=" top: 50px;"></i>
                            <input type="text" name="filter_created_at_to"
                                   class="form-control date-picker date-picker-store-receipt-filter mt-3"
                                   placeholder="' . $this->language->get('filter_select_date_to') . '" data-label="' . $this->language->get('filter_txt_date') . '"
                                   data-value="">
                          </div>';
            }
        } else {
            $show_html .= '<style>#optionone,#optiontwo{display:none !important}</style>';
        }

        echo $show_html .= '';
    }

    public function add()
    {
        $this->load->language('catalog/cost_adjustment_receipt');
        $this->document->setTitle($this->language->get('heading_title_add'));
        $this->load->model('catalog/cost_adjustment_receipt');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_cost_adjustment_receipt->addReceipt($this->request->post);
            if (isset($this->request->post['type_submit']) && $this->request->post['type_submit'] == 'draft') {
                $this->session->data['success'] = $this->language->get('text_success_draft');
            } else {
                $this->session->data['success'] = $this->language->get('text_success_add');
            }
            $this->response->redirect($this->url->link('catalog/cost_adjustment_receipt', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();
    }

    public function edit()
    {
        $this->load->language('catalog/cost_adjustment_receipt');
        $this->document->setTitle($this->language->get('heading_title_edit'));
        $this->load->model('catalog/cost_adjustment_receipt');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_cost_adjustment_receipt->editReceipt($this->request->get['cost_adjustment_receipt_id'], $this->request->post);

            if (isset($this->request->post['type_submit']) && $this->request->post['type_submit'] == 'draft') {
                $this->session->data['success'] = $this->language->get('text_success_draft');
            } else if(isset($this->request->post['type_submit']) && $this->request->post['type_submit'] == 'cancel'){
                $this->session->data['success'] = $this->language->get('text_success_cancel');
            } else {
                $this->session->data['success'] = $this->language->get('text_success_edit');
            }

            $this->response->redirect($this->url->link('catalog/cost_adjustment_receipt', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();
    }

    private function getForm()
    {
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success']) && $this->session->data['success'] != 'text_success') {
            $data['success'] = $this->session->data['success'];

            $this->session->data['success'] = '';
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data['text_form'] = !isset($this->request->get['cost_adjustment_receipt_id']) ? $this->language->get('heading_title_add') : $this->language->get('heading_title_edit');
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/cost_adjustment_receipt', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $data['text_form'],
            'href' => '',
        );

        $current_user_id = $this->user->getId();

        if (!isset($this->request->get['cost_adjustment_receipt_id'])) {
            $data['action'] = $this->url->link('catalog/cost_adjustment_receipt/add', 'user_token=' . $this->session->data['user_token'], true);
            $data['user_create_id'] = isset($current_user_id) ? $current_user_id : 1;
        } else {
            $data['action'] = $this->url->link('catalog/cost_adjustment_receipt/edit', 'user_token=' . $this->session->data['user_token'] . '&cost_adjustment_receipt_id=' . $this->request->get['cost_adjustment_receipt_id'], true);

            $receipt_info = $this->model_catalog_cost_adjustment_receipt->getReceiptById($this->request->get['cost_adjustment_receipt_id']);

            if (!$receipt_info){
                $this->response->redirect($this->url->link('catalog/store_take_receipt', 'user_token=' . $this->session->data['user_token'], true));
            }
            $this->load->model('setting/store');
            $this->load->model('catalog/product');
            $receipt_store_selected[] =[
                'id' => $receipt_info['store_id'],
                'title' => $this->model_setting_store->getStoreNameById($receipt_info['store_id'])
            ];
            $data['receipt_store_selected'] = json_encode($receipt_store_selected);
            $data['receipt_status'] = $receipt_info['status'];
            $data['receipt_note'] = $receipt_info['note'];

            if(isset($receipt_info['user_create_id'])){
                $data['user_create_id'] = $receipt_info['user_create_id'];
            } else {
                $data['user_create_id'] = isset($current_user_id) ? $current_user_id : 1;
            }

            $receipt_products = $this->model_catalog_cost_adjustment_receipt->getReceiptProductById($this->request->get['cost_adjustment_receipt_id']);
            foreach ($receipt_products as &$product){
                $pvid = $product['product_id'];
                $product_name = $this->model_catalog_product->getProductNameById($product['product_id']);
                $product['sku'] = $this->model_catalog_product->getProductSKUById($product['product_id']);

                if ($product['product_version_id']){
                    $pvid .= '-'.$product['product_version_id'];
                    $product_version_info = $this->model_catalog_product->getProductVersionByProductIdAndVersionId($product['product_id'], $product['product_version_id']);
                    if (isset($product_version_info['sku']) && $product_version_info['sku']){
                        $product['sku'] = $product_version_info['sku'];
                    }
                    if (isset($product_version_info['version']) && $product_version_info['version']){
                        $product_name .= '(' . $this->language->get('text_version') . $product_version_info['version'] . ')';
                    }
                    if (!is_array($product_version_info) || !array_key_exists('deleted', $product_version_info)){
                        $product['deleted'] = 1;
                    }else{
                        $product['deleted'] = $product_version_info['deleted'];
                    }
                }else{
                    if ($this->model_catalog_product->checkProductSingleVersionExist($product['product_id']) > 0) {
                        $product['deleted'] = 0;
                    } else {
                        $product['deleted'] = 1;
                    }
                }
                $product['name'] = $product_name;
                $product['pvid'] = $pvid;
                $product['adjustment_cost_price'] = (float)$product['adjustment_cost_price'];
                $product['current_cost_price'] = (float)$product['current_cost_price'];
                $product_in_store = $this->model_catalog_cost_adjustment_receipt->getProductInStore($receipt_info['store_id'], $product['product_id'], $product['product_version_id']);
                if (empty($product_in_store)){
                    $product['deleted'] = 1; // Không có hàng này trong kho
                }
                if (isset($receipt_info['status']) && $receipt_info['status'] == ModelCatalogCostAdjustmentReceipt::RECEIPT_DRAPT) {
                    $product['current_cost_price'] =  isset($product_in_store['cost_price']) ? (float)$product_in_store['cost_price'] : (float)$product['current_cost_price'];
                }
            }
            unset($product);
            $data['receipt_products'] = $receipt_products;
        }

        $data['access_modify'] = $this->user->hasPermission('modify', 'catalog/cost_adjustment_receipt');
        $data['cancel'] = $this->url->link('catalog/cost_adjustment_receipt', 'user_token=' . $this->session->data['user_token'], true);
        $data['custom_header'] = $this->load->controller('common/custom_header');
        $data['custom_column_left'] = $this->load->controller('common/custom_column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $data['user_token'] = $this->session->data['user_token'];

        $this->response->setOutput($this->load->view('catalog/cost_adjustment_receipt_form', $data));
    }

    public function getStoreLazyLoad()
    {
        $json = array();

        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $this->load->model('setting/store');

        $filter_data = array(
            'order' => 'ASC',
            'start' => ($page - 1) * 6,
            'limit' => 6
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        $json['results'] = $this->model_setting_store->getStoresForUser($filter_data);

        $count_store = $this->model_setting_store->getTotalStoreForUser($filter_data);
        $json['count'] = $count_store;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getProductsLazyLoad()
    {
        $json = array();
        $count_tag = 0;

        $this->load->language('catalog/store_receipt');
        $this->load->model('catalog/cost_adjustment_receipt');
        $this->load->model('tool/image');

        $store_id = isset($this->request->get['store']) ? $this->request->get['store'] : '';
        if ($store_id == '') {
            return;
        }
        $page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        // display 6 product in select config theme is many, or setup other config
        $filter_data = array(
            'limit' => 6,
            'start' => ($page - 1) * 6,
        );
        if (isset($this->request->get['filter_name'])) {
            $filter_data['filter_name'] = trim($this->request->get['filter_name']);
        }

        $user_id = $this->user->getId();
        if (!$this->user->canAccessAll()) {
            $filter_data['current_user_id'] = (int)$user_id;
        }

        if (!$this->user->hasPermission('modify', 'catalog/product')) {
            $json['count'] = $count_tag;
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));

            return;
        }

        $results = $this->model_catalog_cost_adjustment_receipt->getProductByStore($store_id, $filter_data);
        foreach ($results as $key => $value) {
            $version_name = implode(' • ', explode(',', $value['version']));
            if ($version_name != '') {
                $version_name = $this->language->get('text_version') . $version_name;
            }

            if ($value['image'] != '') {
                $image = $this->model_tool_image->resize($value['image'], 100, 100);
            } else {
                $image = $this->model_tool_image->resize('no_image.png', 100, 100);
            }

            $json['results'][] = array(
                'id' => $value['id'],
                'product_id' => $value['product_id'],
                'product_version_id' => $value['product_version_id'],
                'version' => $version_name,
                'subname' => $version_name,
                'text' => $value['name'],
                'product_name' => $value['product_name'],
                'image' => $image,
                'weight' => number_format($value['weight'], 0, '', ''),
                'price' => number_format($value['price'], 0, '', ','),
                'quantity' => $value['quantity'],
                'sale_on_out_of_stock' => $value['sale_on_out_of_stock'],
                'status' => $value['status'],
                'sku' => $value['sku'],
                'cost_price' => (float)$value['cost_price']
            );
        }

        $count_tag = $this->model_catalog_cost_adjustment_receipt->countProductByStore($store_id, $filter_data);
        $json['count'] = $count_tag;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function validateForm()
    {
        $this->load->language('catalog/cost_adjustment_receipt');
        if (!$this->user->hasPermission('modify', 'catalog/cost_adjustment_receipt')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!isset($this->request->post['store_id']) || !isset($this->request->post['product_version_id']) || !isset($this->request->post['product_cost_price'])) {
            $this->error['warning'] = $this->language->get('error_data');
        }

        return !$this->error;
    }
}