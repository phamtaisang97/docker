<?php
// HTTP
define('HTTP_SERVER', 'http://bestme.test/admin/');
define('HTTP_CATALOG', 'http://bestme.test/');

// HTTPS
define('HTTPS_SERVER', 'http://bestme.test/admin/');
define('HTTPS_CATALOG', 'http://bestme.test/');

define('URL_SCHEME_HTTPS', 'https://');

// DIR
define('DIR_APPLICATION', '/var/www/html/bestme-v1/admin/');
define('DIR_SYSTEM', '/var/www/html/bestme-v1/system/');
define('DIR_IMAGE', '/var/www/html/bestme-v1/image/');
define('DIR_ASSET', '/var/www/html/bestme-v1/asset/');
define('HTTP_ASSET', '/asset/');
define('DIR_STORAGE', '/var/www/html/storage/vendor/');
define('DIR_CATALOG', '/var/www/html/bestme-v1/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'db');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'mypass');
define('DB_DATABASE', 'bestme_v1');
define('DB_PORT', '3306');
define('DB_PREFIX', 'cfr_');

// Dashboard. TODO: add config on installation...
define('DASHBOARD_CHART_NODE', 7);  // Number of point on Dashboard chart. Default 7 for this week!

// Config Image Server for uploading images
define('IMAGE_SERVER_UPLOAD', 'bestme'); // bestme, cloudinary

// Cloudinary for uploading images
define('TEMPORY_IMAGE_PREFIX', 'tempory_');
define('MAX_IMAGES_UPLOAD', 1000); // 1000 images
define('MAX_IMAGE_SIZE_UPLOAD', 2097152); // 2048 kb
define('CLOUD_NAME', 'novaon-x2');
define('API_KEY', 'YOUR_API_KEY');
define('API_SECRET', 'YOUR_API_SECRET');

// bestme image server
define('BESTME_IMAGE_SEVER_UPLOAD_URL', 'http://127.0.0.1:8989');
define('BESTME_IMAGE_SEVER_SERVE_URL', 'http://cdn.bestme.test');
define('BESTME_IMAGE_SEVER_API_KEY', 'ccd50a36-8594-11ed-97bb-056d19430246');
// bestme image redis config for crawling image from Cloudinary to Bestme image server
define('BESTME_IMAGE_JOB_REDIS_QUEUE', 'PULL_IMAGES_FROM_CLOUDINARY');
define('MUL_REDIS_DB_BESTME_IMAGE', 2);
define('BESTME_IMAGE_JOB_MAX_PROCESS', 1);
define('BESTME_IMAGE_CJ_POLLING_INTERVAL', 600); // in seconds

// import products .xls config
define('MAX_ROWS_IMPORT_PRODUCTS', 501); // 500 + Header
define('MAX_ROWS_EXPORT_PRODUCTS', 500); // 500 + Without Header
define('MAX_FILE_SIZE_IMPORT_PRODUCTS', 2097152); // bytes ~ 2097152 = 2Mb

// SHOP_ID
define('SHOP_ID', 'x2.bestme.test');

//hot line
define('CONFIG_HOT_LINE', '094.442.7799');

// Bestme homepage
define('BESTME_SERVER', 'https://www.bestme.asia/');
define('BESTME_SERVER_PRICING', 'https://www.bestme.asia/bang-gia');
define('BESTME_SERVER_POS', 'https://www.bestme.asia/bestme-pos');
define('BESTME_IP', '128.12.2.16');

// Shop name suffix
define('SHOP_NAME_SUFFIX', '.bestme.asia');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');

// Redis to communicate with landingpage
define('MUL_REDIS_HOST', '127.0.0.1');
define('MUL_REDIS_PORT', 6379);
define('MUL_REDIS_DB', 1); // for channel 'UpdateDomain'
define('MUL_REDIS_DOMAIN_PUBLISH_CHANEL', 'UpdateDomain');
//// for Shopee redis job queue
define('MUL_REDIS_DB_SHOPEE', 2);
// for customer staff
define('MUL_REDIS_CUSTOMER_STAFF_PUBLISH_CHANEL', 'UpdateCustomerStaff');

// sync_bestme_website_data_to_welcome
define('MUL_REDIS_SYNC_BESTME_WEBSITE_DATA_PUBLISH_CHANEL', 'SyncBestmeWebsiteData');

// AutoAds Google Shopping homepage
define('AUTOADS_GG_SHOPPING_SERVER', 'https://autoads.asia/vi/cong-cu/googleshopping/');
define('AUTOADS_GG_SHOPPING_GUIDE', 'https://support.bestme.asia/1736-hdsd-google-shopping/');

// AutoAds Google Shopping homepage
define('AUTOADS_MAXLEAD_SERVER', 'https://autoads.asia/cong-cu/maxlead');

// Google Analytics homepage
define('GG_ANALYTICS_SERVER', 'https://support.google.com/analytics/?hl=vi#topic=3544906');

// Facebook Pixel homepage
define('FB_PIXEL_SERVER', 'https://www.facebook.com/business/help/651294705016616');

// File Manager tool for Theme development tool
define('THEME_TOOL_FILE_MANAGER_URL', 'https://bestme.vn/theme-tool/file-manager');

define('THEME_TOOL_VALIDATE_CONFIG', serialize(
        array(
            'v1.0' => array(
                'required' => array(
                    'asset',
                    'asset/sass',
                    'asset/sass/override',
                    'asset/sass/override/color.css',
                    'asset/sass/override/text.css'
                )
            )
        ))
);

//config date trial
define('PACKET_TRIAL_PERIOD_DEFAULT', 15);

// Novaon Chatbot homepage and guide
define('CHATBOT_NOVAON_HOMEPAGE', 'https://chatbot.novaonx.com/');
define('CHATBOT_NOVAON_GUIDE', 'https://support.bestme.asia/1594-dang-ky-kich-hoat/');

// Giao hang nhanh
define('TRANSPORT_GHN_GUIDE', 'https://docs.google.com/document/d/1g8Pfedb5BPVnkUKpLa_ZwUyDKvlpnymdywtsNDqtKCc/edit?usp=sharing');
define('TRANSPORT_GHN_BESTME_TOKEN', 'b577b882-c001-11ea-b354-e6945d70dd56'); // staging: b577b882-c001-11ea-b354-e6945d70dd56, production: xxx
define('TRANSPORT_GHN_BESTME_PHONE', '0977200443'); // staging: 0977200443, production: 0961680486

// Viettel Post
define('TRANSPORT_VIETTELPOST_GUIDE', 'https://docs.google.com/document/d/1uOduwm25O7ut9iCiF3ZPyueliGuMnO5CksKD19iY5Yo/edit?usp=sharing');

// Config Partner Viettel_Post
define('VIETTEL_POST_TOKEN', 'eyJhbGciOiJFUzI1NiJ9.eyJzdWIiOiIwMzM4OTkyMjI1IiwiVXNlcklkIjo2OTAxNjkyLCJGcm9tU291cmNlIjo1LCJUb2tlbiI6IlMxMzFNNTdVQ0pBUFMxQjhHSzRGIiwiZXhwIjoxNjg3OTI3NzQ0LCJQYXJ0bmVyIjo2OTAxNjkyfQ.VSh0V6288jmz6hOvz8AjAdtl79Ize4cDhz3_wSH8mWrdWjZauQVqrlnE598f84BIPb-zzA7CjICYTExEv2IXpQ');
define('VIETTEL_POST_USERNAME', 'dunght@novaon.asia');
define('VIETTEL_POST_PASSWORD', 'xxx');

// Shopee partner
define('SHOPEE_PARTNER_ID', 843411); // sandbox: 840568, real: 843411
define('SHOPEE_PARTNER_KEY', '455ac8f7232734f8f41924904a1be8dab2d46d1909147b2e45433577ee29c773'); // sandbox 424967cfa6038dbaab13f5fecb18094dd162f1e19d31b77f91b129be52780bde, real: 455ac8f7232734f8f41924904a1be8dab2d46d1909147b2e45433577ee29c773
define('SHOPEE_API_URL_PREFIX', 'https://partner.shopeemobile.com/'); // sandbox: https://partner.uat.shopeemobile.com/, real: https://partner.shopeemobile.com/

// Shopee job redis queue
define('SHOPEE_JOB_REDIS_QUEUE', 'SHOPEE_JOB');
define('SHOPEE_ORDER_JOB_REDIS_QUEUE', 'SHOPEE_ORDER_JOB');
define('SHOPEE_JOB_MAX_PROCESS', 5);
define('SHOPEE_MAX_JOB_IN_A_PROCESS', 100);

// Shopee job redis process info
define('SHOPEE_PROCESS_INFO_PREFIX', 'SHOPEE_PROCESS_INFO_');
define('SHOPEE_PROCESS_INFO_TIMEOUT', 7200); // in seconds. default: 7200s = 2h

// Shopee test option
define('SHOPEE_SYNC_INTERVAL_TEST_VALUE', ''); // minutes, e.g: 5 for 5 min. Let empty or absent for no test

// Shopee upload
define('SHOPEE_UPLOAD_JOB_REDIS_QUEUE', 'SHOPEE_UPLOAD_JOB');

// One Shopee id for getting categories, logistics for caching...
define('SHOPEE_SHOP_ID', 311278093);

// Sample product upload homepage
define('SAMPLE_PRODUCT_LIST_UPLOAD', 'https://support.bestme.asia/wp-content/uploads/2020/10/file-mau-upload-san-pham-1.xlsx');
define('SAMPLE_PRODUCT_LIST_UPLOAD_EN', 'https://support.bestme.asia/wp-content/uploads/2020/10/product_upload_sample-1.xlsx');

// Sample customer upload homepage
define('SAMPLE_CUSTOMER_LIST_UPLOAD', 'https://docs.google.com/spreadsheets/d/13waKnlW0OSdrKHNHr0u8vbCJI4QQA4n0RhC0IzJbIfY/edit');
define('SAMPLE_CUSTOMER_LIST_UPLOAD_EN', 'https://docs.google.com/spreadsheets/d/13waKnlW0OSdrKHNHr0u8vbCJI4QQA4n0RhC0IzJbIfY/edit');
define('MAX_FILE_SIZE_IMPORT_CUSTOMER', 2097152); // bytes ~ 2097152 = 2Mb

// Bestme POS
define('BESTME_POS_URL_TEMPLATE', 'https://%s.bestme.asia/pos');
define('BESTME_POS_SECURE_KEY', 'KPS0LU0KyLKvCdnB4QPiuah7Yg2QDp4aKPS0LU0KyLKvCdnB4QPiuah7Yg2QDp4aKPS0LU0KyLKvCdnB4QPiuah7Yg2QDp4a');

// Bestme guide
define('BESTME_INTERFACE_GUIDE', 'https://www.youtube.com/watch?v=_oeYXdEwOBU');
define('BESTME_CONTENT_GUIDE', 'https://www.youtube.com/watch?v=ltqoJ9cizB8');
define('BESTME_MENU_GUIDE', 'https://www.youtube.com/watch?v=VxPwi8dcHZI');
define('BESTME_DOMAIN_GUIDE', 'https://www.youtube.com/watch?v=0lEfU89xDI0');
define('BESTME_UPLOAD_PRODUCT_GUIDE', 'https://support.bestme.asia/2803-them-san-pham-moi/');
define('BESTME_COLLECTION_GUIDE', 'https://www.youtube.com/watch?v=vGSvfPj48k4');
define('BESTME_GOOGLE_SHOPPING_GUIDE', 'https://support.bestme.asia/2685-video-huong-dan-ket-noi-google-shopping/');
define('BESTME_FACEBOOK_PIXEL_GUIDE', 'https://support.bestme.asia/2682-video-huong-dan-gan-ma-facebook-pixel-len-website/');
define('BESTME_GOOGLE_ANALYTICS_GUIDE', 'https://support.bestme.asia/2679-video-huong-dan-gan-ma-google-analytics-len-website/');
define('BESTME_SHOPEE_GUIDE', 'https://support.bestme.asia/2676-video-huong-dan-dong-bo-san-pham-tu-shopee-ve-bestme/');
define('BESTME_BANNER_GUIDE', 'https://support.bestme.asia/2673-video-huong-dan-thay-banner-quang-cao-tren-website/');
define('BESTME_SLIDE_GUIDE', 'https://support.bestme.asia/2691-video-huong-dan-thay-doi-slide-quang-cao-tren-website/');

// Asycn Callback job redis queue
define('ASYCN_CALLBACK_JOB_REDIS_QUEUE', 'ASYCN_CALLBACK_JOB');
define('ASYNC_CALLBACK_DB', 3);
define('ASYNC_CALLBACK_CHATBOT_V2_ORDER_NOTIFY_URL', 'https://social-stg.novaonx.com/api/v1/orders/bestme-update'); // staging: https://social-stg.novaonx.com/api/v1/orders/bestme-update, production: https://social.novaonx.com/api/v1/orders/bestme-update

// link onboarding gift
define('ONBOARDING_GITF_LINK', 'https://m.me/567316780340939?ref=nv36493875673167803409391589357398');

// import categories .xls config
define('MAX_ROWS_IMPORT_CATEGORIES', 501); // 500 + Header
define('MAX_FILE_SIZE_IMPORT_CATEGORIES', 2097152); // bytes ~ 2097152 = 2Mb
define('SAMPLE_CATEGORY_UPLOAD', 'https://support.bestme.asia/wp-content/uploads/2020/07/file-mau-upload-loai-san-pham.xlsx');
define('SAMPLE_CATEGORY_UPLOAD_EN', 'https://support.bestme.asia/wp-content/uploads/2020/07/category-upload-sample.xlsx');

// VNPay. This will override define in VNPay. Let absent to use original value from VNPay
define('VNPAY_REGISTER', 'https://vnpayment.vnpay.vn/Lien-he-Vnpayment.htm');
define('VNPAY_MERCHANT_LOGIN', 'https://merchant.vnpay.vn/Users/Login.htm');
define('VNPAY_GUIDE', 'https://docs.google.com/document/d/1p2J9qJU8T2L80TNgHFB7CuuaUhbM35yDSBkzNGvzvjk/edit');

// GHTK. This will override define in GHTK. Let absent to use original value from GHTK
define('GHTK_LOGIN_URL', 'https://khachhang.giaohangtietkiem.vn/khach-hang/dang_nhap');
define('GHTK_ACCOUNT_INFO_URL', 'https://khachhang.giaohangtietkiem.vn/khach-hang/thong-tin-ca-nhan');
define('GHTK_GET_HUBS_URL', 'https://services.giaohangtietkiem.vn/services/shipment/list_pick_add');
define('GHTK_GUIDE_URL', 'https://docs.google.com/document/d/1FX-YKsBxbtRNhGRRT6aYCbnJJHytqoKNRCw8eFE6Bps/edit#');
define('GHTK_CREATE_ORDER_URL', 'https://services.giaohangtietkiem.vn/services/shipment/order');
define('GHTK_CANCEL_ORDER_URL', 'https://services.giaohangtietkiem.vn/services/shipment/cancel/');
define('GHTK_GET_ORDER_URL', 'https://services.giaohangtietkiem.vn/services/shipment/v2/');
define('GHTK_SHIPPING_SERVICES_ORDER_URL', 'https://services.giaohangtietkiem.vn/services/shipment/fee?');
// GHTK token of BESTME for callback
define('TRANSPORT_GHTK_TOKEN', 'C1ddd07592B80F08021faa8cf4b64bC0D9ba2eCa'); // staging: C1ddd07592B80F08021faa8cf4b64bC0D9ba2eCa, production: xxx

// Connect to NovaonX Social
//define('MIX_CLIENT_ID_SSO', '5f2b9d8ed43c2339f21a86a8'); // staging: 5f2b9d8ed43c2339f21a86a8, production: xxx
define('REDIRECT_TO_NOVAON_X_SOCIAL', 'https://accounts.novaonx.com?client_id=${CLIENT_ID}&shop_id=${SHOP_ID}&email=${EMAIL}'); // staging: https://accounts-dev.novaonx.com, production: https://accounts.novaonx.com
//// new: sso
define('GO_TO_NOVAON_X_SOCIAL', 'https://social.novaonx.com'); // staging: https://social-dev.novaonx.com, production: https://social.novaonx.com

// Lazada connection
//// app config
define('LAZADA_APP_KEY', 121867); // test: 121867, staging: 121865, production: xxx
define('LAZADA_APP_SECRET_KEY', 'ZmFWCRbi6MIzrwEmXBvwpbNonDBYZTNL'); // test: ZmFWCRbi6MIzrwEmXBvwpbNonDBYZTNL, staging: 1fP9ALPOPx5aI3Dws9geTh5Q9gveKTPE, production: xxx
define('LAZADA_API_URL_PREFIX', 'https://api.lazada.vn/rest');
//// redis config
define('MUL_REDIS_DB_LAZADA', 2);
define('LAZADA_PRODUCT_JOB_REDIS_QUEUE', 'LAZADA_PRODUCT_JOB');
define('LAZADA_PROCESS_INFO_PREFIX', 'LAZADA_PROCESS_INFO_');
define('LAZADA_PROCESS_INFO_TIMEOUT', 7200); // in seconds. default: 7200s = 2h
define('LAZADA_ORDER_JOB_REDIS_QUEUE', 'LAZADA_ORDER_JOB');
//// optimize mem
define('LAZADA_JOB_MAX_PROCESS', 5);
define('LAZADA_MAX_JOB_IN_A_PROCESS', 100);

// Lazada test option
define('LAZADA_SYNC_INTERVAL_TEST_VALUE', ''); // minutes, e.g: 5 for 5 min. Let empty or absent for no test

// email
define('MAIL_SMTP_HOST_NAME', 'tls://smtp.gmail.com');
define('MAIL_SMTP_PORT', '587');
define('MAIL_SMTP_USERNAME', 'bestme@mail.test');
define('MAIL_SMTP_PASSWORD', '123456');

# NOVAONX SSO
define('MIX_URL_SSO','https://accounts-dev.novaonx.com/');
define('MIX_CLIENT_ID_SSO','5eaff0cb9708f7588674d66a');
define('MIX_CLIENT_SECRET_SSO','66axntq4Av3WCmUVH9zzW');

// redis config for caching queries
/* Define redis server for cache */
define('MUL_REDIS_HOST_CACHE', '127.0.0.1');
define('REDIS_DB_CACHE_QUERY', 0);

# Redis to save count traffic web
define('MUL_REDIS_DB_BESTME_COUNT_TRAFFICT',4);
define('BESTME_WEB_TRAFFICT_REDIS', 'BESTME_WEB_TRAFFICT_REDIS');

// change password
define('CHANGE_PASSWORD_HREF', 'https://id.novaon.asia');

// v3.5.1 - base url open api
define('OPEN_API_BASE_URL', 'http://bestme.asia/api/open-api');
define('OPEN_API_BASE_URL_IMAGE_LOGO', 'http://bestme.asia/image/AppTheme/');

// rabbitmq config
define('RABBITMQ_HOST', 'localhost');
define('RABBITMQ_PORT', 5672);
define('RABBITMQ_USER_LOGIN', 'admin');
define('RABBITMQ_USER_PASSWORD', '123456');

// source production for adg
define('SOURCE_PRODUCTION_FOR_ADG', false);

define('PRODUCTION_BRAND_NAME', 'Bestme');
define('PRODUCTION_BRAND_NAME_UPPERCASE', 'BESTME');
define('PRODUCTION_BRAND_PHONE', '0961680486');
define('PRODUCTION_BRAND_LOGO', 'https://cdn.bestme.vn/images/bestme/logo.png');
define('PRODUCTION_BRAND_FAVICON', 'https://cdn.bestme.vn/images/bestme/logo.png');

define('BESTME_ELASTICSEARCH_SEVER_UPLOAD_URL', 'http://127.0.0.1:9200');
