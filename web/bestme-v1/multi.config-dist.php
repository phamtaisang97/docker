<?php


define('MUL_DOMAIN_NAME','bestme.test');
define('MUL_DOMAIN_NAME_PRE','bestme');
define('MUL_DOMAIN_NAME_POST', 'test');
define('MUL_CFG_DB_DRIVER','mysqli');
define('MUL_CFG_DB_HOST','localhost');
define('MUL_CFG_DB_NAME','x2_landing');
define('MUL_CFG_DB_USERNAME','root');
define('MUL_CFG_DB_PASSWORD','123456');
define('MUL_CFG_DB_PORT',3306);

define('MUL_REDIS_HOST','localhost');
define('MUL_REDIS_PORT',6379);
define('MUL_REDIS_DB',1);
define('MUL_REDIS_DOMAIN_PUBLISH_CHANEL', 'UpdateDomain');
define('MUL_REDIS_CUSTOMER_STAFF_PUBLISH_CHANEL', 'UpdateCustomerStaff');

define('TEMPORY_IMAGE_PREFIX', 'tempory_');
define('MAX_IMAGES_UPLOAD', 1000); // 1000 images
define('MAX_IMAGE_SIZE_UPLOAD', 2097152); // 2048 kb
define('CLOUD_NAME', 'novaonx2');
define('API_KEY', '1234567890123456');
define('API_SECRET', 'CxIlsEg1U4lY7VQ2k44iPL6goBt');

// Bestme POS
define('BESTME_POS_SECURE_KEY', 'KPS0LU0KyLKvCdnB4QPiuah7Yg2QDp4aKPS0LU0KyLKvCdnB4QPiuah7Yg2QDp4aKPS0LU0KyLKvCdnB4QPiuah7Yg2QDp4a');

// NKT: show error page on un-couched system exception
define('MUL_ERR_FILEPATH', '/var/www/html/opencart/system/storage/httperr/');
define('MUL_PRODUCTION_LOG', true);
define('MUL_PRODUCTION_LOG_FILE', '/var/www/opencart_storage/logs/error.log');
