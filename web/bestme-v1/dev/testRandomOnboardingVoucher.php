<?php

$rand_len = 5;
$max = pow(10, $rand_len) - 1;
while (1) {
    $rand_num = mt_rand(0, $max);
    $num_tmp = sprintf('%0' . $rand_len . 'd', $rand_num);
    $checksum = cal_checksum($num_tmp);
    $voucher = sprintf('%d%d%s', 43, $checksum, $num_tmp);

    if (strlen($voucher) < 8) {
        break;
    }

    //echo $voucher . "\n";
    usleep(100);
}

/**
 * Calculate checksum
 *
 * @param string $num
 * @return int
 */
function cal_checksum($num)
{
    $odd_sum = 0;
    $even_sum = 0;
    for ($i = 0; $i < strlen($num); $i++) {
        if (($i + 1) % 2 == 0) {
            //even
            $even_sum += $num[$i];
        } else {
            //odd:
            $odd_sum += $num[$i];
        }
    }

    $sum = $odd_sum * 3 + $even_sum;

    //divide modulo:
    $checksum = 10 - $sum % 10;

    return $checksum == 10 ? 0 : $checksum;
}

echo 'error: ' . $num_tmp . ' - ' . $checksum . ' - ' . $voucher;
