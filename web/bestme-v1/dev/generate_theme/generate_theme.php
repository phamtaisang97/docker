    <?php

define('DIR_ROOT', __DIR__ . '/../../');

$theme_configs = [
    [
        'code' => 'adg_topal_55',
        'name' => 'Adg Topal 55',
        'clone_from' => 'adg_topal_prima', // default: ''
        'class_name' => 'AdgTopal55'
    ],
];

writeln('Start generating theme . . .');

foreach ($theme_configs as $theme_config) {
    writelnSeparate();
    generateTheme($theme_config);
}

function generateTheme($theme_config)
{
    /* check if theme exists (check controller file) */
    if (file_exists(DIR_ROOT . '/admin/controller/extension/theme/' . $theme_config['code'] . '.php')) {
        writeln('Theme exists!');
    } else {
        /**
         * admin
         */
        /* generate controller file */
        $controller_content = file_get_contents(__DIR__ . '/templates/admin/controller.txt');

        // replace theme code
        $controller_content = str_replace('%THEME_CODE%', $theme_config['code'], $controller_content);

        // replace clone from section
        $clone_from_content = '' == $theme_config['clone_from'] ? '' :
"/**
 * Class ControllerExtensionTheme%THEME_CLASS_NAME%
 *
 * Cloned from {$theme_config['clone_from']}
 */";
        $controller_content = str_replace('%THEME_CLONE_FROM_SECTION%', $clone_from_content, $controller_content);

        // replace theme name
        $controller_content = str_replace('%THEME_CLASS_NAME%', $theme_config['class_name'], $controller_content);

        // create new controller file
        $controller_file_path = DIR_ROOT . 'admin/controller/extension/theme/' . $theme_config['code'] . '.php';
        createFile($controller_file_path, $controller_content);

        /* generate language en files */
        $language_en_content = file_get_contents(__DIR__ . '/templates/admin/language_en.txt');

        // replace theme code
        $language_en_content = str_replace('%THEME_NAME%', $theme_config['name'], $language_en_content);

        // create new language en file
        $language_en_file_path = DIR_ROOT . 'admin/language/en-gb/extension/theme/' . $theme_config['code'] . '.php';
        createFile($language_en_file_path, $language_en_content);

        /* generate language vi files */
        $language_vi_content = file_get_contents(__DIR__ . '/templates/admin/language_vi.txt');

        // replace theme code
        $language_vi_content = str_replace('%THEME_NAME%', $theme_config['name'], $language_vi_content);

        // create new language vi file
        $language_vi_file_path = DIR_ROOT . 'admin/language/vi-vn/extension/theme/' . $theme_config['code'] . '.php';
        createFile($language_vi_file_path, $language_vi_content);

        /* generate view files */
        $view_content = file_get_contents(__DIR__ . '/templates/admin/view.txt');

        // replace theme code
        $view_content = str_replace('%THEME_CODE%', $theme_config['code'], $view_content);

        // create new view file
        $view_file_path = DIR_ROOT . 'admin/view/template/extension/theme/' . $theme_config['code'] . '.twig';
        createFile($view_file_path, $view_content);

        writelnSeparate();

        /**
         * special theme
         */
        // create new special_theme folder
        $special_theme_folder_path = DIR_ROOT . 'system/library/theme_config/demo_data/special_theme/' . $theme_config['code'];
        if (createDirectory($special_theme_folder_path)) {
            // scan templates special_theme folder to copy file to destination (system/library/theme_config/demo_data/special_theme)
            $sql_files = scandir(DIR_ROOT . 'dev/generate_theme/templates/special_theme');
            if (false !== $sql_files) {
                // remove '.', '..' from sql_files
                $sql_files = array_diff($sql_files, ['.', '..']);
                foreach ($sql_files as $sql_file) {
                    // copy files
                    $new_sql_file_path = $special_theme_folder_path . '/' . $sql_file;
                    copy(DIR_ROOT . 'dev/generate_theme/templates/special_theme/' . $sql_file, $new_sql_file_path);
                    writeln(realpath($new_sql_file_path));
                }
            }
        } else {
            writeln('theme ' . $theme_config['name'] . ' already have special_theme folder!');
        }

        writelnSeparate();

        /**
         * catalog
         */
        // create new catalog theme folder
        $catalog_theme_folder_path = DIR_ROOT . 'catalog/view/theme/' . $theme_config['code'];
        if (createDirectory($catalog_theme_folder_path)) {
            $source_path = DIR_ROOT . ('' == $theme_config['clone_from'] ? 'dev/generate_theme/templates/catalog' : 'catalog/view/theme/' . $theme_config['clone_from']);
            recurseCopyDir($source_path, $catalog_theme_folder_path);
            writeln('add folder ' . realpath($catalog_theme_folder_path));

            // rename theme image
            $old_image_name = '' == $theme_config['clone_from'] ? 'theme_code' : $theme_config['clone_from'];
            if (rename($catalog_theme_folder_path . '/image/' . $old_image_name . '.png', $catalog_theme_folder_path . '/image/' . $theme_config['code'] . '.png')) {
                writeln('rename theme image to ' . $theme_config['code'] . '.png');
            } else {
                writeln('could not rename theme image');
            }
        } else {
            writeln('theme ' . $theme_config['name'] . ' already have catalog theme folder!');
        }

        writelnSeparate();

        /**
         * dev-init-db.sql
         */
        /* get dev-init-db.sql content */
        $dev_init_db_file_path = DIR_ROOT . 'dev/dev-init-db.sql';
        $dev_sql_content = file_get_contents($dev_init_db_file_path);

        /* update insert oc_extension statement */
        $dev_sql_content = str_replace('-- %EXTENSION_PLACEHOLDER% --', "('theme', '" . $theme_config['code'] . "')," . PHP_EOL . "-- %EXTENSION_PLACEHOLDER% --", $dev_sql_content);

        /* add insert oc_setting statement */
        $new_theme_sql = file_get_contents(DIR_ROOT . 'dev/generate_theme/templates/dev-init-db_oc_setting.txt');
        $new_theme_sql = str_replace('%THEME_CODE%', $theme_config['code'], $new_theme_sql);
        $dev_sql_content = str_replace('-- %SETTING_PLACEHOLDER% --', $new_theme_sql . PHP_EOL . PHP_EOL . "-- %SETTING_PLACEHOLDER% --", $dev_sql_content);

        /* update dev-init-db.sql content */
        file_put_contents($dev_init_db_file_path, $dev_sql_content);

        writeln('update dev/dev-init-db.sql: insert extension record, insert setting records');

        writelnSeparate();
    }
}

/* === local functions === */
function writelnSeparate()
{
    writeln('--------------------------------------------------');
}

function writeln($message)
{
    echo sprintf("%s%s", $message, PHP_EOL);
}

function createFile($filename, $data, $flags = 0)
{
    if (file_exists($filename)) {
        writeln("file '" . realpath($filename) . "' already exists!");
    } else {
        file_put_contents($filename, $data, $flags);
        writeln(realpath($filename));
    }
}

function createDirectory($dirname)
{
    if (is_dir($dirname)) {
        return false;
    }

    return mkdir($dirname . '/', 0777, TRUE);
}

function recurseCopyDir($src, $dst)
{
    $dir = opendir($src);
    @mkdir($dst);
    while (false !== ($file = readdir($dir))) {
        if (($file != '.') && ($file != '..')) {
            if (is_dir($src . '/' . $file)) {
                recurseCopyDir($src . '/' . $file, $dst . '/' . $file);
            } else {
                copy($src . '/' . $file, $dst . '/' . $file);
            }
        }
    }
    closedir($dir);
}