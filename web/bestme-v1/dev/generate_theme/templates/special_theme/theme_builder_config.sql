-- oc_theme_builder_config NO PRIMARY KEY
-- remove first
DELETE FROM `oc_theme_builder_config`
WHERE `store_id` = '0'
  AND `config_theme` = ''
  AND `code` = 'config';

-- insert
INSERT IGNORE INTO `oc_theme_builder_config` (`store_id`, `config_theme`, `code`, `key`, `config`, `date_added`, `date_modified`) VALUES
