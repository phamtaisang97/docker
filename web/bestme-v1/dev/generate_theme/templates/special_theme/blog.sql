-- oc_blog PRIMARY KEY (`blog_id`)
INSERT IGNORE INTO `oc_blog` (`blog_id`, `author`, `status`, `demo`, `date_publish`, `date_added`, `date_modified`) VALUES


-- oc_blog_category PRIMARY KEY (`blog_category_id`)
INSERT IGNORE INTO `oc_blog_category` (`blog_category_id`, `status`, `date_added`, `date_modified`) VALUES


-- oc_blog_category_description PRIMARY KEY (`blog_category_id`,`language_id`)
INSERT IGNORE INTO `oc_blog_category_description` (`blog_category_id`, `language_id`, `title`, `meta_title`, `meta_description`, `alias`) VALUES


-- oc_blog_description PRIMARY KEY (`blog_id`,`language_id`)
INSERT IGNORE INTO `oc_blog_description` (`blog_id`, `language_id`, `title`, `content`, `short_content`, `image`, `meta_title`, `meta_description`, `seo_keywords`, `alias`, `alt`, `type`, `video_url`) VALUES


-- oc_blog_to_blog_category NO PRIMARY KEY
-- remove first
DELETE FROM `oc_blog_to_blog_category`
WHERE `blog_id` IN () AND `blog_category_id` = 1;

-- insert
INSERT IGNORE INTO `oc_blog_to_blog_category` (`blog_id`, `blog_category_id`) VALUES
