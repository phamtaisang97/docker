<?php
function migrateBlogCategory($data) {
    global $db;
    global $language_id;
    $DB_PREFIX = DB_PREFIX;
    
    $sql = "INSERT INTO `{$DB_PREFIX}blog_category` SET status = {$data['status']}, `date_added` = NOW(), `date_modified` = NOW()";
    $db->query($sql);
    $category_id = $db->getLastId();

    //seo url
    $category_alias = $data['title'];
    if (isset($data['alias']) && $data['alias'] != '') {
        $category_alias = $data['alias'];
    }
    $slug_custom = createSlug($category_alias, '-');
    $alias = 'blog_category_id=' . $category_id;
    $sql = "INSERT INTO `{$DB_PREFIX}seo_url` 
                SET `language_id` = '" . (int)$language_id . "', 
                    `query` = '" . $alias . "', 
                    `keyword` = '" . $slug_custom . "', 
                    `table_name` = 'blog_category'";
    $db->query($sql);

    //add description
    $sql = "INSERT INTO `{$DB_PREFIX}blog_category_description`
                          SET `blog_category_id` = '" . (int)$category_id . "', 
                          `language_id` = '" . (int)$language_id . "', 
                          `title` = '" . $db->escape($data['title']) . "',
                          `meta_title` = '" . $db->escape($data['seo_title']) . "', 
                          `meta_description` = '" . $db->escape($data['seo_description']) . "', 
                          `alias` = '" . $db->escape($slug_custom) . "'";
    $db->query($sql);
}