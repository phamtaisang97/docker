<?php
try {
    // load simplehtmldom
    require_once DIR_SYSTEM . 'library/simplehtmldom/simple_html_dom.php';

    $inputFileName = __DIR__ . '/BESTME - 2022 - page content.xlsx';
    $sheet_name = 'Danh sách pages';

    /**  Identify the type of $inputFileName  **/
    $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
    /**  Create a new Reader of the type that has been identified  **/
    $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
    /**  Advise the Reader of which WorkSheets we want to load  **/
    $reader->setLoadSheetsOnly($sheet_name);
    /**  Load $inputFileName to a Spreadsheet Object  **/
    $spreadsheet = $reader->load($inputFileName);
    $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

    foreach ($sheetData as $key => $rowData) {
        if (empty($rowData['B'])) {
            continue;
        }

        $page_content_url = str_replace('dhcvietnam.com.vn', 'old.dhcvietnam.com.vn', strval($rowData['B']));

        // get blog category ??
        $url_parts = parse_url($page_content_url, PHP_URL_PATH);
        if (empty($url_parts)) {
            writeln("{$key}: {$page_content_url} missing category");
            continue;
        }

        $paths = explode('/', $url_parts);
        if (empty($paths[1]) || 'pages' != $paths[1]) {
            writeln("{$key}: {$page_content_url} is not a page url");
            continue;
        }

        if (4 < count($paths)) {
            writeln("{$key}: {$page_content_url} invalid url");
            continue;
        }
        $page_content_seo_url = $paths[2];
        // check if blog seo url is existed
        $sql = "SELECT EXISTS(SELECT 1 FROM " . DB_PREFIX . "seo_url WHERE `keyword` = '{$page_content_seo_url}' AND `language_id` = '2') result";
        $page_content_seo_url_exists = $db->query($sql);
        if (!empty($page_content_seo_url_exists->row['result']) && $page_content_seo_url_exists->row['result'] == 1) {
            writeln("{$key}: {$page_content_url} blog seo url is existed");
            continue;
        }

        // get blog data
        $page_content_data = getPageContentDataFromHtml($page_content_url);
        if (empty($page_content_data)) {
            writeln("{$key}: {$page_content_url} invalid page source");
            continue;
        }
        $page_content_data['alias'] = $page_content_seo_url;
        addContent($page_content_data);
    }

    writeln('Done!');
} catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
    writeln($e->getMessage());
}

function getPageContentDataFromHtml($url)
{
    try {
        $result = [];
        $html = file_get_html($url);
        if (false === $html) {
            writeln("Error: file_get_contents failed");
            return false;
        }

        // title
        $title = $html->find('.breadcrumb-small > h1', 0);
        if (empty($title)) {
            writeln("Error: title missing");
            return false;
        }
        $result['title'] = html_entity_decode($title->innertext);

        // seo title
        $seo_title = $html->find('meta[property=og:title]', 0);
        if (empty($seo_title)) {
            writeln("Error: seo title missing");
            return false;
        }
        $result['seo_title'] = html_entity_decode($seo_title->content);

        // description
        $description = $html->find('.main-content', 0);
        if (empty($description)) {
            writeln("Error: description missing");
            return false;
        }
        $result['description'] = html_entity_decode($description->innertext);

        // seo description
        $seo_description = $html->find('meta[name=description]', 0);
        if (empty($seo_description)) {
            writeln("Error: seo_description missing");
            return false;
        }
        $result['seo_description'] = html_entity_decode($seo_description->content);

        return $result;
    } catch (Exception $exception) {
        writeln($exception->getMessage());
        return false;
    }
}

function addContent($data)
{
    global $db;

    $data['status'] = 1;
    $db->query("INSERT INTO " . DB_PREFIX . "page_contents SET title = '" . $db->escape($data['title']) . "',
     seo_title = '" . $db->escape($data['seo_title']) . "', description = '" . $db->escape($data['description']) . "',
      seo_description = '" . $db->escape($data['seo_description']) . "', status = " . (int)$data['status'] . ", date_added = NOW()");

    $last_id = $db->getLastId();

    $languages = [1, 2];

    // SEO URL
    if (isset($data['alias'])) {
        $keyword = to_slug($data['alias']);
        $store_id = 0;
        $keyword = getSlugUnique($keyword);
        foreach ($languages as $language_id) {
            if (trim($keyword)) {
                $db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'page_id=" . (int)$last_id . "', keyword = '" . $db->escape($keyword) . "', `table_name` = 'page_content'");
            }
        }
    }

    return $last_id;
}

function getSlugUnique($slug)
{
    global $db;

    //we only bother doing this if there is a conflicting slug already
    $sql = "
            SELECT keyword FROM " . DB_PREFIX . "seo_url WHERE keyword  LIKE '" . $slug . "%'
        ";
    $query = $db->query($sql);
    $data = $query->rows;
    $slugs = [];
    foreach ($data as $value) {
        $slugs[] = $value['keyword'];
    }
    $count = count($data);

    if ($count != 0 && in_array($slug, $slugs)) {
        $max = 0;
        //keep incrementing $max until a space is found
        while (in_array(($slug . '-' . ++$max), $slugs)) ;
        //update $slug with the appendage
        $slug .= '-' . $max;
    }
    return $slug;
}