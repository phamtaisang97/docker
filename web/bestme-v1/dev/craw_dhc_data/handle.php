<?php
/**
 * command:
 * php dev\/craw_dhc_data\/handle.php -t blog
 * t: theme code
 *
 */
$options = getopt("t:");
const ASSET_RESOURCES_JSON_FILE_NAME = 'resources.json';
const FINAL_MINIFIED_FILE_NAME = 'final_minified';

if (empty($options['t'])) {
    exit('missing theme option!');
}

if (!is_file(__DIR__ . '/../../config.php')) {
    exit('config.php not found!');
}
require_once(__DIR__ . '/../../config.php');

if (!is_file(DIR_STORAGE . 'vendor/autoload.php')) {
    exit('autoload.php not found!');
}
require_once(DIR_STORAGE . 'vendor/autoload.php');

if (!is_file(__DIR__ . '/../../system/helper/general.php')) {
    exit('general.php not found!');
}
require_once(__DIR__ . '/../../system/helper/general.php');

// init lib
// Modification Override
spl_autoload_register('library');
spl_autoload_extensions('.php');

// init db
$db_hostname = DB_HOSTNAME;
$db_port = DB_PORT;
$db_username = DB_USERNAME;
$db_password = DB_PASSWORD;
$db_database = DB_DATABASE;
$db_prefix = DB_PREFIX;
$db_driver = DB_DRIVER;

$db = null;
try {
    $db = new DB($db_driver,
        htmlspecialchars_decode($db_hostname),
        htmlspecialchars_decode($db_username),
        htmlspecialchars_decode($db_password),
        htmlspecialchars_decode($db_database),
        htmlspecialchars_decode($db_port)
    );
} catch (Exception $e) {
    println(sprintf('Connect db "%s"... failed. Error: %s', $db_database, $e->getMessage()));
    return;
}
$language_id = 2;

// handle
switch ($options['t']) {
    case 'blog_category':
        require_once(__DIR__ . '/blog_category.php');
        $blog_categories_data = [
            [
                'status' => 1,
                'title' => 'Tin Khuyến mại',
                'alias' => 'khuyen-mai',
                'seo_title' => 'Khuyến mãi DHC - Hàng ngàn ưu đãi giảm giá, quà tặng hấp dẫn',
                'seo_description' => 'Tổng hợp các chương trình khuyến mại mới nhất tại DHC Việt Nam. Luôn ngập tràn ưu đãi với vô vàn sản phẩm thực phẩm chức năng, chăm sóc da, trang điểm cực hấp dẫn.'
            ],
            [
                'status' => 1,
                'title' => 'Hỏi đáp cùng chuyên gia',
                'alias' => 'hoi-dap-cung-chuyen-gia-dhc-viet-nam',
                'seo_title' => 'Hỏi đáp cùng chuyên gia',
                'seo_description' => 'Hãy cùng chuyên gia của DHC Việt Nam hỏi đáp về các vấn đề phổ biến trong quá trình chăm sóc sức khỏe và làm đẹp nhé.Chuyên gia: Dược sĩ Phạm HảiDược sĩ Phạm Hải tốt nghiệp tiến sĩ năm 2012, sau 4 năm tiếp tục làm nghiên sinh tại Cu Ba, Tây Ban Nha và tu nghiệp tại Châu Âu. Dược sĩ có trên 10 năm kinh nghiệm làm nghiên'
            ],
            [
                'status' => 1,
                'title' => 'Mẹo làm đẹp',
                'alias' => 'meo-lam-dep',
                'seo_title' => 'Tổng hợp các mẹo làm đẹp, bí quyết chăm sóc da, sống khỏe mạnh!',
                'seo_description' => 'Chia sẻ các bí quyết làm đẹp chung cho các nàng cùng xinh, cùng duyên dáng với làn da giữ mãi tuổi 20. Cùng DHC làm đẹp ngay!'
            ],
            [
                'status' => 1,
                'title' => 'CHỐNG LÃO HÓA',
                'alias' => 'chong-lao-hoa',
                'seo_title' => 'CHỐNG LÃO HÓA',
                'seo_description' => ''
            ],
            [
                'status' => 1,
                'title' => 'Làm đẹp cùng DHC',
                'alias' => 'lam-dep-cung-dhc',
                'seo_title' => 'Tổng hợp kiến thức và chia sẻ kinh nghiệm làm đẹp cùng sản phẩm DHC',
                'seo_description' => 'Tổng hợp kiến thức và chia sẻ kinh nghiệm, mẹo chăm sóc sức khỏe, làm đẹp hiệu quả từ sản phẩm của DHC. Cùng DHC Việt Nam khám phá ngay nhé!'
            ],
            [
                'status' => 1,
                'title' => 'DHC News',
                'alias' => 'news',
                'seo_title' => 'Tổng hợp các tin tức, sự kiện tại DHC Việt Nam',
                'seo_description' => 'Làm thế nào để chăm sóc da đúng cách? Làm thế nào để trở nên ngày càng xinh đẹp hơn? Hãy đón đọc DHC thường xuyên để cập nhật những xu hướng làm đẹp mới nhất nhé!'
            ],
            [
                'status' => 1,
                'title' => 'Cơ hội việc làm',
                'alias' => 'co-hoi-viec-lam',
                'seo_title' => 'Tuyển dụng, cơ hội việc làm hấp dẫn tại DHC Việt Nam',
                'seo_description' => 'Tổng hợp thông tin tuyển dụng mới nhất của DHC Việt Nam, nhiều việc làm với mức thu nhập hấp dẫn tới từ DHC - thương hiệu mỹ phẩm & TPCN Nhật Bản uy tín...'
            ],
            [
                'status' => 1,
                'title' => 'Cửa hàng Tp. Hồ Chí Minh',
                'alias' => 'cua-hang-tp-ho-chi-minh',
                'seo_title' => 'Cửa hàng Tp. Hồ Chí Minh',
                'seo_description' => 'Cửa hàng Tp. Hồ Chí Minh'
            ]
        ];
        foreach ($blog_categories_data as $blog_category_data) {
            migrateBlogCategory($blog_category_data);
        }
        break;

    case 'blog':
        require_once(__DIR__ . '/blog.php');
        break;

    case 'page_content':
        require_once(__DIR__ . '/page_content.php');
        break;
}

/* === local functions === */
function modification($filename)
{
    $file = null;

    if (substr($filename, 0, strlen(DIR_SYSTEM)) == DIR_SYSTEM) {
        $file = DIR_MODIFICATION . 'system/' . substr($filename, strlen(DIR_SYSTEM));
    }

    if (is_file($file)) {
        return $file;
    }

    return $filename;
}

function library($class)
{
    $file = DIR_SYSTEM . 'library/' . str_replace('\\', '/', strtolower($class)) . '.php';

    if (is_file($file)) {
        include_once(modification($file));

        return true;
    } else {
        return false;
    }
}

function writelnSeparate()
{
    writeln('--------------------------------------------------');
}

function writeln($message)
{
    echo sprintf("%s%s", $message, PHP_EOL);
}

function println($str)
{
    $now = (new DateTime())->format('YmdHis');

    echo "[$now]$str\n";
}

function createSlug($str, $delimiter = '-') {
    $str = to_slug($str);
    $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));

    return $slug;
}

function to_slug($str)
{
    $str = trim(mb_strtolower($str));
    $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
    $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
    $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
    $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
    $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
    $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
    $str = preg_replace('/(đ)/', 'd', $str);
    $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
    $str = preg_replace('/([\s]+)/', '-', $str);

    return $str;
}

function writeLog($content) {
    file_put_contents('/logs.txt', $content . PHP_EOL, FILE_APPEND);
}

function formatDateString($date_string) {
    $myDateTime = DateTime::createFromFormat('d/m/y', $date_string);
    return $myDateTime->format('Y-m-d H:i:s');
}