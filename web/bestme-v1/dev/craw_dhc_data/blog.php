<?php
try {
    // load simplehtmldom
    require_once DIR_SYSTEM . 'library/simplehtmldom/simple_html_dom.php';

    $inputFileName = __DIR__ . '/direct 301 DHC.xlsx';
    $sheet_name = 'clone old dhc to bestme';

    /**  Identify the type of $inputFileName  **/
    $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
    /**  Create a new Reader of the type that has been identified  **/
    $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
    /**  Advise the Reader of which WorkSheets we want to load  **/
    $reader->setLoadSheetsOnly($sheet_name);
    /**  Load $inputFileName to a Spreadsheet Object  **/
    $spreadsheet = $reader->load($inputFileName);
    $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

    foreach ($sheetData as $key => $rowData) {
        if (empty($rowData['A'])) {
            continue;
        }
        
//        $blog_url = str_replace('bestme.vn', 'dhcvietnam.com.vn', strval($rowData['B']));
        $blog_url = trim($rowData['A']);

        // get blog category
        $url_parts = parse_url($blog_url, PHP_URL_PATH);
        if (empty($url_parts)) {
            writeln("{$key}: {$blog_url} missing category");
            continue;
        }

        $paths = explode('/', $url_parts);
        if (empty($paths[1]) || 'blogs' != $paths[1]) {
            writeln("{$key}: {$blog_url} is not a blog url");
            continue;
        }

        if (4 < count($paths)) {
            writeln("{$key}: {$blog_url} invalid url");
            continue;
        }
        $blog_seo_url = $paths[3];
        // check if blog seo url is existed
        $sql = "SELECT EXISTS(SELECT 1 FROM " . DB_PREFIX . "seo_url WHERE `keyword` = '{$blog_seo_url}' AND `table_name` = 'blog' AND `language_id` = '2') result";
        $blog_seo_url_exists = $db->query($sql);
        if (!empty($blog_seo_url_exists->row['result']) && $blog_seo_url_exists->row['result'] == 1) {
            writeln("{$key}: {$blog_url} blog seo url is existed");
            continue;
        }

        $category_seo_keyword = $paths[2];
        $sql = "SELECT * FROM `" . DB_PREFIX . "seo_url` WHERE `keyword` = '{$category_seo_keyword}' AND `table_name` = 'blog_category' AND `language_id` = '2' LIMIT 1";
        $category_seo_url = $db->query($sql);
        if (!$category_seo_url->row) {
            writeln("{$key}: {$blog_url} could not find blog category seo url");
            continue;
        }

        $blog_category_query = $category_seo_url->row['query'];
        $blog_category_query_arr = explode('=', $blog_category_query);
        if (2 != count($blog_category_query_arr) && 'blog_category_id' != $blog_category_query_arr[0]) {
            writeln("{$key}: {$blog_url} invalid blog category query");
            continue;
        }

        // get blog data
        $blog_data = getBlogDataFromHtml($blog_url);
        if (empty($blog_data)) {
            writeln("{$key}: {$blog_url} invalid page source");
            continue;
        }
        $blog_data['blog_category'] = $blog_category_query_arr[1];
        $blog_data['alias'] =  $blog_seo_url;
        addBlog($blog_data);
    }

    writeln('Done!');
} catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
    writeln($e->getMessage());
}

function getBlogDataFromHtml($url) {
    try {
        $result = [];
        $html = file_get_html($url);
        if (false === $html) {
            writeln("Error: file_get_contents failed");
            return false;
        }

        // title
        $title = $html->find('meta[name=twitter:title]', 0);
        if (empty($title)) {
            writeln("Error: title missing");
            return false;
        }
        $result['title'] = html_entity_decode($title->content);

        // date_publish
        $date_publish_parent = $html->find('.meta-wrap-item', 0);
        if (empty($date_publish_parent)) {
            writeln("Error: date_publish parent missing");
            return false;
        }
        $date_publish = $date_publish_parent->find('span', 1);
        if (empty($date_publish)) {
            writeln("Error: date_publish missing");
            return false;
        }
        $result['date_publish'] = formatDateString(html_entity_decode($date_publish->innertext));

        // content
        $product_detail = $html->find('.wrap_blog_left', 0);
        if ($product_detail) {
            $product_detail->remove();
        }
        $content = $html->find('#ega-uti-editable-content', 0);
        if (empty($content)) {
            writeln("Error: content missing");
            return false;
        }
        $result['content'] = html_entity_decode($content->innertext);

        // short content
        $short_content = $content->find('p', 0);
        if (empty($short_content)) {
            writeln("Error: short content missing");
            return false;
        }
        $result['short_content'] = html_entity_decode($short_content->innertext);

        // image
        $image = $html->find('meta[property=twitter:image]', 0);
        if (empty($image)) {
            writeln("Error: image missing");
            return false;
        }
        $result['image'] = html_entity_decode($image->content);

        // description
        $description = $html->find('meta[name=description]', 0);
        if (empty($description)) {
            writeln("Error: description missing");
            return false;
        }
        $result['description'] = html_entity_decode($description->content);

        // tags
        $tags = $html->find('.tag_list', 0)->find('a');
        foreach ($tags as &$tag) {
            if (empty($tag)) {
                continue;
            }
            $tag = html_entity_decode($tag->innertext);
        }
        unset($tag);
        $result['tag_list'] = insertTag($tags);

        return $result;
    } catch (Exception $exception) {
        writeln($exception->getMessage());
        return false;
    }
}

function addBlog($data)
{
    global $db;
    global $language_id;

    $sql = "INSERT INTO " . DB_PREFIX . "blog 
                          SET demo = 0,
                          author = '" . 1 . "', 
                          status = '" . 1 . "', 
                          date_publish = '{$data['date_publish']}', 
                          date_added = '{$data['date_publish']}', 
                          date_modified = '{$data['date_publish']}'";
    $db->query($sql);
    $blog_id = $db->getLastId();

    $keywords = 'bestme.vn, ' . mb_strtolower($data['title']);

    $language_id = 2;
    $db->query("INSERT INTO " . DB_PREFIX . "blog_description 
                          SET blog_id = '" . $blog_id . "', 
                          language_id = '" . (int)$language_id . "', 
                          title = '" . $db->escape($data['title']) . "', 
                          content = '" . $db->escape($data['content']) . "', 
                          short_content = '" . $db->escape($data['short_content']) . "', 
                          image = '" . $db->escape($data['image']) . "',
                          alt = '" . $db->escape($data['title']) . "', 
                          meta_title = '" . $db->escape($data['title']) . "', 
                          meta_description = '" . $db->escape($data['description']) . "', 
                          alias = '" . $db->escape($data['alias']) . "',
                          seo_keywords = '" . $db->escape($keywords) . "',
                          `type` = 'image',
                          video_url = ''"
    );

    $db->query("INSERT INTO " . DB_PREFIX . "blog_to_blog_category 
                          SET  blog_id = '" . $blog_id . "', 
                          blog_category_id = '" . $db->escape($data['blog_category']) . "'"
    );

    // seo url
    $blog_alias = $data['alias'];
    $alias = 'blog=' . $blog_id;

    // delete seo before create
    $sql = "DELETE FROM `" . DB_PREFIX . "seo_url` WHERE `keyword` = '{$blog_alias}' AND `table_name` = 'blog' AND `language_id` = '2'";
    $db->query($sql);

    // create seo
    $db->query("UPDATE " . DB_PREFIX . "blog_description SET alias = '" . $db->escape($blog_alias) . "' WHERE blog_id = '" . (int)$blog_id . "'");
    $sql = "INSERT INTO " . DB_PREFIX . "seo_url 
                SET language_id = '" . (int)$language_id . "', 
                `query` = '" . $alias . "', 
                `keyword` = '" . $blog_alias . "', 
                `table_name` = 'blog'";
    $db->query($sql);

    if (isset($data['tag_list'])) {
        $tags_list = [];
        if (is_string($data['tag_list'])) {
            $tags_list = explode(',', $data['tag_list']);
        } elseif (is_array($data['tag_list'])) {
            $tags_list = $data['tag_list'];
        }

        foreach ($tags_list as $tag_id) {
            $db->query("INSERT INTO " . DB_PREFIX . "blog_tag 
                                  SET blog_id = '" . (int)$blog_id . "', 
                                  tag_id = '" . (int)$tag_id . "'");
        }
    }

    return $blog_id;
}

function insertTag($tag_list)
{
    global $db;
    $list_add = [];
    foreach ($tag_list as $tag) {
        $sql_search = "SELECT tag_id FROM " . DB_PREFIX . "tag WHERE `value`='" . $db->escape($tag) . "'";
        $data = $db->query($sql_search);
        if ($data->row) {
            $list_add[] = $data->row['tag_id'];
        } else {
            $sql_update = "INSERT INTO " . DB_PREFIX . "tag (value) VALUES ('" . $db->escape($tag) . "')";
            $db->query($sql_update);
            $list_add[] = $db->getLastId();
        }
    }
    return $list_add;
}