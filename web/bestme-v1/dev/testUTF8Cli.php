<?php

$fullName = "Hà Thế Dũng";

$fullNameArr = explode(' ', $fullName);

$dbHost = 'localhost';
$dbPort = 3306;
$dbUserName = 'root';
$dbPassword = '123456';
$dbName = 'opencart';

/* create db connection */
$conn = new mysqli($dbHost, $dbUserName, $dbPassword, $dbName, $dbPort);

/* check connection */
if ($conn->connect_error) {
    println(sprintf('Connect to db %s@%s:%s failed. Error: %s', $dbUserName, $dbHost, $dbPort, $conn->connect_error));
    return;
}

println(sprintf('Connected to db %s@%s:%s!', $dbUserName, $dbHost, $dbPort));

function println($msg)
{
    echo sprintf('[%s] %s %s', (new DateTime())->format('Y/m/d-H:i:s'), $msg, PHP_EOL);
}

/* create table if not existed */
$dbTable = 'test_utf8_user';
$sql = sprintf('CREATE TABLE IF NOT EXISTS `%s`
                (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `username` VARCHAR(100) CHARACTER SET utf8 NOT NULL,
                  `first_name` VARCHAR(100) CHARACTER SET utf8 NOT NULL,
                  `last_name` VARCHAR(100) CHARACTER SET utf8 NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
                ',
    $dbTable
);
$result = $conn->query($sql);
if ($result !== true) {
    println(sprintf('Could not create table %s!. Error: %s', $dbTable, $conn->error));
    exit(1);
}

println(sprintf('Created (if not exists) table %s!', $dbTable));

/* insert */
$username = 'username1';
$firstName = 'Dũng';
$lastName = 'Hà Thế';
$sql = sprintf("INSERT INTO `%s` (`username`, `first_name`, `last_name`) VALUES 
                ('%s', '%s', '%s')
                ",
    $dbTable,
    $username,
    $firstName,
    $lastName
);

$result = $conn->query($sql);
if ($result !== true) {
    println(sprintf('Could not insert new record! [%s, %s, %s]. Error: %s', $username, $firstName, $lastName, $conn->error));
    return;
}

println(sprintf('Inserted new record! [%s, %s, %s].', $username, $firstName, $lastName));

/* close connection */
$conn->close();

println('End!');