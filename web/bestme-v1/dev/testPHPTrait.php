<?php

trait A
{
    public function great()
    {
        echo "[A] Hello";
    }
}

trait B
{
    public function great()
    {
        echo "[B] Hello";
    }
}

class C
{
    use A, B {
        /*
         * notice: if missing below line code, the error will be:
         * PHP Fatal error:  Trait method great has not been applied, because there are collisions with other trait methods on C
         */
        A::great insteadof B;
    }

    public function myGreat()
    {
        $this->great();
    }
}

$c = new C();
$c->myGreat();
