SET sql_mode = '';

/* INSERT DATA */
-- product_to_store
INSERT INTO `$SHOP_ID$_product_to_store` (`product_id`, `store_id`, `product_version_id`, `quantity`, `cost_price`) VALUES


-- product_to_category
INSERT INTO `$SHOP_ID$_product_to_category` (`product_id`, `category_id`) VALUES


-- product image
INSERT INTO `$SHOP_ID$_product_image` (`product_image_id`, `product_id`, `image`, `image_alt`, `sort_order`) VALUES


-- product description
INSERT INTO `$SHOP_ID$_product_description` (`product_id`, `language_id`, `name`, `description`, `sub_description`, `seo_title`, `seo_description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES


-- product collection
INSERT INTO `$SHOP_ID$_product_collection` (`product_collection_id`, `product_id`, `collection_id`, `sort_order`) VALUES

-- product
INSERT INTO `$SHOP_ID$_product` (`product_id`, `model`, `sku`, `common_barcode`, `common_sku`, `common_compare_price`, `common_price`, `common_cost_price`, `barcode`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `sale_on_out_of_stock`, `stock_status_id`, `image`, `image_alt`, `multi_versions`, `manufacturer_id`, `shipping`, `price`, `price_currency_id`, `compare_price`, `c_price_currency_id`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `channel`, `viewed`, `demo`, `deleted`, `default_store_id`, `date_added`, `date_modified`) VALUES
