SET sql_mode = '';

/* INSERT DATA */
-- blog_to_blog_category
INSERT INTO `$SHOP_ID$_blog_to_blog_category` (`blog_id`, `blog_category_id`) VALUES


-- blog_category_description
INSERT INTO `$SHOP_ID$_blog_category_description` (`blog_category_id`, `language_id`, `title`, `meta_title`, `meta_description`, `alias`) VALUES


-- blog_category
INSERT INTO `$SHOP_ID$_blog_category` (`blog_category_id`, `status`, `date_added`, `date_modified`) VALUES


-- blog_description
INSERT INTO `$SHOP_ID$_blog_description` (`blog_id`, `language_id`, `title`, `content`, `short_content`, `image`, `meta_title`, `meta_description`, `seo_keywords`, `alias`, `alt`, `type`, `video_url`) VALUES


-- blog
INSERT INTO `$SHOP_ID$_blog` (`blog_id`, `author`, `status`, `demo`, `date_publish`, `date_added`, `date_modified`) VALUES

