SET sql_mode = '';

/* TRUNCATE */
TRUNCATE TABLE `$SHOP_ID$_category`;
TRUNCATE TABLE `$SHOP_ID$_category_description`;
TRUNCATE TABLE `$SHOP_ID$_category_path`;
TRUNCATE TABLE `$SHOP_ID$_category_to_store`;
TRUNCATE TABLE `$SHOP_ID$_collection`;
TRUNCATE TABLE `$SHOP_ID$_collection_description`;
TRUNCATE TABLE `$SHOP_ID$_product`;
TRUNCATE TABLE `$SHOP_ID$_product_collection`;
TRUNCATE TABLE `$SHOP_ID$_product_description`;
TRUNCATE TABLE `$SHOP_ID$_product_image`;
TRUNCATE TABLE `$SHOP_ID$_product_to_category`;
TRUNCATE TABLE `$SHOP_ID$_product_to_store`;
TRUNCATE TABLE `$SHOP_ID$_warehouse`;
TRUNCATE TABLE `$SHOP_ID$_seo_url`;
TRUNCATE TABLE `$SHOP_ID$_theme_builder_config`;
TRUNCATE TABLE `$SHOP_ID$_blog`;
TRUNCATE TABLE `$SHOP_ID$_blog_description`;
TRUNCATE TABLE `$SHOP_ID$_blog_category`;
TRUNCATE TABLE `$SHOP_ID$_blog_category_description`;
TRUNCATE TABLE `$SHOP_ID$_blog_to_blog_category`;
