<?php
global $db;

try {
    $categories = json_decode(file_get_contents(__DIR__ . '/categories.json'), true);

    foreach ($categories as $category) {
        // check if category name exists
        $DB_PREFIX = DB_PREFIX;
        $sql = "SELECT * FROM `{$DB_PREFIX}category_description` WHERE `name` = '{$category['name']}' LIMIT 1";
        $query = $db->query($sql);

        if ($query->row) {
            // exists: update
            editCategory($query->row['category_id'], $category);
        } else {
            // not exists: add
            addCategory($category);
        }
    }

    writeln('Done!');
} catch (\Exception $e) {
    writeln($e->getMessage());
}

function addCategory($data)
{
    global $db;

    /* Create new category */
    $cateogry_image = isset($data['image']) ? trim($data['image']) : '';
    $db->query("INSERT INTO " . DB_PREFIX . "category 
                          SET parent_id = '" . (int)$data['parent_id'] . "', 
                          status = '1', 
                          image = '" . $db->escape($cateogry_image) . "', 
                          date_modified = NOW(), 
                          date_added = NOW()");

    $category_id = $db->getLastId();
    $languages = [1,2];
    if (isset($data['name'])) {
        // add with all languages
        $meta_title = isset($data['meta_title']) ? $data['meta_title'] : '';
        $meta_keyword = isset($data['meta_keyword']) ? $data['meta_keyword'] : '';
        $meta_description = isset($data['meta_description']) ? $data['meta_description'] : '';

        foreach ($languages as $language) {
            $language_id = $language;
            $db->query("INSERT INTO " . DB_PREFIX . "category_description 
                                  SET category_id = '" . (int)$category_id . "', 
                                  language_id = '" . (int)$language_id . "', 
                                  `name` = '" . $db->escape(trim($data['name'])) . "', 
                                  `description` = '" . $db->escape(trim($data['name'])) . "',
                                  `meta_title` = '" . $db->escape(trim($meta_title)) . "', 
                                  `meta_keyword` = '" . $db->escape(trim($meta_keyword)) . "', 
                                  `meta_description` = '" . $db->escape(trim($meta_description)) . "'"
            );
        }
    }

    $store_id = 0;
    $db->query("INSERT INTO " . DB_PREFIX . "category_to_store 
                          SET category_id = '" . (int)$category_id . "', 
                          store_id = '" . (int)$store_id . "'");

    // seo
    $slug_custom = createSlug($data['keyword'], '-');

    foreach ($languages as $key => $language) {
        $language_id = $language;
        $alias = 'category_id=' . $category_id;
        $sql = "INSERT INTO " . DB_PREFIX . "seo_url 
                    SET language_id = '" . (int)$language_id . "', 
                        query = '" . $alias . "', 
                        keyword = '" . $slug_custom . "', 
                        table_name = 'category'
            ";
        $db->query($sql);
    }

    return $category_id;
}

function editCategory($category_id, $data)
{
    global $db;

    if (isset($data['image'])) {
        $db->query("UPDATE " . DB_PREFIX . "category  
                          SET image = '" . $db->escape(trim($data['image'])) . "' 
                          WHERE category_id = '" . (int)$category_id . "'");
    }

    $db->query("DELETE FROM " . DB_PREFIX . "category_description 
                          WHERE category_id = '" . (int)$category_id . "'");

    $languages = [1,2];
    if (isset($data['name'])) {
        // add with all languages
        $meta_title = isset($data['meta_title']) ? $data['meta_title'] : '';
        $meta_keyword = isset($data['meta_keyword']) ? $data['meta_keyword'] : '';
        $meta_description = isset($data['meta_description']) ? $data['meta_description'] : '';

        foreach ($languages as $language) {
            $language_id = $language;
            $db->query("INSERT INTO " . DB_PREFIX . "category_description 
                                  SET category_id = '" . (int)$category_id . "', 
                                  language_id = '" . (int)$language_id . "', 
                                  `name` = '" . $db->escape(trim($data['name'])) . "', 
                                  `description` = '" . $db->escape(trim($data['name'])) . "',
                                  `meta_title` = '" . $db->escape(trim($meta_title)) . "', 
                                  `meta_keyword` = '" . $db->escape(trim($meta_keyword)) . "', 
                                  `meta_description` = '" . $db->escape(trim($meta_description)) . "'"
            );
        }
    }

    //delete seo product
    $db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'category_id=" . (int)$category_id . "'");
    //seo
    $slug_custom = createSlug($data['keyword'], '-');
    //create new seo product
    foreach ($languages as $key => $language) {
        $language_id = $language;
        $alias = 'category_id=' . $category_id;
        $sql = "INSERT INTO " . DB_PREFIX . "seo_url SET language_id = '" . (int)$language_id . "', query = '" . $alias . "', keyword = '" . $slug_custom . "', table_name = 'category'";
        $db->query($sql);
    }
}