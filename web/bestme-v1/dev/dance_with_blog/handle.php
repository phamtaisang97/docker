<?php
/**
 * command:
 * php dev\/dance_with_blog\/handle.php -t blog
 * t: theme code
 *
 */
const ASSET_RESOURCES_JSON_FILE_NAME = 'resources.json';
const FINAL_MINIFIED_FILE_NAME = 'final_minified';

$options = getopt("t:");
if (empty($options['t'])) {
    exit('missing theme option!');
}

if (!is_file(__DIR__ . '/../../config.php')) {
    exit('config.php not found!');
}
require_once(__DIR__ . '/../../config.php');

if (!is_file(DIR_STORAGE . 'vendor/autoload.php')) {
    exit('autoload.php not found!');
}
require_once(DIR_STORAGE . 'vendor/autoload.php');

if (!is_file(__DIR__ . '/../../system/helper/general.php')) {
    exit('general.php not found!');
}
require_once(__DIR__ . '/../../system/helper/general.php');

// init lib
// Modification Override
spl_autoload_register('library');
spl_autoload_extensions('.php');

// init db
$db_hostname = DB_HOSTNAME;
$db_port = DB_PORT;
$db_username = DB_USERNAME;
$db_password = DB_PASSWORD;
$db_database = DB_DATABASE;
$db_prefix = DB_PREFIX;
$db_driver = DB_DRIVER;

$db = null;
try {
    $db = new DB($db_driver,
        htmlspecialchars_decode($db_hostname),
        htmlspecialchars_decode($db_username),
        htmlspecialchars_decode($db_password),
        htmlspecialchars_decode($db_database),
        htmlspecialchars_decode($db_port)
    );
} catch (Exception $e) {
    println(sprintf('Connect db "%s"... failed. Error: %s', $db_database, $e->getMessage()));
    return;
}
$language_id = 2;

// handle
switch ($options['t']) {
    case 'category':
        require_once(__DIR__ . '/categories.php');
        break;
    case 'blog':
        require_once(__DIR__ . '/blogs.php');
        break;
}

/* === local functions === */
function modification($filename)
{
    $file = null;

    if (substr($filename, 0, strlen(DIR_SYSTEM)) == DIR_SYSTEM) {
        $file = DIR_MODIFICATION . 'system/' . substr($filename, strlen(DIR_SYSTEM));
    }

    if (is_file($file)) {
        return $file;
    }

    return $filename;
}

function library($class)
{
    $file = DIR_SYSTEM . 'library/' . str_replace('\\', '/', strtolower($class)) . '.php';

    if (is_file($file)) {
        include_once(modification($file));

        return true;
    } else {
        return false;
    }
}

function writelnSeparate()
{
    writeln('--------------------------------------------------');
}

function writeln($message)
{
    echo sprintf("%s%s", $message, PHP_EOL);
}

function println($str)
{
    $now = (new DateTime())->format('YmdHis');

    echo "[$now]$str\n";
}

function createSlug($str, $delimiter = '-') {
    $str = to_slug($str);
    $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));

    return $slug;
}

function to_slug($str)
{
    $str = trim(mb_strtolower($str));
    $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
    $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
    $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
    $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
    $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
    $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
    $str = preg_replace('/(đ)/', 'd', $str);
    $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
    $str = preg_replace('/([\s]+)/', '-', $str);

    return $str;
}

function writeLog($content) {
    file_put_contents('/logs.txt', $content . PHP_EOL, FILE_APPEND);
}

function formatDateString($date_string) {
    $myDateTime = DateTime::createFromFormat('d/m/y', $date_string);
    return $myDateTime->format('Y-m-d H:i:s');
}