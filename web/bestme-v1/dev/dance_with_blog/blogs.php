<?php
global $db;

try {
    $blogs = getListBlog();
    writeln(count($blogs));
    foreach ($blogs as $blog) {
        $content = replaceContent($blog['content']);
        $short_content = replaceContent($blog['short_content']);
        if ($content != $blog['content'] || $short_content != $blog['short_content']) {
            $query = "UPDATE " . DB_PREFIX . "blog_description 
                          SET 
                          `content` = '" . $db->escape($content) . "', 
                          `short_content` = '" . $db->escape($short_content) . "'
                          WHERE `blog_id` = " . (int)$blog['blog_id'];
            $db->query($query);
            writeln($blog['title']);
        }
    }

    writeln('Done!');
} catch (\Exception $e) {
    writeln($e->getMessage());
}

function replaceContent($content) {
    return str_replace(['dhcvietnam.com.vn', 'DHC Việt Nam'], ['bestme.vn', 'Bestme'], $content);
}

function getAllBlogsSql($data = array())
{
    global $db;
    $db_prefix = DB_PREFIX;
    $language_id = 2;
    $sql = "SELECT b.*,bd.`language_id` ,bd.`title` ,bd.`content`,bd.`short_content` ,bd.`image` ,bd.`meta_description` ,bd.`alias`, bd.`source_author_name`,bc.`blog_category_id` ,
                       bc.`status` as blog_category_status, bcd.`title` as blog_category_title, u.`firstname`, u.`lastname` FROM `{$db_prefix}blog` b 
                LEFT JOIN `{$db_prefix}blog_description` bd ON (b.`blog_id` = bd.`blog_id`) 
                LEFT JOIN `{$db_prefix}user` u ON (b.`author` = u.`user_id`)
                LEFT JOIN `{$db_prefix}blog_to_blog_category` btbc ON (b.`blog_id` = btbc.`blog_id`)
                LEFT JOIN `{$db_prefix}blog_category` bc ON (bc.`blog_category_id` = btbc.`blog_category_id`)
                LEFT JOIN `{$db_prefix}blog_category_description` bcd ON (bc.`blog_category_id` = bcd.`blog_category_id`)
                ";

    if (isset($data['filter_tag']) && $data['filter_tag'] != '') {
        $sql .= " LEFT JOIN `{$db_prefix}blog_tag` bt ON (bt.`blog_id` = b.`blog_id`)";
        $sql .= " LEFT JOIN `{$db_prefix}tag` t ON (t.`tag_id` = bt.`tag_id`)";
    }

    $sql .= " WHERE 1=1 
                  AND bd.language_id = '" . $language_id . "' 
                  AND bcd.language_id = '" . $language_id . "' ";

    if (!empty($data['filter_name'])) {
        $sql .= " AND bd.`title` LIKE '%" . $db->escape($data['filter_name']) . "%'";
    }

    // TODO: use array, do not set '' for multiple statuses...
    if (!empty($data['filter_status'])) {
        $order_statuses = explode(',', $data['filter_status']);
        $order_statuses = array_map(function ($blog_status_id) {
            return "'" . (int)$blog_status_id . "'";

        }, $order_statuses);

        $sql .= " AND b.`status` IN (" . implode(",", $order_statuses) . ")";
    } elseif (isset($data['filter_status']) && $data['filter_status'] !== '') {
        $sql .= " AND b.`status` = '" . (int)$data['filter_status'] . "'";
    }
    
    if (!empty($data['filter_author']) && is_array($data['filter_author'])) {
        $blog_authors = array_map(function ($blog_author_id) {
            return "'" . (int)$blog_author_id . "'";

        }, $data['filter_author']);

        $sql .= " AND b.`author` IN (" . implode(", ", $blog_authors) . ")";
    }

    if (!empty($data['filter_category']) && is_array($data['filter_category'])) {
        $blog_categories = array_map(function ($blog_catrgory_id) {
            return "'" . (int)$blog_catrgory_id . "'";

        }, $data['filter_category']);

        $sql .= " AND bc.`blog_category_id` IN (" . implode(", ", $blog_categories) . ")";
    }

    if (!empty($data['filter_tag']) && is_array($data['filter_tag'])) {
        $blog_tags = array_map(function ($blog_tag_value) {
            return "'" . $blog_tag_value . "'";

        }, $data['filter_tag']);

        $sql .= " AND t.`value` IN (" . implode(", ", $blog_tags) . ")";
    }

    $sql .= " ORDER BY b.`date_modified` DESC";

    return $sql;
}

function getListBlog($data = array())
{
    global $db;
    $sql = getAllBlogsSql($data);

    // limit
    if (isset($data['start']) || isset($data['limit'])) {
        if ($data['start'] < 0) {
            $data['start'] = 0;
        }

        if ($data['limit'] < 1) {
            $data['limit'] = 20;
        }
        $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
    }

    $query = $db->query($sql);

    return $query->rows;
}