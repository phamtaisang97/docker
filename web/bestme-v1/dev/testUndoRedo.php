<?php

/* setup */
// Version
define('VERSION', '3.0.2.0');

// Configuration
if (is_file(__DIR__ . '/../config.php')) {
    require_once(__DIR__ . '/../config.php');
}

// Modification Override
function modification($filename)
{
    if (defined('DIR_CATALOG')) {
        $file = DIR_MODIFICATION . 'admin/' . substr($filename, strlen(DIR_APPLICATION));
    } elseif (defined('DIR_OPENCART')) {
        $file = DIR_MODIFICATION . 'install/' . substr($filename, strlen(DIR_APPLICATION));
    } else {
        $file = DIR_MODIFICATION . 'catalog/' . substr($filename, strlen(DIR_APPLICATION));
    }

    if (substr($filename, 0, strlen(DIR_SYSTEM)) == DIR_SYSTEM) {
        $file = DIR_MODIFICATION . 'system/' . substr($filename, strlen(DIR_SYSTEM));
    }

    if (is_file($file)) {
        return $file;
    }

    return $filename;
}

// Autoloader
if (is_file(DIR_STORAGE . 'vendor/autoload.php')) {
    require_once(DIR_STORAGE . 'vendor/autoload.php');
}

function library($class)
{
    $file = DIR_SYSTEM . 'library/' . str_replace('\\', '/', strtolower($class)) . '.php';

    if (is_file($file)) {
        include_once(modification($file));

        return true;
    } else {
        return false;
    }
}

spl_autoload_register('library');
spl_autoload_extensions('.php');

/* test */
$undoRedo = new \theme_config\Undo_Redo();

// init
echo "add : ";
$undoRedo->add(1);
echo "add : ";
$undoRedo->add(2);
echo "add : ";
$undoRedo->add(3);
echo "add : ";
$undoRedo->add(4);
echo "add : ";
$undoRedo->add(5);
printCurrentData($undoRedo, [1, 2, 3, 4, 5]);

// add
echo "add : ";
$undoRedo->add(6);
printCurrentData($undoRedo, [2, 3, 4, 5, 6]);

// undo
echo "undo: ";
$undoRedo->undo();
printCurrentData($undoRedo, [2, 3, 4, 5]);

echo "undo: ";
$undoRedo->undo();
printCurrentData($undoRedo, [2, 3, 4]);

// then redo
echo "redo: ";
$undoRedo->redo();
printCurrentData($undoRedo, [2, 3, 4, 5]);

// then add more, redo, then undo for overriding
echo "add : ";
$undoRedo->add(7);
printCurrentData($undoRedo, [2, 3, 4, 5, 7]);

echo "add : ";
$undoRedo->add(8);
printCurrentData($undoRedo, [3, 4, 5, 7, 8]);

echo "undo: ";
$undoRedo->undo();
printCurrentData($undoRedo, [3, 4, 5, 7]);

echo "add : ";
$undoRedo->add(9);
printCurrentData($undoRedo, [3, 4, 5, 7, 9]);


function printCurrentData(\theme_config\Undo_Redo $undoRedo, array $expect)
{
    $currentData = $undoRedo->allManagedData();

    echo sprintf('data: %s, expect: %s [%s]%s',
        implode(',', $currentData),
        implode(',', $expect),
        $currentData == $expect ? 'PASSED' : 'FAILED',
        PHP_EOL
    );
}