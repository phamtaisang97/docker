<?php
if (isset($_POST['submit'])) {
    $file = isset($_FILES['list']['tmp_name']) ? $_FILES['list']['tmp_name'] : null;
    $content = [];
    if (file_exists($file) && is_readable($file)) {
        $file = fopen($file, "r");
        while (!feof($file)) {
            $row = fgetcsv($file);
            if ($row) {
                $content[] = $row;
            }
        }
        fclose($file);
    } else {
        echo "<script>alert('Vui lòng chọn file!');</script>";
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Bestme admin tool</title>
    <script src="js/jquery.min.js"></script>
    <style>
        .copy {
            float: right;
        }
    </style>
</head>

<body>
<div style="margin-top: 40px; text-align: center">
    <h1 style="color: #0100b4;">Danh sách shop theo shopname</h1>
    <textarea rows="10" cols="100" name="shop" id="shop"
              placeholder="Các shop cách nhau bởi dấu ';' hoặc dấu xuống dòng, ví dụ: shop1;shop2"></textarea>
    <br>
    <br>
    Link database:<a href="http://pma.bestme.asia/tbl_sql.php?db=NOVX2_welcome&table=shop_customers" target="_blank">
        &nbsp;http://pma.bestme.asia/</a>
</div>

<div style="font-size: 20px; font-weight: bold; text-align: center; margin-top: 24px; padding: 15px;">
    SQL lấy thông tin các shop
</div>
<div style="font-style: italic; text-align: center; padding: 15px; border: 1px solid #eee8d5">
    <button data-id='shops_info' class='copy'>copy</button>
    <div id="shops_info">
        ...
    </div>
</div>

<div style="font-size: 20px; font-weight: bold; text-align: center; margin-top: 24px; padding: 15px;">
    Mở Link database >> Copy SQL >> thực thi SQL >> xuất file .csv
</div>

<div style="text-align: center; margin-top: 24px; " id="list">
    <form action="" method="post" enctype="multipart/form-data">
        Chọn file .csv vừa export ở trên <input type="file" name="list">
        <button type="submit" name="submit">Submit</button>
    </form>
</div>

<?php
if (isset($content)) {
    ?>
    <div style="text-align: center; margin-top: 24px;">
        Gói:
        <select id="type">
            <option value="basic">Cơ bản</option>
            <option value="optimal" selected>Tối ưu</option>
            <option value="advance">Nâng cao</option>
        </select>

        Từ ngày:&nbsp;<input id="start" type="text" value="2019-08-23">&nbsp;đến ngày:&nbsp;<input id="end" type="text"
                                                                                                   value="2021-02-23">
    </div>

    <div style="text-align: left; width: 80%; margin: 50px auto 0 auto; padding: 15px; border: 1px solid #eee8d5">
        <button data-id='sqltext' class='copy'>copy</button>
        <div id="sqltext">
            <?php
            foreach ($content as $item) {
                echo "<br>---<br><br>
                <b>shop_id:</b> $item[0]<br>
                <b>db:</b> $item[2]<br>
                <b>shopname:</b> $item[3]<br>
                <b>email:</b> $item[5]<br>
                ";
                if ($item['2'] <= 29) {
                    $server = 1;
                } else if (30 <= $item['2'] && $item[2] <= 109) {
                    $server = 2;
                } else if (110 <= $item['2'] && $item[2] <= 199) {
                    $server = 3;
                } else {
                    $server = 4;
                }
            ?>
                <a target="_blank"
                   href="http://pma.bestme.asia/sql.php?server=<?php echo $server ?>&db=NX2_<?php echo $item[2] ?>&table=<?php echo $item[0] ?>_user&pos=0">http://pma.bestme.asia/sql.php?server=<?php echo $server ?>
                    &db=NX2_<?php echo $item[2] ?>&table=<?php echo $item[0] ?>_user&pos=0</a>
                <p style="font-style: italic">
                    <br>```<br>
                    SELECT * FROM `<?php echo $item[0] ?>_setting` WHERE `key` LIKE '%config_packet_paid%'
                    <br>```<br>
                </p>
                <p style="font-style: italic">
                    <br>```<br>
                    INSERT INTO `<?php echo $item[0] ?>_setting` (`store_id`, `code`, `key`, `value`, `serialized`)
                    VALUES<br>
                    (0, 'config', 'config_packet_paid', '1', 0),<br>
                    (0, 'config', 'config_packet_paid_start', <b class="fill_start">'2019-08-23'</b>, 0),<br>
                    (0, 'config', 'config_packet_paid_type', <b class="fill_type">'optimal'</b>, 0),<br>
                    (0, 'config', 'config_packet_paid_end', <b class="fill_end">'2021-02-23'</b>, 0);
                    <br>```<br>
                </p>
                <?php
            }
            ?>
        </div>
    </div>
    <?php
}
?>

<script>
    $('#type').on('change', function () {
        $('.fill_type').html("'" + $(this).val() + "'");
    });

    $('#start').on('keyup', function () {
        $('.fill_start').html("'" + $(this).val() + "'");
    });

    $('#end').on('keyup', function () {
        $('.fill_end').html("'" + $(this).val() + "'");
    });

    $('#shop').on('keyup', function () {
        var shops = $('#shop').val()
            .replace(/(^\w+:|^)\/\//gm, '')
            .replace(/(.bestme.asia)/gm, '')
            .replace(/(.mybestme.net)/gm, '')
            .replace(/(.com|.net|.com.vn|.tk|.org|.vn)/gm, '')
            .replace(/\/$/gm, '')
            .replace(/\n/gm, ';')
            .split(';');

        var shop_context = "";
        $.each(shops, function (k, shop) {
            shop = $.trim(shop);
            if (shop === '') {
                return true;
            }
            if (k !== 0) {
                shop_context = shop_context + ", " + "<br>";
            }
            shop_context = shop_context + "'" + shop + "'";
        });

        $('#shops_info').html("" +
            "SELECT `id`, `customer_id`, `database_id`, `shopname`, `shop_domain`, `email`" + "<br>" +
            "FROM `shop_customers` " + "<br>" +
            "WHERE `shopname` IN (" + "<br>" + shop_context + ");");
    });

    $(document).on('click', '.copy', function () {
        var div = $(this).attr('data-id');
        div = $('#' + div);
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(div.text()).select();
        document.execCommand("copy");
        $temp.remove();
        document.execCommand("copy");
    });
</script>
</body>
</html>
