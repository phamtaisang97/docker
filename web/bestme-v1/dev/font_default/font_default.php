<?php
define('DIR_ROOT', __DIR__ . '/../../');
$catalog_theme_folder_path = DIR_ROOT . 'catalog/view/theme/';
$source_path = 'catalog/view/theme/';
//recurseCopyDir($source_path, $catalog_theme_folder_path);

// Define một function để xuất các file trong một thư mục
outputFiles($catalog_theme_folder_path);
function outputFiles($path){
    // Kiểm tra thư mục có tồn tại hay không
    if(file_exists($path) && is_dir($path)){
        // Quét tất cả các file trong thư mục
        $result = scandir($path);

        // Lọc ra các thư mục hiện tại (.) và các thư mục cha (..)
        $files = array_diff($result, array('.', '..'));

        if(count($files) > 0){
            // Lặp qua mảng đã trả lại
            $arr_fonts = [];
            foreach($files as $file){
                if(is_file("$path/$file")){
                    // Hiển thị tên File
                    echo $file . "<br>";
                } else if(is_dir("$path/$file")){
                    // Gọi đệ quy hàm nếu tìm thấy thư mục
                    $path_default_theme = $path.$file . "/asset/default_config/default.json";
                    $fp = @fopen($path_default_theme, "r");
                    // Kiểm tra file mở thành công không
                    if (!$fp) {
                        echo 'Mở file không thành công';
                    }
                    else
                    {
                        $data = json_decode(fread($fp, filesize($path_default_theme)), true);
                        $fonts = $data['config_theme_text']['all']['supported-fonts'];
                        $arr_fonts = array_merge($arr_fonts,$fonts);
                    }
                }
            }
            $fonts = array_unique($arr_fonts);
            print_r($fonts);
        } else{
            echo "ERROR: Không có file nào trong thư mục.";
        }
    } else {
        echo "ERROR: Thư mục không tồn tại.";
    }
}