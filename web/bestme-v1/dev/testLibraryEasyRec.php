<?php

/* setup */
// Version
define('VERSION', '3.0.2.0');

// Configuration
if (is_file(__DIR__ . '/../config.php')) {
    require_once(__DIR__ . '/../config.php');
}

// Autoloader
if (is_file(DIR_STORAGE . 'vendor/autoload.php')) {
    require_once(DIR_STORAGE . 'vendor/autoload.php');
}

/* easyrec test */
$VERSION = '1.1';
$BASE_URL = 'http://localhost:8081/easyrec-web/';
$API_KEY = '6491d97a8a0c3f7d07b1df5d83a83521';
$TENANT_ID = 'EASYREC_DEMO';
$API_VERSION = '1.1';
$TOKEN = '970217a66b67ea7f0145ab82f2a7ec92';

$esrConfig = new \Hafael\Easyrec\Config(
    $VERSION,
    $BASE_URL,
    $API_KEY,
    $TENANT_ID,
    $API_VERSION,
    $TOKEN
);
$esrActions = new \Hafael\Easyrec\Api\Actions($esrConfig);

$sessionId = 'SESSIONID001';
$itemId = 'ITEMID001';
$itemDescription = 'ITEMID001 DESC';
$itemUrl = 'http://localhost:8001/shop/ITEMID001';
$result = $esrActions->view(
    $sessionId,
    $itemId,
    $itemDescription,
    $itemUrl,
    $itemImageUrl = null,
    $itemType = null,
    $userId = null,
    $actionTime = null,
    $actionInfo = null
);

if (is_array($result)) {
    echo "Success! \n";
} else {
    echo "Failed! \n";
}
