<?php
// Version
define('VERSION', '3.0.2.0');
// Configuration
if (is_file(__DIR__ . '/../admin/config.php')) {
    require_once(__DIR__ . '/../admin/config.php');
}

global $db, $DB_PREFIX;
global $script;

$DB_PREFIX = DB_PREFIX;

/* run */
$argv = $_SERVER['argv'];
$script = array_shift($argv);
$action = array_shift($argv);
if (!in_array($action, ['--up', '--down'])) {
    writeln('Missing param --up or --down');
    usage();
    exit();
}

if ($action === '--up') {
    up();
}

if ($action === '--down') {
    down();
}

/* === default functions */

/**
 * on up version
 */
function up()
{
    writeln('Init default policy for google shopping');

    global $db;

    $db = getDB();

    /* payment */
    up_payment_method();

    /* delivery */
    up_delivery_method();

    /* policy */
    up_policy_method();

    /* update already run version */
    update_already_run();

    writeln('Done!');
}

/**
 * on down version
 */
function down()
{
    writeln('Roll back all default policy for google shopping');

    // 99.99% not used. Temp do nothing. // TODO: add code if need...
    // return;
    global $db;

    $db = getDB();

    /* payment */
    down_payment_method();

    /* delivery */
    down_delivery_method();

    /* policy */
    down_policy_method();

    /* update not run version */
    update_not_run();

    writeln('Done!');
}

/* === end - default functions */

/* === local functions */
function usage()
{
    writeln("Usage:");
    writeln("======");
    writeln("");
    $options = implode(" ", [
        '[--up]', '(will init default policy for google shopping)',
        '[--down]', '(will roll back all default policy for google shopping)'
    ]);
    writeln('php 20190718-default-policy-gg-shopping.php ' . $options);
    writeln("");
}

function getDB()
{
    // Create connection
    $conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    writeln("Connected successfully");

    /* enable UTF8 for connection */
    mysqli_query($conn, "SET NAMES 'utf8'");
    mysqli_query($conn, "SET CHARACTER SET utf8");
    mysqli_query($conn, "SET CHARACTER_SET_CONNECTION=utf8");
    mysqli_query($conn, "SET SQL_MODE = ''");

    return $conn;
}

function update_already_run()
{
    update_version_status($run = true);
}

function update_not_run()
{
    update_version_status($run = false);
}

function update_version_status($run = true)
{
    $run = $run ? 1 : 0;
    writeln('Update version status: ' . ($run === 1 ? 'run' : 'not run'));

    global $db, $DB_PREFIX;
    global $script;

    // create migration table if not existed
    $sql = "CREATE TABLE IF NOT EXISTS `{$DB_PREFIX}migration` (
              `migration_id` int(11) NOT NULL AUTO_INCREMENT,
              `migration` varchar(255) NOT NULL,
              `run` tinyint(1) NOT NULL DEFAULT 1,
              `date_run` datetime NOT NULL,
              PRIMARY KEY (`migration_id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";

    $query = $db->query($sql);
    if (!$query) {
        return false;
    }

    $migration = pathinfo($script, PATHINFO_FILENAME);

    // get existing version
    $sql = "SELECT * FROM `{$DB_PREFIX}migration`";
    $query = $db->query($sql);
    if (!$query) {
        // create new
        $sql = "INSERT INTO `{$DB_PREFIX}migration` (`migration`, `run`, `date_run`) VALUES 
                ('$migration', $run, NOW());";
        $query = $db->query($sql);
        if (!$query) {
            return false;
        }

        return true;
    }

    // else: edit
    $sql = "UPDATE `{$DB_PREFIX}migration` SET `run` = $run WHERE `migration` = '$migration'";
    $db->query($sql);

    return true;
}

function up_payment_method()
{
    writeln('Up payment methods');

    global $db, $DB_PREFIX;

    /* check if existed */
    if (is_method_existing('payment_methods')) {
        writeln('Payment methods already existed');
        return;
    }

    $sql = "DELETE FROM `{$DB_PREFIX}setting` WHERE `code` = 'payment' AND `key` = 'payment_methods';";
    $db->query($sql);

    $sql = "INSERT INTO `{$DB_PREFIX}setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES 
            (0, 'payment', 'payment_methods', '[{\"payment_id\":\"198535319739A\",\"name\":\"Thanh toán khi nhận hàng (COD)\",\"guide\":\"Bạn sẽ thanh toán toàn bộ giá trị đơn hàng khi nhận hàng\",\"status\":\"1\"}]', 1)";
    $db->query($sql);
}

function up_delivery_method()
{
    writeln('Up delivery methods');

    global $db, $DB_PREFIX;

    /* check if existed */
    if (is_method_existing('delivery_methods')) {
        writeln('Delivery methods already existed!');
        return;
    }

    $sql = "DELETE FROM `{$DB_PREFIX}setting` WHERE `code` = 'delivery' AND `key` = 'delivery_methods';";
    $db->query($sql);

    $sql = "INSERT INTO `{$DB_PREFIX}setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
            (0, 'delivery', 'delivery_methods', '[{\"id\":0,\"name\":\"Vận chuyển tiêu chuẩn\",\"status\":\"1\",\"fee_region\":\"20000\",\"fee_regions\":[{\"id\":1,\"name\":\"Hà Nội - Miễn phí vận chuyển\",\"provinces\":[\"Hà Nội\"],\"weight_steps\":[{\"type\":\"FROM\",\"from\":\"1\",\"to\":\"101\",\"price\":\"0\"}]}],\"fee_cod\":{\"fixed\":{\"status\":\"1\",\"value\":\"0\"},\"dynamic\":{\"status\":\"0\",\"value\":\"\"}}}]', 1)";
    $db->query($sql);
}

function up_policy_method()
{
    writeln('Up policy page');

    global $db, $DB_PREFIX;

    /* insert policy page default */
    // check if existed
    if (is_record_existed("{$DB_PREFIX}page_contents", 'page_id')) {
        writeln('Default Policy Page already existed!');
        return;
    }

    // insert
    $sql = "INSERT IGNORE INTO `{$DB_PREFIX}page_contents` (`page_id`,`title`, `description`, `seo_title`, `seo_description`, `status`, `date_added`) VALUES
            (1000, 'Chính sách và bảo mật', '&lt;h1 dir=&quot;ltr&quot;&gt;&lt;strong&gt;Chính sách thanh toán - giao hàng&lt;/strong&gt;&lt;/h1&gt;
                    &lt;p dir=&quot;ltr&quot;&gt;&lt;strong&gt;1. GIAO HÀNG – TRẢ TIỀN MẶT (COD)&lt;/strong&gt;&lt;/p&gt;
                    &lt;p dir=&quot;ltr&quot;&gt;&lt;strong&gt;- Chúng tôi áp dụng hình thức giao hàng COD cho tất cả các vùng miền trên toàn quốc.&lt;/strong&gt;&lt;/p&gt;
                    &lt;p dir=&quot;ltr&quot;&gt;&lt;strong&gt;2. CHI PHÍ GIAO HÀNG&lt;/strong&gt;&lt;/p&gt;
                    &lt;p dir=&quot;ltr&quot;&gt;&lt;strong&gt;- Chúng tôi giao hàng miễn phí đối với khách hàng tại nội thành Hà Nội. Các tỉnh thành khác sẽ áp dụng mức chi phí của đối tác vận chuyển khác.&lt;/strong&gt;&lt;/p&gt;
                    &lt;h1 dir=&quot;ltr&quot;&gt;&lt;strong&gt;Chính sách đổi trả&lt;/strong&gt;&lt;/h1&gt;
                    &lt;h3 dir=&quot;ltr&quot;&gt;&lt;strong&gt;1. Lý do chấp nhận đổi trả&lt;/strong&gt;&lt;/h3&gt;
                    &lt;p dir=&quot;ltr&quot;&gt;&lt;strong&gt;- Sản phẩm bị lỗi do nhà sản xuất trong 7 ngày đầu sử dụng, đối với khách mua hàng online thì áp dụng từ khi đơn hàng thành công. &lt;/strong&gt;&lt;/p&gt;
                    &lt;p&gt;&lt;strong&gt;2. Yêu cầu cho sản phẩm đổi trả&lt;/strong&gt;&lt;/p&gt;
                    &lt;p dir=&quot;ltr&quot;&gt;&lt;strong&gt;- Sản phẩm còn nguyên vẹn, đầy đủ nhãn mác, nguyên đai kiện, niêm phong theo quy cách ban đầu.&lt;/strong&gt;&lt;/p&gt;
                    &lt;p dir=&quot;ltr&quot;&gt;&lt;strong&gt;- Sản phẩm/dịch vụ còn đầy đủ phụ kiện.&lt;/strong&gt;&lt;/p&gt;
                    &lt;p&gt;&lt;strong&gt;3. Thời gian đổi trả&lt;/strong&gt;&lt;/p&gt;
                    &lt;p dir=&quot;ltr&quot;&gt;&lt;strong&gt;- Trong vòng 07 ngày kể từ ngày nhận sản phẩm.&lt;/strong&gt;&lt;/p&gt;
                    &lt;h1 dir=&quot;ltr&quot;&gt;&lt;strong&gt;CHÍNH SÁCH BẢO MẬT&lt;/strong&gt;&lt;/h1&gt;
                    &lt;p dir=&quot;ltr&quot;&gt;&lt;strong&gt;1. Thu thập thông tin cá nhân&lt;/strong&gt;&lt;/p&gt;
                    &lt;p dir=&quot;ltr&quot;&gt;&lt;strong&gt;- Chúng tôi sẽ dùng thông tin bạn đã cung cấp để xử lý đơn đặt hàng, cung cấp các dịch vụ và thông tin yêu cầu thông qua website và theo yêu cầu của bạn.&lt;/strong&gt;&lt;/p&gt;
                    &lt;p dir=&quot;ltr&quot;&gt;&lt;strong&gt;- Chúng tôi có thể chuyển tên và địa chỉ cho bên thứ ba để họ giao hàng cho bạn.&lt;/strong&gt;&lt;/p&gt;
                    &lt;p&gt; &lt;/p&gt;
                    &lt;p dir=&quot;ltr&quot;&gt;&lt;strong&gt;2. Bảo mật&lt;/strong&gt;&lt;/p&gt;
                    &lt;p dir=&quot;ltr&quot;&gt;&lt;strong&gt;- Chúng tôi có biện pháp thích hợp về kỹ thuật và an ninh để ngăn chặn truy cập trái phép hoặc trái pháp luật hoặc mất mát hoặc hủy hoặc thiệt hại cho thông tin của bạn.&lt;/strong&gt;&lt;/p&gt;
                    &lt;p&gt; &lt;/p&gt;', 'Chính sách thanh toán - giao hàng, Chính sách đổi trả, Chính sách bảo mật' , 'Chính sách thanh toán - giao hàng, Chính sách đổi trả, Chính sách bảo mật', 1, NOW())";
    $db->query($sql);

    /* insert seo_url for policy page default */
    // check if existed
    if (is_record_existed("{$DB_PREFIX}seo_url", 'seo_url_id')) {
        writeln('Seo url for Default Policy Page already existed!');
        return;
    }

    // insert
    $sql = "INSERT IGNORE INTO `{$DB_PREFIX}seo_url` (`seo_url_id`,`store_id`, `language_id`, `query`, `keyword`) VALUES
            (1000, 0, 1, 'page_id=1000' , 'chinh-sach-va-bao-mat'),
            (1001, 0, 2, 'page_id=1000' , 'chinh-sach-va-bao-mat')";
    $db->query($sql);

    /* add policy page to menu */
    // check if existed
    if (is_record_existed("{$DB_PREFIX}novaon_menu_item", 'id')) {
        writeln('Policy page already existed in menu!');
        return;
    }

    // insert
    $sql = "INSERT IGNORE INTO `{$DB_PREFIX}novaon_menu_item` (`id`, `parent_id`, `group_menu_id`, `status`, `position`, `created_at`, `updated_at`) VALUES
            (1000, 0, 1, 1, 0, NOW(), NOW())";
    $db->query($sql);

    /* insert oc_novaon_menu_item_description */
    // check if existed
    if (is_record_existed("{$DB_PREFIX}novaon_menu_item_description", 'menu_item_id')) {
        writeln('Menu description already existed!');
        return;
    }

    // insert
    $sql = "INSERT INTO `{$DB_PREFIX}novaon_menu_item_description` (`id`, `language_id`, `name`, `menu_item_id`) VALUES
            (1000, 1, 'Chính sách' , 1000),
            (1001, 2, 'Chính sách' , 1000)";
    $db->query($sql);

    /* insert to relation table */
    // check if existed
    if (is_record_existed("{$DB_PREFIX}novaon_relation_table", 'main_id')) {
        writeln('Relation already existed in relation table!');
        return;
    }

    // insert
    $sql = "INSERT INTO `{$DB_PREFIX}novaon_relation_table` (`main_name`, `main_id`, `child_name`, `child_id`, `type_id`, `url`, `redirect`, `weight_number`) VALUES
            ('group_menu', 1, 'menu_item', 1000, 5, '', 0, 0),
            ('menu_item', 1000, '', 0, 5, 'chinh-sach-va-bao-mat', 1, 0)";
    $db->query($sql);
}

function down_payment_method()
{
    down_setting('payment_methods', 'payment_id', "198535319739A");
}

function down_delivery_method()
{
    down_setting('delivery_methods', 'id', 0);
}

function down_policy_method()
{
    // TODO: do something...
}

function down_setting($key, $compare_id, $compare)
{
    global $db, $DB_PREFIX;

    $data = $db->query("SELECT * FROM {$DB_PREFIX}setting WHERE `key` = '$key'");
    $row = $data->fetch_row();
    if (!is_array($row) || (count($row) < 6)) {
        return;
    }
    $methods = $row[4];
    $setting_id = $row[0];
    $value = json_decode($methods, true);
    $i = 0;
    $is_existing_setting = false;
    foreach ($value as $item) {
        if (!is_array($item)) continue;
        $id = $item[$compare_id];
        if ($id == $compare) {
            if (count($value) == 1) {
                $sql = "DELETE FROM {$DB_PREFIX}setting WHERE `setting_id` = $setting_id";
                $db->query($sql);
                $is_existing_setting = false;
            } else {
                unset($value[$i]);
                $is_existing_setting = true;
            }
        }
        $i++;
    }

    if (!$is_existing_setting) {
        return;
    }
    $value = array_values($value);
    $value = json_encode($value, JSON_UNESCAPED_UNICODE);
    $sql = "UPDATE {$DB_PREFIX}setting SET `value`= '$value' WHERE `setting_id` = $setting_id";
    $db->query($sql);
}

function is_method_existing($key = '')
{
    global $db, $DB_PREFIX;

    $data = $db->query("SELECT * FROM `{$DB_PREFIX}setting` WHERE `key` = '$key'");
    // check payment method in db
    if ($row = $data->fetch_row()) {
        if (is_array($row) && count($row) >= 5 && !empty(json_decode($row[4], true))) {
            return true;
        }
    }

    return false;
}

// Check record for police tables
function is_record_existed($table, $id)
{
    global $db;

    $query = "SELECT * FROM `$table` WHERE `$id` = 1000";
    $data = $db->query($query);
    // check policy page existing in db
    if ($data->fetch_row()) {
        return true;
    }

    return false;
}

function writeln($message)
{
    echo sprintf("[%s] %s%s", (new DateTime())->format('YmdHis'), $message, PHP_EOL);
}
/* === end - local functions */
