<?php
/**
 * command:
 * php dev\/minify_theme_resources.php -t furniture_furniter
 * t: theme code
 *
 */
$options = getopt("t:");
const ASSET_RESOURCES_JSON_FILE_NAME = 'resources.json';
const FINAL_MINIFIED_FILE_NAME = 'final_minified';

if (empty($options['t'])) {
    exit('missing theme option!');
}

if (!is_file(__DIR__ . '/../config.php')) {
    exit('config.php not found!');
}

require_once(__DIR__ . '/../config.php');

if (!is_file(DIR_STORAGE . 'vendor/autoload.php')) {
    exit('autoload.php not found!');
}

require_once(DIR_STORAGE . 'vendor/autoload.php');

if (!is_file(__DIR__ . '/../system/helper/general.php')) {
    exit('general.php not found!');
}

require_once(__DIR__ . '/../system/helper/general.php');

$theme = $options['t'];
$theme_directory = DIR_APPLICATION . 'view/theme/' . $theme . '/';
$minifiedCssContent = $minifiedJsContent = '';

if (is_file($theme_directory . "/asset/" . ASSET_RESOURCES_JSON_FILE_NAME)) {
    $source = file_get_contents($theme_directory . "asset/" . ASSET_RESOURCES_JSON_FILE_NAME);
    $source = json_decode($source, true);
    $version = is_array($source) && array_key_exists('version', $source) ? $source['version'] : '1';

    if (array_key_exists('css', $source) && is_array($source['css'])) {
        foreach ($source['css'] as $item) {
            if (strpos($item, 'https://') === 0 || strpos($item, 'http://') === 0) {
                $itemCssContent = file_get_contents($item);
            } else {
                $itemCssContent = file_get_contents($theme_directory . $item);
            }

            // minify content
            $itemCssContent = minifyCss($itemCssContent);

            if (!empty($itemCssContent)) {
                $minifiedCssContent .= $itemCssContent;
            }
        }
    }

    if (array_key_exists('js', $source) && is_array($source['js'])) {
        foreach ($source['js'] as $item) {
            if (strpos($item, 'https://') === 0 || strpos($item, 'http://') === 0) {
                $itemJsContent = file_get_contents($item);
            } else {
                $itemJsContent = file_get_contents($theme_directory . $item);
            }

            // minify content
            $itemJsContent = minifyJs($itemJsContent);

            if (!empty($itemJsContent)) {
                $minifiedJsContent .= $itemJsContent;
            }
        }
    }
}

file_put_contents($theme_directory . 'css/' . FINAL_MINIFIED_FILE_NAME . '.css', $minifiedCssContent);
file_put_contents($theme_directory . 'js/' . FINAL_MINIFIED_FILE_NAME . '.js', $minifiedJsContent);


/* === local functions === */
function writelnSeparate()
{
    writeln('--------------------------------------------------');
}

function writeln($message)
{
    echo sprintf("%s%s", $message, PHP_EOL);
}

