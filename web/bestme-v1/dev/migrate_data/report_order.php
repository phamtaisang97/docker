<?php
global $db;

try {
    $db_prefix = DB_PREFIX;
    $sql = "SELECT * FROM `{$db_prefix}order` WHERE `order_id` NOT IN (SELECT `order_id` FROM `{$db_prefix}report_order`)";
    $orders = $db->query($sql);
    
    foreach ($orders->rows as $order) {
        $query = $db->query( "SELECT pts.cost_price AS cost_price, op.*
                FROM " . DB_PREFIX . "order_product op
                left join " . DB_PREFIX . "product p ON op.product_id = p.product_id   
                LEFT JOIN " . DB_PREFIX . "product_to_store pts ON p.product_id = pts.product_id AND p.default_store_id = pts.store_id 
                WHERE p.product_id=op.product_id
                AND if(p.multi_versions = 1, pts.product_version_id, 1) = if(p.multi_versions = 1, op.product_version_id, 1)
                AND op.order_id = ".(int)$order['order_id']);
        $products = $query->rows;

        $total_discount = 0;

        // order discount is divided by product
        $total_before_discount = 0;
        foreach ($products as $key => $product) {
            $total_before_discount += $product['quantity'] * $product['price'];
        }
        foreach ($products as $key => $product) {
            if($total_before_discount == 0) {
                $products[$key]['discount_with_order_discount'] = 0;
            } else {
                $percent_price = $product['quantity'] * $product['price'] / $total_before_discount;
                $products[$key]['discount_with_order_discount'] = $product['discount'] + $percent_price * $order['discount'];
                $total_discount += $product['discount'] + $percent_price * $order['discount'];
            }
        }

        $date = date_create($order['date_added']);
        $report_order_data = [
            'report_time' => date_format($date,"Y-m-d H".':00:00'),
            'report_date' => date_format($date,"Y-m-d"),
            'order_id' => $order['order_id'],
            'customer_id' => $order['customer_id'],
            'total_amount' => $order['total'],
            'order_status' => $order['order_status_id'],
            'store_id' => $order['store_id'],
            'user_id' => $order['user_id'],
            'source' => $order['source'],
            'discount' => $total_discount,
            'shipping_fee' => $order['shipping_fee'],
            'payment_method' => $order['payment_method']
        ];
        saveOrderReport($report_order_data);
    }

    writeln('Done!');
} catch (\Exception $e) {
    writeln($e->getMessage());
}

function saveOrderReport($data)
{
    global $db;
    /* validate order status: skip update order report if draft or cancelled */
    if (!isset($data['order_status']) || !in_array($data['order_status'], [7,8,9])) {
        writeln('invalid order ' . $data['order_id'] . ' ' . $data['order_status']);
        return;
    }

    $sql = "INSERT INTO " . DB_PREFIX . "report_order SET report_time = '" . $data['report_time'] . "'";
    $sql .= ", report_date = '" . $data['report_date'] . "'";
    $sql .= ", order_id = '" . $data['order_id'] . "'";
    $sql .= ", customer_id = '" . $data['customer_id'] . "'";
    $sql .= ", total_amount = '" . $data['total_amount'] . "'";
    $sql .= ", order_status = '" . $data['order_status'] . "'";
    $sql .= ", store_id = '" . $data['store_id'] . "'";
    $sql .= ", user_id = '" . $data['user_id'] . "'";
    $sql .= ", source = '" . $data['source'] . "'";
    $sql .= ", discount = '" . $data['discount'] . "'";
    $sql .= ", shipping_fee = '" . $data['shipping_fee'] . "'";
    $db->query($sql);

}