<?php

/* setup */
// Version
define('VERSION', '3.0.2.0');

// Configuration
if (is_file(__DIR__ . '/../config.php')) {
    require_once(__DIR__ . '/../config.php');
}

/* connect */
// Create connection
$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
writeln("Connected successfully");

/* enable UTF8 for connection */
mysqli_query($conn, "SET NAMES 'utf8'");
mysqli_query($conn, "SET CHARACTER SET utf8");
mysqli_query($conn, "SET CHARACTER_SET_CONNECTION=utf8");
mysqli_query($conn, "SET SQL_MODE = ''");

$allProducts = $conn->query("SELECT product_id FROM " . DB_PREFIX . "product")->fetch_all(MYSQLI_ASSOC);
$randProducts = [];
for ($i = 0; $i < 3; $i++) {
    $randProducts[] = $allProducts[rand(0, count($allProducts))]['product_id'];
}

/* generate test dashboard report */
$reportProducts = [];
$reportOrders = [];
$reportCustomers = [];
$reportWebTraffic = [];

$START_DATE = date_create_from_format('Y-m-d', '2019-10-01');
$END_DATE = date_create_from_format('Y-m-d', '2019-12-20');
$HOURS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];

$dateInterval = $END_DATE->diff($START_DATE);
$datePeriod = new DatePeriod($START_DATE, new DateInterval('P1D'), $END_DATE);
foreach ($datePeriod as $date) {
    /** @var DateTime $date */
    foreach ($HOURS as $h) {
        $reportProducts[] = [
            $date->setTime($h, 0, 0)->format('Y-m-d H:i:s'),
            $date->format('Y-m-d'),
            rand(0, 1000),
            $randProducts[rand(0,2)],
            NULL,
            rand(10, 100),
            rand(100e3, 700e3),
            rand(100e3, 700e3)
        ];
        $reportProducts[] = [
            $date->setTime($h, 0, 0)->format('Y-m-d H:i:s'),
            $date->format('Y-m-d'),
            rand(0, 1000),
            $randProducts[rand(0,2)],
            NULL,
            rand(10, 100),
            rand(100e3, 700e3),
            rand(100e3, 700e3)
        ];

        $reportOrders[] = [
            $date->setTime($h, 0, 0)->format('Y-m-d H:i:s'),
            $date->format('Y-m-d'),
            rand(0, 1000),
            rand(0, 1000),
            rand(0, (10e6 - 1)) * 1000,
            rand(6, 10)
        ];

        $reportOrders[] = [
            $date->setTime($h, 0, 0)->format('Y-m-d H:i:s'),
            $date->format('Y-m-d'),
            rand(0, 1000),
            rand(0, 1000),
            rand(0, (10e6 - 1)) * 1000,
            rand(6, 10)
        ];

        $reportOrders[] = [
            $date->setTime($h, 0, 0)->format('Y-m-d H:i:s'),
            $date->format('Y-m-d'),
            rand(0, 1000),
            rand(0, 1000),
            rand(0, (10e6 - 1)) * 1000,
            rand(6, 10)
        ];

        $reportCustomers[] = [
            $date->setTime($h, 0, 0)->format('Y-m-d H:i:s'),
            $date->format('Y-m-d'),
            rand(0, 1000)
        ];

        $reportCustomers[] = [
            $date->setTime($h, 0, 0)->format('Y-m-d H:i:s'),
            $date->format('Y-m-d'),
            rand(0, 1000)
        ];

        $reportWebTraffic[] = [
            $date->setTime($h, 0, 0)->format('Y-m-d H:i:s'),
            $date->format('Y-m-d'),
            rand(10, 100000),
            $date->format('Y-m-d H:i:s')
        ];
    }
}

if (1 == 0) {
    writeln('generated reports:');
    foreach ($reports as $report) {
        writeln(implode(', ', $report));
    }
    writeln("");
}

$valueReportProductsArr = [];
foreach ($reportProducts as $report) {
    foreach ($report as &$r) {
        $r = sprintf("'%s'", mysqli_real_escape_string($conn, $r));
    }
    unset($r);

    $valueReportProductsArr[] = sprintf('(%s)', implode(', ', $report));
}

$valueReportOrdersArr = [];
foreach ($reportOrders as $report) {
    foreach ($report as &$r) {
        $r = sprintf("'%s'", mysqli_real_escape_string($conn, $r));
    }
    unset($r);

    $valueReportOrdersArr[] = sprintf('(%s)', implode(', ', $report));
}

$valueReportCustomersArr = [];
foreach ($reportCustomers as $report) {
    foreach ($report as &$r) {
        $r = sprintf("'%s'", mysqli_real_escape_string($conn, $r));
    }
    unset($r);

    $valueReportCustomersArr[] = sprintf('(%s)', implode(', ', $report));
}

$valueReportWebTrafficArr = [];
foreach ($reportWebTraffic as $report) {
    foreach ($report as &$r) {
        $r = sprintf("'%s'", mysqli_real_escape_string($conn, $r));
    }
    unset($r);

    $valueReportWebTrafficArr[] = sprintf('(%s)', implode(', ', $report));
}

/* insert */
$sqlReportProducts = "INSERT IGNORE INTO " . DB_PREFIX . "report_product (`report_time`, `report_date`, `order_id`, `product_id`, `product_version_id`, `quantity`, `price`, `total_amount`)
VALUES " . implode(', ', $valueReportProductsArr);

if ($conn->query($sqlReportProducts) === TRUE) {
    writeln("New " . count($valueReportProductsArr) . " records data report products created successfully");
} else {
    writeln("Error: " . $conn->error);
}

/* insert */
$sqlReportOrders = "INSERT IGNORE INTO " . DB_PREFIX . "report_order (`report_time`, `report_date`, `order_id`, `customer_id`, `total_amount`, `order_status`)
VALUES " . implode(', ', $valueReportOrdersArr);

if ($conn->query($sqlReportOrders) === TRUE) {
    writeln("New " . count($valueReportOrdersArr) . " records data report orders created successfully");
} else {
    writeln("Error: " . $conn->error);
}

/* insert */
$sqlReportCustomers = "INSERT IGNORE INTO " . DB_PREFIX . "report_customer (`report_time`, `report_date`, `customer_id`)
VALUES " . implode(', ', $valueReportCustomersArr);

if ($conn->query($sqlReportCustomers) === TRUE) {
    writeln("New " . count($valueReportCustomersArr) . " records data report customers created successfully");
} else {
    writeln("Error: " . $conn->error);
}

/* insert */
$sqlReportWebTraffic = "INSERT IGNORE INTO " . DB_PREFIX . "report_web_traffic (`report_time`, `report_date`, `value`, `last_updated`)
VALUES " . implode(', ', $valueReportWebTrafficArr);

if ($conn->query($sqlReportWebTraffic) === TRUE) {
    writeln("New " . count($valueReportWebTrafficArr) . " records data report web traffic created successfully");
} else {
    writeln("Error: " . $conn->error);
}

/* close connection */
$conn->close();

/* = = = = = = = = = = = = = = = = */

function writeln($message)
{
    echo sprintf("[%s] %s%s", (new DateTime())->format('YmdHis'), $message, PHP_EOL);
}