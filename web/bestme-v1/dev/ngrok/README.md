# Setup and run ngrok

1. Download ngrok bin at [https://ngrok.com/download](https://ngrok.com/download)

1. Extract here

1. Edit config: ngrok.yml. See guide [https://ngrok.com/docs#config-location](https://ngrok.com/docs#config-location)

1. Provide permission to ngrok bin

   ```
   sudo chmod +x ngrok
   ```

1. Run

   ```
   ./start-ngrok.sh
   ```