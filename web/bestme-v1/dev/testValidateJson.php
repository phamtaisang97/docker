<?php

/* setup */
// Version
define('VERSION', '3.0.2.0');

// Configuration
if (is_file(__DIR__ . '/../config.php')) {
    require_once(__DIR__ . '/../config.php');
}

// Modification Override
function modification($filename)
{
    if (defined('DIR_CATALOG')) {
        $file = DIR_MODIFICATION . 'admin/' . substr($filename, strlen(DIR_APPLICATION));
    } elseif (defined('DIR_OPENCART')) {
        $file = DIR_MODIFICATION . 'install/' . substr($filename, strlen(DIR_APPLICATION));
    } else {
        $file = DIR_MODIFICATION . 'catalog/' . substr($filename, strlen(DIR_APPLICATION));
    }

    if (substr($filename, 0, strlen(DIR_SYSTEM)) == DIR_SYSTEM) {
        $file = DIR_MODIFICATION . 'system/' . substr($filename, strlen(DIR_SYSTEM));
    }

    if (is_file($file)) {
        return $file;
    }

    return $filename;
}

// Autoloader
if (is_file(DIR_STORAGE . 'vendor/autoload.php')) {
    require_once(DIR_STORAGE . 'vendor/autoload.php');
}

function library($class)
{
    $file = DIR_SYSTEM . 'library/' . str_replace('\\', '/', strtolower($class)) . '.php';

    if (is_file($file)) {
        include_once(modification($file));

        return true;
    } else {
        return false;
    }
}

spl_autoload_register('library');
spl_autoload_extensions('.php');

/* test */

$schemas = [
    /* theme */
    __DIR__ . '/../system/library/theme_config/schema/theme/color.json',
    __DIR__ . '/../system/library/theme_config/schema/theme/text.json',
    __DIR__ . '/../system/library/theme_config/schema/theme/favicon.json',
    __DIR__ . '/../system/library/theme_config/schema/theme/social.json',
    __DIR__ . '/../system/library/theme_config/schema/theme/checkout-page.json',
    /* section */
    __DIR__ . '/../system/library/theme_config/schema/section/sections.json',
    __DIR__ . '/../system/library/theme_config/schema/section/banner.json',
    __DIR__ . '/../system/library/theme_config/schema/section/best_sales_product.json',
    __DIR__ . '/../system/library/theme_config/schema/section/best_views_product.json',
    __DIR__ . '/../system/library/theme_config/schema/section/blog.json',
    __DIR__ . '/../system/library/theme_config/schema/section/detail_product.json',
    __DIR__ . '/../system/library/theme_config/schema/section/footer.json',
    __DIR__ . '/../system/library/theme_config/schema/section/header.json',
    __DIR__ . '/../system/library/theme_config/schema/section/hot_product.json',
    __DIR__ . '/../system/library/theme_config/schema/section/list_product.json',
    __DIR__ . '/../system/library/theme_config/schema/section/partner.json',
    __DIR__ . '/../system/library/theme_config/schema/section/slideshow.json',
];

$configs = [
    __DIR__ . '/../system/library/theme_config/default_config/theme/color.json',
    __DIR__ . '/../system/library/theme_config/default_config/theme/text.json',
    __DIR__ . '/../system/library/theme_config/default_config/theme/favicon.json',
    __DIR__ . '/../system/library/theme_config/default_config/theme/social.json',
    __DIR__ . '/../system/library/theme_config/default_config/theme/checkout-page.json',
    /* section */
    __DIR__ . '/../system/library/theme_config/default_config/section/sections.json',
    __DIR__ . '/../system/library/theme_config/default_config/section/banner.json',
    __DIR__ . '/../system/library/theme_config/default_config/section/best_sales_product.json',
    __DIR__ . '/../system/library/theme_config/default_config/section/best_views_product.json',
    __DIR__ . '/../system/library/theme_config/default_config/section/blog.json',
    __DIR__ . '/../system/library/theme_config/default_config/section/detail_product.json',
    __DIR__ . '/../system/library/theme_config/default_config/section/footer.json',
    __DIR__ . '/../system/library/theme_config/default_config/section/header.json',
    __DIR__ . '/../system/library/theme_config/default_config/section/hot_product.json',
    __DIR__ . '/../system/library/theme_config/default_config/section/list_product.json',
    __DIR__ . '/../system/library/theme_config/default_config/section/partner.json',
    __DIR__ . '/../system/library/theme_config/default_config/section/slideshow.json',
];

$errors = 0;
foreach ($schemas as $idx => $schema) {
    echo "{$configs[$idx]}:" . PHP_EOL;
    $validation = new \theme_config\validators\Theme_Validator(realpath($schema));
    $config = json_decode(file_get_contents($configs[$idx]), true);

    try {
        $validation->validate($config);
    } catch (Exception $e) {
        $errors++;
    }

    print_r($validation->getErrors());
    echo PHP_EOL;
}

echo 'Done! Total: ' . count($schemas) . ' - Errors: ' . $errors . PHP_EOL;
