<?php

/* === DO LEARNING === */

/*
 * scalar type declarations
 * string, int, float, bool
 */
function test1(array $a, string $b, float $c, int ...$ints): string
{
    return sprintf('test1: %s %s %s %s', implode('', $a), $b, round($c, 4), array_sum($ints));
}

echoln(test1(['arr', 'ay'], 'string', 1.123456789, 1, 2, 3, 4, 5, 6));


/*null coalesing operator*/
function test2($arr1, $arr2): string
{
    return $arr1['a'] ?? $arr2['b'] ?? 'not found neither a and b';
}

echoln(sprintf('test2: %s', test2(['a' => 1], ['b' => 2])));


/*spaceship operator*/
function test3($a, $b): int
{
    return $a <=> $b;
}

echoln(sprintf('test3: %s (%s)', test3(1, 2), test3(1, 2) == -1 ? 'less' : 'greater or equal'));


/*const array*/
const CONST_ARRAY = ['test4', 'test4(1)', 'test4(again)'];
function test4(): array
{
    return CONST_ARRAY;
}

echoln(sprintf('test4: %s', implode(', ', test4())));


/*anonymous class*/

interface Logger
{
    public function log($msg);
}

function test5($msg)
{
    $anonymousClass = new class implements Logger
    {
        function log($msg)
        {
            echoln(sprintf('[Logger] %s', $msg));
        }
    };

    $anonymousClass->log($msg);
}

echoln(sprintf('test5: %s', ''));
test5('Log from anonymous class');


/*closure call*/

class Test6
{
    protected $gain = 1.5;
}

function test6(int $a, int $b): int
{
    $closureFn = function ($a, $b) {
        return $a + $b * 1.5;
    };

    return $closureFn->call(new Test6(), $a, $b);
}

echoln(sprintf('test6: %s', test6(1, 2)));


/*serialize/unserialize*/

Class SerializeClass
{
    protected $a;
    protected $b;

    /**
     * SerializeClass constructor.
     * @param int $a
     * @param array $b
     */
    public function __construct(int $a, array $b)
    {
        $this->a = $a;
        $this->b = $b;
    }

    public function toString(): string
    {
        return sprintf('%s-%s', $this->a, implode('%', $this->b));
    }
}

function test7(): array
{
    $c = new SerializeClass(1, [2, 'val3']);
    $d = serialize($c);
    $e = unserialize($d, ['allowed_classes' => false]);
    $f = unserialize($d, ['allowed_classes' => ['SerializeClass']]);
    return [$c->toString(), $d, $e, $f];
}

$test7Result = test7();
echoln(sprintf('test7: orig=%s serialize=%s unserialize(no class)=%s unserialize(with class)=%s', $test7Result[0], $test7Result[1], '$test7Result[2] but line below by var_dump...', $test7Result[3]->toString()));
var_dump($test7Result[2]);


/*group use declarations*/
function test8(): array
{
    return [
        'use some\namespace\{Class1, Class2, Class3 as C3};',
        'use function some\namespace\{fn1, fn2};',
        'use const some\namespace\{CONST1, CONST2};',
    ];
}

echoln(sprintf('test8:%s%s', PHP_EOL, implode(PHP_EOL, test8())));

/*generator function with expression*/
function test9(): Generator
{
    for ($i = 0; $i < 100; $i++) {
        $injected = yield $i => bin2hex(random_bytes(10));
        if ($injected === 'stop') return; // php7: return when need!
    }
}

$test9Result = test9();
$test9ResultArr = [];
$test9Count = 5;
foreach ($test9Result as $t10rK => $t10rV) {
    $test9Count--;
    // optional
    if ($test9Count < 0) {
        $test9Result->send('stop'); // TODO: check if this equals to "break;" line?
    }

    $test9ResultArr[$t10rK] = $t10rV;
}
echoln(sprintf('test9: %s', implode(', ', $test9ResultArr)));


/*generator delegation*/
function test10(): Generator
{
    for ($i = 0; $i < 2; $i++) {
        yield $i => bin2hex(random_bytes(10));
    }
}

function test10A(): Generator
{
    for ($i = 0; $i < 4; $i++) {
        yield $i => rand(0, (int)10e20);

        if ($i > 1) {
            yield from test10();
        }
    }
}

$test10Result = test10A();
$test10ResultArr = [];
foreach ($test10Result as $t10rK => $t10rV) {
    $test10ResultArr[$t10rK] = $t10rV;
}

// TODO: unknown why result not in correct order...
// TODO: [2019-01-28 11:02:31] test10: 9cdb76664756789d7a89, 9576b5973fd8a20e3b21, 1484121619338119424, 2446976103873978368
echoln(sprintf('test10: %s', implode(', ', $test10ResultArr)));


/*int div*/
function test11(int $a, int $b): int
{
    return $b === 0 ? 'invalid devisor' : intdiv($a, $b);
}

echoln(sprintf('test11: %s', test11(10, 3)));


/*println*/
function echoln($msg)
{
    echo sprintf('[%s] %s%s', (new DateTime())->format('Y-m-d H:i:s'), $msg, PHP_EOL);
}


/* === DO TESTING === */
echo (function ($x) {return [$x, $x + 1, $x + 2];})(4)[2];

$a = null;
$b = 1;
$c = 'c';
echo $a??'!a';
echo $b??'!b';
echo $c??'!c';
echo $d??'!d';

echo 1 <=>2;
echo 'aa' <=> 'zz';
echo [1,2,3] <=> [7,8,9];


// PHP7.1
//function getMultiplier(int $multiplier): callable
//{
//    return function (array $arr) use ($multiplier): ?int {
//        return $multiplier * array_sum($arr);
//    };
//}
//echo getMultiplier(4)([1,2,3]);


// PHP7.1
//try {
//    $a = 1;
//} catch (Exception | Exception $e2) {
//}


class Ex1 extends Exception {};
class Ex2 extends Ex1 {};
try {
    throw new Ex1('abc');
} catch (Ex2 $ex2) {
    echo $ex2->getMessage();
} catch (Ex1 $e) {
    echo get_class($e);
}


$anonymousClass = new class ('Dzung')
    {
        protected $a;

        function __construct($a)
        {
            $this->a = $a;
        }

    function log($msg)
        {
            echo (sprintf('[Logger] ['.$this->a.'] %s', $msg));
        }
    };

$anonymousClass->log('Hello');


function getName () {
    return "ElNi\u{00F1}o";
}
echo getName();


// PHP7.1
//function increase($x): void {
//    $x++;
//    return $x;
//}
//$x = 0;
//echo increase($x);


$a = null;
echo $a ?? 0;
