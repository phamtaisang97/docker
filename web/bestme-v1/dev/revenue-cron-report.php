#!/usr/bin/env php
<?php
# This file would be say, '/usr/local/bin/run.php'

// code
/* setup */
// Version
define('VERSION', '3.0.2.0');

// Configuration
if (is_file(__DIR__ . '/../config.php')) {
    require_once(__DIR__ . '/../config.php');
}

/* connect */
// Create connection
$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
writeln("Connected successfully");

/* enable UTF8 for connection */
mysqli_query($conn, "SET NAMES 'utf8'");
mysqli_query($conn, "SET CHARACTER SET utf8");
mysqli_query($conn, "SET CHARACTER_SET_CONNECTION=utf8");
mysqli_query($conn, "SET SQL_MODE = ''");

//get time
$START_DATE = getStartDate($conn);
date_default_timezone_set('Asia/Ho_Chi_Minh');
$END_DATE = date('Y-m-d H:i:s');
$TYPE_REVENUE = 1;

//get total order

$total = getTotalOrder($conn, $START_DATE, $END_DATE);

//insert total
if ($total) {
    $data = [$END_DATE, date('Y-m-d', strtotime($END_DATE)), $TYPE_REVENUE, 'Doanh thu', '', $total, $END_DATE];
    $report = implode("', '", $data);
    $sql = "INSERT INTO " . DB_PREFIX . "report (`report_time`, `report_date`, `type`, `type_description`, `d_field`, `m_value`, `last_updated`)
    VALUES ('" . $report . "')";

    if ($conn->query($sql) === TRUE) {
        writeln("New " . 1 . " records created successfully");
    } else {
        writeln("Error: " . $conn->error);
    }
}

/* close connection */
$conn->close();

/* = = = = = = = = = = = = = = = = */

function writeln($message)
{
    echo sprintf("[%s] %s%s", (new DateTime())->format('YmdHis'), $message, PHP_EOL);
}

function getStartDate($conn) {
    $sql = "SELECT report_time FROM ".DB_PREFIX."report where type = 1 order by report_time DESC LIMIT 1";
    $query = $conn->query($sql);
    if ($query->num_rows > 0) {
        while ($row = $query->fetch_assoc()) {
            return $row["report_time"];
        }
    } else {
        return '';
    }
}

function getTotalOrder($conn, $START_DATE, $END_DATE) {
    if ($START_DATE) {
        $sql = "SELECT SUM(total) as total FROM ".DB_PREFIX."order WHERE date_added BETWEEN '".date('Y-m-d H:i:s', strtotime($START_DATE))."' AND '".date('Y-m-d H:i:s', strtotime($END_DATE))."'";
        $query = $conn->query($sql);
        if ($query->num_rows > 0) {
            while ($row = $query->fetch_assoc()) {
                return $row['total'];
            }
        } else {
            return 0;
        }
    } else {
        $sql = "SELECT SUM(total) as total FROM ".DB_PREFIX."order WHERE date_added < '".date('Y-m-d H:i:s', strtotime($END_DATE))."'";
        $query = $conn->query($sql);
        if ($query->num_rows > 0) {
            while ($row = $query->fetch_assoc()) {
                return $row['total'];
            }
        } else {
            return 0;
        }
    }
}

