# Chatbot integration

### Document for integration with chatbot - Belief. Powered by Bestme - v2.0

I. Summary

- product management:
    - create product
    - edit product
    - delete product
    - get products list
    - get product detail

- order management:
    - create order
    - edit order
    - get orders list
    - get order detail

- utils:
    - verify api key

- administrative management:
    - get provinces list
    - get districts list by province
    - get wards list by districts

- delivery management:
    - get Delivery List
    - get Delivery Fee

- transportation management:
    - get shipping service list
    - create delivery order
    - cancel delivery order

- callback management:
    - callback change order to Chatbot

- staff management:
    - create staff
    - edit staff (NA)
    - get staffs list
    - get staff detail
    - delete staff (NA)

- category management:
    - get categories list

- manufacturer management:
    - get manufacturers list

- customers management:
    - get customers list
    - create customer

- payment methods management:
    - get payment methods list
    
- user group management:
    - get user group list
    
- dashboard management:
    - get dashboard info

II. Detail

**\* Notice:**

- assume shop base url is "https://myshop.bestme.asia", then below will use sub path only

- all requests MUST use "token" for security purpose. "token" is a string value, calculated by following way:

    - if method is GET: token = md5(param-1 . param-2 . API_KEY)

      e.g: path "/api/v1/products?page=1&limit=15&searchKey=ao%20nam&searchField=name", API_KEY=1234567890ABCDE

      => token = md5(115ao%20namname1234567890ABCDE)

    - if method is POST: token = md5(body-data . API_KEY). Notice: body-data WITHOUT "token"

    - how to validate request authorization:

        - step 1: re-calculate token by Bestme

          /api/v1/products?page=3&limit=15&searchKey=ao%20nam&searchField=name&token=ASDFGHJKL1234567890

          => calculated token = md5(115ao%20namname1234567890ABCDE) (not include "token" in checksum!)

        - step 2: compare calculated token with requested token (sent from chat bot)

            - if match: valid => continue processing

            - if not matched: invalid => return error code

- API_KEY: the secure key is store in both Shop (owed by Bestme) and Chat Bot, used to calculate token for each request

1. get products list

    - method: GET

    - url:
      /api/chatbot_novaon_v2/products?page=3&limit=15&searchKey=ao%20nam&searchField=name&except_product_ids=12,23_1,23_3&is_all=0&ver=1.1&token=ASDFGHJKL1234567890

    - request:

        - header: "content-type": "application/json"

        - where:

            - page: page number. If absent or empty value ('') => not use, then param "limit" will be not used too

            - limit: the maximum records will be returned for that request. If absent or empty value ('') => default 15

            - searchKey: the search value, escaped. e.g "ao nam" => "ao+nam"

            - searchField: the field to search, supported: "name|status"
              
            - quantity: (ONLY FOR VER 1.1), quantity search field, format is {operator_text}_{quantity_value}. eg lt_2, eq_3, gt_4
              
            - category: (ONLY FOR VER 1.1), category search field
              
            - manufacturer: (ONLY FOR VER 1.1), manufacturer search field

            - status: (ONLY FOR VER 1.1), filter getting all products activeStatus = 1|0

            - product_ids: the product ids (and may be product version ids) to be included, multiple values split
              by comma (,). e.g 12,13_1,13_3,... where:

                - 12: product id
                - 13_1, 13_3: product id 13 and it's version id 1, 3

            - except_product_ids: the product ids (and may be product version ids) to be excepted, multiple values split
              by comma (,). e.g 12,13_1,13_3,... where:

                - 12: product id
                - 13_1, 13_3: product id 13 and it's version id 1, 3

            - is_all: filter getting all products with 1=out-of-stock or 0=in-stock, default empty or 0 for in-stock
              only

            - ver: api version. support: 1.0|1.1, or let empty for default "1.0"
                - 1.0: flat product versions
                - 1.1: group product versions

    - response:

        - type: application/json

        - value: json for products list. example format:

          ```
          {
               "total": "62",
               "page": "1",
               "records": [
                   {
                       "id": "1107",
                       "name": "sp co ban",
                       "original_price": "50000.0000",
                       "sale_price": "34000.0000",
                       "image": "http://127.0.0.1:8000/images/x2/apple_cinema_30-500x500_MCTR3aQ.jpg",
                       "url": "http://x2.bestme.test/sp-co-ban",
                       "quantity": -1,
                       "sku": "",
                       "in_stock": 1,
                       "sale_on_out_of_stock": "1",
                       "version_quantity": 0,
                       "version": [],
                       "nameCategory": "",
                       "manufacturer": "",
                       "activeStatus": "1",
                       "user_name": "Nguyễn  Văn Admin",
                       "product_attributes": []
                   },
                   {
                       "id": "671",
                       "name": "Dép sandal xoắn cổ chân",
                       "original_price": "390000.0000",
                       "sale_price": "230000.0000",
                       "image": "/catalog/view/theme/default/image/fashion_shoeszone/products/98/thumb.jpg",
                       "url": "http://x2.bestme.test/dep-sandal-xoan-co-chan",
                       "quantity": 1,
                       "sku": "SKU215xzxz",
                       "in_stock": 1,
                       "sale_on_out_of_stock": "0",
                       "version_quantity": 0,
                       "version": [],
                       "nameCategory": "NHÓM SẢN PHẨM FIGHTING CORONA - PHÒNG DỊCH CORONA",
                       "manufacturer": "",
                       "activeStatus": "1",
                       "user_name": "Nguyễn  Văn Admin",
                       "product_attributes": []
                   },
                   {
                       "id": "1104",
                       "name": "Bàn phím B",
                       "original_price": "0.0000",
                       "sale_price": "0.0000",
                       "image": "http://127.0.0.1:8000/images/x2/slideshow1_ExmkVZI.png",
                       "url": "http://x2.bestme.test/ban-phim-b",
                       "quantity": 147,
                       "sku": "KB1131",
                       "in_stock": 1,
                       "sale_on_out_of_stock": "0",
                       "version_quantity": "6",
                       "version": [
                           {
                               "version": "Đen • S",
                               "price": 135000,
                               "compare_price": 150000,
                               "sku": "",
                               "barcode": "",
                               "quantity": "30",
                               "status": "1",
                               "product_version_id": "1022",
                               "image": "http://127.0.0.1:8000/images/x2/car_parts_auto.png",
                               "image_alt": "",
                               "version_for_store": "_en_S_ce9afa6814f0e963cffdc7d8c9f936f4"
                           },
                           {
                               "version": "Đen • L",
                               "price": 135000,
                               "compare_price": 150000,
                               "sku": "",
                               "barcode": "",
                               "quantity": "30",
                               "status": "1",
                               "product_version_id": "1024",
                               "image": "",
                               "image_alt": "",
                               "version_for_store": "_en_L_12cedc5afb2762fa31505a495640be21"
                           },
                           {
                               "version": "Đỏ • M",
                               "price": 135000,
                               "compare_price": 150000,
                               "sku": "",
                               "barcode": "",
                               "quantity": "28",
                               "status": "1",
                               "product_version_id": "1026",
                               "image": "http://127.0.0.1:8000/images/x2/apple_cinema_30-500x500_MCTR3aQ.jpg",
                               "image_alt": "",
                               "version_for_store": "_M_bd2e9f7fa0fc81b8c9f39b48a73ed864"
                           },
                           {
                               "version": "Đỏ • S",
                               "price": 135000,
                               "compare_price": 150000,
                               "sku": "",
                               "barcode": "",
                               "quantity": "29",
                               "status": "1",
                               "product_version_id": "1025",
                               "image": "",
                               "image_alt": "",
                               "version_for_store": "_S_f98d15273a2634ce63ffdf71a644bc79"
                           },
                           {
                               "version": "Đỏ • L",
                               "price": 135000,
                               "compare_price": 150000,
                               "sku": "",
                               "barcode": "",
                               "quantity": "30",
                               "status": "1",
                               "product_version_id": "1027",
                               "image": "",
                               "image_alt": "",
                               "version_for_store": "_L_20e9bcd5c0f382f1cfd1c354b9aad42d"
                           },
                           {
                               "version": "Đen • M",
                               "price": 135000,
                               "compare_price": 150000,
                               "sku": "",
                               "barcode": "",
                               "quantity": "30",
                               "status": "1",
                               "product_version_id": "1023",
                               "image": "http://127.0.0.1:8000/images/x2/canon_eos_5d_1-500x500.jpg",
                               "image_alt": "",
                               "version_for_store": "_en_M_e4b633957ea43ad3f63d69c9893398d2"
                           }
                       ],
                       "nameCategory": "Đồng hồ,Áo nữ,Áo thun,Áo cộc tay",
                       "manufacturer": "Zorka",
                       "activeStatus": "1",
                       "user_name": "Nguyễn  Văn Admin",
                       "product_attributes": [
                          {
                              "attribute_id": "808",
                              "name": "Màu sắc",
                              "detail": [
                                  "Đen",
                                  "Đỏ"
                              ]
                          },
                          {
                              "attribute_id": "809",
                              "name": "Kích cỡ",
                              "detail": [
                                  "S",
                                  "M",
                                  "L"
                              ]
                          }
                      ]
                   }
               ]
          }
          ```

    - where:

        - total: total products (and match the search if provided)

        - page: current page. If empty ('') => no page used, return all products in records

        - records: contains products list (with summary info). If count of records < limit: reach the end of products
          list
          
          where:

            - quantity of product in default store

            - default_store: setting in product form at Bestme admin


1. get product detail

    - method: GET

    - url: /api/chatbot_novaon_v2/products?id=1&product_version_id=2&token=ASDFGHJKL1234567890

    - request:

        - header: "content-type": "application/json"

        - where:
            - id: the product id to get detail

            - product_version_id: the id related to specific version of product

    - response:

        - type: application/json

        - value: json for products list. example format:
            - if get whole product:

              ```
              {
                   "id": "1104",
                   "name": "Bàn phím B",
                   "original_price": "150000.0000",
                   "sale_price": "135000.0000",
                   "image": "http:\/\/127.0.0.1:8000\/images\/x2\/slideshow1_ExmkVZI.png",
                   "url": "http:\/\/x2.bestme.test\/ban-phim-b",
                   "version": "Đen • S",
                   "quantity": "30",
                   "weight": "100.00000000",
                   "nameCategory": "Đồng hồ,Áo nữ,Áo thun,Áo cộc tay",
                   "manufacturer": "Zorka",
                   "activeStatus": "1",
                   "product_attributes": [
                       {
                           "attribute_id": "808",
                           "name": "Màu sắc",
                           "detail": [
                               "Đen",
                               "Đỏ"
                           ]
                       },
                       {
                           "attribute_id": "809",
                           "name": "Kích cỡ",
                           "detail": [
                               "S",
                               "M",
                               "L"
                           ]
                       }
                   ]
              }
              ```
    
            - if get only one attribute:
                ```
                {
                   "id": "1104",
                   "name": "Bàn phím B",
                   "original_price": "150000.0000",
                   "sale_price": "135000.0000",
                   "image": "http:\/\/127.0.0.1:8000\/images\/x2\/slideshow1_ExmkVZI.png",
                   "url": "http:\/\/x2.bestme.test\/ban-phim-b",
                   "version": "Đen • S",
                   "product_version_id": "1022",
                   "quantity": "30",
                   "weight": "100.00000000",
                   "nameCategory": "Đồng hồ,Áo nữ,Áo thun,Áo cộc tay",
                   "manufacturer": "Zorka",
                   "activeStatus": "1"
                }
                ```


1. create product

   see edit product bellow: remove product_id in body data

1. edit product

    - method: POST

    - url: /api/chatbot_novaon_v2/products

    - request:

        - header: "content-type": "application/json"

    - body data:

        - if single product:

        ```
          {
            "name": "san pham 1 pb 1150",
            "description": "&lt;p&gt;mo ta&lt;\/p&gt;",
            "summary": "&lt;p&gt;mo ta ngan&lt;\/p&gt;",
            "product_version": "0",
            "price": "50,000",
            "promotion_price": "36,999",
            "weight": "200",
            "sku": "sp1pb 1150",
            "barcode": "",
            "stores_default_id": "0",
            "sale_on_out_of_stock": "1",
            "common_compare_price": "",
            "common_price": "",
            "common_weight": "0",
            "common_sku": "",
            "common_barcode": "",
            "stores_default_id_mul": "0",
            "common_cost_price": "0",
            "common_sale_on_out_of_stock": "1",
            "attribute_name": [""],
            "stores":
            {
                "0": ["0"],
                "1000": ["2"]
            },
            "original_inventory":
            {
                "0": ["4"],
                "1000": ["2"]
            },
            "cost_price":
            {
                "0": ["5,000"],
                "1000": ["4,000"]
            },
            "seo_title": "",
            "seo_desciption": "",
            "alias": "",
            "meta_keyword": "",
            "images": ["http:\/\/127.0.0.1:8000\/images\/x2\/apple_cinema_30-500x500_MCTR3aQ.jpg"],
            "image-alts": [""],
            "status": "1",
            "category": ["cat 224", "cat 223"],
            "manufacturer": "manuface f",
            "collection_list": ["collection 42"],
            "tag_list": ["tag 46"],
            "channel": "0",
            "created_user_id": "4"
        }
      ```

        - if multi versions product:

      ```
      {
        "name": "san pham 1109 pb",
        "description": "<p>mo ta<\/p>",
        "summary": "<p>mo ta ngan<\/p>",
        "product_version": "1",
        "price": "",
        "promotion_price": "",
        "weight": "1,000",
        "sku": "spnpb 1109",
        "barcode": "SP1pb",
        "stores_default_id": "0",
        "sale_on_out_of_stock": "1",
        "common_compare_price": "50,000",
        "common_price": "49,000",
        "common_weight": "1,000",
        "common_sku": "SPNPB 1109",
        "common_barcode": "WTIOW",
        "stores_default_id_mul": "0",
        "common_cost_price": "50,000",
        "common_sale_on_out_of_stock": "1",
        "attribute_name": [
            "m\u00e0u",
            "cao"
        ],
        "attribute_values": [
            [
                "X",
                "D"
            ],
            [
                "thap",
                "ca"
            ]
        ],
        "product_display": [
            "D \u2022 thap",
            "X \u2022 thap",
            "D \u2022 ca",
            "X \u2022 ca"
        ],
        "product_version_names": [
            "D \u2022 thap",
            "X \u2022 thap",
            "D \u2022 ca",
            "X \u2022 ca"
        ],
        "product_prices": [
            "50,000",
            "50,000",
            "50,000",
            "50,000"
        ],
        "product_promotion_prices": [
            "49,000",
            "49,000",
            "49,000",
            "49,000"
        ],
        "product_quantities": [
            "0",
            "0",
            "0",
            "0"
        ],
        "product_skus": [
            "SPNPB1 1109",
            "SPNPB2 1109",
            "SPNPB3 1109",
            "SPNPB4 1109"
        ],
        "product_barcodes": [
            "WTIOW",
            "WTIOW",
            "WTIOW",
            "WTIOW"
        ],
        "stores":
        [
            [
                "0",
                "2"
            ],
            [
                "0"
            ],
            [
                "0"
            ],
            [
                "0"
            ]
        ],
        "original_inventory":
        [
            [
                "10",
                "20"
            ],
            [
                "0"
            ],
            [
                "0"
            ],
            [
                "0"
            ]
        ],
        "cost_price":
        [
            [
                "50,000",
                "45,000"
            ],
            [
                "50,000"
            ],
            [
                "50,000"
            ],
            [
                "50,000"
            ]
        ],
        "seo_title": "",
        "seo_description": "",
        "alias": "",
        "meta_keyword": "",
        "images": [
            "http:\/\/127.0.0.1:8000\/images\/x2\/apple_cinema_30-500x500_MCTR3aQ.jpg"
        ],
        "image-alts": [
            ""
        ],
        "product_version_images": [
            "",
            "",
            "",
            ""
        ],
        "product_version_image_alts": [
            "",
            "",
            "",
            ""
        ],
        "status": "1",
        "category": [
            "lsp a",
            "lsp b"
        ],
        "manufacturer": "32",
        "collection_list": [
            "bst 1",
            "bst 2"
        ],
        "tag_list": [
            "tag 1",
            "tag n"
        ],
        "channel": "0",
        "created_user_id": "4"
      }
      ```

        - where (* is required):

            - product_id*: (required on editing ONLY) product id. Let empty for creating product, or existing product id
              for editing
            - name*: product name, max length 255
            - description*: product description, max length 30.000, should be encoded html. E.g "&lt;p&gt;test social 1
              - desc&lt;\/p&gt;" known as "test-social 1 - desc"
            - summary*: product description, max length 500, should be encoded html. E.g "&lt;p&gt;test social 1 -
              desc&lt;\/p&gt;" known as "test-social 1 - desc"
            - product_version*: =0 if single product, =1 if multi versions
            - price*: original price (vnd), max 99,999,999,999
            - promotion_price*: sale price (vnd), max 99,999,999,999
            - weight*: weight (g), max 9,999,999
            - sku: SKU
            - barcode: barcode
            - stores_default_id*: default store, used for decrease product quantity when bought from website
            - sale_on_out_of_stock*: allow sale on out of stock or not, =0: not allow, =1: allow

            - attribute_name*: (MULTI VERSIONS ONLY) array of attribute names. each name max length 30
            - attribute_values*: (MULTI VERSIONS ONLY) array of attribute names. each value max length 30, total max 100
              values
            - product_version_names*: (MULTI VERSIONS ONLY) array of version names. create from attribute_values
            - product_display*: (MULTI VERSIONS ONLY) array of versions have display attribute is true.
            - product_version_ids*: (MULTI VERSIONS ONLY) array of version id, keep value in same order with attribute
              name and value
            - product_prices*: (MULTI VERSIONS ONLY) array of original price (vnd), max 99,999,999,999, keep value in
              same order with attribute name and value
            - product_promotion_prices*: (MULTI VERSIONS ONLY) array of sale price (vnd), max 99,999,999,999, keep value
              in same order with attribute name and value
            - product_quantities*: (MULTI VERSIONS ONLY) array of quantity, max length 9,999,999, keep value in same
              order with attribute name and value
            - product_skus*: (MULTI VERSIONS ONLY) array of sku, keep value in same order with attribute name and value
            - product_barcodes*: (MULTI VERSIONS ONLY) array of barcode, keep value in same order with attribute name
              and value
              
            - common_compare_price*: product original price
            - common_price*: product promotion price
            - common_weight*: product weight
            - common_sku*: product sku
            - common_cost_price*: product cost price
              
            - stores*: array of store, keep value in same order with attribute name
              and value
            - original_inventory*: array of stores inventory number, max 9,999,999
            - cost_price*: array of store cost price, max 9,999,999
            - seo_title: seo title
            - seo_description: seo description
            - alias: seo alias
            - meta_keyword: seo meta keywords, string separated by comma
            - images*: array of image url, first is main image, each url max length 255
            - image-alts*: array of image alt, associated to images above, each alt max length 255
            - product_version_images*: array of product version image url, each url max length 255
            - product_version_image_alts*: array of product version image alt, associated to product version images above, each alt max length 255
            - status*: product status, =0: disabled, =1: enabled
            - category*: array of category names, CASE INSENSITIVE. For each name, auto create category if name not
              existing
            - manufacturer*: manufacturer names, CASE INSENSITIVE. Auto create manufacturer if name not existing
            - collection_list*: array of collection titles, CASE INSENSITIVE. For each title, auto create collection if title not
              existing
            - tag_list*: array of tag, CASE INSENSITIVE. For each tag, auto create tag if tag not
              existing
            - channel*: channel sale product. =0: website, =1: POS, =2: both website + POS
            - created_user_id*: id of current logged in user

    - response:

        - type: application/json

        - value: json for create/edit order result. example format:

          ```
              {
                  "code": 0,
                  "message": "product is created successfully",
                  "product_id": '13265'
              }
          ```

          if edit:

          ```
              {
                  "code": 0,
                  "message": "product is edited successfully",
              }
          ```

        - where:

            - response "code":

                - 200: product is created successfully

                - 1: credential (api key) is verified
    
                - 400: bad request

                - 500: internal server error
        - Notice:

            - ... collection, tag ... will be added next version

1. delete product

    - method: POST

    - url: /api/chatbot_novaon_v2/products

    - request:

        - header: "content-type": "application/json"

    - body data:

      ```body
      {
          "token": "ASDFGHJKL1234567890"
          "data": {
              "product_id": 1246,
              "action": "delete"
          }
      }
      ```

        - where:

            - required: all keys in above sample data

            - action: current supported "delete"

    - response:

        - type: application/json

        - value: json for delete product result. example format:

          ```
              {
                  "code": 0,
                  "message": "product is deleted successfully",
              }
          ```

        - where:

            - response "code":

                - 0: product is deleted successfully

                - 506: product (with product_id) does not existed

                - 201: unknown error

                - 400: token invalid

1. create order

   see edit order bellow

1. edit order

    - method: POST

    - url: /api/chatbot_novaon_v2/orders

    - request:

        - header: "content-type": "application/json"
      
      - body data: 
      
        ```body
        {
            "token": "ASDFGHJKL1234567890",
            "data": {
                "customer": {
                    "name": "Nguyen Thuy Linh",
                    "phone_number": "0987654321",
                    "email": "nguyenthuylinh@gmail.com",
                    "delivery_addr": "So 1 Ngo 2 Duy Tan",
                    "ward": "30280",
                    "district": "883",
                    "city": "89",
                    "note": "Goi hang can than va du so luong nhe shop. Thanks!",
                    "subscriber_id" : ""
                },
                "status": 6,
                "customer_ref_id": "",
                "order_id": 213,
                "tags": "tag1, tag2",
                "utm": [
                    {
                        "key": "campaign",
                        "value": "Campaign 1"
                    },
                    {
                        "key": "campaign",
                        "value": "Campaign 2"
                    }
                ],
                "products": [
                    {
                        "id": 1,
                        "quantity": 2,
                        "product_version_id": 0
                    },
                    {
                        "id": 2,
                        "quantity": 3,
                        "product_version_id": 2
                    }
                ],
                "voucher_product": {
                    "voucher_code": "ABCDEFGH99",
                    "voucher_type": 1,
                    "amount": 20000,
                    "products": [
                        {
                            "id": 1,
                            "product_version_id": 0
                        },
                        {
                            "id": 2,
                            "product_version_id": 2
                        }
                    ],
                    "start_at": "2021-04-22 13:41:00",
                    "end_at": "2021-04-22 13:41:59",
                    "user_create_id": "10"
                },
                "campaign_id": "manual",
                "channel_id": "",
                "payment_trans": "-2",
                "payment_trans_custom_name": "Shop tự vận chuyển",
                "payment_trans_custom_fee": 25000,
                "payment_status": 1,
                "payment_method": "198535319739A"
            }
        }
        ```
        
        - where:

            - status: 6=draft, 7=processing, 8=delivering, 9=complete, 10=canceled

            - customer_ref_id: optional, for Chatbot marking reference between Chatbot's customer and Bestme's order

            - order_id: int, null or empty for creating order, or valid value for editing order

            - product_version_id: is version_id or 0

            - tags: if EDIT, MUST include old tags (get from order detail). Give tags empty or absent for REMOVING ALL
              tags!

            - payment_trans: delivery code
                - -2: custom fee, then 2 need additional 2 keys: payment_trans_custom_name (string),
                  payment_trans_custom_fee (int)
                - -3: free (fee = 0)
                - not -2 and -3: delivery id (got from delivery list api). e.g: -1, 1, 2, 3, ...

            - payment_status: payment status (paid or not yet paid)
                - 1: paid
                - other value, empty or absent: not yet paid

            - voucher_product: list voucher in order (can empty)
              - voucher_type: 1 is percent, 2 is amount
              - amount: discount amount for each product belonging to the code 
              - products: list product apply voucher
              - user_create_id: id of staff or admin create campaigns
            - campaign_id: not empty default is manual
            - subscriber_id: can empty
            - channel_id: can empty
            - payment_method: can empty
      
      - response:
      
        - type: application/json

        - value: json for create/edit order result. example format:

          ```
              {
                  "code": 0,
                  "message": "order is created successfully",
                  "order_id": '13265',
                  "order_code": 'OD13265'
              }
          ```

          if edit:

          ```
              {
                  "code": 0,
                  "message": "order is edited successfully",
              }
          ```

        - where:

            - response "code":

                - 0: order is created successfully

                - 1: credential (api key) is verified

                - 100: missing customer

                - 101: customer info invalid (email, name, phone number, ...)

                - 102: products info invalid (id, quantity, ...)

                - 103: missing products

                - 104: products are out of stock or do not existed

                - 1041: products are out of stock

                - 1042: products do not existed

                - 201: unknown error

                - 400: token invalid

        - Notice:

            - Order status is "Draft"

            - if code=104, return additional products info like info from order, with detail code 1041,1042... e.g

              ```
                  {
                      "code": 104,
                      "message": "create order failed: products are out of stock or do not existed",
                      "products": [
                          {
                              "id": 1,
                              "name": "Ao phong nam H2T",
                              "description": "Ao phong nam H2T chat lieu dep",
                              "short_description": "Ao phong nam H2T chat lieu dep",
                              "original_price": 200000,
                              "sale_price": 195000,
                              "image": "https://novaon-cloud.vn/img/1/12345678-1.png",
                              "error_code": 1041
                          },
                          {
                              "id": 2,
                              "name": "Ao phong nam H2T",
                              "description": "Ao phong nam H2T chat lieu dep",
                              "short_description": "Ao phong nam H2T chat lieu dep",
                              "original_price": 200000,
                              "sale_price": 195000,
                              "image": "https://novaon-cloud.vn/img/1/12345678-1.png",
                              "error_code": 1042
                          },
                          ...
                      ]
                  }
              ```

1. update order status
    
    - method: POST
    
    - url: /api/chatbot_novaon_v2/orders
    
        - request:
    
            - header: "content-type": "application/json"
          
            - body data: 
              
                ```body
                {
                    "token": "ASDFGHJKL1234567890",
                    "data": {
                        "order_id" : 1,
                        "order_status_id": 8
                    }
                }
                ```
            - where:
          
                - order_id: not empty
            
                - order_status_id: not empty and satisfies the order status that is allowed to update, current status is 8 not update to 6..

            - response:
            
                - type: application/json
            
                - value: json for update status result. example format:
        
                  ```
                      {
                          "code": 200,
                          "message": "Update status success",
                      }
                  ```
        
                - where:
        
                    - response "code":
        
                        - 200: Update status success
                        
                        - 404: Order not found
        
                        - 400: Bad request
                        
                        - 403: Status invalid
            
1. get orders list

    - method: GET

    - url: /api/chatbot_novaon_v2/orders?page=1&limit=15&order_ids=&order_status=6&order_payment=0&order_total_type=lt_1000000%2C+gt_300000&order_code=&order_campaign=8s41vdft78wert1&order_channel=1909543419365215&order_product=&order_customer_ids=60&order_promotion_code=ABCDEFGH99,ABCDEFG998&token=45b599b200b9759d6a36913182913fa4

    - request:

        - header: "content-type": "application/json"

        - where:

            - page: page number. If absent or empty value ('') => not use, then param "limit" will be not used too

            - limit: the maximum records will be returned for that request. If absent or empty value ('') => default 15

            - order_ids: the orders ids to be returned, multiple values split by comma (,). e.g 213,216,...
            
            - order_status: the orders status to be returned, multiple values split by comma (,). e.g 6,7,8,...
            
            - order_payment: the orders payment_status to be returned, multiple values split by comma (,). e.g 0,1,2
                - 0: Unpaid
                
                - 1: Already paid
                
                - 2: Refunded
            
            - order_total_type: filter order total
            
            - order_code: enter code
            
            - order_campaign: the orders campaign to be returned, multiple values split by comma (,). e.g 8s41vdft78wert1,...
            
            - order_channel: the orders channel to be returned, multiple values split by comma (,). e.g 1909543419365215,...

            - order_product: enter product name
            
            - order_customer_ids: the customer_ids to be returned, multiple values split by comma (,). e.g 1,2,3,...
            
            - order_promotion_code: the order_promotion_code to be returned, multiple values split by comma (,). e.g ABCDEFGH99,ABCDEFG998,...
            
            - order_subscriber_ids: the order_subscriber_ids to be returned, multiple values split by comma (,). e.g dftcfb57fgvi24545,tpbvfb57fgvi85486,...
            
    - response:

        - type: application/json

        - value: json. empty if not found. order by "order_id" DESC. example format:

          ```
          {
              "total": 120,
              "page": 3,
              "total_revenue": 820000,
              "records": [
                  {
                      "customer": {
                          "name": "Nguyen Thuy Linh",
                          "phone_number": "0987654321",
                          "email": "nguyenthuylinh@gmail.com",
                          "delivery_addr": "So 1 Ngo 2 Duy Tan",
                          "full_delivery_addr": "So 1 Ngo 2 Duy Tan, Phuong Dich Vong, Quan Cau Giay, Ha Noi",
                          "ward": "30280",
                          "district": "883",
                          "city": "89",
                          "note": "Goi hang can than va du so luong nhe shop. Thanks!",
                          "customer_id": "30",
                          "subscriber_id": "dftcfb57fgvi24545",
                          "customer_group_id": "1"
                      },
                      "status": 6,
                      "customer_ref_id": "1234",
                      "order_id": 213,
                      "order_code": "#00000000015",
                      "tags": ["tag1,tag2"],
                      "total": 280000,
                      "date_added": "2020-02-29 15:22:09",
                      "date_modified": "2020-03-05 08:39:04",
                      "products": [
                          {
                              "id": 1,
                              "name": "Ao phong nam H2T",
                              "description": "Ao phong nam H2T chat lieu dep",
                              "short_description": "Ao phong nam H2T chat lieu dep",
                              "original_price": 200000,
                              "sale_price": 195000,
                              "image": "https://novaon-cloud.vn/img/1/12345678-1.png",
                              "url": "https://myshop.com/ao-phong-nam-h2t",
                              "quantity": 2,
                              "inventory": 10,
                              "discount": "2000.0000"
                          },
                          {
                              "id": 2,
                              "name": "Ao phong nam H2T",
                              "description": "Ao phong nam H2T chat lieu dep",
                              "short_description": "Ao phong nam H2T chat lieu dep",
                              "original_price": 200000,
                              "sale_price": 195000,
                              "image": "https://novaon-cloud.vn/img/1/12345678-1.png",
                              "url": "https://myshop.com/ao-phong-nam-h2t",
                              "quantity": 3,
                              "inventory": 30,
                              "discount": "2000.0000"
                          },
                          ...
                      ],
                      "payment_trans": "-2",
                      "payment_trans_custom_name": "Shop tự vận chuyển",
                      "payment_trans_custom_fee": 25000,
                      "payment_status": 1,
                      "delivery_fee": 25000,
                      "changable_order_statuses": [
                          {
                              "order_status_id": 9,
                              "order_status_description": "Hoàn thành"
                          },
                          {
                              "order_status_id": 10,
                              "order_status_description": "Đã huỷ"
                          }
                      ],
                      "from_district_id_ghn": "1312412",
                      "payment_method": "198535319739A",
                      "user_id": "1",
                      "order_source": "chatbot",
                      "transports": {
                          "customer_id": "60",
                          "firstname": "Linh",
                          "lastname": "Nguyen Thuy",
                          "fullname": "Nguyen Thuy Linh 333",
                          "email": "nguyenthuylinh@gmail.com",
                          "telephone": "0987654322",
                          "transport_order_code": "GAUNRHGX",
                          "transport_name": "Ghn",
                          "transport_service_name": "Đi bộ",
                          "transport_display_name": "GHN",
                          "transport_collection_amount": "485000.0000",
                          "transport_order_active": true,
                          "transport_order_detail": {
                              "PickWarehouseName": "Phố Duy Tân, Dịch Vọng Hậu, Cầu Giấy, Hà Nội, Việt Nam",
                              "OrderCode": "GAUNRHGX",
                              "CurrentStatus": "cancel",
                              "CurrentStatusLanguage": "Đơn hàng bị hủy",
                              "Cancelable": 0,
                              "CollectionAmount": "485000.0000"
                          }
                      },
                      "text_pick_address": "Số 6, Ngõ 82 dịch Vọng Hậu",
                      "campaign_id": "8s41vdft78wert1",
                      "channel_id": "1909543419365215"
                  },
                  ...
              ]
          }
          ```

            - where:

                - total: total orders (and match the search if provided)

                - page: current page. If empty ('') => no page used, return all orders in records

                - total_revenue: total revenue of orders due to current filter conditions (regardless pagination)

                - records: contains orders list (with summary info). If count of records < limit: reach the end of
                  orders list

                - status: 6=draft, 7=processing, 8=delivering, 9=complete, 10=cancel

                - payment_trans: delivery code
                    - -2: custom fee, then 2 need additional 2 keys: payment_trans_custom_name (string),
                      payment_trans_custom_fee (int)
                    - -3: free (fee = 0)
                    - not -2 and -3: delivery id (got from delivery list api). e.g: -1, 1, 2, 3, ...

                - payment_status:
                    - 1: paid
                    - 0: not yet paid

                - delivery_fee: delivery fee due to payment_trans above
                
                - changable_order_statuses: changable order status

1. get order detail

    - method: GET

    - url: /api/chatbot_novaon_v2/orders?id=123&token=ASDFGHJKL1234567890

    - request:

        - header: "content-type": "application/json"

    - response:

        - type: application/json

        - value: json. empty if not found. example format:

          ```
          {
              "customer": {
                 "name": "Nguyen Thuy Linh",
                 "phone_number": "0987654321",
                 "email": "nguyenthuylinh@gmail.com",
                 "delivery_addr": "So 1 Ngo 2 Duy Tan",
                 "full_delivery_addr": "So 1 Ngo 2 Duy Tan, Phuong Dich Vong, Quan Cau Giay, Ha Noi",
                 "ward": "30280",
                 "district": "883",
                 "city": "89",
                 "note": "Goi hang can than va du so luong nhe shop. Thanks!",
                 "customer_id": "30",
                 "subscriber_id": "dftcfb57fgvi24545",
                 "customer_group_id": "1"
              },
              "status": 6,
              "customer_ref_id": "",
              "order_id": 213,
              "order_code": "#00000000213",
              "tags": ["tag1,tag2"],
              "total": 280000,
              "date_added": "2020-02-29 15:22:09",
              "date_modified": "2020-03-05 08:39:04",
              "products": [
                  {
                      "id": 1,
                      "name": "Ao phong nam H2T",
                      "description": "Ao phong nam H2T chat lieu dep",
                      "short_description": "Ao phong nam H2T chat lieu dep",
                      "original_price": 200000,
                      "sale_price": 195000,
                      "image": "https://novaon-cloud.vn/img/1/12345678-1.png",
                      "url": "https://myshop.com/ao-phong-nam-h2t",
                      "quantity": 2,
                      "inventory": 10,
                      "discount": "2000.0000"
                  },
                  {
                      "id": 2,
                      "name": "Ao phong nam H2T",
                      "description": "Ao phong nam H2T chat lieu dep",
                      "short_description": "Ao phong nam H2T chat lieu dep",
                      "original_price": 200000,
                      "sale_price": 195000,
                      "image": "https://novaon-cloud.vn/img/1/12345678-1.png",
                      "url": "https://myshop.com/ao-phong-nam-h2t",
                      "quantity": 3,
                      "quantity": 30,
                      "discount": "2000.0000"
                  },
                  ...
              ],
              "payment_trans": "-3",
              "payment_trans_custom_name": "Shop tự vận chuyển",
              "payment_trans_custom_fee": 25000,
              "payment_status": 1,
              "delivery_fee": 25000,
              "changable_order_statuses": [
                  {
                      "order_status_id": 9,
                      "order_status_description": "Hoàn thành"
                  },
                  {
                      "order_status_id": 10,
                      "order_status_description": "Đã huỷ"
                  }
              ],
              "from_district_id_ghn": "1312412",
              "payment_method": "198535319739A",
              "user_id": "1",
              "order_source": "chatbot",
              "transports": {
                  "customer_id": "60",
                  "firstname": "Linh",
                  "lastname": "Nguyen Thuy",
                  "fullname": "Nguyen Thuy Linh 333",
                  "email": "nguyenthuylinh@gmail.com",
                  "telephone": "0987654322",
                  "transport_order_code": "GAUNRHGX",
                  "transport_name": "Ghn",
                  "transport_service_name": "Đi bộ",
                  "transport_display_name": "GHN",
                  "transport_collection_amount": "485000.0000",
                  "transport_order_active": true,
                  "transport_order_detail": {
                      "PickWarehouseName": "Phố Duy Tân, Dịch Vọng Hậu, Cầu Giấy, Hà Nội, Việt Nam",
                      "OrderCode": "GAUNRHGX",
                      "CurrentStatus": "cancel",
                      "CurrentStatusLanguage": "Đơn hàng bị hủy",
                      "Cancelable": 0,
                      "CollectionAmount": "485000.0000"
                  }
              },
              "text_pick_address": "Số 6, Ngõ 82 dịch Vọng Hậu",
              "campaign_id": "8s41vdft78wert1",
              "channel_id": "1909543419365215"
          }
          ```

1. verify api key

    - method: GET

    - url: /api/chatbot_novaon_v2/verify?data=data_for_verifying_api_key&token=ASDFGHJKL1234567890

    - request:

        - header: "content-type": "application/json"

    - response:

        - type: application/json

        - value: json. example format:

          ```response
              {
                  "code": 1,
                  "message": "credential (api key) is verified"
              }
          ]
          ```

          other error code: 400: token invalid, ...

        - where:

            - data: data for verifying api key. e.g: "this data for trust api key..."

8. get provinces list

    - method: GET

    - url: /api/chatbot_novaon_v2/provinces

    - request:

        - header: "content-type": "application/json"

    - response:

        - type: application/json

        - value: json for provinces list. example format:

          ```provinces list
          [
              {
                  "name": "An Giang",
                  "name_with_type": "Tỉnh An Giang",
                  "code": "89"
              },
              {
                  "name": "Kon Tum",
                  "name_with_type": "Tỉnh Kon Tum",
                  "code": "62"
              },
              ...
          ]
          ```
    - note:

        - Return full provinces json, no need pagination

9. get districts list by province

    - method: GET

    - url: /api/chatbot_novaon_v2/districts?province=PROVINCE_CODE

    - request:

        - header: "content-type": "application/json"

    - response:

        - type: application/json

        - value: json for districts list. example format:

          ```districts list
          [
              {
                  "name": "Long Xuyên",
                  "name_with_type": "Thành phố Long Xuyên",
                  "code": "883"
              },
              {
                  "name": "Châu Đốc",
                  "name_with_type": "Thành phố Châu Đốc",
                  "code": "884"
              },
              ...
          ]
          ```
    - where:

        - province: province code (PROVINCE_CODE). e.g: "89" for "An Giang"

    - note:

        - need province param in request to shorten districts list for result

10. get wards list by district

    - method: GET
    
    - url: /api/chatbot_novaon_v2/wards?district=DISTRICT_CODE
    
    - request:
    
        - header: "content-type": "application/json"
    
    - response:
    
        - type: application/json
    
        - value: json for products list. example format:
    
          ```wards list
          [
              {
                  "name": "Mỹ Bình",
                  "name_with_type": "Phường Mỹ Bình",
                  "code": "30280"
              },
              {
                  "name": "Mỹ Long",
                  "name_with_type": "Phường Mỹ Long",
                  "code": "30283"
              },
              ...
          ]
          ```
    - where:
    
        - district: province code (DISTRICT_CODE). e.g: "883" for "Long Xuyên"
    
    - note:
    
        - need district param in request to shorten wards list for result

1. get Delivery List

    - method: GET
    
    - url: /api/chatbot_novaon_v2/deliveries
    
    - request:
    
        - header: "content-type": "application/json"
    
    - response:
    
        - type: application/json
    
        - value: json for delivery list. example format:
    
          ```delivery list
          [
              {
                  "id": "-1",
                  "name": "Giao hàng tiêu chuẩn",
                  "status": "1",
                  "fee_region": "40000"
              },
              {
                  "id": 1,
                  "name": "Miễn phí vận chuyển Hà Nội",
                  "status": "1",
                  "fee_region": 0,
                  "fee_regions": [
                      {
                          "id": 1,
                          "name": "Mien phi HN",
                          "provinces": [
                              "Hà Nội"
                          ],
                          "steps": "weight",
                          "weight_steps": [
                              {
                                  "type": "FROM",
                                  "from": "0",
                                  "to": "2,000",
                                  "price": "0"
                              }
                          ],
                          "price_steps": [
                              {
                                  "type": "ABOVE",
                                  "from": "0",
                                  "to": "",
                                  "price": "0"
                              }
                          ]
                      }
                  ],
                  "fee_cod": {
                      "fixed": {
                          "status": "1",
                          "value": "0"
                      },
                      "dynamic": {
                          "status": "0",
                          "value": ""
                      }
                  }
              }
          ]
          ```
    - where:
    
        - fee_regions: config fee due to region (province)
    
        - provinces: list province in one fee_region config. Use "name" got from api /provinces
    
        - steps: weight=config fee due to weight step, price=config fee due to total amount step
    
        - weight_steps: used ONLY steps is "weight"
    
        - price_steps: used ONLY steps is "price"
    
        - fee_cod: fee for COD

12. get Delivery Fee

    - method: POST
    
    - url: /api/chatbot_novaon_v2/delivery_fee
    
    - request:
    
        - header: "content-type": "application/json"
    
    - body data:
    
      ```body
      {
           "products": [
               {
                   "id": 135,
                   "quantity":  3
               }
           ],
           "province": 89,
           "into_money": 200004,
           "delivery_id": ""
      }
      ```
    
        - where:
    
            - into_money: total order amount without delivery fee. e.g this may be the sum of product total amount and some
              additional shop fee...
    
            - delivery_id: special delivery id need to calculate only for one! default set empty for calculating all
              deliveries
    
    - response:
    
        - type: application/json
    
        - value: json for products list. example format:
    
          ```
          {
              "order_info": {
                  "products": [
                      {
                          "product_id": 135,
                          "quantity": 3,
                          "is_existing": false
                      }
                  ],
                  "amount": 3,
                  "into_money": 200004,
                  "into_money_format": "200.004đ",
                  "total_money": "200.004đ"
              },
              "delivery_fees": [
                  {
                      "delivery": {
                          "id": "-1",
                          "name": "Giao hàng tiêu chuẩn",
                          "delivery_method": {
                              "id": "-1",
                              "name": "Giao hàng tiêu chuẩn",
                              "status": "1",
                              "fee_region": "40000"
                          },
                          "fee_region": "40000",
                          "fee_weight": 0,
                          "fee_cod": 0,
                          "fee": "40000",
                          "error": "",
                          "fee_format": "40.000đ"
                      },
                      "fee": "40000",
                      "fee_format": "40.000đ"
                  }
              ],
              "error": ""
          }
          ```
    
        - where:
    
            - order_info: info of order, include product list with quantity (and is_existing = true|false), total money
    
            - delivery_fees: satisfied deliveries with fee
    
            - error: error if has, e.g Product not found, ...

13. get shipping service list

    - method: GET

    - url: /api/chatbot_novaon_v2/shipping_services?total_weight=100&package_length=0&package_width=0&package_height=0&from_district_id_ghn=1312412&total_pay=742719&transport_shipping_full_address=ca ugiay han oi, Xã Nậm Hăn, Huyện Sìn Hồ, Tỉnh Lai Châu&transport_shipping_wards=03547&transport_shipping_district=108&transport_shipping_province=12&token=3b3d5d1c124d66fe2a3e910818de9767

    - request:

        - header: "content-type": "application/json"

        - where:

            - total_weight: total weight of products in order, must be greater than 0, unit is gram

            - package_length: must be greater than 0, unit is centimeter

            - package_width: must be greater than 0, unit is centimeter

            - package_height: must be greater than 0, unit is centimeter

            - from_district_id_ghn: district id of shop that saved in Giao hàng nhanh

            - total_pay: value of the order

            - transport_shipping_full_address: full address of customer (delivery address)

            - transport_shipping_wards: ward code of customer address (delivery address)

            - transport_shipping_district: district code of customer address (delivery address)

            - transport_shipping_province: province code of customer address (delivery address)

    - response:

        - type: application/json

        - value: json for products list. example format:

       ```
       {
         "code": 200,
         "data": [
             {
                 "shipping_unit_name": "Giao hàng tiết kiệm",
                 "methods": [
                     {
                         "service_name": "fly",
                         "delivery_method": "trans_ons_ghtk",
                         "service_id": "1854",
                         "method_name": "Giao hàng tiết kiệm - fly",
                         "method_fee": "35,000đ"
                     },
                     {
                         "service_name": "road",
                         "delivery_method": "trans_ons_ghtk",
                         "service_id": "1854",
                         "method_name": "Giao hàng tiết kiệm - road",
                         "method_fee": "35,000đ"
                     }
                 ]
             },
             {
                 "shipping_unit_name": "viettel_post",
                 "methods": [
                     {
                         "service_name": "VCN Chuyển phát nhanh",
                         "delivery_method": "viettel_post",
                         "service_id": "VCN",
                         "method_name": "VCN Chuyển phát nhanh",
                         "method_fee": "0đ"
                     },
                     {
                         "service_name": "VTK Tiết kiệm",
                         "delivery_method": "viettel_post",
                         "service_id": "VTK",
                         "method_name": "VTK Tiết kiệm",
                         "method_fee": "0đ"
                     },
                     {
                         "service_name": "VHT Phát Hỏa tốc",
                         "delivery_method": "viettel_post",
                         "service_id": "VHT",
                         "method_name": "VHT Phát Hỏa tốc",
                         "method_fee": "0đ"
                     },
                     {
                         "service_name": "SCOD Giao hàng thu tiền",
                         "delivery_method": "viettel_post",
                         "service_id": "SCOD",
                         "method_name": "SCOD Giao hàng thu tiền",
                         "method_fee": "0đ"
                     }
                 ]
             },
             {
                 "shipping_unit_name": "ghn",
                 "methods": [
                     {
                         "service_name": "Đi bộ",
                         "delivery_method": "Ghn",
                         "service_id": 53321,
                         "method_name": "ghn - Đi bộ",
                         "method_fee": "47,000đ"
                     }
                 ]
             }
         ]
       }
       ```

    - where:

        - code: api response code

        - data: array of

        - error: error if has, e.g Product not found, ...

14. create delivery order

    - method: POST

    - url: /api/chatbot_novaon_v2/delivery_order

    - request

        - header: "content-type": "application/json"

    - body data:

      ```
      {
        "token": "f8818215898576b8e80eaa775fde96c3",
        "data": {
            "total_weight": "100",
            "shipping_fee": "0",
            "shipping_method": "Free",
            "total_pay": "742719",
            "order_source": "shop",
            "payment_method": "198535319739A",
            "payment_status": "1",
            "ghn_district_hub": "1312412",
            "shipping_district_code": "108",
            "order_id": "11",
            "transport_method": "Ghn",
            "service_id": "53321",
            "service_name": "\u0110i b\u1ed9",
            "transport_shipping_fullname": "thuong test 1",
            "transport_shipping_phone": "0386548862",
            "transport_shipping_address": "ca ugiay han oi",
            "transport_shipping_wards": "03547",
            "transport_shipping_district": "108",
            "transport_shipping_province": "12",
            "collection_amount": "742719",
            "package_length": "1",
            "package_width": "1",
            "package_height": "1",
            "transport_note": "bestme test ghn, ban dung giao hang nha",
            "order_status": "7",
            "old_order_status_id": "7",
            "user_id": "1",
            "customer": "3",
            "customer_group_id": "",
            "shipping_fullname": "thuong test 1",
            "shipping_phone": "0386548862",
            "shipping_address": "ca ugiay han oi",
            "shipping_wards": "03547",
            "shipping_district": "108",
            "shipping_province": "12",
            "shipping_bill": "#00000000011",
            "note": "",
            "delivery": "-1",
            "customer_full_name": "thuong test 1",
            "customer_phone": "0386548862",
            "customer_email": "thuong.test1@mail.com",
            "customer_province": "",
            "customer_address": ""
        }
      }
      ```

        - where (* is required):

            - total_weight*: total weight of all products in order package, unit is gram
            - shipping_fee*: bestme order shipping fee, must be greater than 0, unit is đ 
            - shipping_method*: bestme order shipping method
            - total_pay*: the value of the order, unit is đ
            - order_source*: source of the order, could be shop|website|pos|shopee|lazada ...
            - payment_method*: payment method of the order
            - payment_status*: payment status of the order
            - ghn_district_hub*: district hub id of ghn shop, only use for ghn delivery order
            - shipping_district_code*: district code of shipping address
            - order_id*: order id
            - transport_method*: transport method name, could be Ghn|viettel_post|trans_ons_ghtk
            - service_id*: delivery shipping service id
            - service_name*: delivery shipping service name
            - transport_shipping_fullname*: fullname of shipping address
            - transport_shipping_phone*: phone of shipping address
            - transport_shipping_address*: shipping address
            - transport_shipping_wards*: ward code of shipping address
            - transport_shipping_district*: district code of shipping address
            - transport_shipping_province*: province code of shipping address
            - collection_amount*: cash on delivery of order, unit is đ
            - package_length*: the length of the package, unit is centimeter
            - package_width*: the width of the package, unit is centimeter
            - package_height*: the height of the package, unit is centimeter
            - transport_note*: note of the order creator for the shipping service
            - order_status*: new status id after editing of the order
            - old_order_status_id*: current status id of the order
            - user_id*: staff id of order creator
            - customer*: id of customer
            - customer_group_id*: customer group id
            - shipping_fullname*: fullname of shipping address (like transport_shipping_fullname)
            - shipping_phone*: phone of shipping address (like transport_shipping_phone)
            - shipping_address*: shipping address (like transport_shipping_address)
            - shipping_wards*: ward code of shipping address (like transport_shipping_wards)
            - shipping_district*: district code of shipping address (like transport_shipping_district)
            - shipping_province*: province code of shipping address (like transport_shipping_province)
            - shipping_bill*: bestme order code
            - note*: note of the order
            - delivery*: shipping method value
            - customer_full_name*: fullname of the customer, use in case create new customer, default is empty string
            - customer_phone*: phone of the customer, use in case create new customer, default is empty string
            - customer_email*: email of the customer, use in case create new customer, default is empty string
            - customer_province*: province code of the customer, use in case create new customer, default is empty string
            - customer_address*: address of the customer, use in case create new customer, default is empty string
    
    - response:

        - type: application/json

        - value:
          if success:

          ```
              {
                "code": 200,
                "message": "Create order successfully",
                "return_data": {
                  "customer_id": "3",
                  "firstname": "test1",
                  "lastname": "thuong",
                  "fullname": "thuong test 1",
                  "email": "thuong.test1@mail.com",
                  "telephone": "02342423",
                  "transport_order_code": "14367610061",
                  "transport_name": "viettel_post",
                  "transport_service_name": "VCN Chuyển phát nhanh",
                  "transport_display_name": "Viettel Post",
                  "transport_collection_amount": "0",
                  "transport_order_active": true,
                  "transport_order_detail": {
                      "PickWarehouseName": "12 không biết tên, THỊ TRẤN TÂY ĐẰNG, HUYỆN BA VÌ, Hà Nội",
                      "OrderCode": "14367610061",
                      "CurrentStatus": -100,
                      "CurrentStatusLanguage": "Đơn hàng mới tạo, chưa duyệt",
                      "Cancelable": 1,
                      "CollectionAmount": "0"
                  }
                }
              }
          ```

          if error:

          ```
              {
                "code": 400,
                "message": "missing or invalid transport_method"
              }
          ```

        - where:

            - response "code":

                - 200: delivery order is created successfully

                - 400: bad request

                - 500: server error

15. cancel delivery order

    - method: PUT

    - url: /api/chatbot_novaon_v2/delivery_order

    - request

        - header: "content-type": "application/json"

    - body data:

      ```
      {
        "token": "a948759eb67cd4d41c3f7fdb4788fb49",
        "data": {
            "transport_method": "trans_ons_ghtk",
            "transport_order_code": "S14570506.MB23.A15.649554278",
            "data_order_id": 8
        }
      }
      ```

        - where (* is required):

            - transport_method*: transport method name, could be Ghn|viettel_post|trans_ons_ghtk
            - transport_order_code*: order code of shipping order
            - data_order_id*: bestme order id
    
    - response:

        - type: application/json

        - value:
          if success:

          ```
              {
                "code": 200,
                "message": "Cancel delivery order successfully"
              }
          ```

          if error:

          ```
              {
                "code": 400,
                "message": "missing or invalid transport_method"
              }
          ```

        - where:

            - response "code":

                - 200: delivery order is canceled successfully

                - 400: bad request

                - 500: server error


13. callback change order to Chatbot

    - Link test: \[POST] https://social-beta.novaonx.com/api/v1/orders/bestme-update

    - request data: same as api get order info

14. create staff

    - method: POST

    - url: /api/chatbot_novaon_v2/staffs

    - request:

        - header: "content-type": "application/json"

    - body data:

     ```body
     {
        "token": "72f3d3d6d6b3f9bc3dd6e8abff4fa4ca",
        "data": {
            "avatar": "",
            "lastname": "bv",
            "firstname": "thuong",
            "email": "thuong.staff3@gmail.com",
            "telephone": "0386548739",
            "info": "",
            "user_group": "11"
        }
    }
     ```

    - where:

        - avatar*: staff avatar
        - lastname*: staff lastname
        - firstname*: staff firstname
        - email*: staff email
        - telephone*: staff telephone
        - info*: staff info
        - user_group*: staff user_group (get from api list user groups)
    
    - type: application/json

    - value: json for create/edit staff result. example format:

      ```
          {
            "code": 200,
            "message": "Add successful staff!"
          }
      ```

    - where:

        - response "code":

            - 200: staff is created successfully

            - 400: bad request

            - 403: forbidden request

            - 500: internal server error

15. edit staff

    - method: POST

    - url: /api/chatbot_novaon_v2/staffs

    - request:

        - header: "content-type": "application/json"

    - body data:

     ```body
     {
         "token": "ASDFGHJKL1234567890",
         "data": {
             "info": {
                 "password": "12345678",
                 "email": "nguyenthuylinh@gmail.com"
             },
             "status": 1,
             "is_all_permissions": 1,
             "permission": [
                 "product_permission"
             ],
             "staff_id": 6
         }
     }
     ```

    - where:

        - required: all keys in above sample data. except:

            - staff_id if creating (required on editing ONLY)

            - password if no change (let empty for no change on editing)

        - email: ONLY for creating, no change on editing

        - staff_id: ONLY for editing, let absent when creating new

        - status: 1=active, 0=deactivated

        - is_all_permissions: 0=not all permissions, 1=all permissions. if 1, the key "permission" will be not used

        - permission: allowed permissions. Support
            - "home_permission": default, not need include this
            - "order_permission": Order permission, includes it's sub-menu permissions,
            - "product_permission": Product permission, includes it's sub-menu permissions,
            - "customer_permission": Customer permission, includes it's sub-menu permissions,
            - "setting_permission": Setting permission, includes it's sub-menu permissions,
            - "interface_permission": Interface permission, includes it's sub-menu permissions,
            - "menu_permission": Menu permission, includes it's sub-menu permissions,
            - "content_permission": Content permission, includes it's sub-menu permissions,
            - "domain_permission": Domain permission, includes it's sub-menu permissions, - response:

    - type: application/json

    - value: json for create/edit staff result. example format:

      ```
          {
              "code": 0,
              "message": "staff created successfully",
              "staff_id": 3
          }
      ```

      if edit:

      ```
          {
              "code": 0,
              "message": "staff edited successfully",
          }
      ```

    - where:

        - response "code":

            - 0: staff is created successfully

            - 500: missing info

            - 501: staff info invalid (email, ...)

            - 502: invalid status, supported 0|1

            - 503: is_all_permissions invalid, supported 0|1

            - 504: permission invalid

            - 506: staff (with staff_id) does not existed

            - 507: staff (with email) already existed

            - 508: Excess limit to allow creating staff accounts

            - 201: unknown error

            - 400: token invalid

    - Notice:

        - submit full data on editing

16. get staffs list

    - method: GET

    - url: /api/chatbot_novaon_v2/staffs?page=3&limit=15&token=ASDFGHJKL1234567890

    - request:

        - header: "content-type": "application/json"

        - where:
            - page: page number. If absent or empty value ('') => not use, then param "limit" will be not used too
            - limit: the maximum records will be returned for that request. If absent or empty value ('') => default 15
            - sort: field used to sort (username, status, date_added), default if empty is username
            - order: sort order value (ASC, DESC), default if empty is ASC

    - response:

        - type: application/json

        - value: json. empty if not found. order by "username" ASC. example format:

       ```
       {
            "total": 3,
            "page": 1,
            "records": [
                {
                    "id": "6",
                    "avatar": "",
                    "username": "thuong.staff3@gmail.com",
                    "firstname": "thuong",
                    "lastname": "bv",
                    "user_group_name": "nhom 1"
                },
                {
                    "id": "5",
                    "avatar": "",
                    "username": "thuong.staff2@gmail.com",
                    "firstname": "thuong",
                    "lastname": "bv",
                    "user_group_name": "nhom 1"
                },
                {
                    "id": "4",
                    "avatar": "",
                    "username": "thuong.staff@gmail.com",
                    "firstname": "Staff",
                    "lastname": "Thuong",
                    "user_group_name": "nhom 1"
                }
            ]
       }
       ```

        - where:

            - total: total staffs (and match the search if provided)

            - page: current page. If empty ('') => no page used, return all staffs in records

            - records: contains staffs list (with summary info). If count of records < limit: reach the end of staffs list

17. get staff detail

    - method: GET
    
    - url: /api/chatbot_novaon_v2/staffs?id=3&token=ASDFGHJKL1234567890
    
    - request:
    
        - header: "content-type": "application/json"
    
    - response:
    
        - type: application/json
    
        - value: json. empty if not found. example format:
    
          ```
            {
                "code": 200,
                "data": {
                    "id": "5",
                    "avatar": "",
                    "username": "thuong.staff2@gmail.com",
                    "firstname": "thuong",
                    "lastname": "bv",
                    "user_group_name": "nhom 1"
                }
            }
          ```

18. delete staff

    - method: POST
    
    - url: /api/chatbot_novaon_v2/staffs
    
    - request:
    
        - header: "content-type": "application/json"
    
    - body data:
    
      ```body
      {
          "token": "ASDFGHJKL1234567890"
          "data": {
              "staff_id": 6,
              "action": "delete"
          }
      }
      ```
    
        - where:
    
            - required: all keys in above sample data
    
            - action: current supported "delete"
    
    - response:
    
        - type: application/json
    
        - value: json for delete staff result. example format:
    
          ```
              {
                  "code": 0,
                  "message": "staff is deleted successfully",
              }
          ```
    
        - where:
    
            - response "code":
    
                - 0: staff is deleted successfully
    
                - 506: staff (with staff_id) does not existed
    
                - 201: unknown error
    
                - 400: token invalid

19. get categories list

    - method: GET
    
    - url: /api/chatbot_novaon_v2/categories?page=3&limit=15&category_ids=6,7&token=ASDFGHJKL1234567890
    
    - request:
    
        - header: "content-type": "application/json"
    
        - where:
    
            - page: page number. If absent or empty value ('') => not use, then param "limit" will be not used too
    
            - limit: the maximum records will be returned for that request. If absent or empty value ('') => default 15
    
            - category_ids: the category ids to be returned, multiple values split by comma (,). e.g 6,7,...
    
    - response:
    
        - type: application/json
    
        - value: json. empty if not found. order by "category_id" DESC. example format:
    
          ```
          {
              "total": 18,
              "page": 3,
              "records": [
                  {
                      "category_id": 2,
                      "name": "Loại sp 2",
                      "image": ""
                  },
                  {
                      "category_id": 3,
                      "name": "Loại sp 3",
                      "image": ""
                  },
                  ...
              ]
          }
          ```
    
            - where:
    
                - total: total categories (and match the search if provided)
    
                - page: current page. If empty ('') => no page used, return all categories in records
    
                - records: contains categories list (with summary info). If count of records < limit: reach the end of
                  categories list

20. get manufacturers list

    - method: GET
    
    - url: /api/chatbot_novaon_v2/manufacturers?page=3&limit=15&manufacturer_ids=6,7&token=ASDFGHJKL1234567890
    
    - request:
    
        - header: "content-type": "application/json"
    
        - where:
    
            - page: page number. If absent or empty value ('') => not use, then param "limit" will be not used too
    
            - limit: the maximum records will be returned for that request. If absent or empty value ('') => default 15
    
            - manufacturer_ids: the manufacturer ids to be returned, multiple values split by comma (,). e.g 6,7,...
    
    - response:
    
        - type: application/json
    
        - value: json. empty if not found. order by "manufacturer_id" DESC. example format:
    
          ```
          {
              "total": 18,
              "page": 3,
              "records": [
                  {
                      "manufacturer_id": 2,
                      "name": "NCC 2"
                  },
                  {
                      "manufacturer_id": 3,
                      "name": "NCC 3"
                  },
                  ...
              ]
          }
          ```
    
            - where:
    
                - total: total manufacturers (and match the search if provided)
    
                - page: current page. If empty ('') => no page used, return all manufacturers in records
    
                - records: contains manufacturers list (with summary info). If count of records < limit: reach the end of
                  manufacturers list

21. get customers list

    - method: GET

    -
    url: http://x2.bestme.test/api/chatbot_novaon_v2/customers?page=1&limit=100&filter_search=&phone=&phone_operator=&order=&order_operator=&order_value=&order_latest=&order_latest_operator=&order_latest_value=&amount=&amount_operator=&amount_value=&source=&source_value=&token=dd1f152099fb23a87ded858f2444cc12

    - request:

        - header: "content-type": "application/json"

        - where:

            - page: page number. If absent or empty value ('') => not use, then param "limit" will be not used too

            - limit: the maximum records will be returned for that request. If absent or empty value ('') => default 15

            - filter_search: the search value, escaped. e.g "pham tai sang" => "pham+tai+sang"
            
            - filter phone eg: [&phone=1&phone_operator=NAT] => Filter a list of unauthenticated customers
                
                - phone: not be empty 
                
                - phone_operator: "AT" or "NAT" ( AT is authenticated, NAT is not authenticated ) 
                
            - filter order eg: [&order=1&order_operator=LT&order_value=10] => Filter the list of customers with orders less than 10
                
                - order: not be empty
                
                - order_operator: "LT", "GT", "EQ" (LT is get <, GT is get >, BG equals)
                
                - order_value: number
                
            - filter order latest eg: [&order_latest=1&order_latest_operator=IS&order_latest_value=2020-11-26+10%3A29%3A07] => Filter the list of customers with latest order is 2020-11-26 10:29:07
            
                - order_latest: not be empty
                
                - order_latest_operator: "AT", "BF", "EQ" (AT is after, BF is before, EQ)
                
                - order_latest_value: date value 
                
            - filter payment amount eg: [&amount=1&amount_operator=LT&amount_value=122000000] => Filter a list of customers with paid amounts less than 122.000.000đ
                
                - amount: not be empty
                
                - amount_operator: "LT", "GT", "EQ" (LT is get <, GT is get >, BG equals)
                
                - amount_value: value payment amount
                
            - filter customer source eg: [&source=1&source_value=2] => Filter a list of customers with source is Facebook
            
                - source: not be empty
                
                - source_value: "1" = NovaonX Social, "2" = Facebook, "3" = Bestme
                
            - filter subscriber id eg: [&subscriber_id=93ohir] => Filter a list of customers with subscriber_id is '93ohir'

    - response:

        - type: application/json

        - value: json. empty if not found. order by "customer_id DESC". example format:

          ```
          {
              "total": 18,
              "page": 3,
              "records": [
                 {
                     "customer_id": "11",
                     "customer_name": "Nguyen Thuy Linh",
                     "telephone": "0987654036",
                     "customer_source": "NovaonX Social",
                     "is_authenticated": "Đã xác thực",
                     "count_order": "0",
                     "order_total": "0",
                     "order_lastest": null,
                     "subscriber_id": null,
                     "addresses": [
                        {
                            "customer_id": "24",
                            "firstname": "A",
                            "lastname": "nguyen Van",
                            "address": "So 1 Ngo 2 Duy Tan 364",
                            "city": "89",
                            "district": "886",
                            "wards": "30373",
                            "phone": "09446324536",
                            "default_address": true
                        },
                        {
                            "customer_id": "24",
                            "firstname": "sangpttest",
                            "lastname": "sangpttest",
                            "address": "HN",
                            "city": "89",
                            "district": "884",
                            "wards": "30316",
                            "phone": "0655996323",
                            "default_address": false
                        },
                        {
                            "customer_id": "24",
                            "firstname": "sangpttest22222",
                            "lastname": "sangpttest22222",
                            "address": "HN ghahahaha",
                            "city": "24",
                            "district": "218",
                            "wards": "07459",
                            "phone": "0655996323",
                            "default_address": false
                        }
                     ]
                 },
                 {
                     "customer_id": "4",
                     "customer_name": "pham tai sang",
                     "telephone": "09312645443",
                     "customer_source": "NovaonX Social",
                     "is_authenticated": "Đã xác thực",
                     "count_order": "3",
                     "order_total": "112,130,900",
                     "order_lastest": "2020-11-19 11:27:55",
                     "subscriber_id": null,
                     "addresses": [
                         {
                            "customer_id": "4",
                            "firstname": "sangpttest22222",
                            "lastname": "sangpttest22222",
                            "address": "HN ghahahaha",
                            "city": "24",
                            "district": "218",
                            "wards": "07459",
                            "phone": "0655996323",
                            "default_address": true
                          }
                     ]
                 },
                  ...
              ]
          }
          ```

            - where:

                - is_authenticated : "1" = phone number authenticated , "0" = is not authenticated
                
                - customer_source : "1" = NovaonX Social, "2" = Facebook, "3" = Bestme	
                
                - total: total categories (and match the search if provided)

                - page: current page. If empty ('') => no page used, return all categories in records

                - records: contains customers list (with summary info). If count of records < limit: reach the end of
                  customers list

22. create customer

    - method: POST

    - url: /api/chatbot_novaon_v2/customers

    - request:

        - header: "content-type": "application/json"

    - body data:

      ```
      {
          "token": "ASDFGHJKL1234567890"
          "data": {
                      "customer": {
                          "name": "Nguyen Thuy Linh",
                          "phone_number": "0987654036",
                          "customer_source": "1",
                          "is_authenticated": 1,
                          "email": "nguyenthuylinh363@gmail.com",
                          "delivery_addr": "So 1 Ngo 2 Duy Tan 364",
                          "ward": "30373",
                          "district": "886",
                          "city": "89",
                          "note": "Goi hang can than va du so luong nhe shop. Thanks! 364",
                          "subscriber_id": "134xyz"
                      }
                  }
      }
      ```

        - where (* is required):

            - customer_id* : (required on editing ONLY) customer id. Let empty for creating customer
            - name* : not be empty
            - phone_number* : unique
            - email* : unique
            - is_authenticated : default 1 , "1" = authenticated , "0" = not authenticated
            - customer_source : default 1 , "1" = NovaonX Social, "2" = Facebook, "3" = Bestme
            - subscriber_id : empty

    - response:

        - type: application/json

        - value: json for create/edit order result. example format:

          ```
              {
                   "code": 200,
                   "message": "Success"
              }
          ```

        - where:

            - response "code":

                - 200: customer is created successfully

                - 101: missing or invalid name
                - 125: error phone exist
                - 125: error email exist

                - 201: unknown error

                - 400: token invalid

23. get payment methods list

    - method: GET

    - url: /api/chatbot_novaon_v2/payment_methods?token=dd1f152099fb23a87ded858f2444cc12

    - request:

        - header: "content-type": "application/json"

    - response:

        - type: application/json

        - value: json. example format:

          ```
          {
            "code": 200,
            "message": "Lấy danh sách phương thức thanh toán thành công",
            "data": [
                {
                    "payment_id": "198535319739A",
                    "name": "Thanh toán khi nhận hàng (COD)",
                    "guide": "Bạn sẽ thanh toán toàn bộ giá trị đơn hàng khi nhận hàng",
                    "status": "1"
                },
                {
                    "payment_id": "paym_ons_vnpay",
                    "name": "text_vnpay_payment",
                    "guide": "",
                    "status": 1
                }
            ]
          }
          ```

            - where:

                - code: response code

                - message: response message

                - data: list of payment method objects

24. get user group list

    - method: GET

    - url: /api/chatbot_novaon_v2/user_groups?token=dd1f152099fb23a87ded858f2444cc12

    - request:

        - header: "content-type": "application/json"

    - response:

        - type: application/json

        - value: json. example format:

          ```
          {
            "code": 200,
            "data": [
                {
                    "name": "nhom 1",
                    "total": "3",
                    "id": "11"
                }
            ]
          }
          ```

            - where:
                - code: response code
                - data: list of user group records
8. get dashboard info

    - method: GET

    - url: /api/chatbot_novaon_v2/dashboard?start=2021-02-04+00%3A00%3A00&end=2021-06-11+23%3A59%3A59

    - request:

        - header: "content-type": "application/json"

    - response:

        - type: application/json

        - value: json for products list. example format:

          ```
          {
               "code": 200,
               "data":
               {
                   "revenue":
                   {
                       "content": [
                           {
                               "label": "11\/2020",
                               "value": 0
                           },
                           {
                               "label": "12\/2020",
                               "value": 0
                           },
                           {
                               "label": "01\/2021",
                               "value": 0
                           },
                           {
                               "label": "02\/2021",
                               "value": 0
                           },
                           {
                               "label": "03\/2021",
                               "value": "977710000.0000"
                           },
                           {
                               "label": "04\/2021",
                               "value": 0
                           }
                       ],
                       "total": "864220000.0000"
                   },
                   "customer":
                   {
                       "content": [
                           {
                               "label": "11\/2020",
                               "value": 0
                           },
                           {
                               "label": "12\/2020",
                               "value": 0
                           },
                           {
                               "label": "01\/2021",
                               "value": 0
                           },
                           {
                               "label": "02\/2021",
                               "value": 0
                           },
                           {
                               "label": "03\/2021",
                               "value": "9"
                           },
                           {
                               "label": "04\/2021",
                               "value": 0
                           }
                       ],
                       "total": "9"
                   },
                   "order":
                   {
                       "content": [
                           {
                               "label": "11\/2020",
                               "value": 0
                           },
                           {
                               "label": "12\/2020",
                               "value": 0
                           },
                           {
                               "label": "01\/2021",
                               "value": 0
                           },
                           {
                               "label": "02\/2021",
                               "value": 0
                           },
                           {
                               "label": "03\/2021",
                               "value": "15"
                           },
                           {
                               "label": "04\/2021",
                               "value": 0
                           }
                       ],
                       "total": "15",
                       "total_completed": "15"
                   }
               }
          }
          ```
    - where:

        - start: date start to filter

        - end: date end to filter

    - note:

        - if date start or date end are empty: get dashboard info for last 7 days

III. Discussion

- map provinces, districts, wards? => OK

