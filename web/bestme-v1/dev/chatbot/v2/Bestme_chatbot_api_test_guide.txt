1. Tài liệu "Bestme-Chatbot-api-v2.0.md"

2. Code test mẫu nếu cần tham khảo "test_chatbot_v2.php"

3. Steps:

Chú ý: 

- API KEY sử dụng test là "1234567890ABCDEFGHIJ"

- Trang online tính md5: https://www.md5online.org/

3.1: test verify api key

- Mở tool Advanced Restclient

- Chọn method: GET

- Nhập Url: http://123.30.146.248:8686/api/chatbot_novaon_v2/verify?data=[text tester tự thêm vào]&token=[token]

Ví dụ: 

[text tester tự thêm vào] = 'Nhung_test_xac_thuc_api_key'

=> mở tool tính md5 online, nhập vào chuỗi sau: Nhung_test_xac_thuc_api_key1234567890ABCDEFGHIJ

=> tính ra md5 = 04d190bea4b6716dad5d558650992256 => đây là gía trị của token => thay vào [token]

=> http://123.30.146.248:8686/api/chatbot_novaon_v2/verify?data=Nhung_test_xac_thuc_api_key&token=04d190bea4b6716dad5d558650992256

- Bấm nút SEND trên Advanced Rest client

- Xem kết quả dạng:

{
    "code": [code],
    "message": "[message]"
}

Đối chiếu bảng mã code...

3.2: get products list

http://123.30.146.248:8686/api/chatbot_novaon_v2/products?page=1&limit=19&searchKey=&searchField=name&token=xxx

=> md5: = [page][limit][searchKey][searchField][API KEY] = [1][19][][name][1234567890ABCDEFGHIJ] =>  119name1234567890ABCDEFGHIJ

=> token: c2a41f6606617a4231042a1417b0a657

=> http://123.30.146.248:8686/api/chatbot_novaon_v2/products?page=1&limit=19&searchKey=&searchField=name&token=c2a41f6606617a4231042a1417b0a657

get page 2:


=> token: c0d310304a6fa4be2d723e861905191f

=> http://123.30.146.248:8686/api/chatbot_novaon_v2/products?page=2&limit=19&searchKey=&searchField=name&token=c0d310304a6fa4be2d723e861905191f















