<?php

define('BESTME_CHATBOT_API_BASE_URL', 'http://x2.bestme.test/api/chatbot_novaon_v2/');
define('BESTME_CHATBOT_API_KEY', 'S6PEokUxtdD1Eg9Mg9wnSS71GqXcJdeG');

//define('BESTME_CHATBOT_API_BASE_URL', 'http://123.30.146.248:8686/api/chatbot_novaon_v2/');
//define('BESTME_CHATBOT_API_KEY', 'QurSJd89rcrinw0K0z74NGIunie4JbE2');

//define('BESTME_CHATBOT_API_BASE_URL', 'https://dunght42.mybestme.net/api/chatbot_novaon_v2/');
//define('BESTME_CHATBOT_API_KEY', '2a7Pf6uU6nmGNyVcBYxztbfwNSnvPoqu');

//define('BESTME_CHATBOT_API_BASE_URL', 'https://dunght42.bestme.asia/api/chatbot_novaon_v2/');
//define('BESTME_CHATBOT_API_KEY', 'MUNv9kqQlNguRAAZXotHb4Mu8ax0p9Zw');

$choose = 29;

if (is_array($argv) && count($argv) > 1) {
    $param_case = str_replace('case=', '', $argv[1]);
    $choose = (int)$param_case;
}

$allCases = false;

switch ($choose) {
    case 99:
        $allCases = true;

    case 1:
        /* test verify api key */
        testVefifyApiKey();
        if (!$allCases) {
            break;
        }

    case 2:
        /* test get products list */
        testGetProductsList();
        if (!$allCases) {
            break;
        }

    case 3:
        /* test get product detail */
        testGetProduct();
        if (!$allCases) {
            break;
        }

    case 4:
        /* test create order */
        testCreateOrder();
        if (!$allCases) {
            break;
        }

    case 5:
        /* test create order */
        testEditOrder();
        if (!$allCases) {
            break;
        }

    case 6:
        /* test get orders list */
        testGetOrdersList();
        if (!$allCases) {
            break;
        }

    case 7:
        /* test get order detail */
        testGetOrder();
        if (!$allCases) {
            break;
        }

    case 8:
        /* test get transport list */
        testGetDeliveryList();
        if (!$allCases) {
            break;
        }

    case 9:
        /* test get transport fee */
        testGetDeliveryFee();
        if (!$allCases) {
            break;
        }

    case 10:
        /* test create staff */
        testCreateStaff();
        if (!$allCases) {
            break;
        }

    case 11:
        /* test get staffs list */
        testGetStaffsList();
        if (!$allCases) {
            break;
        }

    case 12:
        /* delete staff */
        testDeleteStaff();
        if (!$allCases) {
            break;
        }

    case 13:
        testCreateProduct();
        if (!$allCases) {
            break;
        }

    case 14:
        testEditProduct();
        if (!$allCases) {
            break;
        }

    case 15:
        testDeleteProduct();
        if (!$allCases) {
            break;
        }

    case 16:
        /* test get Categories list */
        testGetCategoriesList();
        if (!$allCases) {
            break;
        }

    case 17:
        /* test get Manufacturer list */
        testGetmanufacturersList();
        if (!$allCases) {
            break;
        }
    case 18:
        testGetCustomersList();
        if (!$allCases) {
            break;
        }
    case 19:
        testCreateCustomer();
        if (!$allCases) {
            break;
        }
    case 20:
        testProvinces();
        if (!$allCases) {
            break;
        }
    case 21:
        testDistricts();
        if (!$allCases) {
            break;
        }
    case 22:
        testWards();
        if (!$allCases) {
            break;
        }
    case 23:
        testGetPaymentMethods();
        if (!$allCases) {
            break;
        }
    case 24:
        testGetShippingServices();
        if (!$allCases) {
            break;
        }
    case 25:
        testCreateDeliveryOrder();
        if (!$allCases) {
            break;
        }
    case 26:
        testCancelDeliveryOrder();
        if (!$allCases) {
            break;
        }
    case 27:
        testGetStaff();
        if (!$allCases) {
            break;
        }
    case 28:
        testGetUserGroups();
        if (!$allCases) {
            break;
        }
    case 29:
        testGetDashboard();
        if (!$allCases) {
            break;
        }
}

// /api/chatbot_novaon_v2/verify?data=data-for-testing-credential-blala&token=05433efa664ebe43b124ff73591de08f
function testVefifyApiKey()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    $data = "data-for-testing-credential-blala";
    $token = md5("{$data}{$apiKey}");
    $url = "{$baseUrl}verify?data=$data&token=$token";
    println("Calling $url");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if (!is_array($response)) {
        println('Invalid response!');
        return;
    }

    if ($response['code'] != 1) {
        println("Invalid credentials! Code: {$response['code']}");
        return;
    }

    println("Trusted credentials!");
}

// /api/chatbot_novaon_v2/products?page=1&limit=15&searchKey=&searchField=name&product_ids=123_13,123_14&except_product_ids=98,103_1&is_all=0&ver=1.1&token=f3bbbadbb12a1f158b51adc71c2dbacf
function testGetProductsList()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    $page = 1;
    $limit = 3;
    $searchKey = '1'; //urlencode('sandal quai');
    $searchField = 'status';
    $quantity = ''; // lt_2
    $category = 'cor'; // 'cor'
    $manufacturer = ''; // 'zor
    $product_ids = ''; //'123_13,123_14';
    $except_product_ids = ''; //'94,102,103_1,103_3';
    $is_all = '';
    $status = 0;
    $ver = '1.1';
    $token = md5("{$page}{$limit}{$searchKey}{$searchField}{$quantity}{$category}{$manufacturer}{$product_ids}{$except_product_ids}{$is_all}{$status}{$apiKey}");
    $url = "{$baseUrl}products?page={$page}&limit={$limit}&searchKey={$searchKey}&searchField={$searchField}&quantity={$quantity}&category={$category}&manufacturer={$manufacturer}&product_ids={$product_ids}&except_product_ids={$except_product_ids}&is_all={$is_all}&status={$status}&ver={$ver}&token=$token&XDEBUG_SESSION_START=1";
    println("Calling $url");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $res = curl_exec($curl);
    $response = json_decode($res, true);

    if (!is_array($response) ||
        !array_key_exists('total', $response) ||
        !array_key_exists('page', $response) ||
        !array_key_exists('records', $response) ||
        !is_array($response['records'])
    ) {
        println('Invalid response! Got: ' . $res);
        return;
    }

    println("Products list: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

// /api/chatbot_novaon_v2/products?id=1&token=9ddde6630ff0ea2bc9c79507f316c88a
function testGetProduct()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    $productId = 1104;
    $productVersionId = '';//1022 // or null or ''
    $ver = '1.0';
    $token = md5("{$productId}{$productVersionId}{$apiKey}");
    $url = "{$baseUrl}products?id={$productId}&product_version_id={$productVersionId}&ver={$ver}&token={$token}&XDEBUG_SESSION_START=1";
    println("Calling $url");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if (!is_array($response) ||
        !array_key_exists('id', $response) ||
        !array_key_exists('name', $response) // and check more...
    ) {
        println('Invalid response!');
        return;
    }

    println("Product detail: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

// /api/chatbot_novaon_v2/products
function testCreateProduct()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    /* init body data to post, absent "token" that to be added later */

    /* 1 pb
    {
        "name": "san pham 1 pb 1359",
        "description": "&lt;p&gt;mo ta&lt;\/p&gt;",
        "summary": "&lt;p&gt;mo ta ngan&lt;\/p&gt;",
        "product_version": "0",
        "price": "50,000",
        "promotion_price": "36,999",
        "weight": "200",
        "sku": "sp1pb 1359",
        "barcode": "",
        "stores_default_id": "0",
        "sale_on_out_of_stock": "1",
        "common_compare_price": "",
        "common_price": "",
        "common_weight": "0",
        "common_sku": "",
        "common_barcode": "",
        "stores_default_id_mul": "0",
        "common_cost_price": "0",
        "common_sale_on_out_of_stock": "1",
        "attribute_name": [""],
        "stores":
        [
            ["0"],
            ["2"]
        ],
        "original_inventory":
        [
            ["4"],
            ["2"]
        ],
        "cost_price":
        [
            ["5,000"],
            ["4,000"]
        ],
        "seo_title": "",
        "seo_desciption": "",
        "alias": "",
        "meta_keyword": "",
        "images": ["http:\/\/127.0.0.1:8000\/images\/x2\/apple_cinema_30-500x500_MCTR3aQ.jpg"],
        "image-alts": [""],
        "status": "1",
        "category": ["cat 224", "cat 223"],
        "manufacturer": "manuface f",
        "collection_list": ["collection 42"],
        "tag_list": ["tag 46"],
        "channel": "0",
        "created_user_id": "4"
    }
    */
    /*
    * npb
        {
            "name": "san pham 1109 pb",
            "description": "<p>mo ta<\/p>",
            "summary": "<p>mo ta ngan<\/p>",
            "product_version": "1",
            "price": "",
            "promotion_price": "",
            "weight": "1,000",
            "sku": "spnpb 1109",
            "barcode": "SP1pb",
            "stores_default_id": "0",
            "sale_on_out_of_stock": "1",
            "common_compare_price": "50,000",
            "common_price": "49,000",
            "common_weight": "1,000",
            "common_sku": "SPNPB 1109",
            "common_barcode": "WTIOW",
            "stores_default_id_mul": "0",
            "common_cost_price": "50,000",
            "common_sale_on_out_of_stock": "1",
            "attribute_name": [
                "m\u00e0u",
                "cao"
            ],
            "attribute_values": [
                [
                    "X",
                    "D"
                ],
                [
                    "thap",
                    "ca"
                ]
            ],
            "product_display": [
                "D \u2022 thap",
                "X \u2022 thap",
                "D \u2022 ca",
                "X \u2022 ca"
            ],
            "product_version_names": [
                "D \u2022 thap",
                "X \u2022 thap",
                "D \u2022 ca",
                "X \u2022 ca"
            ],
            "product_prices": [
                "50,000",
                "50,000",
                "50,000",
                "50,000"
            ],
            "product_promotion_prices": [
                "49,000",
                "49,000",
                "49,000",
                "49,000"
            ],
            "product_quantities": [
                "0",
                "0",
                "0",
                "0"
            ],
            "product_skus": [
                "SPNPB1 1109",
                "SPNPB2 1109",
                "SPNPB3 1109",
                "SPNPB4 1109"
            ],
            "product_barcodes": [
                "WTIOW",
                "WTIOW",
                "WTIOW",
                "WTIOW"
            ],
            "stores":
            [
                [
                    "0",
                    "2"
                ],
                [
                    "0"
                ],
                [
                    "0"
                ],
                [
                    "0"
                ]
            ],
            "original_inventory":
            [
                [
                    "10",
                    "20"
                ],
                [
                    "0"
                ],
                [
                    "0"
                ],
                [
                    "0"
                ]
            ],
            "cost_price":
            [
                [
                    "50,000",
                    "45,000"
                ],
                [
                    "50,000"
                ],
                [
                    "50,000"
                ],
                [
                    "50,000"
                ]
            ],
            "seo_title": "",
            "seo_desciption": "",
            "alias": "",
            "meta_keyword": "",
            "images": [
                "http:\/\/127.0.0.1:8000\/images\/x2\/apple_cinema_30-500x500_MCTR3aQ.jpg"
            ],
            "image-alts": [
                ""
            ],
            "product_version_images": [
                "",
                "",
                "",
                ""
            ],
            "product_version_image_alts": [
                "",
                "",
                "",
                ""
            ],
            "status": "1",
            "category": [
                "lsp a",
                "lsp b"
            ],
            "manufacturer": "32",
            "collection_list": [
                "bst 1",
                "bst 2"
            ],
            "tag_list": [
                "tag 1",
                "tag n"
            ],
            "channel": "0",
            "created_user_id": "4"
        }
     */

    $body = '' .
        '{
            "data": {
                "name": "san pham 1 pb 1359",
                "description": "&lt;p&gt;mo ta&lt;\/p&gt;",
                "summary": "&lt;p&gt;mo ta ngan&lt;\/p&gt;",
                "product_version": "0",
                "price": "50,000",
                "promotion_price": "36,999",
                "weight": "200",
                "sku": "sp1pb 1359",
                "barcode": "",
                "stores_default_id": "0",
                "sale_on_out_of_stock": "1",
                "common_compare_price": "",
                "common_price": "",
                "common_weight": "0",
                "common_sku": "",
                "common_barcode": "",
                "stores_default_id_mul": "0",
                "common_cost_price": "0",
                "common_sale_on_out_of_stock": "1",
                "attribute_name": [""],
                "stores":
                [
                    ["0"],
                    ["2"]
                ],
                "original_inventory":
                [
                    ["4"],
                    ["2"]
                ],
                "cost_price":
                [
                    ["5,000"],
                    ["4,000"]
                ],
                "seo_title": "",
                "seo_desciption": "",
                "alias": "",
                "meta_keyword": "",
                "images": ["http:\/\/127.0.0.1:8000\/images\/x2\/apple_cinema_30-500x500_MCTR3aQ.jpg"],
                "image-alts": [""],
                "status": "1",
                "category": ["cat 224", "cat 223"],
                "manufacturer": "manuface f",
                "collection_list": ["collection 42"],
                "tag_list": ["tag 46"],
                "channel": "0",
                "created_user_id": "4"
            }
        }';

    /*
     * calculate token. Notice: json encode without space and \r\n characters
     * expected $bodyData as:
     * {"data":{"customer":{"name":"Nguyen Thuy Linh","phone_number":"0987654321","email":"nguyenthuylinh@gmail.com","delivery_addr":"So 1 Ngo 2 Duy Tan","ward":"Dich Vong Hau","district":"Cau Giay","city":"Ha Noi","note":"Goi hang can than va du so luong nhe shop. Thanks!"},"products":[{"id":86,"quantity":2}]}}
     */
    $bodyData = json_encode(json_decode($body, true));
    $token = md5("{$bodyData}{$apiKey}"); // 54d1dc2960f864ccf7e568a66866e26f
    $url = "{$baseUrl}products?XDEBUG_SESSION_START=1";
    println("Calling $url");
    println("Using token $token");

    /* set token to body */
    $body = json_decode($body, true);
    $body['token'] = $token;
    $body = json_encode($body);

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $body,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
        )
    ));

    $resp = curl_exec($curl);
    $response = json_decode($resp, true);
    curl_close($curl);

    if (!is_array($response) ||
        !array_key_exists('code', $response) ||
        !array_key_exists('message', $response)
    ) {
        println('Invalid response!');
        var_dump($resp);
        return;
    }

    println("Create product response: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));

    if ($response['code'] == 0) {
        println("=> product created successfully!");
    }
}

// /api/chatbot_novaon_v2/products
function testEditProduct()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    /* init body data to post, absent "token" that to be added later */
    $body = '' .
        '{
            "data": {
                "product_id" 127,
                "name": "San pham test Social edit",
                "description": "San pham test Social description",
                "summary": "San pham test Social sumary",
                "product_version": "0",
                "price": "10,000",
                "promotion_price": "9,000",
                "weight": "123",
                "sku": "SOCIAL TEST 1",
                "barcode": "82574258001",
                "sale_on_out_of_stock": "1",
                "attribute_name": [
                    ""
                ],
                "original_inventory": [
                    [
                        "50"
                    ]
                ],
                "images": [
                    "https:\/\/res.cloudinary.com\/bestme-test\/image\/upload\/v1587983715\/x2.bestme.test\/cf714c11ee1dab3ee6fbac9e7bea41eb.jpg",
                    "https:\/\/res.cloudinary.com\/bestme-test\/image\/upload\/v1587983704\/x2.bestme.test\/942706098598332177618186301982977727397888n.jpg"
                ],
                "image-alts": [
                    "",
                    ""
                ],
                "status": "1",
                "category": [182,183],
                "manufacturer": "48",
                "channel": "0"
            }
        }';

    /*
     * calculate token. Notice: json encode without space and \r\n characters
     * expected $bodyData as:
     * {"data":{"customer":{"name":"Nguyen Thuy Linh","phone_number":"0987654321","email":"nguyenthuylinh@gmail.com","delivery_addr":"So 1 Ngo 2 Duy Tan","ward":"Dich Vong Hau","district":"Cau Giay","city":"Ha Noi","note":"Goi hang can than va du so luong nhe shop. Thanks!"},"products":[{"id":86,"quantity":2}]}}
     */
    $bodyData = json_encode(json_decode($body, true));
    $token = md5("{$bodyData}{$apiKey}"); // 54d1dc2960f864ccf7e568a66866e26f
    $url = "{$baseUrl}products?XDEBUG_SESSION_START=1";
    println("Calling $url");

    /* set token to body */
    $body = json_decode($body, true);
    $body['token'] = $token;
    $body = json_encode($body);

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $body,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
        )
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);
    curl_close($curl);

    if (!is_array($response) ||
        !array_key_exists('code', $response) ||
        !array_key_exists('message', $response)
    ) {
        println('Invalid response!');
        return;
    }

    println("Edit product response: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));

    if ($response['code'] == 0) {
        println("Edit product successfully: code={$response['code']}, message={$response['message']}");
    }
}

// /api/chatbot_novaon_v2/products
function testDeleteProduct()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    /* init body data to post, absent "token" that to be added later */
    $body = '' .
        '{
            "data": {
                "product_id": 128,
                "action": "delete"
            }
        }';

    /*
     * calculate token. Notice: json encode without space and \r\n characters
     * expected $bodyData as:
     * {"data":{...}}
     */
    $bodyData = json_encode(json_decode($body, true));
    $token = md5("{$bodyData}{$apiKey}"); // 54d1dc2960f864ccf7e568a66866e26f
    $url = "{$baseUrl}products?XDEBUG_SESSION_START=1";
    println("Calling $url");

    /* set token to body */
    $body = json_decode($body, true);
    $body['token'] = $token;
    $body = json_encode($body);

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $body,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
        )
    ));

    $resp = curl_exec($curl);
    $response = json_decode($resp, true);
    curl_close($curl);

    if (!is_array($response) ||
        !array_key_exists('code', $response) ||
        !array_key_exists('message', $response)
    ) {
        println('Invalid response!');
        var_dump($resp);
        return;
    }

    println("Delete product response: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));

    if ($response['code'] == 0) {
        println("=> product deleted successfully!");
    }
}

// /api/chatbot_novaon_v2/orders
function testCreateOrder()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    /* init body data to post, absent "token" that to be added later */
    $body = '' .
        '{
            "data": {
                "customer": {
                    "name": "Nguyen Thuy Linh",
                    "phone_number": "0987654321",
                    "email": "nguyenthuylinh@gmail.com",
                    "delivery_addr": "So 1 Ngo 2 Duy Tan",
                    "ward": "30280",
                    "district": "883",
                    "city": "89",
                    "note": "Goi hang can than va du so luong nhe shop. Thanks!",
                    "subscriber_id" : ""
                },
                "status": 6,
                "customer_ref_id": "",
                "tags": "tag1, tag2",
                "utm": [
                    {
                        "key": "campaign",
                        "value": "Campaign 1"
                    },
                    {
                        "key": "campaign",
                        "value": "Campaign 2"
                    }
                ],
                "products": [
                    {
                        "id": 1,
                        "quantity": 2,
                        "product_version_id": 0
                    },
                    {
                        "id": 2,
                        "quantity": 3,
                        "product_version_id": 2
                    }
                ],
                "voucher_product": {
                    "voucher_code": "ABCDEFGH99",
                    "voucher_type": 1,
                    "amount": 20000,
                    "products": [
                        {
                            "id": 1,
                            "product_version_id": 0
                        },
                        {
                            "id": 2,
                            "product_version_id": 2
                        }
                    ],
                    "start_at": "2021-04-22 13:41:00",
                    "end_at": "2021-04-22 13:41:59",
                    "user_create_id": "10"
                },
                "campaign_id": "",
                "channel_id": "",
                "payment_trans": "-2",
                "payment_trans_custom_name": "Shop tự vận chuyển",
                "payment_trans_custom_fee": 25000,
                "payment_status": 1
            }
        }';

    /*
     * calculate token. Notice: json encode without space and \r\n characters
     * expected $bodyData as:
     * {"data":{"customer":{"name":"Nguyen Thuy Linh","phone_number":"0987654321","email":"nguyenthuylinh@gmail.com","delivery_addr":"So 1 Ngo 2 Duy Tan","ward":"Dich Vong Hau","district":"Cau Giay","city":"Ha Noi","note":"Goi hang can than va du so luong nhe shop. Thanks!"},"products":[{"id":86,"quantity":2}]}}
     */
    $bodyData = json_encode(json_decode($body, true));
    $token = md5("{$bodyData}{$apiKey}"); // 54d1dc2960f864ccf7e568a66866e26f
    $url = "{$baseUrl}orders?XDEBUG_SESSION_START=1";
    println("Calling $url");
    println("Using token: $token");

    /* set token to body */
    $body = json_decode($body, true);
    $body['token'] = $token;
    $body = json_encode($body);

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $body,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
        )
    ));

    $resp = curl_exec($curl);
    $response = json_decode($resp, true);
    curl_close($curl);

    if (!is_array($response) ||
        !array_key_exists('code', $response) ||
        !array_key_exists('message', $response)
    ) {
        println('Invalid response!');
        var_dump($resp);
        return;
    }

    println("Create order response: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));

    if ($response['code'] == 0) {
        println("=> order created successfully!");
    }
}

// /api/chatbot_novaon_v2/orders
function testEditOrder()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    /* init body data to post, absent "token" that to be added later */
    $body = '' .
        '{
            "data": {
                "customer": {
                    "name": "Nguyen Thuy Linh",
                    "phone_number": "0987654321",
                    "email": "nguyenthuylinh@gmail.com",
                    "delivery_addr": "So 1 Ngo 2 Duy Tan",
                    "ward": "30280",
                    "district": "883",
                    "city": "89",
                    "note": "Goi hang can than va du so luong nhe shop. Thanks!",
                    "subscriber_id" : ""
                },
                "status": 6,
                "customer_ref_id": "",
                "order_id": 213,
                "tags": "tag1, tag2",
                "utm": [
                    {
                        "key": "campaign",
                        "value": "Campaign 1"
                    },
                    {
                        "key": "campaign",
                        "value": "Campaign 2"
                    }
                ],
                "products": [
                    {
                        "id": 1,
                        "quantity": 2,
                        "product_version_id": 0
                    },
                    {
                        "id": 2,
                        "quantity": 3,
                        "product_version_id": 2
                    }
                ],
                "voucher_product": {
                    "voucher_code": "ABCDEFGH99",
                    "voucher_type": 1,
                    "amount": 20000,
                    "products": [
                        {
                            "id": 1,
                            "product_version_id": 0
                        },
                        {
                            "id": 2,
                            "product_version_id": 2
                        }
                    ],
                    "start_at": "2021-04-22 13:41:00",
                    "end_at": "2021-04-22 13:41:59",
                    "user_create_id": "10"
                },
                "campaign_id": "",
                "channel_id": ""
            }
        }';

    /*
     * calculate token. Notice: json encode without space and \r\n characters
     * expected $bodyData as:
     * {"data":{"customer":{"name":"Nguyen Thuy Linh","phone_number":"0987654321","email":"nguyenthuylinh@gmail.com","delivery_addr":"So 1 Ngo 2 Duy Tan","ward":"Dich Vong Hau","district":"Cau Giay","city":"Ha Noi","note":"Goi hang can than va du so luong nhe shop. Thanks!"},"products":[{"id":86,"quantity":2}]}}
     */
    $bodyData = json_encode(json_decode($body, true));
    $token = md5("{$bodyData}{$apiKey}"); // 54d1dc2960f864ccf7e568a66866e26f
    $url = "{$baseUrl}orders?XDEBUG_SESSION_START=1";
    println("Calling $url");

    /* set token to body */
    $body = json_decode($body, true);
    $body['token'] = $token;
    $body = json_encode($body);

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $body,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
        )
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);
    curl_close($curl);

    if (!is_array($response) ||
        !array_key_exists('code', $response) ||
        !array_key_exists('message', $response)
    ) {
        println('Invalid response!');
        return;
    }

    println("Edit order response: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));

    if ($response['code'] == 0) {
        println("Edit order successfully: code={$response['code']}, message={$response['message']}");
    }
}

// /api/chatbot_novaon_v2/orders?page=1&limit=15&customer_ref_id=1234&order_ids=213,216&token=f3bbbadbb12a1f158b51adc71c2dbacf
function testGetOrdersList()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    $page = 1;
    $limit = 15;
    // $customer_ref_id = '';
    // $order_tag = urlencode('tag1');
    // $order_utm_key = urlencode('campaign');
    // $order_utm_value = urlencode('Campaign 1, Campaign 2');
    $order_ids = '';
    $order_status = '6';
    $order_payment = '0';
    $order_total_type = urlencode('lt_1000000, gt_300000');
    $order_code = urlencode('');
    $order_campaign = '8s41vdft78wert1';
    $order_channel = '1909543419365215';
    $order_product = urlencode('');

    $token = md5("{$page}{$limit}{$order_ids}{$order_status}{$order_payment}{$order_total_type}{$order_code}{$order_campaign}{$order_channel}{$order_product}{$apiKey}");

    $url = "{$baseUrl}orders?page={$page}&limit={$limit}&order_ids={$order_ids}&order_status={$order_status}&order_payment={$order_payment}&order_total_type={$order_total_type}&order_code={$order_code}&order_campaign={$order_campaign}&order_channel={$order_channel}&order_product={$order_product}&token=$token&XDEBUG_SESSION_START=1";
    println("Calling $url");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $res = curl_exec($curl);
    $response = json_decode($res, true);

    if (!is_array($response) ||
        !array_key_exists('total', $response) ||
        !array_key_exists('page', $response) ||
        !array_key_exists('records', $response) ||
        !is_array($response['records'])
    ) {
        println('Invalid response! Got: ' . $res);
        return;
    }

    println("Orders list: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

// /api/chatbot_novaon_v2/orders?id=1&token=9ddde6630ff0ea2bc9c79507f316c88a
function testGetOrder()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    $orderId = 368;
    $token = md5("{$orderId}{$apiKey}");
    $url = "{$baseUrl}orders?id={$orderId}&token=$token&XDEBUG_SESSION_START=1";
    println("Calling $url");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if (!is_array($response) ||
        !array_key_exists('customer', $response) ||
        !array_key_exists('products', $response) // and check more...
    ) {
        println('Invalid response!');
        return;
    }

    println("Order detail: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

// /api/chatbot_novaon_v2/deliveries?page=1&limit=15&token=9ddde6630ff0ea2bc9c79507f316c88a
function testGetDeliveryList()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $url = "{$baseUrl}deliveries?XDEBUG_SESSION_START=1";
    println("Calling $url");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if (!is_array($response)) {
        println('Invalid response!');
        return;
    }

    println("Delivery list: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

// /api/chatbot_novaon_v2/delivery_fee
function testGetDeliveryFee()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    /* init body data to post, absent "token" that to be added later */
    $body = '' .
        '{
             "products": [
                 {
                     "id": 148,
                     "quantity":  1
                 }
             ],
             "province": "02",
             "into_money": 200004,
             "delivery_id": "2"
        }';

    /*
     * calculate token. Notice: json encode without space and \r\n characters
     * expected $bodyData as:
     * {"data":{"customer":{"name":"Nguyen Thuy Linh","phone_number":"0987654321","email":"nguyenthuylinh@gmail.com","delivery_addr":"So 1 Ngo 2 Duy Tan","ward":"Dich Vong Hau","district":"Cau Giay","city":"Ha Noi","note":"Goi hang can than va du so luong nhe shop. Thanks!"},"products":[{"id":86,"quantity":2}]}}
     */
    $bodyData = json_encode(json_decode($body, true));
    $token = md5("{$bodyData}{$apiKey}"); // 54d1dc2960f864ccf7e568a66866e26f

    /* set token to body */
    $body = json_decode($body, true);
    $body['token'] = $token;
    $body = json_encode($body);

    $url = "{$baseUrl}delivery_fee?XDEBUG_SESSION_START=1";
    println("Calling $url");

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $body,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
        )
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if (!is_array($response)) {
        println('Invalid response!');
        return;
    }

    println("Delivery fee: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

function testCreateStaff()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    /* init body data to post, absent "token" that to be added later */
    $body = '' .
        '{
            "data": {
                "avatar": "",
                "lastname": "bv",
                "firstname": "thuong",
                "email": "thuong.staff3@gmail.com",
                "telephone": "0386548739",
                "info": "",
                "user_group": "11"
            }
        }';

    /*
     * calculate token. Notice: json encode without space and \r\n characters
     * expected $bodyData as:
     * {
            "token": "72f3d3d6d6b3f9bc3dd6e8abff4fa4ca",
            "data": {
                "avatar": "",
                "lastname": "bv",
                "firstname": "thuong",
                "email": "thuong.staff3@gmail.com",
                "telephone": "0386548739",
                "info": "",
                "user_group": "11"
            }
        }
     */
    $bodyData = json_encode(json_decode($body, true));
    $token = md5("{$bodyData}{$apiKey}");
    $url = "{$baseUrl}staffs?XDEBUG_SESSION_START=1";
    println("Calling $url");
    println("Using token $token");

    /* set token to body */
    $body = json_decode($body, true);
    $body['token'] = $token;
    $body = json_encode($body);

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $body,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body)
        )
    ));

    $resp = curl_exec($curl);
    $response = json_decode($resp, true);
    curl_close($curl);
    if (!is_array($response) ||
        !array_key_exists('code', $response) ||
        !array_key_exists('message', $response)
    ) {
        println('Invalid response!');
        var_dump($resp);
        return;
    }

    println("Add staff response: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));

    if ($response['code'] == 200) {
        println("=> staff created successfully!");
    }
}

// /api/chatbot_novaon_v2/staffs?page=1&limit=15&staff_ids=6,7&token=f3bbbadbb12a1f158b51adc71c2dbacf
function testGetStaffsList()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    $page = 1;
    $limit = 15;
    $sort = 'username';
    $order = 'ASC';
    $token = md5("{$page}{$limit}{$sort}{$order}{$apiKey}");

    $url = "{$baseUrl}staffs?page={$page}&limit={$limit}&sort={$sort}&order={$order}&token=$token&XDEBUG_SESSION_START=1";
    println("Calling $url");
    println("Using token: $token");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $res = curl_exec($curl);
    $response = json_decode($res, true);

    if (!is_array($response) ||
        !array_key_exists('total', $response) ||
        !array_key_exists('page', $response) ||
        !array_key_exists('records', $response) ||
        !is_array($response['records'])
    ) {
        println('Invalid response! Got: ' . $res);
        return;
    }

    println("Staffs list: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

// /api/chatbot_novaon_v2/staffs
function testDeleteStaff()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    /* init body data to post, absent "token" that to be added later */
    $body = '' .
        '{
            "data": {
                "staff_id": 6,
                "action": "delete"
            }
        }';

    /*
     * calculate token. Notice: json encode without space and \r\n characters
     * expected $bodyData as:
     * {"data":{...}}
     */
    $bodyData = json_encode(json_decode($body, true));
    $token = md5("{$bodyData}{$apiKey}"); // 54d1dc2960f864ccf7e568a66866e26f
    $url = "{$baseUrl}staffs?XDEBUG_SESSION_START=1";
    println("Calling $url");

    /* set token to body */
    $body = json_decode($body, true);
    $body['token'] = $token;
    $body = json_encode($body);

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $body,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
        )
    ));

    $resp = curl_exec($curl);
    $response = json_decode($resp, true);
    curl_close($curl);

    if (!is_array($response) ||
        !array_key_exists('code', $response) ||
        !array_key_exists('message', $response)
    ) {
        println('Invalid response!');
        var_dump($resp);
        return;
    }

    println("Delete staff response: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));

    if ($response['code'] == 0) {
        println("=> staff deleted successfully!");
    }
}

// /api/chatbot_novaon_v2/categories?page=1&limit=15&staff_ids=6,7&token=f3bbbadbb12a1f158b51adc71c2dbacf
function testGetCategoriesList()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    $page = 1;
    $limit = 15;
    $staff_ids = '';
    $token = md5("{$page}{$limit}{$staff_ids}{$apiKey}");

    $url = "{$baseUrl}categories?page={$page}&limit={$limit}&category_ids={$staff_ids}&token=$token&XDEBUG_SESSION_START=1";
    println("Calling $url");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $res = curl_exec($curl);
    $response = json_decode($res, true);

    if (!is_array($response) ||
        !array_key_exists('total', $response) ||
        !array_key_exists('page', $response) ||
        !array_key_exists('records', $response) ||
        !is_array($response['records'])
    ) {
        println('Invalid response! Got: ' . $res);
        return;
    }

    println("Categories list: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

// /api/chatbot_novaon_v2/manufacturers?page=1&limit=15&staff_ids=6,7&token=f3bbbadbb12a1f158b51adc71c2dbacf
function testGetManufacturersList()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    $page = 1;
    $limit = 15;
    $staff_ids = '';
    $token = md5("{$page}{$limit}{$staff_ids}{$apiKey}");

    $url = "{$baseUrl}manufacturers?page={$page}&limit={$limit}&manufacturer_ids={$staff_ids}&token=$token&XDEBUG_SESSION_START=1";
    println("Calling $url");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $res = curl_exec($curl);
    $response = json_decode($res, true);

    if (!is_array($response) ||
        !array_key_exists('total', $response) ||
        !array_key_exists('page', $response) ||
        !array_key_exists('records', $response) ||
        !is_array($response['records'])
    ) {
        println('Invalid response! Got: ' . $res);
        return;
    }

    println("Manufacturerss list: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

function println($str)
{
    echo "$str\n";
}

function testGetCustomersList() {
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    $page = 1;
    $limit = 100;
    $filter_search = "";

    $phone = "";
    $phone_operator = "";

    $order = 1;
    $order_operator = "";
    $order_value = "";

    $order_latest = 1;
    $order_latest_operator = "AT";
    $order_latest_value = urlencode("2020-11-26 10:29:07");

    $amount = "";
    $amount_operator = "";
    $amount_value = "";

    $source = "";
    $source_value = "";

    $subscriber_id = "";

    $token = md5("{$page}{$limit}{$filter_search}{$phone}{$phone_operator}{$order}{$order_operator}{$order_value}{$order_latest}{$order_latest_operator}{$order_latest_value}{$amount}{$amount_operator}{$amount_value}{$source}{$source_value}{$subscriber_id}{$apiKey}");
    $url = "{$baseUrl}customers?page={$page}&limit={$limit}&filter_search={$filter_search}&phone={$phone}&phone_operator={$phone_operator}&order={$order}&order_operator={$order_operator}&order_value={$order_value}&order_latest={$order_latest}&order_latest_operator={$order_latest_operator}&order_latest_value={$order_latest_value}&amount={$amount}&amount_operator={$amount_operator}&amount_value={$amount_value}&source={$source}&source_value={$source_value}&subscriber_id={$subscriber_id}&token=$token&XDEBUG_SESSION_START=1";
    println("Calling $url");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $res = curl_exec($curl);
    $response = json_decode($res, true);

    if (!is_array($response) ||
        !array_key_exists('total', $response) ||
        !array_key_exists('page', $response) ||
        !array_key_exists('records', $response) ||
        !is_array($response['records'])
    ) {
        println('Invalid response! Got: ' . $res);
        return;
    }

    println("Products list: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

// /api/chatbot_novaon_v2/createCustomer
function testCreateCustomer()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    /* init body data to post, absent "token" that to be added later */
    $body = '' .
        '{
            "data": {
                "customer": {
                    "name": "nguyen Van A",
                    "phone_number": "09446324536",
                    "customer_source": 3,
                    "is_authenticated": 1,
                    "email": "test2@example.com",
                    "delivery_addr": "So 1 Ngo 2 Duy Tan 364",
                    "ward": "30373",
                    "district": "886",
                    "city": "89",
                    "note": "Goi hang can than va du so luong nhe shop. Thanks! 364"
                }
            }
        }';
    /*
     * calculate token. Notice: json encode without space and \r\n characters
     * expected $bodyData as:
     * {"data":{"customer":{"name":"Nguyen Thuy Linh","phone_number":"0987654321","email":"nguyenthuylinh@gmail.com","delivery_addr":"So 1 Ngo 2 Duy Tan","ward":"Dich Vong Hau","district":"Cau Giay","city":"Ha Noi","note":"Goi hang can than va du so luong nhe shop. Thanks!"},"products":[{"id":86,"quantity":2}]}}
     */
    $bodyData = json_encode(json_decode($body, true));
    $token = md5("{$bodyData}{$apiKey}"); // 54d1dc2960f864ccf7e568a66866e26f
    $url = "{$baseUrl}customers?XDEBUG_SESSION_START=1";
    println("Calling $url");

    /* set token to body */
    $body = json_decode($body, true);
    $body['token'] = $token;
    $body = json_encode($body);

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $body,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
        )
    ));

    $resp = curl_exec($curl);
    $response = json_decode($resp, true);
    curl_close($curl);

    if (!is_array($response) ||
        !array_key_exists('code', $response) ||
        !array_key_exists('message', $response)
    ) {
        println('Invalid response!');
        var_dump($resp);
        return;
    }

    println("Create customer response: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

function testProvinces() {
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;
    $token = md5("{$apiKey}");
    $url = "{$baseUrl}provinces?token=$token&XDEBUG_SESSION_START=1";
    println("Calling $url");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $res = curl_exec($curl);
    $response = json_decode($res, true);

    if (!is_array($response) ||
        !array_key_exists('total', $response) ||
        !array_key_exists('page', $response) ||
        !array_key_exists('records', $response) ||
        !is_array($response['records'])
    ) {
        println('Invalid response! Got: ' . $res);
        return;
    }

    println("Provinces list: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

function testDistricts() {
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;
    $province = "83";
    $token = md5("{$province}{$apiKey}");
    $url = "{$baseUrl}districts?province={$province}&token=$token&XDEBUG_SESSION_START=1";
    println("Calling $url");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $res = curl_exec($curl);
    $response = json_decode($res, true);

    if (!is_array($response) ||
        !array_key_exists('total', $response) ||
        !array_key_exists('page', $response) ||
        !array_key_exists('records', $response) ||
        !is_array($response['records'])
    ) {
        println('Invalid response! Got: ' . $res);
        return;
    }

    println("Districts list: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

function testWards() {
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;
    $district = "829";
    $token = md5("{$district}{$apiKey}");
    $url = "{$baseUrl}wards?district=$district&token=$token&XDEBUG_SESSION_START=1";
    println("Calling $url");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $res = curl_exec($curl);
    $response = json_decode($res, true);

    if (!is_array($response) ||
        !array_key_exists('total', $response) ||
        !array_key_exists('page', $response) ||
        !array_key_exists('records', $response) ||
        !is_array($response['records'])
    ) {
        println('Invalid response! Got: ' . $res);
        return;
    }

    println("Wards list: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

function testGetPaymentMethods() {
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;
    $token = md5("{$apiKey}");
    $url = "{$baseUrl}payment_methods?token=$token";
    println("Calling $url");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $res = curl_exec($curl);
    $response = json_decode($res, true);

    if (!is_array($response)) {
        println('Invalid response! Got: ' . $res);
        return;
    }

    println("Payment methods: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

function testGetShippingServices() {
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    $total_weight = '100';
    $package_length = '0';
    $package_width = '0';
    $package_height = '0';
    $from_district_id_ghn = '1312412'; // require for giao hang nhanh
    $total_pay = '742719';
    $transport_shipping_full_address = urlencode('ca ugiay han oi, Xã Nậm Hăn, Huyện Sìn Hồ, Tỉnh Lai Châu');
    $transport_shipping_wards = '03547';
    $transport_shipping_district = '108';
    $transport_shipping_province = '12';

    $token = md5("{$total_weight}{$package_length}{$package_width}{$package_height}{$from_district_id_ghn}{$total_pay}{$transport_shipping_full_address}{$transport_shipping_wards}{$transport_shipping_district}{$transport_shipping_province}{$apiKey}");
    $url = "{$baseUrl}shipping_services?total_weight={$total_weight}&package_length={$package_length}&package_width={$package_width}&package_height={$package_height}&from_district_id_ghn={$from_district_id_ghn}&total_pay={$total_pay}&transport_shipping_full_address={$transport_shipping_full_address}&transport_shipping_wards={$transport_shipping_wards}&transport_shipping_district={$transport_shipping_district}&transport_shipping_province={$transport_shipping_province}&token=$token&XDEBUG_SESSION_START=1";
    println("Calling $url");
    println('Using token: ' . $token);

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $res = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($res, true);

    if (!is_array($response)) {
        println('Invalid response! Got: ' . $res);
        return;
    }

    println("Shipping services: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

function testCreateDeliveryOrder()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    /* init body data to post, absent "token" that to be added later */
    $body = '' .
        '{
            "data": {
              "total_weight": "1000",
              "shipping_fee": 0,
              "shipping_method": "Free",
              "total_pay": "10000",
              "order_source": "shop",
              "payment_method": "198535319739A",
              "payment_status": 1,
              "ghn_district_hub": "203055",
              "shipping_district_code": "750",
              "order_id": "8",
              "transport_method": "Ghn",
              "service_id": 53321,
              "service_name": "Bay",
              "transport_shipping_fullname": "nguyen van a",
              "transport_shipping_phone": "0985485388",
              "transport_shipping_address": "fasfasfasfáfsa, Xã Bình Ba Huyện Châu Đức Tỉnh Bà Rịa - Vũng Tàu, Xã Bình Ba, Huyện Châu Đức, Tỉnh Bà Rịa - Vũng Tàu",
              "transport_shipping_wards": "26578",
              "transport_shipping_district": "750",
              "transport_shipping_province": "77",
              "collection_amount": "10000",
              "package_length": "10",
              "package_width": "10",
              "package_height": "10",
              "transport_note": "",
              "order_status": "7",
              "old_order_status_id": "7",
              "user_id": "1",
              "customer": "1",
              "customer_group_id": "1",
              "shipping_fullname": "nguyen van a",
              "shipping_phone": "0985485388",
              "shipping_address": "anhmt@novaon.net",
              "shipping_wards": "77",
              "shipping_district": "750",
              "shipping_province": "77",
              "shipping_bill": "#00000000007",
              "note": "",
              "delivery": "198535319739A",
              "customer_full_name": "nguyen van a",
              "customer_phone": "0985485388",
              "customer_email": "anhmt@novaon.net",
              "customer_province": "77",
              "customer_address": "fasfasfasfáfsa, Xã Bình Ba Huyện Châu Đức Tỉnh Bà Rịa - Vũng Tàu"
            }
        }';

    /*
     * calculate token. Notice: json encode without space and \r\n characters
     * expected $bodyData as:
     * {"data":{"total_weight":"100","shipping_fee":"0","shipping_method":"Free","total_pay":"742719","order_source":"shop","payment_method":"198535319739A","payment_status":"1","ghn_district_hub":"1312412","shipping_district_code":"108","order_id":"11","transport_method":"trans_ons_ghtk","service_id":"1854","service_name":"road","transport_shipping_fullname":"thuong test 1","transport_shipping_phone":"02342423","transport_shipping_address":"ca ugiay han oi","transport_shipping_wards":"03547","transport_shipping_district":"108","transport_shipping_province":"12","collection_amount":"742719","package_length":"1","package_width":"1","package_height":"1","transport_note":"bestme test ghn, ban dung giao hang nha","order_status":"7","old_order_status_id":"7","user_id":"1","customer":"3","customer_group_id":"","shipping_fullname":"thuong test 1","shipping_phone":"02342423","shipping_address":"ca ugiay han oi","shipping_wards":"03547","shipping_district":"108","shipping_province":"12","shipping_bill":"#00000000011","note":"","delivery":"-1","customer_full_name":"thuong test 1","customer_phone":"02342423","customer_email":"thuong.test1@mail.com","customer_province":"","customer_address":""},"token":"d1d9560c7ade57d3cc302ba3b691d4a4"}
     */
    $bodyData = json_encode(json_decode($body, true));
    $token = md5("{$bodyData}{$apiKey}");
    $url = "{$baseUrl}delivery_order?XDEBUG_SESSION_START=1";
    println("Calling $url");
    println("Using token $token");

    /* set token to body */
    $body = json_decode($body, true);
    $body['token'] = $token;
    $body = json_encode($body);

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $body,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body)
        )
    ));

    $resp = curl_exec($curl);
    $response = json_decode($resp, true);
    curl_close($curl);
    if (!is_array($response) ||
        !array_key_exists('code', $response) ||
        !array_key_exists('message', $response)
    ) {
        println('Invalid response!');
        var_dump($resp);
        return;
    }

    println("Create delivery order response: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));

    if ($response['code'] == 200) {
        println("=> delivery order created successfully!");
    }
}

function testCancelDeliveryOrder()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    /* init body data to post, absent "token" that to be added later */
    $body = '' .
        '{
            "data": {
                "transport_method": "trans_ons_ghtk",
                "transport_order_code": "S14570506.MB23.A15.649554278",
                "data_order_id": 8
            }
        }';

    /*
     * calculate token. Notice: json encode without space and \r\n characters
     * expected $bodyData as:
     * {"data": {"transport_method": "Ghn","transport_order_code": "GAU76PY9","data_order_id": 11, "token": "GAU76PY9GAU76PY9GAU76PY9GAU76PY9"}}
     */
    $bodyData = json_encode(json_decode($body, true));
    $token = md5("{$bodyData}{$apiKey}");
    $url = "{$baseUrl}delivery_order?XDEBUG_SESSION_START=1";
    println("Calling $url");
    println("Using token $token");

    /* set token to body */
    $body = json_decode($body, true);
    $body['token'] = $token;
    $body = json_encode($body);

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "PUT",
        CURLOPT_POSTFIELDS => $body,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body)
        )
    ));

    $resp = curl_exec($curl);
    $response = json_decode($resp, true);
    curl_close($curl);
    if (!is_array($response) ||
        !array_key_exists('code', $response) ||
        !array_key_exists('message', $response)
    ) {
        println('Invalid response!');
        var_dump($resp);
        return;
    }

    println("Cancel delivery order response: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));

    if ($response['code'] == 200) {
        println("=> delivery order created successfully!");
    }
}

function testGetStaff() {
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    $id = 5;

    $token = md5("{$id}{$apiKey}");
    $url = "{$baseUrl}staffs?id={$id}&token=$token&XDEBUG_SESSION_START=1";
    println("Calling $url");
    println('Using token: ' . $token);

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $res = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($res, true);

    if (!is_array($response)) {
        println('Invalid response! Got: ' . $res);
        return;
    }

    println("Staff detail: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

function testGetUserGroups() {
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    $token = md5("{$apiKey}");
    $url = "{$baseUrl}user_groups?token=$token&XDEBUG_SESSION_START=1";
    println("Calling $url");
    println('Using token: ' . $token);

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $res = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($res, true);

    if (!is_array($response)) {
        println('Invalid response! Got: ' . $res);
        return;
    }

    println("User group: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

function testGetDashboard() {
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    $start = urlencode('2020-11-01 00:00:00');
    $end = urlencode('2021-07-02 23:59:59');

    $token = md5("{$start}{$end}{$apiKey}");
    $url = "{$baseUrl}dashboard?start={$start}&end={$end}&token=$token&XDEBUG_SESSION_START=1";
    println("Calling $url");
    println('Using token: ' . $token);

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $res = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($res, true);

    if (!is_array($response)) {
        println('Invalid response! Got: ' . $res);
        return;
    }

    println("Dashboard info: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}