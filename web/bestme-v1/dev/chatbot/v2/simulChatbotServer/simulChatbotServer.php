<?php

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    println('<h2 style="text-align: center">Welcome to simul Chatbot Server</h2>');
    println('<p style="text-align: center">Please use POST method to send Registration result</p>');
    die;
}

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    println('<h2 style="text-align: center">Not supported! Use POST instead!</h2>');
    die;
}

$post = json_decode(file_get_contents('php://input'), true);
$params = [
    "code" => isset($post['code']) ? $post['code'] : '',
    "message" => isset($post['message']) ? $post['message'] : '',
    "data" => isset($post['data']) ? $post['data'] : ''
];

// simul delay
sleep(5);

$data = [
    'code' => 1,
    'message' => 'Callback success'
];

writeLog('Received callback params: ' . json_encode($params) . '. Now response: ' . json_encode($data) . PHP_EOL);

header('Content-Type: application/json');
echo json_encode($data);

function println($msg)
{
    echo "$msg" . PHP_EOL;
}

function writeLog($msg)
{
    $now = (new DateTime())->format('YmdHis');
    file_put_contents('./server.log', "[{$now}] {$msg}" . PHP_EOL, FILE_APPEND);
}