# Chatbot integration

### Document for integration with chatbot - Novaon. Powered by Bestme - v1.0

I. Summary
   
   - get products list
   
   - get product detail
   
   - create order

   - verify api key
   
   - get provinces list
   
   - get districts list by province
   
   - get wards list by districts
   
II. Detail

   **\* Notice:**
   
   - assume shop base url is "https://myshop.bestme.asia", then below will use sub path only
   
   - all requests MUST use "token" for security purpose. "token" is a string value, calculated by following way:
   
     - if method is GET: token = md5(param-1 . param-2 . API_KEY)
     
       e.g: path "/api/v1/products?page=1&limit=15&searchKey=ao%20nam&searchField=name", API_KEY=1234567890ABCDE
       
       => token = md5(115ao%20namname1234567890ABCDE)
     
     - if method is POST: token = md5(body-data . API_KEY). Notice: body-data WITHOUT "token"
     
     - how to validate request authorization:
     
       - step 1: re-calculate token by Bestme
       
         /api/v1/products?page=3&limit=15&searchKey=ao%20nam&searchField=name&token=ASDFGHJKL1234567890
         
         => calculated token = md5(115ao%20namname1234567890ABCDE) (not include "token" in checksum!)
       
       - step 2: compare calculated token with requested token (sent from chat bot)
       
         - if match: valid => continue processing
         
         - if not matched: invalid => return error code
     
   - API_KEY: the secure key is store in both Shop (owed by Bestme) and Chat Bot, used to calculate token for each request

   1. get products list
   
      - method: GET
      
      - url: /api/chatbot_novaon/products?page=3&limit=15&searchKey=ao%20nam&searchField=name&token=ASDFGHJKL1234567890
      
      - request:
      
        - header: "content-type": "application/json"
      
      - response:
      
        - type: application/json
        
        - value: json for products list. example format:
        
          ```products list
          {
              "total": 120,
              "page": 3,
              "records": [
                  {
                      "id": 1,
                      "name": "Ao phong nam H2T",
                      "description": "Ao phong nam H2T chat lieu dep",
                      "short_description": "Ao phong nam H2T chat lieu dep",
                      "original_price": 200000,
                      "sale_price": 195000,
                      "image": "https://novaon-cloud.vn/img/1/12345678-1.png"
                  },
                  {
                      "id": 2,
                      "product_version_id": 1,
                      "name": "Ao phong nu H2T",
                      "description": "Ao phong nu H2T chat lieu dep",
                      "short_description": "Ao phong nu H2T chat lieu dep",
                      "original_price": 250000,
                      "sale_price": 235000,
                      "image": "https://novaon-cloud.vn/img/2/12345678-2.png",
                      "attribute": {
                          "Mau sac": "Do",
                          "Size": "S"
                      }
                  },
                  {
                      "id": 2,
                      "product_version_id": 2,
                      "name": "Ao phong nu H2T",
                      "description": "Ao phong nu H2T chat lieu dep",
                      "short_description": "Ao phong nu H2T chat lieu dep",
                      "original_price": 250000,
                      "sale_price": 235000,
                      "image": "https://novaon-cloud.vn/img/2/12345678-2.png",
                      "attribute": {
                          "Mau sac": "Do",
                          "Size": "L"
                      }
                  }
              ]
          }
          
          ```
          
      - where:
      
        - page: page number. If absent or empty value ('') => not use, then param "limit" will be not used too
        
        - limit: the maximum records will be returned for that request. If absent or empty value ('') => default 15
        
        - searchKey: the search value, escaped. e.g "ao nam" => "ao%20nam"
        
        - searchField: the field to search, supported: "name" 
        
        - response:
        
          - total: total products (and match the search if provided)
          
          - page: current page. If empty ('') => no page used, return all products in records
          
          - records: contains products list (with summary info). If count of records < limit: reach the end of products list
      
   2. get product detail
   
      - method: GET
         
      - url: /api/chatbot_novaon/products?id=1&token=ASDFGHJKL1234567890
      
      - request:
      
        - header: "content-type": "application/json"
      
      - response:
      
        - type: application/json
        
        - value: json for products list. example format:
        
          ```product info
          {
              "id": 1,
              "product_version_id": 2, // optional, if exists => must include this value when POST to create order
              "name": "Ao phong nam H2T",
              "description": "Ao phong nam H2T chat lieu dep",
              "short_description": "Ao phong nam H2T chat lieu dep",
              "original_price": 200000,
              "sale_price": 195000,
              "image": "https://novaon-cloud.vn/img/1/12345678-1.png",
              "sku": "ASDFGH123456-1",
              "barcode": "12345678901"
          }
          ```
        
        - where:
              
            - product_id: the product id to get detail
            
        - notice:
              
            - product_version_id: the id related to specific version of product
   
   3. create order
   
      - method: POST
            
      - url: /api/chatbot_novaon/orders
      
      - request:
      
        - header: "content-type": "application/json"
      
      - body data: 
      
        ```body
        {
            "token": "ASDFGHJKL1234567890"
            "data": {
                "customer": {
                    "name": "Nguyen Thuy Linh",
                    "phone_number": "0987654321",
                    "email": "nguyenthuylinh@gmail.com",
                    "delivery_addr": "So 1 Ngo 2 Duy Tan",
                    "ward": "30280",
                    "district": "883",
                    "city": "89",
                    "note": "Goi hang can than va du so luong nhe shop. Thanks!"
                },
                "products": [
                    {
                        "id": 1,
                        "quantity": 2
                    },
                    {
                        "id": 2,
                        "quantity": 3,
                        'product_version_id' => 2 // include this if the product detail contains this
                    }
                ]
            }
        }
        ```
      
      - response:
      
        - type: application/json
        
        - value: json for products list. example format:
        
          ```response
              {
                  "code": 0,
                  "message": "order created successfully"
              }
          ]
          ```
        
        - where:
              
            - response "code":
            
              - 0: order is created successfully
              
              - 1: credential (api key) is verified
              
              - 100: missing customer
              
              - 101: customer info invalid (email, name, phone number, ...)
              
              - 102: products info invalid (id, quantity, ...)
              
              - 103: missing products
              
              - 201: unknown error
              
              - 400: token invalid
              
        - Notice: Order status is "Draft"
        
   4. verify api key
   
      - method: GET
         
      - url: /api/chatbot_novaon/verify?data=data_for_verifying_api_key&token=ASDFGHJKL1234567890
      
      - request:
      
        - header: "content-type": "application/json"
      
      - response:
      
        - type: application/json
        
        - value: json. example format:
                
          ```response
              {
                  "code": 1,
                  "message": "credential (api key) is verified"
              }
          ]
          ```
          
          other error code: 400: token invalid, ...
        
        - where:
              
            - data: data for verifying api key. e.g: "this data for trust api key..."

   5. get provinces list
   
      - method: GET
      
      - url: /api/chatbot_novaon/provinces
      
      - request:
      
        - header: "content-type": "application/json"
      
      - response:
      
        - type: application/json
        
        - value: json for provinces list. example format:
        
          ```provinces list
          [
              {
                  "name": "An Giang",
                  "name_with_type": "Tỉnh An Giang",
                  "code": "89"
              },
              {
                  "name": "Kon Tum",
                  "name_with_type": "Tỉnh Kon Tum",
                  "code": "62"
              },
              ...
          ]
          ```
      - note:
                     
        - Return full provinces json, no need pagination

   7. get districts list by province
      
      - method: GET
      
      - url: /api/chatbot_novaon/districts?province=PROVINCE_CODE
      
      - request:
      
        - header: "content-type": "application/json"
      
      - response:
      
        - type: application/json
        
        - value: json for districts list. example format:
        
          ```districts list
          [
              {
                  "name": "Long Xuyên",
                  "name_with_type": "Thành phố Long Xuyên",
                  "code": "883"
              },
              {
                  "name": "Châu Đốc",
                  "name_with_type": "Thành phố Châu Đốc",
                  "code": "884"
              },
              ...
          ]
          ```
      - where:
                     
        - province: province code (PROVINCE_CODE). e.g: "89" for "An Giang"

      - note:
                     
        - need province param in request to shorten districts list for result

   6. get wards list by district
      
      - method: GET
      
      - url: /api/chatbot_novaon/wards?district=DISTRICT_CODE
      
      - request:
      
        - header: "content-type": "application/json"
      
      - response:
      
        - type: application/json
        
        - value: json for products list. example format:
        
          ```wards list
          [
              {
                  "name": "Mỹ Bình",
                  "name_with_type": "Phường Mỹ Bình",
                  "code": "30280"
              },
              {
                  "name": "Mỹ Long",
                  "name_with_type": "Phường Mỹ Long",
                  "code": "30283"
              },
              ...
          ]
          ```
      - where:
                     
        - district: province code (DISTRICT_CODE). e.g: "883" for "Long Xuyên"

      - note:
                     
        - need district param in request to shorten wards list for result
              
III. Discussion

- manage order? => NO

- map provinces, districts, wards? => OK

