<?php

define('BESTME_CHATBOT_API_BASE_URL', 'http://x2.bestme.test/api/chatbot_novaon/');
define('BESTME_CHATBOT_API_KEY', 'die05PRUGxT1vCHHjFmzBOEbKIHbfwGG');

/* test verify api key */
testVefifyApiKey();

/* test get products list */
testGetProductsList();

/* test get product detail */
testGetProduct();

/* test create order */
testCreateOrder();

// /api/chatbot_novaon/verify?data=data-for-testing-credential-blala&token=05433efa664ebe43b124ff73591de08f
function testVefifyApiKey()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    $data = "data-for-testing-credential-blala";
    $token = md5("{$data}{$apiKey}");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "{$baseUrl}verify?data=$data&token=$token",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if (!is_array($response)) {
        println('Invalid response!');
        return;
    }

    if ($response['code'] != 1) {
        println("Invalid credentials! Code: {$response['code']}");
        return;
    }

    println("Trusted credentials!");
}

// /api/chatbot_novaon/products?page=1&limit=15&searchKey=&searchField=name&token=f3bbbadbb12a1f158b51adc71c2dbacf
function testGetProductsList()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    $page = 1;
    $limit = 15;
    $searchKey = '';
    $searchField = 'name';
    $token = md5("{$page}{$limit}{$searchKey}{$searchField}{$apiKey}");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "{$baseUrl}products?page={$page}&limit={$limit}&searchKey={$searchField}&searchField={$searchKey}&token=$token",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if (!is_array($response) ||
        !array_key_exists('total', $response) ||
        !array_key_exists('page', $response) ||
        !array_key_exists('records', $response) ||
        !is_array($response['records'])
    ) {
        println('Invalid response!');
        return;
    }

    println("Products list: " . json_encode($response, JSON_PRETTY_PRINT));
}

// /api/chatbot_novaon/products?id=1&token=9ddde6630ff0ea2bc9c79507f316c88a
function testGetProduct()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    $productId = 86;
    $token = md5("{$productId}{$apiKey}");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "{$baseUrl}products?id={$productId}&token=$token",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if (!is_array($response) ||
        !array_key_exists('id', $response) ||
        !array_key_exists('name', $response) // and check more...
    ) {
        println('Invalid response!');
        return;
    }

    println("Product detail: " . json_encode($response, JSON_PRETTY_PRINT));
}

// /api/chatbot_novaon/orders
function testCreateOrder()
{
    $baseUrl = BESTME_CHATBOT_API_BASE_URL;
    $apiKey = BESTME_CHATBOT_API_KEY;

    /* init body data to post, absent "token" that to be added later */
    $body = '' .
        '{
            "data": {
                "customer": {
                    "name": "Nguyen Thuy Linh",
                    "phone_number": "0987654321",
                    "email": "nguyenthuylinh@gmail.com",
                    "delivery_addr": "So 1 Ngo 2 Duy Tan",
                    "ward": "30373",
                    "district": "886",
                    "city": "89",
                    "note": "Goi hang can than va du so luong nhe shop. Thanks!"
                },
                "products": [
                    {
                        "id": 75,
                        "quantity": 2
                    },
                    {
                        "id": 75,
                        "quantity": 3
                    },
                    {
                        "id": 81,
                        "quantity": 6
                    },
                    {
                        "id": 89,
                        "quantity": 7
                    }
                ]
            }
        }';

    /*
     * calculate token. Notice: json encode without space and \r\n characters
     * expected $bodyData as:
     * {"data":{"customer":{"name":"Nguyen Thuy Linh","phone_number":"0987654321","email":"nguyenthuylinh@gmail.com","delivery_addr":"So 1 Ngo 2 Duy Tan","ward":"Dich Vong Hau","district":"Cau Giay","city":"Ha Noi","note":"Goi hang can than va du so luong nhe shop. Thanks!"},"products":[{"id":86,"quantity":2}]}}
     */
    $bodyData = json_encode(json_decode($body, true));
    $token = md5("{$bodyData}{$apiKey}"); // 54d1dc2960f864ccf7e568a66866e26f

    /* set token to body */
    $body = json_decode($body, true);
    $body['token'] = $token;
    $body = json_encode($body);

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "{$baseUrl}orders?XDEBUG_SESSION_START=1",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $body,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
        )
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);
    curl_close($curl);

    if (!is_array($response) ||
        !array_key_exists('code', $response) ||
        !array_key_exists('message', $response)
    ) {
        println('Invalid response!');
        return;
    }

    println("Create order: code={$response['code']}, message={$response['message']}");

    if ($response['code'] == 0) {
        println("=> order created successfully!");
    }
}

function println($str)
{
    echo "$str\n";
}



