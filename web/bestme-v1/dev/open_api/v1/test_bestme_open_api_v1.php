<?php

define('BESTME_OPEN_API_BASE_URL', 'http://x2.bestme.test/api/open_api_v1/');
define('BESTME_OPEN_API_KEY', 'xxx');

//define('BESTME_OPEN_API_BASE_URL', 'http://123.30.146.248:8686/api/open_api_v2/');
//define('BESTME_OPEN_API_KEY', 'xxx');

//define('BESTME_OPEN_API_BASE_URL', 'https://dunght42.mybestme.net/api/open_api_v2/');
//define('BESTME_OPEN_API_KEY', 'xxx');

//define('BESTME_OPEN_API_BASE_URL', 'https://dunght42.bestme.asia/api/open_api_v2/');
//define('BESTME_OPEN_API_KEY', 'xxx');

/* test verify api key */
testVefifyApiKey();

/* test get products list */
testGetProductsList();

/* test get product detail */
testGetProduct();

// /api/open_api_v1/verify?data=data-for-testing-credential-blala&token=05433efa664ebe43b124ff73591de08f
function testVefifyApiKey()
{
    $baseUrl = BESTME_OPEN_API_BASE_URL;
    $apiKey = BESTME_OPEN_API_KEY;

    $data = "data-for-testing-credential-blala";
    $token = md5("{$data}{$apiKey}");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "{$baseUrl}verify?data=$data&token=$token",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if (!is_array($response)) {
        println('Invalid response!');
        return;
    }

    if ($response['code'] != 1) {
        println("Invalid credentials! Code: {$response['code']}");
        return;
    }

    println("Trusted credentials!");
}

// /api/open_api_v1/products?page=1&limit=15&searchKey=&searchField=name&token=f3bbbadbb12a1f158b51adc71c2dbacf
function testGetProductsList()
{
    $baseUrl = BESTME_OPEN_API_BASE_URL;
    $apiKey = BESTME_OPEN_API_KEY;

    $page = 1;
    $limit = 15;
    $searchKey = '';
    $searchField = 'name';
    $token = md5("{$page}{$limit}{$searchKey}{$searchField}{$apiKey}");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "{$baseUrl}products?page={$page}&limit={$limit}&searchKey={$searchField}&searchField={$searchKey}&token=$token",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if (!is_array($response) ||
        !array_key_exists('total', $response) ||
        !array_key_exists('page', $response) ||
        !array_key_exists('records', $response) ||
        !is_array($response['records'])
    ) {
        println('Invalid response!');
        return;
    }

    println("Products list: " . json_encode($response, JSON_PRETTY_PRINT));
}

// /api/open_api_v1/products?id=1&token=9ddde6630ff0ea2bc9c79507f316c88a
function testGetProduct()
{
    $baseUrl = BESTME_OPEN_API_BASE_URL;
    $apiKey = BESTME_OPEN_API_KEY;

    $productId = 86;
    $token = md5("{$productId}{$apiKey}");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "{$baseUrl}products?id={$productId}&token=$token",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if (!is_array($response) ||
        !array_key_exists('id', $response) ||
        !array_key_exists('name', $response) // and check more...
    ) {
        println('Invalid response!');
        return;
    }

    println("Product detail: " . json_encode($response, JSON_PRETTY_PRINT));
}

function println($str)
{
    echo "$str\n";
}



