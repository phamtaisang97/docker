<?php

define('BESTME_OPEN_API_BASE_URL', 'http://x2.bestme.test/api/open_api_v2/');
define('BESTME_OPEN_API_KEY', 'VsPRbLLCFUNoQ0GX11mnDnlTY4KTlLln');

//define('BESTME_OPEN_API_BASE_URL', 'http://123.30.146.248:8686/api/open_api_v2/');
//define('BESTME_OPEN_API_KEY', 'xxx');

//define('BESTME_OPEN_API_BASE_URL', 'https://dunght42.mybestme.net/api/open_api_v2/');
//define('BESTME_OPEN_API_KEY', 'xxx');

//define('BESTME_OPEN_API_BASE_URL', 'https://dunght42.bestme.asia/api/open_api_v2/');
//define('BESTME_OPEN_API_KEY', 'xxx');

$choose = 5;

if (is_array($argv) && count($argv) > 1) {
    $param_case = str_replace('case=', '', $argv[1]);
    $choose = (int)$param_case;
}

$allCases = false;

switch ($choose) {
    case 99:
        $allCases = true;

    case 1:
        /* test verify api key */
        testVefifyApiKey();
        if (!$allCases) {
            break;
        }

    case 2:
        /* test get products list */
        testGetProductsList();
        if (!$allCases) {
            break;
        }

    case 3:
        /* test get product detail */
        testGetProduct();
        if (!$allCases) {
            break;
        }

    case 4:
        /* test get orders list */
        testGetOrdersList();
        if (!$allCases) {
            break;
        }

    case 5:
        /* test get order detail */
        testGetOrder();
        if (!$allCases) {
            break;
        }
}

// /api/open_api_v2/verify?data=data-for-testing-credential-blala&token=05433efa664ebe43b124ff73591de08f
function testVefifyApiKey()
{
    $baseUrl = BESTME_OPEN_API_BASE_URL;
    $apiKey = BESTME_OPEN_API_KEY;

    $data = "data-for-testing-credential-blala";
    $token = md5("{$data}{$apiKey}");
    $url = "{$baseUrl}verify?data=$data&token=$token";
    println("Calling $url");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if (!is_array($response)) {
        println('Invalid response!');
        return;
    }

    if ($response['code'] != 1) {
        println("Invalid credentials! Code: {$response['code']}");
        return;
    }

    println("Trusted credentials!");
}

// /api/open_api_v2/products?page=1&limit=15&searchKey=&searchField=name&product_ids=123_13,123_14&except_product_ids=98,103_1&ver=2.1&token=f3bbbadbb12a1f158b51adc71c2dbacf
function testGetProductsList()
{
    $baseUrl = BESTME_OPEN_API_BASE_URL;
    $apiKey = BESTME_OPEN_API_KEY;

    $page = 1;
    $limit = 15;
    $searchKey = ''; //urlencode('sandal quai');
    $searchField = 'name';
    $product_ids = '123_13,123_14';
    $except_product_ids = '94,102,103_1,103_3';
    $ver = '2.1';
    $token = md5("{$page}{$limit}{$searchKey}{$searchField}{$product_ids}{$except_product_ids}{$apiKey}");
    $url = "{$baseUrl}products?page={$page}&limit={$limit}&searchKey={$searchKey}&searchField={$searchField}&product_ids={$product_ids}&except_product_ids={$except_product_ids}&ver={$ver}&token=$token&XDEBUG_SESSION_START=1";
    println("Calling $url");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $res = curl_exec($curl);
    $response = json_decode($res, true);

    if (!is_array($response) ||
        !array_key_exists('total', $response) ||
        !array_key_exists('page', $response) ||
        !array_key_exists('records', $response) ||
        !is_array($response['records'])
    ) {
        println('Invalid response! Got: ' . $res);
        return;
    }

    println("Products list: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

// /api/open_api_v2/products?id=1&token=9ddde6630ff0ea2bc9c79507f316c88a
function testGetProduct()
{
    $baseUrl = BESTME_OPEN_API_BASE_URL;
    $apiKey = BESTME_OPEN_API_KEY;

    $productId = 145;
    $productVersionId = ''; // or null or ''
    $token = md5("{$productId}{$productVersionId}{$apiKey}");
    $url = "{$baseUrl}products?id={$productId}&product_version_id=$productVersionId&token=$token&XDEBUG_SESSION_START=1";
    println("Calling $url");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if (!is_array($response) ||
        !array_key_exists('id', $response) ||
        !array_key_exists('name', $response) // and check more...
    ) {
        println('Invalid response!');
        return;
    }

    println("Product detail: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

// /api/open_api_v2/orders?page=1&limit=15&customer_ref_id=1234&order_ids=213,216&token=f3bbbadbb12a1f158b51adc71c2dbacf
function testGetOrdersList()
{
    $baseUrl = BESTME_OPEN_API_BASE_URL;
    $apiKey = BESTME_OPEN_API_KEY;

    $page = 1;
    $limit = 15;
    $customer_ref_id = '';
    $order_ids = '';
    $order_tag = ''; //urlencode('tag1');
    $order_utm_key = ''; //urlencode('campaign');
    $order_utm_value = ''; //urlencode('Campaign 1, Campaign 2');
    $token = md5("{$page}{$limit}{$customer_ref_id}{$order_ids}{$order_tag}{$order_utm_key}{$order_utm_value}{$apiKey}");

    $url = "{$baseUrl}orders?page={$page}&limit={$limit}&customer_ref_id={$customer_ref_id}&order_tag={$order_tag}&order_ids={$order_ids}&order_utm_key={$order_utm_key}&order_utm_value={$order_utm_value}&token=$token&XDEBUG_SESSION_START=1";
    println("Calling $url");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $res = curl_exec($curl);
    $response = json_decode($res, true);

    if (!is_array($response) ||
        !array_key_exists('total', $response) ||
        !array_key_exists('page', $response) ||
        !array_key_exists('records', $response) ||
        !is_array($response['records'])
    ) {
        println('Invalid response! Got: ' . $res);
        return;
    }

    println("Orders list: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

// /api/open_api_v2/orders?id=1&token=9ddde6630ff0ea2bc9c79507f316c88a
function testGetOrder()
{
    $baseUrl = BESTME_OPEN_API_BASE_URL;
    $apiKey = BESTME_OPEN_API_KEY;

    $orderId = 0;
    $orderCode = urlencode('#00000000130'); // #00000000130 => %2300000000130

    if (!empty($orderId)) {
        $token = md5("{$orderId}{$apiKey}");
        $url = "{$baseUrl}orders?id={$orderId}&token=$token&XDEBUG_SESSION_START=1";
    } else {
        $token = md5("{$orderCode}{$apiKey}");
        $url = "{$baseUrl}orders?order_code={$orderCode}&token=$token&XDEBUG_SESSION_START=1";
    }

    println("Calling $url");

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if (!is_array($response) ||
        !array_key_exists('customer', $response) ||
        !array_key_exists('products', $response) // and check more...
    ) {
        println('Invalid response!');
        return;
    }

    println("Order detail: " . json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

function println($str)
{
    echo "$str\n";
}



