# Bestme Open Api integration

### Document for integration with Bestme Open Api. Powered by Bestme - v2.0

I. Summary
   
   - get products list
   
   - get product detail
   
   - get orders list
   
   - get order detail

   - verify api key
   
   - get provinces list
   
   - get districts list by province
   
   - get wards list by districts
   
II. Detail

   **\* Notice:**
   
   - assume shop base url is "https://myshop.bestme.asia", then below will use sub path only
   
   - all requests MUST use "token" for security purpose. "token" is a string value, calculated by following way:
   
     - if method is GET: token = md5(param-1 . param-2 . API_KEY)
     
       e.g: path "/api/v1/products?page=1&limit=15&searchKey=ao%20nam&searchField=name", API_KEY=1234567890ABCDE
       
       => token = md5(115ao%20namname1234567890ABCDE)
     
     - if method is POST: token = md5(body-data . API_KEY). Notice: body-data WITHOUT "token"
     
     - how to validate request authorization:
     
       - step 1: re-calculate token by Bestme
       
         /api/v1/products?page=3&limit=15&searchKey=ao%20nam&searchField=name&token=ASDFGHJKL1234567890
         
         => calculated token = md5(115ao%20namname1234567890ABCDE) (not include "token" in checksum!)
       
       - step 2: compare calculated token with requested token (sent from chat bot)
       
         - if match: valid => continue processing
         
         - if not matched: invalid => return error code
     
   - API_KEY: the secure key is store in both Shop (owed by Bestme) and Chat Bot, used to calculate token for each request

   1. get products list
   
      - method: GET
      
      - url: /api/open_api_v2/products?page=3&limit=15&searchKey=ao%20nam&searchField=name&except_product_ids=12,23_1,23_3&ver=1.1&token=ASDFGHJKL1234567890
      
      - request:
      
        - header: "content-type": "application/json"
        
        - where:
              
          - page: page number. If absent or empty value ('') => not use, then param "limit" will be not used too
          
          - limit: the maximum records will be returned for that request. If absent or empty value ('') => default 15
          
          - searchKey: the search value, escaped. e.g "ao nam" => "ao+nam"
          
          - searchField: the field to search, supported: "name" 
          
          - except_product_ids: the product ids (and may be product version ids) to be excepted, multiple values split by comma (,). 
            e.g 12,13_1,13_3,... where:
            
            - 12: product id
            - 13_1, 13_3: product id 13 and it's version id 1, 3
            
          - ver: api version. support: 1.0|1.1, or let empty for default "1.0"
            - 1.0: flat product versions
            - 1.0: group product versions
      
      - response:
      
        - type: application/json
        
        - value: json for products list. example format:
        
          ```
          {
              "total": 120,
              "page": 3,
              "records": [
                  {
                      "id": 1,
                      "name": "Ao phong nam H2T",
                      "description": "Ao phong nam H2T chat lieu dep",
                      "short_description": "Ao phong nam H2T chat lieu dep",
                      "original_price": 200000,
                      "sale_price": 195000,
                      "image": "https://novaon-cloud.vn/img/1/12345678-1.png",
                      "quantity": "12",
                      "default_store": {
                          "id": "0",
                          "name": "Kho trung tâm"
                      },
                      "sku": "JK34J5K7H34J1"
                  },
                  {
                      "id": 2,
                      "product_version_id": 1,
                      "name": "Ao phong nu H2T",
                      "description": "Ao phong nu H2T chat lieu dep",
                      "short_description": "Ao phong nu H2T chat lieu dep",
                      "original_price": 250000,
                      "sale_price": 235000,
                      "image": "https://novaon-cloud.vn/img/2/12345678-2.png",
                      "attribute": {
                          "Mau sac": "Do",
                          "Size": "S"
                      },
                      "quantity": "12",
                      "default_store": {
                          "id": "0",
                          "name": "Kho trung tâm"
                      },
                      "sku": "JK34J5K7H34J2"
                  },
                  {
                      "id": 2,
                      "product_version_id": 2,
                      "name": "Ao phong nu H2T",
                      "description": "Ao phong nu H2T chat lieu dep",
                      "short_description": "Ao phong nu H2T chat lieu dep",
                      "original_price": 250000,
                      "sale_price": 235000,
                      "image": "https://novaon-cloud.vn/img/2/12345678-2.png",
                      "attribute": {
                          "Mau sac": "Do",
                          "Size": "L"
                      },
                      "quantity": "12",
                      "default_store": {
                          "id": "0",
                          "name": "Kho trung tâm"
                      },
                      "sku": "JK34J5K7H34J3"
                  }
              ]
          }
          
          ```
          
      - where:
      
        - total: total products (and match the search if provided)
        
        - page: current page. If empty ('') => no page used, return all products in records
        
        - records: contains products list (with summary info). If count of records < limit: reach the end of products list
        
        - 20200420: return more: 
        
          ```
          "quantity": "12",
          "default_store": {
              "id": "0",
              "name": "Kho trung tâm"
          }
          ```
          
          where: 
          
            - quantity of product in default store
             
            - default_store: setting in product form at Bestme admin 
      
   2. get product detail
   
      - method: GET
         
      - url: /api/open_api_v2/products?id=1&ver=1.1&token=ASDFGHJKL1234567890
      
      - request:
      
        - header: "content-type": "application/json"
        
        - where:
          - ver: api version. support: 1.0|1.1, or let empty for default "1.0"
            - 1.0: flat product versions
            - 1.0: group product versions
      
      - response:
      
        - type: application/json
        
        - value: json for products list. example format:
        
          ```
          {
              "id": 2,
              "product_version_id": 2,
              "name": "Ao phong nu H2T",
              "description": "Ao phong nu H2T chat lieu dep",
              "short_description": "Ao phong nu H2T chat lieu dep",
              "original_price": 250000,
              "sale_price": 235000,
              "image": "https://novaon-cloud.vn/img/2/12345678-2.png",
              "attribute": {
                  "Mau sac": "Do",
                  "Size": "L"
              },
              "quantity": "12",
              "default_store": {
                  "id": "0",
                  "name": "Kho trung tâm"
              },
              "sku": "JK34J5K7H34J5"
          }
          ```
        
        - where:
              
            - product_id: the product id to get detail
        
            - 20200420: return more: 
            
              ```
              "quantity": "12",
              "default_store": {
                  "id": "0",
                  "name": "Kho trung tâm"
              }
              ```
              
              where: 
              
                - quantity of product in default store
                 
                - default_store: setting in product form at Bestme admin
            
        - notice:
              
            - product_version_id: the id related to specific version of product
   
   3. get orders list
   
      - method: GET
                  
      - url: /api/open_api_v2/orders?page=3&limit=15&customer_ref_id=1234&order_ids=213,216
             &order_tag=tag1,tag2&order_utm_key=campaign&order_utm_value=Campaign+1,Campaign+2&token=ASDFGHJKL1234567890
      
      - request:
      
        - header: "content-type": "application/json"
        
        - where: 
        
          - page: page number. If absent or empty value ('') => not use, then param "limit" will be not used too
                  
          - limit: the maximum records will be returned for that request. If absent or empty value ('') => default 15
          
          - customer_ref_id: optional, for marking reference between 3rd party customer and Bestme's order
          
          - order_ids: the orders ids to be returned, multiple values split by comma (,). e.g 213,216,...
          
          - order_tag: tag list to be filtered, multiple values split by comma (,). e.g tag1,tag2,..., need urlencode
          
          - order_utm_key: utm key to be filtered, need urlencode
          
          - order_utm_value: utm value list to be filtered, multiple values split by comma (,). e.g Campaign+1,Campaign+2,..., need urlencode
      
      - response:
      
        - type: application/json
        
        - value: json. empty if not found. order by "order_id" DESC. example format:
                
          ```
          {
              "total": 120,
              "page": 3,
              "total_revenue": 820000,
              "records": [
                  {
                      "customer": {
                          "name": "Nguyen Thuy Linh",
                          "phone_number": "0987654321",
                          "email": "nguyenthuylinh@gmail.com",
                          "delivery_addr": "So 1 Ngo 2 Duy Tan",
                          "full_delivery_addr": "So 1 Ngo 2 Duy Tan, Phuong Dich Vong, Quan Cau Giay, Ha Noi",
                          "ward": "30280",
                          "district": "883",
                          "city": "89",
                          "note": "Goi hang can than va du so luong nhe shop. Thanks!"
                      },
                      "status": 6,
                      "customer_ref_id": "1234",
                      "order_id": 213,
                      "order_code": "#00000000015",
                      "total": 280000,
                      "date_added": "2020-02-29 15:22:09",
                      "date_modified": "2020-03-05 08:39:04",
                      "products": [
                          {
                              "id": 1,
                              "name": "Ao phong nam H2T",
                              "description": "Ao phong nam H2T chat lieu dep",
                              "short_description": "Ao phong nam H2T chat lieu dep",
                              "original_price": 200000,
                              "sale_price": 195000,
                              "image": "https://novaon-cloud.vn/img/1/12345678-1.png",
                              "quantity": 2
                          },
                          {
                              "id": 2,
                              "name": "Ao phong nam H2T",
                              "description": "Ao phong nam H2T chat lieu dep",
                              "short_description": "Ao phong nam H2T chat lieu dep",
                              "original_price": 200000,
                              "sale_price": 195000,
                              "image": "https://novaon-cloud.vn/img/1/12345678-1.png",
                              "quantity": 3
                          },
                          ...
                      ],
                      "payment_trans": "-2",
                      "payment_trans_custom_name": "Shop tự vận chuyển",
                      "payment_trans_custom_fee": 25000,
                      "payment_status": 1,
                      "delivery_fee": 25000
                  },
                  ...
              ]
          }
          ```
          
          - where:
          
            - total: total orders (and match the search if provided)
            
            - page: current page. If empty ('') => no page used, return all orders in records
            
            - total_revenue: total revenue of orders due to current filter conditions (regardless pagination)
            
            - records: contains orders list (with summary info). If count of records < limit: reach the end of orders list
            
            - status: 6=draft, 7=processing, 8=delivering, 9=complete, 10=cancel
            
            - payment_trans: delivery code
              - -2: custom fee, then 2 need additional 2 keys: payment_trans_custom_name (string), payment_trans_custom_fee (int)
              - -3: free (fee = 0)
              - not -2 and -3: delivery id (got from delivery list api). e.g: -1, 1, 2, 3, ...
            
            - payment_status:
              - 1: paid
              - 0: not yet paid
              
            - delivery_fee: delivery fee due to payment_trans above
   
   4. get order detail
   
      - method: GET
               
      - if by order id: url: /api/open_api_v2/orders?id=123&token=ASDFGHJKL1234567890
               
      - if by order code: url: /api/open_api_v2/orders?order_code=#000123&token=ASDFGHJKL1234567890
      
        note: order_code need to be urlencode, e.g: #00000000130 => %2300000000130
      
      - request:
      
        - header: "content-type": "application/json"
      
      - response:
      
        - type: application/json
        
        - value: json. empty if not found. example format:
                
          ```
          {
              "customer": {
                  "name": "Nguyen Thuy Linh",
                  "phone_number": "0987654321",
                  "email": "nguyenthuylinh@gmail.com",
                  "delivery_addr": "So 1 Ngo 2 Duy Tan",
                  "ward": "30280",
                  "district": "883",
                  "city": "89",
                  "note": "Goi hang can than va du so luong nhe shop. Thanks!"
              },
              "status": 6,
              "customer_ref_id": "",
              "order_id": 213,
              "order_code": "#00000000213",
              "total": 280000,
              "date_added": "2020-02-29 15:22:09",
              "date_modified": "2020-03-05 08:39:04",
              "products": [
                  {
                      "id": 1,
                      "name": "Ao phong nam H2T",
                      "description": "Ao phong nam H2T chat lieu dep",
                      "short_description": "Ao phong nam H2T chat lieu dep",
                      "original_price": 200000,
                      "sale_price": 195000,
                      "image": "https://novaon-cloud.vn/img/1/12345678-1.png",
                      "quantity": 2
                  },
                  {
                      "id": 2,
                      "name": "Ao phong nam H2T",
                      "description": "Ao phong nam H2T chat lieu dep",
                      "short_description": "Ao phong nam H2T chat lieu dep",
                      "original_price": 200000,
                      "sale_price": 195000,
                      "image": "https://novaon-cloud.vn/img/1/12345678-1.png",
                      "quantity": 3
                  },
                  ...
              ],
              "payment_trans": "-3",
              "payment_trans_custom_name": "Shop tự vận chuyển",
              "payment_trans_custom_fee": 25000,
              "payment_status": 1,
              "delivery_fee": 25000
          }
          ```
   
   5. verify api key
   
      - method: GET
         
      - url: /api/open_api_v2/verify?data=data_for_verifying_api_key&token=ASDFGHJKL1234567890
      
      - request:
      
        - header: "content-type": "application/json"
      
      - response:
      
        - type: application/json
        
        - value: json. example format:
                
          ```response
              {
                  "code": 1,
                  "message": "credential (api key) is verified"
              }
          ]
          ```
          
          other error code: 400: token invalid, ...
        
        - where:
              
            - data: data for verifying api key. e.g: "this data for trust api key..."

   6. get provinces list
   
      - method: GET
      
      - url: /api/open_api_v2/provinces
      
      - request:
      
        - header: "content-type": "application/json"
      
      - response:
      
        - type: application/json
        
        - value: json for provinces list. example format:
        
          ```provinces list
          [
              {
                  "name": "An Giang",
                  "name_with_type": "Tỉnh An Giang",
                  "code": "89"
              },
              {
                  "name": "Kon Tum",
                  "name_with_type": "Tỉnh Kon Tum",
                  "code": "62"
              },
              ...
          ]
          ```
      - note:
                     
        - Return full provinces json, no need pagination

   7. get districts list by province
      
      - method: GET
      
      - url: /api/open_api_v2/districts?province=PROVINCE_CODE
      
      - request:
      
        - header: "content-type": "application/json"
      
      - response:
      
        - type: application/json
        
        - value: json for districts list. example format:
        
          ```districts list
          [
              {
                  "name": "Long Xuyên",
                  "name_with_type": "Thành phố Long Xuyên",
                  "code": "883"
              },
              {
                  "name": "Châu Đốc",
                  "name_with_type": "Thành phố Châu Đốc",
                  "code": "884"
              },
              ...
          ]
          ```
      - where:
                     
        - province: province code (PROVINCE_CODE). e.g: "89" for "An Giang"

      - note:
                     
        - need province param in request to shorten districts list for result

   8. get wards list by districts
      
      - method: GET
      
      - url: /api/open_api_v2/wards?district=DISTRICT_CODE
      
      - request:
      
        - header: "content-type": "application/json"
      
      - response:
      
        - type: application/json
        
        - value: json for products list. example format:
        
          ```wards list
          [
              {
                  "name": "Mỹ Bình",
                  "name_with_type": "Phường Mỹ Bình",
                  "code": "30280"
              },
              {
                  "name": "Mỹ Long",
                  "name_with_type": "Phường Mỹ Long",
                  "code": "30283"
              },
              ...
          ]
          ```
      - where:
                     
        - district: province code (DISTRICT_CODE). e.g: "883" for "Long Xuyên"

      - note:
                     
        - need district param in request to shorten wards list for result

III. Discussion

- map provinces, districts, wards? => OK

