<?php

/* setup */
// Version
define('VERSION', '3.0.2.0');

// Configuration
if (is_file(__DIR__ . '/../config.php')) {
    require_once(__DIR__ . '/../config.php');
}

/* connect */
// Create connection
$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
writeln("Connected successfully");

/* enable UTF8 for connection */
mysqli_query($conn, "SET NAMES 'utf8'");
mysqli_query($conn, "SET CHARACTER SET utf8");
mysqli_query($conn, "SET CHARACTER_SET_CONNECTION=utf8");
mysqli_query($conn, "SET SQL_MODE = ''");

/* get data */
$sql = "SELECT * FROM " . DB_PREFIX . "report";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    writeln("{$result->num_rows} results:");

    // output data of each row
    $isFirst = true;
    $currentRow = 0;
    while ($row = $result->fetch_assoc()) {
        // columns
        if ($isFirst) {
            $isFirst = false;
            writeln(implode(', ', array_keys($row)));
        }

        // records
        writeln(implode(', ', $row));

        $currentRow++;
        if ($currentRow > 10) {
            writeln('... more');
            break;
        }
    }
} else {
    writeln("0 results");
}

writeln("");

/* generate test dashboard report */
// report_time, report_date, type, type_description, d_field, m_value, last_updated
// '2019-03-16 15:13:17', '2019-03-16', 1, 'Doanh thu', , 115000000, '2019-03-16 15:14:21'
$reports = [];

$START_DATE = date_create_from_format('Y-m-d', '2019-04-16');
$END_DATE = date_create_from_format('Y-m-d', '2019-04-19');
$HOURS = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23];

$dateInterval = $END_DATE->diff($START_DATE);
$datePeriod = new DatePeriod($START_DATE, new DateInterval('P1D'), $END_DATE);
foreach ($datePeriod as $date) {
    /** @var DateTime $date */
    foreach ($HOURS as $h) {
        // revenue
        /** @var DateTime $date */
        $reports[] = [
            $date->setTime($h, 0, 0)->format('Y-m-d H:i:s'),
            $date->format('Y-m-d'),
            1,
            'Doanh thu',
            '',
            rand(0, (10e6 - 1)) * 1000,
            $date->format('Y-m-d H:i:s')
        ];

        // orders
        /** @var DateTime $date */
        $reports[] = [
            $date->setTime($h, 0, 0)->format('Y-m-d H:i:s'),
            $date->format('Y-m-d'),
            2,
            'Số lượng đơn',
            '',
            rand(0, (10e4 - 1)),
            $date->format('Y-m-d H:i:s')
        ];

        // new customers
        /** @var DateTime $date */
        $reports[] = [
            $date->setTime($h, 0, 0)->format('Y-m-d H:i:s'),
            $date->format('Y-m-d'),
            3,
            'Khách hàng mới',
            '',
            rand(0, (10e4 - 1)),
            $date->format('Y-m-d H:i:s')
        ];

        // top 5 sale
        /** @var DateTime $date */
        $reports[] = [
            $date->setTime($h, 0, 0)->format('Y-m-d H:i:s'),
            $date->format('Y-m-d'),
            4,
            'Top 5 sản phẩm bán chạy',
            'Điện thoại iPhone Xs Max 512GB,Máy tính bảng Samsung Galaxy Tab A 10.5 inch,Laptop Dell Inspiron 3576 i5 8250U/4GB/1TB/Win10/(P63F002N76F),Dây cáp Lightning 1.2 m Devia Aex
,Apple Watch S3 GPS 42mm viền nhôm xám dây cao su màu đen MTF32VN/A',
            implode(',', [
                rand(0, (10e4 - 1)),
                rand(0, (10e4 - 1)),
                rand(0, (10e4 - 1)),
                rand(0, (10e4 - 1)),
                rand(0, (10e4 - 1))
            ]),
            $date->format('Y-m-d H:i:s')
        ];
    }
}

if (1 == 0) {
    writeln('generated reports:');
    foreach ($reports as $report) {
        writeln(implode(', ', $report));
    }
    writeln("");
}

$valueArr = [];
foreach ($reports as $report) {
    foreach ($report as &$r) {
        $r = sprintf("'%s'", mysqli_real_escape_string($conn, $r));
    }
    unset($r);

    $valueArr[] = sprintf('(%s)', implode(', ', $report));
}

/* insert */
$sql = "INSERT INTO " . DB_PREFIX . "report (`report_time`, `report_date`, `type`, `type_description`, `d_field`, `m_value`, `last_updated`)
VALUES " . implode(', ', $valueArr);

if ($conn->query($sql) === TRUE) {
    writeln("New " . count($valueArr) . " records created successfully");
} else {
    writeln("Error: " . $conn->error);
}

/* close connection */
$conn->close();

/* = = = = = = = = = = = = = = = = */

function writeln($message)
{
    echo sprintf("[%s] %s%s", (new DateTime())->format('YmdHis'), $message, PHP_EOL);
}