<?php

$SEED = 5;
$RAND_TIMES = 100;
$RAND_MIN = 1;
$RAND_MAX = 10e16;
$CHECK_TIMES = 100;

writeln('Test random multi times with same Seed');

$previous_result = '';
$success = true;
$startTime = microtime();
for ($t = 0; $t < $CHECK_TIMES; $t++) {
    writelnSeparate();
    writeln("Turn: $t");

    $rand_1 = [];
    srand($SEED);
    for ($i = 0; $i < $RAND_TIMES; $i++) {
        $rand_1[] = getRand();
    }

    $current_result = implode(' - ', $rand_1);
    writeln("  rand_$t: $current_result");

    /* compare value */
    $same = $current_result === $previous_result;
    if (!empty($previous_result) && !$same) {
        writeln('Not Same');
        $success = false;
        break;
    }

    writeln('Result: Same');
    $previous_result = empty($current_result) ? 'ERROR' : $current_result;
}
$endTime = microtime();

writelnSeparate();
writeln('Test random multi times with same Seed: ' . ($success ? 'SUCCESS' : 'FAILED') . ' after ' . round(($endTime - $startTime) * 1000, 0) . 'ms');

###

function getRand()
{
    global $RAND_MIN;
    global $RAND_MAX;
    return rand($RAND_MIN, $RAND_MAX);
}

function writelnSeparate()
{
    writeln('--------------------------------------------------');
}

function writeln($message)
{
    echo sprintf("[%s] %s%s", (new DateTime())->format('YmdHis'), $message, PHP_EOL);
}