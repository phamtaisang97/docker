<?php


$dir = '/var/www/html/opencart/catalog/view/theme/beauty_xoiza/template';

/* travel dir */
$structure = read_dir_content($dir);

/* print preview */
println("<ul>" . $structure . "</ul>");

/* write to file */
file_put_contents('output.html', $structure);

function read_dir_content($parent_dir, $depth = 0)
{
    $str_result = "";

    $str_result .= "<li>\n    " . pathinfo($parent_dir, PATHINFO_BASENAME) . "\n</li>\n";
    $str_result .= "<ul>\n    ";
    if ($handle = opendir($parent_dir)) {
        while (false !== ($file = readdir($handle))) {
            if (in_array($file, array('.', '..'))) continue;
            if (is_dir($parent_dir . "/" . $file)) {
                $str_result .= "<li>\n    " . read_dir_content($parent_dir . "/" . $file, $depth++) . "\n</li>\n";
            }
            $str_result .= "<li>\n    {$file}\n</li>\n";
        }
        closedir($handle);
    }
    $str_result .= "</ul>\n";

    return $str_result;
}

function println($str)
{
    echo "$str\n";
}
