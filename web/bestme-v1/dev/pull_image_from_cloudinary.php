<?php
require_once __DIR__ . './../system/cronjob/cronjob_init.php';

$db_hostname = DB_HOSTNAME;
$db_port = DB_PORT;
$db_username = DB_USERNAME;
$db_password = DB_PASSWORD;
$db_database = DB_DATABASE;
$db_prefix = DB_PREFIX;
$db_driver = DB_DRIVER;

/* run function pull images */
doDevPullImages($db_hostname, $db_port, $db_username, $db_password, $db_database, $db_prefix, $db_driver);

/**
 * do Pull Images
 *
 * @param string $db_hostname
 * @param string $db_port
 * @param string $db_username
 * @param string $db_password
 * @param string $db_database
 * @param string $db_prefix
 * @param string $db_driver
 * @param string $shop_name
 */
function doDevPullImages($db_hostname, $db_port, $db_username, $db_password, $db_database, $db_prefix, $db_driver)
{
    $db = null;
    try {
        $db = new DB($db_driver,
            htmlspecialchars_decode($db_hostname),
            htmlspecialchars_decode($db_username),
            htmlspecialchars_decode($db_password),
            htmlspecialchars_decode($db_database),
            htmlspecialchars_decode($db_port)
        );
    } catch (Exception $e) {
        println(sprintf('Pull images for db "%s"... failed. Error: %s', $db_database, $e->getMessage()));
        return;
    }

    // get shopname
    $shop_name_object = $db->query("SELECT `value` FROM `{$db_prefix}setting` WHERE `code` = 'config' AND `key` = 'shop_name' LIMIT 1");
    $shop_name = $shop_name_object->row['value'];

    println(sprintf('Pull images for shop "%s"...', $shop_name));

    global $image_pulled;
    $image_pulled = array();
    $last_images_count = 0;

    pullDevImageInProductThumb($db, $db_prefix, $shop_name);
    println(sprintf('Pull product thumb images for shop "%s" done! Number of images: %s', $shop_name, count($image_pulled) - $last_images_count));
    $last_images_count = count($image_pulled);

    pullDevImageInProductImages($db, $db_prefix, $shop_name);
    println(sprintf('Pull product images images for shop "%s" done! Number of images: %s', $shop_name, count($image_pulled) - $last_images_count));
    $last_images_count = count($image_pulled);

    pullDevImageInCollection($db, $db_prefix, $shop_name);
    println(sprintf('Pull collection thumb images for shop "%s" done! Number of images: %s', $shop_name, count($image_pulled) - $last_images_count));
    $last_images_count = count($image_pulled);

    pullDevImageInProductCategory($db, $db_prefix, $shop_name);
    println(sprintf('Pull category thumb images for shop "%s" done! Number of images: %s', $shop_name, count($image_pulled) - $last_images_count));
    $last_images_count = count($image_pulled);

    pullDevImageInBlog($db, $db_prefix, $shop_name);
    println(sprintf('Pull blog thumb images for shop "%s" done! Number of images: %s', $shop_name, count($image_pulled) - $last_images_count));
    $last_images_count = count($image_pulled);

    pullDevImageInBuilder($db, $db_prefix, $shop_name);
    println(sprintf('Pull builder images for shop "%s" done! Number of images: %s', $shop_name, count($image_pulled) - $last_images_count));

    println(sprintf('Pull images for shop "%s"... done. Total: %s', $shop_name, count($image_pulled)));
}

function pullDevImageInProductThumb(DB $db, $db_prefix, $shop_name, $limit = 100, $offset = 0)
{

    $product_thumbs = $db->query("SELECT `product_id`, `image` FROM `{$db_prefix}product` WHERE `deleted` IS NULL ORDER BY `product_id` LIMIT {$limit} OFFSET {$offset}");

    foreach ($product_thumbs->rows as $image) {
        if (!array_key_exists('product_id', $image) || !array_key_exists('image', $image)) {
            continue;
        }

        $new_url = uploadToBestmeServer($image['image'], $shop_name);
        if ($new_url) {
            $db->query("UPDATE `{$db_prefix}product` SET `image` = '$new_url' WHERE `product_id` = " . $image['product_id']);
        }
    }

    if ($product_thumbs->num_rows >= $limit) {
        pullDevImageInProductThumb($db, $db_prefix, $shop_name, $limit, $offset + $limit);
    }
}

function pullDevImageInProductImages(DB $db, $db_prefix, $shop_name, $limit = 100, $offset = 0)
{
    $product_images = $db->query("SELECT `product_image_id`, `image` FROM `{$db_prefix}product_image` ORDER BY `product_image_id` LIMIT {$limit} OFFSET {$offset}");

    foreach ($product_images->rows as $image) {
        if (!array_key_exists('product_image_id', $image) || !array_key_exists('image', $image)) {
            continue;
        }

        $new_url = uploadToBestmeServer($image['image'], $shop_name);
        if ($new_url) {
            $db->query("UPDATE `{$db_prefix}product_image` SET `image` = '$new_url' WHERE `product_image_id` = " . $image['product_image_id']);
        }
    }

    if ($product_images->num_rows >= $limit) {
        pullDevImageInProductImages($db, $db_prefix, $shop_name, $limit, $offset + $limit);
    }
}

function pullDevImageInCollection(DB $db, $db_prefix, $shop_name, $limit = 100, $offset = 0)
{
    $collection_icons = $db->query("SELECT `collection_id`, `image` FROM `{$db_prefix}collection_description` ORDER BY `collection_id` LIMIT {$limit} OFFSET {$offset}");

    foreach ($collection_icons->rows as $image) {
        if (!array_key_exists('collection_id', $image) || !array_key_exists('image', $image)) {
            continue;
        }

        $new_url = uploadToBestmeServer($image['image'], $shop_name);
        if ($new_url) {
            $db->query("UPDATE `{$db_prefix}collection_description` SET `image` = '$new_url' WHERE `collection_id` = " . $image['collection_id']);
        }
    }

    if ($collection_icons->num_rows >= $limit) {
        pullDevImageInCollection($db, $db_prefix, $shop_name, $limit, $offset + $limit);
    }
}

function pullDevImageInProductCategory(DB $db, $db_prefix, $shop_name, $limit = 100, $offset = 0)
{
    $category_icons = $db->query("SELECT `category_id`, `image` FROM `{$db_prefix}category` ORDER BY `category_id` LIMIT {$limit} OFFSET {$offset}");

    foreach ($category_icons->rows as $image) {
        if (!array_key_exists('category_id', $image) || !array_key_exists('image', $image)) {
            continue;
        }

        $new_url = uploadToBestmeServer($image['image'], $shop_name);
        if ($new_url) {
            $db->query("UPDATE `{$db_prefix}category` SET `image` = '$new_url' WHERE `category_id` = " . $image['category_id']);
        }
    }

    if ($category_icons->num_rows >= $limit) {
        pullDevImageInProductCategory($db, $db_prefix, $shop_name, $limit, $offset + $limit);
    }
}

function pullDevImageInBlog(DB $db, $db_prefix, $shop_name, $limit = 100, $offset = 0)
{
    $blog_thumbs = $db->query("SELECT `blog_id`, `image` FROM `{$db_prefix}blog_description` ORDER BY `blog_id` LIMIT {$limit} OFFSET {$offset}");

    foreach ($blog_thumbs->rows as $image) {
        if (!array_key_exists('blog_id', $image) || !array_key_exists('image', $image)) {
            continue;
        }

        $new_url = uploadToBestmeServer($image['image'], $shop_name);
        if ($new_url) {
            $db->query("UPDATE `{$db_prefix}blog_description` SET `image` = '$new_url' WHERE `blog_id` = " . $image['blog_id']);
        }
    }

    if ($blog_thumbs->num_rows >= $limit) {
        pullDevImageInBlog($db, $db_prefix, $shop_name, $limit, $offset + $limit);
    }
}

function pullDevImageInBuilder(DB $db, $db_prefix, $shop_name)
{
    // header logo
    $header_dataes = $db->query("SELECT `id`, `config` FROM `{$db_prefix}theme_builder_config` WHERE `key` = 'config_section_header'");
    foreach ($header_dataes->rows as $header_data) {
        if (!array_key_exists('id', $header_data) || !array_key_exists('config', $header_data)) {
            continue;
        }
        $data = $header_data['config'];
        $data = json_decode($data, true);
        if (isset($data['logo']['url'])) {
            $new_url = uploadToBestmeServer($data['logo']['url'], $shop_name);
            if ($new_url) {
                $data['logo']['url'] = $new_url;
                $data = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG);
                $data = $db->escape($data);
                $db->query("UPDATE `{$db_prefix}theme_builder_config` SET `config` = '$data' WHERE `id` = " . $header_data['id']);
            }
        }
    }
    unset($data, $new_url, $header_dataes);

    // slide show
    $slide_show_datas = $db->query("SELECT `id`, `config` FROM `{$db_prefix}theme_builder_config` WHERE `key` = 'config_section_slideshow'");
    foreach ($slide_show_datas->rows as $slide_show_data) {
        if (!array_key_exists('id', $slide_show_data) || !array_key_exists('config', $slide_show_data)) {
            continue;
        }
        $data = $slide_show_data['config'];
        $data = json_decode($data, true);
        if (isset($data['display'])) {
            if (is_array($data['display'])) {
                $flag = false;
                foreach ($data['display'] as &$image) {
                    if (isset($image['image-url'])) {
                        $new_url = uploadToBestmeServer($image['image-url'], $shop_name);
                        if ($new_url) {
                            $flag = true;
                            $image['image-url'] = $new_url;
                        }
                    }
                }
                unset($image);
                if ($flag) {
                    $data = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG);
                    $data = $db->escape($data);
                    $db->query("UPDATE `{$db_prefix}theme_builder_config` SET `config` = '$data' WHERE `id` = " . $slide_show_data['id']);
                }
            }
        }
    }
    unset($data, $new_url, $flag, $slide_show_data, $slide_show_datas);

    // banner
    $banner_datas = $db->query("SELECT `id`, `config` FROM `{$db_prefix}theme_builder_config` WHERE `key` = 'config_section_banner'");
    foreach ($banner_datas->rows as $banner_data) {
        if (!array_key_exists('id', $banner_data) || !array_key_exists('config', $banner_data)) {
            continue;
        }
        $data = $banner_data['config'];
        $data = json_decode($data, true);
        if (isset($data['display'])) {
            if (is_array($data['display'])) {
                $flag = false;
                foreach ($data['display'] as &$image) {
                    if (isset($image['image-url'])) {
                        $new_url = uploadToBestmeServer($image['image-url'], $shop_name);
                        if ($new_url) {
                            $flag = true;
                            $image['image-url'] = $new_url;
                        }
                    }
                }
                unset($image);
                if ($flag) {
                    $data = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG);
                    $data = $db->escape($data);
                    $db->query("UPDATE `{$db_prefix}theme_builder_config` SET `config` = '$data' WHERE `id` = " . $banner_data['id']);
                }
            }
        }
    }
    unset($data, $new_url, $flag, $banner_data, $banner_datas);

    // partner
    $partner_datas = $db->query("SELECT `id`, `config` FROM `{$db_prefix}theme_builder_config` WHERE `key` = 'config_section_partner'");
    foreach ($partner_datas->rows as $partner_data) {
        if (!array_key_exists('id', $partner_data) || !array_key_exists('config', $partner_data)) {
            continue;
        }
        $data = $partner_data['config'];
        $data = json_decode($data, true);
        if (isset($data['display'])) {
            if (is_array($data['display'])) {
                $flag = false;
                foreach ($data['display'] as &$image) {
                    if (isset($image['image-url'])) {
                        $new_url = uploadToBestmeServer($image['image-url'], $shop_name);
                        if ($new_url) {
                            $flag = true;
                            $image['image-url'] = $new_url;
                        }
                    }
                }
                unset($image);
                if ($flag) {
                    $data = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG);
                    $data = $db->escape($data);
                    $db->query("UPDATE `{$db_prefix}theme_builder_config` SET `config` = '$data' WHERE `id` = " . $partner_data['id']);
                }
            }
        }
    }
    unset($data, $new_url, $flag, $partner_data, $partner_datas);

    // list product banner
    $list_product_banner_dataes = $db->query("SELECT `id`, `config` FROM `{$db_prefix}theme_builder_config` WHERE `key` = 'config_section_category_banner'");
    foreach ($list_product_banner_dataes->rows as $list_product_banner_data) {
        if (!array_key_exists('id', $list_product_banner_data) || !array_key_exists('config', $list_product_banner_data)) {
            continue;
        }
        $data = $list_product_banner_data['config'];
        $data = json_decode($data, true);
        if (isset($data['display'])) {
            if (is_array($data['display'])) {
                $flag = false;
                foreach ($data['display'] as &$image) {
                    if (isset($image['image-url'])) {
                        $new_url = uploadToBestmeServer($image['image-url'], $shop_name);
                        if ($new_url) {
                            $flag = true;
                            $image['image-url'] = $new_url;
                        }
                    }
                }
                unset($image);
                if ($flag) {
                    $data = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG);
                    $data = $db->escape($data);
                    $db->query("UPDATE `{$db_prefix}theme_builder_config` SET `config` = '$data' WHERE `id` = " . $list_product_banner_data['id']);
                }
            }
        }
    }
    unset($data, $new_url, $flag, $list_product_banner_data, $list_product_banner_dataes);

    $favicon_dataes = $db->query("SELECT `id`, `config` FROM `{$db_prefix}theme_builder_config` WHERE `key` = 'config_theme_favicon'");
    foreach ($favicon_dataes->rows as $favicon_data) {
        if (!array_key_exists('id', $favicon_data) || !array_key_exists('config', $favicon_data)) {
            continue;
        }
        $data = $favicon_data['config'];
        $data = json_decode($data, true);
        if (isset($data['image']['url'])) {
            $new_url = uploadToBestmeServer($data['image']['url'], $shop_name);
            if ($new_url) {
                $data['image']['url'] = $new_url;
                $data = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG);
                $data = $db->escape($data);
                $db->query("UPDATE `{$db_prefix}theme_builder_config` SET `config` = '$data' WHERE `id` = " . $favicon_data['id']);
            }
        }
    }
    unset($data, $new_url, $flag, $favicon_data, $favicon_dataes);

    $content_customize_dataes = $db->query("SELECT `id`, `config` FROM `{$db_prefix}theme_builder_config` WHERE `key` = 'config_section_content_customize'");
    foreach ($content_customize_dataes->rows as $content_customize_data) {
        if (!array_key_exists('id', $content_customize_data) || !array_key_exists('config', $content_customize_data)) {
            continue;
        }
        $data = $content_customize_data['config'];
        $data = json_decode($data, true);
        if (isset($data['display'])) {
            $need_update = false;
            foreach ($data['display'] as &$display) {
                if (isset($display['content']['icon'])) {
                    $new_url = uploadToBestmeServer($display['content']['icon'], $shop_name);
                    if ($new_url) {
                        $need_update = true;
                        $display['content']['icon'] = $new_url;
                    }
                }
            }

            if ($need_update) {
                $data = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG);
                $data = $db->escape($data);
                $db->query("UPDATE `{$db_prefix}theme_builder_config` SET `config` = '$data' WHERE `id` = " . $content_customize_data['id']);
            }
        }
    }
    unset($data, $new_url, $flag, $content_customize_data, $content_customize_dataes);
}

function uploadToBestmeServer($url, $shop_name, $image_query_object = [])
{
    $url = trim($url);
    $shop_name = trim($shop_name);
    if (!$url) {
        return false;
    }

    global $image_pulled;
    if (array_key_exists($url, $image_pulled)) {
        return $image_pulled[$url];
    }

    if (filter_var($url, FILTER_VALIDATE_URL) == FALSE) {
        return false;
    }

    if (!isCloudinaryImage($url)) {
        return false;
    }

    $fields = array(
        "shop_name" => $shop_name,
        "image_url" => $url
    );

    /** @var \Image\Bestme_Image_Manager $image_server */
    $image_server = Image_Server::getImageServer('bestme');
    $result = $image_server->upload_by_url($fields);

    if (array_key_exists('url', $result)) {
        if (trim($result['url'])) {
            $image_pulled[$url] = $result['url'];

            // update image size in images table
            if (!empty($image_query_object)) {
                $image_query_object['db']->query("UPDATE `{$image_query_object['db_prefix']}images` 
                                                  SET `size` = {$result['size']} 
                                                  WHERE `image_id` = " . $image_query_object['image_id']);
            }

            return $result['url'];
        }
    }

    // return default url
    $default_url = 'https://cdn.bestme.asia/images/x2/placeholder.png';
    $image_pulled[$url] = $default_url;
    return $default_url;
}

function isCloudinaryImage($url)
{
    if (strpos($url, '//res.cloudinary.com/') !== false) {
        return true;
    }

    return false;
}