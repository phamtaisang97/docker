<?php
$data = array(
    "pick_province" => "Thái Bình", // dia chi gui hang
    "pick_district" => "Huyện Đông Hưng", // dia chi gui hang
    "province" => "Hồ Chí Minh", // dia chi nhan hang
    "district" => "Quận 9", // dia chi nhan hang
    "address" => "P.503 tòa nhà Auu Việt, số 1 Lê Đức Thọ",
    "weight" => 1000,
    "value" => 3000000,
//    "transport" => "fly"
//    "transport" => "road"
);
$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "https://services.giaohangtietkiem.vn/services/shipment/fee?" . http_build_query($data),
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_HTTPHEADER => array(
        "Token: C5Ac7d1cb73c6603F5946093Fdc70c096bB05647",
    ),
));

$response = json_decode(curl_exec($curl));
curl_close($curl);

echo 'Response: ' . $response;
?>