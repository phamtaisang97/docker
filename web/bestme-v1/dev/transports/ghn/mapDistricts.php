<?php
const DIR_SYSTEM = "D:/xampp/htdocs/novaon_x2/system/";
const JSON_FILE_DISTRICT = DIR_SYSTEM . 'library/vietnam_administrative/district.json';
const JSON_FILE_DISTRICT_CONVERT = DIR_SYSTEM . 'library/localisation/ghn/ghn_districts.php';
mapDistricts();
function mapDistricts()
{
    $result = [];
    $onShopDistricts = [];
    try {
        $districtsRaw = file_get_contents(JSON_FILE_DISTRICT);
        $onShopDistricts = json_decode($districtsRaw, true);
    } catch (Exception $e) {

    }
    $order = '
        {
            "token": "5d52278d34ef8800072b5f96"
        }';

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://console.ghn.vn/api/v1/apiv3/GetDistricts",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $order,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($order),
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);
    $ghnDistricts = $response['data'];
    foreach ($onShopDistricts as $district) {
        $bestme_district_name = mb_strtolower($district['name']);
        $bestme_district_name_with_type = mb_strtolower($district['name_with_type']);
        $bestme_district_name_for_check = [
            escapeVietnamese($bestme_district_name),
            escapeVietnamese("Huyen $bestme_district_name"),
            escapeVietnamese($bestme_district_name_with_type),
        ];
        foreach ($ghnDistricts as $ghnDistrict) {
            $district_name = mb_strtolower($ghnDistrict['DistrictName']);
            if (in_array(escapeVietnamese($district_name), $bestme_district_name_for_check)) {
                $result[$district['code']] = $ghnDistrict['DistrictID'];
            }
        }
    }
    file_put_contents(JSON_FILE_DISTRICT_CONVERT, '<?php $arr = ' . var_export($result, true) . ';');
}
function escapeVietnamese($str)
{
    $str = trim(mb_strtolower($str));
    $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
    $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
    $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
    $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
    $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
    $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
    $str = preg_replace('/(đ)/', 'd', $str);
    // $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
    // $str = preg_replace('/([\s]+)/', '-', $str);

    return $str;
}