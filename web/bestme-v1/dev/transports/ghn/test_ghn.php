<?php
/**
 * Function:  signIn()
 * Đăng nhập tài khoản để lấy token
 * Tham khảo: https://api.ghn.vn/home/docs/detail?id=39
 */
$token = getToken();

/**
 * Function:  signIn()
 * Đăng nhập tài khoản để lấy token
 * Tham khảo: https://api.ghn.vn/home/docs/detail?id=39
 */
$token = signIn();

/**
 * Function:  getDistricts()
 * Lấy danh sách địa chỉ tỉnh thành cho:
 * 1: địa chỉ gửi hàng.
 * 2. địa chỉ giao hàng
 * Tham khảo: https://api.ghn.vn/home/docs/detail?id=26
 */
$getDistricts = getDistricts();

/**
 * Function:  getWards()
 * Lấy danh sách địa chỉ Quận huyện cho:
 * 1: địa chỉ gửi hàng.
 * 2. địa chỉ giao hàng
 * Tham khảo: https://api.ghn.vn/home/docs/detail?id=40
 */
$getWards = getWards($getDistricts);

/**
 * Function:  getFindAvailableServices()
 * Lấy thông tin các dịnh vụ có sẵn GHN
 * Tham khảo: https://api.ghn.vn/home/docs/detail?id=27
 */
$FromDistricts = getDistricts();
$ToDistricts = getDistricts();

$listServices = getFindAvailableServices($FromDistricts, $ToDistricts);

/**
 * Function:  createOrder($token)
 * Tạo đơn hàng
 * Tham khảo: https://api.ghn.vn/home/docs/detail?id=28
 */
$createOrder = createOrder($token);

/**
 * Function:  orderDetail($token, $orderDetail)
 * Xem chi tiết 1 đơn hàng bất kỳ đã có OrderCode
 * Tham khảo: https://api.ghn.vn/home/docs/detail?id=29
 */
//$orderDetail = orderDetail($token, $orderDetail);

/**
 * Function:  UpdateOrder($token, $orderDetail)
 * Cập nhật 1 đơn hàng bất kỳ đã có OrderCode
 * Tham khảo: https://api.ghn.vn/home/docs/detail?id=30
 */
//$updateOrder = UpdateOrder($token, $orderDetail);


/**
 * Function:  CancelOrder($token, $OrderCode)
 * Hủy 1 đơn hàng bất kỳ đã có OrderCode
 * Tham khảo: https://api.ghn.vn/home/docs/detail?id=32
 */
//CancelOrder($token, $orderDetail);

function getToken()
{
    $order = '{
          "phone": "dokhanhhung95@gmail.com",
          "password": "123456789",
          "app_name": "apiv3"
        }';

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://online-gateway.ghn.vn/sso/public-api/client/login",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $order,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($order),
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);

    $response = json_decode($response, true);

    if (!is_array($response) || !array_key_exists('code', $response) || !array_key_exists('data', $response)) {
        return false;
    }

    if ($response['code'] != 200) {
        return false;
    }

    if (!is_array($response['data']) || !array_key_exists('url', $response['data']) || !array_key_exists('token', $response['data'])) {
        return false;
    }

    /* account url */
    $account_url = $response['data']['url'] . $response['data']['token'];

    if (1==1) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $account_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
    }

    if (1==1) {
        $context = stream_context_create(
            array(
                'http' => array(
                    'follow_location' => false
                )
            )
        );

        $html = file_get_contents($account_url, false, $context);
    }

    if (1==1) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $account_url);
        curl_setopt($ch, CURLOPT_HEADER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $a = curl_exec($ch);
        if(preg_match('#Location: (.*)#', $a, $r)) {
            $l = trim($r[1]);
            $account_url = 'https://api.ghn.vn/home/account';

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $account_url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);
        }
    }

    return $token = '';
}

/**
 * Function:  CancelOrder($token, $OrderCode)
 * Hủy 1 đơn hàng bất kỳ đã có OrderCode
 * Tham khảo: https://api.ghn.vn/home/docs/detail?id=32
 */
//CancelOrder($token, $orderDetail);

function signIn()
{
    $order = '{
            "token": "2f3511ba20e146f1bbe6338fb5a70197",
            "Email": "dokhanhhung95@gmail.com",
            "Password": "123456789"
        }';

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://console.ghn.vn/api/v1/apiv3/SignIn",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $order,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($order),
        ),
    ));

    $response = curl_exec($curl);

    $response = json_decode($response, true);

    if ($response['code'] != 1) {
        return false;
    }

    if (!is_array($response)) {
        return false;
    }

    return $token = [
        'ClientID' => $response['data']['ClientID'],
        'Token' => $response['data']['Token']
    ];

    curl_close($curl);
}

function getDistricts()
{
    $order = '{
            "token": "2f3511ba20e146f1bbe6338fb5a70197"
        }';

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://console.ghn.vn/api/v1/apiv3/GetDistricts",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $order,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($order),
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if ($response['code'] != 1) {
        return false;
    }

    if (!is_array($response)) {
        return false;
    }

    // Vi du: lay gia tri dau tien: Quận 12
    return $token = [
        'Code' => $response['data'][0]['Code'],
        'DistrictID' => $response['data'][0]['DistrictID'],
        'DistrictName' => $response['data'][0]['DistrictName'],
        'ProvinceID' => $response['data'][0]['ProvinceID'],
        'ProvinceName' => $response['data'][0]['ProvinceName']
    ];

    curl_close($curl);
}

function getWards($getDistricts)
{
    $order = '{
                "token": "2f3511ba20e146f1bbe6338fb5a70197",
                "DistrictID": ' . $getDistricts['DistrictID'] . '
              }';

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://console.ghn.vn/api/v1/apiv3/GetWards",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $order,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($order),
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if ($response['code'] != 1) {
        return false;
    }

    if (!is_array($response)) {
        return false;
    }

    // Vi du: lay gia tri dau tien: Phường Thạnh Xuân
    return $token = [
        'WardCode' => $response['data']['Wards'][0]['WardCode'],
        'WardName' => $response['data']['Wards'][0]['WardName'],
        'DistrictCode' => $response['data']['Wards'][0]['DistrictCode'],
        'ProvinceID' => $response['data']['Wards'][0]['ProvinceID'],
        'DistrictID' => $response['data']['Wards'][0]['DistrictID']
    ];

    curl_close($curl);
}

function getFindAvailableServices($FromDistricts, $ToDistricts)
{
    $order = '{
                "token": "2f3511ba20e146f1bbe6338fb5a70197",
                "Weight": 10000,
                "Length": 10,
                "Width": 110,
                "Height": 20,
                "FromDistrictID": ' . $FromDistricts['DistrictID'] . ',
                "ToDistrictID": ' . $ToDistricts['DistrictID'] . '
            }';

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://console.ghn.vn/api/v1/apiv3/FindAvailableServices",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $order,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($order),
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if ($response['code'] != 1) {
        return false;
    }

    if (!is_array($response)) {
        return false;
    }

    // lấy dịch vụ Vi du: dịch vụ đầu tiên: Chuẩn
    return $token = [
        'ServiceFee' => $response['data'][0]['ServiceFee'],
        'ServiceID' => $response['data'][0]['ServiceID'],
        'ShippingOrderCosts_ServiceID' => $response['data'][0]['Extras'][0]['ServiceID']
    ];

    curl_close($curl);
}

function createOrder($token)
{
    $FromDistrictID = 1454;   // get $FromDistricts = getDistricts();
    $FromWardCode = 21203;   // get $getWards = getWards($FromDistricts);

    $ToDistrictID = 1453;   // get $ToDistrictID = getDistricts();
    $ToWardCode = 21110;   // get $getWards = getWards($ToDistrictID);

    $ServiceID = 53320;   // get $ServiceID = getFindAvailableServices($FromDistricts, $ToDistricts)
    $ShippingOrderCosts_ServiceID = 53331;   // get $ShippingOrderCosts_ServiceID = getFindAvailableServices($FromDistricts, $ToDistricts)

    $order = '{
        "token": "' . $token['Token'] . '",
        "PaymentTypeID": 1,
        "FromDistrictID": ' . $FromDistrictID . ',
        "FromWardCode": "' . $FromWardCode . '",
        "ToDistrictID": ' . $ToDistrictID . ',
        "ToWardCode": "' . $ToWardCode . '",
        "Note": "Tạo ĐH qua API",
        "SealCode": "",
        "ExternalCode": "",
        "ClientContactName": "Giao Hang Nhanh",
        "ClientContactPhone": "19001206",
        "ClientAddress": "70 Lữ Gia",
        "CustomerName": "GHN",
        "CustomerPhone": "18006328",
        "ShippingAddress": "70 Lữ Gia",
        "CoDAmount": 447000,
        "NoteCode": "CHOXEMHANGKHONGTHU",
        "InsuranceFee": 1000000,
        "ClientHubID": 0,
        "ServiceID": ' . $ServiceID . ',
        "ToLatitude": 0,
        "ToLongitude": 0,
        "FromLat": 0,
        "FromLng": 0,
        "Content": "Test nội dung",
        "CouponCode": "",
        "Weight": 100,
        "Length": 10,
        "Width": 10,
        "Height": 10,
        "CheckMainBankAccount": false,
        "ShippingOrderCosts":
        [
            {
                "ServiceID": ' . $ShippingOrderCosts_ServiceID . '
            }
        ],
        "ReturnContactName": "",
        "ReturnContactPhone": "",
        "ReturnAddress": "",
        "ReturnDistrictID": 0,
        "ExternalReturnCode": "",
        "IsCreditCreate": false,
        "AffiliateID": 0
    }';


    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://console.ghn.vn/api/v1/apiv3/CreateOrder",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $order,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($order),
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($response, true);

    if ($response['code'] != 1) {
        return false;
    }

    return $orderDetail = [
        'OrderID' => $response['data']['OrderID'],
        'PaymentTypeID' => $response['data']['PaymentTypeID'],
        'OrderCode' => $response['data']['OrderCode'],
        'ExtraFee' => $response['data']['ExtraFee'],
        'TotalServiceFee' => $response['data']['TotalServiceFee'],
        'ExpectedDeliveryTime' => $response['data']['ExpectedDeliveryTime'],
        'ClientHubID' => $response['data']['ClientHubID'],
        'SortCode' => $response['data']['SortCode']
    ];


}

function orderDetail($token, $OrderCode = '')
{
    $OrderCode = 'EHFYAFDU';
    $order = '
        {
            "token": "' . $token['Token'] . '",
            "OrderCode": "' . $OrderCode . '"
        }
    ';

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://console.ghn.vn/api/v1/apiv3/OrderInfo",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $order,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($order),
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($response, true);

    if ($response['code'] != 1) {
        return false;
    }

    return $orderDetail = [
        'ShippingOrderID' => $response['data']['ShippingOrderID'],
        'OrderCode' => $response['data']['OrderCode'],
        'PaymentTypeID' => $response['data']['PaymentTypeID'],
        'FromDistrictID' => $response['data']['FromDistrictID'],
        'FromWardCode' => $response['data']['FromWardCode'],
        'ToDistrictID' => $response['data']['ToDistrictID'],
        'ToWardCode' => $response['data']['ToWardCode'],
        'Note' => $response['data']['Note'],
        'NoteCode' => $response['data']['NoteCode'],
        'CustomerName' => $response['data']['CustomerName'],
        'CustomerPhone' => $response['data']['CustomerPhone'],
        'ShippingAddress' => $response['data']['ShippingAddress'],
        'CoDAmount' => $response['data']['CoDAmount'],
        'InsuranceFee' => $response['data']['InsuranceFee'],
        'ClientHubID' => $response['data']['ClientHubID'],
        'ServiceID' => $response['data']['ServiceID'],
        'Content' => $response['data']['Content'],
        'CouponCode' => $response['data']['CouponCode'],
        'Weight' => $response['data']['Weight'],
        'Length' => $response['data']['Length'],
        'Width' => $response['data']['Width'],
        'Height' => $response['data']['Height']
    ];

}


function UpdateOrder($token, $orderDetail)
{
    $FromDistrictID = 1454;
    $FromWardCode = 21203;
    $ToDistrictID = 1453;
    $ToWardCode = 21110;
    $ServiceID = 53320;
    $ShippingOrderCosts_ServiceID = 53331;

    $order = '{
            "token": "' . $token['Token'] . '",
            "ShippingOrderID": ' . $orderDetail['ShippingOrderID'] . ',
            "OrderCode": "23QAX56N",
            "PaymentTypeID": 1,
            "FromDistrictID": 1452,
            "FromWardCode": "21015",
            "ToDistrictID": 1452,
            "ToWardCode": "21015",
            "Note": "Lưu ĐH qua API ",
            "NoteCode": "CHOTHUHANG",
            "ClientContactName": "client name",
            "ClientContactPhone": "0987654321",
            "ClientAddress": "140 Lê Trọng Tấn",
            "CustomerName": "Nguyễn Văn Update",
            "CustomerPhone": "01666666666",
            "ShippingAddress": "137 Lê Quang Định",
            "CoDAmount": 12500000,
            "InsuranceFee": 0,
            "ClientHubID": 0,
            "ServiceID": 53320,
            "Content": "Test nội dung",
            "CouponCode": "",
            "Weight": 10200,
            "Length": 10,
            "Width": 10,
            "Height": 10,
            "OrderCosts": [
                {
                    "ServiceID": 53327
                },
                {
                    "ServiceID": 53320
                }
            ],
            "ReturnContactName": "",
            "ReturnContactPhone": "",
            "ReturnAddress": "",
            "ReturnDistrictCode": "",
            "ExternalReturnCode": "",
    }';


    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://console.ghn.vn/api/v1/apiv3/UpdateOrder",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $order,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($order),
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($response, true);

    if ($response['code'] != 1) {
        return false;
    }

    echo "<pre>";
    var_dump($response['data']);
    echo "<pre>";


}

function CancelOrder($token, $orderDetail)
{
    $OrderCode = 'EHFYAFDU';
    //$OrderCode = $orderDetail['ShippingOrderID'];
    $order = '
        {
            "token": "' . $token['Token'] . '",
            "OrderCode": "' . $OrderCode . '"
        }
    ';

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://console.ghn.vn/api/v1/apiv3/CancelOrder",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $order,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($order),
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($response, true);

    if ($response['code'] != 1) {
        return false;
    }

    echo "<pre>";
    var_dump($response);
    echo "<pre>";
}

?>
