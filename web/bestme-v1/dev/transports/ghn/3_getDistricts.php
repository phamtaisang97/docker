<?php
    $order = '{
                "token": "2f3511ba20e146f1bbe6338fb5a70197"
            }';

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://console.ghn.vn/api/v1/apiv3/GetDistricts",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $order,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($order),
        ),
    ));

    $response = curl_exec($curl);
    $result = json_decode($response);
    if ($result->msg != 'Success') {
        return false;
    }

    echo "<pre>";
    var_dump($result->data);
    echo "<pre>";

    $district_list = $result->data;
    foreach ($district_list as $key => $vl) {
        $Code_district = $vl->Code;
        $DistrictID_district = $vl->DistrictID;
        $DistrictName_district = $vl->DistrictName;
        $ProvinceID_district = $vl->ProvinceID;
        $ProvinceName_district = $vl->ProvinceName;
    }
    curl_close($curl);

?>



