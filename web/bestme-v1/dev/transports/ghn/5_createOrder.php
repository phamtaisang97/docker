<?php

$FromDistrictID = 1454;
$FromWardCode = 21203;
$ToDistrictID = 1453;
$ToWardCode = 21110;
$ServiceID = 53320;
$ShippingOrderCosts_ServiceID = 53331;

$order = '{
        "token": "2f3511ba20e146f1bbe6338fb5a70197",
        "PaymentTypeID": 1,
        "FromDistrictID": '.$FromDistrictID.',
        "FromWardCode": "'.$FromWardCode.'",
        "ToDistrictID": '.$ToDistrictID.',
        "ToWardCode": "'.$ToWardCode.'",
        "Note": "Tạo ĐH qua API",
        "SealCode": "",
        "ExternalCode": "",
        "ClientContactName": "Giao Hang Nhanh",
        "ClientContactPhone": "19001206",
        "ClientAddress": "70 Lữ Gia",
        "CustomerName": "GHN",
        "CustomerPhone": "18006328",
        "ShippingAddress": "70 Lữ Gia",
        "CoDAmount": 447000,
        "NoteCode": "CHOXEMHANGKHONGTHU",
        "InsuranceFee": 1000000,
        "ClientHubID": 0,
        "ServiceID": '.$ServiceID.',
        "ToLatitude": 0,
        "ToLongitude": 0,
        "FromLat": 0,
        "FromLng": 0,
        "Content": "Test nội dung",
        "CouponCode": "",
        "Weight": 100,
        "Length": 10,
        "Width": 10,
        "Height": 10,
        "CheckMainBankAccount": false,
        "ShippingOrderCosts":
        [
            {
                "ServiceID": '.$ShippingOrderCosts_ServiceID.'
            }
        ],
        "ReturnContactName": "",
        "ReturnContactPhone": "",
        "ReturnAddress": "",
        "ReturnDistrictID": 0,
        "ExternalReturnCode": "",
        "IsCreditCreate": false,
        "AffiliateID": 0
    }';


$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "https://console.ghn.vn/api/v1/apiv3/CreateOrder",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => $order,
    CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Content-Length: " . strlen($order),
    ),
));

$response = curl_exec($curl);
curl_close($curl);

echo 'Response: ' . $response;
?>



