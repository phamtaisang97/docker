<?php

$districtJsonFile = __DIR__ . '/ghn_district.json';
$districts = json_decode(file_get_contents($districtJsonFile), true);

$provinces = [];
foreach ($districts as $district) {
    $province = [];
    $province['ProvinceID'] = $district['ProvinceID'];
    $province['ProvinceName'] = $district['ProvinceName'];

    $provinces[$province['ProvinceID']] = $province;
}

echo json_encode($provinces, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);