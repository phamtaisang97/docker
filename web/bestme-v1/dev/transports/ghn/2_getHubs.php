<?php
$order = '{
        "token": "2f3511ba20e146f1bbe6338fb5a70197"
    }';

$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "https://console.ghn.vn/api/v1/apiv3/GetHubs",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => $order,
    CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Content-Length: " . strlen($order),
    ),
));

$response = curl_exec($curl);
curl_close($curl);

echo 'Response: ' . $response;
?>



