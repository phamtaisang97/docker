<?php
// Config Partner Viettel_Post
define('VIETTEL_POST_TOKEN', 'eyJhbGciOiJFUzI1NiJ9.eyJVc2VySWQiOjY5MDE2OTIsIkZyb21Tb3VyY2UiOjUsIlRva2VuIjoiUzEzMU01N1VDSkFQUzFCOEdLNEYiLCJleHAiOjE2NTI4Njk5NDMsIlBhcnRuZXIiOjY5MDE2OTJ9.Hu2g_TiKNGT55-7CqecBxigQomB9q3JAFLD372th9GNAHGaFMbeC59vxX_6HaP92K-QLmNG-c8youePy8aMeHw');
define('VIETTEL_POST_USERNAME', 'dunght@novaon.asia');
define('VIETTEL_POST_PASSWORD', 'NOVAON12nam');
/**
 * Quy trình tổng quan viettelPost
 * Tham khảo: https://partner.viettelpost.vn/expose/
 */

/**
 * Đăng nhập tài khoản đối tác
 * Function:  signIn()
 * Đăng nhập tài khoản để lấy token
 * https://partner.viettelpost.vn/expose/login
 */
$token = signIn();

/**
 * Kết nối tài khoản khách hàng
 * Function:  ownerconnect()
 * Tham khảo: https://partner.viettelpost.vn/expose/ownerConnect
 */
$token = ownerconnect($token);

/**
 * Danh sách tỉnh/thành phố - quận/huyện - phường/xã
 */
$listProvince = listProvince();

$listDistrict = listDistrict($listProvince);

$listWards = listWards($listDistrict);

/**
 * Đăng ký tài khoản khách hàng
 * Function:  ownerRegister()
 * Tham khảo: https://partner.viettelpost.vn/expose/ownerRegister
 */
//$ownerRegister = ownerRegister($token, $listWards);

/**
 * Đăng ký kho (nếu chưa có kho)
 * Function registerInventory($token, $listWards)
 * Tham khảo: https://partner.viettelpost.vn/expose/registerInventory
 */
//$registerInventory = registerInventory($token, $listWards);

/**
 * Lấy danh sách các kho
 * Function listInventory($token)
 * Tham khảo: https://partner.viettelpost.vn/expose/registerInventory
 */
//$listInventory = listInventory($token);

/**
 * Tạo đơn hàng
 * Function createOrder($token, $listInventory, $listProvince, $listDistrict, $listWards);
 * Tham khảo: https://partner.viettelpost.vn/expose/createOrder
 */
//createOrder($token, $listInventory, $listProvince, $listDistrict, $listWards);

/**
 * Cập nhật trạng thái đơn hàng
 * Function UpdateOrder($token, $order_number = '');
 * $order_number: MÃ VẬN ĐƠN.
 * Tham khảo: https://partner.viettelpost.vn/expose/updateOrder
 */
//UpdateOrder($token, $order_number = '');

/**  ALL FUNCTION VIETTELPOST */
function signIn()
{
    $input = '{
            "USERNAME":"dunght@novaon.asia",
            "PASSWORD":"NOVAON12nam"
        }';

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://partner.viettelpost.vn/v2/user/Login",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $input,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($input),
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);

    $response = json_decode($response, true);
    if ($response['error'] == 'true') {
        return false;
    }

    if (!is_array($response)) {
        return false;
    }

    $token = [
        'userId' => $response['data']['userId'],
        'token' => $response['data']['token'],
        'partner' => $response['data']['partner'],
    ];
    echo "<pre>";
    var_dump($token);
    echo "<pre>";
    //return $token;
}

function ownerconnect($token)
{
    $input = '{
            "USERNAME":"chiemnv@novaon.vn",
            "PASSWORD":"123456"
        }';

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://partner.viettelpost.vn/v2/user/ownerconnect",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $input,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($input),
            "Token: " . VIETTEL_POST_TOKEN . "",
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);
    curl_close($curl);
    if ($response['error'] == 'true') {
        return false;
    }

    if (!is_array($response)) {
        return false;
    }
    $token = [
        'userId' => $response['data']['userId'],
        'token' => $response['data']['token'],
        'partner' => $response['data']['partner'],
    ];
    echo "<pre>";
    var_dump($token);
    echo "<pre>";
    //return $token;

}

function ownerRegister($token, $listWards)
{
    $input = '{
            "EMAIL":"chiemnv@novaosn.vn",
            "PHONE":"0984515356",
            "NAME":"chiemnv 2",
            "ADDRESS":"34 phố vẫy",
            "WARDS_ID":' . $listWards['WARDS_ID'] . '
        }';

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://partner.viettelpost.vn/v2/user/ownerRegister",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $input,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($input),
            "Token: " . $token['token'] . "",
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if ($response['error'] == 'true') {
        return false;
    }

    if (!is_array($response)) {
        return false;
    }

    return $ownerRegister = [
        'userId' => $response['data']['userId'],
        'token' => $response['data']['token'],
        'partner' => $response['data']['partner'],
        'phone' => $response['data']['phone'],
    ];

    curl_close($curl);
}

function registerInventory($token, $listWards)
{
    $input = '{
           "PHONE":"0968323596",
           "NAME":"Kho số 4",
           "ADDRESS":"121 Cầu Diễn",
           "WARDS_ID":' . $listWards['WARDS_ID'] . '
        }
        ';

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://partner.viettelpost.vn/v2/user/registerInventory",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $input,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Token: " . $token['token'] . "",
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if ($response['error'] == 'true') {
        return false;
    }

    if (!is_array($response)) {
        return false;
    }

    return $registerInventory = [
        'groupaddressId' => $response['data'][0]['groupaddressId'],
        'cusId' => $response['data'][0]['cusId'],
        'name' => $response['data'][0]['name'],
        'phone' => $response['data'][0]['phone'],
        'address' => $response['data'][0]['address'],
        'provinceId' => $response['data'][0]['provinceId'],
        'districtId' => $response['data'][0]['districtId'],
        'wardsId' => $response['data'][0]['wardsId'],
    ];

    curl_close($curl);
}

function listInventory($token)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://partner.viettelpost.vn/v2/user/listInventory",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Token: " . $token['token'] . "",
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if ($response['error'] == 'true') {
        return false;
    }

    if (!is_array($response)) {
        return false;
    }
    /** Danh sách các kho hiện tại VD: lấy kho thứ 0 */
    return $listInventory = [
        'groupaddressId' => $response['data'][0]['groupaddressId'],
        'cusId' => $response['data'][0]['cusId'],
        'name' => $response['data'][0]['name'],
        'phone' => $response['data'][0]['phone'],
        'address' => $response['data'][0]['address'],
        'provinceId' => $response['data'][0]['provinceId'],
        'districtId' => $response['data'][0]['districtId'],
        'wardsId' => $response['data'][0]['wardsId'],
    ];

    curl_close($curl);
}


function createOrder($token, $listInventory, $listProvince, $listDistrict, $listWards)
{
    $GROUPADDRESS_ID = $listInventory['groupaddressId'];
    $CUS_ID = $listInventory['cusId'];
    // Đang để dữ liệu demo: PHƯỜNG HÀNG BUỒM -  QUẬN HOÀN KIẾM - thành phố HÀ NỘI
    $SENDER_FULLNAME = $listWards['name'];
    $SENDER_ADDRESS = $listWards['address'];
    $SENDER_PHONE = $listWards['phone'];
    $SENDER_WARD = $listWards['wardsId'];
    $SENDER_DISTRICT = $listWards['districtId'];
    $SENDER_PROVINCE = $listWards['provinceId'];

    $RECEIVER_WARD = $listWards['WARDS_ID']; // phường/xã người nhận
    $RECEIVER_DISTRICT = $listDistrict['DISTRICT_ID'];
    $RECEIVER_PROVINCE = $listProvince['PROVINCE_ID'];
    $PRODUCT_TYPE = 'HH'; // Kiểu hàng hóa: / Product type + TH: Thư/ Envelope + HH: Hàng hóa/ Goods
    $input = '{
            "ORDER_NUMBER" : "MADONHANG001",
            "GROUPADDRESS_ID" : ' . $GROUPADDRESS_ID . ',
            "CUS_ID" : ' . $CUS_ID . ',
            "DELIVERY_DATE" : "11/8/2019 15:09:52",
            "SENDER_FULLNAME" : "' . $SENDER_FULLNAME . '",
            "SENDER_ADDRESS" : "' . $SENDER_ADDRESS . '",
            "SENDER_PHONE" : "' . $SENDER_PHONE . '",
            "SENDER_EMAIL" : "",
            "SENDER_WARD" : ' . $SENDER_WARD . ',
            "SENDER_DISTRICT" : ' . $SENDER_DISTRICT . ',
            "SENDER_PROVINCE" : ' . $SENDER_PROVINCE . ',
            "SENDER_LATITUDE" : 0,
            "SENDER_LONGITUDE" : 0,
            "RECEIVER_FULLNAME" : "Hoàng - Test",
            "RECEIVER_ADDRESS" : "Số 20",
            "RECEIVER_PHONE" : "0907882792",
            "RECEIVER_EMAIL" : "hoangnh50@fpt.com.vn",
            "RECEIVER_WARD" : ' . $RECEIVER_WARD . ',
            "RECEIVER_DISTRICT" : ' . $RECEIVER_DISTRICT . ',
            "RECEIVER_PROVINCE" : ' . $RECEIVER_PROVINCE . ',
            "RECEIVER_LATITUDE" : 0,
            "RECEIVER_LONGITUDE" : 0,
            "PRODUCT_NAME" : "Máy xay sinh tố Philips HR2118 2.0L ",
            "PRODUCT_DESCRIPTION" : "Máy xay sinh tố Philips HR2118 2.0L ",
            "PRODUCT_QUANTITY" : 1,
            "PRODUCT_PRICE" : 2292764,
            "PRODUCT_WEIGHT" : 40000,
            "PRODUCT_LENGTH" : 38,
            "PRODUCT_WIDTH" : 24,
            "PRODUCT_HEIGHT" : 25,
            "PRODUCT_TYPE" : "' . $PRODUCT_TYPE . '",
            "ORDER_PAYMENT" : 3,
            "ORDER_SERVICE" : "VCN",
            "ORDER_SERVICE_ADD" : "",
            "ORDER_VOUCHER" : "",
            "ORDER_NOTE" : "cho xem hàng, không cho thử",
            "MONEY_COLLECTION" : 2292764,
            "MONEY_TOTALFEE" : 0,
            "MONEY_FEECOD" : 0,
            "MONEY_FEEVAS" : 0,
            "MONEY_FEEINSURRANCE" : 0,
            "MONEY_FEE" : 0,
            "MONEY_FEEOTHER" : 0,
            "MONEY_TOTALVAT" : 0,
            "MONEY_TOTAL" : 0,
            "LIST_ITEM" : [
                {
                    "PRODUCT_NAME" : "Máy xay sinh tố Philips HR2118 2.0L ",
                    "PRODUCT_PRICE" : 2150000,
                    "PRODUCT_WEIGHT" : 2500,
                    "PRODUCT_QUANTITY" : 1
                }
            ]
        }';

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://partner.viettelpost.vn/v2/order/createOrder",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $input,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($input),
            "Token: " . $token['token'] . ""
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if ($response['error'] == 'true') {
        return false;
    }

    if (!is_array($response)) {
        return false;
    }

    return $lastOrder = [
        'ORDER_NUMBER' => $response['data']['ORDER_NUMBER'],
        'MONEY_COLLECTION' => $response['data']['MONEY_COLLECTION'],
        'EXCHANGE_WEIGHT' => $response['data']['EXCHANGE_WEIGHT'],
        'MONEY_TOTAL' => $response['data']['MONEY_TOTAL'],
        'MONEY_TOTAL_FEE' => $response['data']['MONEY_TOTAL_FEE'],
        'MONEY_FEE' => $response['data']['MONEY_FEE'],
        'MONEY_COLLECTION_FEE' => $response['data']['MONEY_COLLECTION_FEE'],
        'MONEY_VAT' => $response['data']['MONEY_VAT'],
        'KPI_HT' => $response['data']['KPI_HT'],
    ];

    curl_close($curl);
}

function UpdateOrder($token, $order_number = '')
{
    $order_number = "11381731213";
    $type = 4;
    //Loại trạng thái:
    // 1. Duyệt đơn hàng/ Confirm order
    // 2. Duyệt chuyển hoàn/ Confirm return shipping
    // 3. Phát tiếp/ delivery again
    // 4. Hủy đơn hàng/ delivery again
    // 5. Lấy lại đơn hàng (Gửi lại)/ get back order (re-order)
    // 11. Xóa đơn hàng đã hủy(delete canceled order)
    $input = '
        {
           "TYPE" : ' . $type . ',
           "ORDER_NUMBER" : "' . $order_number . '",
           "NOTE" : "Ghi chú",
           "DATE" : "07/8/2019 17:20:55"
        }
    ';

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://partner.viettelpost.vn/v2/order/UpdateOrder",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $input,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Token: " . $token['token'] . "",
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if ($response['error'] == 'true') {
        return false;
    }

    return $orderUpdate = [
        'message' => $response['message']
    ];

    curl_close($curl);
}

/** All Function  Danh sách Tỉnh/thành phố - Quận/huyện - Phường/xã */

function listProvince()
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://partner.viettelpost.vn/v2/categories/listProvince",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if ($response['error'] == 'true') {
        return false;
    }

    if (!is_array($response)) {
        return false;
    }

    // Lấy ví dụ: thành phố HÀ NỘI
    return $listProvince = [
        'PROVINCE_ID' => $response['data'][0]['PROVINCE_ID'],
        'PROVINCE_CODE' => $response['data'][0]['PROVINCE_CODE'],
        'PROVINCE_NAME' => $response['data'][0]['PROVINCE_NAME'],
    ];

    curl_close($curl);
}

function listDistrict($listProvince)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://partner.viettelpost.vn/v2/categories/listDistrict?provinceId=" . $listProvince['PROVINCE_ID'] . "",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if ($response['error'] == 'true') {
        return false;
    }

    if (!is_array($response)) {
        return false;
    }

    //Lấy ví dụ: QUẬN HOÀN KIẾM
    return $listDistrict = [
        'DISTRICT_ID' => $response['data'][0]['DISTRICT_ID'],
        'DISTRICT_VALUE' => $response['data'][0]['DISTRICT_VALUE'],
        'DISTRICT_NAME' => $response['data'][0]['DISTRICT_NAME'],
        'PROVINCE_ID' => $response['data'][0]['PROVINCE_ID'],
    ];

    curl_close($curl);
}

function listWards($listDistrict)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://partner.viettelpost.vn/v2/categories/listWards?districtId=" . $listDistrict['DISTRICT_ID'] . "",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);

    if ($response['error'] == 'true') {
        return false;
    }

    if (!is_array($response)) {
        return false;
    }

    //Lấy ví dụ: PHƯỜNG HÀNG BUỒM
    return $listWards = [
        'WARDS_ID' => $response['data'][0]['WARDS_ID'],
        'WARDS_NAME' => $response['data'][0]['WARDS_NAME'],
        'DISTRICT_ID' => $response['data'][0]['DISTRICT_ID'],
    ];

    curl_close($curl);
}


?>
