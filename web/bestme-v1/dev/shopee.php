<?php

define('PARTNER_ID', '9999');
define('SHOP_ID', '98953394');

getItemsList(100, 0);
GetCategories();
getItemDetail('2447683614');


function GetCategories()
{
    $data = [
        'language' => 'vi',
    ];

    $url = "https://partner.uat.shopeemobile.com/api/v1/item/categories/get";  // test url
    //$url = "https://partner.shopeemobile.com/api/v1/item/categories/get";

    call_api($url, $data, 'POST');
}

function getItemsList($limit, $offset)
{
    $data = [
        'pagination_offset' => $offset,
        'pagination_entries_per_page' => $limit,
        //'update_time_from' => 1171502725,   // optional
        //'update_time_to' => 1565681588,     // optional
    ];

    $url = "https://partner.uat.shopeemobile.com/api/v1/items/get";  // test url
    //$url = "https://partner.shopeemobile.com/api/v1/items/get";

    call_api($url, $data, 'POST');
}

function getItemDetail($item_id)
{
    $data = [
        'item_id' => $item_id,
    ];

    $url = "https://partner.uat.shopeemobile.com/api/v1/item/get";  // test url
    //$url = "https://partner.shopeemobile.com/api/v1/item/get";

    call_api($url, $data, 'POST');
}

function call_api($url, $data, $method = 'POST')
{
    $date = new DateTime();
    $timestamp = $date->getTimestamp();

    $data['partner_id'] = PARTNER_ID;
    $data['shopid'] = SHOP_ID;
    $data['timestamp'] = $timestamp;

    $data = json_encode($data);

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => $method,
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($data),
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);

    echo 'Response: ' . $response;
}