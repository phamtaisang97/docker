<?php

/* setup */
// Version
define('VERSION', '3.0.2.0');

// Configuration
if (is_file(__DIR__ . '/../config.php')) {
    require_once(__DIR__ . '/../config.php');
}

// Autoloader
if (is_file(DIR_STORAGE . 'vendor/autoload.php')) {
    require_once(DIR_STORAGE . 'vendor/autoload.php');
}

///* === sender === */
///* include the library */
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
//
///* create a connection to the server */
//$connection = new AMQPStreamConnection('localhost', 8072, 'admin', '123456'); // default: 5672, 'guest', 'guest'
//$channel = $connection->channel();
//
///* must declare a queue to send to */
//$channel->queue_declare('hello', false, false, false, false);
//
///* publish a message to the queue */
//$msg = new AMQPMessage('Hello World!');
//$channel->basic_publish($msg, '', 'hello');
//
//echo " [x] Sent 'Hello World!'\n";
//
///* Lastly, close the channel and the connection; */
//$channel->close();
//$connection->close();

/* === receiver ===*/
/* open a connection and a channel, and declare the queue */
$connection = new AMQPStreamConnection('localhost', 5672, 'admin', '123456'); // default: 5672, 'guest', 'guest'
$channel = $connection->channel();

// make sure the queue exists before trying to consume messages from it
$channel->queue_declare('order', false, true, false, false);

/*
 * define a PHP callable that will receive the messages sent by the server
 * Keep in mind that messages are sent asynchronously from the server to the clients
 */
echo " [*] Waiting for messages. To exit press CTRL+C\n";

$callback = function ($msg) {
    echo ' [x] Received ', $msg->body, "\n";
};

$channel->basic_consume('order', '', false, false, false, false, $callback);

while (count($channel->callbacks)) {
    $channel->wait();
}

/* Lastly, close the channel and the connection; */
$channel->close();
$connection->close();