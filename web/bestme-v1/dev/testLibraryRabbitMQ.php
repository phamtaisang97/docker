<?php

/* setup */
// Version
define('VERSION', '3.0.2.0');

// Configuration
if (is_file(__DIR__ . '/../config.php')) {
    require_once(__DIR__ . '/../config.php');
}

// Modification Override
function modification($filename)
{
    if (defined('DIR_CATALOG')) {
        $file = DIR_MODIFICATION . 'admin/' . substr($filename, strlen(DIR_APPLICATION));
    } elseif (defined('DIR_OPENCART')) {
        $file = DIR_MODIFICATION . 'install/' . substr($filename, strlen(DIR_APPLICATION));
    } else {
        $file = DIR_MODIFICATION . 'catalog/' . substr($filename, strlen(DIR_APPLICATION));
    }

    if (substr($filename, 0, strlen(DIR_SYSTEM)) == DIR_SYSTEM) {
        $file = DIR_MODIFICATION . 'system/' . substr($filename, strlen(DIR_SYSTEM));
    }

    if (is_file($file)) {
        return $file;
    }

    return $filename;
}

// Autoloader
if (is_file(DIR_STORAGE . 'vendor/autoload.php')) {
    require_once(DIR_STORAGE . 'vendor/autoload.php');
}

function library($class)
{
    $file = DIR_SYSTEM . 'library/' . str_replace('\\', '/', strtolower($class)) . '.php';

    if (is_file($file)) {
        include_once(modification($file));

        return true;
    } else {
        return false;
    }
}

spl_autoload_register('library');
spl_autoload_extensions('.php');

/* sender */
$rabitMQSender = new Queue\RabbitMQ\RabbitMQ_Sender('hello-again', 'localhost', 8072, 'admin', '123456');
$rabitMQSender->enqueue('Hello World again!');

/* receiver */
$rabitMQReceiver = new Queue\RabbitMQ\RabbitMQ_Receiver('hello-again', 'localhost', 8072, 'admin', '123456');
$callback = function ($msg) {
    echo '[x] Received ', $msg->body, "\n";
};
$rabitMQReceiver->dequeue($callback);
