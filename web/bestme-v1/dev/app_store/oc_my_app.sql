CREATE TABLE `oc_my_app` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_code` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL,
  `expiration_date` date NOT NULL,
  `app_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `trial` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;