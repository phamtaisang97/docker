CREATE TABLE `oc_appstore` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `path_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort_description` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_appstore`
--

INSERT INTO `oc_appstore` (`name`, `code`, `path_image`, `sort_description`, `description`, `price`) VALUES
('App Demo', 'app_demo', 'payment/eway.png', 'App Demo cho các chiến dịch chạy sale của shop', 'Ứng dụng giúp tạo 1 chiến dịch quảng cáo trực quan ngay trên hệ thống website của shop.\r\n                            Các sản phẩm được lựa chọn sẽ hiển thị trên trang khuyến mãi,\r\n                            giúp kích thích nhu cầu và sự chú ý của khách hàng, từ đó tăng khả năng bán hàng của cửa hàng.', 100000);

