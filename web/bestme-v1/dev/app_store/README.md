# Appstore wiki for dev

### Welcome!

...



migrate:

create tables

- oc_app_config_theme
- oc_appstore_setting
- oc_my_app

for dev only:
- oc_appstore


add permissions:

- app_store/apps
- app_store/market
- app_store/my_app
- extension/extension/appstore
- common/app_config


(auto: 
extension/appstore/app_demo)