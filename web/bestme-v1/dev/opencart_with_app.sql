-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 28, 2020 at 11:00 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `opencart`
--

-- --------------------------------------------------------

--
-- Table structure for table `oc_address`
--

CREATE TABLE `oc_address` (
  `address_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(40) NOT NULL,
  `address` varchar(128) NOT NULL,
  `city` varchar(128) DEFAULT NULL,
  `district` varchar(128) DEFAULT NULL,
  `wards` varchar(128) DEFAULT NULL,
  `phone` varchar(32) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_api`
--

CREATE TABLE `oc_api` (
  `api_id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `key` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_api_ip`
--

CREATE TABLE `oc_api_ip` (
  `api_ip_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_api_session`
--

CREATE TABLE `oc_api_session` (
  `api_session_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_appstore`
--

CREATE TABLE `oc_appstore` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `path_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort_description` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_appstore`
--

INSERT INTO `oc_appstore` (`id`, `name`, `code`, `path_image`, `sort_description`, `description`, `price`) VALUES
(2, 'Flash Sale', 'flash_sale', 'payment/eway.png', 'App Flash Sale cho các chiến dịch chạy sale của shop', 'Ứng dụng giúp tạo 1 chiến dịch quảng cáo trực quan ngay trên hệ thống website của shop.\r\n                            Các sản phẩm được lựa chọn sẽ hiển thị trên trang khuyến mãi, \r\n                            giúp kích thích nhu cầu và sự chú ý của khách hàng, từ đó tăng khả năng bán hàng của cửa hàng.', 100000);

-- --------------------------------------------------------

--
-- Table structure for table `oc_appstore_setting`
--

CREATE TABLE `oc_appstore_setting` (
  `module_id` int(11) NOT NULL,
  `name` varchar(64) CHARACTER SET utf8 NOT NULL,
  `code` varchar(64) CHARACTER SET utf8 NOT NULL,
  `setting` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oc_app_config_theme`
--

CREATE TABLE `oc_app_config_theme` (
  `id` int(255) NOT NULL,
  `module_id` int(255) NOT NULL,
  `theme_value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_app_config_theme`
--

INSERT INTO `oc_app_config_theme` (`id`, `module_id`, `theme_value`, `page`, `position`, `sort`) VALUES
(2, 3, 'beauty_app_cosmetic', 'home_page', 'element_bestme_1', 1),
(3, 1, 'beauty_app_cosmetic', 'home_page', 'element_bestme_1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_app_flash_sale_product`
--

CREATE TABLE `oc_app_flash_sale_product` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_published` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_app_flash_sale_product`
--

INSERT INTO `oc_app_flash_sale_product` (`id`, `product_id`, `date_added`, `date_published`, `date_modified`) VALUES
(1, 96, '2020-04-22 14:36:36', '2020-04-22 14:36:36', '2020-04-22 14:36:36'),
(2, 95, '2020-04-22 14:36:36', '2020-04-22 14:36:36', '2020-04-22 14:36:36'),
(3, 92, '2020-04-22 14:36:36', '2020-04-22 14:36:36', '2020-04-22 14:36:36'),
(4, 91, '2020-04-22 17:21:01', '2020-04-22 17:21:01', '2020-04-22 17:21:01'),
(5, 90, '2020-04-22 17:21:01', '2020-04-22 17:21:01', '2020-04-22 17:21:01'),
(6, 81, '2020-04-22 17:21:01', '2020-04-22 17:21:01', '2020-04-22 17:21:01'),
(7, 94, '2020-04-23 11:41:43', '2020-04-23 11:41:43', '2020-04-23 11:41:43'),
(8, 93, '2020-04-23 13:18:41', '2020-04-23 13:18:41', '2020-04-23 13:18:41');

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute`
--

CREATE TABLE `oc_attribute` (
  `attribute_id` int(11) NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_description`
--

CREATE TABLE `oc_attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_group`
--

CREATE TABLE `oc_attribute_group` (
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_group_description`
--

CREATE TABLE `oc_attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_banner`
--

CREATE TABLE `oc_banner` (
  `banner_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_banner_image`
--

CREATE TABLE `oc_banner_image` (
  `banner_image_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_blog`
--

CREATE TABLE `oc_blog` (
  `blog_id` int(11) NOT NULL,
  `author` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_publish` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_blog_category`
--

CREATE TABLE `oc_blog_category` (
  `blog_category_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_blog_category`
--

INSERT INTO `oc_blog_category` (`blog_category_id`, `status`, `date_added`, `date_modified`) VALUES
(1, 1, '2019-11-11 13:37:51', '2019-11-11 19:03:46');

-- --------------------------------------------------------

--
-- Table structure for table `oc_blog_category_description`
--

CREATE TABLE `oc_blog_category_description` (
  `blog_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `alias` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_blog_category_description`
--

INSERT INTO `oc_blog_category_description` (`blog_category_id`, `language_id`, `title`, `meta_title`, `meta_description`, `alias`) VALUES
(1, 1, 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'danh-muc-mac-dinh-uncategorized'),
(1, 2, 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'danh-muc-mac-dinh-uncategorized');

-- --------------------------------------------------------

--
-- Table structure for table `oc_blog_description`
--

CREATE TABLE `oc_blog_description` (
  `blog_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text,
  `short_content` text,
  `image` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` text,
  `alias` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_blog_tag`
--

CREATE TABLE `oc_blog_tag` (
  `blog_tag_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oc_blog_to_blog_category`
--

CREATE TABLE `oc_blog_to_blog_category` (
  `blog_id` int(11) NOT NULL,
  `blog_category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_bm_author`
--

CREATE TABLE `oc_bm_author` (
  `author_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `author_group_id` int(11) NOT NULL,
  `custom` int(1) DEFAULT '0',
  `setting` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_bm_author`
--

INSERT INTO `oc_bm_author` (`author_id`, `user_id`, `author_group_id`, `custom`, `setting`) VALUES
(1, 1, 1, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_bm_author_description`
--

CREATE TABLE `oc_bm_author_description` (
  `author_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `short_description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keyword` text NOT NULL,
  `author_description_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_bm_author_description`
--

INSERT INTO `oc_bm_author_description` (`author_id`, `language_id`, `name`, `description`, `short_description`, `meta_title`, `meta_description`, `meta_keyword`, `author_description_id`) VALUES
(1, 1, 'Author', '&lt;p&gt;Lorem ipsum dolor sit amet, justo aliquid reformidans ea vel, vim porro dictas et, ut elit partem invidunt vis. Saepe melius complectitur eum ea. Zril delenit vis ut. His suavitate rationibus in, tale discere ceteros eu nec. Vel ut utamur laoreet vituperata, in discere contentiones definitionem ius.&lt;/p&gt;&lt;p&gt;Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;&lt;p&gt;It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).&lt;/p&gt;', 'Lorem ipsum dolor sit amet, justo aliquid reformidans ea vel, vim porro dictas et, ut elit partem invidunt vis. Saepe melius complectitur eum ea. Zril delenit vis ut. His suavitate rationibus in, tale discere ceteros eu nec. Vel ut utamur laoreet vituperata, in discere contentiones definitionem ius.', 'Author', '', '', 3),
(1, 2, 'Author', '&lt;p&gt;Lorem ipsum dolor sit amet, justo aliquid reformidans ea vel, vim porro dictas et, ut elit partem invidunt vis. Saepe melius complectitur eum ea. Zril delenit vis ut. His suavitate rationibus in, tale discere ceteros eu nec. Vel ut utamur laoreet vituperata, in discere contentiones definitionem ius.&lt;/p&gt;&lt;p&gt;Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;&lt;p&gt;It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).&lt;/p&gt;', 'Lorem ipsum dolor sit amet, justo aliquid reformidans ea vel, vim porro dictas et, ut elit partem invidunt vis. Saepe melius complectitur eum ea. Zril delenit vis ut. His suavitate rationibus in, tale discere ceteros eu nec. Vel ut utamur laoreet vituperata, in discere contentiones definitionem ius.', 'Author', '', '', 4);

-- --------------------------------------------------------

--
-- Table structure for table `oc_bm_author_group`
--

CREATE TABLE `oc_bm_author_group` (
  `author_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_bm_author_group`
--

INSERT INTO `oc_bm_author_group` (`author_group_id`, `name`, `permission`) VALUES
(1, 'admin', '[\"add_posts\",\"edit_posts\",\"delete_posts\",\"edit_others_posts\",\"delete_others_posts\",\"add_reviews\",\"edit_reviews\",\"delete_reviews\",\"add_others_reviews\",\"edit_others_reviews\",\"delete_others_reviews\",\"add_authors\",\"edit_authors\",\"delete_authors\",\"add_author_groups\",\"edit_author_groups\",\"delete_author_groups\",\"add_categories\",\"edit_categories\",\"delete_categories\",\"change_post_author\"]'),
(2, 'editor', '[\"add_posts\",\"edit_posts\",\"delete_posts\",\"edit_others_posts\",\"delete_others_posts\",\"add_reviews\",\"edit_reviews\",\"delete_reviews\",\"add_others_reviews\",\"edit_others_reviews\",\"delete_others_reviews\",\"add_authors\",\"edit_authors\",\"delete_authors\",\"add_author_groups\",\"edit_author_groups\",\"delete_author_groups\",\"add_categories\",\"edit_categories\",\"delete_categories\",\"change_post_author\"]'),
(3, 'author', '[\"add_posts\",\"edit_posts\",\"add_reviews\",\"edit_reviews\",\"delete_reviews\"]'),
(4, 'moderator', '[\"add_reviews\",\"edit_reviews\",\"delete_reviews\",\"add_others_reviews\",\"edit_others_reviews\",\"delete_others_reviews\"]');

-- --------------------------------------------------------

--
-- Table structure for table `oc_bm_category`
--

CREATE TABLE `oc_bm_category` (
  `category_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `limit_access_user` int(1) NOT NULL DEFAULT '0',
  `limit_users` text,
  `limit_access_user_group` int(1) NOT NULL DEFAULT '0',
  `limit_user_groups` text,
  `custom` int(1) DEFAULT '0',
  `setting` text NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_bm_category`
--

INSERT INTO `oc_bm_category` (`category_id`, `parent_id`, `sort_order`, `image`, `status`, `limit_access_user`, `limit_users`, `limit_access_user_group`, `limit_user_groups`, `custom`, `setting`, `date_added`, `date_modified`) VALUES
(1, 0, 1, 'catalog/d_blog_module/category/Photo_blog_17.jpg', 1, 0, NULL, 0, NULL, 0, '', '2016-04-09 11:28:15', '2016-04-18 18:16:48');

-- --------------------------------------------------------

--
-- Table structure for table `oc_bm_category_description`
--

CREATE TABLE `oc_bm_category_description` (
  `category_description_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `short_description` text NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keyword` text NOT NULL,
  `meta_description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_bm_category_description`
--

INSERT INTO `oc_bm_category_description` (`category_description_id`, `category_id`, `language_id`, `title`, `short_description`, `description`, `meta_title`, `meta_keyword`, `meta_description`) VALUES
(1, 1, 1, 'Blog', 'Lorem ipsum dolor sit amet, justo aliquid reformidans ea vel, vim porro dictas et, ut elit partem invidunt vis. Saepe melius complectitur eum ea. Zril delenit vis ut. His suavitate rationibus in, tale discere ceteros eu nec. Vel ut utamur laoreet vituperata, in discere contentiones definitionem ius.', '&lt;p&gt;&lt;span style=&quot;line-height: 1.42857;&quot;&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas, soluta! Non magnam ex, illo maxime maiores, quia perspiciatis sed voluptate quaerat dolorum enim veritatis recusandae qui ad voluptates aspernatur beatae.&amp;nbsp;&lt;/span&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;line-height: 1.42857;&quot;&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas, soluta! Non magnam ex, illo maxime maiores, quia perspiciatis sed voluptate quaerat dolorum enim veritatis recusandae qui ad voluptates aspernatur beatae.&lt;/span&gt;&lt;br&gt;&lt;/p&gt;', 'Blog', '', ''),
(2, 1, 2, 'Blog', 'Lorem ipsum dolor sit amet, justo aliquid reformidans ea vel, vim porro dictas et, ut elit partem invidunt vis. Saepe melius complectitur eum ea. Zril delenit vis ut. His suavitate rationibus in, tale discere ceteros eu nec. Vel ut utamur laoreet vituperata, in discere contentiones definitionem ius.', '&lt;p&gt;&lt;span style=&quot;line-height: 1.42857;&quot;&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas, soluta! Non magnam ex, illo maxime maiores, quia perspiciatis sed voluptate quaerat dolorum enim veritatis recusandae qui ad voluptates aspernatur beatae.&amp;nbsp;&lt;/span&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;line-height: 1.42857;&quot;&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas, soluta! Non magnam ex, illo maxime maiores, quia perspiciatis sed voluptate quaerat dolorum enim veritatis recusandae qui ad voluptates aspernatur beatae.&lt;/span&gt;&lt;br&gt;&lt;/p&gt;', 'Blog', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_bm_category_path`
--

CREATE TABLE `oc_bm_category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_bm_category_path`
--

INSERT INTO `oc_bm_category_path` (`category_id`, `path_id`, `level`) VALUES
(1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_bm_category_to_layout`
--

CREATE TABLE `oc_bm_category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_bm_category_to_layout`
--

INSERT INTO `oc_bm_category_to_layout` (`category_id`, `store_id`, `layout_id`) VALUES
(1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_bm_category_to_store`
--

CREATE TABLE `oc_bm_category_to_store` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_bm_category_to_store`
--

INSERT INTO `oc_bm_category_to_store` (`category_id`, `store_id`) VALUES
(1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_bm_post`
--

CREATE TABLE `oc_bm_post` (
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_title` varchar(255) DEFAULT NULL,
  `image_alt` varchar(255) DEFAULT NULL,
  `review_display` int(1) DEFAULT '0',
  `images_review` int(1) DEFAULT '0',
  `viewed` int(11) DEFAULT '1',
  `status` int(1) DEFAULT '1',
  `limit_access_user` int(1) NOT NULL DEFAULT '0',
  `limit_users` text,
  `limit_access_user_group` int(1) NOT NULL DEFAULT '0',
  `limit_user_groups` text,
  `custom` int(1) DEFAULT '0',
  `setting` text NOT NULL,
  `date_added` datetime NOT NULL,
  `date_published` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_bm_post_description`
--

CREATE TABLE `oc_bm_post_description` (
  `post_description_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `short_description` text NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keyword` text NOT NULL,
  `tag` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_bm_post_related`
--

CREATE TABLE `oc_bm_post_related` (
  `post_id` int(11) NOT NULL,
  `post_related_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_bm_post_to_category`
--

CREATE TABLE `oc_bm_post_to_category` (
  `post_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_bm_post_to_layout`
--

CREATE TABLE `oc_bm_post_to_layout` (
  `post_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_bm_post_to_product`
--

CREATE TABLE `oc_bm_post_to_product` (
  `post_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_bm_post_to_store`
--

CREATE TABLE `oc_bm_post_to_store` (
  `post_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_bm_post_video`
--

CREATE TABLE `oc_bm_post_video` (
  `post_id` int(11) NOT NULL,
  `video` varchar(255) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `text` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_bm_review`
--

CREATE TABLE `oc_bm_review` (
  `review_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `reply_to_review_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `guest_email` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `rating` int(11) NOT NULL,
  `status` int(1) DEFAULT '1',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_bm_review_to_image`
--

CREATE TABLE `oc_bm_review_to_image` (
  `review_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_cart`
--

CREATE TABLE `oc_cart` (
  `cart_id` int(11) UNSIGNED NOT NULL,
  `api_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `option` text NOT NULL,
  `quantity` int(5) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category`
--

CREATE TABLE `oc_category` (
  `category_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category`
--

INSERT INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(10, NULL, 0, 0, 0, 0, 1, '2019-05-20 16:05:16', '2019-05-20 16:05:16'),
(11, NULL, 0, 0, 0, 0, 1, '2019-05-20 16:37:28', '2019-05-20 16:37:28'),
(13, NULL, 0, 0, 0, 0, 1, '2019-05-20 16:54:28', '2019-05-20 16:54:28'),
(15, NULL, 0, 0, 0, 0, 1, '2019-05-21 13:49:04', '2019-05-21 13:49:04'),
(16, NULL, 0, 0, 0, 0, 1, '2019-05-21 14:28:02', '2019-05-21 14:28:02');

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_description`
--

CREATE TABLE `oc_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_description`
--

INSERT INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(16, 2, 'Dầu gội', 'Dầu gội', '', '', ''),
(10, 1, 'Sữa dưỡng thể', 'Sữa dưỡng thể', '', '', ''),
(11, 1, 'Sữa rửa mặt', 'Sữa rửa mặt', '', '', ''),
(13, 2, 'Kem chống nắng', 'Kem chống nắng', '', '', ''),
(13, 1, 'Kem chống nắng', 'Kem chống nắng', '', '', ''),
(15, 2, 'Kem dưỡng da', 'Kem dưỡng da', '', '', ''),
(15, 1, 'Kem dưỡng da', 'Kem dưỡng da', '', '', ''),
(11, 2, 'Sữa rửa mặt', 'Sữa rửa mặt', '', '', ''),
(10, 2, 'Sữa dưỡng thể', 'Sữa dưỡng thể', '', '', ''),
(16, 1, 'Dầu gội', 'Dầu gội', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_filter`
--

CREATE TABLE `oc_category_filter` (
  `category_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_path`
--

CREATE TABLE `oc_category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_path`
--

INSERT INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(10, 10, 0),
(11, 11, 0),
(13, 13, 0),
(15, 15, 0),
(16, 16, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_to_layout`
--

CREATE TABLE `oc_category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_to_store`
--

CREATE TABLE `oc_category_to_store` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_to_store`
--

INSERT INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(10, 0),
(11, 0),
(13, 0),
(15, 0),
(16, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_collection`
--

CREATE TABLE `oc_collection` (
  `collection_id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `sort_order` int(11) DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `product_type_sort` varchar(256) DEFAULT NULL,
  `type` varchar(255) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_collection`
--

INSERT INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(5, 'Sản phẩm hot', 0, 1, '', '0'),
(6, 'Sản phẩm mới', 0, 1, '', '0'),
(4, 'Sản phẩm khuyến mại', 0, 1, '', '0'),
(7, 'Hàng ngày', 0, 1, NULL, '0');

-- --------------------------------------------------------

--
-- Table structure for table `oc_collection_description`
--

CREATE TABLE `oc_collection_description` (
  `collection_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_collection_description`
--

INSERT INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(5, 'https://res.cloudinary.com/novaon-x2/image/upload/v1555900690/x2-8787.bestme.test/dell-vostro-5568-077m52-vangdong-450x300-450x300-600x600.png', '&lt;p&gt;sản phẩm hot&lt;/p&gt;', '', '', ''),
(6, '', '&lt;p&gt;nhũng sản phẩm mới nhất c&amp;oacute; mặt tại cửa h&amp;agrave;ng&lt;/p&gt;', '', '', ''),
(7, 'https://res.cloudinary.com/novaon-x2/image/upload/v1558345596/x2-8888.bestme.test/8ts19s033-fw125-l_2.jpg', '&lt;p&gt;Bộ sưu tập h&amp;agrave;ng ng&amp;agrave;y&lt;/p&gt;', '', '', ''),
(4, 'https://res.cloudinary.com/novaon-x2/image/upload/v1555900767/x2-8787.bestme.test/Capture28.png', '&lt;p&gt;sản phẩm khuyến mại&lt;/p&gt;', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_collection_to_collection`
--

CREATE TABLE `oc_collection_to_collection` (
  `collection_to_collection_id` int(11) NOT NULL,
  `collection_id` int(11) NOT NULL,
  `parent_collection_id` int(11) NOT NULL,
  `sort_order` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_country`
--

CREATE TABLE `oc_country` (
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_country`
--

INSERT INTO `oc_country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1),
(2, 'Albania', 'AL', 'ALB', '', 0, 1),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1),
(5, 'Andorra', 'AD', 'AND', '', 0, 1),
(6, 'Angola', 'AO', 'AGO', '', 0, 1),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1),
(11, 'Armenia', 'AM', 'ARM', '', 0, 1),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1),
(13, 'Australia', 'AU', 'AUS', '', 0, 1),
(14, 'Austria', 'AT', 'AUT', '', 0, 1),
(15, 'Azerbaijan', 'AZ', 'AZE', '', 0, 1),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1),
(20, 'Belarus', 'BY', 'BLR', '', 0, 1),
(21, 'Belgium', 'BE', 'BEL', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', '', 0, 1),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1),
(38, 'Canada', 'CA', 'CAN', '', 0, 1),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1),
(42, 'Chad', 'TD', 'TCD', '', 0, 1),
(43, 'Chile', 'CL', 'CHL', '', 0, 1),
(44, 'China', 'CN', 'CHN', '', 0, 1),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1),
(47, 'Colombia', 'CO', 'COL', '', 0, 1),
(48, 'Comoros', 'KM', 'COM', '', 0, 1),
(49, 'Congo', 'CG', 'COG', '', 0, 1),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1),
(52, 'Cote D\'Ivoire', 'CI', 'CIV', '', 0, 1),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1),
(61, 'East Timor', 'TL', 'TLS', '', 0, 1),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1),
(67, 'Estonia', 'EE', 'EST', '', 0, 1),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1),
(72, 'Finland', 'FI', 'FIN', '', 0, 1),
(74, 'France, Metropolitan', 'FR', 'FRA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1),
(80, 'Georgia', 'GE', 'GEO', '', 0, 1),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1),
(84, 'Greece', 'GR', 'GRC', '', 0, 1),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1),
(88, 'Guam', 'GU', 'GUM', '', 0, 1),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1),
(91, 'Guinea-Bissau', 'GW', 'GNB', '', 0, 1),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1),
(95, 'Honduras', 'HN', 'HND', '', 0, 1),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1),
(99, 'India', 'IN', 'IND', '', 0, 1),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1),
(104, 'Israel', 'IL', 'ISR', '', 0, 1),
(105, 'Italy', 'IT', 'ITA', '', 0, 1),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1),
(107, 'Japan', 'JP', 'JPN', '', 0, 1),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', '', 0, 1),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1),
(113, 'South Korea', 'KR', 'KOR', '', 0, 1),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', '', 0, 1),
(116, 'Lao People\'s Democratic Republic', 'LA', 'LAO', '', 0, 1),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1),
(125, 'Macau', 'MO', 'MAC', '', 0, 1),
(126, 'FYROM', 'MK', 'MKD', '', 0, 1),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1),
(131, 'Mali', 'ML', 'MLI', '', 0, 1),
(132, 'Malta', 'MT', 'MLT', '', 0, 1),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1),
(140, 'Moldova, Republic of', 'MD', 'MDA', '', 0, 1),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1),
(155, 'Niger', 'NE', 'NER', '', 0, 1),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1),
(157, 'Niue', 'NU', 'NIU', '', 0, 1),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1),
(160, 'Norway', 'NO', 'NOR', '', 0, 1),
(161, 'Oman', 'OM', 'OMN', '', 0, 1),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1),
(163, 'Palau', 'PW', 'PLW', '', 0, 1),
(164, 'Panama', 'PA', 'PAN', '', 0, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1),
(167, 'Peru', 'PE', 'PER', '', 0, 1),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1),
(170, 'Poland', 'PL', 'POL', '', 0, 1),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1),
(174, 'Reunion', 'RE', 'REU', '', 0, 1),
(175, 'Romania', 'RO', 'ROM', '', 0, 1),
(176, 'Russian Federation', 'RU', 'RUS', '', 0, 1),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1),
(195, 'Spain', 'ES', 'ESP', '', 0, 1),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1),
(203, 'Sweden', 'SE', 'SWE', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1),
(207, 'Tajikistan', 'TJ', 'TJK', '', 0, 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1),
(209, 'Thailand', 'TH', 'THA', '', 0, 1),
(210, 'Togo', 'TG', 'TGO', '', 0, 1),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1),
(212, 'Tonga', 'TO', 'TON', '', 0, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1),
(216, 'Turkmenistan', 'TM', 'TKM', '', 0, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1),
(220, 'Ukraine', 'UA', 'UKR', '', 0, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1),
(226, 'Uzbekistan', 'UZ', 'UZB', '', 0, 1),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1),
(242, 'Montenegro', 'ME', 'MNE', '', 0, 1),
(243, 'Serbia', 'RS', 'SRB', '', 0, 1),
(244, 'Aaland Islands', 'AX', 'ALA', '', 0, 1),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '', 0, 1),
(246, 'Curacao', 'CW', 'CUW', '', 0, 1),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', '', 0, 1),
(248, 'South Sudan', 'SS', 'SSD', '', 0, 1),
(249, 'St. Barthelemy', 'BL', 'BLM', '', 0, 1),
(250, 'St. Martin (French part)', 'MF', 'MAF', '', 0, 1),
(251, 'Canary Islands', 'IC', 'ICA', '', 0, 1),
(252, 'Ascension Island (British)', 'AC', 'ASC', '', 0, 1),
(253, 'Kosovo, Republic of', 'XK', 'UNK', '', 0, 1),
(254, 'Isle of Man', 'IM', 'IMN', '', 0, 1),
(255, 'Tristan da Cunha', 'TA', 'SHN', '', 0, 1),
(256, 'Guernsey', 'GG', 'GGY', '', 0, 1),
(257, 'Jersey', 'JE', 'JEY', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon`
--

CREATE TABLE `oc_coupon` (
  `coupon_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(20) NOT NULL,
  `type` char(1) NOT NULL,
  `discount` decimal(15,4) NOT NULL,
  `logged` tinyint(1) NOT NULL,
  `shipping` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `uses_total` int(11) NOT NULL,
  `uses_customer` varchar(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_category`
--

CREATE TABLE `oc_coupon_category` (
  `coupon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_history`
--

CREATE TABLE `oc_coupon_history` (
  `coupon_history_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_product`
--

CREATE TABLE `oc_coupon_product` (
  `coupon_product_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_currency`
--

CREATE TABLE `oc_currency` (
  `currency_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` double(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_currency`
--

INSERT INTO `oc_currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(1, 'Pound Sterling', 'GBP', '£', '', '2', 29720.44000000, 1, '2019-01-09 16:40:33'),
(4, 'US Dollar', 'USD', '$', '', '2', 23245.00000000, 1, '2019-01-09 16:45:56'),
(3, 'Euro', 'EUR', '', '€', '2', 27137.74000000, 1, '2019-01-09 16:39:38'),
(2, 'Vietnam đồng', 'VND', '', 'đ', '0', 1.00000000, 1, '2019-05-16 08:11:36');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer`
--

CREATE TABLE `oc_customer` (
  `customer_id` int(11) NOT NULL,
  `customer_group_id` int(11) DEFAULT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) DEFAULT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) DEFAULT NULL,
  `password` varchar(40) NOT NULL,
  `password_temp` varchar(100) DEFAULT NULL,
  `salt` varchar(9) NOT NULL,
  `salt_temp` varchar(9) DEFAULT NULL,
  `cart` text,
  `wishlist` text,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `customer_source` varchar(128) DEFAULT NULL,
  `note` text,
  `custom_field` text,
  `ip` varchar(40) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `safe` tinyint(1) DEFAULT NULL,
  `token` text,
  `code` varchar(40) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_activity`
--

CREATE TABLE `oc_customer_activity` (
  `customer_activity_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_affiliate`
--

CREATE TABLE `oc_customer_affiliate` (
  `customer_id` int(11) NOT NULL,
  `company` varchar(40) NOT NULL,
  `website` varchar(255) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `commission` decimal(4,2) NOT NULL DEFAULT '0.00',
  `tax` varchar(64) NOT NULL,
  `payment` varchar(6) NOT NULL,
  `cheque` varchar(100) NOT NULL,
  `paypal` varchar(64) NOT NULL,
  `bank_name` varchar(64) NOT NULL,
  `bank_branch_number` varchar(64) NOT NULL,
  `bank_swift_code` varchar(64) NOT NULL,
  `bank_account_name` varchar(64) NOT NULL,
  `bank_account_number` varchar(64) NOT NULL,
  `custom_field` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_approval`
--

CREATE TABLE `oc_customer_approval` (
  `customer_approval_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `type` varchar(9) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_group`
--

CREATE TABLE `oc_customer_group` (
  `customer_group_id` int(11) NOT NULL,
  `approval` int(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_group_description`
--

CREATE TABLE `oc_customer_group_description` (
  `customer_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_history`
--

CREATE TABLE `oc_customer_history` (
  `customer_history_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_ip`
--

CREATE TABLE `oc_customer_ip` (
  `customer_ip_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_login`
--

CREATE TABLE `oc_customer_login` (
  `customer_login_id` int(11) NOT NULL,
  `email` varchar(96) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `total` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_online`
--

CREATE TABLE `oc_customer_online` (
  `ip` varchar(40) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `referer` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_reward`
--

CREATE TABLE `oc_customer_reward` (
  `customer_reward_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_search`
--

CREATE TABLE `oc_customer_search` (
  `customer_search_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category` tinyint(1) NOT NULL,
  `description` tinyint(1) NOT NULL,
  `products` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_transaction`
--

CREATE TABLE `oc_customer_transaction` (
  `customer_transaction_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_wishlist`
--

CREATE TABLE `oc_customer_wishlist` (
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field`
--

CREATE TABLE `oc_custom_field` (
  `custom_field_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `validation` varchar(255) NOT NULL,
  `location` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_customer_group`
--

CREATE TABLE `oc_custom_field_customer_group` (
  `custom_field_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_description`
--

CREATE TABLE `oc_custom_field_description` (
  `custom_field_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_value`
--

CREATE TABLE `oc_custom_field_value` (
  `custom_field_value_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_value_description`
--

CREATE TABLE `oc_custom_field_value_description` (
  `custom_field_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_demo_data`
--

CREATE TABLE `oc_demo_data` (
  `id` int(11) NOT NULL,
  `data_id` int(11) NOT NULL,
  `table_name` varchar(255) DEFAULT '',
  `theme_group` varchar(255) DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_demo_data`
--

INSERT INTO `oc_demo_data` (`id`, `data_id`, `table_name`, `theme_group`) VALUES
(1, 75, 'product', 'beauty'),
(2, 81, 'product', 'beauty'),
(3, 89, 'product', 'beauty'),
(4, 90, 'product', 'beauty'),
(5, 91, 'product', 'beauty'),
(6, 92, 'product', 'beauty'),
(7, 93, 'product', 'beauty'),
(8, 94, 'product', 'beauty'),
(9, 95, 'product', 'beauty'),
(10, 96, 'product', 'beauty'),
(11, 10, 'category', 'beauty'),
(12, 11, 'category', 'beauty'),
(13, 13, 'category', 'beauty'),
(14, 15, 'category', 'beauty'),
(15, 16, 'category', 'beauty'),
(16, 4, 'collection', 'beauty'),
(17, 5, 'collection', 'beauty'),
(18, 6, 'collection', 'beauty'),
(19, 7, 'collection', 'beauty');

-- --------------------------------------------------------

--
-- Table structure for table `oc_discount`
--

CREATE TABLE `oc_discount` (
  `discount_id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `discount_type_id` int(11) NOT NULL,
  `discount_status_id` int(11) NOT NULL,
  `usage_limit` int(11) NOT NULL DEFAULT '0',
  `times_used` int(11) NOT NULL DEFAULT '0',
  `config` mediumtext,
  `order_source` varchar(128) DEFAULT NULL,
  `customer_group` varchar(128) DEFAULT 'All',
  `start_at` datetime NOT NULL,
  `end_at` datetime DEFAULT NULL,
  `pause_reason` varchar(256) DEFAULT NULL,
  `paused_at` datetime DEFAULT NULL,
  `delete_reason` varchar(256) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_discount_description`
--

CREATE TABLE `oc_discount_description` (
  `discount_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` varchar(256) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_discount_status`
--

CREATE TABLE `oc_discount_status` (
  `discount_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(128) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_discount_status`
--

INSERT INTO `oc_discount_status` (`discount_status_id`, `language_id`, `name`, `description`) VALUES
(1, 2, 'Đã lập lịch', NULL),
(2, 2, 'Đang hoạt đông', NULL),
(3, 2, 'Hết hạn', NULL),
(4, 2, 'Đã xóa', NULL),
(1, 1, 'Scheduled', NULL),
(2, 1, 'Activated', NULL),
(3, 1, 'Paused', NULL),
(4, 1, 'Deleted', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `oc_discount_to_store`
--

CREATE TABLE `oc_discount_to_store` (
  `discount_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_discount_type`
--

CREATE TABLE `oc_discount_type` (
  `discount_type_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(128) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_discount_type`
--

INSERT INTO `oc_discount_type` (`discount_type_id`, `language_id`, `name`, `description`) VALUES
(1, 2, 'Chiết khấu theo tổng giá trị đơn hàng', NULL),
(2, 2, 'Chiết khấu theo từng sản phẩm', NULL),
(3, 2, 'Chiết khấu theo loại sản phẩm', NULL),
(4, 2, 'Chiết khấu theo nhà cung cấp', NULL),
(1, 1, 'Discount by total order value', NULL),
(2, 1, 'Discount by each product', NULL),
(3, 1, 'Discount by product type', NULL),
(4, 1, 'Discount by supplier', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `oc_download`
--

CREATE TABLE `oc_download` (
  `download_id` int(11) NOT NULL,
  `filename` varchar(160) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_download_description`
--

CREATE TABLE `oc_download_description` (
  `download_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_event`
--

CREATE TABLE `oc_event` (
  `event_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `trigger` text NOT NULL,
  `action` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_event`
--

INSERT INTO `oc_event` (`event_id`, `code`, `trigger`, `action`, `status`, `sort_order`) VALUES
(1, 'activity_customer_add', 'catalog/model/account/customer/addCustomer/after', 'event/activity/addCustomer', 1, 0),
(2, 'activity_customer_edit', 'catalog/model/account/customer/editCustomer/after', 'event/activity/editCustomer', 1, 0),
(3, 'activity_customer_password', 'catalog/model/account/customer/editPassword/after', 'event/activity/editPassword', 1, 0),
(4, 'activity_customer_forgotten', 'catalog/model/account/customer/editCode/after', 'event/activity/forgotten', 1, 0),
(5, 'activity_transaction', 'catalog/model/account/customer/addTransaction/after', 'event/activity/addTransaction', 1, 0),
(6, 'activity_customer_login', 'catalog/model/account/customer/deleteLoginAttempts/after', 'event/activity/login', 1, 0),
(7, 'activity_address_add', 'catalog/model/account/address/addAddress/after', 'event/activity/addAddress', 1, 0),
(8, 'activity_address_edit', 'catalog/model/account/address/editAddress/after', 'event/activity/editAddress', 1, 0),
(9, 'activity_address_delete', 'catalog/model/account/address/deleteAddress/after', 'event/activity/deleteAddress', 1, 0),
(10, 'activity_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'event/activity/addAffiliate', 1, 0),
(11, 'activity_affiliate_edit', 'catalog/model/account/customer/editAffiliate/after', 'event/activity/editAffiliate', 1, 0),
(12, 'activity_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'event/activity/addOrderHistory', 1, 0),
(13, 'activity_return_add', 'catalog/model/account/return/addReturn/after', 'event/activity/addReturn', 1, 0),
(14, 'mail_transaction', 'catalog/model/account/customer/addTransaction/after', 'mail/transaction', 1, 0),
(15, 'mail_forgotten', 'catalog/model/account/customer/editCode/after', 'mail/forgotten', 1, 0),
(16, 'mail_customer_add', 'catalog/model/account/customer/addCustomer/after', 'mail/register', 1, 0),
(17, 'mail_customer_alert', 'catalog/model/account/customer/addCustomer/after', 'mail/register/alert', 1, 0),
(18, 'mail_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate', 1, 0),
(19, 'mail_affiliate_alert', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate/alert', 1, 0),
(20, 'mail_voucher', 'catalog/model/checkout/order/addOrderHistory/after', 'extension/total/voucher/send', 1, 0),
(21, 'mail_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order', 1, 0),
(22, 'mail_order_alert', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order/alert', 1, 0),
(23, 'statistics_review_add', 'catalog/model/catalog/review/addReview/after', 'event/statistics/addReview', 1, 0),
(24, 'statistics_return_add', 'catalog/model/account/return/addReturn/after', 'event/statistics/addReturn', 1, 0),
(25, 'statistics_order_history', 'catalog/model/checkout/order/addOrderHistory/after', 'event/statistics/addOrderHistory', 1, 0),
(26, 'admin_mail_affiliate_approve', 'admin/model/customer/customer_approval/approveAffiliate/after', 'mail/affiliate/approve', 1, 0),
(27, 'admin_mail_affiliate_deny', 'admin/model/customer/customer_approval/denyAffiliate/after', 'mail/affiliate/deny', 1, 0),
(28, 'admin_mail_customer_approve', 'admin/model/customer/customer_approval/approveCustomer/after', 'mail/customer/approve', 1, 0),
(29, 'admin_mail_customer_deny', 'admin/model/customer/customer_approval/denyCustomer/after', 'mail/customer/deny', 1, 0),
(30, 'admin_mail_reward', 'admin/model/customer/customer/addReward/after', 'mail/reward', 1, 0),
(31, 'admin_mail_transaction', 'admin/model/customer/customer/addTransaction/after', 'mail/transaction', 1, 0),
(32, 'admin_mail_return', 'admin/model/sale/return/addReturn/after', 'mail/return', 1, 0),
(33, 'admin_mail_forgotten', 'admin/model/user/user/editCode/after', 'mail/forgotten', 1, 0),
(34, 'add order', 'admin/model/sale/order/addOrder/after', 'event/statistics/createOrder', 1, 0),
(35, 'edit order', 'admin/model/sale/order/updateProductOrder/after', 'event/statistics/editOrder', 1, 0),
(36, 'change status order', 'admin/model/sale/order/updateStatus/after', 'event/statistics/changeStatus', 1, 0),
(37, 'delete order', 'admin/model/sale/order/deleteOrderById/after', 'event/statistics/deleteOrder', 1, 0),
(38, 'edit order 2', 'admin/model/sale/order/editOrder/after', 'event/statistics/editOrderDetail', 1, 0),
(39, 'change status order 2', 'admin/model/sale/order/editOrder/before', 'event/statistics/updateStatusOrderDetail', 1, 0),
(40, 'create new order', 'admin/model/sale/order/addOrder/after', 'event/report_order/afterCreateOrder', 1, 0),
(41, 'edit order in list', 'admin/model/sale/order/updateColumnTotalOrder/after', 'event/report_order/editOrderInOrderList', 1, 0),
(42, 'change status order in order list', 'admin/model/sale/order/updateStatus/after', 'event/report_order/updateOrderStatusInOrderList', 1, 0),
(43, 'edit order in order detail', 'admin/model/sale/order/editOrder/after', 'event/report_order/editOrderInOrderDetail', 1, 0),
(44, 'create new order', 'admin/model/sale/order/addOrder/after', 'event/report_product/afterCreateOrder', 1, 0),
(45, 'edit order in list', 'admin/model/sale/order/updateProductOrder/after', 'event/report_product/editOrderInOrderList', 1, 0),
(46, 'change status order in order list', 'admin/model/sale/order/updateStatus/after', 'event/report_product/updateOrderStatusInOrderList', 1, 0),
(47, 'edit order in order detail', 'admin/model/sale/order/editOrder/after', 'event/report_product/editOrderInOrderDetail', 1, 0),
(48, 'create customer', 'admin/model/customer/customer/addCustomer/after', 'event/report_customer/createCustomer', 1, 0),
(49, 'delete customer', 'admin/model/customer/customer/deleteCustomer/after', 'event/report_customer/deleteCustomer', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_extension`
--

CREATE TABLE `oc_extension` (
  `extension_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_extension`
--

INSERT INTO `oc_extension` (`extension_id`, `type`, `code`) VALUES
(1, 'payment', 'cod'),
(2, 'total', 'shipping'),
(3, 'total', 'sub_total'),
(4, 'total', 'tax'),
(5, 'total', 'total'),
(6, 'module', 'banner'),
(7, 'module', 'carousel'),
(8, 'total', 'credit'),
(9, 'shipping', 'flat'),
(10, 'total', 'handling'),
(11, 'total', 'low_order_fee'),
(12, 'total', 'coupon'),
(13, 'module', 'category'),
(14, 'module', 'account'),
(15, 'total', 'reward'),
(16, 'total', 'voucher'),
(17, 'payment', 'free_checkout'),
(18, 'module', 'featured'),
(19, 'module', 'slideshow'),
(21, 'dashboard', 'activity'),
(22, 'dashboard', 'sale'),
(23, 'dashboard', 'recent'),
(24, 'dashboard', 'order'),
(25, 'dashboard', 'online'),
(26, 'dashboard', 'map'),
(27, 'dashboard', 'customer'),
(28, 'dashboard', 'chart'),
(29, 'report', 'sale_coupon'),
(31, 'report', 'customer_search'),
(32, 'report', 'customer_transaction'),
(33, 'report', 'product_purchased'),
(34, 'report', 'product_viewed'),
(35, 'report', 'sale_return'),
(36, 'report', 'sale_order'),
(37, 'report', 'sale_shipping'),
(38, 'report', 'sale_tax'),
(39, 'report', 'customer_activity'),
(40, 'report', 'customer_order'),
(41, 'report', 'customer_reward'),
(42, 'module', 'theme_builder_config'),
(159, 'theme', 'fashion_shoezone'),
(183, 'module', 'NovaonApi'),
(184, 'theme', 'beauty_xoiza'),
(185, 'theme', 'beauty_xoiza_1_1'),
(186, 'theme', 'beauty_xoiza_1_2'),
(187, 'theme', 'beauty_xoiza_2'),
(188, 'theme', 'beauty_xoiza_2_1'),
(189, 'theme', 'beauty_xoiza_2_2'),
(190, 'theme', 'fashion_luzun'),
(191, 'theme', 'fashion_luzun_2'),
(192, 'theme', 'fashion_luzun_3'),
(193, 'theme', 'fashion_monstar'),
(194, 'theme', 'fashion_monstar_2'),
(195, 'theme', 'fashion_monstar_3'),
(196, 'theme', 'fashion_shoezone_2'),
(197, 'theme', 'fashion_shoezone_3'),
(198, 'theme', 'fashion_xoiza'),
(199, 'theme', 'fashion_xoiza_2'),
(200, 'theme', 'fashion_xoiza_3'),
(201, 'theme', 'fashion_zork'),
(202, 'theme', 'fashion_zork_2'),
(203, 'theme', 'fashion_zork_3'),
(204, 'theme', 'tech_allegorithmic'),
(205, 'theme', 'tech_allegorithmic_2'),
(206, 'theme', 'tech_allegorithmic_3'),
(207, 'theme', 'tech_digitech'),
(208, 'theme', 'tech_digitech_2'),
(209, 'theme', 'tech_digitech_3'),
(210, 'theme', 'tech_marte'),
(211, 'theme', 'tech_marte_2'),
(212, 'theme', 'tech_marte_3'),
(213, 'theme', 'furniture_lujun_decor'),
(214, 'theme', 'furniture_lujun_decor_2'),
(215, 'theme', 'furniture_lujun_decor_3'),
(216, 'theme', 'fashion_m_boutique'),
(217, 'theme', 'fashion_m_boutique_2'),
(218, 'theme', 'fashion_m_boutique_3'),
(219, 'theme', 'fashion_luvibee'),
(220, 'theme', 'fashion_luvibee_2'),
(221, 'theme', 'fashion_luvibee_3'),
(222, 'theme', 'fashion_ega_fashion'),
(223, 'theme', 'fashion_ega_womenshoes'),
(224, 'theme', 'office_ap_office'),
(225, 'theme', 'fashion_ap_new_fashion'),
(226, 'theme', 'fashion_ap_signme'),
(227, 'theme', 'beauty_ega_cosmetics'),
(228, 'theme', 'beauty_ega_healthyfood'),
(229, 'theme', 'beauty_ap_cosmetic'),
(230, 'theme', 'fashion_flavor'),
(231, 'theme', 'tech_egomall'),
(264, 'module', 'age_restriction'),
(270, 'appstore', 'flash_sale'),
(262, 'analytics', 'google');

-- --------------------------------------------------------

--
-- Table structure for table `oc_extension_install`
--

CREATE TABLE `oc_extension_install` (
  `extension_install_id` int(11) NOT NULL,
  `extension_download_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_extension_install`
--

INSERT INTO `oc_extension_install` (`extension_install_id`, `extension_download_id`, `filename`, `date_added`) VALUES
(7, 0, 'flashsale.ocmod.zip', '2020-04-24 11:52:58'),
(5, 0, 'FlashSale.ocmod.zip', '2020-04-22 14:35:44'),
(6, 0, 'FlashSale.ocmod.zip', '2020-04-23 08:40:59');

-- --------------------------------------------------------

--
-- Table structure for table `oc_extension_path`
--

CREATE TABLE `oc_extension_path` (
  `extension_path_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_extension_path`
--

INSERT INTO `oc_extension_path` (`extension_path_id`, `extension_install_id`, `path`, `date_added`) VALUES
(664, 5, 'admin/controller/extension/module/flash_sale.php', '2020-04-22 14:35:44'),
(665, 5, 'admin/model/extension/module/flash_sale.php', '2020-04-22 14:35:44'),
(666, 5, 'catalog/controller/extension/module/flash_sale.php', '2020-04-22 14:35:44'),
(667, 5, 'catalog/model/extension/module/flash_sale.php', '2020-04-22 14:35:44'),
(668, 5, 'admin/language/en-gb/extension/module/flash_sale.php', '2020-04-22 14:35:44'),
(669, 5, 'admin/view/template/extension/module/flash_sale.twig', '2020-04-22 14:35:44'),
(670, 5, 'admin/view/template/extension/module/flash_sale/flash_sale_config.twig', '2020-04-22 14:35:44'),
(671, 5, 'admin/view/template/extension/module/flash_sale/flash_sale_setup_product.twig', '2020-04-22 14:35:44'),
(672, 5, 'catalog/view/theme/default/template/extension/module/flash_sale.twig', '2020-04-22 14:35:44'),
(673, 5, 'catalog/view/theme/default/template/extension/module/flash_sale_detail.twig', '2020-04-22 14:35:44'),
(674, 6, 'admin/controller/extension/module/flash_sale.php', '2020-04-23 08:40:59'),
(675, 6, 'admin/model/extension/module/flash_sale.php', '2020-04-23 08:40:59'),
(676, 6, 'catalog/controller/extension/module/flash_sale.php', '2020-04-23 08:40:59'),
(677, 6, 'catalog/model/extension/module/flash_sale.php', '2020-04-23 08:40:59'),
(678, 6, 'admin/language/en-gb/extension/module/flash_sale.php', '2020-04-23 08:40:59'),
(679, 6, 'admin/view/template/extension/module/flash_sale', '2020-04-23 08:40:59'),
(680, 6, 'admin/view/template/extension/module/flash_sale.twig', '2020-04-23 08:40:59'),
(681, 6, 'admin/view/template/extension/module/flash_sale/flash_sale_config.twig', '2020-04-23 08:40:59'),
(682, 6, 'admin/view/template/extension/module/flash_sale/flash_sale_setup_product.twig', '2020-04-23 08:40:59'),
(683, 6, 'catalog/view/theme/default/template/extension/module/flash_sale.twig', '2020-04-23 08:40:59'),
(684, 6, 'catalog/view/theme/default/template/extension/module/flash_sale_detail.twig', '2020-04-23 08:40:59'),
(685, 7, 'admin/controller/extension/appstore/flash_sale.php', '2020-04-24 11:53:05'),
(686, 7, 'admin/model/extension/appstore/flash_sale.php', '2020-04-24 11:53:05'),
(687, 7, 'catalog/controller/extension/module/flash_sale.php', '2020-04-24 11:53:05'),
(688, 7, 'catalog/model/extension/module/flash_sale.php', '2020-04-24 11:53:05'),
(689, 7, 'admin/language/en-gb/extension/appstore/flash_sale.php', '2020-04-24 11:53:05'),
(690, 7, 'admin/view/template/extension/appstore/flash_sale.twig', '2020-04-24 11:53:05'),
(691, 7, 'admin/view/template/extension/appstore/flash_sale/flash_sale_setup_product.twig', '2020-04-24 11:53:05'),
(692, 7, 'catalog/view/theme/default/template/extension/module/flash_sale.twig', '2020-04-24 11:53:05'),
(693, 7, 'catalog/view/theme/default/template/extension/module/flash_sale_detail.twig', '2020-04-24 11:53:05');

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter`
--

CREATE TABLE `oc_filter` (
  `filter_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter`
--

INSERT INTO `oc_filter` (`filter_id`, `filter_group_id`, `sort_order`) VALUES
(1, 1, 0),
(2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_description`
--

CREATE TABLE `oc_filter_description` (
  `filter_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter_description`
--

INSERT INTO `oc_filter_description` (`filter_id`, `language_id`, `filter_group_id`, `name`) VALUES
(1, 2, 1, 'Iphone'),
(1, 1, 1, 'Iphone'),
(2, 2, 1, 'Oppo'),
(2, 1, 1, 'Oppo');

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_group`
--

CREATE TABLE `oc_filter_group` (
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter_group`
--

INSERT INTO `oc_filter_group` (`filter_group_id`, `sort_order`) VALUES
(1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_group_description`
--

CREATE TABLE `oc_filter_group_description` (
  `filter_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter_group_description`
--

INSERT INTO `oc_filter_group_description` (`filter_group_id`, `language_id`, `name`) VALUES
(1, 1, 'filter group'),
(1, 2, 'Nhóm bộ lọc');

-- --------------------------------------------------------

--
-- Table structure for table `oc_geo_zone`
--

CREATE TABLE `oc_geo_zone` (
  `geo_zone_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_geo_zone`
--

INSERT INTO `oc_geo_zone` (`geo_zone_id`, `name`, `description`, `date_added`, `date_modified`) VALUES
(3, 'UK VAT Zone', 'UK VAT', '2009-01-06 23:26:25', '2010-02-26 22:33:24'),
(4, 'UK Shipping', 'UK Shipping Zones', '2009-06-23 01:14:53', '2010-12-15 15:18:13');

-- --------------------------------------------------------

--
-- Table structure for table `oc_images`
--

CREATE TABLE `oc_images` (
  `image_id` int(11) NOT NULL,
  `type` varchar(56) NOT NULL DEFAULT 'image',
  `name` varchar(256) DEFAULT NULL,
  `url` varchar(256) NOT NULL,
  `directory` varchar(56) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `source` varchar(56) NOT NULL DEFAULT 'Cloudinary',
  `source_id` varchar(256) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_information`
--

CREATE TABLE `oc_information` (
  `information_id` int(11) NOT NULL,
  `bottom` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_description`
--

CREATE TABLE `oc_information_description` (
  `information_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` mediumtext NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_to_layout`
--

CREATE TABLE `oc_information_to_layout` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_to_store`
--

CREATE TABLE `oc_information_to_store` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_language`
--

CREATE TABLE `oc_language` (
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_language`
--

INSERT INTO `oc_language` (`language_id`, `name`, `code`, `locale`, `image`, `directory`, `sort_order`, `status`) VALUES
(1, 'English', 'en-gb', 'en-US,en_US.UTF-8,en_US,en-gb,english', 'en-gb.png', 'english', 2, 0),
(2, 'Vietnamese', 'vi-vn', 'vi-VN,vi_VN.UTF-8,vi_VN,vi-vn,vietnamese', 'vi-vn.png', 'vietnamese', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout`
--

CREATE TABLE `oc_layout` (
  `layout_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_layout`
--

INSERT INTO `oc_layout` (`layout_id`, `name`) VALUES
(1, 'Home'),
(2, 'Product'),
(3, 'Category'),
(4, 'Default'),
(5, 'Manufacturer'),
(6, 'Account'),
(7, 'Checkout'),
(8, 'Contact'),
(9, 'Sitemap'),
(10, 'Affiliate'),
(11, 'Information'),
(12, 'Compare'),
(13, 'Search'),
(100, 'Blog post'),
(101, 'Blog category'),
(102, 'Blog search'),
(103, 'Blog author');

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout_module`
--

CREATE TABLE `oc_layout_module` (
  `layout_module_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `position` varchar(14) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_layout_module`
--

INSERT INTO `oc_layout_module` (`layout_module_id`, `layout_id`, `code`, `position`, `sort_order`) VALUES
(2, 4, '0', 'content_top', 0),
(3, 4, '0', 'content_top', 1),
(20, 5, '0', 'column_left', 2),
(69, 10, 'account', 'column_right', 1),
(68, 6, 'account', 'column_right', 1),
(72, 3, 'category', 'column_left', 1),
(73, 3, 'banner.30', 'column_left', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout_route`
--

CREATE TABLE `oc_layout_route` (
  `layout_route_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_layout_route`
--

INSERT INTO `oc_layout_route` (`layout_route_id`, `layout_id`, `store_id`, `route`) VALUES
(38, 6, 0, 'account/%'),
(17, 10, 0, 'affiliate/%'),
(44, 3, 0, 'product/category'),
(56, 1, 0, 'common/home'),
(20, 2, 0, 'product/product'),
(24, 11, 0, 'information/information'),
(23, 7, 0, 'checkout/%'),
(31, 8, 0, 'information/contact'),
(32, 9, 0, 'information/sitemap'),
(34, 4, 0, ''),
(45, 5, 0, 'product/manufacturer'),
(52, 12, 0, 'product/compare'),
(53, 13, 0, 'product/search'),
(57, 100, 0, 'extension/d_blog_module/post'),
(58, 101, 0, 'extension/d_blog_module/category'),
(59, 102, 0, 'extension/d_blog_module/search'),
(60, 103, 0, 'extension/d_blog_module/author');

-- --------------------------------------------------------

--
-- Table structure for table `oc_length_class`
--

CREATE TABLE `oc_length_class` (
  `length_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_length_class`
--

INSERT INTO `oc_length_class` (`length_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '10.00000000'),
(3, '0.39370000');

-- --------------------------------------------------------

--
-- Table structure for table `oc_length_class_description`
--

CREATE TABLE `oc_length_class_description` (
  `length_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_length_class_description`
--

INSERT INTO `oc_length_class_description` (`length_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Centimeter', 'cm'),
(2, 1, 'Millimeter', 'mm'),
(3, 1, 'Inch', 'in'),
(1, 2, 'Centimeter', 'cm'),
(2, 2, 'Millimeter', 'mm'),
(3, 2, 'Inch', 'in');

-- --------------------------------------------------------

--
-- Table structure for table `oc_location`
--

CREATE TABLE `oc_location` (
  `location_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `address` text NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `geocode` varchar(32) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `open` text NOT NULL,
  `comment` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_manufacturer`
--

CREATE TABLE `oc_manufacturer` (
  `manufacturer_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL,
  `manufacturer_code` varchar(50) DEFAULT NULL,
  `telephone` varchar(30) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `tax_code` varchar(200) DEFAULT NULL,
  `address` text,
  `province` varchar(50) DEFAULT NULL,
  `district` varchar(50) DEFAULT NULL,
  `status` int(4) NOT NULL DEFAULT '1',
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_manufacturer_to_store`
--

CREATE TABLE `oc_manufacturer_to_store` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_marketing`
--

CREATE TABLE `oc_marketing` (
  `marketing_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(64) NOT NULL,
  `clicks` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_menu`
--

CREATE TABLE `oc_menu` (
  `menu_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `menu_type_id` tinyint(2) DEFAULT NULL,
  `target_value` varchar(256) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_menu`
--

INSERT INTO `oc_menu` (`menu_id`, `parent_id`, `menu_type_id`, `target_value`) VALUES
(1, NULL, NULL, NULL),
(2, 1, 5, '?route=common/home'),
(3, 1, 5, '?route=common/shop'),
(4, 1, 5, '?route=contact/contact'),
(5, 1, 4, 'chinh-sach-va-bao-mat');

-- --------------------------------------------------------

--
-- Table structure for table `oc_menu_description`
--

CREATE TABLE `oc_menu_description` (
  `menu_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_menu_description`
--

INSERT INTO `oc_menu_description` (`menu_id`, `name`, `language_id`) VALUES
(1, 'Menu Chính', 1),
(1, 'Menu Chính', 2),
(2, 'Trang chủ', 1),
(2, 'Trang chủ', 2),
(3, 'Sản phẩm', 1),
(3, 'Sản phẩm', 2),
(4, 'Liên hệ', 1),
(4, 'Liên hệ', 2),
(5, 'Chính sách', 1),
(5, 'Chính sách', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_menu_type`
--

CREATE TABLE `oc_menu_type` (
  `menu_type_id` int(11) NOT NULL,
  `value` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_menu_type`
--

INSERT INTO `oc_menu_type` (`menu_type_id`, `value`) VALUES
(1, 'category'),
(2, 'collection'),
(3, 'product'),
(4, 'url'),
(5, 'built-in page'),
(6, 'blog');

-- --------------------------------------------------------

--
-- Table structure for table `oc_menu_type_description`
--

CREATE TABLE `oc_menu_type_description` (
  `menu_type_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `description` varchar(256) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_menu_type_description`
--

INSERT INTO `oc_menu_type_description` (`menu_type_id`, `language_id`, `description`) VALUES
(1, 1, 'Category'),
(1, 2, 'Loại sản phẩm'),
(2, 1, 'Collection'),
(2, 2, 'Bộ sưu tập'),
(3, 1, 'Product'),
(3, 2, 'Sản phẩm'),
(4, 1, 'Url'),
(4, 2, 'Đường dẫn'),
(5, 1, 'Built-in Page'),
(5, 2, 'Trang mặc định'),
(6, 1, 'Blog'),
(6, 2, 'Bài viết');

-- --------------------------------------------------------

--
-- Table structure for table `oc_migration`
--

CREATE TABLE `oc_migration` (
  `migration_id` int(11) NOT NULL,
  `migration` varchar(255) NOT NULL,
  `run` tinyint(1) NOT NULL DEFAULT '1',
  `date_run` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_migration`
--

INSERT INTO `oc_migration` (`migration_id`, `migration`, `run`, `date_run`) VALUES
(1, 'migration_20190718_default_policy_gg_shopping', 1, '2020-04-16 11:12:16'),
(2, 'migration_20190729_enable_seo', 1, '2020-04-16 11:12:17'),
(3, 'migration_20190729_2_fix_demo_data_sql', 1, '2020-04-16 11:12:17'),
(4, 'migration_20190731_remake_menu', 1, '2020-04-16 11:12:17'),
(5, 'migration_20190810_staff_permission', 1, '2020-04-16 11:12:17'),
(6, 'migration_20190822_product_multi_version', 1, '2020-04-16 11:12:17'),
(7, 'migration_20190828_chatbot', 1, '2020-04-16 11:12:17'),
(8, 'migration_20190828_transport', 1, '2020-04-16 11:12:17'),
(9, 'migration_20190903_sync_staffs_to_welcome', 1, '2020-04-16 11:12:17'),
(10, 'migration_20190904_keep_edited_demo_data', 1, '2020-04-16 11:12:17'),
(11, 'migration_20190930_increase_decimal_order', 1, '2020-04-16 11:12:17'),
(12, 'migration_20191015_new_report_tables_and_events', 1, '2020-04-16 11:12:17'),
(13, 'migration_20191016_new_report_data', 1, '2020-04-16 11:12:17'),
(14, 'migration_20191023_new_report_events', 1, '2020-04-16 11:12:17'),
(15, 'migration_20191024_report_order_from_shop', 1, '2020-04-16 11:12:17'),
(16, 'migration_20191028_add_field_from_domain_order', 1, '2020-04-16 11:12:17'),
(17, 'migration_20191029_default_delivery_method', 1, '2020-04-16 11:12:17'),
(18, 'migration_20191111_blog_permission_and_seo', 1, '2020-04-16 11:12:17'),
(19, 'migration_20191115_order_success_seo_and_text', 1, '2020-04-16 11:12:17'),
(20, 'migration_20191116_add_settings_classify_permission', 1, '2020-04-16 11:12:17'),
(21, 'migration_20191127_sync_onboading_to_welcome', 1, '2020-04-16 11:12:17'),
(22, 'migration_20191209_default_store', 1, '2020-04-16 11:12:17'),
(23, 'migration_20191210_add_columns_for_pos', 1, '2020-04-16 11:12:17'),
(24, 'migration_20191223_save_image_url_to_database', 1, '2020-04-16 11:12:17'),
(25, 'migration_20191225_builder_product_per_row_mobile', 1, '2020-04-16 11:12:17'),
(26, 'migration_20191226_register_success_text', 1, '2020-04-16 11:12:17'),
(27, 'migration_20200103_soft_delete_product', 1, '2020-04-16 11:12:17'),
(28, 'migration_20200117_add_settings_notify_permission', 1, '2020-04-16 11:12:17'),
(29, 'migration_20200212_discount', 1, '2020-04-16 11:12:17'),
(30, 'migration_20200314_chatbot_v2', 1, '2020-04-16 11:12:17'),
(31, 'migration_20200312_store_receipt', 1, '2020-04-16 11:12:17'),
(32, 'migration_20200410_sync_report_order_to_cms', 1, '2020-04-23 08:30:30');

-- --------------------------------------------------------

--
-- Table structure for table `oc_modification`
--

CREATE TABLE `oc_modification` (
  `modification_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(64) NOT NULL,
  `author` varchar(64) NOT NULL,
  `version` varchar(32) NOT NULL,
  `link` varchar(255) NOT NULL,
  `xml` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_modification`
--

INSERT INTO `oc_modification` (`modification_id`, `extension_install_id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`) VALUES
(5, 6, 'Bestme Flash Sale', 'Flash Sale', 'Bestme Asia group', '1.0.1', 'https://novaon.asia', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<modification>\n    <name>Bestme Flash Sale</name>\n    <version>1.0.1</version>\n    <author>Bestme Asia group</author>\n    <link>https://novaon.asia</link>\n    <code>Flash Sale</code>\n    <description>Flash Sale App</description>\n    <file path=\"admin/controller/extension/module/flash_sale.php\">\n        <operation>\n            <search><![CDATA[ if ($this->user->hasPermission(\'access\', \'marketplace/extension\')) { ]]></search>\n            <add position=\"after\"><![CDATA[\n                $marketplace[] = array(\n                    \'name\'     => \"Modules\",\n                    \'href\'     => $this->url->link(\'marketplace/extension&type=module\', \'user_token=\' . $this->session->data[\'user_token\'], true),\n                    \'children\' => array()\n                );\n            ]]></add>\n        </operation>\n    </file>\n</modification>\n', 1, '2020-04-23 08:40:59'),
(6, 7, 'APP FLASH SALE', 'APP FLASH SALE', 'Bestme Asia group', '1.0.1', 'https://novaon.asia', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<modification>\n    <name>APP FLASH SALE</name>\n    <version>1.0.1</version>\n    <author>Bestme Asia group</author>\n    <link>https://novaon.asia</link>\n    <code>APP FLASH SALE</code>\n    <description>APP FLASH SALE</description>\n    <file path=\"admin/controller/extension/appstore/flash_sale.php\">\n        <operation>\n            <search><![CDATA[ if ($this->user->hasPermission(\'access\', \'marketplace/extension\')) { ]]></search>\n            <add position=\"after\"><![CDATA[\n                $marketplace[] = array(\n                    \'name\'     => \"Appstore\",\n                    \'href\'     => $this->url->link(\'marketplace/extension&type=appstore\', \'user_token=\' . $this->session->data[\'user_token\'], true),\n                    \'children\' => array()\n                );\n            ]]></add>\n        </operation>\n    </file>\n</modification>\n', 1, '2020-04-24 11:53:08');

-- --------------------------------------------------------

--
-- Table structure for table `oc_module`
--

CREATE TABLE `oc_module` (
  `module_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(32) NOT NULL,
  `setting` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_module`
--

INSERT INTO `oc_module` (`module_id`, `name`, `code`, `setting`) VALUES
(30, 'Category', 'banner', '{\"name\":\"Category\",\"banner_id\":\"6\",\"width\":\"182\",\"height\":\"182\",\"status\":\"1\"}'),
(29, 'Home Page', 'carousel', '{\"name\":\"Home Page\",\"banner_id\":\"8\",\"width\":\"130\",\"height\":\"100\",\"status\":\"1\"}'),
(28, 'Home Page', 'featured', '{\"name\":\"Home Page\",\"product\":[\"43\",\"40\",\"42\",\"30\"],\"limit\":\"4\",\"width\":\"200\",\"height\":\"200\",\"status\":\"1\"}'),
(27, 'Home Page', 'slideshow', '{\"name\":\"Home Page\",\"banner_id\":\"7\",\"width\":\"1140\",\"height\":\"380\",\"status\":\"1\"}'),
(31, 'Banner 1', 'banner', '{\"name\":\"Banner 1\",\"banner_id\":\"6\",\"width\":\"182\",\"height\":\"182\",\"status\":\"1\"}'),
(50, 'Age Restriction (21)', 'age_restriction', '{\"name\":\"Age Restriction (21)\",\"message\":\"Are you %s and older?\",\"age\":21,\"redirect_url\":\"http:\\/\\/www.example.org\",\"status\":1}');

-- --------------------------------------------------------

--
-- Table structure for table `oc_my_app`
--

CREATE TABLE `oc_my_app` (
  `id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `expiration_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_api_credentials`
--

CREATE TABLE `oc_novaon_api_credentials` (
  `id` int(11) NOT NULL,
  `consumer_key` varchar(255) NOT NULL,
  `consumer_secret` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_novaon_api_credentials`
--

INSERT INTO `oc_novaon_api_credentials` (`id`, `consumer_key`, `consumer_secret`, `create_date`, `status`) VALUES
(1, 'GHNHTYDSTw567', 'GHNDRTUYUKMVFYJKNBGFFGFHJJ7886GH', '2019-06-02 18:15:59', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_group_menu`
--

CREATE TABLE `oc_novaon_group_menu` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_novaon_group_menu`
--

INSERT INTO `oc_novaon_group_menu` (`id`, `parent_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 1, '2019-01-09 16:05:17', '2019-01-09 16:05:17'),
(2, 0, 1, '2019-01-09 16:06:51', '2019-01-09 16:06:51'),
(3, 0, 1, '2019-01-09 16:09:21', '2019-01-09 16:09:21'),
(4, 0, 1, '2019-01-09 16:11:17', '2019-01-09 16:11:17'),
(5, 0, 1, '2019-01-09 16:12:50', '2019-01-09 16:12:50');

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_group_menu_description`
--

CREATE TABLE `oc_novaon_group_menu_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `group_menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_novaon_group_menu_description`
--

INSERT INTO `oc_novaon_group_menu_description` (`id`, `language_id`, `name`, `group_menu_id`) VALUES
(1, 2, 'Menu chính', 1),
(2, 1, 'Menu chính', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_menu_item`
--

CREATE TABLE `oc_novaon_menu_item` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `group_menu_id` int(11) NOT NULL COMMENT 'id của bảng group_menu',
  `status` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_novaon_menu_item`
--

INSERT INTO `oc_novaon_menu_item` (`id`, `parent_id`, `group_menu_id`, `status`, `position`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 1, 0, '2019-01-09 16:05:17', '2019-01-09 16:05:17'),
(2, 0, 1, 1, 0, '2019-01-09 16:05:17', '2019-01-09 16:05:17'),
(3, 0, 1, 1, 0, '2019-01-09 16:05:18', '2019-01-09 16:05:18'),
(1000, 0, 1, 1, 0, '2020-04-16 11:12:16', '2020-04-16 11:12:16');

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_menu_item_description`
--

CREATE TABLE `oc_novaon_menu_item_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `menu_item_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_novaon_menu_item_description`
--

INSERT INTO `oc_novaon_menu_item_description` (`id`, `language_id`, `name`, `menu_item_id`) VALUES
(1, 2, 'Trang chủ', 1),
(2, 1, 'Trang chủ', 1),
(3, 2, 'Sản phẩm', 2),
(4, 1, 'Sản phẩm', 2),
(5, 2, 'Liên hệ', 3),
(6, 1, 'Liên hệ', 3),
(1000, 1, 'Chính sách', 1000),
(1001, 2, 'Chính sách', 1000);

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_relation_table`
--

CREATE TABLE `oc_novaon_relation_table` (
  `id` int(11) NOT NULL,
  `main_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên bảng chính(ví dụ group_menu)',
  `main_id` int(11) NOT NULL COMMENT 'id bảng chính, ví dụ group_menu_id =1)',
  `child_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên bảng con ví dụ menu_item',
  `child_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL COMMENT 'type_id =1 là sản phẩm type_id =2 là danh mục sản phẩm type_id =3 là bài viết type_id =4 là danh mục bài viết type_id =5 là url',
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `redirect` int(11) NOT NULL DEFAULT '0' COMMENT 'nếu = 1 là khi click vào sẽ redirect đến đâu, 0 là dùng dể hover vào show ra các tphan con',
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `weight_number` int(11) NOT NULL COMMENT 'trọng số hiển thị'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_novaon_relation_table`
--

INSERT INTO `oc_novaon_relation_table` (`id`, `main_name`, `main_id`, `child_name`, `child_id`, `type_id`, `url`, `redirect`, `title`, `weight_number`) VALUES
(1, 'menu_item', 1, '', 0, 5, '?route=common/home', 1, '', 0),
(2, 'menu_item', 2, '', 0, 5, '?route=common/shop', 1, '', 0),
(3, 'menu_item', 3, '', 0, 5, '?route=contact/contact', 1, '', 0),
(4, 'group_menu', 1, 'menu_item', 1, 5, '', 0, '', 0),
(5, 'group_menu', 1, 'menu_item', 2, 5, '', 0, '', 0),
(6, 'group_menu', 1, 'menu_item', 3, 5, '', 0, '', 0),
(45, 'group_menu', 1, 'menu_item', 1000, 5, '', 0, '', 0),
(46, 'menu_item', 1000, '', 0, 5, 'chinh-sach-va-bao-mat', 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_type`
--

CREATE TABLE `oc_novaon_type` (
  `id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `weight_number` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_novaon_type`
--

INSERT INTO `oc_novaon_type` (`id`, `status`, `weight_number`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_type_description`
--

CREATE TABLE `oc_novaon_type_description` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_novaon_type_description`
--

INSERT INTO `oc_novaon_type_description` (`id`, `type_id`, `language_id`, `name`) VALUES
(1, 1, 1, 'Product'),
(2, 1, 2, 'Sản phẩm'),
(3, 2, 1, 'Category'),
(4, 2, 2, 'Danh mục sản phẩm'),
(5, 3, 1, 'Blog'),
(6, 3, 2, 'Bài viết'),
(7, 4, 1, 'Blog category'),
(8, 4, 2, 'Danh mục bài viết'),
(9, 5, 1, 'Url'),
(10, 5, 2, 'Url');

-- --------------------------------------------------------

--
-- Table structure for table `oc_option`
--

CREATE TABLE `oc_option` (
  `option_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option`
--

INSERT INTO `oc_option` (`option_id`, `type`, `sort_order`) VALUES
(1, 'radio', 1),
(2, 'checkbox', 2),
(4, 'text', 3),
(5, 'select', 4),
(6, 'textarea', 5),
(7, 'file', 6),
(8, 'date', 7),
(9, 'time', 8),
(10, 'datetime', 9),
(11, 'select', 10),
(12, 'date', 11);

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_description`
--

CREATE TABLE `oc_option_description` (
  `option_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option_description`
--

INSERT INTO `oc_option_description` (`option_id`, `language_id`, `name`) VALUES
(1, 1, 'Radio'),
(2, 1, 'Checkbox'),
(4, 1, 'Text'),
(6, 1, 'Textarea'),
(8, 1, 'Date'),
(7, 1, 'File'),
(5, 1, 'Select'),
(9, 1, 'Time'),
(10, 1, 'Date &amp; Time'),
(12, 1, 'Delivery Date'),
(11, 1, 'Size');

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_value`
--

CREATE TABLE `oc_option_value` (
  `option_value_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option_value`
--

INSERT INTO `oc_option_value` (`option_value_id`, `option_id`, `image`, `sort_order`) VALUES
(43, 1, '', 3),
(32, 1, '', 1),
(45, 2, '', 4),
(44, 2, '', 3),
(42, 5, '', 4),
(41, 5, '', 3),
(39, 5, '', 1),
(40, 5, '', 2),
(31, 1, '', 2),
(23, 2, '', 1),
(24, 2, '', 2),
(46, 11, '', 1),
(47, 11, '', 2),
(48, 11, '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_value_description`
--

CREATE TABLE `oc_option_value_description` (
  `option_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option_value_description`
--

INSERT INTO `oc_option_value_description` (`option_value_id`, `language_id`, `option_id`, `name`) VALUES
(43, 1, 1, 'Large'),
(32, 1, 1, 'Small'),
(45, 1, 2, 'Checkbox 4'),
(44, 1, 2, 'Checkbox 3'),
(31, 1, 1, 'Medium'),
(42, 1, 5, 'Yellow'),
(41, 1, 5, 'Green'),
(39, 1, 5, 'Red'),
(40, 1, 5, 'Blue'),
(23, 1, 2, 'Checkbox 1'),
(24, 1, 2, 'Checkbox 2'),
(48, 1, 11, 'Large'),
(47, 1, 11, 'Medium'),
(46, 1, 11, 'Small');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order`
--

CREATE TABLE `oc_order` (
  `order_id` int(11) NOT NULL,
  `order_code` varchar(50) DEFAULT NULL,
  `pos_id_ref` varchar(50) DEFAULT NULL,
  `customer_ref_id` varchar(128) DEFAULT NULL,
  `invoice_no` int(11) NOT NULL DEFAULT '0',
  `invoice_prefix` varchar(26) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `store_name` varchar(64) NOT NULL,
  `store_url` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `fullname` varchar(255) DEFAULT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `order_payment` int(11) DEFAULT NULL,
  `order_transfer` int(11) DEFAULT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `transport_service_name` varchar(64) DEFAULT NULL,
  `transport_order_code` varchar(64) DEFAULT NULL,
  `transport_name` varchar(64) DEFAULT NULL,
  `transport_money_total` varchar(255) NOT NULL,
  `transport_weight` varchar(255) NOT NULL,
  `transport_status` varchar(255) NOT NULL,
  `transport_current_warehouse` varchar(255) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `custom_field` text NOT NULL,
  `payment_firstname` varchar(32) NOT NULL,
  `payment_lastname` varchar(32) NOT NULL,
  `payment_company` varchar(60) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(10) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `payment_country_id` int(11) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_zone_id` int(11) NOT NULL,
  `payment_address_format` text NOT NULL,
  `payment_custom_field` text NOT NULL,
  `payment_method` varchar(128) NOT NULL,
  `payment_code` varchar(128) NOT NULL,
  `payment_status` tinyint(4) NOT NULL DEFAULT '0',
  `shipping_firstname` varchar(32) NOT NULL,
  `shipping_lastname` varchar(32) NOT NULL,
  `shipping_company` varchar(40) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(10) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `shipping_ward_code` varchar(50) DEFAULT NULL,
  `shipping_district_code` varchar(50) DEFAULT NULL,
  `shipping_province_code` varchar(50) DEFAULT NULL,
  `shipping_country_id` int(11) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_zone_id` int(11) NOT NULL,
  `shipping_address_format` text NOT NULL,
  `shipping_custom_field` text NOT NULL,
  `shipping_method` varchar(128) NOT NULL,
  `shipping_method_value` int(11) DEFAULT NULL,
  `shipping_fee` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `shipping_code` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `order_cancel_comment` varchar(500) DEFAULT NULL,
  `discount` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `previous_order_status_id` varchar(50) DEFAULT NULL,
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `marketing_id` int(11) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL DEFAULT '1.00000000',
  `ip` varchar(40) NOT NULL,
  `forwarded_ip` varchar(40) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `accept_language` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `source` varchar(50) DEFAULT NULL,
  `from_domain` varchar(500) DEFAULT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_history`
--

CREATE TABLE `oc_order_history` (
  `order_history_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(256) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_option`
--

CREATE TABLE `oc_order_option` (
  `order_option_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_product`
--

CREATE TABLE `oc_order_product` (
  `order_product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_version_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `discount` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `reward` int(8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_recurring`
--

CREATE TABLE `oc_order_recurring` (
  `order_recurring_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `recurring_name` varchar(255) NOT NULL,
  `recurring_description` varchar(255) NOT NULL,
  `recurring_frequency` varchar(25) NOT NULL,
  `recurring_cycle` smallint(6) NOT NULL,
  `recurring_duration` smallint(6) NOT NULL,
  `recurring_price` decimal(10,4) NOT NULL,
  `trial` tinyint(1) NOT NULL,
  `trial_frequency` varchar(25) NOT NULL,
  `trial_cycle` smallint(6) NOT NULL,
  `trial_duration` smallint(6) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_recurring_transaction`
--

CREATE TABLE `oc_order_recurring_transaction` (
  `order_recurring_transaction_id` int(11) NOT NULL,
  `order_recurring_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `amount` decimal(10,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_shipment`
--

CREATE TABLE `oc_order_shipment` (
  `order_shipment_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `shipping_courier_id` varchar(255) NOT NULL DEFAULT '',
  `tracking_number` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_status`
--

CREATE TABLE `oc_order_status` (
  `order_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_order_status`
--

INSERT INTO `oc_order_status` (`order_status_id`, `language_id`, `name`) VALUES
(1, 1, 'Draft'),
(2, 1, 'Processing'),
(3, 1, 'Delivering'),
(5, 1, 'Canceled'),
(4, 1, 'Complete'),
(6, 2, 'Nháp'),
(7, 2, 'Đang xử lý'),
(8, 2, 'Đang giao hàng '),
(9, 2, 'Hoàn thành'),
(10, 2, 'Đã hủy');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_tag`
--

CREATE TABLE `oc_order_tag` (
  `order_tag_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_total`
--

CREATE TABLE `oc_order_total` (
  `order_total_id` int(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_to_discount`
--

CREATE TABLE `oc_order_to_discount` (
  `order_id` int(11) NOT NULL,
  `discount_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_voucher`
--

CREATE TABLE `oc_order_voucher` (
  `order_voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_page_contents`
--

CREATE TABLE `oc_page_contents` (
  `page_id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `seo_title` varchar(256) DEFAULT NULL,
  `seo_description` text,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_page_contents`
--

INSERT INTO `oc_page_contents` (`page_id`, `title`, `description`, `seo_title`, `seo_description`, `status`, `date_added`) VALUES
(1000, 'Chính sách và bảo mật', 'I. Chính sách thanh toán - giao hàng\r\n                                <br/>\r\n                                1. GIAO HÀNG – TRẢ TIỀN MẶT (COD)\r\n                                <br/>\r\n                                - Chúng tôi áp dụng hình thức giao hàng COD cho tất cả các vùng miền trên toàn quốc.\r\n                                <br/>\r\n                                2. CHI PHÍ GIAO HÀNG\r\n                                <br/>\r\n                                - Chúng tôi giao hàng miễn phí đối với khách hàng tại nội thành Hà Nội. Các tỉnh thành khác sẽ áp dụng mức chi phí của đối tác vận chuyển khác.\r\n                                <br/>\r\n                                Chính sách đổi trả\r\n                                1. Lý do chấp nhận đổi trả\r\n                                <br/>\r\n                                - Sản phẩm bị lỗi do nhà sản xuất trong 7 ngày đầu sử dụng, đối với khách mua hàng online thì áp dụng từ khi đơn hàng thành công. \r\n                                <br/>\r\n                                2. Yêu cầu cho sản phẩm đổi trả\r\n                                <br/>\r\n                                - Sản phẩm còn nguyên vẹn, đầy đủ nhãn mác, nguyên đai kiện, niêm phong theo quy cách ban đầu.\r\n                                <br/>\r\n                                - Sản phẩm/dịch vụ còn đầy đủ phụ kiện.\r\n                                <br/>\r\n                                3. Thời gian đổi trả\r\n                                <br/>\r\n                                - Trong vòng 07 ngày kể từ ngày nhận sản phẩm.\r\n                                <br/>\r\n                                <br/>\r\n                                II. CHÍNH SÁCH BẢO MẬT\r\n                                <br/>\r\n                                1. Thu thập thông tin cá nhân\r\n                                <br/>\r\n                                - Chúng tôi sẽ dùng thông tin bạn đã cung cấp để xử lý đơn đặt hàng, cung cấp các dịch vụ và thông tin yêu cầu thông qua website và theo yêu cầu của bạn.\r\n                                <br/>\r\n                                - Chúng tôi có thể chuyển tên và địa chỉ cho bên thứ ba để họ giao hàng cho bạn.\r\n                                <br/>\r\n                                2. Bảo mật\r\n                                <br/>\r\n                                - Chúng tôi có biện pháp thích hợp về kỹ thuật và an ninh để ngăn chặn truy cập trái phép hoặc trái pháp luật hoặc mất mát hoặc hủy hoặc thiệt hại cho thông tin của bạn.', 'Chính sách thanh toán - giao hàng, Chính sách đổi trả, Chính sách bảo mật', 'Chính sách thanh toán - giao hàng, Chính sách đổi trả, Chính sách bảo mật', 1, '2020-04-16 11:12:16');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product`
--

CREATE TABLE `oc_product` (
  `product_id` int(11) NOT NULL,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `common_barcode` varchar(128) DEFAULT NULL,
  `common_sku` varchar(64) DEFAULT NULL,
  `common_compare_price` decimal(15,4) DEFAULT NULL,
  `common_price` decimal(15,4) DEFAULT NULL,
  `barcode` varchar(128) DEFAULT NULL,
  `upc` varchar(12) NOT NULL,
  `ean` varchar(14) NOT NULL,
  `jan` varchar(13) NOT NULL,
  `isbn` varchar(17) NOT NULL,
  `mpn` varchar(64) NOT NULL,
  `location` varchar(128) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `sale_on_out_of_stock` tinyint(4) NOT NULL DEFAULT '0',
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_alt` varchar(256) DEFAULT NULL,
  `multi_versions` tinyint(1) NOT NULL DEFAULT '0',
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `price_currency_id` int(11) NOT NULL,
  `compare_price` decimal(15,4) DEFAULT NULL,
  `c_price_currency_id` int(11) NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `tax_class_id` int(11) NOT NULL,
  `date_available` date NOT NULL DEFAULT '0000-00-00',
  `weight` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `weight_class_id` int(11) NOT NULL DEFAULT '0',
  `length` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `width` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `height` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `length_class_id` int(11) NOT NULL DEFAULT '0',
  `subtract` tinyint(1) NOT NULL DEFAULT '1',
  `minimum` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `channel` int(11) DEFAULT '2',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `demo` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` varchar(50) DEFAULT NULL,
  `default_store_id` int(11) DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product`
--

INSERT INTO `oc_product` (`product_id`, `model`, `sku`, `common_barcode`, `common_sku`, `common_compare_price`, `common_price`, `barcode`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `sale_on_out_of_stock`, `stock_status_id`, `image`, `image_alt`, `multi_versions`, `manufacturer_id`, `shipping`, `price`, `price_currency_id`, `compare_price`, `c_price_currency_id`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `channel`, `viewed`, `demo`, `deleted`, `default_store_id`, `date_added`, `date_modified`) VALUES
(75, '', 'SKU123', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 400, 0, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558663987/x2-8888.bestme.test/2.png', '', 0, 14, 1, '240000.0000', 1, '350000.0000', 1, 0, 0, '0000-00-00', '250.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2019-05-20 16:08:44', '2019-05-20 16:08:44'),
(81, '', 'SKU250', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 350, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664073/x2-8888.bestme.test/4.png', '', 0, 15, 1, '140000.0000', 1, '250000.0000', 1, 0, 0, '0000-00-00', '200.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2019-05-20 16:40:20', '2019-05-20 16:40:20'),
(89, '', 'SKU435', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 360, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664151/x2-8888.bestme.test/6.png', '', 0, 17, 1, '350000.0000', 1, '430000.0000', 1, 0, 0, '0000-00-00', '200.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2019-05-20 17:51:54', '2019-05-20 17:51:54'),
(90, '', 'SKU255', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 370, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664248/x2-8888.bestme.test/9.png', '', 0, 18, 1, '260000.0000', 1, '335000.0000', 1, 0, 0, '0000-00-00', '200.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2019-05-21 13:42:58', '2019-05-21 13:42:58'),
(91, '', 'SKU199', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 420, 0, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664345/x2-8888.bestme.test/11.png', '', 0, 19, 1, '199000.0000', 1, '250000.0000', 1, 0, 0, '0000-00-00', '200.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2019-05-21 13:49:13', '2019-05-21 13:49:13'),
(92, '', '', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664478/x2-8888.bestme.test/15.png', '', 0, 20, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2019-05-21 14:16:14', '2019-05-21 14:16:14'),
(93, '', 'SKU360', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 450, 1, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664581/x2-8888.bestme.test/18.png', '', 0, 21, 1, '280000.0000', 1, '360000.0000', 1, 0, 0, '0000-00-00', '200.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2019-05-21 14:28:48', '2019-05-21 14:28:48'),
(94, '', '', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 0, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664663/x2-8888.bestme.test/600x600/20.png', '', 0, 22, 1, '0.0000', 1, '0.0000', 1, 0, 0, '0000-00-00', '1500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2019-05-21 14:43:39', '2019-05-21 14:43:39'),
(95, '', 'SKU850', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 510, 0, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664811/x2-8888.bestme.test/22.png', '', 0, 23, 1, '850000.0000', 1, '1000000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2019-05-21 15:01:39', '2019-05-21 15:01:39'),
(96, '', 'SKU150', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 390, 0, 0, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664954/x2-8888.bestme.test/25.png', '', 0, 23, 1, '215000.0000', 1, '280000.0000', 1, 0, 0, '0000-00-00', '300.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2019-05-21 15:12:38', '2019-05-21 15:12:38');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_attribute`
--

CREATE TABLE `oc_product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_collection`
--

CREATE TABLE `oc_product_collection` (
  `product_collection_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `collection_id` int(11) NOT NULL,
  `sort_order` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_collection`
--

INSERT INTO `oc_product_collection` (`product_collection_id`, `product_id`, `collection_id`, `sort_order`) VALUES
(85, 89, 6, 0),
(78, 81, 4, 0),
(75, 75, 4, 0),
(88, 92, 6, 0),
(87, 90, 5, 0),
(91, 93, 6, 0),
(90, 93, 4, 0),
(89, 92, 4, 0),
(93, 94, 4, 0),
(92, 94, 5, 0),
(95, 95, 5, 0),
(94, 94, 6, 0),
(96, 95, 6, 0),
(97, 96, 4, 0),
(98, 96, 5, 0),
(99, 96, 6, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_description`
--

CREATE TABLE `oc_product_description` (
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `sub_description` text,
  `seo_title` varchar(256) DEFAULT NULL,
  `seo_description` varchar(512) DEFAULT NULL,
  `tag` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_description`
--

INSERT INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `sub_description`, `seo_title`, `seo_description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(75, 2, 'Sữa dưỡng thể trắng da chống nắng Hatomugi SPF Nhật Bản', '&lt;p&gt;*** C&amp;ocirc;ng dụng: &amp;bull; Đ&amp;acirc;y l&amp;agrave; si&amp;ecirc;u phẩm cho m&amp;ugrave;a h&amp;egrave; m&amp;aacute;t lạnh với t&amp;aacute;c dụng vừa chống nắng vừa l&amp;agrave;m trắng da &amp;bull; Gi&amp;uacute;p tr&amp;aacute;nh được t&amp;igrave;nh trạng ch&amp;aacute;y nắng đỏ r&amp;aacute;t khi ra ngo&amp;agrave;i trời m&amp;ugrave;a h&amp;egrave;. &amp;bull; Dễ thẩm thấu ngay v&amp;agrave;o da khi b&amp;ocirc;i kh&amp;ocirc;ng h&amp;acirc;y t&amp;igrave;nh trạng bết d&amp;iacute;nh kh&amp;oacute; chịu. &amp;bull; Gi&amp;uacute;p dưỡng ẩm s&amp;acirc;u, l&amp;agrave;m mềm da. &amp;bull; Gi&amp;uacute;p dưỡng da s&amp;aacute;ng hơn, trắng hơn, x&amp;oacute;a mờ th&amp;acirc;m từng ng&amp;agrave;y. &amp;bull; Gi&amp;uacute;p l&amp;agrave;m dịu da, chống vi&amp;ecirc;m, cải thiện t&amp;igrave;nh trạng mụn, trứng c&amp;aacute;. &amp;bull; L&amp;agrave;m mịn da, giảm sự th&amp;ocirc; r&amp;aacute;p tr&amp;ecirc;n bề mặt da. &amp;bull; C&amp;oacute; t&amp;aacute;c dụng se kh&amp;iacute;t lỗ ch&amp;acirc;n l&amp;ocirc;ng. &amp;bull; L&amp;agrave;m săn chắc da, chống l&amp;atilde;o h&amp;oacute;a, cải thiện dấu hiệu của tuổi t&amp;aacute;c. &amp;bull; B&amp;igrave;nh thường h&amp;oacute;a chu kỳ sừng h&amp;oacute;a của tế b&amp;agrave;o da. &amp;bull; Sữa dưỡng thể l&amp;agrave;m trắng, chống nắng Hatomugi SPF31 PA+++ kh&amp;ocirc;ng g&amp;acirc;y hại cho da v&amp;igrave; th&amp;agrave;nh phần từ hạt &amp;yacute; dĩ &amp;ndash; một dược liệu qu&amp;yacute; v&amp;agrave; c&amp;oacute; &amp;iacute;ch với sức khỏe v&amp;agrave; c&amp;ocirc;ng dụng thần kỳ trong việc l&amp;agrave;m đẹp l&amp;agrave;m trắng da phi thường của người Nhật *** C&amp;aacute;ch d&amp;ugrave;ng: &amp;bull; Thoa đều l&amp;ecirc;n da body v&amp;agrave;o mỗi buổi s&amp;aacute;ng l&amp;agrave; đủ cho cả ng&amp;agrave;y d&amp;agrave;i nắng n&amp;oacute;ng &amp;bull; Chất lotion lỏng nhẹ được thoa l&amp;ecirc;n da, tan v&amp;agrave; thấm ngay v&amp;agrave;o da chỉ sau 2-3s.&lt;/p&gt;', '', '', '', '', '', '', ''),
(81, 2, 'Sữa Rửa Mặt Kose Softymo 190g Chính Hãng Nhật Bản', '&lt;p&gt;✅Sữa rửa mặt Kose Softymo Hyaluronic Acid (tu&amp;yacute;p m&amp;agrave;u hồng): dưỡng ẩm d&amp;agrave;nh cho da kh&amp;ocirc; Tu&amp;yacute;p m&amp;agrave;u hồng: cung cấp th&amp;ecirc;m độ ẩm cho da kh&amp;ocirc;. Sữa rửa mặt Kose Softymo Hyaluronic Acid Cleansing Wash nhẹ nh&amp;agrave;ng đ&amp;aacute;nh bật v&amp;agrave; loại bỏ c&amp;aacute;c vết bẩn v&amp;agrave; tẩy trang. C&amp;aacute;c hạt liti trong Sữa rửa mặt thấm s&amp;acirc;u v&amp;agrave;o trong c&amp;aacute;c khe hở mang đi c&amp;aacute;c chất dầu tr&amp;ecirc;n mặt.&lt;/p&gt;\r\n\r\n&lt;p&gt;✅Sữa rửa mặt Kose Softymo Collagen (tu&amp;yacute;p m&amp;agrave;u xanh): d&amp;agrave;nh cho da thường Tu&amp;yacute;p m&amp;agrave;u xanh: sữa mặt Collagen gi&amp;uacute;p l&amp;agrave;m trẻ h&amp;oacute;a, căng mịn l&amp;agrave;n da, chứa c&amp;aacute;c th&amp;agrave;nh phần tự nhi&amp;ecirc;n v&amp;agrave; tinh chất dược th&amp;acirc;m nhập s&amp;acirc;u v&amp;agrave;o c&amp;aacute;c khe hở lấy đi c&amp;aacute;c chất bẩn v&amp;agrave; dầu tr&amp;ecirc;n da mặt, mang lại l&amp;agrave;n da khỏe khoắn v&amp;agrave; sạch đẹp.&lt;/p&gt;\r\n\r\n&lt;p&gt;✅Sữa rửa mặt Kose Softymo White (tu&amp;yacute;p m&amp;agrave;u trắng): th&amp;iacute;ch hợp cho da mụn. Tu&amp;yacute;p m&amp;agrave;u trắng: Sữa rửa mặt Kose Softymo White gi&amp;uacute;p l&amp;agrave;m sạch s&amp;acirc;u, đồng thời chứa tinh chất l&amp;agrave;m trắng, l&amp;agrave;m mờ c&amp;aacute;c vết th&amp;acirc;m, đốm, cho da bạn s&amp;aacute;ng bừng l&amp;ecirc;n mỗi ng&amp;agrave;y Shop HAT - &amp;quot;C&amp;ugrave;ng bạn Xinh Đẹp - Hạnh Ph&amp;uacute;c&amp;quot;&lt;/p&gt;', '', '', '', '', '', '', ''),
(89, 2, 'Kem chống nắng Laroche-Posay Gel Cream Dry Touch SPF 50', '&lt;p&gt;✴️✴️✴️Kem chống nắng Laroche-Posay Gel Cream Dry Touch SPF 50+ &amp;ndash; ', '', '', '', '', '', '', ''),
(90, 2, 'Kem Chống Nắng Ziajka SPF50 Bảo Vệ Làn Da Cho Bé - Balan', '&lt;p&gt;T&amp;ecirc;n sản phẩm : Kem Chống Nắng Ziajka SPF 50+&lt;/p&gt;\r\n\r\n&lt;p&gt;Xuất xứ : Ba lan&lt;/p&gt;\r\n\r\n&lt;p&gt;Thương Hiệu : Ziajka&lt;/p&gt;\r\n\r\n&lt;p&gt;Dung t&amp;iacute;ch : 125ml Hạn sử dụng : 2/2022&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kem Chống Nắng Ziajka SPF 50+ Dạng sữa kh&amp;ocirc;ng thấm nước c&amp;oacute; t&amp;aacute;c dụng chống nắng gi&amp;uacute;p bảo vệ l&amp;agrave;n da của b&amp;eacute; khi vui chơi, giữ ẩm da b&amp;eacute; m&amp;agrave; kh&amp;ocirc;ng g&amp;acirc;y nhờn d&amp;iacute;nh.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kem Chống Nắng Ziajka SPF 50+ Hấp thụ tia s&amp;aacute;ng tốt, tr&amp;aacute;nh hao hụt vitamin D.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kem Chống Nắng Ziajka SPF 50+ L&amp;agrave;m &amp;ecirc;m dịu da, chống lại tia cực t&amp;iacute;m v&amp;agrave; sự g&amp;acirc;y hại từ c&amp;aacute;c t&amp;aacute;c nh&amp;acirc;n b&amp;ecirc;n ngo&amp;agrave;i.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kem Chống Nắng Ziajka SPF 50+ Gi&amp;uacute;p chăm s&amp;oacute;c da b&amp;eacute; lu&amp;ocirc;n mềm mại, mượt m&amp;agrave;.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kem Chống Nắng Ziajka SPF 50+ Sản phẩm kh&amp;ocirc;ng m&amp;agrave;u, kh&amp;ocirc;ng m&amp;ugrave;i, kh&amp;ocirc;ng g&amp;acirc;y dị ứng rất an to&amp;agrave;n cho b&amp;eacute;.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kem Chống Nắng Ziajka SPF 50+ Dạng sữa n&amp;ecirc;n kh&amp;ocirc;ng hề b&amp;oacute;ng nhờn, kh&amp;ocirc; ngay tại chỗ (dạng sữa thường kh&amp;ocirc; nhanh v&amp;agrave; &amp;iacute;t b&amp;oacute;ng nhờn hơn dạng kem)&lt;/p&gt;\r\n\r\n&lt;p&gt;C&amp;aacute;ch d&amp;ugrave;ng - Lắc đều, b&amp;ocirc;i một lượt kem mỏng l&amp;ecirc;n v&amp;ugrave;ng da.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Tr&amp;aacute;nh v&amp;agrave;o mắt. Kh&amp;ocirc;ng b&amp;ocirc;i l&amp;ecirc;n vết thương hở.&lt;/p&gt;', '', '', '', '', '', '', ''),
(91, 2, 'Kem dưỡng da trắng mượt đều màu ban đêm LOreal Paris White Perfect 50ml', '&lt;p&gt;C&amp;ocirc;ng dụng&lt;/p&gt;\r\n\r\n&lt;p&gt;- Dưỡng trắng đều m&amp;agrave;u: với vitamin Cg, gi&amp;uacute;p giảm v&amp;agrave; c&amp;acirc;n bằng hắc sắc tố ở mức thấp nhất.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Dưỡng da mịn mượt: với Glycerin, tạo v&amp;agrave; lưu giữ độ ẩm tr&amp;ecirc;n da.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Rạng rỡ l&amp;agrave;n da: với tinh thể đ&amp;aacute; qu&amp;yacute; Tourmaline, gi&amp;uacute;p th&amp;uacute;c đẩy qu&amp;aacute; tr&amp;igrave;nh tuần ho&amp;agrave;n dưới da.&lt;/p&gt;\r\n\r\n&lt;p&gt;Hiệu quả&lt;/p&gt;\r\n\r\n&lt;p&gt;- Ngay lập tức, l&amp;agrave;n da được dưỡng ẩm mềm mịn v&amp;agrave; tươi s&amp;aacute;ng r&amp;otilde; rệt.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Sau 1 tuần, l&amp;agrave;n da dần đều m&amp;agrave;u &amp;amp; được cải thiện r&amp;otilde; rệt.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Sau 1 th&amp;aacute;ng, l&amp;agrave;n da trở n&amp;ecirc;n trắng mượt, đều m&amp;agrave;u thấy r&amp;otilde;. Hướng dẫn sử dụng - Sử dụng v&amp;agrave;o mỗi tối, trước khi ngủ.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Sau bước l&amp;agrave;m sạch da, sử dụng một lượng kem vừa đủ (khoảng 1 hạt bắp) v&amp;agrave; chia đều tr&amp;ecirc;n năm điểm: tr&amp;aacute;n, mũi, cằm, hai b&amp;ecirc;n m&amp;aacute;.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Sau đ&amp;oacute;, thoa đều nhẹ nh&amp;agrave;ng hướng từ dưới l&amp;ecirc;n tr&amp;ecirc;n v&amp;agrave; vỗ nhẹ.&lt;/p&gt;\r\n\r\n&lt;p&gt;- N&amp;ecirc;n d&amp;ugrave;ng th&amp;ecirc;m cho v&amp;ugrave;ng da ở cổ. Tr&amp;aacute;nh v&amp;ugrave;ng da quanh mắt. Hướng dẫn bảo quản - Bảo quản nơi kh&amp;ocirc; r&amp;aacute;o, tho&amp;aacute;ng m&amp;aacute;t.&lt;/p&gt;', '', '', '', '', '', '', ''),
(92, 2, 'KEM DƯỠNG DA V7 TONING LIGHT DR JART 50ML', '&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Thương hiệu: Dr.Jart+&lt;/p&gt;\r\n\r\n&lt;p&gt;- Trọng lượng: 50ml&lt;/p&gt;\r\n\r\n&lt;p&gt;1. D&amp;ugrave;ng cho mọi loại da, đặc biệt l&amp;agrave; da tối m&amp;agrave;u hoặc th&amp;acirc;m do mụn.&lt;/p&gt;\r\n\r\n&lt;p&gt;2. L&amp;agrave; sản phẩm trị th&amp;acirc;m v&amp;agrave; l&amp;agrave;m đều m&amp;agrave;u da nhanh nhất, được nghi&amp;ecirc;n cứu v&amp;agrave; sản xuất dưới sự hợp t&amp;aacute;c giữa Mỹ v&amp;agrave; H&amp;agrave;n Quốc; l&amp;agrave; sản phẩm nổi tiếng v&amp;agrave; được b&amp;aacute;n tại Sephora c&amp;ugrave;ng c&amp;aacute;c cửa h&amp;agrave;ng lớn tr&amp;ecirc;n thế giới.&lt;/p&gt;\r\n\r\n&lt;p&gt;3. L&amp;agrave; kem dưỡng trắng, l&amp;agrave;m s&amp;aacute;ng tone da ngay lập tức; l&amp;agrave;m mờ t&amp;agrave;n nhang, th&amp;acirc;m, n&amp;aacute;m v&amp;agrave; đốm n&amp;acirc;u, gi&amp;uacute;p da s&amp;aacute;ng đều m&amp;agrave;u hơn; gi&amp;uacute;p t&amp;aacute;i tạo, phục hồi da; giảm nhờn b&amp;oacute;ng, se kh&amp;iacute;t lỗ ch&amp;acirc;n l&amp;ocirc;ng; ngăn ngừa sự h&amp;igrave;nh th&amp;agrave;nh, ph&amp;aacute;t triển của mụn.&lt;/p&gt;\r\n\r\n&lt;p&gt;4. Chứa Niacinamide, 7 loại vitamin c&amp;ugrave;ng c&amp;aacute;c th&amp;agrave;nh phần Korean Jade Water, White Jade v&amp;agrave; Licorice extract c&amp;oacute; t&amp;aacute;c dụng l&amp;agrave;m da trắng s&amp;aacute;ng, n&amp;acirc;ng tone da. Dr.Jart+ V7 Toning Light l&amp;agrave; Sản phẩm dưỡng trắng mới đang cực hot b&amp;ecirc;n H&amp;agrave;n, đặc biệt được feedback l&amp;agrave; sản phẩm trị th&amp;acirc;m v&amp;agrave; l&amp;agrave;m đều m&amp;agrave;u da nhanh hơn tất cả c&amp;aacute;c loại kem kh&amp;aacute;c.&lt;/p&gt;\r\n\r\n&lt;p&gt;Đ&amp;acirc;y ch&amp;iacute;nh l&amp;agrave; cứu c&amp;aacute;nh cho những bạn c&amp;oacute; l&amp;agrave;n da tối m&amp;agrave;u v&amp;agrave; l&amp;agrave;n da th&amp;acirc;m do mụn. Dr.Jart+ V7 Toning Light được nghi&amp;ecirc;n cứu v&amp;agrave; sản xuất dưới sự hợp t&amp;aacute;c giữa Mỹ v&amp;agrave; H&amp;agrave;n Quốc, l&amp;agrave; sản phẩm nổi tiếng v&amp;agrave; được b&amp;aacute;n tại Sephora c&amp;ugrave;ng c&amp;aacute;c cửa h&amp;agrave;ng lớn tr&amp;ecirc;n thế giới.&lt;/p&gt;', '', '', '', '', '', '', ''),
(93, 2, 'Dầu gội suôn mượt mềm mại Tsubaki Botanical 450ml', '&lt;p&gt;&amp;bull; Dầu gội ngăn ngừa xơ rối gi&amp;uacute;p duy tr&amp;igrave; m&amp;aacute;i t&amp;oacute;c su&amp;ocirc;n mượt, m&amp;ecirc;̀m mại.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với phức hợp giàu dưỡng ch&amp;acirc;́t thiết yếu bổ sung dưỡng chất từ ch&amp;acirc;n t&amp;oacute;c, gi&amp;uacute;p thẳng mượt, &amp;oacute;ng ả.&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;bull; M&amp;ugrave;i hương tinh t&amp;ecirc;́ và gợi cảm được chiết xuất từ t&amp;ocirc;̉ hợp c&amp;aacute;c loại hoa v&amp;agrave; quả mọng.&lt;/p&gt;\r\n\r\n&lt;p&gt;3 bước chăm s&amp;oacute;c t&amp;oacute;c cơ bản:&lt;/p&gt;\r\n\r\n&lt;p&gt;1. Dầu gội: Dầu gội su&amp;ocirc;n mượt mềm mại (Tsubaki smooth shampoo)&lt;/p&gt;\r\n\r\n&lt;p&gt;2. Dầu xả: Dầu xả su&amp;ocirc;n mượt mềm mại (Tsubaki smooth conditioner)&lt;/p&gt;\r\n\r\n&lt;p&gt;3. Xịt dưỡng t&amp;oacute;c: Xịt dưỡng t&amp;oacute;c su&amp;ocirc;n mượt mềm mại (Tsubaki smooth hair water)&lt;/p&gt;\r\n\r\n&lt;p&gt;3 bước chăm s&amp;oacute;c t&amp;oacute;c phục hồi:&lt;/p&gt;\r\n\r\n&lt;p&gt;1. Dầu gội: Dầu gội su&amp;ocirc;n mượt mềm mại (Tsubaki smooth shampoo)&lt;/p&gt;\r\n\r\n&lt;p&gt;2. Kem xả phục hồi: Kem xả phục hồi chuy&amp;ecirc;n s&amp;acirc;u su&amp;ocirc;n mượt mềm mại (Tsubaki smooth treatment)&lt;/p&gt;\r\n\r\n&lt;p&gt;3. Xịt dưỡng t&amp;oacute;c: Xịt dưỡng t&amp;oacute;c su&amp;ocirc;n mượt mềm mại (Tsubaki smooth hair water)&lt;/p&gt;\r\n\r\n&lt;p&gt;HDSD: Giữ nắp chai v&amp;agrave; xoay v&amp;ograve;i ngược chiều kim đồng hồ, lấy một lượng vừa đủ dầu gội l&amp;ecirc;n tay, d&amp;ugrave;ng đầu ng&amp;oacute;n tay xoa đều dầu gội l&amp;ecirc;n t&amp;oacute;c từ gốc đến ngọn v&amp;agrave; m&amp;aacute;t-xa nhẹ nh&amp;agrave;ng da đầu. Sau đ&amp;oacute; xả sạch t&amp;oacute;c với nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Lưu &amp;yacute;: Để xa tầm tay trẻ em. Tr&amp;aacute;nh tiếp x&amp;uacute;c với mắt, trong trường hợp tiếp x&amp;uacute;c với mắt, kh&amp;ocirc;ng được ch&amp;agrave; x&amp;aacute;t, phải rửa ngay với nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Sau 2-3 lần bổ sung dầu gội cần thay vỏ chai mới để tr&amp;aacute;nh vỏ chai bị nhiễm khuẩn. Ngưng sử dụng ngay khi c&amp;oacute; c&amp;aacute;c biểu hiện k&amp;iacute;ch ứng.&lt;/p&gt;\r\n\r\n&lt;p&gt;Xuất xứ: Nhật Bản&lt;/p&gt;', '', '', '', '', '', '', ''),
(94, 2, 'Dầu Gội Pantene chai 900G', '&lt;p&gt;Gi&amp;uacute;p ngăn ngừa 10 dấu hiệu t&amp;oacute;c hư tổn*** ***10 dấu hiệu hư tổn bao gồm:&lt;/p&gt;\r\n\r\n&lt;p&gt;t&amp;oacute;c rối, t&amp;oacute;c xỉn m&amp;agrave;u, dễ g&amp;atilde;y, t&amp;oacute;c chẻ ngọn, t&amp;oacute;c g&amp;atilde;y rụng, t&amp;oacute;c kh&amp;ocirc;, nh&amp;aacute;m, yếu, x&amp;ugrave;,kh&amp;ocirc;ng v&amp;agrave;o nếp.&lt;/p&gt;\r\n\r\n&lt;p&gt;C&amp;ocirc;ng thức Pro-V tăng cường th&amp;ecirc;m dưỡng chất gi&amp;uacute;p ngăn ngừa hư tổn lớp Keratin&lt;/p&gt;\r\n\r\n&lt;p&gt;** &amp;bull; Gi&amp;uacute;p cải thiện sự duy tr&amp;igrave; li&amp;ecirc;n kết protein tự nhi&amp;ecirc;n để ngăn chặn qu&amp;aacute; tr&amp;igrave;nh hư tổn &amp;bull; Gi&amp;uacute;p ngừa t&amp;oacute;c chẻ ngọn mới ph&amp;aacute;t sinh ngay cả sau 3 th&amp;aacute;ng****&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;bull; Kh&amp;ocirc;ng chất m&amp;agrave;u, kh&amp;ocirc;ng Paraben Tr&amp;aacute;nh để d&amp;iacute;nh v&amp;agrave;o mắt. Nếu dầu gội d&amp;iacute;nh v&amp;agrave;o mắt, rửa sạch với nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Bảo quản sản phẩm nơi tho&amp;aacute;ng m&amp;aacute;t, tr&amp;aacute;nh &amp;aacute;nh nắng trực tiếp. Hướng dẫn sử dụng: Thoa dầu gội, m&amp;aacute;t xa nhẹ nh&amp;agrave;ng tr&amp;ecirc;n t&amp;oacute;c ướt/ da đầu.&lt;/p&gt;\r\n\r\n&lt;p&gt;Sau đ&amp;oacute;, gội sạch bằng nước.&lt;/p&gt;', '', '', '', '', '', '', ''),
(95, 2, 'Kem Dưỡng Da Trắng Hồng Vichy Ideal Lumiere Day Cream 50ml', '&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;Loại sản phẩm - Kem dưỡng da trắng hồng căng mọng ban ng&amp;agrave;y.&lt;/p&gt;\r\n\r\n&lt;p&gt;Loại da ph&amp;ugrave; hợp - Ph&amp;ugrave; hợp với mọi loại da.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kh&amp;aacute;ch h&amp;agrave;ng muốn c&amp;oacute; l&amp;agrave;n da trắng hồng căng mọng.&lt;/p&gt;\r\n\r\n&lt;p&gt;Độ an to&amp;agrave;n - Kh&amp;ocirc;ng Paraben.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kh&amp;ocirc;ng dị ứng. - Kiểm nghiệm tr&amp;ecirc;n l&amp;agrave;n da Ch&amp;acirc;u &amp;Aacute; dưới sự gi&amp;aacute;m s&amp;aacute;t của B&amp;aacute;c sĩ da liễu.&lt;/p&gt;\r\n\r\n&lt;p&gt;Th&amp;agrave;nh phần&lt;/p&gt;\r\n\r\n&lt;p&gt;C&amp;ocirc;ng nghệ dưỡng da trắng hồng ti&amp;ecirc;n tiến:&lt;/p&gt;\r\n\r\n&lt;p&gt;1. CHIẾT XUẤT RỄ HOA MẪU ĐƠN V&amp;Agrave; VITAMIN C: Hỗ trợ tăng cường tuần ho&amp;agrave;n m&amp;aacute;u, ngăn ngừa qu&amp;aacute; tr&amp;igrave;nh oxi h&amp;oacute;a, dưỡng trắng tăng cường, cải thiện độ xỉn m&amp;agrave;u v&amp;agrave; tăng sắc hồng cho l&amp;agrave;n da, gi&amp;uacute;p da trắng hồng rạng rỡ.&lt;/p&gt;\r\n\r\n&lt;p&gt;2. HYALURONIC ACID: Với khả năng cấp nước v&amp;agrave; giữ nước gấp ng&amp;agrave;n lần khối lượng, gi&amp;uacute;p dưỡng ẩm tăng cường cho l&amp;agrave;n da.&lt;/p&gt;\r\n\r\n&lt;p&gt;3. NƯỚC KHO&amp;Aacute;NG VICHY CHỨA 15 LOẠI KHO&amp;Aacute;NG CHẤT: L&amp;agrave;m dịu, tăng cường sức đề kh&amp;aacute;ng cho l&amp;agrave;n da, gi&amp;uacute;p da chống lại c&amp;aacute;c t&amp;aacute;c nh&amp;acirc;n từ m&amp;ocirc;i trường.&lt;/p&gt;', '', '', '', '', '', '', ''),
(96, 2, 'Mặt Nạ Ngủ Dưỡng Ẩm Giúp Làm Sáng Da Vichy 15ml', '&lt;p&gt;TH&amp;Ocirc;NG TIN SẢN PHẨM&lt;/p&gt;\r\n\r\n&lt;p&gt;Loại sản phẩm&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kem dưỡng kết hợp mặt nạ ngủ cung cấp nước tức th&amp;igrave; v&amp;agrave; phục hồi chuy&amp;ecirc;n s&amp;acirc;u.&lt;/p&gt;\r\n\r\n&lt;p&gt;Loại da ph&amp;ugrave; hợp&lt;/p&gt;\r\n\r\n&lt;p&gt;- Mọi loại da, kể cả da nhạy cảm.&lt;/p&gt;\r\n\r\n&lt;p&gt;Độ an to&amp;agrave;n&lt;/p&gt;\r\n\r\n&lt;p&gt;- An to&amp;agrave;n với da nhạy cảm.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kh&amp;ocirc;ng Paraben.&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Kiểm nghiệm 100% tr&amp;ecirc;n l&amp;agrave;n da Ch&amp;acirc;u &amp;Aacute; nhạy cảm.&lt;/p&gt;\r\n\r\n&lt;p&gt;Th&amp;agrave;nh phần - 50% nước kho&amp;aacute;ng Vichy Mineralizing Water: gi&amp;uacute;p phục hồi v&amp;agrave; củng cố h&amp;agrave;ng r&amp;agrave;o bảo vệ da.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Chiết xuất bạch quả Ginkgo Extract: cung cấp oxy cho từng tế b&amp;agrave;o, gi&amp;uacute;p da trở n&amp;ecirc;n s&amp;aacute;ng mịn hơn.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Hoạt chất Hyaluronic Acid v&amp;agrave; Aquabioryl: lưu giữ độ ẩm tr&amp;ecirc;n bề mặt da, cho da căng mịn.&lt;/p&gt;', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_discount`
--

CREATE TABLE `oc_product_discount` (
  `product_discount_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_filter`
--

CREATE TABLE `oc_product_filter` (
  `product_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_filter`
--

INSERT INTO `oc_product_filter` (`product_id`, `filter_id`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_image`
--

CREATE TABLE `oc_product_image` (
  `product_image_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_alt` varchar(256) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_image`
--

INSERT INTO `oc_product_image` (`product_image_id`, `product_id`, `image`, `image_alt`, `sort_order`) VALUES
(94, 75, 'https://res.cloudinary.com/novaonx2/image/upload/v1558663981/x2-8888.bestme.test/1.png', '', 0),
(95, 81, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664067/x2-8888.bestme.test/3.png', '', 0),
(106, 89, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664146/x2-8888.bestme.test/5.png', '', 0),
(107, 90, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664238/x2-8888.bestme.test/7.png', '', 0),
(108, 90, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664243/x2-8888.bestme.test/8.png', '', 1),
(109, 91, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664343/x2-8888.bestme.test/10.png', '', 0),
(110, 92, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664461/x2-8888.bestme.test/12.png', '', 0),
(111, 92, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664467/x2-8888.bestme.test/13.png', '', 1),
(112, 92, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664473/x2-8888.bestme.test/14.png', '', 2),
(113, 93, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664574/x2-8888.bestme.test/16.png', '', 0),
(114, 93, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664579/x2-8888.bestme.test/17.png', '', 1),
(115, 94, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664661/x2-8888.bestme.test/19.png', '', 0),
(116, 95, 'https://res.cloudinary.com/novaonx2/image/upload/v1558664809/x2-8888.bestme.test/21.png', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_option`
--

CREATE TABLE `oc_product_option` (
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_option_value`
--

CREATE TABLE `oc_product_option_value` (
  `product_option_value_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtract` tinyint(1) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `price_prefix` varchar(1) NOT NULL,
  `points` int(8) NOT NULL,
  `points_prefix` varchar(1) NOT NULL,
  `weight` decimal(15,8) NOT NULL,
  `weight_prefix` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_recurring`
--

CREATE TABLE `oc_product_recurring` (
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_related`
--

CREATE TABLE `oc_product_related` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_reward`
--

CREATE TABLE `oc_product_reward` (
  `product_reward_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `points` int(8) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_special`
--

CREATE TABLE `oc_product_special` (
  `product_special_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_tag`
--

CREATE TABLE `oc_product_tag` (
  `product_tag_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_category`
--

CREATE TABLE `oc_product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_to_category`
--

INSERT INTO `oc_product_to_category` (`product_id`, `category_id`) VALUES
(75, 10),
(81, 11),
(89, 13),
(90, 13),
(91, 15),
(92, 15),
(93, 16),
(94, 16),
(95, 15),
(96, 15);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_download`
--

CREATE TABLE `oc_product_to_download` (
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_layout`
--

CREATE TABLE `oc_product_to_layout` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_store`
--

CREATE TABLE `oc_product_to_store` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `product_version_id` int(11) NOT NULL DEFAULT '0',
  `quantity` int(11) DEFAULT '0',
  `cost_price` decimal(15,4) DEFAULT '0.0000'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_to_store`
--

INSERT INTO `oc_product_to_store` (`product_id`, `store_id`, `product_version_id`, `quantity`, `cost_price`) VALUES
(75, 0, 0, 0, '0.0000'),
(81, 0, 0, 0, '0.0000'),
(89, 0, 0, 0, '0.0000'),
(90, 0, 0, 0, '0.0000'),
(91, 0, 0, 0, '0.0000'),
(92, 0, 0, 0, '0.0000'),
(93, 0, 0, 0, '0.0000'),
(94, 0, 0, 0, '0.0000'),
(95, 0, 0, 0, '0.0000'),
(96, 0, 0, 0, '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_version`
--

CREATE TABLE `oc_product_version` (
  `product_version_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `version` varchar(256) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `compare_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sku` varchar(64) NOT NULL,
  `barcode` varchar(124) NOT NULL,
  `quantity` int(7) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_recurring`
--

CREATE TABLE `oc_recurring` (
  `recurring_id` int(11) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `duration` int(10) UNSIGNED NOT NULL,
  `cycle` int(10) UNSIGNED NOT NULL,
  `trial_status` tinyint(4) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `trial_frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `trial_duration` int(10) UNSIGNED NOT NULL,
  `trial_cycle` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_recurring_description`
--

CREATE TABLE `oc_recurring_description` (
  `recurring_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_report`
--

CREATE TABLE `oc_report` (
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `report_date` date NOT NULL,
  `type` tinyint(4) NOT NULL,
  `type_description` varchar(100) NOT NULL,
  `d_field` varchar(500) NOT NULL,
  `m_value` varchar(500) NOT NULL,
  `last_updated` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_report_customer`
--

CREATE TABLE `oc_report_customer` (
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `report_date` date NOT NULL,
  `customer_id` int(11) DEFAULT '0',
  `store_id` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_report_order`
--

CREATE TABLE `oc_report_order` (
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `report_date` date NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT '0',
  `total_amount` decimal(18,4) DEFAULT NULL,
  `order_status` int(11) DEFAULT '0',
  `store_id` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_report_product`
--

CREATE TABLE `oc_report_product` (
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `report_date` date NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_version_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(18,4) DEFAULT NULL,
  `total_amount` decimal(18,4) DEFAULT NULL,
  `store_id` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_report_web_traffic`
--

CREATE TABLE `oc_report_web_traffic` (
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `report_date` date NOT NULL,
  `value` varchar(500) NOT NULL,
  `last_updated` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_report_web_traffic`
--

INSERT INTO `oc_report_web_traffic` (`report_time`, `report_date`, `value`, `last_updated`) VALUES
('2020-04-16 04:00:00', '2020-04-16', '4', '2020-04-16 04:59:56'),
('2020-04-16 05:00:00', '2020-04-16', '4', '2020-04-16 05:59:34'),
('2020-04-16 06:00:00', '2020-04-16', '1', '2020-04-16 06:30:42'),
('2020-04-16 10:00:00', '2020-04-16', '1', '2020-04-16 10:43:04'),
('2020-04-17 03:00:00', '2020-04-17', '1', '2020-04-17 03:16:54'),
('2020-04-17 06:00:00', '2020-04-17', '3', '2020-04-17 06:42:33'),
('2020-04-17 07:00:00', '2020-04-17', '2', '2020-04-17 07:47:11'),
('2020-04-17 08:00:00', '2020-04-17', '2', '2020-04-17 08:51:52'),
('2020-04-18 01:00:00', '2020-04-18', '1', '2020-04-18 01:29:43'),
('2020-04-18 03:00:00', '2020-04-18', '1', '2020-04-18 03:33:39'),
('2020-04-18 04:00:00', '2020-04-18', '1', '2020-04-18 04:05:22'),
('2020-04-20 01:00:00', '2020-04-20', '1', '2020-04-20 01:31:21'),
('2020-04-20 04:00:00', '2020-04-20', '1', '2020-04-20 04:37:46'),
('2020-04-21 02:00:00', '2020-04-21', '2', '2020-04-21 02:48:00'),
('2020-04-21 03:00:00', '2020-04-21', '1', '2020-04-21 03:38:07'),
('2020-04-21 10:00:00', '2020-04-21', '1', '2020-04-21 10:13:51'),
('2020-04-22 01:00:00', '2020-04-22', '1', '2020-04-22 01:47:50'),
('2020-04-22 06:00:00', '2020-04-22', '1', '2020-04-22 06:28:50'),
('2020-04-22 07:00:00', '2020-04-22', '3', '2020-04-22 07:36:43'),
('2020-04-22 08:00:00', '2020-04-22', '1', '2020-04-22 08:22:59'),
('2020-04-22 09:00:00', '2020-04-22', '2', '2020-04-22 09:49:20'),
('2020-04-22 11:00:00', '2020-04-22', '1', '2020-04-22 11:07:33'),
('2020-04-23 01:00:00', '2020-04-23', '2', '2020-04-23 01:43:24'),
('2020-04-23 05:00:00', '2020-04-23', '1', '2020-04-23 05:46:51'),
('2020-04-23 06:00:00', '2020-04-23', '2', '2020-04-23 06:48:43'),
('2020-04-23 07:00:00', '2020-04-23', '1', '2020-04-23 07:51:28'),
('2020-04-23 08:00:00', '2020-04-23', '2', '2020-04-23 08:57:24'),
('2020-04-23 09:00:00', '2020-04-23', '5', '2020-04-23 09:36:08'),
('2020-04-23 10:00:00', '2020-04-23', '2', '2020-04-23 10:59:16'),
('2020-04-24 01:00:00', '2020-04-24', '1', '2020-04-24 01:35:40'),
('2020-04-24 03:00:00', '2020-04-24', '1', '2020-04-24 03:23:06'),
('2020-04-24 04:00:00', '2020-04-24', '1', '2020-04-24 04:04:39'),
('2020-04-24 06:00:00', '2020-04-24', '2', '2020-04-24 06:05:28'),
('2020-04-24 07:00:00', '2020-04-24', '1', '2020-04-24 07:39:21'),
('2020-04-27 01:00:00', '2020-04-27', '1', '2020-04-27 01:30:18'),
('2020-04-27 02:00:00', '2020-04-27', '1', '2020-04-27 02:53:12'),
('2020-04-28 01:00:00', '2020-04-28', '1', '2020-04-28 01:21:44'),
('2020-04-28 06:00:00', '2020-04-28', '1', '2020-04-28 06:28:29'),
('2020-04-28 07:00:00', '2020-04-28', '1', '2020-04-28 07:38:28');

-- --------------------------------------------------------

--
-- Table structure for table `oc_return`
--

CREATE TABLE `oc_return` (
  `return_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `product` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `opened` tinyint(1) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `return_action_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `comment` text,
  `date_ordered` date NOT NULL DEFAULT '0000-00-00',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_action`
--

CREATE TABLE `oc_return_action` (
  `return_action_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_return_action`
--

INSERT INTO `oc_return_action` (`return_action_id`, `language_id`, `name`) VALUES
(1, 1, 'Refunded'),
(2, 1, 'Credit Issued'),
(3, 1, 'Replacement Sent'),
(4, 2, 'Refunded'),
(5, 2, 'Credit Issued'),
(6, 2, 'Replacement Sent');

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_history`
--

CREATE TABLE `oc_return_history` (
  `return_history_id` int(11) NOT NULL,
  `return_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_reason`
--

CREATE TABLE `oc_return_reason` (
  `return_reason_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_return_reason`
--

INSERT INTO `oc_return_reason` (`return_reason_id`, `language_id`, `name`) VALUES
(1, 1, 'Dead On Arrival'),
(2, 1, 'Received Wrong Item'),
(3, 1, 'Order Error'),
(4, 1, 'Faulty, please supply details'),
(5, 1, 'Other, please supply details'),
(6, 2, 'Dead On Arrival'),
(7, 2, 'Received Wrong Item'),
(8, 2, 'Order Error'),
(9, 2, 'Faulty, please supply details'),
(10, 2, 'Other, please supply details');

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_status`
--

CREATE TABLE `oc_return_status` (
  `return_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_return_status`
--

INSERT INTO `oc_return_status` (`return_status_id`, `language_id`, `name`) VALUES
(1, 1, 'Pending'),
(3, 1, 'Complete'),
(2, 1, 'Awaiting Products'),
(4, 2, 'Pending'),
(5, 2, 'Complete'),
(6, 2, 'Awaiting Products');

-- --------------------------------------------------------

--
-- Table structure for table `oc_review`
--

CREATE TABLE `oc_review` (
  `review_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_seo_url`
--

CREATE TABLE `oc_seo_url` (
  `seo_url_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `table_name` varchar(256) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_seo_url`
--

INSERT INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`, `table_name`) VALUES
(2063, 0, 1, 'product_id=83', 'd-prod-giay-co-dien-sang-trong-mau-hong', 'product'),
(2064, 0, 2, 'product_id=83', 'd-prod-giay-co-dien-sang-trong-mau-hong', 'product'),
(2065, 0, 1, 'product_id=84', 'd-prod-giay-cao-got-got-kim-sa-xu-doc-la', 'product'),
(2066, 0, 2, 'product_id=84', 'd-prod-giay-cao-got-got-kim-sa-xu-doc-la', 'product'),
(2067, 0, 1, 'product_id=85', 'd-prod-giay-cao-got-quai-trong-phoi-dinh', 'product'),
(2068, 0, 2, 'product_id=85', 'd-prod-giay-cao-got-quai-trong-phoi-dinh', 'product'),
(2069, 0, 1, 'product_id=86', 'd-prod-giay-cao-got-dep-den-no', 'product'),
(2070, 0, 2, 'product_id=86', 'd-prod-giay-cao-got-dep-den-no', 'product'),
(2071, 0, 1, 'product_id=87', 'd-prod-giay-valen-vien-dinh-size-36-37-38', 'product'),
(2072, 0, 2, 'product_id=87', 'd-prod-giay-valen-vien-dinh-size-36-37-38', 'product'),
(2073, 0, 1, 'product_id=88', 'd-prod-giay-sandal-cao-got-quai-trong-got-kim-sa-size-34-den-40', 'product'),
(2074, 0, 2, 'product_id=88', 'd-prod-giay-sandal-cao-got-quai-trong-got-kim-sa-size-34-den-40', 'product'),
(2075, 0, 1, 'product_id=97', 'd-prod-giay-sandal-quai-ngang-anh-7-mau-1', 'product'),
(2076, 0, 2, 'product_id=97', 'd-prod-giay-sandal-quai-ngang-anh-7-mau-1', 'product'),
(2077, 0, 1, 'product_id=98', 'd-prod-dep-sandal-xoan-co-chan', 'product'),
(2078, 0, 2, 'product_id=98', 'd-prod-dep-sandal-xoan-co-chan', 'product'),
(2079, 0, 1, 'product_id=99', 'd-prod-giay-sandal-chien-binh-day-keo-1-ben', 'product'),
(2080, 0, 2, 'product_id=99', 'd-prod-giay-sandal-chien-binh-day-keo-1-ben', 'product'),
(2081, 0, 1, 'product_id=103', 'd-prod-giay-dup-cao-11cm', 'product'),
(2082, 0, 2, 'product_id=103', 'd-prod-giay-dup-cao-11cm', 'product'),
(2035, 0, 2, 'product_id=75', 'product-75', 'product'),
(2036, 0, 1, 'product_id=81', 'product-81', 'product'),
(2037, 0, 2, 'product_id=81', 'product-81', 'product'),
(2038, 0, 1, 'product_id=89', 'product-89', 'product'),
(2039, 0, 2, 'product_id=89', 'product-89', 'product'),
(2040, 0, 1, 'product_id=90', 'product-90', 'product'),
(2041, 0, 2, 'product_id=90', 'product-90', 'product'),
(2042, 0, 1, 'product_id=91', 'product-91', 'product'),
(2043, 0, 2, 'product_id=91', 'product-91', 'product'),
(1000, 0, 1, 'page_id=1000', 'chinh-sach-va-bao-mat', ''),
(1001, 0, 2, 'page_id=1000', 'chinh-sach-va-bao-mat', ''),
(2093, 0, 1, 'common/home', 'home', ''),
(2094, 0, 2, 'common/home', 'trang-chu', ''),
(2095, 0, 1, 'common/shop', 'products', ''),
(2096, 0, 2, 'common/shop', 'san-pham', ''),
(2097, 0, 1, 'contact/contact', 'contact', ''),
(2098, 0, 2, 'contact/contact', 'lien-he', ''),
(2099, 0, 1, 'checkout/profile', 'profile', ''),
(2100, 0, 2, 'checkout/profile', 'tai-khoan', ''),
(2101, 0, 1, 'account/login', 'login', ''),
(2102, 0, 2, 'account/login', 'dang-nhap', ''),
(2103, 0, 1, 'account/register', 'register', ''),
(2104, 0, 2, 'account/register', 'dang-ky', ''),
(2105, 0, 1, 'account/logout', 'logout', ''),
(2106, 0, 2, 'account/logout', 'dang-xuat', ''),
(2107, 0, 1, 'checkout/setting', 'setting', ''),
(2108, 0, 2, 'checkout/setting', 'cai-dat', ''),
(2109, 0, 1, 'checkout/my_orders', 'checkout-cart', ''),
(2110, 0, 2, 'checkout/my_orders', 'gio-hang', ''),
(2111, 0, 1, 'checkout/order_preview', 'payment', ''),
(2112, 0, 2, 'checkout/order_preview', 'thanh-toan', ''),
(2113, 0, 1, 'checkout/order_preview/orderSuccess', 'payment-success', ''),
(2114, 0, 2, 'checkout/order_preview/orderSuccess', 'dat-hang-thanh-cong', ''),
(2115, 0, 1, 'blog/blog', 'blog', ''),
(2116, 0, 2, 'blog/blog', 'bai-viet', ''),
(2044, 0, 1, 'product_id=92', 'product-92', 'product'),
(2045, 0, 2, 'product_id=92', 'product-92', 'product'),
(2046, 0, 1, 'product_id=93', 'product-93', 'product'),
(2047, 0, 2, 'product_id=93', 'product-93', 'product'),
(2048, 0, 1, 'product_id=94', 'product-94', 'product'),
(2049, 0, 2, 'product_id=94', 'product-94', 'product'),
(2050, 0, 1, 'product_id=95', 'product-95', 'product'),
(2051, 0, 2, 'product_id=95', 'product-95', 'product'),
(2052, 0, 1, 'product_id=96', 'product-96', 'product'),
(2053, 0, 2, 'product_id=96', 'product-96', 'product'),
(2054, 0, 2, 'category_id=10', 'category-10', 'category'),
(2055, 0, 2, 'category_id=11', 'category-11', 'category'),
(2056, 0, 2, 'category_id=13', 'category-13', 'category'),
(2057, 0, 2, 'category_id=15', 'category-15', 'category'),
(2058, 0, 2, 'category_id=16', 'category-16', 'category'),
(2034, 0, 1, 'product_id=75', 'product-75', 'product'),
(2059, 0, 2, 'collection=4', 'collection-4', 'category'),
(2060, 0, 2, 'collection=5', 'collection-5', 'category'),
(2061, 0, 2, 'collection=6', 'collection-6', 'category'),
(2062, 0, 2, 'collection=7', 'collection-7', 'category');

-- --------------------------------------------------------

--
-- Table structure for table `oc_session`
--

CREATE TABLE `oc_session` (
  `session_id` varchar(32) NOT NULL,
  `data` text NOT NULL,
  `expire` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_session`
--

INSERT INTO `oc_session` (`session_id`, `data`, `expire`) VALUES
('1182a52e78c5700e1016b32220', '{\"language\":\"vi-vn\",\"currency\":\"VND\",\"user_id\":\"1\",\"user_token\":\"kyZI6H3CLyseawvjK8BxIEJm9tJKRzHS\"}', '2020-04-22 05:35:36'),
('11e6410d9aef2bcdd95ee88593', '{\"language\":\"vi-vn\",\"currency\":\"VND\"}', '2020-04-23 09:26:11'),
('1a7e45025ea25d08c13f7c58f4', '{\"language\":\"vi-vn\",\"currency\":\"VND\",\"user_id\":\"1\",\"user_token\":\"fzSxVxUmPb8OCYajsV4A2NPYEgbX75bI\"}', '2020-04-28 08:37:34'),
('26c3da9eb779d5de5ed41eb77d', '{\"language\":\"vi-vn\",\"currency\":\"VND\"}', '2020-04-23 09:30:04'),
('270cbcccb2bfbb65ce266af0af', '{\"language\":\"vi-vn\",\"currency\":\"VND\"}', '2020-04-23 10:00:11'),
('31a64af6ae794aa11a2f09333f', '{\"language\":\"vi-vn\",\"currency\":\"VND\",\"user_id\":\"1\",\"user_token\":\"WiZSVpvCjN2RP3O7lcDVoLwLqzyA7UKS\"}', '2020-04-20 10:44:11'),
('32844d1ac292be6d87b7a71fc8', '{\"language\":\"vi-vn\",\"currency\":\"VND\",\"user_id\":\"1\",\"user_token\":\"632eeFnmFOIMQtioMRB5YT5FTqS8ahYE\",\"install\":\"bhsV3f5G4v\"}', '2020-04-22 11:31:59'),
('48d781b3d7eacf27ff9c8ee213', '{\"language\":\"vi-vn\",\"currency\":\"USD\"}', '2020-04-23 11:23:14'),
('4afb110c3632f4072cb1101bc7', '{\"language\":\"vi-vn\",\"currency\":\"VND\"}', '2020-04-23 09:26:13'),
('5fe5e06f2c2f3c4c08d725306e', '{\"language\":\"vi-vn\",\"currency\":\"VND\",\"user_id\":\"1\",\"user_token\":\"2BWUJF2RXMbejqW3CVohFIKbsWt7uE5g\"}', '2020-04-21 11:22:20'),
('60e64287a277bb0cebc43dcb13', '{\"language\":\"vi-vn\",\"currency\":\"USD\"}', '2020-04-24 06:29:26'),
('611e8a3dd210292f1bd85fc870', '{\"language\":\"vi-vn\",\"currency\":\"USD\"}', '2020-04-22 07:52:26'),
('6dcf4c3f820b0cba798715a443', '{\"language\":\"vi-vn\",\"currency\":\"VND\"}', '2020-04-23 09:22:21'),
('7716b496b687b68373ba7d717a', '{\"language\":\"vi-vn\",\"currency\":\"VND\"}', '2020-04-23 09:21:31'),
('778d7702813f472b8a557f9461', '{\"language\":\"vi-vn\",\"currency\":\"USD\"}', '2020-04-23 02:07:24'),
('7bece9bbd273a206fb4cccdbdf', '{\"language\":\"vi-vn\",\"currency\":\"VND\"}', '2020-04-16 05:06:50'),
('836f3ffa2f4fe0c98b7f7402e0', '{\"language\":\"vi-vn\",\"currency\":\"VND\",\"user_id\":\"1\",\"user_token\":\"kl3Lypz3h7ZhH2EX89StB75k468SbhWe\",\"install\":\"Oq35F2lyIW\"}', '2020-04-24 11:26:38'),
('8ee3087b01bc2abe94efd30fb9', '{\"language\":\"vi-vn\",\"currency\":\"VND\"}', '2020-04-23 09:30:02'),
('a74bf3973ea79412a67330a693', '{\"language\":\"vi-vn\",\"currency\":\"VND\",\"user_id\":\"1\",\"user_token\":\"orXI7DhldxLhqVNcylUqCqDsHQKsMGWJ\"}', '2020-04-23 11:34:16'),
('afb6585f2b8f80c79c7e7955a9', '{\"language\":\"vi-vn\",\"currency\":\"VND\",\"user_id\":\"1\",\"user_token\":\"4MorvUZgW4sqJc4XOsZUONLnrREyUiUf\"}', '2020-04-27 07:47:20'),
('c9f86047dd9a86d534a4e56cd7', '{\"language\":\"vi-vn\",\"currency\":\"VND\",\"user_id\":\"1\",\"user_token\":\"RSwAxI2YGesoWIvjrUNiQwRiFSuzElzR\"}', '2020-04-18 04:32:39'),
('cb11a50834e4abc4a08abafd2a', '{\"language\":\"vi-vn\",\"currency\":\"USD\"}', '2020-04-16 05:23:56'),
('cb7b925cbbffed00b1bf425f27', '{\"language\":\"vi-vn\",\"currency\":\"VND\"}', '2020-04-24 06:29:28'),
('cd225f1215b6b93250e2c0a99e', '{\"language\":\"vi-vn\",\"currency\":\"VND\",\"user_id\":\"1\",\"user_token\":\"znjV2TYnFeSyPBD1trVuEnHk8Mv7612Z\",\"install\":\"MEBDuxPMM3\"}', '2020-04-23 08:24:05'),
('cd4232e2af5d908cb1e92c4363', '{\"language\":\"vi-vn\",\"currency\":\"VND\",\"user_id\":\"1\",\"user_token\":\"SYLaUNyEbABg43YxCehLkHqdMYNWb87m\",\"install\":\"cfCdwKI3IF\",\"theme_builder_config\":{\"fashion_shoezone\":{\"config_section_sections\":{\"index\":0,\"data\":[\"{\\n    \\\"name\\\": \\\"Theme Config\\\",\\n    \\\"version\\\": \\\"1.0\\\",\\n    \\\"section\\\": {\\n        \\\"header\\\": {\\n            \\\"name\\\": \\\"header\\\",\\n            \\\"text\\\": \\\"Header\\\",\\n            \\\"visible\\\": \\\"1\\\"\\n        },\\n        \\\"slide-show\\\": {\\n            \\\"name\\\": \\\"slide-show\\\",\\n            \\\"text\\\": \\\"Slideshow\\\",\\n            \\\"visible\\\": \\\"1\\\"\\n        },\\n        \\\"categories\\\": {\\n            \\\"name\\\": \\\"categories\\\",\\n            \\\"text\\\": \\\"Danh m\\u1ee5c s\\u1ea3n ph\\u1ea9m\\\",\\n            \\\"visible\\\": \\\"1\\\"\\n        },\\n        \\\"hot-deals\\\": {\\n            \\\"name\\\": \\\"hot-deals\\\",\\n            \\\"text\\\": \\\"S\\u1ea3n ph\\u1ea9m khuy\\u1ebfn m\\u1ea1i\\\",\\n            \\\"visible\\\": \\\"1\\\"\\n        },\\n        \\\"feature-products\\\": {\\n            \\\"name\\\": \\\"feature-products\\\",\\n            \\\"text\\\": \\\"S\\u1ea3n ph\\u1ea9m b\\u00e1n ch\\u1ea1y\\\",\\n            \\\"visible\\\": \\\"1\\\"\\n        },\\n        \\\"related-products\\\": {\\n            \\\"name\\\": \\\"related-products\\\",\\n            \\\"text\\\": \\\"S\\u1ea3n ph\\u1ea9m xem nhi\\u1ec1u\\\",\\n            \\\"visible\\\": \\\"1\\\"\\n        },\\n        \\\"new-products\\\": {\\n            \\\"name\\\": \\\"new-products\\\",\\n            \\\"text\\\": \\\"S\\u1ea3n ph\\u1ea9m m\\u1edbi\\\",\\n            \\\"visible\\\": \\\"1\\\"\\n        },\\n        \\\"product-detail\\\": {\\n            \\\"name\\\": \\\"product-details\\\",\\n            \\\"text\\\": \\\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\\\",\\n            \\\"visible\\\": \\\"1\\\"\\n        },\\n        \\\"banner\\\": {\\n            \\\"name\\\": \\\"banner\\\",\\n            \\\"text\\\": \\\"Banner\\\",\\n            \\\"visible\\\": \\\"1\\\"\\n        },\\n        \\\"partners\\\": {\\n            \\\"name\\\": \\\"partners\\\",\\n            \\\"text\\\": \\\"\\u0110\\u1ed1i t\\u00e1c\\\",\\n            \\\"visible\\\": \\\"1\\\"\\n        },\\n        \\\"blog\\\": {\\n            \\\"name\\\": \\\"blog\\\",\\n            \\\"text\\\": \\\"Blog\\\",\\n            \\\"visible\\\": \\\"1\\\"\\n        },\\n        \\\"footer\\\": {\\n            \\\"name\\\": \\\"footer\\\",\\n            \\\"text\\\": \\\"Footer\\\",\\n            \\\"visible\\\": \\\"1\\\"\\n        }\\n    }\\n}\"]},\"config_section_product_groups\":{\"index\":0,\"data\":[\"{\\n    \\\"name\\\": \\\"group products\\\",\\n    \\\"text\\\": \\\"nh\\u00f3m s\\u1ea3n ph\\u1ea9m\\\",\\n    \\\"visible\\\": \\\"1\\\",\\n    \\\"list\\\": [\\n        {\\n            \\\"text\\\": \\\"Danh s\\u00e1ch s\\u1ea3n ph\\u1ea9m 1\\\",\\n            \\\"visible\\\": \\\"0\\\",\\n            \\\"setting\\\": {\\n                \\\"title\\\": \\\"Danh s\\u00e1ch s\\u1ea3n ph\\u1ea9m 1\\\",\\n                \\\"collection_id\\\": \\\"4\\\",\\n                \\\"sub_title\\\": \\\"Danh s\\u00e1ch s\\u1ea3n ph\\u1ea9m 1\\\"\\n            },\\n            \\\"display\\\": {\\n                \\\"grid\\\": {\\n                    \\\"quantity\\\": \\\"6\\\"\\n                },\\n                \\\"grid_mobile\\\": {\\n                    \\\"quantity\\\": 2\\n                }\\n            }\\n        }\\n    ]\\n}\"]}}},\"preview_builder_config_version\":\"PnyQCOpSGpb8G3MSS4DueGjDBwWvatfy\"}', '2020-04-16 11:07:04'),
('ceaea3f5b4b05e6453e2ff9a42', '{\"language\":\"vi-vn\",\"currency\":\"VND\"}', '2020-04-23 09:26:07'),
('cec3ec5e6a246573ea8cf799ba', '{\"language\":\"vi-vn\",\"currency\":\"VND\"}', '2020-04-23 09:26:04'),
('df24ce5dbfbb2206ad142a0dfb', '{\"language\":\"vi-vn\",\"currency\":\"VND\"}', '2020-04-23 09:21:27'),
('e28546b1a24b7f604714a6423b', '{\"user_id\":\"1\",\"user_token\":\"lsw0ZKJSpzpaLwvNpewsIyLQvucTIVXt\",\"theme_builder_config\":{\"beauty_ap_cosmetic\":{\"config_section_sections\":{\"index\":0,\"data\":[\"{\\n    \\\"name\\\": \\\"Theme Config\\\",\\n    \\\"version\\\": \\\"1.0\\\",\\n    \\\"section\\\": {\\n        \\\"header\\\": {\\n            \\\"name\\\": \\\"header\\\",\\n            \\\"text\\\": \\\"Header\\\",\\n            \\\"visible\\\": 1\\n        },\\n        \\\"slide-show\\\": {\\n            \\\"name\\\": \\\"slide-show\\\",\\n            \\\"text\\\": \\\"Slideshow\\\",\\n            \\\"visible\\\": 1\\n        },\\n        \\\"categories\\\": {\\n            \\\"name\\\": \\\"categories\\\",\\n            \\\"text\\\": \\\"Danh m\\u1ee5c s\\u1ea3n ph\\u1ea9m\\\",\\n            \\\"visible\\\": 1\\n        },\\n        \\\"hot-deals\\\": {\\n            \\\"name\\\": \\\"hot-deals\\\",\\n            \\\"text\\\": \\\"S\\u1ea3n ph\\u1ea9m khuy\\u1ebfn m\\u1ea1i\\\",\\n            \\\"visible\\\": 1\\n        },\\n        \\\"feature-products\\\": {\\n            \\\"name\\\": \\\"feature-products\\\",\\n            \\\"text\\\": \\\"S\\u1ea3n ph\\u1ea9m b\\u00e1n ch\\u1ea1y\\\",\\n            \\\"visible\\\": 1\\n        },\\n        \\\"related-products\\\": {\\n            \\\"name\\\": \\\"related-products\\\",\\n            \\\"text\\\": \\\"S\\u1ea3n ph\\u1ea9m xem nhi\\u1ec1u\\\",\\n            \\\"visible\\\": 1\\n        },\\n        \\\"new-products\\\": {\\n            \\\"name\\\": \\\"new-products\\\",\\n            \\\"text\\\": \\\"S\\u1ea3n ph\\u1ea9m m\\u1edbi\\\",\\n            \\\"visible\\\": 1\\n        },\\n        \\\"product-detail\\\": {\\n            \\\"name\\\": \\\"product-details\\\",\\n            \\\"text\\\": \\\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\\\",\\n            \\\"visible\\\": 1\\n        },\\n        \\\"banner\\\": {\\n            \\\"name\\\": \\\"banner\\\",\\n            \\\"text\\\": \\\"Banner\\\",\\n            \\\"visible\\\": 1\\n        },\\n        \\\"content_customize\\\": {\\n            \\\"name\\\": \\\"content-customize\\\",\\n            \\\"text\\\": \\\"N\\u1ed9i dung t\\u00f9y ch\\u1ec9nh\\\",\\n            \\\"visible\\\": 1\\n        },\\n        \\\"partners\\\": {\\n            \\\"name\\\": \\\"partners\\\",\\n            \\\"text\\\": \\\"\\u0110\\u1ed1i t\\u00e1c\\\",\\n            \\\"visible\\\": 1\\n        },\\n        \\\"blog\\\": {\\n            \\\"name\\\": \\\"blog\\\",\\n            \\\"text\\\": \\\"Blog\\\",\\n            \\\"visible\\\": 1\\n        },\\n        \\\"footer\\\": {\\n            \\\"name\\\": \\\"footer\\\",\\n            \\\"text\\\": \\\"Footer\\\",\\n            \\\"visible\\\": 1\\n        }\\n    }\\n}\"]},\"config_section_product_groups\":{\"index\":0,\"data\":[\"{\\n    \\\"name\\\": \\\"group products\\\",\\n    \\\"text\\\": \\\"nh\\u00f3m s\\u1ea3n ph\\u1ea9m\\\",\\n    \\\"visible\\\": 1,\\n    \\\"list\\\": [\\n        {\\n            \\\"text\\\": \\\"Danh s\\u00e1ch s\\u1ea3n ph\\u1ea9m 1\\\",\\n            \\\"visible\\\": \\\"0\\\",\\n            \\\"setting\\\": {\\n                \\\"title\\\": \\\"Danh s\\u00e1ch s\\u1ea3n ph\\u1ea9m 1\\\",\\n                \\\"collection_id\\\": \\\"4\\\",\\n                \\\"sub_title\\\": \\\"Danh s\\u00e1ch s\\u1ea3n ph\\u1ea9m 1\\\"\\n            },\\n            \\\"display\\\": {\\n                \\\"grid\\\": {\\n                    \\\"quantity\\\": \\\"6\\\"\\n                },\\n                \\\"grid_mobile\\\": {\\n                    \\\"quantity\\\": 2\\n                }\\n            }\\n        }\\n    ]\\n}\"]}}},\"language\":\"vi-vn\",\"currency\":\"VND\",\"preview_builder_config_version\":\"smPv9cCeny1gRLhv08Ev5pHFWSQ3mCel\"}', '2020-04-17 04:48:15'),
('f17c7500d40dad55744ec4439c', '{\"language\":\"vi-vn\",\"currency\":\"VND\"}', '2020-04-22 07:52:26'),
('f3af86de3fc0252a223592b6fd', '{\"user_id\":\"1\",\"user_token\":\"BKWQbHKgWBhWcLxnNFEEmqzvO2J3duHh\",\"language\":\"vi-vn\",\"currency\":\"VND\"}', '2020-04-17 09:39:28'),
('f5c90297ba0b42a4c214405abf', '{\"language\":\"vi-vn\",\"currency\":\"VND\"}', '2020-04-23 11:23:16'),
('fd5b5c6961c51d44e1718e9d36', '{\"language\":\"vi-vn\",\"currency\":\"USD\"}', '2020-04-16 05:06:31');

-- --------------------------------------------------------

--
-- Table structure for table `oc_setting`
--

CREATE TABLE `oc_setting` (
  `setting_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(128) NOT NULL,
  `key` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_setting`
--

INSERT INTO `oc_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(1, 0, 'config', 'config_robots', 'abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshaihulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg', 0),
(2, 0, 'config', 'config_shared', '0', 0),
(3, 0, 'config', 'config_secure', '0', 0),
(4, 0, 'voucher', 'total_voucher_sort_order', '8', 0),
(5, 0, 'voucher', 'total_voucher_status', '1', 0),
(6, 0, 'config', 'config_fraud_detection', '0', 0),
(7, 0, 'config', 'config_ftp_status', '0', 0),
(8, 0, 'config', 'config_ftp_root', '', 0),
(9, 0, 'config', 'config_ftp_password', '', 0),
(10, 0, 'config', 'config_ftp_username', '', 0),
(11, 0, 'config', 'config_ftp_port', '21', 0),
(12, 0, 'config', 'config_ftp_hostname', '', 0),
(13, 0, 'config', 'config_meta_title', 'Your Store', 0),
(14, 0, 'config', 'config_meta_description', 'My Store', 0),
(15, 0, 'config', 'config_meta_keyword', '', 0),
(6695, 0, 'config', 'config_theme', 'beauty_ap_cosmetic', 0),
(17, 0, 'config', 'config_layout_id', '4', 0),
(18, 0, 'config', 'config_country_id', '222', 0),
(19, 0, 'config', 'config_zone_id', '3563', 0),
(6702, 0, 'config', 'config_language', 'en-gb', 0),
(6703, 0, 'config', 'config_admin_language', 'en-gb', 0),
(22, 0, 'config', 'config_currency', 'VND', 0),
(23, 0, 'config', 'config_currency_auto', '1', 0),
(24, 0, 'config', 'config_length_class_id', '1', 0),
(25, 0, 'config', 'config_weight_class_id', '1', 0),
(26, 0, 'config', 'config_product_count', '1', 0),
(27, 0, 'config', 'config_limit_admin', '25', 0),
(28, 0, 'config', 'config_review_status', '1', 0),
(29, 0, 'config', 'config_review_guest', '1', 0),
(30, 0, 'config', 'config_voucher_min', '1', 0),
(31, 0, 'config', 'config_voucher_max', '1000', 0),
(32, 0, 'config', 'config_tax', '1', 0),
(33, 0, 'config', 'config_tax_default', 'shipping', 0),
(34, 0, 'config', 'config_tax_customer', 'shipping', 0),
(35, 0, 'config', 'config_customer_online', '0', 0),
(36, 0, 'config', 'config_customer_activity', '0', 0),
(37, 0, 'config', 'config_customer_search', '0', 0),
(38, 0, 'config', 'config_customer_group_id', '1', 0),
(39, 0, 'config', 'config_customer_group_display', '[\"1\"]', 1),
(40, 0, 'config', 'config_customer_price', '0', 0),
(41, 0, 'config', 'config_account_id', '3', 0),
(42, 0, 'config', 'config_invoice_prefix', 'INV-2013-00', 0),
(43, 0, 'config', 'config_api_id', '1', 0),
(44, 0, 'config', 'config_cart_weight', '1', 0),
(45, 0, 'config', 'config_checkout_guest', '1', 0),
(46, 0, 'config', 'config_checkout_id', '5', 0),
(47, 0, 'config', 'config_order_status_id', '1', 0),
(48, 0, 'config', 'config_processing_status', '[\"5\",\"1\",\"2\",\"12\",\"3\"]', 1),
(49, 0, 'config', 'config_complete_status', '[\"5\",\"3\"]', 1),
(50, 0, 'config', 'config_stock_display', '0', 0),
(51, 0, 'config', 'config_stock_warning', '0', 0),
(52, 0, 'config', 'config_stock_checkout', '0', 0),
(53, 0, 'config', 'config_affiliate_approval', '0', 0),
(54, 0, 'config', 'config_affiliate_auto', '0', 0),
(55, 0, 'config', 'config_affiliate_commission', '5', 0),
(56, 0, 'config', 'config_affiliate_id', '4', 0),
(57, 0, 'config', 'config_return_id', '0', 0),
(58, 0, 'config', 'config_return_status_id', '2', 0),
(59, 0, 'config', 'config_logo', 'catalog/logo.png', 0),
(60, 0, 'config', 'config_icon', 'catalog/cart.png', 0),
(61, 0, 'config', 'config_comment', '', 0),
(62, 0, 'config', 'config_open', '', 0),
(63, 0, 'config', 'config_image', '', 0),
(64, 0, 'config', 'config_fax', '', 0),
(65, 0, 'config', 'config_telephone', '', 0),
(66, 0, 'config', 'config_email', 'demo@opencart.com', 0),
(67, 0, 'config', 'config_geocode', '', 0),
(68, 0, 'config', 'config_owner', 'Your Name', 0),
(69, 0, 'config', 'config_address', '', 0),
(70, 0, 'config', 'config_name', '', 0),
(71, 0, 'config', 'config_seo_url', '1', 0),
(72, 0, 'config', 'config_file_max_size', '300000', 0),
(73, 0, 'config', 'config_file_ext_allowed', 'zip\r\ntxt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\ndoc', 0),
(74, 0, 'config', 'config_file_mime_allowed', 'text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/gif\r\nimage/bmp\r\nimage/tiff\r\nimage/svg+xml\r\napplication/zip\r\n&quot;application/zip&quot;\r\napplication/x-zip\r\n&quot;application/x-zip&quot;\r\napplication/x-zip-compressed\r\n&quot;application/x-zip-compressed&quot;\r\napplication/rar\r\n&quot;application/rar&quot;\r\napplication/x-rar\r\n&quot;application/x-rar&quot;\r\napplication/x-rar-compressed\r\n&quot;application/x-rar-compressed&quot;\r\napplication/octet-stream\r\n&quot;application/octet-stream&quot;\r\naudio/mpeg\r\nvideo/quicktime\r\napplication/pdf', 0),
(75, 0, 'config', 'config_maintenance', '0', 0),
(76, 0, 'config', 'config_password', '1', 0),
(77, 0, 'config', 'config_encryption', '', 0),
(78, 0, 'config', 'config_compression', '0', 0),
(79, 0, 'config', 'config_error_display', '1', 0),
(80, 0, 'config', 'config_error_log', '1', 0),
(81, 0, 'config', 'config_error_filename', 'error.log', 0),
(82, 0, 'config', 'config_google_analytics', '', 0),
(83, 0, 'config', 'config_mail_engine', 'smtp', 0),
(84, 0, 'config', 'config_mail_parameter', '', 0),
(85, 0, 'config', 'config_mail_smtp_hostname', 'tls://smtp.gmail.com', 0),
(86, 0, 'config', 'config_mail_smtp_username', 'hotro@bestme.asia', 0),
(87, 0, 'config', 'config_mail_smtp_password', 'NOVAONshop@1234', 0),
(88, 0, 'config', 'config_mail_smtp_port', '587', 0),
(89, 0, 'config', 'config_mail_smtp_timeout', '15', 0),
(90, 0, 'config', 'config_mail_alert_email', '', 0),
(91, 0, 'config', 'config_mail_alert', '[\"order\"]', 1),
(92, 0, 'config', 'config_captcha', 'basic', 0),
(93, 0, 'config', 'config_captcha_page', '[\"review\",\"return\",\"contact\"]', 1),
(94, 0, 'config', 'config_login_attempts', '5', 0),
(95, 0, 'payment_free_checkout', 'payment_free_checkout_status', '1', 0),
(96, 0, 'payment_free_checkout', 'free_checkout_order_status_id', '1', 0),
(97, 0, 'payment_free_checkout', 'payment_free_checkout_sort_order', '1', 0),
(98, 0, 'payment_cod', 'payment_cod_sort_order', '5', 0),
(99, 0, 'payment_cod', 'payment_cod_total', '0.01', 0),
(100, 0, 'payment_cod', 'payment_cod_order_status_id', '1', 0),
(101, 0, 'payment_cod', 'payment_cod_geo_zone_id', '0', 0),
(102, 0, 'payment_cod', 'payment_cod_status', '1', 0),
(103, 0, 'shipping_flat', 'shipping_flat_sort_order', '1', 0),
(104, 0, 'shipping_flat', 'shipping_flat_status', '1', 0),
(105, 0, 'shipping_flat', 'shipping_flat_geo_zone_id', '0', 0),
(106, 0, 'shipping_flat', 'shipping_flat_tax_class_id', '9', 0),
(107, 0, 'shipping_flat', 'shipping_flat_cost', '5.00', 0),
(108, 0, 'total_shipping', 'total_shipping_sort_order', '3', 0),
(109, 0, 'total_sub_total', 'sub_total_sort_order', '1', 0),
(110, 0, 'total_sub_total', 'total_sub_total_status', '1', 0),
(111, 0, 'total_tax', 'total_tax_status', '1', 0),
(112, 0, 'total_total', 'total_total_sort_order', '9', 0),
(113, 0, 'total_total', 'total_total_status', '1', 0),
(114, 0, 'total_tax', 'total_tax_sort_order', '5', 0),
(115, 0, 'total_credit', 'total_credit_sort_order', '7', 0),
(116, 0, 'total_credit', 'total_credit_status', '1', 0),
(117, 0, 'total_reward', 'total_reward_sort_order', '2', 0),
(118, 0, 'total_reward', 'total_reward_status', '1', 0),
(119, 0, 'total_shipping', 'total_shipping_status', '1', 0),
(120, 0, 'total_shipping', 'total_shipping_estimator', '1', 0),
(121, 0, 'total_coupon', 'total_coupon_sort_order', '4', 0),
(122, 0, 'total_coupon', 'total_coupon_status', '1', 0),
(123, 0, 'module_category', 'module_category_status', '1', 0),
(124, 0, 'module_account', 'module_account_status', '1', 0),
(125, 0, 'theme_default', 'theme_default_product_limit', '15', 0),
(126, 0, 'theme_default', 'theme_default_product_description_length', '100', 0),
(127, 0, 'theme_default', 'theme_default_image_thumb_width', '228', 0),
(128, 0, 'theme_default', 'theme_default_image_thumb_height', '228', 0),
(129, 0, 'theme_default', 'theme_default_image_popup_width', '500', 0),
(130, 0, 'theme_default', 'theme_default_image_popup_height', '500', 0),
(131, 0, 'theme_default', 'theme_default_image_category_width', '80', 0),
(132, 0, 'theme_default', 'theme_default_image_category_height', '80', 0),
(133, 0, 'theme_default', 'theme_default_image_product_width', '228', 0),
(134, 0, 'theme_default', 'theme_default_image_product_height', '228', 0),
(135, 0, 'theme_default', 'theme_default_image_additional_width', '74', 0),
(136, 0, 'theme_default', 'theme_default_image_additional_height', '74', 0),
(137, 0, 'theme_default', 'theme_default_image_related_width', '200', 0),
(138, 0, 'theme_default', 'theme_default_image_related_height', '200', 0),
(139, 0, 'theme_default', 'theme_default_image_compare_width', '90', 0),
(140, 0, 'theme_default', 'theme_default_image_compare_height', '90', 0),
(141, 0, 'theme_default', 'theme_default_image_wishlist_width', '47', 0),
(142, 0, 'theme_default', 'theme_default_image_wishlist_height', '47', 0),
(143, 0, 'theme_default', 'theme_default_image_cart_height', '47', 0),
(144, 0, 'theme_default', 'theme_default_image_cart_width', '47', 0),
(145, 0, 'theme_default', 'theme_default_image_location_height', '50', 0),
(146, 0, 'theme_default', 'theme_default_image_location_width', '268', 0),
(147, 0, 'theme_default', 'theme_default_directory', 'default', 0),
(148, 0, 'theme_default', 'theme_default_status', '1', 0),
(149, 0, 'dashboard_activity', 'dashboard_activity_status', '1', 0),
(150, 0, 'dashboard_activity', 'dashboard_activity_sort_order', '7', 0),
(151, 0, 'dashboard_sale', 'dashboard_sale_status', '1', 0),
(152, 0, 'dashboard_sale', 'dashboard_sale_width', '3', 0),
(153, 0, 'dashboard_chart', 'dashboard_chart_status', '1', 0),
(154, 0, 'dashboard_chart', 'dashboard_chart_width', '6', 0),
(155, 0, 'dashboard_customer', 'dashboard_customer_status', '1', 0),
(156, 0, 'dashboard_customer', 'dashboard_customer_width', '3', 0),
(157, 0, 'dashboard_map', 'dashboard_map_status', '1', 0),
(158, 0, 'dashboard_map', 'dashboard_map_width', '6', 0),
(159, 0, 'dashboard_online', 'dashboard_online_status', '1', 0),
(160, 0, 'dashboard_online', 'dashboard_online_width', '3', 0),
(161, 0, 'dashboard_order', 'dashboard_order_sort_order', '1', 0),
(162, 0, 'dashboard_order', 'dashboard_order_status', '1', 0),
(163, 0, 'dashboard_order', 'dashboard_order_width', '3', 0),
(164, 0, 'dashboard_sale', 'dashboard_sale_sort_order', '2', 0),
(165, 0, 'dashboard_customer', 'dashboard_customer_sort_order', '3', 0),
(166, 0, 'dashboard_online', 'dashboard_online_sort_order', '4', 0),
(167, 0, 'dashboard_map', 'dashboard_map_sort_order', '5', 0),
(168, 0, 'dashboard_chart', 'dashboard_chart_sort_order', '6', 0),
(169, 0, 'dashboard_recent', 'dashboard_recent_status', '1', 0),
(170, 0, 'dashboard_recent', 'dashboard_recent_sort_order', '8', 0),
(171, 0, 'dashboard_activity', 'dashboard_activity_width', '4', 0),
(172, 0, 'dashboard_recent', 'dashboard_recent_width', '8', 0),
(173, 0, 'report_customer_activity', 'report_customer_activity_status', '1', 0),
(174, 0, 'report_customer_activity', 'report_customer_activity_sort_order', '1', 0),
(175, 0, 'report_customer_order', 'report_customer_order_status', '1', 0),
(176, 0, 'report_customer_order', 'report_customer_order_sort_order', '2', 0),
(177, 0, 'report_customer_reward', 'report_customer_reward_status', '1', 0),
(178, 0, 'report_customer_reward', 'report_customer_reward_sort_order', '3', 0),
(179, 0, 'report_customer_search', 'report_customer_search_sort_order', '3', 0),
(180, 0, 'report_customer_search', 'report_customer_search_status', '1', 0),
(181, 0, 'report_customer_transaction', 'report_customer_transaction_status', '1', 0),
(182, 0, 'report_customer_transaction', 'report_customer_transaction_status_sort_order', '4', 0),
(183, 0, 'report_sale_tax', 'report_sale_tax_status', '1', 0),
(184, 0, 'report_sale_tax', 'report_sale_tax_sort_order', '5', 0),
(185, 0, 'report_sale_shipping', 'report_sale_shipping_status', '1', 0),
(186, 0, 'report_sale_shipping', 'report_sale_shipping_sort_order', '6', 0),
(187, 0, 'report_sale_return', 'report_sale_return_status', '1', 0),
(188, 0, 'report_sale_return', 'report_sale_return_sort_order', '7', 0),
(189, 0, 'report_sale_order', 'report_sale_order_status', '1', 0),
(190, 0, 'report_sale_order', 'report_sale_order_sort_order', '8', 0),
(191, 0, 'report_sale_coupon', 'report_sale_coupon_status', '1', 0),
(192, 0, 'report_sale_coupon', 'report_sale_coupon_sort_order', '9', 0),
(193, 0, 'report_product_viewed', 'report_product_viewed_status', '1', 0),
(194, 0, 'report_product_viewed', 'report_product_viewed_sort_order', '10', 0),
(195, 0, 'report_product_purchased', 'report_product_purchased_status', '1', 0),
(196, 0, 'report_product_purchased', 'report_product_purchased_sort_order', '11', 0),
(197, 0, 'report_marketing', 'report_marketing_status', '1', 0),
(198, 0, 'report_marketing', 'report_marketing_sort_order', '12', 0),
(4968, 0, 'developer', 'developer_sass', '0', 0),
(201, 0, 'module_theme_builder_config', 'module_theme_builder_config_status', '1', 0),
(4700, 0, 'theme_novaon', 'theme_novaon_image_wishlist_height', '47', 0),
(4701, 0, 'theme_novaon', 'theme_novaon_image_wishlist_width', '47', 0),
(4702, 0, 'theme_novaon', 'theme_novaon_image_compare_height', '90', 0),
(4703, 0, 'theme_novaon', 'theme_novaon_image_compare_width', '90', 0),
(4704, 0, 'theme_novaon', 'theme_novaon_image_related_height', '80', 0),
(4705, 0, 'theme_novaon', 'theme_novaon_image_related_width', '80', 0),
(4706, 0, 'theme_novaon', 'theme_novaon_image_additional_height', '74', 0),
(4707, 0, 'theme_novaon', 'theme_novaon_image_additional_width', '74', 0),
(4708, 0, 'theme_novaon', 'theme_novaon_image_product_height', '228', 0),
(4709, 0, 'theme_novaon', 'theme_novaon_image_product_width', '228', 0),
(4710, 0, 'theme_novaon', 'theme_novaon_image_popup_height', '500', 0),
(4711, 0, 'theme_novaon', 'theme_novaon_image_popup_width', '500', 0),
(4712, 0, 'theme_novaon', 'theme_novaon_image_thumb_height', '228', 0),
(4713, 0, 'theme_novaon', 'theme_novaon_image_thumb_width', '228', 0),
(4714, 0, 'theme_novaon', 'theme_novaon_image_category_height', '80', 0),
(4715, 0, 'theme_novaon', 'theme_novaon_image_category_width', '80', 0),
(4716, 0, 'theme_novaon', 'theme_novaon_product_description_length', '100', 0),
(4717, 0, 'theme_novaon', 'theme_novaon_product_limit', '15', 0),
(4718, 0, 'theme_novaon', 'theme_novaon_status', '1', 0),
(4719, 0, 'theme_novaon', 'theme_novaon_directory', 'novaon', 0),
(4720, 0, 'theme_novaon', 'theme_novaon_image_cart_width', '47', 0),
(4721, 0, 'theme_novaon', 'theme_novaon_image_cart_height', '47', 0),
(4722, 0, 'theme_novaon', 'theme_novaon_image_location_width', '268', 0),
(4723, 0, 'theme_novaon', 'theme_novaon_image_location_height', '50', 0),
(4892, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_image_wishlist_height', '47', 0),
(4893, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_image_wishlist_width', '47', 0),
(4894, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_image_compare_height', '90', 0),
(4895, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_image_compare_width', '90', 0),
(4896, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_image_related_height', '80', 0),
(4897, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_image_related_width', '80', 0),
(4898, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_image_additional_height', '74', 0),
(4899, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_image_additional_width', '74', 0),
(4900, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_image_product_height', '228', 0),
(4901, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_image_product_width', '228', 0),
(4902, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_image_popup_height', '500', 0),
(4903, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_image_popup_width', '500', 0),
(4904, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_image_thumb_height', '228', 0),
(4905, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_image_thumb_width', '228', 0),
(4906, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_image_category_height', '80', 0),
(4907, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_image_category_width', '80', 0),
(4908, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_product_description_length', '100', 0),
(4909, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_product_limit', '15', 0),
(4910, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_status', '1', 0),
(4911, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_directory', 'fashion_shoezone', 0),
(4912, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_image_cart_width', '47', 0),
(4913, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_image_cart_height', '47', 0),
(4914, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_image_location_width', '268', 0),
(4915, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_image_location_height', '50', 0),
(4967, 0, 'developer', 'developer_theme', '0', 0),
(5516, 0, 'config', 'config_gg_shopping', '', 0),
(5517, 0, 'payment', 'payment_methods', '[{\"payment_id\":\"198535319739A\",\"name\":\"Thanh toán khi nhận hàng (COD)\",\"guide\":\"Bạn sẽ thanh toán toàn bộ giá trị đơn hàng khi nhận hàng\",\"status\":\"1\"}]', 1),
(5518, 0, 'delivery', 'delivery_methods', '[{\"id\":\"-1\",\"name\":\"Giao hàng tiêu chuẩn\",\"status\":\"1\",\"fee_region\":\"40000\"}]', 1),
(5519, 0, 'config', 'config_VIETTEL_POST_token_webhook', '', 0),
(5520, 0, 'config', 'config_onboarding_complete', '0', 0),
(6692, 0, 'config', 'config_onboarding_step_active', '2', 0),
(6687, 0, 'config', 'config_onboarding_voucher', '16391821', 0),
(5523, 0, 'config', 'config_onboarding_used_voucher', '0', 0),
(5524, 0, 'config', 'builder_config_version', '3', 0),
(5525, 0, 'config', 'config_order_success_text', 'MUA HÀNG THÀNH CÔNG!', 0),
(5526, 0, 'config', 'config_register_success_text', '<p>Chúc mừng! Tài khoản mới của bạn đã được tạo thành công!</p>\n\n<p>Bây giờ bạn có thể tận hưởng lợi ích thành viên khi tham gia trải nghiệm mua sắm cùng chúng tôi.</p>\n\n<p>Nếu bạn có câu hỏi gì về hoạt động của cửa hàng, vui lòng liên hệ với chủ cửa hàng.</p>\n\n<p>Một email xác nhận đã được gửi tới hòm thư của bạn. Nếu bạn không nhận được email này trong vòng 1 giờ, vui lòng liên hệ chúng tôi.</p>', 0),
(5527, 0, 'config', 'config_name', 'yourshop', 0),
(5528, 0, 'config', 'shop_name', 'yourshop', 0),
(5529, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_image_wishlist_height', '47', 0),
(5530, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_image_wishlist_width', '47', 0),
(5531, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_image_compare_height', '90', 0),
(5532, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_image_compare_width', '90', 0),
(5533, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_image_related_height', '80', 0),
(5534, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_image_related_width', '80', 0),
(5535, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_image_additional_height', '74', 0),
(5536, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_image_additional_width', '74', 0),
(5537, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_image_product_height', '228', 0),
(5538, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_image_product_width', '228', 0),
(5539, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_image_popup_height', '500', 0),
(5540, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_image_popup_width', '500', 0),
(5541, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_image_thumb_height', '228', 0),
(5542, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_image_thumb_width', '228', 0),
(5543, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_image_category_height', '80', 0),
(5544, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_image_category_width', '80', 0),
(5545, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_product_description_length', '100', 0),
(5546, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_product_limit', '15', 0),
(5547, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_status', '1', 0),
(5548, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_directory', 'beauty_xoiza', 0),
(5549, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_image_cart_width', '47', 0),
(5550, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_image_cart_height', '47', 0),
(5551, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_image_location_width', '268', 0),
(5552, 0, 'theme_beauty_xoiza', 'theme_beauty_xoiza_image_location_height', '50', 0),
(5553, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_image_wishlist_height', '47', 0),
(5554, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_image_wishlist_width', '47', 0),
(5555, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_image_compare_height', '90', 0),
(5556, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_image_compare_width', '90', 0),
(5557, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_image_related_height', '80', 0),
(5558, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_image_related_width', '80', 0),
(5559, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_image_additional_height', '74', 0),
(5560, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_image_additional_width', '74', 0),
(5561, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_image_product_height', '228', 0),
(5562, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_image_product_width', '228', 0),
(5563, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_image_popup_height', '500', 0),
(5564, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_image_popup_width', '500', 0),
(5565, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_image_thumb_height', '228', 0),
(5566, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_image_thumb_width', '228', 0),
(5567, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_image_category_height', '80', 0),
(5568, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_image_category_width', '80', 0),
(5569, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_product_description_length', '100', 0),
(5570, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_product_limit', '15', 0),
(5571, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_status', '1', 0),
(5572, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_directory', 'beauty_xoiza_2', 0),
(5573, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_image_cart_width', '47', 0),
(5574, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_image_cart_height', '47', 0),
(5575, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_image_location_width', '268', 0),
(5576, 0, 'theme_beauty_xoiza_2', 'theme_beauty_xoiza_2_image_location_height', '50', 0),
(5577, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_image_wishlist_height', '47', 0),
(5578, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_image_wishlist_width', '47', 0),
(5579, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_image_compare_height', '90', 0),
(5580, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_image_compare_width', '90', 0),
(5581, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_image_related_height', '80', 0),
(5582, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_image_related_width', '80', 0),
(5583, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_image_additional_height', '74', 0),
(5584, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_image_additional_width', '74', 0),
(5585, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_image_product_height', '228', 0),
(5586, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_image_product_width', '228', 0),
(5587, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_image_popup_height', '500', 0),
(5588, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_image_popup_width', '500', 0),
(5589, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_image_thumb_height', '228', 0),
(5590, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_image_thumb_width', '228', 0),
(5591, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_image_category_height', '80', 0),
(5592, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_image_category_width', '80', 0),
(5593, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_product_description_length', '100', 0),
(5594, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_product_limit', '15', 0),
(5595, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_status', '1', 0),
(5596, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_directory', 'fashion_luzun', 0),
(5597, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_image_cart_width', '47', 0),
(5598, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_image_cart_height', '47', 0),
(5599, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_image_location_width', '268', 0),
(5600, 0, 'theme_fashion_luzun', 'theme_fashion_luzun_image_location_height', '50', 0),
(5601, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_image_wishlist_height', '47', 0),
(5602, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_image_wishlist_width', '47', 0),
(5603, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_image_compare_height', '90', 0),
(5604, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_image_compare_width', '90', 0),
(5605, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_image_related_height', '80', 0),
(5606, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_image_related_width', '80', 0),
(5607, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_image_additional_height', '74', 0),
(5608, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_image_additional_width', '74', 0),
(5609, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_image_product_height', '228', 0),
(5610, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_image_product_width', '228', 0),
(5611, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_image_popup_height', '500', 0),
(5612, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_image_popup_width', '500', 0),
(5613, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_image_thumb_height', '228', 0),
(5614, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_image_thumb_width', '228', 0),
(5615, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_image_category_height', '80', 0),
(5616, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_image_category_width', '80', 0),
(5617, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_product_description_length', '100', 0),
(5618, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_product_limit', '15', 0),
(5619, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_status', '1', 0),
(5620, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_directory', 'fashion_monstar', 0),
(5621, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_image_cart_width', '47', 0),
(5622, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_image_cart_height', '47', 0),
(5623, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_image_location_width', '268', 0),
(5624, 0, 'theme_fashion_monstar', 'theme_fashion_monstar_image_location_height', '50', 0),
(5625, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_image_wishlist_height', '47', 0),
(5626, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_image_wishlist_width', '47', 0),
(5627, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_image_compare_height', '90', 0),
(5628, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_image_compare_width', '90', 0),
(5629, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_image_related_height', '80', 0),
(5630, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_image_related_width', '80', 0),
(5631, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_image_additional_height', '74', 0),
(5632, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_image_additional_width', '74', 0),
(5633, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_image_product_height', '228', 0),
(5634, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_image_product_width', '228', 0),
(5635, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_image_popup_height', '500', 0),
(5636, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_image_popup_width', '500', 0),
(5637, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_image_thumb_height', '228', 0),
(5638, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_image_thumb_width', '228', 0),
(5639, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_image_category_height', '80', 0),
(5640, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_image_category_width', '80', 0),
(5641, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_product_description_length', '100', 0),
(5642, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_product_limit', '15', 0),
(5643, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_status', '1', 0),
(5644, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_directory', 'fashion_xoiza', 0),
(5645, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_image_cart_width', '47', 0),
(5646, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_image_cart_height', '47', 0),
(5647, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_image_location_width', '268', 0),
(5648, 0, 'theme_fashion_xoiza', 'theme_fashion_xoiza_image_location_height', '50', 0),
(5649, 0, 'theme_fashion_zork', 'theme_fashion_zork_image_wishlist_height', '47', 0),
(5650, 0, 'theme_fashion_zork', 'theme_fashion_zork_image_wishlist_width', '47', 0),
(5651, 0, 'theme_fashion_zork', 'theme_fashion_zork_image_compare_height', '90', 0),
(5652, 0, 'theme_fashion_zork', 'theme_fashion_zork_image_compare_width', '90', 0),
(5653, 0, 'theme_fashion_zork', 'theme_fashion_zork_image_related_height', '80', 0),
(5654, 0, 'theme_fashion_zork', 'theme_fashion_zork_image_related_width', '80', 0),
(5655, 0, 'theme_fashion_zork', 'theme_fashion_zork_image_additional_height', '74', 0),
(5656, 0, 'theme_fashion_zork', 'theme_fashion_zork_image_additional_width', '74', 0),
(5657, 0, 'theme_fashion_zork', 'theme_fashion_zork_image_product_height', '228', 0),
(5658, 0, 'theme_fashion_zork', 'theme_fashion_zork_image_product_width', '228', 0),
(5659, 0, 'theme_fashion_zork', 'theme_fashion_zork_image_popup_height', '500', 0),
(5660, 0, 'theme_fashion_zork', 'theme_fashion_zork_image_popup_width', '500', 0),
(5661, 0, 'theme_fashion_zork', 'theme_fashion_zork_image_thumb_height', '228', 0),
(5662, 0, 'theme_fashion_zork', 'theme_fashion_zork_image_thumb_width', '228', 0),
(5663, 0, 'theme_fashion_zork', 'theme_fashion_zork_image_category_height', '80', 0),
(5664, 0, 'theme_fashion_zork', 'theme_fashion_zork_image_category_width', '80', 0),
(5665, 0, 'theme_fashion_zork', 'theme_fashion_zork_product_description_length', '100', 0),
(5666, 0, 'theme_fashion_zork', 'theme_fashion_zork_product_limit', '15', 0),
(5667, 0, 'theme_fashion_zork', 'theme_fashion_zork_status', '1', 0),
(5668, 0, 'theme_fashion_zork', 'theme_fashion_zork_directory', 'fashion_zork', 0),
(5669, 0, 'theme_fashion_zork', 'theme_fashion_zork_image_cart_width', '47', 0),
(5670, 0, 'theme_fashion_zork', 'theme_fashion_zork_image_cart_height', '47', 0),
(5671, 0, 'theme_fashion_zork', 'theme_fashion_zork_image_location_width', '268', 0),
(5672, 0, 'theme_fashion_zork', 'theme_fashion_zork_image_location_height', '50', 0),
(5673, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_image_wishlist_height', '47', 0),
(5674, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_image_wishlist_width', '47', 0),
(5675, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_image_compare_height', '90', 0),
(5676, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_image_compare_width', '90', 0),
(5677, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_image_related_height', '80', 0),
(5678, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_image_related_width', '80', 0),
(5679, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_image_additional_height', '74', 0),
(5680, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_image_additional_width', '74', 0),
(5681, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_image_product_height', '228', 0),
(5682, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_image_product_width', '228', 0),
(5683, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_image_popup_height', '500', 0),
(5684, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_image_popup_width', '500', 0),
(5685, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_image_thumb_height', '228', 0),
(5686, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_image_thumb_width', '228', 0),
(5687, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_image_category_height', '80', 0),
(5688, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_image_category_width', '80', 0),
(5689, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_product_description_length', '100', 0),
(5690, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_product_limit', '15', 0),
(5691, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_status', '1', 0),
(5692, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_directory', 'tech_allegorithmic', 0),
(5693, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_image_cart_width', '47', 0),
(5694, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_image_cart_height', '47', 0),
(5695, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_image_location_width', '268', 0),
(5696, 0, 'theme_tech_allegorithmic', 'theme_tech_allegorithmic_image_location_height', '50', 0),
(5697, 0, 'theme_tech_digitech', 'theme_tech_digitech_image_wishlist_height', '47', 0),
(5698, 0, 'theme_tech_digitech', 'theme_tech_digitech_image_wishlist_width', '47', 0),
(5699, 0, 'theme_tech_digitech', 'theme_tech_digitech_image_compare_height', '90', 0),
(5700, 0, 'theme_tech_digitech', 'theme_tech_digitech_image_compare_width', '90', 0),
(5701, 0, 'theme_tech_digitech', 'theme_tech_digitech_image_related_height', '80', 0),
(5702, 0, 'theme_tech_digitech', 'theme_tech_digitech_image_related_width', '80', 0),
(5703, 0, 'theme_tech_digitech', 'theme_tech_digitech_image_additional_height', '74', 0),
(5704, 0, 'theme_tech_digitech', 'theme_tech_digitech_image_additional_width', '74', 0),
(5705, 0, 'theme_tech_digitech', 'theme_tech_digitech_image_product_height', '228', 0),
(5706, 0, 'theme_tech_digitech', 'theme_tech_digitech_image_product_width', '228', 0),
(5707, 0, 'theme_tech_digitech', 'theme_tech_digitech_image_popup_height', '500', 0),
(5708, 0, 'theme_tech_digitech', 'theme_tech_digitech_image_popup_width', '500', 0),
(5709, 0, 'theme_tech_digitech', 'theme_tech_digitech_image_thumb_height', '228', 0),
(5710, 0, 'theme_tech_digitech', 'theme_tech_digitech_image_thumb_width', '228', 0),
(5711, 0, 'theme_tech_digitech', 'theme_tech_digitech_image_category_height', '80', 0),
(5712, 0, 'theme_tech_digitech', 'theme_tech_digitech_image_category_width', '80', 0),
(5713, 0, 'theme_tech_digitech', 'theme_tech_digitech_product_description_length', '100', 0),
(5714, 0, 'theme_tech_digitech', 'theme_tech_digitech_product_limit', '15', 0),
(5715, 0, 'theme_tech_digitech', 'theme_tech_digitech_status', '1', 0),
(5716, 0, 'theme_tech_digitech', 'theme_tech_digitech_directory', 'tech_digitech', 0),
(5717, 0, 'theme_tech_digitech', 'theme_tech_digitech_image_cart_width', '47', 0),
(5718, 0, 'theme_tech_digitech', 'theme_tech_digitech_image_cart_height', '47', 0),
(5719, 0, 'theme_tech_digitech', 'theme_tech_digitech_image_location_width', '268', 0),
(5720, 0, 'theme_tech_digitech', 'theme_tech_digitech_image_location_height', '50', 0),
(5721, 0, 'theme_tech_marte', 'theme_tech_marte_image_wishlist_height', '47', 0),
(5722, 0, 'theme_tech_marte', 'theme_tech_marte_image_wishlist_width', '47', 0),
(5723, 0, 'theme_tech_marte', 'theme_tech_marte_image_compare_height', '90', 0),
(5724, 0, 'theme_tech_marte', 'theme_tech_marte_image_compare_width', '90', 0),
(5725, 0, 'theme_tech_marte', 'theme_tech_marte_image_related_height', '80', 0),
(5726, 0, 'theme_tech_marte', 'theme_tech_marte_image_related_width', '80', 0),
(5727, 0, 'theme_tech_marte', 'theme_tech_marte_image_additional_height', '74', 0),
(5728, 0, 'theme_tech_marte', 'theme_tech_marte_image_additional_width', '74', 0),
(5729, 0, 'theme_tech_marte', 'theme_tech_marte_image_product_height', '228', 0),
(5730, 0, 'theme_tech_marte', 'theme_tech_marte_image_product_width', '228', 0),
(5731, 0, 'theme_tech_marte', 'theme_tech_marte_image_popup_height', '500', 0),
(5732, 0, 'theme_tech_marte', 'theme_tech_marte_image_popup_width', '500', 0),
(5733, 0, 'theme_tech_marte', 'theme_tech_marte_image_thumb_height', '228', 0),
(5734, 0, 'theme_tech_marte', 'theme_tech_marte_image_thumb_width', '228', 0),
(5735, 0, 'theme_tech_marte', 'theme_tech_marte_image_category_height', '80', 0),
(5736, 0, 'theme_tech_marte', 'theme_tech_marte_image_category_width', '80', 0),
(5737, 0, 'theme_tech_marte', 'theme_tech_marte_product_description_length', '100', 0),
(5738, 0, 'theme_tech_marte', 'theme_tech_marte_product_limit', '15', 0),
(5739, 0, 'theme_tech_marte', 'theme_tech_marte_status', '1', 0),
(5740, 0, 'theme_tech_marte', 'theme_tech_marte_directory', 'tech_marte', 0),
(5741, 0, 'theme_tech_marte', 'theme_tech_marte_image_cart_width', '47', 0),
(5742, 0, 'theme_tech_marte', 'theme_tech_marte_image_cart_height', '47', 0),
(5743, 0, 'theme_tech_marte', 'theme_tech_marte_image_location_width', '268', 0),
(5744, 0, 'theme_tech_marte', 'theme_tech_marte_image_location_height', '50', 0),
(5745, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_image_wishlist_height', '47', 0),
(5746, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_image_wishlist_width', '47', 0),
(5747, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_image_compare_height', '90', 0),
(5748, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_image_compare_width', '90', 0),
(5749, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_image_related_height', '80', 0),
(5750, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_image_related_width', '80', 0),
(5751, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_image_additional_height', '74', 0),
(5752, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_image_additional_width', '74', 0),
(5753, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_image_product_height', '228', 0),
(5754, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_image_product_width', '228', 0),
(5755, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_image_popup_height', '500', 0),
(5756, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_image_popup_width', '500', 0),
(5757, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_image_thumb_height', '228', 0),
(5758, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_image_thumb_width', '228', 0),
(5759, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_image_category_height', '80', 0),
(5760, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_image_category_width', '80', 0),
(5761, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_product_description_length', '100', 0),
(5762, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_product_limit', '15', 0),
(5763, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_status', '1', 0),
(5764, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_directory', 'beauty_xoiza_1_1', 0),
(5765, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_image_cart_width', '47', 0),
(5766, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_image_cart_height', '47', 0),
(5767, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_image_location_width', '268', 0),
(5768, 0, 'theme_beauty_xoiza_1_1', 'theme_beauty_xoiza_1_1_image_location_height', '50', 0),
(5769, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_image_wishlist_height', '47', 0),
(5770, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_image_wishlist_width', '47', 0),
(5771, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_image_compare_height', '90', 0),
(5772, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_image_compare_width', '90', 0),
(5773, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_image_related_height', '80', 0),
(5774, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_image_related_width', '80', 0),
(5775, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_image_additional_height', '74', 0),
(5776, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_image_additional_width', '74', 0),
(5777, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_image_product_height', '228', 0),
(5778, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_image_product_width', '228', 0),
(5779, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_image_popup_height', '500', 0),
(5780, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_image_popup_width', '500', 0),
(5781, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_image_thumb_height', '228', 0),
(5782, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_image_thumb_width', '228', 0),
(5783, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_image_category_height', '80', 0),
(5784, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_image_category_width', '80', 0),
(5785, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_product_description_length', '100', 0),
(5786, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_product_limit', '15', 0),
(5787, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_status', '1', 0),
(5788, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_directory', 'beauty_xoiza_1_2', 0),
(5789, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_image_cart_width', '47', 0),
(5790, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_image_cart_height', '47', 0),
(5791, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_image_location_width', '268', 0),
(5792, 0, 'theme_beauty_xoiza_1_2', 'theme_beauty_xoiza_1_2_image_location_height', '50', 0),
(5793, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_image_wishlist_height', '47', 0),
(5794, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_image_wishlist_width', '47', 0),
(5795, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_image_compare_height', '90', 0),
(5796, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_image_compare_width', '90', 0),
(5797, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_image_related_height', '80', 0),
(5798, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_image_related_width', '80', 0),
(5799, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_image_additional_height', '74', 0),
(5800, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_image_additional_width', '74', 0),
(5801, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_image_product_height', '228', 0),
(5802, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_image_product_width', '228', 0),
(5803, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_image_popup_height', '500', 0),
(5804, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_image_popup_width', '500', 0),
(5805, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_image_thumb_height', '228', 0),
(5806, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_image_thumb_width', '228', 0),
(5807, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_image_category_height', '80', 0),
(5808, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_image_category_width', '80', 0),
(5809, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_product_description_length', '100', 0),
(5810, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_product_limit', '15', 0),
(5811, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_status', '1', 0),
(5812, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_directory', 'beauty_xoiza_2_1', 0),
(5813, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_image_cart_width', '47', 0),
(5814, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_image_cart_height', '47', 0),
(5815, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_image_location_width', '268', 0),
(5816, 0, 'theme_beauty_xoiza_2_1', 'theme_beauty_xoiza_2_1_image_location_height', '50', 0),
(5817, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_image_wishlist_height', '47', 0),
(5818, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_image_wishlist_width', '47', 0),
(5819, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_image_compare_height', '90', 0),
(5820, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_image_compare_width', '90', 0),
(5821, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_image_related_height', '80', 0),
(5822, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_image_related_width', '80', 0),
(5823, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_image_additional_height', '74', 0),
(5824, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_image_additional_width', '74', 0),
(5825, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_image_product_height', '228', 0),
(5826, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_image_product_width', '228', 0),
(5827, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_image_popup_height', '500', 0),
(5828, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_image_popup_width', '500', 0),
(5829, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_image_thumb_height', '228', 0),
(5830, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_image_thumb_width', '228', 0),
(5831, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_image_category_height', '80', 0),
(5832, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_image_category_width', '80', 0),
(5833, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_product_description_length', '100', 0),
(5834, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_product_limit', '15', 0),
(5835, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_status', '1', 0),
(5836, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_directory', 'beauty_xoiza_2_2', 0),
(5837, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_image_cart_width', '47', 0),
(5838, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_image_cart_height', '47', 0),
(5839, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_image_location_width', '268', 0),
(5840, 0, 'theme_beauty_xoiza_2_2', 'theme_beauty_xoiza_2_2_image_location_height', '50', 0),
(5841, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_image_wishlist_height', '47', 0),
(5842, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_image_wishlist_width', '47', 0),
(5843, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_image_compare_height', '90', 0),
(5844, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_image_compare_width', '90', 0),
(5845, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_image_related_height', '80', 0);
INSERT INTO `oc_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(5846, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_image_related_width', '80', 0),
(5847, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_image_additional_height', '74', 0),
(5848, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_image_additional_width', '74', 0),
(5849, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_image_product_height', '228', 0),
(5850, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_image_product_width', '228', 0),
(5851, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_image_popup_height', '500', 0),
(5852, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_image_popup_width', '500', 0),
(5853, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_image_thumb_height', '228', 0),
(5854, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_image_thumb_width', '228', 0),
(5855, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_image_category_height', '80', 0),
(5856, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_image_category_width', '80', 0),
(5857, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_product_description_length', '100', 0),
(5858, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_product_limit', '15', 0),
(5859, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_status', '1', 0),
(5860, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_directory', 'fashion_luzun_2', 0),
(5861, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_image_cart_width', '47', 0),
(5862, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_image_cart_height', '47', 0),
(5863, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_image_location_width', '268', 0),
(5864, 0, 'theme_fashion_luzun_2', 'theme_fashion_luzun_2_image_location_height', '50', 0),
(5865, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_image_wishlist_width', '47', 0),
(5866, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_image_compare_height', '90', 0),
(5867, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_image_wishlist_height', '47', 0),
(5868, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_image_compare_width', '90', 0),
(5869, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_image_related_height', '80', 0),
(5870, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_image_related_width', '80', 0),
(5871, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_image_additional_height', '74', 0),
(5872, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_image_additional_width', '74', 0),
(5873, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_image_product_height', '228', 0),
(5874, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_image_product_width', '228', 0),
(5875, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_image_popup_height', '500', 0),
(5876, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_image_popup_width', '500', 0),
(5877, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_image_thumb_height', '228', 0),
(5878, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_image_thumb_width', '228', 0),
(5879, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_image_category_height', '80', 0),
(5880, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_image_category_width', '80', 0),
(5881, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_product_description_length', '100', 0),
(5882, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_product_limit', '15', 0),
(5883, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_status', '1', 0),
(5884, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_directory', 'fashion_luzun_3', 0),
(5885, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_image_cart_width', '47', 0),
(5886, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_image_cart_height', '47', 0),
(5887, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_image_location_width', '268', 0),
(5888, 0, 'theme_fashion_luzun_3', 'theme_fashion_luzun_3_image_location_height', '50', 0),
(5889, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_image_wishlist_width', '47', 0),
(5890, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_image_compare_height', '90', 0),
(5891, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_image_wishlist_height', '47', 0),
(5892, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_image_compare_width', '90', 0),
(5893, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_image_related_height', '80', 0),
(5894, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_image_related_width', '80', 0),
(5895, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_image_additional_height', '74', 0),
(5896, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_image_additional_width', '74', 0),
(5897, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_image_product_height', '228', 0),
(5898, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_image_product_width', '228', 0),
(5899, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_image_popup_height', '500', 0),
(5900, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_image_popup_width', '500', 0),
(5901, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_image_thumb_height', '228', 0),
(5902, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_image_thumb_width', '228', 0),
(5903, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_image_category_height', '80', 0),
(5904, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_image_category_width', '80', 0),
(5905, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_product_description_length', '100', 0),
(5906, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_product_limit', '15', 0),
(5907, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_status', '1', 0),
(5908, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_directory', 'fashion_monstar_2', 0),
(5909, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_image_cart_width', '47', 0),
(5910, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_image_cart_height', '47', 0),
(5911, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_image_location_width', '268', 0),
(5912, 0, 'theme_fashion_monstar_2', 'theme_fashion_monstar_2_image_location_height', '50', 0),
(5913, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_image_wishlist_width', '47', 0),
(5914, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_image_compare_height', '90', 0),
(5915, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_image_wishlist_height', '47', 0),
(5916, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_image_compare_width', '90', 0),
(5917, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_image_related_height', '80', 0),
(5918, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_image_related_width', '80', 0),
(5919, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_image_additional_height', '74', 0),
(5920, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_image_additional_width', '74', 0),
(5921, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_image_product_height', '228', 0),
(5922, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_image_product_width', '228', 0),
(5923, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_image_popup_height', '500', 0),
(5924, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_image_popup_width', '500', 0),
(5925, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_image_thumb_height', '228', 0),
(5926, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_image_thumb_width', '228', 0),
(5927, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_image_category_height', '80', 0),
(5928, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_image_category_width', '80', 0),
(5929, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_product_description_length', '100', 0),
(5930, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_product_limit', '15', 0),
(5931, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_status', '1', 0),
(5932, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_directory', 'fashion_monstar_3', 0),
(5933, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_image_cart_width', '47', 0),
(5934, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_image_cart_height', '47', 0),
(5935, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_image_location_width', '268', 0),
(5936, 0, 'theme_fashion_monstar_3', 'theme_fashion_monstar_3_image_location_height', '50', 0),
(5937, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_image_compare_height', '90', 0),
(5938, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_image_wishlist_height', '47', 0),
(5939, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_image_wishlist_width', '47', 0),
(5940, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_image_compare_width', '90', 0),
(5941, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_image_related_height', '80', 0),
(5942, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_image_related_width', '80', 0),
(5943, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_image_additional_height', '74', 0),
(5944, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_image_additional_width', '74', 0),
(5945, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_image_product_height', '228', 0),
(5946, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_image_product_width', '228', 0),
(5947, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_image_popup_height', '500', 0),
(5948, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_image_popup_width', '500', 0),
(5949, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_image_thumb_height', '228', 0),
(5950, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_image_thumb_width', '228', 0),
(5951, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_image_category_height', '80', 0),
(5952, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_image_category_width', '80', 0),
(5953, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_product_description_length', '100', 0),
(5954, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_product_limit', '15', 0),
(5955, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_status', '1', 0),
(5956, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_directory', 'fashion_shoezone_2', 0),
(5957, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_image_cart_width', '47', 0),
(5958, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_image_cart_height', '47', 0),
(5959, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_image_location_width', '268', 0),
(5960, 0, 'theme_fashion_shoezone_2', 'theme_fashion_shoezone_2_image_location_height', '50', 0),
(5961, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_image_compare_height', '90', 0),
(5962, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_image_wishlist_height', '47', 0),
(5963, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_image_wishlist_width', '47', 0),
(5964, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_image_compare_width', '90', 0),
(5965, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_image_related_height', '80', 0),
(5966, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_image_related_width', '80', 0),
(5967, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_image_additional_height', '74', 0),
(5968, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_image_additional_width', '74', 0),
(5969, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_image_product_height', '228', 0),
(5970, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_image_product_width', '228', 0),
(5971, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_image_popup_height', '500', 0),
(5972, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_image_popup_width', '500', 0),
(5973, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_image_thumb_height', '228', 0),
(5974, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_image_thumb_width', '228', 0),
(5975, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_image_category_height', '80', 0),
(5976, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_image_category_width', '80', 0),
(5977, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_product_description_length', '100', 0),
(5978, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_product_limit', '15', 0),
(5979, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_status', '1', 0),
(5980, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_directory', 'fashion_shoezone_3', 0),
(5981, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_image_cart_width', '47', 0),
(5982, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_image_cart_height', '47', 0),
(5983, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_image_location_width', '268', 0),
(5984, 0, 'theme_fashion_shoezone_3', 'theme_fashion_shoezone_3_image_location_height', '50', 0),
(5985, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_image_compare_height', '90', 0),
(5986, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_image_wishlist_height', '47', 0),
(5987, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_image_wishlist_width', '47', 0),
(5988, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_image_compare_width', '90', 0),
(5989, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_image_related_height', '80', 0),
(5990, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_image_related_width', '80', 0),
(5991, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_image_additional_height', '74', 0),
(5992, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_image_additional_width', '74', 0),
(5993, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_image_product_height', '228', 0),
(5994, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_image_product_width', '228', 0),
(5995, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_image_popup_height', '500', 0),
(5996, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_image_popup_width', '500', 0),
(5997, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_image_thumb_height', '228', 0),
(5998, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_image_thumb_width', '228', 0),
(5999, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_image_category_height', '80', 0),
(6000, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_image_category_width', '80', 0),
(6001, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_product_description_length', '100', 0),
(6002, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_product_limit', '15', 0),
(6003, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_status', '1', 0),
(6004, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_directory', 'fashion_xoiza_2', 0),
(6005, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_image_cart_width', '47', 0),
(6006, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_image_cart_height', '47', 0),
(6007, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_image_location_width', '268', 0),
(6008, 0, 'theme_fashion_xoiza_2', 'theme_fashion_xoiza_2_image_location_height', '50', 0),
(6009, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_image_compare_height', '90', 0),
(6010, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_image_wishlist_height', '47', 0),
(6011, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_image_wishlist_width', '47', 0),
(6012, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_image_compare_width', '90', 0),
(6013, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_image_related_height', '80', 0),
(6014, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_image_related_width', '80', 0),
(6015, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_image_additional_height', '74', 0),
(6016, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_image_additional_width', '74', 0),
(6017, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_image_product_height', '228', 0),
(6018, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_image_product_width', '228', 0),
(6019, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_image_popup_height', '500', 0),
(6020, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_image_popup_width', '500', 0),
(6021, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_image_thumb_height', '228', 0),
(6022, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_image_thumb_width', '228', 0),
(6023, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_image_category_height', '80', 0),
(6024, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_image_category_width', '80', 0),
(6025, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_product_description_length', '100', 0),
(6026, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_product_limit', '15', 0),
(6027, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_status', '1', 0),
(6028, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_directory', 'fashion_xoiza_3', 0),
(6029, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_image_cart_width', '47', 0),
(6030, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_image_cart_height', '47', 0),
(6031, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_image_location_width', '268', 0),
(6032, 0, 'theme_fashion_xoiza_3', 'theme_fashion_xoiza_3_image_location_height', '50', 0),
(6033, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_image_compare_height', '90', 0),
(6034, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_image_wishlist_height', '47', 0),
(6035, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_image_wishlist_width', '47', 0),
(6036, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_image_compare_width', '90', 0),
(6037, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_image_related_height', '80', 0),
(6038, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_image_related_width', '80', 0),
(6039, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_image_additional_height', '74', 0),
(6040, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_image_additional_width', '74', 0),
(6041, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_image_product_height', '228', 0),
(6042, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_image_product_width', '228', 0),
(6043, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_image_popup_height', '500', 0),
(6044, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_image_popup_width', '500', 0),
(6045, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_image_thumb_height', '228', 0),
(6046, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_image_thumb_width', '228', 0),
(6047, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_image_category_height', '80', 0),
(6048, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_image_category_width', '80', 0),
(6049, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_product_description_length', '100', 0),
(6050, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_product_limit', '15', 0),
(6051, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_status', '1', 0),
(6052, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_directory', 'fashion_zork_2', 0),
(6053, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_image_cart_width', '47', 0),
(6054, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_image_cart_height', '47', 0),
(6055, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_image_location_width', '268', 0),
(6056, 0, 'theme_fashion_zork_2', 'theme_fashion_zork_2_image_location_height', '50', 0),
(6057, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_image_compare_height', '90', 0),
(6058, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_image_wishlist_height', '47', 0),
(6059, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_image_wishlist_width', '47', 0),
(6060, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_image_compare_width', '90', 0),
(6061, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_image_related_height', '80', 0),
(6062, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_image_related_width', '80', 0),
(6063, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_image_additional_height', '74', 0),
(6064, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_image_additional_width', '74', 0),
(6065, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_image_product_height', '228', 0),
(6066, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_image_product_width', '228', 0),
(6067, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_image_popup_height', '500', 0),
(6068, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_image_popup_width', '500', 0),
(6069, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_image_thumb_height', '228', 0),
(6070, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_image_thumb_width', '228', 0),
(6071, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_image_category_height', '80', 0),
(6072, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_image_category_width', '80', 0),
(6073, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_product_description_length', '100', 0),
(6074, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_product_limit', '15', 0),
(6075, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_status', '1', 0),
(6076, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_directory', 'fashion_zork_3', 0),
(6077, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_image_cart_width', '47', 0),
(6078, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_image_cart_height', '47', 0),
(6079, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_image_location_width', '268', 0),
(6080, 0, 'theme_fashion_zork_3', 'theme_fashion_zork_3_image_location_height', '50', 0),
(6081, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_image_compare_height', '90', 0),
(6082, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_image_wishlist_height', '47', 0),
(6083, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_image_wishlist_width', '47', 0),
(6084, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_image_compare_width', '90', 0),
(6085, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_image_related_height', '80', 0),
(6086, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_image_related_width', '80', 0),
(6087, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_image_additional_height', '74', 0),
(6088, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_image_additional_width', '74', 0),
(6089, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_image_product_height', '228', 0),
(6090, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_image_product_width', '228', 0),
(6091, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_image_popup_height', '500', 0),
(6092, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_image_popup_width', '500', 0),
(6093, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_image_thumb_height', '228', 0),
(6094, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_image_thumb_width', '228', 0),
(6095, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_image_category_height', '80', 0),
(6096, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_image_category_width', '80', 0),
(6097, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_product_description_length', '100', 0),
(6098, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_product_limit', '15', 0),
(6099, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_status', '1', 0),
(6100, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_directory', 'tech_allegorithmic_2', 0),
(6101, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_image_cart_width', '47', 0),
(6102, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_image_cart_height', '47', 0),
(6103, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_image_location_width', '268', 0),
(6104, 0, 'theme_tech_allegorithmic_2', 'theme_tech_allegorithmic_2_image_location_height', '50', 0),
(6105, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_image_compare_height', '90', 0),
(6106, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_image_wishlist_height', '47', 0),
(6107, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_image_wishlist_width', '47', 0),
(6108, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_image_compare_width', '90', 0),
(6109, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_image_related_height', '80', 0),
(6110, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_image_related_width', '80', 0),
(6111, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_image_additional_height', '74', 0),
(6112, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_image_additional_width', '74', 0),
(6113, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_image_product_height', '228', 0),
(6114, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_image_product_width', '228', 0),
(6115, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_image_popup_height', '500', 0),
(6116, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_image_popup_width', '500', 0),
(6117, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_image_thumb_height', '228', 0),
(6118, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_image_thumb_width', '228', 0),
(6119, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_image_category_height', '80', 0),
(6120, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_image_category_width', '80', 0),
(6121, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_product_description_length', '100', 0),
(6122, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_product_limit', '15', 0),
(6123, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_status', '1', 0),
(6124, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_directory', 'tech_allegorithmic_3', 0),
(6125, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_image_cart_width', '47', 0),
(6126, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_image_cart_height', '47', 0),
(6127, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_image_location_width', '268', 0),
(6128, 0, 'theme_tech_allegorithmic_3', 'theme_tech_allegorithmic_3_image_location_height', '50', 0),
(6129, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_image_compare_height', '90', 0),
(6130, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_image_wishlist_height', '47', 0),
(6131, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_image_wishlist_width', '47', 0),
(6132, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_image_compare_width', '90', 0),
(6133, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_image_related_height', '80', 0),
(6134, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_image_related_width', '80', 0),
(6135, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_image_additional_height', '74', 0),
(6136, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_image_additional_width', '74', 0),
(6137, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_image_product_height', '228', 0),
(6138, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_image_product_width', '228', 0),
(6139, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_image_popup_height', '500', 0),
(6140, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_image_popup_width', '500', 0),
(6141, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_image_thumb_height', '228', 0),
(6142, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_image_thumb_width', '228', 0),
(6143, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_image_category_height', '80', 0),
(6144, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_image_category_width', '80', 0),
(6145, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_product_description_length', '100', 0),
(6146, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_product_limit', '15', 0),
(6147, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_status', '1', 0),
(6148, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_directory', 'tech_digitech_2', 0),
(6149, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_image_cart_width', '47', 0),
(6150, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_image_cart_height', '47', 0),
(6151, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_image_location_width', '268', 0),
(6152, 0, 'theme_tech_digitech_2', 'theme_tech_digitech_2_image_location_height', '50', 0),
(6153, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_image_compare_height', '90', 0),
(6154, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_image_wishlist_height', '47', 0),
(6155, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_image_wishlist_width', '47', 0),
(6156, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_image_compare_width', '90', 0),
(6157, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_image_related_height', '80', 0),
(6158, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_image_related_width', '80', 0),
(6159, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_image_additional_height', '74', 0),
(6160, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_image_additional_width', '74', 0),
(6161, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_image_product_height', '228', 0),
(6162, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_image_product_width', '228', 0),
(6163, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_image_popup_height', '500', 0),
(6164, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_image_popup_width', '500', 0),
(6165, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_image_thumb_height', '228', 0),
(6166, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_image_thumb_width', '228', 0),
(6167, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_image_category_height', '80', 0),
(6168, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_image_category_width', '80', 0),
(6169, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_product_description_length', '100', 0),
(6170, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_product_limit', '15', 0),
(6171, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_status', '1', 0),
(6172, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_directory', 'tech_digitech_3', 0),
(6173, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_image_cart_width', '47', 0),
(6174, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_image_cart_height', '47', 0),
(6175, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_image_location_width', '268', 0),
(6176, 0, 'theme_tech_digitech_3', 'theme_tech_digitech_3_image_location_height', '50', 0),
(6177, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_image_compare_height', '90', 0),
(6178, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_image_wishlist_height', '47', 0),
(6179, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_image_wishlist_width', '47', 0),
(6180, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_image_compare_width', '90', 0),
(6181, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_image_related_height', '80', 0),
(6182, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_image_related_width', '80', 0),
(6183, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_image_additional_height', '74', 0),
(6184, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_image_additional_width', '74', 0),
(6185, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_image_product_height', '228', 0),
(6186, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_image_product_width', '228', 0),
(6187, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_image_popup_height', '500', 0),
(6188, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_image_popup_width', '500', 0),
(6189, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_image_thumb_height', '228', 0),
(6190, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_image_thumb_width', '228', 0),
(6191, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_image_category_height', '80', 0),
(6192, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_image_category_width', '80', 0),
(6193, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_product_description_length', '100', 0),
(6194, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_product_limit', '15', 0),
(6195, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_status', '1', 0),
(6196, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_directory', 'tech_marte_2', 0),
(6197, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_image_cart_width', '47', 0),
(6198, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_image_cart_height', '47', 0),
(6199, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_image_location_width', '268', 0),
(6200, 0, 'theme_tech_marte_2', 'theme_tech_marte_2_image_location_height', '50', 0),
(6201, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_image_compare_height', '90', 0),
(6202, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_image_wishlist_height', '47', 0),
(6203, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_image_wishlist_width', '47', 0),
(6204, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_image_compare_width', '90', 0),
(6205, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_image_related_height', '80', 0),
(6206, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_image_related_width', '80', 0),
(6207, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_image_additional_height', '74', 0),
(6208, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_image_additional_width', '74', 0),
(6209, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_image_product_height', '228', 0),
(6210, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_image_product_width', '228', 0),
(6211, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_image_popup_height', '500', 0),
(6212, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_image_popup_width', '500', 0),
(6213, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_image_thumb_height', '228', 0),
(6214, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_image_thumb_width', '228', 0),
(6215, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_image_category_height', '80', 0),
(6216, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_image_category_width', '80', 0),
(6217, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_product_description_length', '100', 0),
(6218, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_product_limit', '15', 0),
(6219, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_status', '1', 0),
(6220, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_directory', 'tech_marte_3', 0),
(6221, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_image_cart_width', '47', 0),
(6222, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_image_cart_height', '47', 0),
(6223, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_image_location_width', '268', 0),
(6224, 0, 'theme_tech_marte_3', 'theme_tech_marte_3_image_location_height', '50', 0),
(6225, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_image_compare_height', '90', 0),
(6226, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_image_wishlist_height', '47', 0),
(6227, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_image_wishlist_width', '47', 0),
(6228, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_image_compare_width', '90', 0),
(6229, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_image_related_height', '80', 0),
(6230, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_image_related_width', '80', 0),
(6231, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_image_additional_height', '74', 0),
(6232, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_image_additional_width', '74', 0),
(6233, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_image_product_height', '228', 0),
(6234, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_image_product_width', '228', 0),
(6235, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_image_popup_height', '500', 0),
(6236, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_image_popup_width', '500', 0),
(6237, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_image_thumb_height', '228', 0),
(6238, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_image_thumb_width', '228', 0),
(6239, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_image_category_height', '80', 0),
(6240, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_image_category_width', '80', 0),
(6241, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_product_description_length', '100', 0),
(6242, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_product_limit', '15', 0),
(6243, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_status', '1', 0),
(6244, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_directory', 'furniture_lujun_decor', 0),
(6245, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_image_cart_width', '47', 0),
(6246, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_image_cart_height', '47', 0),
(6247, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_image_location_width', '268', 0),
(6248, 0, 'theme_furniture_lujun_decor', 'theme_furniture_lujun_decor_image_location_height', '50', 0),
(6249, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_image_compare_height', '90', 0),
(6250, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_image_wishlist_height', '47', 0),
(6251, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_image_wishlist_width', '47', 0),
(6252, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_image_compare_width', '90', 0),
(6253, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_image_related_height', '80', 0),
(6254, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_image_related_width', '80', 0),
(6255, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_image_additional_height', '74', 0),
(6256, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_image_additional_width', '74', 0),
(6257, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_image_product_height', '228', 0),
(6258, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_image_product_width', '228', 0),
(6259, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_image_popup_height', '500', 0),
(6260, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_image_popup_width', '500', 0),
(6261, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_image_thumb_height', '228', 0),
(6262, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_image_thumb_width', '228', 0),
(6263, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_image_category_height', '80', 0),
(6264, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_image_category_width', '80', 0),
(6265, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_product_description_length', '100', 0),
(6266, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_product_limit', '15', 0),
(6267, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_status', '1', 0),
(6268, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_directory', 'furniture_lujun_decor_2', 0),
(6269, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_image_cart_width', '47', 0),
(6270, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_image_cart_height', '47', 0),
(6271, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_image_location_width', '268', 0),
(6272, 0, 'theme_furniture_lujun_decor_2', 'theme_furniture_lujun_decor_2_image_location_height', '50', 0),
(6273, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_image_compare_height', '90', 0),
(6274, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_image_wishlist_height', '47', 0),
(6275, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_image_wishlist_width', '47', 0),
(6276, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_image_compare_width', '90', 0),
(6277, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_image_related_height', '80', 0),
(6278, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_image_related_width', '80', 0),
(6279, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_image_additional_height', '74', 0),
(6280, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_image_additional_width', '74', 0),
(6281, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_image_product_height', '228', 0),
(6282, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_image_product_width', '228', 0),
(6283, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_image_popup_height', '500', 0),
(6284, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_image_popup_width', '500', 0),
(6285, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_image_thumb_height', '228', 0),
(6286, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_image_thumb_width', '228', 0),
(6287, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_image_category_height', '80', 0),
(6288, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_image_category_width', '80', 0),
(6289, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_product_description_length', '100', 0),
(6290, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_product_limit', '15', 0),
(6291, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_status', '1', 0),
(6292, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_directory', 'furniture_lujun_decor_3', 0),
(6293, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_image_cart_width', '47', 0),
(6294, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_image_cart_height', '47', 0),
(6295, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_image_location_width', '268', 0),
(6296, 0, 'theme_furniture_lujun_decor_3', 'theme_furniture_lujun_decor_3_image_location_height', '50', 0),
(6297, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_image_compare_height', '90', 0),
(6298, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_image_wishlist_height', '47', 0),
(6299, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_image_wishlist_width', '47', 0),
(6300, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_image_compare_width', '90', 0),
(6301, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_image_related_height', '80', 0),
(6302, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_image_related_width', '80', 0),
(6303, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_image_additional_height', '74', 0),
(6304, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_image_additional_width', '74', 0),
(6305, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_image_product_height', '228', 0),
(6306, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_image_product_width', '228', 0),
(6307, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_image_popup_height', '500', 0),
(6308, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_image_popup_width', '500', 0),
(6309, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_image_thumb_height', '228', 0),
(6310, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_image_thumb_width', '228', 0),
(6311, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_image_category_height', '80', 0),
(6312, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_image_category_width', '80', 0),
(6313, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_product_description_length', '100', 0),
(6314, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_product_limit', '15', 0),
(6315, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_status', '1', 0),
(6316, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_directory', 'fashion_m_boutique', 0),
(6317, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_image_cart_width', '47', 0),
(6318, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_image_cart_height', '47', 0),
(6319, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_image_location_width', '268', 0),
(6320, 0, 'theme_fashion_m_boutique', 'theme_fashion_m_boutique_image_location_height', '50', 0),
(6321, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_image_compare_height', '90', 0),
(6322, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_image_wishlist_height', '47', 0),
(6323, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_image_wishlist_width', '47', 0),
(6324, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_image_compare_width', '90', 0),
(6325, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_image_related_height', '80', 0),
(6326, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_image_related_width', '80', 0),
(6327, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_image_additional_height', '74', 0),
(6328, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_image_additional_width', '74', 0),
(6329, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_image_product_height', '228', 0),
(6330, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_image_product_width', '228', 0),
(6331, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_image_popup_height', '500', 0),
(6332, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_image_popup_width', '500', 0),
(6333, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_image_thumb_height', '228', 0),
(6334, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_image_thumb_width', '228', 0),
(6335, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_image_category_height', '80', 0),
(6336, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_image_category_width', '80', 0),
(6337, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_product_description_length', '100', 0),
(6338, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_product_limit', '15', 0),
(6339, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_status', '1', 0),
(6340, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_directory', 'fashion_m_boutique_2', 0),
(6341, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_image_cart_width', '47', 0),
(6342, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_image_cart_height', '47', 0),
(6343, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_image_location_width', '268', 0),
(6344, 0, 'theme_fashion_m_boutique_2', 'theme_fashion_m_boutique_2_image_location_height', '50', 0),
(6345, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_image_compare_height', '90', 0),
(6346, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_image_wishlist_height', '47', 0),
(6347, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_image_wishlist_width', '47', 0),
(6348, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_image_compare_width', '90', 0),
(6349, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_image_related_height', '80', 0),
(6350, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_image_related_width', '80', 0),
(6351, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_image_additional_height', '74', 0),
(6352, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_image_additional_width', '74', 0),
(6353, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_image_product_height', '228', 0),
(6354, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_image_product_width', '228', 0),
(6355, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_image_popup_height', '500', 0),
(6356, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_image_popup_width', '500', 0),
(6357, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_image_thumb_height', '228', 0),
(6358, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_image_thumb_width', '228', 0),
(6359, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_image_category_height', '80', 0),
(6360, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_image_category_width', '80', 0),
(6361, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_product_description_length', '100', 0),
(6362, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_product_limit', '15', 0),
(6363, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_status', '1', 0),
(6364, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_directory', 'fashion_m_boutique_3', 0),
(6365, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_image_cart_width', '47', 0),
(6366, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_image_cart_height', '47', 0),
(6367, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_image_location_width', '268', 0),
(6368, 0, 'theme_fashion_m_boutique_3', 'theme_fashion_m_boutique_3_image_location_height', '50', 0),
(6369, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_image_compare_height', '90', 0),
(6370, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_image_wishlist_height', '47', 0),
(6371, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_image_wishlist_width', '47', 0),
(6372, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_image_compare_width', '90', 0),
(6373, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_image_related_height', '80', 0),
(6374, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_image_related_width', '80', 0),
(6375, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_image_additional_height', '74', 0),
(6376, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_image_additional_width', '74', 0),
(6377, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_image_product_height', '228', 0),
(6378, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_image_product_width', '228', 0),
(6379, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_image_popup_height', '500', 0),
(6380, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_image_popup_width', '500', 0),
(6381, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_image_thumb_height', '228', 0),
(6382, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_image_thumb_width', '228', 0),
(6383, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_image_category_height', '80', 0),
(6384, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_image_category_width', '80', 0);
INSERT INTO `oc_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(6385, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_product_description_length', '100', 0),
(6386, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_product_limit', '15', 0),
(6387, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_status', '1', 0),
(6388, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_directory', 'fashion_luvibee', 0),
(6389, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_image_cart_width', '47', 0),
(6390, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_image_cart_height', '47', 0),
(6391, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_image_location_width', '268', 0),
(6392, 0, 'theme_fashion_luvibee', 'theme_fashion_luvibee_image_location_height', '50', 0),
(6393, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_compare_height', '90', 0),
(6394, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_wishlist_height', '47', 0),
(6395, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_wishlist_width', '47', 0),
(6396, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_compare_width', '90', 0),
(6397, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_related_height', '80', 0),
(6398, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_related_width', '80', 0),
(6399, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_additional_height', '74', 0),
(6400, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_additional_width', '74', 0),
(6401, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_product_height', '228', 0),
(6402, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_product_width', '228', 0),
(6403, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_popup_height', '500', 0),
(6404, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_popup_width', '500', 0),
(6405, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_thumb_height', '228', 0),
(6406, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_thumb_width', '228', 0),
(6407, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_category_height', '80', 0),
(6408, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_category_width', '80', 0),
(6409, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_product_description_length', '100', 0),
(6410, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_product_limit', '15', 0),
(6411, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_status', '1', 0),
(6412, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_directory', 'fashion_luvibee_2', 0),
(6413, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_cart_width', '47', 0),
(6414, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_cart_height', '47', 0),
(6415, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_location_width', '268', 0),
(6416, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_location_height', '50', 0),
(6417, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_image_compare_height', '90', 0),
(6418, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_image_wishlist_height', '47', 0),
(6419, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_image_wishlist_width', '47', 0),
(6420, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_image_compare_width', '90', 0),
(6421, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_image_related_height', '80', 0),
(6422, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_image_related_width', '80', 0),
(6423, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_image_additional_height', '74', 0),
(6424, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_image_additional_width', '74', 0),
(6425, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_image_product_height', '228', 0),
(6426, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_image_product_width', '228', 0),
(6427, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_image_popup_height', '500', 0),
(6428, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_image_popup_width', '500', 0),
(6429, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_image_thumb_height', '228', 0),
(6430, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_image_thumb_width', '228', 0),
(6431, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_image_category_height', '80', 0),
(6432, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_image_category_width', '80', 0),
(6433, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_product_description_length', '100', 0),
(6434, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_product_limit', '15', 0),
(6435, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_status', '1', 0),
(6436, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_directory', 'fashion_luvibee_3', 0),
(6437, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_image_cart_width', '47', 0),
(6438, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_image_cart_height', '47', 0),
(6439, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_image_location_width', '268', 0),
(6440, 0, 'theme_fashion_luvibee_3', 'theme_fashion_luvibee_3_image_location_height', '50', 0),
(6441, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_image_compare_height', '90', 0),
(6442, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_image_wishlist_height', '47', 0),
(6443, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_image_wishlist_width', '47', 0),
(6444, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_image_compare_width', '90', 0),
(6445, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_image_related_height', '80', 0),
(6446, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_image_related_width', '80', 0),
(6447, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_image_additional_height', '74', 0),
(6448, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_image_additional_width', '74', 0),
(6449, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_image_product_height', '228', 0),
(6450, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_image_product_width', '228', 0),
(6451, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_image_popup_height', '500', 0),
(6452, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_image_popup_width', '500', 0),
(6453, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_image_thumb_height', '228', 0),
(6454, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_image_thumb_width', '228', 0),
(6455, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_image_category_height', '80', 0),
(6456, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_image_category_width', '80', 0),
(6457, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_product_description_length', '100', 0),
(6458, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_product_limit', '15', 0),
(6459, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_status', '1', 0),
(6460, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_directory', 'fashion_ega_fashion', 0),
(6461, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_image_cart_width', '47', 0),
(6462, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_image_cart_height', '47', 0),
(6463, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_image_location_width', '268', 0),
(6464, 0, 'theme_fashion_ega_fashion', 'theme_fashion_ega_fashion_image_location_height', '50', 0),
(6465, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_image_compare_height', '90', 0),
(6466, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_image_wishlist_height', '47', 0),
(6467, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_image_wishlist_width', '47', 0),
(6468, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_image_compare_width', '90', 0),
(6469, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_image_related_height', '80', 0),
(6470, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_image_related_width', '80', 0),
(6471, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_image_additional_height', '74', 0),
(6472, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_image_additional_width', '74', 0),
(6473, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_image_product_height', '228', 0),
(6474, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_image_product_width', '228', 0),
(6475, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_image_popup_height', '500', 0),
(6476, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_image_popup_width', '500', 0),
(6477, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_image_thumb_height', '228', 0),
(6478, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_image_thumb_width', '228', 0),
(6479, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_image_category_height', '80', 0),
(6480, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_image_category_width', '80', 0),
(6481, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_product_description_length', '100', 0),
(6482, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_product_limit', '15', 0),
(6483, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_status', '1', 0),
(6484, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_directory', 'fashion_ega_womenshoes', 0),
(6485, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_image_cart_width', '47', 0),
(6486, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_image_cart_height', '47', 0),
(6487, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_image_location_width', '268', 0),
(6488, 0, 'theme_fashion_ega_womenshoes', 'theme_fashion_ega_womenshoes_image_location_height', '50', 0),
(6489, 0, 'theme_office_ap_office', 'theme_office_ap_office_image_compare_height', '90', 0),
(6490, 0, 'theme_office_ap_office', 'theme_office_ap_office_image_wishlist_height', '47', 0),
(6491, 0, 'theme_office_ap_office', 'theme_office_ap_office_image_wishlist_width', '47', 0),
(6492, 0, 'theme_office_ap_office', 'theme_office_ap_office_image_compare_width', '90', 0),
(6493, 0, 'theme_office_ap_office', 'theme_office_ap_office_image_related_height', '80', 0),
(6494, 0, 'theme_office_ap_office', 'theme_office_ap_office_image_related_width', '80', 0),
(6495, 0, 'theme_office_ap_office', 'theme_office_ap_office_image_additional_height', '74', 0),
(6496, 0, 'theme_office_ap_office', 'theme_office_ap_office_image_additional_width', '74', 0),
(6497, 0, 'theme_office_ap_office', 'theme_office_ap_office_image_product_height', '228', 0),
(6498, 0, 'theme_office_ap_office', 'theme_office_ap_office_image_product_width', '228', 0),
(6499, 0, 'theme_office_ap_office', 'theme_office_ap_office_image_popup_height', '500', 0),
(6500, 0, 'theme_office_ap_office', 'theme_office_ap_office_image_popup_width', '500', 0),
(6501, 0, 'theme_office_ap_office', 'theme_office_ap_office_image_thumb_height', '228', 0),
(6502, 0, 'theme_office_ap_office', 'theme_office_ap_office_image_thumb_width', '228', 0),
(6503, 0, 'theme_office_ap_office', 'theme_office_ap_office_image_category_height', '80', 0),
(6504, 0, 'theme_office_ap_office', 'theme_office_ap_office_image_category_width', '80', 0),
(6505, 0, 'theme_office_ap_office', 'theme_office_ap_office_product_description_length', '100', 0),
(6506, 0, 'theme_office_ap_office', 'theme_office_ap_office_product_limit', '15', 0),
(6507, 0, 'theme_office_ap_office', 'theme_office_ap_office_status', '1', 0),
(6508, 0, 'theme_office_ap_office', 'theme_office_ap_office_directory', 'office_ap_office', 0),
(6509, 0, 'theme_office_ap_office', 'theme_office_ap_office_image_cart_width', '47', 0),
(6510, 0, 'theme_office_ap_office', 'theme_office_ap_office_image_cart_height', '47', 0),
(6511, 0, 'theme_office_ap_office', 'theme_office_ap_office_image_location_width', '268', 0),
(6512, 0, 'theme_office_ap_office', 'theme_office_ap_office_image_location_height', '50', 0),
(6513, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_image_compare_height', '90', 0),
(6514, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_image_wishlist_height', '47', 0),
(6515, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_image_wishlist_width', '47', 0),
(6516, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_image_compare_width', '90', 0),
(6517, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_image_related_height', '80', 0),
(6518, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_image_related_width', '80', 0),
(6519, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_image_additional_height', '74', 0),
(6520, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_image_additional_width', '74', 0),
(6521, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_image_product_height', '228', 0),
(6522, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_image_product_width', '228', 0),
(6523, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_image_popup_height', '500', 0),
(6524, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_image_popup_width', '500', 0),
(6525, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_image_thumb_height', '228', 0),
(6526, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_image_thumb_width', '228', 0),
(6527, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_image_category_height', '80', 0),
(6528, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_image_category_width', '80', 0),
(6529, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_product_description_length', '100', 0),
(6530, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_product_limit', '15', 0),
(6531, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_status', '1', 0),
(6532, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_directory', 'fashion_ap_new_fashion', 0),
(6533, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_image_cart_width', '47', 0),
(6534, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_image_cart_height', '47', 0),
(6535, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_image_location_width', '268', 0),
(6536, 0, 'theme_fashion_ap_new_fashion', 'theme_fashion_ap_new_fashion_image_location_height', '50', 0),
(6537, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_image_compare_height', '90', 0),
(6538, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_image_wishlist_height', '47', 0),
(6539, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_image_wishlist_width', '47', 0),
(6540, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_image_compare_width', '90', 0),
(6541, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_image_related_height', '80', 0),
(6542, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_image_related_width', '80', 0),
(6543, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_image_additional_height', '74', 0),
(6544, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_image_additional_width', '74', 0),
(6545, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_image_product_height', '228', 0),
(6546, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_image_product_width', '228', 0),
(6547, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_image_popup_height', '500', 0),
(6548, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_image_popup_width', '500', 0),
(6549, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_image_thumb_height', '228', 0),
(6550, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_image_thumb_width', '228', 0),
(6551, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_image_category_height', '80', 0),
(6552, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_image_category_width', '80', 0),
(6553, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_product_description_length', '100', 0),
(6554, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_product_limit', '15', 0),
(6555, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_status', '1', 0),
(6556, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_directory', 'fashion_ap_signme', 0),
(6557, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_image_cart_width', '47', 0),
(6558, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_image_cart_height', '47', 0),
(6559, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_image_location_width', '268', 0),
(6560, 0, 'theme_fashion_ap_signme', 'theme_fashion_ap_signme_image_location_height', '50', 0),
(6561, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_image_compare_height', '90', 0),
(6562, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_image_wishlist_height', '47', 0),
(6563, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_image_wishlist_width', '47', 0),
(6564, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_image_compare_width', '90', 0),
(6565, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_image_related_height', '80', 0),
(6566, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_image_related_width', '80', 0),
(6567, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_image_additional_height', '74', 0),
(6568, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_image_additional_width', '74', 0),
(6569, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_image_product_height', '228', 0),
(6570, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_image_product_width', '228', 0),
(6571, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_image_popup_height', '500', 0),
(6572, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_image_popup_width', '500', 0),
(6573, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_image_thumb_height', '228', 0),
(6574, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_image_thumb_width', '228', 0),
(6575, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_image_category_height', '80', 0),
(6576, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_image_category_width', '80', 0),
(6577, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_product_description_length', '100', 0),
(6578, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_product_limit', '15', 0),
(6579, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_status', '1', 0),
(6580, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_directory', 'beauty_ega_cosmetics', 0),
(6581, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_image_cart_width', '47', 0),
(6582, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_image_cart_height', '47', 0),
(6583, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_image_location_width', '268', 0),
(6584, 0, 'theme_beauty_ega_cosmetics', 'theme_beauty_ega_cosmetics_image_location_height', '50', 0),
(6585, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_image_compare_height', '90', 0),
(6586, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_image_wishlist_height', '47', 0),
(6587, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_image_wishlist_width', '47', 0),
(6588, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_image_compare_width', '90', 0),
(6589, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_image_related_height', '80', 0),
(6590, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_image_related_width', '80', 0),
(6591, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_image_additional_height', '74', 0),
(6592, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_image_additional_width', '74', 0),
(6593, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_image_product_height', '228', 0),
(6594, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_image_product_width', '228', 0),
(6595, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_image_popup_height', '500', 0),
(6596, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_image_popup_width', '500', 0),
(6597, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_image_thumb_height', '228', 0),
(6598, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_image_thumb_width', '228', 0),
(6599, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_image_category_height', '80', 0),
(6600, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_image_category_width', '80', 0),
(6601, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_product_description_length', '100', 0),
(6602, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_product_limit', '15', 0),
(6603, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_status', '1', 0),
(6604, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_directory', 'beauty_ega_healthyfood', 0),
(6605, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_image_cart_width', '47', 0),
(6606, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_image_cart_height', '47', 0),
(6607, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_image_location_width', '268', 0),
(6608, 0, 'theme_beauty_ega_healthyfood', 'theme_beauty_ega_healthyfood_image_location_height', '50', 0),
(6609, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_image_compare_height', '90', 0),
(6610, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_image_wishlist_height', '47', 0),
(6611, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_image_wishlist_width', '47', 0),
(6612, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_image_compare_width', '90', 0),
(6613, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_image_related_height', '80', 0),
(6614, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_image_related_width', '80', 0),
(6615, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_image_additional_height', '74', 0),
(6616, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_image_additional_width', '74', 0),
(6617, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_image_product_height', '228', 0),
(6618, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_image_product_width', '228', 0),
(6619, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_image_popup_height', '500', 0),
(6620, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_image_popup_width', '500', 0),
(6621, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_image_thumb_height', '228', 0),
(6622, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_image_thumb_width', '228', 0),
(6623, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_image_category_height', '80', 0),
(6624, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_image_category_width', '80', 0),
(6625, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_product_description_length', '100', 0),
(6626, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_product_limit', '15', 0),
(6627, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_status', '1', 0),
(6628, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_directory', 'beauty_ap_cosmetic', 0),
(6629, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_image_cart_width', '47', 0),
(6630, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_image_cart_height', '47', 0),
(6631, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_image_location_width', '268', 0),
(6632, 0, 'theme_beauty_ap_cosmetic', 'theme_beauty_ap_cosmetic_image_location_height', '50', 0),
(6633, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_image_compare_height', '90', 0),
(6634, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_image_wishlist_height', '47', 0),
(6635, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_image_wishlist_width', '47', 0),
(6636, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_image_compare_width', '90', 0),
(6637, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_image_related_height', '80', 0),
(6638, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_image_related_width', '80', 0),
(6639, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_image_additional_height', '74', 0),
(6640, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_image_additional_width', '74', 0),
(6641, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_image_product_height', '228', 0),
(6642, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_image_product_width', '228', 0),
(6643, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_image_popup_height', '500', 0),
(6644, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_image_popup_width', '500', 0),
(6645, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_image_thumb_height', '228', 0),
(6646, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_image_thumb_width', '228', 0),
(6647, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_image_category_height', '80', 0),
(6648, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_image_category_width', '80', 0),
(6649, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_product_description_length', '100', 0),
(6650, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_product_limit', '15', 0),
(6651, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_status', '1', 0),
(6652, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_directory', 'fashion_flavor', 0),
(6653, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_image_cart_width', '47', 0),
(6654, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_image_cart_height', '47', 0),
(6655, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_image_location_width', '268', 0),
(6656, 0, 'theme_fashion_flavor', 'theme_fashion_flavor_image_location_height', '50', 0),
(6657, 0, 'theme_tech_egomall', 'theme_tech_egomall_image_compare_height', '90', 0),
(6658, 0, 'theme_tech_egomall', 'theme_tech_egomall_image_wishlist_height', '47', 0),
(6659, 0, 'theme_tech_egomall', 'theme_tech_egomall_image_wishlist_width', '47', 0),
(6660, 0, 'theme_tech_egomall', 'theme_tech_egomall_image_compare_width', '90', 0),
(6661, 0, 'theme_tech_egomall', 'theme_tech_egomall_image_related_height', '80', 0),
(6662, 0, 'theme_tech_egomall', 'theme_tech_egomall_image_related_width', '80', 0),
(6663, 0, 'theme_tech_egomall', 'theme_tech_egomall_image_additional_height', '74', 0),
(6664, 0, 'theme_tech_egomall', 'theme_tech_egomall_image_additional_width', '74', 0),
(6665, 0, 'theme_tech_egomall', 'theme_tech_egomall_image_product_height', '228', 0),
(6666, 0, 'theme_tech_egomall', 'theme_tech_egomall_image_product_width', '228', 0),
(6667, 0, 'theme_tech_egomall', 'theme_tech_egomall_image_popup_height', '500', 0),
(6668, 0, 'theme_tech_egomall', 'theme_tech_egomall_image_popup_width', '500', 0),
(6669, 0, 'theme_tech_egomall', 'theme_tech_egomall_image_thumb_height', '228', 0),
(6670, 0, 'theme_tech_egomall', 'theme_tech_egomall_image_thumb_width', '228', 0),
(6671, 0, 'theme_tech_egomall', 'theme_tech_egomall_image_category_height', '80', 0),
(6672, 0, 'theme_tech_egomall', 'theme_tech_egomall_image_category_width', '80', 0),
(6673, 0, 'theme_tech_egomall', 'theme_tech_egomall_product_description_length', '100', 0),
(6674, 0, 'theme_tech_egomall', 'theme_tech_egomall_product_limit', '15', 0),
(6675, 0, 'theme_tech_egomall', 'theme_tech_egomall_status', '1', 0),
(6676, 0, 'theme_tech_egomall', 'theme_tech_egomall_directory', 'tech_egomall', 0),
(6677, 0, 'theme_tech_egomall', 'theme_tech_egomall_image_cart_width', '47', 0),
(6678, 0, 'theme_tech_egomall', 'theme_tech_egomall_image_cart_height', '47', 0),
(6679, 0, 'theme_tech_egomall', 'theme_tech_egomall_image_location_width', '268', 0),
(6680, 0, 'theme_tech_egomall', 'theme_tech_egomall_image_location_height', '50', 0),
(6681, 0, 'config', 'config_packet_trial_start', '2019-08-01', 0),
(6682, 0, 'config', 'config_packet_trial_period', '15', 0),
(6683, 0, 'config', 'config_packet_paid', '1', 0),
(6684, 0, 'config', 'config_packet_paid_start', '2019-08-01', 0),
(6685, 0, 'config', 'config_packet_paid_type', 'advance', 0),
(6686, 0, 'config', 'config_packet_paid_end', '2021-08-01', 0),
(6688, 0, 'config', 'shopee_config', '{\"sync_name\":1,\"sync_desc\":1,\"sync_price\":1,\"sync_quantity\":1,\"sync_weight\":1}', 0),
(6705, 0, 'module_age_restriction', 'module_age_restriction_status', '1', 0),
(6691, 0, 'theme_fashion_shoezone', 'theme_fashion_shoezone_app_config_12', '{\r\n   \"page\": 1,\r\n   \"position\": {\r\n       \"before\": \"bestme_el_1\",\r\n       \"after\": \"bestme_el_2\",\r\n       \"sort_order\": 3\r\n   }\r\n}', 0),
(6710, 0, 'app_flash_sale', 'app_flash_sale_status', '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_shipping_courier`
--

CREATE TABLE `oc_shipping_courier` (
  `shipping_courier_id` int(11) NOT NULL,
  `shipping_courier_code` varchar(255) NOT NULL DEFAULT '',
  `shipping_courier_name` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_shopee_product`
--

CREATE TABLE `oc_shopee_product` (
  `id` int(11) NOT NULL,
  `item_id` bigint(64) NOT NULL,
  `shopid` bigint(64) NOT NULL,
  `sku` varchar(64) DEFAULT NULL,
  `stock` int(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `images` text NOT NULL,
  `currency` varchar(32) NOT NULL,
  `price` float(15,4) NOT NULL,
  `original_price` float(15,4) NOT NULL,
  `weight` float(15,4) NOT NULL,
  `category_id` bigint(64) NOT NULL,
  `has_variation` tinyint(1) NOT NULL DEFAULT '0',
  `variations` text,
  `attributes` text,
  `bestme_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_statistics`
--

CREATE TABLE `oc_statistics` (
  `statistics_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `value` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_statistics`
--

INSERT INTO `oc_statistics` (`statistics_id`, `code`, `value`) VALUES
(1, 'order_sale', '0.0000'),
(2, 'order_processing', '0.0000'),
(3, 'order_complete', '0.0000'),
(4, 'order_other', '0.0000'),
(5, 'returns', '0.0000'),
(6, 'product', '0.0000'),
(7, 'review', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `oc_stock_status`
--

CREATE TABLE `oc_stock_status` (
  `stock_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_stock_status`
--

INSERT INTO `oc_stock_status` (`stock_status_id`, `language_id`, `name`) VALUES
(7, 1, 'In Stock'),
(8, 1, 'Pre-Order'),
(5, 1, 'Out Of Stock'),
(6, 1, '2-3 Days'),
(7, 2, 'In Stock'),
(8, 2, 'Pre-Order'),
(5, 2, 'Out Of Stock'),
(6, 2, '2-3 Days');

-- --------------------------------------------------------

--
-- Table structure for table `oc_store`
--

CREATE TABLE `oc_store` (
  `store_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  `ssl` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_store`
--

INSERT INTO `oc_store` (`store_id`, `name`, `url`, `ssl`) VALUES
(0, 'Kho trung tâm', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_store_receipt`
--

CREATE TABLE `oc_store_receipt` (
  `store_receipt_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `store_receipt_code` varchar(50) DEFAULT NULL,
  `total` decimal(18,4) DEFAULT NULL,
  `total_to_pay` decimal(18,4) DEFAULT NULL,
  `total_paid` decimal(18,4) DEFAULT NULL,
  `discount` decimal(18,4) DEFAULT NULL,
  `allocation_criteria_type` varchar(50) DEFAULT NULL,
  `allocation_criteria_total` decimal(18,4) DEFAULT NULL,
  `allocation_criteria_fees` mediumtext,
  `owed` decimal(18,4) DEFAULT NULL,
  `comment` text,
  `status` int(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_store_receipt_to_product`
--

CREATE TABLE `oc_store_receipt_to_product` (
  `store_receipt_product_id` int(11) NOT NULL,
  `store_receipt_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_version_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `cost_price` decimal(18,4) DEFAULT NULL,
  `fee` decimal(18,4) DEFAULT NULL,
  `discount` decimal(18,4) DEFAULT NULL,
  `total` decimal(18,4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_tag`
--

CREATE TABLE `oc_tag` (
  `tag_id` int(11) NOT NULL,
  `value` varchar(500) NOT NULL,
  `sort_order` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_class`
--

CREATE TABLE `oc_tax_class` (
  `tax_class_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_class`
--

INSERT INTO `oc_tax_class` (`tax_class_id`, `title`, `description`, `date_added`, `date_modified`) VALUES
(9, 'Taxable Goods', 'Taxed goods', '2009-01-06 23:21:53', '2011-09-23 14:07:50'),
(10, 'Downloadable Products', 'Downloadable', '2011-09-21 22:19:39', '2011-09-22 10:27:36');

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rate`
--

CREATE TABLE `oc_tax_rate` (
  `tax_rate_id` int(11) NOT NULL,
  `geo_zone_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  `rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `type` char(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rate`
--

INSERT INTO `oc_tax_rate` (`tax_rate_id`, `geo_zone_id`, `name`, `rate`, `type`, `date_added`, `date_modified`) VALUES
(86, 3, 'VAT (20%)', '20.0000', 'P', '2011-03-09 21:17:10', '2011-09-22 22:24:29'),
(87, 3, 'Eco Tax (-2.00)', '2.0000', 'F', '2011-09-21 21:49:23', '2011-09-23 00:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rate_to_customer_group`
--

CREATE TABLE `oc_tax_rate_to_customer_group` (
  `tax_rate_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rate_to_customer_group`
--

INSERT INTO `oc_tax_rate_to_customer_group` (`tax_rate_id`, `customer_group_id`) VALUES
(86, 1),
(87, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rule`
--

CREATE TABLE `oc_tax_rule` (
  `tax_rule_id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `based` varchar(10) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rule`
--

INSERT INTO `oc_tax_rule` (`tax_rule_id`, `tax_class_id`, `tax_rate_id`, `based`, `priority`) VALUES
(121, 10, 86, 'payment', 1),
(120, 10, 87, 'store', 0),
(128, 9, 86, 'shipping', 1),
(127, 9, 87, 'shipping', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_theme`
--

CREATE TABLE `oc_theme` (
  `theme_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `theme` varchar(64) NOT NULL,
  `route` varchar(64) NOT NULL,
  `code` mediumtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_theme_builder_config`
--

CREATE TABLE `oc_theme_builder_config` (
  `id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `config_theme` varchar(128) NOT NULL,
  `code` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `config` mediumtext NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_theme_builder_config`
--

INSERT INTO `oc_theme_builder_config` (`id`, `store_id`, `config_theme`, `code`, `key`, `config`, `date_added`, `date_modified`) VALUES
(340, 0, 'fashion_shoezone', 'config', 'config_theme_color', '{\n    \"main\": {\n        \"main\": {\n            \"hex\": \"#AB3737\",\n            \"visible\": 1\n        },\n        \"button\": {\n            \"hex\": \"#AB3737\",\n            \"visible\": 1\n        }\n    }\n}', '2019-05-16 18:24:39', '2019-06-05 15:11:28'),
(341, 0, 'fashion_shoezone', 'config', 'config_theme_text', '{\n    \"all\": {\n        \"title\": \"Font Tahoma\",\n        \"font\": \"Tahoma\",\n        \"supported-fonts\": [\n            \"Open Sans\",\n            \"IBM Plex Sans\",\n            \"Courier New\",\n            \"Roboto\",\n            \"Nunito\",\n            \"Arial\",\n            \"DejaVu Sans\",\n            \"Tahoma\",\n            \"Time News Roman\"\n        ]\n    },\n    \"heading\": {\n        \"font\": \"Tahoma\",\n        \"supported-fonts\": [\n            \"Open Sans\",\n            \"IBM Plex Sans\",\n            \"Courier New\",\n            \"Roboto\",\n            \"Nunito\",\n            \"Arial\",\n            \"DejaVu Sans\",\n            \"Tahoma\",\n            \"Time News Roman\"\n        ],\n        \"font-type\": \"regular\",\n        \"supported-font-types\": [\n            \"regular\",\n            \"bold\",\n            \"italic\"\n        ],\n        \"font-size\": 32\n    },\n    \"body\": {\n        \"font\": \"Aria\",\n        \"supported-fonts\": [\n            \"Open Sans\",\n            \"IBM Plex Sans\",\n            \"Courier New\",\n            \"Roboto\",\n            \"Nunito\",\n            \"Arial\",\n            \"DejaVu Sans\",\n            \"Tahoma\",\n            \"Time News Roman\"\n        ],\n        \"font-type\": \"regular\",\n        \"supported-font-types\": [\n            \"regular\",\n            \"bold\",\n            \"italic\"\n        ],\n        \"font-size\": 14\n    }\n}', '2019-05-16 18:24:39', '2019-06-05 15:11:28'),
(342, 0, 'fashion_shoezone', 'config', 'config_theme_favicon', '{\n    \"image\": {\n        \"url\": \"/catalog/view/theme/default/image/fashion_shoeszone/favicon.png\"\n    }\n}', '2019-05-16 18:24:39', '2019-06-05 15:11:28'),
(343, 0, 'fashion_shoezone', 'config', 'config_theme_social', '[\n    {\n        \"name\": \"facebook\",\n        \"text\": \"Facebook\",\n        \"url\": \"https://www.facebook.com\"\n    },\n    {\n        \"name\": \"twitter\",\n        \"text\": \"Twitter\",\n        \"url\": \"https://www.twitter.com\"\n    },\n    {\n        \"name\": \"instagram\",\n        \"text\": \"Instagram\",\n        \"url\": \"https://www.instagram.com\"\n    },\n    {\n        \"name\": \"tumblr\",\n        \"text\": \"Tumblr\",\n        \"url\": \"https://www.tumblr.com\"\n    },\n    {\n        \"name\": \"youtube\",\n        \"text\": \"Youtube\",\n        \"url\": \"https://www.youtube.com\"\n    },\n    {\n        \"name\": \"googleplus\",\n        \"text\": \"Google Plus\",\n        \"url\": \"https://www.plus.google.com\"\n    }\n]', '2019-05-16 18:24:39', '2019-06-05 15:11:28'),
(344, 0, 'fashion_shoezone', 'config', 'config_theme_checkout_page', '{\n}', '2019-05-16 18:24:39', '2019-06-05 15:11:28'),
(345, 0, 'fashion_shoezone', 'config', 'config_section_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"header\": {\n            \"name\": \"header\",\n            \"text\": \"Header\",\n            \"visible\": \"1\"\n        },\n        \"slide-show\": {\n            \"name\": \"slide-show\",\n            \"text\": \"Slideshow\",\n            \"visible\": \"1\"\n        },\n        \"categories\": {\n            \"name\": \"categories\",\n            \"text\": \"Danh mục sản phẩm\",\n            \"visible\": \"1\"\n        },\n        \"hot-deals\": {\n            \"name\": \"hot-deals\",\n            \"text\": \"Sản phẩm khuyến mại\",\n            \"visible\": \"1\"\n        },\n        \"feature-products\": {\n            \"name\": \"feature-products\",\n            \"text\": \"Sản phẩm bán chạy\",\n            \"visible\": \"1\"\n        },\n        \"related-products\": {\n            \"name\": \"related-products\",\n            \"text\": \"Sản phẩm xem nhiều\",\n            \"visible\": \"1\"\n        },\n        \"new-products\": {\n            \"name\": \"new-products\",\n            \"text\": \"Sản phẩm mới\",\n            \"visible\": \"1\"\n        },\n        \"product-detail\": {\n            \"name\": \"product-details\",\n            \"text\": \"Chi tiết sản phẩm\",\n            \"visible\": \"1\"\n        },\n        \"banner\": {\n            \"name\": \"banner\",\n            \"text\": \"Banner\",\n            \"visible\": \"1\"\n        },\n        \"partners\": {\n            \"name\": \"partners\",\n            \"text\": \"Đối tác\",\n            \"visible\": \"1\"\n        },\n        \"blog\": {\n            \"name\": \"blog\",\n            \"text\": \"Blog\",\n            \"visible\": \"1\"\n        },\n        \"footer\": {\n            \"name\": \"footer\",\n            \"text\": \"Footer\",\n            \"visible\": \"1\"\n        }\n    }\n}', '2019-05-16 18:24:39', '2019-06-05 15:12:38'),
(346, 0, 'fashion_shoezone', 'config', 'config_section_banner', '{\n    \"name\": \"banner\",\n    \"text\": \"Banner\",\n    \"visible\": 1,\n    \"display\": [\n        {\n            \"image-url\": \"/catalog/view/theme/default/image/fashion_shoeszone/banner_home/banner_1.png\",\n            \"url\": \"#\",\n            \"description\": \"Banner 1\",\n            \"visible\": 1\n        },\n        {\n            \"image-url\": \"/catalog/view/theme/default/image/fashion_shoeszone/banner_home/banner_2.png\",\n            \"url\": \"#\",\n            \"description\": \"Banner 2\",\n            \"visible\": 1\n        },\n        {\n            \"image-url\": \"/catalog/view/theme/default/image/fashion_shoeszone/banner_home/banner_3.png\",\n            \"url\": \"#\",\n            \"description\": \"Banner 3\",\n            \"visible\": 1\n        }\n    ]\n}', '2019-05-16 18:24:39', '2019-06-05 15:11:28'),
(347, 0, 'fashion_shoezone', 'config', 'config_section_best_sales_product', '{\n    \"name\": \"feature-product\",\n    \"text\": \"Sản phẩm bán chạy\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Sản phẩm bán chạy\",\n        \"auto_retrieve_data\": 1,\n        \"collection_id\": 1\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 4\n        },\n        \"grid_mobile\": {\n            \"quantity\": 2\n        }\n    }\n}', '2019-05-16 18:24:39', '2019-06-05 15:11:28'),
(348, 0, 'fashion_shoezone', 'config', 'config_section_new_product', '{\n    \"name\": \"new-product\",\n    \"text\": \"Sản phẩm mới\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Sản phẩm mới\",\n        \"auto_retrieve_data\": 1,\n        \"collection_id\": 1\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 4\n        },\n        \"grid_mobile\": {\n            \"quantity\": 2\n        }\n    }\n}', '2019-05-16 18:24:39', '2019-06-05 15:11:28'),
(349, 0, 'fashion_shoezone', 'config', 'config_section_best_views_product', '{\n    \"name\": \"related-product\",\n    \"text\": \"Sản phẩm xem nhiều\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Sản phẩm xem nhiều\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 10\n        },\n        \"grid_mobile\": {\n            \"quantity\": 2\n        }\n    }\n}', '2019-05-16 18:24:39', '2019-06-05 15:11:28'),
(350, 0, 'fashion_shoezone', 'config', 'config_section_blog', '{\n    \"name\": \"blog\",\n    \"text\": \"Blog\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Blog\"\n    },\n    \"display\": {\n        \"menu\": [\n            {\n                \"type\": \"existing\",\n                \"menu\": {\n                    \"id\": 12,\n                    \"name\": \"Danh sách bài viết 1\"\n                }\n            },\n            {\n                \"type\": \"manual\",\n                \"entries\": [\n                    {\n                        \"id\": 10,\n                        \"name\": \"Entry 10\"\n                    },\n                    {\n                        \"id\": 11,\n                        \"name\": \"Entry 11\"\n                    }\n                ]\n            }\n        ],\n        \"grid\": {\n            \"quantity\": 10\n        }\n    }\n}', '2019-05-16 18:24:39', '2019-06-05 15:11:28'),
(351, 0, 'fashion_shoezone', 'config', 'config_section_detail_product', '{\n    \"name\": \"product-detail\",\n    \"text\": \"Chi tiết sản phẩm\",\n    \"visible\": 1,\n    \"display\": {\n        \"name\": true,\n        \"description\": false,\n        \"price\": true,\n        \"price-compare\": false,\n        \"status\": false,\n        \"sale\": false,\n        \"rate\": false\n    }\n}', '2019-05-16 18:24:39', '2019-06-05 15:11:28'),
(352, 0, 'fashion_shoezone', 'config', 'config_section_footer', '{\n    \"name\": \"footer\",\n    \"text\": \"Footer\",\n    \"visible\": 1,\n    \"contact\": {\n        \"title\": \"Liên hệ chúng tôi\",\n        \"address\": \"Cau Giay - Ha Noi\",\n        \"phone-number\": \"(+84)987654321\",\n        \"email\": \"contact-us@xshop.com\",\n        \"visible\": 1\n    },\n    \"collection\": {\n        \"title\": \"Bộ sưu tập\",\n        \"menu_id\": 1,\n        \"visible\": 1\n    },\n    \"quick-links\": {\n        \"title\": \"Liên kết nhanh\",\n        \"menu_id\": 1,\n        \"visible\": 1\n    },\n    \"subscribe\": {\n        \"title\": \"Đăng ký theo dõi\",\n        \"social_network\": 1,\n        \"visible\": 1\n    }\n}', '2019-05-16 18:24:39', '2019-06-05 15:11:28'),
(353, 0, 'fashion_shoezone', 'config', 'config_section_header', '{\n    \"name\": \"header\",\n    \"text\": \"Header\",\n    \"visible\": 1,\n    \"notify-bar\": {\n        \"name\": \"Thanh thông báo\",\n        \"visible\": 1,\n        \"content\": \"Mua sắm online thuận tiện và dễ dàng\",\n        \"url\": \"#\"\n    },\n    \"logo\": {\n        \"name\": \"Logo\",\n        \"url\": \"/catalog/view/theme/default/image/fashion_shoeszone/logo.png\"\n    },\n    \"menu\": {\n        \"name\": \"Menu\",\n        \"display-list\": {\n            \"name\": \"Menu chính\",\n            \"id\": 1\n        }\n    }\n}', '2019-05-16 18:24:39', '2019-06-05 15:11:28'),
(354, 0, 'fashion_shoezone', 'config', 'config_section_hot_product', '{\n    \"name\": \"hot-deals\",\n    \"text\": \"Sản phẩm khuyến mại\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Sản phẩm khuyến mại\",\n        \"auto_retrieve_data\": 1,\n        \"collection_id\": 1\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 4\n        },\n        \"grid_mobile\": {\n            \"quantity\": 2\n        }\n    }\n}', '2019-05-16 18:24:39', '2019-06-05 15:11:28'),
(355, 0, 'fashion_shoezone', 'config', 'config_section_list_product', '{\n    \"name\": \"categories\",\n    \"text\": \"Danh mục sản phẩm\",\n    \"visible\": 0,\n    \"setting\": {\n        \"title\": \"Danh mục sản phẩm\"\n    },\n    \"display\": {\n        \"menu\": {\n            \"id\": 1,\n            \"name\": \"Danh mục sản phẩm 1\"\n        }\n    }\n}', '2019-05-16 18:24:39', '2019-06-05 15:11:28'),
(356, 0, 'fashion_shoezone', 'config', 'config_section_partner', '{\n    \"name\": \"partners\",\n    \"text\": \"Đối tác\",\n    \"visible\": 0,\n    \"limit-per-line\": 4,\n    \"display\": [\n        {\n            \"image-url\": \"/catalog/view/theme/default/image/fashion_shoeszone/partners/partner_1.jpg\",\n            \"url\": \"#\",\n            \"description\": \"Partner 1\",\n            \"visible\": 1\n        },\n        {\n            \"image-url\": \"/catalog/view/theme/default/image/fashion_shoeszone/partners/partner_2.jpg\",\n            \"url\": \"#\",\n            \"description\": \"Partner 2\",\n            \"visible\": 1\n        },\n        {\n            \"image-url\": \"/catalog/view/theme/default/image/fashion_shoeszone/partners/partner_3.jpg\",\n            \"url\": \"#\",\n            \"description\": \"Partner 3\",\n            \"visible\": 1\n        },\n        {\n            \"image-url\": \"/catalog/view/theme/default/image/fashion_shoeszone/partners/partner_4.jpg\",\n            \"url\": \"#\",\n            \"description\": \"Partner 4\",\n            \"visible\": 1\n        }\n    ]\n}', '2019-05-16 18:24:39', '2019-06-05 15:11:28'),
(357, 0, 'fashion_shoezone', 'config', 'config_section_slideshow', '{\n    \"name\": \"slide-show\",\n    \"text\": \"Slideshow\",\n    \"visible\": 1,\n    \"setting\": {\n        \"transition-time\": 5\n    },\n    \"display\": [\n        {\n            \"image-url\": \"/catalog/view/theme/default/image/fashion_shoeszone/slideshow/slide_1.png\",\n            \"url\": \"#\",\n            \"description\": \"slide_1\"\n        },\n        {\n            \"image-url\": \"/catalog/view/theme/default/image/fashion_shoeszone/slideshow/slide_2.png\",\n            \"url\": \"#\",\n            \"description\": \"slide_2\"\n        },\n        {\n            \"image-url\": \"/catalog/view/theme/default/image/fashion_shoeszone/slideshow/slide_3.png\",\n            \"url\": \"#\",\n            \"description\": \"slide_3\"\n        }\n    ]\n}', '2019-05-16 18:24:40', '2019-06-05 15:11:28'),
(358, 0, 'fashion_shoezone', 'config', 'config_section_category_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"banner\": {\n            \"name\": \"banner\",\n            \"text\": \"Banner\",\n            \"visible\": 1\n        },\n        \"filter\": {\n            \"name\": \"filter\",\n            \"text\": \"Filter\",\n            \"visible\": 1\n        },\n        \"product_category\": {\n            \"name\": \"product_category\",\n            \"text\": \"Product Category\",\n            \"visible\": 1\n        },\n        \"product_list\": {\n            \"name\": \"product_list\",\n            \"text\": \"Product List\",\n            \"visible\": 1\n        }\n    }\n}', '2019-05-16 18:24:40', '2019-06-05 15:11:28'),
(359, 0, 'fashion_shoezone', 'config', 'config_section_category_banner', '{\n    \"name\": \"banner\",\n    \"text\": \"Banner\",\n    \"visible\": 0,\n    \"display\": [\n        {\n            \"image-url\": \"/catalog/view/theme/default/image/fashion_shoeszone/banner_home/banner_1.png\",\n            \"url\": \"#\",\n            \"description\": \"Banner top\",\n            \"visible\": 1\n        },\n        {\n            \"image-url\": \"/catalog/view/theme/default/image/fashion_shoeszone/banner_home/banner_2.png\",\n            \"url\": \"#\",\n            \"description\": \"Banner left\",\n            \"visible\": 1\n        },\n        {\n            \"image-url\": \"/catalog/view/theme/default/image/fashion_shoeszone/banner_home/banner_3.png\",\n            \"url\": \"#\",\n            \"description\": \"Banner bottom\",\n            \"visible\": 1\n        }\n    ]\n}', '2019-05-16 18:24:40', '2019-06-05 15:11:28'),
(360, 0, 'fashion_shoezone', 'config', 'config_section_category_filter', '{\n    \"name\": \"filter\",\n    \"text\": \"Bộ lọc\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Bộ lọc\"\n    },\n    \"display\": {\n        \"supplier\": {\n            \"title\": \"Nhà cung cấp\",\n            \"visible\": 1\n        },\n        \"product-type\": {\n            \"title\": \"Loại sản phẩm\",\n            \"visible\": 1\n        },\n        \"collection\": {\n            \"title\": \"Bộ sưu tập\",\n            \"visible\": 1\n        },\n        \"property\": {\n            \"title\": \"Lọc theo tt - k dung\",\n            \"visible\": 1,\n            \"prop\": [\n                \"all\",\n                \"color\",\n                \"weight\",\n                \"size\"\n            ]\n        },\n        \"product-price\": {\n            \"title\": \"Giá sản phẩm\",\n            \"visible\": 1,\n            \"range\": {\n                \"from\": 0,\n                \"to\": 100000000\n            }\n        }\n    }\n}', '2019-05-16 18:24:40', '2019-06-05 15:11:28'),
(361, 0, 'fashion_shoezone', 'config', 'config_section_category_product_category', '{\n    \"name\": \"product-category\",\n    \"text\": \"Danh mục sản phẩm\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Danh mục sản phẩm\"\n    },\n    \"display\": {\n        \"menu\": {\n            \"id\": 1,\n            \"name\": \"Danh mục sản phẩm 1\"\n        }\n    }\n}', '2019-05-16 18:24:40', '2019-06-05 15:11:28'),
(362, 0, 'fashion_shoezone', 'config', 'config_section_category_product_list', '{\n    \"name\": \"product-list\",\n    \"text\": \"Danh sách sản phẩm\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Danh sách sản phẩm\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 10\n        }\n    }\n}', '2019-05-16 18:24:40', '2019-06-05 15:11:28'),
(363, 0, 'fashion_shoezone', 'config', 'config_section_product_detail_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"related_product\": {\n            \"name\": \"related_product\",\n            \"text\": \"Related Product\",\n            \"visible\": 1\n        },\n        \"template\": {\n            \"name\": \"template\",\n            \"text\": \"Template\",\n            \"visible\": 1\n        }\n    }\n}', '2019-05-16 18:24:40', '2019-06-05 15:11:28'),
(364, 0, 'fashion_shoezone', 'config', 'config_section_product_detail_related_product', '{\n    \"name\": \"related-product\",\n    \"text\": \"Sản phẩm liên quan\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Sản phẩm liên quan\",\n        \"auto_retrieve_data\": 0,\n        \"collection_id\": 1\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 4\n        }\n    }\n}', '2019-05-16 18:24:40', '2019-06-05 15:11:28'),
(365, 0, 'fashion_shoezone', 'config', 'config_section_product_detail_template', '{\n    \"name\": \"template\",\n    \"text\": \"Giao diện\",\n    \"visible\": 1,\n    \"display\": {\n        \"template\": {\n            \"id\": 10\n        }\n    }\n}', '2019-05-16 18:24:40', '2019-06-05 15:11:28'),
(366, 0, 'fashion_shoezone', 'config', 'config_section_blog_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"blog_category\": {\n            \"name\": \"blog_category\",\n            \"text\": \"Blog Category\",\n            \"visible\": 1\n        },\n        \"blog_list\": {\n            \"name\": \"blog_list\",\n            \"text\": \"Blog List\",\n            \"visible\": 1\n        },\n        \"latest_blog\": {\n            \"name\": \"latest_blog\",\n            \"text\": \"Latest Blog\",\n            \"visible\": 1\n        }\n    }\n}', '2019-05-16 18:24:40', '2019-06-05 15:11:28'),
(367, 0, 'fashion_shoezone', 'config', 'config_section_blog_blog_category', '{\n    \"name\": \"blog-category\",\n    \"text\": \"Danh mục bài viết\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Danh mục bài viết\"\n    },\n    \"display\": {\n        \"menu\": {\n            \"id\": 1,\n            \"name\": \"Danh mục bài viết 1\"\n        }\n    }\n}', '2019-05-16 18:24:40', '2019-06-05 15:11:28'),
(368, 0, 'fashion_shoezone', 'config', 'config_section_blog_blog_list', '{\n    \"name\": \"blog-list\",\n    \"text\": \"Danh sách bài viết\",\n    \"visible\": 1,\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 20\n        }\n    }\n}', '2019-05-16 18:24:40', '2019-06-05 15:11:28'),
(369, 0, 'fashion_shoezone', 'config', 'config_section_blog_latest_blog', '{\n    \"name\": \"latest-blog\",\n    \"text\": \"Bài viết mới nhất\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Bài viết mới nhất\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 10\n        }\n    }\n}', '2019-05-16 18:24:40', '2019-06-05 15:11:28'),
(370, 0, 'fashion_shoezone', 'config', 'config_section_contact_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"map\": {\n            \"name\": \"map\",\n            \"text\": \"Bản đồ\",\n            \"address\": \"<iframe src=\\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\\" width=\\\"600\\\" height=\\\"450\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\" allowfullscreen></iframe>\",\n            \"visible\": 1\n        },\n        \"info\": {\n            \"name\": \"info\",\n            \"text\": \"Thông tin cửa hàng\",\n            \"email\": \"contact-us@novaon.asia\",\n            \"phone\": \"0000000000\",\n            \"address\": \"address\",\n            \"visible\": 1\n        },\n        \"form\": {\n            \"name\": \"form\",\n            \"title\": \"From liên hệ\",\n            \"email\": \"email@mail.com\",\n            \"visible\": 1\n        }\n    }\n}', '2019-05-16 18:24:40', '2019-06-05 15:11:28'),
(371, 0, 'fashion_shoezone', 'config', 'config_section_contact_map', '{   //////// CÁI NÀY KHÔNG DÙNG NỮA\n    \"name\": \"map\",\n    \"text\": \"Bản đồ\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Bản đồ\"\n    },\n    \"display\": {\n        \"address\": \"<iframe src=\\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\\" width=\\\"600\\\" height=\\\"450\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\" allowfullscreen></iframe>\"\n    }\n}', '2019-05-16 18:24:40', '2019-06-05 15:11:28'),
(372, 0, 'fashion_shoezone', 'config', 'config_section_contact_contact', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\n    \"name\": \"contact\",\n    \"text\": \"Liên hệ\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Vị trí của chúng tôi\"\n    },\n    \"display\": {\n        \"store\": {\n            \"title\": \"Gian hàng\",\n            \"content\": \"Số 12 Dịch Vọng Hậu, Cầu Giấy, Hà Nội\",\n            \"visible\": 1\n        },\n        \"telephone\": {\n            \"title\": \"Điện thoại\",\n            \"content\": \"(+84) 987 654 321\",\n            \"visible\": 1\n        },\n        \"social_follow\": {\n            \"socials\": [\n                {\n                    \"name\": \"facebook\",\n                    \"text\": \"Facebook\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"twitter\",\n                    \"text\": \"Twitter\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"instagram\",\n                    \"text\": \"Instagram\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"tumblr\",\n                    \"text\": \"Tumblr\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"youtube\",\n                    \"text\": \"Youtube\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"googleplus\",\n                    \"text\": \"Google Plus\",\n                    \"visible\": 1\n                }\n            ],\n            \"visible\": 1\n        }\n    }\n}', '2019-05-16 18:24:40', '2019-06-05 15:11:28'),
(373, 0, 'fashion_shoezone', 'config', 'config_section_contact_form', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\n    \"name\": \"form\",\n    \"text\": \"Biểu mẫu\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Liên hệ với chúng tôi\"\n    },\n    \"data\": {\n        \"email\": \"sale.247@xshop.com\"\n    }\n}', '2019-05-16 18:24:40', '2019-06-05 15:11:28'),
(374, 0, 'fashion_shoezone', 'config', 'config_section_product_groups', '{\n    \"name\": \"group products\",\n    \"text\": \"nhóm sản phẩm\",\n    \"visible\": \"1\",\n    \"list\": [\n        {\n            \"text\": \"Danh sách sản phẩm 1\",\n            \"visible\": \"0\",\n            \"setting\": {\n                \"title\": \"Danh sách sản phẩm 1\",\n                \"collection_id\": \"4\",\n                \"sub_title\": \"Danh sách sản phẩm 1\"\n            },\n            \"display\": {\n                \"grid\": {\n                    \"quantity\": \"6\"\n                },\n                \"grid_mobile\": {\n                    \"quantity\": 2\n                }\n            }\n        }\n    ]\n}', '2019-12-26 16:56:25', '2019-12-26 17:03:25'),
(375, 0, 'beauty_ap_cosmetic', 'config', 'config_theme_color', '{\n    \"main\": {\n        \"main\": {\n            \"hex\": \"#D0203F\",\n            \"visible\": 1\n        },\n        \"button\": {\n            \"hex\": \"#D0203F\",\n            \"visible\": 1\n        }\n    }\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(376, 0, 'beauty_ap_cosmetic', 'config', 'config_theme_text', '{\n    \"all\": {\n        \"title\": \"Font Tahoma\",\n        \"font\": \"Tahoma\",\n        \"supported-fonts\": [\n            \"Open Sans\",\n            \"IBM Plex Sans\",\n            \"Courier New\",\n            \"Roboto\",\n            \"Nunito\",\n            \"Arial\",\n            \"DejaVu Sans\",\n            \"Tahoma\",\n            \"Time News Roman\"\n        ]\n    },\n    \"heading\": {\n        \"font\": \"Tahoma\",\n        \"supported-fonts\": [\n            \"Open Sans\",\n            \"IBM Plex Sans\",\n            \"Courier New\",\n            \"Roboto\",\n            \"Nunito\",\n            \"Arial\",\n            \"DejaVu Sans\",\n            \"Tahoma\",\n            \"Time News Roman\"\n        ],\n        \"font-type\": \"regular\",\n        \"supported-font-types\": [\n            \"regular\",\n            \"bold\",\n            \"italic\"\n        ],\n        \"font-size\": 32\n    },\n    \"body\": {\n        \"font\": \"Aria\",\n        \"supported-fonts\": [\n            \"Open Sans\",\n            \"IBM Plex Sans\",\n            \"Courier New\",\n            \"Roboto\",\n            \"Nunito\",\n            \"Arial\",\n            \"DejaVu Sans\",\n            \"Tahoma\",\n            \"Time News Roman\"\n        ],\n        \"font-type\": \"regular\",\n        \"supported-font-types\": [\n            \"regular\",\n            \"bold\",\n            \"italic\"\n        ],\n        \"font-size\": 14\n    }\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(377, 0, 'beauty_ap_cosmetic', 'config', 'config_theme_favicon', '{\r\n    \"image\": {\r\n        \"url\": \"/catalog/view/theme/default/image/favicon.ico\"\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(378, 0, 'beauty_ap_cosmetic', 'config', 'config_theme_social', '[\r\n    {\r\n        \"name\": \"facebook\",\r\n        \"text\": \"Facebook\",\r\n        \"url\": \"https://www.facebook.com\"\r\n    },\r\n    {\r\n        \"name\": \"twitter\",\r\n        \"text\": \"Twitter\",\r\n        \"url\": \"https://www.twitter.com\"\r\n    },\r\n    {\r\n        \"name\": \"instagram\",\r\n        \"text\": \"Instagram\",\r\n        \"url\": \"https://www.instagram.com\"\r\n    },\r\n    {\r\n        \"name\": \"tumblr\",\r\n        \"text\": \"Tumblr\",\r\n        \"url\": \"https://www.tumblr.com\"\r\n    },\r\n    {\r\n        \"name\": \"youtube\",\r\n        \"text\": \"Youtube\",\r\n        \"url\": \"https://www.youtube.com\"\r\n    },\r\n    {\r\n        \"name\": \"googleplus\",\r\n        \"text\": \"Google Plus\",\r\n        \"url\": \"https://www.plus.google.com\"\r\n    }\r\n]', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(379, 0, 'beauty_ap_cosmetic', 'config', 'config_theme_override_css', '{\r\n    \"css\": \"/* override css */\"\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(380, 0, 'beauty_ap_cosmetic', 'config', 'config_theme_checkout_page', '{\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(381, 0, 'beauty_ap_cosmetic', 'config', 'config_section_sections', '{\r\n    \"name\": \"Theme Config\",\r\n    \"version\": \"1.0\",\r\n    \"section\": {\r\n        \"header\": {\r\n            \"name\": \"header\",\r\n            \"text\": \"Header\",\r\n            \"visible\": 1\r\n        },\r\n        \"slide-show\": {\r\n            \"name\": \"slide-show\",\r\n            \"text\": \"Slideshow\",\r\n            \"visible\": 1\r\n        },\r\n        \"categories\": {\r\n            \"name\": \"categories\",\r\n            \"text\": \"Danh mục sản phẩm\",\r\n            \"visible\": 1\r\n        },\r\n        \"hot-deals\": {\r\n            \"name\": \"hot-deals\",\r\n            \"text\": \"Sản phẩm khuyến mại\",\r\n            \"visible\": 1\r\n        },\r\n        \"feature-products\": {\r\n            \"name\": \"feature-products\",\r\n            \"text\": \"Sản phẩm bán chạy\",\r\n            \"visible\": 1\r\n        },\r\n        \"related-products\": {\r\n            \"name\": \"related-products\",\r\n            \"text\": \"Sản phẩm xem nhiều\",\r\n            \"visible\": 1\r\n        },\r\n        \"new-products\": {\r\n            \"name\": \"new-products\",\r\n            \"text\": \"Sản phẩm mới\",\r\n            \"visible\": 1\r\n        },\r\n        \"product-detail\": {\r\n            \"name\": \"product-details\",\r\n            \"text\": \"Chi tiết sản phẩm\",\r\n            \"visible\": 1\r\n        },\r\n        \"banner\": {\r\n            \"name\": \"banner\",\r\n            \"text\": \"Banner\",\r\n            \"visible\": 1\r\n        },\r\n        \"content_customize\" : {\r\n            \"name\" : \"content-customize\",\r\n            \"text\" : \"Nội dung tùy chỉnh\",\r\n            \"visible\" : 1\r\n        },\r\n        \"partners\": {\r\n            \"name\": \"partners\",\r\n            \"text\": \"Đối tác\",\r\n            \"visible\": 1\r\n        },\r\n        \"blog\": {\r\n            \"name\": \"blog\",\r\n            \"text\": \"Blog\",\r\n            \"visible\": 1\r\n        },\r\n        \"footer\": {\r\n            \"name\": \"footer\",\r\n            \"text\": \"Footer\",\r\n            \"visible\": 1\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(382, 0, 'beauty_ap_cosmetic', 'config', 'config_section_banner', '{\r\n    \"name\": \"banner\",\r\n    \"text\": \"Banner\",\r\n    \"visible\": 1,\r\n    \"display\": [\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/banner/img-banner-06.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"Banner 1\",\r\n            \"visible\": 1\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/banner/img-banner-02.png\",\r\n            \"url\": \"#\",\r\n            \"description\": \"Banner 2\",\r\n            \"visible\": 1\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/banner/img-banner-03.png\",\r\n            \"url\": \"#\",\r\n            \"description\": \"Banner 3\",\r\n            \"visible\": 1\r\n        }\r\n    ]\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(383, 0, 'beauty_ap_cosmetic', 'config', 'config_section_best_sales_product', '{\r\n    \"name\": \"feature-product\",\r\n    \"text\": \"Sản phẩm bán chạy\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Sản phẩm bán chạy\",\r\n        \"sub_title\" : \"Mô tả tiêu đề\",\r\n        \"auto_retrieve_data\": 1,\r\n        \"collection_id\": 1\r\n    },\r\n    \"display\": {\r\n        \"grid\": {\r\n            \"quantity\": 4\r\n        },\r\n        \"grid_mobile\": {\r\n            \"quantity\": 2\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(384, 0, 'beauty_ap_cosmetic', 'config', 'config_section_new_product', '{\r\n    \"name\": \"new-product\",\r\n    \"text\": \"Sản phẩm mới\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Sản phẩm mới\",\r\n        \"sub_title\" : \"Mô tả tiêu đề\",\r\n        \"auto_retrieve_data\": 1,\r\n        \"collection_id\": 1\r\n    },\r\n    \"display\": {\r\n        \"grid\": {\r\n            \"quantity\": 4\r\n        },\r\n        \"grid_mobile\": {\r\n            \"quantity\": 2\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(385, 0, 'beauty_ap_cosmetic', 'config', 'config_section_best_views_product', '{\r\n    \"name\": \"related-product\",\r\n    \"text\": \"Sản phẩm xem nhiều\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Sản phẩm xem nhiều\"\r\n    },\r\n    \"display\": {\r\n        \"grid\": {\r\n            \"quantity\": 10\r\n        },\r\n        \"grid_mobile\": {\r\n            \"quantity\": 2\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(386, 0, 'beauty_ap_cosmetic', 'config', 'config_section_blog', '{\r\n    \"name\": \"blog\",\r\n    \"text\": \"Blog\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Blog\"\r\n    },\r\n    \"display\": {\r\n        \"menu\": [\r\n            {\r\n                \"type\": \"existing\",\r\n                \"menu\": {\r\n                    \"id\": 12,\r\n                    \"name\": \"Danh sách bài viết 1\"\r\n                }\r\n            },\r\n            {\r\n                \"type\": \"manual\",\r\n                \"entries\": [\r\n                    {\r\n                        \"id\": 10,\r\n                        \"name\": \"Entry 10\"\r\n                    },\r\n                    {\r\n                        \"id\": 11,\r\n                        \"name\": \"Entry 11\"\r\n                    }\r\n                ]\r\n            }\r\n        ],\r\n        \"grid\": {\r\n            \"quantity\": 10\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(387, 0, 'beauty_ap_cosmetic', 'config', 'config_section_detail_product', '{\r\n    \"name\": \"product-detail\",\r\n    \"text\": \"Chi tiết sản phẩm\",\r\n    \"visible\": 1,\r\n    \"display\": {\r\n        \"name\": true,\r\n        \"description\": false,\r\n        \"price\": true,\r\n        \"price-compare\": false,\r\n        \"status\": false,\r\n        \"sale\": false,\r\n        \"rate\": false\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(388, 0, 'beauty_ap_cosmetic', 'config', 'config_section_footer', '{\n    \"name\": \"footer\",\n    \"text\": \"Footer\",\n    \"visible\": 1,\n    \"contact\": {\n        \"title\": \"Liên hệ chúng tôi\",\n        \"address\": \"Cau Giay - Ha Noi\",\n        \"phone-number\": \"(+84)987654321\",\n        \"email\": \"dunght@novaon.asia\",\n        \"visible\": 1\n    },\n    \"collection\": {\n        \"title\": \"Bộ sưu tập\",\n        \"menu_id\": 1,\n        \"visible\": 1\n    },\n    \"quick-links\": {\n        \"title\": \"Liên kết nhanh\",\n        \"menu_id\": 1,\n        \"visible\": 1\n    },\n    \"subscribe\": {\n        \"title\": \"Đăng ký theo dõi\",\n        \"social_network\": 1,\n        \"visible\": 1,\n        \"youtube_visible\": 1,\n        \"facebook_visible\": 1,\n        \"instagram_visible\": 1\n    }\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(389, 0, 'beauty_ap_cosmetic', 'config', 'config_section_header', '{\r\n    \"name\": \"header\",\r\n    \"text\": \"Header\",\r\n    \"visible\": 1,\r\n    \"notify-bar\": {\r\n        \"name\": \"Thanh thông báo\",\r\n        \"visible\": 1,\r\n        \"content\": \"Mua sắm online thuận tiện và dễ dàng\",\r\n        \"url\": \"#\"\r\n    },\r\n    \"logo\": {\r\n        \"name\": \"Logo\",\r\n        \"url\": \"/catalog/view/theme/default/image/logo.png\",\r\n        \"height\": null\r\n    },\r\n    \"menu\": {\r\n        \"name\": \"Menu\",\r\n        \"display-list\": {\r\n            \"name\": \"Menu chính\",\r\n            \"id\": 1\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(390, 0, 'beauty_ap_cosmetic', 'config', 'config_section_hot_product', '{\r\n    \"name\": \"hot-deals\",\r\n    \"text\": \"Sản phẩm khuyến mại\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Sản phẩm khuyến mại\",\r\n        \"sub_title\" : \"Mô tả tiêu đề\",\r\n        \"auto_retrieve_data\": 1,\r\n        \"collection_id\": 1\r\n    },\r\n    \"display\": {\r\n        \"grid\": {\r\n            \"quantity\": 4\r\n        },\r\n        \"grid_mobile\": {\r\n            \"quantity\": 2\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(391, 0, 'beauty_ap_cosmetic', 'config', 'config_section_list_product', '{\r\n    \"name\": \"categories\",\r\n    \"text\": \"Danh mục sản phẩm\",\r\n    \"visible\": 0,\r\n    \"setting\": {\r\n        \"title\": \"Danh mục sản phẩm\"\r\n    },\r\n    \"display\": {\r\n        \"menu\": {\r\n            \"id\": 1,\r\n            \"name\": \"Danh mục sản phẩm 1\"\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(392, 0, 'beauty_ap_cosmetic', 'config', 'config_section_partner', '{\r\n    \"name\": \"partners\",\r\n    \"text\": \"Đối tác\",\r\n    \"visible\": 1,\r\n    \"limit-per-line\": 4,\r\n    \"display\": [\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/novaon/asset/img/partner_1.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"Partner 1\",\r\n            \"visible\": 1\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/novaon/asset/img/partner_2.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"Partner 2\",\r\n            \"visible\": 1\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/novaon/asset/img/partner_3.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"Partner 3\",\r\n            \"visible\": 1\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/novaon/asset/img/partner_4.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"Partner 4\",\r\n            \"visible\": 1\r\n        }\r\n    ]\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(393, 0, 'beauty_ap_cosmetic', 'config', 'config_section_slideshow', '{\r\n    \"name\": \"slide-show\",\r\n    \"text\": \"Slideshow\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"transition-time\": 5\r\n    },\r\n    \"display\": [\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/slideshow/slide_1.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"slide_1\"\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/slideshow/slide_2.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"slide_2\"\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/slideshow/slide_3.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"slide_3\"\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/slideshow/slide_4.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"slide_4\"\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/slideshow/slide_5.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"slide_5\"\r\n        }\r\n    ]\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(394, 0, 'beauty_ap_cosmetic', 'config', 'config_section_product_groups', '{\r\n    \"name\": \"group products\",\r\n    \"text\": \"nhóm sản phẩm\",\r\n    \"visible\": 1,\r\n    \"list\": [\r\n        {\r\n            \"text\": \"Danh sách sản phẩm 1\",\r\n            \"visible\": \"0\",\r\n            \"setting\": {\r\n                \"title\": \"Danh sách sản phẩm 1\",\r\n                \"collection_id\": \"4\",\r\n                \"sub_title\": \"Danh sách sản phẩm 1\"\r\n            },\r\n            \"display\": {\r\n                \"grid\": {\r\n                    \"quantity\": \"6\"\r\n                },\r\n                \"grid_mobile\": {\r\n                    \"quantity\": 2\r\n                }\r\n            }\r\n        }\r\n    ]\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(395, 0, 'beauty_ap_cosmetic', 'config', 'config_section_content_customize', '{\r\n    \"name\": \"content_customize\",\r\n    \"title\": \"Nội dung tuỳ chỉnh\",\r\n    \"display\": [\r\n        {\r\n            \"name\": \"content-1\",\r\n            \"title\": \"Bảo đảm chất lượng\",\r\n            \"type\": \"fixed\",\r\n            \"content\": {\r\n                \"icon\": \"/catalog/view/theme/default/image/favicon.ico\",\r\n                \"title\": \"Bảo đảm chất lượng\",\r\n                \"description\": \"Sản phẩm đảm bảo chất lượng\",\r\n                \"html\": \"\"\r\n            }\r\n        },\r\n        {\r\n            \"name\": \"content-2\",\r\n            \"title\": \"Miễn phí giao hàng\",\r\n            \"type\": \"fixed\",\r\n            \"content\": {\r\n                \"icon\": \"/catalog/view/theme/default/image/favicon.ico\",\r\n                \"title\": \"Miễn phí giao hàng\",\r\n                \"description\": \"Cho đơn hàng từ 2 triệu\",\r\n                \"html\": \"\"\r\n            }\r\n        },\r\n        {\r\n            \"name\": \"content-3\",\r\n            \"title\": \"Hỗ trợ 24/7\",\r\n            \"type\": \"fixed\",\r\n            \"content\": {\r\n                \"icon\": \"/catalog/view/theme/default/image/favicon.ico\",\r\n                \"title\": \"Hỗ trợ 24/7\",\r\n                \"description\": \"Hotline 012.345.678\",\r\n                \"html\": \"\"\r\n            }\r\n        },\r\n        {\r\n            \"name\": \"content-4\",\r\n            \"title\": \"Đổi trả hàng\",\r\n            \"type\": \"fixed\",\r\n            \"content\": {\r\n                \"icon\": \"/catalog/view/theme/default/image/favicon.ico\",\r\n                \"title\": \"Đổi trả hàng\",\r\n                \"description\": \"Trong vòng 7 ngày\",\r\n                \"html\": \"\"\r\n            }\r\n        }\r\n    ]\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(396, 0, 'beauty_ap_cosmetic', 'config', 'config_section_category_sections', '{\r\n    \"name\": \"Theme Config\",\r\n    \"version\": \"1.0\",\r\n    \"section\": {\r\n        \"banner\": {\r\n            \"name\": \"banner\",\r\n            \"text\": \"Banner\",\r\n            \"visible\": 1\r\n        },\r\n        \"filter\": {\r\n            \"name\": \"filter\",\r\n            \"text\": \"Filter\",\r\n            \"visible\": 1\r\n        },\r\n        \"product_category\": {\r\n            \"name\": \"product_category\",\r\n            \"text\": \"Product Category\",\r\n            \"visible\": 1\r\n        },\r\n        \"product_list\": {\r\n            \"name\": \"product_list\",\r\n            \"text\": \"Product List\",\r\n            \"visible\": 1\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(397, 0, 'beauty_ap_cosmetic', 'config', 'config_section_category_banner', '{\r\n    \"name\": \"banner\",\r\n    \"text\": \"Banner\",\r\n    \"visible\": 1,\r\n    \"display\": [\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/banner/img-banner-06.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"Banner 1\",\r\n            \"visible\": 1\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/banner/img-banner-02.png\",\r\n            \"url\": \"#\",\r\n            \"description\": \"Banner 2\",\r\n            \"visible\": 1\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/banner/img-banner-03.png\",\r\n            \"url\": \"#\",\r\n            \"description\": \"Banner 3\",\r\n            \"visible\": 1\r\n        }\r\n    ]\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(398, 0, 'beauty_ap_cosmetic', 'config', 'config_section_category_filter', '{\r\n    \"name\": \"filter\",\r\n    \"text\": \"Bộ lọc\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Bộ lọc\"\r\n    },\r\n    \"display\": {\r\n        \"supplier\": {\r\n            \"title\": \"Nhà cung cấp\",\r\n            \"visible\": 1\r\n        },\r\n        \"product-type\": {\r\n            \"title\": \"Loại sản phẩm\",\r\n            \"visible\": 1\r\n        },\r\n        \"collection\": {\r\n            \"title\": \"Bộ sưu tập\",\r\n            \"visible\": 1\r\n        },\r\n        \"property\": {\r\n            \"title\": \"Lọc theo tt - k dung\",\r\n            \"visible\": 1,\r\n            \"prop\": [\r\n                \"all\",\r\n                \"color\",\r\n                \"weight\",\r\n                \"size\"\r\n            ]\r\n        },\r\n        \"product-price\": {\r\n            \"title\": \"Giá sản phẩm\",\r\n            \"visible\": 1,\r\n            \"range\": {\r\n                \"from\": 0,\r\n                \"to\": 100000000\r\n            }\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(399, 0, 'beauty_ap_cosmetic', 'config', 'config_section_category_product_category', '{\r\n    \"name\": \"product-category\",\r\n    \"text\": \"Danh mục sản phẩm\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Danh mục sản phẩm\"\r\n    },\r\n    \"display\": {\r\n        \"menu\": {\r\n            \"id\": 1,\r\n            \"name\": \"Danh mục sản phẩm 1\"\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(400, 0, 'beauty_ap_cosmetic', 'config', 'config_section_category_product_list', '{\r\n    \"name\": \"product-list\",\r\n    \"text\": \"Danh sách sản phẩm\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Danh sách sản phẩm\"\r\n    },\r\n    \"display\": {\r\n        \"grid\": {\r\n            \"quantity\": 10\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(401, 0, 'beauty_ap_cosmetic', 'config', 'config_section_product_detail_sections', '{\r\n    \"name\": \"Theme Config\",\r\n    \"version\": \"1.0\",\r\n    \"section\": {\r\n        \"related_product\": {\r\n            \"name\": \"related_product\",\r\n            \"text\": \"Related Product\",\r\n            \"visible\": 1\r\n        },\r\n        \"template\": {\r\n            \"name\": \"template\",\r\n            \"text\": \"Template\",\r\n            \"visible\": 1\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(402, 0, 'beauty_ap_cosmetic', 'config', 'config_section_product_detail_related_product', '{\r\n    \"name\": \"related-product\",\r\n    \"text\": \"Sản phẩm liên quan\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Sản phẩm liên quan\",\r\n        \"auto_retrieve_data\": 0,\r\n        \"collection_id\": 1\r\n    },\r\n    \"display\": {\r\n        \"grid\": {\r\n            \"quantity\": 4\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(403, 0, 'beauty_ap_cosmetic', 'config', 'config_section_product_detail_template', '{\r\n    \"name\": \"template\",\r\n    \"text\": \"Giao diện\",\r\n    \"visible\": 1,\r\n    \"display\": {\r\n        \"template\": {\r\n            \"id\": 10\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(404, 0, 'beauty_ap_cosmetic', 'config', 'config_section_blog_sections', '{\r\n    \"name\": \"Theme Config\",\r\n    \"version\": \"1.0\",\r\n    \"section\": {\r\n        \"blog_category\": {\r\n            \"name\": \"blog_category\",\r\n            \"text\": \"Blog Category\",\r\n            \"visible\": 1\r\n        },\r\n        \"blog_list\": {\r\n            \"name\": \"blog_list\",\r\n            \"text\": \"Blog List\",\r\n            \"visible\": 1\r\n        },\r\n        \"latest_blog\": {\r\n            \"name\": \"latest_blog\",\r\n            \"text\": \"Latest Blog\",\r\n            \"visible\": 1\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(405, 0, 'beauty_ap_cosmetic', 'config', 'config_section_blog_blog_category', '{\r\n    \"name\": \"blog-category\",\r\n    \"text\": \"Danh mục bài viết\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Danh mục bài viết\"\r\n    },\r\n    \"display\": {\r\n        \"menu\": {\r\n            \"id\": 1,\r\n            \"name\": \"Danh mục bài viết 1\"\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(406, 0, 'beauty_ap_cosmetic', 'config', 'config_section_blog_blog_list', '{\r\n    \"name\": \"blog-list\",\r\n    \"text\": \"Danh sách bài viết\",\r\n    \"visible\": 1,\r\n    \"display\": {\r\n        \"grid\": {\r\n            \"quantity\": 20\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(407, 0, 'beauty_ap_cosmetic', 'config', 'config_section_blog_latest_blog', '{\r\n    \"name\": \"latest-blog\",\r\n    \"text\": \"Bài viết mới nhất\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Bài viết mới nhất\"\r\n    },\r\n    \"display\": {\r\n        \"grid\": {\r\n            \"quantity\": 10\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21');
INSERT INTO `oc_theme_builder_config` (`id`, `store_id`, `config_theme`, `code`, `key`, `config`, `date_added`, `date_modified`) VALUES
(408, 0, 'beauty_ap_cosmetic', 'config', 'config_section_contact_sections', '{\r\n    \"name\": \"Theme Config\",\r\n    \"version\": \"1.0\",\r\n    \"section\": {\r\n        \"map\": {\r\n            \"name\": \"map\",\r\n            \"text\": \"Bản đồ\",\r\n            \"address\": \"<iframe src=\\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\\" width=\\\"600\\\" height=\\\"450\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\" allowfullscreen></iframe>\",\r\n            \"visible\": 1\r\n        },\r\n        \"info\": {\r\n            \"name\": \"info\",\r\n            \"text\": \"Thông tin cửa hàng\",\r\n            \"email\": \"contact-us@novaon.asia\",\r\n            \"phone\": \"0000000000\",\r\n            \"address\": \"address\",\r\n            \"visible\": 1\r\n        },\r\n        \"form\": {\r\n            \"name\": \"form\",\r\n            \"title\": \"Form liên hệ\",\r\n            \"email\": \"email@mail.com\",\r\n            \"visible\": 1\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(409, 0, 'beauty_ap_cosmetic', 'config', 'config_section_contact_map', '{   //////// CÁI NÀY KHÔNG DÙNG NỮA\r\n    \"name\": \"map\",\r\n    \"text\": \"Bản đồ\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Bản đồ\"\r\n    },\r\n    \"display\": {\r\n        \"address\": \"<iframe src=\\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\\" width=\\\"600\\\" height=\\\"450\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\" allowfullscreen></iframe>\"\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(410, 0, 'beauty_ap_cosmetic', 'config', 'config_section_contact_contact', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\r\n    \"name\": \"contact\",\r\n    \"text\": \"Liên hệ\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Vị trí của chúng tôi\"\r\n    },\r\n    \"display\": {\r\n        \"store\": {\r\n            \"title\": \"Gian hàng\",\r\n            \"content\": \"Số 12 Dịch Vọng Hậu, Cầu Giấy, Hà Nội\",\r\n            \"visible\": 1\r\n        },\r\n        \"telephone\": {\r\n            \"title\": \"Điện thoại\",\r\n            \"content\": \"(+84) 987 654 321\",\r\n            \"visible\": 1\r\n        },\r\n        \"social_follow\": {\r\n            \"socials\": [\r\n                {\r\n                    \"name\": \"facebook\",\r\n                    \"text\": \"Facebook\",\r\n                    \"visible\": 1\r\n                },\r\n                {\r\n                    \"name\": \"twitter\",\r\n                    \"text\": \"Twitter\",\r\n                    \"visible\": 1\r\n                },\r\n                {\r\n                    \"name\": \"instagram\",\r\n                    \"text\": \"Instagram\",\r\n                    \"visible\": 1\r\n                },\r\n                {\r\n                    \"name\": \"tumblr\",\r\n                    \"text\": \"Tumblr\",\r\n                    \"visible\": 1\r\n                },\r\n                {\r\n                    \"name\": \"youtube\",\r\n                    \"text\": \"Youtube\",\r\n                    \"visible\": 1\r\n                },\r\n                {\r\n                    \"name\": \"googleplus\",\r\n                    \"text\": \"Google Plus\",\r\n                    \"visible\": 1\r\n                }\r\n            ],\r\n            \"visible\": 1\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(411, 0, 'beauty_ap_cosmetic', 'config', 'config_section_contact_form', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\r\n    \"name\": \"form\",\r\n    \"text\": \"Biểu mẫu\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Liên hệ với chúng tôi\"\r\n    },\r\n    \"data\": {\r\n        \"email\": \"sale.247@xshop.com\"\r\n    }\r\n}', '2020-04-16 13:41:21', '2020-04-16 13:41:21'),
(412, 0, 'beauty_ega_cosmetics', 'config', 'config_theme_color', '{\n    \"main\": {\n        \"main\": {\n            \"hex\": \"#81BE16\",\n            \"visible\": 1\n        },\n        \"button\": {\n            \"hex\": \"#F7941E\",\n            \"visible\": 1\n        }\n    }\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(413, 0, 'beauty_ega_cosmetics', 'config', 'config_theme_text', '{\n    \"all\": {\n        \"title\": \"Font Roboto\",\n        \"font\": \"Roboto\",\n        \"supported-fonts\": [\n            \"Open Sans\",\n            \"IBM Plex Sans\",\n            \"Courier New\",\n            \"Roboto\",\n            \"Nunito\",\n            \"Arial\",\n            \"DejaVu Sans\",\n            \"Tahoma\",\n            \"Time News Roman\"\n        ]\n    },\n    \"heading\": {\n        \"font\": \"Tahoma\",\n        \"supported-fonts\": [\n            \"Open Sans\",\n            \"IBM Plex Sans\",\n            \"Courier New\",\n            \"Roboto\",\n            \"Nunito\",\n            \"Arial\",\n            \"DejaVu Sans\",\n            \"Tahoma\",\n            \"Time News Roman\"\n        ],\n        \"font-type\": \"regular\",\n        \"supported-font-types\": [\n            \"regular\",\n            \"bold\",\n            \"italic\"\n        ],\n        \"font-size\": 32\n    },\n    \"body\": {\n        \"font\": \"Aria\",\n        \"supported-fonts\": [\n            \"Open Sans\",\n            \"IBM Plex Sans\",\n            \"Courier New\",\n            \"Roboto\",\n            \"Nunito\",\n            \"Arial\",\n            \"DejaVu Sans\",\n            \"Tahoma\",\n            \"Time News Roman\"\n        ],\n        \"font-type\": \"regular\",\n        \"supported-font-types\": [\n            \"regular\",\n            \"bold\",\n            \"italic\"\n        ],\n        \"font-size\": 14\n    }\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(414, 0, 'beauty_ega_cosmetics', 'config', 'config_theme_favicon', '{\r\n    \"image\": {\r\n        \"url\": \"/catalog/view/theme/default/image/favicon.ico\"\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(415, 0, 'beauty_ega_cosmetics', 'config', 'config_theme_social', '[\r\n    {\r\n        \"name\": \"facebook\",\r\n        \"text\": \"Facebook\",\r\n        \"url\": \"https://www.facebook.com\"\r\n    },\r\n    {\r\n        \"name\": \"twitter\",\r\n        \"text\": \"Twitter\",\r\n        \"url\": \"https://www.twitter.com\"\r\n    },\r\n    {\r\n        \"name\": \"instagram\",\r\n        \"text\": \"Instagram\",\r\n        \"url\": \"https://www.instagram.com\"\r\n    },\r\n    {\r\n        \"name\": \"tumblr\",\r\n        \"text\": \"Tumblr\",\r\n        \"url\": \"https://www.tumblr.com\"\r\n    },\r\n    {\r\n        \"name\": \"youtube\",\r\n        \"text\": \"Youtube\",\r\n        \"url\": \"https://www.youtube.com\"\r\n    },\r\n    {\r\n        \"name\": \"googleplus\",\r\n        \"text\": \"Google Plus\",\r\n        \"url\": \"https://www.plus.google.com\"\r\n    }\r\n]', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(416, 0, 'beauty_ega_cosmetics', 'config', 'config_theme_override_css', '{\r\n    \"css\": \"/* override css */\"\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(417, 0, 'beauty_ega_cosmetics', 'config', 'config_theme_checkout_page', '{\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(418, 0, 'beauty_ega_cosmetics', 'config', 'config_section_sections', '{\r\n    \"name\": \"Theme Config\",\r\n    \"version\": \"1.0\",\r\n    \"section\": {\r\n        \"header\": {\r\n            \"name\": \"header\",\r\n            \"text\": \"Header\",\r\n            \"visible\": 1\r\n        },\r\n        \"slide-show\": {\r\n            \"name\": \"slide-show\",\r\n            \"text\": \"Slideshow\",\r\n            \"visible\": 1\r\n        },\r\n        \"categories\": {\r\n            \"name\": \"categories\",\r\n            \"text\": \"Danh mục sản phẩm\",\r\n            \"visible\": 1\r\n        },\r\n        \"hot-deals\": {\r\n            \"name\": \"hot-deals\",\r\n            \"text\": \"Sản phẩm khuyến mại\",\r\n            \"visible\": 1\r\n        },\r\n        \"feature-products\": {\r\n            \"name\": \"feature-products\",\r\n            \"text\": \"Sản phẩm bán chạy\",\r\n            \"visible\": 1\r\n        },\r\n        \"related-products\": {\r\n            \"name\": \"related-products\",\r\n            \"text\": \"Sản phẩm xem nhiều\",\r\n            \"visible\": 1\r\n        },\r\n        \"new-products\": {\r\n            \"name\": \"new-products\",\r\n            \"text\": \"Sản phẩm mới\",\r\n            \"visible\": 1\r\n        },\r\n        \"product-detail\": {\r\n            \"name\": \"product-details\",\r\n            \"text\": \"Chi tiết sản phẩm\",\r\n            \"visible\": 1\r\n        },\r\n        \"banner\": {\r\n            \"name\": \"banner\",\r\n            \"text\": \"Banner\",\r\n            \"visible\": 1\r\n        },\r\n        \"content_customize\" : {\r\n            \"name\" : \"content-customize\",\r\n            \"text\" : \"Nội dung tùy chỉnh\",\r\n            \"visible\" : 1\r\n        },\r\n        \"partners\": {\r\n            \"name\": \"partners\",\r\n            \"text\": \"Đối tác\",\r\n            \"visible\": 1\r\n        },\r\n        \"blog\": {\r\n            \"name\": \"blog\",\r\n            \"text\": \"Blog\",\r\n            \"visible\": 1\r\n        },\r\n        \"footer\": {\r\n            \"name\": \"footer\",\r\n            \"text\": \"Footer\",\r\n            \"visible\": 1\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(419, 0, 'beauty_ega_cosmetics', 'config', 'config_section_banner', '{\r\n    \"name\": \"banner\",\r\n    \"text\": \"Banner\",\r\n    \"visible\": 1,\r\n    \"display\": [\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/banner/img-banner-06.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"Banner 1\",\r\n            \"visible\": 1\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/banner/img-banner-02.png\",\r\n            \"url\": \"#\",\r\n            \"description\": \"Banner 2\",\r\n            \"visible\": 1\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/banner/img-banner-03.png\",\r\n            \"url\": \"#\",\r\n            \"description\": \"Banner 3\",\r\n            \"visible\": 1\r\n        }\r\n    ]\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(420, 0, 'beauty_ega_cosmetics', 'config', 'config_section_best_sales_product', '{\r\n    \"name\": \"feature-product\",\r\n    \"text\": \"Sản phẩm bán chạy\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Sản phẩm bán chạy\",\r\n        \"sub_title\" : \"Mô tả tiêu đề\",\r\n        \"auto_retrieve_data\": 1,\r\n        \"collection_id\": 1\r\n    },\r\n    \"display\": {\r\n        \"grid\": {\r\n            \"quantity\": 4\r\n        },\r\n        \"grid_mobile\": {\r\n            \"quantity\": 2\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(421, 0, 'beauty_ega_cosmetics', 'config', 'config_section_new_product', '{\r\n    \"name\": \"new-product\",\r\n    \"text\": \"Sản phẩm mới\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Sản phẩm mới\",\r\n        \"sub_title\" : \"Mô tả tiêu đề\",\r\n        \"auto_retrieve_data\": 1,\r\n        \"collection_id\": 1\r\n    },\r\n    \"display\": {\r\n        \"grid\": {\r\n            \"quantity\": 4\r\n        },\r\n        \"grid_mobile\": {\r\n            \"quantity\": 2\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(422, 0, 'beauty_ega_cosmetics', 'config', 'config_section_best_views_product', '{\r\n    \"name\": \"related-product\",\r\n    \"text\": \"Sản phẩm xem nhiều\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Sản phẩm xem nhiều\"\r\n    },\r\n    \"display\": {\r\n        \"grid\": {\r\n            \"quantity\": 10\r\n        },\r\n        \"grid_mobile\": {\r\n            \"quantity\": 2\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(423, 0, 'beauty_ega_cosmetics', 'config', 'config_section_blog', '{\r\n    \"name\": \"blog\",\r\n    \"text\": \"Blog\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Blog\"\r\n    },\r\n    \"display\": {\r\n        \"menu\": [\r\n            {\r\n                \"type\": \"existing\",\r\n                \"menu\": {\r\n                    \"id\": 12,\r\n                    \"name\": \"Danh sách bài viết 1\"\r\n                }\r\n            },\r\n            {\r\n                \"type\": \"manual\",\r\n                \"entries\": [\r\n                    {\r\n                        \"id\": 10,\r\n                        \"name\": \"Entry 10\"\r\n                    },\r\n                    {\r\n                        \"id\": 11,\r\n                        \"name\": \"Entry 11\"\r\n                    }\r\n                ]\r\n            }\r\n        ],\r\n        \"grid\": {\r\n            \"quantity\": 10\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(424, 0, 'beauty_ega_cosmetics', 'config', 'config_section_detail_product', '{\r\n    \"name\": \"product-detail\",\r\n    \"text\": \"Chi tiết sản phẩm\",\r\n    \"visible\": 1,\r\n    \"display\": {\r\n        \"name\": true,\r\n        \"description\": false,\r\n        \"price\": true,\r\n        \"price-compare\": false,\r\n        \"status\": false,\r\n        \"sale\": false,\r\n        \"rate\": false\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(425, 0, 'beauty_ega_cosmetics', 'config', 'config_section_footer', '{\n    \"name\": \"footer\",\n    \"text\": \"Footer\",\n    \"visible\": 1,\n    \"contact\": {\n        \"title\": \"Liên hệ chúng tôi\",\n        \"address\": \"Cau Giay - Ha Noi\",\n        \"phone-number\": \"(+84)987654321\",\n        \"email\": \"dunght@novaon.asia\",\n        \"visible\": 1\n    },\n    \"collection\": {\n        \"title\": \"Bộ sưu tập\",\n        \"menu_id\": 1,\n        \"visible\": 1\n    },\n    \"quick-links\": {\n        \"title\": \"Liên kết nhanh\",\n        \"menu_id\": 1,\n        \"visible\": 1\n    },\n    \"subscribe\": {\n        \"title\": \"Đăng ký theo dõi\",\n        \"social_network\": 1,\n        \"visible\": 1,\n        \"youtube_visible\": 1,\n        \"facebook_visible\": 1,\n        \"instagram_visible\": 1\n    }\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(426, 0, 'beauty_ega_cosmetics', 'config', 'config_section_header', '{\r\n    \"name\": \"header\",\r\n    \"text\": \"Header\",\r\n    \"visible\": 1,\r\n    \"notify-bar\": {\r\n        \"name\": \"Thanh thông báo\",\r\n        \"visible\": 1,\r\n        \"content\": \"Mua sắm online thuận tiện và dễ dàng\",\r\n        \"url\": \"#\"\r\n    },\r\n    \"logo\": {\r\n        \"name\": \"Logo\",\r\n        \"url\": \"/catalog/view/theme/default/image/logo.png\",\r\n        \"height\": null\r\n    },\r\n    \"menu\": {\r\n        \"name\": \"Menu\",\r\n        \"display-list\": {\r\n            \"name\": \"Menu chính\",\r\n            \"id\": 1\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(427, 0, 'beauty_ega_cosmetics', 'config', 'config_section_hot_product', '{\r\n    \"name\": \"hot-deals\",\r\n    \"text\": \"Sản phẩm khuyến mại\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Sản phẩm khuyến mại\",\r\n        \"sub_title\" : \"Mô tả tiêu đề\",\r\n        \"auto_retrieve_data\": 1,\r\n        \"collection_id\": 1\r\n    },\r\n    \"display\": {\r\n        \"grid\": {\r\n            \"quantity\": 4\r\n        },\r\n        \"grid_mobile\": {\r\n            \"quantity\": 2\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(428, 0, 'beauty_ega_cosmetics', 'config', 'config_section_list_product', '{\r\n    \"name\": \"categories\",\r\n    \"text\": \"Danh mục sản phẩm\",\r\n    \"visible\": 0,\r\n    \"setting\": {\r\n        \"title\": \"Danh mục sản phẩm\"\r\n    },\r\n    \"display\": {\r\n        \"menu\": {\r\n            \"id\": 1,\r\n            \"name\": \"Danh mục sản phẩm 1\"\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(429, 0, 'beauty_ega_cosmetics', 'config', 'config_section_partner', '{\r\n    \"name\": \"partners\",\r\n    \"text\": \"Đối tác\",\r\n    \"visible\": 1,\r\n    \"limit-per-line\": 4,\r\n    \"display\": [\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/novaon/asset/img/partner_1.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"Partner 1\",\r\n            \"visible\": 1\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/novaon/asset/img/partner_2.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"Partner 2\",\r\n            \"visible\": 1\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/novaon/asset/img/partner_3.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"Partner 3\",\r\n            \"visible\": 1\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/novaon/asset/img/partner_4.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"Partner 4\",\r\n            \"visible\": 1\r\n        }\r\n    ]\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(430, 0, 'beauty_ega_cosmetics', 'config', 'config_section_slideshow', '{\r\n    \"name\": \"slide-show\",\r\n    \"text\": \"Slideshow\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"transition-time\": 5\r\n    },\r\n    \"display\": [\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/slideshow/slide_1.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"slide_1\"\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/slideshow/slide_2.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"slide_2\"\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/slideshow/slide_3.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"slide_3\"\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/slideshow/slide_4.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"slide_4\"\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/slideshow/slide_5.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"slide_5\"\r\n        }\r\n    ]\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(431, 0, 'beauty_ega_cosmetics', 'config', 'config_section_product_groups', '{\r\n    \"name\": \"group products\",\r\n    \"text\": \"nhóm sản phẩm\",\r\n    \"visible\": 1,\r\n    \"list\": [\r\n        {\r\n            \"text\": \"Danh sách sản phẩm 1\",\r\n            \"visible\": \"0\",\r\n            \"setting\": {\r\n                \"title\": \"Danh sách sản phẩm 1\",\r\n                \"collection_id\": \"4\",\r\n                \"sub_title\": \"Danh sách sản phẩm 1\"\r\n            },\r\n            \"display\": {\r\n                \"grid\": {\r\n                    \"quantity\": \"6\"\r\n                },\r\n                \"grid_mobile\": {\r\n                    \"quantity\": 2\r\n                }\r\n            }\r\n        }\r\n    ]\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(432, 0, 'beauty_ega_cosmetics', 'config', 'config_section_content_customize', '{\r\n    \"name\": \"content_customize\",\r\n    \"title\": \"Nội dung tuỳ chỉnh\",\r\n    \"display\": [\r\n        {\r\n            \"name\": \"content-1\",\r\n            \"title\": \"Bảo đảm chất lượng\",\r\n            \"type\": \"fixed\",\r\n            \"content\": {\r\n                \"icon\": \"/catalog/view/theme/default/image/favicon.ico\",\r\n                \"title\": \"Bảo đảm chất lượng\",\r\n                \"description\": \"Sản phẩm đảm bảo chất lượng\",\r\n                \"html\": \"\"\r\n            }\r\n        },\r\n        {\r\n            \"name\": \"content-2\",\r\n            \"title\": \"Miễn phí giao hàng\",\r\n            \"type\": \"fixed\",\r\n            \"content\": {\r\n                \"icon\": \"/catalog/view/theme/default/image/favicon.ico\",\r\n                \"title\": \"Miễn phí giao hàng\",\r\n                \"description\": \"Cho đơn hàng từ 2 triệu\",\r\n                \"html\": \"\"\r\n            }\r\n        },\r\n        {\r\n            \"name\": \"content-3\",\r\n            \"title\": \"Hỗ trợ 24/7\",\r\n            \"type\": \"fixed\",\r\n            \"content\": {\r\n                \"icon\": \"/catalog/view/theme/default/image/favicon.ico\",\r\n                \"title\": \"Hỗ trợ 24/7\",\r\n                \"description\": \"Hotline 012.345.678\",\r\n                \"html\": \"\"\r\n            }\r\n        },\r\n        {\r\n            \"name\": \"content-4\",\r\n            \"title\": \"Đổi trả hàng\",\r\n            \"type\": \"fixed\",\r\n            \"content\": {\r\n                \"icon\": \"/catalog/view/theme/default/image/favicon.ico\",\r\n                \"title\": \"Đổi trả hàng\",\r\n                \"description\": \"Trong vòng 7 ngày\",\r\n                \"html\": \"\"\r\n            }\r\n        }\r\n    ]\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(433, 0, 'beauty_ega_cosmetics', 'config', 'config_section_category_sections', '{\r\n    \"name\": \"Theme Config\",\r\n    \"version\": \"1.0\",\r\n    \"section\": {\r\n        \"banner\": {\r\n            \"name\": \"banner\",\r\n            \"text\": \"Banner\",\r\n            \"visible\": 1\r\n        },\r\n        \"filter\": {\r\n            \"name\": \"filter\",\r\n            \"text\": \"Filter\",\r\n            \"visible\": 1\r\n        },\r\n        \"product_category\": {\r\n            \"name\": \"product_category\",\r\n            \"text\": \"Product Category\",\r\n            \"visible\": 1\r\n        },\r\n        \"product_list\": {\r\n            \"name\": \"product_list\",\r\n            \"text\": \"Product List\",\r\n            \"visible\": 1\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(434, 0, 'beauty_ega_cosmetics', 'config', 'config_section_category_banner', '{\r\n    \"name\": \"banner\",\r\n    \"text\": \"Banner\",\r\n    \"visible\": 1,\r\n    \"display\": [\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/banner/img-banner-06.jpg\",\r\n            \"url\": \"#\",\r\n            \"description\": \"Banner 1\",\r\n            \"visible\": 1\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/banner/img-banner-02.png\",\r\n            \"url\": \"#\",\r\n            \"description\": \"Banner 2\",\r\n            \"visible\": 1\r\n        },\r\n        {\r\n            \"image-url\": \"/catalog/view/theme/default/image/banner/img-banner-03.png\",\r\n            \"url\": \"#\",\r\n            \"description\": \"Banner 3\",\r\n            \"visible\": 1\r\n        }\r\n    ]\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(435, 0, 'beauty_ega_cosmetics', 'config', 'config_section_category_filter', '{\r\n    \"name\": \"filter\",\r\n    \"text\": \"Bộ lọc\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Bộ lọc\"\r\n    },\r\n    \"display\": {\r\n        \"supplier\": {\r\n            \"title\": \"Nhà cung cấp\",\r\n            \"visible\": 1\r\n        },\r\n        \"product-type\": {\r\n            \"title\": \"Loại sản phẩm\",\r\n            \"visible\": 1\r\n        },\r\n        \"collection\": {\r\n            \"title\": \"Bộ sưu tập\",\r\n            \"visible\": 1\r\n        },\r\n        \"property\": {\r\n            \"title\": \"Lọc theo tt - k dung\",\r\n            \"visible\": 1,\r\n            \"prop\": [\r\n                \"all\",\r\n                \"color\",\r\n                \"weight\",\r\n                \"size\"\r\n            ]\r\n        },\r\n        \"product-price\": {\r\n            \"title\": \"Giá sản phẩm\",\r\n            \"visible\": 1,\r\n            \"range\": {\r\n                \"from\": 0,\r\n                \"to\": 100000000\r\n            }\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(436, 0, 'beauty_ega_cosmetics', 'config', 'config_section_category_product_category', '{\r\n    \"name\": \"product-category\",\r\n    \"text\": \"Danh mục sản phẩm\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Danh mục sản phẩm\"\r\n    },\r\n    \"display\": {\r\n        \"menu\": {\r\n            \"id\": 1,\r\n            \"name\": \"Danh mục sản phẩm 1\"\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(437, 0, 'beauty_ega_cosmetics', 'config', 'config_section_category_product_list', '{\r\n    \"name\": \"product-list\",\r\n    \"text\": \"Danh sách sản phẩm\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Danh sách sản phẩm\"\r\n    },\r\n    \"display\": {\r\n        \"grid\": {\r\n            \"quantity\": 10\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(438, 0, 'beauty_ega_cosmetics', 'config', 'config_section_product_detail_sections', '{\r\n    \"name\": \"Theme Config\",\r\n    \"version\": \"1.0\",\r\n    \"section\": {\r\n        \"related_product\": {\r\n            \"name\": \"related_product\",\r\n            \"text\": \"Related Product\",\r\n            \"visible\": 1\r\n        },\r\n        \"template\": {\r\n            \"name\": \"template\",\r\n            \"text\": \"Template\",\r\n            \"visible\": 1\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(439, 0, 'beauty_ega_cosmetics', 'config', 'config_section_product_detail_related_product', '{\r\n    \"name\": \"related-product\",\r\n    \"text\": \"Sản phẩm liên quan\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Sản phẩm liên quan\",\r\n        \"auto_retrieve_data\": 0,\r\n        \"collection_id\": 1\r\n    },\r\n    \"display\": {\r\n        \"grid\": {\r\n            \"quantity\": 4\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(440, 0, 'beauty_ega_cosmetics', 'config', 'config_section_product_detail_template', '{\r\n    \"name\": \"template\",\r\n    \"text\": \"Giao diện\",\r\n    \"visible\": 1,\r\n    \"display\": {\r\n        \"template\": {\r\n            \"id\": 10\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(441, 0, 'beauty_ega_cosmetics', 'config', 'config_section_blog_sections', '{\r\n    \"name\": \"Theme Config\",\r\n    \"version\": \"1.0\",\r\n    \"section\": {\r\n        \"blog_category\": {\r\n            \"name\": \"blog_category\",\r\n            \"text\": \"Blog Category\",\r\n            \"visible\": 1\r\n        },\r\n        \"blog_list\": {\r\n            \"name\": \"blog_list\",\r\n            \"text\": \"Blog List\",\r\n            \"visible\": 1\r\n        },\r\n        \"latest_blog\": {\r\n            \"name\": \"latest_blog\",\r\n            \"text\": \"Latest Blog\",\r\n            \"visible\": 1\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(442, 0, 'beauty_ega_cosmetics', 'config', 'config_section_blog_blog_category', '{\r\n    \"name\": \"blog-category\",\r\n    \"text\": \"Danh mục bài viết\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Danh mục bài viết\"\r\n    },\r\n    \"display\": {\r\n        \"menu\": {\r\n            \"id\": 1,\r\n            \"name\": \"Danh mục bài viết 1\"\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(443, 0, 'beauty_ega_cosmetics', 'config', 'config_section_blog_blog_list', '{\r\n    \"name\": \"blog-list\",\r\n    \"text\": \"Danh sách bài viết\",\r\n    \"visible\": 1,\r\n    \"display\": {\r\n        \"grid\": {\r\n            \"quantity\": 20\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(444, 0, 'beauty_ega_cosmetics', 'config', 'config_section_blog_latest_blog', '{\r\n    \"name\": \"latest-blog\",\r\n    \"text\": \"Bài viết mới nhất\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Bài viết mới nhất\"\r\n    },\r\n    \"display\": {\r\n        \"grid\": {\r\n            \"quantity\": 10\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(445, 0, 'beauty_ega_cosmetics', 'config', 'config_section_contact_sections', '{\r\n    \"name\": \"Theme Config\",\r\n    \"version\": \"1.0\",\r\n    \"section\": {\r\n        \"map\": {\r\n            \"name\": \"map\",\r\n            \"text\": \"Bản đồ\",\r\n            \"address\": \"<iframe src=\\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\\" width=\\\"600\\\" height=\\\"450\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\" allowfullscreen></iframe>\",\r\n            \"visible\": 1\r\n        },\r\n        \"info\": {\r\n            \"name\": \"info\",\r\n            \"text\": \"Thông tin cửa hàng\",\r\n            \"email\": \"contact-us@novaon.asia\",\r\n            \"phone\": \"0000000000\",\r\n            \"address\": \"address\",\r\n            \"visible\": 1\r\n        },\r\n        \"form\": {\r\n            \"name\": \"form\",\r\n            \"title\": \"Form liên hệ\",\r\n            \"email\": \"email@mail.com\",\r\n            \"visible\": 1\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(446, 0, 'beauty_ega_cosmetics', 'config', 'config_section_contact_map', '{   //////// CÁI NÀY KHÔNG DÙNG NỮA\r\n    \"name\": \"map\",\r\n    \"text\": \"Bản đồ\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Bản đồ\"\r\n    },\r\n    \"display\": {\r\n        \"address\": \"<iframe src=\\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\\" width=\\\"600\\\" height=\\\"450\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\" allowfullscreen></iframe>\"\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(447, 0, 'beauty_ega_cosmetics', 'config', 'config_section_contact_contact', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\r\n    \"name\": \"contact\",\r\n    \"text\": \"Liên hệ\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Vị trí của chúng tôi\"\r\n    },\r\n    \"display\": {\r\n        \"store\": {\r\n            \"title\": \"Gian hàng\",\r\n            \"content\": \"Số 12 Dịch Vọng Hậu, Cầu Giấy, Hà Nội\",\r\n            \"visible\": 1\r\n        },\r\n        \"telephone\": {\r\n            \"title\": \"Điện thoại\",\r\n            \"content\": \"(+84) 987 654 321\",\r\n            \"visible\": 1\r\n        },\r\n        \"social_follow\": {\r\n            \"socials\": [\r\n                {\r\n                    \"name\": \"facebook\",\r\n                    \"text\": \"Facebook\",\r\n                    \"visible\": 1\r\n                },\r\n                {\r\n                    \"name\": \"twitter\",\r\n                    \"text\": \"Twitter\",\r\n                    \"visible\": 1\r\n                },\r\n                {\r\n                    \"name\": \"instagram\",\r\n                    \"text\": \"Instagram\",\r\n                    \"visible\": 1\r\n                },\r\n                {\r\n                    \"name\": \"tumblr\",\r\n                    \"text\": \"Tumblr\",\r\n                    \"visible\": 1\r\n                },\r\n                {\r\n                    \"name\": \"youtube\",\r\n                    \"text\": \"Youtube\",\r\n                    \"visible\": 1\r\n                },\r\n                {\r\n                    \"name\": \"googleplus\",\r\n                    \"text\": \"Google Plus\",\r\n                    \"visible\": 1\r\n                }\r\n            ],\r\n            \"visible\": 1\r\n        }\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26'),
(448, 0, 'beauty_ega_cosmetics', 'config', 'config_section_contact_form', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\r\n    \"name\": \"form\",\r\n    \"text\": \"Biểu mẫu\",\r\n    \"visible\": 1,\r\n    \"setting\": {\r\n        \"title\": \"Liên hệ với chúng tôi\"\r\n    },\r\n    \"data\": {\r\n        \"email\": \"sale.247@xshop.com\"\r\n    }\r\n}', '2020-04-16 13:41:26', '2020-04-16 13:41:26');

-- --------------------------------------------------------

--
-- Table structure for table `oc_timezones`
--

CREATE TABLE `oc_timezones` (
  `id` int(11) NOT NULL,
  `value` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_timezones`
--

INSERT INTO `oc_timezones` (`id`, `value`, `label`) VALUES
(1, '-12:00', '(GMT -12:00) Eniwetok, Kwajalein'),
(2, '-11:00', '(GMT -11:00) Midway Island, Samoa'),
(3, '-10:00', '(GMT -10:00) Hawaii'),
(4, '-09:50', '(GMT -9:30) Taiohae'),
(5, '-09:00', '(GMT -9:00) Alaska'),
(6, '-08:00', '(GMT -8:00) Pacific Time (US &amp; Canada)'),
(7, '-07:00', '(GMT -7:00) Mountain Time (US &amp; Canada)'),
(8, '-06:00', '(GMT -6:00) Central Time (US &amp; Canada), Mexico City'),
(9, '-05:00', '(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima'),
(10, '-04:50', '(GMT -4:30) Caracas'),
(11, '-04:00', '(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz'),
(12, '-03:50', '(GMT -3:30) Newfoundland'),
(13, '-03:00', '(GMT -3:00) Brazil, Buenos Aires, Georgetown'),
(14, '-02:00', '(GMT -2:00) Mid-Atlantic'),
(15, '-01:00', '(GMT -1:00) Azores, Cape Verde Islands'),
(16, '+00:00', '(GMT) Western Europe Time, London, Lisbon, Casablanca'),
(17, '+01:00', '(GMT +1:00) Brussels, Copenhagen, Madrid, Paris'),
(18, '+02:00', '(GMT +2:00) Kaliningrad, South Africa'),
(19, '+03:00', '(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg'),
(20, '+03:50', '(GMT +3:30) Tehran'),
(21, '+04:00', '(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi'),
(22, '+04:50', '(GMT +4:30) Kabul'),
(23, '+05:00', '(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent'),
(24, '+05:50', '(GMT +5:30) Bombay, Calcutta, Madras, New Delhi'),
(25, '+05:75', '(GMT +5:45) Kathmandu, Pokhar'),
(26, '+06:00', '(GMT +6:00) Almaty, Dhaka, Colombo'),
(27, '+06:50', '(GMT +6:30) Yangon, Mandalay'),
(28, '+07:00', '(GMT +7:00) Bangkok, Hanoi, Jakarta'),
(29, '+08:00', '(GMT +8:00) Beijing, Perth, Singapore, Hong Kong'),
(30, '+08:75', '(GMT +8:45) Eucla'),
(31, '+09:00', '(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk'),
(32, '+09:50', '(GMT +9:30) Adelaide, Darwin'),
(33, '+10:00', '(GMT +10:00) Eastern Australia, Guam, Vladivostok'),
(34, '+10:50', '(GMT +10:30) Lord Howe Island'),
(35, '+11:00', '(GMT +11:00) Magadan, Solomon Islands, New Caledonia'),
(36, '+11:50', '(GMT +11:30) Norfolk Island'),
(37, '+12:00', '(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka'),
(38, '+12:75', '(GMT +12:45) Chatham Islands'),
(39, '+13:00', '(GMT +13:00) Apia, Nukualofa'),
(40, '+14:00', '(GMT +14:00) Line Islands, Tokelau');

-- --------------------------------------------------------

--
-- Table structure for table `oc_translation`
--

CREATE TABLE `oc_translation` (
  `translation_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_upload`
--

CREATE TABLE `oc_upload` (
  `upload_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_user`
--

CREATE TABLE `oc_user` (
  `user_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(500) NOT NULL,
  `password` varchar(100) NOT NULL,
  `password_temp` varchar(100) DEFAULT NULL,
  `salt` varchar(9) NOT NULL,
  `salt_temp` varchar(9) DEFAULT NULL,
  `firstname` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `telephone` varchar(32) DEFAULT NULL,
  `image` varchar(1024) NOT NULL,
  `code` varchar(40) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `info` varchar(1024) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `permission` text,
  `theme_develop` tinyint(1) DEFAULT '0',
  `date_added` datetime NOT NULL,
  `last_logged_in` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_user`
--

INSERT INTO `oc_user` (`user_id`, `user_group_id`, `username`, `password`, `password_temp`, `salt`, `salt_temp`, `firstname`, `lastname`, `email`, `telephone`, `image`, `code`, `ip`, `status`, `info`, `type`, `permission`, `theme_develop`, `date_added`, `last_logged_in`) VALUES
(1, 1, 'admin', '9851f4699e0a94d3a49e0d6782f1a45d54e26d84', '', 'XNQwUwXEU', '', 'Admin', 'Nguyễn  Văn', 'dunght@novaon.asia', '0987654322', 'view/image/custom/icons/avatar.svg', '', '127.0.0.1', 1, 'Tôi là admin', 'Admin', '\"\"', 0, '2018-11-29 16:53:44', '2020-04-28 03:22:16');

-- --------------------------------------------------------

--
-- Table structure for table `oc_user_group`
--

CREATE TABLE `oc_user_group` (
  `user_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_user_group`
--

INSERT INTO `oc_user_group` (`user_group_id`, `name`, `permission`) VALUES
(1, 'Administrator', '{\"access\":[\"app_store\\/market\",\"app_store\\/my_app\",\"blog\\/blog\",\"blog\\/category\",\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/collection\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"catalog\\/store_receipt\",\"catalog\\/warehouse\",\"common\\/cloudinary_upload\",\"common\\/column_left\",\"common\\/custom_column_left\",\"common\\/custom_header\",\"common\\/delivery_api\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/forgotten_orig\",\"common\\/login_orig\",\"common\\/profile\",\"common\\/reset_orig\",\"common\\/security\",\"custom\\/group_menu\",\"custom\\/menu\",\"custom\\/menu_item\",\"custom\\/preference\",\"custom\\/theme\",\"custom\\/theme_develop\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"design\\/banner\",\"design\\/layout\",\"design\\/seo_url\",\"design\\/theme\",\"design\\/translation\",\"discount\\/coupon\",\"discount\\/discount\",\"discount\\/setting\",\"event\\/language\",\"event\\/statistics\",\"event\\/theme\",\"extension\\/analytics\\/google\",\"extension\\/appstore\\/flash_sale\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/appstore\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/module\\/account\",\"extension\\/module\\/age_restriction\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/customer_welcome\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/ebay_listing\",\"extension\\/module\\/featured\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/hello_world\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/modification_editor\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_braintree_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/recommendation\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/simple_blog\",\"extension\\/module\\/simple_blog\\/article\",\"extension\\/module\\/simple_blog\\/author\",\"extension\\/module\\/simple_blog\\/category\",\"extension\\/module\\/simple_blog\\/comment\",\"extension\\/module\\/simple_blog\\/install\",\"extension\\/module\\/simple_blog\\/report\",\"extension\\/module\\/simple_blog_category\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/so_categories\",\"extension\\/module\\/so_category_slider\",\"extension\\/module\\/so_deals\",\"extension\\/module\\/so_extra_slider\",\"extension\\/module\\/so_facebook_message\",\"extension\\/module\\/so_filter_shop_by\",\"extension\\/module\\/so_home_slider\",\"extension\\/module\\/so_html_content\",\"extension\\/module\\/so_latest_blog\",\"extension\\/module\\/so_listing_tabs\",\"extension\\/module\\/so_megamenu\",\"extension\\/module\\/so_newletter_custom_popup\",\"extension\\/module\\/so_onepagecheckout\",\"extension\\/module\\/so_page_builder\",\"extension\\/module\\/so_quickview\",\"extension\\/module\\/so_searchpro\",\"extension\\/module\\/so_sociallogin\",\"extension\\/module\\/so_tools\",\"extension\\/module\\/soconfig\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/module\\/theme_builder_config\",\"extension\\/openbay\\/amazon\",\"extension\\/openbay\\/amazon_listing\",\"extension\\/openbay\\/amazon_product\",\"extension\\/openbay\\/amazonus\",\"extension\\/openbay\\/amazonus_listing\",\"extension\\/openbay\\/amazonus_product\",\"extension\\/openbay\\/ebay\",\"extension\\/openbay\\/ebay_profile\",\"extension\\/openbay\\/ebay_template\",\"extension\\/openbay\\/etsy\",\"extension\\/openbay\\/etsy_product\",\"extension\\/openbay\\/etsy_shipping\",\"extension\\/openbay\\/etsy_shop\",\"extension\\/openbay\\/fba\",\"extension\\/payment\\/alipay\",\"extension\\/payment\\/alipay_cross\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_braintree\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/squareup\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/wechat_pay\",\"extension\\/payment\\/worldpay\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/citylink\",\"extension\\/shipping\\/ec_ship\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/theme\\/default\",\"extension\\/theme\\/dunght\",\"extension\\/theme\\/novaon\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/forgotten_orig\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"marketplace\\/openbay\",\"online_store\\/contents\",\"online_store\\/domain\",\"online_store\\/google_shopping\",\"report\\/online\",\"report\\/overview\",\"report\\/report\",\"report\\/statistics\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"sale_channel\\/chatbot_novaon\",\"sale_channel\\/shopee\",\"section\\/banner\",\"section\\/best_sales_product\",\"section\\/best_views_product\",\"section\\/blog\",\"section\\/content_customize\",\"section\\/detail_product\",\"section\\/footer\",\"section\\/header\",\"section\\/hot_product\",\"section\\/list_product\",\"section\\/new_product\",\"section\\/partner\",\"section\\/sections\",\"section\\/slideshow\",\"section_blog\\/blog_category\",\"section_blog\\/blog_list\",\"section_blog\\/latest_blog\",\"section_blog\\/sections\",\"section_category\\/banner\",\"section_category\\/filter\",\"section_category\\/product_category\",\"section_category\\/product_list\",\"section_category\\/sections\",\"section_contact\\/contact\",\"section_contact\\/form\",\"section_contact\\/map\",\"section_contact\\/sections\",\"section_product_detail\\/related_product\",\"section_product_detail\\/sections\",\"setting\\/setting\",\"setting\\/store\",\"settings\\/account\",\"settings\\/classify\",\"settings\\/delivery\",\"settings\\/general\",\"settings\\/notify\",\"settings\\/payment\",\"settings\\/settings\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"super\\/dashboard\",\"theme\\/color\",\"theme\\/favicon\",\"theme\\/section_theme\",\"theme\\/social\",\"theme\\/text\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/analytics\\/google\",\"analytics\\/google\",\"extension\\/appstore\\/flash_sale\",\"extension\\/module\\/age_restriction\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\"],\"modify\":[\"app_store\\/market\",\"app_store\\/my_app\",\"blog\\/blog\",\"blog\\/category\",\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/collection\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"catalog\\/store_receipt\",\"catalog\\/warehouse\",\"common\\/cloudinary_upload\",\"common\\/column_left\",\"common\\/custom_column_left\",\"common\\/custom_header\",\"common\\/delivery_api\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/forgotten_orig\",\"common\\/login_orig\",\"common\\/profile\",\"common\\/reset_orig\",\"common\\/security\",\"custom\\/group_menu\",\"custom\\/menu\",\"custom\\/menu_item\",\"custom\\/preference\",\"custom\\/theme\",\"custom\\/theme_develop\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"design\\/banner\",\"design\\/layout\",\"design\\/seo_url\",\"design\\/theme\",\"design\\/translation\",\"discount\\/coupon\",\"discount\\/discount\",\"discount\\/setting\",\"event\\/language\",\"event\\/statistics\",\"event\\/theme\",\"extension\\/analytics\\/google\",\"extension\\/appstore\\/flash_sale\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/appstore\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/module\\/account\",\"extension\\/module\\/age_restriction\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/customer_welcome\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/ebay_listing\",\"extension\\/module\\/featured\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/hello_world\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/modification_editor\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_braintree_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/recommendation\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/simple_blog\",\"extension\\/module\\/simple_blog\\/article\",\"extension\\/module\\/simple_blog\\/author\",\"extension\\/module\\/simple_blog\\/category\",\"extension\\/module\\/simple_blog\\/comment\",\"extension\\/module\\/simple_blog\\/install\",\"extension\\/module\\/simple_blog\\/report\",\"extension\\/module\\/simple_blog_category\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/so_categories\",\"extension\\/module\\/so_category_slider\",\"extension\\/module\\/so_deals\",\"extension\\/module\\/so_extra_slider\",\"extension\\/module\\/so_facebook_message\",\"extension\\/module\\/so_filter_shop_by\",\"extension\\/module\\/so_home_slider\",\"extension\\/module\\/so_html_content\",\"extension\\/module\\/so_latest_blog\",\"extension\\/module\\/so_listing_tabs\",\"extension\\/module\\/so_megamenu\",\"extension\\/module\\/so_newletter_custom_popup\",\"extension\\/module\\/so_onepagecheckout\",\"extension\\/module\\/so_page_builder\",\"extension\\/module\\/so_quickview\",\"extension\\/module\\/so_searchpro\",\"extension\\/module\\/so_sociallogin\",\"extension\\/module\\/so_tools\",\"extension\\/module\\/soconfig\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/module\\/theme_builder_config\",\"extension\\/openbay\\/amazon\",\"extension\\/openbay\\/amazon_listing\",\"extension\\/openbay\\/amazon_product\",\"extension\\/openbay\\/amazonus\",\"extension\\/openbay\\/amazonus_listing\",\"extension\\/openbay\\/amazonus_product\",\"extension\\/openbay\\/ebay\",\"extension\\/openbay\\/ebay_profile\",\"extension\\/openbay\\/ebay_template\",\"extension\\/openbay\\/etsy\",\"extension\\/openbay\\/etsy_product\",\"extension\\/openbay\\/etsy_shipping\",\"extension\\/openbay\\/etsy_shop\",\"extension\\/openbay\\/fba\",\"extension\\/payment\\/alipay\",\"extension\\/payment\\/alipay_cross\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_braintree\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/squareup\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/wechat_pay\",\"extension\\/payment\\/worldpay\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/citylink\",\"extension\\/shipping\\/ec_ship\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/theme\\/default\",\"extension\\/theme\\/dunght\",\"extension\\/theme\\/novaon\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/forgotten_orig\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"marketplace\\/openbay\",\"online_store\\/contents\",\"online_store\\/domain\",\"online_store\\/google_shopping\",\"report\\/online\",\"report\\/overview\",\"report\\/report\",\"report\\/statistics\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"sale_channel\\/chatbot_novaon\",\"sale_channel\\/shopee\",\"section\\/banner\",\"section\\/best_sales_product\",\"section\\/best_views_product\",\"section\\/blog\",\"section\\/content_customize\",\"section\\/detail_product\",\"section\\/footer\",\"section\\/header\",\"section\\/hot_product\",\"section\\/list_product\",\"section\\/new_product\",\"section\\/partner\",\"section\\/sections\",\"section\\/slideshow\",\"section_blog\\/blog_category\",\"section_blog\\/blog_list\",\"section_blog\\/latest_blog\",\"section_blog\\/sections\",\"section_category\\/banner\",\"section_category\\/filter\",\"section_category\\/product_category\",\"section_category\\/product_list\",\"section_category\\/sections\",\"section_contact\\/contact\",\"section_contact\\/form\",\"section_contact\\/map\",\"section_contact\\/sections\",\"section_product_detail\\/related_product\",\"section_product_detail\\/sections\",\"setting\\/setting\",\"setting\\/store\",\"settings\\/account\",\"settings\\/classify\",\"settings\\/delivery\",\"settings\\/general\",\"settings\\/notify\",\"settings\\/payment\",\"settings\\/settings\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"super\\/dashboard\",\"theme\\/color\",\"theme\\/favicon\",\"theme\\/section_theme\",\"theme\\/social\",\"theme\\/text\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/analytics\\/google\",\"analytics\\/google\",\"extension\\/appstore\\/flash_sale\",\"extension\\/module\\/age_restriction\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\",\"extension\\/appstore\\/flash_sale\"]}'),
(10, 'Demonstration', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher`
--

CREATE TABLE `oc_voucher` (
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_history`
--

CREATE TABLE `oc_voucher_history` (
  `voucher_history_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_theme`
--

CREATE TABLE `oc_voucher_theme` (
  `voucher_theme_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_voucher_theme`
--

INSERT INTO `oc_voucher_theme` (`voucher_theme_id`, `image`) VALUES
(8, 'catalog/demo/canon_eos_5d_2.jpg'),
(7, 'catalog/demo/gift-voucher-birthday.jpg'),
(6, 'catalog/demo/apple_logo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_theme_description`
--

CREATE TABLE `oc_voucher_theme_description` (
  `voucher_theme_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_voucher_theme_description`
--

INSERT INTO `oc_voucher_theme_description` (`voucher_theme_id`, `language_id`, `name`) VALUES
(6, 1, 'Christmas'),
(7, 1, 'Birthday'),
(8, 1, 'General');

-- --------------------------------------------------------

--
-- Table structure for table `oc_warehouse`
--

CREATE TABLE `oc_warehouse` (
  `warehouse_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_version_id` varchar(256) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_warehouse`
--

INSERT INTO `oc_warehouse` (`warehouse_id`, `product_id`, `product_version_id`) VALUES
(1045, 75, NULL),
(1053, 81, NULL),
(1085, 89, NULL),
(1086, 90, NULL),
(1087, 91, NULL),
(1088, 92, NULL),
(1091, 93, NULL),
(1092, 94, NULL),
(1096, 95, NULL),
(1097, 96, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `oc_weight_class`
--

CREATE TABLE `oc_weight_class` (
  `weight_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL DEFAULT '0.00000000'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_weight_class`
--

INSERT INTO `oc_weight_class` (`weight_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '1000.00000000'),
(5, '2.20460000'),
(6, '35.27400000');

-- --------------------------------------------------------

--
-- Table structure for table `oc_weight_class_description`
--

CREATE TABLE `oc_weight_class_description` (
  `weight_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_weight_class_description`
--

INSERT INTO `oc_weight_class_description` (`weight_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Kilogram', 'kg'),
(2, 1, 'Gram', 'g'),
(5, 1, 'Pound ', 'lb'),
(6, 1, 'Ounce', 'oz'),
(1, 2, 'Kilogram', 'kg'),
(2, 2, 'Gram', 'g'),
(5, 2, 'Pound ', 'lb'),
(6, 2, 'Ounce', 'oz');

-- --------------------------------------------------------

--
-- Table structure for table `oc_zone`
--

CREATE TABLE `oc_zone` (
  `zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_zone`
--

INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1, 230, 'Hà Nội', 'HN', 1),
(2, 230, 'Hồ Chí Minh', 'HCM', 1),
(3, 230, 'An Giang', 'AG', 1),
(4, 230, 'Bà Rịa-Vũng Tàu', 'BR', 1),
(5, 230, 'Bạc Liêu', 'BL', 1),
(6, 230, 'Bắc Kạn', 'BK', 1),
(7, 230, 'Bắc Giang', 'BG', 1),
(8, 230, 'Bắc Ninh', 'BN', 1),
(9, 230, 'Bến Tre', 'BT', 1),
(10, 230, 'Bình Dương', 'BD', 1),
(11, 230, 'Bình Định', 'BĐ', 1),
(12, 230, 'Bình Phước', 'BP', 1),
(13, 230, 'Bình Thuận', 'BT', 1),
(14, 230, 'Cà Mau', 'CM', 1),
(15, 230, 'Cao Bằng', 'CB', 1),
(16, 230, 'Cần Thơ', 'CT', 1),
(17, 230, 'Đà Nẵng', 'DN', 1),
(18, 230, 'Đắk Lắk', 'DL', 1),
(19, 230, 'Đắk Nông', 'DKN', 1),
(20, 230, 'Điện Biên', 'DB', 1),
(21, 230, 'Đồng Nai', 'DNN', 1),
(22, 230, 'Đồng Tháp', 'DT', 1),
(23, 230, 'Gia Lai', 'GL', 1),
(24, 230, 'Hà Giang', 'HG', 1),
(25, 230, 'Hà Nam', 'HNN', 1),
(26, 230, 'Hà Tĩnh', 'HT', 1),
(27, 230, 'Hải Dương', 'HD', 1),
(28, 230, 'Hải Phòng', 'HP', 1),
(29, 230, 'Hòa Bình', 'HB', 1),
(30, 230, 'Hậu Giang', 'HG', 1),
(31, 230, 'Hưng Yên', 'HY', 1),
(32, 230, 'Khánh Hòa', 'KH', 1),
(33, 230, 'Kiên Giang', 'KG', 1),
(34, 230, 'Kon Tum', 'KT', 1),
(35, 230, 'Lai Châu', 'LCU', 1),
(36, 230, 'Lào Cai', 'LC', 1),
(37, 230, 'Lạng Sơn', 'LS', 1),
(38, 230, 'Lâm Đồng', 'LD', 1),
(39, 230, 'Long An', 'LA', 1),
(40, 230, 'Nam Định', 'ND', 1),
(41, 230, 'Nghệ An', 'NA', 1),
(42, 230, 'Ninh Bình', 'NB', 1),
(43, 230, 'Ninh Thuận', 'NT', 1),
(44, 230, 'Phú Thọ', 'PT', 1),
(45, 230, 'Phú Yên', 'PY', 1),
(46, 230, 'Quảng Bình', 'QB', 1),
(47, 230, 'Quảng Nam', 'QNN', 1),
(48, 230, 'Quảng Ngãi', 'QNG', 1),
(49, 230, 'Quảng Ninh', 'QN', 1),
(50, 230, 'Quảng Trị', 'QT', 1),
(51, 230, 'Sóc Trăng', 'ST', 1),
(52, 230, 'Sơn La', 'SL', 1),
(53, 230, 'Tây Ninh', 'TN', 1),
(54, 230, 'Thái Bình', 'TB', 1),
(55, 230, 'Thái Nguyên', 'TN', 1),
(56, 230, 'Thanh Hóa', 'TH', 1),
(57, 230, 'Thừa Thiên - Huế', 'TTH', 1),
(58, 230, 'Tiền Giang', 'TG', 1),
(59, 230, 'Trà Vinh', 'TV', 1),
(60, 230, 'Tuyên Quang', 'TQ', 1),
(61, 230, 'Vĩnh Long', 'VL', 1),
(62, 230, 'Vĩnh Phúc', 'VP', 1),
(63, 230, 'Yên Bái', 'YB', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_zone_to_geo_zone`
--

CREATE TABLE `oc_zone_to_geo_zone` (
  `zone_to_geo_zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `geo_zone_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_zone_to_geo_zone`
--

INSERT INTO `oc_zone_to_geo_zone` (`zone_to_geo_zone_id`, `country_id`, `zone_id`, `geo_zone_id`, `date_added`, `date_modified`) VALUES
(1, 222, 0, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 222, 3513, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 222, 3514, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 222, 3515, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 222, 3516, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 222, 3517, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 222, 3518, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 222, 3519, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 222, 3520, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 222, 3521, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 222, 3522, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 222, 3523, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 222, 3524, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 222, 3525, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 222, 3526, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 222, 3527, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 222, 3528, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 222, 3529, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 222, 3530, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 222, 3531, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 222, 3532, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 222, 3533, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 222, 3534, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 222, 3535, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 222, 3536, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 222, 3537, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 222, 3538, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 222, 3539, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 222, 3540, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 222, 3541, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 222, 3542, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 222, 3543, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 222, 3544, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 222, 3545, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 222, 3546, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 222, 3547, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 222, 3548, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 222, 3549, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 222, 3550, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 222, 3551, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 222, 3552, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 222, 3553, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 222, 3554, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 222, 3555, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 222, 3556, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 222, 3557, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 222, 3558, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 222, 3559, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 222, 3560, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 222, 3561, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 222, 3562, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 222, 3563, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 222, 3564, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 222, 3565, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 222, 3566, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 222, 3567, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 222, 3568, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 222, 3569, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 222, 3570, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 222, 3571, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 222, 3572, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 222, 3573, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 222, 3574, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 222, 3575, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 222, 3576, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 222, 3577, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 222, 3578, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 222, 3579, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 222, 3580, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 222, 3581, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 222, 3582, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 222, 3583, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 222, 3584, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 222, 3585, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 222, 3586, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 222, 3587, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 222, 3588, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 222, 3589, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 222, 3590, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 222, 3591, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 222, 3592, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 222, 3593, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 222, 3594, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 222, 3595, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 222, 3596, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 222, 3597, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 222, 3598, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 222, 3599, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 222, 3600, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 222, 3601, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 222, 3602, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 222, 3603, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 222, 3604, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 222, 3605, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 222, 3606, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 222, 3607, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 222, 3608, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 222, 3609, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 222, 3610, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 222, 3611, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 222, 3612, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 222, 3949, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 222, 3950, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 222, 3951, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 222, 3952, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 222, 3953, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 222, 3954, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 222, 3955, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 222, 3972, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `oc_address`
--
ALTER TABLE `oc_address`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `oc_api`
--
ALTER TABLE `oc_api`
  ADD PRIMARY KEY (`api_id`);

--
-- Indexes for table `oc_api_ip`
--
ALTER TABLE `oc_api_ip`
  ADD PRIMARY KEY (`api_ip_id`);

--
-- Indexes for table `oc_api_session`
--
ALTER TABLE `oc_api_session`
  ADD PRIMARY KEY (`api_session_id`);

--
-- Indexes for table `oc_appstore`
--
ALTER TABLE `oc_appstore`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_appstore_setting`
--
ALTER TABLE `oc_appstore_setting`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `oc_app_config_theme`
--
ALTER TABLE `oc_app_config_theme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_app_flash_sale_product`
--
ALTER TABLE `oc_app_flash_sale_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_attribute`
--
ALTER TABLE `oc_attribute`
  ADD PRIMARY KEY (`attribute_id`);

--
-- Indexes for table `oc_attribute_description`
--
ALTER TABLE `oc_attribute_description`
  ADD PRIMARY KEY (`attribute_id`,`language_id`);

--
-- Indexes for table `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  ADD PRIMARY KEY (`attribute_group_id`);

--
-- Indexes for table `oc_attribute_group_description`
--
ALTER TABLE `oc_attribute_group_description`
  ADD PRIMARY KEY (`attribute_group_id`,`language_id`);

--
-- Indexes for table `oc_banner`
--
ALTER TABLE `oc_banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  ADD PRIMARY KEY (`banner_image_id`);

--
-- Indexes for table `oc_blog`
--
ALTER TABLE `oc_blog`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `oc_blog_category`
--
ALTER TABLE `oc_blog_category`
  ADD PRIMARY KEY (`blog_category_id`);

--
-- Indexes for table `oc_blog_category_description`
--
ALTER TABLE `oc_blog_category_description`
  ADD PRIMARY KEY (`blog_category_id`,`language_id`);

--
-- Indexes for table `oc_blog_description`
--
ALTER TABLE `oc_blog_description`
  ADD PRIMARY KEY (`blog_id`,`language_id`);

--
-- Indexes for table `oc_blog_tag`
--
ALTER TABLE `oc_blog_tag`
  ADD PRIMARY KEY (`blog_tag_id`);

--
-- Indexes for table `oc_bm_author`
--
ALTER TABLE `oc_bm_author`
  ADD PRIMARY KEY (`author_id`);

--
-- Indexes for table `oc_bm_author_description`
--
ALTER TABLE `oc_bm_author_description`
  ADD PRIMARY KEY (`author_description_id`);

--
-- Indexes for table `oc_bm_author_group`
--
ALTER TABLE `oc_bm_author_group`
  ADD PRIMARY KEY (`author_group_id`);

--
-- Indexes for table `oc_bm_category`
--
ALTER TABLE `oc_bm_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `oc_bm_category_description`
--
ALTER TABLE `oc_bm_category_description`
  ADD PRIMARY KEY (`category_description_id`);

--
-- Indexes for table `oc_bm_category_path`
--
ALTER TABLE `oc_bm_category_path`
  ADD PRIMARY KEY (`category_id`,`path_id`);

--
-- Indexes for table `oc_bm_category_to_store`
--
ALTER TABLE `oc_bm_category_to_store`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Indexes for table `oc_bm_post`
--
ALTER TABLE `oc_bm_post`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `oc_bm_post_description`
--
ALTER TABLE `oc_bm_post_description`
  ADD PRIMARY KEY (`post_description_id`);

--
-- Indexes for table `oc_bm_post_related`
--
ALTER TABLE `oc_bm_post_related`
  ADD PRIMARY KEY (`post_id`,`post_related_id`);

--
-- Indexes for table `oc_bm_post_to_category`
--
ALTER TABLE `oc_bm_post_to_category`
  ADD PRIMARY KEY (`category_id`,`post_id`);

--
-- Indexes for table `oc_bm_post_to_product`
--
ALTER TABLE `oc_bm_post_to_product`
  ADD PRIMARY KEY (`product_id`,`post_id`);

--
-- Indexes for table `oc_bm_post_video`
--
ALTER TABLE `oc_bm_post_video`
  ADD PRIMARY KEY (`post_id`,`video`);

--
-- Indexes for table `oc_bm_review`
--
ALTER TABLE `oc_bm_review`
  ADD PRIMARY KEY (`review_id`);

--
-- Indexes for table `oc_bm_review_to_image`
--
ALTER TABLE `oc_bm_review_to_image`
  ADD PRIMARY KEY (`review_id`,`image`);

--
-- Indexes for table `oc_cart`
--
ALTER TABLE `oc_cart`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `cart_id` (`api_id`,`customer_id`,`session_id`,`product_id`,`recurring_id`);

--
-- Indexes for table `oc_category`
--
ALTER TABLE `oc_category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `oc_category_description`
--
ALTER TABLE `oc_category_description`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_category_filter`
--
ALTER TABLE `oc_category_filter`
  ADD PRIMARY KEY (`category_id`,`filter_id`);

--
-- Indexes for table `oc_category_path`
--
ALTER TABLE `oc_category_path`
  ADD PRIMARY KEY (`category_id`,`path_id`);

--
-- Indexes for table `oc_category_to_layout`
--
ALTER TABLE `oc_category_to_layout`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Indexes for table `oc_category_to_store`
--
ALTER TABLE `oc_category_to_store`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Indexes for table `oc_collection`
--
ALTER TABLE `oc_collection`
  ADD PRIMARY KEY (`collection_id`);

--
-- Indexes for table `oc_collection_description`
--
ALTER TABLE `oc_collection_description`
  ADD PRIMARY KEY (`collection_id`);

--
-- Indexes for table `oc_collection_to_collection`
--
ALTER TABLE `oc_collection_to_collection`
  ADD PRIMARY KEY (`collection_to_collection_id`);

--
-- Indexes for table `oc_country`
--
ALTER TABLE `oc_country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `oc_coupon`
--
ALTER TABLE `oc_coupon`
  ADD PRIMARY KEY (`coupon_id`);

--
-- Indexes for table `oc_coupon_category`
--
ALTER TABLE `oc_coupon_category`
  ADD PRIMARY KEY (`coupon_id`,`category_id`);

--
-- Indexes for table `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  ADD PRIMARY KEY (`coupon_history_id`);

--
-- Indexes for table `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  ADD PRIMARY KEY (`coupon_product_id`);

--
-- Indexes for table `oc_currency`
--
ALTER TABLE `oc_currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Indexes for table `oc_customer`
--
ALTER TABLE `oc_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  ADD PRIMARY KEY (`customer_activity_id`);

--
-- Indexes for table `oc_customer_affiliate`
--
ALTER TABLE `oc_customer_affiliate`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `oc_customer_approval`
--
ALTER TABLE `oc_customer_approval`
  ADD PRIMARY KEY (`customer_approval_id`);

--
-- Indexes for table `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  ADD PRIMARY KEY (`customer_group_id`);

--
-- Indexes for table `oc_customer_group_description`
--
ALTER TABLE `oc_customer_group_description`
  ADD PRIMARY KEY (`customer_group_id`,`language_id`);

--
-- Indexes for table `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  ADD PRIMARY KEY (`customer_history_id`);

--
-- Indexes for table `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  ADD PRIMARY KEY (`customer_ip_id`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  ADD PRIMARY KEY (`customer_login_id`),
  ADD KEY `email` (`email`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `oc_customer_online`
--
ALTER TABLE `oc_customer_online`
  ADD PRIMARY KEY (`ip`);

--
-- Indexes for table `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  ADD PRIMARY KEY (`customer_reward_id`);

--
-- Indexes for table `oc_customer_search`
--
ALTER TABLE `oc_customer_search`
  ADD PRIMARY KEY (`customer_search_id`);

--
-- Indexes for table `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  ADD PRIMARY KEY (`customer_transaction_id`);

--
-- Indexes for table `oc_customer_wishlist`
--
ALTER TABLE `oc_customer_wishlist`
  ADD PRIMARY KEY (`customer_id`,`product_id`);

--
-- Indexes for table `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  ADD PRIMARY KEY (`custom_field_id`);

--
-- Indexes for table `oc_custom_field_customer_group`
--
ALTER TABLE `oc_custom_field_customer_group`
  ADD PRIMARY KEY (`custom_field_id`,`customer_group_id`);

--
-- Indexes for table `oc_custom_field_description`
--
ALTER TABLE `oc_custom_field_description`
  ADD PRIMARY KEY (`custom_field_id`,`language_id`);

--
-- Indexes for table `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  ADD PRIMARY KEY (`custom_field_value_id`);

--
-- Indexes for table `oc_custom_field_value_description`
--
ALTER TABLE `oc_custom_field_value_description`
  ADD PRIMARY KEY (`custom_field_value_id`,`language_id`);

--
-- Indexes for table `oc_demo_data`
--
ALTER TABLE `oc_demo_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_discount`
--
ALTER TABLE `oc_discount`
  ADD PRIMARY KEY (`discount_id`);

--
-- Indexes for table `oc_discount_description`
--
ALTER TABLE `oc_discount_description`
  ADD PRIMARY KEY (`discount_id`,`language_id`);

--
-- Indexes for table `oc_discount_status`
--
ALTER TABLE `oc_discount_status`
  ADD PRIMARY KEY (`discount_status_id`,`language_id`);

--
-- Indexes for table `oc_discount_to_store`
--
ALTER TABLE `oc_discount_to_store`
  ADD PRIMARY KEY (`discount_id`,`store_id`);

--
-- Indexes for table `oc_discount_type`
--
ALTER TABLE `oc_discount_type`
  ADD PRIMARY KEY (`discount_type_id`,`language_id`);

--
-- Indexes for table `oc_download`
--
ALTER TABLE `oc_download`
  ADD PRIMARY KEY (`download_id`);

--
-- Indexes for table `oc_download_description`
--
ALTER TABLE `oc_download_description`
  ADD PRIMARY KEY (`download_id`,`language_id`);

--
-- Indexes for table `oc_event`
--
ALTER TABLE `oc_event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `oc_extension`
--
ALTER TABLE `oc_extension`
  ADD PRIMARY KEY (`extension_id`);

--
-- Indexes for table `oc_extension_install`
--
ALTER TABLE `oc_extension_install`
  ADD PRIMARY KEY (`extension_install_id`);

--
-- Indexes for table `oc_extension_path`
--
ALTER TABLE `oc_extension_path`
  ADD PRIMARY KEY (`extension_path_id`);

--
-- Indexes for table `oc_filter`
--
ALTER TABLE `oc_filter`
  ADD PRIMARY KEY (`filter_id`);

--
-- Indexes for table `oc_filter_description`
--
ALTER TABLE `oc_filter_description`
  ADD PRIMARY KEY (`filter_id`,`language_id`);

--
-- Indexes for table `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  ADD PRIMARY KEY (`filter_group_id`);

--
-- Indexes for table `oc_filter_group_description`
--
ALTER TABLE `oc_filter_group_description`
  ADD PRIMARY KEY (`filter_group_id`,`language_id`);

--
-- Indexes for table `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  ADD PRIMARY KEY (`geo_zone_id`);

--
-- Indexes for table `oc_images`
--
ALTER TABLE `oc_images`
  ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `oc_information`
--
ALTER TABLE `oc_information`
  ADD PRIMARY KEY (`information_id`);

--
-- Indexes for table `oc_information_description`
--
ALTER TABLE `oc_information_description`
  ADD PRIMARY KEY (`information_id`,`language_id`);

--
-- Indexes for table `oc_information_to_layout`
--
ALTER TABLE `oc_information_to_layout`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Indexes for table `oc_information_to_store`
--
ALTER TABLE `oc_information_to_store`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Indexes for table `oc_language`
--
ALTER TABLE `oc_language`
  ADD PRIMARY KEY (`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_layout`
--
ALTER TABLE `oc_layout`
  ADD PRIMARY KEY (`layout_id`);

--
-- Indexes for table `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  ADD PRIMARY KEY (`layout_module_id`);

--
-- Indexes for table `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  ADD PRIMARY KEY (`layout_route_id`);

--
-- Indexes for table `oc_length_class`
--
ALTER TABLE `oc_length_class`
  ADD PRIMARY KEY (`length_class_id`);

--
-- Indexes for table `oc_length_class_description`
--
ALTER TABLE `oc_length_class_description`
  ADD PRIMARY KEY (`length_class_id`,`language_id`);

--
-- Indexes for table `oc_location`
--
ALTER TABLE `oc_location`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  ADD PRIMARY KEY (`manufacturer_id`);

--
-- Indexes for table `oc_manufacturer_to_store`
--
ALTER TABLE `oc_manufacturer_to_store`
  ADD PRIMARY KEY (`manufacturer_id`,`store_id`);

--
-- Indexes for table `oc_marketing`
--
ALTER TABLE `oc_marketing`
  ADD PRIMARY KEY (`marketing_id`);

--
-- Indexes for table `oc_menu`
--
ALTER TABLE `oc_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `oc_menu_description`
--
ALTER TABLE `oc_menu_description`
  ADD PRIMARY KEY (`menu_id`,`language_id`);

--
-- Indexes for table `oc_menu_type`
--
ALTER TABLE `oc_menu_type`
  ADD PRIMARY KEY (`menu_type_id`);

--
-- Indexes for table `oc_menu_type_description`
--
ALTER TABLE `oc_menu_type_description`
  ADD PRIMARY KEY (`menu_type_id`,`language_id`);

--
-- Indexes for table `oc_migration`
--
ALTER TABLE `oc_migration`
  ADD PRIMARY KEY (`migration_id`);

--
-- Indexes for table `oc_modification`
--
ALTER TABLE `oc_modification`
  ADD PRIMARY KEY (`modification_id`);

--
-- Indexes for table `oc_module`
--
ALTER TABLE `oc_module`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `oc_my_app`
--
ALTER TABLE `oc_my_app`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_novaon_api_credentials`
--
ALTER TABLE `oc_novaon_api_credentials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_novaon_group_menu`
--
ALTER TABLE `oc_novaon_group_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_novaon_group_menu_description`
--
ALTER TABLE `oc_novaon_group_menu_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_novaon_menu_item`
--
ALTER TABLE `oc_novaon_menu_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_novaon_menu_item_description`
--
ALTER TABLE `oc_novaon_menu_item_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_novaon_relation_table`
--
ALTER TABLE `oc_novaon_relation_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_novaon_type`
--
ALTER TABLE `oc_novaon_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_novaon_type_description`
--
ALTER TABLE `oc_novaon_type_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_option`
--
ALTER TABLE `oc_option`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `oc_option_description`
--
ALTER TABLE `oc_option_description`
  ADD PRIMARY KEY (`option_id`,`language_id`);

--
-- Indexes for table `oc_option_value`
--
ALTER TABLE `oc_option_value`
  ADD PRIMARY KEY (`option_value_id`);

--
-- Indexes for table `oc_option_value_description`
--
ALTER TABLE `oc_option_value_description`
  ADD PRIMARY KEY (`option_value_id`,`language_id`);

--
-- Indexes for table `oc_order`
--
ALTER TABLE `oc_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `oc_order_history`
--
ALTER TABLE `oc_order_history`
  ADD PRIMARY KEY (`order_history_id`);

--
-- Indexes for table `oc_order_option`
--
ALTER TABLE `oc_order_option`
  ADD PRIMARY KEY (`order_option_id`);

--
-- Indexes for table `oc_order_product`
--
ALTER TABLE `oc_order_product`
  ADD PRIMARY KEY (`order_product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  ADD PRIMARY KEY (`order_recurring_id`);

--
-- Indexes for table `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  ADD PRIMARY KEY (`order_recurring_transaction_id`);

--
-- Indexes for table `oc_order_shipment`
--
ALTER TABLE `oc_order_shipment`
  ADD PRIMARY KEY (`order_shipment_id`);

--
-- Indexes for table `oc_order_status`
--
ALTER TABLE `oc_order_status`
  ADD PRIMARY KEY (`order_status_id`,`language_id`);

--
-- Indexes for table `oc_order_tag`
--
ALTER TABLE `oc_order_tag`
  ADD PRIMARY KEY (`order_tag_id`);

--
-- Indexes for table `oc_order_total`
--
ALTER TABLE `oc_order_total`
  ADD PRIMARY KEY (`order_total_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `oc_order_to_discount`
--
ALTER TABLE `oc_order_to_discount`
  ADD PRIMARY KEY (`order_id`,`discount_id`);

--
-- Indexes for table `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  ADD PRIMARY KEY (`order_voucher_id`);

--
-- Indexes for table `oc_page_contents`
--
ALTER TABLE `oc_page_contents`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `oc_product`
--
ALTER TABLE `oc_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `oc_product_attribute`
--
ALTER TABLE `oc_product_attribute`
  ADD PRIMARY KEY (`product_id`,`attribute_id`,`language_id`);

--
-- Indexes for table `oc_product_collection`
--
ALTER TABLE `oc_product_collection`
  ADD PRIMARY KEY (`product_collection_id`);

--
-- Indexes for table `oc_product_description`
--
ALTER TABLE `oc_product_description`
  ADD PRIMARY KEY (`product_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  ADD PRIMARY KEY (`product_discount_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_filter`
--
ALTER TABLE `oc_product_filter`
  ADD PRIMARY KEY (`product_id`,`filter_id`);

--
-- Indexes for table `oc_product_image`
--
ALTER TABLE `oc_product_image`
  ADD PRIMARY KEY (`product_image_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_option`
--
ALTER TABLE `oc_product_option`
  ADD PRIMARY KEY (`product_option_id`);

--
-- Indexes for table `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  ADD PRIMARY KEY (`product_option_value_id`);

--
-- Indexes for table `oc_product_recurring`
--
ALTER TABLE `oc_product_recurring`
  ADD PRIMARY KEY (`product_id`,`recurring_id`,`customer_group_id`);

--
-- Indexes for table `oc_product_related`
--
ALTER TABLE `oc_product_related`
  ADD PRIMARY KEY (`product_id`,`related_id`);

--
-- Indexes for table `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  ADD PRIMARY KEY (`product_reward_id`);

--
-- Indexes for table `oc_product_special`
--
ALTER TABLE `oc_product_special`
  ADD PRIMARY KEY (`product_special_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_tag`
--
ALTER TABLE `oc_product_tag`
  ADD PRIMARY KEY (`product_tag_id`);

--
-- Indexes for table `oc_product_to_category`
--
ALTER TABLE `oc_product_to_category`
  ADD PRIMARY KEY (`product_id`,`category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `oc_product_to_download`
--
ALTER TABLE `oc_product_to_download`
  ADD PRIMARY KEY (`product_id`,`download_id`);

--
-- Indexes for table `oc_product_to_layout`
--
ALTER TABLE `oc_product_to_layout`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Indexes for table `oc_product_to_store`
--
ALTER TABLE `oc_product_to_store`
  ADD PRIMARY KEY (`product_id`,`product_version_id`,`store_id`);

--
-- Indexes for table `oc_product_version`
--
ALTER TABLE `oc_product_version`
  ADD PRIMARY KEY (`product_version_id`);

--
-- Indexes for table `oc_recurring`
--
ALTER TABLE `oc_recurring`
  ADD PRIMARY KEY (`recurring_id`);

--
-- Indexes for table `oc_recurring_description`
--
ALTER TABLE `oc_recurring_description`
  ADD PRIMARY KEY (`recurring_id`,`language_id`);

--
-- Indexes for table `oc_report`
--
ALTER TABLE `oc_report`
  ADD PRIMARY KEY (`report_time`,`type`);

--
-- Indexes for table `oc_return`
--
ALTER TABLE `oc_return`
  ADD PRIMARY KEY (`return_id`);

--
-- Indexes for table `oc_return_history`
--
ALTER TABLE `oc_return_history`
  ADD PRIMARY KEY (`return_history_id`);

--
-- Indexes for table `oc_review`
--
ALTER TABLE `oc_review`
  ADD PRIMARY KEY (`review_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_seo_url`
--
ALTER TABLE `oc_seo_url`
  ADD PRIMARY KEY (`seo_url_id`),
  ADD KEY `query` (`query`),
  ADD KEY `keyword` (`keyword`);

--
-- Indexes for table `oc_session`
--
ALTER TABLE `oc_session`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `oc_setting`
--
ALTER TABLE `oc_setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `oc_shipping_courier`
--
ALTER TABLE `oc_shipping_courier`
  ADD PRIMARY KEY (`shipping_courier_id`);

--
-- Indexes for table `oc_shopee_product`
--
ALTER TABLE `oc_shopee_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_statistics`
--
ALTER TABLE `oc_statistics`
  ADD PRIMARY KEY (`statistics_id`);

--
-- Indexes for table `oc_store`
--
ALTER TABLE `oc_store`
  ADD PRIMARY KEY (`store_id`);

--
-- Indexes for table `oc_store_receipt`
--
ALTER TABLE `oc_store_receipt`
  ADD PRIMARY KEY (`store_receipt_id`);

--
-- Indexes for table `oc_store_receipt_to_product`
--
ALTER TABLE `oc_store_receipt_to_product`
  ADD PRIMARY KEY (`store_receipt_product_id`);

--
-- Indexes for table `oc_tag`
--
ALTER TABLE `oc_tag`
  ADD PRIMARY KEY (`tag_id`);

--
-- Indexes for table `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  ADD PRIMARY KEY (`tax_class_id`);

--
-- Indexes for table `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  ADD PRIMARY KEY (`tax_rate_id`);

--
-- Indexes for table `oc_tax_rate_to_customer_group`
--
ALTER TABLE `oc_tax_rate_to_customer_group`
  ADD PRIMARY KEY (`tax_rate_id`,`customer_group_id`);

--
-- Indexes for table `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  ADD PRIMARY KEY (`tax_rule_id`);

--
-- Indexes for table `oc_theme`
--
ALTER TABLE `oc_theme`
  ADD PRIMARY KEY (`theme_id`);

--
-- Indexes for table `oc_theme_builder_config`
--
ALTER TABLE `oc_theme_builder_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_timezones`
--
ALTER TABLE `oc_timezones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_translation`
--
ALTER TABLE `oc_translation`
  ADD PRIMARY KEY (`translation_id`);

--
-- Indexes for table `oc_upload`
--
ALTER TABLE `oc_upload`
  ADD PRIMARY KEY (`upload_id`);

--
-- Indexes for table `oc_user`
--
ALTER TABLE `oc_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `oc_user_group`
--
ALTER TABLE `oc_user_group`
  ADD PRIMARY KEY (`user_group_id`);

--
-- Indexes for table `oc_voucher`
--
ALTER TABLE `oc_voucher`
  ADD PRIMARY KEY (`voucher_id`);

--
-- Indexes for table `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  ADD PRIMARY KEY (`voucher_history_id`);

--
-- Indexes for table `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  ADD PRIMARY KEY (`voucher_theme_id`);

--
-- Indexes for table `oc_voucher_theme_description`
--
ALTER TABLE `oc_voucher_theme_description`
  ADD PRIMARY KEY (`voucher_theme_id`,`language_id`);

--
-- Indexes for table `oc_warehouse`
--
ALTER TABLE `oc_warehouse`
  ADD PRIMARY KEY (`warehouse_id`);

--
-- Indexes for table `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  ADD PRIMARY KEY (`weight_class_id`);

--
-- Indexes for table `oc_weight_class_description`
--
ALTER TABLE `oc_weight_class_description`
  ADD PRIMARY KEY (`weight_class_id`,`language_id`);

--
-- Indexes for table `oc_zone`
--
ALTER TABLE `oc_zone`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  ADD PRIMARY KEY (`zone_to_geo_zone_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `oc_address`
--
ALTER TABLE `oc_address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_api`
--
ALTER TABLE `oc_api`
  MODIFY `api_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_api_ip`
--
ALTER TABLE `oc_api_ip`
  MODIFY `api_ip_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_api_session`
--
ALTER TABLE `oc_api_session`
  MODIFY `api_session_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_appstore`
--
ALTER TABLE `oc_appstore`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oc_appstore_setting`
--
ALTER TABLE `oc_appstore_setting`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_app_config_theme`
--
ALTER TABLE `oc_app_config_theme`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `oc_app_flash_sale_product`
--
ALTER TABLE `oc_app_flash_sale_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `oc_attribute`
--
ALTER TABLE `oc_attribute`
  MODIFY `attribute_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  MODIFY `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_banner`
--
ALTER TABLE `oc_banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  MODIFY `banner_image_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_blog`
--
ALTER TABLE `oc_blog`
  MODIFY `blog_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_blog_category`
--
ALTER TABLE `oc_blog_category`
  MODIFY `blog_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oc_blog_tag`
--
ALTER TABLE `oc_blog_tag`
  MODIFY `blog_tag_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_bm_author`
--
ALTER TABLE `oc_bm_author`
  MODIFY `author_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oc_bm_author_description`
--
ALTER TABLE `oc_bm_author_description`
  MODIFY `author_description_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oc_bm_author_group`
--
ALTER TABLE `oc_bm_author_group`
  MODIFY `author_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oc_bm_category`
--
ALTER TABLE `oc_bm_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oc_bm_category_description`
--
ALTER TABLE `oc_bm_category_description`
  MODIFY `category_description_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oc_bm_post`
--
ALTER TABLE `oc_bm_post`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_bm_post_description`
--
ALTER TABLE `oc_bm_post_description`
  MODIFY `post_description_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_bm_review`
--
ALTER TABLE `oc_bm_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_cart`
--
ALTER TABLE `oc_cart`
  MODIFY `cart_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_category`
--
ALTER TABLE `oc_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `oc_collection`
--
ALTER TABLE `oc_collection`
  MODIFY `collection_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `oc_collection_to_collection`
--
ALTER TABLE `oc_collection_to_collection`
  MODIFY `collection_to_collection_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_country`
--
ALTER TABLE `oc_country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT for table `oc_coupon`
--
ALTER TABLE `oc_coupon`
  MODIFY `coupon_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  MODIFY `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  MODIFY `coupon_product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_currency`
--
ALTER TABLE `oc_currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oc_customer`
--
ALTER TABLE `oc_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  MODIFY `customer_activity_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_customer_approval`
--
ALTER TABLE `oc_customer_approval`
  MODIFY `customer_approval_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  MODIFY `customer_group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  MODIFY `customer_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  MODIFY `customer_ip_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  MODIFY `customer_login_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  MODIFY `customer_reward_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_customer_search`
--
ALTER TABLE `oc_customer_search`
  MODIFY `customer_search_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  MODIFY `customer_transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  MODIFY `custom_field_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  MODIFY `custom_field_value_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_demo_data`
--
ALTER TABLE `oc_demo_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `oc_discount`
--
ALTER TABLE `oc_discount`
  MODIFY `discount_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_download`
--
ALTER TABLE `oc_download`
  MODIFY `download_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_event`
--
ALTER TABLE `oc_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `oc_extension`
--
ALTER TABLE `oc_extension`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=271;

--
-- AUTO_INCREMENT for table `oc_extension_install`
--
ALTER TABLE `oc_extension_install`
  MODIFY `extension_install_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `oc_extension_path`
--
ALTER TABLE `oc_extension_path`
  MODIFY `extension_path_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=694;

--
-- AUTO_INCREMENT for table `oc_filter`
--
ALTER TABLE `oc_filter`
  MODIFY `filter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  MODIFY `filter_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  MODIFY `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oc_images`
--
ALTER TABLE `oc_images`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_information`
--
ALTER TABLE `oc_information`
  MODIFY `information_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_language`
--
ALTER TABLE `oc_language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oc_layout`
--
ALTER TABLE `oc_layout`
  MODIFY `layout_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  MODIFY `layout_module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  MODIFY `layout_route_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `oc_length_class`
--
ALTER TABLE `oc_length_class`
  MODIFY `length_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `oc_location`
--
ALTER TABLE `oc_location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  MODIFY `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `oc_marketing`
--
ALTER TABLE `oc_marketing`
  MODIFY `marketing_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_menu`
--
ALTER TABLE `oc_menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `oc_menu_type`
--
ALTER TABLE `oc_menu_type`
  MODIFY `menu_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oc_migration`
--
ALTER TABLE `oc_migration`
  MODIFY `migration_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `oc_modification`
--
ALTER TABLE `oc_modification`
  MODIFY `modification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oc_module`
--
ALTER TABLE `oc_module`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `oc_my_app`
--
ALTER TABLE `oc_my_app`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_novaon_api_credentials`
--
ALTER TABLE `oc_novaon_api_credentials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oc_novaon_group_menu`
--
ALTER TABLE `oc_novaon_group_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `oc_novaon_group_menu_description`
--
ALTER TABLE `oc_novaon_group_menu_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oc_novaon_menu_item`
--
ALTER TABLE `oc_novaon_menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1001;

--
-- AUTO_INCREMENT for table `oc_novaon_menu_item_description`
--
ALTER TABLE `oc_novaon_menu_item_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1002;

--
-- AUTO_INCREMENT for table `oc_novaon_relation_table`
--
ALTER TABLE `oc_novaon_relation_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `oc_novaon_type`
--
ALTER TABLE `oc_novaon_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `oc_novaon_type_description`
--
ALTER TABLE `oc_novaon_type_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `oc_option`
--
ALTER TABLE `oc_option`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `oc_option_value`
--
ALTER TABLE `oc_option_value`
  MODIFY `option_value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `oc_order`
--
ALTER TABLE `oc_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_order_history`
--
ALTER TABLE `oc_order_history`
  MODIFY `order_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_order_option`
--
ALTER TABLE `oc_order_option`
  MODIFY `order_option_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_order_product`
--
ALTER TABLE `oc_order_product`
  MODIFY `order_product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  MODIFY `order_recurring_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  MODIFY `order_recurring_transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_order_shipment`
--
ALTER TABLE `oc_order_shipment`
  MODIFY `order_shipment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_order_status`
--
ALTER TABLE `oc_order_status`
  MODIFY `order_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `oc_order_tag`
--
ALTER TABLE `oc_order_tag`
  MODIFY `order_tag_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_order_total`
--
ALTER TABLE `oc_order_total`
  MODIFY `order_total_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  MODIFY `order_voucher_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_page_contents`
--
ALTER TABLE `oc_page_contents`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1001;

--
-- AUTO_INCREMENT for table `oc_product`
--
ALTER TABLE `oc_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `oc_product_collection`
--
ALTER TABLE `oc_product_collection`
  MODIFY `product_collection_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;

--
-- AUTO_INCREMENT for table `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  MODIFY `product_discount_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_product_image`
--
ALTER TABLE `oc_product_image`
  MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;

--
-- AUTO_INCREMENT for table `oc_product_option`
--
ALTER TABLE `oc_product_option`
  MODIFY `product_option_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  MODIFY `product_option_value_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  MODIFY `product_reward_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_product_special`
--
ALTER TABLE `oc_product_special`
  MODIFY `product_special_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_product_tag`
--
ALTER TABLE `oc_product_tag`
  MODIFY `product_tag_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_product_version`
--
ALTER TABLE `oc_product_version`
  MODIFY `product_version_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_recurring`
--
ALTER TABLE `oc_recurring`
  MODIFY `recurring_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_return`
--
ALTER TABLE `oc_return`
  MODIFY `return_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_return_history`
--
ALTER TABLE `oc_return_history`
  MODIFY `return_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_review`
--
ALTER TABLE `oc_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_seo_url`
--
ALTER TABLE `oc_seo_url`
  MODIFY `seo_url_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2117;

--
-- AUTO_INCREMENT for table `oc_setting`
--
ALTER TABLE `oc_setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6711;

--
-- AUTO_INCREMENT for table `oc_shopee_product`
--
ALTER TABLE `oc_shopee_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_statistics`
--
ALTER TABLE `oc_statistics`
  MODIFY `statistics_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `oc_store`
--
ALTER TABLE `oc_store`
  MODIFY `store_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oc_store_receipt`
--
ALTER TABLE `oc_store_receipt`
  MODIFY `store_receipt_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_store_receipt_to_product`
--
ALTER TABLE `oc_store_receipt_to_product`
  MODIFY `store_receipt_product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_tag`
--
ALTER TABLE `oc_tag`
  MODIFY `tag_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  MODIFY `tax_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  MODIFY `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  MODIFY `tax_rule_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `oc_theme`
--
ALTER TABLE `oc_theme`
  MODIFY `theme_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_theme_builder_config`
--
ALTER TABLE `oc_theme_builder_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=449;

--
-- AUTO_INCREMENT for table `oc_timezones`
--
ALTER TABLE `oc_timezones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `oc_translation`
--
ALTER TABLE `oc_translation`
  MODIFY `translation_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_upload`
--
ALTER TABLE `oc_upload`
  MODIFY `upload_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_user`
--
ALTER TABLE `oc_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oc_user_group`
--
ALTER TABLE `oc_user_group`
  MODIFY `user_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `oc_voucher`
--
ALTER TABLE `oc_voucher`
  MODIFY `voucher_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  MODIFY `voucher_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  MODIFY `voucher_theme_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `oc_warehouse`
--
ALTER TABLE `oc_warehouse`
  MODIFY `warehouse_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1138;

--
-- AUTO_INCREMENT for table `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  MODIFY `weight_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oc_zone`
--
ALTER TABLE `oc_zone`
  MODIFY `zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  MODIFY `zone_to_geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
