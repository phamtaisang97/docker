-- -----------------------------------------------------------

--
-- Database: `chifure`
--

-- -----------------------------------------------------------

SET sql_mode = '';
set SQL_SAFE_UPDATES = 0;
--
-- Table structure for table `cfr_address`
--

CREATE TABLE `cfr_address` (
  `address_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(40) NOT NULL,
  `address` varchar(128) NOT NULL,
  `city` varchar(128) DEFAULT NULL,
  `district` varchar(128) DEFAULT NULL,
  `wards` varchar(128) DEFAULT NULL,
  `phone` varchar(32) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_api`
--

CREATE TABLE `cfr_api` (
  `api_id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `key` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_api_ip`
--

CREATE TABLE `cfr_api_ip` (
  `api_ip_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_api_session`
--

CREATE TABLE `cfr_api_session` (
  `api_session_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_attribute`
--

CREATE TABLE `cfr_attribute` (
  `attribute_id` int(11) NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_attribute_description`
--

CREATE TABLE `cfr_attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_attribute_group`
--

CREATE TABLE `cfr_attribute_group` (
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_attribute_group_description`
--

CREATE TABLE `cfr_attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_banner`
--

CREATE TABLE `cfr_banner` (
  `banner_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_banner_image`
--

CREATE TABLE `cfr_banner_image` (
  `banner_image_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_cart`
--

CREATE TABLE `cfr_cart` (
  `cart_id` int(11) UNSIGNED NOT NULL,
  `api_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `option` text NOT NULL,
  `quantity` int(5) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_category`
--

CREATE TABLE `cfr_category` (
  `category_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_category_description`
--

CREATE TABLE `cfr_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_category_filter`
--

CREATE TABLE `cfr_category_filter` (
  `category_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_category_path`
--

CREATE TABLE `cfr_category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_category_to_layout`
--

CREATE TABLE `cfr_category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_category_to_store`
--

CREATE TABLE `cfr_category_to_store` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_collection`
--

CREATE TABLE `cfr_collection` (
  `collection_id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `sort_order` int(11) DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `product_type_sort` varchar(256) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_collection_description`
--

CREATE TABLE `cfr_collection_description` (
  `collection_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_country`
--

CREATE TABLE `cfr_country` (
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_country`
--

INSERT INTO `cfr_country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1),
(2, 'Albania', 'AL', 'ALB', '', 0, 1),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1),
(5, 'Andorra', 'AD', 'AND', '', 0, 1),
(6, 'Angola', 'AO', 'AGO', '', 0, 1),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1),
(11, 'Armenia', 'AM', 'ARM', '', 0, 1),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1),
(13, 'Australia', 'AU', 'AUS', '', 0, 1),
(14, 'Austria', 'AT', 'AUT', '', 0, 1),
(15, 'Azerbaijan', 'AZ', 'AZE', '', 0, 1),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1),
(20, 'Belarus', 'BY', 'BLR', '', 0, 1),
(21, 'Belgium', 'BE', 'BEL', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', '', 0, 1),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1),
(38, 'Canada', 'CA', 'CAN', '', 0, 1),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1),
(42, 'Chad', 'TD', 'TCD', '', 0, 1),
(43, 'Chile', 'CL', 'CHL', '', 0, 1),
(44, 'China', 'CN', 'CHN', '', 0, 1),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1),
(47, 'Colombia', 'CO', 'COL', '', 0, 1),
(48, 'Comoros', 'KM', 'COM', '', 0, 1),
(49, 'Congo', 'CG', 'COG', '', 0, 1),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1),
(52, 'Cote D\'Ivoire', 'CI', 'CIV', '', 0, 1),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1),
(61, 'East Timor', 'TL', 'TLS', '', 0, 1),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1),
(67, 'Estonia', 'EE', 'EST', '', 0, 1),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1),
(72, 'Finland', 'FI', 'FIN', '', 0, 1),
(74, 'France, Metropolitan', 'FR', 'FRA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1),
(80, 'Georgia', 'GE', 'GEO', '', 0, 1),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1),
(84, 'Greece', 'GR', 'GRC', '', 0, 1),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1),
(88, 'Guam', 'GU', 'GUM', '', 0, 1),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1),
(91, 'Guinea-Bissau', 'GW', 'GNB', '', 0, 1),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1),
(95, 'Honduras', 'HN', 'HND', '', 0, 1),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1),
(99, 'India', 'IN', 'IND', '', 0, 1),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1),
(104, 'Israel', 'IL', 'ISR', '', 0, 1),
(105, 'Italy', 'IT', 'ITA', '', 0, 1),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1),
(107, 'Japan', 'JP', 'JPN', '', 0, 1),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', '', 0, 1),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1),
(113, 'South Korea', 'KR', 'KOR', '', 0, 1),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', '', 0, 1),
(116, 'Lao People\'s Democratic Republic', 'LA', 'LAO', '', 0, 1),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1),
(125, 'Macau', 'MO', 'MAC', '', 0, 1),
(126, 'FYROM', 'MK', 'MKD', '', 0, 1),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1),
(131, 'Mali', 'ML', 'MLI', '', 0, 1),
(132, 'Malta', 'MT', 'MLT', '', 0, 1),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1),
(140, 'Moldova, Republic of', 'MD', 'MDA', '', 0, 1),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1),
(155, 'Niger', 'NE', 'NER', '', 0, 1),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1),
(157, 'Niue', 'NU', 'NIU', '', 0, 1),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1),
(160, 'Norway', 'NO', 'NOR', '', 0, 1),
(161, 'Oman', 'OM', 'OMN', '', 0, 1),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1),
(163, 'Palau', 'PW', 'PLW', '', 0, 1),
(164, 'Panama', 'PA', 'PAN', '', 0, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1),
(167, 'Peru', 'PE', 'PER', '', 0, 1),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1),
(170, 'Poland', 'PL', 'POL', '', 0, 1),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1),
(174, 'Reunion', 'RE', 'REU', '', 0, 1),
(175, 'Romania', 'RO', 'ROM', '', 0, 1),
(176, 'Russian Federation', 'RU', 'RUS', '', 0, 1),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1),
(195, 'Spain', 'ES', 'ESP', '', 0, 1),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1),
(203, 'Sweden', 'SE', 'SWE', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1),
(207, 'Tajikistan', 'TJ', 'TJK', '', 0, 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1),
(209, 'Thailand', 'TH', 'THA', '', 0, 1),
(210, 'Togo', 'TG', 'TGO', '', 0, 1),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1),
(212, 'Tonga', 'TO', 'TON', '', 0, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1),
(216, 'Turkmenistan', 'TM', 'TKM', '', 0, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1),
(220, 'Ukraine', 'UA', 'UKR', '', 0, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1),
(226, 'Uzbekistan', 'UZ', 'UZB', '', 0, 1),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1),
(242, 'Montenegro', 'ME', 'MNE', '', 0, 1),
(243, 'Serbia', 'RS', 'SRB', '', 0, 1),
(244, 'Aaland Islands', 'AX', 'ALA', '', 0, 1),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '', 0, 1),
(246, 'Curacao', 'CW', 'CUW', '', 0, 1),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', '', 0, 1),
(248, 'South Sudan', 'SS', 'SSD', '', 0, 1),
(249, 'St. Barthelemy', 'BL', 'BLM', '', 0, 1),
(250, 'St. Martin (French part)', 'MF', 'MAF', '', 0, 1),
(251, 'Canary Islands', 'IC', 'ICA', '', 0, 1),
(252, 'Ascension Island (British)', 'AC', 'ASC', '', 0, 1),
(253, 'Kosovo, Republic of', 'XK', 'UNK', '', 0, 1),
(254, 'Isle of Man', 'IM', 'IMN', '', 0, 1),
(255, 'Tristan da Cunha', 'TA', 'SHN', '', 0, 1),
(256, 'Guernsey', 'GG', 'GGY', '', 0, 1),
(257, 'Jersey', 'JE', 'JEY', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cfr_coupon`
--

CREATE TABLE `cfr_coupon` (
  `coupon_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(20) NOT NULL,
  `type` char(1) NOT NULL,
  `discount` decimal(15,4) NOT NULL,
  `logged` tinyint(1) NOT NULL,
  `shipping` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `uses_total` int(11) NOT NULL,
  `uses_customer` varchar(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_coupon_category`
--

CREATE TABLE `cfr_coupon_category` (
  `coupon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_coupon_history`
--

CREATE TABLE `cfr_coupon_history` (
  `coupon_history_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_coupon_product`
--

CREATE TABLE `cfr_coupon_product` (
  `coupon_product_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_currency`
--

CREATE TABLE `cfr_currency` (
  `currency_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` double(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_currency`
--

INSERT INTO `cfr_currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(1, 'Pound Sterling', 'GBP', '£', '', '2', 29720.44000000, 1, '2019-01-09 16:40:33'),
(4, 'US Dollar', 'USD', '$', '', '2', 23245.00000000, 1, '2019-01-09 16:45:56'),
(3, 'Euro', 'EUR', '', '€', '2', 27137.74000000, 1, '2019-01-09 16:39:38'),
(2, 'Vietnam đồng', 'VND', '', 'đ', '0', 1.00000000, 1, '2019-05-16 08:11:36');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_customer`
--

CREATE TABLE `cfr_customer` (
  `customer_id` int(11) NOT NULL,
  `customer_group_id` int(11) DEFAULT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) DEFAULT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) DEFAULT NULL,
  `password` varchar(40) NOT NULL,
  `password_temp` varchar(100) DEFAULT NULL,
  `salt` varchar(9) NOT NULL,
  `salt_temp` varchar(9) DEFAULT NULL,
  `cart` text,
  `wishlist` text,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `customer_source` varchar(128) DEFAULT NULL,
  `note` text,
  `custom_field` text,
  `ip` varchar(40) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `safe` tinyint(1) DEFAULT NULL,
  `token` text,
  `code` varchar(40) DEFAULT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_customer_activity`
--

CREATE TABLE `cfr_customer_activity` (
  `customer_activity_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_customer_affiliate`
--

CREATE TABLE `cfr_customer_affiliate` (
  `customer_id` int(11) NOT NULL,
  `company` varchar(40) NOT NULL,
  `website` varchar(255) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `commission` decimal(4,2) NOT NULL DEFAULT '0.00',
  `tax` varchar(64) NOT NULL,
  `payment` varchar(6) NOT NULL,
  `cheque` varchar(100) NOT NULL,
  `paypal` varchar(64) NOT NULL,
  `bank_name` varchar(64) NOT NULL,
  `bank_branch_number` varchar(64) NOT NULL,
  `bank_swift_code` varchar(64) NOT NULL,
  `bank_account_name` varchar(64) NOT NULL,
  `bank_account_number` varchar(64) NOT NULL,
  `custom_field` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_customer_approval`
--

CREATE TABLE `cfr_customer_approval` (
  `customer_approval_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `type` varchar(9) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_customer_group`
--

CREATE TABLE `cfr_customer_group` (
  `customer_group_id` int(11) NOT NULL,
  `approval` int(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_customer_group_description`
--

CREATE TABLE `cfr_customer_group_description` (
  `customer_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_customer_history`
--

CREATE TABLE `cfr_customer_history` (
  `customer_history_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_customer_ip`
--

CREATE TABLE `cfr_customer_ip` (
  `customer_ip_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_customer_login`
--

CREATE TABLE `cfr_customer_login` (
  `customer_login_id` int(11) NOT NULL,
  `email` varchar(96) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `total` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_customer_online`
--

CREATE TABLE `cfr_customer_online` (
  `ip` varchar(40) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `referer` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_customer_reward`
--

CREATE TABLE `cfr_customer_reward` (
  `customer_reward_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_customer_search`
--

CREATE TABLE `cfr_customer_search` (
  `customer_search_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category` tinyint(1) NOT NULL,
  `description` tinyint(1) NOT NULL,
  `products` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_customer_transaction`
--

CREATE TABLE `cfr_customer_transaction` (
  `customer_transaction_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_customer_wishlist`
--

CREATE TABLE `cfr_customer_wishlist` (
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_custom_field`
--

CREATE TABLE `cfr_custom_field` (
  `custom_field_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `validation` varchar(255) NOT NULL,
  `location` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_custom_field_customer_group`
--

CREATE TABLE `cfr_custom_field_customer_group` (
  `custom_field_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_custom_field_description`
--

CREATE TABLE `cfr_custom_field_description` (
  `custom_field_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_custom_field_value`
--

CREATE TABLE `cfr_custom_field_value` (
  `custom_field_value_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_custom_field_value_description`
--

CREATE TABLE `cfr_custom_field_value_description` (
  `custom_field_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_download`
--

CREATE TABLE `cfr_download` (
  `download_id` int(11) NOT NULL,
  `filename` varchar(160) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_download_description`
--

CREATE TABLE `cfr_download_description` (
  `download_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_event`
--

CREATE TABLE `cfr_event` (
  `event_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `trigger` text NOT NULL,
  `action` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_event`
--

INSERT INTO `cfr_event` (`event_id`, `code`, `trigger`, `action`, `status`, `sort_order`) VALUES
(1, 'activity_customer_add', 'catalog/model/account/customer/addCustomer/after', 'event/activity/addCustomer', 1, 0),
(2, 'activity_customer_edit', 'catalog/model/account/customer/editCustomer/after', 'event/activity/editCustomer', 1, 0),
(3, 'activity_customer_password', 'catalog/model/account/customer/editPassword/after', 'event/activity/editPassword', 1, 0),
(4, 'activity_customer_forgotten', 'catalog/model/account/customer/editCode/after', 'event/activity/forgotten', 1, 0),
(5, 'activity_transaction', 'catalog/model/account/customer/addTransaction/after', 'event/activity/addTransaction', 1, 0),
(6, 'activity_customer_login', 'catalog/model/account/customer/deleteLoginAttempts/after', 'event/activity/login', 1, 0),
(7, 'activity_address_add', 'catalog/model/account/address/addAddress/after', 'event/activity/addAddress', 1, 0),
(8, 'activity_address_edit', 'catalog/model/account/address/editAddress/after', 'event/activity/editAddress', 1, 0),
(9, 'activity_address_delete', 'catalog/model/account/address/deleteAddress/after', 'event/activity/deleteAddress', 1, 0),
(10, 'activity_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'event/activity/addAffiliate', 1, 0),
(11, 'activity_affiliate_edit', 'catalog/model/account/customer/editAffiliate/after', 'event/activity/editAffiliate', 1, 0),
(12, 'activity_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'event/activity/addOrderHistory', 1, 0),
(13, 'activity_return_add', 'catalog/model/account/return/addReturn/after', 'event/activity/addReturn', 1, 0),
(14, 'mail_transaction', 'catalog/model/account/customer/addTransaction/after', 'mail/transaction', 1, 0),
(15, 'mail_forgotten', 'catalog/model/account/customer/editCode/after', 'mail/forgotten', 1, 0),
(16, 'mail_customer_add', 'catalog/model/account/customer/addCustomer/after', 'mail/register', 1, 0),
(17, 'mail_customer_alert', 'catalog/model/account/customer/addCustomer/after', 'mail/register/alert', 1, 0),
(18, 'mail_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate', 1, 0),
(19, 'mail_affiliate_alert', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate/alert', 1, 0),
(20, 'mail_voucher', 'catalog/model/checkout/order/addOrderHistory/after', 'extension/total/voucher/send', 1, 0),
(21, 'mail_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order', 1, 0),
(22, 'mail_order_alert', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order/alert', 1, 0),
(23, 'statistics_review_add', 'catalog/model/catalog/review/addReview/after', 'event/statistics/addReview', 1, 0),
(24, 'statistics_return_add', 'catalog/model/account/return/addReturn/after', 'event/statistics/addReturn', 1, 0),
(25, 'statistics_order_history', 'catalog/model/checkout/order/addOrderHistory/after', 'event/statistics/addOrderHistory', 1, 0),
(26, 'admin_mail_affiliate_approve', 'admin/model/customer/customer_approval/approveAffiliate/after', 'mail/affiliate/approve', 1, 0),
(27, 'admin_mail_affiliate_deny', 'admin/model/customer/customer_approval/denyAffiliate/after', 'mail/affiliate/deny', 1, 0),
(28, 'admin_mail_customer_approve', 'admin/model/customer/customer_approval/approveCustomer/after', 'mail/customer/approve', 1, 0),
(29, 'admin_mail_customer_deny', 'admin/model/customer/customer_approval/denyCustomer/after', 'mail/customer/deny', 1, 0),
(30, 'admin_mail_reward', 'admin/model/customer/customer/addReward/after', 'mail/reward', 1, 0),
(31, 'admin_mail_transaction', 'admin/model/customer/customer/addTransaction/after', 'mail/transaction', 1, 0),
(32, 'admin_mail_return', 'admin/model/sale/return/addReturn/after', 'mail/return', 1, 0),
(33, 'admin_mail_forgotten', 'admin/model/user/user/editCode/after', 'mail/forgotten', 1, 0),
(34, 'add order', 'admin/model/sale/order/addOrder/after', 'event/statistics/createOrder', 1, 0),
(35, 'edit order', 'admin/model/sale/order/updateProductOrder/after', 'event/statistics/editOrder', 1, 0),
(36, 'change status order', 'admin/model/sale/order/updateStatus/after', 'event/statistics/changeStatus', 1, 0),
(37, 'delete order', 'admin/model/sale/order/deleteOrderById/after', 'event/statistics/deleteOrder', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cfr_extension`
--

CREATE TABLE `cfr_extension` (
  `extension_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_extension`
--

INSERT INTO `cfr_extension` (`extension_id`, `type`, `code`) VALUES
(1, 'payment', 'cod'),
(2, 'total', 'shipping'),
(3, 'total', 'sub_total'),
(4, 'total', 'tax'),
(5, 'total', 'total'),
(6, 'module', 'banner'),
(7, 'module', 'carousel'),
(8, 'total', 'credit'),
(9, 'shipping', 'flat'),
(10, 'total', 'handling'),
(11, 'total', 'low_order_fee'),
(12, 'total', 'coupon'),
(13, 'module', 'category'),
(14, 'module', 'account'),
(15, 'total', 'reward'),
(16, 'total', 'voucher'),
(17, 'payment', 'free_checkout'),
(18, 'module', 'featured'),
(19, 'module', 'slideshow'),
(21, 'dashboard', 'activity'),
(22, 'dashboard', 'sale'),
(23, 'dashboard', 'recent'),
(24, 'dashboard', 'order'),
(25, 'dashboard', 'online'),
(26, 'dashboard', 'map'),
(27, 'dashboard', 'customer'),
(28, 'dashboard', 'chart'),
(29, 'report', 'sale_coupon'),
(31, 'report', 'customer_search'),
(32, 'report', 'customer_transaction'),
(33, 'report', 'product_purchased'),
(34, 'report', 'product_viewed'),
(35, 'report', 'sale_return'),
(36, 'report', 'sale_order'),
(37, 'report', 'sale_shipping'),
(38, 'report', 'sale_tax'),
(39, 'report', 'customer_activity'),
(40, 'report', 'customer_order'),
(41, 'report', 'customer_reward'),
(42, 'module', 'theme_builder_config'),
(159, 'theme', 'fashion_luvibee_2');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_extension_install`
--

CREATE TABLE `cfr_extension_install` (
  `extension_install_id` int(11) NOT NULL,
  `extension_download_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_extension_path`
--

CREATE TABLE `cfr_extension_path` (
  `extension_path_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_filter`
--

CREATE TABLE `cfr_filter` (
  `filter_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_filter`
--

INSERT INTO `cfr_filter` (`filter_id`, `filter_group_id`, `sort_order`) VALUES
(1, 1, 0),
(2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cfr_filter_description`
--

CREATE TABLE `cfr_filter_description` (
  `filter_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_filter_description`
--

INSERT INTO `cfr_filter_description` (`filter_id`, `language_id`, `filter_group_id`, `name`) VALUES
(1, 2, 1, 'Iphone'),
(1, 1, 1, 'Iphone'),
(2, 2, 1, 'Oppo'),
(2, 1, 1, 'Oppo');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_filter_group`
--

CREATE TABLE `cfr_filter_group` (
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_filter_group`
--

INSERT INTO `cfr_filter_group` (`filter_group_id`, `sort_order`) VALUES
(1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cfr_filter_group_description`
--

CREATE TABLE `cfr_filter_group_description` (
  `filter_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_filter_group_description`
--

INSERT INTO `cfr_filter_group_description` (`filter_group_id`, `language_id`, `name`) VALUES
(1, 1, 'filter group'),
(1, 2, 'Nhóm bộ lọc');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_geo_zone`
--

CREATE TABLE `cfr_geo_zone` (
  `geo_zone_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_geo_zone`
--

INSERT INTO `cfr_geo_zone` (`geo_zone_id`, `name`, `description`, `date_added`, `date_modified`) VALUES
(3, 'UK VAT Zone', 'UK VAT', '2009-01-06 23:26:25', '2010-02-26 22:33:24'),
(4, 'UK Shipping', 'UK Shipping Zones', '2009-06-23 01:14:53', '2010-12-15 15:18:13');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_information`
--

CREATE TABLE `cfr_information` (
  `information_id` int(11) NOT NULL,
  `bottom` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_information_description`
--

CREATE TABLE `cfr_information_description` (
  `information_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` mediumtext NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_information_to_layout`
--

CREATE TABLE `cfr_information_to_layout` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_information_to_store`
--

CREATE TABLE `cfr_information_to_store` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_language`
--

CREATE TABLE `cfr_language` (
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_language`
--

INSERT INTO `cfr_language` (`language_id`, `name`, `code`, `locale`, `image`, `directory`, `sort_order`, `status`) VALUES
(1, 'English', 'en-gb', 'en-US,en_US.UTF-8,en_US,en-gb,english', 'en-gb.png', 'english', 2, 0),
(2, 'Vietnamese', 'vi-vn', 'vi-VN,vi_VN.UTF-8,vi_VN,vi-vn,vietnamese', 'vi-vn.png', 'vietnamese', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cfr_layout`
--

CREATE TABLE `cfr_layout` (
  `layout_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_layout`
--

INSERT INTO `cfr_layout` (`layout_id`, `name`) VALUES
(1, 'Home'),
(2, 'Product'),
(3, 'Category'),
(4, 'Default'),
(5, 'Manufacturer'),
(6, 'Account'),
(7, 'Checkout'),
(8, 'Contact'),
(9, 'Sitemap'),
(10, 'Affiliate'),
(11, 'Information'),
(12, 'Compare'),
(13, 'Search');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_layout_module`
--

CREATE TABLE `cfr_layout_module` (
  `layout_module_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `position` varchar(14) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_layout_module`
--

INSERT INTO `cfr_layout_module` (`layout_module_id`, `layout_id`, `code`, `position`, `sort_order`) VALUES
(2, 4, '0', 'content_top', 0),
(3, 4, '0', 'content_top', 1),
(20, 5, '0', 'column_left', 2),
(69, 10, 'account', 'column_right', 1),
(68, 6, 'account', 'column_right', 1),
(67, 1, 'carousel.29', 'content_top', 3),
(66, 1, 'slideshow.27', 'content_top', 1),
(65, 1, 'featured.28', 'content_top', 2),
(72, 3, 'category', 'column_left', 1),
(73, 3, 'banner.30', 'column_left', 2);

-- --------------------------------------------------------

--
-- Table structure for table `cfr_layout_route`
--

CREATE TABLE `cfr_layout_route` (
  `layout_route_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_layout_route`
--

INSERT INTO `cfr_layout_route` (`layout_route_id`, `layout_id`, `store_id`, `route`) VALUES
(38, 6, 0, 'account/%'),
(17, 10, 0, 'affiliate/%'),
(44, 3, 0, 'product/category'),
(42, 1, 0, 'common/home'),
(20, 2, 0, 'product/product'),
(24, 11, 0, 'information/information'),
(23, 7, 0, 'checkout/%'),
(31, 8, 0, 'information/contact'),
(32, 9, 0, 'information/sitemap'),
(34, 4, 0, ''),
(45, 5, 0, 'product/manufacturer'),
(52, 12, 0, 'product/compare'),
(53, 13, 0, 'product/search');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_length_class`
--

CREATE TABLE `cfr_length_class` (
  `length_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_length_class`
--

INSERT INTO `cfr_length_class` (`length_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '10.00000000'),
(3, '0.39370000');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_length_class_description`
--

CREATE TABLE `cfr_length_class_description` (
  `length_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_length_class_description`
--

INSERT INTO `cfr_length_class_description` (`length_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Centimeter', 'cm'),
(2, 1, 'Millimeter', 'mm'),
(3, 1, 'Inch', 'in'),
(1, 2, 'Centimeter', 'cm'),
(2, 2, 'Millimeter', 'mm'),
(3, 2, 'Inch', 'in');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_location`
--

CREATE TABLE `cfr_location` (
  `location_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `address` text NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `geocode` varchar(32) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `open` text NOT NULL,
  `comment` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_manufacturer`
--

CREATE TABLE `cfr_manufacturer` (
  `manufacturer_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_manufacturer_to_store`
--

CREATE TABLE `cfr_manufacturer_to_store` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_marketing`
--

CREATE TABLE `cfr_marketing` (
  `marketing_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(64) NOT NULL,
  `clicks` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_modification`
--

CREATE TABLE `cfr_modification` (
  `modification_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(64) NOT NULL,
  `author` varchar(64) NOT NULL,
  `version` varchar(32) NOT NULL,
  `link` varchar(255) NOT NULL,
  `xml` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_module`
--

CREATE TABLE `cfr_module` (
  `module_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(32) NOT NULL,
  `setting` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_module`
--

INSERT INTO `cfr_module` (`module_id`, `name`, `code`, `setting`) VALUES
(30, 'Category', 'banner', '{"name":"Category","banner_id":"6","width":"182","height":"182","status":"1"}'),
(29, 'Home Page', 'carousel', '{"name":"Home Page","banner_id":"8","width":"130","height":"100","status":"1"}'),
(28, 'Home Page', 'featured', '{"name":"Home Page","product":["43","40","42","30"],"limit":"4","width":"200","height":"200","status":"1"}'),
(27, 'Home Page', 'slideshow', '{"name":"Home Page","banner_id":"7","width":"1140","height":"380","status":"1"}'),
(31, 'Banner 1', 'banner', '{"name":"Banner 1","banner_id":"6","width":"182","height":"182","status":"1"}');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_novaon_group_menu`
--

CREATE TABLE `cfr_novaon_group_menu` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cfr_novaon_group_menu`
--

INSERT INTO `cfr_novaon_group_menu` (`id`, `parent_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 1, '2019-01-09 16:05:17', '2019-01-09 16:05:17'),
(2, 0, 1, '2019-01-09 16:06:51', '2019-01-09 16:06:51'),
(3, 0, 1, '2019-01-09 16:09:21', '2019-01-09 16:09:21'),
(4, 0, 1, '2019-01-09 16:11:17', '2019-01-09 16:11:17'),
(5, 0, 1, '2019-01-09 16:12:50', '2019-01-09 16:12:50');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_novaon_group_menu_description`
--

CREATE TABLE `cfr_novaon_group_menu_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `group_menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cfr_novaon_group_menu_description`
--

INSERT INTO `cfr_novaon_group_menu_description` (`id`, `language_id`, `name`, `group_menu_id`) VALUES
(1, 2, 'Menu chính', 1),
(2, 1, 'Menu chính', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cfr_novaon_menu_item`
--

CREATE TABLE `cfr_novaon_menu_item` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `group_menu_id` int(11) NOT NULL COMMENT 'id của bảng group_menu',
  `status` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cfr_novaon_menu_item`
--

INSERT INTO `cfr_novaon_menu_item` (`id`, `parent_id`, `group_menu_id`, `status`, `position`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 1, 0, '2019-01-09 16:05:17', '2019-01-09 16:05:17'),
(2, 0, 1, 1, 0, '2019-01-09 16:05:17', '2019-01-09 16:05:17'),
(3, 0, 1, 1, 0, '2019-01-09 16:05:18', '2019-01-09 16:05:18');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_novaon_menu_item_description`
--

CREATE TABLE `cfr_novaon_menu_item_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `menu_item_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cfr_novaon_menu_item_description`
--

INSERT INTO `cfr_novaon_menu_item_description` (`id`, `language_id`, `name`, `menu_item_id`) VALUES
(1, 2, 'Trang chủ', 1),
(2, 1, 'Trang chủ', 1),
(3, 2, 'Sản phẩm', 2),
(4, 1, 'Sản phẩm', 2),
(5, 2, 'Liên hệ', 3),
(6, 1, 'Liên hệ', 3);

-- --------------------------------------------------------

--
-- Table structure for table `cfr_novaon_relation_table`
--

CREATE TABLE `cfr_novaon_relation_table` (
  `id` int(11) NOT NULL,
  `main_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên bảng chính(ví dụ group_menu)',
  `main_id` int(11) NOT NULL COMMENT 'id bảng chính, ví dụ group_menu_id =1)',
  `child_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên bảng con ví dụ menu_item',
  `child_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL COMMENT 'type_id =1 là sản phẩm type_id =2 là danh mục sản phẩm type_id =3 là bài viết type_id =4 là danh mục bài viết type_id =5 là url',
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `redirect` int(11) NOT NULL DEFAULT '0' COMMENT 'nếu = 1 là khi click vào sẽ redirect đến đâu, 0 là dùng dể hover vào show ra các tphan con',
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `weight_number` int(11) NOT NULL COMMENT 'trọng số hiển thị'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cfr_novaon_relation_table`
--

INSERT INTO `cfr_novaon_relation_table` (`id`, `main_name`, `main_id`, `child_name`, `child_id`, `type_id`, `url`, `redirect`, `title`, `weight_number`) VALUES
(1, 'menu_item', 1, '', 0, 5, '?route=common/home', 1, '', 0),
(2, 'menu_item', 2, '', 0, 5, '?route=common/shop', 1, '', 0),
(3, 'menu_item', 3, '', 0, 5, '?route=contact/contact', 1, '', 0),
(4, 'group_menu', 1, 'menu_item', 1, 5, '', 0, '', 0),
(5, 'group_menu', 1, 'menu_item', 2, 5, '', 0, '', 0),
(6, 'group_menu', 1, 'menu_item', 3, 5, '', 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cfr_novaon_type`
--

CREATE TABLE `cfr_novaon_type` (
  `id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `weight_number` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cfr_novaon_type`
--

INSERT INTO `cfr_novaon_type` (`id`, `status`, `weight_number`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `cfr_novaon_type_description`
--

CREATE TABLE `cfr_novaon_type_description` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cfr_novaon_type_description`
--

INSERT INTO `cfr_novaon_type_description` (`id`, `type_id`, `language_id`, `name`) VALUES
(1, 1, 1, 'Product'),
(2, 1, 2, 'Sản phẩm'),
(3, 2, 1, 'Category'),
(4, 2, 2, 'Danh mục sản phẩm'),
(5, 3, 1, 'Blog'),
(6, 3, 2, 'Bài viết'),
(7, 4, 1, 'Blog category'),
(8, 4, 2, 'Danh mục bài viết'),
(9, 5, 1, 'Url'),
(10, 5, 2, 'Url');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_option`
--

CREATE TABLE `cfr_option` (
  `option_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_option`
--

INSERT INTO `cfr_option` (`option_id`, `type`, `sort_order`) VALUES
(1, 'radio', 1),
(2, 'checkbox', 2),
(4, 'text', 3),
(5, 'select', 4),
(6, 'textarea', 5),
(7, 'file', 6),
(8, 'date', 7),
(9, 'time', 8),
(10, 'datetime', 9),
(11, 'select', 10),
(12, 'date', 11);

-- --------------------------------------------------------

--
-- Table structure for table `cfr_option_description`
--

CREATE TABLE `cfr_option_description` (
  `option_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_option_description`
--

INSERT INTO `cfr_option_description` (`option_id`, `language_id`, `name`) VALUES
(1, 1, 'Radio'),
(2, 1, 'Checkbox'),
(4, 1, 'Text'),
(6, 1, 'Textarea'),
(8, 1, 'Date'),
(7, 1, 'File'),
(5, 1, 'Select'),
(9, 1, 'Time'),
(10, 1, 'Date &amp; Time'),
(12, 1, 'Delivery Date'),
(11, 1, 'Size');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_option_value`
--

CREATE TABLE `cfr_option_value` (
  `option_value_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_option_value`
--

INSERT INTO `cfr_option_value` (`option_value_id`, `option_id`, `image`, `sort_order`) VALUES
(43, 1, '', 3),
(32, 1, '', 1),
(45, 2, '', 4),
(44, 2, '', 3),
(42, 5, '', 4),
(41, 5, '', 3),
(39, 5, '', 1),
(40, 5, '', 2),
(31, 1, '', 2),
(23, 2, '', 1),
(24, 2, '', 2),
(46, 11, '', 1),
(47, 11, '', 2),
(48, 11, '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `cfr_option_value_description`
--

CREATE TABLE `cfr_option_value_description` (
  `option_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_option_value_description`
--

INSERT INTO `cfr_option_value_description` (`option_value_id`, `language_id`, `option_id`, `name`) VALUES
(43, 1, 1, 'Large'),
(32, 1, 1, 'Small'),
(45, 1, 2, 'Checkbox 4'),
(44, 1, 2, 'Checkbox 3'),
(31, 1, 1, 'Medium'),
(42, 1, 5, 'Yellow'),
(41, 1, 5, 'Green'),
(39, 1, 5, 'Red'),
(40, 1, 5, 'Blue'),
(23, 1, 2, 'Checkbox 1'),
(24, 1, 2, 'Checkbox 2'),
(48, 1, 11, 'Large'),
(47, 1, 11, 'Medium'),
(46, 1, 11, 'Small');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_order`
--

CREATE TABLE `cfr_order` (
  `order_id` int(11) NOT NULL,
  `order_code` varchar(50) DEFAULT NULL,
  `invoice_no` int(11) NOT NULL DEFAULT '0',
  `invoice_prefix` varchar(26) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `store_name` varchar(64) NOT NULL,
  `store_url` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `fullname` varchar(255) DEFAULT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `order_payment` int(11) DEFAULT NULL,
  `order_transfer` int(11) DEFAULT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `custom_field` text NOT NULL,
  `payment_firstname` varchar(32) NOT NULL,
  `payment_lastname` varchar(32) NOT NULL,
  `payment_company` varchar(60) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(10) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `payment_country_id` int(11) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_zone_id` int(11) NOT NULL,
  `payment_address_format` text NOT NULL,
  `payment_custom_field` text NOT NULL,
  `payment_method` varchar(128) NOT NULL,
  `payment_code` varchar(128) NOT NULL,
  `payment_status` tinyint(4) NOT NULL DEFAULT '0',
  `shipping_firstname` varchar(32) NOT NULL,
  `shipping_lastname` varchar(32) NOT NULL,
  `shipping_company` varchar(40) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(10) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `shipping_ward_code` varchar(50) DEFAULT NULL,
  `shipping_district_code` varchar(50) DEFAULT NULL,
  `shipping_province_code` varchar(50) DEFAULT NULL,
  `shipping_country_id` int(11) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_zone_id` int(11) NOT NULL,
  `shipping_address_format` text NOT NULL,
  `shipping_custom_field` text NOT NULL,
  `shipping_method` varchar(128) NOT NULL,
  `shipping_method_value` int(11) DEFAULT NULL,
  `shipping_fee` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `shipping_code` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `order_cancel_comment` varchar(500) DEFAULT NULL,
  `total` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `marketing_id` int(11) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL DEFAULT '1.00000000',
  `ip` varchar(40) NOT NULL,
  `forwarded_ip` varchar(40) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `accept_language` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_order_history`
--

CREATE TABLE `cfr_order_history` (
  `order_history_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(256) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_order_option`
--

CREATE TABLE `cfr_order_option` (
  `order_option_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_order_product`
--

CREATE TABLE `cfr_order_product` (
  `order_product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_version_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `reward` int(8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_order_recurring`
--

CREATE TABLE `cfr_order_recurring` (
  `order_recurring_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `recurring_name` varchar(255) NOT NULL,
  `recurring_description` varchar(255) NOT NULL,
  `recurring_frequency` varchar(25) NOT NULL,
  `recurring_cycle` smallint(6) NOT NULL,
  `recurring_duration` smallint(6) NOT NULL,
  `recurring_price` decimal(10,4) NOT NULL,
  `trial` tinyint(1) NOT NULL,
  `trial_frequency` varchar(25) NOT NULL,
  `trial_cycle` smallint(6) NOT NULL,
  `trial_duration` smallint(6) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_order_recurring_transaction`
--

CREATE TABLE `cfr_order_recurring_transaction` (
  `order_recurring_transaction_id` int(11) NOT NULL,
  `order_recurring_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `amount` decimal(10,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_order_shipment`
--

CREATE TABLE `cfr_order_shipment` (
  `order_shipment_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `shipping_courier_id` varchar(255) NOT NULL DEFAULT '',
  `tracking_number` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_order_status`
--

CREATE TABLE `cfr_order_status` (
  `order_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_order_status`
--

INSERT INTO `cfr_order_status` (`order_status_id`, `language_id`, `name`) VALUES
(1, 1, 'Draft'),
(2, 1, 'Processing'),
(3, 1, 'Delivering'),
(5, 1, 'Canceled'),
(4, 1, 'Complete'),
(6, 2, 'Nháp'),
(7, 2, 'Đang xử lý'),
(8, 2, 'Đang giao hàng '),
(9, 2, 'Hoàn thành'),
(10, 2, 'Đã hủy');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_order_tag`
--

CREATE TABLE `cfr_order_tag` (
  `order_tag_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_order_total`
--

CREATE TABLE `cfr_order_total` (
  `order_total_id` int(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_order_voucher`
--

CREATE TABLE `cfr_order_voucher` (
  `order_voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_page_contents`
--

CREATE TABLE `cfr_page_contents` (
  `page_id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `seo_title` varchar(256) DEFAULT NULL,
  `seo_description` text,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_product`
--

CREATE TABLE `cfr_product` (
  `product_id` int(11) NOT NULL,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `barcode` varchar(128) DEFAULT NULL,
  `upc` varchar(12) NOT NULL,
  `ean` varchar(14) NOT NULL,
  `jan` varchar(13) NOT NULL,
  `isbn` varchar(17) NOT NULL,
  `mpn` varchar(64) NOT NULL,
  `location` varchar(128) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `sale_on_out_of_stock` tinyint(4) NOT NULL DEFAULT '0',
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_alt` varchar(256) DEFAULT NULL,
  `multi_versions` tinyint(1) NOT NULL DEFAULT '0',
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `price_currency_id` int(11) NOT NULL,
  `compare_price` decimal(15,4) DEFAULT NULL,
  `c_price_currency_id` int(11) NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `tax_class_id` int(11) NOT NULL,
  `date_available` date NOT NULL DEFAULT '0000-00-00',
  `weight` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `weight_class_id` int(11) NOT NULL DEFAULT '0',
  `length` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `width` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `height` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `length_class_id` int(11) NOT NULL DEFAULT '0',
  `subtract` tinyint(1) NOT NULL DEFAULT '1',
  `minimum` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `demo` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_product_attribute`
--

CREATE TABLE `cfr_product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_product_collection`
--

CREATE TABLE `cfr_product_collection` (
  `product_collection_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `collection_id` int(11) NOT NULL,
  `sort_order` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_product_description`
--

CREATE TABLE `cfr_product_description` (
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `sub_description` text,
  `seo_title` varchar(256) DEFAULT NULL,
  `seo_description` varchar(512) DEFAULT NULL,
  `tag` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_product_discount`
--

CREATE TABLE `cfr_product_discount` (
  `product_discount_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_product_filter`
--

CREATE TABLE `cfr_product_filter` (
  `product_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_product_filter`
--

INSERT INTO `cfr_product_filter` (`product_id`, `filter_id`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `cfr_product_image`
--

CREATE TABLE `cfr_product_image` (
  `product_image_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_alt` varchar(256) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_product_option`
--

CREATE TABLE `cfr_product_option` (
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_product_option_value`
--

CREATE TABLE `cfr_product_option_value` (
  `product_option_value_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtract` tinyint(1) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `price_prefix` varchar(1) NOT NULL,
  `points` int(8) NOT NULL,
  `points_prefix` varchar(1) NOT NULL,
  `weight` decimal(15,8) NOT NULL,
  `weight_prefix` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_product_recurring`
--

CREATE TABLE `cfr_product_recurring` (
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_product_related`
--

CREATE TABLE `cfr_product_related` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_product_reward`
--

CREATE TABLE `cfr_product_reward` (
  `product_reward_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `points` int(8) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_product_special`
--

CREATE TABLE `cfr_product_special` (
  `product_special_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_product_tag`
--

CREATE TABLE `cfr_product_tag` (
  `product_tag_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_product_to_category`
--

CREATE TABLE `cfr_product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_product_to_download`
--

CREATE TABLE `cfr_product_to_download` (
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_product_to_layout`
--

CREATE TABLE `cfr_product_to_layout` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_product_to_store`
--

CREATE TABLE `cfr_product_to_store` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_product_version`
--

CREATE TABLE `cfr_product_version` (
  `product_version_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `version` varchar(256) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `compare_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sku` varchar(64) NOT NULL,
  `barcode` varchar(124) NOT NULL,
  `quantity` int(7) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_recurring`
--

CREATE TABLE `cfr_recurring` (
  `recurring_id` int(11) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `duration` int(10) UNSIGNED NOT NULL,
  `cycle` int(10) UNSIGNED NOT NULL,
  `trial_status` tinyint(4) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `trial_frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `trial_duration` int(10) UNSIGNED NOT NULL,
  `trial_cycle` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_recurring_description`
--

CREATE TABLE `cfr_recurring_description` (
  `recurring_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_report`
--

CREATE TABLE `cfr_report` (
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `report_date` date NOT NULL,
  `type` tinyint(4) NOT NULL,
  `type_description` varchar(100) NOT NULL,
  `d_field` varchar(500) NOT NULL,
  `m_value` varchar(500) NOT NULL,
  `last_updated` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_return`
--

CREATE TABLE `cfr_return` (
  `return_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `product` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `opened` tinyint(1) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `return_action_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `comment` text,
  `date_ordered` date NOT NULL DEFAULT '0000-00-00',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_return_action`
--

CREATE TABLE `cfr_return_action` (
  `return_action_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_return_action`
--

INSERT INTO `cfr_return_action` (`return_action_id`, `language_id`, `name`) VALUES
(1, 1, 'Refunded'),
(2, 1, 'Credit Issued'),
(3, 1, 'Replacement Sent'),
(4, 2, 'Refunded'),
(5, 2, 'Credit Issued'),
(6, 2, 'Replacement Sent');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_return_history`
--

CREATE TABLE `cfr_return_history` (
  `return_history_id` int(11) NOT NULL,
  `return_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_return_reason`
--

CREATE TABLE `cfr_return_reason` (
  `return_reason_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_return_reason`
--

INSERT INTO `cfr_return_reason` (`return_reason_id`, `language_id`, `name`) VALUES
(1, 1, 'Dead On Arrival'),
(2, 1, 'Received Wrong Item'),
(3, 1, 'Order Error'),
(4, 1, 'Faulty, please supply details'),
(5, 1, 'Other, please supply details'),
(6, 2, 'Dead On Arrival'),
(7, 2, 'Received Wrong Item'),
(8, 2, 'Order Error'),
(9, 2, 'Faulty, please supply details'),
(10, 2, 'Other, please supply details');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_return_status`
--

CREATE TABLE `cfr_return_status` (
  `return_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_return_status`
--

INSERT INTO `cfr_return_status` (`return_status_id`, `language_id`, `name`) VALUES
(1, 1, 'Pending'),
(3, 1, 'Complete'),
(2, 1, 'Awaiting Products'),
(4, 2, 'Pending'),
(5, 2, 'Complete'),
(6, 2, 'Awaiting Products');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_review`
--

CREATE TABLE `cfr_review` (
  `review_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_seo_url`
--

CREATE TABLE `cfr_seo_url` (
  `seo_url_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `table_name` varchar(256) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_session`
--

CREATE TABLE `cfr_session` (
  `session_id` varchar(32) NOT NULL,
  `data` text NOT NULL,
  `expire` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_setting`
--

CREATE TABLE `cfr_setting` (
  `setting_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(128) NOT NULL,
  `key` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_setting`
--

INSERT INTO `cfr_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(1, 0, 'config', 'config_robots', 'abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshaihulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg', 0),
(2, 0, 'config', 'config_shared', '0', 0),
(3, 0, 'config', 'config_secure', '0', 0),
(4, 0, 'voucher', 'total_voucher_sort_order', '8', 0),
(5, 0, 'voucher', 'total_voucher_status', '1', 0),
(6, 0, 'config', 'config_fraud_detection', '0', 0),
(7, 0, 'config', 'config_ftp_status', '0', 0),
(8, 0, 'config', 'config_ftp_root', '', 0),
(9, 0, 'config', 'config_ftp_password', '', 0),
(10, 0, 'config', 'config_ftp_username', '', 0),
(11, 0, 'config', 'config_ftp_port', '21', 0),
(12, 0, 'config', 'config_ftp_hostname', '', 0),
(13, 0, 'config', 'config_meta_title', 'Your Store', 0),
(14, 0, 'config', 'config_meta_description', 'My Store', 0),
(15, 0, 'config', 'config_meta_keyword', '', 0),
(5515, 0, 'config', 'config_theme', 'fashion_luvibee_2', 0),
(17, 0, 'config', 'config_layout_id', '4', 0),
(18, 0, 'config', 'config_country_id', '222', 0),
(19, 0, 'config', 'config_zone_id', '3563', 0),
(20, 0, 'config', 'config_language', 'vi-vn', 0),
(21, 0, 'config', 'config_admin_language', 'vi-vn', 0),
(22, 0, 'config', 'config_currency', 'VND', 0),
(23, 0, 'config', 'config_currency_auto', '1', 0),
(24, 0, 'config', 'config_length_class_id', '1', 0),
(25, 0, 'config', 'config_weight_class_id', '1', 0),
(26, 0, 'config', 'config_product_count', '1', 0),
(27, 0, 'config', 'config_limit_admin', '25', 0),
(28, 0, 'config', 'config_review_status', '1', 0),
(29, 0, 'config', 'config_review_guest', '1', 0),
(30, 0, 'config', 'config_voucher_min', '1', 0),
(31, 0, 'config', 'config_voucher_max', '1000', 0),
(32, 0, 'config', 'config_tax', '1', 0),
(33, 0, 'config', 'config_tax_default', 'shipping', 0),
(34, 0, 'config', 'config_tax_customer', 'shipping', 0),
(35, 0, 'config', 'config_customer_online', '0', 0),
(36, 0, 'config', 'config_customer_activity', '0', 0),
(37, 0, 'config', 'config_customer_search', '0', 0),
(38, 0, 'config', 'config_customer_group_id', '1', 0),
(39, 0, 'config', 'config_customer_group_display', '["1"]', 1),
(40, 0, 'config', 'config_customer_price', '0', 0),
(41, 0, 'config', 'config_account_id', '3', 0),
(42, 0, 'config', 'config_invoice_prefix', 'INV-2013-00', 0),
(43, 0, 'config', 'config_api_id', '1', 0),
(44, 0, 'config', 'config_cart_weight', '1', 0),
(45, 0, 'config', 'config_checkout_guest', '1', 0),
(46, 0, 'config', 'config_checkout_id', '5', 0),
(47, 0, 'config', 'config_order_status_id', '1', 0),
(48, 0, 'config', 'config_processing_status', '["5","1","2","12","3"]', 1),
(49, 0, 'config', 'config_complete_status', '["5","3"]', 1),
(50, 0, 'config', 'config_stock_display', '0', 0),
(51, 0, 'config', 'config_stock_warning', '0', 0),
(52, 0, 'config', 'config_stock_checkout', '0', 0),
(53, 0, 'config', 'config_affiliate_approval', '0', 0),
(54, 0, 'config', 'config_affiliate_auto', '0', 0),
(55, 0, 'config', 'config_affiliate_commission', '5', 0),
(56, 0, 'config', 'config_affiliate_id', '4', 0),
(57, 0, 'config', 'config_return_id', '0', 0),
(58, 0, 'config', 'config_return_status_id', '2', 0),
(59, 0, 'config', 'config_logo', 'catalog/logo.png', 0),
(60, 0, 'config', 'config_icon', 'catalog/cart.png', 0),
(61, 0, 'config', 'config_comment', '', 0),
(62, 0, 'config', 'config_open', '', 0),
(63, 0, 'config', 'config_image', '', 0),
(64, 0, 'config', 'config_fax', '', 0),
(65, 0, 'config', 'config_telephone', '', 0),
(66, 0, 'config', 'config_email', 'demo@opencart.com', 0),
(67, 0, 'config', 'config_geocode', '', 0),
(68, 0, 'config', 'config_owner', 'Your Name', 0),
(69, 0, 'config', 'config_address', '', 0),
(70, 0, 'config', 'config_name', '', 0),
(71, 0, 'config', 'config_seo_url', '0', 0),
(72, 0, 'config', 'config_file_max_size', '300000', 0),
(73, 0, 'config', 'config_file_ext_allowed', 'zip\r\ntxt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\ndoc', 0),
(74, 0, 'config', 'config_file_mime_allowed', 'text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/gif\r\nimage/bmp\r\nimage/tiff\r\nimage/svg+xml\r\napplication/zip\r\n&quot;application/zip&quot;\r\napplication/x-zip\r\n&quot;application/x-zip&quot;\r\napplication/x-zip-compressed\r\n&quot;application/x-zip-compressed&quot;\r\napplication/rar\r\n&quot;application/rar&quot;\r\napplication/x-rar\r\n&quot;application/x-rar&quot;\r\napplication/x-rar-compressed\r\n&quot;application/x-rar-compressed&quot;\r\napplication/octet-stream\r\n&quot;application/octet-stream&quot;\r\naudio/mpeg\r\nvideo/quicktime\r\napplication/pdf', 0),
(75, 0, 'config', 'config_maintenance', '0', 0),
(76, 0, 'config', 'config_password', '1', 0),
(77, 0, 'config', 'config_encryption', '', 0),
(78, 0, 'config', 'config_compression', '0', 0),
(79, 0, 'config', 'config_error_display', '1', 0),
(80, 0, 'config', 'config_error_log', '1', 0),
(81, 0, 'config', 'config_error_filename', 'error.log', 0),
(82, 0, 'config', 'config_google_analytics', '', 0),
(83, 0, 'config', 'config_mail_engine', 'smtp', 0),
(84, 0, 'config', 'config_mail_parameter', '', 0),
(85, 0, 'config', 'config_mail_smtp_hostname', 'tls://smtp.gmail.com', 0),
(86, 0, 'config', 'config_mail_smtp_username', 'hotro@bestme.asia', 0),
(87, 0, 'config', 'config_mail_smtp_password', 'NOVAONshop@1234', 0),
(88, 0, 'config', 'config_mail_smtp_port', '587', 0),
(89, 0, 'config', 'config_mail_smtp_timeout', '15', 0),
(90, 0, 'config', 'config_mail_alert_email', '', 0),
(91, 0, 'config', 'config_mail_alert', '["order"]', 1),
(92, 0, 'config', 'config_captcha', 'basic', 0),
(93, 0, 'config', 'config_captcha_page', '["review","return","contact"]', 1),
(94, 0, 'config', 'config_login_attempts', '5', 0),
(95, 0, 'payment_free_checkout', 'payment_free_checkout_status', '1', 0),
(96, 0, 'payment_free_checkout', 'free_checkout_order_status_id', '1', 0),
(97, 0, 'payment_free_checkout', 'payment_free_checkout_sort_order', '1', 0),
(98, 0, 'payment_cod', 'payment_cod_sort_order', '5', 0),
(99, 0, 'payment_cod', 'payment_cod_total', '0.01', 0),
(100, 0, 'payment_cod', 'payment_cod_order_status_id', '1', 0),
(101, 0, 'payment_cod', 'payment_cod_geo_zone_id', '0', 0),
(102, 0, 'payment_cod', 'payment_cod_status', '1', 0),
(103, 0, 'shipping_flat', 'shipping_flat_sort_order', '1', 0),
(104, 0, 'shipping_flat', 'shipping_flat_status', '1', 0),
(105, 0, 'shipping_flat', 'shipping_flat_geo_zone_id', '0', 0),
(106, 0, 'shipping_flat', 'shipping_flat_tax_class_id', '9', 0),
(107, 0, 'shipping_flat', 'shipping_flat_cost', '5.00', 0),
(108, 0, 'total_shipping', 'total_shipping_sort_order', '3', 0),
(109, 0, 'total_sub_total', 'sub_total_sort_order', '1', 0),
(110, 0, 'total_sub_total', 'total_sub_total_status', '1', 0),
(111, 0, 'total_tax', 'total_tax_status', '1', 0),
(112, 0, 'total_total', 'total_total_sort_order', '9', 0),
(113, 0, 'total_total', 'total_total_status', '1', 0),
(114, 0, 'total_tax', 'total_tax_sort_order', '5', 0),
(115, 0, 'total_credit', 'total_credit_sort_order', '7', 0),
(116, 0, 'total_credit', 'total_credit_status', '1', 0),
(117, 0, 'total_reward', 'total_reward_sort_order', '2', 0),
(118, 0, 'total_reward', 'total_reward_status', '1', 0),
(119, 0, 'total_shipping', 'total_shipping_status', '1', 0),
(120, 0, 'total_shipping', 'total_shipping_estimator', '1', 0),
(121, 0, 'total_coupon', 'total_coupon_sort_order', '4', 0),
(122, 0, 'total_coupon', 'total_coupon_status', '1', 0),
(123, 0, 'module_category', 'module_category_status', '1', 0),
(124, 0, 'module_account', 'module_account_status', '1', 0),
(125, 0, 'theme_default', 'theme_default_product_limit', '15', 0),
(126, 0, 'theme_default', 'theme_default_product_description_length', '100', 0),
(127, 0, 'theme_default', 'theme_default_image_thumb_width', '228', 0),
(128, 0, 'theme_default', 'theme_default_image_thumb_height', '228', 0),
(129, 0, 'theme_default', 'theme_default_image_popup_width', '500', 0),
(130, 0, 'theme_default', 'theme_default_image_popup_height', '500', 0),
(131, 0, 'theme_default', 'theme_default_image_category_width', '80', 0),
(132, 0, 'theme_default', 'theme_default_image_category_height', '80', 0),
(133, 0, 'theme_default', 'theme_default_image_product_width', '228', 0),
(134, 0, 'theme_default', 'theme_default_image_product_height', '228', 0),
(135, 0, 'theme_default', 'theme_default_image_additional_width', '74', 0),
(136, 0, 'theme_default', 'theme_default_image_additional_height', '74', 0),
(137, 0, 'theme_default', 'theme_default_image_related_width', '200', 0),
(138, 0, 'theme_default', 'theme_default_image_related_height', '200', 0),
(139, 0, 'theme_default', 'theme_default_image_compare_width', '90', 0),
(140, 0, 'theme_default', 'theme_default_image_compare_height', '90', 0),
(141, 0, 'theme_default', 'theme_default_image_wishlist_width', '47', 0),
(142, 0, 'theme_default', 'theme_default_image_wishlist_height', '47', 0),
(143, 0, 'theme_default', 'theme_default_image_cart_height', '47', 0),
(144, 0, 'theme_default', 'theme_default_image_cart_width', '47', 0),
(145, 0, 'theme_default', 'theme_default_image_location_height', '50', 0),
(146, 0, 'theme_default', 'theme_default_image_location_width', '268', 0),
(147, 0, 'theme_default', 'theme_default_directory', 'default', 0),
(148, 0, 'theme_default', 'theme_default_status', '1', 0),
(149, 0, 'dashboard_activity', 'dashboard_activity_status', '1', 0),
(150, 0, 'dashboard_activity', 'dashboard_activity_sort_order', '7', 0),
(151, 0, 'dashboard_sale', 'dashboard_sale_status', '1', 0),
(152, 0, 'dashboard_sale', 'dashboard_sale_width', '3', 0),
(153, 0, 'dashboard_chart', 'dashboard_chart_status', '1', 0),
(154, 0, 'dashboard_chart', 'dashboard_chart_width', '6', 0),
(155, 0, 'dashboard_customer', 'dashboard_customer_status', '1', 0),
(156, 0, 'dashboard_customer', 'dashboard_customer_width', '3', 0),
(157, 0, 'dashboard_map', 'dashboard_map_status', '1', 0),
(158, 0, 'dashboard_map', 'dashboard_map_width', '6', 0),
(159, 0, 'dashboard_online', 'dashboard_online_status', '1', 0),
(160, 0, 'dashboard_online', 'dashboard_online_width', '3', 0),
(161, 0, 'dashboard_order', 'dashboard_order_sort_order', '1', 0),
(162, 0, 'dashboard_order', 'dashboard_order_status', '1', 0),
(163, 0, 'dashboard_order', 'dashboard_order_width', '3', 0),
(164, 0, 'dashboard_sale', 'dashboard_sale_sort_order', '2', 0),
(165, 0, 'dashboard_customer', 'dashboard_customer_sort_order', '3', 0),
(166, 0, 'dashboard_online', 'dashboard_online_sort_order', '4', 0),
(167, 0, 'dashboard_map', 'dashboard_map_sort_order', '5', 0),
(168, 0, 'dashboard_chart', 'dashboard_chart_sort_order', '6', 0),
(169, 0, 'dashboard_recent', 'dashboard_recent_status', '1', 0),
(170, 0, 'dashboard_recent', 'dashboard_recent_sort_order', '8', 0),
(171, 0, 'dashboard_activity', 'dashboard_activity_width', '4', 0),
(172, 0, 'dashboard_recent', 'dashboard_recent_width', '8', 0),
(173, 0, 'report_customer_activity', 'report_customer_activity_status', '1', 0),
(174, 0, 'report_customer_activity', 'report_customer_activity_sort_order', '1', 0),
(175, 0, 'report_customer_order', 'report_customer_order_status', '1', 0),
(176, 0, 'report_customer_order', 'report_customer_order_sort_order', '2', 0),
(177, 0, 'report_customer_reward', 'report_customer_reward_status', '1', 0),
(178, 0, 'report_customer_reward', 'report_customer_reward_sort_order', '3', 0),
(179, 0, 'report_customer_search', 'report_customer_search_sort_order', '3', 0),
(180, 0, 'report_customer_search', 'report_customer_search_status', '1', 0),
(181, 0, 'report_customer_transaction', 'report_customer_transaction_status', '1', 0),
(182, 0, 'report_customer_transaction', 'report_customer_transaction_status_sort_order', '4', 0),
(183, 0, 'report_sale_tax', 'report_sale_tax_status', '1', 0),
(184, 0, 'report_sale_tax', 'report_sale_tax_sort_order', '5', 0),
(185, 0, 'report_sale_shipping', 'report_sale_shipping_status', '1', 0),
(186, 0, 'report_sale_shipping', 'report_sale_shipping_sort_order', '6', 0),
(187, 0, 'report_sale_return', 'report_sale_return_status', '1', 0),
(188, 0, 'report_sale_return', 'report_sale_return_sort_order', '7', 0),
(189, 0, 'report_sale_order', 'report_sale_order_status', '1', 0),
(190, 0, 'report_sale_order', 'report_sale_order_sort_order', '8', 0),
(191, 0, 'report_sale_coupon', 'report_sale_coupon_status', '1', 0),
(192, 0, 'report_sale_coupon', 'report_sale_coupon_sort_order', '9', 0),
(193, 0, 'report_product_viewed', 'report_product_viewed_status', '1', 0),
(194, 0, 'report_product_viewed', 'report_product_viewed_sort_order', '10', 0),
(195, 0, 'report_product_purchased', 'report_product_purchased_status', '1', 0),
(196, 0, 'report_product_purchased', 'report_product_purchased_sort_order', '11', 0),
(197, 0, 'report_marketing', 'report_marketing_status', '1', 0),
(198, 0, 'report_marketing', 'report_marketing_sort_order', '12', 0),
(4968, 0, 'developer', 'developer_sass', '0', 0),
(201, 0, 'module_theme_builder_config', 'module_theme_builder_config_status', '1', 0),
(4700, 0, 'theme_novaon', 'theme_novaon_image_wishlist_height', '47', 0),
(4701, 0, 'theme_novaon', 'theme_novaon_image_wishlist_width', '47', 0),
(4702, 0, 'theme_novaon', 'theme_novaon_image_compare_height', '90', 0),
(4703, 0, 'theme_novaon', 'theme_novaon_image_compare_width', '90', 0),
(4704, 0, 'theme_novaon', 'theme_novaon_image_related_height', '80', 0),
(4705, 0, 'theme_novaon', 'theme_novaon_image_related_width', '80', 0),
(4706, 0, 'theme_novaon', 'theme_novaon_image_additional_height', '74', 0),
(4707, 0, 'theme_novaon', 'theme_novaon_image_additional_width', '74', 0),
(4708, 0, 'theme_novaon', 'theme_novaon_image_product_height', '228', 0),
(4709, 0, 'theme_novaon', 'theme_novaon_image_product_width', '228', 0),
(4710, 0, 'theme_novaon', 'theme_novaon_image_popup_height', '500', 0),
(4711, 0, 'theme_novaon', 'theme_novaon_image_popup_width', '500', 0),
(4712, 0, 'theme_novaon', 'theme_novaon_image_thumb_height', '228', 0),
(4713, 0, 'theme_novaon', 'theme_novaon_image_thumb_width', '228', 0),
(4714, 0, 'theme_novaon', 'theme_novaon_image_category_height', '80', 0),
(4715, 0, 'theme_novaon', 'theme_novaon_image_category_width', '80', 0),
(4716, 0, 'theme_novaon', 'theme_novaon_product_description_length', '100', 0),
(4717, 0, 'theme_novaon', 'theme_novaon_product_limit', '15', 0),
(4718, 0, 'theme_novaon', 'theme_novaon_status', '1', 0),
(4719, 0, 'theme_novaon', 'theme_novaon_directory', 'novaon', 0),
(4720, 0, 'theme_novaon', 'theme_novaon_image_cart_width', '47', 0),
(4721, 0, 'theme_novaon', 'theme_novaon_image_cart_height', '47', 0),
(4722, 0, 'theme_novaon', 'theme_novaon_image_location_width', '268', 0),
(4723, 0, 'theme_novaon', 'theme_novaon_image_location_height', '50', 0),
(4892, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_wishlist_height', '47', 0),
(4893, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_wishlist_width', '47', 0),
(4894, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_compare_height', '90', 0),
(4895, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_compare_width', '90', 0),
(4896, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_related_height', '80', 0),
(4897, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_related_width', '80', 0),
(4898, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_additional_height', '74', 0),
(4899, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_additional_width', '74', 0),
(4900, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_product_height', '228', 0),
(4901, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_product_width', '228', 0),
(4902, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_popup_height', '500', 0),
(4903, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_popup_width', '500', 0),
(4904, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_thumb_height', '228', 0),
(4905, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_thumb_width', '228', 0),
(4906, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_category_height', '80', 0),
(4907, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_category_width', '80', 0),
(4908, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_product_description_length', '100', 0),
(4909, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_product_limit', '15', 0),
(4910, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_status', '1', 0),
(4911, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_directory', 'fashion_luvibee_2', 0),
(4912, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_cart_width', '47', 0),
(4913, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_cart_height', '47', 0),
(4914, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_location_width', '268', 0),
(4915, 0, 'theme_fashion_luvibee_2', 'theme_fashion_luvibee_2_image_location_height', '50', 0),
(4967, 0, 'developer', 'developer_theme', '0', 0);
-- --------------------------------------------------------

--
-- Table structure for table `cfr_shipping_courier`
--

CREATE TABLE `cfr_shipping_courier` (
  `shipping_courier_id` int(11) NOT NULL,
  `shipping_courier_code` varchar(255) NOT NULL DEFAULT '',
  `shipping_courier_name` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_statistics`
--

CREATE TABLE `cfr_statistics` (
  `statistics_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `value` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_statistics`
--

INSERT INTO `cfr_statistics` (`statistics_id`, `code`, `value`) VALUES
(1, 'order_sale', '0.0000'),
(2, 'order_processing', '0.0000'),
(3, 'order_complete', '0.0000'),
(4, 'order_other', '0.0000'),
(5, 'returns', '0.0000'),
(6, 'product', '0.0000'),
(7, 'review', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_stock_status`
--

CREATE TABLE `cfr_stock_status` (
  `stock_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_stock_status`
--

INSERT INTO `cfr_stock_status` (`stock_status_id`, `language_id`, `name`) VALUES
(7, 1, 'In Stock'),
(8, 1, 'Pre-Order'),
(5, 1, 'Out Of Stock'),
(6, 1, '2-3 Days'),
(7, 2, 'In Stock'),
(8, 2, 'Pre-Order'),
(5, 2, 'Out Of Stock'),
(6, 2, '2-3 Days');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_store`
--

CREATE TABLE `cfr_store` (
  `store_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  `ssl` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_tag`
--

CREATE TABLE `cfr_tag` (
  `tag_id` int(11) NOT NULL,
  `value` varchar(500) NOT NULL,
  `sort_order` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_tax_class`
--

CREATE TABLE `cfr_tax_class` (
  `tax_class_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_tax_class`
--

INSERT INTO `cfr_tax_class` (`tax_class_id`, `title`, `description`, `date_added`, `date_modified`) VALUES
(9, 'Taxable Goods', 'Taxed goods', '2009-01-06 23:21:53', '2011-09-23 14:07:50'),
(10, 'Downloadable Products', 'Downloadable', '2011-09-21 22:19:39', '2011-09-22 10:27:36');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_tax_rate`
--

CREATE TABLE `cfr_tax_rate` (
  `tax_rate_id` int(11) NOT NULL,
  `geo_zone_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  `rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `type` char(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_tax_rate`
--

INSERT INTO `cfr_tax_rate` (`tax_rate_id`, `geo_zone_id`, `name`, `rate`, `type`, `date_added`, `date_modified`) VALUES
(86, 3, 'VAT (20%)', '20.0000', 'P', '2011-03-09 21:17:10', '2011-09-22 22:24:29'),
(87, 3, 'Eco Tax (-2.00)', '2.0000', 'F', '2011-09-21 21:49:23', '2011-09-23 00:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_tax_rate_to_customer_group`
--

CREATE TABLE `cfr_tax_rate_to_customer_group` (
  `tax_rate_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_tax_rate_to_customer_group`
--

INSERT INTO `cfr_tax_rate_to_customer_group` (`tax_rate_id`, `customer_group_id`) VALUES
(86, 1),
(87, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cfr_tax_rule`
--

CREATE TABLE `cfr_tax_rule` (
  `tax_rule_id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `based` varchar(10) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_tax_rule`
--

INSERT INTO `cfr_tax_rule` (`tax_rule_id`, `tax_class_id`, `tax_rate_id`, `based`, `priority`) VALUES
(121, 10, 86, 'payment', 1),
(120, 10, 87, 'store', 0),
(128, 9, 86, 'shipping', 1),
(127, 9, 87, 'shipping', 2);

-- --------------------------------------------------------

--
-- Table structure for table `cfr_theme`
--

CREATE TABLE `cfr_theme` (
  `theme_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `theme` varchar(64) NOT NULL,
  `route` varchar(64) NOT NULL,
  `code` mediumtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_theme_builder_config`
--

CREATE TABLE `cfr_theme_builder_config` (
  `id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `config_theme` varchar(128) NOT NULL,
  `code` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `config` mediumtext NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_theme_builder_config`
--

INSERT INTO `cfr_theme_builder_config` (`id`, `store_id`, `config_theme`, `code`, `key`, `config`, `date_added`, `date_modified`) VALUES
(340, 0, 'fashion_luvibee_2', 'config', 'config_theme_color', '{\n    \"main\": {\n        \"main\": {\n            \"hex\": \"F8BD2F\",\n            \"visible\": \"1\"\n        },\n        \"button\": {\n            \"hex\": \"F8BD2F\",\n            \"visible\": \"1\"\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-08-21 09:33:32'),
(341, 0, 'fashion_luvibee_2', 'config', 'config_theme_text', '{\n    \"all\": {\n        \"title\": \"Font Roboto\",\n        \"font\": \"\'Roboto\', sans-serif\",\n        \"supported-fonts\": [\n            \"Open Sans\",\n            \"IBM Plex Sans\",\n            \"Courier New\",\n            \"\'Roboto\', sans-serif\",\n            \"Nunito\",\n            \"Arial\",\n            \"DejaVu Sans\",\n            \"Tahoma\",\n            \"Time News Roman\"\n        ]\n    },\n    \"heading\": {\n        \"font\": \"Tahoma\",\n        \"supported-fonts\": [\n            \"Open Sans\",\n            \"IBM Plex Sans\",\n            \"Courier New\",\n            \"Roboto\",\n            \"Nunito\",\n            \"Arial\",\n            \"DejaVu Sans\",\n            \"Tahoma\",\n            \"Time News Roman\"\n        ],\n        \"font-type\": \"regular\",\n        \"supported-font-types\": [\n            \"regular\",\n            \"bold\",\n            \"italic\"\n        ],\n        \"font-size\": 32\n    },\n    \"body\": {\n        \"font\": \"Aria\",\n        \"supported-fonts\": [\n            \"Open Sans\",\n            \"IBM Plex Sans\",\n            \"Courier New\",\n            \"Roboto\",\n            \"Nunito\",\n            \"Arial\",\n            \"DejaVu Sans\",\n            \"Tahoma\",\n            \"Time News Roman\"\n        ],\n        \"font-type\": \"regular\",\n        \"supported-font-types\": [\n            \"regular\",\n            \"bold\",\n            \"italic\"\n        ],\n        \"font-size\": 14\n    }\n}', '2020-06-09 18:00:35', '2020-08-21 09:33:32'),
(342, 0, 'fashion_luvibee_2', 'config', 'config_theme_favicon', '{\n    \"image\": {\n        \"url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1591700491\\/23952\\/logo-lubie.png\"\n    }\n}', '2020-06-09 18:00:35', '2020-08-21 09:33:32'),
(343, 0, 'fashion_luvibee_2', 'config', 'config_theme_social', '[\n    {\n        \"name\": \"facebook\",\n        \"text\": \"Facebook\",\n        \"url\": \"https:\\/\\/www.facebook.com\"\n    },\n    {\n        \"name\": \"twitter\",\n        \"text\": \"Twitter\",\n        \"url\": \"https:\\/\\/www.twitter.com\"\n    },\n    {\n        \"name\": \"instagram\",\n        \"text\": \"Instagram\",\n        \"url\": \"https:\\/\\/www.instagram.com\"\n    },\n    {\n        \"name\": \"tumblr\",\n        \"text\": \"Tumblr\",\n        \"url\": \"https:\\/\\/www.tumblr.com\"\n    },\n    {\n        \"name\": \"youtube\",\n        \"text\": \"Youtube\",\n        \"url\": \"https:\\/\\/www.youtube.com\"\n    },\n    {\n        \"name\": \"googleplus\",\n        \"text\": \"Google Plus\",\n        \"url\": \"https:\\/\\/www.plus.google.com\"\n    }\n]', '2020-06-09 18:00:35', '2020-08-21 09:33:32'),
(344, 0, 'fashion_luvibee_2', 'config', 'config_theme_override_css', '{\n    \"css\": \".header-default {\\n    padding: 10px 0px;\\n    background-color: #FFCE59 !important;\\n}\\n.mt-lg-5, .my-lg-5 {\\n    margin-top: 1rem!important;\\n}\\n.navbar-light .navbar-nav .nav-link.active, .navbar-light .navbar-nav .nav-link:hover {\\n    color: #FF8C00;\\n}\\n.text-primary {\\n    color: #FF8C00!important;\\n}\\n.mb-md-5, .my-md-5 {\\n    margin-bottom: 1rem!important;\\n}\\n.block-footer {\\n    padding: 15px 0;\\n}\\n.mt-5, .my-5 {\\n    margin-top: 1rem!important;\\n}\\n.mb-5, .my-5 {\\n    margin-bottom: 1rem!important;\\n}\\n.pt-md-5, .py-md-5 {\\n    padding-top: 1rem!important;\\n}\\n.pb-md-5, .py-md-5 {\\n    padding-bottom: 0rem!important;\\n}\\n.mt-md-5, .my-md-5 {\\n    margin-top: 0rem!important;\\n}\"\n}', '2020-06-09 18:00:35', '2020-08-21 09:33:32'),
(345, 0, 'fashion_luvibee_2', 'config', 'config_theme_checkout_page', '{\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(346, 0, 'fashion_luvibee_2', 'config', 'config_section_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"header\": {\n            \"name\": \"header\",\n            \"text\": \"Header\",\n            \"visible\": 1\n        },\n        \"slide-show\": {\n            \"name\": \"slide-show\",\n            \"text\": \"Slideshow\",\n            \"visible\": 1\n        },\n        \"categories\": {\n            \"name\": \"categories\",\n            \"text\": \"Danh mục sản phẩm\",\n            \"visible\": 1\n        },\n        \"hot-deals\": {\n            \"name\": \"hot-deals\",\n            \"text\": \"Sản phẩm khuyến mại\",\n            \"visible\": 1\n        },\n        \"feature-products\": {\n            \"name\": \"feature-products\",\n            \"text\": \"Sản phẩm bán chạy\",\n            \"visible\": 1\n        },\n        \"related-products\": {\n            \"name\": \"related-products\",\n            \"text\": \"Sản phẩm xem nhiều\",\n            \"visible\": 1\n        },\n        \"new-products\": {\n            \"name\": \"new-products\",\n            \"text\": \"Sản phẩm mới\",\n            \"visible\": 1\n        },\n        \"product-detail\": {\n            \"name\": \"product-details\",\n            \"text\": \"Chi tiết sản phẩm\",\n            \"visible\": 1\n        },\n        \"banner\": {\n            \"name\": \"banner\",\n            \"text\": \"Banner\",\n            \"visible\": 1\n        },\n        \"content_customize\" : {\n            \"name\" : \"content-customize\",\n            \"text\" : \"Nội dung tùy chỉnh\",\n            \"visible\" : 1\n        },\n        \"partners\": {\n            \"name\": \"partners\",\n            \"text\": \"Đối tác\",\n            \"visible\": 1\n        },\n        \"blog\": {\n            \"name\": \"blog\",\n            \"text\": \"Blog\",\n            \"visible\": 1\n        },\n        \"footer\": {\n            \"name\": \"footer\",\n            \"text\": \"Footer\",\n            \"visible\": 1\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(347, 0, 'fashion_luvibee_2', 'config', 'config_section_banner', '{\n    \"name\": \"banner\",\n    \"text\": \"Banner\",\n    \"visible\": \"1\",\n    \"display\": [\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1597976604\\/23952\\/banner1b.jpg\",\n            \"url\": \"https:\\/\\/fashionluvibee.bestme.asia\\/san-pham\",\n            \"description\": \"Banner 1\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1597976622\\/23952\\/banner1a.jpg\",\n            \"url\": \"https:\\/\\/fashionluvibee.bestme.asia\\/san-pham\",\n            \"description\": \"Banner 2\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1597977163\\/23952\\/slider1%281597977161%29.jpg\",\n            \"url\": \"https:\\/\\/fashionluvibee.bestme.asia\\/san-pham\",\n            \"description\": \"Banner 3\",\n            \"visible\": \"1\"\n        }\n    ]\n}', '2020-06-09 18:00:35', '2020-08-21 09:32:52'),
(348, 0, 'fashion_luvibee_2', 'config', 'config_section_best_sales_product', '{\n    \"name\": \"feature-product\",\n    \"text\": \"Sản phẩm bán chạy\",\n    \"visible\": \"1\",\n    \"setting\": {\n        \"title\": \"Sản phẩm bán chạy\",\n        \"sub_title\": \"Mô tả tiêu đề\",\n        \"auto_retrieve_data\": \"0\",\n        \"collection_id\": \"8\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": \"4\"\n        },\n        \"grid_mobile\": {\n            \"quantity\": \"2\"\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 19:38:41'),
(349, 0, 'fashion_luvibee_2', 'config', 'config_section_new_product', '{\n    \"name\": \"new-product\",\n    \"text\": \"Sản phẩm mới\",\n    \"visible\": \"1\",\n    \"setting\": {\n        \"title\": \"Sản phẩm mới\",\n        \"sub_title\": \"Mô tả tiêu đề\",\n        \"auto_retrieve_data\": \"0\",\n        \"collection_id\": \"8\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": \"4\"\n        },\n        \"grid_mobile\": {\n            \"quantity\": \"2\"\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 19:38:56'),
(350, 0, 'fashion_luvibee_2', 'config', 'config_section_best_views_product', '{\n    \"name\": \"related-product\",\n    \"text\": \"Sản phẩm xem nhiều\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Sản phẩm xem nhiều\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 10\n        },\n        \"grid_mobile\": {\n            \"quantity\": 2\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(351, 0, 'fashion_luvibee_2', 'config', 'config_section_blog', '{\n    \"name\": \"blog\",\n    \"text\": \"Blog\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Blog\"\n    },\n    \"display\": {\n        \"menu\": [\n            {\n                \"type\": \"existing\",\n                \"menu\": {\n                    \"id\": 12,\n                    \"name\": \"Danh sách bài viết 1\"\n                }\n            },\n            {\n                \"type\": \"manual\",\n                \"entries\": [\n                    {\n                        \"id\": 10,\n                        \"name\": \"Entry 10\"\n                    },\n                    {\n                        \"id\": 11,\n                        \"name\": \"Entry 11\"\n                    }\n                ]\n            }\n        ],\n        \"grid\": {\n            \"quantity\": 10\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(352, 0, 'fashion_luvibee_2', 'config', 'config_section_detail_product', '{\n    \"name\": \"product-detail\",\n    \"text\": \"Chi tiết sản phẩm\",\n    \"visible\": 1,\n    \"display\": {\n        \"name\": true,\n        \"description\": false,\n        \"price\": true,\n        \"price-compare\": false,\n        \"status\": false,\n        \"sale\": false,\n        \"rate\": false\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(353, 0, 'fashion_luvibee_2', 'config', 'config_section_footer', '{\n    \"name\": \"footer\",\n    \"text\": \"Footer\",\n    \"visible\": 1,\n    \"contact\": {\n        \"title\": \"Liên hệ chúng tôi\",\n        \"address\": \"Cau Giay - Ha Noi\",\n        \"phone-number\": \"(+84)987654321\",\n        \"email\": \"dungbt@novaon.vn\",\n        \"visible\": 1\n    },\n    \"collection\": {\n        \"title\": \"Bộ sưu tập\",\n        \"menu_id\": \"6\",\n        \"visible\": 1\n    },\n    \"quick-links\": {\n        \"title\": \"Liên kết nhanh\",\n        \"menu_id\": 1,\n        \"visible\": 1\n    },\n    \"subscribe\": {\n        \"title\": \"Đăng ký theo dõi\",\n        \"social_network\": 1,\n        \"visible\": 1,\n        \"youtube_visible\": 1,\n        \"facebook_visible\": 1,\n        \"instagram_visible\": 1\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 20:09:27'),
(354, 0, 'fashion_luvibee_2', 'config', 'config_section_header', '{\n    \"name\": \"header\",\n    \"text\": \"Header\",\n    \"visible\": \"1\",\n    \"notify-bar\": {\n        \"name\": \"Thanh thông báo\",\n        \"visible\": \"1\",\n        \"content\": \"Mua sắm online thuận tiện và dễ dàng\",\n        \"url\": \"#\"\n    },\n    \"logo\": {\n        \"name\": \"Logo\",\n        \"url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1591700491\\/23952\\/logo-lubie.png\",\n        \"height\": \"\"\n    },\n    \"menu\": {\n        \"name\": \"Menu\",\n        \"display-list\": {\n            \"name\": \"Menu chính\",\n            \"id\": \"1\"\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 19:40:02'),
(355, 0, 'fashion_luvibee_2', 'config', 'config_section_hot_product', '{\n    \"name\": \"hot-deals\",\n    \"text\": \"Sản phẩm khuyến mại\",\n    \"visible\": \"1\",\n    \"setting\": {\n        \"title\": \"Sản phẩm khuyến mại\",\n        \"sub_title\": \"Mô tả tiêu đề\",\n        \"auto_retrieve_data\": \"0\",\n        \"collection_id\": \"8\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": \"4\"\n        },\n        \"grid_mobile\": {\n            \"quantity\": \"2\"\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 19:38:32'),
(356, 0, 'fashion_luvibee_2', 'config', 'config_section_list_product', '{\n    \"name\": \"categories\",\n    \"text\": \"Danh mục sản phẩm\",\n    \"visible\": 0,\n    \"setting\": {\n        \"title\": \"Danh mục sản phẩm\"\n    },\n    \"display\": {\n        \"menu\": {\n            \"id\": 1,\n            \"name\": \"Danh mục sản phẩm 1\"\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(357, 0, 'fashion_luvibee_2', 'config', 'config_section_partner', '{\n    \"name\": \"partners\",\n    \"text\": \"Đối tác\",\n    \"visible\": \"1\",\n    \"limit-per-line\": \"4\",\n    \"display\": [\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1591700521\\/23952\\/logo-4-1.png\",\n            \"url\": \"https:\\/\\/fashionluvibee.bestme.asia\\/san-pham\",\n            \"description\": \"Partner 1\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1591700520\\/23952\\/logo-3.png\",\n            \"url\": \"https:\\/\\/fashionluvibee.bestme.asia\\/san-pham\",\n            \"description\": \"Partner 2\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1591700518\\/23952\\/logo-1.png\",\n            \"url\": \"https:\\/\\/fashionluvibee.bestme.asia\\/san-pham\",\n            \"description\": \"Partner 3\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1591700516\\/23952\\/1a.png\",\n            \"url\": \"https:\\/\\/fashionluvibee.bestme.asia\\/san-pham\",\n            \"description\": \"Partner 4\",\n            \"visible\": \"1\"\n        }\n    ]\n}', '2020-06-09 18:00:35', '2020-06-09 18:02:51'),
(358, 0, 'fashion_luvibee_2', 'config', 'config_section_slideshow', '{\n    \"name\": \"slide-show\",\n    \"text\": \"Slideshow\",\n    \"visible\": \"1\",\n    \"setting\": {\n        \"transition-time\": \"5\"\n    },\n    \"display\": [\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1596699757\\/23952\\/slider1.jpg\",\n            \"url\": \"https:\\/\\/fashionluvibee.bestme.asia\\/san-pham\",\n            \"description\": \"\",\n            \"id\": \"11179844\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1596699797\\/23952\\/slider2.jpg\",\n            \"url\": \"https:\\/\\/fashionluvibee.bestme.asia\\/san-pham\",\n            \"description\": \"\",\n            \"id\": \"18094842\"\n        }\n    ]\n}', '2020-06-09 18:00:35', '2020-08-06 14:43:49'),
(359, 0, 'fashion_luvibee_2', 'config', 'config_section_product_groups', '{\n    \"name\": \"group products\",\n    \"text\": \"nhóm sản phẩm\",\n    \"visible\": 1,\n    \"list\": [\n        {\n            \"text\": \"Danh sách sản phẩm 1\",\n            \"visible\": \"0\",\n            \"setting\": {\n                \"title\": \"Danh sách sản phẩm 1\",\n                \"collection_id\": \"4\",\n                \"sub_title\": \"Danh sách sản phẩm 1\"\n            },\n            \"display\": {\n                \"grid\": {\n                    \"quantity\": \"6\"\n                },\n                \"grid_mobile\": {\n                    \"quantity\": 2\n                }\n            }\n        }\n    ]\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(360, 0, 'fashion_luvibee_2', 'config', 'config_section_content_customize', '{\n    \"name\": \"content-customize\",\n    \"title\": \"Nội dung tuỳ chỉnh\",\n    \"display\": [\n        {\n            \"name\": \"content-1\",\n            \"title\": \"Bảo đảm chất lượng\",\n            \"type\": \"fixed\",\n            \"content\": {\n                \"icon\": \"/catalog/view/theme/default/image/favicon.ico\",\n                \"title\": \"Bảo đảm chất lượng\",\n                \"description\": \"Sản phẩm đảm bảo chất lượng\",\n                \"html\": \"\"\n            }\n        },\n        {\n            \"name\": \"content-2\",\n            \"title\": \"Miễn phí giao hàng\",\n            \"type\": \"fixed\",\n            \"content\": {\n                \"icon\": \"/catalog/view/theme/default/image/favicon.ico\",\n                \"title\": \"Miễn phí giao hàng\",\n                \"description\": \"Cho đơn hàng từ 2 triệu\",\n                \"html\": \"\"\n            }\n        },\n        {\n            \"name\": \"content-3\",\n            \"title\": \"Hỗ trợ 24/7\",\n            \"type\": \"fixed\",\n            \"content\": {\n                \"icon\": \"/catalog/view/theme/default/image/favicon.ico\",\n                \"title\": \"Hỗ trợ 24/7\",\n                \"description\": \"Hotline 012.345.678\",\n                \"html\": \"\"\n            }\n        },\n        {\n            \"name\": \"content-4\",\n            \"title\": \"Đổi trả hàng\",\n            \"type\": \"fixed\",\n            \"content\": {\n                \"icon\": \"/catalog/view/theme/default/image/favicon.ico\",\n                \"title\": \"Đổi trả hàng\",\n                \"description\": \"Trong vòng 7 ngày\",\n                \"html\": \"\"\n            }\n        }\n    ]\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(361, 0, 'fashion_luvibee_2', 'config', 'config_section_category_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"banner\": {\n            \"name\": \"banner\",\n            \"text\": \"Banner\",\n            \"visible\": 1\n        },\n        \"filter\": {\n            \"name\": \"filter\",\n            \"text\": \"Filter\",\n            \"visible\": 1\n        },\n        \"product_category\": {\n            \"name\": \"product_category\",\n            \"text\": \"Product Category\",\n            \"visible\": 1\n        },\n        \"product_list\": {\n            \"name\": \"product_list\",\n            \"text\": \"Product List\",\n            \"visible\": 1\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(362, 0, 'fashion_luvibee_2', 'config', 'config_section_category_banner', '{\n    \"name\": \"banner\",\n    \"text\": \"Banner\",\n    \"visible\": \"1\",\n    \"display\": [\n        {\n            \"image-url\": \"https:\\/\\/res.cloudinary.com\\/novaonx2\\/image\\/upload\\/v1597976881\\/23952\\/1-1.png\",\n            \"url\": \"#\",\n            \"description\": \"Banner 1\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"\\/catalog\\/view\\/theme\\/default\\/image\\/banner\\/img-banner-02.png\",\n            \"url\": \"#\",\n            \"description\": \"Banner 2\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"\\/catalog\\/view\\/theme\\/default\\/image\\/banner\\/img-banner-03.png\",\n            \"url\": \"#\",\n            \"description\": \"Banner 3\",\n            \"visible\": \"1\"\n        }\n    ]\n}', '2020-06-09 18:00:35', '2020-08-21 09:56:05'),
(363, 0, 'fashion_luvibee_2', 'config', 'config_section_category_filter', '{\n    \"name\": \"filter\",\n    \"text\": \"Bộ lọc\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Bộ lọc\"\n    },\n    \"display\": {\n        \"supplier\": {\n            \"title\": \"Nhà cung cấp\",\n            \"visible\": 1\n        },\n        \"product-type\": {\n            \"title\": \"Loại sản phẩm\",\n            \"visible\": 1\n        },\n        \"collection\": {\n            \"title\": \"Bộ sưu tập\",\n            \"visible\": 1\n        },\n        \"property\": {\n            \"title\": \"Lọc theo tt - k dung\",\n            \"visible\": 1,\n            \"prop\": [\n                \"all\",\n                \"color\",\n                \"weight\",\n                \"size\"\n            ]\n        },\n        \"product-price\": {\n            \"title\": \"Giá sản phẩm\",\n            \"visible\": 1,\n            \"range\": {\n                \"from\": 0,\n                \"to\": 100000000\n            }\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(364, 0, 'fashion_luvibee_2', 'config', 'config_section_category_product_category', '{\n    \"name\": \"product-category\",\n    \"text\": \"Danh mục sản phẩm\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Danh mục sản phẩm\"\n    },\n    \"display\": {\n        \"menu\": {\n            \"id\": 1,\n            \"name\": \"Danh mục sản phẩm 1\"\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(365, 0, 'fashion_luvibee_2', 'config', 'config_section_category_product_list', '{\n    \"name\": \"product-list\",\n    \"text\": \"Danh sách sản phẩm\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Danh sách sản phẩm\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 10\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(366, 0, 'fashion_luvibee_2', 'config', 'config_section_product_detail_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"related_product\": {\n            \"name\": \"related_product\",\n            \"text\": \"Related Product\",\n            \"visible\": 1\n        },\n        \"template\": {\n            \"name\": \"template\",\n            \"text\": \"Template\",\n            \"visible\": 1\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(367, 0, 'fashion_luvibee_2', 'config', 'config_section_product_detail_related_product', '{\n    \"name\": \"related-product\",\n    \"text\": \"Sản phẩm liên quan\",\n    \"visible\": \"1\",\n    \"setting\": {\n        \"title\": \"Sản phẩm liên quan\",\n        \"auto_retrieve_data\": \"1\",\n        \"collection_id\": \"1\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": \"4\"\n        },\n        \"grid_mobile\": {\n            \"quantity\": \"2\"\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 19:39:38'),
(368, 0, 'fashion_luvibee_2', 'config', 'config_section_product_detail_template', '{\n    \"name\": \"template\",\n    \"text\": \"Giao diện\",\n    \"visible\": 1,\n    \"display\": {\n        \"template\": {\n            \"id\": 10\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(369, 0, 'fashion_luvibee_2', 'config', 'config_section_blog_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"blog_category\": {\n            \"name\": \"blog_category\",\n            \"text\": \"Blog Category\",\n            \"visible\": 1\n        },\n        \"blog_list\": {\n            \"name\": \"blog_list\",\n            \"text\": \"Blog List\",\n            \"visible\": 1\n        },\n        \"latest_blog\": {\n            \"name\": \"latest_blog\",\n            \"text\": \"Latest Blog\",\n            \"visible\": 1\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(370, 0, 'fashion_luvibee_2', 'config', 'config_section_blog_blog_category', '{\n    \"name\": \"blog-category\",\n    \"text\": \"Danh mục bài viết\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Danh mục bài viết\"\n    },\n    \"display\": {\n        \"menu\": {\n            \"id\": 1,\n            \"name\": \"Danh mục bài viết 1\"\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(371, 0, 'fashion_luvibee_2', 'config', 'config_section_blog_blog_list', '{\n    \"name\": \"blog-list\",\n    \"text\": \"Danh sách bài viết\",\n    \"visible\": 1,\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 20\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(372, 0, 'fashion_luvibee_2', 'config', 'config_section_blog_latest_blog', '{\n    \"name\": \"latest-blog\",\n    \"text\": \"Bài viết mới nhất\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Bài viết mới nhất\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 10\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(373, 0, 'fashion_luvibee_2', 'config', 'config_section_contact_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"map\": {\n            \"name\": \"map\",\n            \"text\": \"Bản đồ\",\n            \"address\": \"<iframe src=\\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\\" width=\\\"600\\\" height=\\\"450\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\" allowfullscreen></iframe>\",\n            \"visible\": 1\n        },\n        \"info\": {\n            \"name\": \"info\",\n            \"text\": \"Thông tin cửa hàng\",\n            \"email\": \"contact-us@novaon.asia\",\n            \"phone\": \"0000000000\",\n            \"address\": \"address\",\n            \"visible\": 1\n        },\n        \"form\": {\n            \"name\": \"form\",\n            \"title\": \"Form liên hệ\",\n            \"email\": \"email@mail.com\",\n            \"visible\": 1\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(374, 0, 'fashion_luvibee_2', 'config', 'config_section_contact_map', '{   //////// CÁI NÀY KHÔNG DÙNG NỮA\n    \"name\": \"map\",\n    \"text\": \"Bản đồ\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Bản đồ\"\n    },\n    \"display\": {\n        \"address\": \"<iframe src=\\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\\" width=\\\"600\\\" height=\\\"450\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\" allowfullscreen></iframe>\"\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(375, 0, 'fashion_luvibee_2', 'config', 'config_section_contact_contact', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\n    \"name\": \"contact\",\n    \"text\": \"Liên hệ\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Vị trí của chúng tôi\"\n    },\n    \"display\": {\n        \"store\": {\n            \"title\": \"Gian hàng\",\n            \"content\": \"Số 12 Dịch Vọng Hậu, Cầu Giấy, Hà Nội\",\n            \"visible\": 1\n        },\n        \"telephone\": {\n            \"title\": \"Điện thoại\",\n            \"content\": \"(+84) 987 654 321\",\n            \"visible\": 1\n        },\n        \"social_follow\": {\n            \"socials\": [\n                {\n                    \"name\": \"facebook\",\n                    \"text\": \"Facebook\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"twitter\",\n                    \"text\": \"Twitter\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"instagram\",\n                    \"text\": \"Instagram\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"tumblr\",\n                    \"text\": \"Tumblr\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"youtube\",\n                    \"text\": \"Youtube\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"googleplus\",\n                    \"text\": \"Google Plus\",\n                    \"visible\": 1\n                }\n            ],\n            \"visible\": 1\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(376, 0, 'fashion_luvibee_2', 'config', 'config_section_contact_form', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\n    \"name\": \"form\",\n    \"text\": \"Biểu mẫu\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Liên hệ với chúng tôi\"\n    },\n    \"data\": {\n        \"email\": \"sale.247@xshop.com\"\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_translation`
--

CREATE TABLE `cfr_translation` (
  `translation_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_upload`
--

CREATE TABLE `cfr_upload` (
  `upload_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_user`
--

CREATE TABLE `cfr_user` (
  `user_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(500) NOT NULL,
  `password` varchar(100) NOT NULL,
  `password_temp` varchar(100) DEFAULT NULL,
  `salt` varchar(9) NOT NULL,
  `salt_temp` varchar(9) DEFAULT NULL,
  `firstname` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `telephone` varchar(32) DEFAULT NULL,
  `image` varchar(1024) NOT NULL,
  `code` varchar(40) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `info` varchar(1024) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `permission` text,
  `date_added` datetime NOT NULL,
  `last_logged_in` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_user_group`
--

CREATE TABLE `cfr_user_group` (
  `user_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_user_group`
--

INSERT INTO `cfr_user_group` (`user_group_id`, `name`, `permission`) VALUES
(1, 'Administrator', '{"access":["app_store\\/detail","theme\\/ecommerce","settings\\/user_group","cash_flow\\/cash_flow","cash_flow\\/receipt_voucher","cash_flow\\/payment_voucher","app_store\\/my_app","extension\\/extension\\/appstore","common\\/app_config","blog\\/blog","blog\\/category","catalog\\/attribute","catalog\\/attribute_group","catalog\\/category","catalog\\/collection","catalog\\/download","catalog\\/filter","catalog\\/information","catalog\\/manufacturer","catalog\\/option","catalog\\/product","catalog\\/recurring","catalog\\/review","catalog\\/store_receipt","catalog\\/warehouse","catalog\\/store","catalog\\/cost_adjustment_receipt","catalog\\/store_take_receipt","catalog\\/store_transfer_receipt","common\\/cloudinary_upload","common\\/column_left","common\\/custom_column_left","common\\/custom_header","common\\/delivery_api","common\\/developer","common\\/filemanager","common\\/forgotten_orig","common\\/login_orig","common\\/profile","common\\/reset_orig","common\\/security","custom\\/group_menu","custom\\/menu","custom\\/menu_item","custom\\/preference","custom\\/theme","custom\\/theme_develop","customer\\/custom_field","customer\\/customer","customer\\/customer_approval","customer\\/customer_group","design\\/banner","design\\/layout","design\\/seo_url","design\\/theme","design\\/translation","discount\\/discount","discount\\/coupon","discount\\/setting","event\\/language","event\\/statistics","event\\/theme","extension\\/analytics\\/google","extension\\/captcha\\/basic","extension\\/captcha\\/google","extension\\/dashboard\\/activity","extension\\/dashboard\\/chart","extension\\/dashboard\\/customer","extension\\/dashboard\\/map","extension\\/dashboard\\/online","extension\\/dashboard\\/order","extension\\/dashboard\\/recent","extension\\/dashboard\\/sale","extension\\/extension\\/analytics","extension\\/extension\\/captcha","extension\\/extension\\/dashboard","extension\\/extension\\/feed","extension\\/extension\\/fraud","extension\\/extension\\/menu","extension\\/extension\\/module","extension\\/extension\\/payment","extension\\/extension\\/report","extension\\/extension\\/shipping","extension\\/extension\\/theme","extension\\/extension\\/total","extension\\/feed\\/google_base","extension\\/feed\\/google_sitemap","extension\\/feed\\/openbaypro","extension\\/fraud\\/fraudlabspro","extension\\/fraud\\/ip","extension\\/fraud\\/maxmind","extension\\/module\\/account","extension\\/module\\/age_restriction","extension\\/module\\/amazon_login","extension\\/module\\/amazon_pay","extension\\/module\\/banner","extension\\/module\\/bestseller","extension\\/module\\/carousel","extension\\/module\\/category","extension\\/module\\/customer_welcome","extension\\/module\\/divido_calculator","extension\\/module\\/ebay_listing","extension\\/module\\/featured","extension\\/module\\/filter","extension\\/module\\/google_hangouts","extension\\/module\\/hello_world","extension\\/module\\/html","extension\\/module\\/information","extension\\/module\\/klarna_checkout_module","extension\\/module\\/latest","extension\\/module\\/laybuy_layout","extension\\/module\\/modification_editor","extension\\/module\\/pilibaba_button","extension\\/module\\/pp_braintree_button","extension\\/module\\/pp_button","extension\\/module\\/pp_login","extension\\/module\\/recommendation","extension\\/module\\/sagepay_direct_cards","extension\\/module\\/sagepay_server_cards","extension\\/module\\/simple_blog","extension\\/module\\/simple_blog\\/article","extension\\/module\\/simple_blog\\/author","extension\\/module\\/simple_blog\\/category","extension\\/module\\/simple_blog\\/comment","extension\\/module\\/simple_blog\\/install","extension\\/module\\/simple_blog\\/report","extension\\/module\\/simple_blog_category","extension\\/module\\/slideshow","extension\\/module\\/so_categories","extension\\/module\\/so_category_slider","extension\\/module\\/so_deals","extension\\/module\\/so_extra_slider","extension\\/module\\/so_facebook_message","extension\\/module\\/so_filter_shop_by","extension\\/module\\/so_home_slider","extension\\/module\\/so_html_content","extension\\/module\\/so_latest_blog","extension\\/module\\/so_listing_tabs","extension\\/module\\/so_megamenu","extension\\/module\\/so_newletter_custom_popup","extension\\/module\\/so_onepagecheckout","extension\\/module\\/so_page_builder","extension\\/module\\/so_quickview","extension\\/module\\/so_searchpro","extension\\/module\\/so_sociallogin","extension\\/module\\/so_tools","extension\\/module\\/soconfig","extension\\/module\\/special","extension\\/module\\/store","extension\\/module\\/theme_builder_config","extension\\/openbay\\/amazon","extension\\/openbay\\/amazon_listing","extension\\/openbay\\/amazon_product","extension\\/openbay\\/amazonus","extension\\/openbay\\/amazonus_listing","extension\\/openbay\\/amazonus_product","extension\\/openbay\\/ebay","extension\\/openbay\\/ebay_profile","extension\\/openbay\\/ebay_template","extension\\/openbay\\/etsy","extension\\/openbay\\/etsy_product","extension\\/openbay\\/etsy_shipping","extension\\/openbay\\/etsy_shop","extension\\/openbay\\/fba","extension\\/payment\\/alipay","extension\\/payment\\/alipay_cross","extension\\/payment\\/amazon_login_pay","extension\\/payment\\/authorizenet_aim","extension\\/payment\\/authorizenet_sim","extension\\/payment\\/bank_transfer","extension\\/payment\\/bluepay_hosted","extension\\/payment\\/bluepay_redirect","extension\\/payment\\/cardconnect","extension\\/payment\\/cardinity","extension\\/payment\\/cheque","extension\\/payment\\/cod","extension\\/payment\\/divido","extension\\/payment\\/eway","extension\\/payment\\/firstdata","extension\\/payment\\/firstdata_remote","extension\\/payment\\/free_checkout","extension\\/payment\\/g2apay","extension\\/payment\\/globalpay","extension\\/payment\\/globalpay_remote","extension\\/payment\\/klarna_account","extension\\/payment\\/klarna_checkout","extension\\/payment\\/klarna_invoice","extension\\/payment\\/laybuy","extension\\/payment\\/liqpay","extension\\/payment\\/nochex","extension\\/payment\\/paymate","extension\\/payment\\/paypoint","extension\\/payment\\/payza","extension\\/payment\\/perpetual_payments","extension\\/payment\\/pilibaba","extension\\/payment\\/pp_braintree","extension\\/payment\\/pp_express","extension\\/payment\\/pp_payflow","extension\\/payment\\/pp_payflow_iframe","extension\\/payment\\/pp_pro","extension\\/payment\\/pp_pro_iframe","extension\\/payment\\/pp_standard","extension\\/payment\\/realex","extension\\/payment\\/realex_remote","extension\\/payment\\/sagepay_direct","extension\\/payment\\/sagepay_server","extension\\/payment\\/sagepay_us","extension\\/payment\\/securetrading_pp","extension\\/payment\\/securetrading_ws","extension\\/payment\\/skrill","extension\\/payment\\/squareup","extension\\/payment\\/twocheckout","extension\\/payment\\/web_payment_software","extension\\/payment\\/wechat_pay","extension\\/payment\\/worldpay","extension\\/report\\/customer_activity","extension\\/report\\/customer_order","extension\\/report\\/customer_reward","extension\\/report\\/customer_search","extension\\/report\\/customer_transaction","extension\\/report\\/marketing","extension\\/report\\/product_purchased","extension\\/report\\/product_viewed","extension\\/report\\/sale_coupon","extension\\/report\\/sale_order","extension\\/report\\/sale_return","extension\\/report\\/sale_shipping","extension\\/report\\/sale_tax","extension\\/shipping\\/auspost","extension\\/shipping\\/citylink","extension\\/shipping\\/ec_ship","extension\\/shipping\\/fedex","extension\\/shipping\\/flat","extension\\/shipping\\/free","extension\\/shipping\\/item","extension\\/shipping\\/parcelforce_48","extension\\/shipping\\/pickup","extension\\/shipping\\/royal_mail","extension\\/shipping\\/ups","extension\\/shipping\\/usps","extension\\/shipping\\/weight","extension\\/theme\\/default","extension\\/theme\\/dunght","extension\\/theme\\/novaon","extension\\/total\\/coupon","extension\\/total\\/credit","extension\\/total\\/handling","extension\\/total\\/klarna_fee","extension\\/total\\/low_order_fee","extension\\/total\\/reward","extension\\/total\\/shipping","extension\\/total\\/sub_total","extension\\/total\\/tax","extension\\/total\\/total","extension\\/total\\/voucher","localisation\\/country","localisation\\/currency","localisation\\/geo_zone","localisation\\/language","localisation\\/length_class","localisation\\/location","localisation\\/order_status","localisation\\/return_action","localisation\\/return_reason","localisation\\/return_status","localisation\\/stock_status","localisation\\/tax_class","localisation\\/tax_rate","localisation\\/weight_class","localisation\\/zone","mail\\/affiliate","mail\\/customer","mail\\/forgotten","mail\\/forgotten_orig","mail\\/return","mail\\/reward","mail\\/transaction","marketing\\/contact","marketing\\/coupon","marketing\\/marketing","marketplace\\/api","marketplace\\/event","marketplace\\/extension","marketplace\\/install","marketplace\\/installer","marketplace\\/marketplace","marketplace\\/modification","marketplace\\/openbay","online_store\\/contents","online_store\\/domain","online_store\\/google_shopping","report\\/online","report\\/overview","report\\/report","report\\/statistics","report\\/product","report\\/order","report\\/financial","report\\/staff","report\\/store","sale\\/order","sale\\/return_receipt","sale\\/recurring","sale\\/return","sale\\/voucher","sale\\/voucher_theme","sale_channel\\/pos_novaon","extension\\/appstore\\/mar_ons_chatbot","extension\\/appstore\\/mar_ons_onfluencer","sale_channel\\/novaonx_social","section\\/rate","rate\\/rate","section\\/blog","section\\/customize_layout","section\\/banner","section\\/best_sales_product","section\\/best_views_product","section\\/blog","section\\/content_customize","section\\/detail_product","section\\/footer","section\\/header","section\\/hot_product","section\\/list_product","section\\/new_product","section\\/partner","section\\/preview","section\\/sections","section\\/slideshow","section_blog\\/blog_category","section_blog\\/blog_list","section_blog\\/latest_blog","section_blog\\/sections","section_category\\/banner","section_category\\/filter","section_category\\/product_category","section_category\\/product_list","section_category\\/sections","section_contact\\/contact","section_contact\\/form","section_contact\\/map","section_contact\\/sections","section_product_detail\\/related_product","section_product_detail\\/sections","setting\\/setting","setting\\/store","settings\\/account","settings\\/classify","settings\\/delivery","settings\\/general","settings\\/payment","settings\\/settings","settings\\/notify","startup\\/error","startup\\/event","startup\\/login","startup\\/permission","startup\\/router","startup\\/sass","startup\\/startup","super\\/dashboard","theme\\/color","theme\\/favicon","theme\\/section_theme","theme\\/social","theme\\/text","tool\\/backup","tool\\/log","tool\\/upload","user\\/api","user\\/user","user\\/user_permission"],"modify":["app_store\\/detail","theme\\/ecommerce","settings\\/user_group","cash_flow\\/cash_flow","cash_flow\\/receipt_voucher","cash_flow\\/payment_voucher","app_store\\/my_app","extension\\/extension\\/appstore","common\\/app_config", "blog\\/blog","blog\\/category","catalog\\/attribute","catalog\\/attribute_group","catalog\\/category","catalog\\/collection","catalog\\/download","catalog\\/filter","catalog\\/information","catalog\\/manufacturer","catalog\\/option","catalog\\/product","catalog\\/recurring","catalog\\/review","catalog\\/store_receipt","catalog\\/warehouse","catalog\\/store","catalog\\/cost_adjustment_receipt","catalog\\/store_take_receipt","catalog\\/store_transfer_receipt","common\\/cloudinary_upload","common\\/column_left","common\\/custom_column_left","common\\/custom_header","common\\/delivery_api","common\\/developer","common\\/filemanager","common\\/forgotten_orig","common\\/login_orig","common\\/profile","common\\/reset_orig","common\\/security","custom\\/group_menu","custom\\/menu","custom\\/menu_item","custom\\/preference","custom\\/theme","custom\\/theme_develop","customer\\/custom_field","customer\\/customer","customer\\/customer_approval","customer\\/customer_group","design\\/banner","design\\/layout","design\\/seo_url","design\\/theme","design\\/translation","discount\\/discount","discount\\/coupon","discount\\/setting","event\\/language","event\\/statistics","event\\/theme","extension\\/analytics\\/google","extension\\/captcha\\/basic","extension\\/captcha\\/google","extension\\/dashboard\\/activity","extension\\/dashboard\\/chart","extension\\/dashboard\\/customer","extension\\/dashboard\\/map","extension\\/dashboard\\/online","extension\\/dashboard\\/order","extension\\/dashboard\\/recent","extension\\/dashboard\\/sale","extension\\/extension\\/analytics","extension\\/extension\\/captcha","extension\\/extension\\/dashboard","extension\\/extension\\/feed","extension\\/extension\\/fraud","extension\\/extension\\/menu","extension\\/extension\\/module","extension\\/extension\\/payment","extension\\/extension\\/report","extension\\/extension\\/shipping","extension\\/extension\\/theme","extension\\/extension\\/total","extension\\/feed\\/google_base","extension\\/feed\\/google_sitemap","extension\\/feed\\/openbaypro","extension\\/fraud\\/fraudlabspro","extension\\/fraud\\/ip","extension\\/fraud\\/maxmind","extension\\/module\\/account","extension\\/module\\/age_restriction","extension\\/module\\/amazon_login","extension\\/module\\/amazon_pay","extension\\/module\\/banner","extension\\/module\\/bestseller","extension\\/module\\/carousel","extension\\/module\\/category","extension\\/module\\/customer_welcome","extension\\/module\\/divido_calculator","extension\\/module\\/ebay_listing","extension\\/module\\/featured","extension\\/module\\/filter","extension\\/module\\/google_hangouts","extension\\/module\\/hello_world","extension\\/module\\/html","extension\\/module\\/information","extension\\/module\\/klarna_checkout_module","extension\\/module\\/latest","extension\\/module\\/laybuy_layout","extension\\/module\\/modification_editor","extension\\/module\\/pilibaba_button","extension\\/module\\/pp_braintree_button","extension\\/module\\/pp_button","extension\\/module\\/pp_login","extension\\/module\\/recommendation","extension\\/module\\/sagepay_direct_cards","extension\\/module\\/sagepay_server_cards","extension\\/module\\/simple_blog","extension\\/module\\/simple_blog\\/article","extension\\/module\\/simple_blog\\/author","extension\\/module\\/simple_blog\\/category","extension\\/module\\/simple_blog\\/comment","extension\\/module\\/simple_blog\\/install","extension\\/module\\/simple_blog\\/report","extension\\/module\\/simple_blog_category","extension\\/module\\/slideshow","extension\\/module\\/so_categories","extension\\/module\\/so_category_slider","extension\\/module\\/so_deals","extension\\/module\\/so_extra_slider","extension\\/module\\/so_facebook_message","extension\\/module\\/so_filter_shop_by","extension\\/module\\/so_home_slider","extension\\/module\\/so_html_content","extension\\/module\\/so_latest_blog","extension\\/module\\/so_listing_tabs","extension\\/module\\/so_megamenu","extension\\/module\\/so_newletter_custom_popup","extension\\/module\\/so_onepagecheckout","extension\\/module\\/so_page_builder","extension\\/module\\/so_quickview","extension\\/module\\/so_searchpro","extension\\/module\\/so_sociallogin","extension\\/module\\/so_tools","extension\\/module\\/soconfig","extension\\/module\\/special","extension\\/module\\/store","extension\\/module\\/theme_builder_config","extension\\/openbay\\/amazon","extension\\/openbay\\/amazon_listing","extension\\/openbay\\/amazon_product","extension\\/openbay\\/amazonus","extension\\/openbay\\/amazonus_listing","extension\\/openbay\\/amazonus_product","extension\\/openbay\\/ebay","extension\\/openbay\\/ebay_profile","extension\\/openbay\\/ebay_template","extension\\/openbay\\/etsy","extension\\/openbay\\/etsy_product","extension\\/openbay\\/etsy_shipping","extension\\/openbay\\/etsy_shop","extension\\/openbay\\/fba","extension\\/payment\\/alipay","extension\\/payment\\/alipay_cross","extension\\/payment\\/amazon_login_pay","extension\\/payment\\/authorizenet_aim","extension\\/payment\\/authorizenet_sim","extension\\/payment\\/bank_transfer","extension\\/payment\\/bluepay_hosted","extension\\/payment\\/bluepay_redirect","extension\\/payment\\/cardconnect","extension\\/payment\\/cardinity","extension\\/payment\\/cheque","extension\\/payment\\/cod","extension\\/payment\\/divido","extension\\/payment\\/eway","extension\\/payment\\/firstdata","extension\\/payment\\/firstdata_remote","extension\\/payment\\/free_checkout","extension\\/payment\\/g2apay","extension\\/payment\\/globalpay","extension\\/payment\\/globalpay_remote","extension\\/payment\\/klarna_account","extension\\/payment\\/klarna_checkout","extension\\/payment\\/klarna_invoice","extension\\/payment\\/laybuy","extension\\/payment\\/liqpay","extension\\/payment\\/nochex","extension\\/payment\\/paymate","extension\\/payment\\/paypoint","extension\\/payment\\/payza","extension\\/payment\\/perpetual_payments","extension\\/payment\\/pilibaba","extension\\/payment\\/pp_braintree","extension\\/payment\\/pp_express","extension\\/payment\\/pp_payflow","extension\\/payment\\/pp_payflow_iframe","extension\\/payment\\/pp_pro","extension\\/payment\\/pp_pro_iframe","extension\\/payment\\/pp_standard","extension\\/payment\\/realex","extension\\/payment\\/realex_remote","extension\\/payment\\/sagepay_direct","extension\\/payment\\/sagepay_server","extension\\/payment\\/sagepay_us","extension\\/payment\\/securetrading_pp","extension\\/payment\\/securetrading_ws","extension\\/payment\\/skrill","extension\\/payment\\/squareup","extension\\/payment\\/twocheckout","extension\\/payment\\/web_payment_software","extension\\/payment\\/wechat_pay","extension\\/payment\\/worldpay","extension\\/report\\/customer_activity","extension\\/report\\/customer_order","extension\\/report\\/customer_reward","extension\\/report\\/customer_search","extension\\/report\\/customer_transaction","extension\\/report\\/marketing","extension\\/report\\/product_purchased","extension\\/report\\/product_viewed","extension\\/report\\/sale_coupon","extension\\/report\\/sale_order","extension\\/report\\/sale_return","extension\\/report\\/sale_shipping","extension\\/report\\/sale_tax","extension\\/shipping\\/auspost","extension\\/shipping\\/citylink","extension\\/shipping\\/ec_ship","extension\\/shipping\\/fedex","extension\\/shipping\\/flat","extension\\/shipping\\/free","extension\\/shipping\\/item","extension\\/shipping\\/parcelforce_48","extension\\/shipping\\/pickup","extension\\/shipping\\/royal_mail","extension\\/shipping\\/ups","extension\\/shipping\\/usps","extension\\/shipping\\/weight","extension\\/theme\\/default","extension\\/theme\\/dunght","extension\\/theme\\/novaon","extension\\/total\\/coupon","extension\\/total\\/credit","extension\\/total\\/handling","extension\\/total\\/klarna_fee","extension\\/total\\/low_order_fee","extension\\/total\\/reward","extension\\/total\\/shipping","extension\\/total\\/sub_total","extension\\/total\\/tax","extension\\/total\\/total","extension\\/total\\/voucher","localisation\\/country","localisation\\/currency","localisation\\/geo_zone","localisation\\/language","localisation\\/length_class","localisation\\/location","localisation\\/order_status","localisation\\/return_action","localisation\\/return_reason","localisation\\/return_status","localisation\\/stock_status","localisation\\/tax_class","localisation\\/tax_rate","localisation\\/weight_class","localisation\\/zone","mail\\/affiliate","mail\\/customer","mail\\/forgotten","mail\\/forgotten_orig","mail\\/return","mail\\/reward","mail\\/transaction","marketing\\/contact","marketing\\/coupon","marketing\\/marketing","marketplace\\/api","marketplace\\/event","marketplace\\/extension","marketplace\\/install","marketplace\\/installer","marketplace\\/marketplace","marketplace\\/modification","marketplace\\/openbay","online_store\\/contents","online_store\\/domain","online_store\\/google_shopping","report\\/online","report\\/overview","report\\/report","report\\/statistics","report\\/product","report\\/order","report\\/financial","report\\/staff","report\\/store","sale\\/order","sale\\/return_receipt","sale\\/recurring","sale\\/return","sale\\/voucher","sale\\/voucher_theme","sale_channel\\/pos_novaon","extension\\/appstore\\/mar_ons_chatbot","extension\\/appstore\\/mar_ons_onfluencer","sale_channel\\/novaonx_social","section\\/rate","rate\\/rate","section\\/blog","section\\/customize_layout","section\\/banner","section\\/best_sales_product","section\\/best_views_product","section\\/blog","section\\/content_customize","section\\/detail_product","section\\/footer","section\\/header","section\\/hot_product","section\\/list_product","section\\/new_product","section\\/partner","section\\/preview","section\\/sections","section\\/slideshow","section_blog\\/blog_category","section_blog\\/blog_list","section_blog\\/latest_blog","section_blog\\/sections","section_category\\/banner","section_category\\/filter","section_category\\/product_category","section_category\\/product_list","section_category\\/sections","section_contact\\/contact","section_contact\\/form","section_contact\\/map","section_contact\\/sections","section_product_detail\\/related_product","section_product_detail\\/sections","setting\\/setting","setting\\/store","settings\\/account","settings\\/classify","settings\\/delivery","settings\\/general","settings\\/payment","settings\\/settings","settings\\/notify","startup\\/error","startup\\/event","startup\\/login","startup\\/permission","startup\\/router","startup\\/sass","startup\\/startup","super\\/dashboard","theme\\/color","theme\\/favicon","theme\\/section_theme","theme\\/social","theme\\/text","tool\\/backup","tool\\/log","tool\\/upload","user\\/api","user\\/user","user\\/user_permission"]}'),
(10, 'Demonstration', '');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_voucher`
--

CREATE TABLE `cfr_voucher` (
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_voucher_history`
--

CREATE TABLE `cfr_voucher_history` (
  `voucher_history_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_voucher_theme`
--

CREATE TABLE `cfr_voucher_theme` (
  `voucher_theme_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_voucher_theme`
--

INSERT INTO `cfr_voucher_theme` (`voucher_theme_id`, `image`) VALUES
(8, 'catalog/demo/canon_eos_5d_2.jpg'),
(7, 'catalog/demo/gift-voucher-birthday.jpg'),
(6, 'catalog/demo/apple_logo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_voucher_theme_description`
--

CREATE TABLE `cfr_voucher_theme_description` (
  `voucher_theme_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_voucher_theme_description`
--

INSERT INTO `cfr_voucher_theme_description` (`voucher_theme_id`, `language_id`, `name`) VALUES
(6, 1, 'Christmas'),
(7, 1, 'Birthday'),
(8, 1, 'General');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_warehouse`
--

CREATE TABLE `cfr_warehouse` (
  `warehouse_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_version_id` varchar(256) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cfr_weight_class`
--

CREATE TABLE `cfr_weight_class` (
  `weight_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL DEFAULT '0.00000000'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_weight_class`
--

INSERT INTO `cfr_weight_class` (`weight_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '1000.00000000'),
(5, '2.20460000'),
(6, '35.27400000');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_weight_class_description`
--

CREATE TABLE `cfr_weight_class_description` (
  `weight_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_weight_class_description`
--

INSERT INTO `cfr_weight_class_description` (`weight_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Kilogram', 'kg'),
(2, 1, 'Gram', 'g'),
(5, 1, 'Pound ', 'lb'),
(6, 1, 'Ounce', 'oz'),
(1, 2, 'Kilogram', 'kg'),
(2, 2, 'Gram', 'g'),
(5, 2, 'Pound ', 'lb'),
(6, 2, 'Ounce', 'oz');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_zone`
--

CREATE TABLE `cfr_zone` (
  `zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_zone`
--

INSERT INTO `cfr_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1, 230, 'Hà Nội', 'HN', 1),
(2, 230, 'Hồ Chí Minh', 'HCM', 1),
(3, 230, 'An Giang', 'AG', 1),
(4, 230, 'Bà Rịa-Vũng Tàu', 'BR', 1),
(5, 230, 'Bạc Liêu', 'BL', 1),
(6, 230, 'Bắc Kạn', 'BK', 1),
(7, 230, 'Bắc Giang', 'BG', 1),
(8, 230, 'Bắc Ninh', 'BN', 1),
(9, 230, 'Bến Tre', 'BT', 1),
(10, 230, 'Bình Dương', 'BD', 1),
(11, 230, 'Bình Định', 'BĐ', 1),
(12, 230, 'Bình Phước', 'BP', 1),
(13, 230, 'Bình Thuận', 'BT', 1),
(14, 230, 'Cà Mau', 'CM', 1),
(15, 230, 'Cao Bằng', 'CB', 1),
(16, 230, 'Cần Thơ', 'CT', 1),
(17, 230, 'Đà Nẵng', 'DN', 1),
(18, 230, 'Đắk Lắk', 'DL', 1),
(19, 230, 'Đắk Nông', 'DKN', 1),
(20, 230, 'Điện Biên', 'DB', 1),
(21, 230, 'Đồng Nai', 'DNN', 1),
(22, 230, 'Đồng Tháp', 'DT', 1),
(23, 230, 'Gia Lai', 'GL', 1),
(24, 230, 'Hà Giang', 'HG', 1),
(25, 230, 'Hà Nam', 'HNN', 1),
(26, 230, 'Hà Tĩnh', 'HT', 1),
(27, 230, 'Hải Dương', 'HD', 1),
(28, 230, 'Hải Phòng', 'HP', 1),
(29, 230, 'Hòa Bình', 'HB', 1),
(30, 230, 'Hậu Giang', 'HG', 1),
(31, 230, 'Hưng Yên', 'HY', 1),
(32, 230, 'Khánh Hòa', 'KH', 1),
(33, 230, 'Kiên Giang', 'KG', 1),
(34, 230, 'Kon Tum', 'KT', 1),
(35, 230, 'Lai Châu', 'LCU', 1),
(36, 230, 'Lào Cai', 'LC', 1),
(37, 230, 'Lạng Sơn', 'LS', 1),
(38, 230, 'Lâm Đồng', 'LD', 1),
(39, 230, 'Long An', 'LA', 1),
(40, 230, 'Nam Định', 'ND', 1),
(41, 230, 'Nghệ An', 'NA', 1),
(42, 230, 'Ninh Bình', 'NB', 1),
(43, 230, 'Ninh Thuận', 'NT', 1),
(44, 230, 'Phú Thọ', 'PT', 1),
(45, 230, 'Phú Yên', 'PY', 1),
(46, 230, 'Quảng Bình', 'QB', 1),
(47, 230, 'Quảng Nam', 'QNN', 1),
(48, 230, 'Quảng Ngãi', 'QNG', 1),
(49, 230, 'Quảng Ninh', 'QN', 1),
(50, 230, 'Quảng Trị', 'QT', 1),
(51, 230, 'Sóc Trăng', 'ST', 1),
(52, 230, 'Sơn La', 'SL', 1),
(53, 230, 'Tây Ninh', 'TN', 1),
(54, 230, 'Thái Bình', 'TB', 1),
(55, 230, 'Thái Nguyên', 'TN', 1),
(56, 230, 'Thanh Hóa', 'TH', 1),
(57, 230, 'Thừa Thiên - Huế', 'TTH', 1),
(58, 230, 'Tiền Giang', 'TG', 1),
(59, 230, 'Trà Vinh', 'TV', 1),
(60, 230, 'Tuyên Quang', 'TQ', 1),
(61, 230, 'Vĩnh Long', 'VL', 1),
(62, 230, 'Vĩnh Phúc', 'VP', 1),
(63, 230, 'Yên Bái', 'YB', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cfr_zone_to_geo_zone`
--

CREATE TABLE `cfr_zone_to_geo_zone` (
  `zone_to_geo_zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `geo_zone_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cfr_zone_to_geo_zone`
--

INSERT INTO `cfr_zone_to_geo_zone` (`zone_to_geo_zone_id`, `country_id`, `zone_id`, `geo_zone_id`, `date_added`, `date_modified`) VALUES
(1, 222, 0, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 222, 3513, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 222, 3514, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 222, 3515, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 222, 3516, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 222, 3517, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 222, 3518, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 222, 3519, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 222, 3520, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 222, 3521, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 222, 3522, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 222, 3523, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 222, 3524, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 222, 3525, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 222, 3526, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 222, 3527, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 222, 3528, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 222, 3529, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 222, 3530, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 222, 3531, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 222, 3532, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 222, 3533, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 222, 3534, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 222, 3535, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 222, 3536, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 222, 3537, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 222, 3538, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 222, 3539, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 222, 3540, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 222, 3541, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 222, 3542, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 222, 3543, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 222, 3544, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 222, 3545, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 222, 3546, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 222, 3547, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 222, 3548, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 222, 3549, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 222, 3550, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 222, 3551, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 222, 3552, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 222, 3553, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 222, 3554, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 222, 3555, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 222, 3556, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 222, 3557, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 222, 3558, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 222, 3559, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 222, 3560, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 222, 3561, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 222, 3562, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 222, 3563, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 222, 3564, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 222, 3565, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 222, 3566, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 222, 3567, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 222, 3568, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 222, 3569, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 222, 3570, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 222, 3571, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 222, 3572, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 222, 3573, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 222, 3574, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 222, 3575, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 222, 3576, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 222, 3577, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 222, 3578, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 222, 3579, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 222, 3580, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 222, 3581, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 222, 3582, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 222, 3583, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 222, 3584, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 222, 3585, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 222, 3586, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 222, 3587, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 222, 3588, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 222, 3589, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 222, 3590, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 222, 3591, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 222, 3592, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 222, 3593, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 222, 3594, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 222, 3595, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 222, 3596, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 222, 3597, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 222, 3598, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 222, 3599, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 222, 3600, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 222, 3601, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 222, 3602, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 222, 3603, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 222, 3604, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 222, 3605, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 222, 3606, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 222, 3607, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 222, 3608, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 222, 3609, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 222, 3610, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 222, 3611, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 222, 3612, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 222, 3949, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 222, 3950, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 222, 3951, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 222, 3952, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 222, 3953, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 222, 3954, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 222, 3955, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 222, 3972, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cfr_timezones`
--

CREATE TABLE `cfr_timezones` (
  `id` int(11) NOT NULL,
  `value` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cfr_timezones`
--

INSERT INTO `cfr_timezones` (`id`, `value`, `label`) VALUES
(1, '-12:00', '(GMT -12:00) Eniwetok, Kwajalein'),
(2, '-11:00', '(GMT -11:00) Midway Island, Samoa'),
(3, '-10:00', '(GMT -10:00) Hawaii'),
(4, '-09:50', '(GMT -9:30) Taiohae'),
(5, '-09:00', '(GMT -9:00) Alaska'),
(6, '-08:00', '(GMT -8:00) Pacific Time (US &amp; Canada)'),
(7, '-07:00', '(GMT -7:00) Mountain Time (US &amp; Canada)'),
(8, '-06:00', '(GMT -6:00) Central Time (US &amp; Canada), Mexico City'),
(9, '-05:00', '(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima'),
(10, '-04:50', '(GMT -4:30) Caracas'),
(11, '-04:00', '(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz'),
(12, '-03:50', '(GMT -3:30) Newfoundland'),
(13, '-03:00', '(GMT -3:00) Brazil, Buenos Aires, Georgetown'),
(14, '-02:00', '(GMT -2:00) Mid-Atlantic'),
(15, '-01:00', '(GMT -1:00) Azores, Cape Verde Islands'),
(16, '+00:00', '(GMT) Western Europe Time, London, Lisbon, Casablanca'),
(17, '+01:00', '(GMT +1:00) Brussels, Copenhagen, Madrid, Paris'),
(18, '+02:00', '(GMT +2:00) Kaliningrad, South Africa'),
(19, '+03:00', '(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg'),
(20, '+03:50', '(GMT +3:30) Tehran'),
(21, '+04:00', '(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi'),
(22, '+04:50', '(GMT +4:30) Kabul'),
(23, '+05:00', '(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent'),
(24, '+05:50', '(GMT +5:30) Bombay, Calcutta, Madras, New Delhi'),
(25, '+05:75', '(GMT +5:45) Kathmandu, Pokhar'),
(26, '+06:00', '(GMT +6:00) Almaty, Dhaka, Colombo'),
(27, '+06:50', '(GMT +6:30) Yangon, Mandalay'),
(28, '+07:00', '(GMT +7:00) Bangkok, Hanoi, Jakarta'),
(29, '+08:00', '(GMT +8:00) Beijing, Perth, Singapore, Hong Kong'),
(30, '+08:75', '(GMT +8:45) Eucla'),
(31, '+09:00', '(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk'),
(32, '+09:50', '(GMT +9:30) Adelaide, Darwin'),
(33, '+10:00', '(GMT +10:00) Eastern Australia, Guam, Vladivostok'),
(34, '+10:50', '(GMT +10:30) Lord Howe Island'),
(35, '+11:00', '(GMT +11:00) Magadan, Solomon Islands, New Caledonia'),
(36, '+11:50', '(GMT +11:30) Norfolk Island'),
(37, '+12:00', '(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka'),
(38, '+12:75', '(GMT +12:45) Chatham Islands'),
(39, '+13:00', '(GMT +13:00) Apia, Nukualofa'),
(40, '+14:00', '(GMT +14:00) Line Islands, Tokelau');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cfr_address`
--
ALTER TABLE `cfr_address`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `cfr_api`
--
ALTER TABLE `cfr_api`
  ADD PRIMARY KEY (`api_id`);

--
-- Indexes for table `cfr_api_ip`
--
ALTER TABLE `cfr_api_ip`
  ADD PRIMARY KEY (`api_ip_id`);

--
-- Indexes for table `cfr_api_session`
--
ALTER TABLE `cfr_api_session`
  ADD PRIMARY KEY (`api_session_id`);

--
-- Indexes for table `cfr_attribute`
--
ALTER TABLE `cfr_attribute`
  ADD PRIMARY KEY (`attribute_id`);

--
-- Indexes for table `cfr_attribute_description`
--
ALTER TABLE `cfr_attribute_description`
  ADD PRIMARY KEY (`attribute_id`,`language_id`);

--
-- Indexes for table `cfr_attribute_group`
--
ALTER TABLE `cfr_attribute_group`
  ADD PRIMARY KEY (`attribute_group_id`);

--
-- Indexes for table `cfr_attribute_group_description`
--
ALTER TABLE `cfr_attribute_group_description`
  ADD PRIMARY KEY (`attribute_group_id`,`language_id`);

--
-- Indexes for table `cfr_banner`
--
ALTER TABLE `cfr_banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `cfr_banner_image`
--
ALTER TABLE `cfr_banner_image`
  ADD PRIMARY KEY (`banner_image_id`);

--
-- Indexes for table `cfr_cart`
--
ALTER TABLE `cfr_cart`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `cart_id` (`api_id`,`customer_id`,`session_id`,`product_id`,`recurring_id`);

--
-- Indexes for table `cfr_category`
--
ALTER TABLE `cfr_category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `cfr_category_description`
--
ALTER TABLE `cfr_category_description`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `cfr_category_filter`
--
ALTER TABLE `cfr_category_filter`
  ADD PRIMARY KEY (`category_id`,`filter_id`);

--
-- Indexes for table `cfr_category_path`
--
ALTER TABLE `cfr_category_path`
  ADD PRIMARY KEY (`category_id`,`path_id`);

--
-- Indexes for table `cfr_category_to_layout`
--
ALTER TABLE `cfr_category_to_layout`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Indexes for table `cfr_category_to_store`
--
ALTER TABLE `cfr_category_to_store`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Indexes for table `cfr_collection`
--
ALTER TABLE `cfr_collection`
  ADD PRIMARY KEY (`collection_id`);

--
-- Indexes for table `cfr_collection_description`
--
ALTER TABLE `cfr_collection_description`
  ADD PRIMARY KEY (`collection_id`);

--
-- Indexes for table `cfr_country`
--
ALTER TABLE `cfr_country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `cfr_coupon`
--
ALTER TABLE `cfr_coupon`
  ADD PRIMARY KEY (`coupon_id`);

--
-- Indexes for table `cfr_coupon_category`
--
ALTER TABLE `cfr_coupon_category`
  ADD PRIMARY KEY (`coupon_id`,`category_id`);

--
-- Indexes for table `cfr_coupon_history`
--
ALTER TABLE `cfr_coupon_history`
  ADD PRIMARY KEY (`coupon_history_id`);

--
-- Indexes for table `cfr_coupon_product`
--
ALTER TABLE `cfr_coupon_product`
  ADD PRIMARY KEY (`coupon_product_id`);

--
-- Indexes for table `cfr_currency`
--
ALTER TABLE `cfr_currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Indexes for table `cfr_customer`
--
ALTER TABLE `cfr_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `cfr_customer_activity`
--
ALTER TABLE `cfr_customer_activity`
  ADD PRIMARY KEY (`customer_activity_id`);

--
-- Indexes for table `cfr_customer_affiliate`
--
ALTER TABLE `cfr_customer_affiliate`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `cfr_customer_approval`
--
ALTER TABLE `cfr_customer_approval`
  ADD PRIMARY KEY (`customer_approval_id`);

--
-- Indexes for table `cfr_customer_group`
--
ALTER TABLE `cfr_customer_group`
  ADD PRIMARY KEY (`customer_group_id`);

--
-- Indexes for table `cfr_customer_group_description`
--
ALTER TABLE `cfr_customer_group_description`
  ADD PRIMARY KEY (`customer_group_id`,`language_id`);

--
-- Indexes for table `cfr_customer_history`
--
ALTER TABLE `cfr_customer_history`
  ADD PRIMARY KEY (`customer_history_id`);

--
-- Indexes for table `cfr_customer_ip`
--
ALTER TABLE `cfr_customer_ip`
  ADD PRIMARY KEY (`customer_ip_id`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `cfr_customer_login`
--
ALTER TABLE `cfr_customer_login`
  ADD PRIMARY KEY (`customer_login_id`),
  ADD KEY `email` (`email`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `cfr_customer_online`
--
ALTER TABLE `cfr_customer_online`
  ADD PRIMARY KEY (`ip`);

--
-- Indexes for table `cfr_customer_reward`
--
ALTER TABLE `cfr_customer_reward`
  ADD PRIMARY KEY (`customer_reward_id`);

--
-- Indexes for table `cfr_customer_search`
--
ALTER TABLE `cfr_customer_search`
  ADD PRIMARY KEY (`customer_search_id`);

--
-- Indexes for table `cfr_customer_transaction`
--
ALTER TABLE `cfr_customer_transaction`
  ADD PRIMARY KEY (`customer_transaction_id`);

--
-- Indexes for table `cfr_customer_wishlist`
--
ALTER TABLE `cfr_customer_wishlist`
  ADD PRIMARY KEY (`customer_id`,`product_id`);

--
-- Indexes for table `cfr_custom_field`
--
ALTER TABLE `cfr_custom_field`
  ADD PRIMARY KEY (`custom_field_id`);

--
-- Indexes for table `cfr_custom_field_customer_group`
--
ALTER TABLE `cfr_custom_field_customer_group`
  ADD PRIMARY KEY (`custom_field_id`,`customer_group_id`);

--
-- Indexes for table `cfr_custom_field_description`
--
ALTER TABLE `cfr_custom_field_description`
  ADD PRIMARY KEY (`custom_field_id`,`language_id`);

--
-- Indexes for table `cfr_custom_field_value`
--
ALTER TABLE `cfr_custom_field_value`
  ADD PRIMARY KEY (`custom_field_value_id`);

--
-- Indexes for table `cfr_custom_field_value_description`
--
ALTER TABLE `cfr_custom_field_value_description`
  ADD PRIMARY KEY (`custom_field_value_id`,`language_id`);

--
-- Indexes for table `cfr_download`
--
ALTER TABLE `cfr_download`
  ADD PRIMARY KEY (`download_id`);

--
-- Indexes for table `cfr_download_description`
--
ALTER TABLE `cfr_download_description`
  ADD PRIMARY KEY (`download_id`,`language_id`);

--
-- Indexes for table `cfr_event`
--
ALTER TABLE `cfr_event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `cfr_extension`
--
ALTER TABLE `cfr_extension`
  ADD PRIMARY KEY (`extension_id`);

--
-- Indexes for table `cfr_extension_install`
--
ALTER TABLE `cfr_extension_install`
  ADD PRIMARY KEY (`extension_install_id`);

--
-- Indexes for table `cfr_extension_path`
--
ALTER TABLE `cfr_extension_path`
  ADD PRIMARY KEY (`extension_path_id`);

--
-- Indexes for table `cfr_filter`
--
ALTER TABLE `cfr_filter`
  ADD PRIMARY KEY (`filter_id`);

--
-- Indexes for table `cfr_filter_description`
--
ALTER TABLE `cfr_filter_description`
  ADD PRIMARY KEY (`filter_id`,`language_id`);

--
-- Indexes for table `cfr_filter_group`
--
ALTER TABLE `cfr_filter_group`
  ADD PRIMARY KEY (`filter_group_id`);

--
-- Indexes for table `cfr_filter_group_description`
--
ALTER TABLE `cfr_filter_group_description`
  ADD PRIMARY KEY (`filter_group_id`,`language_id`);

--
-- Indexes for table `cfr_geo_zone`
--
ALTER TABLE `cfr_geo_zone`
  ADD PRIMARY KEY (`geo_zone_id`);

--
-- Indexes for table `cfr_information`
--
ALTER TABLE `cfr_information`
  ADD PRIMARY KEY (`information_id`);

--
-- Indexes for table `cfr_information_description`
--
ALTER TABLE `cfr_information_description`
  ADD PRIMARY KEY (`information_id`,`language_id`);

--
-- Indexes for table `cfr_information_to_layout`
--
ALTER TABLE `cfr_information_to_layout`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Indexes for table `cfr_information_to_store`
--
ALTER TABLE `cfr_information_to_store`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Indexes for table `cfr_language`
--
ALTER TABLE `cfr_language`
  ADD PRIMARY KEY (`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `cfr_layout`
--
ALTER TABLE `cfr_layout`
  ADD PRIMARY KEY (`layout_id`);

--
-- Indexes for table `cfr_layout_module`
--
ALTER TABLE `cfr_layout_module`
  ADD PRIMARY KEY (`layout_module_id`);

--
-- Indexes for table `cfr_layout_route`
--
ALTER TABLE `cfr_layout_route`
  ADD PRIMARY KEY (`layout_route_id`);

--
-- Indexes for table `cfr_length_class`
--
ALTER TABLE `cfr_length_class`
  ADD PRIMARY KEY (`length_class_id`);

--
-- Indexes for table `cfr_length_class_description`
--
ALTER TABLE `cfr_length_class_description`
  ADD PRIMARY KEY (`length_class_id`,`language_id`);

--
-- Indexes for table `cfr_location`
--
ALTER TABLE `cfr_location`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `cfr_manufacturer`
--
ALTER TABLE `cfr_manufacturer`
  ADD PRIMARY KEY (`manufacturer_id`);

--
-- Indexes for table `cfr_manufacturer_to_store`
--
ALTER TABLE `cfr_manufacturer_to_store`
  ADD PRIMARY KEY (`manufacturer_id`,`store_id`);

--
-- Indexes for table `cfr_marketing`
--
ALTER TABLE `cfr_marketing`
  ADD PRIMARY KEY (`marketing_id`);

--
-- Indexes for table `cfr_modification`
--
ALTER TABLE `cfr_modification`
  ADD PRIMARY KEY (`modification_id`);

--
-- Indexes for table `cfr_module`
--
ALTER TABLE `cfr_module`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `cfr_novaon_group_menu`
--
ALTER TABLE `cfr_novaon_group_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cfr_novaon_group_menu_description`
--
ALTER TABLE `cfr_novaon_group_menu_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cfr_novaon_menu_item`
--
ALTER TABLE `cfr_novaon_menu_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cfr_novaon_menu_item_description`
--
ALTER TABLE `cfr_novaon_menu_item_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cfr_novaon_relation_table`
--
ALTER TABLE `cfr_novaon_relation_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cfr_novaon_type`
--
ALTER TABLE `cfr_novaon_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cfr_novaon_type_description`
--
ALTER TABLE `cfr_novaon_type_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cfr_option`
--
ALTER TABLE `cfr_option`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `cfr_option_description`
--
ALTER TABLE `cfr_option_description`
  ADD PRIMARY KEY (`option_id`,`language_id`);

--
-- Indexes for table `cfr_option_value`
--
ALTER TABLE `cfr_option_value`
  ADD PRIMARY KEY (`option_value_id`);

--
-- Indexes for table `cfr_option_value_description`
--
ALTER TABLE `cfr_option_value_description`
  ADD PRIMARY KEY (`option_value_id`,`language_id`);

--
-- Indexes for table `cfr_order`
--
ALTER TABLE `cfr_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `cfr_order_history`
--
ALTER TABLE `cfr_order_history`
  ADD PRIMARY KEY (`order_history_id`);

--
-- Indexes for table `cfr_order_option`
--
ALTER TABLE `cfr_order_option`
  ADD PRIMARY KEY (`order_option_id`);

--
-- Indexes for table `cfr_order_product`
--
ALTER TABLE `cfr_order_product`
  ADD PRIMARY KEY (`order_product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `cfr_order_recurring`
--
ALTER TABLE `cfr_order_recurring`
  ADD PRIMARY KEY (`order_recurring_id`);

--
-- Indexes for table `cfr_order_recurring_transaction`
--
ALTER TABLE `cfr_order_recurring_transaction`
  ADD PRIMARY KEY (`order_recurring_transaction_id`);

--
-- Indexes for table `cfr_order_shipment`
--
ALTER TABLE `cfr_order_shipment`
  ADD PRIMARY KEY (`order_shipment_id`);

--
-- Indexes for table `cfr_order_status`
--
ALTER TABLE `cfr_order_status`
  ADD PRIMARY KEY (`order_status_id`,`language_id`);

--
-- Indexes for table `cfr_order_tag`
--
ALTER TABLE `cfr_order_tag`
  ADD PRIMARY KEY (`order_tag_id`);

--
-- Indexes for table `cfr_order_total`
--
ALTER TABLE `cfr_order_total`
  ADD PRIMARY KEY (`order_total_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `cfr_order_voucher`
--
ALTER TABLE `cfr_order_voucher`
  ADD PRIMARY KEY (`order_voucher_id`);

--
-- Indexes for table `cfr_page_contents`
--
ALTER TABLE `cfr_page_contents`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `cfr_product`
--
ALTER TABLE `cfr_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `cfr_product_attribute`
--
ALTER TABLE `cfr_product_attribute`
  ADD PRIMARY KEY (`product_id`,`attribute_id`,`language_id`);

--
-- Indexes for table `cfr_product_collection`
--
ALTER TABLE `cfr_product_collection`
  ADD PRIMARY KEY (`product_collection_id`);

--
-- Indexes for table `cfr_product_description`
--
ALTER TABLE `cfr_product_description`
  ADD PRIMARY KEY (`product_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `cfr_product_discount`
--
ALTER TABLE `cfr_product_discount`
  ADD PRIMARY KEY (`product_discount_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `cfr_product_filter`
--
ALTER TABLE `cfr_product_filter`
  ADD PRIMARY KEY (`product_id`,`filter_id`);

--
-- Indexes for table `cfr_product_image`
--
ALTER TABLE `cfr_product_image`
  ADD PRIMARY KEY (`product_image_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `cfr_product_option`
--
ALTER TABLE `cfr_product_option`
  ADD PRIMARY KEY (`product_option_id`);

--
-- Indexes for table `cfr_product_option_value`
--
ALTER TABLE `cfr_product_option_value`
  ADD PRIMARY KEY (`product_option_value_id`);

--
-- Indexes for table `cfr_product_recurring`
--
ALTER TABLE `cfr_product_recurring`
  ADD PRIMARY KEY (`product_id`,`recurring_id`,`customer_group_id`);

--
-- Indexes for table `cfr_product_related`
--
ALTER TABLE `cfr_product_related`
  ADD PRIMARY KEY (`product_id`,`related_id`);

--
-- Indexes for table `cfr_product_reward`
--
ALTER TABLE `cfr_product_reward`
  ADD PRIMARY KEY (`product_reward_id`);

--
-- Indexes for table `cfr_product_special`
--
ALTER TABLE `cfr_product_special`
  ADD PRIMARY KEY (`product_special_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `cfr_product_tag`
--
ALTER TABLE `cfr_product_tag`
  ADD PRIMARY KEY (`product_tag_id`);

--
-- Indexes for table `cfr_product_to_category`
--
ALTER TABLE `cfr_product_to_category`
  ADD PRIMARY KEY (`product_id`,`category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `cfr_product_to_download`
--
ALTER TABLE `cfr_product_to_download`
  ADD PRIMARY KEY (`product_id`,`download_id`);

--
-- Indexes for table `cfr_product_to_layout`
--
ALTER TABLE `cfr_product_to_layout`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Indexes for table `cfr_product_to_store`
--
ALTER TABLE `cfr_product_to_store`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Indexes for table `cfr_product_version`
--
ALTER TABLE `cfr_product_version`
  ADD PRIMARY KEY (`product_version_id`);

--
-- Indexes for table `cfr_recurring`
--
ALTER TABLE `cfr_recurring`
  ADD PRIMARY KEY (`recurring_id`);

--
-- Indexes for table `cfr_recurring_description`
--
ALTER TABLE `cfr_recurring_description`
  ADD PRIMARY KEY (`recurring_id`,`language_id`);

--
-- Indexes for table `cfr_report`
--
ALTER TABLE `cfr_report`
  ADD PRIMARY KEY (`report_time`,`type`);

--
-- Indexes for table `cfr_return`
--
ALTER TABLE `cfr_return`
  ADD PRIMARY KEY (`return_id`);

--
-- Indexes for table `cfr_return_history`
--
ALTER TABLE `cfr_return_history`
  ADD PRIMARY KEY (`return_history_id`);

--
-- Indexes for table `cfr_review`
--
ALTER TABLE `cfr_review`
  ADD PRIMARY KEY (`review_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `cfr_seo_url`
--
ALTER TABLE `cfr_seo_url`
  ADD PRIMARY KEY (`seo_url_id`),
  ADD KEY `query` (`query`),
  ADD KEY `keyword` (`keyword`);

--
-- Indexes for table `cfr_session`
--
ALTER TABLE `cfr_session`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `cfr_setting`
--
ALTER TABLE `cfr_setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `cfr_shipping_courier`
--
ALTER TABLE `cfr_shipping_courier`
  ADD PRIMARY KEY (`shipping_courier_id`);

--
-- Indexes for table `cfr_statistics`
--
ALTER TABLE `cfr_statistics`
  ADD PRIMARY KEY (`statistics_id`);

--
-- Indexes for table `cfr_store`
--
ALTER TABLE `cfr_store`
  ADD PRIMARY KEY (`store_id`);

--
-- Indexes for table `cfr_tag`
--
ALTER TABLE `cfr_tag`
  ADD PRIMARY KEY (`tag_id`);

--
-- Indexes for table `cfr_tax_class`
--
ALTER TABLE `cfr_tax_class`
  ADD PRIMARY KEY (`tax_class_id`);

--
-- Indexes for table `cfr_tax_rate`
--
ALTER TABLE `cfr_tax_rate`
  ADD PRIMARY KEY (`tax_rate_id`);

--
-- Indexes for table `cfr_tax_rate_to_customer_group`
--
ALTER TABLE `cfr_tax_rate_to_customer_group`
  ADD PRIMARY KEY (`tax_rate_id`,`customer_group_id`);

--
-- Indexes for table `cfr_tax_rule`
--
ALTER TABLE `cfr_tax_rule`
  ADD PRIMARY KEY (`tax_rule_id`);

--
-- Indexes for table `cfr_theme`
--
ALTER TABLE `cfr_theme`
  ADD PRIMARY KEY (`theme_id`);

--
-- Indexes for table `cfr_theme_builder_config`
--
ALTER TABLE `cfr_theme_builder_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cfr_translation`
--
ALTER TABLE `cfr_translation`
  ADD PRIMARY KEY (`translation_id`);

--
-- Indexes for table `cfr_upload`
--
ALTER TABLE `cfr_upload`
  ADD PRIMARY KEY (`upload_id`);

--
-- Indexes for table `cfr_user`
--
ALTER TABLE `cfr_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `cfr_user_group`
--
ALTER TABLE `cfr_user_group`
  ADD PRIMARY KEY (`user_group_id`);

--
-- Indexes for table `cfr_voucher`
--
ALTER TABLE `cfr_voucher`
  ADD PRIMARY KEY (`voucher_id`);

--
-- Indexes for table `cfr_voucher_history`
--
ALTER TABLE `cfr_voucher_history`
  ADD PRIMARY KEY (`voucher_history_id`);

--
-- Indexes for table `cfr_voucher_theme`
--
ALTER TABLE `cfr_voucher_theme`
  ADD PRIMARY KEY (`voucher_theme_id`);

--
-- Indexes for table `cfr_voucher_theme_description`
--
ALTER TABLE `cfr_voucher_theme_description`
  ADD PRIMARY KEY (`voucher_theme_id`,`language_id`);

--
-- Indexes for table `cfr_warehouse`
--
ALTER TABLE `cfr_warehouse`
  ADD PRIMARY KEY (`warehouse_id`);

--
-- Indexes for table `cfr_weight_class`
--
ALTER TABLE `cfr_weight_class`
  ADD PRIMARY KEY (`weight_class_id`);

--
-- Indexes for table `cfr_weight_class_description`
--
ALTER TABLE `cfr_weight_class_description`
  ADD PRIMARY KEY (`weight_class_id`,`language_id`);

--
-- Indexes for table `cfr_zone`
--
ALTER TABLE `cfr_zone`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `cfr_zone_to_geo_zone`
--
ALTER TABLE `cfr_zone_to_geo_zone`
  ADD PRIMARY KEY (`zone_to_geo_zone_id`);

--
-- Indexes for table `cfr_timezones`
--
ALTER TABLE `cfr_timezones`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cfr_address`
--
ALTER TABLE `cfr_address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_api`
--
ALTER TABLE `cfr_api`
  MODIFY `api_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_api_ip`
--
ALTER TABLE `cfr_api_ip`
  MODIFY `api_ip_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_api_session`
--
ALTER TABLE `cfr_api_session`
  MODIFY `api_session_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_attribute`
--
ALTER TABLE `cfr_attribute`
  MODIFY `attribute_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `cfr_attribute_group`
--
ALTER TABLE `cfr_attribute_group`
  MODIFY `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_banner`
--
ALTER TABLE `cfr_banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_banner_image`
--
ALTER TABLE `cfr_banner_image`
  MODIFY `banner_image_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_cart`
--
ALTER TABLE `cfr_cart`
  MODIFY `cart_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_category`
--
ALTER TABLE `cfr_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `cfr_collection`
--
ALTER TABLE `cfr_collection`
  MODIFY `collection_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `cfr_country`
--
ALTER TABLE `cfr_country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;
--
-- AUTO_INCREMENT for table `cfr_coupon`
--
ALTER TABLE `cfr_coupon`
  MODIFY `coupon_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_coupon_history`
--
ALTER TABLE `cfr_coupon_history`
  MODIFY `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_coupon_product`
--
ALTER TABLE `cfr_coupon_product`
  MODIFY `coupon_product_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_currency`
--
ALTER TABLE `cfr_currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cfr_customer`
--
ALTER TABLE `cfr_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_customer_activity`
--
ALTER TABLE `cfr_customer_activity`
  MODIFY `customer_activity_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_customer_approval`
--
ALTER TABLE `cfr_customer_approval`
  MODIFY `customer_approval_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_customer_group`
--
ALTER TABLE `cfr_customer_group`
  MODIFY `customer_group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_customer_history`
--
ALTER TABLE `cfr_customer_history`
  MODIFY `customer_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_customer_ip`
--
ALTER TABLE `cfr_customer_ip`
  MODIFY `customer_ip_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_customer_login`
--
ALTER TABLE `cfr_customer_login`
  MODIFY `customer_login_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_customer_reward`
--
ALTER TABLE `cfr_customer_reward`
  MODIFY `customer_reward_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_customer_search`
--
ALTER TABLE `cfr_customer_search`
  MODIFY `customer_search_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_customer_transaction`
--
ALTER TABLE `cfr_customer_transaction`
  MODIFY `customer_transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_custom_field`
--
ALTER TABLE `cfr_custom_field`
  MODIFY `custom_field_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_custom_field_value`
--
ALTER TABLE `cfr_custom_field_value`
  MODIFY `custom_field_value_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_download`
--
ALTER TABLE `cfr_download`
  MODIFY `download_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_event`
--
ALTER TABLE `cfr_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `cfr_extension`
--
ALTER TABLE `cfr_extension`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=183;
--
-- AUTO_INCREMENT for table `cfr_extension_install`
--
ALTER TABLE `cfr_extension_install`
  MODIFY `extension_install_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_extension_path`
--
ALTER TABLE `cfr_extension_path`
  MODIFY `extension_path_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_filter`
--
ALTER TABLE `cfr_filter`
  MODIFY `filter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cfr_filter_group`
--
ALTER TABLE `cfr_filter_group`
  MODIFY `filter_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cfr_geo_zone`
--
ALTER TABLE `cfr_geo_zone`
  MODIFY `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cfr_information`
--
ALTER TABLE `cfr_information`
  MODIFY `information_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_language`
--
ALTER TABLE `cfr_language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cfr_layout`
--
ALTER TABLE `cfr_layout`
  MODIFY `layout_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `cfr_layout_module`
--
ALTER TABLE `cfr_layout_module`
  MODIFY `layout_module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `cfr_layout_route`
--
ALTER TABLE `cfr_layout_route`
  MODIFY `layout_route_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `cfr_length_class`
--
ALTER TABLE `cfr_length_class`
  MODIFY `length_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `cfr_location`
--
ALTER TABLE `cfr_location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_manufacturer`
--
ALTER TABLE `cfr_manufacturer`
  MODIFY `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `cfr_marketing`
--
ALTER TABLE `cfr_marketing`
  MODIFY `marketing_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_modification`
--
ALTER TABLE `cfr_modification`
  MODIFY `modification_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_module`
--
ALTER TABLE `cfr_module`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `cfr_novaon_group_menu`
--
ALTER TABLE `cfr_novaon_group_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `cfr_novaon_group_menu_description`
--
ALTER TABLE `cfr_novaon_group_menu_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cfr_novaon_menu_item`
--
ALTER TABLE `cfr_novaon_menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `cfr_novaon_menu_item_description`
--
ALTER TABLE `cfr_novaon_menu_item_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `cfr_novaon_relation_table`
--
ALTER TABLE `cfr_novaon_relation_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `cfr_novaon_type`
--
ALTER TABLE `cfr_novaon_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `cfr_novaon_type_description`
--
ALTER TABLE `cfr_novaon_type_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cfr_option`
--
ALTER TABLE `cfr_option`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `cfr_option_value`
--
ALTER TABLE `cfr_option_value`
  MODIFY `option_value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `cfr_order`
--
ALTER TABLE `cfr_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_order_history`
--
ALTER TABLE `cfr_order_history`
  MODIFY `order_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_order_option`
--
ALTER TABLE `cfr_order_option`
  MODIFY `order_option_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_order_product`
--
ALTER TABLE `cfr_order_product`
  MODIFY `order_product_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_order_recurring`
--
ALTER TABLE `cfr_order_recurring`
  MODIFY `order_recurring_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_order_recurring_transaction`
--
ALTER TABLE `cfr_order_recurring_transaction`
  MODIFY `order_recurring_transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_order_shipment`
--
ALTER TABLE `cfr_order_shipment`
  MODIFY `order_shipment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_order_status`
--
ALTER TABLE `cfr_order_status`
  MODIFY `order_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cfr_order_tag`
--
ALTER TABLE `cfr_order_tag`
  MODIFY `order_tag_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_order_total`
--
ALTER TABLE `cfr_order_total`
  MODIFY `order_total_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_order_voucher`
--
ALTER TABLE `cfr_order_voucher`
  MODIFY `order_voucher_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_page_contents`
--
ALTER TABLE `cfr_page_contents`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_product`
--
ALTER TABLE `cfr_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;
--
-- AUTO_INCREMENT for table `cfr_product_collection`
--
ALTER TABLE `cfr_product_collection`
  MODIFY `product_collection_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;
--
-- AUTO_INCREMENT for table `cfr_product_discount`
--
ALTER TABLE `cfr_product_discount`
  MODIFY `product_discount_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_product_image`
--
ALTER TABLE `cfr_product_image`
  MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;
--
-- AUTO_INCREMENT for table `cfr_product_option`
--
ALTER TABLE `cfr_product_option`
  MODIFY `product_option_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_product_option_value`
--
ALTER TABLE `cfr_product_option_value`
  MODIFY `product_option_value_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_product_reward`
--
ALTER TABLE `cfr_product_reward`
  MODIFY `product_reward_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_product_special`
--
ALTER TABLE `cfr_product_special`
  MODIFY `product_special_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_product_tag`
--
ALTER TABLE `cfr_product_tag`
  MODIFY `product_tag_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_product_version`
--
ALTER TABLE `cfr_product_version`
  MODIFY `product_version_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_recurring`
--
ALTER TABLE `cfr_recurring`
  MODIFY `recurring_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_return`
--
ALTER TABLE `cfr_return`
  MODIFY `return_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_return_history`
--
ALTER TABLE `cfr_return_history`
  MODIFY `return_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_review`
--
ALTER TABLE `cfr_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_seo_url`
--
ALTER TABLE `cfr_seo_url`
  MODIFY `seo_url_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2204;
--
-- AUTO_INCREMENT for table `cfr_setting`
--
ALTER TABLE `cfr_setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5516;
--
-- AUTO_INCREMENT for table `cfr_statistics`
--
ALTER TABLE `cfr_statistics`
  MODIFY `statistics_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `cfr_store`
--
ALTER TABLE `cfr_store`
  MODIFY `store_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_tag`
--
ALTER TABLE `cfr_tag`
  MODIFY `tag_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `cfr_tax_class`
--
ALTER TABLE `cfr_tax_class`
  MODIFY `tax_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cfr_tax_rate`
--
ALTER TABLE `cfr_tax_rate`
  MODIFY `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `cfr_tax_rule`
--
ALTER TABLE `cfr_tax_rule`
  MODIFY `tax_rule_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;
--
-- AUTO_INCREMENT for table `cfr_theme`
--
ALTER TABLE `cfr_theme`
  MODIFY `theme_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_theme_builder_config`
--
ALTER TABLE `cfr_theme_builder_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=376;
--
-- AUTO_INCREMENT for table `cfr_translation`
--
ALTER TABLE `cfr_translation`
  MODIFY `translation_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_upload`
--
ALTER TABLE `cfr_upload`
  MODIFY `upload_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_user`
--
ALTER TABLE `cfr_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_user_group`
--
ALTER TABLE `cfr_user_group`
  MODIFY `user_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cfr_voucher`
--
ALTER TABLE `cfr_voucher`
  MODIFY `voucher_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_voucher_history`
--
ALTER TABLE `cfr_voucher_history`
  MODIFY `voucher_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cfr_voucher_theme`
--
ALTER TABLE `cfr_voucher_theme`
  MODIFY `voucher_theme_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `cfr_warehouse`
--
ALTER TABLE `cfr_warehouse`
  MODIFY `warehouse_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1138;
--
-- AUTO_INCREMENT for table `cfr_weight_class`
--
ALTER TABLE `cfr_weight_class`
  MODIFY `weight_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cfr_zone`
--
ALTER TABLE `cfr_zone`
  MODIFY `zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `cfr_zone_to_geo_zone`
--
ALTER TABLE `cfr_zone_to_geo_zone`
  MODIFY `zone_to_geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT for table `cfr_timezones`
--
ALTER TABLE `cfr_timezones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

-- -----------------------------------------------------------
--
-- v1.4
--
-- -----------------------------------------------------------

--
-- more event for order
--
INSERT INTO `cfr_event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES
('edit order 2', 'admin/model/sale/order/editOrder/after', 'event/statistics/editOrderDetail', 1, 0),
('change status order 2', 'admin/model/sale/order/editOrder/before', 'event/statistics/updateStatusOrderDetail', 1, 0);


-- -----------------------------------------------------------
--
-- v1.5
--
-- -----------------------------------------------------------

--
-- Create table `cfr_novaon_api_credentials`
--
CREATE TABLE `cfr_novaon_api_credentials` (
  `id` int(11) NOT NULL,
  `consumer_key` varchar(255) NOT NULL,
  `consumer_secret` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `cfr_novaon_api_credentials` (`id`, `consumer_key`, `consumer_secret`, `create_date`, `status`) VALUES
(1, 'GHNHTYDSTw567', 'GHNDRTUYUKMVFYJKNBGFFGFHJJ7886GH', '2019-06-03 01:15:59', 1);

ALTER TABLE `cfr_novaon_api_credentials`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `cfr_novaon_api_credentials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- enable extension module NovaonApi
--
INSERT INTO `cfr_extension` (`type`, `code`) VALUES
('module', 'NovaonApi');

--
-- google shopping meta config
--
INSERT INTO `cfr_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'config', 'config_gg_shopping', '', 0);

--
-- add column previous_order_status_id to cfr_order
--
ALTER TABLE `cfr_order` ADD `previous_order_status_id` VARCHAR(50) NULL AFTER `order_status_id`;


--
-- v1.5.2 Smart loading demo data base on theme group (theme type)
-- Note: following content is copied from file "library/theme_config/demo_data/tech.sql" for default
--
-- 2020/04: More support loading demo data from special theme. If no special theme defined, use theme group as above
-- Note: following content may be copied from file "library/theme_config/demo_data/special_shop/xxx.sql" if special file defined

-- 2020/07/30: change theme demo to fashion_luvibee_2.
-- Note: following content is copied from file "library/theme_config/demo_data/special_shop/fashion_luvibee_2.sql"
--

-- IMPORTANT: Now move to end of file. Reason:
-- + we have some migrate sql lines below this
-- + the demo data should be latest version

-- -----------------------------------------------------------
-- --------end v1.5.2 demo data for theme---------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.5.2 tool theme----------------------------------
-- -----------------------------------------------------------

ALTER TABLE `cfr_user` ADD `theme_develop` tinyint(1) NULL NULL DEFAULT '0' AFTER `permission`;

-- -----------------------------------------------------------
-- --------end v1.5.2 tool theme------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- v1.5.2 Remake menu ----------------------------------------
-- -----------------------------------------------------------

--
-- create table cfr_menu
--

CREATE TABLE `cfr_menu` (
  `menu_id` INT(11) NOT NULL AUTO_INCREMENT,
  `parent_id` INT(11) NULL,
  `menu_type_id` TINYINT(2) NULL,
  `target_value` VARCHAR(256) NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- create table cfr_menu_description
--

CREATE TABLE `cfr_menu_description` (
  `menu_id` INT(11) NOT NULL,
  `name` VARCHAR(256) NOT NULL,
  `language_id` INT(11) NOT NULL,
  PRIMARY KEY( `menu_id`, `language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- create table cfr_menu_type
--

CREATE TABLE `cfr_menu_type` (
  `menu_type_id` INT(11) NOT NULL AUTO_INCREMENT,
  `value` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`menu_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- create table cfr_menu_type_description
--

CREATE TABLE `cfr_menu_type_description` (
  `menu_type_id` INT(11) NOT NULL,
  `language_id` INT(11) NOT NULL,
  `description` VARCHAR(256) NOT NULL,
  PRIMARY KEY( `menu_type_id`, `language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- insert menu type db
--
INSERT INTO `cfr_menu_type` (`menu_type_id`, `value`) VALUES
('1', 'category'),
('2', 'collection'),
('3', 'product'),
('4', 'url'),
('5', 'built-in page'),
('6', 'blog');

--
-- insert cfr_menu_type_description db
--
INSERT INTO `cfr_menu_type_description` (`menu_type_id`, `language_id`, `description`) VALUES
('1', '1', 'Category'),
('1', '2', 'Loại sản phẩm'),
('2', '1', 'Collection'),
('2', '2', 'Bộ sưu tập'),
('3', '1', 'Product'),
('3', '2', 'Sản phẩm'),
('4', '1', 'Url'),
('4', '2', 'Đường dẫn'),
('5', '1', 'Built-in Page'),
('5', '2', 'Trang mặc định'),
('6', '1', 'Blog'),
('6', '2', 'Bài viết');

--
-- Dumping data for table `cfr_menu`
--
INSERT INTO `cfr_menu` (`menu_id`, `parent_id`, `menu_type_id`, `target_value`) VALUES
(1, Null, Null, Null),
(2, 1, 5, '?route=common/home'),
(3, 1, 5, '?route=common/shop'),
(4, 1, 5, '?route=contact/contact');

--
-- Dumping data for table `cfr_menu_description`
--
INSERT INTO `cfr_menu_description` (`menu_id`, `name`, `language_id`) VALUES
(1, 'Menu Chính', 1),
(1, 'Menu Chính', 2),
(2, 'Trang chủ', 1),
(2, 'Trang chủ', 2),
(3, 'Sản phẩm', 1),
(3, 'Sản phẩm', 2),
(4, 'Liên hệ', 1),
(4, 'Liên hệ', 2);

-- -----------------------------------------------------------
-- --------end v1.5.2 Remake menu ----------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.5.2 default policy for gg shopping--------------
-- -----------------------------------------------------------

-- create migrate table
CREATE TABLE IF NOT EXISTS `cfr_migration` (
  `migration_id` int(11) NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `run` tinyint(1) NOT NULL DEFAULT 1,
  `date_run` datetime NOT NULL,
  PRIMARY KEY (`migration_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `cfr_migration` (`migration`, `run`, `date_run`) VALUES
('migration_20190718_default_policy_gg_shopping', 1, NOW());

-- default payment methods and delivery methods
INSERT INTO `cfr_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'payment', 'payment_methods', '[{"payment_id":"198535319739A","name":"Thanh toán khi nhận hàng (COD)","guide":"Bạn sẽ thanh toán toàn bộ giá trị đơn hàng khi nhận hàng","status":"1"}]', 1),
(0, 'delivery', 'delivery_methods', '[{"id":"-1","name":"Giao hàng tiêu chuẩn(liên hệ)","status":"1","fee_region":"0"}]', 1);

-- default policy page and menu
INSERT IGNORE INTO `cfr_page_contents` (`page_id`,`title`, `description`, `seo_title`, `seo_description`, `status`, `date_added`) VALUES
(1000,'Chính sách và bảo mật', 'I. Chính sách thanh toán - giao hàng
                                <br/>
                                1. GIAO HÀNG – TRẢ TIỀN MẶT (COD)
                                <br/>
                                - Chúng tôi áp dụng hình thức giao hàng COD cho tất cả các vùng miền trên toàn quốc.
                                <br/>
                                2. CHI PHÍ GIAO HÀNG
                                <br/>
                                - Chúng tôi giao hàng miễn phí đối với khách hàng tại nội thành Hà Nội. Các tỉnh thành khác sẽ áp dụng mức chi phí của đối tác vận chuyển khác.
                                <br/>
                                Chính sách đổi trả
                                1. Lý do chấp nhận đổi trả
                                <br/>
                                - Sản phẩm bị lỗi do nhà sản xuất trong 7 ngày đầu sử dụng, đối với khách mua hàng online thì áp dụng từ khi đơn hàng thành công. 
                                <br/>
                                2. Yêu cầu cho sản phẩm đổi trả
                                <br/>
                                - Sản phẩm còn nguyên vẹn, đầy đủ nhãn mác, nguyên đai kiện, niêm phong theo quy cách ban đầu.
                                <br/>
                                - Sản phẩm/dịch vụ còn đầy đủ phụ kiện.
                                <br/>
                                3. Thời gian đổi trả
                                <br/>
                                - Trong vòng 07 ngày kể từ ngày nhận sản phẩm.
                                <br/>
                                <br/>
                                II. CHÍNH SÁCH BẢO MẬT
                                <br/>
                                1. Thu thập thông tin cá nhân
                                <br/>
                                - Chúng tôi sẽ dùng thông tin bạn đã cung cấp để xử lý đơn đặt hàng, cung cấp các dịch vụ và thông tin yêu cầu thông qua website và theo yêu cầu của bạn.
                                <br/>
                                - Chúng tôi có thể chuyển tên và địa chỉ cho bên thứ ba để họ giao hàng cho bạn.
                                <br/>
                                2. Bảo mật
                                <br/>
                                - Chúng tôi có biện pháp thích hợp về kỹ thuật và an ninh để ngăn chặn truy cập trái phép hoặc trái pháp luật hoặc mất mát hoặc hủy hoặc thiệt hại cho thông tin của bạn.', 'Chính sách thanh toán - giao hàng, Chính sách đổi trả, Chính sách bảo mật', 'Chính sách thanh toán - giao hàng, Chính sách đổi trả, Chính sách bảo mật', 1, NOW());

INSERT IGNORE INTO `cfr_seo_url` (`seo_url_id`,`store_id`, `language_id`, `query`, `keyword`) VALUES
            (1000, 0, 1, 'page_id=1000', 'chinh-sach-va-bao-mat'),
            (1001, 0, 2, 'page_id=1000', 'chinh-sach-va-bao-mat');

-- old menu. TODO: remove if work properly...
INSERT IGNORE INTO `cfr_novaon_menu_item` (`id`, `parent_id`, `group_menu_id`, `status`, `position`, `created_at`, `updated_at`) VALUES
(1000, 0, 1, 1, 0, NOW(), NOW());

-- old menu. TODO: remove if work properly...
INSERT IGNORE INTO `cfr_novaon_menu_item_description` (`id`, `language_id`, `name`, `menu_item_id`) VALUES
(1000, 1, 'Chính sách', 1000),
(1001, 2, 'Chính sách', 1000);

-- old menu. TODO: remove if work properly...
INSERT IGNORE INTO `cfr_novaon_relation_table` (`main_name`, `main_id`, `child_name`, `child_id`, `type_id`, `url`, `redirect`, `weight_number`) VALUES
('group_menu', 1, 'menu_item', 1000, 5, '', 0, 0),
('menu_item', 1000, '', 0, 5, 'chinh-sach-va-bao-mat', 1, 0);

-- Create menu item for default policy page content
INSERT INTO `cfr_menu` (`menu_id`, `parent_id`, `menu_type_id`, `target_value`) VALUES
(5, 1, 4, 'chinh-sach-va-bao-mat');

--
-- Dumping data for table `cfr_menu_description`
--
INSERT INTO `cfr_menu_description` (`menu_id`, `name`, `language_id`) VALUES
(5, 'Chính sách', 1),
(5, 'Chính sách', 2);


-- -----------------------------------------------------------
-- --------end v1.5.2 default policy for gg shopping----------
-- -----------------------------------------------------------


-- -----------------------------------------------------------
-- --------enable seo ----------------------------------------
-- -----------------------------------------------------------
UPDATE `cfr_setting` SET `value` = '1' WHERE `code` = 'config' AND `key` = 'config_seo_url';

INSERT INTO `cfr_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`, `table_name`) VALUES
(NULL, '0', '1', 'common/home', 'home', ''),
(NULL, '0', '2', 'common/home', 'trang-chu', ''),
(NULL, '0', '1', 'common/shop', 'products', ''),
(NULL, '0', '2', 'common/shop', 'san-pham', ''),
(NULL, '0', '1', 'contact/contact', 'contact', ''),
(NULL, '0', '2', 'contact/contact', 'lien-he', ''),
(NULL, '0', '1', 'checkout/profile', 'profile', ''),
(NULL, '0', '2', 'checkout/profile', 'tai-khoan', ''),
(NULL, '0', '1', 'account/login', 'login', ''),
(NULL, '0', '2', 'account/login', 'dang-nhap', ''),
(NULL, '0', '1', 'account/register', 'register', ''),
(NULL, '0', '2', 'account/register', 'dang-ky', ''),
(NULL, '0', '1', 'account/logout', 'logout', ''),
(NULL, '0', '2', 'account/logout', 'dang-xuat', ''),
(NULL, '0', '1', 'checkout/setting', 'setting', ''),
(NULL, '0', '2', 'checkout/setting', 'cai-dat', ''),
(NULL, '0', '1', 'checkout/my_orders', 'checkout-cart', ''),
(NULL, '0', '2', 'checkout/my_orders', 'gio-hang', ''),
(NULL, '0', '1', 'checkout/order_preview', 'payment', ''),
(NULL, '0', '2', 'checkout/order_preview', 'thanh-toan', ''),
(NULL, '0', '1', 'checkout/order_preview/orderSuccess', 'payment-success', ''),
(NULL, '0', '2', 'checkout/order_preview/orderSuccess', 'dat-hang-thanh-cong', ''),
(NULL, '0', '1', 'blog/blog', 'blog', ''),
(NULL, '0', '2', 'blog/blog', 'bai-viet', '');

-- -----------------------------------------------------------
-- --------enable seo ----------------------------------------
-- -----------------------------------------------------------


-- -----------------------------------------------------------
-- --------v1.5.3 tuning 56 key points -----------------------
-- -----------------------------------------------------------

ALTER TABLE `cfr_product` ADD `common_price` decimal(15,4) DEFAULT NULL AFTER `sku`;
ALTER TABLE `cfr_product` ADD `common_compare_price` decimal(15,4) DEFAULT NULL AFTER `sku`;
ALTER TABLE `cfr_product` ADD `common_sku` varchar(64) DEFAULT NULL AFTER `sku`;
ALTER TABLE `cfr_product` ADD `common_barcode` varchar(128) DEFAULT NULL AFTER `sku`;

-- -----------------------------------------------------------
-- --------end v1.5.3 tuning 56 key points -------------------
-- -----------------------------------------------------------


-- -----------------------------------------------------------
-- --------v1.6.1 integrate 3rd API --------------------------
-- -----------------------------------------------------------

-- add column `source` to cfr_order
ALTER TABLE `cfr_order`
  ADD `source` VARCHAR(50) NULL AFTER `user_id`,
  ADD `transport_name` varchar(64) DEFAULT NULL AFTER `telephone`,
  ADD `transport_order_code` varchar(64) DEFAULT NULL AFTER `telephone`,
  ADD `transport_service_name` varchar(64) DEFAULT NULL AFTER `telephone`,
  ADD `transport_money_total` VARCHAR(255) NOT NULL AFTER `transport_name`,
  ADD `transport_weight` VARCHAR(255) NOT NULL AFTER `transport_money_total`,
  ADD `transport_status` VARCHAR(255) NOT NULL AFTER `transport_weight`,
  ADD `transport_current_warehouse` VARCHAR(255) NOT NULL AFTER `transport_status`;

INSERT INTO `cfr_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'config', 'config_VIETTEL_POST_token_webhook', '', 0);

-- -----------------------------------------------------------
-- --------end v1.6.1 integrate 3rd API ----------------------
-- -----------------------------------------------------------


-- -----------------------------------------------------------
-- --------v1.7.1 create report tables -----------------------
-- -----------------------------------------------------------

-- create table `cfr_report_order` for report revenue, order and customer with order
CREATE TABLE IF NOT EXISTS `cfr_report_order` (
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `report_date` date NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT 0,
  `total_amount` decimal(18,4) DEFAULT NULL,
  `order_status` int(11) DEFAULT 0
  -- PRIMARY KEY (`report_time`, `order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- create table `cfr_report_product` for report product with quantity and total_amount
CREATE TABLE IF NOT EXISTS `cfr_report_product` (
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `report_date` date NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_version_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(18,4) DEFAULT NULL,
  `total_amount` decimal(18,4) DEFAULT NULL
  -- PRIMARY KEY (`report_time`, `order_id`, `product_id`, `product_version_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- create table `cfr_report_product` for report new customer
CREATE TABLE IF NOT EXISTS `cfr_report_customer` (
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `report_date` date NOT NULL,
  `customer_id` int(11) DEFAULT 0
  -- PRIMARY KEY (`report_time`, `customer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- create table `cfr_report_web_traffic` for report web traffic (then calculate conversion rate, ...)
CREATE TABLE IF NOT EXISTS `cfr_report_web_traffic` (
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `report_date` date NOT NULL,
  `value` varchar(500) NOT NULL,
  `last_updated` timestamp NULL DEFAULT NULL
  -- PRIMARY KEY (`report_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- create some events to insert data to some report tables above

-- on order change event
INSERT INTO `cfr_event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES
('create new order', 'admin/model/sale/order/addOrder/after', 'event/report_order/afterCreateOrder', 1, 0),
('edit order in list', 'admin/model/sale/order/updateColumnTotalOrder/after', 'event/report_order/editOrderInOrderList', 1, 0),
('change status order in order list', 'admin/model/sale/order/updateStatus/after', 'event/report_order/updateOrderStatusInOrderList', 1, 0),
('edit order in order detail', 'admin/model/sale/order/editOrder/after', 'event/report_order/editOrderInOrderDetail', 1, 0),
('create new order', 'admin/model/sale/order/addOrder/after', 'event/report_product/afterCreateOrder', 1, 0),
('edit order in list', 'admin/model/sale/order/updateProductOrder/after', 'event/report_product/editOrderInOrderList', 1, 0),
('change status order in order list', 'admin/model/sale/order/updateStatus/after', 'event/report_product/updateOrderStatusInOrderList', 1, 0),
('edit order in order detail', 'admin/model/sale/order/editOrder/after', 'event/report_product/editOrderInOrderDetail', 1, 0);

-- on customer change event
INSERT INTO `cfr_event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES
('create customer', 'admin/model/customer/customer/addCustomer/after', 'event/report_customer/createCustomer', 1, 0),
('delete customer', 'admin/model/customer/customer/deleteCustomer/after', 'event/report_customer/deleteCustomer', 1, 0);

-- -----------------------------------------------------------
-- --------end v1.7.1 create report tables -------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.7.2 onboarding ---------------------------------
-- -----------------------------------------------------------

INSERT INTO `cfr_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'config', 'config_onboarding_complete', '0', 0),
(0, 'config', 'config_onboarding_step_active', '', 0),
(0, 'config', 'config_onboarding_voucher', '', 0),
(0, 'config', 'config_onboarding_used_voucher', '0', 0);

-- -----------------------------------------------------------
-- --------end v1.7.2 onboarding -----------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.7.3 tuning -------------------------------------
-- -----------------------------------------------------------

-- add column `from_domain` to cfr_order
ALTER TABLE `cfr_order`
  ADD `from_domain` VARCHAR(500) NULL DEFAULT NULL AFTER `source`;

--
-- init first override css version
--
INSERT INTO `cfr_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'config', 'builder_config_version', '1', 0);

-- -----------------------------------------------------------
-- --------end v1.7.3 tuning ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.8.1 theme api + builder + blog -----------------
-- -----------------------------------------------------------

-- create table `cfr_collection_to_collection`
CREATE TABLE IF NOT EXISTS `cfr_collection_to_collection` (
  `collection_to_collection_id` int(11) NOT NULL AUTO_INCREMENT,
  `collection_id` int(11) NOT NULL,
  `parent_collection_id` int(11) NOT NULL,
  `sort_order` int(11) DEFAULT '0',
  PRIMARY KEY (`collection_to_collection_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- add `type` to table `cfr_collection`
ALTER TABLE `cfr_collection` ADD `type` varchar(255) DEFAULT '0';

-- create table `cfr_blog`
CREATE TABLE IF NOT EXISTS `cfr_blog` (
  `blog_id` int(11) NOT NULL AUTO_INCREMENT,
  `author` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_publish` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`blog_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- create table `cfr_blog`
CREATE TABLE IF NOT EXISTS `cfr_blog_description` (
  `blog_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text,
  `short_content` text,
  `image` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` text,
  `alias` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`blog_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- create table `cfr_blog_category`
CREATE TABLE IF NOT EXISTS `cfr_blog_category` (
  `blog_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`blog_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table `cfr_blog_category`
INSERT INTO `cfr_blog_category` (`blog_category_id`, `status`, `date_added`, `date_modified`) VALUES
(1, 1, '2019-11-11 13:37:51', '2019-11-11 19:03:46');

-- create table `cfr_blog_category_description`
CREATE TABLE IF NOT EXISTS `cfr_blog_category_description` (
  `blog_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `alias` varchar(255) NOT NULL,
  PRIMARY KEY (`blog_category_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table `cfr_blog_category_description`
INSERT INTO `cfr_blog_category_description` (`blog_category_id`, `language_id`, `title`, `meta_title`, `meta_description`, `alias`) VALUES
(1, 1, 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'danh-muc-mac-dinh-uncategorized'),
(1, 2, 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'danh-muc-mac-dinh-uncategorized');

-- create table `cfr_blog_to_blog_category`
CREATE TABLE IF NOT EXISTS `cfr_blog_to_blog_category` (
  `blog_id` int(11) NOT NULL,
  `blog_category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- create table `cfr_blog_tag`
CREATE TABLE IF NOT EXISTS `cfr_blog_tag` (
  `blog_tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`blog_tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- -----------------------------------------------------------
-- --------end v1.8.1 theme api + builder + blog -------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.8.2 tuning -------------------------------------
-- -----------------------------------------------------------

-- default order success text
INSERT INTO `cfr_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
('0', 'config', 'config_order_success_text', 'MUA HÀNG THÀNH CÔNG!', '0');

-- -----------------------------------------------------------
-- --------end v1.8.2 tuning ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.9.2 pos ----------------------------------------
-- -----------------------------------------------------------

-- add column previous_order_status_id to table order
ALTER TABLE `cfr_order` ADD `pos_id_ref` VARCHAR(50) NULL DEFAULT NULL AFTER `order_code`;

-- add column store_id and user_id to tables report. default store_id = 0 and user_id = 1
ALTER TABLE `cfr_report_product`
  ADD `store_id` INT(11) NULL DEFAULT 0 AFTER `total_amount`,
  ADD `user_id` INT NULL DEFAULT 1 AFTER `store_id`;

ALTER TABLE `cfr_report_order`
  ADD `store_id` INT(11) NULL DEFAULT 0 AFTER `order_status`,
  ADD `user_id` INT(11) NULL DEFAULT 1 AFTER `store_id`;

ALTER TABLE `cfr_report_customer`
  ADD `store_id` INT(11) NULL DEFAULT 0 AFTER `customer_id`,
  ADD `user_id` INT(11) NULL DEFAULT 1 AFTER `store_id`;

-- add column channel to table product. null or 0 -> website, 1 -> offline, 2 -> website + offline
ALTER TABLE `cfr_product` ADD `channel` INT NULL DEFAULT 2 AFTER `status`;

-- insert default store to new shop
INSERT INTO `cfr_store` (`name`, `url`, `ssl`) VALUES
('Kho trung tâm', '', '');

-- IMPORTANT: force from 0, because current sql_mode is not 'NO_AUTO_VALUE_ON_ZERO'
UPDATE `cfr_store` SET `store_id` = 0 WHERE `name` = 'Kho trung tâm';

-- -----------------------------------------------------------
-- --------end v1.9.2 pos ------------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.9.3 tuning -------------------------------------
-- -----------------------------------------------------------

-- create table `cfr_images`
CREATE TABLE IF NOT EXISTS `cfr_images` (
    `image_id` INT(11) NOT NULL AUTO_INCREMENT,
    `type` VARCHAR(56) NOT NULL DEFAULT 'image',
    `name` varchar(256) NULL default NULL,
    `url` VARCHAR(256) NOT NULL,
    `directory` VARCHAR(56) NULL DEFAULT NULL,
    `status` TINYINT(1) NOT NULL DEFAULT '1',
    `source` VARCHAR(56) NOT NULL DEFAULT 'Cloudinary',
    `source_id` VARCHAR (256) NULL DEFAULT  NULL,
     PRIMARY KEY (`image_id`)
)ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- add default register success text
INSERT INTO `cfr_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'config', 'config_register_success_text', '<p>Chúc mừng! Tài khoản mới của bạn đã được tạo thành công!</p>\n\n<p>Bây giờ bạn có thể tận hưởng lợi ích thành viên khi tham gia trải nghiệm mua sắm cùng chúng tôi.</p>\n\n<p>Nếu bạn có câu hỏi gì về hoạt động của cửa hàng, vui lòng liên hệ với chủ cửa hàng.</p>\n\n<p>Một email xác nhận đã được gửi tới hòm thư của bạn. Nếu bạn không nhận được email này trong vòng 1 giờ, vui lòng liên hệ chúng tôi.</p>', 0);

-- -----------------------------------------------------------
-- --------end v1.9.3 tuning ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------HOT: soft deletable product -----------------------
-- -----------------------------------------------------------

ALTER TABLE `cfr_product` ADD `deleted` VARCHAR(50) DEFAULT NULL AFTER `demo`;
ALTER TABLE `cfr_product_version` ADD `deleted` VARCHAR(50) NULL AFTER `status`;

-- -----------------------------------------------------------
-- --------end HOT: soft deletable product -------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- v2.1 discount ------------------------------------
-- -----------------------------------------------------------

-- create table `cfr_discount`
CREATE TABLE IF NOT EXISTS `cfr_discount` (
  `discount_id` INT(11) NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(50) NOT NULL,
  `discount_type_id` INT(11) NOT NULL,
  `discount_status_id` INT(11) NOT NULL,
  `usage_limit` INT(11) NOT NULL DEFAULT '0',
  `times_used` INT(11) NOT NULL DEFAULT '0',
  `config` MEDIUMTEXT NULL DEFAULT NULL,
  `order_source` VARCHAR(128) NULL DEFAULT NULL,
  `customer_group` VARCHAR(128) NULL DEFAULT 'All',
  `start_at` DATETIME NOT NULL,
  `end_at` DATETIME NULL,
  `pause_reason` VARCHAR(256) NULL,
  `paused_at` DATETIME NULL,
  `delete_reason` VARCHAR(256) NULL,
  `deleted_at` DATETIME NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`discount_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- create table `cfr_discount_description`
CREATE TABLE IF NOT EXISTS `cfr_discount_description` (
  `discount_id` INT(11) NOT NULL,
  `language_id` INT(11) NOT NULL,
  `name` VARCHAR(256) NOT NULL,
  `description` VARCHAR(256) NOT NULL,
  PRIMARY KEY (discount_id, `language_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- create table `cfr_discount_to_store`
CREATE TABLE IF NOT EXISTS `cfr_discount_to_store` (
  `discount_id` INT(11) NOT NULL,
  `store_id` INT(11) NOT NULL,
  PRIMARY KEY (`discount_id`, `store_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- create table `cfr_discount_type`
CREATE TABLE IF NOT EXISTS `cfr_discount_type` (
  `discount_type_id` INT(11) NOT NULL,
  `language_id` INT(11) NOT NULL,
  `name` VARCHAR(128) NOT NULL,
  `description` VARCHAR(128) NULL,
  PRIMARY KEY (`discount_type_id`, `language_id` )
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- create table `cfr_discount_status`
CREATE TABLE IF NOT EXISTS `cfr_discount_status` (
  `discount_status_id` INT(11) NOT NULL,
  `language_id` INT(11) NOT NULL,
  `name` VARCHAR(128) NOT NULL,
  `description` VARCHAR(128) NULL,
  PRIMARY KEY (`discount_status_id`, `language_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- create table `cfr_order_to_discount`
CREATE TABLE IF NOT EXISTS `cfr_order_to_discount` (
  `order_id` INT(11) NOT NULL,
  `discount_id` INT(11) NOT NULL,
  PRIMARY KEY (`order_id`, `discount_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- add discount to table order_product
ALTER TABLE `cfr_order_product` ADD `discount` decimal(18,4) NOT NULL DEFAULT '0.0000' AFTER `price`;

-- add discount to table order
ALTER TABLE `cfr_order` ADD `discount` decimal(18,4) NOT NULL DEFAULT '0.0000' AFTER `order_cancel_comment`;

-- init data for table `cfr_discount_status`
INSERT INTO `cfr_discount_status` (`discount_status_id`, `language_id`, `name`, `description`) VALUES
('1', '2', 'Đã lập lịch', NULL),
('2', '2', 'Đang hoạt đông', NULL),
('3', '2', 'Hết hạn', NULL),
('4', '2', 'Đã xóa', NULL),
('1', '1', 'Scheduled', NULL),
('2', '1', 'Activated', NULL),
('3', '1', 'Paused', NULL),
('4', '1', 'Deleted', NULL);

-- init data for table `cfr_discount_type`
INSERT INTO `cfr_discount_type` (`discount_type_id`, `language_id`, `name`, `description`) VALUES
('1', '2', 'Chiết khấu theo tổng giá trị đơn hàng', NULL),
('2', '2', 'Chiết khấu theo từng sản phẩm', NULL),
('3', '2', 'Chiết khấu theo loại sản phẩm', NULL),
('4', '2', 'Chiết khấu theo nhà cung cấp', NULL),
('1', '1', 'Discount by total order value', NULL),
('2', '1', 'Discount by each product', NULL),
('3', '1', 'Discount by product type', NULL),
('4', '1', 'Discount by supplier', NULL);

-- -----------------------------------------------------------
-- --------end v2.1 discount ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v2.2.1 api chatbot v2 -----------------------------
-- -----------------------------------------------------------

-- add discount to table order
ALTER TABLE `cfr_order` ADD `customer_ref_id` VARCHAR(128) NULL DEFAULT NULL AFTER `pos_id_ref`;

-- -----------------------------------------------------------
-- --------end v2.2.1 api chatbot v2 -------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- v2.2.2-store-receipt -----------------------------
-- -----------------------------------------------------------

-- create table `cfr_store_receipt`
CREATE TABLE IF NOT EXISTS `cfr_store_receipt` (
  `store_receipt_id` INT(11) NOT NULL AUTO_INCREMENT,
  `store_id` INT(11) NOT NULL,
  `manufacturer_id` INT(11) NOT NULL,
  `store_receipt_code` varchar(50) NULL,
  `total` decimal(18,4) DEFAULT NULL,
  `total_to_pay` decimal(18,4) DEFAULT NULL,
  `total_paid` decimal(18,4) DEFAULT NULL,
  `discount` decimal(18,4) DEFAULT NULL,
  `allocation_criteria_type` VARCHAR(50) DEFAULT NULL,
  `allocation_criteria_total` decimal(18,4) DEFAULT NULL,
  `allocation_criteria_fees` MEDIUMTEXT NULL DEFAULT NULL,
  `owed` decimal(18,4) DEFAULT NULL,
  `comment` TEXT NULL DEFAULT NULL,
  `status` INT(4) NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`store_receipt_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- create table `cfr_store_receipt_to_product`
CREATE TABLE IF NOT EXISTS `cfr_store_receipt_to_product` (
  `store_receipt_product_id` INT(11) NOT NULL  AUTO_INCREMENT,
  `store_receipt_id` INT(11) NOT NULL,
  `product_id` INT(11) NOT NULL,
  `product_version_id` INT(11) NULL,
  `quantity` INT(11) NULL,
  `cost_price` decimal(18,4) DEFAULT NULL,
  `fee` decimal(18,4) DEFAULT NULL,
  `discount` decimal(18,4) DEFAULT NULL,
  `total` decimal(18,4) DEFAULT NULL,
  PRIMARY KEY (`store_receipt_product_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- new column `cfr_manufacturer`
ALTER TABLE `cfr_manufacturer`
  ADD `manufacturer_code` varchar(50) NULL AFTER `sort_order`,
  ADD `telephone` varchar(30) NULL AFTER `manufacturer_code`,
  ADD `email` varchar(200) NULL AFTER `telephone`,
  ADD `tax_code` varchar(200) NULL AFTER `email`,
  ADD `address` text NULL AFTER `tax_code`,
  ADD `province` varchar(50) NULL AFTER `address`,
  ADD `district` varchar(50) NULL AFTER `province`,
  ADD `status` int(4) NOT NULL DEFAULT '1' AFTER `district`,
  ADD `date_added` datetime NULL AFTER `status`,
  ADD `date_modified` datetime NULL AFTER `date_added`;

-- new column cfr_product_to_store
ALTER TABLE `cfr_product_to_store`
  ADD `product_version_id` int(11) NULL DEFAULT 0 AFTER `store_id`,
  ADD `quantity` int(11) NULL DEFAULT 0 AFTER `product_version_id`,
  ADD `cost_price` DECIMAL(15,4 ) NULL DEFAULT '0' AFTER `quantity`;

-- change primary key cfr_product_to_store
ALTER TABLE `cfr_product_to_store`
  DROP PRIMARY KEY, ADD PRIMARY KEY(`product_id`, `product_version_id`, `store_id`);

-- new column cfr_product
ALTER TABLE `cfr_product`
  ADD `default_store_id` INT(11) NULL DEFAULT '0' AFTER `deleted`;

-- -----------------------------------------------------------
-- --------end v2.2.2-store-receipt --------------------------
-- -----------------------------------------------------------


-- -----------------------------------------------------------
-- -------- v2.2.3-single signon + chatbot api v2 more -------
-- -----------------------------------------------------------

CREATE TABLE IF NOT EXISTS `cfr_order_utm` (
  `order_utm_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `code` varchar(256) NOT NULL,
  `key` varchar(256) NOT NULL,
  `value` text NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
   PRIMARY KEY (`order_utm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- -----------------------------------------------------------
-- --------end v2.2.3-single signon + chatbot api v2 more ----
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------start v2.3.2-tuning -------------------------------
-- -----------------------------------------------------------

-- new column cfr_product
ALTER TABLE `cfr_product`
   ADD `common_cost_price` DECIMAL(15,4 ) NULL DEFAULT '0' AFTER `common_price`;

-- -----------------------------------------------------------
-- --------end v2.3.2-tuning ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------start v2.4.2-app-store -------------------------------
-- -----------------------------------------------------------
-- create table my_app
CREATE TABLE IF NOT EXISTS `cfr_my_app` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_code` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1)  NULL,
  `expiration_date` datetime  DEFAULT NULL,
  `app_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path_asset` text COLLATE utf8_unicode_ci  NULL,
  `trial` int(2)  NULL,
  `installed` int(11) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- INSERT APP DEFAULT FOR APP
INSERT INTO `cfr_my_app` (`app_code`, `status`, `expiration_date`, `app_name`, `path_logo`, `path_asset`, `trial`, `installed`,`date_added`,`date_modified`) VALUES
('mar_ons_chatbot', 1, NULL, 'NovaonX ChatBot', 'view/image/appstore/mar_ons_chatbot.jpg', '', 0, 1, NOW(), NOW()),
('mar_ons_maxlead', 1, NULL, 'AutoAds Maxlead', 'view/image/appstore/mar_ons_maxlead.jpg', '', 0, 1, NOW(), NOW()),
('trans_ons_vtpost', 1, NULL, 'VietelPost', 'view/image/appstore/trans_ons_vtpost.jpg', '', 0, 1, NOW(), NOW()),
('trans_ons_ghn', 1, NULL, 'Giao Hàng Nhanh', 'view/image/appstore/trans_ons_ghn.jpg', '', 0, 1, NOW(), NOW());

-- craete table appstore_setting
CREATE TABLE IF NOT EXISTS `cfr_appstore_setting` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 NOT NULL,
  `code` varchar(64) CHARACTER SET utf8 NOT NULL,
  `setting` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- create table app_config_theme
CREATE TABLE IF NOT EXISTS `cfr_app_config_theme` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `module_id` int(255) NOT NULL,
  `theme_value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NULL,
  `sort` int(10)  NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- -----------------------------------------------------------
-- --------end v2.4.2-app-store ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- start v2.5.2 Advance Store Management ------------
-- -----------------------------------------------------------

-- store_transfer_receipt : Table to save transfer receipt of store
CREATE TABLE IF NOT EXISTS `cfr_store_transfer_receipt` (
  `store_transfer_receipt_id` INT(11) NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(128) NOT NULL,
  `export_store_id` INT(11) NOT NULL,
  `import_store_id` INT(11) NOT NULL,
  `common_quantity_transfer` INT(16) NULL,
  `total` decimal(18,4) NOT NULL,
  `status` TINYINT(2) NOT NULL DEFAULT '0' COMMENT '0: lưu nháp, 1: đã chuyển, 2: đã nhận, 9: đã hủy',
  `date_exported` DATE NOT NULL,
  `date_imported` DATE NULL,
  `date_added` DATETIME NOT NULL,
  `date_modified` DATETIME NOT NULL,
  PRIMARY KEY (`store_transfer_receipt_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- store_transfer_receipt_to_product : relationship transfer receipt with product
CREATE TABLE IF NOT EXISTS `cfr_store_transfer_receipt_to_product` (
  `store_transfer_receipt_id` INT(11) NOT NULL,
  `product_id` INT(11) NOT NULL,
  `product_version_id` INT(11) NOT NULL DEFAULT '0',
  `quantity` INT(11) NOT NULL,
  `current_quantity` INT(11) NULL,
  `price` decimal(18,4) NOT NULL,
  PRIMARY KEY (`store_transfer_receipt_id`, `product_id`, `product_version_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- store_take_receipt : Table to save take receipt of store
CREATE TABLE IF NOT EXISTS `cfr_store_take_receipt` (
  `store_take_receipt_id` INT(11) NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(128) NOT NULL,
  `store_id` INT(11) NOT NULL,
  `difference_amount` decimal(18,4) NOT NULL,
  `status` TINYINT(2) NOT NULL DEFAULT '0',
  `date_taked` DATE NOT NULL,
  `date_added` DATETIME NOT NULL,
  `date_modified` DATETIME NOT NULL,
  PRIMARY KEY (`store_take_receipt_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- store_take_receipt_to_product : Relationship for take receipt with product
CREATE TABLE IF NOT EXISTS `cfr_store_take_receipt_to_product` (
  `store_take_receipt_id` INT(11) NOT NULL,
  `product_id` INT(11) NOT NULL,
  `product_version_id` INT(11) NOT NULL DEFAULT '0',
  `inventory_quantity` INT(11) NOT NULL,
  `actual_quantity` INT(11) NOT NULL,
  `difference_amount` decimal(18,4) NOT NULL,
  `reason`  VARCHAR(256),
  PRIMARY KEY (`store_take_receipt_id`, `product_id`, `product_version_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- cost_adjustment_receipt: table to save cost adjustment receipt
CREATE TABLE IF NOT EXISTS `cfr_cost_adjustment_receipt` (
  `cost_adjustment_receipt_id` INT NOT NULL AUTO_INCREMENT,
  `cost_adjustment_receipt_code` VARCHAR(255) NULL,
  `store_id` INT(255) NOT NULL,
  `note` TEXT NULL,
  `status` tinyint(2) NULL DEFAULT 0,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`cost_adjustment_receipt_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- cost_adjustment_receipt_to_product: Relationship of cost adjustment receipt with product
CREATE TABLE IF NOT EXISTS `cfr_cost_adjustment_receipt_to_product` (
  `cost_adjustment_receipt_id` INT NOT NULL,
  `product_id` INT(255) NOT NULL,
  `product_version_id` INT(255) NOT NULL DEFAULT '0',
  `adjustment_cost_price` DECIMAL(18,4) NULL,
  `current_cost_price` DECIMAL(18,4) NULL,
  `note` VARCHAR(255) NULL,
  PRIMARY KEY (`cost_adjustment_receipt_id`, `product_id`, `product_version_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- -----------------------------------------------------------
-- --------- end v2.5.2 Advance Store Management -------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- v2.5.4-tuning ------------------------------------
-- -----------------------------------------------------------

-- Create menu item for blog
INSERT INTO `cfr_menu` (`menu_id`, `parent_id`, `menu_type_id`, `target_value`) VALUES
(6, 1, 5, '?route=blog/blog');

--
-- Dumping data for table `cfr_menu_description`
--
INSERT INTO `cfr_menu_description` (`menu_id`, `name`, `language_id`) VALUES
(6, 'Blog', 1),
(6, 'Blog', 2);

-- -----------------------------------------------------------
-- --------end v2.5.4-tuning ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- start v2.6.1 Shopee connection -------------------
-- -----------------------------------------------------------

-- shopee_order
CREATE TABLE IF NOT EXISTS `cfr_shopee_order` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(128) NOT NULL,
  `cod` TINYINT(1) NOT NULL,
  `currency` VARCHAR(64) NOT NULL,
  `fullname` VARCHAR(256) NULL COMMENT 'Tên người nhận',
  `phone` VARCHAR(32) NOT NULL COMMENT 'Số ',
  `town` VARCHAR(256) NULL,
  `district` VARCHAR(256) NULL,
  `city` VARCHAR(256) NULL,
  `state` VARCHAR(256) NULL,
  `country` VARCHAR(256) NULL,
  `zipcode` VARCHAR(32) NULL,
  `full_address` VARCHAR(512) NULL ,
  `actual_shipping_cost` DECIMAL(18,4) NULL,
  `total_amount` DECIMAL(18,4) NOT NULL,
  `order_status` VARCHAR(128) NOT NULL,
  `shipping_method` VARCHAR(256) NOT NULL COMMENT 'shipping_carrier',
  `payment_method` VARCHAR(256) NOT NULL,
  `payment_status` TINYINT(2) NULL DEFAULT 0,
  `note_for_shop` TEXT NULL COMMENT 'message_to_seller',
  `note` TEXT NULL,
  `create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` DATETIME NULL,
  `shopee_shop_id` BIGINT(64) NOT NULL,
  `bestme_order_id` INT(11) NULL,
  `sync_status` TINYINT(2)  NULL,
  `is_edited` TINYINT(1) NOT NULL DEFAULT 0,
  `sync_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- cfr_shopee_order_to_product
CREATE TABLE IF NOT EXISTS `cfr_shopee_order_to_product` (
  `shopee_order_code` VARCHAR(128) NOT NULL COMMENT 'order code on shopee',
  `item_id` BIGINT(64) NOT NULL COMMENT '~ product_id ',
  `variation_id` BIGINT(64) NOT NULL COMMENT '~ product_version_id',
  `variation_name` VARCHAR(256) NULL ,
  `quantity` INT(11) NOT NULL COMMENT 'variation_quantity_purchased',
  `original_price` DECIMAL(18,4) NOT NULL COMMENT 'variation_original_price',
  `price` DECIMAL(18,4) NOT NULL COMMENT 'variation_discounted_price'
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- cfr_shopee_shop_config
CREATE TABLE IF NOT EXISTS `cfr_shopee_shop_config` (
  `shop_id` BIGINT(64) NOT NULL,
  `shop_name` VARCHAR(256) NULL,
  `sync_interval` INT(11) NOT NULL COMMENT '0: không bao giờ',
  `connected` TINYINT(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`shop_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- -----------------------------------------------------------
-- -------- end v2.6.1 Shopee connection ---------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- Fix load demo data -------------------------------
-- -----------------------------------------------------------

-- add column `demo` to cfr_blog
ALTER TABLE `cfr_blog` ADD `demo` TINYINT(1) NOT NULL DEFAULT '0' AFTER `status`;

-- -----------------------------------------------------------
-- -------- Fix load demo data -------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- start v2.6.3 Lazada connection -------------------
-- -----------------------------------------------------------

-- cfr_lazada_category_attributes
CREATE TABLE IF NOT EXISTS `cfr_lazada_category_attributes` (
  `lazada_cateogry_id` BIGINT(32) NOT NULL ,
  `data` LONGTEXT NOT NULL ,
  PRIMARY KEY (`lazada_cateogry_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- cfr_lazada_shop_config
CREATE TABLE IF NOT EXISTS `cfr_lazada_shop_config` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `seller_id` BIGINT(32) NOT NULL ,
  `country` VARCHAR(16) NOT NULL ,
  `shop_name` VARCHAR(256) NOT NULL ,
  `access_token` VARCHAR(256) NOT NULL ,
  `expires_in` DATETIME NOT NULL ,
  `refresh_token` VARCHAR(256) NOT NULL ,
  `refresh_expires_in` DATETIME NOT NULL ,
  `sync_interval` INT(11) NOT NULL DEFAULT '0' ,
  `connected` TINYINT(2) NOT NULL DEFAULT '1' ,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- cfr_lazada_product
CREATE TABLE IF NOT EXISTS `cfr_lazada_product` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `shop_id` INT(11) NOT NULL ,
  `item_id` VARCHAR(64) NOT NULL ,
  `primary_category` VARCHAR(64) NOT NULL ,
  `name` VARCHAR(256) NOT NULL ,
  `description` VARCHAR(512) NULL ,
  `short_description` VARCHAR(256) NULL ,
  `brand` VARCHAR(128) NULL ,
  `bestme_product_id` INT(11) NULL ,
  `sync_status` INT(11) NULL ,
  `created_at` DATETIME NULL ,
  `updated_at` DATETIME NULL ,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- cfr_lazada_product_version
CREATE TABLE IF NOT EXISTS `cfr_lazada_product_version` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `item_id` VARCHAR(64) NOT NULL ,
  `version_name` VARCHAR(128) NULL ,
  `status` VARCHAR(26) NULL ,
  `quantity` INT(11) NULL ,
  `product_weight` DECIMAL(15,4) NULL ,
  `images` MEDIUMTEXT NULL COMMENT 'list image' ,
  `seller_sku` VARCHAR(128) NOT NULL ,
  `shop_sku` VARCHAR(128) NULL ,
  `url` VARCHAR(256) NULL ,
  `package_width` DECIMAL(10,2) NULL COMMENT 'centimet' ,
  `package_height` DECIMAL(10,2) NULL COMMENT 'centimet' ,
  `package_length` DECIMAL(10,2) NULL COMMENT 'centimet',
  `package_weight` DECIMAL(10,4) NULL COMMENT 'kilogam' ,
  `price` DECIMAL(15,4) NULL ,
  `available` INT(11) NULL ,
  `sku_id` BIGINT(32) NULL ,
  `bestme_product_id` INT(11) NULL ,
  `bestme_product_version_id` INT(11) NULL ,
  `sync_status` INT(11) NULL ,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- cfr_lazada_category_tree
CREATE TABLE IF NOT EXISTS `cfr_lazada_category_tree` (
  `id` INT(11) NOT NULL ,
  `parent_id` INT(11) NOT NULL DEFAULT '0' ,
  `name` VARCHAR(256) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- cfr_lazada_category_description
CREATE TABLE IF NOT EXISTS `cfr_lazada_category_description` (
  `category_id` INT(11) NOT NULL ,
  `name` VARCHAR(256) NOT NULL
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- -----------------------------------------------------------
-- -------- end v2.6.3 Lazada connection ---------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- v2.7.1 Lazada Order ------------------------------
-- -----------------------------------------------------------

-- cfr_lazada_order
CREATE TABLE IF NOT EXISTS `cfr_lazada_order` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `order_id` BIGINT(64) NOT NULL ,
  `fullname` VARCHAR(256) NULL COMMENT 'address_shipping name' ,
  `phone` VARCHAR(32) NULL COMMENT 'address_shipping phone' ,
  `ward` VARCHAR(256) NULL COMMENT 'address_billing address5' ,
  `district` VARCHAR(256) NULL COMMENT 'address_billing address4' ,
  `province` VARCHAR(256) NULL COMMENT 'address_billing address3' ,
  `full_address` VARCHAR(256) NULL COMMENT 'address_billing address1' ,
  `shipping_fee_original` DECIMAL(18,4) NULL ,
  `shipping_fee` DECIMAL(18,4) NULL ,
  `shipping_method` VARCHAR(256) NULL COMMENT 'price' ,
  `total_amount` DECIMAL(18.4) NULL ,
  `order_status` VARCHAR(64) NULL ,
  `payment_method` VARCHAR(256) NULL ,
  `payment_status` TINYINT(2) NULL ,
  `voucher` DECIMAL(18,4) NULL ,
  `voucher_seller` DECIMAL(18,4) NULL ,
  `remarks` VARCHAR(512) NULL ,
  `create_time` DATETIME NULL ,
  `update_time` DATETIME NULL ,
  `lazada_shop_id` INT(11) NOT NULL ,
  `bestme_order_id` INT(11) NULL ,
  `sync_status` TINYINT(2) NULL ,
  `is_edited` TINYINT(2) NULL ,
  `sync_time` DATETIME NULL ,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- cfr_lazada_order_to_product
CREATE TABLE IF NOT EXISTS `cfr_lazada_order_to_product` (
  `lazada_order_id` BIGINT(64) NOT NULL ,
  `sku` VARCHAR(128) NULL ,
  `shop_sku` VARCHAR(128) NULL ,
  `variation` VARCHAR(256) NULL ,
  `name` VARCHAR(256) NULL ,
  `quantity` INT(11) NOT NULL DEFAULT 0,
  `item_price` DECIMAL(18,4) NULL ,
  `paid_price` DECIMAL(18,4) NULL
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- -----------------------------------------------------------
-- -------- end - v2.7.1 Lazada Order ------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- v2.7.2 Tuning ------------------------------------
-- -----------------------------------------------------------

-- alt blog image
ALTER TABLE `cfr_blog_description`
  ADD `alt` TEXT NULL AFTER `alias`;

-- -----------------------------------------------------------
-- -------- end - v2.7.2 Tunging -----------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- v2.7.3 New Customer Design -----------------------
-- -----------------------------------------------------------

ALTER TABLE `cfr_customer`
  ADD `sex` TINYINT(2) NULL COMMENT '1: Nam, 2: Nữ, 3: Khác' AFTER `date_added`,
  ADD `birthday` DATE NULL AFTER `sex`,
  ADD `full_name` VARCHAR(255) NOT NULL AFTER `birthday`,
  ADD `website` VARCHAR(255) NULL AFTER `full_name`,
  ADD `tax_code` VARCHAR(100) NULL AFTER `website`,
  ADD `staff_in_charge` INT(11) NULL AFTER `tax_code`;

ALTER TABLE `cfr_customer`
  CHANGE `firstname` `firstname` VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  CHANGE `lastname` `lastname` VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;

ALTER TABLE `cfr_customer_group`
  ADD `customer_group_code` VARCHAR(50) NULL AFTER `sort_order`;

-- -----------------------------------------------------------
-- -------- end v2.7.3 New Customer Design -------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------------- start v2.7.4 Cash Flow --------------------
-- -----------------------------------------------------------

-- cfr_receipt_voucher
CREATE TABLE IF NOT EXISTS `cfr_receipt_voucher` (
    `receipt_voucher_id` INT(11) NOT NULL AUTO_INCREMENT,
    `receipt_voucher_code` VARCHAR(50) NOT NULL,
    `receipt_voucher_type_id` INT(11) NOT NULL,
    `order_id` INT(11) NULL DEFAULT NULL,
    `status` INT(11) NOT NULL,
    `in_business_report_status` TINYINT(4) DEFAULT 1,
    `amount` decimal(18,4) NOT NULL DEFAULT '0.0000',
    `store_id` INT(11) NOT NULL,
    `cash_flow_method_id` INT(11) NOT NULL,
    `cash_flow_object_id` INT(11) NULL DEFAULT NULL,
    `object_info` TEXT NULL DEFAULT NULL,
    `receipt_type_other` VARCHAR (255) NULL DEFAULT NULL,
    `note` TEXT NULL DEFAULT NULL,
    `date_added` DATETIME NOT NULL,
    `date_modified` DATETIME NOT NULL,
    PRIMARY KEY (`receipt_voucher_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- cfr_payment_voucher
CREATE TABLE IF NOT EXISTS `cfr_payment_voucher` (
    `payment_voucher_id` INT(11) NOT NULL AUTO_INCREMENT,
    `payment_voucher_type_id` INT(11) NOT NULL,
    `payment_voucher_code` VARCHAR(50) NOT NULL,
    `store_receipt_id` INT(11) NULL DEFAULT NULL,
    `status` INT(11) NOT NULL,
    `in_business_report_status` TINYINT(4) DEFAULT 1,
    `amount` decimal(18,4) NOT NULL DEFAULT '0.0000',
    `store_id` INT(11) NOT NULL,
    `cash_flow_method_id` INT(11) NOT NULL,
    `cash_flow_object_id` INT(11) NULL DEFAULT NULL,
    `object_info` TEXT NULL DEFAULT NULL,
    `payment_type_other` VARCHAR (255) NULL DEFAULT NULL,
    `note` TEXT NULL DEFAULT NULL,
    `date_added` DATETIME NOT NULL,
    `date_modified` DATETIME NOT NULL,
    PRIMARY KEY (`payment_voucher_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- cfr_receipt_voucher_type
CREATE TABLE IF NOT EXISTS `cfr_receipt_voucher_type` (
    `receipt_voucher_type_id` INT(11) NOT NULL,
    `language_id` TINYINT(4) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (`receipt_voucher_type_id`, `language_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- cfr_payment_voucher_type
CREATE TABLE IF NOT EXISTS `cfr_payment_voucher_type` (
    `payment_voucher_type_id` INT(11) NOT NULL,
    `language_id` TINYINT(4) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (`payment_voucher_type_id`, `language_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- cfr_cash_flow_method
CREATE TABLE IF NOT EXISTS `cfr_cash_flow_method` (
    `cash_flow_method_id` INT(11) NOT NULL,
    `language_id` TINYINT(4) NOT NULL,
    `name` VARCHAR (255) NOT NULL,
    `description` VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (`cash_flow_method_id`, `language_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- cfr_cash_flow_object
CREATE TABLE IF NOT EXISTS `cfr_cash_flow_object` (
    `cash_flow_object_id` INT(11) NOT NULL,
    `language_id` TINYINT(4) NOT NULL,
    `name` VARCHAR (255) NOT NULL,
    `description` VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (`cash_flow_object_id`, `language_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- Thêm dữ liệu cho các bảng: cfr_cash_flow_method, cfr_cash_flow_object, cfr_receipt_voucher_type, cfr_payment_voucher_type
INSERT INTO `cfr_cash_flow_method` (`cash_flow_method_id`, `language_id`, `name`, `description`) VALUES
(1, 2, 'Tiền mặt', NULL),
(1, 1, 'Cash', NULL),
(2, 2, 'Chuyển khoản', NULL),
(2, 1, 'Transfer', NULL),
(3, 2, 'COD', NULL),
(3, 1, 'COD', NULL),
(4, 2, 'Quẹt thẻ', NULL),
(4, 1, 'Card', NULL);

INSERT INTO `cfr_cash_flow_object` (`cash_flow_object_id`, `language_id`, `name`, `description`) VALUES
(1, 2, 'Khách hàng', NULL),
(1, 1, 'Customer', NULL),
(2, 2, 'Nhân viên', NULL),
(2, 1, 'Staff', NULL),
(3, 2, 'Nhà cung cấp', NULL),
(3, 1, 'Supplier', NULL),
(4, 2, 'Đối tác vận chuyển', NULL),
(4, 1, 'Shipping partner', NULL),
(5, 2, 'Khác', NULL),
(5, 1, 'Other', NULL);

INSERT INTO `cfr_receipt_voucher_type` (`receipt_voucher_type_id`, `language_id`, `name`, `description`) VALUES
(1, 2, 'Đối tác vận chuyển đặt cọc', NULL),
(1, 1, 'Shipping partner makes a deposit', NULL),
(2, 2, 'Thu nợ đối tác vận chuyển', NULL),
(2, 1, 'Collecting debt from shipping partner', NULL),
(3, 2, 'Thu nhập khác', NULL),
(3, 1, 'Other income', NULL),
(4, 2, 'Tiền thưởng', NULL),
(4, 1, 'Bonus/Reward', NULL),
(5, 2, 'Tiền bồi thường', NULL),
(5, 1, 'Compensation', NULL),
(6, 2, 'Cho thuê, thanh lý tài sản', NULL),
(6, 1, 'Rent and liquidation property', NULL),
(7, 2, 'Thu nợ khách hàng', NULL),
(7, 1, 'Collecting debt from customer', NULL),
(8, 2, 'Thu tự động', NULL),
(8, 1, 'Automation receipt', NULL),
(9, 2, 'Khác', NULL),
(9, 1, 'Other', NULL);

INSERT INTO `cfr_payment_voucher_type` (`payment_voucher_type_id`, `language_id`, `name`, `description`) VALUES
(1, 2, 'Trả nợ đối tác vận chuyển', NULL),
(1, 1, 'Paying debt for shipping partner', NULL),
(2, 2, 'Chi phí sản xuất', NULL),
(2, 1, 'Production cost', NULL),
(3, 2, 'Chi phí nguyên - vật liệu', NULL),
(3, 1, 'Raw materials cost', NULL),
(4, 2, 'Chi phí sinh hoạt', NULL),
(4, 1, 'Living cost', NULL),
(5, 2, 'Chi phí nhân công', NULL),
(5, 1, 'Labor cost', NULL),
(6, 2, 'Chi phí bán hàng', NULL),
(6, 1, 'Selling cost', NULL),
(7, 2, 'Chi phí quản lý cửa hàng', NULL),
(7, 1, 'Store management cost', NULL),
(8, 2, 'Chi tự động', NULL),
(8, 1, 'Automation Payment', NULL),
(9, 2, 'Chi phí khác', NULL),
(9, 1, 'Other cost', NULL);

-- -----------------------------------------------------------
-- ---------------- end v2.7.4 Cash Flow ---------------------
-- -----------------------------------------------------------


-- -----------------------------------------------------------
-- ---- HOT FIX change type fields on table lazada_product ---
-- -----------------------------------------------------------

ALTER TABLE `cfr_lazada_product` CHANGE
             `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
             CHANGE `short_description` `short_description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

-- -----------------------------------------------------------
-- -- end HOT FIX change type fields on table lazada_product--
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- start activity log analytics ---------------------
-- -----------------------------------------------------------

-- Insert event to tracking in table cfr_event
INSERT INTO `cfr_event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES
('tracking_order_complete', 'admin/model/sale/order/updateStatus/after', 'event/activity_tracking/trackingOrderComplete', 1, 0),
('tracking_login', 'admin/model/user/user/editLastLoggedIn/after', 'event/activity_tracking/trackingLogin', 1, 0),
('tracking_product_category', 'admin/model/catalog/store_take_receipt/addStoreTakeReceipt/after', 'event/activity_tracking/trackingStoreTakeReceipt', 1, 0),
('tracking_new_product', 'admin/model/catalog/product/addProduct/after', 'event/activity_tracking/trackingNewProduct', 1, 0),
('tracking_transfer_order_status', 'admin/model/sale/order_history/addHistory/after', 'event/activity_tracking/trackingTransferOrderStatus', 1, 0),
('tracking_new_product_category', 'admin/model/catalog/category/addCategory/after', 'event/activity_tracking/trackingNewProductCategory', 1, 0),
('tracking_new_product_category', 'admin/model/catalog/category/addCategoryFast/after', 'event/activity_tracking/trackingNewProductCategory', 1, 0),
('tracking_delete_product_category', 'admin/model/catalog/category/deleteCategory/after', 'event/activity_tracking/trackingDeleteProductCategory', 1, 0),
('tracking_product_price', 'admin/model/catalog/product/addProduct/after', 'event/activity_tracking/trackingProductPrice', 1, 0),
('tracking_product_price', 'admin/model/catalog/product/editProduct/after', 'event/activity_tracking/trackingProductPrice', 1, 0),
('tracking_new_store_transfer_receipt', 'admin/model/catalog/store_transfer_receipt/addStoreTransferReceipt/after', 'event/activity_tracking/trackingStoreTransferReceipt', 1, 0),
('tracking_new_store_receipt', 'admin/model/catalog/store_receipt/addStoreReceipt/after', 'event/activity_tracking/trackingStoreReceipt', 1, 0),
('tracking_new_cost_adjustment_receipt', 'admin/model/catalog/cost_adjustment_receipt/addReceipt/after', 'event/activity_tracking/trackingCostAdjustmentReceipt', 1, 0),
('tracking_new_discount', 'admin/model/discount/discount/addDiscount/after', 'event/activity_tracking/trackingCountDiscount', 1, 0),
('tracking_new_order_catalog', 'catalog/model/sale/order/addOrderCommon/after', 'event/activity_tracking/trackingCountOrder', 1, 0),
('tracking_new_order_admin', 'admin/model/sale/order/addOrder/after', 'event/activity_tracking/trackingCountOrder', 1, 0),
('tracking_new_shopee', 'admin/model/catalog/shopee/addShopeeShop/after', 'event/activity_tracking/trackingCountShopee', 1, 0),
('tracking_new_shopee_product', 'admin/model/catalog/product/addProduct/after', 'event/activity_tracking/trackingCountShopeeProduct', 1, 0),
('tracking_pos_order_complete', 'catalog/model/sale/order/addOrderCommon/after', 'event/activity_tracking/trackingOrderComplete', 1, 0);

-- -----------------------------------------------------------
-- -------- end activity log analytics -----------------------
-- -----------------------------------------------------------


-- -----------------------------------------------------------
-- ---------- start v2.8.1 Shopee advance product ------------
-- -----------------------------------------------------------

-- IMPORTANT: all shopee tables SHOULD in model/sale_channel/shopee on add/remove from left menu!!!

-- cfr_shopee_product_version
CREATE TABLE IF NOT EXISTS `cfr_shopee_product_version` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `item_id` VARCHAR(64) NOT NULL ,
  `variation_id` BIGINT(64) NOT NULL ,
  `version_name` VARCHAR(128) NULL ,
  `reserved_stock` INT(11) NULL DEFAULT '0' ,
  `product_weight` DECIMAL(15,4) NULL ,
  `original_price` DECIMAL(15,4) NULL ,
  `price` DECIMAL(15,4) NULL ,
  `discount_id` INT NULL DEFAULT '0' ,
  `create_time` VARCHAR(64) NULL DEFAULT NULL ,
  `update_time` VARCHAR(64) NULL DEFAULT NULL ,
  `sku` VARCHAR(64) NULL ,
  `stock` INT(11) NULL DEFAULT '0' ,
  `is_set_item` BOOLEAN NULL ,
  `bestme_product_id` INT(11) NULL ,
  `bestme_product_version_id` INT(11) NULL ,
  `sync_status` INT(11) NULL ,
  `status` VARCHAR(32) NULL ,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- IMPORTANT: ALWAYS create table "shopee_product" because of below alter table sql "ALTER TABLE `cfr_shopee_product`..."
CREATE TABLE IF NOT EXISTS `cfr_shopee_product` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `item_id` BIGINT(64) NOT NULL ,
  `shopid` BIGINT(64) NOT NULL ,
  `sku` VARCHAR(64) NULL ,
  `stock` INT(32) NULL ,
  `status` VARCHAR(32) NULL ,
  `name` VARCHAR(255) CHARACTER SET utf8 NOT NULL ,
  `description` TEXT CHARACTER SET utf8 NOT NULL ,
  `images` TEXT NOT NULL ,
  `currency` VARCHAR(32) NOT NULL ,
  `price` FLOAT(15,4) NOT NULL ,
  `original_price` FLOAT(15,4) NOT NULL ,
  `weight` FLOAT(15,4) NOT NULL ,
  `category_id` BIGINT(64) NOT NULL ,
  `has_variation` BOOLEAN NOT NULL DEFAULT FALSE ,
  `variations` TEXT CHARACTER SET utf8 NULL ,
  `attributes` TEXT CHARACTER SET utf8 NULL ,
  `bestme_id` INT(11) NULL ,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- more columns to "cfr_shopee_product"
ALTER TABLE `cfr_shopee_product`
    ADD `sync_status` INT(11) NULL AFTER `updated_at`,
    ADD `bestme_product_version_id` INT(11) NULL AFTER `sync_status`;

-- -----------------------------------------------------------
-- ----------- end v2.8.1 Shopee advance product -------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------ start v2.8.2 Transport Status Management -----------
-- -----------------------------------------------------------

ALTER TABLE `cfr_order` ADD `manual_update_status` TINYINT NOT NULL DEFAULT '0' AFTER `date_modified`;

-- -----------------------------------------------------------
-- ------ end v2.8.2 Transport Status Management -------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ---------------- start v2.8.3 tuning ----------------------
-- -----------------------------------------------------------

-- temp not use FULLTEXT
-- ALTER TABLE `cfr_product_description`
--   ADD FULLTEXT(`name`);

ALTER TABLE `cfr_blog_description`
  ADD `type` VARCHAR (255) NOT NULL DEFAULT 'image' AFTER `alt`,
  ADD `video_url` TEXT NULL AFTER `type`;

-- -----------------------------------------------------------
-- ------------------ end v2.8.3 tuning ----------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------------ start v2.8.4 Pos return goods ----------------
-- -----------------------------------------------------------

-- cfr_return_receipt
CREATE TABLE IF NOT EXISTS `cfr_return_receipt` (
  `return_receipt_id` INT(11) NOT NULL AUTO_INCREMENT,
  `order_id` INT(11) NOT NULL,
  `return_receipt_code` VARCHAR(50) NOT NULL,
  `return_fee` DECIMAL(18,4) NULL DEFAULT NULL,
  `total` DECIMAL(18,4) NULL DEFAULT NULL,
  `note` VARCHAR(255) NULL DEFAULT NULL,
  `date_added` DATETIME NOT NULL,
  PRIMARY KEY (`return_receipt_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- cfr_return_product
CREATE TABLE IF NOT EXISTS `cfr_return_product` (
  `return_product_id` INT(11) NOT NULL AUTO_INCREMENT,
  `return_receipt_id` INT(11) NOT NULL,
  `product_id` INT(11) NOT NULL,
  `product_version_id` INT(11) NULL DEFAULT '0',
  `price` DECIMAL(18,4) NULL DEFAULT NULL,
  `quantity` INT(4) NOT NULL,
  PRIMARY KEY (`return_product_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- add column return_receipt_id to table cfr_payment_voucher
ALTER TABLE `cfr_payment_voucher`
  ADD COLUMN `return_receipt_id` INT(11) NULL DEFAULT NULL AFTER `store_receipt_id`;

-- -----------------------------------------------------------
-- ------------- end v2.8.4 Pos return goods -----------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ---------------- start v2.9.1 Bestme package --------------
-- -----------------------------------------------------------

-- update more detail of current paid packet in cfr_setting
INSERT INTO `cfr_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
('0','config','config_packet_capacity','',0),
('0','config','config_packet_number_of_stores','',0),
('0','config','config_packet_number_of_staffs','',0),
('0','config','config_packet_unlimited_capacity','1',0),
('0','config','config_packet_unlimited_stores','1',0),
('0','config','config_packet_unlimited_staffs','1',0);

-- add size to table cfr_images
ALTER TABLE `cfr_images` ADD `size` FLOAT (5)  NOT NULL DEFAULT 0 AFTER `source_id`;

-- -----------------------------------------------------------
-- ---------------- end v2.9.1 Bestme package ----------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ---------------- start v2.10.1 image folder --------------
-- -----------------------------------------------------------

CREATE TABLE IF NOT EXISTS `cfr_image_folder` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `cfr_images`
    ADD COLUMN `folder_path` INT(11) NOT NULL DEFAULT 0 AFTER `size`;

-- -----------------------------------------------------------
-- ---------------- end v2.10.1 image folder --------------

-- -----------------------------------------------------------
-- ---------------- start v2.9.2 Advance Report --------------

ALTER TABLE `cfr_report_product`
    ADD `cost_price` DECIMAL(18,4) NULL DEFAULT '0.0000' AFTER `total_amount`,
    ADD `discount` DECIMAL(18,4) NOT NULL DEFAULT '0.0000' AFTER `quantity`,
    ADD `return_amount` DECIMAL(18,4) NULL DEFAULT '0.0000' AFTER `discount`,
    ADD `quantity_return` INT(11) NULL DEFAULT '0' AFTER `return_amount`;

ALTER TABLE `cfr_report_order`
    ADD `shipping_fee`DECIMAL(18,4) NOT NULL DEFAULT '0.0000' AFTER `total_amount`,
    ADD `discount` DECIMAL(18,4) NOT NULL DEFAULT '0.0000' AFTER `shipping_fee`,
    ADD `source` VARCHAR(50) NULL DEFAULT NULL AFTER `order_status`,
    ADD `total_return` DECIMAL(18,4) NOT NULL DEFAULT '0.0000' AFTER `total_amount`;

-- rename value 'Chi phí quản lý cửa hàng'
UPDATE `cfr_payment_voucher_type` SET `name` = 'Chi phí thuế TNDN'
    WHERE `payment_voucher_type_id` = 7
    AND `language_id` = 2;

-- rename value 'Store management cost'
UPDATE `cfr_payment_voucher_type` SET `name` = 'Enterprise income tax expenses'
    WHERE `payment_voucher_type_id` = 7
    AND `language_id` = 1;

INSERT INTO `cfr_event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES
   ('update_report_when_create_return_receipt', 'admin/model/sale/return_receipt/addReturnReceipt/after', 'event/report_order/afterCreateReturnReceipt', 1, 0);
-- -----------------------------------------------------------
-- ---------------- end v2.9.2 Advance Report ----------------

-- -----------------------------------------------------------
-- ----------- start v2.9.2-advance-report-xon-3448 ----------

ALTER TABLE `cfr_return_receipt`
  ADD `store_id` INT(11) NULL AFTER `note`;

-- -----------------------------------------------------------
-- ----------- end v2.9.2-advance-report-xon-3448 ------------

-- -----------------------------------------------------------
-- ------------ start v2.10.2 Tuning ---------------------------
-- -----------------------------------------------------------

-- -- Custom filters by product attributes
-- cfr_attribute_filter
CREATE TABLE IF NOT EXISTS `cfr_attribute_filter` (
    `attribute_filter_id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` TEXT NOT NULL COLLATE 'utf8_general_ci',
    `value` TEXT NOT NULL COLLATE 'utf8_general_ci',
    `status` TINYINT(1) NOT NULL DEFAULT 0,
    `language_id` INT(11) NOT NULL,
    PRIMARY KEY (`attribute_filter_id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

-- cfr_product_attribute_filter
CREATE TABLE IF NOT EXISTS `cfr_product_attribute_filter` (
    `product_id` INT(11) NOT NULL,
    `attribute_filter_id` INT(11) NOT NULL,
    `attribute_filter_value` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',
    PRIMARY KEY (`product_id`, `attribute_filter_id`, `attribute_filter_value`) USING BTREE
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

-- Alter cfr_blog_description add seo_keywords
ALTER TABLE `cfr_blog_description`
    ADD `seo_keywords` VARCHAR(500) NULL DEFAULT NULL AFTER `meta_description`;

-- -----------------------------------------------------------
-- ------------ end v2.10.2 Tuning ---------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ---------------- start v2.10.3 - shopee-upload-product ----
-- -----------------------------------------------------------

-- insert column version in talbe cfr_migration
ALTER TABLE `cfr_migration`
  ADD `version` INT(11) DEFAULT 1 NULL AFTER `date_run`;

-- table cache data for Shopee's categories
CREATE TABLE IF NOT EXISTS `cfr_shopee_category` (
  `shopee_category_id` BIGINT(64) NOT NULL AUTO_INCREMENT ,
  `parent_id` BIGINT(64) NOT NULL ,
  `name_vi` VARCHAR(255) NULL ,
  `name_en` VARCHAR(255) NULL ,
  `has_children` BOOLEAN NOT NULL ,
  PRIMARY KEY (`shopee_category_id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- table cache data for Shopee's logistics
CREATE TABLE IF NOT EXISTS `cfr_shopee_logistics` (
  `logistic_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `logistic_name` VARCHAR(255) NULL ,
  `enabled` BOOLEAN NULL ,
  `fee_type` VARCHAR(255) NULL ,
  `sizes` TEXT NULL ,
  `item_max_weight` DECIMAL(15,8) NULL ,
  `item_min_weight` DECIMAL(15,8) NULL ,
  `height` DECIMAL(15,8) NULL ,
  `width` DECIMAL(15,8) NULL ,
  `length` DECIMAL(15,8) NULL ,
  `volumetric_factor` INT(11) NULL ,
  PRIMARY KEY (`logistic_id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- table `cfr_shopee_upload_product`
CREATE TABLE IF NOT EXISTS `cfr_shopee_upload_product` (
   `shopee_upload_product_id` INT(11) NOT NULL AUTO_INCREMENT ,
   `bestme_product_id` INT(11) NULL ,
  `name` VARCHAR(255) NOT NULL ,
  `description` TEXT NOT NULL ,
  `category_id` BIGINT(64) NULL ,
  `version_attribute` TEXT NULL,
  `image` VARCHAR(255) NULL ,
  `price` DECIMAL(18,4) NOT NULL ,
  `stock` INT(11) NOT NULL ,
  `sku` VARCHAR(255) NULL ,
  `common_price` DECIMAL(18,4)  NULL ,
  `common_stock` INT(11) NULL ,
  `common_sku` VARCHAR(255)  NULL ,
  `weight` INT(11) NULL ,
  `length` INT(11) NULL ,
  `width` INT(11) NULL ,
  `height` INT(11) NULL ,
  `date_added` TIMESTAMP NULL ,
  `date_modify` TIMESTAMP NULL ,
  PRIMARY KEY (`shopee_upload_product_id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- table `cfr_shopee_upload_product_version`
CREATE TABLE IF NOT EXISTS `cfr_shopee_upload_product_version` (
  `shopee_upload_product_version_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `shopee_upload_product_id` INT(11) NOT NULL ,
  `version_name` VARCHAR(255) NOT NULL ,
  `version_price` DECIMAL(18,4) NOT NULL,
  `version_stock` INT(11) NOT NULL ,
  `version_sku` VARCHAR(255) NULL ,
  `variation_id` BIGINT(32) NULL ,
  PRIMARY KEY (`shopee_upload_product_version_id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- table `cfr_shopee_upload_product_image`
CREATE TABLE IF NOT EXISTS `cfr_shopee_upload_product_image` (
  `shopee_upload_product_image_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `product_id` INT(11) NOT NULL ,
  `image` VARCHAR(255) NULL ,
  `sort_order` INT(11) NULL ,
  PRIMARY KEY (`shopee_upload_product_image_id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- table `cfr_shopee_upload_product_attribute`
CREATE TABLE IF NOT EXISTS `cfr_shopee_upload_product_attribute` (
  `shopee_upload_product_attribute_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `product_id` INT(11) NOT NULL ,
  `attribute_id` INT(11) NOT NULL ,
  `value` VARCHAR(255) NULL ,
  PRIMARY KEY (`shopee_upload_product_attribute_id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- table `cfr_shopee_upload_product_logistic`
CREATE TABLE IF NOT EXISTS `cfr_shopee_upload_product_logistic` (
  `shopee_upload_product_logistic_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `logistic_id` INT(11) NOT NULL ,
  `product_id` INT(11) NOT NULL ,
  `enabled` BOOLEAN NOT NULL ,
  PRIMARY KEY (`shopee_upload_product_logistic_id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- table `cfr_shopee_upload_product_shop_update_result`
CREATE TABLE IF NOT EXISTS `cfr_shopee_upload_product_shop_update_result` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `shopee_upload_product_id` INT(11) NULL ,
  `shopee_shop_id` BIGINT(64) NULL ,
  `shopee_item_id` BIGINT(64) NULL ,
  `status` TINYINT(4) NULL ,
  `in_process` TINYINT(2) NULL DEFAULT '0' ,
  `error_code` VARCHAR(255) NULL ,
  `error_msg` VARCHAR(255) NULL ,
  `variation_result` TEXT NULL ,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- -----------------------------------------------------------
-- ---------------- end v2.10.3-shopee-upload-product ------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------------ start v2.11.2 Tuning -------------------------
-- -----------------------------------------------------------

-- Config product version images
-- product_version
ALTER TABLE `cfr_product_version`
    ADD COLUMN `image` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci' AFTER `deleted`,
    ADD COLUMN `image_alt` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci' AFTER `image`;

-- add table cfr_product_description_tab
CREATE TABLE `cfr_product_description_tab` (
      `product_description_tab_id` int(11) NOT NULL AUTO_INCREMENT,
      `product_id` int(11) NOT NULL,
      `title` varchar(255) COLLATE utf8_general_ci NOT NULL,
      `description` text COLLATE utf8_general_ci NOT NULL,
      PRIMARY KEY (`product_description_tab_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- add table cfr_rate_website
CREATE TABLE IF NOT EXISTS `cfr_rate_website` (
    `rate_id` int(11) NOT NULL AUTO_INCREMENT,
    `status` tinyint(4) NOT NULL,
    `customer_name` varchar(255) NOT NULL,
    `sub_info` text,
    `content` text,
    `image` varchar(255) DEFAULT NULL,
    `alt` varchar(255) DEFAULT NULL,
    `date_added` datetime NOT NULL,
    `date_modified` datetime NOT NULL,
    PRIMARY KEY (`rate_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- add new column to cfr_shopee_shop_config
ALTER TABLE `cfr_shopee_shop_config`
  ADD `order_sync_range` INT(11) NULL DEFAULT '15' AFTER `connected`,
  ADD `allow_update_stock_product` TINYINT(2) NULL DEFAULT '0' AFTER `order_sync_range`,
  ADD `allow_update_price_product` TINYINT(2) NULL DEFAULT '0' AFTER `allow_update_stock_product`,
  ADD `last_sync_time_product` TIMESTAMP NULL AFTER `allow_update_price_product`,
  ADD `last_sync_time_order` TIMESTAMP NULL AFTER `last_sync_time_product`;

-- add new column to cfr_lazada_shop_config
ALTER TABLE `cfr_lazada_shop_config`
  ADD `order_sync_range` INT(11) NULL DEFAULT '15' AFTER `connected`,
  ADD `allow_update_stock_product` TINYINT(2) NULL DEFAULT '0' AFTER `order_sync_range`,
  ADD `allow_update_price_product` TINYINT(2) NULL DEFAULT '0' AFTER `allow_update_stock_product`,
  ADD `last_sync_time_product` TIMESTAMP NULL AFTER `allow_update_price_product`,
  ADD `last_sync_time_order` TIMESTAMP NULL AFTER `last_sync_time_product`;

-- -----------------------------------------------------------
-- ------------ end v2.11.2 Tuning ---------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------------ start v3.1.3 Generate demo data Website ------
-- -----------------------------------------------------------

INSERT INTO `cfr_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'config', 'config_use_demo_data', '1', 0);

-- -----------------------------------------------------------
-- ---------------- end v3.1.3 Generate demo data Website ----
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------------ start v3.3.1 Advance Staff Permission ------
-- -----------------------------------------------------------

-- Add table user_store
CREATE TABLE IF NOT EXISTS `cfr_user_store` (
        `id` INT(11) NOT NULL AUTO_INCREMENT ,
        `user_id` INT(11) NOT NULL ,
        `store_id` VARCHAR(255) NULL ,
        PRIMARY KEY (`id`)) ENGINE = MyISAM
        CHARSET=utf8 COLLATE utf8_general_ci;

-- Add access_all column in user_group table
ALTER TABLE `cfr_user_group`
	ADD COLUMN `access_all` TINYINT NULL DEFAULT '1' AFTER `permission`;

-- Add user_create_id for multi table
ALTER TABLE `cfr_customer`
	ADD COLUMN `user_create_id` INT(11) NULL DEFAULT '1' AFTER `staff_in_charge`;

ALTER TABLE `cfr_customer_group`
	ADD COLUMN `user_create_id` INT(11) NULL DEFAULT '1' AFTER `customer_group_code`;

ALTER TABLE `cfr_manufacturer`
	ADD COLUMN `user_create_id` INT(11) NULL DEFAULT '1' AFTER `status`;

ALTER TABLE `cfr_collection`
	ADD COLUMN `user_create_id` INT(11) NULL DEFAULT '1' AFTER `type`;

ALTER TABLE `cfr_product`
	ADD COLUMN `user_create_id` INT(11) NULL DEFAULT '1' AFTER `default_store_id`;

ALTER TABLE `cfr_return_receipt`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `note`;

ALTER TABLE `cfr_payment_voucher`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `note`;

ALTER TABLE `cfr_receipt_voucher`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `note`;

ALTER TABLE `cfr_store_receipt`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `status`;

ALTER TABLE `cfr_store_transfer_receipt`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `total`;

ALTER TABLE `cfr_store_take_receipt`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `difference_amount`;

ALTER TABLE `cfr_cost_adjustment_receipt`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `note`;

ALTER TABLE `cfr_discount`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `end_at`;


-- -----------------------------------------------------------
-- ------------ end v3.3.1 Advance Staff Permission ------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------------ start v3.4.2 Sync bestme social --------------
-- -----------------------------------------------------------

ALTER TABLE `cfr_customer` ADD `is_authenticated` TINYINT(1) NOT NULL DEFAULT '1' AFTER `customer_source`;

ALTER TABLE `cfr_customer` CHANGE `customer_source` `customer_source` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '1: NovaonX Social, 2: Facebook, 3: Bestme';

ALTER TABLE `cfr_customer` ADD `subscriber_id` VARCHAR(255) NULL AFTER `user_create_id`;

CREATE TABLE `cfr_order_channel` (
  `order_id` INT(11) NOT NULL ,
  `channel_id` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`order_id`, `channel_id`)
) ENGINE = INNODB;

CREATE TABLE `cfr_order_campaign` (
  `order_id` INT(11) NOT NULL ,
  `campaign_id` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`order_id`, `campaign_id`)
) ENGINE = INNODB;

CREATE TABLE `cfr_campaign_voucher` (
  `campaign_voucher_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `voucher_code` VARCHAR(255) NULL DEFAULT NULL ,
  `voucher_type` INT(11) NULL COMMENT '1: percent. 2: amount' ,
  `amount` DECIMAL(18,4) NULL DEFAULT 0.000 ,
  `user_create_id` INT(11) NULL ,
  `start_at` TIMESTAMP NULL DEFAULT NULL ,
  `end_at` TIMESTAMP NULL DEFAULT NULL ,
  `date_added` TIMESTAMP NULL DEFAULT NULL ,
  `date_modified` TIMESTAMP NULL DEFAULT NULL ,
  PRIMARY KEY (`campaign_voucher_id`)
) ENGINE = MyISAM;

ALTER TABLE `cfr_order_product` ADD `campaign_voucher_id` INT(11) NULL DEFAULT NULL AFTER `tax`;

ALTER TABLE `cfr_campaign_voucher` ADD `order_id` INT(11) NULL DEFAULT NULL AFTER `campaign_voucher_id`;

-- -----------------------------------------------------------
-- ------------ end v3.4.2 Sync bestme social ----------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------------ start XOCF-192 -------------------------------
-- -----------------------------------------------------------

ALTER TABLE `cfr_order` ADD `collection_amount` DECIMAL(18,4) NULL DEFAULT NULL AFTER `total`;

UPDATE `cfr_order` SET `collection_amount` = `total` WHERE `collection_amount` IS NULL;

-- -----------------------------------------------------------
-- ------------ end XOCF-192 ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------------ start v3.5.1 Open Api ------------------------
-- -----------------------------------------------------------

-- create table `cfr_keyword_sync`
CREATE TABLE `cfr_keyword_sync` (
  `keyword_id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` text COLLATE utf8_unicode_ci NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (keyword_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- add `source` column to tables: `product`, `manufacturer`, `blog`, `blog_category`, `page_contents`, `discount`, `category`
ALTER TABLE `cfr_product` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `user_create_id`;
ALTER TABLE `cfr_manufacturer` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `user_create_id`;
ALTER TABLE `cfr_blog` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `demo`;
ALTER TABLE `cfr_blog_category` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `status`;
ALTER TABLE `cfr_page_contents` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `seo_description`;
ALTER TABLE `cfr_discount` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `updated_at`;
ALTER TABLE `cfr_category` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `status`;

-- -----------------------------------------------------------
-- ------------ end v3.5.1 Open Api ------------------------
-- -----------------------------------------------------------

-- -------------- start XOCF-264 -------------------------------
CREATE TABLE `cfr_email_subscribers` (
    `email_id` int(11) NOT NULL AUTO_INCREMENT,
    `email` text COLLATE utf8_unicode_ci NOT NULL,
    `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (email_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
-- -------------- end XOCF-264 -------------------------------

-- -----------------v3.6.2-agency-brands----------------------
ALTER TABLE `cfr_product` ADD `brand_shop_name` VARCHAR(255) NULL DEFAULT NULL AFTER `source`;
-- -----------------End v3.6.2-agency-brands------------------

-- -----------------migration_20211201_add_source_author_name_to_blog_description_table----------------------
ALTER TABLE `cfr_blog_description` ADD `source_author_name` VARCHAR(255) NULL DEFAULT NULL AFTER `video_url`;
-- -----------------End migration_20211201_add_source_author_name_to_blog_description_table------------------

-- -----------------migration_20220415_set_sitemap_detail_lastmod----------------------
INSERT INTO `cfr_setting` (`store_id`, `code`, `key`,`value`, `serialized`) SELECT 0, 'config', 'sitemap_categories_lastmod', '2022-04-15', '0' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM `cfr_setting` WHERE `code` = 'config' AND `key` = 'sitemap_categories_lastmod');
INSERT INTO `cfr_setting` (`store_id`, `code`, `key`,`value`, `serialized`) SELECT 0, 'config', 'sitemap_products_lastmod', '2022-04-15', '0' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM `cfr_setting` WHERE `code` = 'config' AND `key` = 'sitemap_products_lastmod');
INSERT INTO `cfr_setting` (`store_id`, `code`, `key`,`value`, `serialized`) SELECT 0, 'config', 'sitemap_manufacturers_lastmod', '2022-04-15', '0' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM `cfr_setting` WHERE `code` = 'config' AND `key` = 'sitemap_manufacturers_lastmod');
INSERT INTO `cfr_setting` (`store_id`, `code`, `key`,`value`, `serialized`) SELECT 0, 'config', 'sitemap_collections_lastmod', '2022-04-15', '0' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM `cfr_setting` WHERE `code` = 'config' AND `key` = 'sitemap_collections_lastmod');
-- -----------------End migration_20220415_set_sitemap_detail_lastmod------------------

-- -----------------migration_20220416_add_seo_url_events----------------------
INSERT INTO `cfr_event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES
('add_categories_sitemap_lastmod_after', 'admin/model/catalog/category/addCategory/after', 'event/seo_url/afterAddCategory', 1, 0),
('add_blog_categories_sitemap_lastmod_after', 'admin/model/blog/category/createBlogCategory/after', 'event/seo_url/afterCreateBlogCategory', 1, 0),
('add_collections_sitemap_lastmod_after', 'admin/model/catalog/collection/addCollection/after', 'event/seo_url/afterAddCollection', 1, 0),
('add_products_sitemap_lastmod_after', 'admin/model/catalog/product/addProduct/after', 'event/seo_url/afterAddProduct', 1, 0),
('add_manufacturers_sitemap_lastmod_after', 'admin/model/catalog/manufacturer/addManufacturer/after', 'event/seo_url/afterAddManufacturer', 1, 0),
('add_blogs_sitemap_lastmod_after', 'admin/model/blog/blog/addBlog/after', 'event/seo_url/afterAddBlog', 1, 0),

('delete_categories_sitemap_lastmod_after', 'admin/model/catalog/category/deleteCategory/after', 'event/seo_url/afterDeleteCategory', 1, 0),
('delete_blog_categories_sitemap_lastmod_after', 'admin/model/blog/category/deleteCategory/after', 'event/seo_url/afterDeleteBlogCategory', 1, 0),
('delete_collections_sitemap_lastmod_after', 'admin/model/catalog/collection/deleteCollection/after', 'event/seo_url/afterDeleteCollection', 1, 0),
('delete_products_sitemap_lastmod_before', 'admin/model/catalog/product/deleteProduct/before', 'event/seo_url/beforeDeleteProduct', 1, 0),
('delete_blogs_sitemap_lastmod_before', 'admin/model/blog/blog/deleteBlog/before', 'event/seo_url/beforeDeleteBlog', 1, 0),

('update_categories_sitemap_lastmod_before', 'admin/model/catalog/category/editCategory/before', 'event/seo_url/beforeEditCategory', 1, 0),
('update_blog_categories_sitemap_lastmod_before', 'admin/model/blog/category/editBlogCategory/before', 'event/seo_url/beforeEditBlogCategory', 1, 0),
('update_collections_sitemap_lastmod_before', 'admin/model/catalog/collection/editCollection/before', 'event/seo_url/beforeEditCollection', 1, 0),
('update_products_sitemap_lastmod_after', 'admin/model/catalog/product/editProduct/after', 'event/seo_url/afterEditProduct', 1, 0),
('update_manufacturers_sitemap_lastmod_before', 'admin/model/catalog/manufacturer/editManufacturer/before', 'event/seo_url/beforeEditManufacturer', 1, 0),
('update_blogs_sitemap_lastmod_after', 'admin/model/blog/blog/editBlog/after', 'event/seo_url/afterEditBlog', 1, 0);
-- -----------------End migration_20220416_add_seo_url_events------------------

-- -----------------migration_20220520_create_flash_sale_table----------------------
CREATE TABLE `cfr_flash_sale` (
    `flash_sale_id` INT(11) NOT NULL AUTO_INCREMENT,
    `collection_id` INT(11) NOT NULL,
    `starting_date` DATE NOT NULL,
    `ending_date` DATE NOT NULL,
    `starting_time` TIME NOT NULL,
    `ending_time` TIME NOT NULL,
    `status` TINYINT(1) NOT NULL DEFAULT '1',
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`flash_sale_id`),
    INDEX `collection_id` (`collection_id`)
) ENGINE=MyISAM COLLATE='utf8mb4_unicode_ci';
-- -----------------End migration_20220520_create_flash_sale_table------------------

--
-- TODO: Migrate by NOVAON from here...
--

-- -----------------------------------------------------------
-- --------Always at the end of file -------------------------
-- -----------------------------------------------------------

-- --------Load theme default data ---------------------------
-- TODO: call in cli_install?...

--
-- v1.5.2 Smart loading demo data base on theme group (theme type)
-- Note: following content is copied from file "library/theme_config/demo_data/tech.sql" for default
--
-- 2020/04: More support loading demo data from special theme. If no special theme defined, use theme group as above
-- Note: following content may be copied from file "library/theme_config/demo_data/special_shop/xxx.sql" if special file defined

-- 2020/07/30: change theme demo to fashion_luvibee_2.
-- Note: following content is copied from file "library/theme_config/demo_data/special_shop/fashion_luvibee_2.sql"
--

-- cfr_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `cfr_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(20, '', 0, 0, 0, 0, 1, '2019-05-21 16:20:38', '2020-06-09 20:28:48'),
(19, NULL, 0, 0, 0, 0, 1, '2019-05-21 15:47:12', '2019-05-21 15:47:12'),
(18, NULL, 0, 0, 0, 0, 1, '2019-05-21 15:44:23', '2019-05-21 15:44:23'),
(17, NULL, 0, 0, 0, 0, 1, '2019-05-21 15:36:21', '2019-05-21 15:36:21'),
(14, NULL, 0, 0, 0, 0, 1, '2019-05-20 17:45:50', '2019-05-20 17:45:50'),
(12, NULL, 0, 0, 0, 0, 1, '2019-05-20 16:48:18', '2019-05-20 16:48:18'),
(21, NULL, 0, 0, 0, 0, 1, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(22, NULL, 0, 0, 0, 0, 1, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(23, '', 0, 0, 0, 0, 1, '2020-06-09 17:50:59', '2020-06-09 20:29:17'),
(24, NULL, 0, 0, 0, 0, 1, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(25, NULL, 0, 0, 0, 0, 1, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(26, NULL, 0, 0, 0, 0, 1, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(27, NULL, 0, 0, 0, 0, 1, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(28, '', 23, 0, 0, 0, 1, '2020-06-09 20:29:17', '2020-06-09 20:29:57'),
(29, '', 23, 0, 0, 0, 1, '2020-06-09 20:29:17', '2020-06-09 20:29:17'),
(30, '', 28, 0, 0, 0, 1, '2020-06-09 20:29:57', '2020-06-09 20:29:57'),
(31, '', 28, 0, 0, 0, 1, '2020-06-09 20:29:57', '2020-06-09 20:29:57');

-- cfr_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `cfr_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(18, 2, 'Giày sandal', 'Giày sandal', '', '', ''),
(18, 1, 'Giày sandal', 'Giày sandal', '', '', ''),
(17, 2, 'Giày valen', 'Giày valen', '', '', ''),
(17, 1, 'Giày valen', 'Giày valen', '', '', ''),
(14, 1, 'Giày cao gót', 'Giày cao gót', '', '', ''),
(14, 2, 'Giày cao gót', 'Giày cao gót', '', '', ''),
(12, 1, 'Giày cổ điển', 'Giày cổ điển', '', '', ''),
(12, 2, 'Giày cổ điển', 'Giày cổ điển', '', '', ''),
(21, 2, 'Váy dự tiệc', 'Váy dự tiệc', '', '', ''),
(21, 1, 'Váy dự tiệc', 'Váy dự tiệc', '', '', ''),
(22, 2, 'Váy đầm xòe', 'Váy đầm xòe', '', '', ''),
(22, 1, 'Váy đầm xòe', 'Váy đầm xòe', '', '', ''),
(23, 2, 'Váy dáng suông', 'Váy dáng suông', '', '', ''),
(23, 1, 'Váy dáng suông', 'Váy dáng suông', '', '', ''),
(24, 2, 'Váy đầm sơ mi', 'Váy đầm sơ mi', '', '', ''),
(24, 1, 'Váy đầm sơ mi', 'Váy đầm sơ mi', '', '', ''),
(25, 2, 'Váy đầm xòe họa tiết', 'Váy đầm xòe họa tiết', '', '', ''),
(25, 1, 'Váy đầm xòe họa tiết', 'Váy đầm xòe họa tiết', '', '', ''),
(26, 2, 'Váy đầm lụa đuôi cá', 'Váy đầm lụa đuôi cá', '', '', ''),
(26, 1, 'Váy đầm lụa đuôi cá', 'Váy đầm lụa đuôi cá', '', '', ''),
(27, 2, 'Váy đầm công sở', 'Váy đầm công sở', '', '', ''),
(27, 1, 'Váy đầm công sở', 'Váy đầm công sở', '', '', ''),
(19, 2, 'Dép sandal', 'Dép sandal', '', '', ''),
(19, 1, 'Dép sandal', 'Dép sandal', '', '', ''),
(20, 2, 'Giày búp bê', 'Giày búp bê', '', '', ''),
(20, 1, 'Giày búp bê', 'Giày búp bê', '', '', ''),
(28, 2, 'Váy công sở', 'Váy công sở', '', '', ''),
(28, 1, 'Váy công sở', 'Váy công sở', '', '', ''),
(29, 2, 'Váy dạ tiệc', 'Váy dạ tiệc', '', '', ''),
(29, 1, 'Váy dạ tiệc', 'Váy dạ tiệc', '', '', ''),
(30, 2, 'Váy ngắn', 'Váy ngắn', '', '', ''),
(30, 1, 'Váy ngắn', 'Váy ngắn', '', '', ''),
(31, 2, 'Váy dài', 'Váy dài', '', '', ''),
(31, 1, 'Váy dài', 'Váy dài', '', '', '');

-- cfr_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `cfr_category_path` (`category_id`, `path_id`, `level`) VALUES
(20, 20, 0),
(19, 19, 0),
(18, 18, 0),
(17, 17, 0),
(14, 14, 0),
(12, 12, 0),
(21, 21, 0),
(22, 22, 0),
(23, 23, 0),
(24, 24, 0),
(25, 25, 0),
(26, 26, 0),
(27, 27, 0),
(28, 23, 0),
(28, 28, 1),
(29, 23, 0),
(29, 29, 1),
(30, 23, 0),
(30, 28, 1),
(30, 30, 2),
(31, 23, 0),
(31, 28, 1),
(31, 31, 2);

-- cfr_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `cfr_category_to_store` (`category_id`, `store_id`) VALUES
(12, 0),
(14, 0),
(17, 0),
(18, 0),
(19, 0),
(20, 0),
(21, 0),
(22, 0),
(23, 0),
(24, 0),
(25, 0),
(26, 0),
(27, 0),
(28, 0),
(29, 0),
(30, 0),
(31, 0);

-- cfr_collection PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `cfr_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES
(4, 'Sản phẩm khuyến mại', 0, 1, '', '0'),
(8, 'BST tổng hợp', 0, 1, '', '0'),
(5, 'Sản phẩm hot', 0, 1, '', '0'),
(6, 'Sản phẩm mới', 0, 1, '', '0'),
(7, 'Hàng ngày', 0, 1, NULL, '0');

-- cfr_collection_description PRIMARY KEY (`collection_id`)
INSERT IGNORE INTO `cfr_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES
(7, '/catalog/view/theme/default/image/fashion_shoeszone/products/86/thumb.jpg', '&lt;p&gt;Bộ sưu tập h&amp;agrave;ng ng&amp;agrave;y&lt;/p&gt;', '', '', ''),
(6, '/catalog/view/theme/default/image/fashion_shoeszone/products/85/thumb.jpg', '&lt;p&gt;nhũng sản phẩm mới nhất c&amp;oacute; mặt tại cửa h&amp;agrave;ng&lt;/p&gt;', '', '', ''),
(5, '/catalog/view/theme/default/image/fashion_shoeszone/products/84/thumb.jpg', '&lt;p&gt;sản phẩm hot&lt;/p&gt;', '', '', ''),
(8, '', '', '', '', ''),
(4, '/catalog/view/theme/default/image/fashion_shoeszone/products/83/thumb.jpg', '&lt;p&gt;sản phẩm khuyến mại&lt;/p&gt;', '', '', '');

-- cfr_product PRIMARY KEY (`product_id`)
-- delete first
INSERT IGNORE INTO `cfr_product` (`product_id`, `model`, `sku`, `common_barcode`, `common_sku`, `common_compare_price`, `common_price`, `common_cost_price`, `barcode`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `sale_on_out_of_stock`, `stock_status_id`, `image`, `image_alt`, `multi_versions`, `manufacturer_id`, `shipping`, `price`, `price_currency_id`, `compare_price`, `c_price_currency_id`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `channel`, `viewed`, `demo`, `deleted`, `default_store_id`, `date_added`, `date_modified`) VALUES
(104, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/20209ed8d3e5-f16a-48a7-97c3-afbecb3c0e83.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(105, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020e75418de-aa87-4af8-a37c-c8201ef50716.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(106, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020e06602d8-6162-456c-86af-9f013c2dd601.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(107, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/20204524bc56-1184-43c1-bda7-a9d73c749926.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(108, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020ede6c474-bb04-4f8e-9eff-ad83ee39777f.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(109, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/20201fc77ad9-86cb-4059-baaa-d3a02e9facca.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(110, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/20204db40300-5921-41bc-99c7-d95a8cafd193.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(111, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/20201f1c7af1-02bd-4c51-817a-732a0d29d434.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(112, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/20205fc54653-3cbb-4274-af9d-792cdfe47fe6.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(113, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020f3e60b4e-b2e7-4b71-8d78-5f63620c8ac7.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(114, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020b0cbe789-54f4-4246-ad24-604b6fbf8161.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(115, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/20201c7683f0-093a-42f6-a215-4bbe527314bb.png', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(116, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020f0028a91-1412-4731-b6e3-c8a6110b4ea2.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(117, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020cfc8e4cb-d80e-41d8-8d19-25f93496fc2a.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(118, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020dfc16b74-b265-43f9-afa4-8145d313c70f.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(119, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/202042ff250a-cc98-4bea-97f2-a502800eefa8.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(120, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020cf8f5821-3007-41fe-9c97-8ab57cc50c70.jpg', '', 0, 26, 1, '499000.0000', 1, '699000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 20:37:53'),
(121, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/20208e3c7ca6-1150-4f8d-bb38-4ab31fc60e51.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(122, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020de762b31-5ac6-45b3-9529-20b17417ce06.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(123, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/20203e9203ca-37b1-4cfa-9250-7b7ed13d6446.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(124, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020181d6438-8fb5-4f27-b4b3-6143e035b754.png', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(125, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020c057c3e8-534e-41ff-bdf7-6ead16eccee9.png', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(126, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020c32f96ac-ec40-4cb5-97d1-626fdfb05120.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(127, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/202021ace2ea-a975-4e79-a047-892674ad4629.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(128, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/20205b5be943-f5dc-4a4e-a1a4-766c26e19c1c.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(129, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/20206d07350d-0b13-4737-b089-82e1d63ba904.jpg', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(130, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020105c4d37-5ed2-4653-a4a4-24039929891c.png', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59'),
(131, '', '', '', '', '0.0000', '0.0000', '0.0000', '', '', '', '', '', '', '', 0, 0, 0, 'https://2.pik.vn/2020f92fba6d-d431-4c95-a2c8-0bfea2ede436.png', '', 0, 26, 1, '499000.0000', 1, '599000.0000', 1, 0, 0, '0000-00-00', '500.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, '2020-06-09 17:50:59', '2020-06-09 17:50:59');

-- restore soft-deleted products if demo
UPDATE `cfr_product` SET `deleted` = NULL
WHERE `demo` = 1
  AND `product_id` IN (
      104,
      105,
      106,
      107,
      108,
      109,
      110,
      111,
      112,
      113,
      114,
      115,
      116,
      117,
      118,
      119,
      120,
      121,
      122,
      123,
      124,
      125,
      126,
      127,
      128,
      129,
      130,
      131
  );

-- cfr_product_collection PRIMARY KEY (`product_collection_id`)
INSERT IGNORE INTO `cfr_product_collection` (`product_collection_id`, `product_id`, `collection_id`, `sort_order`) VALUES
(147, 120, 8, 0),
(145, 121, 8, 0),
(144, 122, 8, 0),
(143, 123, 8, 0),
(142, 124, 8, 0),
(141, 125, 8, 0),
(140, 126, 8, 0),
(139, 127, 8, 0),
(138, 128, 8, 0),
(137, 129, 8, 0),
(136, 130, 8, 0),
(135, 131, 8, 0);

-- cfr_product_description PRIMARY KEY (`product_id`,`language_id`)
INSERT IGNORE INTO `cfr_product_description` (`product_id`, `language_id`, `name`, `description`, `sub_description`, `seo_title`, `seo_description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(104, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD01', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(105, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD02', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(106, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD03', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(107, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD04', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(108, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD05', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(109, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD06', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(110, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD07', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(111, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD08', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(112, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD09', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(113, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD10', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(114, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD11', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(115, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD12', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(116, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD13', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(117, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD14', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(118, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD15', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(119, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD16', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(120, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD17', '&lt;p&gt;- Váy suông ren là item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. - Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.&lt;/p&gt;', '', '', '', '', '', '', ''),
(121, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD18', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(122, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD19', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(123, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD20', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(124, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD21', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(125, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD22', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(126, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD23', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(127, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD24', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(128, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD25', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(129, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD26', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(130, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD27', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', ''),
(131, 2, 'Váy đầm dự tiệc sang trọng - Độc đáo - Giá cả hợp lý VD28', ' - Váy suông ren là  item chưa bao giờ hết hot, mang đến vẻ quyến rũ cho phái đẹp và đặc biệt giúp bạn che đi khuyết điểm cơ thể mình. \n- Chất liệu ren gợi cảm, sang trọng mang lại cho quý cô phong cách thời trang thời thượng và thanh lịch.', '', '', '', '', '', '', '');

-- cfr_product_image PRIMARY KEY (`product_image_id`)
-- nothing

-- cfr_product_to_category PRIMARY KEY (`product_id`,`category_id`)
INSERT IGNORE INTO `cfr_product_to_category` (`product_id`, `category_id`) VALUES
(104, 21),
(105, 22),
(106, 23),
(107, 24),
(108, 25),
(109, 26),
(110, 27),
(111, 21),
(112, 22),
(113, 23),
(114, 24),
(115, 25),
(116, 26),
(117, 27),
(118, 21),
(119, 22),
(120, 21),
(120, 23),
(120, 24),
(120, 27),
(121, 24),
(121, 27),
(122, 25),
(122, 27),
(123, 26),
(124, 23),
(124, 27),
(125, 23),
(125, 27),
(126, 23),
(126, 27),
(127, 23),
(127, 27),
(128, 23),
(128, 27),
(129, 23),
(129, 27),
(130, 23),
(130, 27),
(131, 23),
(131, 27);

-- cfr_product_to_store PRIMARY KEY (`product_id`,`store_id`)
INSERT IGNORE INTO `cfr_product_to_store` (`product_id`, `store_id`, `product_version_id`, `quantity`, `cost_price`) VALUES
(104, 0, 0, 50, '0.0000'),
(105, 0, 0, 50, '0.0000'),
(106, 0, 0, 50, '0.0000'),
(107, 0, 0, 50, '0.0000'),
(108, 0, 0, 50, '0.0000'),
(109, 0, 0, 50, '0.0000'),
(110, 0, 0, 50, '0.0000'),
(111, 0, 0, 50, '0.0000'),
(112, 0, 0, 50, '0.0000'),
(113, 0, 0, 49, '0.0000'),
(114, 0, 0, 50, '0.0000'),
(115, 0, 0, 50, '0.0000'),
(116, 0, 0, 50, '0.0000'),
(117, 0, 0, 50, '0.0000'),
(118, 0, 0, 50, '0.0000'),
(119, 0, 0, 50, '0.0000'),
(120, 0, 0, 50, '0.0000'),
(121, 0, 0, 50, '0.0000'),
(122, 0, 0, 50, '0.0000'),
(123, 0, 0, 50, '0.0000'),
(124, 0, 0, 50, '0.0000'),
(125, 0, 0, 49, '0.0000'),
(126, 0, 0, 50, '0.0000'),
(127, 0, 0, 50, '0.0000'),
(128, 0, 0, 50, '0.0000'),
(129, 0, 0, 50, '0.0000'),
(130, 0, 0, 50, '0.0000'),
(131, 0, 0, 50, '0.0000');

-- cfr_seo_url PRIMARY KEY (`seo_url_id`)
INSERT IGNORE INTO `cfr_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`, `table_name`) VALUES
(2083, 0, 2, 'category_id=12', 'd-cat-giay-co-dien', 'category'),
(2082, 0, 2, 'product_id=103', 'd-prod-giay-dup-cao-11cm', 'product'),
(2081, 0, 1, 'product_id=103', 'd-prod-giay-dup-cao-11cm', 'product'),
(2080, 0, 2, 'product_id=99', 'd-prod-giay-sandal-chien-binh-day-keo-1-ben', 'product'),
(2079, 0, 1, 'product_id=99', 'd-prod-giay-sandal-chien-binh-day-keo-1-ben', 'product'),
(2078, 0, 2, 'product_id=98', 'd-prod-dep-sandal-xoan-co-chan', 'product'),
(2077, 0, 1, 'product_id=98', 'd-prod-dep-sandal-xoan-co-chan', 'product'),
(2076, 0, 2, 'product_id=97', 'd-prod-giay-sandal-quai-ngang-anh-7-mau-1', 'product'),
(2075, 0, 1, 'product_id=97', 'd-prod-giay-sandal-quai-ngang-anh-7-mau-1', 'product'),
(2074, 0, 2, 'product_id=88', 'd-prod-giay-sandal-cao-got-quai-trong-got-kim-sa-size-34-den-40', 'product'),
(2073, 0, 1, 'product_id=88', 'd-prod-giay-sandal-cao-got-quai-trong-got-kim-sa-size-34-den-40', 'product'),
(2072, 0, 2, 'product_id=87', 'd-prod-giay-valen-vien-dinh-size-36-37-38', 'product'),
(2071, 0, 1, 'product_id=87', 'd-prod-giay-valen-vien-dinh-size-36-37-38', 'product'),
(2070, 0, 2, 'product_id=86', 'd-prod-giay-cao-got-dep-den-no', 'product'),
(2069, 0, 1, 'product_id=86', 'd-prod-giay-cao-got-dep-den-no', 'product'),
(2068, 0, 2, 'product_id=85', 'd-prod-giay-cao-got-quai-trong-phoi-dinh', 'product'),
(2067, 0, 1, 'product_id=85', 'd-prod-giay-cao-got-quai-trong-phoi-dinh', 'product'),
(2066, 0, 2, 'product_id=84', 'd-prod-giay-cao-got-got-kim-sa-xu-doc-la', 'product'),
(2065, 0, 1, 'product_id=84', 'd-prod-giay-cao-got-got-kim-sa-xu-doc-la', 'product'),
(2064, 0, 2, 'product_id=83', 'd-prod-giay-co-dien-sang-trong-mau-hong', 'product'),
(2063, 0, 1, 'product_id=83', 'd-prod-giay-co-dien-sang-trong-mau-hong', 'product'),
(2193, 0, 1, 'menu_id=6', 'menu-bo-suu-tap', ''),
(2192, 0, 2, 'menu_id=6', 'menu-bo-suu-tap', ''),
(2189, 0, 2, 'collection=8', 'bst-tong-hop', 'collection'),
(1000, 0, 1, 'page_id=1000', 'chinh-sach-va-bao-mat', ''),
(1001, 0, 2, 'page_id=1000', 'chinh-sach-va-bao-mat', ''),
(2093, 0, 1, 'common/home', 'home', ''),
(2094, 0, 2, 'common/home', 'trang-chu', ''),
(2095, 0, 1, 'common/shop', 'products', ''),
(2096, 0, 2, 'common/shop', 'san-pham', ''),
(2097, 0, 1, 'contact/contact', 'contact', ''),
(2098, 0, 2, 'contact/contact', 'lien-he', ''),
(2099, 0, 1, 'checkout/profile', 'profile', ''),
(2100, 0, 2, 'checkout/profile', 'tai-khoan', ''),
(2101, 0, 1, 'account/login', 'login', ''),
(2102, 0, 2, 'account/login', 'dang-nhap', ''),
(2103, 0, 1, 'account/register', 'register', ''),
(2104, 0, 2, 'account/register', 'dang-ky', ''),
(2105, 0, 1, 'account/logout', 'logout', ''),
(2106, 0, 2, 'account/logout', 'dang-xuat', ''),
(2107, 0, 1, 'checkout/setting', 'setting', ''),
(2108, 0, 2, 'checkout/setting', 'cai-dat', ''),
(2109, 0, 1, 'checkout/my_orders', 'checkout-cart', ''),
(2110, 0, 2, 'checkout/my_orders', 'gio-hang', ''),
(2111, 0, 1, 'checkout/order_preview', 'payment', ''),
(2112, 0, 2, 'checkout/order_preview', 'thanh-toan', ''),
(2113, 0, 1, 'checkout/order_preview/orderSuccess', 'payment-success', ''),
(2114, 0, 2, 'checkout/order_preview/orderSuccess', 'dat-hang-thanh-cong', ''),
(2115, 0, 1, 'blog/blog', 'blog', ''),
(2116, 0, 2, 'blog/blog', 'bai-viet', ''),
(2117, 0, 2, 'manufacturer_id=26', '', 'manufacturer'),
(2118, 0, 1, 'manufacturer_id=26', '', 'manufacturer'),
(2119, 0, 2, 'category_id=21', 'vay-du-tiec', 'category'),
(2120, 0, 1, 'category_id=21', 'vay-du-tiec', 'category'),
(2121, 0, 2, 'product_id=104', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd01', 'product'),
(2122, 0, 1, 'product_id=104', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd01', 'product'),
(2123, 0, 2, 'category_id=22', 'vay-dam-xoe', 'category'),
(2124, 0, 1, 'category_id=22', 'vay-dam-xoe', 'category'),
(2125, 0, 2, 'product_id=105', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd02', 'product'),
(2126, 0, 1, 'product_id=105', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd02', 'product'),
(2196, 0, 2, 'category_id=23', 'vay-dang-suong-1', 'category'),
(2129, 0, 2, 'product_id=106', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd03', 'product'),
(2130, 0, 1, 'product_id=106', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd03', 'product'),
(2131, 0, 2, 'category_id=24', 'vay-dam-so-mi', 'category'),
(2132, 0, 1, 'category_id=24', 'vay-dam-so-mi', 'category'),
(2133, 0, 2, 'product_id=107', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd04', 'product'),
(2134, 0, 1, 'product_id=107', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd04', 'product'),
(2135, 0, 2, 'category_id=25', 'vay-dam-xoe-hoa-tiet', 'category'),
(2136, 0, 1, 'category_id=25', 'vay-dam-xoe-hoa-tiet', 'category'),
(2137, 0, 2, 'product_id=108', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd05', 'product'),
(2138, 0, 1, 'product_id=108', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd05', 'product'),
(2139, 0, 2, 'category_id=26', 'vay-dam-lua-duoi-ca', 'category'),
(2140, 0, 1, 'category_id=26', 'vay-dam-lua-duoi-ca', 'category'),
(2141, 0, 2, 'product_id=109', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd06', 'product'),
(2142, 0, 1, 'product_id=109', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd06', 'product'),
(2143, 0, 2, 'category_id=27', 'vay-dam-cong-so', 'category'),
(2144, 0, 1, 'category_id=27', 'vay-dam-cong-so', 'category'),
(2145, 0, 2, 'product_id=110', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd07', 'product'),
(2146, 0, 1, 'product_id=110', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd07', 'product'),
(2147, 0, 2, 'product_id=111', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd08', 'product'),
(2148, 0, 1, 'product_id=111', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd08', 'product'),
(2149, 0, 2, 'product_id=112', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd09', 'product'),
(2150, 0, 1, 'product_id=112', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd09', 'product'),
(2151, 0, 2, 'product_id=113', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd10', 'product'),
(2152, 0, 1, 'product_id=113', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd10', 'product'),
(2153, 0, 2, 'product_id=114', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd11', 'product'),
(2154, 0, 1, 'product_id=114', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd11', 'product'),
(2155, 0, 2, 'product_id=115', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd12', 'product'),
(2156, 0, 1, 'product_id=115', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd12', 'product'),
(2157, 0, 2, 'product_id=116', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd13', 'product'),
(2158, 0, 1, 'product_id=116', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd13', 'product'),
(2159, 0, 2, 'product_id=117', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd14', 'product'),
(2160, 0, 1, 'product_id=117', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd14', 'product'),
(2161, 0, 2, 'product_id=118', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd15', 'product'),
(2162, 0, 1, 'product_id=118', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd15', 'product'),
(2163, 0, 2, 'product_id=119', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd16', 'product'),
(2164, 0, 1, 'product_id=119', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd16', 'product'),
(2201, 0, 1, 'product_id=120', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd17', 'product'),
(2200, 0, 2, 'product_id=120', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd17', 'product'),
(2167, 0, 2, 'product_id=121', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd18', 'product'),
(2168, 0, 1, 'product_id=121', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd18', 'product'),
(2169, 0, 2, 'product_id=122', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd19', 'product'),
(2170, 0, 1, 'product_id=122', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd19', 'product'),
(2171, 0, 2, 'product_id=123', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd20', 'product'),
(2172, 0, 1, 'product_id=123', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd20', 'product'),
(2173, 0, 2, 'product_id=124', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd21', 'product'),
(2174, 0, 1, 'product_id=124', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd21', 'product'),
(2175, 0, 2, 'product_id=125', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd22', 'product'),
(2176, 0, 1, 'product_id=125', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd22', 'product'),
(2177, 0, 2, 'product_id=126', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd23', 'product'),
(2178, 0, 1, 'product_id=126', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd23', 'product'),
(2179, 0, 2, 'product_id=127', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd24', 'product'),
(2180, 0, 1, 'product_id=127', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd24', 'product'),
(2181, 0, 2, 'product_id=128', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd25', 'product'),
(2182, 0, 1, 'product_id=128', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd25', 'product'),
(2183, 0, 2, 'product_id=129', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd26', 'product'),
(2184, 0, 1, 'product_id=129', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd26', 'product'),
(2185, 0, 2, 'product_id=130', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd27', 'product'),
(2186, 0, 1, 'product_id=130', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd27', 'product'),
(2187, 0, 2, 'product_id=131', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd28', 'product'),
(2188, 0, 1, 'product_id=131', 'vay-dam-du-tiec-sang-trong-doc-dao-gia-ca-hop-ly-vd28', 'product'),
(2084, 0, 2, 'category_id=14', 'd-cat-giay-cao-got', 'category'),
(2085, 0, 2, 'category_id=17', 'd-cat-giay-valen', 'category'),
(2086, 0, 2, 'category_id=18', 'd-cat-giay-sandal', 'category'),
(2087, 0, 2, 'category_id=19', 'd-cat-dep-sandal', 'category'),
(2194, 0, 2, 'category_id=20', 'giay-bup-be', 'category'),
(2089, 0, 2, 'collection=4', 'd-col-san-pham-khuyen-mai', 'category'),
(2090, 0, 2, 'collection=5', 'd-col-san-pham-hot', 'category'),
(2091, 0, 2, 'collection=6', 'd-col-san-pham-moi', 'category'),
(2092, 0, 2, 'collection=7', 'd-col-hang-ngay', 'category'),
(2195, 0, 1, 'category_id=20', 'giay-bup-be', 'category'),
(2197, 0, 1, 'category_id=23', 'vay-dang-suong-1', 'category'),
(2198, 0, 2, 'category_id=28', 'vay-cong-so', 'category'),
(2199, 0, 1, 'category_id=28', 'vay-cong-so', 'category'),
(2202, 0, 2, 'blog=1', '7-dang-vay-maxi-hop-nhat-cho-cac-nang-chan-ngan', 'blog'),
(2203, 0, 2, 'blog=2', '7-phong-cach-thoi-trang-co-ban-nhat', 'blog'),
(2204, 0, 2, 'blog=3', '4-cach-mac-dep-voi-chan-vay-suong-cho-moi-dang-nguoi', 'blog');

-- cfr_theme_builder_config NO PRIMARY KEY
-- remove first
DELETE FROM `cfr_theme_builder_config`
WHERE `store_id` = '0'
  AND `config_theme` = 'fashion_luvibee_2'
  AND `code` = 'config';

-- insert
INSERT IGNORE INTO `cfr_theme_builder_config` (`store_id`, `config_theme`, `code`, `key`, `config`, `date_added`, `date_modified`) VALUES
(0, 'fashion_luvibee_2', 'config', 'config_theme_color', '{\n    \"main\": {\n        \"main\": {\n            \"hex\": \"F8BD2F\",\n            \"visible\": \"1\"\n        },\n        \"button\": {\n            \"hex\": \"F8BD2F\",\n            \"visible\": \"1\"\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-08-21 09:33:32'),
(0, 'fashion_luvibee_2', 'config', 'config_theme_text', '{\n    \"all\": {\n        \"title\": \"Font Roboto\",\n        \"font\": \"\'Roboto\', sans-serif\",\n        \"supported-fonts\": [\n            \"Open Sans\",\n            \"IBM Plex Sans\",\n            \"Courier New\",\n            \"\'Roboto\', sans-serif\",\n            \"Nunito\",\n            \"Arial\",\n            \"DejaVu Sans\",\n            \"Tahoma\",\n            \"Time News Roman\"\n        ]\n    },\n    \"heading\": {\n        \"font\": \"Tahoma\",\n        \"supported-fonts\": [\n            \"Open Sans\",\n            \"IBM Plex Sans\",\n            \"Courier New\",\n            \"Roboto\",\n            \"Nunito\",\n            \"Arial\",\n            \"DejaVu Sans\",\n            \"Tahoma\",\n            \"Time News Roman\"\n        ],\n        \"font-type\": \"regular\",\n        \"supported-font-types\": [\n            \"regular\",\n            \"bold\",\n            \"italic\"\n        ],\n        \"font-size\": 32\n    },\n    \"body\": {\n        \"font\": \"Aria\",\n        \"supported-fonts\": [\n            \"Open Sans\",\n            \"IBM Plex Sans\",\n            \"Courier New\",\n            \"Roboto\",\n            \"Nunito\",\n            \"Arial\",\n            \"DejaVu Sans\",\n            \"Tahoma\",\n            \"Time News Roman\"\n        ],\n        \"font-type\": \"regular\",\n        \"supported-font-types\": [\n            \"regular\",\n            \"bold\",\n            \"italic\"\n        ],\n        \"font-size\": 14\n    }\n}', '2020-06-09 18:00:35', '2020-08-21 09:33:32'),
(0, 'fashion_luvibee_2', 'config', 'config_theme_favicon', '{\n    \"image\": {\n        \"url\": \"https:\\/\\/cdn.bestme.asia\\/images\\/x2\\/logo-lubie_aOdIjT9.png\"\n    }\n}', '2020-06-09 18:00:35', '2020-08-21 09:33:32'),
(0, 'fashion_luvibee_2', 'config', 'config_theme_social', '[\n    {\n        \"name\": \"facebook\",\n        \"text\": \"Facebook\",\n        \"url\": \"https:\\/\\/www.facebook.com\"\n    },\n    {\n        \"name\": \"twitter\",\n        \"text\": \"Twitter\",\n        \"url\": \"https:\\/\\/www.twitter.com\"\n    },\n    {\n        \"name\": \"instagram\",\n        \"text\": \"Instagram\",\n        \"url\": \"https:\\/\\/www.instagram.com\"\n    },\n    {\n        \"name\": \"tumblr\",\n        \"text\": \"Tumblr\",\n        \"url\": \"https:\\/\\/www.tumblr.com\"\n    },\n    {\n        \"name\": \"youtube\",\n        \"text\": \"Youtube\",\n        \"url\": \"https:\\/\\/www.youtube.com\"\n    },\n    {\n        \"name\": \"googleplus\",\n        \"text\": \"Google Plus\",\n        \"url\": \"https:\\/\\/www.plus.google.com\"\n    }\n]', '2020-06-09 18:00:35', '2020-08-21 09:33:32'),
(0, 'fashion_luvibee_2', 'config', 'config_theme_override_css', '{\n    \"css\": \".header-default {\\n    padding: 10px 0px;\\n    background-color: #FFCE59 !important;\\n}\\n.mt-lg-5, .my-lg-5 {\\n    margin-top: 1rem!important;\\n}\\n.navbar-light .navbar-nav .nav-link.active, .navbar-light .navbar-nav .nav-link:hover {\\n    color: #FF8C00;\\n}\\n.text-primary {\\n    color: #FF8C00!important;\\n}\\n.mb-md-5, .my-md-5 {\\n    margin-bottom: 1rem!important;\\n}\\n.block-footer {\\n    padding: 15px 0;\\n}\\n.mt-5, .my-5 {\\n    margin-top: 1rem!important;\\n}\\n.mb-5, .my-5 {\\n    margin-bottom: 1rem!important;\\n}\\n.pt-md-5, .py-md-5 {\\n    padding-top: 1rem!important;\\n}\\n.pb-md-5, .py-md-5 {\\n    padding-bottom: 0rem!important;\\n}\\n.mt-md-5, .my-md-5 {\\n    margin-top: 0rem!important;\\n}\"\n}', '2020-06-09 18:00:35', '2020-08-21 09:33:32'),
(0, 'fashion_luvibee_2', 'config', 'config_theme_checkout_page', '{\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(0, 'fashion_luvibee_2', 'config', 'config_section_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"header\": {\n            \"name\": \"header\",\n            \"text\": \"Header\",\n            \"visible\": 1\n        },\n        \"slide-show\": {\n            \"name\": \"slide-show\",\n            \"text\": \"Slideshow\",\n            \"visible\": 1\n        },\n        \"categories\": {\n            \"name\": \"categories\",\n            \"text\": \"Danh mục sản phẩm\",\n            \"visible\": 1\n        },\n        \"hot-deals\": {\n            \"name\": \"hot-deals\",\n            \"text\": \"Sản phẩm khuyến mại\",\n            \"visible\": 1\n        },\n        \"feature-products\": {\n            \"name\": \"feature-products\",\n            \"text\": \"Sản phẩm bán chạy\",\n            \"visible\": 1\n        },\n        \"related-products\": {\n            \"name\": \"related-products\",\n            \"text\": \"Sản phẩm xem nhiều\",\n            \"visible\": 1\n        },\n        \"new-products\": {\n            \"name\": \"new-products\",\n            \"text\": \"Sản phẩm mới\",\n            \"visible\": 1\n        },\n        \"product-detail\": {\n            \"name\": \"product-details\",\n            \"text\": \"Chi tiết sản phẩm\",\n            \"visible\": 1\n        },\n        \"banner\": {\n            \"name\": \"banner\",\n            \"text\": \"Banner\",\n            \"visible\": 1\n        },\n        \"content_customize\" : {\n            \"name\" : \"content-customize\",\n            \"text\" : \"Nội dung tùy chỉnh\",\n            \"visible\" : 1\n        },\n        \"partners\": {\n            \"name\": \"partners\",\n            \"text\": \"Đối tác\",\n            \"visible\": 1\n        },\n        \"blog\": {\n            \"name\": \"blog\",\n            \"text\": \"Blog\",\n            \"visible\": 1\n        },\n        \"footer\": {\n            \"name\": \"footer\",\n            \"text\": \"Footer\",\n            \"visible\": 1\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(0, 'fashion_luvibee_2', 'config', 'config_section_banner', '{\n    \"name\": \"banner\",\n    \"text\": \"Banner\",\n    \"visible\": \"1\",\n    \"display\": [\n        {\n            \"image-url\": \"https:\\/\\/cdn.bestme.asia\\/images\\/x2\\/banner1b_J5efdLS.jpg\",\n            \"url\": \"https:\\/\\/fashionluvibee.bestme.asia\\/san-pham\",\n            \"description\": \"Banner 1\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/cdn.bestme.asia\\/images\\/x2\\/banner1a_2OGO0zJ.jpg\",\n            \"url\": \"https:\\/\\/fashionluvibee.bestme.asia\\/san-pham\",\n            \"description\": \"Banner 2\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/cdn.bestme.asia\\/images\\/x2\\/slider128159797716129_bbkjlBN.jpg\",\n            \"url\": \"https:\\/\\/fashionluvibee.bestme.asia\\/san-pham\",\n            \"description\": \"Banner 3\",\n            \"visible\": \"1\"\n        }\n    ]\n}', '2020-06-09 18:00:35', '2020-08-21 09:32:52'),
(0, 'fashion_luvibee_2', 'config', 'config_section_best_sales_product', '{\n    \"name\": \"feature-product\",\n    \"text\": \"Sản phẩm bán chạy\",\n    \"visible\": \"1\",\n    \"setting\": {\n        \"title\": \"Sản phẩm bán chạy\",\n        \"sub_title\": \"Mô tả tiêu đề\",\n        \"auto_retrieve_data\": \"0\",\n        \"collection_id\": \"8\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": \"4\"\n        },\n        \"grid_mobile\": {\n            \"quantity\": \"2\"\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 19:38:41'),
(0, 'fashion_luvibee_2', 'config', 'config_section_new_product', '{\n    \"name\": \"new-product\",\n    \"text\": \"Sản phẩm mới\",\n    \"visible\": \"1\",\n    \"setting\": {\n        \"title\": \"Sản phẩm mới\",\n        \"sub_title\": \"Mô tả tiêu đề\",\n        \"auto_retrieve_data\": \"0\",\n        \"collection_id\": \"8\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": \"4\"\n        },\n        \"grid_mobile\": {\n            \"quantity\": \"2\"\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 19:38:56'),
(0, 'fashion_luvibee_2', 'config', 'config_section_best_views_product', '{\n    \"name\": \"related-product\",\n    \"text\": \"Sản phẩm xem nhiều\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Sản phẩm xem nhiều\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 10\n        },\n        \"grid_mobile\": {\n            \"quantity\": 2\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(0, 'fashion_luvibee_2', 'config', 'config_section_blog', '{\n    \"name\": \"blog\",\n    \"text\": \"Blog\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Blog\"\n    },\n    \"display\": {\n        \"menu\": [\n            {\n                \"type\": \"existing\",\n                \"menu\": {\n                    \"id\": 12,\n                    \"name\": \"Danh sách bài viết 1\"\n                }\n            },\n            {\n                \"type\": \"manual\",\n                \"entries\": [\n                    {\n                        \"id\": 10,\n                        \"name\": \"Entry 10\"\n                    },\n                    {\n                        \"id\": 11,\n                        \"name\": \"Entry 11\"\n                    }\n                ]\n            }\n        ],\n        \"grid\": {\n            \"quantity\": 10\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(0, 'fashion_luvibee_2', 'config', 'config_section_detail_product', '{\n    \"name\": \"product-detail\",\n    \"text\": \"Chi tiết sản phẩm\",\n    \"visible\": 1,\n    \"display\": {\n        \"name\": true,\n        \"description\": false,\n        \"price\": true,\n        \"price-compare\": false,\n        \"status\": false,\n        \"sale\": false,\n        \"rate\": false\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(0, 'fashion_luvibee_2', 'config', 'config_section_footer', '{\n    \"name\": \"footer\",\n    \"text\": \"Footer\",\n    \"visible\": 1,\n    \"contact\": {\n        \"title\": \"Liên hệ chúng tôi\",\n        \"address\": \"Cau Giay - Ha Noi\",\n        \"phone-number\": \"(+84)987654321\",\n        \"email\": \"dungbt@novaon.vn\",\n        \"visible\": 1\n    },\n    \"collection\": {\n        \"title\": \"Bộ sưu tập\",\n        \"menu_id\": \"6\",\n        \"visible\": 1\n    },\n    \"quick-links\": {\n        \"title\": \"Liên kết nhanh\",\n        \"menu_id\": 1,\n        \"visible\": 1\n    },\n    \"subscribe\": {\n        \"title\": \"Đăng ký theo dõi\",\n        \"social_network\": 1,\n        \"visible\": 1,\n        \"youtube_visible\": 1,\n        \"facebook_visible\": 1,\n        \"instagram_visible\": 1\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 20:09:27'),
(0, 'fashion_luvibee_2', 'config', 'config_section_header', '{\n    \"name\": \"header\",\n    \"text\": \"Header\",\n    \"visible\": \"1\",\n    \"notify-bar\": {\n        \"name\": \"Thanh thông báo\",\n        \"visible\": \"1\",\n        \"content\": \"Mua sắm online thuận tiện và dễ dàng\",\n        \"url\": \"#\"\n    },\n    \"logo\": {\n        \"name\": \"Logo\",\n        \"url\": \"https:\\/\\/cdn.bestme.asia\\/images\\/x2\\/logo-lubie_aOdIjT9.png\",\n        \"height\": \"\"\n    },\n    \"menu\": {\n        \"name\": \"Menu\",\n        \"display-list\": {\n            \"name\": \"Menu chính\",\n            \"id\": \"1\"\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 19:40:02'),
(0, 'fashion_luvibee_2', 'config', 'config_section_hot_product', '{\n    \"name\": \"hot-deals\",\n    \"text\": \"Sản phẩm khuyến mại\",\n    \"visible\": \"1\",\n    \"setting\": {\n        \"title\": \"Sản phẩm khuyến mại\",\n        \"sub_title\": \"Mô tả tiêu đề\",\n        \"auto_retrieve_data\": \"0\",\n        \"collection_id\": \"8\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": \"4\"\n        },\n        \"grid_mobile\": {\n            \"quantity\": \"2\"\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 19:38:32'),
(0, 'fashion_luvibee_2', 'config', 'config_section_list_product', '{\n    \"name\": \"categories\",\n    \"text\": \"Danh mục sản phẩm\",\n    \"visible\": 0,\n    \"setting\": {\n        \"title\": \"Danh mục sản phẩm\"\n    },\n    \"display\": {\n        \"menu\": {\n            \"id\": 1,\n            \"name\": \"Danh mục sản phẩm 1\"\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(0, 'fashion_luvibee_2', 'config', 'config_section_partner', '{\n    \"name\": \"partners\",\n    \"text\": \"Đối tác\",\n    \"visible\": \"1\",\n    \"limit-per-line\": \"4\",\n    \"display\": [\n        {\n            \"image-url\": \"https:\\/\\/cdn.bestme.asia\\/images\\/x2\\/logo-4-1_QZZnBnD.png\",\n            \"url\": \"https:\\/\\/fashionluvibee.bestme.asia\\/san-pham\",\n            \"description\": \"Partner 1\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/cdn.bestme.asia\\/images\\/x2\\/logo-3_4HCIyoT.png\",\n            \"url\": \"https:\\/\\/fashionluvibee.bestme.asia\\/san-pham\",\n            \"description\": \"Partner 2\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/cdn.bestme.asia\\/images\\/x2\\/logo-1_TSQ1BS7.png\",\n            \"url\": \"https:\\/\\/fashionluvibee.bestme.asia\\/san-pham\",\n            \"description\": \"Partner 3\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/cdn.bestme.asia\\/images\\/x2\\/1a_RF68Xfw.png\",\n            \"url\": \"https:\\/\\/fashionluvibee.bestme.asia\\/san-pham\",\n            \"description\": \"Partner 4\",\n            \"visible\": \"1\"\n        }\n    ]\n}', '2020-06-09 18:00:35', '2020-06-09 18:02:51'),
(0, 'fashion_luvibee_2', 'config', 'config_section_slideshow', '{\n    \"name\": \"slide-show\",\n    \"text\": \"Slideshow\",\n    \"visible\": \"1\",\n    \"setting\": {\n        \"transition-time\": \"5\"\n    },\n    \"display\": [\n        {\n            \"image-url\": \"https:\\/\\/cdn.bestme.asia\\/images\\/x2\\/slider1_XZb5htY.jpg\",\n            \"url\": \"https:\\/\\/fashionluvibee.bestme.asia\\/san-pham\",\n            \"description\": \"\",\n            \"id\": \"11179844\"\n        },\n        {\n            \"image-url\": \"https:\\/\\/cdn.bestme.asia\\/images\\/x2\\/slider2_p84HgeJ.jpg\",\n            \"url\": \"https:\\/\\/fashionluvibee.bestme.asia\\/san-pham\",\n            \"description\": \"\",\n            \"id\": \"18094842\"\n        }\n    ]\n}', '2020-06-09 18:00:35', '2020-08-06 14:43:49'),
(0, 'fashion_luvibee_2', 'config', 'config_section_product_groups', '{\n    \"name\": \"group products\",\n    \"text\": \"nhóm sản phẩm\",\n    \"visible\": 1,\n    \"list\": [\n        {\n            \"text\": \"Danh sách sản phẩm 1\",\n            \"visible\": \"0\",\n            \"setting\": {\n                \"title\": \"Danh sách sản phẩm 1\",\n                \"collection_id\": \"4\",\n                \"sub_title\": \"Danh sách sản phẩm 1\"\n            },\n            \"display\": {\n                \"grid\": {\n                    \"quantity\": \"6\"\n                },\n                \"grid_mobile\": {\n                    \"quantity\": 2\n                }\n            }\n        }\n    ]\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(0, 'fashion_luvibee_2', 'config', 'config_section_content_customize', '{\n    \"name\": \"content-customize\",\n    \"title\": \"Nội dung tuỳ chỉnh\",\n    \"display\": [\n        {\n            \"name\": \"content-1\",\n            \"title\": \"Bảo đảm chất lượng\",\n            \"type\": \"fixed\",\n            \"content\": {\n                \"icon\": \"/catalog/view/theme/default/image/favicon.ico\",\n                \"title\": \"Bảo đảm chất lượng\",\n                \"description\": \"Sản phẩm đảm bảo chất lượng\",\n                \"html\": \"\"\n            }\n        },\n        {\n            \"name\": \"content-2\",\n            \"title\": \"Miễn phí giao hàng\",\n            \"type\": \"fixed\",\n            \"content\": {\n                \"icon\": \"/catalog/view/theme/default/image/favicon.ico\",\n                \"title\": \"Miễn phí giao hàng\",\n                \"description\": \"Cho đơn hàng từ 2 triệu\",\n                \"html\": \"\"\n            }\n        },\n        {\n            \"name\": \"content-3\",\n            \"title\": \"Hỗ trợ 24/7\",\n            \"type\": \"fixed\",\n            \"content\": {\n                \"icon\": \"/catalog/view/theme/default/image/favicon.ico\",\n                \"title\": \"Hỗ trợ 24/7\",\n                \"description\": \"Hotline 012.345.678\",\n                \"html\": \"\"\n            }\n        },\n        {\n            \"name\": \"content-4\",\n            \"title\": \"Đổi trả hàng\",\n            \"type\": \"fixed\",\n            \"content\": {\n                \"icon\": \"/catalog/view/theme/default/image/favicon.ico\",\n                \"title\": \"Đổi trả hàng\",\n                \"description\": \"Trong vòng 7 ngày\",\n                \"html\": \"\"\n            }\n        }\n    ]\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(0, 'fashion_luvibee_2', 'config', 'config_section_category_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"banner\": {\n            \"name\": \"banner\",\n            \"text\": \"Banner\",\n            \"visible\": 1\n        },\n        \"filter\": {\n            \"name\": \"filter\",\n            \"text\": \"Filter\",\n            \"visible\": 1\n        },\n        \"product_category\": {\n            \"name\": \"product_category\",\n            \"text\": \"Product Category\",\n            \"visible\": 1\n        },\n        \"product_list\": {\n            \"name\": \"product_list\",\n            \"text\": \"Product List\",\n            \"visible\": 1\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(0, 'fashion_luvibee_2', 'config', 'config_section_category_banner', '{\n    \"name\": \"banner\",\n    \"text\": \"Banner\",\n    \"visible\": \"1\",\n    \"display\": [\n        {\n            \"image-url\": \"https:\\/\\/cdn.bestme.asia\\/images\\/x2\\/1-1_Zt1U4oi.png\",\n            \"url\": \"#\",\n            \"description\": \"Banner 1\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"\\/catalog\\/view\\/theme\\/default\\/image\\/banner\\/img-banner-02.png\",\n            \"url\": \"#\",\n            \"description\": \"Banner 2\",\n            \"visible\": \"1\"\n        },\n        {\n            \"image-url\": \"\\/catalog\\/view\\/theme\\/default\\/image\\/banner\\/img-banner-03.png\",\n            \"url\": \"#\",\n            \"description\": \"Banner 3\",\n            \"visible\": \"1\"\n        }\n    ]\n}', '2020-06-09 18:00:35', '2020-08-21 09:56:05'),
(0, 'fashion_luvibee_2', 'config', 'config_section_category_filter', '{\n    \"name\": \"filter\",\n    \"text\": \"Bộ lọc\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Bộ lọc\"\n    },\n    \"display\": {\n        \"supplier\": {\n            \"title\": \"Nhà cung cấp\",\n            \"visible\": 1\n        },\n        \"product-type\": {\n            \"title\": \"Loại sản phẩm\",\n            \"visible\": 1\n        },\n        \"collection\": {\n            \"title\": \"Bộ sưu tập\",\n            \"visible\": 1\n        },\n        \"property\": {\n            \"title\": \"Lọc theo tt - k dung\",\n            \"visible\": 1,\n            \"prop\": [\n                \"all\",\n                \"color\",\n                \"weight\",\n                \"size\"\n            ]\n        },\n        \"product-price\": {\n            \"title\": \"Giá sản phẩm\",\n            \"visible\": 1,\n            \"range\": {\n                \"from\": 0,\n                \"to\": 100000000\n            }\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(0, 'fashion_luvibee_2', 'config', 'config_section_category_product_category', '{\n    \"name\": \"product-category\",\n    \"text\": \"Danh mục sản phẩm\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Danh mục sản phẩm\"\n    },\n    \"display\": {\n        \"menu\": {\n            \"id\": 1,\n            \"name\": \"Danh mục sản phẩm 1\"\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(0, 'fashion_luvibee_2', 'config', 'config_section_category_product_list', '{\n    \"name\": \"product-list\",\n    \"text\": \"Danh sách sản phẩm\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Danh sách sản phẩm\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 10\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(0, 'fashion_luvibee_2', 'config', 'config_section_product_detail_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"related_product\": {\n            \"name\": \"related_product\",\n            \"text\": \"Related Product\",\n            \"visible\": 1\n        },\n        \"template\": {\n            \"name\": \"template\",\n            \"text\": \"Template\",\n            \"visible\": 1\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(0, 'fashion_luvibee_2', 'config', 'config_section_product_detail_related_product', '{\n    \"name\": \"related-product\",\n    \"text\": \"Sản phẩm liên quan\",\n    \"visible\": \"1\",\n    \"setting\": {\n        \"title\": \"Sản phẩm liên quan\",\n        \"auto_retrieve_data\": \"1\",\n        \"collection_id\": \"1\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": \"4\"\n        },\n        \"grid_mobile\": {\n            \"quantity\": \"2\"\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 19:39:38'),
(0, 'fashion_luvibee_2', 'config', 'config_section_product_detail_template', '{\n    \"name\": \"template\",\n    \"text\": \"Giao diện\",\n    \"visible\": 1,\n    \"display\": {\n        \"template\": {\n            \"id\": 10\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(0, 'fashion_luvibee_2', 'config', 'config_section_blog_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"blog_category\": {\n            \"name\": \"blog_category\",\n            \"text\": \"Blog Category\",\n            \"visible\": 1\n        },\n        \"blog_list\": {\n            \"name\": \"blog_list\",\n            \"text\": \"Blog List\",\n            \"visible\": 1\n        },\n        \"latest_blog\": {\n            \"name\": \"latest_blog\",\n            \"text\": \"Latest Blog\",\n            \"visible\": 1\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(0, 'fashion_luvibee_2', 'config', 'config_section_blog_blog_category', '{\n    \"name\": \"blog-category\",\n    \"text\": \"Danh mục bài viết\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Danh mục bài viết\"\n    },\n    \"display\": {\n        \"menu\": {\n            \"id\": 1,\n            \"name\": \"Danh mục bài viết 1\"\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(0, 'fashion_luvibee_2', 'config', 'config_section_blog_blog_list', '{\n    \"name\": \"blog-list\",\n    \"text\": \"Danh sách bài viết\",\n    \"visible\": 1,\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 20\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(0, 'fashion_luvibee_2', 'config', 'config_section_blog_latest_blog', '{\n    \"name\": \"latest-blog\",\n    \"text\": \"Bài viết mới nhất\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Bài viết mới nhất\"\n    },\n    \"display\": {\n        \"grid\": {\n            \"quantity\": 10\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(0, 'fashion_luvibee_2', 'config', 'config_section_contact_sections', '{\n    \"name\": \"Theme Config\",\n    \"version\": \"1.0\",\n    \"section\": {\n        \"map\": {\n            \"name\": \"map\",\n            \"text\": \"Bản đồ\",\n            \"address\": \"<iframe src=\\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\\" width=\\\"600\\\" height=\\\"450\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\" allowfullscreen></iframe>\",\n            \"visible\": 1\n        },\n        \"info\": {\n            \"name\": \"info\",\n            \"text\": \"Thông tin cửa hàng\",\n            \"email\": \"contact-us@novaon.asia\",\n            \"phone\": \"0000000000\",\n            \"address\": \"address\",\n            \"visible\": 1\n        },\n        \"form\": {\n            \"name\": \"form\",\n            \"title\": \"Form liên hệ\",\n            \"email\": \"email@mail.com\",\n            \"visible\": 1\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(0, 'fashion_luvibee_2', 'config', 'config_section_contact_map', '{   //////// CÁI NÀY KHÔNG DÙNG NỮA\n    \"name\": \"map\",\n    \"text\": \"Bản đồ\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Bản đồ\"\n    },\n    \"display\": {\n        \"address\": \"<iframe src=\\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\\" width=\\\"600\\\" height=\\\"450\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\" allowfullscreen></iframe>\"\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(0, 'fashion_luvibee_2', 'config', 'config_section_contact_contact', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\n    \"name\": \"contact\",\n    \"text\": \"Liên hệ\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Vị trí của chúng tôi\"\n    },\n    \"display\": {\n        \"store\": {\n            \"title\": \"Gian hàng\",\n            \"content\": \"Số 12 Dịch Vọng Hậu, Cầu Giấy, Hà Nội\",\n            \"visible\": 1\n        },\n        \"telephone\": {\n            \"title\": \"Điện thoại\",\n            \"content\": \"(+84) 987 654 321\",\n            \"visible\": 1\n        },\n        \"social_follow\": {\n            \"socials\": [\n                {\n                    \"name\": \"facebook\",\n                    \"text\": \"Facebook\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"twitter\",\n                    \"text\": \"Twitter\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"instagram\",\n                    \"text\": \"Instagram\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"tumblr\",\n                    \"text\": \"Tumblr\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"youtube\",\n                    \"text\": \"Youtube\",\n                    \"visible\": 1\n                },\n                {\n                    \"name\": \"googleplus\",\n                    \"text\": \"Google Plus\",\n                    \"visible\": 1\n                }\n            ],\n            \"visible\": 1\n        }\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35'),
(0, 'fashion_luvibee_2', 'config', 'config_section_contact_form', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\n    \"name\": \"form\",\n    \"text\": \"Biểu mẫu\",\n    \"visible\": 1,\n    \"setting\": {\n        \"title\": \"Liên hệ với chúng tôi\"\n    },\n    \"data\": {\n        \"email\": \"sale.247@xshop.com\"\n    }\n}', '2020-06-09 18:00:35', '2020-06-09 18:00:35');

-- cfr_blog PRIMARY KEY (`blog_id`)
INSERT IGNORE INTO `cfr_blog` (`blog_id`, `author`, `status`, `date_publish`, `date_added`, `date_modified`) VALUES
(1, 1, 1, '2020-06-10 11:51:00', '2020-06-10 11:51:00', '2020-06-10 11:51:00'),
(2, 1, 1, '2020-06-10 11:55:33', '2020-06-10 11:55:33', '2020-06-10 11:55:33'),
(3, 1, 1, '2020-06-10 12:05:27', '2020-06-10 12:05:27', '2020-06-10 12:05:27');

-- cfr_blog_category PRIMARY KEY (`blog_category_id`)
INSERT IGNORE INTO `cfr_blog_category` (`blog_category_id`, `status`, `date_added`, `date_modified`) VALUES
(1, 1, '2019-11-11 13:37:51', '2019-11-11 19:03:46');

-- cfr_blog_category_description PRIMARY KEY (`blog_category_id`,`language_id`)
INSERT IGNORE INTO `cfr_blog_category_description` (`blog_category_id`, `language_id`, `title`, `meta_title`, `meta_description`, `alias`) VALUES
(1, 1, 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'danh-muc-mac-dinh-uncategorized'),
(1, 2, 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'danh-muc-mac-dinh-uncategorized');

-- cfr_blog_description PRIMARY KEY (`blog_id`,`language_id`)
INSERT IGNORE INTO `cfr_blog_description` (`blog_id`, `language_id`, `title`, `content`, `short_content`, `image`, `meta_title`, `meta_description`, `seo_keywords`, `alias`, `alt`, `type`, `video_url`) VALUES
(1, 2, '7 dáng váy maxi hợp nhất cho các nàng chân ngắn', '&lt;p&gt;&lt;meta charset=&quot;utf-8&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;h2 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;7 dáng váy maxi hợp nhất cho các nàng chân ngắn (Tiêu đề bài viết có thể là tiêu đề SEO - Heading1) &lt;/b&gt;&lt;/h2&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Mùa Xuân HÈ này, các nàng đừng quên bỏ qua những chiếc váy maxi tuyệt đẹp nhé. Đặc biệt là những nàng chân ngắn, tại sao không thử phong cách mới môt chút nhỉ?!  ...(bạn có thể dùng tên của hàng mình trong dấu..)đã chọn ra được 14 mẫu phong cách mà nàng có thể thích. Hãy tham khảo ngay dưới đây!  (Nội dung tóm tắt bài viết - chữ in nghiêng)&lt;/b&gt;&lt;br /&gt;\r\n &lt;/p&gt;\r\n\r\n&lt;h3 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;1 .Váy áo maxi họa tiết cùng giày đế bệt (Heading 2 - Tiêu đề 2 bôi đậm-  cơ chữ nhỏ hơn tiêu đề 1)&lt;/b&gt;&lt;/h3&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Một phong cách mới mẻ cùng chiếc váy áo maxi. Hơn thế nữa, đây có vẻ là chiếc váy dạng maxi hiếm hoi dành cho các cô nàng “chân ngắn” khi chỉ dài che mắt cá! Đây là chiếc váy dành cho cô nàng không thích đi giày cao khi hoàn toàn có thể kết hợp với giày đề bệt và Sneaker trẻ trung, cá tính.&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;&lt;img alt=&quot;áo váy maxi cho nàng 1m50&quot; height=&quot;307&quot; src=&quot;https://lh6.googleusercontent.com/cpWwiLVg_H4M4Wr0vD0O9FlbRW3J0daRV3UkLU5kNQjz0aOMixnV_kCLAqP3-KhAA551kbPu-7SdIVMsDO04bxz4YI8h-l9hgH79qaewTs4vkmANaF4DcYJf90brIxZoiMUwZKGb&quot; width=&quot;268&quot; /&gt;&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Váy hoạ tiết chấm bi dài cùng giày đế bệt (phải có tiêu đề ảnh, tiêu đề ảnh viết chữ nghiêng, cỡ chữ nhỏ hơn)&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;h3 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Lưu ý: trong mỗi mục có thể sẽ có nhiều ý nhỏ, những ý nhỏ được gọi là Heading 3 -Tiêu đề 3 bôi đậm và in nghiêng - cơ chữ nhỏ hơn tiêu đề 2 và 1) Ví dụ: Trong bài viết này, không có mục nhỏ hơn những nếu có thì heading 3 sẽ là 1.1 Váy áo maxi hoạ tiết hoa đỏ hay 1.2  Váy áo maxi hoạ tiết  chấm bi &lt;/b&gt;&lt;br /&gt;\r\n &lt;/h3&gt;\r\n\r\n&lt;h3 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;2. Váy maxi cổ vuông xẻ tà&lt;/b&gt;&lt;/h3&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;&lt;img alt=&quot;váy maxi cổ vuông chữ U cho nàng chân ngắn 1m50&quot; height=&quot;394&quot; src=&quot;https://lh5.googleusercontent.com/l92AF0OeHxHiFEpGtP31kGJIp5bMvfkg4TSw51Jg0pdzfIXnBSAM0iChHwQd6rdcKOP7ozMI_v9MwHtJUssAOciIN2gzjHJKdcwhIcxZEO478SJMGKQaPqsHmM4hcS99RGhJwUZJ&quot; width=&quot;309&quot; /&gt;&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;h3 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Váy maxi cổ vuông xẻ tà&lt;/b&gt;&lt;/h3&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Quyến rũ, quý phái cùng chiếc váy xẻ tà các nàng nhé! Váy maxi xẻ tà lộ một phần chân giúp các nàng nhỏ nhưng chân đẹp có thể phô ra điểm mạnh của mình!&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;h3 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt; &lt;/b&gt;&lt;/h3&gt;\r\n\r\n&lt;h3 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;3. Đầm maxi voan họa tiết hoa&lt;/b&gt;&lt;/h3&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Cổ chữ V cùng họa tiết hoa trung bình mang tới sự quý phái trong các bữa tiệc cho các nàng mà che hết đi được chiều cao khiêm tốn!&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt; &lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;&lt;img alt=&quot;đầm maxi voan họa tiết hoa cổ chữ V cho nàng chân ngắn&quot; height=&quot;451&quot; src=&quot;https://lh6.googleusercontent.com/mMILtOLBwt-66ri8TAOOZlrS5IV7lAVNoy2Lwx3hy_m2vhpkuIK7Nlp00SmCPW7X6SqiJdGWahWLQw17rhr-WrF4L7Wbtlk32F4DzXKxDryFpZj1Y5X6aeVhjlARexkeWyIGtZxt&quot; width=&quot;294&quot; /&gt;&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;h3 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Đầm maxi voan họa tiết hoa&lt;/b&gt;&lt;/h3&gt;\r\n\r\n&lt;h3 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt; &lt;/b&gt;&lt;/h3&gt;\r\n\r\n&lt;h3 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;4. Đầm Maxi hoa dáng chữ A chiết eo&lt;/b&gt;&lt;/h3&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Đầm chữ A chưa bao giờ là lỗi mốt, một chiếc váy chiết eo thì rất hoàn hảo cho các cô nàng “1m50”!&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;&lt;img alt=&quot;đầm chữ A chiết eo dành cho co nang 3m bẻ đôi&quot; height=&quot;314&quot; src=&quot;https://lh6.googleusercontent.com/FfOxL3HrWylYO_jCIqZ6AdUb_hqR2JFz7G7SHX7TW7KWrp6rA7maHH-XP2THJaxchWTVYoPs670NpGHMpvYMtDMOd3tAmU-ekFPZNAYOV_Gi9X9Gi1O28iYaaWFZSN51m0BnggKJ&quot; width=&quot;204&quot; /&gt;&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;h3 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Đầm Maxi hoa dáng chữ A chiết eo&lt;/b&gt;&lt;/h3&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;h3 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;5. Váy Maxi xòe chấm gót&lt;/b&gt;&lt;/h3&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Một chiếc váy xòe, tại sao không? Một chút khéo léo với phụ kiện và đôi giày cao gót thần thánh là nàng thỏa sức khoe dáng rồi!&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt; &lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;&lt;img alt=&quot;váy xòe maxi cho cô nàng thấp bé&quot; height=&quot;347&quot; src=&quot;https://lh3.googleusercontent.com/FIbtkbgEZ3T4WaVQeqmOtWf-2ce3xUudWQEofwUUWlRZJs5vWV0rNlihh5XwTPwG2riF_us5FYPBbjp56OB40MUMNm4HO9AdIkilnP25R8ztuQ4HWwjTw9kwEZdpc3QEvH7TDOBh&quot; width=&quot;228&quot; /&gt;&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;h3 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Váy Maxi xòe chấm gót&lt;/b&gt;&lt;/h3&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;h3 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;6. Váy Maxi dài tay basic quyến rũ&lt;/b&gt;&lt;/h3&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Tông màu hồng theo trend cùng tay dài, nàng vừa có thể mặc mùa đông với một chiếc jacket vừa có thể tự tin về dáng của mình!&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt; &lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;&lt;img alt=&quot;đầm dài tay maxi basic cho cô nàng 1m50&quot; height=&quot;352&quot; src=&quot;https://lh6.googleusercontent.com/p-8MIQf4czsAzgxgElrvIoBa9V47Vr894sunx4YltNhhtJKE1AwYf8Su53bIFQMEUkKIPVAFiuP5_EMocv-C8DjvZQxmUSpwoHAWGS5G_SzdzQfaKEJ-Mm4EHNN6eNJZ8qVfoU78&quot; width=&quot;275&quot; /&gt;&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;h3 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Váy Maxi dài tay basic quyến rũ&lt;/b&gt;&lt;/h3&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;h3 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;7. Đầm Maxi cổ cao cho cô nàng chân ngắn&lt;/b&gt;&lt;/h3&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Sexy hơn với chiếc cổ cao được xẻ rất sâu xuống trước cùng tà được xe phía dưới. Tự tin trong mỗi bữa tiệc mà nàng được mời!&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt; &lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;&lt;img alt=&quot;chiếc váy ôm maxi là hoàn hảo cho các co nàng chân ngắn&quot; height=&quot;428&quot; src=&quot;https://lh5.googleusercontent.com/iRHvoBx_bXJ2hJUX3loGL8TJ-2gmoUTWYXaiGkMNswXRATImg2hJhYWfO3RXkVpwBeXzOsmJ8EtW3RQlLYODxBy9dz80yKh2Crr2KYP1gikvDJkc7xB7TWa3jRQ8_39UIQcT2RcY&quot; width=&quot;323&quot; /&gt;&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;h3 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Đầm Maxi cổ cao cho cô nàng chân ngắn&lt;/b&gt;&lt;/h3&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Cho dù phom người của bạn có như thế nào đi chăng nữa thì Maxi là một trong nhiều loại váy đẹp đáng để bạn bổ sung cho gu ăn mặc của mình. Hãy chọn cho mình các sản phẩm phù hợp dựa trên những gợi ý trên đây để “diện” trong những ngày xuân hè sắp tới đây. Hãy ghé thăm &lt;a href=&quot;http://canifa.com/chan-vay-nu-cate.html&quot;&gt;bộ sưu tập váy đẹp&lt;/a&gt; từ thời trang CANIFA để bắt đầu shopping những mẫu váy đẹp nhất cho mình.&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Hãy tránh xa những sản phẩm được nhấn nhá quá nhiều ở phần chân váy và được sản xuất từ những loại vải cứng nhắc.&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt; &lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Lưu ý: Trong phần “Xem trước kết quả tìm kiếm&quot; bạn cần phải hoàn thành 3 mục sau:&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li dir=&quot;ltr&quot;&gt;\r\n	&lt;p dir=&quot;ltr&quot; role=&quot;presentation&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Thẻ tiêu đề: Tiêu đề bài viết, tên bài viết như 7 dáng váy maxi hợp nhất cho các nàng chân ngắn  (Không quá 70 ký tự bao gồm cả dấu phẩy)&lt;br /&gt;\r\n	&lt;a href=&quot;https://halink.vn/tienich/dem-ky-tu/&quot;&gt;https://halink.vn/tienich/dem-ky-tu/&lt;/a&gt;  (website giúp bạn đếm ký tự miễn phí)&lt;/b&gt;&lt;/p&gt;\r\n	&lt;/li&gt;\r\n	&lt;li dir=&quot;ltr&quot;&gt;\r\n	&lt;p dir=&quot;ltr&quot; role=&quot;presentation&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Nội dung bài viết: (Tóm tắt ý chính của bài, không quá 320 ký tự) Mùa Xuân HÈ này, các nàng đừng quên bỏ qua những chiếc váy maxi tuyệt đẹp nhé. Đặc biệt là những nàng chân ngắn, tại sao không thử phong cách mới môt chút nhỉ?!  ...(bạn có thể dùng tên của hàng mình trong dấu..)đã chọn ra được 14 mẫu phong cách mà nàng có thể thích. Hãy tham khảo ngay dưới đây!&lt;/b&gt;&lt;/p&gt;\r\n	&lt;/li&gt;\r\n	&lt;li dir=&quot;ltr&quot;&gt;\r\n	&lt;p dir=&quot;ltr&quot; role=&quot;presentation&quot;&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Đường dẫn: tênmiềnwebsite/tên-bai-viet   VD: abc.com/7-dang-vay-maxi&lt;/b&gt;&lt;/p&gt;\r\n	&lt;/li&gt;\r\n&lt;/ul&gt;', '&lt;p&gt;&lt;b id=&quot;docs-internal-guid-c477e9c7-7fff-036d-b425-2e49bd06bd7d&quot;&gt;Mùa Xuân HÈ này, các nàng đừng quên bỏ qua những chiếc váy maxi tuyệt đẹp nhé. Đặc biệt là những nàng chân ngắn, tại sao không thử phong cách mới môt chút nhỉ?!  ...(bạn có thể dùng tên của hàng mình trong dấu..)đã chọn ra được 14 mẫu phong cách mà nàng có thể thích. Hãy tham khảo ngay dưới đây!&lt;/b&gt;&lt;/p&gt;', 'https://cdn.bestme.asia/images/x2/vay-maxi-cao-cap-palvin-h0626-dam-maxi-dep-nguon-hang-thoi-trang-nu-5.jpg', '', '', NULL, '7-dang-vay-maxi-hop-nhat-cho-cac-nang-chan-ngan', NULL, 'image', NULL),
(2, 2, '7 phong cách thời trang cơ bản nhất', '&lt;p&gt;&lt;meta charset=&quot;utf-8&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;h2 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;7 phong cách thời trang cơ bản nhất (Tiêu đề bài viết có thể là tiêu đề SEO - Heading1) &lt;/b&gt;&lt;/h2&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;Thời trang vốn là chủ đề bất tận với vô vàn biến hóa và câu chuyện thú vị gắn liền với lịch sử hình thành và phát triển của nhân loại trong hàng ngàn năm qua. Hiện tại các cô gái đã định hình được phong cách riêng cho mình chưa? Sau đây là top 7 phong cách thời trang cơ bản phổ biến nhất mà … (... là tên shop kinh doanh) lựa chọn để giới thiệu đến các bạn.  (Nội dung tóm tắt bài viết - chữ in nghiêng)&lt;br /&gt;\r\n&lt;br /&gt;\r\n1. Phong cách Vintage (Heading 2 - Tiêu đề 2 bôi đậm-  cỡ chữ nhỏ hơn tiêu đề 1)&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;Chắc hẳn trong tất cả các phong cách thời trang hiện có thì phong cách Vintage cổ điển là style không còn quá xa lạ đối với các tín đồ thời trang và được phái đẹp hết lòng ủng hộ từ lâu nay. Vậy bạn hiểu thế nào về phong cách Vintage này nhỉ? Chẳng những xu hướng Vintage và Retro gắn liền với thời trang mà còn là phong cách phổ biển được sử dụng rộng rãi trong thiết kế đồ họa, kiến trúc và sự kiện đã từng tồn tại cách chúng ta khoảng 60 năm. Theo định nghĩa chung từ &quot;Vintage&quot; được mặc định thể hiện tính chất &quot;cũ và cổ&quot; xuất hiện trên các mẫu thiết kế quần áo từ thập niên 30 của thế kỷ trước có đặc điểm chung là váy xòe có phần eo bé, có tay hay không tay kết hợp cùng găng tay, nón và một số phụ kiện khác,... Còn xu hướng Retro có nghĩa là sử dụng lại trang phục của thập niên trước bao gồm quần áo Vintage và quần áo có phong cách khác.&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;&lt;img alt=&quot;Top 8 phong cách thời trang cơ bản có thể bạn muốn biết&quot; height=&quot;430&quot; src=&quot;https://lh5.googleusercontent.com/Tmdf5_4sVX1nWLFSCx62y8eLCP5JmovgeZDnQjNQVhSZDfV6ED4iCBiMCW0ymCir-cJDQeBWysu4Vz5Krpc5DvTZhvRTA8UqOcrXwJlwKYgXbkI154fB73wEMy-Y_FMIKDUroQkG&quot; width=&quot;470&quot; /&gt;&lt;br /&gt;\r\nVáy hình những chú mèo đáng yêu hơi hướng cổ điển (phải có tiêu đề ảnh, tiêu đề ảnh viết chữ nghiêng, cỡ chữ nhỏ hơn)&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt; &lt;/p&gt;\r\n\r\n&lt;h3 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;Lưu ý: trong mỗi mục có thể sẽ có nhiều ý nhỏ, những ý nhỏ được gọi là Heading 3 -Tiêu đề 3 bôi đậm và in nghiêng - cơ chữ nhỏ hơn tiêu đề 2 và 1) Ví dụ: Trong bài viết này, không có mục nhỏ hơn những nếu có thì heading 3 sẽ là 1.1 Mẫu váy Vintage 1.2  Mua đồ Vintage tại đâu đẹp &lt;/b&gt;&lt;/h3&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;2. Phong cách  Minimalism (tối giản)&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;Có nguồn gốc ra đời từ phong tục gấp giấy origami Nhật Bản với sự hạn chế tối đa những đường nét cầu kỳ, phức tạp chỉ chú trọng vào màu sắc và mẫu mã đơn giản nhất có thể, phong cách Minimalism (tối giản) đang là xu hướng thời trang &quot;thống trị&quot; làng mốt thế giới hiện nay. Nhiều người lầm tưởng rằng phong cách tối giản sẽ tạo cảm giác đơn điệu nhàm chán về kiểu dáng và tông màu nhưng chính phong cách này lại sở hữu nét đẹp cá tính và thanh lịch đầy cuốn hút từ nguyên tắc cơ bản &quot;Simple is the best&quot;. Tuy không chú trọng vào màu sắc sặc sỡ nhưng mỗi bộ cánh theo phong cách tối giản đều chú trọng vào đường nét tỉ mỉ, cầu kỳ vào từng đường kim mũi chỉ. Khi diện phong cách tối giản thì hãy chú ý sự kết hợp hài hòa bổ trợ nhau của những tông màu trung tính là yếu tố tiên quyết giúp bạn diện đẹp chất mọi lúc mọi nơi nhé. &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt; &lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;&lt;img alt=&quot;Top 8 phong cách thời trang cơ bản có thể bạn muốn biết&quot; height=&quot;444&quot; src=&quot;https://lh5.googleusercontent.com/qN1z2bV5aQp8l_hls_ea6bxIAYgSOEScHLPPn9UJFqRLc2jkRR2fY6nVtgXZRmjVi4XPmcQIscWtsvuKq1OQ4ijcH7J8OWsjKAHQfpIlca09WNFD6LffR1oNPVE7XKzII85oI-eN&quot; width=&quot;602&quot; /&gt;Màu sắc trung tính là chìa khóa tạo nên phong cách tối giản&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;3. Phong cách tự do (Hippie)&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;Phong cách tự do (Hippie) - bạn đã từng nghe về phong cách này bao giờ chưa nhỉ? Thực chất phong cách tự do Hippie bắt nguồn từ những con người được cho là &quot;kỳ quặc và bất thường&quot; từ những năm 60 của thế kỷ trước tại Bắc Mỹ và Tây Âu được biết đến là những người mong muốn nâng niu cuộc sống một cách trọn vẹn nhất - họ là những người tuyên truyền hòa bình, tình yêu và hạnh phúc, là đại biểu cho phương châm &quot;Mình thích thì mình làm thôi&quot; là như vậy đấy các bạn. Chính tư tưởng &quot;nổi loạn&quot; không chịu ràng buộc này của họ đã chi phối đến cách ăn mặc, tạo ra cuộc cách mạng cấp tiến về thời trang mang chút gì đó vừa phóng khoáng vừa hoang dại và bí ẩn. Đối với một tín đồ Hippie chính hiệu họ yêu thích lăng xê những trang phục làm từ sợi tơ tự nhiên hay cotton, len và những chiếc váy, đầm, quần ghép nhiều mảnh vải. &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;&lt;img alt=&quot;Top 8 phong cách thời trang cơ bản có thể bạn muốn biết&quot; height=&quot;528&quot; src=&quot;https://lh3.googleusercontent.com/tLYZegeVtFC_wKYHMJVgVHPyCaU6NcMs_Xlmxo7k_HiXmKjJUhuz8NuswWW0Avre4-cQORFhSTl4M1yQsXGmO9cbSDR9wEyUx8RTp2QtEvsUZh7N3S_qiTXqC8Y35ghOD6dczKEW&quot; width=&quot;394&quot; /&gt;&lt;br /&gt;\r\nVáy trắng gợi cảm theo phong cách Hippie tự do cho mùa hè&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt; &lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;4. Phong cách Bohemian&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;Được bị nhầm lẫn với phong cách Hippie tự do, phong cách du mục Bohemian bắt nguồn từ đế chế Bohemia xưa cổ tại Châu Âu đã tạo ra hiệu ứng mạnh mẽ ảnh hưởng đến quan điểm thẫm mỹ của đời sống xã hội, đặc biệt là trong lĩnh vực thời trang. Mỗi trang phục theo phong cách Bohemian đều truyền cảm hứng cho nhận thức tự do không ràng buộc, lối sống phóng khoáng và lãng mạn giống như cuộc sống của những cư dân du mục xa xưa. Với một bộ cánh &quot;lộn xộn đầy phong cách&quot; các cô gái có thể tha hồ thể hiện nguồn cảm hứng sáng tạo thông qua những chiếc đầm/váy bồng xòe xếp li kết hợp với họa tiết dịu dàng nữ tính kết hợp với các phụ kiện &quot;hoang dã&quot; như vòng, lắc tay từ dây cói, dây mây và cách tết tóc đuôi sam,... Thể hiện trọn vẹn vẻ đẹp duyên dáng của mỗi người phụ nữ bất kỳ cô gái nào diện lên trang phục Bohemian đều là đại diện của tinh thần tự do không chịu ràng buộc và mong muốn thoát khỏi guồng quay của xã hội hiện đại để bay bổng và truyền cảm hứng khắp mọi phương trời. &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;&lt;img alt=&quot;Top 8 phong cách thời trang cơ bản có thể bạn muốn biết&quot; height=&quot;504&quot; src=&quot;https://lh6.googleusercontent.com/rJf-gWrTWio70E3HmgZoRDiBw7KZdElZU3NuYydYnwFoFrOOPFbc8Avoah0ELUuxhjyTk781U1bXSCI-qKd5vinbibl3QCmqpYtRtb47MBKh65uJ9cP_THo7pv9Ie7H_5xCd91pd&quot; width=&quot;335&quot; /&gt;&lt;br /&gt;\r\nPhong cách BOHO náo động thời trang hè&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;5. Phong cách Preppy&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;Xuất phát từ thời trang học đường của tầng lớp trung lưu và thương lưu Mỹ từ năm 1912, phong cách Preppy đã trở thành đại diện tiêu biểu cho lối sống và trang phục đặc trưng của người dân xứ cờ hoa. Đặc tính nổi bật nhất để tạo ra phong cách Preppy chính là sự đồng điệu và tính cổ điển được kết hợp hài hòa trong trang phục, thần thái ứng xử và cách trang điểm. Với những item cơ bản kết hợp cùng nhau như áo sơ mi, váy xếp li cùng giày đế bệt, các tín đồ Preppy luôn nói không với những phụ kiện phức tạp, cầu kỳ hay những đường cut out và cúp ngực táo bạo trong thiết kế mà chú trọng vào sự tỉ mỉ kỹ lưỡng trong từng đường may của trang phục. Phong cách học đường siêu hot này đã chinh phục hoàn toàn những cô nàng tri thức yêu vẻ đẹp thanh lịch kín đáo. Một số item không thể thiếu cho những nàng muốn diện &quot;xì tai&quot; Preppy bao gồm: Áo Polo, áo khoác blazer, áo pull len, áo sơ mi,... &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;br /&gt;\r\n &lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;&lt;img alt=&quot;Top 8 phong cách thời trang cơ bản có thể bạn muốn biết&quot; height=&quot;504&quot; src=&quot;https://lh5.googleusercontent.com/r1-SJdMk-EaIYgtStkK-i5Kddj3JgXPYFAf0zA3Etagm2IKNKLRVBvVjIaJ-VHvoWfa3myxask1iDzyxsFkeFwqYTz77mmEIVWvDNerJ-K7aScboDTKyDUFdOngBZYc7BUoK64UR&quot; width=&quot;309&quot; /&gt;&lt;br /&gt;\r\nVáy thủy thủ mùa hè phối nơ&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;6. Phong cách thể thao&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;Từ định kiến cho rằng những chiếc áo Sport Chic chỉ dành riêng cho những buổi tập luyện trong phòng tập gym thì giờ đây sự &quot;nổi dậy&quot; mạnh mẽ của trang phục phong cách thể thao đã nhận được sự khẳng định các tín đồ thời trang trên toàn thế giới, đặc biệt là giới siêu sao Hollywood dường như cũng rất &quot;mê&quot; xu hướng này thì phải. Với những những chiếc áo oversize dáng thụng, chiếc áo hoodie hoặc tanktop bó sát kết hợp với quần jean boyfriend, quần short rách hay quần cotton cùng màu sẽ tạo ra phong cách năng động, trẻ trung mang tính ứng dụng cao thích hợp với nhiều dáng người. Và bạn đừng quên những item phụ kiện &quot;ruột&quot; của Sporty Chic như là mũ lưỡi trai và giày sneaker nữa nhé.&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;&lt;img alt=&quot;Top 8 phong cách thời trang cơ bản có thể bạn muốn biết&quot; height=&quot;441&quot; src=&quot;https://lh3.googleusercontent.com/eeED7ltQ5OTj5TIfMeyDY9TIS_TdYHBdmZbhtlUBVjSPm5S5cRdbv8pMyJ-_8hIvfVnnF6pxeXNfzGgZ6MuEQT4x4q8mU79bRh5Ds7lRG9Ib9mqXrMI6dvQL0_G5ugs_Afaxz86m&quot; width=&quot;331&quot; /&gt;&lt;br /&gt;\r\nBộ sưu tập cách mix áo thể thao với chân váy hoặc short jean năng động&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;7. Phong cách Normcore&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;Từ đầu năm 2014 xu hướng ăn mặc đơn giản quá độ Normcore được các tín đồ thời trang hết lòng tuyên ngôn đang là phong cách mới gây không ít sự hiếu kỳ và tranh luận trong giới thời trang hiện nay. Thực ra nguồn gốc của Normcore đến từ phong cách Unisex (lối ăn mặc đơn giản từ những trang phục đời thường) là đại diện của xu hướng streetstyle tạo ra luồng gió mới phủ định những xu hướng thời trang hoa mỹ, thực dụng và đắt tiền để tận hưởng sự tự do thoát khỏi sự gò bó của những bộ suit khuôn phép. Những item cơ bản gắn liền với phong cách Normcore mà bạn có thể áp dụng thử như là: Áo thun trắng /đen/in slogan/màu trơn, áo hoodie, áo sơ mi không tay, quần jean boy-friend, quần kaki chino, quần short rộng lưng cao, giày thể thao, giày sandal màu đơn giản,... Đây đúng là phong cách cực hữu ích dành cho các cô nàng &quot;lười&quot; nên dự phòng để giải quyết tình trạng không biết phải mặc gì mỗi sáng thức dậy phải không nào.&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;&lt;img alt=&quot;Top 8 phong cách thời trang cơ bản có thể bạn muốn biết&quot; height=&quot;508&quot; src=&quot;https://lh5.googleusercontent.com/wgaUJiBVC5Can0kzezEmn_WOfc8p8bxg4O-XHEG4qsqZwRe_76yAv_RLChPOSS3Yt26gRaLxUAv1pZbg5_vSPERSP_H3OvBCI1ooeBOYxWwTZsRQUAiVx1uo2luRneOtg7BBkhDv&quot; width=&quot;338&quot; /&gt;&lt;br /&gt;\r\nNormcore diện nhanh tức thì cho các cô nàng &quot;lười&quot; khi dạo phố&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;Bạn đã tìm được phong cách thời trang yêu thích phù hợp với cá tính của mình chưa? Thời trang luôn vận động và biến hóa không ngừng nhưng 7 phong cách thời trang cơ bản trên đây chắc hẳn vẫn còn giữ &quot;phong độ&quot; lâu dài trong làng mốt thế giới đấy. Chúc các bạn định hình và phát triển được &quot;xì tai&quot; đẹp miễn chê để tự tin tỏa sáng mọi lúc mọi nơi nhé. &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt; &lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;Lưu ý: Trong phần “Xem trước kết quả tìm kiếm&quot; bạn cần phải hoàn thành 3 mục sau:&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li dir=&quot;ltr&quot;&gt;\r\n	&lt;p dir=&quot;ltr&quot; role=&quot;presentation&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;Thẻ tiêu đề: Tiêu đề bài viết, tên bài viết như 7 phong cách thời trang cơ bản nhất  (Không quá 70 ký tự bao gồm cả dấu phẩy)&lt;br /&gt;\r\n	&lt;a href=&quot;https://halink.vn/tienich/dem-ky-tu/&quot;&gt;https://halink.vn/tienich/dem-ky-tu/&lt;/a&gt;  (website giúp bạn đếm ký tự miễn phí)&lt;/b&gt;&lt;/p&gt;\r\n	&lt;/li&gt;\r\n	&lt;li dir=&quot;ltr&quot;&gt;\r\n	&lt;p dir=&quot;ltr&quot; role=&quot;presentation&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;Nội dung bài viết: (Tóm tắt ý chính của bài, không quá 320 ký tự) Thời trang vốn là chủ đề bất tận gắn liền với lịch sử hình thành và phát triển của nhân loại trong hàng ngàn năm qua. Hiện tại các cô gái đã định hình được phong cách riêng cho mình chưa? Sau đây là top 7 phong cách thời trang cơ bản phổ biến nhất mà … (... là tên shop kinh doanh) lựa chọn để giới thiệu đến các bạn&lt;/b&gt;&lt;/p&gt;\r\n	&lt;/li&gt;\r\n	&lt;li dir=&quot;ltr&quot;&gt;\r\n	&lt;p dir=&quot;ltr&quot; role=&quot;presentation&quot;&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;Đường dẫn: tênmiềnwebsite/tên-bai-viet   VD: abc.com/7-phong-cach-thoi-trang&lt;/b&gt;&lt;/p&gt;\r\n	&lt;/li&gt;\r\n&lt;/ul&gt;', '&lt;p&gt;&lt;b id=&quot;docs-internal-guid-32e914d8-7fff-6e23-a5f7-848de8c47d84&quot;&gt;Thời trang vốn là chủ đề bất tận với vô vàn biến hóa và câu chuyện thú vị gắn liền với lịch sử hình thành và phát triển của nhân loại trong hàng ngàn năm qua. Hiện tại các cô gái đã định hình được phong cách riêng cho mình chưa? Sau đây là top 7 phong cách thời trang cơ bản phổ biến nhất mà …&lt;/b&gt;&lt;/p&gt;', 'https://cdn.bestme.asia/images/x2/2651-dam-xoe-kieu-co-thuyen-tay-con-phong-3.jpg', '', '', NULL, '7-phong-cach-thoi-trang-co-ban-nhat', NULL, 'image', NULL),
(3, 2, '4 cách mặc đẹp với chân váy suông cho mọi dáng người', '&lt;p&gt;&lt;meta charset=&quot;utf-8&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;h1 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;4 cách mặc đẹp với chân váy suông cho mọi dáng người (Tiêu đề bài viết có thể là tiêu đề SEO - Heading1) &lt;/b&gt;&lt;/h1&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;Chân váy suông là một món đồ vô cùng quen thuộc vói các bạn nữ. Nhưng không phải phụ kiện nào mặc cùng với váy suông cũng đều hợp thời trang.  Nếu không tìm ra cách phối đồ phù hợp, chân váy suông có thể khiến bạn trong xuề xòa và trông già hơn tuổi. Để xuống phố thật sành điệu và tự tin cùng thiết kế váy này, các nàng hãy cũng điểm qua những gợi ý mặc đẹp đáng thử sau đây (Nội dung tóm tắt bài viết - chữ in nghiêng)&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li dir=&quot;ltr&quot;&gt;\r\n	&lt;p dir=&quot;ltr&quot; role=&quot;presentation&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;Thế nào là chân váy suông (Heading 2 - Tiêu đề 2 bôi đậm-  cỡ chữ nhỏ hơn tiêu đề 1)&lt;/b&gt;&lt;/p&gt;\r\n	&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;Chân váy suông có phom dáng hình chữ nhật, có độ dài trải từ dưới đầu gối đến trên mắt cá chân và ôm sát vừa phải. Chúng là sự kết hợp giữa chân váy bút chì và chân váy xòe. Kiểu váy này thường có chất liệu mỏng nhẹ như voan và satin. Chân váy suông giúp người mặc trông thon gọn hơn và phù hợp với tiết trời mùa Hè.&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;Tuy nhiên, nếu không tìm ra cách phối đồ phù hợp, chân váy suông có thể khiến bạn trong xuề xòa và trông già hơn tuổi. Để xuống phố thật sành điệu và tự tin cùng thiết kế váy này, các nàng hãy cũng điểm qua những gợi ý mặc đẹp đáng thử sau đây.&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;ol start=&quot;2&quot;&gt;\r\n	&lt;li dir=&quot;ltr&quot;&gt;\r\n	&lt;h2 dir=&quot;ltr&quot; role=&quot;presentation&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;Cách phối đồ với chân váy suông&lt;br /&gt;\r\n	&lt;br /&gt;\r\n	2.1 Phối cùng áo buộc vạt (Heading 3 - Tiêu đề 3 bôi đậm -  cỡ chữ nhỏ hơn tiêu đề 2 và in nghiêng)&lt;/b&gt;&lt;/h2&gt;\r\n	&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;Đây là kiểu phối được fashionista trên khắp thế giới ưa chuộng. Cách kết hợp này giúp các nàng “ăn gian” tỷ lệ cơ thể và trông cao hơn. Bạn có thể chọn áo thun hoặc áo sơ mi trắng đơn giản có sẵn trong tủ đồ hoặc áo cùng tông màu với váy. Bạn chỉ cần lưu ý chọn áo mỏng vừa phải, không quá dày để buộc lên dễ dàng.&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;&lt;img alt=&quot;Cô gái mặc áo thun trắng với chân váy suông vàng&quot; height=&quot;495&quot; src=&quot;https://lh5.googleusercontent.com/k_mO0nPm4k771QTPHGmd0r2iP0l7EcAanenU0elxA5NplLOQv-iDYfpOo4k7j4Mcsz6euHvqcihccciJvmIpfWes1MyBm_VJpgEscnaZCL_GSV4MOqeHt7fT3QVjoh7ZQyLvHyVH&quot; width=&quot;396&quot; /&gt;&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;(Ảnh: Sarah Styles Seattle) (phải có tiêu đề ảnh, tiêu đề ảnh viết chữ nghiêng, cỡ chữ nhỏ hơn)&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;h2 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;2.2 Kết hợp cùng áo Crop top&lt;/b&gt;&lt;/h2&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;Những cô nàng tự tin vào vòng hai có thể diện áo crop top cùng chân váy suông trong Hè này. Áo ngắn và chân váy rộng sẽ mang lại cho bạn cảm giác thoáng mát, dễ chịu trong mùa Hè.&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;&lt;img alt=&quot;Cô gái mặc áo crop-top lệch vai và chân váy suông lụa&quot; height=&quot;424&quot; src=&quot;https://lh6.googleusercontent.com/uQh8opdl0Jl7aLU6IF418NSQWEKNVLciAY4bZV1b_dGNCMoxfLuvLBT1uMEgQJgANQwFyNgbpMqDvwO8f61x2_S-o_lk0cGPT_kdm91bzEsddiozhccueTCZ87cMtmkEAscZYCP9&quot; width=&quot;340&quot; /&gt;&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;(Ảnh: @katiecung)&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;Thoải mái và sành điệu là thế nhưng kiểu phối này lại có phần “kén” dáng. Những cô nàng có vóc dáng nhỏ nhắn hoặc mũm mĩm nên cân nhắc trước khi áp dụng gợi ý này. Một biện pháp an toàn cho bạn chính là chân váy có phần cạp cao để rút ngắn khoảng cách giữa áo và váy. Bên cạnh đó, bạn cũng nên hạn chế những chiếc váy quá dài, độ dài lý tưởng nhất là từ qua đầu gối đến giữa bắp chân.&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;h2 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;2.3 ÁO VÀ CHÂN VÁY SUÔNG ĐỒNG BỘ&lt;/b&gt;&lt;/h2&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;Một kiểu phối đồ thú vị với chân váy suông chính là áo đồng bộ với váy. Bạn nên chọn kiểu áo ngắn để tạo sự phân cách với phần lưng váy. Bạn có thể chọn áo và váy trơn màu nếu yêu thích phong cách tối giản hoặc trang phục họa tiết nếu muốn tạo vẻ ngoài nổi bật.&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;&lt;img alt=&quot;Fahionista mặc áo và chân váy suông vải satin màu trắng&quot; height=&quot;504&quot; src=&quot;https://lh6.googleusercontent.com/kHtInE2OABzRE3_XvorZvSHi1yIC6hA0ckT6KBQLKTshpzQnYpXxBV9hCMqGRhLtQl_-CERrIYoW_3VtjvBT3jMaO_SR3PPl9o1iQxzOLOsk8Etb0bTbMMV-LXh_kPA70bmM1o25&quot; width=&quot;403&quot; /&gt;&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;(Ảnh: @sandrasemburg)&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;Cũng như khi diện với áo crop top, những cô nàng có chiều cao khiêm tốn nên hạn chế những chiếc chân váy cạp trễ vì chúng sẽ làm thân hình bạn trông mất luộm thuộm và mất cân đối. Vì váy suông có thể khiến chân ngắn đi trông thấy nên các nàng nên chọn những kiểu giày giúp tạo hiệu ứng tăng chiều cao phù hợp với dáng người.&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;h2 dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;2.4 LỰA CHỌN HỌA TIẾT PHÙ HỢP&lt;/b&gt;&lt;/h2&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;Họa tiết in trên chân váy không chỉ tạo điểm nhấn cho bộ trang phục mà còn hiệu ứng thị giác giúp thân hình bạn trông cân đối hơn. Nhưng nếu không biết cách sử dụng họa tiết hợp lý, chiếc chân váy sẽ “phản chủ” và để lộ khuyết điểm. Vi thế, bạn nên cân nhắc chọn màu sắc và họa tiết phù hợp với đặc điểm vóc dáng.&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;Những cô nàng dáng thước kẻ hay đồng hồ cát lý tưởng có thể thử nghiệm các họa tiết to và ấn tượng. Những nàng quả táo nên tìm họa tiết to để cân bằng với nửa thân trên, còn các nàng quả lê thì áp dụng cách ngược lại. Bên cạnh đó, chân váy kẻ sọc đứng hoặc xếp ly sẽ giúp các nàng trông thon và cao hơn.&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;&lt;img alt=&quot;Cô gái mặc áo sơ mi trắng và chân váy suông xếp ly chấm bi&quot; height=&quot;460&quot; src=&quot;https://lh3.googleusercontent.com/snASzGWQjVe4bm8mAe2pm422jC5lrYw9u8WKZM38TZuVU81fq5B03PT_OX3zBzog-hyJ_EOtiFpYR70Fzqakb6xsIPVZxSj8rXM4MX_Gfx9d8YANgoddMHfiun0QZbtgYQpwSfhH&quot; width=&quot;346&quot; /&gt;&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;(Ảnh: The sartorialist)&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;Kết hợp ăn ý với chân váy họa tiết là những chiếc áo tối giản. Bạn nên chọn áo trơn, màu sắc trung tính hoặc có gam màu hài hòa với họa tiết trên váy.&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt; &lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;Lưu ý: Trong phần “Xem trước kết quả tìm kiếm&quot; bạn cần phải hoàn thành 3 mục sau:&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li dir=&quot;ltr&quot;&gt;\r\n	&lt;p dir=&quot;ltr&quot; role=&quot;presentation&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;Thẻ tiêu đề: Tiêu đề bài viết, tên bài viết như 4 cách mặc đẹp với chân váy suông cho mọi dáng người  (Không quá 70 ký tự bao gồm cả dấu phẩy)&lt;br /&gt;\r\n	&lt;a href=&quot;https://halink.vn/tienich/dem-ky-tu/&quot;&gt;https://halink.vn/tienich/dem-ky-tu/&lt;/a&gt;  (website giúp bạn đếm ký tự miễn phí)&lt;/b&gt;&lt;/p&gt;\r\n	&lt;/li&gt;\r\n	&lt;li dir=&quot;ltr&quot;&gt;\r\n	&lt;p dir=&quot;ltr&quot; role=&quot;presentation&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;Nội dung bài viết: (Tóm tắt ý chính của bài, không quá 320 ký tự) Chân váy suông là một món đồ vô cùng quen thuộc vói các bạn nữ. Nhưng không phải phụ kiện nào mặc cùng với váy suông cũng đều hợp thời trang.  Nếu không tìm ra cách phối đồ phù hợp, chân váy suông có thể khiến bạn trong xuề xòa và trông già hơn tuổi.&lt;/b&gt;&lt;/p&gt;\r\n	&lt;/li&gt;\r\n	&lt;li dir=&quot;ltr&quot;&gt;\r\n	&lt;p dir=&quot;ltr&quot; role=&quot;presentation&quot;&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;Đường dẫn: tênmiềnwebsite/ten-bai-viet   VD: abc.com/4-cach-mac-chan-vay-suong&lt;/b&gt;&lt;/p&gt;\r\n	&lt;/li&gt;\r\n&lt;/ul&gt;', '&lt;p&gt;&lt;b id=&quot;docs-internal-guid-f75976b9-7fff-e57d-ce03-a96d33ccdb87&quot;&gt;Chân váy suông là một món đồ vô cùng quen thuộc vói các bạn nữ. Nhưng không phải phụ kiện nào mặc cùng với váy suông cũng đều hợp thời trang.  Nếu không tìm ra cách phối đồ phù hợp, chân váy suông có thể khiến bạn trong xuề xòa và trông già hơn tuổi. Để xuống phố thật sành điệu và tự tin cùng thiết kế váy này, các nàng hãy cũng điểm qua những gợi ý mặc đẹp đáng thử sau đây&lt;/b&gt;&lt;/p&gt;', 'https://cdn.bestme.asia/images/x2/86f88cd8823165c8d3920b7ba4ee0335.jpg', '', '', NULL, '4-cach-mac-dep-voi-chan-vay-suong-cho-moi-dang-nguoi', NULL, 'image', NULL);

-- cfr_blog_to_blog_category NO PRIMARY KEY
-- remove first
DELETE FROM `cfr_blog_to_blog_category`
WHERE `blog_id` IN (1, 2, 3) AND `blog_category_id` = 1;

-- insert
INSERT IGNORE INTO `cfr_blog_to_blog_category` (`blog_id`, `blog_category_id`) VALUES
(1, 1),
(2, 1),
(3, 1);

-- ingredient
CREATE TABLE IF NOT EXISTS `cfr_ingredient` (
 `ingredient_id` INT(10) NOT NULL AUTO_INCREMENT,
 `name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
 `code` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
 `active_ingredient` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 `unit` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 `basic_description` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 `note` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 `overdose` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 `course_certificates` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 `detail_description` LONGTEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 `us_man_19_30` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 `us_man_31_50` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 `us_man_51_70` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 `us_man_gt_70` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 `us_women_19_30` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 `us_women_31_50` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 `us_women_51_70` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 `us_women_gt_70` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 `vn_man_19_50` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 `vn_man_51_60` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 `vn_man_gt_60` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 `vn_women_19_50` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 `vn_women_51_60` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 `vn_women_gt_60` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 `created_at` datetime NOT NULL,
 `updated_at` datetime NOT NULL,
 PRIMARY KEY (`ingredient_id`) USING BTREE
) COLLATE='utf8_unicode_ci' ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `cfr_product_ingredient` (
 `product_id` INT(10) NOT NULL,
 `ingredient_id` INT(10) NOT NULL,
 `quantitative` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
 PRIMARY KEY (`product_id`, `ingredient_id`) USING BTREE
) COLLATE='utf8_unicode_ci' ENGINE=MyISAM;

ALTER TABLE `cfr_ingredient`
    ADD UNIQUE INDEX `code` (`code`);
-- end ingredient

-- -------- new column meta_keyword ------------
ALTER TABLE `cfr_blog_category_description`
    ADD COLUMN `meta_keyword` VARCHAR(255) NULL COLLATE 'utf8_general_ci' AFTER `alias`;

-- -------- end new column meta_keyword --------

-- -------- 20220614 version coupon ----------------
ALTER TABLE `cfr_coupon`
    CHANGE COLUMN `date_start` `date_start` DATETIME NOT NULL AFTER `total`,
    CHANGE COLUMN `date_end` `date_end` DATETIME NOT NULL AFTER `date_start`,
    ADD COLUMN `allow_with_discount` INT(11) NULL DEFAULT NULL COMMENT 'Cho phép sử dụng chung với chương trình khuyến mãi' COLLATE 'utf8_general_ci' AFTER `date_added`,
    ADD COLUMN `limit_times` INT(11) NULL DEFAULT NULL COMMENT 'Số lần sử dụng' COLLATE 'utf8_general_ci' AFTER `allow_with_discount`,
    ADD COLUMN `max_type_value` DECIMAL(15,4) NULL DEFAULT NULL COMMENT 'Giá cao nhất' COLLATE 'utf8_general_ci' AFTER `limit_times`,
    ADD COLUMN `apply_for` INT(11) NULL DEFAULT NULL COMMENT 'Áp dụng cho' COLLATE 'utf8_general_ci' AFTER `max_type_value`,
    ADD COLUMN `applicable_type` INT(11) NULL DEFAULT NULL COMMENT 'Loại áp dụng' COLLATE 'utf8_general_ci' AFTER `apply_for`,
    ADD COLUMN `applicable_type_value` DECIMAL(15,4) NULL DEFAULT NULL COMMENT 'Số lượng sản phẩm áp dụng hoặc Tổng giá trị sản phẩm' COLLATE 'utf8_general_ci' AFTER `applicable_type`,
    ADD COLUMN `sale_channel` INT(11) NULL DEFAULT NULL COMMENT 'Kênh áp dụng' COLLATE 'utf8_general_ci' AFTER `applicable_type_value`,
    ADD COLUMN `order_value_from` DECIMAL(15,4) NULL DEFAULT NULL COMMENT 'Trị giá đơn hàng từ (số tiền)' COLLATE 'utf8_general_ci' AFTER `sale_channel`,
    ADD COLUMN `how_to_apply` INT(11) NULL DEFAULT NULL COMMENT 'Cách áp dụng' COLLATE 'utf8_general_ci' AFTER `order_value_from`,
    ADD COLUMN `subjects_of_application` INT(11) NULL DEFAULT NULL COMMENT 'Đối tượng áp dụng' COLLATE 'utf8_general_ci' AFTER `how_to_apply`;

ALTER TABLE `cfr_coupon_product`
    ADD COLUMN `product_version_id` INT(11) NULL DEFAULT NULL AFTER `product_id`,
DROP PRIMARY KEY,
	ADD PRIMARY KEY (`coupon_product_id`) USING BTREE,
	ADD INDEX `coupon_id` (`coupon_id`),
	ADD INDEX `product_id` (`product_id`),
	ADD INDEX `product_version_id` (`product_version_id`);

ALTER TABLE `cfr_coupon`
    CHANGE COLUMN `date_start` `date_start` DATETIME NOT NULL AFTER `total`,
    ADD COLUMN `never_expired` INT(10) NULL DEFAULT NULL AFTER `subjects_of_application`,
    ADD COLUMN `allow_coupon_with_discount` INT(10) NULL DEFAULT NULL AFTER `never_expired`;

ALTER TABLE `cfr_coupon`
DROP COLUMN `allow_coupon_with_discount`;

ALTER TABLE `cfr_coupon`
    ADD COLUMN `init_limit_times` INT(10) NULL DEFAULT NULL AFTER `limit_times`;

ALTER TABLE `cfr_order_voucher`
    ADD COLUMN `status` INT(10) NULL DEFAULT 1 AFTER `amount`,
	ADD COLUMN `date_added` DATETIME NULL AFTER `status`;

INSERT INTO `cfr_event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES
    ('update_limit_times_coupon', 'catalog/model/sale/order/addOrderVoucher/after', 'event/activity_tracking/afterAddOrderVoucher', 1, 0);

-- -------- End 20220614 version coupon ------------

-- -------- 20330701 version discount --------------

DROP TABLE `cfr_discount`;

CREATE TABLE `cfr_discount` (
   `discount_id` INT(11) NOT NULL AUTO_INCREMENT,
   `code` VARCHAR(100) NOT NULL,
   `name` VARCHAR(255) NULL,
   `description` TEXT NULL,
   `date_started` DATETIME NULL DEFAULT NULL,
   `date_ended` DATETIME NULL DEFAULT NULL,
   `status` TINYINT(4) NULL DEFAULT '1',
   `date_modify` DATETIME NULL DEFAULT NULL,
   PRIMARY KEY (`discount_id`)
) COLLATE='utf8_unicode_ci';

CREATE TABLE `cfr_discount_history` (
   `discount_history_id` INT(11) NOT NULL AUTO_INCREMENT,
   `discount_id` INT(11) NOT NULL,
   `user_id` INT(11) NOT NULL,
   `date_added` DATETIME NOT NULL,
   `action` VARCHAR(255) NULL DEFAULT NULL,
   `reson` TEXT NULL DEFAULT NULL,
   `date_started` DATETIME NOT NULL,
   `date_ended` DATETIME NOT NULL,
   PRIMARY KEY (`discount_history_id`)
) COLLATE='utf8_unicode_ci';

CREATE TABLE `cfr_discount_product` (
   `discount_product_id` INT(11) NOT NULL AUTO_INCREMENT,
   `product_id` INT(11) NOT NULL,
   `product_version_id` INT(11) NOT NULL,
   `discount_id` INT(11) NOT NULL,
   `discount_type` TINYINT(4) NULL DEFAULT '0' COMMENT '0: gia tien, 1: phan tram',
   `discount_value` DECIMAL(15,4) NULL DEFAULT NULL,
   `price_discount` DECIMAL(15,4) NULL DEFAULT NULL,
   PRIMARY KEY (`discount_product_id`)
) COLLATE='utf8_unicode_ci';

ALTER TABLE `cfr_order_product`
    ADD COLUMN `discount_id` INT(11) NULL DEFAULT NULL AFTER `reward`;

ALTER TABLE `cfr_discount_product`
    ADD COLUMN `limit` TINYINT(3) NULL DEFAULT '0' COMMENT '0: khong gioi han, 1: gioi han' AFTER `price_discount`,
	ADD COLUMN `status` TINYINT(3) NULL DEFAULT '1' COMMENT '0: tat, 1 bat' AFTER `limit`;

ALTER TABLE `cfr_discount_product`
    DROP COLUMN `discount_type`,
	DROP COLUMN `discount_value`;

ALTER TABLE `cfr_discount`
    ADD COLUMN `times_used` INT(11) NULL DEFAULT NULL AFTER `date_modify`;

ALTER TABLE `cfr_order_product`
    CHANGE COLUMN `tax` `tax` DECIMAL(15,4) NOT NULL DEFAULT '0.0000' AFTER `total`,
    ADD COLUMN `listed_price` DECIMAL(18,4) NULL DEFAULT '0.0000' AFTER `discount_id`;

ALTER TABLE `cfr_discount` ADD `deleted` TINYINT NOT NULL DEFAULT '0' AFTER `status`;

-- -------- End 20330701 version discount ----------

-- -------- Start 20220718 add column in_home table blog ------------
ALTER TABLE `cfr_blog` ADD `in_home` TINYINT(4) NOT NULL DEFAULT '0' AFTER `source`;
-- -------- End 20220718 add column in_home table blog ------------

-- -------- Start migration_20220728_add_on_deal --------

CREATE TABLE `cfr_add_on_deal` (
    `add_on_deal_id` INT(11) NOT NULL AUTO_INCREMENT ,
    `name` VARCHAR(255) NULL DEFAULT NULL ,
    `date_started` DATETIME NULL DEFAULT NULL ,
    `date_ended` DATETIME NULL DEFAULT NULL ,
    `limit_quantity_product` INT(11) NULL DEFAULT NULL ,
    `status` INT(11) NOT NULL DEFAULT '1' ,
    `deleted` TINYINT(4) NULL DEFAULT NULL ,
    `date_modify` DATETIME NULL DEFAULT NULL ,
    PRIMARY KEY (`add_on_deal_id`)) ENGINE = MyISAM;

CREATE TABLE `cfr_add_on_deal_main_product` (
    `main_product_id` INT(11) NOT NULL AUTO_INCREMENT ,
    `add_on_deal_id` INT(11) NOT NULL ,
    `product_id` INT(11) NOT NULL ,
    `product_version_id` INT(11) NULL DEFAULT '0' ,
    `status` INT(11) NULL DEFAULT '1' ,
    PRIMARY KEY (`main_product_id`)) ENGINE = MyISAM;

CREATE TABLE `cfr_add_on_product` (
    `add_on_product_id` INT(11) NOT NULL AUTO_INCREMENT ,
    `add_on_deal_id` INT(11) NOT NULL ,
    `product_id` INT(11) NOT NULL ,
    `product_version_id` INT(11) NOT NULL ,
    `limit_quantity` INT(11) NULL DEFAULT NULL ,
    `discount_price` DECIMAL(15,4) NULL DEFAULT NULL ,
    `status` INT(11) NULL DEFAULT '1' ,
    PRIMARY KEY (`add_on_product_id`)) ENGINE = MyISAM;

CREATE TABLE `cfr_add_on_deal_history` (
    `history_id` INT(11) NOT NULL AUTO_INCREMENT ,
    `add_on_deal_id` INT(11) NOT NULL ,
    `user_id` INT(11) NOT NULL ,
    `date_added` DATETIME NOT NULL ,
    `action` VARCHAR(255) NOT NULL ,
    `reson` VARCHAR(255) NOT NULL ,
    `date_started` DATETIME NOT NULL ,
    `date_ended` DATETIME NOT NULL ,
    PRIMARY KEY (`history_id`)) ENGINE = InnoDB;

CREATE TABLE `cfr_order_add_on_product` (
    `order_add_on_product_id` INT(11) NOT NULL AUTO_INCREMENT,
    `order_id` INT(11) NOT NULL,
    `main_product_id` INT(11) NOT NULL,
    `main_product_version_id` INT(11) NOT NULL DEFAULT '0',
    `product_id` INT(11) NOT NULL,
    `product_version_id` INT(11) NOT NULL DEFAULT '0',
    PRIMARY KEY (`order_add_on_product_id`)
) ENGINE = MyISAM COLLATE='utf8mb4_unicode_ci';

ALTER TABLE `cfr_order_product` ADD `add_on_deal_id` INT(11) NULL DEFAULT NULL AFTER `discount`;

ALTER TABLE `cfr_add_on_product`
    ADD COLUMN `current_quantity` INT(10) NULL DEFAULT NULL AFTER `limit_quantity`;

-- -------- End migration_20220728_add_on_deal ----------

-- -------- migration_20220815_change_column_content_of_blog_des_table ----------

ALTER TABLE `cfr_blog_description`
    CHANGE COLUMN `content` `content` LONGTEXT NULL COLLATE 'utf8_general_ci' AFTER `title`;

-- -------- End migration_20220815_change_column_content_of_blog_des_table -------

-- -------- Start migration_20230823_robot_meta_product -------

ALTER TABLE `cfr_product_description`
    ADD COLUMN `noindex` TINYINT NOT NULL DEFAULT '0' AFTER `meta_keyword`,
    ADD COLUMN `nofollow` TINYINT NOT NULL DEFAULT '0' AFTER `noindex`,
    ADD COLUMN `noarchive` TINYINT NOT NULL DEFAULT '0' AFTER `nofollow`,
    ADD COLUMN `noimageindex` TINYINT NOT NULL DEFAULT '0' AFTER `noarchive`,
    ADD COLUMN `nosnippet` TINYINT NOT NULL DEFAULT '0' AFTER `noimageindex`;

ALTER TABLE `cfr_category_description`
    ADD COLUMN `noindex` TINYINT NOT NULL DEFAULT '0' AFTER `meta_keyword`,
    ADD COLUMN `nofollow` TINYINT NOT NULL DEFAULT '0' AFTER `noindex`,
    ADD COLUMN `noarchive` TINYINT NOT NULL DEFAULT '0' AFTER `nofollow`,
    ADD COLUMN `noimageindex` TINYINT NOT NULL DEFAULT '0' AFTER `noarchive`,
    ADD COLUMN `nosnippet` TINYINT NOT NULL DEFAULT '0' AFTER `noimageindex`;

ALTER TABLE `cfr_collection_description`
    ADD COLUMN `noindex` TINYINT NOT NULL DEFAULT '0' AFTER `alias`,
    ADD COLUMN `nofollow` TINYINT NOT NULL DEFAULT '0' AFTER `noindex`,
    ADD COLUMN `noarchive` TINYINT NOT NULL DEFAULT '0' AFTER `nofollow`,
    ADD COLUMN `noimageindex` TINYINT NOT NULL DEFAULT '0' AFTER `noarchive`,
    ADD COLUMN `nosnippet` TINYINT NOT NULL DEFAULT '0' AFTER `noimageindex`;

-- -------- End migration_20230823_robot_meta_product ---------

-- --------End - Load theme default data ---------------------

ALTER TABLE `cfr_blog`
    ADD `approval_status` VARCHAR(255) NULL DEFAULT NULL AFTER `in_home`;

-- -------- Start show_gg_product_feed ---------
ALTER TABLE `cfr_product`
    ADD `show_gg_product_feed` TINYINT(1) NOT NULL DEFAULT '1' AFTER `date_modified`;
-- -------- End show_gg_product_feed ---------

-- --------Add migrated versions for new shops ---------------

-- Dumping data for table `cfr_migration`
INSERT INTO `cfr_migration` (`migration`, `run`, `date_run`) VALUES
-- ('migration_20190718_default_policy_gg_shopping', 1, NOW()), -- already above when first time add migration feature
('migration_20190729_enable_seo', 1, NOW()),
('migration_20190729_2_fix_demo_data_sql', 1, NOW()),
('migration_20190731_remake_menu', 1, NOW()),
('migration_20190810_staff_permission', 1, NOW()),
('migration_20190822_product_multi_version', 1, NOW()),
('migration_20190828_chatbot', 1, NOW()),
('migration_20190828_transport', 1, NOW()),
('migration_20190903_sync_staffs_to_welcome', 1, NOW()),
('migration_20190904_keep_edited_demo_data', 1, NOW()),
('migration_20190930_increase_decimal_order', 1, NOW()),
('migration_20191015_new_report_tables_and_events', 1, NOW()),
('migration_20191016_new_report_data', 1, NOW()),
('migration_20191023_new_report_events', 1, NOW()),
('migration_20191024_report_order_from_shop', 1, NOW()),
('migration_20191028_add_field_from_domain_order', 1, NOW()),
('migration_20191029_default_delivery_method', 1, NOW()),
('migration_20191111_blog_permission_and_seo', 1, NOW()),
('migration_20191115_order_success_seo_and_text', 1, NOW()),
('migration_20191116_add_settings_classify_permission', 1, NOW()),
('migration_20191127_sync_onboading_to_welcome', 1, NOW()),
('migration_20191209_default_store', 1, NOW()),
('migration_20191210_add_columns_for_pos', 1, NOW()),
('migration_20191223_save_image_url_to_database', 1, NOW()),
('migration_20191225_builder_product_per_row_mobile', 1, NOW()),
('migration_20191226_register_success_text', 1, NOW()),
('migration_20200103_soft_delete_product', 1, NOW()),
('migration_20200117_add_settings_notify_permission', 1, NOW()),
('migration_20200212_discount', 1, NOW()),
('migration_20200314_chatbot_v2', 1, NOW()),
('migration_20200312_store_receipt', 1, NOW()),
('migration_20200410_sync_report_order_to_cms', 1, NOW()),
('migration_20200420_add_table_order_utm', 1, NOW()),
('migration_20200420_store_permission', 1, NOW()),
('migration_20200421_fill_product_to_store_with_default_store', 1, NOW()),
('migration_20200422_add_column_table_product', 1, NOW()),
('migration_20200520_appstore_version', 1, NOW()),
('migration_20200626_advance_store_manager_version', 1, NOW()),
('migration_20200717_shopee_connection', 1, NOW()),
('migration_20200731_add_column_table_shopee_order', 1, NOW()),
('migration_20200811_novaonx_social', 1, NOW()),
('migration_20200816_add_column_demo_blog_table', 1, NOW()),
('migration_20200812_lazada_connection', 1, NOW()),
('migration_20200817_add_column_alt_image_blog_table', 1, NOW()),
('migration_20200817_move_chatbot_to_app_store', 1, NOW()),
('migration_20200818_onfluencer_app', 1, NOW()),
('migration_20200821_lazada_sync_order', 1, NOW()),
('migration_20200819_add_columns_for_customer', 1, NOW()),
('migration_20200904_cash_flow', 1, NOW()),
('migration_20200821_lazada_sync_order_again', 1, NOW()),
('migration_20200904_activity_log_analytics', 1, NOW()),
('migration_20200921_lazada_product_short_description', 1, NOW()),
('migration_20200915_shopee_advance_product', 1, NOW()),
('migration_20200916_pull_image_from_cloudinary', 1, NOW()),
('migration_20200922_transport_status_management', 1, NOW()),
('migration_20201007_tracking_pos_order_complete', 1, NOW()),
-- ('migration_20200924_full_text_search_product_name', 1, NOW()), -- temp not use
('migration_20201002_add_video_to_blog', 1, NOW()),
('migration_20201021_pos_return_goods', 1, NOW()),
('migration_20201019_bestme_package', 1, NOW()),
('migration_20201023_update_image_size', 1, NOW()),
('migration_20201103_create_image_folder', 1, NOW()),
('migration_20201106_add_columns_for_report', 1, NOW()),
('migration_20201118_add_columns_keyword_for_blog', 1, NOW()),
('migration_20201118_create_attribute_filter_table', 1, NOW()),
('migration_20201130_add_permission_customize_layout', 1, NOW()),
('migration_20201201_shopee_category_and_logistics', 0, NOW()), -- SET RUN=0 to keep running for new shop for loading data from file to shopee_category and shoppe_logistics table!
('migration_20201214_shopee_upload_add_tables', 1, NOW()),
('migration_20210107_tuning_2_11_2', 1, NOW()),
('migration_20210310_v3_3_1_advance_permission', 1, NOW()),
('migration_20210329_3_3_3_finetune', 1, NOW()),
('migration_20210409_add_columns_collection_amount_for_order', 1, NOW()),
('migration_20201125_add_column_store_id_for_return_receipt', 1, NOW()),
('migration_20210422_add_column_store_id_for_return_receipt_if_not_exists', 1, NOW()),
('migration_20210409_add_columns_collection_amount_for_order', 1, NOW()),
('migration_20210504_v3_4_2_sync_bestme_social', 1, NOW()),
('migration_20210517_add_column_order_id_on_campaign_voucher', 1, NOW()),
('migration_20210525_update_migrate_v3_4_2_add_new_table_order_channel', 1, NOW()),
('migration_20210616_v3_5_1_open_api', 1, NOW()),
('migration_20210812_create_keyword_sync_table', 1, NOW()),
('migration_20210817_add_column_source_sync', 1, NOW()),
('migration_20210914_add_email_subscribers', 1, NOW()),
('migration_20211012_add_new_column_to_table_product', 1, NOW()),
('migration_20211201_add_source_author_name_to_blog_description_table', 1, NOW()),
('migration_20220415_set_sitemap_detail_lastmod', 1, NOW()),
('migration_20220416_add_seo_url_events', 1, NOW()),
('migration_20220519_ingredient', 1, NOW()),
('migration_20220420_add_new_column_to_table_blog_category', 1, NOW()),
('migration_20220601_add_column_meta_keyword_table_blog_category', 1, NOW()),
('migration_20221013_coupon', 1, NOW()),
('migration_20220701_discount_new', 1, NOW()),
('migration_20221013_coupon', 1, NOW()),
('migration_20220718_add_column_in_home_for_blog_table', 1, NOW()),
('migration_20220701_discount_new', 1, NOW()),
('migration_20220728_add_on_deal', 1, NOW()),
('migration_20220815_change_column_content_of_blog_des_table', 1, NOW()),
('migration_20220818_add_column_approval_status_for_blog_table', 1, NOW()),
('migration_20220825_add_column_show_gg_product_feed_for_product_table', 1, NOW()),
('migration_20230823_robot_meta_product', 1, NOW());

-- --------end Add migrated versions for new shops -----------

-- -----------------------------------------------------------
