<?php

//
// Command line tool for installing opencart
// Author: Vineet Naik <vineet.naik@kodeplay.com> <naikvin@gmail.com>
//
// (Currently tested on linux only)
//
// Usage:
//
//   cd install
//   php cli_install.php install {merchantid} 	--shop_name yourshop \
//                                              --db_hostname localhost \
//                               			  	--db_username root \
//                               			  	--db_password pass \
//                                 			  	--db_database opencart \
//                               				--db_driver mysqli \
//								 				--db_port 3306 \
//                               				--username admin \
//                               				--password admin \
//                               				--email youremail@example.com \
//								 				--db_prefix ocn_
//                               				--http_server http://localhost/opencart/ \
//                                              --fullname John Doe \
//                                              --phone_number 0987654321
//

ini_set('display_errors', 1);
error_reporting(E_ALL);
$argv = $_SERVER['argv'];
$script = array_shift($argv);
$subcommand = array_shift($argv);
$merchantid = array_shift($argv);

// DIR
define('DIR_APPLICATION', str_replace('\\', '/', realpath(dirname(__FILE__))) . '/');
define('DIR_SYSTEM', str_replace('\\', '/', realpath(dirname(__FILE__) . '/../')) . '/system/');
define('DIR_OPENCART', str_replace('\\', '/', realpath(DIR_APPLICATION . '../')) . '/');
define('DIR_DATABASE', DIR_SYSTEM . 'database/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_MODIFICATION', DIR_SYSTEM . 'modification/');

// Other configurations
if (is_file(str_replace('\\', '/', realpath(dirname(__FILE__))) . '/config.php')) {
    require_once(str_replace('\\', '/', realpath(dirname(__FILE__))) . '/config.php');
}

// Startup
require_once(DIR_SYSTEM . 'startup.php');

// Registry
$registry = new Registry();

// Loader
$loader = new Loader($registry);
$registry->set('load', $loader);


function handleError($errno, $errstr, $errfile, $errline, array $errcontext) {
	// error was suppressed with the @-operator
	if (0 === error_reporting()) {
		return false;
	}
	throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}

set_error_handler('handleError');


function usage() {
	echo "Usage:\n";
	echo "======\n";
	echo "\n";
	$options = implode(" ", array(
		'--shop_name', 'yourshop',
		'--db_hostname', 'localhost',
		'--db_username', 'root',
		'--db_password', 'pass',
		'--db_database', 'opencart',
		'--db_driver', 'mysqli',
		'--db_port', '3306',
		'--username', 'admin',
		'--password', 'admin',
		'--email', 'youremail@example.com',
		'--http_server', 'http://localhost/opencart/',
		'--fullname', 'John Doe',
		'--phone_number', '0987654321'
	));
	echo 'php cli_install.php install ' . $options . "\n\n";
}


function get_options($argv) {
	$defaults = array(
		'db_hostname' => 'localhost',
		'db_database' => 'opencart',
		'db_prefix' => 'oc_',
		'db_driver' => 'mysqli',
		'db_port' => '3306',
		'username' => 'admin',
	);

	$options = array();
	$total = count($argv);
	for ($i=0; $i < $total; $i=$i+2) {
		$is_flag = preg_match('/^--(.*)$/', $argv[$i], $match);
		if (!$is_flag) {
			throw new Exception($argv[$i] . ' found in command line args instead of a valid option name starting with \'--\'');
		}
		$options[$match[1]] = $argv[$i+1];
	}
	return array_merge($defaults, $options);
}


function valid($options) {
	$required = array(
		'shop_name',
		'db_hostname',
		'db_username',
		'db_password',
		'db_database',
		'db_prefix',
		'db_port',
		'username',
		'password',
		'email',
		'http_server',
		'fullname',
		'phone_number',
	);
	$missing = array();
	foreach ($required as $r) {
		if (!array_key_exists($r, $options)) {
			$missing[] = $r;
		}
	}
	if (!preg_match('#/$#', $options['http_server'])) {
		$options['http_server'] = $options['http_server'] . '/';
	}
	$valid = count($missing) === 0;
	return array($valid, $missing);
}


function install($options) {
	$check = check_requirements();
	if ($check[0]) {
		setup_db($options);
		write_config_files($options);
		dir_permissions();
	} else {
		echo 'FAILED! Pre-installation check failed: ' . $check[1] . "\n\n";
		exit(1);
	}
}


function check_requirements() {
	$error = null;
	if (phpversion() < '5.4') {
		$error = 'Warning: You need to use PHP5.4+ or above for OpenCart to work!';
	}

	if (!ini_get('file_uploads')) {
		$error = 'Warning: file_uploads needs to be enabled!';
	}

	if (ini_get('session.auto_start')) {
		$error = 'Warning: OpenCart will not work with session.auto_start enabled!';
	}

	if (!extension_loaded('mysqli')) {
		$error = 'Warning: MySQLi extension needs to be loaded for OpenCart to work!';
	}

	if (!extension_loaded('gd')) {
		$error = 'Warning: GD extension needs to be loaded for OpenCart to work!';
	}

	if (!extension_loaded('curl')) {
		$error = 'Warning: CURL extension needs to be loaded for OpenCart to work!';
	}

	if (!function_exists('openssl_encrypt')) {
		$error = 'Warning: OpenSSL extension needs to be loaded for OpenCart to work!';
	}

	if (!extension_loaded('zlib')) {
		$error = 'Warning: ZLIB extension needs to be loaded for OpenCart to work!';
	}

	return array($error === null, $error);
}


function setup_db($data) {
	$db = new DB($data['db_driver'], htmlspecialchars_decode($data['db_hostname']), htmlspecialchars_decode($data['db_username']), htmlspecialchars_decode($data['db_password']), htmlspecialchars_decode($data['db_database']), $data['db_port']);

    if (isset($data['brand_type']) && !empty($data['brand_type']) && $data['brand_type'] === 'ADG_BRANCH_TOPAL') {
        $file = DIR_APPLICATION . 'opencart_adg_topal.sql';
    } else if ((isset($data['brand_type']) && !empty($data['brand_type'])) &&
        (($data['brand_type'] === 'ADG_BRANCH_AUSTDOOR') || ($data['brand_type'] === 'ADG_BRANCH_ALL'))) {
        $file = DIR_APPLICATION . 'opencart_adg_austdoor.sql';
    } else {
        $file = DIR_APPLICATION . 'opencart.sql';
    }

	if (!file_exists($file)) {
		exit('Could not load sql file: ' . $file);
	}

	$lines = file($file);

	if ($lines) {
		$sql = '';

		foreach ($lines as $line) {
			if ($line && (substr($line, 0, 2) != '--') && (substr($line, 0, 1) != '#')) {
				$sql .= $line;

				if (preg_match('/;\s*$/', $line)) {
					$sql = str_replace("DROP TABLE IF EXISTS `oc_", "DROP TABLE IF EXISTS `" . $data['db_prefix'], $sql);
					$sql = str_replace("CREATE TABLE `oc_", "CREATE TABLE `" . $data['db_prefix'], $sql);
					$sql = str_replace("INSERT INTO `oc_", "INSERT INTO `" . $data['db_prefix'], $sql);
					//nkt add for ALTER:
					$sql = str_replace("CREATE TABLE IF NOT EXISTS `oc_", "CREATE TABLE IF NOT EXISTS `" . $data['db_prefix'], $sql);
					$sql = str_replace("ALTER TABLE `oc_", "ALTER TABLE `" . $data['db_prefix'], $sql);
					$sql = str_replace("INSERT IGNORE INTO `oc_", "INSERT IGNORE INTO `" . $data['db_prefix'], $sql);
					$sql = str_replace("TRUNCATE TABLE `oc_", "TRUNCATE TABLE `" . $data['db_prefix'], $sql);
					$sql = str_replace("UPDATE `oc_", "UPDATE `" . $data['db_prefix'], $sql);
					$sql = str_replace("DELETE FROM `oc_", "DELETE FROM `" . $data['db_prefix'], $sql);
					$sql = str_replace("DELETE FROM oc_", "DELETE FROM " . $data['db_prefix'], $sql);
					$db->query($sql);

					$sql = '';
				}
			} 
		}

		//nkt remove
		//$db->query("SET CHARACTER SET utf8");
		//$db->query("SET @@session.sql_mode = 'MYSQL40'"); 
		

		$db->query("DELETE FROM `" . $data['db_prefix'] . "user` WHERE user_id = '1'");

        //nkt: add firstname lastname họ tên
        $fl_name = extract_name_in_cli($data['fullname']);
        $first_name = isset($fl_name[1])?$fl_name[1]:'Admin';
        $last_name  = isset($fl_name[0])?$fl_name[0]:'Admin';
        //$first_name = $db->escape($first_name);
        //$last_name = $db->escape($last_name);        
		//d $db->query("INSERT INTO `" . $data['db_prefix'] . "user` SET user_id = '1', user_group_id = '1', username = '" . $db->escape($data['username']) . "', salt = '" . $db->escape($salt = token(9)) . "', password = '" . $db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', firstname = 'Admin', lastname = 'Admin', email = '" . $db->escape($data['email']) . "', status = '1', date_added = NOW()");
        $db->query("INSERT INTO `" . $data['db_prefix'] . "user` SET user_id = '1', user_group_id = '1', username = '" . $db->escape($data['username']) . "', salt = '" . $db->escape($salt = token(9)) . "', password = '" . $db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', firstname = '$first_name', lastname = '$last_name', email = '" . $db->escape($data['email']) . "', telephone = '" . $db->escape($data['phone_number']) . "', status = '1', date_added = NOW(), `type` = 'Admin'");


		$db->query("DELETE FROM `" . $data['db_prefix'] . "setting` WHERE `key` = 'config_email'");
		$db->query("INSERT INTO `" . $data['db_prefix'] . "setting` SET `code` = 'config', `key` = 'config_email', value = '" . $db->escape($data['email']) . "'");

		$db->query("DELETE FROM `" . $data['db_prefix'] . "setting` WHERE `key` = 'config_encryption'");
		$db->query("INSERT INTO `" . $data['db_prefix'] . "setting` SET `code` = 'config', `key` = 'config_encryption', value = '" . $db->escape(token(1024)) . "'");

		$db->query("UPDATE `" . $data['db_prefix'] . "product` SET `viewed` = '0'");

		$db->query("INSERT INTO `" . $data['db_prefix'] . "api` SET username = 'Default', `key` = '" . $db->escape(token(256)) . "', status = 1, date_added = NOW(), date_modified = NOW()");

		$api_id = $db->getLastId();

		$db->query("DELETE FROM `" . $data['db_prefix'] . "setting` WHERE `key` = 'config_api_id'");
		$db->query("INSERT INTO `" . $data['db_prefix'] . "setting` SET `code` = 'config', `key` = 'config_api_id', value = '" . (int)$api_id . "'");

		// add shop_name to table "setting"
		$db->query("INSERT INTO `" . $data['db_prefix'] . "setting` SET `store_id` = 0, `code` = 'config', `key` = 'shop_name', value = '" . $data['shop_name'] . "', `serialized` = 0");

        // v1.5.3: update email in builder config footer = email register
        $db->query("UPDATE `" . $data['db_prefix'] . "theme_builder_config` SET `config` = '{\n    \"name\": \"footer\",\n    \"text\": \"Footer\",\n    \"visible\": 1,\n    \"contact\": {\n        \"title\": \"Liên hệ chúng tôi\",\n        \"address\": \"Cau Giay - Ha Noi\",\n        \"phone-number\": \"" . $data['phone_number'] . "\",\n        \"email\": \"" . $data['email'] . "\",\n        \"visible\": 1\n    },\n    \"collection\": {\n        \"title\": \"Bộ sưu tập\",\n        \"menu_id\": 1,\n        \"visible\": 1\n    },\n    \"quick-links\": {\n        \"title\": \"Liên kết nhanh\",\n        \"menu_id\": 1,\n        \"visible\": 1\n    },\n    \"subscribe\": {\n        \"title\": \"Đăng ký theo dõi\",\n        \"social_network\": 1,\n        \"visible\": 1,\n        \"youtube_visible\": 1,\n        \"facebook_visible\": 1,\n        \"instagram_visible\": 1\n    }\n}' 
                    WHERE `config_theme` = 'fashion_shoezone' AND `key` = 'config_section_footer'");

        // v1.5.3: update default general setting
        // sample: UPDATE `oc_setting` SET `value` = '1' WHERE `code` = 'config' AND `key` = 'config_seo_url';
		$db->query("UPDATE `" . $data['db_prefix'] . "setting` SET `value` = '" . $data['shop_name'] . "' WHERE `code` = 'config' AND `key` = 'config_name'");
		$db->query("UPDATE `" . $data['db_prefix'] . "setting` SET `value` = '" . $data['phone_number'] . "' WHERE `code` = 'config' AND `key` = 'config_telephone'");
		$db->query("INSERT INTO `" . $data['db_prefix'] . "setting` SET `store_id` = 0, `code` = 'config', `key` = 'config_email_admin', value = '" . $data['email'] . "', `serialized` = 0");
		$db->query("INSERT INTO `" . $data['db_prefix'] . "setting` SET `store_id` = 0, `code` = 'config', `key` = 'config_email_marketing', value = '" . $data['email'] . "', `serialized` = 0");

		// v2.2.1: generate chatbot api key (support auto register new shop called from chatbot)
        $chatbot_api_key = token(32);
		$db->query("INSERT INTO `" . $data['db_prefix'] . "setting` SET `store_id` = 0, `code` = 'config', `key` = 'config_chatbot_novaon', value = '" . $chatbot_api_key . "', `serialized` = 0");
	}
}


function write_config_files($options) {
	//nkt:
	global $merchantid;
	return;
	
	$output  = '<?php' . "\n";
	$output .= '// HTTP' . "\n";
	$output .= 'define(\'HTTP_SERVER\', \'' . $options['http_server'] . '\');' . "\n";

	$output .= '// HTTPS' . "\n";
	$output .= 'define(\'HTTPS_SERVER\', \'' . $options['http_server'] . '\');' . "\n";

	$output .= '// DIR' . "\n";
	$output .= 'define(\'DIR_APPLICATION\', \'' . addslashes(DIR_OPENCART) . 'catalog/\');' . "\n";
	$output .= 'define(\'DIR_SYSTEM\', \'' . addslashes(DIR_OPENCART) . 'system/\');' . "\n";
	
	$output .= 'define(\'DIR_IMAGE\', \'' . addslashes(DIR_OPENCART) . 'image/'.$merchantid.'/\');' . "\n";

    tryDefiningDirStorage();

    $output .= 'define(\'DIR_STORAGE\', \'' . DIR_STORAGE . $merchantid .'/\');' . "\n";
	$output .= 'define(\'HTTP_IMAGE\', \''  . '/image/'.$merchantid.'/\');' . "\n";
	
	$output .= 'define(\'DIR_LANGUAGE\', DIR_APPLICATION . \'language/\');' . "\n";
	$output .= 'define(\'DIR_TEMPLATE\', DIR_APPLICATION . \'view/theme/\');' . "\n";
	$output .= 'define(\'DIR_CONFIG\', DIR_SYSTEM . \'config/\');' . "\n";
	$output .= 'define(\'DIR_CACHE\', DIR_STORAGE . \'cache/\');' . "\n";
	$output .= 'define(\'DIR_DOWNLOAD\', DIR_STORAGE . \'download/\');' . "\n";
	$output .= 'define(\'DIR_LOGS\', DIR_STORAGE . \'logs/\');' . "\n";
	$output .= 'define(\'DIR_MODIFICATION\', DIR_STORAGE . \'modification/\');' . "\n";
	$output .= 'define(\'DIR_SESSION\', DIR_STORAGE . \'session/\');' . "\n";
	$output .= 'define(\'DIR_UPLOAD\', DIR_STORAGE . \'upload/\');' . "\n\n";

	$output .= '// DB' . "\n";
	$output .= 'define(\'DB_DRIVER\', \'' . addslashes($options['db_driver']) . '\');' . "\n";
	$output .= 'define(\'DB_HOSTNAME\', \'' . addslashes($options['db_hostname']) . '\');' . "\n";
	$output .= 'define(\'DB_USERNAME\', \'' . addslashes($options['db_username']) . '\');' . "\n";
	$output .= 'define(\'DB_PASSWORD\', \'' . addslashes($options['db_password']) . '\');' . "\n";
	$output .= 'define(\'DB_DATABASE\', \'' . addslashes($options['db_database']) . '\');' . "\n";
	$output .= 'define(\'DB_PREFIX\', \'' . addslashes($options['db_prefix']) . '\');' . "\n";
	$output .= 'define(\'DB_PORT\', \'' . addslashes($options['db_port']) . '\');' . "\n";


	$file = fopen(DIR_OPENCART . 'config.php', 'w');

	fwrite($file, $output);

	fclose($file);

	$output  = '<?php' . "\n";
	$output .= '// HTTP' . "\n";
	$output .= 'define(\'HTTP_SERVER\', \'' . $options['http_server'] . 'admin/\');' . "\n";
	$output .= 'define(\'HTTP_CATALOG\', \'' . $options['http_server'] . '\');' . "\n";

	$output .= '// HTTPS' . "\n";
	$output .= 'define(\'HTTPS_SERVER\', \'' . $options['http_server'] . 'admin/\');' . "\n";
	$output .= 'define(\'HTTPS_CATALOG\', \'' . $options['http_server'] . '\');' . "\n";

	$output .= '// DIR' . "\n";
	$output .= 'define(\'DIR_APPLICATION\', \'' . addslashes(DIR_OPENCART) . 'admin/\');' . "\n";
	$output .= 'define(\'DIR_SYSTEM\', \'' . addslashes(DIR_OPENCART) . 'system/\');' . "\n";
	
	$output .= 'define(\'DIR_IMAGE\', \'' . addslashes(DIR_OPENCART) . 'image/'.$merchantid.'/\');' . "\n";
    $output .= 'define(\'DIR_STORAGE\', \'' . DIR_STORAGE . $merchantid .'/\');' . "\n";
	$output .= 'define(\'HTTP_IMAGE\', \''  . '/image/'.$merchantid.'/\');' . "\n";
	
	$output .= 'define(\'DIR_CATALOG\', \'' . addslashes(DIR_OPENCART) . 'catalog/\');' . "\n";
	$output .= 'define(\'DIR_LANGUAGE\', DIR_APPLICATION . \'language/\');' . "\n";
	$output .= 'define(\'DIR_TEMPLATE\', DIR_APPLICATION . \'view/template/\');' . "\n";
	$output .= 'define(\'DIR_CONFIG\', DIR_SYSTEM . \'config/\');' . "\n";
	$output .= 'define(\'DIR_CACHE\', DIR_STORAGE . \'cache/\');' . "\n";
	$output .= 'define(\'DIR_DOWNLOAD\', DIR_STORAGE . \'download/\');' . "\n";
	$output .= 'define(\'DIR_LOGS\', DIR_STORAGE . \'logs/\');' . "\n";
	$output .= 'define(\'DIR_MODIFICATION\', DIR_STORAGE . \'modification/\');' . "\n";
	$output .= 'define(\'DIR_SESSION\', DIR_STORAGE . \'session/\');' . "\n";
	$output .= 'define(\'DIR_UPLOAD\', DIR_STORAGE . \'upload/\');' . "\n\n";

	$output .= '// DB' . "\n";
	$output .= 'define(\'DB_DRIVER\', \'' . addslashes($options['db_driver']) . '\');' . "\n";
	$output .= 'define(\'DB_HOSTNAME\', \'' . addslashes($options['db_hostname']) . '\');' . "\n";
	$output .= 'define(\'DB_USERNAME\', \'' . addslashes($options['db_username']) . '\');' . "\n";
	$output .= 'define(\'DB_PASSWORD\', \'' . addslashes($options['db_password']) . '\');' . "\n";
	$output .= 'define(\'DB_DATABASE\', \'' . addslashes($options['db_database']) . '\');' . "\n";
	$output .= 'define(\'DB_PREFIX\', \'' . addslashes($options['db_prefix']) . '\');' . "\n";
	$output .= 'define(\'DB_PORT\', \'' . addslashes($options['db_port']) . '\');' . "\n";

	$output .= '// OpenCart API' . "\n";
	$output .= 'define(\'OPENCART_SERVER\', \'https://www.opencart.com/\');' . "\n";


	$file = fopen(DIR_OPENCART . 'admin/config.php', 'w');

	fwrite($file, $output);

	fclose($file);
}

function tryDefiningDirStorage()
{
    if (!defined('DIR_STORAGE')) {
        // chay lenh cli
        define('DIR_STORAGE', DIR_SYSTEM . 'storage/');
    }
}

function dir_permissions() {
	//nkt:
	global $merchantid;

    tryDefiningDirStorage();
	
	$dirs = array(
		DIR_OPENCART . "image/$merchantid/",
		DIR_STORAGE . "$merchantid/download/",
		DIR_STORAGE . "$merchantid/upload/",
		DIR_STORAGE . "$merchantid/cache/",
		DIR_STORAGE . "$merchantid/logs/",
		DIR_STORAGE . "$merchantid/modification/",
	    DIR_OPENCART . "asset/$merchantid/",
	);
	foreach($dirs as $dir){
		exec('mkdir -p "'.$dir.'"');
		//echo 'mkdir '.$dir.'\n';
	}
	exec('chmod o+w -R ' . implode(' ', $dirs));
}

/**
 * nkt:
 * notice: becareful function name "extract_name" with files in system helper such as systemp/helper/general.php
 * @param string $fullname
 * @return array
 */
function extract_name_in_cli($fullname){
	// temp for admin due to error Could not extract UTF-8 string:
	// return array('Admin','Admin');
    $fullname = sprintf ('"%s"',$fullname);    
    $fullname = json_decode($fullname);
    $fullname = trim($fullname);
    $delimiter = " ";
    //$fullname = str_replace("_", $delimiter, $fullname);
    $parts = explode($delimiter, $fullname);
    $lastname = array_pop($parts);
    $firstname = implode($delimiter, $parts);
    return array($firstname, $lastname);
}



switch ($subcommand) {

case "install":
	try {
		$options = get_options($argv);
		define('HTTP_OPENCART', $options['http_server']);
		$valid = valid($options);
		if (!$valid[0]) {
			echo "FAILED! Following inputs were missing or invalid: ";
			echo implode(', ', $valid[1]) . "\n\n";
			exit(1);
		}
		install($options);
		echo "SUCCESS! Opencart successfully installed on your server\n";
		echo "Store link: " . $options['http_server'] . "\n";
		echo "Admin link: " . $options['http_server'] . "admin/\n\n";
	} catch (ErrorException $e) {
		echo 'FAILED!: ' . $e->getMessage() . "\n";
		exit(1);
	}
	break;
case "usage":
default:
	echo usage();
}
