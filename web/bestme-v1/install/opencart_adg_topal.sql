-- -----------------------------------------------------------

--
-- Database: `opencart`
--

-- -----------------------------------------------------------

SET sql_mode = '';

--
-- Table structure for table `oc_address`
--

CREATE TABLE `oc_address` (
  `address_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(40) NOT NULL,
  `address` varchar(128) NOT NULL,
  `city` varchar(128) DEFAULT NULL,
  `district` varchar(128) DEFAULT NULL,
  `wards` varchar(128) DEFAULT NULL,
  `phone` varchar(32) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_api`
--

CREATE TABLE `oc_api` (
  `api_id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `key` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_api_ip`
--

CREATE TABLE `oc_api_ip` (
  `api_ip_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_api_session`
--

CREATE TABLE `oc_api_session` (
  `api_session_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute`
--

CREATE TABLE `oc_attribute` (
  `attribute_id` int(11) NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_description`
--

CREATE TABLE `oc_attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_group`
--

CREATE TABLE `oc_attribute_group` (
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_group_description`
--

CREATE TABLE `oc_attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_banner`
--

CREATE TABLE `oc_banner` (
  `banner_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_banner_image`
--

CREATE TABLE `oc_banner_image` (
  `banner_image_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_cart`
--

CREATE TABLE `oc_cart` (
  `cart_id` int(11) UNSIGNED NOT NULL,
  `api_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `option` text NOT NULL,
  `quantity` int(5) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category`
--

CREATE TABLE `oc_category` (
  `category_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_description`
--

CREATE TABLE `oc_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_filter`
--

CREATE TABLE `oc_category_filter` (
  `category_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_path`
--

CREATE TABLE `oc_category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_to_layout`
--

CREATE TABLE `oc_category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_to_store`
--

CREATE TABLE `oc_category_to_store` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_collection`
--

CREATE TABLE `oc_collection` (
  `collection_id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `sort_order` int(11) DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `product_type_sort` varchar(256) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_collection_description`
--

CREATE TABLE `oc_collection_description` (
  `collection_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_country`
--

CREATE TABLE `oc_country` (
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_country`
--

INSERT INTO `oc_country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1),
(2, 'Albania', 'AL', 'ALB', '', 0, 1),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1),
(5, 'Andorra', 'AD', 'AND', '', 0, 1),
(6, 'Angola', 'AO', 'AGO', '', 0, 1),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1),
(11, 'Armenia', 'AM', 'ARM', '', 0, 1),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1),
(13, 'Australia', 'AU', 'AUS', '', 0, 1),
(14, 'Austria', 'AT', 'AUT', '', 0, 1),
(15, 'Azerbaijan', 'AZ', 'AZE', '', 0, 1),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1),
(20, 'Belarus', 'BY', 'BLR', '', 0, 1),
(21, 'Belgium', 'BE', 'BEL', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', '', 0, 1),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1),
(38, 'Canada', 'CA', 'CAN', '', 0, 1),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1),
(42, 'Chad', 'TD', 'TCD', '', 0, 1),
(43, 'Chile', 'CL', 'CHL', '', 0, 1),
(44, 'China', 'CN', 'CHN', '', 0, 1),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1),
(47, 'Colombia', 'CO', 'COL', '', 0, 1),
(48, 'Comoros', 'KM', 'COM', '', 0, 1),
(49, 'Congo', 'CG', 'COG', '', 0, 1),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1),
(52, 'Cote D\'Ivoire', 'CI', 'CIV', '', 0, 1),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1),
(61, 'East Timor', 'TL', 'TLS', '', 0, 1),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1),
(67, 'Estonia', 'EE', 'EST', '', 0, 1),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1),
(72, 'Finland', 'FI', 'FIN', '', 0, 1),
(74, 'France, Metropolitan', 'FR', 'FRA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1),
(80, 'Georgia', 'GE', 'GEO', '', 0, 1),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1),
(84, 'Greece', 'GR', 'GRC', '', 0, 1),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1),
(88, 'Guam', 'GU', 'GUM', '', 0, 1),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1),
(91, 'Guinea-Bissau', 'GW', 'GNB', '', 0, 1),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1),
(95, 'Honduras', 'HN', 'HND', '', 0, 1),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1),
(99, 'India', 'IN', 'IND', '', 0, 1),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1),
(104, 'Israel', 'IL', 'ISR', '', 0, 1),
(105, 'Italy', 'IT', 'ITA', '', 0, 1),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1),
(107, 'Japan', 'JP', 'JPN', '', 0, 1),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', '', 0, 1),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1),
(113, 'South Korea', 'KR', 'KOR', '', 0, 1),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', '', 0, 1),
(116, 'Lao People\'s Democratic Republic', 'LA', 'LAO', '', 0, 1),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1),
(125, 'Macau', 'MO', 'MAC', '', 0, 1),
(126, 'FYROM', 'MK', 'MKD', '', 0, 1),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1),
(131, 'Mali', 'ML', 'MLI', '', 0, 1),
(132, 'Malta', 'MT', 'MLT', '', 0, 1),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1),
(140, 'Moldova, Republic of', 'MD', 'MDA', '', 0, 1),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1),
(155, 'Niger', 'NE', 'NER', '', 0, 1),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1),
(157, 'Niue', 'NU', 'NIU', '', 0, 1),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1),
(160, 'Norway', 'NO', 'NOR', '', 0, 1),
(161, 'Oman', 'OM', 'OMN', '', 0, 1),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1),
(163, 'Palau', 'PW', 'PLW', '', 0, 1),
(164, 'Panama', 'PA', 'PAN', '', 0, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1),
(167, 'Peru', 'PE', 'PER', '', 0, 1),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1),
(170, 'Poland', 'PL', 'POL', '', 0, 1),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1),
(174, 'Reunion', 'RE', 'REU', '', 0, 1),
(175, 'Romania', 'RO', 'ROM', '', 0, 1),
(176, 'Russian Federation', 'RU', 'RUS', '', 0, 1),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1),
(195, 'Spain', 'ES', 'ESP', '', 0, 1),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1),
(203, 'Sweden', 'SE', 'SWE', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1),
(207, 'Tajikistan', 'TJ', 'TJK', '', 0, 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1),
(209, 'Thailand', 'TH', 'THA', '', 0, 1),
(210, 'Togo', 'TG', 'TGO', '', 0, 1),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1),
(212, 'Tonga', 'TO', 'TON', '', 0, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1),
(216, 'Turkmenistan', 'TM', 'TKM', '', 0, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1),
(220, 'Ukraine', 'UA', 'UKR', '', 0, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1),
(226, 'Uzbekistan', 'UZ', 'UZB', '', 0, 1),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1),
(242, 'Montenegro', 'ME', 'MNE', '', 0, 1),
(243, 'Serbia', 'RS', 'SRB', '', 0, 1),
(244, 'Aaland Islands', 'AX', 'ALA', '', 0, 1),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '', 0, 1),
(246, 'Curacao', 'CW', 'CUW', '', 0, 1),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', '', 0, 1),
(248, 'South Sudan', 'SS', 'SSD', '', 0, 1),
(249, 'St. Barthelemy', 'BL', 'BLM', '', 0, 1),
(250, 'St. Martin (French part)', 'MF', 'MAF', '', 0, 1),
(251, 'Canary Islands', 'IC', 'ICA', '', 0, 1),
(252, 'Ascension Island (British)', 'AC', 'ASC', '', 0, 1),
(253, 'Kosovo, Republic of', 'XK', 'UNK', '', 0, 1),
(254, 'Isle of Man', 'IM', 'IMN', '', 0, 1),
(255, 'Tristan da Cunha', 'TA', 'SHN', '', 0, 1),
(256, 'Guernsey', 'GG', 'GGY', '', 0, 1),
(257, 'Jersey', 'JE', 'JEY', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon`
--

CREATE TABLE `oc_coupon` (
  `coupon_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(20) NOT NULL,
  `type` char(1) NOT NULL,
  `discount` decimal(15,4) NOT NULL,
  `logged` tinyint(1) NOT NULL,
  `shipping` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `uses_total` int(11) NOT NULL,
  `uses_customer` varchar(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_category`
--

CREATE TABLE `oc_coupon_category` (
  `coupon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_history`
--

CREATE TABLE `oc_coupon_history` (
  `coupon_history_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_product`
--

CREATE TABLE `oc_coupon_product` (
  `coupon_product_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_currency`
--

CREATE TABLE `oc_currency` (
  `currency_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` double(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_currency`
--

INSERT INTO `oc_currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(1, 'Pound Sterling', 'GBP', '£', '', '2', 29720.44000000, 1, '2019-01-09 16:40:33'),
(4, 'US Dollar', 'USD', '$', '', '2', 23245.00000000, 1, '2019-01-09 16:45:56'),
(3, 'Euro', 'EUR', '', '€', '2', 27137.74000000, 1, '2019-01-09 16:39:38'),
(2, 'Vietnam đồng', 'VND', '', 'đ', '0', 1.00000000, 1, '2019-05-16 08:11:36');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer`
--

CREATE TABLE `oc_customer` (
  `customer_id` int(11) NOT NULL,
  `customer_group_id` int(11) DEFAULT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) DEFAULT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) DEFAULT NULL,
  `password` varchar(40) NOT NULL,
  `password_temp` varchar(100) DEFAULT NULL,
  `salt` varchar(9) NOT NULL,
  `salt_temp` varchar(9) DEFAULT NULL,
  `cart` text,
  `wishlist` text,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `customer_source` varchar(128) DEFAULT NULL,
  `note` text,
  `custom_field` text,
  `ip` varchar(40) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `safe` tinyint(1) DEFAULT NULL,
  `token` text,
  `code` varchar(40) DEFAULT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_activity`
--

CREATE TABLE `oc_customer_activity` (
  `customer_activity_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_affiliate`
--

CREATE TABLE `oc_customer_affiliate` (
  `customer_id` int(11) NOT NULL,
  `company` varchar(40) NOT NULL,
  `website` varchar(255) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `commission` decimal(4,2) NOT NULL DEFAULT '0.00',
  `tax` varchar(64) NOT NULL,
  `payment` varchar(6) NOT NULL,
  `cheque` varchar(100) NOT NULL,
  `paypal` varchar(64) NOT NULL,
  `bank_name` varchar(64) NOT NULL,
  `bank_branch_number` varchar(64) NOT NULL,
  `bank_swift_code` varchar(64) NOT NULL,
  `bank_account_name` varchar(64) NOT NULL,
  `bank_account_number` varchar(64) NOT NULL,
  `custom_field` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_approval`
--

CREATE TABLE `oc_customer_approval` (
  `customer_approval_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `type` varchar(9) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_group`
--

CREATE TABLE `oc_customer_group` (
  `customer_group_id` int(11) NOT NULL,
  `approval` int(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_group_description`
--

CREATE TABLE `oc_customer_group_description` (
  `customer_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_history`
--

CREATE TABLE `oc_customer_history` (
  `customer_history_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_ip`
--

CREATE TABLE `oc_customer_ip` (
  `customer_ip_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_login`
--

CREATE TABLE `oc_customer_login` (
  `customer_login_id` int(11) NOT NULL,
  `email` varchar(96) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `total` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_online`
--

CREATE TABLE `oc_customer_online` (
  `ip` varchar(40) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `referer` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_reward`
--

CREATE TABLE `oc_customer_reward` (
  `customer_reward_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_search`
--

CREATE TABLE `oc_customer_search` (
  `customer_search_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category` tinyint(1) NOT NULL,
  `description` tinyint(1) NOT NULL,
  `products` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_transaction`
--

CREATE TABLE `oc_customer_transaction` (
  `customer_transaction_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_wishlist`
--

CREATE TABLE `oc_customer_wishlist` (
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field`
--

CREATE TABLE `oc_custom_field` (
  `custom_field_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `validation` varchar(255) NOT NULL,
  `location` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_customer_group`
--

CREATE TABLE `oc_custom_field_customer_group` (
  `custom_field_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_description`
--

CREATE TABLE `oc_custom_field_description` (
  `custom_field_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_value`
--

CREATE TABLE `oc_custom_field_value` (
  `custom_field_value_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_value_description`
--

CREATE TABLE `oc_custom_field_value_description` (
  `custom_field_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_download`
--

CREATE TABLE `oc_download` (
  `download_id` int(11) NOT NULL,
  `filename` varchar(160) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_download_description`
--

CREATE TABLE `oc_download_description` (
  `download_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_event`
--

CREATE TABLE `oc_event` (
  `event_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `trigger` text NOT NULL,
  `action` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_event`
--

INSERT INTO `oc_event` (`event_id`, `code`, `trigger`, `action`, `status`, `sort_order`) VALUES
(1, 'activity_customer_add', 'catalog/model/account/customer/addCustomer/after', 'event/activity/addCustomer', 1, 0),
(2, 'activity_customer_edit', 'catalog/model/account/customer/editCustomer/after', 'event/activity/editCustomer', 1, 0),
(3, 'activity_customer_password', 'catalog/model/account/customer/editPassword/after', 'event/activity/editPassword', 1, 0),
(4, 'activity_customer_forgotten', 'catalog/model/account/customer/editCode/after', 'event/activity/forgotten', 1, 0),
(5, 'activity_transaction', 'catalog/model/account/customer/addTransaction/after', 'event/activity/addTransaction', 1, 0),
(6, 'activity_customer_login', 'catalog/model/account/customer/deleteLoginAttempts/after', 'event/activity/login', 1, 0),
(7, 'activity_address_add', 'catalog/model/account/address/addAddress/after', 'event/activity/addAddress', 1, 0),
(8, 'activity_address_edit', 'catalog/model/account/address/editAddress/after', 'event/activity/editAddress', 1, 0),
(9, 'activity_address_delete', 'catalog/model/account/address/deleteAddress/after', 'event/activity/deleteAddress', 1, 0),
(10, 'activity_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'event/activity/addAffiliate', 1, 0),
(11, 'activity_affiliate_edit', 'catalog/model/account/customer/editAffiliate/after', 'event/activity/editAffiliate', 1, 0),
(12, 'activity_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'event/activity/addOrderHistory', 1, 0),
(13, 'activity_return_add', 'catalog/model/account/return/addReturn/after', 'event/activity/addReturn', 1, 0),
(14, 'mail_transaction', 'catalog/model/account/customer/addTransaction/after', 'mail/transaction', 1, 0),
(15, 'mail_forgotten', 'catalog/model/account/customer/editCode/after', 'mail/forgotten', 1, 0),
(16, 'mail_customer_add', 'catalog/model/account/customer/addCustomer/after', 'mail/register', 1, 0),
(17, 'mail_customer_alert', 'catalog/model/account/customer/addCustomer/after', 'mail/register/alert', 1, 0),
(18, 'mail_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate', 1, 0),
(19, 'mail_affiliate_alert', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate/alert', 1, 0),
(20, 'mail_voucher', 'catalog/model/checkout/order/addOrderHistory/after', 'extension/total/voucher/send', 1, 0),
(21, 'mail_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order', 1, 0),
(22, 'mail_order_alert', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order/alert', 1, 0),
(23, 'statistics_review_add', 'catalog/model/catalog/review/addReview/after', 'event/statistics/addReview', 1, 0),
(24, 'statistics_return_add', 'catalog/model/account/return/addReturn/after', 'event/statistics/addReturn', 1, 0),
(25, 'statistics_order_history', 'catalog/model/checkout/order/addOrderHistory/after', 'event/statistics/addOrderHistory', 1, 0),
(26, 'admin_mail_affiliate_approve', 'admin/model/customer/customer_approval/approveAffiliate/after', 'mail/affiliate/approve', 1, 0),
(27, 'admin_mail_affiliate_deny', 'admin/model/customer/customer_approval/denyAffiliate/after', 'mail/affiliate/deny', 1, 0),
(28, 'admin_mail_customer_approve', 'admin/model/customer/customer_approval/approveCustomer/after', 'mail/customer/approve', 1, 0),
(29, 'admin_mail_customer_deny', 'admin/model/customer/customer_approval/denyCustomer/after', 'mail/customer/deny', 1, 0),
(30, 'admin_mail_reward', 'admin/model/customer/customer/addReward/after', 'mail/reward', 1, 0),
(31, 'admin_mail_transaction', 'admin/model/customer/customer/addTransaction/after', 'mail/transaction', 1, 0),
(32, 'admin_mail_return', 'admin/model/sale/return/addReturn/after', 'mail/return', 1, 0),
(33, 'admin_mail_forgotten', 'admin/model/user/user/editCode/after', 'mail/forgotten', 1, 0),
(34, 'add order', 'admin/model/sale/order/addOrder/after', 'event/statistics/createOrder', 1, 0),
(35, 'edit order', 'admin/model/sale/order/updateProductOrder/after', 'event/statistics/editOrder', 1, 0),
(36, 'change status order', 'admin/model/sale/order/updateStatus/after', 'event/statistics/changeStatus', 1, 0),
(37, 'delete order', 'admin/model/sale/order/deleteOrderById/after', 'event/statistics/deleteOrder', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_extension`
--

CREATE TABLE `oc_extension` (
  `extension_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_extension`
--

INSERT INTO `oc_extension` (`extension_id`, `type`, `code`) VALUES
(1, 'payment', 'cod'),
(2, 'total', 'shipping'),
(3, 'total', 'sub_total'),
(4, 'total', 'tax'),
(5, 'total', 'total'),
(6, 'module', 'banner'),
(7, 'module', 'carousel'),
(8, 'total', 'credit'),
(9, 'shipping', 'flat'),
(10, 'total', 'handling'),
(11, 'total', 'low_order_fee'),
(12, 'total', 'coupon'),
(13, 'module', 'category'),
(14, 'module', 'account'),
(15, 'total', 'reward'),
(16, 'total', 'voucher'),
(17, 'payment', 'free_checkout'),
(18, 'module', 'featured'),
(19, 'module', 'slideshow'),
(21, 'dashboard', 'activity'),
(22, 'dashboard', 'sale'),
(23, 'dashboard', 'recent'),
(24, 'dashboard', 'order'),
(25, 'dashboard', 'online'),
(26, 'dashboard', 'map'),
(27, 'dashboard', 'customer'),
(28, 'dashboard', 'chart'),
(29, 'report', 'sale_coupon'),
(31, 'report', 'customer_search'),
(32, 'report', 'customer_transaction'),
(33, 'report', 'product_purchased'),
(34, 'report', 'product_viewed'),
(35, 'report', 'sale_return'),
(36, 'report', 'sale_order'),
(37, 'report', 'sale_shipping'),
(38, 'report', 'sale_tax'),
(39, 'report', 'customer_activity'),
(40, 'report', 'customer_order'),
(41, 'report', 'customer_reward'),
(42, 'module', 'theme_builder_config'),
(159, 'theme', 'adg_topal_xfec');

-- --------------------------------------------------------

--
-- Table structure for table `oc_extension_install`
--

CREATE TABLE `oc_extension_install` (
  `extension_install_id` int(11) NOT NULL,
  `extension_download_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_extension_path`
--

CREATE TABLE `oc_extension_path` (
  `extension_path_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter`
--

CREATE TABLE `oc_filter` (
  `filter_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter`
--

INSERT INTO `oc_filter` (`filter_id`, `filter_group_id`, `sort_order`) VALUES
(1, 1, 0),
(2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_description`
--

CREATE TABLE `oc_filter_description` (
  `filter_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter_description`
--

INSERT INTO `oc_filter_description` (`filter_id`, `language_id`, `filter_group_id`, `name`) VALUES
(1, 2, 1, 'Iphone'),
(1, 1, 1, 'Iphone'),
(2, 2, 1, 'Oppo'),
(2, 1, 1, 'Oppo');

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_group`
--

CREATE TABLE `oc_filter_group` (
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter_group`
--

INSERT INTO `oc_filter_group` (`filter_group_id`, `sort_order`) VALUES
(1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_group_description`
--

CREATE TABLE `oc_filter_group_description` (
  `filter_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter_group_description`
--

INSERT INTO `oc_filter_group_description` (`filter_group_id`, `language_id`, `name`) VALUES
(1, 1, 'filter group'),
(1, 2, 'Nhóm bộ lọc');

-- --------------------------------------------------------

--
-- Table structure for table `oc_geo_zone`
--

CREATE TABLE `oc_geo_zone` (
  `geo_zone_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_geo_zone`
--

INSERT INTO `oc_geo_zone` (`geo_zone_id`, `name`, `description`, `date_added`, `date_modified`) VALUES
(3, 'UK VAT Zone', 'UK VAT', '2009-01-06 23:26:25', '2010-02-26 22:33:24'),
(4, 'UK Shipping', 'UK Shipping Zones', '2009-06-23 01:14:53', '2010-12-15 15:18:13');

-- --------------------------------------------------------

--
-- Table structure for table `oc_information`
--

CREATE TABLE `oc_information` (
  `information_id` int(11) NOT NULL,
  `bottom` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_description`
--

CREATE TABLE `oc_information_description` (
  `information_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` mediumtext NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_to_layout`
--

CREATE TABLE `oc_information_to_layout` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_to_store`
--

CREATE TABLE `oc_information_to_store` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_language`
--

CREATE TABLE `oc_language` (
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_language`
--

INSERT INTO `oc_language` (`language_id`, `name`, `code`, `locale`, `image`, `directory`, `sort_order`, `status`) VALUES
(1, 'English', 'en-gb', 'en-US,en_US.UTF-8,en_US,en-gb,english', 'en-gb.png', 'english', 2, 0),
(2, 'Vietnamese', 'vi-vn', 'vi-VN,vi_VN.UTF-8,vi_VN,vi-vn,vietnamese', 'vi-vn.png', 'vietnamese', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout`
--

CREATE TABLE `oc_layout` (
  `layout_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_layout`
--

INSERT INTO `oc_layout` (`layout_id`, `name`) VALUES
(1, 'Home'),
(2, 'Product'),
(3, 'Category'),
(4, 'Default'),
(5, 'Manufacturer'),
(6, 'Account'),
(7, 'Checkout'),
(8, 'Contact'),
(9, 'Sitemap'),
(10, 'Affiliate'),
(11, 'Information'),
(12, 'Compare'),
(13, 'Search');

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout_module`
--

CREATE TABLE `oc_layout_module` (
  `layout_module_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `position` varchar(14) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_layout_module`
--

INSERT INTO `oc_layout_module` (`layout_module_id`, `layout_id`, `code`, `position`, `sort_order`) VALUES
(2, 4, '0', 'content_top', 0),
(3, 4, '0', 'content_top', 1),
(20, 5, '0', 'column_left', 2),
(69, 10, 'account', 'column_right', 1),
(68, 6, 'account', 'column_right', 1),
(67, 1, 'carousel.29', 'content_top', 3),
(66, 1, 'slideshow.27', 'content_top', 1),
(65, 1, 'featured.28', 'content_top', 2),
(72, 3, 'category', 'column_left', 1),
(73, 3, 'banner.30', 'column_left', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout_route`
--

CREATE TABLE `oc_layout_route` (
  `layout_route_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_layout_route`
--

INSERT INTO `oc_layout_route` (`layout_route_id`, `layout_id`, `store_id`, `route`) VALUES
(38, 6, 0, 'account/%'),
(17, 10, 0, 'affiliate/%'),
(44, 3, 0, 'product/category'),
(42, 1, 0, 'common/home'),
(20, 2, 0, 'product/product'),
(24, 11, 0, 'information/information'),
(23, 7, 0, 'checkout/%'),
(31, 8, 0, 'information/contact'),
(32, 9, 0, 'information/sitemap'),
(34, 4, 0, ''),
(45, 5, 0, 'product/manufacturer'),
(52, 12, 0, 'product/compare'),
(53, 13, 0, 'product/search');

-- --------------------------------------------------------

--
-- Table structure for table `oc_length_class`
--

CREATE TABLE `oc_length_class` (
  `length_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_length_class`
--

INSERT INTO `oc_length_class` (`length_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '10.00000000'),
(3, '0.39370000');

-- --------------------------------------------------------

--
-- Table structure for table `oc_length_class_description`
--

CREATE TABLE `oc_length_class_description` (
  `length_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_length_class_description`
--

INSERT INTO `oc_length_class_description` (`length_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Centimeter', 'cm'),
(2, 1, 'Millimeter', 'mm'),
(3, 1, 'Inch', 'in'),
(1, 2, 'Centimeter', 'cm'),
(2, 2, 'Millimeter', 'mm'),
(3, 2, 'Inch', 'in');

-- --------------------------------------------------------

--
-- Table structure for table `oc_location`
--

CREATE TABLE `oc_location` (
  `location_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `address` text NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `geocode` varchar(32) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `open` text NOT NULL,
  `comment` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_manufacturer`
--

CREATE TABLE `oc_manufacturer` (
  `manufacturer_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_manufacturer_to_store`
--

CREATE TABLE `oc_manufacturer_to_store` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_marketing`
--

CREATE TABLE `oc_marketing` (
  `marketing_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(64) NOT NULL,
  `clicks` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_modification`
--

CREATE TABLE `oc_modification` (
  `modification_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(64) NOT NULL,
  `author` varchar(64) NOT NULL,
  `version` varchar(32) NOT NULL,
  `link` varchar(255) NOT NULL,
  `xml` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_module`
--

CREATE TABLE `oc_module` (
  `module_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(32) NOT NULL,
  `setting` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_module`
--

INSERT INTO `oc_module` (`module_id`, `name`, `code`, `setting`) VALUES
(30, 'Category', 'banner', '{"name":"Category","banner_id":"6","width":"182","height":"182","status":"1"}'),
(29, 'Home Page', 'carousel', '{"name":"Home Page","banner_id":"8","width":"130","height":"100","status":"1"}'),
(28, 'Home Page', 'featured', '{"name":"Home Page","product":["43","40","42","30"],"limit":"4","width":"200","height":"200","status":"1"}'),
(27, 'Home Page', 'slideshow', '{"name":"Home Page","banner_id":"7","width":"1140","height":"380","status":"1"}'),
(31, 'Banner 1', 'banner', '{"name":"Banner 1","banner_id":"6","width":"182","height":"182","status":"1"}');

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_group_menu`
--

CREATE TABLE `oc_novaon_group_menu` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_novaon_group_menu`
--

INSERT INTO `oc_novaon_group_menu` (`id`, `parent_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 1, '2019-01-09 16:05:17', '2019-01-09 16:05:17'),
(2, 0, 1, '2019-01-09 16:06:51', '2019-01-09 16:06:51'),
(3, 0, 1, '2019-01-09 16:09:21', '2019-01-09 16:09:21'),
(4, 0, 1, '2019-01-09 16:11:17', '2019-01-09 16:11:17'),
(5, 0, 1, '2019-01-09 16:12:50', '2019-01-09 16:12:50');

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_group_menu_description`
--

CREATE TABLE `oc_novaon_group_menu_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `group_menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_novaon_group_menu_description`
--

INSERT INTO `oc_novaon_group_menu_description` (`id`, `language_id`, `name`, `group_menu_id`) VALUES
(1, 2, 'Menu chính', 1),
(2, 1, 'Menu chính', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_menu_item`
--

CREATE TABLE `oc_novaon_menu_item` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `group_menu_id` int(11) NOT NULL COMMENT 'id của bảng group_menu',
  `status` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_novaon_menu_item`
--

INSERT INTO `oc_novaon_menu_item` (`id`, `parent_id`, `group_menu_id`, `status`, `position`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 1, 0, '2019-01-09 16:05:17', '2019-01-09 16:05:17'),
(2, 0, 1, 1, 0, '2019-01-09 16:05:17', '2019-01-09 16:05:17'),
(3, 0, 1, 1, 0, '2019-01-09 16:05:18', '2019-01-09 16:05:18');

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_menu_item_description`
--

CREATE TABLE `oc_novaon_menu_item_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `menu_item_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_novaon_menu_item_description`
--

INSERT INTO `oc_novaon_menu_item_description` (`id`, `language_id`, `name`, `menu_item_id`) VALUES
(1, 2, 'Trang chủ', 1),
(2, 1, 'Trang chủ', 1),
(3, 2, 'Sản phẩm', 2),
(4, 1, 'Sản phẩm', 2),
(5, 2, 'Liên hệ', 3),
(6, 1, 'Liên hệ', 3);

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_relation_table`
--

CREATE TABLE `oc_novaon_relation_table` (
  `id` int(11) NOT NULL,
  `main_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên bảng chính(ví dụ group_menu)',
  `main_id` int(11) NOT NULL COMMENT 'id bảng chính, ví dụ group_menu_id =1)',
  `child_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên bảng con ví dụ menu_item',
  `child_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL COMMENT 'type_id =1 là sản phẩm type_id =2 là danh mục sản phẩm type_id =3 là bài viết type_id =4 là danh mục bài viết type_id =5 là url',
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `redirect` int(11) NOT NULL DEFAULT '0' COMMENT 'nếu = 1 là khi click vào sẽ redirect đến đâu, 0 là dùng dể hover vào show ra các tphan con',
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `weight_number` int(11) NOT NULL COMMENT 'trọng số hiển thị'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_novaon_relation_table`
--

INSERT INTO `oc_novaon_relation_table` (`id`, `main_name`, `main_id`, `child_name`, `child_id`, `type_id`, `url`, `redirect`, `title`, `weight_number`) VALUES
(1, 'menu_item', 1, '', 0, 5, '?route=common/home', 1, '', 0),
(2, 'menu_item', 2, '', 0, 5, '?route=common/shop', 1, '', 0),
(3, 'menu_item', 3, '', 0, 5, '?route=contact/contact', 1, '', 0),
(4, 'group_menu', 1, 'menu_item', 1, 5, '', 0, '', 0),
(5, 'group_menu', 1, 'menu_item', 2, 5, '', 0, '', 0),
(6, 'group_menu', 1, 'menu_item', 3, 5, '', 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_type`
--

CREATE TABLE `oc_novaon_type` (
  `id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `weight_number` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_novaon_type`
--

INSERT INTO `oc_novaon_type` (`id`, `status`, `weight_number`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_type_description`
--

CREATE TABLE `oc_novaon_type_description` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_novaon_type_description`
--

INSERT INTO `oc_novaon_type_description` (`id`, `type_id`, `language_id`, `name`) VALUES
(1, 1, 1, 'Product'),
(2, 1, 2, 'Sản phẩm'),
(3, 2, 1, 'Category'),
(4, 2, 2, 'Danh mục sản phẩm'),
(5, 3, 1, 'Blog'),
(6, 3, 2, 'Bài viết'),
(7, 4, 1, 'Blog category'),
(8, 4, 2, 'Danh mục bài viết'),
(9, 5, 1, 'Url'),
(10, 5, 2, 'Url');

-- --------------------------------------------------------

--
-- Table structure for table `oc_option`
--

CREATE TABLE `oc_option` (
  `option_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option`
--

INSERT INTO `oc_option` (`option_id`, `type`, `sort_order`) VALUES
(1, 'radio', 1),
(2, 'checkbox', 2),
(4, 'text', 3),
(5, 'select', 4),
(6, 'textarea', 5),
(7, 'file', 6),
(8, 'date', 7),
(9, 'time', 8),
(10, 'datetime', 9),
(11, 'select', 10),
(12, 'date', 11);

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_description`
--

CREATE TABLE `oc_option_description` (
  `option_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option_description`
--

INSERT INTO `oc_option_description` (`option_id`, `language_id`, `name`) VALUES
(1, 1, 'Radio'),
(2, 1, 'Checkbox'),
(4, 1, 'Text'),
(6, 1, 'Textarea'),
(8, 1, 'Date'),
(7, 1, 'File'),
(5, 1, 'Select'),
(9, 1, 'Time'),
(10, 1, 'Date &amp; Time'),
(12, 1, 'Delivery Date'),
(11, 1, 'Size');

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_value`
--

CREATE TABLE `oc_option_value` (
  `option_value_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option_value`
--

INSERT INTO `oc_option_value` (`option_value_id`, `option_id`, `image`, `sort_order`) VALUES
(43, 1, '', 3),
(32, 1, '', 1),
(45, 2, '', 4),
(44, 2, '', 3),
(42, 5, '', 4),
(41, 5, '', 3),
(39, 5, '', 1),
(40, 5, '', 2),
(31, 1, '', 2),
(23, 2, '', 1),
(24, 2, '', 2),
(46, 11, '', 1),
(47, 11, '', 2),
(48, 11, '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_value_description`
--

CREATE TABLE `oc_option_value_description` (
  `option_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option_value_description`
--

INSERT INTO `oc_option_value_description` (`option_value_id`, `language_id`, `option_id`, `name`) VALUES
(43, 1, 1, 'Large'),
(32, 1, 1, 'Small'),
(45, 1, 2, 'Checkbox 4'),
(44, 1, 2, 'Checkbox 3'),
(31, 1, 1, 'Medium'),
(42, 1, 5, 'Yellow'),
(41, 1, 5, 'Green'),
(39, 1, 5, 'Red'),
(40, 1, 5, 'Blue'),
(23, 1, 2, 'Checkbox 1'),
(24, 1, 2, 'Checkbox 2'),
(48, 1, 11, 'Large'),
(47, 1, 11, 'Medium'),
(46, 1, 11, 'Small');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order`
--

CREATE TABLE `oc_order` (
  `order_id` int(11) NOT NULL,
  `order_code` varchar(50) DEFAULT NULL,
  `invoice_no` int(11) NOT NULL DEFAULT '0',
  `invoice_prefix` varchar(26) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `store_name` varchar(64) NOT NULL,
  `store_url` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `fullname` varchar(255) DEFAULT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `order_payment` int(11) DEFAULT NULL,
  `order_transfer` int(11) DEFAULT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `custom_field` text NOT NULL,
  `payment_firstname` varchar(32) NOT NULL,
  `payment_lastname` varchar(32) NOT NULL,
  `payment_company` varchar(60) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(10) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `payment_country_id` int(11) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_zone_id` int(11) NOT NULL,
  `payment_address_format` text NOT NULL,
  `payment_custom_field` text NOT NULL,
  `payment_method` varchar(128) NOT NULL,
  `payment_code` varchar(128) NOT NULL,
  `payment_status` tinyint(4) NOT NULL DEFAULT '0',
  `shipping_firstname` varchar(32) NOT NULL,
  `shipping_lastname` varchar(32) NOT NULL,
  `shipping_company` varchar(40) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(10) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `shipping_ward_code` varchar(50) DEFAULT NULL,
  `shipping_district_code` varchar(50) DEFAULT NULL,
  `shipping_province_code` varchar(50) DEFAULT NULL,
  `shipping_country_id` int(11) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_zone_id` int(11) NOT NULL,
  `shipping_address_format` text NOT NULL,
  `shipping_custom_field` text NOT NULL,
  `shipping_method` varchar(128) NOT NULL,
  `shipping_method_value` int(11) DEFAULT NULL,
  `shipping_fee` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `shipping_code` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `order_cancel_comment` varchar(500) DEFAULT NULL,
  `total` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `marketing_id` int(11) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL DEFAULT '1.00000000',
  `ip` varchar(40) NOT NULL,
  `forwarded_ip` varchar(40) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `accept_language` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_history`
--

CREATE TABLE `oc_order_history` (
  `order_history_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(256) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_option`
--

CREATE TABLE `oc_order_option` (
  `order_option_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_product`
--

CREATE TABLE `oc_order_product` (
  `order_product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_version_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `reward` int(8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_recurring`
--

CREATE TABLE `oc_order_recurring` (
  `order_recurring_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `recurring_name` varchar(255) NOT NULL,
  `recurring_description` varchar(255) NOT NULL,
  `recurring_frequency` varchar(25) NOT NULL,
  `recurring_cycle` smallint(6) NOT NULL,
  `recurring_duration` smallint(6) NOT NULL,
  `recurring_price` decimal(10,4) NOT NULL,
  `trial` tinyint(1) NOT NULL,
  `trial_frequency` varchar(25) NOT NULL,
  `trial_cycle` smallint(6) NOT NULL,
  `trial_duration` smallint(6) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_recurring_transaction`
--

CREATE TABLE `oc_order_recurring_transaction` (
  `order_recurring_transaction_id` int(11) NOT NULL,
  `order_recurring_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `amount` decimal(10,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_shipment`
--

CREATE TABLE `oc_order_shipment` (
  `order_shipment_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `shipping_courier_id` varchar(255) NOT NULL DEFAULT '',
  `tracking_number` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_status`
--

CREATE TABLE `oc_order_status` (
  `order_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_order_status`
--

INSERT INTO `oc_order_status` (`order_status_id`, `language_id`, `name`) VALUES
(1, 1, 'Draft'),
(2, 1, 'Processing'),
(3, 1, 'Delivering'),
(5, 1, 'Canceled'),
(4, 1, 'Complete'),
(6, 2, 'Nháp'),
(7, 2, 'Đang xử lý'),
(8, 2, 'Đang giao hàng '),
(9, 2, 'Hoàn thành'),
(10, 2, 'Đã hủy');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_tag`
--

CREATE TABLE `oc_order_tag` (
  `order_tag_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_total`
--

CREATE TABLE `oc_order_total` (
  `order_total_id` int(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_voucher`
--

CREATE TABLE `oc_order_voucher` (
  `order_voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_page_contents`
--

CREATE TABLE `oc_page_contents` (
  `page_id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `seo_title` varchar(256) DEFAULT NULL,
  `seo_description` text,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product`
--

CREATE TABLE `oc_product` (
  `product_id` int(11) NOT NULL,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `barcode` varchar(128) DEFAULT NULL,
  `upc` varchar(12) NOT NULL,
  `ean` varchar(14) NOT NULL,
  `jan` varchar(13) NOT NULL,
  `isbn` varchar(17) NOT NULL,
  `mpn` varchar(64) NOT NULL,
  `location` varchar(128) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `sale_on_out_of_stock` tinyint(4) NOT NULL DEFAULT '0',
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_alt` varchar(256) DEFAULT NULL,
  `multi_versions` tinyint(1) NOT NULL DEFAULT '0',
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `price_currency_id` int(11) NOT NULL,
  `compare_price` decimal(15,4) DEFAULT NULL,
  `c_price_currency_id` int(11) NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `tax_class_id` int(11) NOT NULL,
  `date_available` date NOT NULL DEFAULT '0000-00-00',
  `weight` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `weight_class_id` int(11) NOT NULL DEFAULT '0',
  `length` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `width` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `height` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `length_class_id` int(11) NOT NULL DEFAULT '0',
  `subtract` tinyint(1) NOT NULL DEFAULT '1',
  `minimum` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `demo` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_attribute`
--

CREATE TABLE `oc_product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_collection`
--

CREATE TABLE `oc_product_collection` (
  `product_collection_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `collection_id` int(11) NOT NULL,
  `sort_order` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_description`
--

CREATE TABLE `oc_product_description` (
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `sub_description` text,
  `seo_title` varchar(256) DEFAULT NULL,
  `seo_description` varchar(512) DEFAULT NULL,
  `tag` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_discount`
--

CREATE TABLE `oc_product_discount` (
  `product_discount_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_filter`
--

CREATE TABLE `oc_product_filter` (
  `product_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_filter`
--

INSERT INTO `oc_product_filter` (`product_id`, `filter_id`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_image`
--

CREATE TABLE `oc_product_image` (
  `product_image_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_alt` varchar(256) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_option`
--

CREATE TABLE `oc_product_option` (
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_option_value`
--

CREATE TABLE `oc_product_option_value` (
  `product_option_value_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtract` tinyint(1) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `price_prefix` varchar(1) NOT NULL,
  `points` int(8) NOT NULL,
  `points_prefix` varchar(1) NOT NULL,
  `weight` decimal(15,8) NOT NULL,
  `weight_prefix` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_recurring`
--

CREATE TABLE `oc_product_recurring` (
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_related`
--

CREATE TABLE `oc_product_related` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_reward`
--

CREATE TABLE `oc_product_reward` (
  `product_reward_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `points` int(8) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_special`
--

CREATE TABLE `oc_product_special` (
  `product_special_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_tag`
--

CREATE TABLE `oc_product_tag` (
  `product_tag_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_category`
--

CREATE TABLE `oc_product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_download`
--

CREATE TABLE `oc_product_to_download` (
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_layout`
--

CREATE TABLE `oc_product_to_layout` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_store`
--

CREATE TABLE `oc_product_to_store` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_version`
--

CREATE TABLE `oc_product_version` (
  `product_version_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `version` varchar(256) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `compare_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sku` varchar(64) NOT NULL,
  `barcode` varchar(124) NOT NULL,
  `quantity` int(7) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_recurring`
--

CREATE TABLE `oc_recurring` (
  `recurring_id` int(11) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `duration` int(10) UNSIGNED NOT NULL,
  `cycle` int(10) UNSIGNED NOT NULL,
  `trial_status` tinyint(4) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `trial_frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `trial_duration` int(10) UNSIGNED NOT NULL,
  `trial_cycle` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_recurring_description`
--

CREATE TABLE `oc_recurring_description` (
  `recurring_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_report`
--

CREATE TABLE `oc_report` (
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `report_date` date NOT NULL,
  `type` tinyint(4) NOT NULL,
  `type_description` varchar(100) NOT NULL,
  `d_field` varchar(500) NOT NULL,
  `m_value` varchar(500) NOT NULL,
  `last_updated` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return`
--

CREATE TABLE `oc_return` (
  `return_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `product` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `opened` tinyint(1) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `return_action_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `comment` text,
  `date_ordered` date NOT NULL DEFAULT '0000-00-00',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_action`
--

CREATE TABLE `oc_return_action` (
  `return_action_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_return_action`
--

INSERT INTO `oc_return_action` (`return_action_id`, `language_id`, `name`) VALUES
(1, 1, 'Refunded'),
(2, 1, 'Credit Issued'),
(3, 1, 'Replacement Sent'),
(4, 2, 'Refunded'),
(5, 2, 'Credit Issued'),
(6, 2, 'Replacement Sent');

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_history`
--

CREATE TABLE `oc_return_history` (
  `return_history_id` int(11) NOT NULL,
  `return_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_reason`
--

CREATE TABLE `oc_return_reason` (
  `return_reason_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_return_reason`
--

INSERT INTO `oc_return_reason` (`return_reason_id`, `language_id`, `name`) VALUES
(1, 1, 'Dead On Arrival'),
(2, 1, 'Received Wrong Item'),
(3, 1, 'Order Error'),
(4, 1, 'Faulty, please supply details'),
(5, 1, 'Other, please supply details'),
(6, 2, 'Dead On Arrival'),
(7, 2, 'Received Wrong Item'),
(8, 2, 'Order Error'),
(9, 2, 'Faulty, please supply details'),
(10, 2, 'Other, please supply details');

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_status`
--

CREATE TABLE `oc_return_status` (
  `return_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_return_status`
--

INSERT INTO `oc_return_status` (`return_status_id`, `language_id`, `name`) VALUES
(1, 1, 'Pending'),
(3, 1, 'Complete'),
(2, 1, 'Awaiting Products'),
(4, 2, 'Pending'),
(5, 2, 'Complete'),
(6, 2, 'Awaiting Products');

-- --------------------------------------------------------

--
-- Table structure for table `oc_review`
--

CREATE TABLE `oc_review` (
  `review_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_seo_url`
--

CREATE TABLE `oc_seo_url` (
  `seo_url_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `table_name` varchar(256) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_session`
--

CREATE TABLE `oc_session` (
  `session_id` varchar(32) NOT NULL,
  `data` text NOT NULL,
  `expire` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_setting`
--

CREATE TABLE `oc_setting` (
  `setting_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(128) NOT NULL,
  `key` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_setting`
--

INSERT INTO `oc_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(1, 0, 'config', 'config_robots', 'abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshaihulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg', 0),
(2, 0, 'config', 'config_shared', '0', 0),
(3, 0, 'config', 'config_secure', '0', 0),
(4, 0, 'voucher', 'total_voucher_sort_order', '8', 0),
(5, 0, 'voucher', 'total_voucher_status', '1', 0),
(6, 0, 'config', 'config_fraud_detection', '0', 0),
(7, 0, 'config', 'config_ftp_status', '0', 0),
(8, 0, 'config', 'config_ftp_root', '', 0),
(9, 0, 'config', 'config_ftp_password', '', 0),
(10, 0, 'config', 'config_ftp_username', '', 0),
(11, 0, 'config', 'config_ftp_port', '21', 0),
(12, 0, 'config', 'config_ftp_hostname', '', 0),
(13, 0, 'config', 'config_meta_title', 'Your Store', 0),
(14, 0, 'config', 'config_meta_description', 'My Store', 0),
(15, 0, 'config', 'config_meta_keyword', '', 0),
(5515, 0, 'config', 'config_theme', 'adg_topal_xfec', 0),
(17, 0, 'config', 'config_layout_id', '4', 0),
(18, 0, 'config', 'config_country_id', '222', 0),
(19, 0, 'config', 'config_zone_id', '3563', 0),
(20, 0, 'config', 'config_language', 'vi-vn', 0),
(21, 0, 'config', 'config_admin_language', 'vi-vn', 0),
(22, 0, 'config', 'config_currency', 'VND', 0),
(23, 0, 'config', 'config_currency_auto', '1', 0),
(24, 0, 'config', 'config_length_class_id', '1', 0),
(25, 0, 'config', 'config_weight_class_id', '1', 0),
(26, 0, 'config', 'config_product_count', '1', 0),
(27, 0, 'config', 'config_limit_admin', '25', 0),
(28, 0, 'config', 'config_review_status', '1', 0),
(29, 0, 'config', 'config_review_guest', '1', 0),
(30, 0, 'config', 'config_voucher_min', '1', 0),
(31, 0, 'config', 'config_voucher_max', '1000', 0),
(32, 0, 'config', 'config_tax', '1', 0),
(33, 0, 'config', 'config_tax_default', 'shipping', 0),
(34, 0, 'config', 'config_tax_customer', 'shipping', 0),
(35, 0, 'config', 'config_customer_online', '0', 0),
(36, 0, 'config', 'config_customer_activity', '0', 0),
(37, 0, 'config', 'config_customer_search', '0', 0),
(38, 0, 'config', 'config_customer_group_id', '1', 0),
(39, 0, 'config', 'config_customer_group_display', '["1"]', 1),
(40, 0, 'config', 'config_customer_price', '0', 0),
(41, 0, 'config', 'config_account_id', '3', 0),
(42, 0, 'config', 'config_invoice_prefix', 'INV-2013-00', 0),
(43, 0, 'config', 'config_api_id', '1', 0),
(44, 0, 'config', 'config_cart_weight', '1', 0),
(45, 0, 'config', 'config_checkout_guest', '1', 0),
(46, 0, 'config', 'config_checkout_id', '5', 0),
(47, 0, 'config', 'config_order_status_id', '1', 0),
(48, 0, 'config', 'config_processing_status', '["5","1","2","12","3"]', 1),
(49, 0, 'config', 'config_complete_status', '["5","3"]', 1),
(50, 0, 'config', 'config_stock_display', '0', 0),
(51, 0, 'config', 'config_stock_warning', '0', 0),
(52, 0, 'config', 'config_stock_checkout', '0', 0),
(53, 0, 'config', 'config_affiliate_approval', '0', 0),
(54, 0, 'config', 'config_affiliate_auto', '0', 0),
(55, 0, 'config', 'config_affiliate_commission', '5', 0),
(56, 0, 'config', 'config_affiliate_id', '4', 0),
(57, 0, 'config', 'config_return_id', '0', 0),
(58, 0, 'config', 'config_return_status_id', '2', 0),
(59, 0, 'config', 'config_logo', 'catalog/logo.png', 0),
(60, 0, 'config', 'config_icon', 'catalog/cart.png', 0),
(61, 0, 'config', 'config_comment', '', 0),
(62, 0, 'config', 'config_open', '', 0),
(63, 0, 'config', 'config_image', '', 0),
(64, 0, 'config', 'config_fax', '', 0),
(65, 0, 'config', 'config_telephone', '', 0),
(66, 0, 'config', 'config_email', 'demo@opencart.com', 0),
(67, 0, 'config', 'config_geocode', '', 0),
(68, 0, 'config', 'config_owner', 'Your Name', 0),
(69, 0, 'config', 'config_address', '', 0),
(70, 0, 'config', 'config_name', '', 0),
(71, 0, 'config', 'config_seo_url', '0', 0),
(72, 0, 'config', 'config_file_max_size', '300000', 0),
(73, 0, 'config', 'config_file_ext_allowed', 'zip\r\ntxt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\ndoc', 0),
(74, 0, 'config', 'config_file_mime_allowed', 'text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/gif\r\nimage/bmp\r\nimage/tiff\r\nimage/svg+xml\r\napplication/zip\r\n&quot;application/zip&quot;\r\napplication/x-zip\r\n&quot;application/x-zip&quot;\r\napplication/x-zip-compressed\r\n&quot;application/x-zip-compressed&quot;\r\napplication/rar\r\n&quot;application/rar&quot;\r\napplication/x-rar\r\n&quot;application/x-rar&quot;\r\napplication/x-rar-compressed\r\n&quot;application/x-rar-compressed&quot;\r\napplication/octet-stream\r\n&quot;application/octet-stream&quot;\r\naudio/mpeg\r\nvideo/quicktime\r\napplication/pdf', 0),
(75, 0, 'config', 'config_maintenance', '0', 0),
(76, 0, 'config', 'config_password', '1', 0),
(77, 0, 'config', 'config_encryption', '', 0),
(78, 0, 'config', 'config_compression', '0', 0),
(79, 0, 'config', 'config_error_display', '1', 0),
(80, 0, 'config', 'config_error_log', '1', 0),
(81, 0, 'config', 'config_error_filename', 'error.log', 0),
(82, 0, 'config', 'config_google_analytics', '', 0),
(83, 0, 'config', 'config_mail_engine', 'smtp', 0),
(84, 0, 'config', 'config_mail_parameter', '', 0),
(85, 0, 'config', 'config_mail_smtp_hostname', 'tls://smtp.gmail.com', 0),
(86, 0, 'config', 'config_mail_smtp_username', 'hotro@bestme.asia', 0),
(87, 0, 'config', 'config_mail_smtp_password', 'NOVAONshop@1234', 0),
(88, 0, 'config', 'config_mail_smtp_port', '587', 0),
(89, 0, 'config', 'config_mail_smtp_timeout', '15', 0),
(90, 0, 'config', 'config_mail_alert_email', '', 0),
(91, 0, 'config', 'config_mail_alert', '["order"]', 1),
(92, 0, 'config', 'config_captcha', 'basic', 0),
(93, 0, 'config', 'config_captcha_page', '["review","return","contact"]', 1),
(94, 0, 'config', 'config_login_attempts', '5', 0),
(95, 0, 'payment_free_checkout', 'payment_free_checkout_status', '1', 0),
(96, 0, 'payment_free_checkout', 'free_checkout_order_status_id', '1', 0),
(97, 0, 'payment_free_checkout', 'payment_free_checkout_sort_order', '1', 0),
(98, 0, 'payment_cod', 'payment_cod_sort_order', '5', 0),
(99, 0, 'payment_cod', 'payment_cod_total', '0.01', 0),
(100, 0, 'payment_cod', 'payment_cod_order_status_id', '1', 0),
(101, 0, 'payment_cod', 'payment_cod_geo_zone_id', '0', 0),
(102, 0, 'payment_cod', 'payment_cod_status', '1', 0),
(103, 0, 'shipping_flat', 'shipping_flat_sort_order', '1', 0),
(104, 0, 'shipping_flat', 'shipping_flat_status', '1', 0),
(105, 0, 'shipping_flat', 'shipping_flat_geo_zone_id', '0', 0),
(106, 0, 'shipping_flat', 'shipping_flat_tax_class_id', '9', 0),
(107, 0, 'shipping_flat', 'shipping_flat_cost', '5.00', 0),
(108, 0, 'total_shipping', 'total_shipping_sort_order', '3', 0),
(109, 0, 'total_sub_total', 'sub_total_sort_order', '1', 0),
(110, 0, 'total_sub_total', 'total_sub_total_status', '1', 0),
(111, 0, 'total_tax', 'total_tax_status', '1', 0),
(112, 0, 'total_total', 'total_total_sort_order', '9', 0),
(113, 0, 'total_total', 'total_total_status', '1', 0),
(114, 0, 'total_tax', 'total_tax_sort_order', '5', 0),
(115, 0, 'total_credit', 'total_credit_sort_order', '7', 0),
(116, 0, 'total_credit', 'total_credit_status', '1', 0),
(117, 0, 'total_reward', 'total_reward_sort_order', '2', 0),
(118, 0, 'total_reward', 'total_reward_status', '1', 0),
(119, 0, 'total_shipping', 'total_shipping_status', '1', 0),
(120, 0, 'total_shipping', 'total_shipping_estimator', '1', 0),
(121, 0, 'total_coupon', 'total_coupon_sort_order', '4', 0),
(122, 0, 'total_coupon', 'total_coupon_status', '1', 0),
(123, 0, 'module_category', 'module_category_status', '1', 0),
(124, 0, 'module_account', 'module_account_status', '1', 0),
(125, 0, 'theme_default', 'theme_default_product_limit', '15', 0),
(126, 0, 'theme_default', 'theme_default_product_description_length', '100', 0),
(127, 0, 'theme_default', 'theme_default_image_thumb_width', '228', 0),
(128, 0, 'theme_default', 'theme_default_image_thumb_height', '228', 0),
(129, 0, 'theme_default', 'theme_default_image_popup_width', '500', 0),
(130, 0, 'theme_default', 'theme_default_image_popup_height', '500', 0),
(131, 0, 'theme_default', 'theme_default_image_category_width', '80', 0),
(132, 0, 'theme_default', 'theme_default_image_category_height', '80', 0),
(133, 0, 'theme_default', 'theme_default_image_product_width', '228', 0),
(134, 0, 'theme_default', 'theme_default_image_product_height', '228', 0),
(135, 0, 'theme_default', 'theme_default_image_additional_width', '74', 0),
(136, 0, 'theme_default', 'theme_default_image_additional_height', '74', 0),
(137, 0, 'theme_default', 'theme_default_image_related_width', '200', 0),
(138, 0, 'theme_default', 'theme_default_image_related_height', '200', 0),
(139, 0, 'theme_default', 'theme_default_image_compare_width', '90', 0),
(140, 0, 'theme_default', 'theme_default_image_compare_height', '90', 0),
(141, 0, 'theme_default', 'theme_default_image_wishlist_width', '47', 0),
(142, 0, 'theme_default', 'theme_default_image_wishlist_height', '47', 0),
(143, 0, 'theme_default', 'theme_default_image_cart_height', '47', 0),
(144, 0, 'theme_default', 'theme_default_image_cart_width', '47', 0),
(145, 0, 'theme_default', 'theme_default_image_location_height', '50', 0),
(146, 0, 'theme_default', 'theme_default_image_location_width', '268', 0),
(147, 0, 'theme_default', 'theme_default_directory', 'default', 0),
(148, 0, 'theme_default', 'theme_default_status', '1', 0),
(149, 0, 'dashboard_activity', 'dashboard_activity_status', '1', 0),
(150, 0, 'dashboard_activity', 'dashboard_activity_sort_order', '7', 0),
(151, 0, 'dashboard_sale', 'dashboard_sale_status', '1', 0),
(152, 0, 'dashboard_sale', 'dashboard_sale_width', '3', 0),
(153, 0, 'dashboard_chart', 'dashboard_chart_status', '1', 0),
(154, 0, 'dashboard_chart', 'dashboard_chart_width', '6', 0),
(155, 0, 'dashboard_customer', 'dashboard_customer_status', '1', 0),
(156, 0, 'dashboard_customer', 'dashboard_customer_width', '3', 0),
(157, 0, 'dashboard_map', 'dashboard_map_status', '1', 0),
(158, 0, 'dashboard_map', 'dashboard_map_width', '6', 0),
(159, 0, 'dashboard_online', 'dashboard_online_status', '1', 0),
(160, 0, 'dashboard_online', 'dashboard_online_width', '3', 0),
(161, 0, 'dashboard_order', 'dashboard_order_sort_order', '1', 0),
(162, 0, 'dashboard_order', 'dashboard_order_status', '1', 0),
(163, 0, 'dashboard_order', 'dashboard_order_width', '3', 0),
(164, 0, 'dashboard_sale', 'dashboard_sale_sort_order', '2', 0),
(165, 0, 'dashboard_customer', 'dashboard_customer_sort_order', '3', 0),
(166, 0, 'dashboard_online', 'dashboard_online_sort_order', '4', 0),
(167, 0, 'dashboard_map', 'dashboard_map_sort_order', '5', 0),
(168, 0, 'dashboard_chart', 'dashboard_chart_sort_order', '6', 0),
(169, 0, 'dashboard_recent', 'dashboard_recent_status', '1', 0),
(170, 0, 'dashboard_recent', 'dashboard_recent_sort_order', '8', 0),
(171, 0, 'dashboard_activity', 'dashboard_activity_width', '4', 0),
(172, 0, 'dashboard_recent', 'dashboard_recent_width', '8', 0),
(173, 0, 'report_customer_activity', 'report_customer_activity_status', '1', 0),
(174, 0, 'report_customer_activity', 'report_customer_activity_sort_order', '1', 0),
(175, 0, 'report_customer_order', 'report_customer_order_status', '1', 0),
(176, 0, 'report_customer_order', 'report_customer_order_sort_order', '2', 0),
(177, 0, 'report_customer_reward', 'report_customer_reward_status', '1', 0),
(178, 0, 'report_customer_reward', 'report_customer_reward_sort_order', '3', 0),
(179, 0, 'report_customer_search', 'report_customer_search_sort_order', '3', 0),
(180, 0, 'report_customer_search', 'report_customer_search_status', '1', 0),
(181, 0, 'report_customer_transaction', 'report_customer_transaction_status', '1', 0),
(182, 0, 'report_customer_transaction', 'report_customer_transaction_status_sort_order', '4', 0),
(183, 0, 'report_sale_tax', 'report_sale_tax_status', '1', 0),
(184, 0, 'report_sale_tax', 'report_sale_tax_sort_order', '5', 0),
(185, 0, 'report_sale_shipping', 'report_sale_shipping_status', '1', 0),
(186, 0, 'report_sale_shipping', 'report_sale_shipping_sort_order', '6', 0),
(187, 0, 'report_sale_return', 'report_sale_return_status', '1', 0),
(188, 0, 'report_sale_return', 'report_sale_return_sort_order', '7', 0),
(189, 0, 'report_sale_order', 'report_sale_order_status', '1', 0),
(190, 0, 'report_sale_order', 'report_sale_order_sort_order', '8', 0),
(191, 0, 'report_sale_coupon', 'report_sale_coupon_status', '1', 0),
(192, 0, 'report_sale_coupon', 'report_sale_coupon_sort_order', '9', 0),
(193, 0, 'report_product_viewed', 'report_product_viewed_status', '1', 0),
(194, 0, 'report_product_viewed', 'report_product_viewed_sort_order', '10', 0),
(195, 0, 'report_product_purchased', 'report_product_purchased_status', '1', 0),
(196, 0, 'report_product_purchased', 'report_product_purchased_sort_order', '11', 0),
(197, 0, 'report_marketing', 'report_marketing_status', '1', 0),
(198, 0, 'report_marketing', 'report_marketing_sort_order', '12', 0),
(4968, 0, 'developer', 'developer_sass', '0', 0),
(201, 0, 'module_theme_builder_config', 'module_theme_builder_config_status', '1', 0),
(4700, 0, 'theme_novaon', 'theme_novaon_image_wishlist_height', '47', 0),
(4701, 0, 'theme_novaon', 'theme_novaon_image_wishlist_width', '47', 0),
(4702, 0, 'theme_novaon', 'theme_novaon_image_compare_height', '90', 0),
(4703, 0, 'theme_novaon', 'theme_novaon_image_compare_width', '90', 0),
(4704, 0, 'theme_novaon', 'theme_novaon_image_related_height', '80', 0),
(4705, 0, 'theme_novaon', 'theme_novaon_image_related_width', '80', 0),
(4706, 0, 'theme_novaon', 'theme_novaon_image_additional_height', '74', 0),
(4707, 0, 'theme_novaon', 'theme_novaon_image_additional_width', '74', 0),
(4708, 0, 'theme_novaon', 'theme_novaon_image_product_height', '228', 0),
(4709, 0, 'theme_novaon', 'theme_novaon_image_product_width', '228', 0),
(4710, 0, 'theme_novaon', 'theme_novaon_image_popup_height', '500', 0),
(4711, 0, 'theme_novaon', 'theme_novaon_image_popup_width', '500', 0),
(4712, 0, 'theme_novaon', 'theme_novaon_image_thumb_height', '228', 0),
(4713, 0, 'theme_novaon', 'theme_novaon_image_thumb_width', '228', 0),
(4714, 0, 'theme_novaon', 'theme_novaon_image_category_height', '80', 0),
(4715, 0, 'theme_novaon', 'theme_novaon_image_category_width', '80', 0),
(4716, 0, 'theme_novaon', 'theme_novaon_product_description_length', '100', 0),
(4717, 0, 'theme_novaon', 'theme_novaon_product_limit', '15', 0),
(4718, 0, 'theme_novaon', 'theme_novaon_status', '1', 0),
(4719, 0, 'theme_novaon', 'theme_novaon_directory', 'novaon', 0),
(4720, 0, 'theme_novaon', 'theme_novaon_image_cart_width', '47', 0),
(4721, 0, 'theme_novaon', 'theme_novaon_image_cart_height', '47', 0),
(4722, 0, 'theme_novaon', 'theme_novaon_image_location_width', '268', 0),
(4723, 0, 'theme_novaon', 'theme_novaon_image_location_height', '50', 0),
(4892, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_image_wishlist_height', '47', 0),
(4893, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_image_wishlist_width', '47', 0),
(4894, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_image_compare_height', '90', 0),
(4895, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_image_compare_width', '90', 0),
(4896, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_image_related_height', '80', 0),
(4897, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_image_related_width', '80', 0),
(4898, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_image_additional_height', '74', 0),
(4899, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_image_additional_width', '74', 0),
(4900, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_image_product_height', '228', 0),
(4901, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_image_product_width', '228', 0),
(4902, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_image_popup_height', '500', 0),
(4903, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_image_popup_width', '500', 0),
(4904, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_image_thumb_height', '228', 0),
(4905, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_image_thumb_width', '228', 0),
(4906, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_image_category_height', '80', 0),
(4907, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_image_category_width', '80', 0),
(4908, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_product_description_length', '100', 0),
(4909, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_product_limit', '15', 0),
(4910, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_status', '1', 0),
(4911, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_directory', 'adg_topal_xfec', 0),
(4912, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_image_cart_width', '47', 0),
(4913, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_image_cart_height', '47', 0),
(4914, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_image_location_width', '268', 0),
(4915, 0, 'theme_adg_topal_xfec', 'theme_adg_topal_xfec_image_location_height', '50', 0),
(4967, 0, 'developer', 'developer_theme', '0', 0);
-- --------------------------------------------------------

--
-- Table structure for table `oc_shipping_courier`
--

CREATE TABLE `oc_shipping_courier` (
  `shipping_courier_id` int(11) NOT NULL,
  `shipping_courier_code` varchar(255) NOT NULL DEFAULT '',
  `shipping_courier_name` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_statistics`
--

CREATE TABLE `oc_statistics` (
  `statistics_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `value` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_statistics`
--

INSERT INTO `oc_statistics` (`statistics_id`, `code`, `value`) VALUES
(1, 'order_sale', '0.0000'),
(2, 'order_processing', '0.0000'),
(3, 'order_complete', '0.0000'),
(4, 'order_other', '0.0000'),
(5, 'returns', '0.0000'),
(6, 'product', '0.0000'),
(7, 'review', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `oc_stock_status`
--

CREATE TABLE `oc_stock_status` (
  `stock_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_stock_status`
--

INSERT INTO `oc_stock_status` (`stock_status_id`, `language_id`, `name`) VALUES
(7, 1, 'In Stock'),
(8, 1, 'Pre-Order'),
(5, 1, 'Out Of Stock'),
(6, 1, '2-3 Days'),
(7, 2, 'In Stock'),
(8, 2, 'Pre-Order'),
(5, 2, 'Out Of Stock'),
(6, 2, '2-3 Days');

-- --------------------------------------------------------

--
-- Table structure for table `oc_store`
--

CREATE TABLE `oc_store` (
  `store_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  `ssl` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_tag`
--

CREATE TABLE `oc_tag` (
  `tag_id` int(11) NOT NULL,
  `value` varchar(500) NOT NULL,
  `sort_order` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_class`
--

CREATE TABLE `oc_tax_class` (
  `tax_class_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_class`
--

INSERT INTO `oc_tax_class` (`tax_class_id`, `title`, `description`, `date_added`, `date_modified`) VALUES
(9, 'Taxable Goods', 'Taxed goods', '2009-01-06 23:21:53', '2011-09-23 14:07:50'),
(10, 'Downloadable Products', 'Downloadable', '2011-09-21 22:19:39', '2011-09-22 10:27:36');

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rate`
--

CREATE TABLE `oc_tax_rate` (
  `tax_rate_id` int(11) NOT NULL,
  `geo_zone_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  `rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `type` char(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rate`
--

INSERT INTO `oc_tax_rate` (`tax_rate_id`, `geo_zone_id`, `name`, `rate`, `type`, `date_added`, `date_modified`) VALUES
(86, 3, 'VAT (20%)', '20.0000', 'P', '2011-03-09 21:17:10', '2011-09-22 22:24:29'),
(87, 3, 'Eco Tax (-2.00)', '2.0000', 'F', '2011-09-21 21:49:23', '2011-09-23 00:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rate_to_customer_group`
--

CREATE TABLE `oc_tax_rate_to_customer_group` (
  `tax_rate_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rate_to_customer_group`
--

INSERT INTO `oc_tax_rate_to_customer_group` (`tax_rate_id`, `customer_group_id`) VALUES
(86, 1),
(87, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rule`
--

CREATE TABLE `oc_tax_rule` (
  `tax_rule_id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `based` varchar(10) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rule`
--

INSERT INTO `oc_tax_rule` (`tax_rule_id`, `tax_class_id`, `tax_rate_id`, `based`, `priority`) VALUES
(121, 10, 86, 'payment', 1),
(120, 10, 87, 'store', 0),
(128, 9, 86, 'shipping', 1),
(127, 9, 87, 'shipping', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_theme`
--

CREATE TABLE `oc_theme` (
  `theme_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `theme` varchar(64) NOT NULL,
  `route` varchar(64) NOT NULL,
  `code` mediumtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_theme_builder_config`
--

CREATE TABLE `oc_theme_builder_config` (
  `id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `config_theme` varchar(128) NOT NULL,
  `code` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `config` mediumtext NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_theme_builder_config`
--

INSERT INTO `oc_theme_builder_config` (`id`, `store_id`, `config_theme`, `code`, `key`, `config`, `date_added`, `date_modified`) VALUES
(414, 0, 'adg_topal_xfec', 'config', 'config_theme_color', '{\n    "main": {\n        "main": {\n            "hex": "F58220",\n            "visible": "1"\n        },\n        "button": {\n            "hex": "00456E",\n            "visible": "1"\n        }\n    }\n}', '2021-11-08 16:57:19', '2022-02-08 10:16:52'),
(415, 0, 'adg_topal_xfec', 'config', 'config_theme_text', '{\n    "all": {\n        "title": "Font Roboto",\n        "font": "Roboto",\n        "supported-fonts": [\n            "Open Sans",\n            "IBM Plex Sans",\n            "Courier New",\n            "Roboto",\n            "Nunito",\n            "Arial",\n            "DejaVu Sans",\n            "Tahoma",\n            "Time News Roman",\n            "Mulish"\n        ]\n    },\n    "heading": {\n        "font": "Tahoma",\n        "supported-fonts": [\n            "Open Sans",\n            "IBM Plex Sans",\n            "Courier New",\n            "Roboto",\n            "Nunito",\n            "Arial",\n            "DejaVu Sans",\n            "Tahoma",\n            "Time News Roman",\n            "Quicksand",\n            "Montserrat",\n            "KoHo",\n            "Newsreader",\n            "Oswald",\n            "Playfair Display",\n            "Lora",\n            "Inter"\n        ],\n        "font-type": "regular",\n        "supported-font-types": [\n            "regular",\n            "bold",\n            "italic"\n        ],\n        "font-size": 32\n    },\n    "body": {\n        "font": "Aria",\n        "supported-fonts": [\n            "Open Sans",\n            "IBM Plex Sans",\n            "Courier New",\n            "Roboto",\n            "Nunito",\n            "Arial",\n            "DejaVu Sans",\n            "Tahoma",\n            "Time News Roman",\n            "Quicksand",\n            "Montserrat",\n            "KoHo",\n            "Newsreader",\n            "Oswald",\n            "Playfair Display",\n            "Lora",\n            "Inter"\n        ],\n        "font-type": "regular",\n        "supported-font-types": [\n            "regular",\n            "bold",\n            "italic"\n        ],\n        "font-size": 14\n    }\n}', '2021-11-08 16:57:19', '2022-02-08 10:16:52'),
(416, 0, 'adg_topal_xfec', 'config', 'config_theme_favicon', '{\n    "image": {\n        "url": "\\/catalog\\/view\\/theme\\/default\\/image\\/favicon.ico"\n    }\n}', '2021-11-08 16:57:19', '2022-02-08 10:16:52'),
(417, 0, 'adg_topal_xfec', 'config', 'config_theme_social', '[\n    {\n        "name": "facebook",\n        "text": "Facebook",\n        "url": "https:\\/\\/www.facebook.com"\n    },\n    {\n        "name": "twitter",\n        "text": "Twitter",\n        "url": "https:\\/\\/www.twitter.com"\n    },\n    {\n        "name": "instagram",\n        "text": "Instagram",\n        "url": "https:\\/\\/www.instagram.com"\n    },\n    {\n        "name": "tumblr",\n        "text": "Tumblr",\n        "url": "https:\\/\\/www.tumblr.com"\n    },\n    {\n        "name": "youtube",\n        "text": "Youtube",\n        "url": "https:\\/\\/www.youtube.com"\n    },\n    {\n        "name": "googleplus",\n        "text": "Google Plus",\n        "url": "https:\\/\\/www.plus.google.com"\n    }\n]', '2021-11-08 16:57:19', '2022-02-08 10:16:52'),
(418, 0, 'adg_topal_xfec', 'config', 'config_theme_ecommerce', '[\n    {\n        "name": "shopee",\n        "text": "Shopee",\n        "url": "https:\\/\\/shopee.vn"\n    },\n    {\n        "name": "lazada",\n        "text": "Lazada",\n        "url": "https:\\/\\/www.lazada.vn"\n    },\n    {\n        "name": "tiki",\n        "text": "Tiki",\n        "url": "https:\\/\\/tiki.vn"\n    },\n    {\n        "name": "sendo",\n        "text": "Sendo",\n        "url": "https:\\/\\/www.sendo.vn"\n    }\n]', '2021-11-08 16:57:19', '2022-02-08 10:16:52'),
(419, 0, 'adg_topal_xfec', 'config', 'config_theme_override_css', '{\n    "css": "\\/* override css *\\/\\n.bestme-block-banner-top a {\\n    color: #053D5A !important;\\n}\\n.news-item .news-title:hover {\\n    color: #f58220;\\n}\\nsection.py-50.mb-5.news-home {\\n    padding-bottom: 0 !important;\\n    padding-top: 0 !important;\\n}\\n\\nsection.py-50.mb-5.news-home .container {\\n    padding-bottom: 0;\\n}\\n.service .bg-third.h-100.px-lg-20.px-15.pt-lg-40.py-lg-50.py-20:hover {\\n    background: rgb(13 110 253 \\/ 15%) !important;\\n    transition: 0.4s;\\n}\\n.about-us section.pt-lg-50.about-us-home.pb-50 {\\n    padding-top: 0 !important;\\n    padding-bottom: 0 !important;\\n}\\n.why-choose .pt-lg-50.container {\\n    padding-top: 0 !important;\\n}\\n.why-choose .pt-30.pt-lg-50.pb-lg-50.position-relative.z-index-1 {\\n    padding-bottom: 0 !important;\\n    padding-top: 30px !important;\\n}\\n.service section.pt-50.pb-lg-50.pb-20 {\\n    padding-top: 0 !important;\\n    padding-bottom: 0 !important;\\n}\\n.projects-box section.py-50.container.container {\\n    padding-top: 25px !important;\\n    padding-bottom: 0px !important;\\n}\\n.intro-award .py-lg-50 {\\n    padding: 0 !important;\\n}\\n\\n#header .header-search input.form-control {\\n  border-left: none;\\n  padding-right: 12px;\\n}\\n\\n.product-item img {\\n    height: 321px;\\n  \\tobject-fit: cover;\\n}\\n.news-item img {\\n    height: 209px;\\n    object-fit: cover;\\n}\\na.btn.btn-primary.mt-3.rounded-5.contact-now-btn {\\n    margin-top: -10px !important;\\n    border-radius: 10px !important;\\n}\\n.nav-tabs li.nav-item.me-md-4 {\\n    margin-right: 0 !important;\\n}\\n.news-item.high-light.mb-lg-60 img {\\n    height: 332px;\\n    object-fit: cover;\\n}\\n\\n.main-header ul.nav \\u003E li \\u003E a\\u003E.toggle {\\n    background: #fff;\\n    transform: rotate(\\n0deg);\\n    transform: translateX(3px);\\n}\\n\\n.main-header ul.nav.child-nav {\\n    padding: 15px;\\n}\\n\\n.main-header ul.nav.child-nav li{\\n    padding-bottom: 5px;\\n}\\n\\n.main-header ul.nav.child-nav {\\n    padding: 15px;\\n}\\n\\n.main-header ul.nav.child-nav li{\\n    padding: 7px 2px;\\n    border-bottom: 1px solid #0000001c;\\n    font-size: 14px;\\n}\\n\\n.main-header ul.nav.child-nav li:last-child {\\n    border: none;\\n    padding-bottom: 0px;\\n}\\n\\n.main-header ul.nav.child-nav a{\\n    color: #00456E;\\n}\\n\\n.main-header ul.nav.child-nav a:hover{color: #F58220;}\\n.block-header #navbarMainMenu ul.nav ul.nav {\\n    border: none;\\n    transform: translate(2px,20px);\\n    padding-left: 19px;\\n    box-shadow: 5px 7px 8px 1px #dfe4e745;\\n}\\n\\n.bestme-block-header-menu ul.nav.child-nav {\\n    transform: translateX(-17px);\\n}\\n\\nul.nav.navbar-nav.bestme-block-header-menu li.has-child {\\n    padding-right: 15px;\\n}\\n\\n.nav-tabs .nav-link::first-letter {\\n    text-transform: uppercase;\\n}\\n#footer .footer-col:last-child {\\n    text-align: right;\\n}\\n.bestme-block-contact-contact .mt-lg-2.mb-lg-40 {\\n    text-align: center;\\n}\\n.projects-view img {\\n    height: 400px;\\n    object-fit: cover;\\n}\\n.intro-award .col-lg-5.py-lg-4.pt-4 {\\n    display: flex;\\n    flex-direction: column;\\n    justify-content: center;\\n}\\n.news-detail.news-item {\\n    padding: 15px;\\n    margin: 0 !important;\\n\\twidth:100%;\\n}\\n.bestme-block-contact-contact .align-self-end.w-100.pe-lg-50 {\\n    align-self: start !important;\\n}\\n\\n#footer .main-footer {\\n    padding: 40px 0 35px;\\n}\\n\\n.main-footer .d-block.mb-5.text-center img {\\n    transform: translateY(-39px);\\n}\\n\\n.col-12.col-lg-8.news-detail.news-item.mx-auto.my-0.my-lg-3 img {\\n    width: 100% !important;\\n    height: auto;\\n}\\n.bestme-block-contact-contact textarea#your-messenger {\\n    height: 150px;\\n}\\n.main-footer h4 {\\n    font-size: 16px !important;\\n}\\n\\n #footer .nav.flex-column {\\n      list-style: disc;\\n      padding-left: 15px;\\n    }\\n\\n    #footer .footer-col li::marker {\\n      color: #dc2e28;\\n    }\\n\\n    #footer .footer-col li::before {\\n      content: \\u0022\\u0022;\\n      width: 0;\\n    }\\n\\n.breadcrumb-item+.breadcrumb-item {\\n    font-size: 14px;\\n}\\nol.breadcrumb a {\\n    font-size: 14px;\\n}\\n@media(max-width: 768px){\\n  .main-footer .logo {\\n    display: none !important;\\n  }\\n  header.block-header.fixed .logo {\\n    top: 15px !important;\\n  }\\n  .news-box {\\n    padding-top: 30px;\\n  }\\n  .news-item {\\n    border-bottom: 1px solid #80808045;\\n    padding-bottom: 25px;\\n  }\\n  #header .navbar a.nav-link:hover {\\n    color: white !important;\\n }\\n  .why-choose-us-item {\\n    height: auto;\\n  }\\n  .service section.pt-50.pb-lg-50.pb-20 {\\n    padding-top: 20px !important;\\n  }\\n  .about-us section.pt-lg-50.about-us-home.pb-50 {\\n    padding-top: 0 !important;\\n    padding-bottom: 30px !important;\\n  }\\n  .why-choose .pt-lg-50.container {\\n      padding-top: 0 !important;\\n  }\\n  .why-choose .pt-30.pt-lg-50.pb-lg-50.position-relative.z-index-1 {\\n      padding-bottom: 0 !important;\\n      padding-top: 30px !important;\\n  }\\n  .service section.pt-50.pb-lg-50.pb-20 {\\n      padding-top: 0;\\n      padding-bottom: 0 !important;\\n  }\\n  .projects-box section.py-50.container.container {\\n      padding-top: 25px !important;\\n      padding-bottom: 30px !important;\\n  }\\n  \\n  .intro-award .py-lg-50 {\\n      padding: 0 !important;\\n  }\\n  #header .main-header {\\n    padding: 0 !important;\\n  }\\n  #header .header-search .collapse-search a .icon {\\n    top: 20px;\\n  }\\n  .header-top .col-sm-4 {\\n    display: none;\\n  }\\n  .header-top.d-flex.flex-wrap.bestme-block-banner-top.row {\\n    display: none !important;\\n   }\\n  .logo {\\n    top: 16px;\\n    display: block !important;\\n    position: absolute;\\n    z-index: 99;\\n    left: 50%;\\n    transform: translateX(-50%);\\n  }\\n  .header-top .col-sm-8.d-flex.text-end.text-lg-start.ms-auto.ms-lg-0.flex-column.flex-lg-row.fz-10.fz-lg-13 {\\n    position: absolute;\\n    right: 0;\\n    width: 35%;\\n    top: 3px;\\n  }\\n  .product-item img {\\n    height: auto !important;\\n    object-fit: cover;\\n  }\\n  .block-header #navbarMainMenu ul.nav ul.nav {\\n    padding-top: 0 !important;\\n    padding-bottom: 0 !important;\\n  }\\n  .main-header ul.nav.child-nav {\\n    padding: 0;\\n  }\\n  .copy-right br {\\n    display: none;\\n  }\\n  .copy-right {\\n      padding: 15px !important;\\n  }\\n  .service img {\\n    object-fit: cover;\\n    margin-top: 0 !important;\\n  }\\n  #footer .footer-col:last-child {\\n    text-align: left;\\n  }\\n  .block-products h2 {\\n    font-size: 24px;\\n  }\\n  ol.breadcrumb a {\\n    color: #737373;\\n  }\\n  .breadcrumb-item+.breadcrumb-item::before {\\n    padding-left: 3px;\\n  }\\n  #header {\\n    background: #FFFFFF;\\n  }\\n  .bestme-block-contact-contact .mt-lg-2.mb-lg-40 {\\n    text-align: left;\\n  }\\n  .contact-section .contact-item {\\n    margin-bottom: 50px;\\n  }\\n  ol.breadcrumb {\\n    margin-bottom: 5px;\\n  }\\n  section.py-50.project-related {\\n    padding-top: 0 !important;\\n  }\\n  .teaser .news-item .news-img {\\n    height: 110px;\\n  }\\n  div#productDetailTabContent {\\n    padding-top: 0 !important;\\n    padding-bottom: 1px !important;\\n  }\\n  .bestme-block-product-related-product {\\n    background: #F6F6F6 !important;\\n  }\\n  .main-header ul.nav \\u003E li \\u003E a\\u003E.toggle {\\n    background: white;\\n  }\\n  .main-header ul.nav.child-nav a {\\n    color: white;\\n  }\\n  .block-header #navbarMainMenu ul.nav ul.nav {\\n    box-shadow: none;\\n    background: none !important;\\n    display: none;\\n  }\\n\\n.block-header #navbarMainMenu ul.nav \\n li.active ul.nav {\\n    display: block;\\n    padding-bottom: 25px !important;\\n}\\n      .block-header #navbarMainMenu ul.nav ul.nav li {\\n    border: none;\\n    padding:5px 0;\\n  }\\n  .block-header #navbarMainMenu ul.nav ul.nav li a {\\n    text-transform: lowercase;\\n    color: #c4c4c4;\\n  }\\n  #product-category .flex-column, #product-collection .flex-column, #product-provider .flex-column, #product-tags .flex-column, #product-attribute .flex-column {\\n    overflow-y: hidden !important;\\n  }\\n  .main-header li.nav-item {\\n    padding: 5px 0;\\n  }\\n  .copy-right {\\n    font-size: 14px;\\n  }\\n  .news-item.high-light a.news-title {\\n    font-size: 18px;\\n    padding-top: 15px;\\n  }\\n  .news-item .news-title {\\n    font-size: 18px;\\n  }\\n  .project-item .project-title {\\n    font-size: 18px;\\n  }\\n  #header .header-search {\\n    width: 90% !important;\\n    padding-right: 21px;\\n    transform: translateY(9px);\\n    position: absolute;\\n  }\\n  #header .header-search.show .collapse-search a .icon {\\n    top: 49px;\\n  }\\n}\\n\\n#footer .main-footer:before {\\n    background-color: unset;\\n}\\n#footer .main-footer {\\n    background-image: url(https:\\/\\/topal.vn\\/wp-content\\/uploads\\/2017\\/11\\/bg-footer.jpg) !important;\\n    background-size: cover;\\n}\\n#footer .footer-col li,#footer .footer-col li a,#footer .footer-col h4, #footer .footer-col span {\\n    color: #00456E !important;\\n}\\n\\n#footer .footer-col li a:hover, #footer .footer-col li a.active, #footer a:hover {\\n  color: white !important;\\n}\\n\\n#footer .footer-col li::marker {\\n    color: #00456E !important;\\n}\\n\\n#header .navbar a.nav-link {\\n  color: white;\\n}\\n#header .navbar a.nav-link.active {\\n  color: white !important;\\n}\\n#header .navbar a.nav-link:hover {\\n    color: #00456E;\\n}\\n#footer .copy-right {\\n   background-color: #F58220 !important;\\n}\\nfooter#footer p {\\n    color: #00456E !important;\\n}\\n#header .main-header {\\n    padding: 20px 0;\\n    background: #F58220;\\n}\\n\\n.block-header #navbarMainMenu ul.nav ul.nav {\\n    background: white;\\n}\\n.icon.icon-search {\\n   background-color: #fff !important;\\n}\\n\\n#header .header-search.show ~ .navbar {\\n    display: flex;\\n    flex-wrap: wrap;\\n    align-items: center;\\n    width: 50%;\\n}\\n\\n#header .header-search {\\n    width: 35%;\\n}\\n\\n#header .header-search.show form.search-form {\\n    visibility: visible;\\n    width: 437px;\\n}\\n#header .header-search .collapse-search a .icon {\\n    background-color: #f8f9fa;\\n    font-size: 15px;\\n}\\n\\n.product-item:hover {\\n    background: #FFFFFF;\\n    box-shadow: 0 0 10px rgb(0 0 0 \\/ 15%);\\n    border-radius: 5px;\\n}\\n.product-item {\\n    text-align: center;\\n}\\n.product-item:hover .contact-now-btn {\\n    transform: translateY(-10px);\\n}"\n}', '2021-11-08 16:57:19', '2022-02-08 10:16:52'),
(420, 0, 'adg_topal_xfec', 'config', 'config_theme_checkout_page', '{\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(421, 0, 'adg_topal_xfec', 'config', 'config_section_sections', '{\n    "name": "Theme Config",\n    "version": "1.0",\n    "section": {\n        "header": {\n            "name": "header",\n            "text": "Header",\n            "visible": "1"\n        },\n        "slide-show": {\n            "name": "slide-show",\n            "text": "Slideshow",\n            "visible": "1"\n        },\n        "categories": {\n            "name": "categories",\n            "text": "Danh mục sản phẩm",\n            "visible": "1"\n        },\n        "hot-deals": {\n            "name": "hot-deals",\n            "text": "Sản phẩm khuyến mại",\n            "visible": "0"\n        },\n        "feature-products": {\n            "name": "feature-products",\n            "text": "Sản phẩm bán chạy",\n            "visible": "0"\n        },\n        "related-products": {\n            "name": "related-products",\n            "text": "Sản phẩm xem nhiều",\n            "visible": "1"\n        },\n        "new-products": {\n            "name": "new-products",\n            "text": "Sản phẩm mới",\n            "visible": "1"\n        },\n        "product-detail": {\n            "name": "product-details",\n            "text": "Chi tiết sản phẩm",\n            "visible": "1"\n        },\n        "banner": {\n            "name": "banner",\n            "text": "Banner",\n            "visible": "1"\n        },\n        "content_customize": {\n            "name": "content-customize",\n            "text": "Nội dung tùy chỉnh",\n            "visible": "1"\n        },\n        "partners": {\n            "name": "partners",\n            "text": "Đối tác",\n            "visible": "1"\n        },\n        "blog": {\n            "name": "blog",\n            "text": "Blog",\n            "visible": "1"\n        },\n        "rate": {\n            "name": "rate",\n            "text": "Đánh giá website",\n            "visible": "1"\n        },\n        "footer": {\n            "name": "footer",\n            "text": "Footer",\n            "visible": "1"\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-09 09:49:59'),
(422, 0, 'adg_topal_xfec', 'config', 'config_section_banner', '{\n    "name": "banner",\n    "text": "Banner",\n    "visible": 1,\n    "display": [\n        {\n            "image-url": "/catalog/view/theme/default/image/banner/img-banner-06.jpg",\n            "url": "#",\n            "description": "Banner 1",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/default/image/banner/img-banner-02.png",\n            "url": "#",\n            "description": "Banner 2",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/default/image/banner/img-banner-03.png",\n            "url": "#",\n            "description": "Banner 3",\n            "visible": 1\n        }\n    ]\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(423, 0, 'adg_topal_xfec', 'config', 'config_section_best_sales_product', '{\n    "name": "feature-product",\n    "text": "Sản phẩm bán chạy",\n    "visible": 1,\n    "setting": {\n        "title": "Sản phẩm bán chạy",\n        "sub_title" : "Mô tả tiêu đề",\n        "auto_retrieve_data": 1,\n        "resource_type": 1,\n        "collection_id": 1,\n        "category_id": 1,\n        "autoplay": 1,\n        "autoplay_time": 5,\n        "loop": 1\n    },\n    "display": {\n        "grid": {\n            "quantity": 4,\n            "row": 1\n        },\n        "grid_mobile": {\n            "quantity": 2,\n            "row": 1\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(424, 0, 'adg_topal_xfec', 'config', 'config_section_new_product', '{\n    "name": "new-product",\n    "text": "Sản phẩm mới",\n    "visible": "1",\n    "setting": {\n        "title": "Sản phẩm mới",\n        "sub_title": "Mô tả tiêu đề",\n        "auto_retrieve_data": "0",\n        "resource_type": "2",\n        "collection_id": "1",\n        "category_id": "32",\n        "autoplay": "1",\n        "autoplay_time": "5",\n        "loop": "1"\n    },\n    "display": {\n        "grid": {\n            "quantity": "4",\n            "row": "1"\n        },\n        "grid_mobile": {\n            "quantity": "2",\n            "row": "1"\n        }\n    }\n}', '2021-11-08 16:57:19', '2022-01-19 13:54:24'),
(425, 0, 'adg_topal_xfec', 'config', 'config_section_best_views_product', '{\n    "name": "related-product",\n    "text": "Sản phẩm xem nhiều",\n    "visible": 1,\n    "setting": {\n        "title": "Sản phẩm xem nhiều"\n    },\n    "display": {\n        "grid": {\n            "quantity": 10\n        },\n        "grid_mobile": {\n            "quantity": 2\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(426, 0, 'adg_topal_xfec', 'config', 'config_section_blog', '{\n    "name": "blog",\n    "text": "Blog",\n    "visible": 1,\n    "setting": {\n        "title": "Blogs"\n    },\n    "display": {\n        "menu": [\n            {\n                "type": "existing",\n                "menu": {\n                    "id": 12,\n                    "name": "Danh sách bài viết 1"\n                }\n            },\n            {\n                "type": "manual",\n                "entries": [\n                    {\n                        "id": 10,\n                        "name": "Entry 10"\n                    },\n                    {\n                        "id": 11,\n                        "name": "Entry 11"\n                    }\n                ]\n            }\n        ],\n        "grid": {\n            "quantity": 3,\n            "row": 1\n        },\n        "grid_mobile": {\n            "quantity": 1,\n            "row": 1\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(427, 0, 'adg_topal_xfec', 'config', 'config_section_detail_product', '{\n    "name": "product-detail",\n    "text": "Chi tiết sản phẩm",\n    "visible": 1,\n    "display": {\n        "name": true,\n        "description": false,\n        "price": true,\n        "price-compare": false,\n        "status": false,\n        "sale": false,\n        "rate": false\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(428, 0, 'adg_topal_xfec', 'config', 'config_section_footer', '{\n    "name": "footer",\n    "text": "Footer",\n    "visible": "1",\n    "contact": {\n        "title": "Liên hệ chúng tôi",\n        "address": "37 Lê Văn Thiêm, Nhân Chính, Hà Nội&lt;br&gt;&lt;li&gt;Nhà máy: Km7, Đường 39, Thị trấn Yên Mỹ, Hưng Yên",\n        "phone-number": "1900 6828",\n        "email": "topal@austdoor.com",\n        "visible": "1"\n    },\n    "contact_more": [\n        {\n            "title": "Chi nhánh 1",\n            "address": "Duy Tan - Ha Noi",\n            "phone-number": "(+84)987654322",\n            "visible": "0"\n        }\n    ],\n    "collection": {\n        "title": "Bộ sưu tập",\n        "menu_id": "1",\n        "visible": "1"\n    },\n    "quick-links": {\n        "title": "ĐIỀU KHOẢN",\n        "menu_id": "9",\n        "visible": "1"\n    },\n    "subscribe": {\n        "title": "Đăng ký theo dõi",\n        "social_network": "1",\n        "visible": "1",\n        "youtube_visible": "1",\n        "facebook_visible": "1",\n        "instagram_visible": "1",\n        "shopee_visible": "1",\n        "tiki_visible": "1",\n        "sendo_visible": "1",\n        "lazada_visible": "1"\n    }\n}', '2021-11-08 16:57:19', '2021-12-24 10:15:59'),
(429, 0, 'adg_topal_xfec', 'config', 'config_section_header', '{\n    "name": "header",\n    "text": "Header",\n    "visible": "1",\n    "notify-bar": {\n        "name": "Thanh thông báo",\n        "visible": "1",\n        "content": "Mua sắm online thuận tiện và dễ dàng",\n        "url": "#"\n    },\n    "logo": {\n        "name": "Logo",\n        "url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/logotopaltrang.png",\n        "alt": "alt",\n        "height": ""\n    },\n    "menu": {\n        "name": "Menu",\n        "display-list": {\n            "name": "Menu chính",\n            "id": "1"\n        }\n    }\n}', '2021-11-08 16:57:19', '2022-01-25 10:34:17'),
(430, 0, 'adg_topal_xfec', 'config', 'config_section_hot_product', '{\n    "name": "hot-deals",\n    "text": "Sản phẩm khuyến mại",\n    "visible": 1,\n    "setting": {\n        "title": "Sản phẩm khuyến mại",\n        "sub_title" : "Mô tả tiêu đề",\n        "auto_retrieve_data": 1,\n        "resource_type": 1,\n        "collection_id": 1,\n        "category_id": 1,\n        "autoplay": 1,\n        "autoplay_time": 5,\n        "loop": 1\n    },\n    "display": {\n        "grid": {\n            "quantity": 4,\n            "row": 1\n        },\n        "grid_mobile": {\n            "quantity": 2,\n            "row": 1\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(431, 0, 'adg_topal_xfec', 'config', 'config_section_list_product', '{\n    "name": "categories",\n    "text": "Danh mục sản phẩm",\n    "visible": 0,\n    "setting": {\n        "title": "Danh mục sản phẩm"\n    },\n    "display": {\n        "menu": {\n            "id": 1,\n            "name": "Danh mục sản phẩm 1"\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(432, 0, 'adg_topal_xfec', 'config', 'config_section_partner', '{\n    "name": "partners",\n    "text": "Đối tác",\n    "visible": 1,\n    "limit-per-line": 4,\n    "display": [\n        {\n            "image-url": "/catalog/view/theme/novaon/asset/img/partner_1.jpg",\n            "url": "#",\n            "description": "Partner 1",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/novaon/asset/img/partner_2.jpg",\n            "url": "#",\n            "description": "Partner 2",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/novaon/asset/img/partner_3.jpg",\n            "url": "#",\n            "description": "Partner 3",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/novaon/asset/img/partner_4.jpg",\n            "url": "#",\n            "description": "Partner 4",\n            "visible": 1\n        }\n    ],\n    "setting": {\n        "autoplay": 1,\n        "loop": 1,\n        "autoplay_time": 5\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(433, 0, 'adg_topal_xfec', 'config', 'config_section_slideshow', '{\n    "name": "slide-show",\n    "text": "Slideshow",\n    "visible": "1",\n    "setting": {\n        "transition-time": "5"\n    },\n    "display": [\n        {\n            "image-url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/home-slide-1.jpg",\n            "url": "#",\n            "description": "slide_1",\n            "type": "image",\n            "video-url": "",\n            "id": "47897615"\n        },\n        {\n            "image-url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/home-slide-2.jpg",\n            "url": "#",\n            "description": "slide_2",\n            "type": "image",\n            "video-url": "",\n            "id": "25763179"\n        }\n    ]\n}', '2021-11-08 16:57:19', '2021-11-08 17:02:37'),
(434, 0, 'adg_topal_xfec', 'config', 'config_section_product_groups', '{\n    "name": "group products",\n    "text": "nhóm sản phẩm",\n    "visible": 1,\n    "list": [\n        {\n            "text": "Danh sách sản phẩm 1",\n            "visible": "0",\n            "setting": {\n                "title": "Danh sách sản phẩm 1",\n                "collection_id": "4",\n                "resource_type": 1,\n                "category_id": 1,\n                "sub_title": "Danh sách sản phẩm 1",\n                "autoplay": 1,\n                "autoplay_time": 5,\n                "loop": 1\n            },\n            "display": {\n                "grid": {\n                    "quantity": 6,\n                    "row": 1\n                },\n                "grid_mobile": {\n                    "quantity": 2,\n                    "row": 1\n                }\n            }\n        }\n    ]\n}', '2021-11-08 16:57:19', '2021-11-09 09:49:59'),
(435, 0, 'adg_topal_xfec', 'config', 'config_section_content_customize', '{\n    "name": "content_customize",\n    "title": "Nội dung tuỳ chỉnh",\n    "display": [\n        {\n            "name": "mot-san-pham-cua-tap-doan-austdoor-0",\n            "title": "Một sản phẩm của tập đoàn Austdoor",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/group-1036_OnX4jQm.png",\n                "title": "Một sản phẩm của tập đoàn Austdoor",\n                "description": "Topal là thành viên của Tập đoàn Austdoor – đơn vị đã có 15 năm uy tín trong lĩnh vực cung cấp các giải pháp tổng thể về cửa tại Việt Nam.\\n\\n\\nQuy trình sản xuất chất lượng\\nSản phẩm nhôm Topal sản xuất ",\n                "html": ""\n            }\n        },\n        {\n            "name": "quy-trinh-san-xuat-chat-luong-1",\n            "title": "Quy trình sản xuất chất lượng",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/group-1035_dw3pgNY.png",\n                "title": "Quy trình sản xuất chất lượng",\n                "description": "Sản phẩm nhôm Topal sản xuất qua nhiều công đoạn với những tiêu chuẩn kỹ thuật khắt khe, kiểm soát chất lượng nghiêm ngặt.",\n                "html": ""\n            }\n        },\n        {\n            "name": "niem-no-than-thien-voi-khach-hang-2",\n            "title": "Niềm nở thân thiện với khách hàng",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/group-1037_mRg3X2Z.png",\n                "title": "Niềm nở thân thiện với khách hàng",\n                "description": "Không ngừng nỗ lực để thỏa mãn khách hàng. Chất lượng phục vụ là nền tảng để xây dựng thương hiệu vững mạnh.",\n                "html": ""\n            }\n        },\n        {\n            "name": "san-pham-da-dang-dich-vu-chat-luong-tot-nhat-3",\n            "title": "Sản phẩm đa dạng dịch vụ chất lượng tốt nhất",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/group-1034_NCpQukk.png",\n                "title": "Sản phẩm đa dạng dịch vụ chất lượng tốt nhất",\n                "description": "Các sản phẩm phong phú, đa dạng. Đáp ứng đầy đủ nhu cầu của khách hàng trong xây dựng các công trình.",\n                "html": ""\n            }\n        },\n        {\n            "name": "cung-ung-vuot-troi-4",\n            "title": "Cung ứng vượt trội",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/tools-1.png",\n                "title": "Cung ứng vượt trội",\n                "description": "Cung ứng lớn hàng trăm nghìn tấn nhôm\\/năm",\n                "html": ""\n            }\n        },\n        {\n            "name": "cham-soc-khach-hang-5",\n            "title": "Chăm sóc khách hàng",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/guarantee-1.png",\n                "title": "Chăm sóc khách hàng",\n                "description": "Tổng đài chăm sóc khách hàng 19006828 toàn quốc",\n                "html": ""\n            }\n        },\n        {\n            "name": "dong-hanh-cung-dai-ly-6",\n            "title": "Đồng hành cùng đại lý",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/group-1_1dpmxxf.png",\n                "title": "Đồng hành cùng đại lý",\n                "description": "Ưu đãi về kinh doanh, marketing , hỗ trợ các nguồn lực",\n                "html": ""\n            }\n        },\n        {\n            "name": "bao-hanh-7",\n            "title": "Bảo hành",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/group.png",\n                "title": "Bảo hành",\n                "description": "Bảo hành thanh nhôm hệ và phụ kiện dành cho người tiêu dùng",\n                "html": ""\n            }\n        }\n    ]\n}', '2021-11-08 16:57:19', '2021-12-20 13:35:39'),
(436, 0, 'adg_topal_xfec', 'config', 'config_section_customize_layout', '{\n    "config_theme_color": {\n        "main": {\n            "main": {\n                "hex": "#DA3A36",\n                "visible": "1"\n            },\n            "button": {\n                "hex": "#DA3A36",\n                "visible": "1"\n            }\n        }\n    },\n    "config_theme_text": {\n        "all": {\n            "title": "Font Roboto",\n            "font": "Roboto",\n            "supported-fonts": [\n                "Open Sans",\n                "IBM Plex Sans",\n                "Courier New",\n                "Roboto",\n                "Nunito",\n                "Arial",\n                "DejaVu Sans",\n                "Tahoma",\n                "Time News Roman",\n                "Mulish"\n            ]\n        }\n    },\n    "image_size_suggestion": {\n        "favicon": [\n            "35",\n            "35"\n        ],\n        "logo": [\n            "146",\n            "28"\n        ],\n        "home": {\n            "banner_1": [\n                "1240",\n                "500"\n            ],\n            "banner_2": [\n                "360",\n                "205"\n            ],\n            "banner_3": [\n                "555",\n                "210"\n            ],\n            "slide_show": [\n                "1905",\n                "770"\n            ]\n        },\n        "category": {\n            "banner_1": [\n                "263",\n                "307"\n            ]\n        }\n    },\n    "app_config_theme": [\n        {\n            "name": "Trang chủ",\n            "value": "home_page",\n            "position": [\n                {\n                    "name": "Đầu trang",\n                    "value": "bestme_app_after_header"\n                },\n                {\n                    "name": "Dưới slide",\n                    "value": "bestme_app_after_slide"\n                },\n                {\n                    "name": "Dưới danh sách sản phẩm",\n                    "value": "bestme_app_after_product"\n                },\n                {\n                    "name": "Trên cuối trang",\n                    "value": "bestme_app_before_footer"\n                }\n            ]\n        },\n        {\n            "name": "Sản phẩm",\n            "value": "product",\n            "position": [\n                {\n                    "name": "Đầu trang",\n                    "value": "bestme_app_after_header"\n                },\n                {\n                    "name": "Dưới danh sách sản phẩm",\n                    "value": "bestme_app_after_product"\n                },\n                {\n                    "name": "Trên cuối trang",\n                    "value": "bestme_app_before_footer"\n                }\n            ]\n        }\n    ],\n    "default_customize_layout": [\n        {\n            "name": "txt_slide_show_banner",\n            "file": "slideshow-banner"\n        },\n        {\n            "name": "txt_about_us",\n            "file": "about-us"\n        },\n        {\n            "name": "txt_why_choose",\n            "file": "why-choose"\n        },\n        {\n            "name": "txt_service",\n            "file": "service"\n        },\n        {\n            "name": "txt_block_product",\n            "file": "block-products"\n        },\n        {\n            "name": "txt_block_projects",\n            "file": "projects-box"\n        },\n        {\n            "name": "txt_intro_award",\n            "file": "intro-award"\n        },\n        {\n            "name": "txt_block_news",\n            "file": "news-box"\n        }\n    ],\n    "version": "1"\n}', '2021-11-08 16:57:19', '2021-11-08 17:39:43'),
(437, 0, 'adg_topal_xfec', 'config', 'config_section_category_sections', '{\n    "name": "Theme Config",\n    "version": "1.0",\n    "section": {\n        "banner": {\n            "name": "banner",\n            "text": "Banner",\n            "visible": 1\n        },\n        "filter": {\n            "name": "filter",\n            "text": "Filter",\n            "visible": 1\n        },\n        "product_category": {\n            "name": "product_category",\n            "text": "Product Category",\n            "visible": 1\n        },\n        "product_list": {\n            "name": "product_list",\n            "text": "Product List",\n            "visible": 1\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(438, 0, 'adg_topal_xfec', 'config', 'config_section_category_banner', '{\n    "name": "banner",\n    "text": "Banner",\n    "visible": 1,\n    "display": [\n        {\n            "image-url": "/catalog/view/theme/default/image/banner/img-banner-06.jpg",\n            "url": "#",\n            "description": "Banner 1",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/default/image/banner/img-banner-02.png",\n            "url": "#",\n            "description": "Banner 2",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/default/image/banner/img-banner-03.png",\n            "url": "#",\n            "description": "Banner 3",\n            "visible": 1\n        }\n    ]\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(439, 0, 'adg_topal_xfec', 'config', 'config_section_category_filter', '{\n    "name": "filter",\n    "text": "Bộ lọc",\n    "visible": 1,\n    "setting": {\n        "title": "Bộ lọc"\n    },\n    "display": {\n        "supplier": {\n            "title": "Nhà cung cấp",\n            "visible": 1\n        },\n        "product-type": {\n            "title": "Loại sản phẩm",\n            "visible": 1\n        },\n        "collection": {\n            "title": "Bộ sưu tập",\n            "visible": 1\n        },\n        "property": {\n            "title": "Lọc theo tt - k dung",\n            "visible": 1,\n            "prop": [\n                "all",\n                "color",\n                "weight",\n                "size"\n            ]\n        },\n        "product-price": {\n            "title": "Giá sản phẩm",\n            "visible": 1,\n            "range": {\n                "from": 0,\n                "to": 100000000\n            }\n        },\n        "tag": {\n            "title": "Tag",\n            "visible": 1\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(440, 0, 'adg_topal_xfec', 'config', 'config_section_category_product_category', '{\n    "name": "product-category",\n    "text": "Danh mục sản phẩm",\n    "visible": 1,\n    "setting": {\n        "title": "Danh mục sản phẩm"\n    },\n    "display": {\n        "menu": {\n            "id": 1,\n            "name": "Danh mục sản phẩm 1"\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(441, 0, 'adg_topal_xfec', 'config', 'config_section_category_product_list', '{\n    "name": "product-list",\n    "text": "Danh sách sản phẩm",\n    "visible": 1,\n    "setting": {\n        "title": "Danh sách sản phẩm"\n    },\n    "display": {\n        "grid": {\n            "quantity": 10\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(442, 0, 'adg_topal_xfec', 'config', 'config_section_product_detail_sections', '{\n    "name": "Theme Config",\n    "version": "1.0",\n    "section": {\n        "related_product": {\n            "name": "related_product",\n            "text": "Related Product",\n            "visible": 1\n        },\n        "template": {\n            "name": "template",\n            "text": "Template",\n            "visible": 1\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(443, 0, 'adg_topal_xfec', 'config', 'config_section_product_detail_related_product', '{\n    "name": "related-product",\n    "text": "Sản phẩm liên quan",\n    "visible": "1",\n    "setting": {\n        "title": "Sản phẩm liên quan",\n        "auto_retrieve_data": "0",\n        "resource_type": "2",\n        "collection_id": "1",\n        "category_id": "32",\n        "autoplay": "1",\n        "autoplay_time": "5",\n        "loop": "1"\n    },\n    "display": {\n        "grid": {\n            "quantity": "4"\n        },\n        "grid_mobile": {\n            "quantity": "2"\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-09 09:50:48'),
(444, 0, 'adg_topal_xfec', 'config', 'config_section_product_detail_template', '{\n    "name": "template",\n    "text": "Giao diện",\n    "visible": 1,\n    "display": {\n        "template": {\n            "id": 10\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(445, 0, 'adg_topal_xfec', 'config', 'config_section_blog_sections', '{\n    "name": "Theme Config",\n    "version": "1.0",\n    "section": {\n        "blog_category": {\n            "name": "blog_category",\n            "text": "Blog Category",\n            "visible": 1\n        },\n        "blog_list": {\n            "name": "blog_list",\n            "text": "Blog List",\n            "visible": 1\n        },\n        "latest_blog": {\n            "name": "latest_blog",\n            "text": "Latest Blog",\n            "visible": 1\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(446, 0, 'adg_topal_xfec', 'config', 'config_section_blog_blog_category', '{\n    "name": "blog-category",\n    "text": "Danh mục bài viết",\n    "visible": 1,\n    "setting": {\n        "title": "Danh mục bài viết"\n    },\n    "display": {\n        "menu": {\n            "id": 1,\n            "name": "Danh mục bài viết 1"\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(447, 0, 'adg_topal_xfec', 'config', 'config_section_blog_blog_list', '{\n    "name": "blog-list",\n    "text": "Danh sách bài viết",\n    "visible": 1,\n    "display": {\n        "grid": {\n            "quantity": 20\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(448, 0, 'adg_topal_xfec', 'config', 'config_section_blog_latest_blog', '{\n    "name": "latest-blog",\n    "text": "Bài viết mới nhất",\n    "visible": 1,\n    "setting": {\n        "title": "Bài viết mới nhất"\n    },\n    "display": {\n        "grid": {\n            "quantity": 10\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(449, 0, 'adg_topal_xfec', 'config', 'config_section_contact_sections', '{\n    "name": "Theme Config",\n    "version": "1.0",\n    "section": {\n        "map": {\n            "name": "map",\n            "text": "Bản đồ",\n            "address": "<iframe src=\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\" width=\\"600\\" height=\\"450\\" frameborder=\\"0\\" style=\\"border:0\\" allowfullscreen></iframe>",\n            "visible": 1\n        },\n        "info": {\n            "name": "info",\n            "text": "Thông tin cửa hàng",\n            "email": "contact-us@novaon.asia",\n            "phone": "0000000000",\n            "address": "address",\n            "visible": 1\n        },\n        "form": {\n            "name": "form",\n            "title": "Form liên hệ",\n            "email": "email@mail.com",\n            "visible": 1\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(450, 0, 'adg_topal_xfec', 'config', 'config_section_contact_map', '{   //////// CÁI NÀY KHÔNG DÙNG NỮA\n    "name": "map",\n    "text": "Bản đồ",\n    "visible": 1,\n    "setting": {\n        "title": "Bản đồ"\n    },\n    "display": {\n        "address": "<iframe src=\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\" width=\\"600\\" height=\\"450\\" frameborder=\\"0\\" style=\\"border:0\\" allowfullscreen></iframe>"\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(451, 0, 'adg_topal_xfec', 'config', 'config_section_contact_contact', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\n    "name": "contact",\n    "text": "Liên hệ",\n    "visible": 1,\n    "setting": {\n        "title": "Vị trí của chúng tôi"\n    },\n    "display": {\n        "store": {\n            "title": "Gian hàng",\n            "content": "Số 12 Dịch Vọng Hậu, Cầu Giấy, Hà Nội",\n            "visible": 1\n        },\n        "telephone": {\n            "title": "Điện thoại",\n            "content": "(+84) 987 654 321",\n            "visible": 1\n        },\n        "social_follow": {\n            "socials": [\n                {\n                    "name": "facebook",\n                    "text": "Facebook",\n                    "visible": 1\n                },\n                {\n                    "name": "twitter",\n                    "text": "Twitter",\n                    "visible": 1\n                },\n                {\n                    "name": "instagram",\n                    "text": "Instagram",\n                    "visible": 1\n                },\n                {\n                    "name": "tumblr",\n                    "text": "Tumblr",\n                    "visible": 1\n                },\n                {\n                    "name": "youtube",\n                    "text": "Youtube",\n                    "visible": 1\n                },\n                {\n                    "name": "googleplus",\n                    "text": "Google Plus",\n                    "visible": 1\n                }\n            ],\n            "visible": 1\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(452, 0, 'adg_topal_xfec', 'config', 'config_section_contact_form', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\n    "name": "form",\n    "text": "Biểu mẫu",\n    "visible": 1,\n    "setting": {\n        "title": "Liên hệ với chúng tôi"\n    },\n    "data": {\n        "email": "sale.247@xshop.com"\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(453, 0, 'adg_topal_xfec', 'config', 'config_section_rate', '{\n  "name": "rate",\n  "text": "Đánh giá website",\n  "visible": 1,\n  "setting": {\n    "title": "Đánh giá website",\n    "sub_title" : "Mô tả tiêu đề",\n    "auto_retrieve_data": 1,\n    "resource_type": 1,\n    "autoplay": 1,\n    "autoplay_time": 5,\n    "loop": 1\n  },\n  "display": {\n    "grid": {\n      "quantity": 3,\n      "row": 1\n    },\n    "grid_mobile": {\n      "quantity": 1,\n      "row": 1\n    }\n  }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19');

-- --------------------------------------------------------

--
-- Table structure for table `oc_translation`
--

CREATE TABLE `oc_translation` (
  `translation_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_upload`
--

CREATE TABLE `oc_upload` (
  `upload_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_user`
--

CREATE TABLE `oc_user` (
  `user_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(500) NOT NULL,
  `password` varchar(100) NOT NULL,
  `password_temp` varchar(100) DEFAULT NULL,
  `salt` varchar(9) NOT NULL,
  `salt_temp` varchar(9) DEFAULT NULL,
  `firstname` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `telephone` varchar(32) DEFAULT NULL,
  `image` varchar(1024) NOT NULL,
  `code` varchar(40) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `info` varchar(1024) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `permission` text,
  `date_added` datetime NOT NULL,
  `last_logged_in` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_user_group`
--

CREATE TABLE `oc_user_group` (
  `user_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_user_group`
--

INSERT INTO `oc_user_group` (`user_group_id`, `name`, `permission`) VALUES
(1, 'Administrator', '{"access":["app_store\\/detail","theme\\/ecommerce","settings\\/user_group","cash_flow\\/cash_flow","cash_flow\\/receipt_voucher","cash_flow\\/payment_voucher","app_store\\/my_app","extension\\/extension\\/appstore","common\\/app_config","blog\\/blog","blog\\/category","catalog\\/attribute","catalog\\/attribute_group","catalog\\/category","catalog\\/collection","catalog\\/download","catalog\\/filter","catalog\\/information","catalog\\/manufacturer","catalog\\/option","catalog\\/product","catalog\\/recurring","catalog\\/review","catalog\\/store_receipt","catalog\\/warehouse","catalog\\/store","catalog\\/cost_adjustment_receipt","catalog\\/store_take_receipt","catalog\\/store_transfer_receipt","common\\/cloudinary_upload","common\\/column_left","common\\/custom_column_left","common\\/custom_header","common\\/delivery_api","common\\/developer","common\\/filemanager","common\\/forgotten_orig","common\\/login_orig","common\\/profile","common\\/reset_orig","common\\/security","custom\\/group_menu","custom\\/menu","custom\\/menu_item","custom\\/preference","custom\\/theme","custom\\/theme_develop","customer\\/custom_field","customer\\/customer","customer\\/customer_approval","customer\\/customer_group","design\\/banner","design\\/layout","design\\/seo_url","design\\/theme","design\\/translation","discount\\/discount","discount\\/coupon","discount\\/setting","event\\/language","event\\/statistics","event\\/theme","extension\\/analytics\\/google","extension\\/captcha\\/basic","extension\\/captcha\\/google","extension\\/dashboard\\/activity","extension\\/dashboard\\/chart","extension\\/dashboard\\/customer","extension\\/dashboard\\/map","extension\\/dashboard\\/online","extension\\/dashboard\\/order","extension\\/dashboard\\/recent","extension\\/dashboard\\/sale","extension\\/extension\\/analytics","extension\\/extension\\/captcha","extension\\/extension\\/dashboard","extension\\/extension\\/feed","extension\\/extension\\/fraud","extension\\/extension\\/menu","extension\\/extension\\/module","extension\\/extension\\/payment","extension\\/extension\\/report","extension\\/extension\\/shipping","extension\\/extension\\/theme","extension\\/extension\\/total","extension\\/feed\\/google_base","extension\\/feed\\/google_sitemap","extension\\/feed\\/openbaypro","extension\\/fraud\\/fraudlabspro","extension\\/fraud\\/ip","extension\\/fraud\\/maxmind","extension\\/module\\/account","extension\\/module\\/age_restriction","extension\\/module\\/amazon_login","extension\\/module\\/amazon_pay","extension\\/module\\/banner","extension\\/module\\/bestseller","extension\\/module\\/carousel","extension\\/module\\/category","extension\\/module\\/customer_welcome","extension\\/module\\/divido_calculator","extension\\/module\\/ebay_listing","extension\\/module\\/featured","extension\\/module\\/filter","extension\\/module\\/google_hangouts","extension\\/module\\/hello_world","extension\\/module\\/html","extension\\/module\\/information","extension\\/module\\/klarna_checkout_module","extension\\/module\\/latest","extension\\/module\\/laybuy_layout","extension\\/module\\/modification_editor","extension\\/module\\/pilibaba_button","extension\\/module\\/pp_braintree_button","extension\\/module\\/pp_button","extension\\/module\\/pp_login","extension\\/module\\/recommendation","extension\\/module\\/sagepay_direct_cards","extension\\/module\\/sagepay_server_cards","extension\\/module\\/simple_blog","extension\\/module\\/simple_blog\\/article","extension\\/module\\/simple_blog\\/author","extension\\/module\\/simple_blog\\/category","extension\\/module\\/simple_blog\\/comment","extension\\/module\\/simple_blog\\/install","extension\\/module\\/simple_blog\\/report","extension\\/module\\/simple_blog_category","extension\\/module\\/slideshow","extension\\/module\\/so_categories","extension\\/module\\/so_category_slider","extension\\/module\\/so_deals","extension\\/module\\/so_extra_slider","extension\\/module\\/so_facebook_message","extension\\/module\\/so_filter_shop_by","extension\\/module\\/so_home_slider","extension\\/module\\/so_html_content","extension\\/module\\/so_latest_blog","extension\\/module\\/so_listing_tabs","extension\\/module\\/so_megamenu","extension\\/module\\/so_newletter_custom_popup","extension\\/module\\/so_onepagecheckout","extension\\/module\\/so_page_builder","extension\\/module\\/so_quickview","extension\\/module\\/so_searchpro","extension\\/module\\/so_sociallogin","extension\\/module\\/so_tools","extension\\/module\\/soconfig","extension\\/module\\/special","extension\\/module\\/store","extension\\/module\\/theme_builder_config","extension\\/openbay\\/amazon","extension\\/openbay\\/amazon_listing","extension\\/openbay\\/amazon_product","extension\\/openbay\\/amazonus","extension\\/openbay\\/amazonus_listing","extension\\/openbay\\/amazonus_product","extension\\/openbay\\/ebay","extension\\/openbay\\/ebay_profile","extension\\/openbay\\/ebay_template","extension\\/openbay\\/etsy","extension\\/openbay\\/etsy_product","extension\\/openbay\\/etsy_shipping","extension\\/openbay\\/etsy_shop","extension\\/openbay\\/fba","extension\\/payment\\/alipay","extension\\/payment\\/alipay_cross","extension\\/payment\\/amazon_login_pay","extension\\/payment\\/authorizenet_aim","extension\\/payment\\/authorizenet_sim","extension\\/payment\\/bank_transfer","extension\\/payment\\/bluepay_hosted","extension\\/payment\\/bluepay_redirect","extension\\/payment\\/cardconnect","extension\\/payment\\/cardinity","extension\\/payment\\/cheque","extension\\/payment\\/cod","extension\\/payment\\/divido","extension\\/payment\\/eway","extension\\/payment\\/firstdata","extension\\/payment\\/firstdata_remote","extension\\/payment\\/free_checkout","extension\\/payment\\/g2apay","extension\\/payment\\/globalpay","extension\\/payment\\/globalpay_remote","extension\\/payment\\/klarna_account","extension\\/payment\\/klarna_checkout","extension\\/payment\\/klarna_invoice","extension\\/payment\\/laybuy","extension\\/payment\\/liqpay","extension\\/payment\\/nochex","extension\\/payment\\/paymate","extension\\/payment\\/paypoint","extension\\/payment\\/payza","extension\\/payment\\/perpetual_payments","extension\\/payment\\/pilibaba","extension\\/payment\\/pp_braintree","extension\\/payment\\/pp_express","extension\\/payment\\/pp_payflow","extension\\/payment\\/pp_payflow_iframe","extension\\/payment\\/pp_pro","extension\\/payment\\/pp_pro_iframe","extension\\/payment\\/pp_standard","extension\\/payment\\/realex","extension\\/payment\\/realex_remote","extension\\/payment\\/sagepay_direct","extension\\/payment\\/sagepay_server","extension\\/payment\\/sagepay_us","extension\\/payment\\/securetrading_pp","extension\\/payment\\/securetrading_ws","extension\\/payment\\/skrill","extension\\/payment\\/squareup","extension\\/payment\\/twocheckout","extension\\/payment\\/web_payment_software","extension\\/payment\\/wechat_pay","extension\\/payment\\/worldpay","extension\\/report\\/customer_activity","extension\\/report\\/customer_order","extension\\/report\\/customer_reward","extension\\/report\\/customer_search","extension\\/report\\/customer_transaction","extension\\/report\\/marketing","extension\\/report\\/product_purchased","extension\\/report\\/product_viewed","extension\\/report\\/sale_coupon","extension\\/report\\/sale_order","extension\\/report\\/sale_return","extension\\/report\\/sale_shipping","extension\\/report\\/sale_tax","extension\\/shipping\\/auspost","extension\\/shipping\\/citylink","extension\\/shipping\\/ec_ship","extension\\/shipping\\/fedex","extension\\/shipping\\/flat","extension\\/shipping\\/free","extension\\/shipping\\/item","extension\\/shipping\\/parcelforce_48","extension\\/shipping\\/pickup","extension\\/shipping\\/royal_mail","extension\\/shipping\\/ups","extension\\/shipping\\/usps","extension\\/shipping\\/weight","extension\\/theme\\/default","extension\\/theme\\/dunght","extension\\/theme\\/novaon","extension\\/total\\/coupon","extension\\/total\\/credit","extension\\/total\\/handling","extension\\/total\\/klarna_fee","extension\\/total\\/low_order_fee","extension\\/total\\/reward","extension\\/total\\/shipping","extension\\/total\\/sub_total","extension\\/total\\/tax","extension\\/total\\/total","extension\\/total\\/voucher","localisation\\/country","localisation\\/currency","localisation\\/geo_zone","localisation\\/language","localisation\\/length_class","localisation\\/location","localisation\\/order_status","localisation\\/return_action","localisation\\/return_reason","localisation\\/return_status","localisation\\/stock_status","localisation\\/tax_class","localisation\\/tax_rate","localisation\\/weight_class","localisation\\/zone","mail\\/affiliate","mail\\/customer","mail\\/forgotten","mail\\/forgotten_orig","mail\\/return","mail\\/reward","mail\\/transaction","marketing\\/contact","marketing\\/coupon","marketing\\/marketing","marketplace\\/api","marketplace\\/event","marketplace\\/extension","marketplace\\/install","marketplace\\/installer","marketplace\\/marketplace","marketplace\\/modification","marketplace\\/openbay","online_store\\/contents","online_store\\/domain","online_store\\/google_shopping","report\\/online","report\\/overview","report\\/report","report\\/statistics","report\\/product","report\\/order","report\\/financial","report\\/staff","report\\/store","sale\\/order","sale\\/return_receipt","sale\\/recurring","sale\\/return","sale\\/voucher","sale\\/voucher_theme","sale_channel\\/pos_novaon","extension\\/appstore\\/mar_ons_chatbot","extension\\/appstore\\/mar_ons_onfluencer","sale_channel\\/novaonx_social","section\\/rate","rate\\/rate","section\\/blog","section\\/customize_layout","section\\/banner","section\\/best_sales_product","section\\/best_views_product","section\\/blog","section\\/content_customize","section\\/detail_product","section\\/footer","section\\/header","section\\/hot_product","section\\/list_product","section\\/new_product","section\\/partner","section\\/preview","section\\/sections","section\\/slideshow","section_blog\\/blog_category","section_blog\\/blog_list","section_blog\\/latest_blog","section_blog\\/sections","section_category\\/banner","section_category\\/filter","section_category\\/product_category","section_category\\/product_list","section_category\\/sections","section_contact\\/contact","section_contact\\/form","section_contact\\/map","section_contact\\/sections","section_product_detail\\/related_product","section_product_detail\\/sections","setting\\/setting","setting\\/store","settings\\/account","settings\\/classify","settings\\/delivery","settings\\/general","settings\\/payment","settings\\/settings","settings\\/notify","startup\\/error","startup\\/event","startup\\/login","startup\\/permission","startup\\/router","startup\\/sass","startup\\/startup","super\\/dashboard","theme\\/color","theme\\/favicon","theme\\/section_theme","theme\\/social","theme\\/text","tool\\/backup","tool\\/log","tool\\/upload","user\\/api","user\\/user","user\\/user_permission"],"modify":["app_store\\/detail","theme\\/ecommerce","settings\\/user_group","cash_flow\\/cash_flow","cash_flow\\/receipt_voucher","cash_flow\\/payment_voucher","app_store\\/my_app","extension\\/extension\\/appstore","common\\/app_config", "blog\\/blog","blog\\/category","catalog\\/attribute","catalog\\/attribute_group","catalog\\/category","catalog\\/collection","catalog\\/download","catalog\\/filter","catalog\\/information","catalog\\/manufacturer","catalog\\/option","catalog\\/product","catalog\\/recurring","catalog\\/review","catalog\\/store_receipt","catalog\\/warehouse","catalog\\/store","catalog\\/cost_adjustment_receipt","catalog\\/store_take_receipt","catalog\\/store_transfer_receipt","common\\/cloudinary_upload","common\\/column_left","common\\/custom_column_left","common\\/custom_header","common\\/delivery_api","common\\/developer","common\\/filemanager","common\\/forgotten_orig","common\\/login_orig","common\\/profile","common\\/reset_orig","common\\/security","custom\\/group_menu","custom\\/menu","custom\\/menu_item","custom\\/preference","custom\\/theme","custom\\/theme_develop","customer\\/custom_field","customer\\/customer","customer\\/customer_approval","customer\\/customer_group","design\\/banner","design\\/layout","design\\/seo_url","design\\/theme","design\\/translation","discount\\/discount","discount\\/coupon","discount\\/setting","event\\/language","event\\/statistics","event\\/theme","extension\\/analytics\\/google","extension\\/captcha\\/basic","extension\\/captcha\\/google","extension\\/dashboard\\/activity","extension\\/dashboard\\/chart","extension\\/dashboard\\/customer","extension\\/dashboard\\/map","extension\\/dashboard\\/online","extension\\/dashboard\\/order","extension\\/dashboard\\/recent","extension\\/dashboard\\/sale","extension\\/extension\\/analytics","extension\\/extension\\/captcha","extension\\/extension\\/dashboard","extension\\/extension\\/feed","extension\\/extension\\/fraud","extension\\/extension\\/menu","extension\\/extension\\/module","extension\\/extension\\/payment","extension\\/extension\\/report","extension\\/extension\\/shipping","extension\\/extension\\/theme","extension\\/extension\\/total","extension\\/feed\\/google_base","extension\\/feed\\/google_sitemap","extension\\/feed\\/openbaypro","extension\\/fraud\\/fraudlabspro","extension\\/fraud\\/ip","extension\\/fraud\\/maxmind","extension\\/module\\/account","extension\\/module\\/age_restriction","extension\\/module\\/amazon_login","extension\\/module\\/amazon_pay","extension\\/module\\/banner","extension\\/module\\/bestseller","extension\\/module\\/carousel","extension\\/module\\/category","extension\\/module\\/customer_welcome","extension\\/module\\/divido_calculator","extension\\/module\\/ebay_listing","extension\\/module\\/featured","extension\\/module\\/filter","extension\\/module\\/google_hangouts","extension\\/module\\/hello_world","extension\\/module\\/html","extension\\/module\\/information","extension\\/module\\/klarna_checkout_module","extension\\/module\\/latest","extension\\/module\\/laybuy_layout","extension\\/module\\/modification_editor","extension\\/module\\/pilibaba_button","extension\\/module\\/pp_braintree_button","extension\\/module\\/pp_button","extension\\/module\\/pp_login","extension\\/module\\/recommendation","extension\\/module\\/sagepay_direct_cards","extension\\/module\\/sagepay_server_cards","extension\\/module\\/simple_blog","extension\\/module\\/simple_blog\\/article","extension\\/module\\/simple_blog\\/author","extension\\/module\\/simple_blog\\/category","extension\\/module\\/simple_blog\\/comment","extension\\/module\\/simple_blog\\/install","extension\\/module\\/simple_blog\\/report","extension\\/module\\/simple_blog_category","extension\\/module\\/slideshow","extension\\/module\\/so_categories","extension\\/module\\/so_category_slider","extension\\/module\\/so_deals","extension\\/module\\/so_extra_slider","extension\\/module\\/so_facebook_message","extension\\/module\\/so_filter_shop_by","extension\\/module\\/so_home_slider","extension\\/module\\/so_html_content","extension\\/module\\/so_latest_blog","extension\\/module\\/so_listing_tabs","extension\\/module\\/so_megamenu","extension\\/module\\/so_newletter_custom_popup","extension\\/module\\/so_onepagecheckout","extension\\/module\\/so_page_builder","extension\\/module\\/so_quickview","extension\\/module\\/so_searchpro","extension\\/module\\/so_sociallogin","extension\\/module\\/so_tools","extension\\/module\\/soconfig","extension\\/module\\/special","extension\\/module\\/store","extension\\/module\\/theme_builder_config","extension\\/openbay\\/amazon","extension\\/openbay\\/amazon_listing","extension\\/openbay\\/amazon_product","extension\\/openbay\\/amazonus","extension\\/openbay\\/amazonus_listing","extension\\/openbay\\/amazonus_product","extension\\/openbay\\/ebay","extension\\/openbay\\/ebay_profile","extension\\/openbay\\/ebay_template","extension\\/openbay\\/etsy","extension\\/openbay\\/etsy_product","extension\\/openbay\\/etsy_shipping","extension\\/openbay\\/etsy_shop","extension\\/openbay\\/fba","extension\\/payment\\/alipay","extension\\/payment\\/alipay_cross","extension\\/payment\\/amazon_login_pay","extension\\/payment\\/authorizenet_aim","extension\\/payment\\/authorizenet_sim","extension\\/payment\\/bank_transfer","extension\\/payment\\/bluepay_hosted","extension\\/payment\\/bluepay_redirect","extension\\/payment\\/cardconnect","extension\\/payment\\/cardinity","extension\\/payment\\/cheque","extension\\/payment\\/cod","extension\\/payment\\/divido","extension\\/payment\\/eway","extension\\/payment\\/firstdata","extension\\/payment\\/firstdata_remote","extension\\/payment\\/free_checkout","extension\\/payment\\/g2apay","extension\\/payment\\/globalpay","extension\\/payment\\/globalpay_remote","extension\\/payment\\/klarna_account","extension\\/payment\\/klarna_checkout","extension\\/payment\\/klarna_invoice","extension\\/payment\\/laybuy","extension\\/payment\\/liqpay","extension\\/payment\\/nochex","extension\\/payment\\/paymate","extension\\/payment\\/paypoint","extension\\/payment\\/payza","extension\\/payment\\/perpetual_payments","extension\\/payment\\/pilibaba","extension\\/payment\\/pp_braintree","extension\\/payment\\/pp_express","extension\\/payment\\/pp_payflow","extension\\/payment\\/pp_payflow_iframe","extension\\/payment\\/pp_pro","extension\\/payment\\/pp_pro_iframe","extension\\/payment\\/pp_standard","extension\\/payment\\/realex","extension\\/payment\\/realex_remote","extension\\/payment\\/sagepay_direct","extension\\/payment\\/sagepay_server","extension\\/payment\\/sagepay_us","extension\\/payment\\/securetrading_pp","extension\\/payment\\/securetrading_ws","extension\\/payment\\/skrill","extension\\/payment\\/squareup","extension\\/payment\\/twocheckout","extension\\/payment\\/web_payment_software","extension\\/payment\\/wechat_pay","extension\\/payment\\/worldpay","extension\\/report\\/customer_activity","extension\\/report\\/customer_order","extension\\/report\\/customer_reward","extension\\/report\\/customer_search","extension\\/report\\/customer_transaction","extension\\/report\\/marketing","extension\\/report\\/product_purchased","extension\\/report\\/product_viewed","extension\\/report\\/sale_coupon","extension\\/report\\/sale_order","extension\\/report\\/sale_return","extension\\/report\\/sale_shipping","extension\\/report\\/sale_tax","extension\\/shipping\\/auspost","extension\\/shipping\\/citylink","extension\\/shipping\\/ec_ship","extension\\/shipping\\/fedex","extension\\/shipping\\/flat","extension\\/shipping\\/free","extension\\/shipping\\/item","extension\\/shipping\\/parcelforce_48","extension\\/shipping\\/pickup","extension\\/shipping\\/royal_mail","extension\\/shipping\\/ups","extension\\/shipping\\/usps","extension\\/shipping\\/weight","extension\\/theme\\/default","extension\\/theme\\/dunght","extension\\/theme\\/novaon","extension\\/total\\/coupon","extension\\/total\\/credit","extension\\/total\\/handling","extension\\/total\\/klarna_fee","extension\\/total\\/low_order_fee","extension\\/total\\/reward","extension\\/total\\/shipping","extension\\/total\\/sub_total","extension\\/total\\/tax","extension\\/total\\/total","extension\\/total\\/voucher","localisation\\/country","localisation\\/currency","localisation\\/geo_zone","localisation\\/language","localisation\\/length_class","localisation\\/location","localisation\\/order_status","localisation\\/return_action","localisation\\/return_reason","localisation\\/return_status","localisation\\/stock_status","localisation\\/tax_class","localisation\\/tax_rate","localisation\\/weight_class","localisation\\/zone","mail\\/affiliate","mail\\/customer","mail\\/forgotten","mail\\/forgotten_orig","mail\\/return","mail\\/reward","mail\\/transaction","marketing\\/contact","marketing\\/coupon","marketing\\/marketing","marketplace\\/api","marketplace\\/event","marketplace\\/extension","marketplace\\/install","marketplace\\/installer","marketplace\\/marketplace","marketplace\\/modification","marketplace\\/openbay","online_store\\/contents","online_store\\/domain","online_store\\/google_shopping","report\\/online","report\\/overview","report\\/report","report\\/statistics","report\\/product","report\\/order","report\\/financial","report\\/staff","report\\/store","sale\\/order","sale\\/return_receipt","sale\\/recurring","sale\\/return","sale\\/voucher","sale\\/voucher_theme","sale_channel\\/pos_novaon","extension\\/appstore\\/mar_ons_chatbot","extension\\/appstore\\/mar_ons_onfluencer","sale_channel\\/novaonx_social","section\\/rate","rate\\/rate","section\\/blog","section\\/customize_layout","section\\/banner","section\\/best_sales_product","section\\/best_views_product","section\\/blog","section\\/content_customize","section\\/detail_product","section\\/footer","section\\/header","section\\/hot_product","section\\/list_product","section\\/new_product","section\\/partner","section\\/preview","section\\/sections","section\\/slideshow","section_blog\\/blog_category","section_blog\\/blog_list","section_blog\\/latest_blog","section_blog\\/sections","section_category\\/banner","section_category\\/filter","section_category\\/product_category","section_category\\/product_list","section_category\\/sections","section_contact\\/contact","section_contact\\/form","section_contact\\/map","section_contact\\/sections","section_product_detail\\/related_product","section_product_detail\\/sections","setting\\/setting","setting\\/store","settings\\/account","settings\\/classify","settings\\/delivery","settings\\/general","settings\\/payment","settings\\/settings","settings\\/notify","startup\\/error","startup\\/event","startup\\/login","startup\\/permission","startup\\/router","startup\\/sass","startup\\/startup","super\\/dashboard","theme\\/color","theme\\/favicon","theme\\/section_theme","theme\\/social","theme\\/text","tool\\/backup","tool\\/log","tool\\/upload","user\\/api","user\\/user","user\\/user_permission"]}'),
(10, 'Demonstration', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher`
--

CREATE TABLE `oc_voucher` (
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_history`
--

CREATE TABLE `oc_voucher_history` (
  `voucher_history_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_theme`
--

CREATE TABLE `oc_voucher_theme` (
  `voucher_theme_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_voucher_theme`
--

INSERT INTO `oc_voucher_theme` (`voucher_theme_id`, `image`) VALUES
(8, 'catalog/demo/canon_eos_5d_2.jpg'),
(7, 'catalog/demo/gift-voucher-birthday.jpg'),
(6, 'catalog/demo/apple_logo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_theme_description`
--

CREATE TABLE `oc_voucher_theme_description` (
  `voucher_theme_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_voucher_theme_description`
--

INSERT INTO `oc_voucher_theme_description` (`voucher_theme_id`, `language_id`, `name`) VALUES
(6, 1, 'Christmas'),
(7, 1, 'Birthday'),
(8, 1, 'General');

-- --------------------------------------------------------

--
-- Table structure for table `oc_warehouse`
--

CREATE TABLE `oc_warehouse` (
  `warehouse_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_version_id` varchar(256) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_weight_class`
--

CREATE TABLE `oc_weight_class` (
  `weight_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL DEFAULT '0.00000000'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_weight_class`
--

INSERT INTO `oc_weight_class` (`weight_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '1000.00000000'),
(5, '2.20460000'),
(6, '35.27400000');

-- --------------------------------------------------------

--
-- Table structure for table `oc_weight_class_description`
--

CREATE TABLE `oc_weight_class_description` (
  `weight_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_weight_class_description`
--

INSERT INTO `oc_weight_class_description` (`weight_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Kilogram', 'kg'),
(2, 1, 'Gram', 'g'),
(5, 1, 'Pound ', 'lb'),
(6, 1, 'Ounce', 'oz'),
(1, 2, 'Kilogram', 'kg'),
(2, 2, 'Gram', 'g'),
(5, 2, 'Pound ', 'lb'),
(6, 2, 'Ounce', 'oz');

-- --------------------------------------------------------

--
-- Table structure for table `oc_zone`
--

CREATE TABLE `oc_zone` (
  `zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_zone`
--

INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1, 230, 'Hà Nội', 'HN', 1),
(2, 230, 'Hồ Chí Minh', 'HCM', 1),
(3, 230, 'An Giang', 'AG', 1),
(4, 230, 'Bà Rịa-Vũng Tàu', 'BR', 1),
(5, 230, 'Bạc Liêu', 'BL', 1),
(6, 230, 'Bắc Kạn', 'BK', 1),
(7, 230, 'Bắc Giang', 'BG', 1),
(8, 230, 'Bắc Ninh', 'BN', 1),
(9, 230, 'Bến Tre', 'BT', 1),
(10, 230, 'Bình Dương', 'BD', 1),
(11, 230, 'Bình Định', 'BĐ', 1),
(12, 230, 'Bình Phước', 'BP', 1),
(13, 230, 'Bình Thuận', 'BT', 1),
(14, 230, 'Cà Mau', 'CM', 1),
(15, 230, 'Cao Bằng', 'CB', 1),
(16, 230, 'Cần Thơ', 'CT', 1),
(17, 230, 'Đà Nẵng', 'DN', 1),
(18, 230, 'Đắk Lắk', 'DL', 1),
(19, 230, 'Đắk Nông', 'DKN', 1),
(20, 230, 'Điện Biên', 'DB', 1),
(21, 230, 'Đồng Nai', 'DNN', 1),
(22, 230, 'Đồng Tháp', 'DT', 1),
(23, 230, 'Gia Lai', 'GL', 1),
(24, 230, 'Hà Giang', 'HG', 1),
(25, 230, 'Hà Nam', 'HNN', 1),
(26, 230, 'Hà Tĩnh', 'HT', 1),
(27, 230, 'Hải Dương', 'HD', 1),
(28, 230, 'Hải Phòng', 'HP', 1),
(29, 230, 'Hòa Bình', 'HB', 1),
(30, 230, 'Hậu Giang', 'HG', 1),
(31, 230, 'Hưng Yên', 'HY', 1),
(32, 230, 'Khánh Hòa', 'KH', 1),
(33, 230, 'Kiên Giang', 'KG', 1),
(34, 230, 'Kon Tum', 'KT', 1),
(35, 230, 'Lai Châu', 'LCU', 1),
(36, 230, 'Lào Cai', 'LC', 1),
(37, 230, 'Lạng Sơn', 'LS', 1),
(38, 230, 'Lâm Đồng', 'LD', 1),
(39, 230, 'Long An', 'LA', 1),
(40, 230, 'Nam Định', 'ND', 1),
(41, 230, 'Nghệ An', 'NA', 1),
(42, 230, 'Ninh Bình', 'NB', 1),
(43, 230, 'Ninh Thuận', 'NT', 1),
(44, 230, 'Phú Thọ', 'PT', 1),
(45, 230, 'Phú Yên', 'PY', 1),
(46, 230, 'Quảng Bình', 'QB', 1),
(47, 230, 'Quảng Nam', 'QNN', 1),
(48, 230, 'Quảng Ngãi', 'QNG', 1),
(49, 230, 'Quảng Ninh', 'QN', 1),
(50, 230, 'Quảng Trị', 'QT', 1),
(51, 230, 'Sóc Trăng', 'ST', 1),
(52, 230, 'Sơn La', 'SL', 1),
(53, 230, 'Tây Ninh', 'TN', 1),
(54, 230, 'Thái Bình', 'TB', 1),
(55, 230, 'Thái Nguyên', 'TN', 1),
(56, 230, 'Thanh Hóa', 'TH', 1),
(57, 230, 'Thừa Thiên - Huế', 'TTH', 1),
(58, 230, 'Tiền Giang', 'TG', 1),
(59, 230, 'Trà Vinh', 'TV', 1),
(60, 230, 'Tuyên Quang', 'TQ', 1),
(61, 230, 'Vĩnh Long', 'VL', 1),
(62, 230, 'Vĩnh Phúc', 'VP', 1),
(63, 230, 'Yên Bái', 'YB', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_zone_to_geo_zone`
--

CREATE TABLE `oc_zone_to_geo_zone` (
  `zone_to_geo_zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `geo_zone_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_zone_to_geo_zone`
--

INSERT INTO `oc_zone_to_geo_zone` (`zone_to_geo_zone_id`, `country_id`, `zone_id`, `geo_zone_id`, `date_added`, `date_modified`) VALUES
(1, 222, 0, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 222, 3513, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 222, 3514, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 222, 3515, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 222, 3516, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 222, 3517, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 222, 3518, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 222, 3519, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 222, 3520, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 222, 3521, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 222, 3522, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 222, 3523, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 222, 3524, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 222, 3525, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 222, 3526, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 222, 3527, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 222, 3528, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 222, 3529, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 222, 3530, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 222, 3531, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 222, 3532, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 222, 3533, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 222, 3534, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 222, 3535, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 222, 3536, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 222, 3537, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 222, 3538, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 222, 3539, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 222, 3540, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 222, 3541, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 222, 3542, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 222, 3543, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 222, 3544, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 222, 3545, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 222, 3546, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 222, 3547, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 222, 3548, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 222, 3549, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 222, 3550, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 222, 3551, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 222, 3552, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 222, 3553, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 222, 3554, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 222, 3555, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 222, 3556, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 222, 3557, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 222, 3558, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 222, 3559, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 222, 3560, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 222, 3561, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 222, 3562, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 222, 3563, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 222, 3564, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 222, 3565, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 222, 3566, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 222, 3567, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 222, 3568, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 222, 3569, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 222, 3570, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 222, 3571, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 222, 3572, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 222, 3573, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 222, 3574, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 222, 3575, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 222, 3576, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 222, 3577, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 222, 3578, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 222, 3579, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 222, 3580, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 222, 3581, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 222, 3582, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 222, 3583, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 222, 3584, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 222, 3585, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 222, 3586, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 222, 3587, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 222, 3588, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 222, 3589, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 222, 3590, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 222, 3591, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 222, 3592, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 222, 3593, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 222, 3594, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 222, 3595, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 222, 3596, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 222, 3597, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 222, 3598, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 222, 3599, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 222, 3600, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 222, 3601, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 222, 3602, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 222, 3603, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 222, 3604, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 222, 3605, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 222, 3606, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 222, 3607, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 222, 3608, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 222, 3609, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 222, 3610, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 222, 3611, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 222, 3612, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 222, 3949, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 222, 3950, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 222, 3951, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 222, 3952, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 222, 3953, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 222, 3954, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 222, 3955, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 222, 3972, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `oc_timezones`
--

CREATE TABLE `oc_timezones` (
  `id` int(11) NOT NULL,
  `value` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_timezones`
--

INSERT INTO `oc_timezones` (`id`, `value`, `label`) VALUES
(1, '-12:00', '(GMT -12:00) Eniwetok, Kwajalein'),
(2, '-11:00', '(GMT -11:00) Midway Island, Samoa'),
(3, '-10:00', '(GMT -10:00) Hawaii'),
(4, '-09:50', '(GMT -9:30) Taiohae'),
(5, '-09:00', '(GMT -9:00) Alaska'),
(6, '-08:00', '(GMT -8:00) Pacific Time (US &amp; Canada)'),
(7, '-07:00', '(GMT -7:00) Mountain Time (US &amp; Canada)'),
(8, '-06:00', '(GMT -6:00) Central Time (US &amp; Canada), Mexico City'),
(9, '-05:00', '(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima'),
(10, '-04:50', '(GMT -4:30) Caracas'),
(11, '-04:00', '(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz'),
(12, '-03:50', '(GMT -3:30) Newfoundland'),
(13, '-03:00', '(GMT -3:00) Brazil, Buenos Aires, Georgetown'),
(14, '-02:00', '(GMT -2:00) Mid-Atlantic'),
(15, '-01:00', '(GMT -1:00) Azores, Cape Verde Islands'),
(16, '+00:00', '(GMT) Western Europe Time, London, Lisbon, Casablanca'),
(17, '+01:00', '(GMT +1:00) Brussels, Copenhagen, Madrid, Paris'),
(18, '+02:00', '(GMT +2:00) Kaliningrad, South Africa'),
(19, '+03:00', '(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg'),
(20, '+03:50', '(GMT +3:30) Tehran'),
(21, '+04:00', '(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi'),
(22, '+04:50', '(GMT +4:30) Kabul'),
(23, '+05:00', '(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent'),
(24, '+05:50', '(GMT +5:30) Bombay, Calcutta, Madras, New Delhi'),
(25, '+05:75', '(GMT +5:45) Kathmandu, Pokhar'),
(26, '+06:00', '(GMT +6:00) Almaty, Dhaka, Colombo'),
(27, '+06:50', '(GMT +6:30) Yangon, Mandalay'),
(28, '+07:00', '(GMT +7:00) Bangkok, Hanoi, Jakarta'),
(29, '+08:00', '(GMT +8:00) Beijing, Perth, Singapore, Hong Kong'),
(30, '+08:75', '(GMT +8:45) Eucla'),
(31, '+09:00', '(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk'),
(32, '+09:50', '(GMT +9:30) Adelaide, Darwin'),
(33, '+10:00', '(GMT +10:00) Eastern Australia, Guam, Vladivostok'),
(34, '+10:50', '(GMT +10:30) Lord Howe Island'),
(35, '+11:00', '(GMT +11:00) Magadan, Solomon Islands, New Caledonia'),
(36, '+11:50', '(GMT +11:30) Norfolk Island'),
(37, '+12:00', '(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka'),
(38, '+12:75', '(GMT +12:45) Chatham Islands'),
(39, '+13:00', '(GMT +13:00) Apia, Nukualofa'),
(40, '+14:00', '(GMT +14:00) Line Islands, Tokelau');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `oc_address`
--
ALTER TABLE `oc_address`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `oc_api`
--
ALTER TABLE `oc_api`
  ADD PRIMARY KEY (`api_id`);

--
-- Indexes for table `oc_api_ip`
--
ALTER TABLE `oc_api_ip`
  ADD PRIMARY KEY (`api_ip_id`);

--
-- Indexes for table `oc_api_session`
--
ALTER TABLE `oc_api_session`
  ADD PRIMARY KEY (`api_session_id`);

--
-- Indexes for table `oc_attribute`
--
ALTER TABLE `oc_attribute`
  ADD PRIMARY KEY (`attribute_id`);

--
-- Indexes for table `oc_attribute_description`
--
ALTER TABLE `oc_attribute_description`
  ADD PRIMARY KEY (`attribute_id`,`language_id`);

--
-- Indexes for table `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  ADD PRIMARY KEY (`attribute_group_id`);

--
-- Indexes for table `oc_attribute_group_description`
--
ALTER TABLE `oc_attribute_group_description`
  ADD PRIMARY KEY (`attribute_group_id`,`language_id`);

--
-- Indexes for table `oc_banner`
--
ALTER TABLE `oc_banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  ADD PRIMARY KEY (`banner_image_id`);

--
-- Indexes for table `oc_cart`
--
ALTER TABLE `oc_cart`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `cart_id` (`api_id`,`customer_id`,`session_id`,`product_id`,`recurring_id`);

--
-- Indexes for table `oc_category`
--
ALTER TABLE `oc_category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `oc_category_description`
--
ALTER TABLE `oc_category_description`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_category_filter`
--
ALTER TABLE `oc_category_filter`
  ADD PRIMARY KEY (`category_id`,`filter_id`);

--
-- Indexes for table `oc_category_path`
--
ALTER TABLE `oc_category_path`
  ADD PRIMARY KEY (`category_id`,`path_id`);

--
-- Indexes for table `oc_category_to_layout`
--
ALTER TABLE `oc_category_to_layout`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Indexes for table `oc_category_to_store`
--
ALTER TABLE `oc_category_to_store`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Indexes for table `oc_collection`
--
ALTER TABLE `oc_collection`
  ADD PRIMARY KEY (`collection_id`);

--
-- Indexes for table `oc_collection_description`
--
ALTER TABLE `oc_collection_description`
  ADD PRIMARY KEY (`collection_id`);

--
-- Indexes for table `oc_country`
--
ALTER TABLE `oc_country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `oc_coupon`
--
ALTER TABLE `oc_coupon`
  ADD PRIMARY KEY (`coupon_id`);

--
-- Indexes for table `oc_coupon_category`
--
ALTER TABLE `oc_coupon_category`
  ADD PRIMARY KEY (`coupon_id`,`category_id`);

--
-- Indexes for table `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  ADD PRIMARY KEY (`coupon_history_id`);

--
-- Indexes for table `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  ADD PRIMARY KEY (`coupon_product_id`);

--
-- Indexes for table `oc_currency`
--
ALTER TABLE `oc_currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Indexes for table `oc_customer`
--
ALTER TABLE `oc_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  ADD PRIMARY KEY (`customer_activity_id`);

--
-- Indexes for table `oc_customer_affiliate`
--
ALTER TABLE `oc_customer_affiliate`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `oc_customer_approval`
--
ALTER TABLE `oc_customer_approval`
  ADD PRIMARY KEY (`customer_approval_id`);

--
-- Indexes for table `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  ADD PRIMARY KEY (`customer_group_id`);

--
-- Indexes for table `oc_customer_group_description`
--
ALTER TABLE `oc_customer_group_description`
  ADD PRIMARY KEY (`customer_group_id`,`language_id`);

--
-- Indexes for table `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  ADD PRIMARY KEY (`customer_history_id`);

--
-- Indexes for table `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  ADD PRIMARY KEY (`customer_ip_id`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  ADD PRIMARY KEY (`customer_login_id`),
  ADD KEY `email` (`email`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `oc_customer_online`
--
ALTER TABLE `oc_customer_online`
  ADD PRIMARY KEY (`ip`);

--
-- Indexes for table `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  ADD PRIMARY KEY (`customer_reward_id`);

--
-- Indexes for table `oc_customer_search`
--
ALTER TABLE `oc_customer_search`
  ADD PRIMARY KEY (`customer_search_id`);

--
-- Indexes for table `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  ADD PRIMARY KEY (`customer_transaction_id`);

--
-- Indexes for table `oc_customer_wishlist`
--
ALTER TABLE `oc_customer_wishlist`
  ADD PRIMARY KEY (`customer_id`,`product_id`);

--
-- Indexes for table `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  ADD PRIMARY KEY (`custom_field_id`);

--
-- Indexes for table `oc_custom_field_customer_group`
--
ALTER TABLE `oc_custom_field_customer_group`
  ADD PRIMARY KEY (`custom_field_id`,`customer_group_id`);

--
-- Indexes for table `oc_custom_field_description`
--
ALTER TABLE `oc_custom_field_description`
  ADD PRIMARY KEY (`custom_field_id`,`language_id`);

--
-- Indexes for table `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  ADD PRIMARY KEY (`custom_field_value_id`);

--
-- Indexes for table `oc_custom_field_value_description`
--
ALTER TABLE `oc_custom_field_value_description`
  ADD PRIMARY KEY (`custom_field_value_id`,`language_id`);

--
-- Indexes for table `oc_download`
--
ALTER TABLE `oc_download`
  ADD PRIMARY KEY (`download_id`);

--
-- Indexes for table `oc_download_description`
--
ALTER TABLE `oc_download_description`
  ADD PRIMARY KEY (`download_id`,`language_id`);

--
-- Indexes for table `oc_event`
--
ALTER TABLE `oc_event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `oc_extension`
--
ALTER TABLE `oc_extension`
  ADD PRIMARY KEY (`extension_id`);

--
-- Indexes for table `oc_extension_install`
--
ALTER TABLE `oc_extension_install`
  ADD PRIMARY KEY (`extension_install_id`);

--
-- Indexes for table `oc_extension_path`
--
ALTER TABLE `oc_extension_path`
  ADD PRIMARY KEY (`extension_path_id`);

--
-- Indexes for table `oc_filter`
--
ALTER TABLE `oc_filter`
  ADD PRIMARY KEY (`filter_id`);

--
-- Indexes for table `oc_filter_description`
--
ALTER TABLE `oc_filter_description`
  ADD PRIMARY KEY (`filter_id`,`language_id`);

--
-- Indexes for table `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  ADD PRIMARY KEY (`filter_group_id`);

--
-- Indexes for table `oc_filter_group_description`
--
ALTER TABLE `oc_filter_group_description`
  ADD PRIMARY KEY (`filter_group_id`,`language_id`);

--
-- Indexes for table `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  ADD PRIMARY KEY (`geo_zone_id`);

--
-- Indexes for table `oc_information`
--
ALTER TABLE `oc_information`
  ADD PRIMARY KEY (`information_id`);

--
-- Indexes for table `oc_information_description`
--
ALTER TABLE `oc_information_description`
  ADD PRIMARY KEY (`information_id`,`language_id`);

--
-- Indexes for table `oc_information_to_layout`
--
ALTER TABLE `oc_information_to_layout`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Indexes for table `oc_information_to_store`
--
ALTER TABLE `oc_information_to_store`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Indexes for table `oc_language`
--
ALTER TABLE `oc_language`
  ADD PRIMARY KEY (`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_layout`
--
ALTER TABLE `oc_layout`
  ADD PRIMARY KEY (`layout_id`);

--
-- Indexes for table `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  ADD PRIMARY KEY (`layout_module_id`);

--
-- Indexes for table `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  ADD PRIMARY KEY (`layout_route_id`);

--
-- Indexes for table `oc_length_class`
--
ALTER TABLE `oc_length_class`
  ADD PRIMARY KEY (`length_class_id`);

--
-- Indexes for table `oc_length_class_description`
--
ALTER TABLE `oc_length_class_description`
  ADD PRIMARY KEY (`length_class_id`,`language_id`);

--
-- Indexes for table `oc_location`
--
ALTER TABLE `oc_location`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  ADD PRIMARY KEY (`manufacturer_id`);

--
-- Indexes for table `oc_manufacturer_to_store`
--
ALTER TABLE `oc_manufacturer_to_store`
  ADD PRIMARY KEY (`manufacturer_id`,`store_id`);

--
-- Indexes for table `oc_marketing`
--
ALTER TABLE `oc_marketing`
  ADD PRIMARY KEY (`marketing_id`);

--
-- Indexes for table `oc_modification`
--
ALTER TABLE `oc_modification`
  ADD PRIMARY KEY (`modification_id`);

--
-- Indexes for table `oc_module`
--
ALTER TABLE `oc_module`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `oc_novaon_group_menu`
--
ALTER TABLE `oc_novaon_group_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_novaon_group_menu_description`
--
ALTER TABLE `oc_novaon_group_menu_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_novaon_menu_item`
--
ALTER TABLE `oc_novaon_menu_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_novaon_menu_item_description`
--
ALTER TABLE `oc_novaon_menu_item_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_novaon_relation_table`
--
ALTER TABLE `oc_novaon_relation_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_novaon_type`
--
ALTER TABLE `oc_novaon_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_novaon_type_description`
--
ALTER TABLE `oc_novaon_type_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_option`
--
ALTER TABLE `oc_option`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `oc_option_description`
--
ALTER TABLE `oc_option_description`
  ADD PRIMARY KEY (`option_id`,`language_id`);

--
-- Indexes for table `oc_option_value`
--
ALTER TABLE `oc_option_value`
  ADD PRIMARY KEY (`option_value_id`);

--
-- Indexes for table `oc_option_value_description`
--
ALTER TABLE `oc_option_value_description`
  ADD PRIMARY KEY (`option_value_id`,`language_id`);

--
-- Indexes for table `oc_order`
--
ALTER TABLE `oc_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `oc_order_history`
--
ALTER TABLE `oc_order_history`
  ADD PRIMARY KEY (`order_history_id`);

--
-- Indexes for table `oc_order_option`
--
ALTER TABLE `oc_order_option`
  ADD PRIMARY KEY (`order_option_id`);

--
-- Indexes for table `oc_order_product`
--
ALTER TABLE `oc_order_product`
  ADD PRIMARY KEY (`order_product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  ADD PRIMARY KEY (`order_recurring_id`);

--
-- Indexes for table `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  ADD PRIMARY KEY (`order_recurring_transaction_id`);

--
-- Indexes for table `oc_order_shipment`
--
ALTER TABLE `oc_order_shipment`
  ADD PRIMARY KEY (`order_shipment_id`);

--
-- Indexes for table `oc_order_status`
--
ALTER TABLE `oc_order_status`
  ADD PRIMARY KEY (`order_status_id`,`language_id`);

--
-- Indexes for table `oc_order_tag`
--
ALTER TABLE `oc_order_tag`
  ADD PRIMARY KEY (`order_tag_id`);

--
-- Indexes for table `oc_order_total`
--
ALTER TABLE `oc_order_total`
  ADD PRIMARY KEY (`order_total_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  ADD PRIMARY KEY (`order_voucher_id`);

--
-- Indexes for table `oc_page_contents`
--
ALTER TABLE `oc_page_contents`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `oc_product`
--
ALTER TABLE `oc_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `oc_product_attribute`
--
ALTER TABLE `oc_product_attribute`
  ADD PRIMARY KEY (`product_id`,`attribute_id`,`language_id`);

--
-- Indexes for table `oc_product_collection`
--
ALTER TABLE `oc_product_collection`
  ADD PRIMARY KEY (`product_collection_id`);

--
-- Indexes for table `oc_product_description`
--
ALTER TABLE `oc_product_description`
  ADD PRIMARY KEY (`product_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  ADD PRIMARY KEY (`product_discount_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_filter`
--
ALTER TABLE `oc_product_filter`
  ADD PRIMARY KEY (`product_id`,`filter_id`);

--
-- Indexes for table `oc_product_image`
--
ALTER TABLE `oc_product_image`
  ADD PRIMARY KEY (`product_image_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_option`
--
ALTER TABLE `oc_product_option`
  ADD PRIMARY KEY (`product_option_id`);

--
-- Indexes for table `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  ADD PRIMARY KEY (`product_option_value_id`);

--
-- Indexes for table `oc_product_recurring`
--
ALTER TABLE `oc_product_recurring`
  ADD PRIMARY KEY (`product_id`,`recurring_id`,`customer_group_id`);

--
-- Indexes for table `oc_product_related`
--
ALTER TABLE `oc_product_related`
  ADD PRIMARY KEY (`product_id`,`related_id`);

--
-- Indexes for table `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  ADD PRIMARY KEY (`product_reward_id`);

--
-- Indexes for table `oc_product_special`
--
ALTER TABLE `oc_product_special`
  ADD PRIMARY KEY (`product_special_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_tag`
--
ALTER TABLE `oc_product_tag`
  ADD PRIMARY KEY (`product_tag_id`);

--
-- Indexes for table `oc_product_to_category`
--
ALTER TABLE `oc_product_to_category`
  ADD PRIMARY KEY (`product_id`,`category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `oc_product_to_download`
--
ALTER TABLE `oc_product_to_download`
  ADD PRIMARY KEY (`product_id`,`download_id`);

--
-- Indexes for table `oc_product_to_layout`
--
ALTER TABLE `oc_product_to_layout`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Indexes for table `oc_product_to_store`
--
ALTER TABLE `oc_product_to_store`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Indexes for table `oc_product_version`
--
ALTER TABLE `oc_product_version`
  ADD PRIMARY KEY (`product_version_id`);

--
-- Indexes for table `oc_recurring`
--
ALTER TABLE `oc_recurring`
  ADD PRIMARY KEY (`recurring_id`);

--
-- Indexes for table `oc_recurring_description`
--
ALTER TABLE `oc_recurring_description`
  ADD PRIMARY KEY (`recurring_id`,`language_id`);

--
-- Indexes for table `oc_report`
--
ALTER TABLE `oc_report`
  ADD PRIMARY KEY (`report_time`,`type`);

--
-- Indexes for table `oc_return`
--
ALTER TABLE `oc_return`
  ADD PRIMARY KEY (`return_id`);

--
-- Indexes for table `oc_return_history`
--
ALTER TABLE `oc_return_history`
  ADD PRIMARY KEY (`return_history_id`);

--
-- Indexes for table `oc_review`
--
ALTER TABLE `oc_review`
  ADD PRIMARY KEY (`review_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_seo_url`
--
ALTER TABLE `oc_seo_url`
  ADD PRIMARY KEY (`seo_url_id`),
  ADD KEY `query` (`query`),
  ADD KEY `keyword` (`keyword`);

--
-- Indexes for table `oc_session`
--
ALTER TABLE `oc_session`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `oc_setting`
--
ALTER TABLE `oc_setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `oc_shipping_courier`
--
ALTER TABLE `oc_shipping_courier`
  ADD PRIMARY KEY (`shipping_courier_id`);

--
-- Indexes for table `oc_statistics`
--
ALTER TABLE `oc_statistics`
  ADD PRIMARY KEY (`statistics_id`);

--
-- Indexes for table `oc_store`
--
ALTER TABLE `oc_store`
  ADD PRIMARY KEY (`store_id`);

--
-- Indexes for table `oc_tag`
--
ALTER TABLE `oc_tag`
  ADD PRIMARY KEY (`tag_id`);

--
-- Indexes for table `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  ADD PRIMARY KEY (`tax_class_id`);

--
-- Indexes for table `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  ADD PRIMARY KEY (`tax_rate_id`);

--
-- Indexes for table `oc_tax_rate_to_customer_group`
--
ALTER TABLE `oc_tax_rate_to_customer_group`
  ADD PRIMARY KEY (`tax_rate_id`,`customer_group_id`);

--
-- Indexes for table `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  ADD PRIMARY KEY (`tax_rule_id`);

--
-- Indexes for table `oc_theme`
--
ALTER TABLE `oc_theme`
  ADD PRIMARY KEY (`theme_id`);

--
-- Indexes for table `oc_theme_builder_config`
--
ALTER TABLE `oc_theme_builder_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_translation`
--
ALTER TABLE `oc_translation`
  ADD PRIMARY KEY (`translation_id`);

--
-- Indexes for table `oc_upload`
--
ALTER TABLE `oc_upload`
  ADD PRIMARY KEY (`upload_id`);

--
-- Indexes for table `oc_user`
--
ALTER TABLE `oc_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `oc_user_group`
--
ALTER TABLE `oc_user_group`
  ADD PRIMARY KEY (`user_group_id`);

--
-- Indexes for table `oc_voucher`
--
ALTER TABLE `oc_voucher`
  ADD PRIMARY KEY (`voucher_id`);

--
-- Indexes for table `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  ADD PRIMARY KEY (`voucher_history_id`);

--
-- Indexes for table `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  ADD PRIMARY KEY (`voucher_theme_id`);

--
-- Indexes for table `oc_voucher_theme_description`
--
ALTER TABLE `oc_voucher_theme_description`
  ADD PRIMARY KEY (`voucher_theme_id`,`language_id`);

--
-- Indexes for table `oc_warehouse`
--
ALTER TABLE `oc_warehouse`
  ADD PRIMARY KEY (`warehouse_id`);

--
-- Indexes for table `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  ADD PRIMARY KEY (`weight_class_id`);

--
-- Indexes for table `oc_weight_class_description`
--
ALTER TABLE `oc_weight_class_description`
  ADD PRIMARY KEY (`weight_class_id`,`language_id`);

--
-- Indexes for table `oc_zone`
--
ALTER TABLE `oc_zone`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  ADD PRIMARY KEY (`zone_to_geo_zone_id`);

--
-- Indexes for table `oc_timezones`
--
ALTER TABLE `oc_timezones`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `oc_address`
--
ALTER TABLE `oc_address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_api`
--
ALTER TABLE `oc_api`
  MODIFY `api_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_api_ip`
--
ALTER TABLE `oc_api_ip`
  MODIFY `api_ip_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_api_session`
--
ALTER TABLE `oc_api_session`
  MODIFY `api_session_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_attribute`
--
ALTER TABLE `oc_attribute`
  MODIFY `attribute_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  MODIFY `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_banner`
--
ALTER TABLE `oc_banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  MODIFY `banner_image_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_cart`
--
ALTER TABLE `oc_cart`
  MODIFY `cart_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_category`
--
ALTER TABLE `oc_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `oc_collection`
--
ALTER TABLE `oc_collection`
  MODIFY `collection_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `oc_country`
--
ALTER TABLE `oc_country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;
--
-- AUTO_INCREMENT for table `oc_coupon`
--
ALTER TABLE `oc_coupon`
  MODIFY `coupon_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  MODIFY `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  MODIFY `coupon_product_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_currency`
--
ALTER TABLE `oc_currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `oc_customer`
--
ALTER TABLE `oc_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  MODIFY `customer_activity_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_approval`
--
ALTER TABLE `oc_customer_approval`
  MODIFY `customer_approval_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  MODIFY `customer_group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  MODIFY `customer_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  MODIFY `customer_ip_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  MODIFY `customer_login_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  MODIFY `customer_reward_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_search`
--
ALTER TABLE `oc_customer_search`
  MODIFY `customer_search_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  MODIFY `customer_transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  MODIFY `custom_field_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  MODIFY `custom_field_value_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_download`
--
ALTER TABLE `oc_download`
  MODIFY `download_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_event`
--
ALTER TABLE `oc_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `oc_extension`
--
ALTER TABLE `oc_extension`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=183;
--
-- AUTO_INCREMENT for table `oc_extension_install`
--
ALTER TABLE `oc_extension_install`
  MODIFY `extension_install_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_extension_path`
--
ALTER TABLE `oc_extension_path`
  MODIFY `extension_path_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_filter`
--
ALTER TABLE `oc_filter`
  MODIFY `filter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  MODIFY `filter_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  MODIFY `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `oc_information`
--
ALTER TABLE `oc_information`
  MODIFY `information_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_language`
--
ALTER TABLE `oc_language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oc_layout`
--
ALTER TABLE `oc_layout`
  MODIFY `layout_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  MODIFY `layout_module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  MODIFY `layout_route_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `oc_length_class`
--
ALTER TABLE `oc_length_class`
  MODIFY `length_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `oc_location`
--
ALTER TABLE `oc_location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  MODIFY `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `oc_marketing`
--
ALTER TABLE `oc_marketing`
  MODIFY `marketing_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_modification`
--
ALTER TABLE `oc_modification`
  MODIFY `modification_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_module`
--
ALTER TABLE `oc_module`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `oc_novaon_group_menu`
--
ALTER TABLE `oc_novaon_group_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `oc_novaon_group_menu_description`
--
ALTER TABLE `oc_novaon_group_menu_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `oc_novaon_menu_item`
--
ALTER TABLE `oc_novaon_menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `oc_novaon_menu_item_description`
--
ALTER TABLE `oc_novaon_menu_item_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `oc_novaon_relation_table`
--
ALTER TABLE `oc_novaon_relation_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `oc_novaon_type`
--
ALTER TABLE `oc_novaon_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `oc_novaon_type_description`
--
ALTER TABLE `oc_novaon_type_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `oc_option`
--
ALTER TABLE `oc_option`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `oc_option_value`
--
ALTER TABLE `oc_option_value`
  MODIFY `option_value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `oc_order`
--
ALTER TABLE `oc_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_history`
--
ALTER TABLE `oc_order_history`
  MODIFY `order_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_option`
--
ALTER TABLE `oc_order_option`
  MODIFY `order_option_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_product`
--
ALTER TABLE `oc_order_product`
  MODIFY `order_product_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  MODIFY `order_recurring_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  MODIFY `order_recurring_transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_shipment`
--
ALTER TABLE `oc_order_shipment`
  MODIFY `order_shipment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_status`
--
ALTER TABLE `oc_order_status`
  MODIFY `order_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `oc_order_tag`
--
ALTER TABLE `oc_order_tag`
  MODIFY `order_tag_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_total`
--
ALTER TABLE `oc_order_total`
  MODIFY `order_total_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  MODIFY `order_voucher_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_page_contents`
--
ALTER TABLE `oc_page_contents`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_product`
--
ALTER TABLE `oc_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;
--
-- AUTO_INCREMENT for table `oc_product_collection`
--
ALTER TABLE `oc_product_collection`
  MODIFY `product_collection_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;
--
-- AUTO_INCREMENT for table `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  MODIFY `product_discount_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_product_image`
--
ALTER TABLE `oc_product_image`
  MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;
--
-- AUTO_INCREMENT for table `oc_product_option`
--
ALTER TABLE `oc_product_option`
  MODIFY `product_option_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  MODIFY `product_option_value_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  MODIFY `product_reward_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_product_special`
--
ALTER TABLE `oc_product_special`
  MODIFY `product_special_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_product_tag`
--
ALTER TABLE `oc_product_tag`
  MODIFY `product_tag_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_product_version`
--
ALTER TABLE `oc_product_version`
  MODIFY `product_version_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_recurring`
--
ALTER TABLE `oc_recurring`
  MODIFY `recurring_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_return`
--
ALTER TABLE `oc_return`
  MODIFY `return_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_return_history`
--
ALTER TABLE `oc_return_history`
  MODIFY `return_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_review`
--
ALTER TABLE `oc_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_seo_url`
--
ALTER TABLE `oc_seo_url`
  MODIFY `seo_url_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2204;
--
-- AUTO_INCREMENT for table `oc_setting`
--
ALTER TABLE `oc_setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5516;
--
-- AUTO_INCREMENT for table `oc_statistics`
--
ALTER TABLE `oc_statistics`
  MODIFY `statistics_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `oc_store`
--
ALTER TABLE `oc_store`
  MODIFY `store_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_tag`
--
ALTER TABLE `oc_tag`
  MODIFY `tag_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  MODIFY `tax_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  MODIFY `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  MODIFY `tax_rule_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;
--
-- AUTO_INCREMENT for table `oc_theme`
--
ALTER TABLE `oc_theme`
  MODIFY `theme_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_theme_builder_config`
--
ALTER TABLE `oc_theme_builder_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=376;
--
-- AUTO_INCREMENT for table `oc_translation`
--
ALTER TABLE `oc_translation`
  MODIFY `translation_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_upload`
--
ALTER TABLE `oc_upload`
  MODIFY `upload_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_user`
--
ALTER TABLE `oc_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_user_group`
--
ALTER TABLE `oc_user_group`
  MODIFY `user_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `oc_voucher`
--
ALTER TABLE `oc_voucher`
  MODIFY `voucher_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  MODIFY `voucher_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  MODIFY `voucher_theme_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `oc_warehouse`
--
ALTER TABLE `oc_warehouse`
  MODIFY `warehouse_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1138;
--
-- AUTO_INCREMENT for table `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  MODIFY `weight_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `oc_zone`
--
ALTER TABLE `oc_zone`
  MODIFY `zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  MODIFY `zone_to_geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT for table `oc_timezones`
--
ALTER TABLE `oc_timezones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

-- -----------------------------------------------------------
--
-- v1.4
--
-- -----------------------------------------------------------

--
-- more event for order
--
INSERT INTO `oc_event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES
('edit order 2', 'admin/model/sale/order/editOrder/after', 'event/statistics/editOrderDetail', 1, 0),
('change status order 2', 'admin/model/sale/order/editOrder/before', 'event/statistics/updateStatusOrderDetail', 1, 0);


-- -----------------------------------------------------------
--
-- v1.5
--
-- -----------------------------------------------------------

--
-- Create table `oc_novaon_api_credentials`
--
CREATE TABLE `oc_novaon_api_credentials` (
  `id` int(11) NOT NULL,
  `consumer_key` varchar(255) NOT NULL,
  `consumer_secret` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `oc_novaon_api_credentials` (`id`, `consumer_key`, `consumer_secret`, `create_date`, `status`) VALUES
(1, 'GHNHTYDSTw567', 'GHNDRTUYUKMVFYJKNBGFFGFHJJ7886GH', '2019-06-03 01:15:59', 1);

ALTER TABLE `oc_novaon_api_credentials`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `oc_novaon_api_credentials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- enable extension module NovaonApi
--
INSERT INTO `oc_extension` (`type`, `code`) VALUES
('module', 'NovaonApi');

--
-- google shopping meta config
--
INSERT INTO `oc_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'config', 'config_gg_shopping', '', 0);

--
-- add column previous_order_status_id to oc_order
--
ALTER TABLE `oc_order` ADD `previous_order_status_id` VARCHAR(50) NULL AFTER `order_status_id`;


--
-- v1.5.2 Smart loading demo data base on theme group (theme type)
-- Note: following content is copied from file "library/theme_config/demo_data/tech.sql" for default
--
-- 2020/04: More support loading demo data from special theme. If no special theme defined, use theme group as above
-- Note: following content may be copied from file "library/theme_config/demo_data/special_shop/xxx.sql" if special file defined

-- 2020/07/30: change theme demo to adg_topal_xfec.
-- Note: following content is copied from file "library/theme_config/demo_data/special_shop/adg_topal_xfec.sql"
--

-- IMPORTANT: Now move to end of file. Reason:
-- + we have some migrate sql lines below this
-- + the demo data should be latest version

-- -----------------------------------------------------------
-- --------end v1.5.2 demo data for theme---------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.5.2 tool theme----------------------------------
-- -----------------------------------------------------------

ALTER TABLE `oc_user` ADD `theme_develop` tinyint(1) NULL NULL DEFAULT '0' AFTER `permission`;

-- -----------------------------------------------------------
-- --------end v1.5.2 tool theme------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- v1.5.2 Remake menu ----------------------------------------
-- -----------------------------------------------------------

--
-- create table oc_menu
--

CREATE TABLE `oc_menu` (
  `menu_id` INT(11) NOT NULL AUTO_INCREMENT,
  `parent_id` INT(11) NULL,
  `menu_type_id` TINYINT(2) NULL,
  `target_value` VARCHAR(256) NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- create table oc_menu_description
--

CREATE TABLE `oc_menu_description` (
  `menu_id` INT(11) NOT NULL,
  `name` VARCHAR(256) NOT NULL,
  `language_id` INT(11) NOT NULL,
  PRIMARY KEY( `menu_id`, `language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- create table oc_menu_type
--

CREATE TABLE `oc_menu_type` (
  `menu_type_id` INT(11) NOT NULL AUTO_INCREMENT,
  `value` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`menu_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- create table oc_menu_type_description
--

CREATE TABLE `oc_menu_type_description` (
  `menu_type_id` INT(11) NOT NULL,
  `language_id` INT(11) NOT NULL,
  `description` VARCHAR(256) NOT NULL,
  PRIMARY KEY( `menu_type_id`, `language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- insert menu type db
--
INSERT INTO `oc_menu_type` (`menu_type_id`, `value`) VALUES
('1', 'category'),
('2', 'collection'),
('3', 'product'),
('4', 'url'),
('5', 'built-in page'),
('6', 'blog');

--
-- insert oc_menu_type_description db
--
INSERT INTO `oc_menu_type_description` (`menu_type_id`, `language_id`, `description`) VALUES
('1', '1', 'Category'),
('1', '2', 'Loại sản phẩm'),
('2', '1', 'Collection'),
('2', '2', 'Bộ sưu tập'),
('3', '1', 'Product'),
('3', '2', 'Sản phẩm'),
('4', '1', 'Url'),
('4', '2', 'Đường dẫn'),
('5', '1', 'Built-in Page'),
('5', '2', 'Trang mặc định'),
('6', '1', 'Blog'),
('6', '2', 'Bài viết');

--
-- Dumping data for table `oc_menu`
--
INSERT INTO `oc_menu` (`menu_id`, `parent_id`, `menu_type_id`, `target_value`) VALUES
(1, NULL, NULL, NULL),
(8, 1, 4, '/gioi-thieu?about-us'),
(10, 1, 5, '?route=common/shop'),
(11, 1, 4, '/bai-viet?type=du-an'),
(12, 1, 4, '/bai-viet'),
(13, 1, 5, '?route=contact/contact'),
(14, 10, 1, ''),
(15, 10, 1, ''),
(16, NULL, NULL, NULL),
(17, 16, 1, ''),
(18, 16, 1, ''),
(19, 16, 1, ''),
(20, 16, 1, ''),
(21, 16, 1, '');

--
-- Dumping data for table `oc_menu_description`
--
INSERT INTO `oc_menu_description` (`menu_id`, `name`, `language_id`) VALUES
(1, 'Menu Chính', 1),
(1, 'Menu Chính', 2),
(8, 'GIỚI THIỆU', 1),
(8, 'GIỚI THIỆU', 2),
(10, 'SẢN PHẨM', 1),
(10, 'SẢN PHẨM', 2),
(11, 'DỰ ÁN', 1),
(11, 'DỰ ÁN', 2),
(12, 'TIN TỨC', 1),
(12, 'TIN TỨC', 2),
(13, 'LIÊN HỆ', 1),
(13, 'LIÊN HỆ', 2),
(14, 'SẢN PHẨM BÁN CHẠY', 2),
(14, 'SẢN PHẨM BÁN CHẠY', 1),
(15, 'SẢN PHẨM MỚI', 2),
(15, 'SẢN PHẨM MỚI', 1),
(16, 'Điều khoản', 2),
(16, 'Điều khoản', 1),
(17, 'Hướng dẫn mua hàng', 2),
(17, 'Hướng dẫn mua hàng', 1),
(18, 'Bảo mật thông tin', 2),
(18, 'Bảo mật thông tin', 1),
(19, 'Chính sách bán hàng', 2),
(19, 'Chính sách bán hàng', 1),
(20, 'Chính sách đổi/ trả hàng', 2),
(20, 'Chính sách đổi/ trả hàng', 1),
(21, 'Chính sách bảo hành/ bảo trì', 2),
(21, 'Chính sách bảo hành/ bảo trì', 1);

-- -----------------------------------------------------------
-- --------end v1.5.2 Remake menu ----------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.5.2 default policy for gg shopping--------------
-- -----------------------------------------------------------

-- create migrate table
CREATE TABLE IF NOT EXISTS `oc_migration` (
  `migration_id` int(11) NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `run` tinyint(1) NOT NULL DEFAULT 1,
  `date_run` datetime NOT NULL,
  PRIMARY KEY (`migration_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `oc_migration` (`migration`, `run`, `date_run`) VALUES
('migration_20190718_default_policy_gg_shopping', 1, NOW());

-- default payment methods and delivery methods
INSERT INTO `oc_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'payment', 'payment_methods', '[{"payment_id":"198535319739A","name":"Thanh toán khi nhận hàng (COD)","guide":"Bạn sẽ thanh toán toàn bộ giá trị đơn hàng khi nhận hàng","status":"1"}]', 1),
(0, 'delivery', 'delivery_methods', '[{"id":"-1","name":"Giao hàng tiêu chuẩn(liên hệ)","status":"1","fee_region":"0"}]', 1);

-- default policy page and menu
INSERT IGNORE INTO `oc_page_contents` (`page_id`, `title`, `description`, `seo_title`, `seo_description`, `status`, `date_added`) VALUES
(1001, 'Hướng dẫn mua hàng', '&lt;p&gt;Quý khách hàng có thể tìm mua cửa nhôm &lt;strong&gt;Topal&lt;/strong&gt; theo 2 cách:&lt;br /&gt;\r\n1. Cách 1: Gọi đến tổng đài 19006828 để được hướng dẫn&lt;br /&gt;\r\n2. Cách 2: Gõ nơi ở của bạn vào công cụ tìm kiếm dưới đây để biết địa chỉ Đại lý gần bạn nhất.&lt;/p&gt;', '', '', 1, '2022-02-09 09:27:43'),
(1002, 'Bảo mật thông tin', '&lt;p&gt;Topal cam kết sẽ bảo mật những thông tin mang tính riêng tư của khách hàng. Khách hàng vui lòng đọc bản “Chính sách bảo mật” dưới đây để hiểu hơn những cam kết mà Topal thực hiện, nhằm tôn trọng và bảo vệ quyền lợi của người truy cập.&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li&gt;&lt;strong&gt;Mục đích và phạm vi thu thập thông tin&lt;/strong&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p&gt;Để truy cập và sử dụng một số dịch vụ tại Topal, khách hàng có thể sẽ được yêu cầu đăng ký với Topal thông tin cá nhân (Email, Họ tên, Số ĐT liên lạc…) Mọi thông tin khai báo phải đảm bảo tính chính xác và hợp pháp. Topal không chịu mọi trách nhiệm liên quan đến pháp luật của thông tin khai báo.&lt;/p&gt;\r\n\r\n&lt;p&gt;Topal cũng có thể thu thập thông tin về số lần viếng thăm, bao gồm số trang khách hàng xem, số links (liên kết) khách hàng click và những thông tin khác liên quan đến việc kết nối đến Topal.&lt;/p&gt;\r\n\r\n&lt;p&gt;Topal cũng thu thập các thông tin mà ứng dụng/trình duyệt khách hàng sử dụng mỗi khi truy cập vào website.&lt;/p&gt;\r\n\r\n&lt;ol start=&quot;2&quot;&gt;\r\n	&lt;li&gt;&lt;strong&gt;Phạm vi sử dụng thông tin&lt;/strong&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p&gt;Topal thu thập và sử dụng thông tin cá nhân khách hàng với mục đích phù hợp và hoàn toàn tuân thủ nội dung của “Chính sách bảo mật” này. Khi cần thiết, Topal có thể sử dụng những thông tin này để liên hệ trực tiếp với khách hàng dưới các hình thức như: gởi thư ngỏ, đơn đặt hàng, thư cảm ơn, sms, thông tin về kỹ thuật và bảo mật…&lt;/p&gt;\r\n\r\n&lt;ol start=&quot;3&quot;&gt;\r\n	&lt;li&gt;&lt;strong&gt;Thời gian lưu trữ thông tin&lt;/strong&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p&gt;Dữ liệu cá nhân của thành viên sẽ được lưu trữ cho đến khi có yêu cầu hủy bỏ hoặc tự thành viên đăng nhập và thực hiện hủy bỏ. Còn lại trong mọi trường hợp thông tin cá nhân thành viên sẽ được bảo mật trên máy chủ của Topal.&lt;/p&gt;\r\n\r\n&lt;ol start=&quot;4&quot;&gt;\r\n	&lt;li&gt;&lt;strong&gt;Địa chỉ của đơn vị thu thập và quản lý thông tin cá nhân&lt;/strong&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p&gt;– Công ty Cổ phần Tập đoàn Austdoor&lt;/p&gt;\r\n\r\n&lt;p&gt;Số 35 A Đường số 1, Trần Thái Tông, Quận Cầu Giấy, Hà Nội&lt;br /&gt;\r\nĐT: (024) 44550088 – Fax: (04) 44550086&lt;/p&gt;\r\n\r\n&lt;ol start=&quot;5&quot;&gt;\r\n	&lt;li&gt;&lt;strong&gt;Phương tiện và công cụ để người dùng tiếp cận và chỉnh sửa dữ liệu cá nhân&lt;/strong&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p&gt;Hiện website chưa triển khai chỉnh sửa thông tin cá nhân của thành viên, vì thế việc tiếp cận và chỉnh sửa dữ liệu cá nhân dựa vào yêu cầu của khách hàng bằng cách hình thức sau:&lt;/p&gt;\r\n\r\n&lt;p&gt;– Gọi điện thoại đến tổng đài chăm sóc khách hàng 19006828, nhân viên tổng đài sẽ hỗ trợ chỉnh sửa thay người dùng.&lt;/p&gt;\r\n\r\n&lt;p&gt;– Để lại bình luận hoặc gửi góp ý trực tiếp từ website và ứng dụng, quản trị viên kiểm tra thông tin và xem xét nội dung bình luận phù hợp với pháp luật và chính sách của Topal&lt;/p&gt;\r\n\r\n&lt;ol start=&quot;6&quot;&gt;\r\n	&lt;li&gt;&lt;strong&gt;Cam kết bảo mật thông tin cá nhân khách hàng&lt;/strong&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p&gt;– Thông tin cá nhân của thành viên trên website và ứng dụng được Sơn Hà cam kết bảo mật tuyệt đối theo chính sách bảo vệ thông tin cá nhân của công ty. Việc thu thập và sử dụng thông tin của mỗi thành viên chỉ được thực hiện khi có sự đồng ý của khách hàng đó trừ những trường hợp pháp luật có quy định khác.&lt;/p&gt;\r\n\r\n&lt;p&gt;– Không sử dụng, không chuyển giao, cung cấp hay tiết lộ cho bên thứ 3 nào về thông tin cá nhân của thành viên khi không có sự cho phép đồng ý từ thành viên.&lt;/p&gt;\r\n\r\n&lt;p&gt;– Trong trường hợp máy chủ lưu trữ thông tin bị hacker tấn công dẫn đến mất mát dữ liệu cá nhân thành viên, Sơn Hà sẽ có trách nhiệm thông báo vụ việc cho cơ quan chức năng điều tra xử lý kịp thời và thông báo cho thành viên được biết.&lt;/p&gt;\r\n\r\n&lt;p&gt;– Ban quản lý website và ứng dụng yêu cầu các cá nhân khi đăng ký/mua hàng là thành viên, phải cung cấp đầy đủ thông tin cá nhân có liên quan như: Họ và tên, địa chỉ liên lạc, email, số chứng minh nhân dân, điện thoại, số tài khoản, số thẻ thanh toán… và chịu trách nhiệm về tính pháp lý của những thông tin trên.&lt;/p&gt;\r\n\r\n&lt;p&gt;– Ban quản lý webtsite và ứng dụng không chịu trách nhiệm cũng như không giải quyết mọi khiếu nại có liên quan đến quyền lợi của thành viên đó nếu xét thấy tất cả thông tin cá nhân của thành viên đó cung cấp khi đăng ký ban đầu là không chính xác.&lt;/p&gt;', '', '', 1, '2022-02-09 09:29:17'),
(1003, 'Chính sách bán hàng', '&lt;p&gt;Website &lt;a href=&quot;https://topal.vn/&quot;&gt;www.topal.vn&lt;/a&gt; do công ty Cổ phần Tập đoàn Austdoor thực hiện hoạt động và vận hành nhằm giới thiệu thông tin hoạt động của doanh nghiệp, thông tin sản phẩm đến các cổ đông, nhà đầu tư, đối tác và khách hàng trong nước và quốc tế.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;1. Nguyên tắc chung&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;– Thông tin, sản phẩm giới thiệu tại &lt;a href=&quot;https://topal.vn/&quot;&gt;www.topal.vn&lt;/a&gt; phải trung thực, minh bạch, đáp ứng đầy đủ các quy định của pháp luật Việt Nam&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;2. Quy định chung&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;– Tên Miền website &lt;a href=&quot;https://topal.vn/&quot;&gt;www.topal.vn&lt;/a&gt; do Công ty Cổ phần Tập đoàn Austdoor phát triển với tên miền giao dịch là: www.topal.vn&lt;/p&gt;\r\n\r\n&lt;p&gt;– Đơn vị giới thiệu thông tin là Công ty Cổ phần Tập đoàn Austdoor&lt;/p&gt;\r\n\r\n&lt;p&gt;– Người tiếp nhận thông tin là các nhà đầu tư, cổ đông, đối tác và khách hàng trong và ngoài nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Nội dung bản Quy chế này tuân thủ theo các quy định hiện hành của Việt Nam. Thành viên khi tham gia website &lt;a href=&quot;https://topal.vn/&quot;&gt;www.topal.vn&lt;/a&gt; phải tự tìm hiểu trách nhiệm pháp lý của mình đối với luật pháp hiện hành của Việt Nam và cam kết thực hiện đúng những nội dung trong Quy chế này.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;3. Quy trình mua hàng (Dành cho người mua hàng các sản phẩm của AUSTDOOR)&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Bước 1:&lt;/strong&gt; Tìm sản phẩm cần mua.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Bước 2:&lt;/strong&gt; Xem giá và thông tin chi tiết sản phẩm đó, nếu quý khách đồng ý mua hàng, xin vui lòng chọn Đại lý chính hãng gần nhất của Topal để mua hàng.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;4. Quy định và hình thức thanh toán&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;* Thanh toán tại các Đại lý, Chi nhánh, Nhà phân phối của Công ty&lt;br /&gt;\r\nNgay khi Quý khách thanh toán xong, nhân viên của Đại lý, Chi nhánh và Nhà phân phối sẽ hướng dẫn các thủ tục nhận hàng.&lt;/p&gt;\r\n\r\n&lt;p&gt;* Thanh toán tại điểm giao hàng&lt;/p&gt;\r\n\r\n&lt;p&gt;– Quý khách thanh toán cho nhân viên giao nhận toàn bộ hoặc một phần (nếu đã đặt cọc) giá trị hàng hóa đã mua.&lt;/p&gt;\r\n\r\n&lt;p&gt;– Nếu địa điểm giao hàng ngay tại nơi thanh toán, nhân viên giao hàng của Đại lý, Chi nhánh, Nhà Phân phối sẽ thu tiền khi giao hàng.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;5. Chính sách bản quyền và bảo mật&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;– Mọi quyền sở hữu trí tuệ (đã đăng ký hoặc chưa đăng ký), nội dung thông tin và tất cả các thiết kế, văn bản, đồ họa, phần mềm, hình ảnh, video, âm nhạc, âm thanh, biên dịch phần mềm, mã nguồn và phần mềm cơ bản đều là tài sản của Topal. Toàn bộ nội dung của trang web được bảo vệ bởi luật bản quyền của Việt Nam và các công ước quốc tế. Bản quyền đã được bảo lưu.&lt;/p&gt;\r\n\r\n&lt;p&gt;– Topal không thu thập bất kỳ thông tin cá nhân qua www.topal.vn&lt;/p&gt;', 'Chính sách bán hàng', '', 1, '2022-02-09 09:30:47'),
(1004, 'Chính sách đổi/trả hàng', '&lt;p&gt;Chính sách Đổi trả sản phẩm của &lt;a href=&quot;http://www.austdoor.com/&quot;&gt;www.topal.vn&lt;/a&gt; quy định các lý do chấp nhận, yêu cầu cho sản phẩm được đổi trả và thời gian xử lý đổi trả cho khách hàng. (Chỉ áp dụng cho ĐL phân phối và sản xuất)&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Yêu cầu cho sản phẩm đổi trả:&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Điều kiện đổi trả:&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;– Khách hàng đã thanh toán tiền nhưng không nhận được hàng&lt;/p&gt;\r\n\r\n&lt;p&gt;– Sản phẩm bị lỗi hoặc hư hại trong quá trình vận chuyển&lt;/p&gt;\r\n\r\n&lt;p&gt;– Sản phẩm bị mất niêm phong, bị giao sai về số lượng, thông tin và mẫu mã so với đơn đặt hàng&lt;/p&gt;\r\n\r\n&lt;p&gt;– Sản phẩm còn đầy đủ phụ kiện, chứng từ kèm theo (chứng từ mua hàng, hóa đơn VAT nếu có, hướng dẫn sử dụng, phiếu bảo hành…) và tặng phẩm đi kèm (nếu có)&lt;/p&gt;\r\n\r\n&lt;p&gt;– Sản phẩm không bị dơ bẩn, không bị lỗi hình thức, không có dấu hiệu đã qua sử dụng&lt;/p&gt;\r\n\r\n&lt;p&gt;– Sản phẩm được xác định bị lỗi kỹ thuật bởi các kỹ thuật viên của Topal&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Điều kiện hàng hóa không đạt yêu cầu đổi trả:&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;– Khách hàng muốn thay đổi chủng loại, mẫu mã mà không báo trước&lt;/p&gt;\r\n\r\n&lt;p&gt;– Sản phẩm bị hỏng hóc do khách hàng vận hành không đúng chỉ dẫn&lt;/p&gt;\r\n\r\n&lt;p&gt;– Khách hàng tự làm ảnh hưởng đến tình trạng bên ngoài sản phẩm như nứt, vỡ, trầy xước…&lt;/p&gt;\r\n\r\n&lt;p&gt;– Sản phẩm bị lỗi hình thức sau khi khách hàng đã kiểm tra và xác nhận nhận hàng&lt;/p&gt;', '', '', 1, '2022-02-09 09:31:47'),
(1005, 'Chính sách bảo hành/ bảo trì', '&lt;p&gt;&lt;img alt=&quot;&quot; height=&quot;792&quot; sizes=&quot;(max-width: 960px) 100vw, 960px&quot; src=&quot;https://topal.vn/wp-content/uploads/2018/03/29136565_190209514918535_5504481759305138176_n.png&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2018/03/29136565_190209514918535_5504481759305138176_n.png 960w, https://topal.vn/wp-content/uploads/2018/03/29136565_190209514918535_5504481759305138176_n-300x248.png 300w, https://topal.vn/wp-content/uploads/2018/03/29136565_190209514918535_5504481759305138176_n-768x634.png 768w, https://topal.vn/wp-content/uploads/2018/03/29136565_190209514918535_5504481759305138176_n-600x495.png 600w, https://topal.vn/wp-content/uploads/2018/03/29136565_190209514918535_5504481759305138176_n-177x146.png 177w, https://topal.vn/wp-content/uploads/2018/03/29136565_190209514918535_5504481759305138176_n-50x41.png 50w, https://topal.vn/wp-content/uploads/2018/03/29136565_190209514918535_5504481759305138176_n-91x75.png 91w&quot; width=&quot;960&quot; /&gt;&lt;/p&gt;', 'Chính sách bảo hành/ bảo trì', '', 1, '2022-02-09 09:32:19');

INSERT IGNORE INTO `oc_seo_url` (`seo_url_id`,`store_id`, `language_id`, `query`, `keyword`, `table_name`) VALUES
(2417, 0, 1, 'page_id=1004', 'chinh-sach-doi-tra', ''),
(2416, 0, 2, 'page_id=1004', 'chinh-sach-doi-tra', ''),
(2418, 0, 2, 'page_id=1005', 'chinh-sach-bao-hanh', ''),
(2419, 0, 1, 'page_id=1005', 'chinh-sach-bao-hanh', ''),
(2410, 0, 2, 'page_id=1003', 'chinh-sach-ban-hang', ''),
(2407, 0, 1, 'page_id=1002', 'bao-mat-thong-tin', ''),
(2406, 0, 2, 'page_id=1002', 'bao-mat-thong-tin', ''),
(2402, 0, 2, 'page_id=1001', 'huong-dan-mua-hang', ''),
(2403, 0, 1, 'page_id=1001', 'huong-dan-mua-hang', ''),
(2411, 0, 1, 'page_id=1003', 'chinh-sach-ban-hang', '');

-- -----------------------------------------------------------
-- --------end v1.5.2 default policy for gg shopping----------
-- -----------------------------------------------------------


-- -----------------------------------------------------------
-- --------enable seo ----------------------------------------
-- -----------------------------------------------------------
UPDATE `oc_setting` SET `value` = '1' WHERE `code` = 'config' AND `key` = 'config_seo_url';

INSERT INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`, `table_name`) VALUES
(NULL, '0', '1', 'common/home', 'home', ''),
(NULL, '0', '2', 'common/home', 'trang-chu', ''),
(NULL, '0', '1', 'common/shop', 'products', ''),
(NULL, '0', '2', 'common/shop', 'san-pham', ''),
(NULL, '0', '1', 'contact/contact', 'contact', ''),
(NULL, '0', '2', 'contact/contact', 'lien-he', ''),
(NULL, '0', '1', 'checkout/profile', 'profile', ''),
(NULL, '0', '2', 'checkout/profile', 'tai-khoan', ''),
(NULL, '0', '1', 'account/login', 'login', ''),
(NULL, '0', '2', 'account/login', 'dang-nhap', ''),
(NULL, '0', '1', 'account/register', 'register', ''),
(NULL, '0', '2', 'account/register', 'dang-ky', ''),
(NULL, '0', '1', 'account/logout', 'logout', ''),
(NULL, '0', '2', 'account/logout', 'dang-xuat', ''),
(NULL, '0', '1', 'checkout/setting', 'setting', ''),
(NULL, '0', '2', 'checkout/setting', 'cai-dat', ''),
(NULL, '0', '1', 'checkout/my_orders', 'checkout-cart', ''),
(NULL, '0', '2', 'checkout/my_orders', 'gio-hang', ''),
(NULL, '0', '1', 'checkout/order_preview', 'payment', ''),
(NULL, '0', '2', 'checkout/order_preview', 'thanh-toan', ''),
(NULL, '0', '1', 'checkout/order_preview/orderSuccess', 'payment-success', ''),
(NULL, '0', '2', 'checkout/order_preview/orderSuccess', 'dat-hang-thanh-cong', ''),
(NULL, '0', '1', 'blog/blog', 'blog', ''),
(NULL, '0', '2', 'blog/blog', 'bai-viet', '');

-- -----------------------------------------------------------
-- --------enable seo ----------------------------------------
-- -----------------------------------------------------------


-- -----------------------------------------------------------
-- --------v1.5.3 tuning 56 key points -----------------------
-- -----------------------------------------------------------

ALTER TABLE `oc_product` ADD `common_price` decimal(15,4) DEFAULT NULL AFTER `sku`;
ALTER TABLE `oc_product` ADD `common_compare_price` decimal(15,4) DEFAULT NULL AFTER `sku`;
ALTER TABLE `oc_product` ADD `common_sku` varchar(64) DEFAULT NULL AFTER `sku`;
ALTER TABLE `oc_product` ADD `common_barcode` varchar(128) DEFAULT NULL AFTER `sku`;

-- -----------------------------------------------------------
-- --------end v1.5.3 tuning 56 key points -------------------
-- -----------------------------------------------------------


-- -----------------------------------------------------------
-- --------v1.6.1 integrate 3rd API --------------------------
-- -----------------------------------------------------------

-- add column `source` to oc_order
ALTER TABLE `oc_order`
  ADD `source` VARCHAR(50) NULL AFTER `user_id`,
  ADD `transport_name` varchar(64) DEFAULT NULL AFTER `telephone`,
  ADD `transport_order_code` varchar(64) DEFAULT NULL AFTER `telephone`,
  ADD `transport_service_name` varchar(64) DEFAULT NULL AFTER `telephone`,
  ADD `transport_money_total` VARCHAR(255) NOT NULL AFTER `transport_name`,
  ADD `transport_weight` VARCHAR(255) NOT NULL AFTER `transport_money_total`,
  ADD `transport_status` VARCHAR(255) NOT NULL AFTER `transport_weight`,
  ADD `transport_current_warehouse` VARCHAR(255) NOT NULL AFTER `transport_status`;

INSERT INTO `oc_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'config', 'config_VIETTEL_POST_token_webhook', '', 0);

-- -----------------------------------------------------------
-- --------end v1.6.1 integrate 3rd API ----------------------
-- -----------------------------------------------------------


-- -----------------------------------------------------------
-- --------v1.7.1 create report tables -----------------------
-- -----------------------------------------------------------

-- create table `oc_report_order` for report revenue, order and customer with order
CREATE TABLE IF NOT EXISTS `oc_report_order` (
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `report_date` date NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT 0,
  `total_amount` decimal(18,4) DEFAULT NULL,
  `order_status` int(11) DEFAULT 0
  -- PRIMARY KEY (`report_time`, `order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_report_product` for report product with quantity and total_amount
CREATE TABLE IF NOT EXISTS `oc_report_product` (
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `report_date` date NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_version_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(18,4) DEFAULT NULL,
  `total_amount` decimal(18,4) DEFAULT NULL
  -- PRIMARY KEY (`report_time`, `order_id`, `product_id`, `product_version_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_report_product` for report new customer
CREATE TABLE IF NOT EXISTS `oc_report_customer` (
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `report_date` date NOT NULL,
  `customer_id` int(11) DEFAULT 0
  -- PRIMARY KEY (`report_time`, `customer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_report_web_traffic` for report web traffic (then calculate conversion rate, ...)
CREATE TABLE IF NOT EXISTS `oc_report_web_traffic` (
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `report_date` date NOT NULL,
  `value` varchar(500) NOT NULL,
  `last_updated` timestamp NULL DEFAULT NULL
  -- PRIMARY KEY (`report_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- create some events to insert data to some report tables above

-- on order change event
INSERT INTO `oc_event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES
('create new order', 'admin/model/sale/order/addOrder/after', 'event/report_order/afterCreateOrder', 1, 0),
('edit order in list', 'admin/model/sale/order/updateColumnTotalOrder/after', 'event/report_order/editOrderInOrderList', 1, 0),
('change status order in order list', 'admin/model/sale/order/updateStatus/after', 'event/report_order/updateOrderStatusInOrderList', 1, 0),
('edit order in order detail', 'admin/model/sale/order/editOrder/after', 'event/report_order/editOrderInOrderDetail', 1, 0),
('create new order', 'admin/model/sale/order/addOrder/after', 'event/report_product/afterCreateOrder', 1, 0),
('edit order in list', 'admin/model/sale/order/updateProductOrder/after', 'event/report_product/editOrderInOrderList', 1, 0),
('change status order in order list', 'admin/model/sale/order/updateStatus/after', 'event/report_product/updateOrderStatusInOrderList', 1, 0),
('edit order in order detail', 'admin/model/sale/order/editOrder/after', 'event/report_product/editOrderInOrderDetail', 1, 0);

-- on customer change event
INSERT INTO `oc_event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES
('create customer', 'admin/model/customer/customer/addCustomer/after', 'event/report_customer/createCustomer', 1, 0),
('delete customer', 'admin/model/customer/customer/deleteCustomer/after', 'event/report_customer/deleteCustomer', 1, 0);

-- -----------------------------------------------------------
-- --------end v1.7.1 create report tables -------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.7.2 onboarding ---------------------------------
-- -----------------------------------------------------------

INSERT INTO `oc_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'config', 'config_onboarding_complete', '0', 0),
(0, 'config', 'config_onboarding_step_active', '', 0),
(0, 'config', 'config_onboarding_voucher', '', 0),
(0, 'config', 'config_onboarding_used_voucher', '0', 0);

-- -----------------------------------------------------------
-- --------end v1.7.2 onboarding -----------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.7.3 tuning -------------------------------------
-- -----------------------------------------------------------

-- add column `from_domain` to oc_order
ALTER TABLE `oc_order`
  ADD `from_domain` VARCHAR(500) NULL DEFAULT NULL AFTER `source`;

--
-- init first override css version
--
INSERT INTO `oc_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'config', 'builder_config_version', '1', 0);

-- -----------------------------------------------------------
-- --------end v1.7.3 tuning ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.8.1 theme api + builder + blog -----------------
-- -----------------------------------------------------------

-- create table `oc_collection_to_collection`
CREATE TABLE IF NOT EXISTS `oc_collection_to_collection` (
  `collection_to_collection_id` int(11) NOT NULL AUTO_INCREMENT,
  `collection_id` int(11) NOT NULL,
  `parent_collection_id` int(11) NOT NULL,
  `sort_order` int(11) DEFAULT '0',
  PRIMARY KEY (`collection_to_collection_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- add `type` to table `oc_collection`
ALTER TABLE `oc_collection` ADD `type` varchar(255) DEFAULT '0';

-- create table `oc_blog`
CREATE TABLE IF NOT EXISTS `oc_blog` (
  `blog_id` int(11) NOT NULL AUTO_INCREMENT,
  `author` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_publish` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`blog_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_blog`
CREATE TABLE IF NOT EXISTS `oc_blog_description` (
  `blog_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text,
  `short_content` text,
  `image` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` text,
  `alias` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`blog_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_blog_category`
CREATE TABLE IF NOT EXISTS `oc_blog_category` (
  `blog_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`blog_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table `oc_blog_category`
INSERT INTO `oc_blog_category` (`blog_category_id`, `status`, `date_added`, `date_modified`) VALUES
(1, 1, '2019-11-11 13:37:51', '2021-11-24 10:19:29'),
(5, 1, '2021-11-08 17:31:28', '2021-11-08 17:31:28'),
(3, 1, '2021-11-08 17:29:21', '2021-11-08 17:29:21'),
(4, 1, '2021-11-08 17:29:35', '2021-11-08 17:29:35'),
(6, 1, '2021-11-24 13:13:39', '2021-11-24 14:07:25'),
(7, 1, '2021-12-02 16:56:04', '2021-12-02 16:56:04');

-- create table `oc_blog_category_description`
CREATE TABLE IF NOT EXISTS `oc_blog_category_description` (
  `blog_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `alias` varchar(255) NOT NULL,
  PRIMARY KEY (`blog_category_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- create table `oc_blog_to_blog_category`
CREATE TABLE IF NOT EXISTS `oc_blog_to_blog_category` (
  `blog_id` int(11) NOT NULL,
  `blog_category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_blog_tag`
CREATE TABLE IF NOT EXISTS `oc_blog_tag` (
  `blog_tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`blog_tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- -----------------------------------------------------------
-- --------end v1.8.1 theme api + builder + blog -------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.8.2 tuning -------------------------------------
-- -----------------------------------------------------------

-- default order success text
INSERT INTO `oc_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
('0', 'config', 'config_order_success_text', 'MUA HÀNG THÀNH CÔNG!', '0');

-- -----------------------------------------------------------
-- --------end v1.8.2 tuning ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.9.2 pos ----------------------------------------
-- -----------------------------------------------------------

-- add column previous_order_status_id to table order
ALTER TABLE `oc_order` ADD `pos_id_ref` VARCHAR(50) NULL DEFAULT NULL AFTER `order_code`;

-- add column store_id and user_id to tables report. default store_id = 0 and user_id = 1
ALTER TABLE `oc_report_product`
  ADD `store_id` INT(11) NULL DEFAULT 0 AFTER `total_amount`,
  ADD `user_id` INT NULL DEFAULT 1 AFTER `store_id`;

ALTER TABLE `oc_report_order`
  ADD `store_id` INT(11) NULL DEFAULT 0 AFTER `order_status`,
  ADD `user_id` INT(11) NULL DEFAULT 1 AFTER `store_id`;

ALTER TABLE `oc_report_customer`
  ADD `store_id` INT(11) NULL DEFAULT 0 AFTER `customer_id`,
  ADD `user_id` INT(11) NULL DEFAULT 1 AFTER `store_id`;

-- add column channel to table product. null or 0 -> website, 1 -> offline, 2 -> website + offline
ALTER TABLE `oc_product` ADD `channel` INT NULL DEFAULT 2 AFTER `status`;

-- insert default store to new shop
INSERT INTO `oc_store` (`name`, `url`, `ssl`) VALUES
('Kho trung tâm', '', '');

-- IMPORTANT: force from 0, because current sql_mode is not 'NO_AUTO_VALUE_ON_ZERO'
UPDATE `oc_store` SET `store_id` = 0 WHERE `name` = 'Kho trung tâm';

-- -----------------------------------------------------------
-- --------end v1.9.2 pos ------------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.9.3 tuning -------------------------------------
-- -----------------------------------------------------------

-- create table `oc_images`
CREATE TABLE IF NOT EXISTS `oc_images` (
    `image_id` INT(11) NOT NULL AUTO_INCREMENT,
    `type` VARCHAR(56) NOT NULL DEFAULT 'image',
    `name` varchar(256) NULL default NULL,
    `url` VARCHAR(256) NOT NULL,
    `directory` VARCHAR(56) NULL DEFAULT NULL,
    `status` TINYINT(1) NOT NULL DEFAULT '1',
    `source` VARCHAR(56) NOT NULL DEFAULT 'Cloudinary',
    `source_id` VARCHAR (256) NULL DEFAULT  NULL,
     PRIMARY KEY (`image_id`)
)ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- add default register success text
INSERT INTO `oc_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'config', 'config_register_success_text', '<p>Chúc mừng! Tài khoản mới của bạn đã được tạo thành công!</p>\n\n<p>Bây giờ bạn có thể tận hưởng lợi ích thành viên khi tham gia trải nghiệm mua sắm cùng chúng tôi.</p>\n\n<p>Nếu bạn có câu hỏi gì về hoạt động của cửa hàng, vui lòng liên hệ với chủ cửa hàng.</p>\n\n<p>Một email xác nhận đã được gửi tới hòm thư của bạn. Nếu bạn không nhận được email này trong vòng 1 giờ, vui lòng liên hệ chúng tôi.</p>', 0);

-- -----------------------------------------------------------
-- --------end v1.9.3 tuning ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------HOT: soft deletable product -----------------------
-- -----------------------------------------------------------

ALTER TABLE `oc_product` ADD `deleted` VARCHAR(50) DEFAULT NULL AFTER `demo`;
ALTER TABLE `oc_product_version` ADD `deleted` VARCHAR(50) NULL AFTER `status`;

-- -----------------------------------------------------------
-- --------end HOT: soft deletable product -------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- v2.1 discount ------------------------------------
-- -----------------------------------------------------------

-- create table `oc_discount`
CREATE TABLE IF NOT EXISTS `oc_discount` (
  `discount_id` INT(11) NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(50) NOT NULL,
  `discount_type_id` INT(11) NOT NULL,
  `discount_status_id` INT(11) NOT NULL,
  `usage_limit` INT(11) NOT NULL DEFAULT '0',
  `times_used` INT(11) NOT NULL DEFAULT '0',
  `config` MEDIUMTEXT NULL DEFAULT NULL,
  `order_source` VARCHAR(128) NULL DEFAULT NULL,
  `customer_group` VARCHAR(128) NULL DEFAULT 'All',
  `start_at` DATETIME NOT NULL,
  `end_at` DATETIME NULL,
  `pause_reason` VARCHAR(256) NULL,
  `paused_at` DATETIME NULL,
  `delete_reason` VARCHAR(256) NULL,
  `deleted_at` DATETIME NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`discount_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_discount_description`
CREATE TABLE IF NOT EXISTS `oc_discount_description` (
  `discount_id` INT(11) NOT NULL,
  `language_id` INT(11) NOT NULL,
  `name` VARCHAR(256) NOT NULL,
  `description` VARCHAR(256) NOT NULL,
  PRIMARY KEY (discount_id, `language_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_discount_to_store`
CREATE TABLE IF NOT EXISTS `oc_discount_to_store` (
  `discount_id` INT(11) NOT NULL,
  `store_id` INT(11) NOT NULL,
  PRIMARY KEY (`discount_id`, `store_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_discount_type`
CREATE TABLE IF NOT EXISTS `oc_discount_type` (
  `discount_type_id` INT(11) NOT NULL,
  `language_id` INT(11) NOT NULL,
  `name` VARCHAR(128) NOT NULL,
  `description` VARCHAR(128) NULL,
  PRIMARY KEY (`discount_type_id`, `language_id` )
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_discount_status`
CREATE TABLE IF NOT EXISTS `oc_discount_status` (
  `discount_status_id` INT(11) NOT NULL,
  `language_id` INT(11) NOT NULL,
  `name` VARCHAR(128) NOT NULL,
  `description` VARCHAR(128) NULL,
  PRIMARY KEY (`discount_status_id`, `language_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_order_to_discount`
CREATE TABLE IF NOT EXISTS `oc_order_to_discount` (
  `order_id` INT(11) NOT NULL,
  `discount_id` INT(11) NOT NULL,
  PRIMARY KEY (`order_id`, `discount_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- add discount to table order_product
ALTER TABLE `oc_order_product` ADD `discount` decimal(18,4) NOT NULL DEFAULT '0.0000' AFTER `price`;

-- add discount to table order
ALTER TABLE `oc_order` ADD `discount` decimal(18,4) NOT NULL DEFAULT '0.0000' AFTER `order_cancel_comment`;

-- init data for table `oc_discount_status`
INSERT INTO `oc_discount_status` (`discount_status_id`, `language_id`, `name`, `description`) VALUES
('1', '2', 'Đã lập lịch', NULL),
('2', '2', 'Đang hoạt đông', NULL),
('3', '2', 'Hết hạn', NULL),
('4', '2', 'Đã xóa', NULL),
('1', '1', 'Scheduled', NULL),
('2', '1', 'Activated', NULL),
('3', '1', 'Paused', NULL),
('4', '1', 'Deleted', NULL);

-- init data for table `oc_discount_type`
INSERT INTO `oc_discount_type` (`discount_type_id`, `language_id`, `name`, `description`) VALUES
('1', '2', 'Chiết khấu theo tổng giá trị đơn hàng', NULL),
('2', '2', 'Chiết khấu theo từng sản phẩm', NULL),
('3', '2', 'Chiết khấu theo loại sản phẩm', NULL),
('4', '2', 'Chiết khấu theo nhà cung cấp', NULL),
('1', '1', 'Discount by total order value', NULL),
('2', '1', 'Discount by each product', NULL),
('3', '1', 'Discount by product type', NULL),
('4', '1', 'Discount by supplier', NULL);

-- -----------------------------------------------------------
-- --------end v2.1 discount ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v2.2.1 api chatbot v2 -----------------------------
-- -----------------------------------------------------------

-- add discount to table order
ALTER TABLE `oc_order` ADD `customer_ref_id` VARCHAR(128) NULL DEFAULT NULL AFTER `pos_id_ref`;

-- -----------------------------------------------------------
-- --------end v2.2.1 api chatbot v2 -------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- v2.2.2-store-receipt -----------------------------
-- -----------------------------------------------------------

-- create table `oc_store_receipt`
CREATE TABLE IF NOT EXISTS `oc_store_receipt` (
  `store_receipt_id` INT(11) NOT NULL AUTO_INCREMENT,
  `store_id` INT(11) NOT NULL,
  `manufacturer_id` INT(11) NOT NULL,
  `store_receipt_code` varchar(50) NULL,
  `total` decimal(18,4) DEFAULT NULL,
  `total_to_pay` decimal(18,4) DEFAULT NULL,
  `total_paid` decimal(18,4) DEFAULT NULL,
  `discount` decimal(18,4) DEFAULT NULL,
  `allocation_criteria_type` VARCHAR(50) DEFAULT NULL,
  `allocation_criteria_total` decimal(18,4) DEFAULT NULL,
  `allocation_criteria_fees` MEDIUMTEXT NULL DEFAULT NULL,
  `owed` decimal(18,4) DEFAULT NULL,
  `comment` TEXT NULL DEFAULT NULL,
  `status` INT(4) NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`store_receipt_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_store_receipt_to_product`
CREATE TABLE IF NOT EXISTS `oc_store_receipt_to_product` (
  `store_receipt_product_id` INT(11) NOT NULL  AUTO_INCREMENT,
  `store_receipt_id` INT(11) NOT NULL,
  `product_id` INT(11) NOT NULL,
  `product_version_id` INT(11) NULL,
  `quantity` INT(11) NULL,
  `cost_price` decimal(18,4) DEFAULT NULL,
  `fee` decimal(18,4) DEFAULT NULL,
  `discount` decimal(18,4) DEFAULT NULL,
  `total` decimal(18,4) DEFAULT NULL,
  PRIMARY KEY (`store_receipt_product_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- new column `oc_manufacturer`
ALTER TABLE `oc_manufacturer`
  ADD `manufacturer_code` varchar(50) NULL AFTER `sort_order`,
  ADD `telephone` varchar(30) NULL AFTER `manufacturer_code`,
  ADD `email` varchar(200) NULL AFTER `telephone`,
  ADD `tax_code` varchar(200) NULL AFTER `email`,
  ADD `address` text NULL AFTER `tax_code`,
  ADD `province` varchar(50) NULL AFTER `address`,
  ADD `district` varchar(50) NULL AFTER `province`,
  ADD `status` int(4) NOT NULL DEFAULT '1' AFTER `district`,
  ADD `date_added` datetime NULL AFTER `status`,
  ADD `date_modified` datetime NULL AFTER `date_added`;

-- new column oc_product_to_store
ALTER TABLE `oc_product_to_store`
  ADD `product_version_id` int(11) NULL DEFAULT 0 AFTER `store_id`,
  ADD `quantity` int(11) NULL DEFAULT 0 AFTER `product_version_id`,
  ADD `cost_price` DECIMAL(15,4 ) NULL DEFAULT '0' AFTER `quantity`;

-- change primary key oc_product_to_store
ALTER TABLE `oc_product_to_store`
  DROP PRIMARY KEY, ADD PRIMARY KEY(`product_id`, `product_version_id`, `store_id`);

-- new column oc_product
ALTER TABLE `oc_product`
  ADD `default_store_id` INT(11) NULL DEFAULT '0' AFTER `deleted`;

-- -----------------------------------------------------------
-- --------end v2.2.2-store-receipt --------------------------
-- -----------------------------------------------------------


-- -----------------------------------------------------------
-- -------- v2.2.3-single signon + chatbot api v2 more -------
-- -----------------------------------------------------------

CREATE TABLE IF NOT EXISTS `oc_order_utm` (
  `order_utm_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `code` varchar(256) NOT NULL,
  `key` varchar(256) NOT NULL,
  `value` text NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
   PRIMARY KEY (`order_utm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- -----------------------------------------------------------
-- --------end v2.2.3-single signon + chatbot api v2 more ----
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------start v2.3.2-tuning -------------------------------
-- -----------------------------------------------------------

-- new column oc_product
ALTER TABLE `oc_product`
   ADD `common_cost_price` DECIMAL(15,4 ) NULL DEFAULT '0' AFTER `common_price`;

-- -----------------------------------------------------------
-- --------end v2.3.2-tuning ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------start v2.4.2-app-store -------------------------------
-- -----------------------------------------------------------
-- create table my_app
CREATE TABLE IF NOT EXISTS `oc_my_app` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_code` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1)  NULL,
  `expiration_date` datetime  DEFAULT NULL,
  `app_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path_asset` text COLLATE utf8_unicode_ci  NULL,
  `trial` int(2)  NULL,
  `installed` int(11) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- INSERT APP DEFAULT FOR APP
-- INSERT INTO `oc_my_app` (`app_code`, `status`, `expiration_date`, `app_name`, `path_logo`, `path_asset`, `trial`, `installed`,`date_added`,`date_modified`) VALUES
-- ('mar_ons_chatbot', 1, NULL, 'NovaonX ChatBot', 'view/image/appstore/mar_ons_chatbot.jpg', '', 0, 1, NOW(), NOW()),
-- ('mar_ons_maxlead', 1, NULL, 'AutoAds Maxlead', 'view/image/appstore/mar_ons_maxlead.jpg', '', 0, 1, NOW(), NOW()),
-- ('trans_ons_vtpost', 1, NULL, 'VietelPost', 'view/image/appstore/trans_ons_vtpost.jpg', '', 0, 1, NOW(), NOW()),
-- ('trans_ons_ghn', 1, NULL, 'Giao Hàng Nhanh', 'view/image/appstore/trans_ons_ghn.jpg', '', 0, 1, NOW(), NOW());

-- craete table appstore_setting
CREATE TABLE IF NOT EXISTS `oc_appstore_setting` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 NOT NULL,
  `code` varchar(64) CHARACTER SET utf8 NOT NULL,
  `setting` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- create table app_config_theme
CREATE TABLE IF NOT EXISTS `oc_app_config_theme` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `module_id` int(255) NOT NULL,
  `theme_value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NULL,
  `sort` int(10)  NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- -----------------------------------------------------------
-- --------end v2.4.2-app-store ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- start v2.5.2 Advance Store Management ------------
-- -----------------------------------------------------------

-- store_transfer_receipt : Table to save transfer receipt of store
CREATE TABLE IF NOT EXISTS `oc_store_transfer_receipt` (
  `store_transfer_receipt_id` INT(11) NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(128) NOT NULL,
  `export_store_id` INT(11) NOT NULL,
  `import_store_id` INT(11) NOT NULL,
  `common_quantity_transfer` INT(16) NULL,
  `total` decimal(18,4) NOT NULL,
  `status` TINYINT(2) NOT NULL DEFAULT '0' COMMENT '0: lưu nháp, 1: đã chuyển, 2: đã nhận, 9: đã hủy',
  `date_exported` DATE NOT NULL,
  `date_imported` DATE NULL,
  `date_added` DATETIME NOT NULL,
  `date_modified` DATETIME NOT NULL,
  PRIMARY KEY (`store_transfer_receipt_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- store_transfer_receipt_to_product : relationship transfer receipt with product
CREATE TABLE IF NOT EXISTS `oc_store_transfer_receipt_to_product` (
  `store_transfer_receipt_id` INT(11) NOT NULL,
  `product_id` INT(11) NOT NULL,
  `product_version_id` INT(11) NOT NULL DEFAULT '0',
  `quantity` INT(11) NOT NULL,
  `current_quantity` INT(11) NULL,
  `price` decimal(18,4) NOT NULL,
  PRIMARY KEY (`store_transfer_receipt_id`, `product_id`, `product_version_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- store_take_receipt : Table to save take receipt of store
CREATE TABLE IF NOT EXISTS `oc_store_take_receipt` (
  `store_take_receipt_id` INT(11) NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(128) NOT NULL,
  `store_id` INT(11) NOT NULL,
  `difference_amount` decimal(18,4) NOT NULL,
  `status` TINYINT(2) NOT NULL DEFAULT '0',
  `date_taked` DATE NOT NULL,
  `date_added` DATETIME NOT NULL,
  `date_modified` DATETIME NOT NULL,
  PRIMARY KEY (`store_take_receipt_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- store_take_receipt_to_product : Relationship for take receipt with product
CREATE TABLE IF NOT EXISTS `oc_store_take_receipt_to_product` ( 
  `store_take_receipt_id` INT(11) NOT NULL,
  `product_id` INT(11) NOT NULL,
  `product_version_id` INT(11) NOT NULL DEFAULT '0',
  `inventory_quantity` INT(11) NOT NULL,
  `actual_quantity` INT(11) NOT NULL,
  `difference_amount` decimal(18,4) NOT NULL,
  `reason`  VARCHAR(256),
  PRIMARY KEY (`store_take_receipt_id`, `product_id`, `product_version_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- cost_adjustment_receipt: table to save cost adjustment receipt
CREATE TABLE IF NOT EXISTS `oc_cost_adjustment_receipt` ( 
  `cost_adjustment_receipt_id` INT NOT NULL AUTO_INCREMENT,
  `cost_adjustment_receipt_code` VARCHAR(255) NULL,
  `store_id` INT(255) NOT NULL,
  `note` TEXT NULL,
  `status` tinyint(2) NULL DEFAULT 0,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`cost_adjustment_receipt_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- cost_adjustment_receipt_to_product: Relationship of cost adjustment receipt with product
CREATE TABLE IF NOT EXISTS `oc_cost_adjustment_receipt_to_product` ( 
  `cost_adjustment_receipt_id` INT NOT NULL,
  `product_id` INT(255) NOT NULL,
  `product_version_id` INT(255) NOT NULL DEFAULT '0',
  `adjustment_cost_price` DECIMAL(18,4) NULL,
  `current_cost_price` DECIMAL(18,4) NULL,
  `note` VARCHAR(255) NULL,
  PRIMARY KEY (`cost_adjustment_receipt_id`, `product_id`, `product_version_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- -----------------------------------------------------------
-- --------- end v2.5.2 Advance Store Management -------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- v2.5.4-tuning ------------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------end v2.5.4-tuning ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- start v2.6.1 Shopee connection -------------------
-- -----------------------------------------------------------

-- shopee_order
CREATE TABLE IF NOT EXISTS `oc_shopee_order` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(128) NOT NULL,
  `cod` TINYINT(1) NOT NULL,
  `currency` VARCHAR(64) NOT NULL,
  `fullname` VARCHAR(256) NULL COMMENT 'Tên người nhận',
  `phone` VARCHAR(32) NOT NULL COMMENT 'Số ',
  `town` VARCHAR(256) NULL,
  `district` VARCHAR(256) NULL,
  `city` VARCHAR(256) NULL,
  `state` VARCHAR(256) NULL,
  `country` VARCHAR(256) NULL,
  `zipcode` VARCHAR(32) NULL,
  `full_address` VARCHAR(512) NULL ,
  `actual_shipping_cost` DECIMAL(18,4) NULL,
  `total_amount` DECIMAL(18,4) NOT NULL,
  `order_status` VARCHAR(128) NOT NULL,
  `shipping_method` VARCHAR(256) NOT NULL COMMENT 'shipping_carrier',
  `payment_method` VARCHAR(256) NOT NULL,
  `payment_status` TINYINT(2) NULL DEFAULT 0,
  `note_for_shop` TEXT NULL COMMENT 'message_to_seller',
  `note` TEXT NULL,
  `create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` DATETIME NULL,
  `shopee_shop_id` BIGINT(64) NOT NULL,
  `bestme_order_id` INT(11) NULL,
  `sync_status` TINYINT(2)  NULL,
  `is_edited` TINYINT(1) NOT NULL DEFAULT 0,
  `sync_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_shopee_order_to_product
CREATE TABLE IF NOT EXISTS `oc_shopee_order_to_product` (
  `shopee_order_code` VARCHAR(128) NOT NULL COMMENT 'order code on shopee',
  `item_id` BIGINT(64) NOT NULL COMMENT '~ product_id ',
  `variation_id` BIGINT(64) NOT NULL COMMENT '~ product_version_id',
  `variation_name` VARCHAR(256) NULL ,
  `quantity` INT(11) NOT NULL COMMENT 'variation_quantity_purchased',
  `original_price` DECIMAL(18,4) NOT NULL COMMENT 'variation_original_price',
  `price` DECIMAL(18,4) NOT NULL COMMENT 'variation_discounted_price'
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_shopee_shop_config
CREATE TABLE IF NOT EXISTS `oc_shopee_shop_config` (
  `shop_id` BIGINT(64) NOT NULL,
  `shop_name` VARCHAR(256) NULL,
  `sync_interval` INT(11) NOT NULL COMMENT '0: không bao giờ',
  `connected` TINYINT(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`shop_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- -----------------------------------------------------------
-- -------- end v2.6.1 Shopee connection ---------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- Fix load demo data -------------------------------
-- -----------------------------------------------------------

-- add column `demo` to oc_blog
ALTER TABLE `oc_blog` ADD `demo` TINYINT(1) NOT NULL DEFAULT '0' AFTER `status`;

-- -----------------------------------------------------------
-- -------- Fix load demo data -------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- start v2.6.3 Lazada connection -------------------
-- -----------------------------------------------------------

-- oc_lazada_category_attributes
CREATE TABLE IF NOT EXISTS `oc_lazada_category_attributes` (
  `lazada_cateogry_id` BIGINT(32) NOT NULL ,
  `data` LONGTEXT NOT NULL ,
  PRIMARY KEY (`lazada_cateogry_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_lazada_shop_config
CREATE TABLE IF NOT EXISTS `oc_lazada_shop_config` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `seller_id` BIGINT(32) NOT NULL ,
  `country` VARCHAR(16) NOT NULL ,
  `shop_name` VARCHAR(256) NOT NULL ,
  `access_token` VARCHAR(256) NOT NULL ,
  `expires_in` DATETIME NOT NULL ,
  `refresh_token` VARCHAR(256) NOT NULL ,
  `refresh_expires_in` DATETIME NOT NULL ,
  `sync_interval` INT(11) NOT NULL DEFAULT '0' ,
  `connected` TINYINT(2) NOT NULL DEFAULT '1' ,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_lazada_product
CREATE TABLE IF NOT EXISTS `oc_lazada_product` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `shop_id` INT(11) NOT NULL ,
  `item_id` VARCHAR(64) NOT NULL ,
  `primary_category` VARCHAR(64) NOT NULL ,
  `name` VARCHAR(256) NOT NULL ,
  `description` VARCHAR(512) NULL ,
  `short_description` VARCHAR(256) NULL ,
  `brand` VARCHAR(128) NULL ,
  `bestme_product_id` INT(11) NULL ,
  `sync_status` INT(11) NULL ,
  `created_at` DATETIME NULL ,
  `updated_at` DATETIME NULL ,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_lazada_product_version
CREATE TABLE IF NOT EXISTS `oc_lazada_product_version` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `item_id` VARCHAR(64) NOT NULL ,
  `version_name` VARCHAR(128) NULL ,
  `status` VARCHAR(26) NULL ,
  `quantity` INT(11) NULL ,
  `product_weight` DECIMAL(15,4) NULL ,
  `images` MEDIUMTEXT NULL COMMENT 'list image' ,
  `seller_sku` VARCHAR(128) NOT NULL ,
  `shop_sku` VARCHAR(128) NULL ,
  `url` VARCHAR(256) NULL ,
  `package_width` DECIMAL(10,2) NULL COMMENT 'centimet' ,
  `package_height` DECIMAL(10,2) NULL COMMENT 'centimet' ,
  `package_length` DECIMAL(10,2) NULL COMMENT 'centimet',
  `package_weight` DECIMAL(10,4) NULL COMMENT 'kilogam' ,
  `price` DECIMAL(15,4) NULL ,
  `available` INT(11) NULL ,
  `sku_id` BIGINT(32) NULL ,
  `bestme_product_id` INT(11) NULL ,
  `bestme_product_version_id` INT(11) NULL ,
  `sync_status` INT(11) NULL ,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_lazada_category_tree
CREATE TABLE IF NOT EXISTS `oc_lazada_category_tree` (
  `id` INT(11) NOT NULL ,
  `parent_id` INT(11) NOT NULL DEFAULT '0' ,
  `name` VARCHAR(256) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_lazada_category_description
CREATE TABLE IF NOT EXISTS `oc_lazada_category_description` (
  `category_id` INT(11) NOT NULL ,
  `name` VARCHAR(256) NOT NULL
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- -----------------------------------------------------------
-- -------- end v2.6.3 Lazada connection ---------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- v2.7.1 Lazada Order ------------------------------
-- -----------------------------------------------------------

-- oc_lazada_order
CREATE TABLE IF NOT EXISTS `oc_lazada_order` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `order_id` BIGINT(64) NOT NULL ,
  `fullname` VARCHAR(256) NULL COMMENT 'address_shipping name' ,
  `phone` VARCHAR(32) NULL COMMENT 'address_shipping phone' ,
  `ward` VARCHAR(256) NULL COMMENT 'address_billing address5' ,
  `district` VARCHAR(256) NULL COMMENT 'address_billing address4' ,
  `province` VARCHAR(256) NULL COMMENT 'address_billing address3' ,
  `full_address` VARCHAR(256) NULL COMMENT 'address_billing address1' ,
  `shipping_fee_original` DECIMAL(18,4) NULL ,
  `shipping_fee` DECIMAL(18,4) NULL ,
  `shipping_method` VARCHAR(256) NULL COMMENT 'price' ,
  `total_amount` DECIMAL(18,4) NULL ,
  `order_status` VARCHAR(64) NULL ,
  `payment_method` VARCHAR(256) NULL ,
  `payment_status` TINYINT(2) NULL ,
  `voucher` DECIMAL(18,4) NULL ,
  `voucher_seller` DECIMAL(18,4) NULL ,
  `remarks` VARCHAR(512) NULL ,
  `create_time` DATETIME NULL ,
  `update_time` DATETIME NULL ,
  `lazada_shop_id` INT(11) NOT NULL ,
  `bestme_order_id` INT(11) NULL ,
  `sync_status` TINYINT(2) NULL ,
  `is_edited` TINYINT(2) NULL ,
  `sync_time` DATETIME NULL ,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_lazada_order_to_product
CREATE TABLE IF NOT EXISTS `oc_lazada_order_to_product` (
  `lazada_order_id` BIGINT(64) NOT NULL ,
  `sku` VARCHAR(128) NULL ,
  `shop_sku` VARCHAR(128) NULL ,
  `variation` VARCHAR(256) NULL ,
  `name` VARCHAR(256) NULL ,
  `quantity` INT(11) NOT NULL DEFAULT 0,
  `item_price` DECIMAL(18,4) NULL ,
  `paid_price` DECIMAL(18,4) NULL
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- -----------------------------------------------------------
-- -------- end - v2.7.1 Lazada Order ------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- v2.7.2 Tuning ------------------------------------
-- -----------------------------------------------------------

-- alt blog image
ALTER TABLE `oc_blog_description`
  ADD `alt` TEXT NULL AFTER `alias`;

-- -----------------------------------------------------------
-- -------- end - v2.7.2 Tunging -----------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- v2.7.3 New Customer Design -----------------------
-- -----------------------------------------------------------

ALTER TABLE `oc_customer`
  ADD `sex` TINYINT(2) NULL COMMENT '1: Nam, 2: Nữ, 3: Khác' AFTER `date_added`,
  ADD `birthday` DATE NULL AFTER `sex`,
  ADD `full_name` VARCHAR(255) NOT NULL AFTER `birthday`,
  ADD `website` VARCHAR(255) NULL AFTER `full_name`,
  ADD `tax_code` VARCHAR(100) NULL AFTER `website`,
  ADD `staff_in_charge` INT(11) NULL AFTER `tax_code`;

ALTER TABLE `oc_customer`
  CHANGE `firstname` `firstname` VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  CHANGE `lastname` `lastname` VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;

ALTER TABLE `oc_customer_group`
  ADD `customer_group_code` VARCHAR(50) NULL AFTER `sort_order`;

-- -----------------------------------------------------------
-- -------- end v2.7.3 New Customer Design -------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------------- start v2.7.4 Cash Flow --------------------
-- -----------------------------------------------------------

-- oc_receipt_voucher
CREATE TABLE IF NOT EXISTS `oc_receipt_voucher` (
    `receipt_voucher_id` INT(11) NOT NULL AUTO_INCREMENT,
    `receipt_voucher_code` VARCHAR(50) NOT NULL,
    `receipt_voucher_type_id` INT(11) NOT NULL,
    `order_id` INT(11) NULL DEFAULT NULL,
    `status` INT(11) NOT NULL,
    `in_business_report_status` TINYINT(4) DEFAULT 1,
    `amount` decimal(18,4) NOT NULL DEFAULT '0.0000',
    `store_id` INT(11) NOT NULL,
    `cash_flow_method_id` INT(11) NOT NULL,
    `cash_flow_object_id` INT(11) NULL DEFAULT NULL,
    `object_info` TEXT NULL DEFAULT NULL,
    `receipt_type_other` VARCHAR (255) NULL DEFAULT NULL,
    `note` TEXT NULL DEFAULT NULL,
    `date_added` DATETIME NOT NULL,
    `date_modified` DATETIME NOT NULL,
    PRIMARY KEY (`receipt_voucher_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_payment_voucher
CREATE TABLE IF NOT EXISTS `oc_payment_voucher` (
    `payment_voucher_id` INT(11) NOT NULL AUTO_INCREMENT,
    `payment_voucher_type_id` INT(11) NOT NULL,
    `payment_voucher_code` VARCHAR(50) NOT NULL,
    `store_receipt_id` INT(11) NULL DEFAULT NULL,
    `status` INT(11) NOT NULL,
    `in_business_report_status` TINYINT(4) DEFAULT 1,
    `amount` decimal(18,4) NOT NULL DEFAULT '0.0000',
    `store_id` INT(11) NOT NULL,
    `cash_flow_method_id` INT(11) NOT NULL,
    `cash_flow_object_id` INT(11) NULL DEFAULT NULL,
    `object_info` TEXT NULL DEFAULT NULL,
    `payment_type_other` VARCHAR (255) NULL DEFAULT NULL,
    `note` TEXT NULL DEFAULT NULL,
    `date_added` DATETIME NOT NULL,
    `date_modified` DATETIME NOT NULL,
    PRIMARY KEY (`payment_voucher_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_receipt_voucher_type
CREATE TABLE IF NOT EXISTS `oc_receipt_voucher_type` (
    `receipt_voucher_type_id` INT(11) NOT NULL,
    `language_id` TINYINT(4) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (`receipt_voucher_type_id`, `language_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_payment_voucher_type
CREATE TABLE IF NOT EXISTS `oc_payment_voucher_type` (
    `payment_voucher_type_id` INT(11) NOT NULL,
    `language_id` TINYINT(4) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (`payment_voucher_type_id`, `language_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_cash_flow_method
CREATE TABLE IF NOT EXISTS `oc_cash_flow_method` (
    `cash_flow_method_id` INT(11) NOT NULL,
    `language_id` TINYINT(4) NOT NULL,
    `name` VARCHAR (255) NOT NULL,
    `description` VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (`cash_flow_method_id`, `language_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_cash_flow_object
CREATE TABLE IF NOT EXISTS `oc_cash_flow_object` (
    `cash_flow_object_id` INT(11) NOT NULL,
    `language_id` TINYINT(4) NOT NULL,
    `name` VARCHAR (255) NOT NULL,
    `description` VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (`cash_flow_object_id`, `language_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- Thêm dữ liệu cho các bảng: oc_cash_flow_method, oc_cash_flow_object, oc_receipt_voucher_type, oc_payment_voucher_type
INSERT INTO `oc_cash_flow_method` (`cash_flow_method_id`, `language_id`, `name`, `description`) VALUES
(1, 2, 'Tiền mặt', NULL),
(1, 1, 'Cash', NULL),
(2, 2, 'Chuyển khoản', NULL),
(2, 1, 'Transfer', NULL),
(3, 2, 'COD', NULL),
(3, 1, 'COD', NULL),
(4, 2, 'Quẹt thẻ', NULL),
(4, 1, 'Card', NULL);

INSERT INTO `oc_cash_flow_object` (`cash_flow_object_id`, `language_id`, `name`, `description`) VALUES
(1, 2, 'Khách hàng', NULL),
(1, 1, 'Customer', NULL),
(2, 2, 'Nhân viên', NULL),
(2, 1, 'Staff', NULL),
(3, 2, 'Nhà cung cấp', NULL),
(3, 1, 'Supplier', NULL),
(4, 2, 'Đối tác vận chuyển', NULL),
(4, 1, 'Shipping partner', NULL),
(5, 2, 'Khác', NULL),
(5, 1, 'Other', NULL);

INSERT INTO `oc_receipt_voucher_type` (`receipt_voucher_type_id`, `language_id`, `name`, `description`) VALUES
(1, 2, 'Đối tác vận chuyển đặt cọc', NULL),
(1, 1, 'Shipping partner makes a deposit', NULL),
(2, 2, 'Thu nợ đối tác vận chuyển', NULL),
(2, 1, 'Collecting debt from shipping partner', NULL),
(3, 2, 'Thu nhập khác', NULL),
(3, 1, 'Other income', NULL),
(4, 2, 'Tiền thưởng', NULL),
(4, 1, 'Bonus/Reward', NULL),
(5, 2, 'Tiền bồi thường', NULL),
(5, 1, 'Compensation', NULL),
(6, 2, 'Cho thuê, thanh lý tài sản', NULL),
(6, 1, 'Rent and liquidation property', NULL),
(7, 2, 'Thu nợ khách hàng', NULL),
(7, 1, 'Collecting debt from customer', NULL),
(8, 2, 'Thu tự động', NULL),
(8, 1, 'Automation receipt', NULL),
(9, 2, 'Khác', NULL),
(9, 1, 'Other', NULL);

INSERT INTO `oc_payment_voucher_type` (`payment_voucher_type_id`, `language_id`, `name`, `description`) VALUES
(1, 2, 'Trả nợ đối tác vận chuyển', NULL),
(1, 1, 'Paying debt for shipping partner', NULL),
(2, 2, 'Chi phí sản xuất', NULL),
(2, 1, 'Production cost', NULL),
(3, 2, 'Chi phí nguyên - vật liệu', NULL),
(3, 1, 'Raw materials cost', NULL),
(4, 2, 'Chi phí sinh hoạt', NULL),
(4, 1, 'Living cost', NULL),
(5, 2, 'Chi phí nhân công', NULL),
(5, 1, 'Labor cost', NULL),
(6, 2, 'Chi phí bán hàng', NULL),
(6, 1, 'Selling cost', NULL),
(7, 2, 'Chi phí quản lý cửa hàng', NULL),
(7, 1, 'Store management cost', NULL),
(8, 2, 'Chi tự động', NULL),
(8, 1, 'Automation Payment', NULL),
(9, 2, 'Chi phí khác', NULL),
(9, 1, 'Other cost', NULL);

-- -----------------------------------------------------------
-- ---------------- end v2.7.4 Cash Flow ---------------------
-- -----------------------------------------------------------


-- -----------------------------------------------------------
-- ---- HOT FIX change type fields on table lazada_product ---
-- -----------------------------------------------------------

ALTER TABLE `oc_lazada_product` CHANGE
             `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
             CHANGE `short_description` `short_description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

-- -----------------------------------------------------------
-- -- end HOT FIX change type fields on table lazada_product--
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- start activity log analytics ---------------------
-- -----------------------------------------------------------

-- Insert event to tracking in table oc_event
INSERT INTO `oc_event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES
('tracking_order_complete', 'admin/model/sale/order/updateStatus/after', 'event/activity_tracking/trackingOrderComplete', 1, 0),
('tracking_login', 'admin/model/user/user/editLastLoggedIn/after', 'event/activity_tracking/trackingLogin', 1, 0),
('tracking_product_category', 'admin/model/catalog/store_take_receipt/addStoreTakeReceipt/after', 'event/activity_tracking/trackingStoreTakeReceipt', 1, 0),
('tracking_new_product', 'admin/model/catalog/product/addProduct/after', 'event/activity_tracking/trackingNewProduct', 1, 0),
('tracking_transfer_order_status', 'admin/model/sale/order_history/addHistory/after', 'event/activity_tracking/trackingTransferOrderStatus', 1, 0),
('tracking_new_product_category', 'admin/model/catalog/category/addCategory/after', 'event/activity_tracking/trackingNewProductCategory', 1, 0),
('tracking_new_product_category', 'admin/model/catalog/category/addCategoryFast/after', 'event/activity_tracking/trackingNewProductCategory', 1, 0),
('tracking_delete_product_category', 'admin/model/catalog/category/deleteCategory/after', 'event/activity_tracking/trackingDeleteProductCategory', 1, 0),
('tracking_product_price', 'admin/model/catalog/product/addProduct/after', 'event/activity_tracking/trackingProductPrice', 1, 0),
('tracking_product_price', 'admin/model/catalog/product/editProduct/after', 'event/activity_tracking/trackingProductPrice', 1, 0),
('tracking_new_store_transfer_receipt', 'admin/model/catalog/store_transfer_receipt/addStoreTransferReceipt/after', 'event/activity_tracking/trackingStoreTransferReceipt', 1, 0),
('tracking_new_store_receipt', 'admin/model/catalog/store_receipt/addStoreReceipt/after', 'event/activity_tracking/trackingStoreReceipt', 1, 0),
('tracking_new_cost_adjustment_receipt', 'admin/model/catalog/cost_adjustment_receipt/addReceipt/after', 'event/activity_tracking/trackingCostAdjustmentReceipt', 1, 0),
('tracking_new_discount', 'admin/model/discount/discount/addDiscount/after', 'event/activity_tracking/trackingCountDiscount', 1, 0),
('tracking_new_order_catalog', 'catalog/model/sale/order/addOrderCommon/after', 'event/activity_tracking/trackingCountOrder', 1, 0),
('tracking_new_order_admin', 'admin/model/sale/order/addOrder/after', 'event/activity_tracking/trackingCountOrder', 1, 0),
('tracking_new_shopee', 'admin/model/catalog/shopee/addShopeeShop/after', 'event/activity_tracking/trackingCountShopee', 1, 0),
('tracking_new_shopee_product', 'admin/model/catalog/product/addProduct/after', 'event/activity_tracking/trackingCountShopeeProduct', 1, 0),
('tracking_pos_order_complete', 'catalog/model/sale/order/addOrderCommon/after', 'event/activity_tracking/trackingOrderComplete', 1, 0);

-- -----------------------------------------------------------
-- -------- end activity log analytics -----------------------
-- -----------------------------------------------------------


-- -----------------------------------------------------------
-- ---------- start v2.8.1 Shopee advance product ------------
-- -----------------------------------------------------------

-- IMPORTANT: all shopee tables SHOULD in model/sale_channel/shopee on add/remove from left menu!!!

-- oc_shopee_product_version
CREATE TABLE IF NOT EXISTS `oc_shopee_product_version` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `item_id` VARCHAR(64) NOT NULL ,
  `variation_id` BIGINT(64) NOT NULL ,
  `version_name` VARCHAR(128) NULL ,
  `reserved_stock` INT(11) NULL DEFAULT '0' ,
  `product_weight` DECIMAL(15,4) NULL ,
  `original_price` DECIMAL(15,4) NULL ,
  `price` DECIMAL(15,4) NULL ,
  `discount_id` INT NULL DEFAULT '0' ,
  `create_time` VARCHAR(64) NULL DEFAULT NULL ,
  `update_time` VARCHAR(64) NULL DEFAULT NULL ,
  `sku` VARCHAR(64) NULL ,
  `stock` INT(11) NULL DEFAULT '0' ,
  `is_set_item` BOOLEAN NULL ,
  `bestme_product_id` INT(11) NULL ,
  `bestme_product_version_id` INT(11) NULL ,
  `sync_status` INT(11) NULL ,
  `status` VARCHAR(32) NULL ,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- IMPORTANT: ALWAYS create table "shopee_product" because of below alter table sql "ALTER TABLE `oc_shopee_product`..."
CREATE TABLE IF NOT EXISTS `oc_shopee_product` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `item_id` BIGINT(64) NOT NULL ,
  `shopid` BIGINT(64) NOT NULL ,
  `sku` VARCHAR(64) NULL ,
  `stock` INT(32) NULL ,
  `status` VARCHAR(32) NULL ,
  `name` VARCHAR(255) CHARACTER SET utf8 NOT NULL ,
  `description` TEXT CHARACTER SET utf8 NOT NULL ,
  `images` TEXT NOT NULL ,
  `currency` VARCHAR(32) NOT NULL ,
  `price` FLOAT(15,4) NOT NULL ,
  `original_price` FLOAT(15,4) NOT NULL ,
  `weight` FLOAT(15,4) NOT NULL ,
  `category_id` BIGINT(64) NOT NULL ,
  `has_variation` BOOLEAN NOT NULL DEFAULT FALSE ,
  `variations` TEXT CHARACTER SET utf8 NULL ,
  `attributes` TEXT CHARACTER SET utf8 NULL ,
  `bestme_id` INT(11) NULL ,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- more columns to "oc_shopee_product"
ALTER TABLE `oc_shopee_product`
    ADD `sync_status` INT(11) NULL AFTER `updated_at`,
    ADD `bestme_product_version_id` INT(11) NULL AFTER `sync_status`;

-- -----------------------------------------------------------
-- ----------- end v2.8.1 Shopee advance product -------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------ start v2.8.2 Transport Status Management -----------
-- -----------------------------------------------------------

ALTER TABLE `oc_order` ADD `manual_update_status` TINYINT NOT NULL DEFAULT '0' AFTER `date_modified`;

-- -----------------------------------------------------------
-- ------ end v2.8.2 Transport Status Management -------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ---------------- start v2.8.3 tuning ----------------------
-- -----------------------------------------------------------

-- temp not use FULLTEXT
-- ALTER TABLE `oc_product_description`
--   ADD FULLTEXT(`name`);

ALTER TABLE `oc_blog_description`
  ADD `type` VARCHAR (255) NOT NULL DEFAULT 'image' AFTER `alt`,
  ADD `video_url` TEXT NULL AFTER `type`;

-- -----------------------------------------------------------
-- ------------------ end v2.8.3 tuning ----------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------------ start v2.8.4 Pos return goods ----------------
-- -----------------------------------------------------------

-- oc_return_receipt
CREATE TABLE IF NOT EXISTS `oc_return_receipt` (
  `return_receipt_id` INT(11) NOT NULL AUTO_INCREMENT,
  `order_id` INT(11) NOT NULL,
  `return_receipt_code` VARCHAR(50) NOT NULL,
  `return_fee` DECIMAL(18,4) NULL DEFAULT NULL,
  `total` DECIMAL(18,4) NULL DEFAULT NULL,
  `note` VARCHAR(255) NULL DEFAULT NULL,
  `date_added` DATETIME NOT NULL,
  PRIMARY KEY (`return_receipt_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_return_product
CREATE TABLE IF NOT EXISTS `oc_return_product` (
  `return_product_id` INT(11) NOT NULL AUTO_INCREMENT,
  `return_receipt_id` INT(11) NOT NULL,
  `product_id` INT(11) NOT NULL,
  `product_version_id` INT(11) NULL DEFAULT '0',
  `price` DECIMAL(18,4) NULL DEFAULT NULL,
  `quantity` INT(4) NOT NULL,
  PRIMARY KEY (`return_product_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- add column return_receipt_id to table oc_payment_voucher
ALTER TABLE `oc_payment_voucher`
  ADD COLUMN `return_receipt_id` INT(11) NULL DEFAULT NULL AFTER `store_receipt_id`;

-- -----------------------------------------------------------
-- ------------- end v2.8.4 Pos return goods -----------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ---------------- start v2.9.1 Bestme package --------------
-- -----------------------------------------------------------

-- update more detail of current paid packet in oc_setting
INSERT INTO `oc_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
('0','config','config_packet_capacity','',0),
('0','config','config_packet_number_of_stores','',0),
('0','config','config_packet_number_of_staffs','',0),
('0','config','config_packet_unlimited_capacity','1',0),
('0','config','config_packet_unlimited_stores','1',0),
('0','config','config_packet_unlimited_staffs','1',0);

-- add size to table oc_images
ALTER TABLE `oc_images` ADD `size` FLOAT (5)  NOT NULL DEFAULT 0 AFTER `source_id`;

-- -----------------------------------------------------------
-- ---------------- end v2.9.1 Bestme package ----------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ---------------- start v2.10.1 image folder --------------
-- -----------------------------------------------------------

CREATE TABLE IF NOT EXISTS `oc_image_folder` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `oc_images`
    ADD COLUMN `folder_path` INT(11) NOT NULL DEFAULT 0 AFTER `size`;

-- -----------------------------------------------------------
-- ---------------- end v2.10.1 image folder --------------

-- -----------------------------------------------------------
-- ---------------- start v2.9.2 Advance Report --------------

ALTER TABLE `oc_report_product`
    ADD `cost_price` DECIMAL(18,4) NULL DEFAULT '0.0000' AFTER `total_amount`,
    ADD `discount` DECIMAL(18,4) NOT NULL DEFAULT '0.0000' AFTER `quantity`,
    ADD `return_amount` DECIMAL(18,4) NULL DEFAULT '0.0000' AFTER `discount`,
    ADD `quantity_return` INT(11) NULL DEFAULT '0' AFTER `return_amount`;

ALTER TABLE `oc_report_order`
    ADD `shipping_fee`DECIMAL(18,4) NOT NULL DEFAULT '0.0000' AFTER `total_amount`,
    ADD `discount` DECIMAL(18,4) NOT NULL DEFAULT '0.0000' AFTER `shipping_fee`,
    ADD `source` VARCHAR(50) NULL DEFAULT NULL AFTER `order_status`,
    ADD `total_return` DECIMAL(18,4) NOT NULL DEFAULT '0.0000' AFTER `total_amount`;

-- rename value 'Chi phí quản lý cửa hàng'
UPDATE `oc_payment_voucher_type` SET `name` = 'Chi phí thuế TNDN'
    WHERE `payment_voucher_type_id` = 7
    AND `language_id` = 2;

-- rename value 'Store management cost'
UPDATE `oc_payment_voucher_type` SET `name` = 'Enterprise income tax expenses'
    WHERE `payment_voucher_type_id` = 7
    AND `language_id` = 1;

INSERT INTO `oc_event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES
   ('update_report_when_create_return_receipt', 'admin/model/sale/return_receipt/addReturnReceipt/after', 'event/report_order/afterCreateReturnReceipt', 1, 0);
-- -----------------------------------------------------------
-- ---------------- end v2.9.2 Advance Report ----------------

-- -----------------------------------------------------------
-- ----------- start v2.9.2-advance-report-xon-3448 ----------

ALTER TABLE `oc_return_receipt`
  ADD `store_id` INT(11) NULL AFTER `note`;

-- -----------------------------------------------------------
-- ----------- end v2.9.2-advance-report-xon-3448 ------------

-- -----------------------------------------------------------
-- ------------ start v2.10.2 Tuning ---------------------------
-- -----------------------------------------------------------

-- -- Custom filters by product attributes
-- oc_attribute_filter
CREATE TABLE IF NOT EXISTS `oc_attribute_filter` (
    `attribute_filter_id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` TEXT NOT NULL COLLATE 'utf8_general_ci',
    `value` TEXT NOT NULL COLLATE 'utf8_general_ci',
    `status` TINYINT(1) NOT NULL DEFAULT 0,
    `language_id` INT(11) NOT NULL,
    PRIMARY KEY (`attribute_filter_id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

-- oc_product_attribute_filter
CREATE TABLE IF NOT EXISTS `oc_product_attribute_filter` (
    `product_id` INT(11) NOT NULL,
    `attribute_filter_id` INT(11) NOT NULL,
    `attribute_filter_value` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',
    PRIMARY KEY (`product_id`, `attribute_filter_id`, `attribute_filter_value`) USING BTREE
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

-- Alter oc_blog_description add seo_keywords
ALTER TABLE `oc_blog_description`
    ADD `seo_keywords` VARCHAR(500) NULL DEFAULT NULL AFTER `meta_description`;

-- -----------------------------------------------------------
-- ------------ end v2.10.2 Tuning ---------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ---------------- start v2.10.3 - shopee-upload-product ----
-- -----------------------------------------------------------

-- insert column version in talbe oc_migration
ALTER TABLE `oc_migration`
  ADD `version` INT(11) DEFAULT 1 NULL AFTER `date_run`;

-- table cache data for Shopee's categories
CREATE TABLE IF NOT EXISTS `oc_shopee_category` (
  `shopee_category_id` BIGINT(64) NOT NULL AUTO_INCREMENT ,
  `parent_id` BIGINT(64) NOT NULL ,
  `name_vi` VARCHAR(255) NULL ,
  `name_en` VARCHAR(255) NULL ,
  `has_children` BOOLEAN NOT NULL ,
  PRIMARY KEY (`shopee_category_id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- table cache data for Shopee's logistics
CREATE TABLE IF NOT EXISTS `oc_shopee_logistics` (
  `logistic_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `logistic_name` VARCHAR(255) NULL ,
  `enabled` BOOLEAN NULL ,
  `fee_type` VARCHAR(255) NULL ,
  `sizes` TEXT NULL ,
  `item_max_weight` DECIMAL(15,8) NULL ,
  `item_min_weight` DECIMAL(15,8) NULL ,
  `height` DECIMAL(15,8) NULL ,
  `width` DECIMAL(15,8) NULL ,
  `length` DECIMAL(15,8) NULL ,
  `volumetric_factor` INT(11) NULL ,
  PRIMARY KEY (`logistic_id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- table `oc_shopee_upload_product`
CREATE TABLE IF NOT EXISTS `oc_shopee_upload_product` (
   `shopee_upload_product_id` INT(11) NOT NULL AUTO_INCREMENT ,
   `bestme_product_id` INT(11) NULL ,
  `name` VARCHAR(255) NOT NULL ,
  `description` TEXT NOT NULL ,
  `category_id` BIGINT(64) NULL ,
  `version_attribute` TEXT NULL,
  `image` VARCHAR(255) NULL ,
  `price` DECIMAL(18,4) NOT NULL ,
  `stock` INT(11) NOT NULL ,
  `sku` VARCHAR(255) NULL ,
  `common_price` DECIMAL(18,4)  NULL ,
  `common_stock` INT(11) NULL ,
  `common_sku` VARCHAR(255)  NULL ,
  `weight` INT(11) NULL ,
  `length` INT(11) NULL ,
  `width` INT(11) NULL ,
  `height` INT(11) NULL ,
  `date_added` TIMESTAMP NULL ,
  `date_modify` TIMESTAMP NULL ,
  PRIMARY KEY (`shopee_upload_product_id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- table `oc_shopee_upload_product_version`
CREATE TABLE IF NOT EXISTS `oc_shopee_upload_product_version` (
  `shopee_upload_product_version_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `shopee_upload_product_id` INT(11) NOT NULL ,
  `version_name` VARCHAR(255) NOT NULL ,
  `version_price` DECIMAL(18,4) NOT NULL,
  `version_stock` INT(11) NOT NULL ,
  `version_sku` VARCHAR(255) NULL ,
  `variation_id` BIGINT(32) NULL ,
  PRIMARY KEY (`shopee_upload_product_version_id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- table `oc_shopee_upload_product_image`
CREATE TABLE IF NOT EXISTS `oc_shopee_upload_product_image` (
  `shopee_upload_product_image_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `product_id` INT(11) NOT NULL ,
  `image` VARCHAR(255) NULL ,
  `sort_order` INT(11) NULL ,
  PRIMARY KEY (`shopee_upload_product_image_id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- table `oc_shopee_upload_product_attribute`
CREATE TABLE IF NOT EXISTS `oc_shopee_upload_product_attribute` (
  `shopee_upload_product_attribute_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `product_id` INT(11) NOT NULL ,
  `attribute_id` INT(11) NOT NULL ,
  `value` VARCHAR(255) NULL ,
  PRIMARY KEY (`shopee_upload_product_attribute_id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- table `oc_shopee_upload_product_logistic`
CREATE TABLE IF NOT EXISTS `oc_shopee_upload_product_logistic` (
  `shopee_upload_product_logistic_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `logistic_id` INT(11) NOT NULL ,
  `product_id` INT(11) NOT NULL ,
  `enabled` BOOLEAN NOT NULL ,
  PRIMARY KEY (`shopee_upload_product_logistic_id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- table `oc_shopee_upload_product_shop_update_result`
CREATE TABLE IF NOT EXISTS `oc_shopee_upload_product_shop_update_result` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `shopee_upload_product_id` INT(11) NULL ,
  `shopee_shop_id` BIGINT(64) NULL ,
  `shopee_item_id` BIGINT(64) NULL ,
  `status` TINYINT(4) NULL ,
  `in_process` TINYINT(2) NULL DEFAULT '0' ,
  `error_code` VARCHAR(255) NULL ,
  `error_msg` VARCHAR(255) NULL ,
  `variation_result` TEXT NULL ,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- -----------------------------------------------------------
-- ---------------- end v2.10.3-shopee-upload-product ------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------------ start v2.11.2 Tuning -------------------------
-- -----------------------------------------------------------

-- Config product version images
-- product_version
ALTER TABLE `oc_product_version`
    ADD COLUMN `image` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci' AFTER `deleted`,
    ADD COLUMN `image_alt` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci' AFTER `image`;

-- add table oc_product_description_tab
CREATE TABLE `oc_product_description_tab` (
      `product_description_tab_id` int(11) NOT NULL AUTO_INCREMENT,
      `product_id` int(11) NOT NULL,
      `title` varchar(255) COLLATE utf8_general_ci NOT NULL,
      `description` text COLLATE utf8_general_ci NOT NULL,
      PRIMARY KEY (`product_description_tab_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- add table oc_rate_website
CREATE TABLE IF NOT EXISTS `oc_rate_website` (
    `rate_id` int(11) NOT NULL AUTO_INCREMENT,
    `status` tinyint(4) NOT NULL,
    `customer_name` varchar(255) NOT NULL,
    `sub_info` text,
    `content` text,
    `image` varchar(255) DEFAULT NULL,
    `alt` varchar(255) DEFAULT NULL,
    `date_added` datetime NOT NULL,
    `date_modified` datetime NOT NULL,
    PRIMARY KEY (`rate_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- add new column to oc_shopee_shop_config
ALTER TABLE `oc_shopee_shop_config`
  ADD `order_sync_range` INT(11) NULL DEFAULT '15' AFTER `connected`,
  ADD `allow_update_stock_product` TINYINT(2) NULL DEFAULT '0' AFTER `order_sync_range`,
  ADD `allow_update_price_product` TINYINT(2) NULL DEFAULT '0' AFTER `allow_update_stock_product`,
  ADD `last_sync_time_product` TIMESTAMP NULL AFTER `allow_update_price_product`,
  ADD `last_sync_time_order` TIMESTAMP NULL AFTER `last_sync_time_product`;

-- add new column to oc_lazada_shop_config
ALTER TABLE `oc_lazada_shop_config`
  ADD `order_sync_range` INT(11) NULL DEFAULT '15' AFTER `connected`,
  ADD `allow_update_stock_product` TINYINT(2) NULL DEFAULT '0' AFTER `order_sync_range`,
  ADD `allow_update_price_product` TINYINT(2) NULL DEFAULT '0' AFTER `allow_update_stock_product`,
  ADD `last_sync_time_product` TIMESTAMP NULL AFTER `allow_update_price_product`,
  ADD `last_sync_time_order` TIMESTAMP NULL AFTER `last_sync_time_product`;

-- -----------------------------------------------------------
-- ------------ end v2.11.2 Tuning ---------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------------ start v3.1.3 Generate demo data Website ------
-- -----------------------------------------------------------

INSERT INTO `oc_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'config', 'config_use_demo_data', '1', 0),
(0, 'online_store_config', 'online_store_config_maxlead_code', '', 0);

-- -----------------------------------------------------------
-- ---------------- end v3.1.3 Generate demo data Website ----
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------------ start v3.3.1 Advance Staff Permission ------
-- -----------------------------------------------------------

-- Add table user_store
CREATE TABLE IF NOT EXISTS `oc_user_store` (
        `id` INT(11) NOT NULL AUTO_INCREMENT ,
        `user_id` INT(11) NOT NULL ,
        `store_id` VARCHAR(255) NULL ,
        PRIMARY KEY (`id`)) ENGINE = MyISAM
        CHARSET=utf8 COLLATE utf8_general_ci;

-- Add access_all column in user_group table
ALTER TABLE `oc_user_group`
	ADD COLUMN `access_all` TINYINT NULL DEFAULT '1' AFTER `permission`;

-- Add user_create_id for multi table
ALTER TABLE `oc_customer`
	ADD COLUMN `user_create_id` INT(11) NULL DEFAULT '1' AFTER `staff_in_charge`;

ALTER TABLE `oc_customer_group`
	ADD COLUMN `user_create_id` INT(11) NULL DEFAULT '1' AFTER `customer_group_code`;

ALTER TABLE `oc_manufacturer`
	ADD COLUMN `user_create_id` INT(11) NULL DEFAULT '1' AFTER `status`;

ALTER TABLE `oc_collection`
	ADD COLUMN `user_create_id` INT(11) NULL DEFAULT '1' AFTER `type`;

ALTER TABLE `oc_product`
	ADD COLUMN `user_create_id` INT(11) NULL DEFAULT '1' AFTER `default_store_id`;

ALTER TABLE `oc_return_receipt`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `note`;

ALTER TABLE `oc_payment_voucher`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `note`;

ALTER TABLE `oc_receipt_voucher`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `note`;

ALTER TABLE `oc_store_receipt`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `status`;

ALTER TABLE `oc_store_transfer_receipt`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `total`;

ALTER TABLE `oc_store_take_receipt`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `difference_amount`;

ALTER TABLE `oc_cost_adjustment_receipt`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `note`;

ALTER TABLE `oc_discount`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `end_at`;


-- -----------------------------------------------------------
-- ------------ end v3.3.1 Advance Staff Permission ------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------------ start v3.4.2 Sync bestme social --------------
-- -----------------------------------------------------------

ALTER TABLE `oc_customer` ADD `is_authenticated` TINYINT(1) NOT NULL DEFAULT '1' AFTER `customer_source`;

ALTER TABLE `oc_customer` CHANGE `customer_source` `customer_source` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '1: NovaonX Social, 2: Facebook, 3: Bestme';

ALTER TABLE `oc_customer` ADD `subscriber_id` VARCHAR(255) NULL AFTER `user_create_id`;

CREATE TABLE `oc_order_channel` (
  `order_id` INT(11) NOT NULL ,
  `channel_id` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`order_id`, `channel_id`)
) ENGINE = MyISAM;

CREATE TABLE `oc_order_campaign` (
  `order_id` INT(11) NOT NULL ,
  `campaign_id` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`order_id`, `campaign_id`)
) ENGINE = MyISAM;

CREATE TABLE `oc_campaign_voucher` (
  `campaign_voucher_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `voucher_code` VARCHAR(255) NULL DEFAULT NULL ,
  `voucher_type` INT(11) NULL COMMENT '1: percent. 2: amount' ,
  `amount` DECIMAL(18,4) NULL DEFAULT 0.000 ,
  `user_create_id` INT(11) NULL ,
  `start_at` TIMESTAMP NULL DEFAULT NULL ,
  `end_at` TIMESTAMP NULL DEFAULT NULL ,
  `date_added` TIMESTAMP NULL DEFAULT NULL ,
  `date_modified` TIMESTAMP NULL DEFAULT NULL ,
  PRIMARY KEY (`campaign_voucher_id`)
) ENGINE = MyISAM;

ALTER TABLE `oc_order_product` ADD `campaign_voucher_id` INT(11) NULL DEFAULT NULL AFTER `tax`;

ALTER TABLE `oc_campaign_voucher` ADD `order_id` INT(11) NULL DEFAULT NULL AFTER `campaign_voucher_id`;

-- -----------------------------------------------------------
-- ------------ end v3.4.2 Sync bestme social ----------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------------ start XOCF-192 -------------------------------
-- -----------------------------------------------------------

ALTER TABLE `oc_order` ADD `collection_amount` DECIMAL(18,4) NULL DEFAULT NULL AFTER `total`;

UPDATE `oc_order` SET `collection_amount` = `total` WHERE `collection_amount` IS NULL;

-- -----------------------------------------------------------
-- ------------ end XOCF-192 ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------------ start v3.5.1 Open Api ------------------------
-- -----------------------------------------------------------

-- create table `oc_keyword_sync`
CREATE TABLE `oc_keyword_sync` (
  `keyword_id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` text COLLATE utf8_unicode_ci NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (keyword_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- add `source` column to tables: `product`, `manufacturer`, `blog`, `blog_category`, `page_contents`, `discount`, `category`
ALTER TABLE `oc_product` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `user_create_id`;
ALTER TABLE `oc_manufacturer` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `user_create_id`;
ALTER TABLE `oc_blog` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `demo`;
ALTER TABLE `oc_blog_category` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `status`;
ALTER TABLE `oc_page_contents` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `seo_description`;
ALTER TABLE `oc_discount` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `updated_at`;
ALTER TABLE `oc_category` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `status`;

-- add `source_author_name` column to `blog_description` table
ALTER TABLE `oc_blog_description` ADD `source_author_name` VARCHAR(255) NULL DEFAULT NULL AFTER `video_url`;

-- -----------------------------------------------------------
-- ------------ end v3.5.1 Open Api ------------------------
-- -----------------------------------------------------------

-- -------------- start XOCF-264 -------------------------------
CREATE TABLE `oc_email_subscribers` (
    `email_id` int(11) NOT NULL AUTO_INCREMENT,
    `email` text COLLATE utf8_unicode_ci NOT NULL,
    `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (email_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
-- -------------- end XOCF-264 -------------------------------

-- -----------------v3.6.2-agency-brands----------------------
ALTER TABLE `oc_product` ADD `brand_shop_name` VARCHAR(255) NULL DEFAULT NULL AFTER `source`;
-- -----------------End v3.6.2-agency-brands------------------

--
-- TODO: Migrate by NOVAON from here...
--

-- -----------------------------------------------------------
-- --------Always at the end of file -------------------------
-- -----------------------------------------------------------

-- --------Load theme default data ---------------------------
-- TODO: call in cli_install?...

--
-- v1.5.2 Smart loading demo data base on theme group (theme type)
-- Note: following content is copied from file "library/theme_config/demo_data/tech.sql" for default
--
-- 2020/04: More support loading demo data from special theme. If no special theme defined, use theme group as above
-- Note: following content may be copied from file "library/theme_config/demo_data/special_shop/xxx.sql" if special file defined

-- 2020/07/30: change theme demo to adg_topal_xfec.
-- Note: following content is copied from file "library/theme_config/demo_data/special_shop/adg_topal_xfec.sql"
--

-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `source`, `date_added`, `date_modified`) VALUES
(32, NULL, 0, 0, 0, 0, 1, NULL, '2021-11-08 17:20:34', '2021-11-08 17:20:34');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(32, 2, 'demo', 'demo', '', '', ''),
(32, 1, 'demo', 'demo', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(32, 32, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
 (32, 0);

-- oc_collection PRIMARY KEY (`collection_id`)
-- INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES


-- oc_collection_description PRIMARY KEY (`collection_id`)
-- INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES

-- oc_product PRIMARY KEY (`product_id`)
-- delete first
INSERT IGNORE INTO `oc_product` (`product_id`, `model`, `sku`, `common_barcode`, `common_sku`, `common_compare_price`, `common_price`, `common_cost_price`, `barcode`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `sale_on_out_of_stock`, `stock_status_id`, `image`, `image_alt`, `multi_versions`, `manufacturer_id`, `shipping`, `price`, `price_currency_id`, `compare_price`, `c_price_currency_id`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `channel`, `viewed`, `demo`, `deleted`, `default_store_id`, `user_create_id`, `source`, `brand_shop_name`, `date_added`, `date_modified`) VALUES
(139, '', '', '', '', 0.0000, 0.0000, 0.0000, '', '', '', '', '', '', '', 0, 0, 0, 'https://cdn.bestme.asia/images/adgtopalprima/rectangle-8.png', '', 0, 27, 1, 0.0000, 1, 103000.0000, 1, 0, 0, '0000-00-00', 100.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, 'excel', NULL, '2021-11-24 11:49:23', '2021-11-30 11:05:53'),
(138, '', '', '', '', 0.0000, 0.0000, 0.0000, '', '', '', '', '', '', '', 0, 0, 0, 'https://cdn.bestme.asia/images/adgtopalprima/rectangle-8.png', '', 0, 27, 1, 0.0000, 1, 103000.0000, 1, 0, 0, '0000-00-00', 100.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, 'excel', NULL, '2021-11-24 11:49:23', '2021-11-30 11:05:48'),
(137, '', '', '', '', 0.0000, 0.0000, 0.0000, '', '', '', '', '', '', '', 0, 0, 0, 'https://cdn.bestme.asia/images/adgtopalprima/10.jpg', '', 0, 27, 1, 0.0000, 1, 103000.0000, 1, 0, 0, '0000-00-00', 100.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 0, 1, 1, 0, 1, 2, 0, 0, NULL, 1, 1, 'excel', NULL, '2021-11-24 11:49:23', '2021-11-30 11:05:50'),
(136, '', '', '', '', 0.0000, 0.0000, 0.0000, '', '', '', '', '', '', '', 0, 0, 0, 'https://cdn.bestme.asia/images/adgtopalprima/rectangle-8.png', '', 0, 27, 1, 0.0000, 1, 103000.0000, 1, 0, 0, '0000-00-00', 100.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, 'excel', NULL, '2021-11-24 11:49:23', '2021-11-30 11:05:51');

-- restore soft-deleted products if demo
UPDATE `oc_product` SET `deleted` = NULL
WHERE `demo` = 1
  AND `product_id` IN (
       139,
       138,
       137,
       136
    );

-- oc_product_collection PRIMARY KEY (`product_collection_id`)
-- INSERT IGNORE INTO `oc_product_collection` (`product_collection_id`, `product_id`, `collection_id`, `sort_order`) VALUES

-- oc_product_description PRIMARY KEY (`product_id`,`language_id`)
INSERT IGNORE INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `sub_description`, `seo_title`, `seo_description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(132, 2, 'Cửa nhôm Topal', '&lt;div class=&quot;tab-content py-30&quot; id=&quot;productDetailTabContent&quot;&gt;\r\n&lt;div aria-labelledby=&quot;info-tab&quot; class=&quot;tab-pane fade active show&quot; id=&quot;proDesc&quot; role=&quot;tabpanel&quot;&gt;\r\n&lt;h2 class=&quot;fz-20 mb-3 fw-500&quot;&gt;Đạt tiêu chuẩn chống cháy quốc tế&lt;/h2&gt;\r\n\r\n&lt;p&gt;Được kiểm định và cấp chứng nhận chống cháy theo tiêu chuẩn BS EN 16341:2000 Vương quốc Anh và tiêu chuẩn TCXDVN 386:2007 do Cục Phòng cháy, chữa cháy – Bộ Công an cấp. Cửa có khả năng tự động đóng trong trường hợp hỏa hoạn và tự động ngăn chặn sự lan truyền của ngọn lửa (khói) từ khu vực này sang khu vực khác từ 2 đến 4 giờ giúp giảm nhẹ và hạn chế các thiệt hại về vật chất và bảo vệ an toàn tính mạng của con người khi gặp hỏa hoạn.&lt;/p&gt;\r\n\r\n&lt;h2 class=&quot;fz-20 mb-3 fw-500&quot;&gt;Các thiết bị điều khiển chống cháy&lt;/h2&gt;\r\n\r\n&lt;p&gt;Cửa cuốn chống cháy AF100 được trang bị các bộ thiết bị điều khiển chống cháy hiện đại:&lt;br /&gt;\r\n– SDS: Khi nhận tín hiệu cháy, cửa sẽ đóng xuống đến hết hành trình.&lt;br /&gt;\r\n– TTM: Khi nhận tín hiệu cháy, cửa sẽ đóng xuống một phần trong khoảng thời gian theo cài đặt để sơ tán người &amp; tài sản, sau đó mới tiếp tục đóng hết hành trình để cô lập đám cháy.&lt;/p&gt;\r\n\r\n&lt;h2 class=&quot;fz-20 mb-3 fw-500&quot;&gt;Thân cửa&lt;/h2&gt;\r\n\r\n&lt;p&gt;Thân cửa cuốn Chống cháy AF100 được làm bằng thép mạ kẽm có độ dày lên tới 1,2mm và trọng lượng 20kg/m2 được sơn mạ điên phân bám rất chặt trên bề mặt có khả năng chịu được nhiệt và kháng cháy trong khoảng thời gian từ 2 đến 4 giờ giúp giảm nhẹ và hạn chế các thiệt hại về vật chất và bảo vệ an toàn tính mạng của con người khi gặp hỏa hoạn&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div aria-labelledby=&quot;techInfo-tab&quot; class=&quot;tab-pane fade&quot; id=&quot;techInfo&quot; role=&quot;tabpanel&quot;&gt;\r\n&lt;table class=&quot;table table-bordered table-striped&quot;&gt;\r\n	&lt;tbody&gt;\r\n		&lt;tr&gt;\r\n			&lt;th colspan=&quot;2&quot;&gt;THÔNG SỐ&lt;/th&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td width=&quot;35%&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;Kích thước&lt;/span&gt;&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;&lt;span style=&quot;color:#000000;&quot;&gt;Màu sắc&lt;/span&gt;&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;&lt;span style=&quot;color:#000000;&quot;&gt;Chất liệu&lt;/span&gt;&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;&lt;span style=&quot;color:#000000;&quot;&gt;Bề mặt sơn&lt;/span&gt;&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;&lt;span style=&quot;color:#000000;&quot;&gt;Ray&lt;/span&gt;&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n	&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;', '', '', '', '', '', '', ''),
(135, 2, 'Cửa gỗ Huge', '&lt;h2&gt;Đạt tiêu chuẩn chống cháy quốc tế&lt;/h2&gt;\r\n\r\n&lt;p&gt;Được kiểm định và cấp chứng nhận chống cháy theo tiêu chuẩn BS EN 16341:2000 Vương quốc Anh và tiêu chuẩn TCXDVN 386:2007 do Cục Phòng cháy, chữa cháy – Bộ Công an cấp. Cửa có khả năng tự động đóng trong trường hợp hỏa hoạn và tự động ngăn chặn sự lan truyền của ngọn lửa (khói) từ khu vực này sang khu vực khác từ 2 đến 4 giờ giúp giảm nhẹ và hạn chế các thiệt hại về vật chất và bảo vệ an toàn tính mạng của con người khi gặp hỏa hoạn.&lt;/p&gt;\r\n\r\n&lt;h2&gt;Các thiết bị điều khiển chống cháy&lt;/h2&gt;\r\n\r\n&lt;p&gt;Cửa cuốn chống cháy AF100 được trang bị các bộ thiết bị điều khiển chống cháy hiện đại:&lt;br /&gt;\r\n– SDS: Khi nhận tín hiệu cháy, cửa sẽ đóng xuống đến hết hành trình.&lt;br /&gt;\r\n– TTM: Khi nhận tín hiệu cháy, cửa sẽ đóng xuống một phần trong khoảng thời gian theo cài đặt để sơ tán người &amp; tài sản, sau đó mới tiếp tục đóng hết hành trình để cô lập đám cháy.&lt;/p&gt;\r\n\r\n&lt;h2&gt;Thân cửa&lt;/h2&gt;\r\n\r\n&lt;p&gt;Thân cửa cuốn Chống cháy AF100 được làm bằng thép mạ kẽm có độ dày lên tới 1,2mm và trọng lượng 20kg/m2 được sơn mạ điên phân bám rất chặt trên bề mặt có khả năng chịu được nhiệt và kháng cháy trong khoảng thời gian từ 2 đến 4 giờ giúp giảm nhẹ và hạn chế các thiệt hại về vật chất và bảo vệ an toàn tính mạng của con người khi gặp hỏa hoạn&lt;/p&gt;\r\n\r\n&lt;table&gt;\r\n	&lt;tbody&gt;\r\n		&lt;tr&gt;\r\n			&lt;th colspan=&quot;2&quot;&gt;THÔNG SỐ&lt;/th&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td width=&quot;35%&quot;&gt;Kích thước&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Màu sắc&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Chất liệu&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Bề mặt sơn&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Ray&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n	&lt;/tbody&gt;\r\n&lt;/table&gt;', '', '', '', '', '', '', ''),
(134, 2, 'Cửa cuốn Austdoor', '&lt;h2&gt;Đạt tiêu chuẩn chống cháy quốc tế&lt;/h2&gt;\r\n\r\n&lt;p&gt;Được kiểm định và cấp chứng nhận chống cháy theo tiêu chuẩn BS EN 16341:2000 Vương quốc Anh và tiêu chuẩn TCXDVN 386:2007 do Cục Phòng cháy, chữa cháy – Bộ Công an cấp. Cửa có khả năng tự động đóng trong trường hợp hỏa hoạn và tự động ngăn chặn sự lan truyền của ngọn lửa (khói) từ khu vực này sang khu vực khác từ 2 đến 4 giờ giúp giảm nhẹ và hạn chế các thiệt hại về vật chất và bảo vệ an toàn tính mạng của con người khi gặp hỏa hoạn.&lt;/p&gt;\r\n\r\n&lt;h2&gt;Các thiết bị điều khiển chống cháy&lt;/h2&gt;\r\n\r\n&lt;p&gt;Cửa cuốn chống cháy AF100 được trang bị các bộ thiết bị điều khiển chống cháy hiện đại:&lt;br /&gt;\r\n– SDS: Khi nhận tín hiệu cháy, cửa sẽ đóng xuống đến hết hành trình.&lt;br /&gt;\r\n– TTM: Khi nhận tín hiệu cháy, cửa sẽ đóng xuống một phần trong khoảng thời gian theo cài đặt để sơ tán người &amp; tài sản, sau đó mới tiếp tục đóng hết hành trình để cô lập đám cháy.&lt;/p&gt;\r\n\r\n&lt;h2&gt;Thân cửa&lt;/h2&gt;\r\n\r\n&lt;p&gt;Thân cửa cuốn Chống cháy AF100 được làm bằng thép mạ kẽm có độ dày lên tới 1,2mm và trọng lượng 20kg/m2 được sơn mạ điên phân bám rất chặt trên bề mặt có khả năng chịu được nhiệt và kháng cháy trong khoảng thời gian từ 2 đến 4 giờ giúp giảm nhẹ và hạn chế các thiệt hại về vật chất và bảo vệ an toàn tính mạng của con người khi gặp hỏa hoạn&lt;/p&gt;', '', '', '', '', '', '', ''),
(133, 2, 'Cửa cuốn nan nhôm', '&lt;h2&gt;Đạt tiêu chuẩn chống cháy quốc tế&lt;/h2&gt;\r\n\r\n&lt;p&gt;Được kiểm định và cấp chứng nhận chống cháy theo tiêu chuẩn BS EN 16341:2000 Vương quốc Anh và tiêu chuẩn TCXDVN 386:2007 do Cục Phòng cháy, chữa cháy – Bộ Công an cấp. Cửa có khả năng tự động đóng trong trường hợp hỏa hoạn và tự động ngăn chặn sự lan truyền của ngọn lửa (khói) từ khu vực này sang khu vực khác từ 2 đến 4 giờ giúp giảm nhẹ và hạn chế các thiệt hại về vật chất và bảo vệ an toàn tính mạng của con người khi gặp hỏa hoạn.&lt;/p&gt;\r\n\r\n&lt;h2&gt;Các thiết bị điều khiển chống cháy&lt;/h2&gt;\r\n\r\n&lt;p&gt;Cửa cuốn chống cháy AF100 được trang bị các bộ thiết bị điều khiển chống cháy hiện đại:&lt;br /&gt;\r\n– SDS: Khi nhận tín hiệu cháy, cửa sẽ đóng xuống đến hết hành trình.&lt;br /&gt;\r\n– TTM: Khi nhận tín hiệu cháy, cửa sẽ đóng xuống một phần trong khoảng thời gian theo cài đặt để sơ tán người &amp; tài sản, sau đó mới tiếp tục đóng hết hành trình để cô lập đám cháy.&lt;/p&gt;\r\n\r\n&lt;h2&gt;Thân cửa&lt;/h2&gt;\r\n\r\n&lt;p&gt;Thân cửa cuốn Chống cháy AF100 được làm bằng thép mạ kẽm có độ dày lên tới 1,2mm và trọng lượng 20kg/m2 được sơn mạ điên phân bám rất chặt trên bề mặt có khả năng chịu được nhiệt và kháng cháy trong khoảng thời gian từ 2 đến 4 giờ giúp giảm nhẹ và hạn chế các thiệt hại về vật chất và bảo vệ an toàn tính mạng của con người khi gặp hỏa hoạn&lt;/p&gt;', '', '', '', '', '', '', ''),
(136, 2, 'Cửa nhôm Topal', '&lt;div class=&quot;tab-content py-30&quot; id=&quot;productDetailTabContent&quot;&gt;\r\n&lt;div aria-labelledby=&quot;info-tab&quot; class=&quot;tab-pane fade active show&quot; id=&quot;proDesc&quot; role=&quot;tabpanel&quot;&gt;\r\n&lt;h2 class=&quot;fz-20 mb-3 fw-500&quot;&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;Đạt tiêu chuẩn chống cháy quốc tế&lt;/span&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;Được kiểm định và cấp chứng nhận chống cháy theo tiêu chuẩn BS EN 16341:2000 Vương quốc Anh và tiêu chuẩn TCXDVN 386:2007 do Cục Phòng cháy, chữa cháy – Bộ Công an cấp. Cửa có khả năng tự động đóng trong trường hợp hỏa hoạn và tự động ngăn chặn sự lan truyền của ngọn lửa (khói) từ khu vực này sang khu vực khác từ 2 đến 4 giờ giúp giảm nhẹ và hạn chế các thiệt hại về vật chất và bảo vệ an toàn tính mạng của con người khi gặp hỏa hoạn.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/rectangle-161.jpg&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;h2 class=&quot;fz-20 mb-3 fw-500&quot;&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;Các thiết bị điều khiển chống cháy&lt;/span&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;Cửa cuốn chống cháy AF100 được trang bị các bộ thiết bị điều khiển chống cháy hiện đại:&lt;br /&gt;\r\n– SDS: Khi nhận tín hiệu cháy, cửa sẽ đóng xuống đến hết hành trình.&lt;br /&gt;\r\n– TTM: Khi nhận tín hiệu cháy, cửa sẽ đóng xuống một phần trong khoảng thời gian theo cài đặt để sơ tán người &amp; tài sản, sau đó mới tiếp tục đóng hết hành trình để cô lập đám cháy.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/rectangle-161.jpg&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;h2 class=&quot;fz-20 mb-3 fw-500&quot;&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;Thân cửa&lt;/span&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;Thân cửa cuốn Chống cháy AF100 được làm bằng thép mạ kẽm có độ dày lên tới 1,2mm và trọng lượng 20kg/m2 được sơn mạ điên phân bám rất chặt trên bề mặt có khả năng chịu được nhiệt và kháng cháy trong khoảng thời gian từ 2 đến 4 giờ giúp giảm nhẹ và hạn chế các thiệt hại về vật chất và bảo vệ an toàn tính mạng của con người khi gặp hỏa hoạn&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/rectangle-161.jpg&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;', '&lt;p&gt;Cửa cuốn chống cháy AF100 là giải pháp tối ưu cho công trình khi có sự cố hỏa hoạn. Khi xảy ra hỏa hoạn cửa cuốn chống cháy AF100 sẽ tự động đóng xuống ngăn và khoanh vùng cháy, tạo thành hành lang thoát hiểm cho con người và hàng hóa. AF100 được sản xuất theo tiêu chuẩn BS EN 16341:2000 Vương quốc Anh và được cục PCCC – Bộ Công An Việt Nam cấp chứng nhận đạt tiêu chuẩn TCXDVN 386:2007.&lt;/p&gt;', '', '', '', '', '', ''),
(137, 2, 'Cửa gỗ Huge', '&lt;h2&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;Đạt tiêu chuẩn chống cháy quốc tế&lt;/span&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;Được kiểm định và cấp chứng nhận chống cháy theo tiêu chuẩn BS EN 16341:2000 Vương quốc Anh và tiêu chuẩn TCXDVN 386:2007 do Cục Phòng cháy, chữa cháy – Bộ Công an cấp. Cửa có khả năng tự động đóng trong trường hợp hỏa hoạn và tự động ngăn chặn sự lan truyền của ngọn lửa (khói) từ khu vực này sang khu vực khác từ 2 đến 4 giờ giúp giảm nhẹ và hạn chế các thiệt hại về vật chất và bảo vệ an toàn tính mạng của con người khi gặp hỏa hoạn.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/rectangle-161.jpg&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;h2&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;Các thiết bị điều khiển chống cháy&lt;/span&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;Cửa cuốn chống cháy AF100 được trang bị các bộ thiết bị điều khiển chống cháy hiện đại:&lt;br /&gt;\r\n– SDS: Khi nhận tín hiệu cháy, cửa sẽ đóng xuống đến hết hành trình.&lt;br /&gt;\r\n– TTM: Khi nhận tín hiệu cháy, cửa sẽ đóng xuống một phần trong khoảng thời gian theo cài đặt để sơ tán người &amp; tài sản, sau đó mới tiếp tục đóng hết hành trình để cô lập đám cháy.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/rectangle-161.jpg&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;h2&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;Thân cửa&lt;/span&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;Thân cửa cuốn Chống cháy AF100 được làm bằng thép mạ kẽm có độ dày lên tới 1,2mm và trọng lượng 20kg/m2 được sơn mạ điên phân bám rất chặt trên bề mặt có khả năng chịu được nhiệt và kháng cháy trong khoảng thời gian từ 2 đến 4 giờ giúp giảm nhẹ và hạn chế các thiệt hại về vật chất và bảo vệ an toàn tính mạng của con người khi gặp hỏa hoạn&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/rectangle-161.jpg&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;', '&lt;p&gt;Cửa cuốn chống cháy AF100 là giải pháp tối ưu cho công trình khi có sự cố hỏa hoạn. Khi xảy ra hỏa hoạn cửa cuốn chống cháy AF100 sẽ tự động đóng xuống ngăn và khoanh vùng cháy, tạo thành hành lang thoát hiểm cho con người và hàng hóa. AF100 được sản xuất theo tiêu chuẩn BS EN 16341:2000 Vương quốc Anh và được cục PCCC – Bộ Công An Việt Nam cấp chứng nhận đạt tiêu chuẩn TCXDVN 386:2007.&lt;/p&gt;', '', '', '', '', '', ''),
(138, 2, 'Cửa cuốn Austdoor', '&lt;h2&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;Đạt tiêu chuẩn chống cháy quốc tế&lt;/span&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;Được kiểm định và cấp chứng nhận chống cháy theo tiêu chuẩn BS EN 16341:2000 Vương quốc Anh và tiêu chuẩn TCXDVN 386:2007 do Cục Phòng cháy, chữa cháy – Bộ Công an cấp. Cửa có khả năng tự động đóng trong trường hợp hỏa hoạn và tự động ngăn chặn sự lan truyền của ngọn lửa (khói) từ khu vực này sang khu vực khác từ 2 đến 4 giờ giúp giảm nhẹ và hạn chế các thiệt hại về vật chất và bảo vệ an toàn tính mạng của con người khi gặp hỏa hoạn.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/rectangle-161.jpg&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;h2&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;Các thiết bị điều khiển chống cháy&lt;/span&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;Cửa cuốn chống cháy AF100 được trang bị các bộ thiết bị điều khiển chống cháy hiện đại:&lt;br /&gt;\r\n– SDS: Khi nhận tín hiệu cháy, cửa sẽ đóng xuống đến hết hành trình.&lt;br /&gt;\r\n– TTM: Khi nhận tín hiệu cháy, cửa sẽ đóng xuống một phần trong khoảng thời gian theo cài đặt để sơ tán người &amp; tài sản, sau đó mới tiếp tục đóng hết hành trình để cô lập đám cháy.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/rectangle-161.jpg&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;h2&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;Thân cửa&lt;/span&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;Thân cửa cuốn Chống cháy AF100 được làm bằng thép mạ kẽm có độ dày lên tới 1,2mm và trọng lượng 20kg/m2 được sơn mạ điên phân bám rất chặt trên bề mặt có khả năng chịu được nhiệt và kháng cháy trong khoảng thời gian từ 2 đến 4 giờ giúp giảm nhẹ và hạn chế các thiệt hại về vật chất và bảo vệ an toàn tính mạng của con người khi gặp hỏa hoạn&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/rectangle-161.jpg&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;', '&lt;p&gt;Cửa cuốn chống cháy AF100 là giải pháp tối ưu cho công trình khi có sự cố hỏa hoạn. Khi xảy ra hỏa hoạn cửa cuốn chống cháy AF100 sẽ tự động đóng xuống ngăn và khoanh vùng cháy, tạo thành hành lang thoát hiểm cho con người và hàng hóa. AF100 được sản xuất theo tiêu chuẩn BS EN 16341:2000 Vương quốc Anh và được cục PCCC – Bộ Công An Việt Nam cấp chứng nhận đạt tiêu chuẩn TCXDVN 386:2007.&lt;/p&gt;', '', '', '', '', '', ''),
(139, 2, 'Cửa cuốn nan nhôm', '&lt;h2&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;Đạt tiêu chuẩn chống cháy quốc tế&lt;/span&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;Được kiểm định và cấp chứng nhận chống cháy theo tiêu chuẩn BS EN 16341:2000 Vương quốc Anh và tiêu chuẩn TCXDVN 386:2007 do Cục Phòng cháy, chữa cháy – Bộ Công an cấp. Cửa có khả năng tự động đóng trong trường hợp hỏa hoạn và tự động ngăn chặn sự lan truyền của ngọn lửa (khói) từ khu vực này sang khu vực khác từ 2 đến 4 giờ giúp giảm nhẹ và hạn chế các thiệt hại về vật chất và bảo vệ an toàn tính mạng của con người khi gặp hỏa hoạn.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/rectangle-161.jpg&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;h2&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;Các thiết bị điều khiển chống cháy&lt;/span&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;Cửa cuốn chống cháy AF100 được trang bị các bộ thiết bị điều khiển chống cháy hiện đại:&lt;br /&gt;\r\n– SDS: Khi nhận tín hiệu cháy, cửa sẽ đóng xuống đến hết hành trình.&lt;br /&gt;\r\n– TTM: Khi nhận tín hiệu cháy, cửa sẽ đóng xuống một phần trong khoảng thời gian theo cài đặt để sơ tán người &amp; tài sản, sau đó mới tiếp tục đóng hết hành trình để cô lập đám cháy.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/rectangle-161.jpg&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;h2&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;Thân cửa&lt;/span&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;Thân cửa cuốn Chống cháy AF100 được làm bằng thép mạ kẽm có độ dày lên tới 1,2mm và trọng lượng 20kg/m2 được sơn mạ điên phân bám rất chặt trên bề mặt có khả năng chịu được nhiệt và kháng cháy trong khoảng thời gian từ 2 đến 4 giờ giúp giảm nhẹ và hạn chế các thiệt hại về vật chất và bảo vệ an toàn tính mạng của con người khi gặp hỏa hoạn&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/rectangle-161.jpg&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;', '&lt;p&gt;Cửa cuốn chống cháy AF100 là giải pháp tối ưu cho công trình khi có sự cố hỏa hoạn. Khi xảy ra hỏa hoạn cửa cuốn chống cháy AF100 sẽ tự động đóng xuống ngăn và khoanh vùng cháy, tạo thành hành lang thoát hiểm cho con người và hàng hóa. AF100 được sản xuất theo tiêu chuẩn BS EN 16341:2000 Vương quốc Anh và được cục PCCC – Bộ Công An Việt Nam cấp chứng nhận đạt tiêu chuẩn TCXDVN 386:2007.&lt;/p&gt;', '', '', '', '', '', '');

-- oc_product_image PRIMARY KEY (`product_image_id`)
-- nothing

-- oc_product_to_category PRIMARY KEY (`product_id`,`category_id`)
INSERT IGNORE INTO `oc_product_to_category` (`product_id`, `category_id`) VALUES
(136, 32),
(137, 32),
(138, 32),
(139, 32);

-- oc_product_to_store PRIMARY KEY (`product_id`,`store_id`)
INSERT IGNORE INTO `oc_product_to_store` (`product_id`, `store_id`, `product_version_id`, `quantity`, `cost_price`) VALUES
(137, 0, 0, 100, 10000.0000),
(139, 0, 0, 100, 103000.0000),
(138, 0, 0, 100, 103000.0000),
(136, 0, 0, 100, 103000.0000);

-- oc_seo_url PRIMARY KEY (`seo_url_id`)
INSERT IGNORE INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`, `table_name`) VALUES
(2082, 0, 2, 'product_id=103', 'd-prod-giay-dup-cao-11cm', 'product'),
(2081, 0, 1, 'product_id=103', 'd-prod-giay-dup-cao-11cm', 'product'),
(2080, 0, 2, 'product_id=99', 'd-prod-giay-sandal-chien-binh-day-keo-1-ben', 'product'),
(2079, 0, 1, 'product_id=99', 'd-prod-giay-sandal-chien-binh-day-keo-1-ben', 'product'),
(2078, 0, 2, 'product_id=98', 'd-prod-dep-sandal-xoan-co-chan', 'product'),
(2077, 0, 1, 'product_id=98', 'd-prod-dep-sandal-xoan-co-chan', 'product'),
(2076, 0, 2, 'product_id=97', 'd-prod-giay-sandal-quai-ngang-anh-7-mau-1', 'product'),
(2075, 0, 1, 'product_id=97', 'd-prod-giay-sandal-quai-ngang-anh-7-mau-1', 'product'),
(2074, 0, 2, 'product_id=88', 'd-prod-giay-sandal-cao-got-quai-trong-got-kim-sa-size-34-den-40', 'product'),
(2073, 0, 1, 'product_id=88', 'd-prod-giay-sandal-cao-got-quai-trong-got-kim-sa-size-34-den-40', 'product'),
(2072, 0, 2, 'product_id=87', 'd-prod-giay-valen-vien-dinh-size-36-37-38', 'product'),
(2071, 0, 1, 'product_id=87', 'd-prod-giay-valen-vien-dinh-size-36-37-38', 'product'),
(2070, 0, 2, 'product_id=86', 'd-prod-giay-cao-got-dep-den-no', 'product'),
(2069, 0, 1, 'product_id=86', 'd-prod-giay-cao-got-dep-den-no', 'product'),
(2068, 0, 2, 'product_id=85', 'd-prod-giay-cao-got-quai-trong-phoi-dinh', 'product'),
(2067, 0, 1, 'product_id=85', 'd-prod-giay-cao-got-quai-trong-phoi-dinh', 'product'),
(2066, 0, 2, 'product_id=84', 'd-prod-giay-cao-got-got-kim-sa-xu-doc-la', 'product'),
(2065, 0, 1, 'product_id=84', 'd-prod-giay-cao-got-got-kim-sa-xu-doc-la', 'product'),
(2064, 0, 2, 'product_id=83', 'd-prod-giay-co-dien-sang-trong-mau-hong', 'product'),
(2063, 0, 1, 'product_id=83', 'd-prod-giay-co-dien-sang-trong-mau-hong', 'product'),
(2193, 0, 1, 'menu_id=6', 'menu-bo-suu-tap', ''),
(2192, 0, 2, 'menu_id=6', 'menu-bo-suu-tap', ''),
(2318, 0, 2, 'product_id=138', 'cua-cuon-austdoor', 'product'),
(2117, 0, 2, 'manufacturer_id=26', '', 'manufacturer'),
(2118, 0, 1, 'manufacturer_id=26', '', 'manufacturer'),
(2321, 0, 1, 'product_id=137', 'cua-go-huge', 'product'),
(2320, 0, 2, 'product_id=137', 'cua-go-huge', 'product'),
(2323, 0, 1, 'product_id=136', 'cua-nhom-topal', 'product'),
(2322, 0, 2, 'product_id=136', 'cua-nhom-topal', 'product'),
(2243, 0, 2, 'blog_category_id=5', 'projects', 'blog_category'),
(2241, 0, 2, 'blog_category_id=3', 'tin-tuc-dai-ly', 'blog_category'),
(2242, 0, 2, 'blog_category_id=4', 'khuyen-mai', 'blog_category'),
(2305, 0, 2, 'blog=4', 'khu-do-thi-tran-anh-riverside', 'blog'),
(2303, 0, 2, 'blog=5', 'nha-may-vinamilk', 'blog'),
(2304, 0, 2, 'blog=6', 'toa-nha-viettel', 'blog'),
(2314, 0, 2, 'blog=23', 'khu-do-thi-lavila-dong-sai-gon', 'blog'),
(2309, 0, 2, 'blog=7', 'nha-may-nhua-duy-tan-1', 'blog'),
(2301, 0, 2, 'blog=8', 'du-an-to-hop-ttm-vincom-hung-vuong-va-khach-san-5-sao-vinpearl-hue', 'blog'),
(2307, 0, 2, 'blog=9', 'nha-may-huayuan-viet-nam-1', 'blog'),
(2277, 0, 2, 'blog=10', 'top-4-mau-cua-so-duoc-ua-chuong-nhat-nam-2021', 'blog'),
(2276, 0, 2, 'blog=11', 'topal-mach-ban-cach-chon-mau-cua-nhom-phu-hop-nhat-cho-phong-cach-co-dien', 'blog'),
(2275, 0, 2, 'blog=12', 'kinh-nghiem-chon-mua-cua-nhom-do-ben-cao', 'blog'),
(2274, 0, 2, 'blog=13', 'cua-nhom-dong-bo-topal-prima-da-lam-hai-long-khach-hang-nhu-the-nao', 'blog'),
(2273, 0, 2, 'blog=14', 'nhom-topal-mua-cang-nhieu-qua-cang-lon', 'blog'),
(2272, 0, 2, 'blog=15', 'mua-nhom-topal-nhan-ngay-vang-9999', 'blog'),
(2293, 0, 2, 'blog=2', 'thanh-tich-va-giai-thuong', 'blog'),
(2280, 0, 2, 'blog=16', 'quy-trinh-san-xuat-tao-nen-su-khac-biet-cua-nhom-topal', 'blog'),
(2325, 0, 1, 'product_id=139', 'cua-cuon-nan-nhom', 'product'),
(2324, 0, 2, 'product_id=139', 'cua-cuon-nan-nhom', 'product'),
(2291, 0, 2, 'blog_category_id=6', 'tin-noi-bat', 'blog_category'),
(2295, 0, 2, 'blog=18', 'topal-tu-hao-la-thuong-hieu-nhom-tre-nhat-dat-thuong-hieu-quoc-gia-2020', 'blog'),
(2296, 0, 2, 'blog=19', 'nhom-topal-tai-dinh-vi-thuong-hieu', 'blog'),
(2297, 0, 2, 'blog=20', 'lap-cua-topal-ngap-tran-qua-tang', 'blog'),
(2313, 0, 2, 'blog=21', 'sai-gon-peninsula-1', 'blog'),
(2311, 0, 2, 'blog=22', 'du-an-benh-vien-vinmec', 'blog'),
(2315, 0, 2, 'blog=24', 'vincom-ha-tinh', 'blog'),
(2316, 0, 2, 'menu_id=9', 'dieu-khoan', ''),
(2317, 0, 1, 'menu_id=9', 'dieu-khoan', ''),
(2326, 0, 2, 'blog_category_id=7', 'gioi-thieu', 'blog_category'),
(2319, 0, 1, 'product_id=138', 'cua-cuon-austdoor', 'product'),
(2282, 0, 1, 'manufacturer_id=27', '-2', 'manufacturer'),
(2281, 0, 2, 'manufacturer_id=27', '-2', 'manufacturer'),
(2228, 0, 2, 'manufacturer_id=26', '-1', 'manufacturer'),
(2229, 0, 1, 'manufacturer_id=26', '-1', 'manufacturer'),
(2230, 0, 2, 'category_id=32', 'demo', 'category'),
(2231, 0, 1, 'category_id=32', 'demo', 'category'),
(2292, 0, 2, 'blog=1', 'gioi-thieu-ve-chung-toi', 'blog'),
(2294, 0, 2, 'blog=17', 'nhom-topal-xuat-khau-thanh-cong-sang-my-canada', 'blog');

-- oc_theme_builder_config NO PRIMARY KEY
-- remove first
DELETE FROM `oc_theme_builder_config`
WHERE `store_id` = '0'
  AND `config_theme` = 'adg_topal_xfec'
  AND `code` = 'config';

-- insert
INSERT IGNORE INTO `oc_theme_builder_config` (`store_id`, `config_theme`, `code`, `key`, `config`, `date_added`, `date_modified`) VALUES
(0, 'adg_topal_xfec', 'config', 'config_theme_color', '{\n    "main": {\n        "main": {\n            "hex": "F58220",\n            "visible": "1"\n        },\n        "button": {\n            "hex": "00456E",\n            "visible": "1"\n        }\n    }\n}', '2021-11-08 16:57:19', '2022-02-08 10:16:52'),
(0, 'adg_topal_xfec', 'config', 'config_theme_text', '{\n    "all": {\n        "title": "Font Roboto",\n        "font": "Roboto",\n        "supported-fonts": [\n            "Open Sans",\n            "IBM Plex Sans",\n            "Courier New",\n            "Roboto",\n            "Nunito",\n            "Arial",\n            "DejaVu Sans",\n            "Tahoma",\n            "Time News Roman",\n            "Mulish"\n        ]\n    },\n    "heading": {\n        "font": "Tahoma",\n        "supported-fonts": [\n            "Open Sans",\n            "IBM Plex Sans",\n            "Courier New",\n            "Roboto",\n            "Nunito",\n            "Arial",\n            "DejaVu Sans",\n            "Tahoma",\n            "Time News Roman",\n            "Quicksand",\n            "Montserrat",\n            "KoHo",\n            "Newsreader",\n            "Oswald",\n            "Playfair Display",\n            "Lora",\n            "Inter"\n        ],\n        "font-type": "regular",\n        "supported-font-types": [\n            "regular",\n            "bold",\n            "italic"\n        ],\n        "font-size": 32\n    },\n    "body": {\n        "font": "Aria",\n        "supported-fonts": [\n            "Open Sans",\n            "IBM Plex Sans",\n            "Courier New",\n            "Roboto",\n            "Nunito",\n            "Arial",\n            "DejaVu Sans",\n            "Tahoma",\n            "Time News Roman",\n            "Quicksand",\n            "Montserrat",\n            "KoHo",\n            "Newsreader",\n            "Oswald",\n            "Playfair Display",\n            "Lora",\n            "Inter"\n        ],\n        "font-type": "regular",\n        "supported-font-types": [\n            "regular",\n            "bold",\n            "italic"\n        ],\n        "font-size": 14\n    }\n}', '2021-11-08 16:57:19', '2022-02-08 10:16:52'),
(0, 'adg_topal_xfec', 'config', 'config_theme_favicon', '{\n    "image": {\n        "url": "\\/catalog\\/view\\/theme\\/default\\/image\\/favicon.ico"\n    }\n}', '2021-11-08 16:57:19', '2022-02-08 10:16:52'),
(0, 'adg_topal_xfec', 'config', 'config_theme_social', '[\n    {\n        "name": "facebook",\n        "text": "Facebook",\n        "url": "https:\\/\\/www.facebook.com"\n    },\n    {\n        "name": "twitter",\n        "text": "Twitter",\n        "url": "https:\\/\\/www.twitter.com"\n    },\n    {\n        "name": "instagram",\n        "text": "Instagram",\n        "url": "https:\\/\\/www.instagram.com"\n    },\n    {\n        "name": "tumblr",\n        "text": "Tumblr",\n        "url": "https:\\/\\/www.tumblr.com"\n    },\n    {\n        "name": "youtube",\n        "text": "Youtube",\n        "url": "https:\\/\\/www.youtube.com"\n    },\n    {\n        "name": "googleplus",\n        "text": "Google Plus",\n        "url": "https:\\/\\/www.plus.google.com"\n    }\n]', '2021-11-08 16:57:19', '2022-02-08 10:16:52'),
(0, 'adg_topal_xfec', 'config', 'config_theme_ecommerce', '[\n    {\n        "name": "shopee",\n        "text": "Shopee",\n        "url": "https:\\/\\/shopee.vn"\n    },\n    {\n        "name": "lazada",\n        "text": "Lazada",\n        "url": "https:\\/\\/www.lazada.vn"\n    },\n    {\n        "name": "tiki",\n        "text": "Tiki",\n        "url": "https:\\/\\/tiki.vn"\n    },\n    {\n        "name": "sendo",\n        "text": "Sendo",\n        "url": "https:\\/\\/www.sendo.vn"\n    }\n]', '2021-11-08 16:57:19', '2022-02-08 10:16:52'),
(0, 'adg_topal_xfec', 'config', 'config_theme_override_css', '{\n    "css": "\\/* override css *\\/\\n.bestme-block-banner-top a {\\n    color: #053D5A !important;\\n}\\n.news-item .news-title:hover {\\n    color: #f58220;\\n}\\nsection.py-50.mb-5.news-home {\\n    padding-bottom: 0 !important;\\n    padding-top: 0 !important;\\n}\\n\\nsection.py-50.mb-5.news-home .container {\\n    padding-bottom: 0;\\n}\\n.service .bg-third.h-100.px-lg-20.px-15.pt-lg-40.py-lg-50.py-20:hover {\\n    background: rgb(13 110 253 \\/ 15%) !important;\\n    transition: 0.4s;\\n}\\n.about-us section.pt-lg-50.about-us-home.pb-50 {\\n    padding-top: 0 !important;\\n    padding-bottom: 0 !important;\\n}\\n.why-choose .pt-lg-50.container {\\n    padding-top: 0 !important;\\n}\\n.why-choose .pt-30.pt-lg-50.pb-lg-50.position-relative.z-index-1 {\\n    padding-bottom: 0 !important;\\n    padding-top: 30px !important;\\n}\\n.service section.pt-50.pb-lg-50.pb-20 {\\n    padding-top: 0 !important;\\n    padding-bottom: 0 !important;\\n}\\n.projects-box section.py-50.container.container {\\n    padding-top: 25px !important;\\n    padding-bottom: 0px !important;\\n}\\n.intro-award .py-lg-50 {\\n    padding: 0 !important;\\n}\\n\\n#header .header-search input.form-control {\\n  border-left: none;\\n  padding-right: 12px;\\n}\\n\\n.product-item img {\\n    height: 321px;\\n  \\tobject-fit: cover;\\n}\\n.news-item img {\\n    height: 209px;\\n    object-fit: cover;\\n}\\na.btn.btn-primary.mt-3.rounded-5.contact-now-btn {\\n    margin-top: -10px !important;\\n    border-radius: 10px !important;\\n}\\n.nav-tabs li.nav-item.me-md-4 {\\n    margin-right: 0 !important;\\n}\\n.news-item.high-light.mb-lg-60 img {\\n    height: 332px;\\n    object-fit: cover;\\n}\\n\\n.main-header ul.nav \\u003E li \\u003E a\\u003E.toggle {\\n    background: #fff;\\n    transform: rotate(\\n0deg);\\n    transform: translateX(3px);\\n}\\n\\n.main-header ul.nav.child-nav {\\n    padding: 15px;\\n}\\n\\n.main-header ul.nav.child-nav li{\\n    padding-bottom: 5px;\\n}\\n\\n.main-header ul.nav.child-nav {\\n    padding: 15px;\\n}\\n\\n.main-header ul.nav.child-nav li{\\n    padding: 7px 2px;\\n    border-bottom: 1px solid #0000001c;\\n    font-size: 14px;\\n}\\n\\n.main-header ul.nav.child-nav li:last-child {\\n    border: none;\\n    padding-bottom: 0px;\\n}\\n\\n.main-header ul.nav.child-nav a{\\n    color: #00456E;\\n}\\n\\n.main-header ul.nav.child-nav a:hover{color: #F58220;}\\n.block-header #navbarMainMenu ul.nav ul.nav {\\n    border: none;\\n    transform: translate(2px,20px);\\n    padding-left: 19px;\\n    box-shadow: 5px 7px 8px 1px #dfe4e745;\\n}\\n\\n.bestme-block-header-menu ul.nav.child-nav {\\n    transform: translateX(-17px);\\n}\\n\\nul.nav.navbar-nav.bestme-block-header-menu li.has-child {\\n    padding-right: 15px;\\n}\\n\\n.nav-tabs .nav-link::first-letter {\\n    text-transform: uppercase;\\n}\\n#footer .footer-col:last-child {\\n    text-align: right;\\n}\\n.bestme-block-contact-contact .mt-lg-2.mb-lg-40 {\\n    text-align: center;\\n}\\n.projects-view img {\\n    height: 400px;\\n    object-fit: cover;\\n}\\n.intro-award .col-lg-5.py-lg-4.pt-4 {\\n    display: flex;\\n    flex-direction: column;\\n    justify-content: center;\\n}\\n.news-detail.news-item {\\n    padding: 15px;\\n    margin: 0 !important;\\n\\twidth:100%;\\n}\\n.bestme-block-contact-contact .align-self-end.w-100.pe-lg-50 {\\n    align-self: start !important;\\n}\\n\\n#footer .main-footer {\\n    padding: 40px 0 35px;\\n}\\n\\n.main-footer .d-block.mb-5.text-center img {\\n    transform: translateY(-39px);\\n}\\n\\n.col-12.col-lg-8.news-detail.news-item.mx-auto.my-0.my-lg-3 img {\\n    width: 100% !important;\\n    height: auto;\\n}\\n.bestme-block-contact-contact textarea#your-messenger {\\n    height: 150px;\\n}\\n.main-footer h4 {\\n    font-size: 16px !important;\\n}\\n\\n #footer .nav.flex-column {\\n      list-style: disc;\\n      padding-left: 15px;\\n    }\\n\\n    #footer .footer-col li::marker {\\n      color: #dc2e28;\\n    }\\n\\n    #footer .footer-col li::before {\\n      content: \\u0022\\u0022;\\n      width: 0;\\n    }\\n\\n.breadcrumb-item+.breadcrumb-item {\\n    font-size: 14px;\\n}\\nol.breadcrumb a {\\n    font-size: 14px;\\n}\\n@media(max-width: 768px){\\n  .main-footer .logo {\\n    display: none !important;\\n  }\\n  header.block-header.fixed .logo {\\n    top: 15px !important;\\n  }\\n  .news-box {\\n    padding-top: 30px;\\n  }\\n  .news-item {\\n    border-bottom: 1px solid #80808045;\\n    padding-bottom: 25px;\\n  }\\n  #header .navbar a.nav-link:hover {\\n    color: white !important;\\n }\\n  .why-choose-us-item {\\n    height: auto;\\n  }\\n  .service section.pt-50.pb-lg-50.pb-20 {\\n    padding-top: 20px !important;\\n  }\\n  .about-us section.pt-lg-50.about-us-home.pb-50 {\\n    padding-top: 0 !important;\\n    padding-bottom: 30px !important;\\n  }\\n  .why-choose .pt-lg-50.container {\\n      padding-top: 0 !important;\\n  }\\n  .why-choose .pt-30.pt-lg-50.pb-lg-50.position-relative.z-index-1 {\\n      padding-bottom: 0 !important;\\n      padding-top: 30px !important;\\n  }\\n  .service section.pt-50.pb-lg-50.pb-20 {\\n      padding-top: 0;\\n      padding-bottom: 0 !important;\\n  }\\n  .projects-box section.py-50.container.container {\\n      padding-top: 25px !important;\\n      padding-bottom: 30px !important;\\n  }\\n  \\n  .intro-award .py-lg-50 {\\n      padding: 0 !important;\\n  }\\n  #header .main-header {\\n    padding: 0 !important;\\n  }\\n  #header .header-search .collapse-search a .icon {\\n    top: 20px;\\n  }\\n  .header-top .col-sm-4 {\\n    display: none;\\n  }\\n  .header-top.d-flex.flex-wrap.bestme-block-banner-top.row {\\n    display: none !important;\\n   }\\n  .logo {\\n    top: 16px;\\n    display: block !important;\\n    position: absolute;\\n    z-index: 99;\\n    left: 50%;\\n    transform: translateX(-50%);\\n  }\\n  .header-top .col-sm-8.d-flex.text-end.text-lg-start.ms-auto.ms-lg-0.flex-column.flex-lg-row.fz-10.fz-lg-13 {\\n    position: absolute;\\n    right: 0;\\n    width: 35%;\\n    top: 3px;\\n  }\\n  .product-item img {\\n    height: auto !important;\\n    object-fit: cover;\\n  }\\n  .block-header #navbarMainMenu ul.nav ul.nav {\\n    padding-top: 0 !important;\\n    padding-bottom: 0 !important;\\n  }\\n  .main-header ul.nav.child-nav {\\n    padding: 0;\\n  }\\n  .copy-right br {\\n    display: none;\\n  }\\n  .copy-right {\\n      padding: 15px !important;\\n  }\\n  .service img {\\n    object-fit: cover;\\n    margin-top: 0 !important;\\n  }\\n  #footer .footer-col:last-child {\\n    text-align: left;\\n  }\\n  .block-products h2 {\\n    font-size: 24px;\\n  }\\n  ol.breadcrumb a {\\n    color: #737373;\\n  }\\n  .breadcrumb-item+.breadcrumb-item::before {\\n    padding-left: 3px;\\n  }\\n  #header {\\n    background: #FFFFFF;\\n  }\\n  .bestme-block-contact-contact .mt-lg-2.mb-lg-40 {\\n    text-align: left;\\n  }\\n  .contact-section .contact-item {\\n    margin-bottom: 50px;\\n  }\\n  ol.breadcrumb {\\n    margin-bottom: 5px;\\n  }\\n  section.py-50.project-related {\\n    padding-top: 0 !important;\\n  }\\n  .teaser .news-item .news-img {\\n    height: 110px;\\n  }\\n  div#productDetailTabContent {\\n    padding-top: 0 !important;\\n    padding-bottom: 1px !important;\\n  }\\n  .bestme-block-product-related-product {\\n    background: #F6F6F6 !important;\\n  }\\n  .main-header ul.nav \\u003E li \\u003E a\\u003E.toggle {\\n    background: white;\\n  }\\n  .main-header ul.nav.child-nav a {\\n    color: white;\\n  }\\n  .block-header #navbarMainMenu ul.nav ul.nav {\\n    box-shadow: none;\\n    background: none !important;\\n    display: none;\\n  }\\n\\n.block-header #navbarMainMenu ul.nav \\n li.active ul.nav {\\n    display: block;\\n    padding-bottom: 25px !important;\\n}\\n      .block-header #navbarMainMenu ul.nav ul.nav li {\\n    border: none;\\n    padding:5px 0;\\n  }\\n  .block-header #navbarMainMenu ul.nav ul.nav li a {\\n    text-transform: lowercase;\\n    color: #c4c4c4;\\n  }\\n  #product-category .flex-column, #product-collection .flex-column, #product-provider .flex-column, #product-tags .flex-column, #product-attribute .flex-column {\\n    overflow-y: hidden !important;\\n  }\\n  .main-header li.nav-item {\\n    padding: 5px 0;\\n  }\\n  .copy-right {\\n    font-size: 14px;\\n  }\\n  .news-item.high-light a.news-title {\\n    font-size: 18px;\\n    padding-top: 15px;\\n  }\\n  .news-item .news-title {\\n    font-size: 18px;\\n  }\\n  .project-item .project-title {\\n    font-size: 18px;\\n  }\\n  #header .header-search {\\n    width: 90% !important;\\n    padding-right: 21px;\\n    transform: translateY(9px);\\n    position: absolute;\\n  }\\n  #header .header-search.show .collapse-search a .icon {\\n    top: 49px;\\n  }\\n}\\n\\n#footer .main-footer:before {\\n    background-color: unset;\\n}\\n#footer .main-footer {\\n    background-image: url(https:\\/\\/topal.vn\\/wp-content\\/uploads\\/2017\\/11\\/bg-footer.jpg) !important;\\n    background-size: cover;\\n}\\n#footer .footer-col li,#footer .footer-col li a,#footer .footer-col h4, #footer .footer-col span {\\n    color: #00456E !important;\\n}\\n\\n#footer .footer-col li a:hover, #footer .footer-col li a.active, #footer a:hover {\\n  color: white !important;\\n}\\n\\n#footer .footer-col li::marker {\\n    color: #00456E !important;\\n}\\n\\n#header .navbar a.nav-link {\\n  color: white;\\n}\\n#header .navbar a.nav-link.active {\\n  color: white !important;\\n}\\n#header .navbar a.nav-link:hover {\\n    color: #00456E;\\n}\\n#footer .copy-right {\\n   background-color: #F58220 !important;\\n}\\nfooter#footer p {\\n    color: #00456E !important;\\n}\\n#header .main-header {\\n    padding: 20px 0;\\n    background: #F58220;\\n}\\n\\n.block-header #navbarMainMenu ul.nav ul.nav {\\n    background: white;\\n}\\n.icon.icon-search {\\n   background-color: #fff !important;\\n}\\n\\n#header .header-search.show ~ .navbar {\\n    display: flex;\\n    flex-wrap: wrap;\\n    align-items: center;\\n    width: 50%;\\n}\\n\\n#header .header-search {\\n    width: 35%;\\n}\\n\\n#header .header-search.show form.search-form {\\n    visibility: visible;\\n    width: 437px;\\n}\\n#header .header-search .collapse-search a .icon {\\n    background-color: #f8f9fa;\\n    font-size: 15px;\\n}\\n\\n.product-item:hover {\\n    background: #FFFFFF;\\n    box-shadow: 0 0 10px rgb(0 0 0 \\/ 15%);\\n    border-radius: 5px;\\n}\\n.product-item {\\n    text-align: center;\\n}\\n.product-item:hover .contact-now-btn {\\n    transform: translateY(-10px);\\n}"\n}', '2021-11-08 16:57:19', '2022-02-08 10:16:52'),
(0, 'adg_topal_xfec', 'config', 'config_theme_checkout_page', '{\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_sections', '{\n    "name": "Theme Config",\n    "version": "1.0",\n    "section": {\n        "header": {\n            "name": "header",\n            "text": "Header",\n            "visible": "1"\n        },\n        "slide-show": {\n            "name": "slide-show",\n            "text": "Slideshow",\n            "visible": "1"\n        },\n        "categories": {\n            "name": "categories",\n            "text": "Danh mục sản phẩm",\n            "visible": "1"\n        },\n        "hot-deals": {\n            "name": "hot-deals",\n            "text": "Sản phẩm khuyến mại",\n            "visible": "0"\n        },\n        "feature-products": {\n            "name": "feature-products",\n            "text": "Sản phẩm bán chạy",\n            "visible": "0"\n        },\n        "related-products": {\n            "name": "related-products",\n            "text": "Sản phẩm xem nhiều",\n            "visible": "1"\n        },\n        "new-products": {\n            "name": "new-products",\n            "text": "Sản phẩm mới",\n            "visible": "1"\n        },\n        "product-detail": {\n            "name": "product-details",\n            "text": "Chi tiết sản phẩm",\n            "visible": "1"\n        },\n        "banner": {\n            "name": "banner",\n            "text": "Banner",\n            "visible": "1"\n        },\n        "content_customize": {\n            "name": "content-customize",\n            "text": "Nội dung tùy chỉnh",\n            "visible": "1"\n        },\n        "partners": {\n            "name": "partners",\n            "text": "Đối tác",\n            "visible": "1"\n        },\n        "blog": {\n            "name": "blog",\n            "text": "Blog",\n            "visible": "1"\n        },\n        "rate": {\n            "name": "rate",\n            "text": "Đánh giá website",\n            "visible": "1"\n        },\n        "footer": {\n            "name": "footer",\n            "text": "Footer",\n            "visible": "1"\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-09 09:49:59'),
(0, 'adg_topal_xfec', 'config', 'config_section_banner', '{\n    "name": "banner",\n    "text": "Banner",\n    "visible": 1,\n    "display": [\n        {\n            "image-url": "/catalog/view/theme/default/image/banner/img-banner-06.jpg",\n            "url": "#",\n            "description": "Banner 1",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/default/image/banner/img-banner-02.png",\n            "url": "#",\n            "description": "Banner 2",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/default/image/banner/img-banner-03.png",\n            "url": "#",\n            "description": "Banner 3",\n            "visible": 1\n        }\n    ]\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_best_sales_product', '{\n    "name": "feature-product",\n    "text": "Sản phẩm bán chạy",\n    "visible": 1,\n    "setting": {\n        "title": "Sản phẩm bán chạy",\n        "sub_title" : "Mô tả tiêu đề",\n        "auto_retrieve_data": 1,\n        "resource_type": 1,\n        "collection_id": 1,\n        "category_id": 1,\n        "autoplay": 1,\n        "autoplay_time": 5,\n        "loop": 1\n    },\n    "display": {\n        "grid": {\n            "quantity": 4,\n            "row": 1\n        },\n        "grid_mobile": {\n            "quantity": 2,\n            "row": 1\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_new_product', '{\n    "name": "new-product",\n    "text": "Sản phẩm mới",\n    "visible": "1",\n    "setting": {\n        "title": "Sản phẩm mới",\n        "sub_title": "Mô tả tiêu đề",\n        "auto_retrieve_data": "0",\n        "resource_type": "2",\n        "collection_id": "1",\n        "category_id": "32",\n        "autoplay": "1",\n        "autoplay_time": "5",\n        "loop": "1"\n    },\n    "display": {\n        "grid": {\n            "quantity": "4",\n            "row": "1"\n        },\n        "grid_mobile": {\n            "quantity": "2",\n            "row": "1"\n        }\n    }\n}', '2021-11-08 16:57:19', '2022-01-19 13:54:24'),
(0, 'adg_topal_xfec', 'config', 'config_section_best_views_product', '{\n    "name": "related-product",\n    "text": "Sản phẩm xem nhiều",\n    "visible": 1,\n    "setting": {\n        "title": "Sản phẩm xem nhiều"\n    },\n    "display": {\n        "grid": {\n            "quantity": 10\n        },\n        "grid_mobile": {\n            "quantity": 2\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_blog', '{\n    "name": "blog",\n    "text": "Blog",\n    "visible": 1,\n    "setting": {\n        "title": "Blogs"\n    },\n    "display": {\n        "menu": [\n            {\n                "type": "existing",\n                "menu": {\n                    "id": 12,\n                    "name": "Danh sách bài viết 1"\n                }\n            },\n            {\n                "type": "manual",\n                "entries": [\n                    {\n                        "id": 10,\n                        "name": "Entry 10"\n                    },\n                    {\n                        "id": 11,\n                        "name": "Entry 11"\n                    }\n                ]\n            }\n        ],\n        "grid": {\n            "quantity": 3,\n            "row": 1\n        },\n        "grid_mobile": {\n            "quantity": 1,\n            "row": 1\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_detail_product', '{\n    "name": "product-detail",\n    "text": "Chi tiết sản phẩm",\n    "visible": 1,\n    "display": {\n        "name": true,\n        "description": false,\n        "price": true,\n        "price-compare": false,\n        "status": false,\n        "sale": false,\n        "rate": false\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_footer', '{\n    "name": "footer",\n    "text": "Footer",\n    "visible": "1",\n    "contact": {\n        "title": "Liên hệ chúng tôi",\n        "address": "37 Lê Văn Thiêm, Nhân Chính, Hà Nội&lt;br&gt;&lt;li&gt;Nhà máy: Km7, Đường 39, Thị trấn Yên Mỹ, Hưng Yên",\n        "phone-number": "1900 6828",\n        "email": "topal@austdoor.com",\n        "visible": "1"\n    },\n    "contact_more": [\n        {\n            "title": "Chi nhánh 1",\n            "address": "Duy Tan - Ha Noi",\n            "phone-number": "(+84)987654322",\n            "visible": "0"\n        }\n    ],\n    "collection": {\n        "title": "Bộ sưu tập",\n        "menu_id": "1",\n        "visible": "1"\n    },\n    "quick-links": {\n        "title": "ĐIỀU KHOẢN",\n        "menu_id": "9",\n        "visible": "1"\n    },\n    "subscribe": {\n        "title": "Đăng ký theo dõi",\n        "social_network": "1",\n        "visible": "1",\n        "youtube_visible": "1",\n        "facebook_visible": "1",\n        "instagram_visible": "1",\n        "shopee_visible": "1",\n        "tiki_visible": "1",\n        "sendo_visible": "1",\n        "lazada_visible": "1"\n    }\n}', '2021-11-08 16:57:19', '2021-12-24 10:15:59'),
(0, 'adg_topal_xfec', 'config', 'config_section_header', '{\n    "name": "header",\n    "text": "Header",\n    "visible": "1",\n    "notify-bar": {\n        "name": "Thanh thông báo",\n        "visible": "1",\n        "content": "Mua sắm online thuận tiện và dễ dàng",\n        "url": "#"\n    },\n    "logo": {\n        "name": "Logo",\n        "url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/logotopaltrang.png",\n        "alt": "alt",\n        "height": ""\n    },\n    "menu": {\n        "name": "Menu",\n        "display-list": {\n            "name": "Menu chính",\n            "id": "1"\n        }\n    }\n}', '2021-11-08 16:57:19', '2022-01-25 10:34:17'),
(0, 'adg_topal_xfec', 'config', 'config_section_hot_product', '{\n    "name": "hot-deals",\n    "text": "Sản phẩm khuyến mại",\n    "visible": 1,\n    "setting": {\n        "title": "Sản phẩm khuyến mại",\n        "sub_title" : "Mô tả tiêu đề",\n        "auto_retrieve_data": 1,\n        "resource_type": 1,\n        "collection_id": 1,\n        "category_id": 1,\n        "autoplay": 1,\n        "autoplay_time": 5,\n        "loop": 1\n    },\n    "display": {\n        "grid": {\n            "quantity": 4,\n            "row": 1\n        },\n        "grid_mobile": {\n            "quantity": 2,\n            "row": 1\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_list_product', '{\n    "name": "categories",\n    "text": "Danh mục sản phẩm",\n    "visible": 0,\n    "setting": {\n        "title": "Danh mục sản phẩm"\n    },\n    "display": {\n        "menu": {\n            "id": 1,\n            "name": "Danh mục sản phẩm 1"\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_partner', '{\n    "name": "partners",\n    "text": "Đối tác",\n    "visible": 1,\n    "limit-per-line": 4,\n    "display": [\n        {\n            "image-url": "/catalog/view/theme/novaon/asset/img/partner_1.jpg",\n            "url": "#",\n            "description": "Partner 1",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/novaon/asset/img/partner_2.jpg",\n            "url": "#",\n            "description": "Partner 2",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/novaon/asset/img/partner_3.jpg",\n            "url": "#",\n            "description": "Partner 3",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/novaon/asset/img/partner_4.jpg",\n            "url": "#",\n            "description": "Partner 4",\n            "visible": 1\n        }\n    ],\n    "setting": {\n        "autoplay": 1,\n        "loop": 1,\n        "autoplay_time": 5\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_slideshow', '{\n    "name": "slide-show",\n    "text": "Slideshow",\n    "visible": "1",\n    "setting": {\n        "transition-time": "5"\n    },\n    "display": [\n        {\n            "image-url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/home-slide-1.jpg",\n            "url": "#",\n            "description": "slide_1",\n            "type": "image",\n            "video-url": "",\n            "id": "47897615"\n        },\n        {\n            "image-url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/home-slide-2.jpg",\n            "url": "#",\n            "description": "slide_2",\n            "type": "image",\n            "video-url": "",\n            "id": "25763179"\n        }\n    ]\n}', '2021-11-08 16:57:19', '2021-11-08 17:02:37'),
(0, 'adg_topal_xfec', 'config', 'config_section_product_groups', '{\n    "name": "group products",\n    "text": "nhóm sản phẩm",\n    "visible": 1,\n    "list": [\n        {\n            "text": "Danh sách sản phẩm 1",\n            "visible": "0",\n            "setting": {\n                "title": "Danh sách sản phẩm 1",\n                "collection_id": "4",\n                "resource_type": 1,\n                "category_id": 1,\n                "sub_title": "Danh sách sản phẩm 1",\n                "autoplay": 1,\n                "autoplay_time": 5,\n                "loop": 1\n            },\n            "display": {\n                "grid": {\n                    "quantity": 6,\n                    "row": 1\n                },\n                "grid_mobile": {\n                    "quantity": 2,\n                    "row": 1\n                }\n            }\n        }\n    ]\n}', '2021-11-08 16:57:19', '2021-11-09 09:49:59'),
(0, 'adg_topal_xfec', 'config', 'config_section_content_customize', '{\n    "name": "content_customize",\n    "title": "Nội dung tuỳ chỉnh",\n    "display": [\n        {\n            "name": "mot-san-pham-cua-tap-doan-austdoor-0",\n            "title": "Một sản phẩm của tập đoàn Austdoor",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/group-1036_OnX4jQm.png",\n                "title": "Một sản phẩm của tập đoàn Austdoor",\n                "description": "Topal là thành viên của Tập đoàn Austdoor – đơn vị đã có 15 năm uy tín trong lĩnh vực cung cấp các giải pháp tổng thể về cửa tại Việt Nam.\\n\\n\\nQuy trình sản xuất chất lượng\\nSản phẩm nhôm Topal sản xuất ",\n                "html": ""\n            }\n        },\n        {\n            "name": "quy-trinh-san-xuat-chat-luong-1",\n            "title": "Quy trình sản xuất chất lượng",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/group-1035_dw3pgNY.png",\n                "title": "Quy trình sản xuất chất lượng",\n                "description": "Sản phẩm nhôm Topal sản xuất qua nhiều công đoạn với những tiêu chuẩn kỹ thuật khắt khe, kiểm soát chất lượng nghiêm ngặt.",\n                "html": ""\n            }\n        },\n        {\n            "name": "niem-no-than-thien-voi-khach-hang-2",\n            "title": "Niềm nở thân thiện với khách hàng",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/group-1037_mRg3X2Z.png",\n                "title": "Niềm nở thân thiện với khách hàng",\n                "description": "Không ngừng nỗ lực để thỏa mãn khách hàng. Chất lượng phục vụ là nền tảng để xây dựng thương hiệu vững mạnh.",\n                "html": ""\n            }\n        },\n        {\n            "name": "san-pham-da-dang-dich-vu-chat-luong-tot-nhat-3",\n            "title": "Sản phẩm đa dạng dịch vụ chất lượng tốt nhất",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/group-1034_NCpQukk.png",\n                "title": "Sản phẩm đa dạng dịch vụ chất lượng tốt nhất",\n                "description": "Các sản phẩm phong phú, đa dạng. Đáp ứng đầy đủ nhu cầu của khách hàng trong xây dựng các công trình.",\n                "html": ""\n            }\n        },\n        {\n            "name": "cung-ung-vuot-troi-4",\n            "title": "Cung ứng vượt trội",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/tools-1.png",\n                "title": "Cung ứng vượt trội",\n                "description": "Cung ứng lớn hàng trăm nghìn tấn nhôm\\/năm",\n                "html": ""\n            }\n        },\n        {\n            "name": "cham-soc-khach-hang-5",\n            "title": "Chăm sóc khách hàng",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/guarantee-1.png",\n                "title": "Chăm sóc khách hàng",\n                "description": "Tổng đài chăm sóc khách hàng 19006828 toàn quốc",\n                "html": ""\n            }\n        },\n        {\n            "name": "dong-hanh-cung-dai-ly-6",\n            "title": "Đồng hành cùng đại lý",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/group-1_1dpmxxf.png",\n                "title": "Đồng hành cùng đại lý",\n                "description": "Ưu đãi về kinh doanh, marketing , hỗ trợ các nguồn lực",\n                "html": ""\n            }\n        },\n        {\n            "name": "bao-hanh-7",\n            "title": "Bảo hành",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopalxfec\\/group.png",\n                "title": "Bảo hành",\n                "description": "Bảo hành thanh nhôm hệ và phụ kiện dành cho người tiêu dùng",\n                "html": ""\n            }\n        }\n    ]\n}', '2021-11-08 16:57:19', '2021-12-20 13:35:39'),
(0, 'adg_topal_xfec', 'config', 'config_section_customize_layout', '{\n    "config_theme_color": {\n        "main": {\n            "main": {\n                "hex": "#DA3A36",\n                "visible": "1"\n            },\n            "button": {\n                "hex": "#DA3A36",\n                "visible": "1"\n            }\n        }\n    },\n    "config_theme_text": {\n        "all": {\n            "title": "Font Roboto",\n            "font": "Roboto",\n            "supported-fonts": [\n                "Open Sans",\n                "IBM Plex Sans",\n                "Courier New",\n                "Roboto",\n                "Nunito",\n                "Arial",\n                "DejaVu Sans",\n                "Tahoma",\n                "Time News Roman",\n                "Mulish"\n            ]\n        }\n    },\n    "image_size_suggestion": {\n        "favicon": [\n            "35",\n            "35"\n        ],\n        "logo": [\n            "146",\n            "28"\n        ],\n        "home": {\n            "banner_1": [\n                "1240",\n                "500"\n            ],\n            "banner_2": [\n                "360",\n                "205"\n            ],\n            "banner_3": [\n                "555",\n                "210"\n            ],\n            "slide_show": [\n                "1905",\n                "770"\n            ]\n        },\n        "category": {\n            "banner_1": [\n                "263",\n                "307"\n            ]\n        }\n    },\n    "app_config_theme": [\n        {\n            "name": "Trang chủ",\n            "value": "home_page",\n            "position": [\n                {\n                    "name": "Đầu trang",\n                    "value": "bestme_app_after_header"\n                },\n                {\n                    "name": "Dưới slide",\n                    "value": "bestme_app_after_slide"\n                },\n                {\n                    "name": "Dưới danh sách sản phẩm",\n                    "value": "bestme_app_after_product"\n                },\n                {\n                    "name": "Trên cuối trang",\n                    "value": "bestme_app_before_footer"\n                }\n            ]\n        },\n        {\n            "name": "Sản phẩm",\n            "value": "product",\n            "position": [\n                {\n                    "name": "Đầu trang",\n                    "value": "bestme_app_after_header"\n                },\n                {\n                    "name": "Dưới danh sách sản phẩm",\n                    "value": "bestme_app_after_product"\n                },\n                {\n                    "name": "Trên cuối trang",\n                    "value": "bestme_app_before_footer"\n                }\n            ]\n        }\n    ],\n    "default_customize_layout": [\n        {\n            "name": "txt_slide_show_banner",\n            "file": "slideshow-banner"\n        },\n        {\n            "name": "txt_about_us",\n            "file": "about-us"\n        },\n        {\n            "name": "txt_why_choose",\n            "file": "why-choose"\n        },\n        {\n            "name": "txt_service",\n            "file": "service"\n        },\n        {\n            "name": "txt_block_product",\n            "file": "block-products"\n        },\n        {\n            "name": "txt_block_projects",\n            "file": "projects-box"\n        },\n        {\n            "name": "txt_intro_award",\n            "file": "intro-award"\n        },\n        {\n            "name": "txt_block_news",\n            "file": "news-box"\n        }\n    ],\n    "version": "1"\n}', '2021-11-08 16:57:19', '2021-11-08 17:39:43'),
(0, 'adg_topal_xfec', 'config', 'config_section_category_sections', '{\n    "name": "Theme Config",\n    "version": "1.0",\n    "section": {\n        "banner": {\n            "name": "banner",\n            "text": "Banner",\n            "visible": 1\n        },\n        "filter": {\n            "name": "filter",\n            "text": "Filter",\n            "visible": 1\n        },\n        "product_category": {\n            "name": "product_category",\n            "text": "Product Category",\n            "visible": 1\n        },\n        "product_list": {\n            "name": "product_list",\n            "text": "Product List",\n            "visible": 1\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_category_banner', '{\n    "name": "banner",\n    "text": "Banner",\n    "visible": 1,\n    "display": [\n        {\n            "image-url": "/catalog/view/theme/default/image/banner/img-banner-06.jpg",\n            "url": "#",\n            "description": "Banner 1",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/default/image/banner/img-banner-02.png",\n            "url": "#",\n            "description": "Banner 2",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/default/image/banner/img-banner-03.png",\n            "url": "#",\n            "description": "Banner 3",\n            "visible": 1\n        }\n    ]\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_category_filter', '{\n    "name": "filter",\n    "text": "Bộ lọc",\n    "visible": 1,\n    "setting": {\n        "title": "Bộ lọc"\n    },\n    "display": {\n        "supplier": {\n            "title": "Nhà cung cấp",\n            "visible": 1\n        },\n        "product-type": {\n            "title": "Loại sản phẩm",\n            "visible": 1\n        },\n        "collection": {\n            "title": "Bộ sưu tập",\n            "visible": 1\n        },\n        "property": {\n            "title": "Lọc theo tt - k dung",\n            "visible": 1,\n            "prop": [\n                "all",\n                "color",\n                "weight",\n                "size"\n            ]\n        },\n        "product-price": {\n            "title": "Giá sản phẩm",\n            "visible": 1,\n            "range": {\n                "from": 0,\n                "to": 100000000\n            }\n        },\n        "tag": {\n            "title": "Tag",\n            "visible": 1\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_category_product_category', '{\n    "name": "product-category",\n    "text": "Danh mục sản phẩm",\n    "visible": 1,\n    "setting": {\n        "title": "Danh mục sản phẩm"\n    },\n    "display": {\n        "menu": {\n            "id": 1,\n            "name": "Danh mục sản phẩm 1"\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_category_product_list', '{\n    "name": "product-list",\n    "text": "Danh sách sản phẩm",\n    "visible": 1,\n    "setting": {\n        "title": "Danh sách sản phẩm"\n    },\n    "display": {\n        "grid": {\n            "quantity": 10\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_product_detail_sections', '{\n    "name": "Theme Config",\n    "version": "1.0",\n    "section": {\n        "related_product": {\n            "name": "related_product",\n            "text": "Related Product",\n            "visible": 1\n        },\n        "template": {\n            "name": "template",\n            "text": "Template",\n            "visible": 1\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_product_detail_related_product', '{\n    "name": "related-product",\n    "text": "Sản phẩm liên quan",\n    "visible": "1",\n    "setting": {\n        "title": "Sản phẩm liên quan",\n        "auto_retrieve_data": "0",\n        "resource_type": "2",\n        "collection_id": "1",\n        "category_id": "32",\n        "autoplay": "1",\n        "autoplay_time": "5",\n        "loop": "1"\n    },\n    "display": {\n        "grid": {\n            "quantity": "4"\n        },\n        "grid_mobile": {\n            "quantity": "2"\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-09 09:50:48'),
(0, 'adg_topal_xfec', 'config', 'config_section_product_detail_template', '{\n    "name": "template",\n    "text": "Giao diện",\n    "visible": 1,\n    "display": {\n        "template": {\n            "id": 10\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_blog_sections', '{\n    "name": "Theme Config",\n    "version": "1.0",\n    "section": {\n        "blog_category": {\n            "name": "blog_category",\n            "text": "Blog Category",\n            "visible": 1\n        },\n        "blog_list": {\n            "name": "blog_list",\n            "text": "Blog List",\n            "visible": 1\n        },\n        "latest_blog": {\n            "name": "latest_blog",\n            "text": "Latest Blog",\n            "visible": 1\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_blog_blog_category', '{\n    "name": "blog-category",\n    "text": "Danh mục bài viết",\n    "visible": 1,\n    "setting": {\n        "title": "Danh mục bài viết"\n    },\n    "display": {\n        "menu": {\n            "id": 1,\n            "name": "Danh mục bài viết 1"\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_blog_blog_list', '{\n    "name": "blog-list",\n    "text": "Danh sách bài viết",\n    "visible": 1,\n    "display": {\n        "grid": {\n            "quantity": 20\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_blog_latest_blog', '{\n    "name": "latest-blog",\n    "text": "Bài viết mới nhất",\n    "visible": 1,\n    "setting": {\n        "title": "Bài viết mới nhất"\n    },\n    "display": {\n        "grid": {\n            "quantity": 10\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_contact_sections', '{\n    "name": "Theme Config",\n    "version": "1.0",\n    "section": {\n        "map": {\n            "name": "map",\n            "text": "Bản đồ",\n            "address": "<iframe src=\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\" width=\\"600\\" height=\\"450\\" frameborder=\\"0\\" style=\\"border:0\\" allowfullscreen></iframe>",\n            "visible": 1\n        },\n        "info": {\n            "name": "info",\n            "text": "Thông tin cửa hàng",\n            "email": "contact-us@novaon.asia",\n            "phone": "0000000000",\n            "address": "address",\n            "visible": 1\n        },\n        "form": {\n            "name": "form",\n            "title": "Form liên hệ",\n            "email": "email@mail.com",\n            "visible": 1\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_contact_map', '{   //////// CÁI NÀY KHÔNG DÙNG NỮA\n    "name": "map",\n    "text": "Bản đồ",\n    "visible": 1,\n    "setting": {\n        "title": "Bản đồ"\n    },\n    "display": {\n        "address": "<iframe src=\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\" width=\\"600\\" height=\\"450\\" frameborder=\\"0\\" style=\\"border:0\\" allowfullscreen></iframe>"\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_contact_contact', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\n    "name": "contact",\n    "text": "Liên hệ",\n    "visible": 1,\n    "setting": {\n        "title": "Vị trí của chúng tôi"\n    },\n    "display": {\n        "store": {\n            "title": "Gian hàng",\n            "content": "Số 12 Dịch Vọng Hậu, Cầu Giấy, Hà Nội",\n            "visible": 1\n        },\n        "telephone": {\n            "title": "Điện thoại",\n            "content": "(+84) 987 654 321",\n            "visible": 1\n        },\n        "social_follow": {\n            "socials": [\n                {\n                    "name": "facebook",\n                    "text": "Facebook",\n                    "visible": 1\n                },\n                {\n                    "name": "twitter",\n                    "text": "Twitter",\n                    "visible": 1\n                },\n                {\n                    "name": "instagram",\n                    "text": "Instagram",\n                    "visible": 1\n                },\n                {\n                    "name": "tumblr",\n                    "text": "Tumblr",\n                    "visible": 1\n                },\n                {\n                    "name": "youtube",\n                    "text": "Youtube",\n                    "visible": 1\n                },\n                {\n                    "name": "googleplus",\n                    "text": "Google Plus",\n                    "visible": 1\n                }\n            ],\n            "visible": 1\n        }\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_contact_form', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\n    "name": "form",\n    "text": "Biểu mẫu",\n    "visible": 1,\n    "setting": {\n        "title": "Liên hệ với chúng tôi"\n    },\n    "data": {\n        "email": "sale.247@xshop.com"\n    }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19'),
(0, 'adg_topal_xfec', 'config', 'config_section_rate', '{\n  "name": "rate",\n  "text": "Đánh giá website",\n  "visible": 1,\n  "setting": {\n    "title": "Đánh giá website",\n    "sub_title" : "Mô tả tiêu đề",\n    "auto_retrieve_data": 1,\n    "resource_type": 1,\n    "autoplay": 1,\n    "autoplay_time": 5,\n    "loop": 1\n  },\n  "display": {\n    "grid": {\n      "quantity": 3,\n      "row": 1\n    },\n    "grid_mobile": {\n      "quantity": 1,\n      "row": 1\n    }\n  }\n}', '2021-11-08 16:57:19', '2021-11-08 16:57:19');

-- oc_blog PRIMARY KEY (`blog_id`)
INSERT IGNORE INTO `oc_blog` (`blog_id`, `author`, `status`, `demo`, `source`, `date_publish`, `date_added`, `date_modified`) VALUES
(1, 1, 1, 1, NULL, '2020-06-10 11:51:00', '2020-06-10 11:51:00', '2021-12-06 13:37:32'),
(2, 1, 1, 1, NULL, '2020-06-10 11:55:33', '2020-06-10 11:55:33', '2021-12-02 16:57:31'),
(4, 1, 1, 1, NULL, '2021-11-08 17:34:04', '2021-11-08 17:34:04', '2021-11-26 11:13:49'),
(5, 1, 1, 1, NULL, '2021-11-08 17:35:08', '2021-11-08 17:35:08', '2021-11-26 11:14:20'),
(6, 1, 1, 1, NULL, '2021-11-08 17:35:56', '2021-11-08 17:35:56', '2021-11-26 11:14:15'),
(7, 1, 1, 1, NULL, '2021-11-08 17:37:01', '2021-11-08 17:37:01', '2021-11-26 11:28:14'),
(8, 1, 1, 1, NULL, '2021-11-08 17:37:24', '2021-11-08 17:37:24', '2021-11-26 11:14:41'),
(9, 1, 1, 1, NULL, '2021-11-08 17:38:22', '2021-11-08 17:38:22', '2021-11-26 11:26:16'),
(10, 1, 1, 1, NULL, '2021-11-08 17:41:01', '2021-11-08 17:41:01', '2021-11-24 10:43:59'),
(11, 1, 1, 1, NULL, '2021-11-08 17:41:31', '2021-11-08 17:41:31', '2021-11-24 10:42:31'),
(12, 1, 1, 1, NULL, '2021-11-08 17:42:01', '2021-11-08 17:42:01', '2021-11-24 10:51:53'),
(13, 1, 1, 1, NULL, '2021-11-08 17:43:05', '2021-11-08 17:43:05', '2021-11-24 10:39:14'),
(14, 1, 1, 1, NULL, '2021-11-08 17:43:56', '2021-11-08 17:43:56', '2021-11-24 10:35:16'),
(15, 1, 1, 1, NULL, '2021-11-08 17:44:49', '2021-11-08 17:44:49', '2021-11-24 10:33:23'),
(16, 1, 1, 1, NULL, '2021-11-24 10:58:58', '2021-11-24 10:58:58', '2021-11-24 10:58:58'),
(17, 1, 1, 1, NULL, '2021-11-24 13:57:14', '2021-11-24 13:57:14', '2021-11-24 14:00:49'),
(18, 1, 1, 1, NULL, '2021-11-24 13:59:55', '2021-11-24 13:59:55', '2021-11-24 13:59:55'),
(19, 1, 1, 1, NULL, '2021-11-24 14:02:57', '2021-11-24 14:02:57', '2021-11-24 14:02:57'),
(20, 1, 1, 1, NULL, '2021-11-24 14:05:21', '2021-11-24 14:05:21', '2021-11-24 14:05:21'),
(21, 1, 1, 1, NULL, '2021-11-26 10:56:21', '2021-11-26 10:56:21', '2021-11-26 11:41:34'),
(22, 1, 1, 1, NULL, '2021-11-26 10:57:17', '2021-11-26 10:57:17', '2021-11-26 11:42:28'),
(23, 1, 1, 1, NULL, '2021-11-26 11:15:36', '2021-11-26 11:15:36', '2021-11-26 11:42:19'),
(24, 1, 1, 1, NULL, '2021-11-26 11:30:54', '2021-11-26 11:30:54', '2021-11-26 11:41:51');

-- oc_blog_category PRIMARY KEY (`blog_category_id`)
INSERT IGNORE INTO `oc_blog_category` (`blog_category_id`, `status`, `source`, `date_added`, `date_modified`) VALUES
(1, 1, NULL, '2019-11-11 13:37:51', '2021-11-24 10:19:29'),
(5, 1, NULL, '2021-11-08 17:31:28', '2021-11-08 17:31:28'),
(3, 1, NULL, '2021-11-08 17:29:21', '2021-11-08 17:29:21'),
(4, 1, NULL, '2021-11-08 17:29:35', '2021-11-08 17:29:35'),
(6, 1, NULL, '2021-11-24 13:13:39', '2021-11-24 14:07:25'),
(7, 1, NULL, '2021-12-02 16:56:04', '2021-12-02 16:56:04');

-- oc_blog_category_description PRIMARY KEY (`blog_category_id`,`language_id`)
INSERT IGNORE INTO `oc_blog_category_description` (`blog_category_id`, `language_id`, `title`, `meta_title`, `meta_description`, `alias`) VALUES
(1, 1, 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'danh-muc-mac-dinh-uncategorized'),
(1, 2, 'Tin tức ADG', 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'danh-muc-mac-dinh-uncategorized'),
(5, 2, 'projects', '', '', 'projects'),
(3, 2, 'Tin tức Đại lý', '', '', 'tin-tuc-dai-ly'),
(4, 2, 'Khuyến mại', '', '', 'khuyen-mai'),
(6, 2, 'tin nổi bật', '', '', 'tin-noi-bat'),
(7, 2, 'giới thiệu', '', '', 'gioi-thieu');

-- oc_blog_description PRIMARY KEY (`blog_id`,`language_id`)
INSERT IGNORE INTO `oc_blog_description` (`blog_id`, `language_id`, `title`, `content`, `short_content`, `image`, `meta_title`, `meta_description`, `seo_keywords`, `alias`, `alt`, `type`, `video_url`, `source_author_name`) VALUES
(1, 2, 'Giới thiệu', '&lt;div class=&quot;page-content&quot; style=&quot;\r\n&quot;&gt;\r\n&lt;div class=&quot;full-width&quot; style=&quot;width:414px !important;max-width:414px !important;margin-left:-20px;position:relative&quot;&gt;\r\n&lt;div class=&quot;pb-lg-40&quot; style=&quot;background: #F4F4F4E5;&quot;&gt;\r\n&lt;div class=&quot;container&quot; style=&quot;\r\n&quot;&gt;\r\n&lt;h2 class=&quot;mb-lg-20 mb-15 text-uppercase text-center&quot; style=&quot;\r\n    padding-top: 30px;\r\n&quot;&gt;Giới thiệu về chúng tôi&lt;/h2&gt;\r\n\r\n&lt;div class=&quot;mb-lg-3 line-percent&quot;&gt;&lt;/div&gt;\r\n\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-6&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://nguyenductan.net/novaon/bestme/theme-5/img/about-1.jpg&quot; width=&quot;100%&quot;&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col-lg-6 d-flex&quot;&gt;\r\n&lt;div class=&quot;align-self-center&quot;&gt;\r\n&lt;p class=&quot;mb-4&quot;&gt;Thành lập từ năm 2003, Tập đoàn Austdoor sở hữu các thương hiệu nổi tiếng trong ngành cửa và vật liệu xây dựng như cửa cuốn AUSTDOOR, cửa nhựa, cửa nhôm &amp;amp; vách kính SUNSPACE, cửa gỗ HUGE, trung tâm dịch vụ, bảo hành cửa cuốn AUSTCARE, Nhôm hệ &amp;amp; phụ kiện TOPAL. Là đơn vị tiên phong du nhập loại cửa cuốn thông mình từ Australia, mở ra một trang mới trong lĩnh vực sản xuất &amp;amp; cung cấp cửa cuốn, đến nay Tập đoàn Austdoor đang là đơn vị dẫn đầu trong lĩnh vực cung cấp các giải pháp tổng thể về cửa tại thị trường Việt Nam. Với thương hiệu TOPAL, Austdoor hứa hẹn sẽ tạo nên những chuẩn mực giá trị khác biệt mới trong ngành vật liệu xây dựng nói chung và ngành sản xuất cửa nhôm nói riêng.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End container --&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End full-width --&gt;\r\n\r\n&lt;section class=&quot;py-50&quot; style=&quot;\r\n    padding-top: 0 !important;\r\n&quot;&gt;\r\n&lt;div class=&quot;row pt-lg-15&quot;&gt;\r\n&lt;div class=&quot;col-lg-6 d-flex flex-column justify-content-center&quot;&gt;\r\n&lt;h2 class=&quot;mb-lg-20 mb-15 text-uppercase text-center&quot; style=&quot;\r\n    padding-top: 30px;\r\n&quot;&gt;TẦM NHÌN&lt;/h2&gt;\r\n\r\n&lt;div class=&quot;mb-lg-3 line-percent&quot;&gt;&lt;/div&gt;\r\n&lt;p&quot;&gt;Là thành viên của Tập đoàn Austdoor, với sự đầu tư bài bản, Nhôm Topal đặt mục tiêu trở thành nhà sản xuất nhôm hàng đầu Việt Nam, đem đến những chuẩn mực mới trong ngành nhôm xây dựng. &lt;/p&quot;&gt;&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col-lg-3 col-6&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://nguyenductan.net/novaon/bestme/theme-5/img/about-2.jpg&quot; width=&quot;100%&quot;&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col-lg-3 col-6&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://nguyenductan.net/novaon/bestme/theme-5/img/about-3.jpg&quot; width=&quot;100%&quot;&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/section&gt;\r\n\r\n&lt;div class=&quot;full-width&quot; style=&quot;width:414px !important;max-width:414px !important;margin-left:-20px;position:relative&quot;&gt;\r\n&lt;div class=&quot;pt-lg-50 pt-30 pb-lg-40&quot; style=&quot;background: url(https://nguyenductan.net/novaon/bestme/theme-5/img/about-bg-2.png) no-repeat center/cover fixed;&quot;&gt;\r\n&lt;div class=&quot;container py-lg-20&quot;&gt;\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-6 col-12&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://nguyenductan.net/novaon/bestme/theme-5/img/about-4.jpg&quot; width=&quot;100%&quot;&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col-lg-6 d-flex flex-column justify-content-center&quot;&gt;\r\n&lt;h2 class=&quot;mb-lg-20 mb-15 text-uppercase text-center&quot;&gt;SỨ MỆNH&lt;/h2&gt;\r\n\r\n&lt;div class=&quot;mb-lg-3 line-percent&quot;&gt;&lt;/div&gt;\r\n\r\n&lt;p&gt;Đem những sản phẩm nhôm chất lượng nhất đến tay người tiêu dùng thông qua hệ thống đại lý ủy quyền chính hãng và dịch vụ khách hàng chuyên nghiệp.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!--End row--&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;section class=&quot;py-50&quot; style=&quot;\r\n    padding-bottom: 0;\r\n&quot;&gt;\r\n&lt;h2 class=&quot;mb-lg-20 mb-15 text-uppercase text-center&quot;&gt;NHÀ MÁY SẢN XUẤT&lt;/h2&gt;\r\n\r\n&lt;div class=&quot;mb-lg-3 line-percent&quot;&gt;&lt;/div&gt;\r\n\r\n&lt;div class=&quot;row mt-lg-2&quot;&gt;\r\n&lt;div class=&quot;col-12 col-lg-6 mb-3&quot;&gt;&lt;img src=&quot;https://nguyenductan.net/novaon/bestme/theme-5/img/about-5.jpg&quot; width=&quot;100%&quot;&gt;&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col-12 col-lg-6 mb-3&quot;&gt;&lt;img src=&quot;https://nguyenductan.net/novaon/bestme/theme-5/img/about-6.jpg&quot; width=&quot;100%&quot;&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;p class=&quot;mb-lg-4&quot;&gt;Với hệ thống nhà máy trải dài Bắc – Trung – Nam, trong đó có 2 nhà máy nhôm tại Hưng Yên và Nhơn Trạch được đầu tư với tổng quy mô trên 25ha, TOPAL có quy trình sản xuất tiêu chuẩn, trang thiết bị hiện đại, khả năng cung ứng hàng trăm nghìn tấn nhôm mỗi năm cho thị trường toàn quốc. Với quy mô sản xuất lớn, hiện Topal có 3 dây chuyền sơn tĩnh điện trong đó có 1 dây chuyền hoàn toàn tự động với công nghệ sơn hiện đại nhất hiện nay. Đầu năm 2021, nhôm Topal mở rộng 11,000m2 nhà xưởng cho dây chuyền Anode hiện đại cho ra đời các sản phẩm nhôm Topal chất lượng cao mang tiêu chuẩn quốc tế như JIS H8601, JIS H8602,… với 3 dòng chính là Anode phun cát, Anode mờ và phủ ED.&lt;/p&gt;\r\n&lt;/section&gt;\r\n&lt;/div&gt;', '&lt;p&gt;Là thành viên của Tập đoàn Austdoor, Nhôm Topal đặt mục tiêu trở thành nhà sản xuất nhôm quy mô, chuyên nghiệp hàng đầu Đông Nam Á. Với hệ thống nhà máy trải dài Bắc – Trung – Nam, trong đó có 2 nhà máy nhôm tại Hưng Yên và Nhơn Trạch được đầu tư với tổng quy mô trên 25ha, TOPAL có quy trình sản xuất tiêu chuẩn, trang thiết bị hiện đại, khả năng cung ứng hàng trăm nghìn tấn nhôm mỗi năm cho thị trường toàn quốc. một cách nhanh chóng, thuận tiện, giảm chi phí vận chuyển và rút ngắn thời gian giao hàng.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/about-1.jpg', '', '', '', 'gioi-thieu-ve-chung-toi', '', 'image', '', NULL),
(2, 2, 'Thành tích và giải thưởng', '&lt;p&gt;Ra mắt từ năm 2017, chỉ sau 3 năm, Nhôm Topal vinh dự trở thành hãng nhôm trẻ nhất đạt danh hiệu Thương hiệu Quốc gia năm 2020.&lt;/p&gt;', '&lt;p&gt;Ra mắt từ năm 2017, chỉ sau 3 năm, Nhôm Topal vinh dự trở thành hãng nhôm trẻ nhất đạt danh hiệu Thương hiệu Quốc gia năm 2020.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/banner-2.jpg', '', '', '', 'thanh-tich-va-giai-thuong', '', 'image', '', NULL),
(4, 2, 'Khu đô thị Trần Anh Riverside', '&lt;div class=&quot;container&quot;&gt;\r\n&lt;div class=&quot;project-content py-3 mb-4&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/1_AcsfwoB.png&quot; style=&quot;max-width: 100%;&quot;&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Vừa qua, Phú Gia An vinh dự khi là đối tác trong mảng thi công lắp đặt cửa nhôm kính cho toàn bộ hệ thống khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh. Chúng tôi luôn tự tin mang lại sự hài lòng cho mọi khách hàng từ sản phẩm cho đến dịch vụ của mình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh vừa chính thức đi vào hoạt động những ngày qua, đây chính là một trong những đối tác quan trọng mà Phú Gia An hợp tác trong hạng mục thi công lắp đặt cửa nhôm kính. Toàn bộ hệ thống khu nghỉ dưỡng đều sử dụng các sản phẩm cửa nhôm kính do Phú Gia An cung cấp.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia bao gồm 2 toà nhà chính với mật độ xây dựng cửa nhôm kính lên tới 75%. Cùng tham khảo một số hình ảnh đã hoàn thiện của công trình này dưới đây:&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-6&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/2_Rk3ffU4.png&quot;&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/3_SCVHjaX.png&quot;&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n\r\n&lt;p class=&quot;text-center&quot;&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/4_rCSW4ec.png&quot;&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End project content --&gt;&lt;/div&gt;', '&lt;p&gt;Vừa qua, Phú Gia An vinh dự khi là đối tác trong mảng thi công lắp đặt cửa nhôm kính cho toàn bộ hệ thống khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh. Chúng tôi luôn tự tin mang lại sự hài lòng cho mọi khách hàng từ sản phẩm cho đến dịch vụ của mình.&lt;br /&gt;\r\n&lt;br /&gt;\r\n &lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/7.png', '', '', '', 'khu-do-thi-tran-anh-riverside', '', 'image', '', NULL),
(5, 2, 'Nhà máy Vinamilk', '&lt;div class=&quot;container&quot;&gt;\r\n&lt;div class=&quot;project-content py-3 mb-4&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/1_AcsfwoB.png&quot; style=&quot;max-width: 100%;&quot;&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Vừa qua, Phú Gia An vinh dự khi là đối tác trong mảng thi công lắp đặt cửa nhôm kính cho toàn bộ hệ thống khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh. Chúng tôi luôn tự tin mang lại sự hài lòng cho mọi khách hàng từ sản phẩm cho đến dịch vụ của mình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh vừa chính thức đi vào hoạt động những ngày qua, đây chính là một trong những đối tác quan trọng mà Phú Gia An hợp tác trong hạng mục thi công lắp đặt cửa nhôm kính. Toàn bộ hệ thống khu nghỉ dưỡng đều sử dụng các sản phẩm cửa nhôm kính do Phú Gia An cung cấp.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia bao gồm 2 toà nhà chính với mật độ xây dựng cửa nhôm kính lên tới 75%. Cùng tham khảo một số hình ảnh đã hoàn thiện của công trình này dưới đây:&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-6&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/2_Rk3ffU4.png&quot;&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/3_SCVHjaX.png&quot;&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n\r\n&lt;p class=&quot;text-center&quot;&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/4_rCSW4ec.png&quot;&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End project content --&gt;&lt;/div&gt;', '&lt;p&gt;Cửa nhôm kính hiện tại vẫn luôn là xu hướng lựa chọn của nhiều gia chủ trong mọi công trình của mình. Không chỉ mang lại nhưng ưu điểm tuyệt vời về công năng sử dụng mà cửa nhôm kính còn được đánh giá cao về tính thẩm mỹ mang lại cho không gian đó.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/4.png', '', '', '', 'nha-may-vinamilk', '', 'image', '', NULL),
(6, 2, 'Tòa nhà Viettel', '&lt;div class=&quot;container&quot;&gt;\r\n&lt;div class=&quot;project-content py-3 mb-4&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/1_AcsfwoB.png&quot; style=&quot;max-width: 100%;&quot;&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Vừa qua, Phú Gia An vinh dự khi là đối tác trong mảng thi công lắp đặt cửa nhôm kính cho toàn bộ hệ thống khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh. Chúng tôi luôn tự tin mang lại sự hài lòng cho mọi khách hàng từ sản phẩm cho đến dịch vụ của mình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh vừa chính thức đi vào hoạt động những ngày qua, đây chính là một trong những đối tác quan trọng mà Phú Gia An hợp tác trong hạng mục thi công lắp đặt cửa nhôm kính. Toàn bộ hệ thống khu nghỉ dưỡng đều sử dụng các sản phẩm cửa nhôm kính do Phú Gia An cung cấp.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia bao gồm 2 toà nhà chính với mật độ xây dựng cửa nhôm kính lên tới 75%. Cùng tham khảo một số hình ảnh đã hoàn thiện của công trình này dưới đây:&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-6&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/2_Rk3ffU4.png&quot;&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/3_SCVHjaX.png&quot;&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n\r\n&lt;p class=&quot;text-center&quot;&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/4_rCSW4ec.png&quot;&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End project content --&gt;&lt;/div&gt;', '&lt;p&gt;Cửa nhôm kính hiện tại vẫn luôn là xu hướng lựa chọn của nhiều gia chủ trong mọi công trình của mình. Không chỉ mang lại nhưng ưu điểm tuyệt vời về công năng sử dụng mà cửa nhôm kính còn được đánh giá cao về tính thẩm mỹ mang lại cho không gian đó.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/6.png', '', '', '', 'toa-nha-viettel', '', 'image', '', NULL),
(7, 2, 'Nhà máy nhựa Duy Tân', '&lt;div class=&quot;container&quot;&gt;\r\n&lt;div class=&quot;project-content py-3 mb-4&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/1_AcsfwoB.png&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Vừa qua, Phú Gia An vinh dự khi là đối tác trong mảng thi công lắp đặt cửa nhôm kính cho toàn bộ hệ thống khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh. Chúng tôi luôn tự tin mang lại sự hài lòng cho mọi khách hàng từ sản phẩm cho đến dịch vụ của mình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh vừa chính thức đi vào hoạt động những ngày qua, đây chính là một trong những đối tác quan trọng mà Phú Gia An hợp tác trong hạng mục thi công lắp đặt cửa nhôm kính. Toàn bộ hệ thống khu nghỉ dưỡng đều sử dụng các sản phẩm cửa nhôm kính do Phú Gia An cung cấp.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia bao gồm 2 toà nhà chính với mật độ xây dựng cửa nhôm kính lên tới 75%. Cùng tham khảo một số hình ảnh đã hoàn thiện của công trình này dưới đây:&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-6&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/2_Rk3ffU4.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/3_SCVHjaX.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n\r\n&lt;p class=&quot;text-center&quot;&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/4_rCSW4ec.png&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End project content --&gt;&lt;/div&gt;', '&lt;p&gt;Cửa nhôm kính hiện tại vẫn luôn là xu hướng lựa chọn của nhiều gia chủ trong mọi công trình của mình. Không chỉ mang lại nhưng ưu điểm tuyệt vời về công năng sử dụng mà cửa nhôm kính còn được đánh giá cao về tính thẩm mỹ mang lại cho không gian đó.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/8.png', '', '', '', 'nha-may-nhua-duy-tan-1', '', 'image', '', NULL),
(8, 2, 'Dự án tổ hợp TTM Vincom Hùng Vương và khách sạn 5 sao Vinpearl Huế', '&lt;div class=&quot;container&quot;&gt;\r\n&lt;div class=&quot;project-content py-3 mb-4&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/1_AcsfwoB.png&quot; style=&quot;max-width: 100%;&quot;&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Vừa qua, Phú Gia An vinh dự khi là đối tác trong mảng thi công lắp đặt cửa nhôm kính cho toàn bộ hệ thống khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh. Chúng tôi luôn tự tin mang lại sự hài lòng cho mọi khách hàng từ sản phẩm cho đến dịch vụ của mình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh vừa chính thức đi vào hoạt động những ngày qua, đây chính là một trong những đối tác quan trọng mà Phú Gia An hợp tác trong hạng mục thi công lắp đặt cửa nhôm kính. Toàn bộ hệ thống khu nghỉ dưỡng đều sử dụng các sản phẩm cửa nhôm kính do Phú Gia An cung cấp.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia bao gồm 2 toà nhà chính với mật độ xây dựng cửa nhôm kính lên tới 75%. Cùng tham khảo một số hình ảnh đã hoàn thiện của công trình này dưới đây:&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-6&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/2_Rk3ffU4.png&quot;&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/3_SCVHjaX.png&quot;&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n\r\n&lt;p class=&quot;text-center&quot;&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/4_rCSW4ec.png&quot;&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End project content --&gt;&lt;/div&gt;', '&lt;p&gt;Cửa nhôm kính hiện tại vẫn luôn là xu hướng lựa chọn của nhiều gia chủ trong mọi công trình của mình. Không chỉ mang lại nhưng ưu điểm tuyệt vời về công năng sử dụng mà cửa nhôm kính còn được đánh giá cao về tính thẩm mỹ mang lại cho không gian đó.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/2.png', '', '', '', 'du-an-to-hop-ttm-vincom-hung-vuong-va-khach-san-5-sao-vinpearl-hue', '', 'image', '', NULL),
(9, 2, 'Nhà máy HuaYuan Việt Nam', '&lt;div class=&quot;container&quot;&gt;\r\n&lt;div class=&quot;project-content py-3 mb-4&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/1_AcsfwoB.png&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Vừa qua, Phú Gia An vinh dự khi là đối tác trong mảng thi công lắp đặt cửa nhôm kính cho toàn bộ hệ thống khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh. Chúng tôi luôn tự tin mang lại sự hài lòng cho mọi khách hàng từ sản phẩm cho đến dịch vụ của mình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh vừa chính thức đi vào hoạt động những ngày qua, đây chính là một trong những đối tác quan trọng mà Phú Gia An hợp tác trong hạng mục thi công lắp đặt cửa nhôm kính. Toàn bộ hệ thống khu nghỉ dưỡng đều sử dụng các sản phẩm cửa nhôm kính do Phú Gia An cung cấp.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia bao gồm 2 toà nhà chính với mật độ xây dựng cửa nhôm kính lên tới 75%. Cùng tham khảo một số hình ảnh đã hoàn thiện của công trình này dưới đây:&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-6&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/2_Rk3ffU4.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/3_SCVHjaX.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n\r\n&lt;p class=&quot;text-center&quot;&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/4_rCSW4ec.png&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End project content --&gt;&lt;/div&gt;', '&lt;p&gt;Cửa nhôm kính hiện tại vẫn luôn là xu hướng lựa chọn của nhiều gia chủ trong mọi công trình của mình. Không chỉ mang lại nhưng ưu điểm tuyệt vời về công năng sử dụng mà cửa nhôm kính còn được đánh giá cao về tính thẩm mỹ mang lại cho không gian đó.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/5.png', '', '', '', 'nha-may-huayuan-viet-nam-1', '', 'image', '', NULL),
(10, 2, 'Top 4 mẫu cửa sổ được ưa chuộng nhất năm 2021', '&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/05/z2516244660207_810d77389b815bd726c1b0ae4ecb80e1-1024x768.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/05/z2516244660207_810d77389b815bd726c1b0ae4ecb80e1-1024x768.jpg 1024w, https://topal.vn/wp-content/uploads/2021/05/z2516244660207_810d77389b815bd726c1b0ae4ecb80e1-300x225.jpg 300w, https://topal.vn/wp-content/uploads/2021/05/z2516244660207_810d77389b815bd726c1b0ae4ecb80e1-768x576.jpg 768w, https://topal.vn/wp-content/uploads/2021/05/z2516244660207_810d77389b815bd726c1b0ae4ecb80e1-1536x1153.jpg 1536w, https://topal.vn/wp-content/uploads/2021/05/z2516244660207_810d77389b815bd726c1b0ae4ecb80e1-2048x1537.jpg 2048w, https://topal.vn/wp-content/uploads/2021/05/z2516244660207_810d77389b815bd726c1b0ae4ecb80e1-600x450.jpg 600w, https://topal.vn/wp-content/uploads/2021/05/z2516244660207_810d77389b815bd726c1b0ae4ecb80e1-195x146.jpg 195w, https://topal.vn/wp-content/uploads/2021/05/z2516244660207_810d77389b815bd726c1b0ae4ecb80e1-50x38.jpg 50w, https://topal.vn/wp-content/uploads/2021/05/z2516244660207_810d77389b815bd726c1b0ae4ecb80e1-100x75.jpg 100w, https://topal.vn/wp-content/uploads/2021/05/z2516244660207_810d77389b815bd726c1b0ae4ecb80e1-960x720.jpg 960w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Một không gian nhà ở được thiết kế một cách chỉnh chu và tính tế chính là sự kết hợp hài hòa và tỉ mỉ trong từng không gian, món đồ nội thất. Và ở bài viết này, Topal sẽ chia sẻ đến các bạn tổng hợp những mẫu cửa sổ được ưa chuộng nhất hiện nay.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Cửa sổ mở quay&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Là loại cửa truyền thống và ưa chuộng nhất với các công trình nhà ở. Cửa mở quay giúp mở rộng tối đa không gian lấy ánh sáng. Với thanh nhôm to khỏe, vững chãi cùng thanh nhôm dày dặn, cửa sổ mở quay Topal mang vẻ đẹp khỏe khoắn, sang trọng, thích hợp với những công trình nhà ở dân dụng cho đến những biệt thự hiện đại, cao cấp.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/05/z2516211818840_3d4c830fcfaf4b423ad6340c5de557b6-1024x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/05/z2516211818840_3d4c830fcfaf4b423ad6340c5de557b6-1024x1024.jpg 1024w, https://topal.vn/wp-content/uploads/2021/05/z2516211818840_3d4c830fcfaf4b423ad6340c5de557b6-300x300.jpg 300w, https://topal.vn/wp-content/uploads/2021/05/z2516211818840_3d4c830fcfaf4b423ad6340c5de557b6-150x150.jpg 150w, https://topal.vn/wp-content/uploads/2021/05/z2516211818840_3d4c830fcfaf4b423ad6340c5de557b6-768x768.jpg 768w, https://topal.vn/wp-content/uploads/2021/05/z2516211818840_3d4c830fcfaf4b423ad6340c5de557b6-1536x1536.jpg 1536w, https://topal.vn/wp-content/uploads/2021/05/z2516211818840_3d4c830fcfaf4b423ad6340c5de557b6-600x600.jpg 600w, https://topal.vn/wp-content/uploads/2021/05/z2516211818840_3d4c830fcfaf4b423ad6340c5de557b6-146x146.jpg 146w, https://topal.vn/wp-content/uploads/2021/05/z2516211818840_3d4c830fcfaf4b423ad6340c5de557b6-50x50.jpg 50w, https://topal.vn/wp-content/uploads/2021/05/z2516211818840_3d4c830fcfaf4b423ad6340c5de557b6-75x75.jpg 75w, https://topal.vn/wp-content/uploads/2021/05/z2516211818840_3d4c830fcfaf4b423ad6340c5de557b6-85x85.jpg 85w, https://topal.vn/wp-content/uploads/2021/05/z2516211818840_3d4c830fcfaf4b423ad6340c5de557b6-80x80.jpg 80w, https://topal.vn/wp-content/uploads/2021/05/z2516211818840_3d4c830fcfaf4b423ad6340c5de557b6.jpg 1876w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Bên cạnh đó, cửa mở quay Topal được khách hàng lựa chọn sử dụng vì có bản lề chữ A liên kết liền khối với cánh cửa, “tự chống sập” được làm bằng inox 304m, dày 3mm giúp tăng độ chắc chắn, an toàn khi sử dụng.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Cửa sổ mở hất (lật)&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Sở hữu thiết kế phong cách châu Âu tối giản và hiện đại, cửa sổ mở hất(hay còn gọi là cửa sổ mở lật) còn được yêu thích bởi thiết kế vừa thông thoáng vừa kín đáo, chống mưa hắt vào nhà. Mẫu cửa sổ nhôm kính này được dùng phổ biến bởi sự đa năng của nó. Không bị chiếm diện tích, linh hoạt trong quá trình sử dụng.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/05/z2516211742461_b498246581cca90d166daa659e126dea-1024x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/05/z2516211742461_b498246581cca90d166daa659e126dea-1024x1024.jpg 1024w, https://topal.vn/wp-content/uploads/2021/05/z2516211742461_b498246581cca90d166daa659e126dea-300x300.jpg 300w, https://topal.vn/wp-content/uploads/2021/05/z2516211742461_b498246581cca90d166daa659e126dea-150x150.jpg 150w, https://topal.vn/wp-content/uploads/2021/05/z2516211742461_b498246581cca90d166daa659e126dea-768x768.jpg 768w, https://topal.vn/wp-content/uploads/2021/05/z2516211742461_b498246581cca90d166daa659e126dea-1536x1536.jpg 1536w, https://topal.vn/wp-content/uploads/2021/05/z2516211742461_b498246581cca90d166daa659e126dea-600x600.jpg 600w, https://topal.vn/wp-content/uploads/2021/05/z2516211742461_b498246581cca90d166daa659e126dea-146x146.jpg 146w, https://topal.vn/wp-content/uploads/2021/05/z2516211742461_b498246581cca90d166daa659e126dea-50x50.jpg 50w, https://topal.vn/wp-content/uploads/2021/05/z2516211742461_b498246581cca90d166daa659e126dea-75x75.jpg 75w, https://topal.vn/wp-content/uploads/2021/05/z2516211742461_b498246581cca90d166daa659e126dea-85x85.jpg 85w, https://topal.vn/wp-content/uploads/2021/05/z2516211742461_b498246581cca90d166daa659e126dea-80x80.jpg 80w, https://topal.vn/wp-content/uploads/2021/05/z2516211742461_b498246581cca90d166daa659e126dea.jpg 1876w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/05/z2516211766538_9efb2bceaa1f852c92edba1975f7e2b7-1024x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/05/z2516211766538_9efb2bceaa1f852c92edba1975f7e2b7-1024x1024.jpg 1024w, https://topal.vn/wp-content/uploads/2021/05/z2516211766538_9efb2bceaa1f852c92edba1975f7e2b7-300x300.jpg 300w, https://topal.vn/wp-content/uploads/2021/05/z2516211766538_9efb2bceaa1f852c92edba1975f7e2b7-150x150.jpg 150w, https://topal.vn/wp-content/uploads/2021/05/z2516211766538_9efb2bceaa1f852c92edba1975f7e2b7-768x768.jpg 768w, https://topal.vn/wp-content/uploads/2021/05/z2516211766538_9efb2bceaa1f852c92edba1975f7e2b7-1536x1536.jpg 1536w, https://topal.vn/wp-content/uploads/2021/05/z2516211766538_9efb2bceaa1f852c92edba1975f7e2b7-600x600.jpg 600w, https://topal.vn/wp-content/uploads/2021/05/z2516211766538_9efb2bceaa1f852c92edba1975f7e2b7-146x146.jpg 146w, https://topal.vn/wp-content/uploads/2021/05/z2516211766538_9efb2bceaa1f852c92edba1975f7e2b7-50x50.jpg 50w, https://topal.vn/wp-content/uploads/2021/05/z2516211766538_9efb2bceaa1f852c92edba1975f7e2b7-75x75.jpg 75w, https://topal.vn/wp-content/uploads/2021/05/z2516211766538_9efb2bceaa1f852c92edba1975f7e2b7-85x85.jpg 85w, https://topal.vn/wp-content/uploads/2021/05/z2516211766538_9efb2bceaa1f852c92edba1975f7e2b7-80x80.jpg 80w, https://topal.vn/wp-content/uploads/2021/05/z2516211766538_9efb2bceaa1f852c92edba1975f7e2b7.jpg 1876w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Cửa sổ mở hất Topal được thiết kế đồng bộ từ thanh nhôm đến phụ kiện và gioăng, cho độ bền bỉ vượt trội. Cùng với đó, cửa sổ Topal Slima có thể đáp ứng đa dạng phương án kiểu dáng kiến trúc và nhiều lựa chọn phụ kiện đi kèm, đáp ứng tối đa nhu cầu khách hàng.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/05/z2516211750630_46a5606f7ede31a920d696aca6955091-1024x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/05/z2516211750630_46a5606f7ede31a920d696aca6955091-1024x1024.jpg 1024w, https://topal.vn/wp-content/uploads/2021/05/z2516211750630_46a5606f7ede31a920d696aca6955091-300x300.jpg 300w, https://topal.vn/wp-content/uploads/2021/05/z2516211750630_46a5606f7ede31a920d696aca6955091-150x150.jpg 150w, https://topal.vn/wp-content/uploads/2021/05/z2516211750630_46a5606f7ede31a920d696aca6955091-768x768.jpg 768w, https://topal.vn/wp-content/uploads/2021/05/z2516211750630_46a5606f7ede31a920d696aca6955091-1536x1536.jpg 1536w, https://topal.vn/wp-content/uploads/2021/05/z2516211750630_46a5606f7ede31a920d696aca6955091-600x600.jpg 600w, https://topal.vn/wp-content/uploads/2021/05/z2516211750630_46a5606f7ede31a920d696aca6955091-146x146.jpg 146w, https://topal.vn/wp-content/uploads/2021/05/z2516211750630_46a5606f7ede31a920d696aca6955091-50x50.jpg 50w, https://topal.vn/wp-content/uploads/2021/05/z2516211750630_46a5606f7ede31a920d696aca6955091-75x75.jpg 75w, https://topal.vn/wp-content/uploads/2021/05/z2516211750630_46a5606f7ede31a920d696aca6955091-85x85.jpg 85w, https://topal.vn/wp-content/uploads/2021/05/z2516211750630_46a5606f7ede31a920d696aca6955091-80x80.jpg 80w, https://topal.vn/wp-content/uploads/2021/05/z2516211750630_46a5606f7ede31a920d696aca6955091.jpg 1876w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Cửa sổ trượt / cửa lùa&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Cửa sổ trượt hay còn gọi là cửa sổ lùa được ưa chuộng sử dụng vì tiết kiệm diện tích khi sử dụng, đồng thời cũng hạn chế việc chiếm không gian khi mở cửa. Cộng với thiết kế mang tính hiện đại và năng động. Cửa sổ nhôm kính lùa xứng đáng được nhắc đến trong danh sách những mẫu cửa sổ nhôm kính đẹp năm 2021 này.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/05/z2516211814559_f5424524edbd1cd24205703d31f5892c-1024x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/05/z2516211814559_f5424524edbd1cd24205703d31f5892c-1024x1024.jpg 1024w, https://topal.vn/wp-content/uploads/2021/05/z2516211814559_f5424524edbd1cd24205703d31f5892c-300x300.jpg 300w, https://topal.vn/wp-content/uploads/2021/05/z2516211814559_f5424524edbd1cd24205703d31f5892c-150x150.jpg 150w, https://topal.vn/wp-content/uploads/2021/05/z2516211814559_f5424524edbd1cd24205703d31f5892c-768x768.jpg 768w, https://topal.vn/wp-content/uploads/2021/05/z2516211814559_f5424524edbd1cd24205703d31f5892c-1536x1536.jpg 1536w, https://topal.vn/wp-content/uploads/2021/05/z2516211814559_f5424524edbd1cd24205703d31f5892c-600x600.jpg 600w, https://topal.vn/wp-content/uploads/2021/05/z2516211814559_f5424524edbd1cd24205703d31f5892c-146x146.jpg 146w, https://topal.vn/wp-content/uploads/2021/05/z2516211814559_f5424524edbd1cd24205703d31f5892c-50x50.jpg 50w, https://topal.vn/wp-content/uploads/2021/05/z2516211814559_f5424524edbd1cd24205703d31f5892c-75x75.jpg 75w, https://topal.vn/wp-content/uploads/2021/05/z2516211814559_f5424524edbd1cd24205703d31f5892c-85x85.jpg 85w, https://topal.vn/wp-content/uploads/2021/05/z2516211814559_f5424524edbd1cd24205703d31f5892c-80x80.jpg 80w, https://topal.vn/wp-content/uploads/2021/05/z2516211814559_f5424524edbd1cd24205703d31f5892c.jpg 1876w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/05/z2516211690030_b81d3a00059be199bbd527762854021f-1024x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/05/z2516211690030_b81d3a00059be199bbd527762854021f-1024x1024.jpg 1024w, https://topal.vn/wp-content/uploads/2021/05/z2516211690030_b81d3a00059be199bbd527762854021f-300x300.jpg 300w, https://topal.vn/wp-content/uploads/2021/05/z2516211690030_b81d3a00059be199bbd527762854021f-150x150.jpg 150w, https://topal.vn/wp-content/uploads/2021/05/z2516211690030_b81d3a00059be199bbd527762854021f-768x768.jpg 768w, https://topal.vn/wp-content/uploads/2021/05/z2516211690030_b81d3a00059be199bbd527762854021f-1536x1536.jpg 1536w, https://topal.vn/wp-content/uploads/2021/05/z2516211690030_b81d3a00059be199bbd527762854021f-600x600.jpg 600w, https://topal.vn/wp-content/uploads/2021/05/z2516211690030_b81d3a00059be199bbd527762854021f-146x146.jpg 146w, https://topal.vn/wp-content/uploads/2021/05/z2516211690030_b81d3a00059be199bbd527762854021f-50x50.jpg 50w, https://topal.vn/wp-content/uploads/2021/05/z2516211690030_b81d3a00059be199bbd527762854021f-75x75.jpg 75w, https://topal.vn/wp-content/uploads/2021/05/z2516211690030_b81d3a00059be199bbd527762854021f-85x85.jpg 85w, https://topal.vn/wp-content/uploads/2021/05/z2516211690030_b81d3a00059be199bbd527762854021f-80x80.jpg 80w, https://topal.vn/wp-content/uploads/2021/05/z2516211690030_b81d3a00059be199bbd527762854021f.jpg 1876w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/05/z2516211728916_c0709b3cbf61d48a1251f49903368edd-1024x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/05/z2516211728916_c0709b3cbf61d48a1251f49903368edd-1024x1024.jpg 1024w, https://topal.vn/wp-content/uploads/2021/05/z2516211728916_c0709b3cbf61d48a1251f49903368edd-300x300.jpg 300w, https://topal.vn/wp-content/uploads/2021/05/z2516211728916_c0709b3cbf61d48a1251f49903368edd-150x150.jpg 150w, https://topal.vn/wp-content/uploads/2021/05/z2516211728916_c0709b3cbf61d48a1251f49903368edd-768x768.jpg 768w, https://topal.vn/wp-content/uploads/2021/05/z2516211728916_c0709b3cbf61d48a1251f49903368edd-1536x1536.jpg 1536w, https://topal.vn/wp-content/uploads/2021/05/z2516211728916_c0709b3cbf61d48a1251f49903368edd-600x600.jpg 600w, https://topal.vn/wp-content/uploads/2021/05/z2516211728916_c0709b3cbf61d48a1251f49903368edd-146x146.jpg 146w, https://topal.vn/wp-content/uploads/2021/05/z2516211728916_c0709b3cbf61d48a1251f49903368edd-50x50.jpg 50w, https://topal.vn/wp-content/uploads/2021/05/z2516211728916_c0709b3cbf61d48a1251f49903368edd-75x75.jpg 75w, https://topal.vn/wp-content/uploads/2021/05/z2516211728916_c0709b3cbf61d48a1251f49903368edd-85x85.jpg 85w, https://topal.vn/wp-content/uploads/2021/05/z2516211728916_c0709b3cbf61d48a1251f49903368edd-80x80.jpg 80w, https://topal.vn/wp-content/uploads/2021/05/z2516211728916_c0709b3cbf61d48a1251f49903368edd.jpg 1876w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Đặc biệt, Topal Prima có thể đáp ứng bộ cửa sổ trượt không giới hạn số cánh, giúp mở rộng tối đa tầm nhìn cho người sử dụng. Cùng với thiết kế mang thẩm mỹ cao cấp, cửa sổ trượt Topal Prima rất phù hợp với những biệt thự view lớn bên bờ biển.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Cửa sổ gấp trượt&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Là loại cửa mới xuất hiện trên thị trường vài năm gần đây nhưng được các chủ đầu tư lựa chọn sử dụng vì đây là loại cửa kết hợp ưu điểm của cửa lùa và cửa mở quay, cho khoảng mở tối đa mà vẫn đảm bảo khả năng kín nước, chống ồn.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/05/z2519931599281_efe5ad3a368252a03ef4a16765756248-1024x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/05/z2519931599281_efe5ad3a368252a03ef4a16765756248-1024x1024.jpg 1024w, https://topal.vn/wp-content/uploads/2021/05/z2519931599281_efe5ad3a368252a03ef4a16765756248-300x300.jpg 300w, https://topal.vn/wp-content/uploads/2021/05/z2519931599281_efe5ad3a368252a03ef4a16765756248-150x150.jpg 150w, https://topal.vn/wp-content/uploads/2021/05/z2519931599281_efe5ad3a368252a03ef4a16765756248-768x768.jpg 768w, https://topal.vn/wp-content/uploads/2021/05/z2519931599281_efe5ad3a368252a03ef4a16765756248-1536x1536.jpg 1536w, https://topal.vn/wp-content/uploads/2021/05/z2519931599281_efe5ad3a368252a03ef4a16765756248-600x600.jpg 600w, https://topal.vn/wp-content/uploads/2021/05/z2519931599281_efe5ad3a368252a03ef4a16765756248-146x146.jpg 146w, https://topal.vn/wp-content/uploads/2021/05/z2519931599281_efe5ad3a368252a03ef4a16765756248-50x50.jpg 50w, https://topal.vn/wp-content/uploads/2021/05/z2519931599281_efe5ad3a368252a03ef4a16765756248-75x75.jpg 75w, https://topal.vn/wp-content/uploads/2021/05/z2519931599281_efe5ad3a368252a03ef4a16765756248-85x85.jpg 85w, https://topal.vn/wp-content/uploads/2021/05/z2519931599281_efe5ad3a368252a03ef4a16765756248-80x80.jpg 80w, https://topal.vn/wp-content/uploads/2021/05/z2519931599281_efe5ad3a368252a03ef4a16765756248.jpg 1876w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Cấu tạo “cánh chờm cánh” giúp tăng độ kín khít so với các loại cửa gấp trượt thông thường . Ray chịu lực nằm dưới chịu lực tốt, giúp tăng độ vững chắc kết cấu, đảm bảo độ bền cho cửa . Phong cách thiết kế đồng bộ với cửa mở quay, vuông vức, khỏe khoắn, đem lại sự đồng bộ trong không gian công trình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Trên đây là 4 mẫu cửa sổ được ưa chuông nhất năm 2021. Quý khách hàng có nhu cầu tư vấn lắp đặt cửa, vui lòng gọi đến Hotline 19006828 hoặc tìm đại lý Topal gần nhất.&lt;/p&gt;', '&lt;p&gt;Một không gian nhà ở được thiết kế một cách chỉnh chu và tính tế chính là sự kết hợp hài hòa và tỉ mỉ trong từng không gian, món đồ nội thất. Và ở bài viết này, Topal sẽ chia sẻ đến các bạn tổng hợp những mẫu cửa sổ được ưa chuộng nhất hiện nay.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/z2516244660207_810d77389b815bd726c1b0ae4ecb80e1-1024x768.jpg', '', '', '', 'top-4-mau-cua-so-duoc-ua-chuong-nhat-nam-2021', '', 'image', '', NULL),
(11, 2, 'Topal mách bạn cách chọn mẫu cửa nhôm phù hợp nhất cho phong cách cổ điển', '&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/06/z2534075541511_9daac93a01600aac11cfa91e264e310a-1024x768.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/06/z2534075541511_9daac93a01600aac11cfa91e264e310a-1024x768.jpg 1024w, https://topal.vn/wp-content/uploads/2021/06/z2534075541511_9daac93a01600aac11cfa91e264e310a-300x225.jpg 300w, https://topal.vn/wp-content/uploads/2021/06/z2534075541511_9daac93a01600aac11cfa91e264e310a-768x576.jpg 768w, https://topal.vn/wp-content/uploads/2021/06/z2534075541511_9daac93a01600aac11cfa91e264e310a-1536x1152.jpg 1536w, https://topal.vn/wp-content/uploads/2021/06/z2534075541511_9daac93a01600aac11cfa91e264e310a-2048x1536.jpg 2048w, https://topal.vn/wp-content/uploads/2021/06/z2534075541511_9daac93a01600aac11cfa91e264e310a-600x450.jpg 600w, https://topal.vn/wp-content/uploads/2021/06/z2534075541511_9daac93a01600aac11cfa91e264e310a-195x146.jpg 195w, https://topal.vn/wp-content/uploads/2021/06/z2534075541511_9daac93a01600aac11cfa91e264e310a-50x38.jpg 50w, https://topal.vn/wp-content/uploads/2021/06/z2534075541511_9daac93a01600aac11cfa91e264e310a-100x75.jpg 100w, https://topal.vn/wp-content/uploads/2021/06/z2534075541511_9daac93a01600aac11cfa91e264e310a-960x720.jpg 960w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Đối với phong cách kiến trúc cổ điển, bạn sẽ dễ dàng nhận thấy những chi tiết hoa văn chạm trổ nơi tường, ban công, hoặc các vị trí khác trong không gian nội thất. Cũng vì vậy mà trong hạng mục cửa, việc chọn sử dụng những bộ cửa sao cho phù hợp phong cách được xem là bài toán khó.&lt;/p&gt;\r\n\r\n&lt;p&gt;Nếu chỉ sử dụng gỗ truyền thống thì không còn gì đặc biệt để chúng ta đề cập đến. Nhưng với chất liệu có tải trọng nhẹ, độ bền cao, quan trọng nhất là kiểu dáng phù hợp, cửa đi nhôm Topal lại là một “ứng cử viên” tuyệt vời. Cùng Topal tham khảo cách lựa chọn mẫu cửa đi nhôm nào phù hợp cho lối kiến trúc cổ điển nhé !&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Cửa uốn vòm cho phong cách thêm sang trọng&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Được chọn sử dụng nhiều nhất cho hạng mục cửa trong không gian nội thất cổ điển, cửa uốn vòm được đánh giá là mẫu cửa có thể liên tưởng đễn những lối kiến trúc cổ điển sang trọng bậc nhất từ châu Âu. Mẫu cửa nhôm Topal uốn vòm là sự kết hợp của vật liệu hiện đại và kiểu dáng truyền thống sang trọng. Cửa có phần uốn vòm ở vị trí ô cố định hoặc uốn vòm cả phần cánh. Ngoài ra, khách hàng cũng có thể lựa chọn sử dụng thêm chi tiết chia đố cho bộ cửa, các chi tiết này giúp tối ưu giá trị thẩm mĩ cho toàn bộ công trình nói chung, cũng như đối với bộ cửa đi nhôm nói riêng.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/06/z2534075501953_fbb392a4ba72a0f859018f440a79f45d-1024x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/06/z2534075501953_fbb392a4ba72a0f859018f440a79f45d-1024x1024.jpg 1024w, https://topal.vn/wp-content/uploads/2021/06/z2534075501953_fbb392a4ba72a0f859018f440a79f45d-300x300.jpg 300w, https://topal.vn/wp-content/uploads/2021/06/z2534075501953_fbb392a4ba72a0f859018f440a79f45d-150x150.jpg 150w, https://topal.vn/wp-content/uploads/2021/06/z2534075501953_fbb392a4ba72a0f859018f440a79f45d-768x768.jpg 768w, https://topal.vn/wp-content/uploads/2021/06/z2534075501953_fbb392a4ba72a0f859018f440a79f45d-1536x1536.jpg 1536w, https://topal.vn/wp-content/uploads/2021/06/z2534075501953_fbb392a4ba72a0f859018f440a79f45d-2048x2048.jpg 2048w, https://topal.vn/wp-content/uploads/2021/06/z2534075501953_fbb392a4ba72a0f859018f440a79f45d-600x600.jpg 600w, https://topal.vn/wp-content/uploads/2021/06/z2534075501953_fbb392a4ba72a0f859018f440a79f45d-146x146.jpg 146w, https://topal.vn/wp-content/uploads/2021/06/z2534075501953_fbb392a4ba72a0f859018f440a79f45d-50x50.jpg 50w, https://topal.vn/wp-content/uploads/2021/06/z2534075501953_fbb392a4ba72a0f859018f440a79f45d-75x75.jpg 75w, https://topal.vn/wp-content/uploads/2021/06/z2534075501953_fbb392a4ba72a0f859018f440a79f45d-85x85.jpg 85w, https://topal.vn/wp-content/uploads/2021/06/z2534075501953_fbb392a4ba72a0f859018f440a79f45d-80x80.jpg 80w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Lựa chọn phổ biến nhất cho cửa đi nhôm Topal uốn vòm là cửa đi 2 cánh hoặc 4 cánh. Một số công trình thiết kế sử dụng cho cả cửa đi và cửa sổ, tạo nên sự đồng nhất về ngoại quan, nâng cao giá trị thẩm mĩ cho tổng thể. Với chất liệu nhôm bền bỉ, tuổi thọ sử dụng cửa lên đến 5 – 10 năm.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Màu sắc đặc trưng phù hợp nội thất cổ điển&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Ngoài việc chọn kiểu cửa sao cho phù hợp lối thiết kế nội thất, khách hàng còn cần chú ý đến màu sắc bộ cửa. Màu sắc cửa cần được kết hợp một cách hợp lí với những đồ nội thất khác trong không gian, từ đó, có thể tạo nên một không gian hoàn mĩ cho công trình.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/06/z2534075535746_d019a1cb39324c0ac366076f2d948745-1024x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/06/z2534075535746_d019a1cb39324c0ac366076f2d948745-1024x1024.jpg 1024w, https://topal.vn/wp-content/uploads/2021/06/z2534075535746_d019a1cb39324c0ac366076f2d948745-300x300.jpg 300w, https://topal.vn/wp-content/uploads/2021/06/z2534075535746_d019a1cb39324c0ac366076f2d948745-150x150.jpg 150w, https://topal.vn/wp-content/uploads/2021/06/z2534075535746_d019a1cb39324c0ac366076f2d948745-768x768.jpg 768w, https://topal.vn/wp-content/uploads/2021/06/z2534075535746_d019a1cb39324c0ac366076f2d948745-1536x1536.jpg 1536w, https://topal.vn/wp-content/uploads/2021/06/z2534075535746_d019a1cb39324c0ac366076f2d948745-2048x2048.jpg 2048w, https://topal.vn/wp-content/uploads/2021/06/z2534075535746_d019a1cb39324c0ac366076f2d948745-600x600.jpg 600w, https://topal.vn/wp-content/uploads/2021/06/z2534075535746_d019a1cb39324c0ac366076f2d948745-146x146.jpg 146w, https://topal.vn/wp-content/uploads/2021/06/z2534075535746_d019a1cb39324c0ac366076f2d948745-50x50.jpg 50w, https://topal.vn/wp-content/uploads/2021/06/z2534075535746_d019a1cb39324c0ac366076f2d948745-75x75.jpg 75w, https://topal.vn/wp-content/uploads/2021/06/z2534075535746_d019a1cb39324c0ac366076f2d948745-85x85.jpg 85w, https://topal.vn/wp-content/uploads/2021/06/z2534075535746_d019a1cb39324c0ac366076f2d948745-80x80.jpg 80w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Để có thể tạo nên một không gian mang tính cổ điển, quan trọng nhất ở bộ cửa là thể hiện được sự sang trọng. Trong đó, phổ biến nhất là Vân gỗ và Xám đá, Ghi Ánh kim. Đây là ba gam màu được ưa chuộng cho lối thiết kế cổ điển trong không gian nhà ở.&lt;/p&gt;\r\n\r\n&lt;p&gt;Màu nhôm vân gỗ của Topal được sản xuất theo tiêu chuẩn và sơn Decoral nhập khẩu chính hãng từ Italy đem lại màu chân thực khiến bạn khó có thể nhận biết đó là sản phẩm có cấu tạo từ nhôm, độ bền màu lên đến 20 năm ;àm hài lòng khá nhiều khách hàng. Bên cạnh đó, màu xám đá và ánh kim độc quyền của Topal tạo nên hiệu ứng độc đáo cho bộ cửa.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Lựa chọn đơn vị nhôm uy tín&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Hiện nay, trong bối cảnh thị trường nhôm ngày càng trở nên đa dạng, khách hàng dễ rơi vào trường hợp khó phân biệt được sản phẩm chất lượng hoặc kém chất lượng. Bên cạnh đó, để có thể thi công được bộ cửa uốn cong khung hoặc cánh, đòi hỏi nhôm phải có độ cứng T5, tính gia công tốt, độ bền vượt trội; mặt khác, tay nghề của đội ngũ thi công cũng là yếu tố quan trọng để đảm bảo cho bộ cửa vận hành êm ái, tránh cong vênh.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/06/z2534075604200_31c764adc0f8fa1cd29a4c993e182357-1024x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/06/z2534075604200_31c764adc0f8fa1cd29a4c993e182357-1024x1024.jpg 1024w, https://topal.vn/wp-content/uploads/2021/06/z2534075604200_31c764adc0f8fa1cd29a4c993e182357-300x300.jpg 300w, https://topal.vn/wp-content/uploads/2021/06/z2534075604200_31c764adc0f8fa1cd29a4c993e182357-150x150.jpg 150w, https://topal.vn/wp-content/uploads/2021/06/z2534075604200_31c764adc0f8fa1cd29a4c993e182357-768x768.jpg 768w, https://topal.vn/wp-content/uploads/2021/06/z2534075604200_31c764adc0f8fa1cd29a4c993e182357-1536x1536.jpg 1536w, https://topal.vn/wp-content/uploads/2021/06/z2534075604200_31c764adc0f8fa1cd29a4c993e182357-2048x2048.jpg 2048w, https://topal.vn/wp-content/uploads/2021/06/z2534075604200_31c764adc0f8fa1cd29a4c993e182357-600x600.jpg 600w, https://topal.vn/wp-content/uploads/2021/06/z2534075604200_31c764adc0f8fa1cd29a4c993e182357-146x146.jpg 146w, https://topal.vn/wp-content/uploads/2021/06/z2534075604200_31c764adc0f8fa1cd29a4c993e182357-50x50.jpg 50w, https://topal.vn/wp-content/uploads/2021/06/z2534075604200_31c764adc0f8fa1cd29a4c993e182357-75x75.jpg 75w, https://topal.vn/wp-content/uploads/2021/06/z2534075604200_31c764adc0f8fa1cd29a4c993e182357-85x85.jpg 85w, https://topal.vn/wp-content/uploads/2021/06/z2534075604200_31c764adc0f8fa1cd29a4c993e182357-80x80.jpg 80w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Chế độ bảo hành cũng được đảm bảo chính hãng để khách hàng có thể dễ dàng được bảo hành và chăm sóc khi sản phẩm gặp trục trặc khi sử dụng.&lt;/p&gt;\r\n\r\n&lt;p&gt;Quý khách hàng có nhu cầu tư vấn lắp đặt những mẫu cửa nhôm theo phong cách cổ điển đẹp nhất có thể liên hệ với chúng tôi:&lt;/p&gt;\r\n\r\n&lt;p&gt;Cách 1: Gọi ngay đến Hotline 1900 6828 để được tư vấn&lt;/p&gt;\r\n\r\n&lt;p&gt;Cách 2: Truy cập Website Topal theo địa chỉ: &lt;a href=&quot;https://topal.vn/mua-o-dau/?fbclid=IwAR0zI361dXufi67zWSwtQJ-uqSPUaYTiIPPOO_UuFT0LydYgrNFjD2w6qxY#tim-kiem-dai-ly&quot; rel=&quot;noreferrer noopener&quot; target=&quot;_blank&quot;&gt;https://topal.vn/mua-o-dau/#tim-kiem-dai-ly&lt;/a&gt; để tìm kiếm đại lý gần bạn nhất&lt;/p&gt;\r\n\r\n&lt;p&gt;Cách 3: Nhắn tin/để lại bình luận tại fanpage Nhôm Topal để được tư vấn&lt;/p&gt;', '&lt;p&gt;Đối với phong cách kiến trúc cổ điển, bạn sẽ dễ dàng nhận thấy những chi tiết hoa văn chạm trổ nơi tường, ban công, hoặc các vị trí khác trong không gian nội thất. Cũng vì vậy mà trong hạng mục cửa, việc chọn sử dụng những bộ cửa sao cho phù hợp phong cách được xem là bài toán khó.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/z2534075541511_9daac93a01600aac11cfa91e264e310a-1024x768.jpg', '', '', '', 'topal-mach-ban-cach-chon-mau-cua-nhom-phu-hop-nhat-cho-phong-cach-co-dien', '', 'image', '', NULL),
(12, 2, 'Kinh nghiệm chọn mua cửa nhôm độ bền cao', '&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/05/z2505838630702_fb0985ad13999a00e732615a67c060fc-1024x681.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/05/z2505838630702_fb0985ad13999a00e732615a67c060fc-1024x681.jpg 1024w, https://topal.vn/wp-content/uploads/2021/05/z2505838630702_fb0985ad13999a00e732615a67c060fc-300x199.jpg 300w, https://topal.vn/wp-content/uploads/2021/05/z2505838630702_fb0985ad13999a00e732615a67c060fc-768x510.jpg 768w, https://topal.vn/wp-content/uploads/2021/05/z2505838630702_fb0985ad13999a00e732615a67c060fc-1536x1021.jpg 1536w, https://topal.vn/wp-content/uploads/2021/05/z2505838630702_fb0985ad13999a00e732615a67c060fc-600x399.jpg 600w, https://topal.vn/wp-content/uploads/2021/05/z2505838630702_fb0985ad13999a00e732615a67c060fc-220x146.jpg 220w, https://topal.vn/wp-content/uploads/2021/05/z2505838630702_fb0985ad13999a00e732615a67c060fc-50x33.jpg 50w, https://topal.vn/wp-content/uploads/2021/05/z2505838630702_fb0985ad13999a00e732615a67c060fc-113x75.jpg 113w, https://topal.vn/wp-content/uploads/2021/05/z2505838630702_fb0985ad13999a00e732615a67c060fc.jpg 1876w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Trong các loại cửa với sự đa dạng chất liệu trên thị trường hiện nay, cửa nhôm trở thành loại cửa chiếm ưu thế trong việc “lấy lòng” khách hàng. Bằng chứng là bạn có thể dễ dàng bắt gặp cửa nhôm ở bất kì công trình nào tại Việt Nam. Nhưng để lựa chọn được những bộ cửa nhôm đạt độ bền tốt lại không dễ đối với khách hàng. Bởi không giống với các loại cửa thông thường, cửa nhôm sở hữu kết cấu đậm tính kĩ thuật cao nên khách hàng khó có thể đánh giá được chất lượng.&lt;/p&gt;\r\n\r\n&lt;p&gt;Dựa vào đâu để chúng ta có thể đánh giá độ bền của bộ cửa nhôm? Khi chọn mua cửa nhôm thì cần lưu ý những gì ? Nhôm Topal hiểu đó là những gì mà khách hàng đang lo lắng. Vì vậy, trong bài viết này Nhôm Topal sẽ giúp bạn có thêm kinh nghiệm chọn mua cửa nhôm đạt độ bền cao.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Chú ý độ dày của nhôm&lt;/strong&gt;&lt;br /&gt;\r\nĐối với cửa đi nhôm, độ dày của thanh nhôm làm cửa chính là yếu tố góp phần hình thành độ bền của bộ cửa. Để có thể nhìn thấy độ dày nhôm, đơn giản nhất là khách hàng quan sát góc mẫu trưng bày. Góc mẫu là một phần góc của bộ cửa. Thường được trưng bày tại showroom giúp sản phẩm có thể nhìn kĩ hơn về kết cấu của sản phẩm. Do đó, khách hàng nên trực tiếp đến showroom để xem trực tiếp sản phẩm mẫu.&lt;br /&gt;\r\nĐộ dày tiêu chuẩn đối với cửa đi thường trong khoảng từ 1.4mm ~ 2.0mm. Khách hàng đừng quên nếu độ dày dưới 1.4mm thì bộ cửa đi sẽ không đạt được độ vững chãi cần có. Tại các công trình nhà ở như: biệt thự, nhà 2 – 3 tầng,…, thậm chí người sử dụng còn chọn dùng độ dày 2.0mm đối với cửa nhôm. Bởi người dùng xác định được bộ cửa được sử dụng trong thời gian dài. Vì vậy, nếu bạn cũng đang chọn mua cửa cho tổ ấm của mình cũng đừng ngần ngại mà hãy chọn loại có độ dày cao nhé.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/05/z2509171092433_b8b7d72f337094824bf8cf546cd34f57-1024x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/05/z2509171092433_b8b7d72f337094824bf8cf546cd34f57-1024x1024.jpg 1024w, https://topal.vn/wp-content/uploads/2021/05/z2509171092433_b8b7d72f337094824bf8cf546cd34f57-300x300.jpg 300w, https://topal.vn/wp-content/uploads/2021/05/z2509171092433_b8b7d72f337094824bf8cf546cd34f57-150x150.jpg 150w, https://topal.vn/wp-content/uploads/2021/05/z2509171092433_b8b7d72f337094824bf8cf546cd34f57-768x768.jpg 768w, https://topal.vn/wp-content/uploads/2021/05/z2509171092433_b8b7d72f337094824bf8cf546cd34f57-1536x1536.jpg 1536w, https://topal.vn/wp-content/uploads/2021/05/z2509171092433_b8b7d72f337094824bf8cf546cd34f57-600x600.jpg 600w, https://topal.vn/wp-content/uploads/2021/05/z2509171092433_b8b7d72f337094824bf8cf546cd34f57-146x146.jpg 146w, https://topal.vn/wp-content/uploads/2021/05/z2509171092433_b8b7d72f337094824bf8cf546cd34f57-50x50.jpg 50w, https://topal.vn/wp-content/uploads/2021/05/z2509171092433_b8b7d72f337094824bf8cf546cd34f57-75x75.jpg 75w, https://topal.vn/wp-content/uploads/2021/05/z2509171092433_b8b7d72f337094824bf8cf546cd34f57-85x85.jpg 85w, https://topal.vn/wp-content/uploads/2021/05/z2509171092433_b8b7d72f337094824bf8cf546cd34f57-80x80.jpg 80w, https://topal.vn/wp-content/uploads/2021/05/z2509171092433_b8b7d72f337094824bf8cf546cd34f57.jpg 1876w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Phụ kiện đồng bộ chính hãng&lt;/strong&gt;&lt;br /&gt;\r\nMuốn cửa được vận hành tốt và duy trì được độ bền thì phụ kiện chính là yếu tố quan trọng và cần đồng bộ từ tay nắm, bản lề, gioăng, khóa,gioăng…, để các phụ kiện được thiết kế vừa vặn, ăn khớp với nhau giúp bộ cửa vận hành trơn tru, êm ái. Đối với phụ kiện cửa, nhất là tay nắm và bản lề, là những vị trí chịu tác động thường xuyên nhất, bạn nên lựa chọn sử dụng phụ kiện cao cấp, có thể chịu được độ bền đóng mở khoảng 100.000 lần để đỡ tốn chi phí thay mới thường xuyên.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/05/z2506006677256_dfd61beb740c57b6b33f4a9a4f665c9c-1024x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/05/z2506006677256_dfd61beb740c57b6b33f4a9a4f665c9c-1024x1024.jpg 1024w, https://topal.vn/wp-content/uploads/2021/05/z2506006677256_dfd61beb740c57b6b33f4a9a4f665c9c-300x300.jpg 300w, https://topal.vn/wp-content/uploads/2021/05/z2506006677256_dfd61beb740c57b6b33f4a9a4f665c9c-150x150.jpg 150w, https://topal.vn/wp-content/uploads/2021/05/z2506006677256_dfd61beb740c57b6b33f4a9a4f665c9c-768x768.jpg 768w, https://topal.vn/wp-content/uploads/2021/05/z2506006677256_dfd61beb740c57b6b33f4a9a4f665c9c-1536x1536.jpg 1536w, https://topal.vn/wp-content/uploads/2021/05/z2506006677256_dfd61beb740c57b6b33f4a9a4f665c9c-600x600.jpg 600w, https://topal.vn/wp-content/uploads/2021/05/z2506006677256_dfd61beb740c57b6b33f4a9a4f665c9c-146x146.jpg 146w, https://topal.vn/wp-content/uploads/2021/05/z2506006677256_dfd61beb740c57b6b33f4a9a4f665c9c-50x50.jpg 50w, https://topal.vn/wp-content/uploads/2021/05/z2506006677256_dfd61beb740c57b6b33f4a9a4f665c9c-75x75.jpg 75w, https://topal.vn/wp-content/uploads/2021/05/z2506006677256_dfd61beb740c57b6b33f4a9a4f665c9c-85x85.jpg 85w, https://topal.vn/wp-content/uploads/2021/05/z2506006677256_dfd61beb740c57b6b33f4a9a4f665c9c-80x80.jpg 80w, https://topal.vn/wp-content/uploads/2021/05/z2506006677256_dfd61beb740c57b6b33f4a9a4f665c9c.jpg 1876w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Chế độ bảo hành chính hãng&lt;/strong&gt;&lt;br /&gt;\r\nĐể an tâm sử dụng, bạn nên lựa chọn các đơn vị sẵn sàng cam kết và bảo hành với khách hàng với sản phẩm trong quá trình sử dụng. Đối với các sản phẩm nhôm, các đơn vị uy tín sẽ có chế độ bảo hành độ bền màu cho lớp sơn bề mặt phải 5 năm. Đối với phụ kiện chính hãng thường được cam kết bảo hành với thời gian từ 1 năm. Người sử dụng có thể tiết kiệm được chi phí sửa chữa (nếu có) trong quá trình sử dụng cửa.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/05/z2505843952155_8cb0631ea14857c12a24bc04d6861d89-1024x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/05/z2505843952155_8cb0631ea14857c12a24bc04d6861d89-1024x1024.jpg 1024w, https://topal.vn/wp-content/uploads/2021/05/z2505843952155_8cb0631ea14857c12a24bc04d6861d89-300x300.jpg 300w, https://topal.vn/wp-content/uploads/2021/05/z2505843952155_8cb0631ea14857c12a24bc04d6861d89-150x150.jpg 150w, https://topal.vn/wp-content/uploads/2021/05/z2505843952155_8cb0631ea14857c12a24bc04d6861d89-768x768.jpg 768w, https://topal.vn/wp-content/uploads/2021/05/z2505843952155_8cb0631ea14857c12a24bc04d6861d89-1536x1536.jpg 1536w, https://topal.vn/wp-content/uploads/2021/05/z2505843952155_8cb0631ea14857c12a24bc04d6861d89-600x600.jpg 600w, https://topal.vn/wp-content/uploads/2021/05/z2505843952155_8cb0631ea14857c12a24bc04d6861d89-146x146.jpg 146w, https://topal.vn/wp-content/uploads/2021/05/z2505843952155_8cb0631ea14857c12a24bc04d6861d89-50x50.jpg 50w, https://topal.vn/wp-content/uploads/2021/05/z2505843952155_8cb0631ea14857c12a24bc04d6861d89-75x75.jpg 75w, https://topal.vn/wp-content/uploads/2021/05/z2505843952155_8cb0631ea14857c12a24bc04d6861d89-85x85.jpg 85w, https://topal.vn/wp-content/uploads/2021/05/z2505843952155_8cb0631ea14857c12a24bc04d6861d89-80x80.jpg 80w, https://topal.vn/wp-content/uploads/2021/05/z2505843952155_8cb0631ea14857c12a24bc04d6861d89.jpg 1876w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Với những kinh nghiệm từ thực tế người sử dụng, mong rằng khách hàng sẽ có thêm thông tin khi mua cửa. Để tham khảo các sản phẩm nhôm kính đẹp, khách hàng có thể liên hệ Hotline 1900 6828 hoặc tìm ngay các showroom Topal gần nhất để trực tiếp trải nghiệm sản phẩm.&lt;/p&gt;', '&lt;p&gt;&lt;em&gt;Trong các loại cửa với sự đa dạng chất liệu trên thị trường hiện nay, cửa nhôm trở thành loại cửa chiếm ưu thế trong việc “lấy lòng” khách hàng.&lt;/em&gt;&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/z2505838630702_fb0985ad13999a00e732615a67c060fc-1024x681.jpg', '', '', '', 'kinh-nghiem-chon-mua-cua-nhom-do-ben-cao', '', 'image', '', NULL),
(13, 2, 'Cửa nhôm đồng bộ Topal Prima đã làm hài lòng khách hàng như thế nào?', '&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/06/z2555588402430_ff42932ced661e8a4278a7573fec8fa6-1024x682.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/06/z2555588402430_ff42932ced661e8a4278a7573fec8fa6-1024x682.jpg 1024w, https://topal.vn/wp-content/uploads/2021/06/z2555588402430_ff42932ced661e8a4278a7573fec8fa6-300x200.jpg 300w, https://topal.vn/wp-content/uploads/2021/06/z2555588402430_ff42932ced661e8a4278a7573fec8fa6-768x511.jpg 768w, https://topal.vn/wp-content/uploads/2021/06/z2555588402430_ff42932ced661e8a4278a7573fec8fa6-1536x1023.jpg 1536w, https://topal.vn/wp-content/uploads/2021/06/z2555588402430_ff42932ced661e8a4278a7573fec8fa6-2048x1364.jpg 2048w, https://topal.vn/wp-content/uploads/2021/06/z2555588402430_ff42932ced661e8a4278a7573fec8fa6-600x400.jpg 600w, https://topal.vn/wp-content/uploads/2021/06/z2555588402430_ff42932ced661e8a4278a7573fec8fa6-219x146.jpg 219w, https://topal.vn/wp-content/uploads/2021/06/z2555588402430_ff42932ced661e8a4278a7573fec8fa6-50x33.jpg 50w, https://topal.vn/wp-content/uploads/2021/06/z2555588402430_ff42932ced661e8a4278a7573fec8fa6-113x75.jpg 113w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Cửa nhôm ngày nay đã trở thành từ khóa tìm kiếm phổ biến đối với lĩnh vực kiến trúc xây dựng. Nhận được sự đón nhận và tin dùng từ phía khách hàng, Cửa nhôm đồng bộ Topal Prima là một trong những dòng cửa được chọn dùng nhiều nhất cho đến thời điểm hiện tại. Nếu bạn đang tìm những bộ cửa nhôm bền vững và sở hữu giCửa nhôm ngày nay đã trở thành từ khóa tìm kiếm phổ biến đối với lĩnh vực kiến trúc xây dựng. Nhận được sự đón nhận và tin dùng từ phía khách hàng, Cửa nhôm đồng bộ Topal Prima là một trong những dòng cửa được chọn dùng nhiều nhất cho đến thời điểm hiện tại.á trị khác biệt trong mỗi bộ cửa. Chắc chắn rằng bài viết này dành cho bạn.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Đa dạng công năng thiết kế&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Topal Prima có thiết kế đa dạng với nhiều loại cửa, phù hợp cho công trình nhà bạn. Từ cửa sổ mở hất, cửa sổ mở quay, cửa sổ mở trượt, cửa sổ gấp trượt cho đến cửa đi mở quay, cửa đi mở lùa, cửa đi gấp trượt và đặc biệt là cửa đi gấp trượt vuông góc… Đáp ứng các yêu cầu về không gian, cách mở, hướng mở cho công trình; tối ưu tính năng, đạt khả năng kín nước, chống ồn vượt trội theo tiêu chuẩn Châu Âu EN1027.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/06/z2555588286663_266cfa2b1d5679e2c8b019093b948aa9-1024x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/06/z2555588286663_266cfa2b1d5679e2c8b019093b948aa9-1024x1024.jpg 1024w, https://topal.vn/wp-content/uploads/2021/06/z2555588286663_266cfa2b1d5679e2c8b019093b948aa9-300x300.jpg 300w, https://topal.vn/wp-content/uploads/2021/06/z2555588286663_266cfa2b1d5679e2c8b019093b948aa9-150x150.jpg 150w, https://topal.vn/wp-content/uploads/2021/06/z2555588286663_266cfa2b1d5679e2c8b019093b948aa9-768x768.jpg 768w, https://topal.vn/wp-content/uploads/2021/06/z2555588286663_266cfa2b1d5679e2c8b019093b948aa9-1536x1536.jpg 1536w, https://topal.vn/wp-content/uploads/2021/06/z2555588286663_266cfa2b1d5679e2c8b019093b948aa9-2048x2048.jpg 2048w, https://topal.vn/wp-content/uploads/2021/06/z2555588286663_266cfa2b1d5679e2c8b019093b948aa9-600x600.jpg 600w, https://topal.vn/wp-content/uploads/2021/06/z2555588286663_266cfa2b1d5679e2c8b019093b948aa9-146x146.jpg 146w, https://topal.vn/wp-content/uploads/2021/06/z2555588286663_266cfa2b1d5679e2c8b019093b948aa9-50x50.jpg 50w, https://topal.vn/wp-content/uploads/2021/06/z2555588286663_266cfa2b1d5679e2c8b019093b948aa9-75x75.jpg 75w, https://topal.vn/wp-content/uploads/2021/06/z2555588286663_266cfa2b1d5679e2c8b019093b948aa9-85x85.jpg 85w, https://topal.vn/wp-content/uploads/2021/06/z2555588286663_266cfa2b1d5679e2c8b019093b948aa9-80x80.jpg 80w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Thẩm mỹ sang trọng, đẳng cấp&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Với thiết kế thanh nhôm khỏe khoắn, sang trọng, sở hữu sơn bề mặt cao cấp, Topal Prima là lựa chọn phù hợp với các công trình như khách sạn, Biệt thực, Resort nghỉ dưỡng cao cấp,…&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/06/z2555588259099_704ffc3833d4c2944e9d4af39f80e91f-1024x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/06/z2555588259099_704ffc3833d4c2944e9d4af39f80e91f-1024x1024.jpg 1024w, https://topal.vn/wp-content/uploads/2021/06/z2555588259099_704ffc3833d4c2944e9d4af39f80e91f-300x300.jpg 300w, https://topal.vn/wp-content/uploads/2021/06/z2555588259099_704ffc3833d4c2944e9d4af39f80e91f-150x150.jpg 150w, https://topal.vn/wp-content/uploads/2021/06/z2555588259099_704ffc3833d4c2944e9d4af39f80e91f-768x768.jpg 768w, https://topal.vn/wp-content/uploads/2021/06/z2555588259099_704ffc3833d4c2944e9d4af39f80e91f-1536x1536.jpg 1536w, https://topal.vn/wp-content/uploads/2021/06/z2555588259099_704ffc3833d4c2944e9d4af39f80e91f-2048x2048.jpg 2048w, https://topal.vn/wp-content/uploads/2021/06/z2555588259099_704ffc3833d4c2944e9d4af39f80e91f-600x600.jpg 600w, https://topal.vn/wp-content/uploads/2021/06/z2555588259099_704ffc3833d4c2944e9d4af39f80e91f-146x146.jpg 146w, https://topal.vn/wp-content/uploads/2021/06/z2555588259099_704ffc3833d4c2944e9d4af39f80e91f-50x50.jpg 50w, https://topal.vn/wp-content/uploads/2021/06/z2555588259099_704ffc3833d4c2944e9d4af39f80e91f-75x75.jpg 75w, https://topal.vn/wp-content/uploads/2021/06/z2555588259099_704ffc3833d4c2944e9d4af39f80e91f-85x85.jpg 85w, https://topal.vn/wp-content/uploads/2021/06/z2555588259099_704ffc3833d4c2944e9d4af39f80e91f-80x80.jpg 80w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Sở hữu tuổi thọ sử dụng cao&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Khách hàng chọn dùng cửa nhôm đồng bộ Topal Prima có thể tối ưu chi phí một cách hiệu quả nhất cho ví tiền của mình. Tiết kiệm ra sao? Đó là người dùng có thể hạn chế mất tiền cho việc sửa chữa thường xuyên. Bởi khi dùng những loại cửa thông thường, khách hàng sẽ rơi vào trường hợp liên tục phải sửa hoặc thay mới phụ kiện vì bị trục trặc.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/06/z2555588216640_7ad774d72c24267075044ea11937faf8-1024x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/06/z2555588216640_7ad774d72c24267075044ea11937faf8-1024x1024.jpg 1024w, https://topal.vn/wp-content/uploads/2021/06/z2555588216640_7ad774d72c24267075044ea11937faf8-300x300.jpg 300w, https://topal.vn/wp-content/uploads/2021/06/z2555588216640_7ad774d72c24267075044ea11937faf8-150x150.jpg 150w, https://topal.vn/wp-content/uploads/2021/06/z2555588216640_7ad774d72c24267075044ea11937faf8-768x768.jpg 768w, https://topal.vn/wp-content/uploads/2021/06/z2555588216640_7ad774d72c24267075044ea11937faf8-1536x1536.jpg 1536w, https://topal.vn/wp-content/uploads/2021/06/z2555588216640_7ad774d72c24267075044ea11937faf8-2048x2048.jpg 2048w, https://topal.vn/wp-content/uploads/2021/06/z2555588216640_7ad774d72c24267075044ea11937faf8-600x600.jpg 600w, https://topal.vn/wp-content/uploads/2021/06/z2555588216640_7ad774d72c24267075044ea11937faf8-146x146.jpg 146w, https://topal.vn/wp-content/uploads/2021/06/z2555588216640_7ad774d72c24267075044ea11937faf8-50x50.jpg 50w, https://topal.vn/wp-content/uploads/2021/06/z2555588216640_7ad774d72c24267075044ea11937faf8-75x75.jpg 75w, https://topal.vn/wp-content/uploads/2021/06/z2555588216640_7ad774d72c24267075044ea11937faf8-85x85.jpg 85w, https://topal.vn/wp-content/uploads/2021/06/z2555588216640_7ad774d72c24267075044ea11937faf8-80x80.jpg 80w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;&lt;br /&gt;\r\nTrong khi đó, bạn sẽ không mắc phải sai lầm đó nếu chọn sử dụng cửa nhôm đồng bộ Topal Prima bởi đây là sản phẩm đồng bộ được thiết kế đồng bộ từ NGUYÊN VẬT LIỆU (thanh nhôm, phụ kiện, gioăng) cho đến CÔNG NGHỆ SẢN XUẤT (công cụ sản xuất, tài liệu hướng dẫn) giúp cửa nhôm đồng bộ Topal Prima vận hành bền bỉ, các phụ kiện được vận hành ăn khớp với nhau giúp tăng độ bền cho sản phẩm. &lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Bảo hành chính hãng trên toàn quốc&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Chính sách bảo hành chính hãng từ Nhôm Topal đã tạo thêm niềm tin cho khách hàng khi chọn lựa:&lt;/p&gt;\r\n\r\n&lt;p&gt;+ Bảo hành bề mặt sơn, chống bong tróc : 5 năm&lt;br /&gt;\r\n+ Bảo hành phụ kiện kim khí đồng bộ : 1 năm vận hành ( lỗi sản phẩm đổi mới 100% )&lt;br /&gt;\r\n+ Bảo hành gioăng EPDM nhập khẩu chống lão hóa: 5 năm&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/06/z2555588367515_72ddce9168828dc032e032c076ad78de-1024x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/06/z2555588367515_72ddce9168828dc032e032c076ad78de-1024x1024.jpg 1024w, https://topal.vn/wp-content/uploads/2021/06/z2555588367515_72ddce9168828dc032e032c076ad78de-300x300.jpg 300w, https://topal.vn/wp-content/uploads/2021/06/z2555588367515_72ddce9168828dc032e032c076ad78de-150x150.jpg 150w, https://topal.vn/wp-content/uploads/2021/06/z2555588367515_72ddce9168828dc032e032c076ad78de-768x768.jpg 768w, https://topal.vn/wp-content/uploads/2021/06/z2555588367515_72ddce9168828dc032e032c076ad78de-1536x1536.jpg 1536w, https://topal.vn/wp-content/uploads/2021/06/z2555588367515_72ddce9168828dc032e032c076ad78de-2048x2048.jpg 2048w, https://topal.vn/wp-content/uploads/2021/06/z2555588367515_72ddce9168828dc032e032c076ad78de-600x600.jpg 600w, https://topal.vn/wp-content/uploads/2021/06/z2555588367515_72ddce9168828dc032e032c076ad78de-146x146.jpg 146w, https://topal.vn/wp-content/uploads/2021/06/z2555588367515_72ddce9168828dc032e032c076ad78de-50x50.jpg 50w, https://topal.vn/wp-content/uploads/2021/06/z2555588367515_72ddce9168828dc032e032c076ad78de-75x75.jpg 75w, https://topal.vn/wp-content/uploads/2021/06/z2555588367515_72ddce9168828dc032e032c076ad78de-85x85.jpg 85w, https://topal.vn/wp-content/uploads/2021/06/z2555588367515_72ddce9168828dc032e032c076ad78de-80x80.jpg 80w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Luôn luôn có 2 kênh bảo hành trực tiếp cho quý khách hàng sử dụng cửa nhôm TOPAL:&lt;/p&gt;\r\n\r\n&lt;p&gt;+ Kênh bảo hành của đại lý sản xuất ủy quyền chính hãng&lt;br /&gt;\r\n+ Kênh bảo hành của Tập đoàn Austdoor – Nhôm Topal với hotline 1900 6828 trên toàn quốc&lt;/p&gt;', '&lt;p&gt;&lt;em&gt;Cửa nhôm ngày nay đã trở thành từ khóa tìm kiếm phổ biến đối với lĩnh vực kiến trúc xây dựng. Nhận được sự đón nhận và tin dùng từ phía khách hàng, Cửa nhôm đồng bộ Topal Prima là một trong những dòng cửa được chọn dùng nhiều nhất cho đến thời điểm hiện tại.&lt;/em&gt;&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/z2555588402430_ff42932ced661e8a4278a7573fec8fa6-1024x682.jpg', '', '', '', 'cua-nhom-dong-bo-topal-prima-da-lam-hai-long-khach-hang-nhu-the-nao', '', 'image', '', NULL),
(14, 2, '&quot;Topal - Mua càng nhiều, quà càng lớn&quot;', '&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-02-1-1024x1024.png&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-02-1-1024x1024.png 1024w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-02-1-300x300.png 300w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-02-1-150x150.png 150w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-02-1-768x768.png 768w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-02-1-1536x1536.png 1536w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-02-1-2048x2048.png 2048w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-02-1-600x600.png 600w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-02-1-146x146.png 146w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-02-1-50x50.png 50w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-02-1-75x75.png 75w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-02-1-85x85.png 85w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-02-1-80x80.png 80w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;&lt;strong&gt;&lt;em&gt;Nhằm tri ân hệ thống đại lý đã đồng hành trong thời gian qua, Nhôm Topal triển khai chương trình khuyến mại “TOPAL – MUA CÀNG NHIỀU, QUÀ CÀNG LỚN” tại các tỉnh từ Quảng Ngãi trở vào phía Nam từ ngày 10/10/2021-31/12/2021.&lt;/em&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Căn cứ Luật thương mại, Nghị định số 81/2018/NĐ-CP ngày 22 tháng 5 năm 2018 của Chính phủ quy định chi tiết Luật thương mại về hoạt động xúc tiến thương mại, CÔNG TY CỔ PHẦN TẬP ĐOÀN AUSTDOOR thông báo chương trình khuyến mại tới các Quý Đại lý như sau:&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li&gt;Tên chương trình khuyến mại: CTKM Topal – Mua càng nhiều quà càng lớn&lt;/li&gt;\r\n	&lt;li&gt;Địa bàn (phạm vi) khuyến mại: Các tỉnh từ Quảng Ngãi trở vào phía Nam&lt;/li&gt;\r\n	&lt;li&gt;Hình thức khuyến mại: Tặng phiếu mua hàng cho các đơn hàng nhôm hệ Topal và phụ kiện đủ điều kiện đổi thưởng.&lt;/li&gt;\r\n	&lt;li&gt;Thời gian khuyến mại: Từ ngày 10/10/2021 đến ngày 31/12/2021.&lt;/li&gt;\r\n	&lt;li&gt;Áp dung khi mua tất cả các sản phẩm Nhôm hệ Topal và phụ kiện.&lt;/li&gt;\r\n	&lt;li&gt;Hàng hóa, dịch vụ dùng để khuyến mại: Phiếu mua hàng Topal&lt;/li&gt;\r\n	&lt;li&gt;Khách hàng được hưởng khuyến mại: Các Đại lý sản xuất mua hàng trực tiếp từ Công ty hoặc các Đại lý sản xuất mua hàng qua Đại lý phân phối của Công ty tại các tỉnh từ Quảng Ngãi trở vào phía Nam.&lt;/li&gt;\r\n	&lt;li&gt;Cơ cấu giải thưởng: Đại lý sản xuất mua hàng trong thời gian diễn ra chương trình đạt mức doanh thu theo quy định được thưởng những phần quà tương ứng là các phiếu mua hàng trị giá 1.000.000 VNĐ, 4.000.000 VNĐ và 9.000.000 VNĐ. Số lượng phần quà không giới hạn.&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 512px) 100vw, 512px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-03-1-512x1024.png&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-03-1-512x1024.png 512w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-03-1-150x300.png 150w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-03-1-768x1536.png 768w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-03-1-1024x2048.png 1024w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-03-1-300x600.png 300w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-03-1-73x146.png 73w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-03-1-25x50.png 25w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU2-0810-03-1-38x75.png 38w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Văn bản chi tiết hướng dẫn thực hiện chương trình :&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 748px) 100vw, 748px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/10/0001-748x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/10/0001-748x1024.jpg 748w, https://topal.vn/wp-content/uploads/2021/10/0001-219x300.jpg 219w, https://topal.vn/wp-content/uploads/2021/10/0001-768x1051.jpg 768w, https://topal.vn/wp-content/uploads/2021/10/0001-1122x1536.jpg 1122w, https://topal.vn/wp-content/uploads/2021/10/0001-1496x2048.jpg 1496w, https://topal.vn/wp-content/uploads/2021/10/0001-438x600.jpg 438w, https://topal.vn/wp-content/uploads/2021/10/0001-107x146.jpg 107w, https://topal.vn/wp-content/uploads/2021/10/0001-37x50.jpg 37w, https://topal.vn/wp-content/uploads/2021/10/0001-55x75.jpg 55w, https://topal.vn/wp-content/uploads/2021/10/0001-scaled.jpg 1870w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 711px) 100vw, 711px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/10/0002-711x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/10/0002-711x1024.jpg 711w, https://topal.vn/wp-content/uploads/2021/10/0002-208x300.jpg 208w, https://topal.vn/wp-content/uploads/2021/10/0002-768x1106.jpg 768w, https://topal.vn/wp-content/uploads/2021/10/0002-1067x1536.jpg 1067w, https://topal.vn/wp-content/uploads/2021/10/0002-1422x2048.jpg 1422w, https://topal.vn/wp-content/uploads/2021/10/0002-417x600.jpg 417w, https://topal.vn/wp-content/uploads/2021/10/0002-101x146.jpg 101w, https://topal.vn/wp-content/uploads/2021/10/0002-35x50.jpg 35w, https://topal.vn/wp-content/uploads/2021/10/0002-52x75.jpg 52w, https://topal.vn/wp-content/uploads/2021/10/0002-scaled.jpg 1778w&quot; /&gt;&lt;/figure&gt;', '&lt;p&gt;&lt;em&gt;Nhằm tri ân hệ thống đại lý đã đồng hành trong thời gian qua, Nhôm Topal triển khai chương trình khuyến mại “MUA NHÔM TOPAL – NHẬN NGAY VÀNG 9999” tại các tỉnh từ Quảng Nam trở ra phía Bắc từ ngày 10/10/2021-31/12/2021.&lt;/em&gt;&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/tp-ctkm-bu2-0810-02-1-1024x1024.png', '', '', '', 'nhom-topal-mua-cang-nhieu-qua-cang-lon', '', 'image', '', NULL),
(15, 2, '&quot;Mua nhôm Topal - Nhận ngay vàng 9999&quot;', '&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-01-1024x1024.png&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-01-1024x1024.png 1024w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-01-300x300.png 300w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-01-150x150.png 150w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-01-768x768.png 768w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-01-1536x1536.png 1536w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-01-2048x2048.png 2048w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-01-600x600.png 600w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-01-146x146.png 146w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-01-50x50.png 50w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-01-75x75.png 75w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-01-85x85.png 85w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-01-80x80.png 80w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;&lt;strong&gt;&lt;em&gt;Nhằm tri ân hệ thống đại lý đã đồng hành trong thời gian qua, Nhôm Topal triển khai chương trình khuyến mại “MUA NHÔM TOPAL – NHẬN NGAY VÀNG 9999” tại các tỉnh từ Quảng Nam trở ra phía Bắc từ ngày 10/10/2021-31/12/2021.&lt;/em&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Căn cứ Luật thương mại, Nghị định số 81/2018/NĐ-CP ngày 22 tháng 5 năm 2018 của Chính phủ quy định chi tiết Luật thương mại về hoạt động xúc tiến thương mại, CÔNG TY CỔ PHẦN TẬP ĐOÀN AUSTDOOR thông báo chương trình khuyến mại tới các Quý Đại lý như sau:&lt;/p&gt;\r\n\r\n&lt;p&gt;1. Tên chương trình khuyến mại: Mua nhôm Topal nhận ngay vàng 9999&lt;/p&gt;\r\n\r\n&lt;p&gt;2. Địa bàn (phạm vi) khuyến mại: Các tỉnh từ Quảng Nam trở ra phía Bắc&lt;/p&gt;\r\n\r\n&lt;p&gt;3. Hình thức khuyến mại: Tặng vàng cho các đại lý mua hàng đạt đủ điều kiện thưởng.&lt;/p&gt;\r\n\r\n&lt;p&gt;4. Thời gian khuyến mại: 10/10/2021 – 31/12/2021&lt;/p&gt;\r\n\r\n&lt;p&gt;5. Áp dụng với tất cả các sản phẩm Nhôm hệ Topal và phụ kiện.&lt;/p&gt;\r\n\r\n&lt;p&gt;6. Khách hàng được hưởng khuyến mại: Các Đại lý sản xuất mua hàng trực tiếp từ Công ty hoặc các Đại lý sản xuất mua hàng qua Đại lý phân phối của Công ty tại các tỉnh từ Quảng Nam trở ra phía Bắc.&lt;/p&gt;\r\n\r\n&lt;p&gt;7. Cơ cấu giải thưởng : Tặng vàng 9999 cho mỗi đại lý đạt điều kiện trao thưởng. Số lượng giải thưởng không giới hạn.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 512px) 100vw, 512px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-02-512x1024.png&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-02-512x1024.png 512w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-02-150x300.png 150w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-02-768x1536.png 768w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-02-1024x2048.png 1024w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-02-300x600.png 300w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-02-73x146.png 73w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-02-25x50.png 25w, https://topal.vn/wp-content/uploads/2021/10/TP-CTKM-BU1-0810-02-38x75.png 38w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Văn bản chi tiết hướng dẫn thực hiện chương trình :&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 724px) 100vw, 724px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/10/0001-1-724x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/10/0001-1-724x1024.jpg 724w, https://topal.vn/wp-content/uploads/2021/10/0001-1-212x300.jpg 212w, https://topal.vn/wp-content/uploads/2021/10/0001-1-768x1086.jpg 768w, https://topal.vn/wp-content/uploads/2021/10/0001-1-1086x1536.jpg 1086w, https://topal.vn/wp-content/uploads/2021/10/0001-1-1449x2048.jpg 1449w, https://topal.vn/wp-content/uploads/2021/10/0001-1-424x600.jpg 424w, https://topal.vn/wp-content/uploads/2021/10/0001-1-103x146.jpg 103w, https://topal.vn/wp-content/uploads/2021/10/0001-1-35x50.jpg 35w, https://topal.vn/wp-content/uploads/2021/10/0001-1-53x75.jpg 53w, https://topal.vn/wp-content/uploads/2021/10/0001-1-scaled.jpg 1811w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 724px) 100vw, 724px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/10/0002-1-724x1024.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/10/0002-1-724x1024.jpg 724w, https://topal.vn/wp-content/uploads/2021/10/0002-1-212x300.jpg 212w, https://topal.vn/wp-content/uploads/2021/10/0002-1-768x1086.jpg 768w, https://topal.vn/wp-content/uploads/2021/10/0002-1-1086x1536.jpg 1086w, https://topal.vn/wp-content/uploads/2021/10/0002-1-1449x2048.jpg 1449w, https://topal.vn/wp-content/uploads/2021/10/0002-1-424x600.jpg 424w, https://topal.vn/wp-content/uploads/2021/10/0002-1-103x146.jpg 103w, https://topal.vn/wp-content/uploads/2021/10/0002-1-35x50.jpg 35w, https://topal.vn/wp-content/uploads/2021/10/0002-1-53x75.jpg 53w, https://topal.vn/wp-content/uploads/2021/10/0002-1-scaled.jpg 1811w&quot; /&gt;&lt;/figure&gt;', '&lt;p&gt;&lt;em&gt;Nhằm tri ân hệ thống đại lý đã đồng hành trong thời gian qua, Nhôm Topal triển khai chương trình khuyến mại “MUA NHÔM TOPAL – NHẬN NGAY VÀNG 9999” tại các tỉnh từ Quảng Nam trở ra phía Bắc từ ngày 10/10/2021-31/12/2021.&lt;/em&gt;&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/tp-ctkm-bu1-0810-01-1024x1024.png', '', '', '', 'mua-nhom-topal-nhan-ngay-vang-9999', '', 'image', '', NULL),
(18, 2, 'Topal tự hào là thương hiệu nhôm trẻ nhất đạt thương hiệu quốc gia 2020', '&lt;figure&gt;&lt;img alt=&quot;Hình ảnh này chưa có thuộc tính alt; tên tệp của nó là ca46eb2d1721e67fbf30-1024x764.jpg&quot; src=&quot;https://topal.vn/wp-content/uploads/2020/11/ca46eb2d1721e67fbf30-1024x764.jpg&quot; /&gt;\r\n&lt;figcaption&gt;Ông Dương Thạch Nguyên – Phó Tổng Giám Đốc Tập đoàn Austdoor nhận hoa và biểu trưng Thương Hiệu Quốc Gia từ Ông Vương Đình Huệ – Uỷ viên Bộ Chính trị, Bí thư Thành ủy Hà Nội và ông Nguyễn Thế Kỷ – Ủy viên TW Đảng, Tổng Giám đốc Đài tiếng nói Việt Nam.&lt;/figcaption&gt;\r\n&lt;/figure&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;&lt;em&gt;&lt;strong&gt;Cửa cuốn Austdoor và Nhôm Topal vừa được vinh danh trong Lễ công bố các doanh nghiệp đạt Thương hiệu quốc gia 2020. Đây là hai sản phẩm chủ lực của Tập đoàn Austdoor trong hành trình khẳng định vị thế hàng đầu trong lĩnh vực sản xuất và cung cấp cửa và vật liệu xây dựng tại Việt Nam.&lt;/strong&gt;&lt;/em&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Tối ngày hôm nay 25/11, Lễ Công bố các sản phẩm đạt Thương Hiệu Quốc Gia 2020 (THQG) đã chính thức diễn ra tại Nhà Hát Lớn Hà Nội. Vượt qua hơn 1.000 hồ sơ đăng ký, Tập đoàn Austdoor (ADG) đạt chứng nhận cho hai sản phẩm Cửa cuốn Austdoor và Nhôm Topal. Trong đó, Cửa cuốn Austdoor là thương hiệu cửa cuốn đầu tiên và duy nhất đạt THQG 2 lần liên tiếp còn Nhôm Topal là thương hiệu Nhôm “trẻ nhất” đạt danh hiệu chỉ sau 3 năm ra mắt trên thị trường.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;Hình ảnh này chưa có thuộc tính alt; tên tệp của nó là be142674da782b267269-1024x633.jpg&quot; src=&quot;https://topal.vn/wp-content/uploads/2020/11/be142674da782b267269-1024x633.jpg&quot; /&gt;\r\n&lt;figcaption&gt;Ông Dương Thạch Nguyên – Phó Tổng Giám Đốc Tập đoàn Austdoor nhận hoa và biểu trưng Thương Hiệu Quốc Gia từ Ông Vương Đình Huệ – Uỷ viên Bộ Chính trị, Bí thư Thành ủy Hà Nội và ông Nguyễn Thế Kỷ – Ủy viên TW Đảng, Tổng Giám đốc Đài tiếng nói Việt Nam.&lt;/figcaption&gt;\r\n&lt;/figure&gt;\r\n\r\n&lt;p&gt;Đây là năm thứ 2 Tập đoàn Austdoor đạt danh hiệu, trong đó Cửa cuốn Austdoor là thương hiệu cửa cuốn đầu tiên và duy nhất đạt THQG hai lần liên tiếp, còn Nhôm Topal là thương hiệu nhôm “trẻ nhất” được vinh danh chỉ sau 3 năm có mặt trên thị trường. Đây là kết quả ghi nhận cho những nỗ lực đổi mới sáng tạo về chất lượng sản phẩm, đầu tư lớn về quy mô sản xuất, cung ứng, công nghệ thiết bị, con người và cả những đóng góp cho cộng đồng, xã hội. &lt;br /&gt;\r\n﻿&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;Hình ảnh này chưa có thuộc tính alt; tên tệp của nó là e6697d73807f7121286e-1-1024x616.jpg&quot; src=&quot;https://topal.vn/wp-content/uploads/2020/11/e6697d73807f7121286e-1-1024x616.jpg&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Năm nay, vượt qua 1000 doanh nghiệp đăng ký hồ sơ tham dự chương trình, Tập đoàn Austdoor là một trong số 124 doanh nghiệp đủ tiêu chuẩn, vượt qua các tiêu chí khắt khe của chương trình. Đặc biệt khi năm 2020 là kỳ xét chọn đầu tiên Bộ Công thương triển khai Quyết định số 30/2019/QĐ-TTg ngày 08 tháng 10 năm 2019 của Thủ tướng Chính phủ và Thông tư số 33/2019/TT-BCT ngày 22 tháng 11 năm 2019 của Bộ Công Thương. Theo đó, các doanh nghiệp đều phải đảm bảo việc bảo hộ sở hữu trí tuệ, chấp hành pháp luật cũng như việc thực hiện trách nhiệm xã hội, bên cạnh bộ tiêu chí về năng lực.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;Hình ảnh này chưa có thuộc tính alt; tên tệp của nó là 917d3f0b1204e35aba15-1024x738.jpg&quot; src=&quot;https://topal.vn/wp-content/uploads/2020/11/917d3f0b1204e35aba15-1024x738.jpg&quot; /&gt;\r\n&lt;figcaption&gt;Ông Dương Thach Nguyên – Phó Tổng giám đốc Tập đoàn Austdoor trả lời phỏng vấn báo chí&lt;/figcaption&gt;\r\n&lt;/figure&gt;\r\n\r\n&lt;p&gt;Vinh dự khi được nhận Thương hiệu quốc gia, ông Dương Thach Nguyên – Phó Tổng giám đốc Tập đoàn Austdoor chia sẻ: &lt;em&gt;“Sự kiện là dấu mốc đáng nhớ trong hành trình 18 năm  phát triển của Austdoor. Thời gian tới, Tập đoàn sẽ đầu tư lớn từ sản xuất đến phân phối để mang lại trải nghiệm tốt nhất cho khách hàng, giữ vững vị thế cửa cuốn hàng đầu tại Việt Nam đồng thời đưa thương hiệu nhôm Topal phát triển mạnh mẽ, góp phần nâng cao vị thế của nhôm Việt”.&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;Hình ảnh này chưa có thuộc tính alt; tên tệp của nó là 20b1cfa7d8a829f670b9-1024x653.jpg&quot; src=&quot;https://topal.vn/wp-content/uploads/2020/11/20b1cfa7d8a829f670b9-1024x653.jpg&quot; /&gt;\r\n&lt;figcaption&gt;&lt;em&gt;Thủ tướng chụp ảnh lưu niệm với 124 lãnh đạo doanh nghiệp&lt;/em&gt;&lt;/figcaption&gt;\r\n&lt;/figure&gt;\r\n\r\n&lt;p&gt;Từ khi ra đời năm 2003 đến nay, cửa cuốn Austdoor luôn là thương hiệu cửa cuốn hàng đầu được người tiêu dùng trong và ngoài nước tin dùng. Austdoor không ngừng nghiên cứu, áp dụng các công nghệ mới, đa dạng chủng loại, phù hợp với tất cả các công trình từ nhà phố đến công nghiệp… Các công nghệ an toàn và an ninh không ngừng được cải tiến để đảm bảo sự an toàn tối đa cho người sử dụng và đáp ứng nhu cầu của thị trường, bắt kịp với xu hướng công nghệ của các nước tiên tiến trên thế giới.&lt;/p&gt;\r\n\r\n&lt;p&gt;Trong khi đó, Nhôm Topal dù mới gia nhập thị trường chưa lâu (2017-2020) đã nhanh chóng có được  “chỗ đứng” trong ngành nhôm Việt khi phủ sóng 63 tỉnh thành và liên tục cho ra mắt các dòng sản phẩm cửa nhôm cao cấp, nhôm hệ chất lượng cao, cung ứng hàng trăm nghìn tấn nhôm mỗi năm cho cả nước thông qua hệ thống nhà máy rộng khắp.&lt;/p&gt;\r\n\r\n&lt;p&gt;Việc đạt Thương hiệu quốc gia và duy trì và phát huy hơn nữa những kết quả là mục tiêu tiếp theo của Tập đoàn Austdoor. Trong thời gian tới tập đoàn sẽ tiếp tục đầu tư phát triển để mang đến những sản phẩm cửa cuốn, cửa nhôm và nhôm thanh định hình chất lượng tốt nhất cho khách hàng và đối tác, tạo nên những trải nghiệm khách hàng vượt trội để tự tin chinh phục những thị trường khó tính nhất, vươn tầm quốc tế.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;Hình ảnh này chưa có thuộc tính alt; tên tệp của nó là 7161416e5561a43ffd70-1024x726.jpg&quot; src=&quot;https://topal.vn/wp-content/uploads/2020/11/7161416e5561a43ffd70-1024x726.jpg&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Thương hiệu quốc gia là chương trình xúc tiến thương mại đặc thù, dài hạn và duy nhất của Chính phủ nhằm xây dựng, phát triển thương hiệu quốc gia thông qua thương hiệu sản phẩm. Việc tham gia chương trình là một quá trình để doanh nghiệp đánh giá toàn diện các hoạt động, kết quả sản xuất – kinh doanh và chiến lược xây dựng thương hiệu. Từ đó khuyến khích các doanh nghiệp chia sẻ và theo đuổi các giá trị cốt lõi của chương trình, gồm: &lt;strong&gt;chất lượng – đổi mới, sáng tạo – năng lực tiên phong. &lt;/strong&gt;Đây cũng chính là kim chỉ nam cho các hoạt động của Tập đoàn Austdoor để hiện thực hóa tầm nhìn trở thành Nhà cung cấp các giải pháp tổng thể về cửa và vật liệu xây dựng công nghệ cao hàng đầu khu vực Đông Nam Á.  &lt;/p&gt;', '&lt;p&gt;Cửa cuốn Austdoor và Nhôm Topal vừa được vinh danh trong Lễ công bố các doanh nghiệp đạt Thương hiệu quốc gia 2020. Đây là hai sản phẩm chủ lực của Tập đoàn Austdoor trong hành trình khẳng định vị thế hàng đầu trong lĩnh vực sản xuất và cung cấp cửa và vật liệu xây dựng tại Việt Nam.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/ca46eb2d1721e67fbf30-1024x764.jpg', '', '', '', 'topal-tu-hao-la-thuong-hieu-nhom-tre-nhat-dat-thuong-hieu-quoc-gia-2020', '', 'image', '', NULL),
(17, 2, 'Nhôm Topal xuất khẩu thành công sang Mỹ, Canada', '&lt;p&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;&lt;strong&gt;Trong bối cảnh nguồn cung nguyên liệu bị đứt gãy do ảnh hưởng dịch Covid-19, chi phí logistic tăng lên đáng kể… nhưng Topal vẫn quyết định đầu tư và tự tin tiến vào “sân chơi” toàn cầu – xuất khẩu nhôm định hình ra thị trường quốc tế.&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Nhận thấy tiềm năng lớn của thị trường khi lượng nhôm xuất khẩu của cả nước chỉ dưới 10% sản lượng và tập trung ở một số doanh nghiệp FDI, Topal đã sớm xác định phát triển cả 2 mũi nhọn: nội địa và xuất khẩu.&lt;/p&gt;\r\n\r\n&lt;p&gt;Đầu năm 2021, nhà máy nhôm Topal Nhơn Trạch, Đồng Nai đã khởi công giai đoạn 2 xây dựng mở rộng 11,000m2 nhà xưởng cho dây chuyền Anode hiện đại với công suất 6000 tấn/năm. Công nghệ này cho ra đời các sản phẩm nhôm Topal chất lượng cao mang tiêu chuẩn quốc tế như JIS H8601, JIS H8602,… với 3 dòng chính là Anode phun cát, Anode mờ và phủ ED.&lt;/p&gt;\r\n\r\n&lt;p&gt;Công nghệ anode là khâu xử lý bề mặt nhôm hiện đại nhất hiện nay, đó là quá trình đưa các chi tiết bằng nhôm vào môi trường điện ly để tạo trên bề mặt sản phẩm một lớp màng oxit nhôm có tính bền vững, bảo vệ cho chi tiết nhôm không bị ô xi hoá bề mặt. Ngoài ra lớp oxit nhôm có độ cứng cao (gần bằng kim cương), có khả năng chống ăn mòn và mài mòn tốt. Bởi vậy, nhôm được anode hóa không có hiện tượng han gỉ trên bề mặt, sáng bóng hơn, đảm bảo tính thẩm mỹ lâu dài cho sản phẩm.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 821px) 100vw, 821px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/07/1-1.png&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/07/1-1.png 821w, https://topal.vn/wp-content/uploads/2021/07/1-1-300x200.png 300w, https://topal.vn/wp-content/uploads/2021/07/1-1-768x513.png 768w, https://topal.vn/wp-content/uploads/2021/07/1-1-600x400.png 600w, https://topal.vn/wp-content/uploads/2021/07/1-1-219x146.png 219w, https://topal.vn/wp-content/uploads/2021/07/1-1-50x33.png 50w, https://topal.vn/wp-content/uploads/2021/07/1-1-112x75.png 112w&quot; /&gt;\r\n&lt;figcaption&gt;Nhà máy Nhôm Topal là nhà máy nhôm quy mô lớn hàng đầu tại Việt Nam&lt;/figcaption&gt;\r\n&lt;/figure&gt;\r\n\r\n&lt;p&gt;Nhôm anode là loại vật liệu nhẹ, độ bền màu và bề mặt cao nên có tính ứng dụng rộng rãi, không chỉ trong xây dựng mà còn được dùng trong sản xuất công nghiệp như sản xuất máy bay, ô tô, điện tử, y tế, đồ gia dụng…. Sở hữu dây chuyền anode cho phép Topal tự tin cung ứng các sản phẩm nhôm công nghiệp chất lượng cao cho thị trường quốc tế.&lt;/p&gt;\r\n\r\n&lt;p&gt;So với các sản phẩm nhôm thanh định hình dùng trong xây dựng, các sản phẩm nhôm công nghiệp đòi hỏi yêu cầu cao hơn trong các khâu mở khuôn, đùn ép, gia công, xử lý bề mặt và chi tiết sản phẩm,… Với khả năng mở khuôn linh hoạt, nhanh chóng cùng hệ thống dây chuyền đùn ép công suất lớn, có thể đùn billet nhôm với kích thước 10-12inch, đội ngũ nghiên cứu phát triển giàu kinh nghiệm, nhôm Topal đáp ứng yêu cầu các sản phẩm nhôm công nghiệp mọi kích thước từ rất lớn cho đến chi tiết linh kiện điện tử nhỏ như khung giá đỡ pin năng lượng mặt trời, ống vaccum tube máy hút bụi, khung viền tivi, tủ lạnh, máng cáp điện, các chi tiết máy điện tử… Trong những tháng qua, vượt qua những ảnh hưởng của dịch Covid-19, nhôm Topal đã chính thức có những lô hàng xuất khẩu đầu tiên sang các thị trường lớn và khắt khe như Mỹ, Canada, là lựa chọn của nhiều đối tác lớn như nhà cung cấp linh kiện cho Samsung và một số đối tác khác.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 896px) 100vw, 896px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/07/1222-1.png&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/07/1222-1.png 896w, https://topal.vn/wp-content/uploads/2021/07/1222-1-300x208.png 300w, https://topal.vn/wp-content/uploads/2021/07/1222-1-768x531.png 768w, https://topal.vn/wp-content/uploads/2021/07/1222-1-600x415.png 600w, https://topal.vn/wp-content/uploads/2021/07/1222-1-211x146.png 211w, https://topal.vn/wp-content/uploads/2021/07/1222-1-50x35.png 50w, https://topal.vn/wp-content/uploads/2021/07/1222-1-108x75.png 108w&quot; /&gt;\r\n&lt;figcaption&gt;Nhôm Topal được trải qua những công đoạn sản xuất hiện đại, tiêu chuẩn quốc tế.&lt;/figcaption&gt;\r\n&lt;/figure&gt;\r\n\r\n&lt;p&gt;Các sản phẩm nhôm Topal cung ứng ra thị trường nội địa hay xuất khẩu đều đảm bảo tiêu chuẩn quốc tế như: AL6005, AL6063… độ cứng T5, T6. Đây là các hợp kim nhôm có tính bền cao, có khả năng định hình, cứng cáp, khả năng chống mài mòn tốt. Bên cạnh đó là quy trình sản xuất và kiểm soát chất lượng đạt chứng nhận ISO 9001:2015 và Quy chuẩn kỹ thuật quốc gia về sản phẩm, hàng hóa QCVN 16:2019/BXD.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 820px) 100vw, 820px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/07/22222.png&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/07/22222.png 820w, https://topal.vn/wp-content/uploads/2021/07/22222-300x225.png 300w, https://topal.vn/wp-content/uploads/2021/07/22222-768x576.png 768w, https://topal.vn/wp-content/uploads/2021/07/22222-600x450.png 600w, https://topal.vn/wp-content/uploads/2021/07/22222-195x146.png 195w, https://topal.vn/wp-content/uploads/2021/07/22222-50x38.png 50w, https://topal.vn/wp-content/uploads/2021/07/22222-100x75.png 100w&quot; /&gt;\r\n&lt;figcaption&gt;Các kiện hàng nhôm Topal được bao gói sẵn sàng xuất khẩu.&lt;/figcaption&gt;\r\n&lt;/figure&gt;\r\n\r\n&lt;p&gt;Chỉ sau 3 năm, nhôm Topal của Tập đoàn Austdoor đã phát triển nhanh chóng để đạt được vị thế lớn trong ngành. Sở hữu 02 nhà máy quy mô lớn tại tỉnh Đồng Nai và Hưng Yên với tổng diện tích hơn 250,000m2, với dây chuyền công nghệ sản xuất hiện đại, Topal dần trở thành thương hiệu nhà sản xuất nhôm hàng đầu tại thị trường Việt Nam, cung ứng hàng trăm nghìn tấn nhôm mỗi năm.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với kinh nghiệm phát triển thị trường trong nước và nền tảng đầu tư ban đầu giúp Topal sẵn sàng bước chân ra thị trường quốc tế, xứng đáng với vị thế Thương hiệu Quốc gia của Việt Nam.&lt;/p&gt;', '&lt;p&gt;Trong bối cảnh nguồn cung nguyên liệu bị đứt gãy do ảnh hưởng dịch Covid-19, chi phí logistic tăng lên đáng kể… nhưng Topal vẫn quyết định đầu tư và tự tin tiến vào “sân chơi” toàn cầu – xuất khẩu nhôm định hình ra thị trường quốc tế.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/1-1.png', '', '', '', 'nhom-topal-xuat-khau-thanh-cong-sang-my-canada', '', 'image', '', NULL),
(16, 2, 'Quy trình sản xuất tạo nên sự khác biệt của nhôm Topal', '&lt;figure&gt;\r\n&lt;p&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/06/z2561697762863_4d3611cdc2c8c0598a2a9c4dca133165-1024x576.jpg&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/06/z2561697762863_4d3611cdc2c8c0598a2a9c4dca133165-1024x576.jpg 1024w, https://topal.vn/wp-content/uploads/2021/06/z2561697762863_4d3611cdc2c8c0598a2a9c4dca133165-300x169.jpg 300w, https://topal.vn/wp-content/uploads/2021/06/z2561697762863_4d3611cdc2c8c0598a2a9c4dca133165-768x432.jpg 768w, https://topal.vn/wp-content/uploads/2021/06/z2561697762863_4d3611cdc2c8c0598a2a9c4dca133165-1536x864.jpg 1536w, https://topal.vn/wp-content/uploads/2021/06/z2561697762863_4d3611cdc2c8c0598a2a9c4dca133165-2048x1152.jpg 2048w, https://topal.vn/wp-content/uploads/2021/06/z2561697762863_4d3611cdc2c8c0598a2a9c4dca133165-600x338.jpg 600w, https://topal.vn/wp-content/uploads/2021/06/z2561697762863_4d3611cdc2c8c0598a2a9c4dca133165-260x146.jpg 260w, https://topal.vn/wp-content/uploads/2021/06/z2561697762863_4d3611cdc2c8c0598a2a9c4dca133165-50x28.jpg 50w, https://topal.vn/wp-content/uploads/2021/06/z2561697762863_4d3611cdc2c8c0598a2a9c4dca133165-133x75.jpg 133w, https://topal.vn/wp-content/uploads/2021/06/z2561697762863_4d3611cdc2c8c0598a2a9c4dca133165-1200x675.jpg 1200w&quot; /&gt;&lt;/p&gt;\r\n&lt;/figure&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;&lt;em&gt;N&lt;/em&gt;&lt;/strong&gt;&lt;strong&gt;&lt;em&gt;hôm đang là &lt;/em&gt;&lt;/strong&gt;&lt;strong&gt;&lt;em&gt;vật liệu xu thế, là &lt;/em&gt;&lt;/strong&gt;&lt;strong&gt;&lt;em&gt;lựa chọn tối ưu cho các gia đình hiện đại&lt;/em&gt;&lt;/strong&gt;&lt;strong&gt;&lt;em&gt;, được ứng dụng từ hệ thống cửa đi, cửa thông phòng cho đến nhôm trang trí nội ngoại thất&lt;/em&gt;&lt;/strong&gt;&lt;strong&gt;&lt;em&gt;. Tuy nhiên giữa vô vàn các sản phẩm cửa nhôm trên thị trường, đâu là tiêu chí để có thể lựa chọn được sản phẩm nhôm tốt? &lt;/em&gt;&lt;/strong&gt;&lt;strong&gt;&lt;em&gt;Hãy cũng tìm hiểu &lt;/em&gt;&lt;/strong&gt;&lt;strong&gt;&lt;em&gt;quy trình sản xuất để tạo nên sự khác biệt cho các thanh nhôm Topal.&lt;/em&gt;&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;N&lt;/strong&gt;&lt;strong&gt;guyên vật liệu chất lượng cao&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Một bộ cửa nhôm, một sản phẩm trang trí nội ngoại thất tốt cần được làm từ nhôm thanh chất lượng. Nguyên liệu từ hợp kim nhôm cao cấp sẽ giúp bộ cửa có kết cấu chắc chắn, không bị ảnh hưởng của va chạm mạnh.&lt;/p&gt;\r\n\r\n&lt;p&gt;Hiện nay, TOPAL sử dụng billet nhôm có chất lượng cao đạt tiêu chuẩn quốc tế là AL6061 và AL6063.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 821px) 100vw, 821px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/06/6.png&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/06/6.png 821w, https://topal.vn/wp-content/uploads/2021/06/6-300x200.png 300w, https://topal.vn/wp-content/uploads/2021/06/6-768x513.png 768w, https://topal.vn/wp-content/uploads/2021/06/6-600x400.png 600w, https://topal.vn/wp-content/uploads/2021/06/6-219x146.png 219w, https://topal.vn/wp-content/uploads/2021/06/6-50x33.png 50w, https://topal.vn/wp-content/uploads/2021/06/6-112x75.png 112w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Hợp kim nhôm AL6063 có tính bền cao, cứng cáp, chịu được va chạm mạnh, khả năng chống mài mòn cực tốt. &lt;strong&gt;Hợp kim nhôm AL6063&lt;/strong&gt; có thể hàn được, có tính gia công và khả năng định hình tốt, ứng dụng cho một số ngành như: chế tạo máy và hàng không, vũ trụ, viễn thông, giao thông vận tải, sản xuất máy móc, chế tạo cơ khí, công trình xây dựng,…&lt;/p&gt;\r\n\r\n&lt;p&gt;Hợp kim nhôm 6061 có độ bền cao, khả năng định hình, chống mài mòn tốt, thuận tiện cho việc hàn, gia công. Là một loại &lt;strong&gt;nhôm hợp kim&lt;/strong&gt; được sử dụng rộng rãi và rất được phổ biến cho tất cả các ứng dụng kết cấu chẳng hạn như: hàng không, bán dẫn, đồ gá lắp và cố định.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Công nghệ sản xuất và quy trình kiểm soát chất lượng chặt chẽ&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt; Nguyên liệu đầu vào tốt cần được sản xuất trên dây chuyền hiện đại, quy trình kiểm tra chất lượng chặt chẽ và sản xuất đồng bộ để đưa đến tay người tiêu dùng những sản phẩm nhôm tốt nhất.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 821px) 100vw, 821px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/06/5.png&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/06/5.png 821w, https://topal.vn/wp-content/uploads/2021/06/5-300x200.png 300w, https://topal.vn/wp-content/uploads/2021/06/5-768x513.png 768w, https://topal.vn/wp-content/uploads/2021/06/5-600x400.png 600w, https://topal.vn/wp-content/uploads/2021/06/5-219x146.png 219w, https://topal.vn/wp-content/uploads/2021/06/5-50x33.png 50w, https://topal.vn/wp-content/uploads/2021/06/5-112x75.png 112w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Sản phẩm TOPAL được sản xuất trên dây chuyền sản xất tiên tiến được áp dụng các công nghệ hiện đại. Hóa già là một quá trình cần được thực hiện đúng tiêu chuẩn, tiêu tốn nhiều thời gian và năng lượng để đảm bảo độ cứng của thanh nhôm (thường là T5 hoặc T6). Công đoạn kiểm tra chất lượng sau khi hóa già là bước không thể thiếu để đảm bảo cơ tính của thanh nhôm.&lt;/p&gt;\r\n\r\n&lt;figure&gt;\r\n&lt;p&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 821px) 100vw, 821px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/06/15.png&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/06/15.png 821w, https://topal.vn/wp-content/uploads/2021/06/15-300x200.png 300w, https://topal.vn/wp-content/uploads/2021/06/15-768x513.png 768w, https://topal.vn/wp-content/uploads/2021/06/15-600x400.png 600w, https://topal.vn/wp-content/uploads/2021/06/15-219x146.png 219w, https://topal.vn/wp-content/uploads/2021/06/15-50x33.png 50w, https://topal.vn/wp-content/uploads/2021/06/15-112x75.png 112w&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;figcaption&gt;\r\n&lt;p&gt;&lt;em&gt;Quy trình sản xuất &amp; kiểm soát chất lượng theo tiêu chuẩn quốc tế giúp cửa nhôm TOPAL đảm bảo chất lượng cao nhất.&lt;/em&gt;&lt;/p&gt;\r\n&lt;/figcaption&gt;\r\n&lt;/figure&gt;\r\n\r\n&lt;p&gt;Sau đó, thanh nhôm được đưa sang công đoạn xử lý bề mặt và sơn tĩnh điện hoặc phủ vân gỗ để tạo tính thẩm mỹ cho sản phẩm. Sau khi hoàn thiện bề mặt sơn hoặc phủ vân gỗ, thanh nhôm được đưa sang công đoạn dán băng keo để bảo vệ bề mặt, bao gói và nhập kho, sẵn sàng đến với khách hàng trong và ngoài nước, góp phần không nhỏ trong việc tạo ra những bộ cửa nhôm bền bỉ, thẩm mỹ.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Sơn bề mặt cao cấp&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt; Kim loại nhôm ngoài việc có trọng lượng thấp còn có một ưu điểm rất lớn đó là chống lại được sự ăn mòn thụ động. Chính vì vậy, nhôm rất dễ bảo quản ở điều kiện môi trường bình thường. Tuy nhiên, khi bị ngâm trong dung dịch kiềm đặc, nhôm sẽ bị phá hủy do xảy ra phản ứng và tạo muối alumi. Chính vì vậy, việc bảo quản nhôm là việc làm cần thiết.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 821px) 100vw, 821px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/06/35.png&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/06/35.png 821w, https://topal.vn/wp-content/uploads/2021/06/35-300x200.png 300w, https://topal.vn/wp-content/uploads/2021/06/35-768x513.png 768w, https://topal.vn/wp-content/uploads/2021/06/35-600x400.png 600w, https://topal.vn/wp-content/uploads/2021/06/35-219x146.png 219w, https://topal.vn/wp-content/uploads/2021/06/35-50x33.png 50w, https://topal.vn/wp-content/uploads/2021/06/35-112x75.png 112w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Sơn bề mặt trên nhôm ra đời ngoài việc khiến cho bề mặt kim loại có màu sắc đẹp mắt còn tạo ra một lớp sơn bảo vệ. Sơn có khả năng bám dính tốt trên nhôm, tuyệt đối không thấm nước, kháng được cả hóa chất và dung môi, bền theo thời gian nên có thể làm tăng tuổi thọ của kim loại lên một thời gian dài.&lt;/p&gt;\r\n\r\n&lt;p&gt;Các thanh nhôm Topal luôn được nhúng lần lượt qua nhiều bể hóa chất để xử lý bề mặt, từ làm sạch đến tẩy dầu, phủ chromat …để tạo bề mặt hoàn hảo trước khi đưa vào lò sơn tĩnh điện.&lt;/p&gt;\r\n\r\n&lt;p&gt;Được sơn trên dây chuyền sơn tĩnh điện hiện đại, nhôm Topal đã được kiểm định bởi hai hãng sơn hàng đầu thế giới là Akzonobel và Tiger, đủ khả năng sản xuất các loại sơn với thời gian bảo hành lên tới 30 năm.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Thiết kế vượt trội&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Để kiến tạo nên những sản phẩm nhôm tốt, cần phải nghiên cứu, khảo sát thị trường để hiểu rõ nhu cầu sử dụng của người tiêu dùng Việt.&lt;/p&gt;\r\n\r\n&lt;p&gt;Chính vì vậy, Topal đã đầu tư mạnh mẽ vào Nghiên cứu &amp; Phát triển sản phẩm để cho ra đời các dòng sản phẩm được thiết kế riêng biệt, có nhiều tính năng vượt trội, phù hợp và tối ưu với nhu cầu sử dụng của người tiêu dùng, với Văn hóa và khí hậu Việt Nam.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 821px) 100vw, 821px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/06/33.png&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/06/33.png 821w, https://topal.vn/wp-content/uploads/2021/06/33-300x200.png 300w, https://topal.vn/wp-content/uploads/2021/06/33-768x513.png 768w, https://topal.vn/wp-content/uploads/2021/06/33-600x400.png 600w, https://topal.vn/wp-content/uploads/2021/06/33-219x146.png 219w, https://topal.vn/wp-content/uploads/2021/06/33-50x33.png 50w, https://topal.vn/wp-content/uploads/2021/06/33-112x75.png 112w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Không chỉ là các sản phẩm nhôm chất lượng cao, TOPAL còn phát triển hệ thống phụ kiện, vật tư lắp đặt đồng bộ, chất lượng nhằm đảo bảo chất lượng cao nhất cho các sản phẩm hoàn thiện.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;&lt;em&gt;Với tất cả những tiêu chí trên, nhôm của thương hiệu Topal đã vinh dự được chứng nhận sản phẩm Thương hiệu quốc gia năm 2020 và là sự lựa chọn hoàn hảo cho những gia đình Việt Nam hiện đại. Người tiêu dùng có thể dễ dàng đặt mua nhôm Topal thông qua hệ thống Đại lý ủy quyền khắp 63 tỉnh thành. Mỗi Đại lý đều được Topal lựa chọn và đào tạo kỹ lưỡng, được cấp chứng chỉ ủy quyền cung cấp sản phẩm cửa nhôm Topal với mục tiêu giữ vững chất lượng sản phẩm đến tay người tiêu dùng.&lt;/em&gt;&lt;/strong&gt;&lt;/p&gt;', '&lt;p&gt;&lt;em&gt;Nhôm đang là vật liệu xu thế, là lựa chọn tối ưu cho các gia đình hiện đại, được ứng dụng từ hệ thống cửa đi, cửa thông phòng cho đến nhôm trang trí nội ngoại thất. Tuy nhiên giữa vô vàn các sản phẩm cửa nhôm trên thị trường, đâu là tiêu chí để có thể lựa chọn được sản phẩm nhôm tốt? Hãy cũng tìm hiểu quy trình sản xuất để tạo nên sự khác biệt cho các thanh nhôm Topal.&lt;/em&gt;&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/z2561697762863_4d3611cdc2c8c0598a2a9c4dca133165-1200x675.jpg', '', '', '', 'quy-trinh-san-xuat-tao-nen-su-khac-biet-cua-nhom-topal', '', 'image', '', NULL),
(19, 2, 'Nhôm Topal tái định vị thương hiệu', '&lt;p&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;&lt;strong&gt;&lt;em&gt;Không dừng lại ở nhôm hệ và phụ kiện, Topal tuyên bố mục tiêu trở thành nhà sản xuất nhôm hàng đầu Việt Nam.&lt;/em&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Ngày 16/10, Tập đoàn Austdoor tổ chức sự kiện trực tuyến tái định vị thương hiệu với gần 1.000 khách tham dự là các đối tác, đại lý phân phối, đại lý sản xuất của nhôm Topal và các nhãn hiệu khác.&lt;/p&gt;\r\n\r\n&lt;p&gt;Theo ông Dương Quốc Tuấn – Chủ tịch HĐQT, Tổng giám đốc Tập đoàn Austdoor, việc tái định vị thương hiệu là cách mà Topal sẽ tháo bỏ lớp áo đã chật. Để khẳng định việc thay đổi chiến lược và tầm nhìn là nhà sản xuất nhôm hàng đầu tại Việt Nam, tập đoàn xác định chiến lược bao gồm: hoàn thiện chuỗi giá trị nghiên cứu, sản xuất, cung ứng, phân phối, hậu mãi để mang lại những giá trị gia tăng cho khách hàng; xây dựng một hệ sinh thái sản phẩm và dịch vụ đồng bộ, khép kín hay nói cách khác là từ sản xuất ra nguyên vật liệu tới sản phẩm hoàn thiện; và tích cực chuyển đổi số ở mọi hoạt động trong chuỗi giá trị trên.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;Ông Dương Quốc Tuấn – Chủ tịch HĐQT Tập đoàn Austdoor công bố tái định vị thương hiệu Topal&quot; src=&quot;https://i1-kinhdoanh.vnecdn.net/2021/10/16/nhom-jpeg-3571-1634378678.png?w=680&amp;amp;h=0&amp;amp;q=100&amp;amp;dpr=1&amp;amp;fit=crop&amp;amp;s=X1Ei9M5-ke49e9Wvnaz0OQ&quot; /&gt;\r\n&lt;figcaption&gt;Ông Dương Quốc Tuấn – Chủ tịch HĐQT Tập đoàn Austdoor tại sự kiện công bố tái định vị thương hiệu Topal.&lt;/figcaption&gt;\r\n&lt;/figure&gt;\r\n\r\n&lt;p&gt;Sau 4 năm ra mắt, nhôm Topal tạo dựng được vị thế trên thị trường với các sản phẩm nhôm hệ và phụ kiện. Bên cạnh đó, những nỗ lực xây dựng hệ thống từ nhà máy sản xuất, cung ứng đến mạng lưới phân phối ủy quyền rộng khắp đã giúp nhôm Topal đạt được nhiều thành tích, trong đó có chứng nhận “Thương hiệu quốc gia năm 2020”.&lt;/p&gt;\r\n\r\n&lt;p&gt;Tuy nhiên vài năm qua, khách hàng đang nhớ tới Topal chủ yếu với các sản phẩm cửa nhôm đồng bộ kín nước – chống ồn. Thông qua việc tái định vị, thương hiệu, doanh nghiệp này muốn khẳng định với hệ thống nhà máy quy mô lớn, dây chuyền sản xuất và công nghệ anode hiện đại, mạng lưới phân phối, sản xuất lắp đặt tại 63 tỉnh thành, Topal tự tin cung cấp đa dạng các giải pháp nhôm từ xây dựng, công nghiệp cho đến trang trí nội ngoại thất.&lt;/p&gt;\r\n\r\n&lt;p&gt;Cùng việc tái định vị thương hiệu, Topal cũng thay bộ nhận diện mới với màu cam và xanh Topal nổi bật trên nền màu ghi sáng của nhôm nguyên bản, nhằm thể hiện rõ hơn thông điệp “Topal là nhà sản xuất nhôm”, xây dựng một hệ sinh thái khép kín xoay quanh nhôm.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;Ông Dương Quốc Tuấn giới thiệu bộ nhận diện thương hiệu mới của nhôm Topal.&quot; src=&quot;https://i1-kinhdoanh.vnecdn.net/2021/10/16/Nhom-1-6348-1634395130.png?w=680&amp;amp;h=0&amp;amp;q=100&amp;amp;dpr=1&amp;amp;fit=crop&amp;amp;s=DRYsqKcwcTXBVzSunj1Gmg&quot; /&gt;\r\n&lt;figcaption&gt;Ông Dương Quốc Tuấn giới thiệu bộ nhận diện thương hiệu mới của nhôm Topal.&lt;/figcaption&gt;\r\n&lt;/figure&gt;\r\n\r\n&lt;p&gt;Với vai trò là nhà sản xuất nhôm, Topal chú trọng vào chiến lược phát triển kênh phân phối. Hệ thống đại lý phân phối và sản xuất, lắp đặt vốn luôn là mắt xích quan trọng bậc nhất trong chuỗi cung ứng sản phẩm vật liệu xây dựng. Bởi vậy, bên cạnh những hoạt động đồng hành được duy trì trong nhiều năm qua, với chiến lược mới, Topal cũng phát triển các chính sách thu hút và mở rộng mạng lưới kinh doanh.&lt;/p&gt;\r\n\r\n&lt;p&gt;Không chỉ duy trì những chính sách về kinh doanh, marketing, đơn vị còn hỗ trợ các nguồn lực về tư vấn, thiết kế, specs, để triển khai các dự án tại từng địa phương, đây cũng là một hướng đi tiềm năng mà Topal có thể triển khai mạnh cùng các đại lý. Bên cạnh đó, công ty số hóa công cụ, ứng dụng như app thợ-đại lý, e-catalogue, phần mềm đặt hàng hay web-hub và nền tảng thương mại điện tử hợp nhất. Đây là một điểm mới được coi là sẽ tạo nên sự khác biệt của Topal so với các nhà sản xuất khác.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;Nhôm Topal giới thiệu các công cụ số hóa hỗ trợ kinh doanh&quot; src=&quot;https://i1-kinhdoanh.vnecdn.net/2021/10/16/nhom3-4319-1634378678.png?w=680&amp;amp;h=0&amp;amp;q=100&amp;amp;dpr=1&amp;amp;fit=crop&amp;amp;s=7hIaJjv9fadjohsSN3XAfA&quot; /&gt;\r\n&lt;figcaption&gt;Nhôm Topal giới thiệu các công cụ số hóa hỗ trợ kinh doanh.&lt;/figcaption&gt;\r\n&lt;/figure&gt;\r\n\r\n&lt;p&gt;Đại diện công ty chia sẻ, thị trường nhôm Việt Nam đang có một tiềm năng phát triển, không chỉ đáp ứng nhu cầu trong nước mà còn dần trở thành “điểm đến” yêu thích của các doanh nghiệp quốc tế. Với việc tái định vị và những thay đổi mang tính chiến lược trong giai đoạn mới, Topal kỳ vọng sẽ cùng những doanh nghiệp Việt giàu tâm huyết phát triển ngành nhôm Việt và đóng góp ngày một nhiều hơn cho sự hồi phục và phát triển chung của nền kinh tế.&lt;/p&gt;', '&lt;p&gt;Không dừng lại ở nhôm hệ và phụ kiện, Topal tuyên bố mục tiêu trở thành nhà sản xuất nhôm hàng đầu Việt Nam.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/nhom-jpeg-3571-1634378678.png', '', '', '', 'nhom-topal-tai-dinh-vi-thuong-hieu', '', 'image', '', NULL),
(20, 2, 'Lắp cửa Topal - Ngập tràn quà tặng', '&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 900px) 100vw, 900px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-01.png&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-01.png 900w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-01-300x151.png 300w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-01-768x387.png 768w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-01-600x302.png 600w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-01-260x131.png 260w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-01-50x25.png 50w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-01-150x75.png 150w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Từ ngày 05/11/2021 đến hết ngày 05/01/2022, Tập đoàn Austdoor tổ chức chương trình khuyến mãi “Lắp cửa Topal – Ngập tràn quà tặng” với tổng giá trị gần 1 tỷ đồng dành tặng khách hàng trên toàn quốc khi lựa chọn các sản phẩm cửa nhôm Topal.&lt;/p&gt;\r\n\r\n&lt;p&gt;Theo đó, tất các khách hàng trên toàn quốc khi sử dụng cửa nhôm dồng bộ Topal Slima, Topal Prima cho công trình của mình có giá trị nhôm hệ và phụ kiện từ 25 triệu đồng sẽ được tham dự 2 chương trình khuyến mãi cùng lúc và có cơ hội nhận được những phần quà hấp dẫn và ý nghĩa từ Tập đoàn Austdoor.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;CHƯƠNG TRÌNH 1: QUÀ TẶNG MŨ BẢO HIỂM TOPAL&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Trong thời gian diễn ra chương trình, 2022 công trình đầu tiên sử dụng cửa nhôm hệ đồng bộ đảm bảo điều kiện trên sẽ nhận được 01 mũ bảo hiểm Topal.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;CHƯƠNG TRÌNH 2: QUÀ TẶNG QUAY SỐ MAY MẮN&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Trong thời gian diễn ra chương trình, mỗi công trình đảm bảo điều kiện sẽ nhận được 01 mã số dự thưởng để tiến hành quay số ngẫu nhiên bằng phần mềm điện tử xác định khách hàng trúng giải mỗi tháng với các giải thương hấp dẫn như Xe Honda SH Mode, Smart Tivi Sony 43inch, Nồi chiên không dầu.&lt;/p&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 1024px) 100vw, 1024px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-02-1024x1024.png&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-02-1024x1024.png 1024w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-02-300x300.png 300w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-02-150x150.png 150w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-02-768x768.png 768w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-02-1536x1536.png 1536w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-02-600x600.png 600w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-02-146x146.png 146w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-02-50x50.png 50w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-02-75x75.png 75w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-02-85x85.png 85w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-02-80x80.png 80w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-02.png 1876w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;figure&gt;&lt;img alt=&quot;&quot; sizes=&quot;(max-width: 901px) 100vw, 901px&quot; src=&quot;https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-03.png&quot; srcset=&quot;https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-03.png 901w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-03-300x300.png 300w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-03-150x150.png 150w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-03-768x768.png 768w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-03-600x600.png 600w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-03-146x146.png 146w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-03-50x50.png 50w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-03-75x75.png 75w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-03-85x85.png 85w, https://topal.vn/wp-content/uploads/2021/11/CTKM-TP-NTD-Post-FB-03-80x80.png 80w&quot; /&gt;&lt;/figure&gt;\r\n\r\n&lt;p&gt;Mọi thông tin chi tiết xin vui lòng liên hệ với đại lý của nhôm Topal gần nhất, hoặc inbox ngay cho Fanpage Nhôm Topal, hoặc liên hệ số điện thoại đường dây nóng: 1900 6828.&lt;/p&gt;', '&lt;p&gt;Từ ngày 05/11/2021 đến hết ngày 05/01/2022, Tập đoàn Austdoor tổ chức chương trình khuyến mãi “Lắp cửa Topal – Ngập tràn quà tặng” với tổng giá trị gần 1 tỷ đồng dành tặng khách hàng trên toàn quốc khi lựa chọn các sản phẩm cửa nhôm Topal.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/ctkm-tp-ntd-post-fb-01.png', '', '', '', 'lap-cua-topal-ngap-tran-qua-tang', '', 'image', '', NULL),
(21, 2, 'Sài Gòn Peninsula', '&lt;div class=&quot;container&quot;&gt;\r\n&lt;div class=&quot;project-content py-3 mb-4&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/1_AcsfwoB.png&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Vừa qua, Phú Gia An vinh dự khi là đối tác trong mảng thi công lắp đặt cửa nhôm kính cho toàn bộ hệ thống khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh. Chúng tôi luôn tự tin mang lại sự hài lòng cho mọi khách hàng từ sản phẩm cho đến dịch vụ của mình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh vừa chính thức đi vào hoạt động những ngày qua, đây chính là một trong những đối tác quan trọng mà Phú Gia An hợp tác trong hạng mục thi công lắp đặt cửa nhôm kính. Toàn bộ hệ thống khu nghỉ dưỡng đều sử dụng các sản phẩm cửa nhôm kính do Phú Gia An cung cấp.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia bao gồm 2 toà nhà chính với mật độ xây dựng cửa nhôm kính lên tới 75%. Cùng tham khảo một số hình ảnh đã hoàn thiện của công trình này dưới đây:&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-6&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/2_Rk3ffU4.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/3_SCVHjaX.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n\r\n&lt;p class=&quot;text-center&quot;&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/4_rCSW4ec.png&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End project content --&gt;&lt;/div&gt;', '&lt;p&gt;Cửa nhôm kính hiện tại vẫn luôn là xu hướng lựa chọn của nhiều gia chủ trong mọi công trình của mình. Không chỉ mang lại nhưng ưu điểm tuyệt vời về công năng sử dụng mà cửa nhôm kính còn được đánh giá cao về tính thẩm mỹ mang lại cho không gian đó.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/9.png', '', '', '', 'sai-gon-peninsula-1', '', 'image', '', NULL),
(22, 2, 'Dự án bệnh viện Vinmec Nha Trang', '&lt;div class=&quot;container&quot;&gt;\r\n&lt;div class=&quot;project-content py-3 mb-4&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/1_AcsfwoB.png&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Vừa qua, Phú Gia An vinh dự khi là đối tác trong mảng thi công lắp đặt cửa nhôm kính cho toàn bộ hệ thống khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh. Chúng tôi luôn tự tin mang lại sự hài lòng cho mọi khách hàng từ sản phẩm cho đến dịch vụ của mình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh vừa chính thức đi vào hoạt động những ngày qua, đây chính là một trong những đối tác quan trọng mà Phú Gia An hợp tác trong hạng mục thi công lắp đặt cửa nhôm kính. Toàn bộ hệ thống khu nghỉ dưỡng đều sử dụng các sản phẩm cửa nhôm kính do Phú Gia An cung cấp.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia bao gồm 2 toà nhà chính với mật độ xây dựng cửa nhôm kính lên tới 75%. Cùng tham khảo một số hình ảnh đã hoàn thiện của công trình này dưới đây:&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-6&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/2_Rk3ffU4.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/3_SCVHjaX.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n\r\n&lt;p class=&quot;text-center&quot;&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/4_rCSW4ec.png&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End project content --&gt;&lt;/div&gt;', '&lt;p&gt;Cửa nhôm kính hiện tại vẫn luôn là xu hướng lựa chọn của nhiều gia chủ trong mọi công trình của mình. Không chỉ mang lại nhưng ưu điểm tuyệt vời về công năng sử dụng mà cửa nhôm kính còn được đánh giá cao về tính thẩm mỹ mang lại cho không gian đó.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/2.png', '', '', '', 'du-an-benh-vien-vinmec', '', 'image', '', NULL),
(23, 2, 'Dự án khu đô thị Lavila Đông Sài Gòn', '&lt;div class=&quot;container&quot;&gt;\r\n&lt;div class=&quot;project-content py-3 mb-4&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/1_AcsfwoB.png&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Vừa qua, Phú Gia An vinh dự khi là đối tác trong mảng thi công lắp đặt cửa nhôm kính cho toàn bộ hệ thống khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh. Chúng tôi luôn tự tin mang lại sự hài lòng cho mọi khách hàng từ sản phẩm cho đến dịch vụ của mình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh vừa chính thức đi vào hoạt động những ngày qua, đây chính là một trong những đối tác quan trọng mà Phú Gia An hợp tác trong hạng mục thi công lắp đặt cửa nhôm kính. Toàn bộ hệ thống khu nghỉ dưỡng đều sử dụng các sản phẩm cửa nhôm kính do Phú Gia An cung cấp.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia bao gồm 2 toà nhà chính với mật độ xây dựng cửa nhôm kính lên tới 75%. Cùng tham khảo một số hình ảnh đã hoàn thiện của công trình này dưới đây:&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-6&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/2_Rk3ffU4.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/3_SCVHjaX.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n\r\n&lt;p class=&quot;text-center&quot;&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/4_rCSW4ec.png&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End project content --&gt;&lt;/div&gt;', '&lt;p&gt;Cửa nhôm kính hiện tại vẫn luôn là xu hướng lựa chọn của nhiều gia chủ trong mọi công trình của mình. Không chỉ mang lại nhưng ưu điểm tuyệt vời về công năng sử dụng mà cửa nhôm kính còn được đánh giá cao về tính thẩm mỹ mang lại cho không gian đó.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/3.png', '', '', '', 'khu-do-thi-lavila-dong-sai-gon', '', 'image', '', NULL),
(24, 2, 'Vincom Hà Tĩnh', '&lt;div class=&quot;container&quot;&gt;\r\n&lt;div class=&quot;project-content py-3 mb-4&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/1_AcsfwoB.png&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Vừa qua, Phú Gia An vinh dự khi là đối tác trong mảng thi công lắp đặt cửa nhôm kính cho toàn bộ hệ thống khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh. Chúng tôi luôn tự tin mang lại sự hài lòng cho mọi khách hàng từ sản phẩm cho đến dịch vụ của mình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh vừa chính thức đi vào hoạt động những ngày qua, đây chính là một trong những đối tác quan trọng mà Phú Gia An hợp tác trong hạng mục thi công lắp đặt cửa nhôm kính. Toàn bộ hệ thống khu nghỉ dưỡng đều sử dụng các sản phẩm cửa nhôm kính do Phú Gia An cung cấp.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia bao gồm 2 toà nhà chính với mật độ xây dựng cửa nhôm kính lên tới 75%. Cùng tham khảo một số hình ảnh đã hoàn thiện của công trình này dưới đây:&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-6&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/2_Rk3ffU4.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/3_SCVHjaX.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n\r\n&lt;p class=&quot;text-center&quot;&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/4_rCSW4ec.png&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End project content --&gt;&lt;/div&gt;', '&lt;p&gt;Cửa nhôm kính hiện tại vẫn luôn là xu hướng lựa chọn của nhiều gia chủ trong mọi công trình của mình. Không chỉ mang lại nhưng ưu điểm tuyệt vời về công năng sử dụng mà cửa nhôm kính còn được đánh giá cao về tính thẩm mỹ mang lại cho không gian đó.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopalxfec/1.png', '', '', '', 'vincom-ha-tinh', '', 'image', '', NULL);

-- oc_blog_to_blog_category NO PRIMARY KEY
-- remove first
DELETE FROM `oc_blog_to_blog_category`
WHERE `blog_id` IN (1,2,4,5,6,7,8,9,10,11,12,13,14,15,18,17,16,19,20,21,22,23,24) AND `blog_category_id` = 1;

-- insert
INSERT IGNORE INTO `oc_blog_to_blog_category` (`blog_id`, `blog_category_id`) VALUES
(1, 1),
(2, 7),
(4, 5),
(5, 5),
(6, 5),
(7, 5),
(8, 5),
(9, 5),
(10, 1),
(11, 4),
(12, 3),
(13, 1),
(14, 4),
(15, 4),
(16, 1),
(17, 6),
(18, 6),
(19, 6),
(20, 4),
(21, 5),
(22, 5),
(23, 5),
(24, 5);

-- --------End - Load theme default data ---------------------

-- --------Add migrated versions for new shops ---------------

-- Dumping data for table `oc_migration`
INSERT INTO `oc_migration` (`migration`, `run`, `date_run`) VALUES
-- ('migration_20190718_default_policy_gg_shopping', 1, NOW()), -- already above when first time add migration feature
('migration_20190729_enable_seo', 1, NOW()),
('migration_20190729_2_fix_demohttp://x2.bestme.test/du-an-thi-cong-cua-nhom-kinh-cho-he-thong-vinmart?type=du-an_data_sql', 1, NOW()),
('migration_20190731_remake_menu', 1, NOW()),
('migration_20190810_staff_permission', 1, NOW()),
('migration_20190822_product_multi_version', 1, NOW()),
('migration_20190828_chatbot', 1, NOW()),
('migration_20190828_transport', 1, NOW()),
('migration_20190903_sync_staffs_to_welcome', 1, NOW()),
('migration_20190904_keep_edited_demo_data', 1, NOW()),
('migration_20190930_increase_decimal_order', 1, NOW()),
('migration_20191015_new_report_tables_and_events', 1, NOW()),
('migration_20191016_new_report_data', 1, NOW()),
('migration_20191023_new_report_events', 1, NOW()),
('migration_20191024_report_order_from_shop', 1, NOW()),
('migration_20191028_add_field_from_domain_order', 1, NOW()),
('migration_20191029_default_delivery_method', 1, NOW()),
('migration_20191111_blog_permission_and_seo', 1, NOW()),
('migration_20191115_order_success_seo_and_text', 1, NOW()),
('migration_20191116_add_settings_classify_permission', 1, NOW()),
('migration_20191127_sync_onboading_to_welcome', 1, NOW()),
('migration_20191209_default_store', 1, NOW()),
('migration_20191210_add_columns_for_pos', 1, NOW()),
('migration_20191223_save_image_url_to_database', 1, NOW()),
('migration_20191225_builder_product_per_row_mobile', 1, NOW()),
('migration_20191226_register_success_text', 1, NOW()),
('migration_20200103_soft_delete_product', 1, NOW()),
('migration_20200117_add_settings_notify_permission', 1, NOW()),
('migration_20200212_discount', 1, NOW()),
('migration_20200314_chatbot_v2', 1, NOW()),
('migration_20200312_store_receipt', 1, NOW()),
('migration_20200410_sync_report_order_to_cms', 1, NOW()),
('migration_20200420_add_table_order_utm', 1, NOW()),
('migration_20200420_store_permission', 1, NOW()),
('migration_20200421_fill_product_to_store_with_default_store', 1, NOW()),
('migration_20200422_add_column_table_product', 1, NOW()),
('migration_20200520_appstore_version', 1, NOW()),
('migration_20200626_advance_store_manager_version', 1, NOW()),
('migration_20200717_shopee_connection', 1, NOW()),
('migration_20200731_add_column_table_shopee_order', 1, NOW()),
('migration_20200811_novaonx_social', 1, NOW()),
('migration_20200816_add_column_demo_blog_table', 1, NOW()),
('migration_20200812_lazada_connection', 1, NOW()),
('migration_20200817_add_column_alt_image_blog_table', 1, NOW()),
('migration_20200817_move_chatbot_to_app_store', 1, NOW()),
('migration_20200818_onfluencer_app', 1, NOW()),
('migration_20200821_lazada_sync_order', 1, NOW()),
('migration_20200819_add_columns_for_customer', 1, NOW()),
('migration_20200904_cash_flow', 1, NOW()),
('migration_20200821_lazada_sync_order_again', 1, NOW()),
('migration_20200904_activity_log_analytics', 1, NOW()),
('migration_20200921_lazada_product_short_description', 1, NOW()),
('migration_20200915_shopee_advance_product', 1, NOW()),
('migration_20200916_pull_image_from_cloudinary', 1, NOW()),
('migration_20200922_transport_status_management', 1, NOW()),
('migration_20201007_tracking_pos_order_complete', 1, NOW()),
-- ('migration_20200924_full_text_search_product_name', 1, NOW()), -- temp not use
('migration_20201002_add_video_to_blog', 1, NOW()),
('migration_20201021_pos_return_goods', 1, NOW()),
('migration_20201019_bestme_package', 1, NOW()),
('migration_20201023_update_image_size', 1, NOW()),
('migration_20201103_create_image_folder', 1, NOW()),
('migration_20201106_add_columns_for_report', 1, NOW()),
('migration_20201118_add_columns_keyword_for_blog', 1, NOW()),
('migration_20201118_create_attribute_filter_table', 1, NOW()),
('migration_20201130_add_permission_customize_layout', 1, NOW()),
('migration_20201201_shopee_category_and_logistics', 0, NOW()), -- SET RUN=0 to keep running for new shop for loading data from file to shopee_category and shoppe_logistics table!
('migration_20201214_shopee_upload_add_tables', 1, NOW()),
('migration_20210107_tuning_2_11_2', 1, NOW()),
('migration_20210310_v3_3_1_advance_permission', 1, NOW()),
('migration_20210329_3_3_3_finetune', 1, NOW()),
('migration_20210409_add_columns_collection_amount_for_order', 1, NOW()),
('migration_20201125_add_column_store_id_for_return_receipt', 1, NOW()),
('migration_20210422_add_column_store_id_for_return_receipt_if_not_exists', 1, NOW()),
('migration_20210409_add_columns_collection_amount_for_order', 1, NOW()),
('migration_20210504_v3_4_2_sync_bestme_social', 1, NOW()),
('migration_20210517_add_column_order_id_on_campaign_voucher', 1, NOW()),
('migration_20210525_update_migrate_v3_4_2_add_new_table_order_channel', 1, NOW()),
('migration_20210616_v3_5_1_open_api', 1, NOW()),
('migration_20210812_create_keyword_sync_table', 1, NOW()),
('migration_20210817_add_column_source_sync', 1, NOW()),
('migration_20210914_add_email_subscribers', 1, NOW()),
('migration_20211012_add_new_column_to_table_product', 1, NOW());

-- --------end Add migrated versions for new shops -----------

-- -----------------------------------------------------------