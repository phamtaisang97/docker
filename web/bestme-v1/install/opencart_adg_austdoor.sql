-- -----------------------------------------------------------

--
-- Database: `opencart`
--

-- -----------------------------------------------------------

SET sql_mode = '';

--
-- Table structure for table `oc_address`
--

CREATE TABLE `oc_address` (
  `address_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(40) NOT NULL,
  `address` varchar(128) NOT NULL,
  `city` varchar(128) DEFAULT NULL,
  `district` varchar(128) DEFAULT NULL,
  `wards` varchar(128) DEFAULT NULL,
  `phone` varchar(32) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_api`
--

CREATE TABLE `oc_api` (
  `api_id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `key` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_api_ip`
--

CREATE TABLE `oc_api_ip` (
  `api_ip_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_api_session`
--

CREATE TABLE `oc_api_session` (
  `api_session_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute`
--

CREATE TABLE `oc_attribute` (
  `attribute_id` int(11) NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_description`
--

CREATE TABLE `oc_attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_group`
--

CREATE TABLE `oc_attribute_group` (
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_group_description`
--

CREATE TABLE `oc_attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_banner`
--

CREATE TABLE `oc_banner` (
  `banner_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_banner_image`
--

CREATE TABLE `oc_banner_image` (
  `banner_image_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_cart`
--

CREATE TABLE `oc_cart` (
  `cart_id` int(11) UNSIGNED NOT NULL,
  `api_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `option` text NOT NULL,
  `quantity` int(5) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category`
--

CREATE TABLE `oc_category` (
  `category_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_description`
--

CREATE TABLE `oc_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_filter`
--

CREATE TABLE `oc_category_filter` (
  `category_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_path`
--

CREATE TABLE `oc_category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_to_layout`
--

CREATE TABLE `oc_category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_to_store`
--

CREATE TABLE `oc_category_to_store` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_collection`
--

CREATE TABLE `oc_collection` (
  `collection_id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `sort_order` int(11) DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `product_type_sort` varchar(256) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_collection_description`
--

CREATE TABLE `oc_collection_description` (
  `collection_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_country`
--

CREATE TABLE `oc_country` (
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_country`
--

INSERT INTO `oc_country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1),
(2, 'Albania', 'AL', 'ALB', '', 0, 1),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1),
(5, 'Andorra', 'AD', 'AND', '', 0, 1),
(6, 'Angola', 'AO', 'AGO', '', 0, 1),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1),
(11, 'Armenia', 'AM', 'ARM', '', 0, 1),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1),
(13, 'Australia', 'AU', 'AUS', '', 0, 1),
(14, 'Austria', 'AT', 'AUT', '', 0, 1),
(15, 'Azerbaijan', 'AZ', 'AZE', '', 0, 1),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1),
(20, 'Belarus', 'BY', 'BLR', '', 0, 1),
(21, 'Belgium', 'BE', 'BEL', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', '', 0, 1),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1),
(38, 'Canada', 'CA', 'CAN', '', 0, 1),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1),
(42, 'Chad', 'TD', 'TCD', '', 0, 1),
(43, 'Chile', 'CL', 'CHL', '', 0, 1),
(44, 'China', 'CN', 'CHN', '', 0, 1),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1),
(47, 'Colombia', 'CO', 'COL', '', 0, 1),
(48, 'Comoros', 'KM', 'COM', '', 0, 1),
(49, 'Congo', 'CG', 'COG', '', 0, 1),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1),
(52, 'Cote D\'Ivoire', 'CI', 'CIV', '', 0, 1),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1),
(61, 'East Timor', 'TL', 'TLS', '', 0, 1),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1),
(67, 'Estonia', 'EE', 'EST', '', 0, 1),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1),
(72, 'Finland', 'FI', 'FIN', '', 0, 1),
(74, 'France, Metropolitan', 'FR', 'FRA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1),
(80, 'Georgia', 'GE', 'GEO', '', 0, 1),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1),
(84, 'Greece', 'GR', 'GRC', '', 0, 1),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1),
(88, 'Guam', 'GU', 'GUM', '', 0, 1),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1),
(91, 'Guinea-Bissau', 'GW', 'GNB', '', 0, 1),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1),
(95, 'Honduras', 'HN', 'HND', '', 0, 1),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1),
(99, 'India', 'IN', 'IND', '', 0, 1),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1),
(104, 'Israel', 'IL', 'ISR', '', 0, 1),
(105, 'Italy', 'IT', 'ITA', '', 0, 1),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1),
(107, 'Japan', 'JP', 'JPN', '', 0, 1),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', '', 0, 1),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1),
(113, 'South Korea', 'KR', 'KOR', '', 0, 1),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', '', 0, 1),
(116, 'Lao People\'s Democratic Republic', 'LA', 'LAO', '', 0, 1),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1),
(125, 'Macau', 'MO', 'MAC', '', 0, 1),
(126, 'FYROM', 'MK', 'MKD', '', 0, 1),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1),
(131, 'Mali', 'ML', 'MLI', '', 0, 1),
(132, 'Malta', 'MT', 'MLT', '', 0, 1),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1),
(140, 'Moldova, Republic of', 'MD', 'MDA', '', 0, 1),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1),
(155, 'Niger', 'NE', 'NER', '', 0, 1),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1),
(157, 'Niue', 'NU', 'NIU', '', 0, 1),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1),
(160, 'Norway', 'NO', 'NOR', '', 0, 1),
(161, 'Oman', 'OM', 'OMN', '', 0, 1),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1),
(163, 'Palau', 'PW', 'PLW', '', 0, 1),
(164, 'Panama', 'PA', 'PAN', '', 0, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1),
(167, 'Peru', 'PE', 'PER', '', 0, 1),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1),
(170, 'Poland', 'PL', 'POL', '', 0, 1),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1),
(174, 'Reunion', 'RE', 'REU', '', 0, 1),
(175, 'Romania', 'RO', 'ROM', '', 0, 1),
(176, 'Russian Federation', 'RU', 'RUS', '', 0, 1),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1),
(195, 'Spain', 'ES', 'ESP', '', 0, 1),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1),
(203, 'Sweden', 'SE', 'SWE', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1),
(207, 'Tajikistan', 'TJ', 'TJK', '', 0, 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1),
(209, 'Thailand', 'TH', 'THA', '', 0, 1),
(210, 'Togo', 'TG', 'TGO', '', 0, 1),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1),
(212, 'Tonga', 'TO', 'TON', '', 0, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1),
(216, 'Turkmenistan', 'TM', 'TKM', '', 0, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1),
(220, 'Ukraine', 'UA', 'UKR', '', 0, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1),
(226, 'Uzbekistan', 'UZ', 'UZB', '', 0, 1),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1),
(242, 'Montenegro', 'ME', 'MNE', '', 0, 1),
(243, 'Serbia', 'RS', 'SRB', '', 0, 1),
(244, 'Aaland Islands', 'AX', 'ALA', '', 0, 1),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '', 0, 1),
(246, 'Curacao', 'CW', 'CUW', '', 0, 1),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', '', 0, 1),
(248, 'South Sudan', 'SS', 'SSD', '', 0, 1),
(249, 'St. Barthelemy', 'BL', 'BLM', '', 0, 1),
(250, 'St. Martin (French part)', 'MF', 'MAF', '', 0, 1),
(251, 'Canary Islands', 'IC', 'ICA', '', 0, 1),
(252, 'Ascension Island (British)', 'AC', 'ASC', '', 0, 1),
(253, 'Kosovo, Republic of', 'XK', 'UNK', '', 0, 1),
(254, 'Isle of Man', 'IM', 'IMN', '', 0, 1),
(255, 'Tristan da Cunha', 'TA', 'SHN', '', 0, 1),
(256, 'Guernsey', 'GG', 'GGY', '', 0, 1),
(257, 'Jersey', 'JE', 'JEY', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon`
--

CREATE TABLE `oc_coupon` (
  `coupon_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(20) NOT NULL,
  `type` char(1) NOT NULL,
  `discount` decimal(15,4) NOT NULL,
  `logged` tinyint(1) NOT NULL,
  `shipping` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `uses_total` int(11) NOT NULL,
  `uses_customer` varchar(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_category`
--

CREATE TABLE `oc_coupon_category` (
  `coupon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_history`
--

CREATE TABLE `oc_coupon_history` (
  `coupon_history_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_product`
--

CREATE TABLE `oc_coupon_product` (
  `coupon_product_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_currency`
--

CREATE TABLE `oc_currency` (
  `currency_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` double(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_currency`
--

INSERT INTO `oc_currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(1, 'Pound Sterling', 'GBP', '£', '', '2', 29720.44000000, 1, '2019-01-09 16:40:33'),
(4, 'US Dollar', 'USD', '$', '', '2', 23245.00000000, 1, '2019-01-09 16:45:56'),
(3, 'Euro', 'EUR', '', '€', '2', 27137.74000000, 1, '2019-01-09 16:39:38'),
(2, 'Vietnam đồng', 'VND', '', 'đ', '0', 1.00000000, 1, '2019-05-16 08:11:36');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer`
--

CREATE TABLE `oc_customer` (
  `customer_id` int(11) NOT NULL,
  `customer_group_id` int(11) DEFAULT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) DEFAULT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) DEFAULT NULL,
  `password` varchar(40) NOT NULL,
  `password_temp` varchar(100) DEFAULT NULL,
  `salt` varchar(9) NOT NULL,
  `salt_temp` varchar(9) DEFAULT NULL,
  `cart` text,
  `wishlist` text,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `customer_source` varchar(128) DEFAULT NULL,
  `note` text,
  `custom_field` text,
  `ip` varchar(40) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `safe` tinyint(1) DEFAULT NULL,
  `token` text,
  `code` varchar(40) DEFAULT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_activity`
--

CREATE TABLE `oc_customer_activity` (
  `customer_activity_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_affiliate`
--

CREATE TABLE `oc_customer_affiliate` (
  `customer_id` int(11) NOT NULL,
  `company` varchar(40) NOT NULL,
  `website` varchar(255) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `commission` decimal(4,2) NOT NULL DEFAULT '0.00',
  `tax` varchar(64) NOT NULL,
  `payment` varchar(6) NOT NULL,
  `cheque` varchar(100) NOT NULL,
  `paypal` varchar(64) NOT NULL,
  `bank_name` varchar(64) NOT NULL,
  `bank_branch_number` varchar(64) NOT NULL,
  `bank_swift_code` varchar(64) NOT NULL,
  `bank_account_name` varchar(64) NOT NULL,
  `bank_account_number` varchar(64) NOT NULL,
  `custom_field` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_approval`
--

CREATE TABLE `oc_customer_approval` (
  `customer_approval_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `type` varchar(9) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_group`
--

CREATE TABLE `oc_customer_group` (
  `customer_group_id` int(11) NOT NULL,
  `approval` int(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_group_description`
--

CREATE TABLE `oc_customer_group_description` (
  `customer_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_history`
--

CREATE TABLE `oc_customer_history` (
  `customer_history_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_ip`
--

CREATE TABLE `oc_customer_ip` (
  `customer_ip_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_login`
--

CREATE TABLE `oc_customer_login` (
  `customer_login_id` int(11) NOT NULL,
  `email` varchar(96) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `total` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_online`
--

CREATE TABLE `oc_customer_online` (
  `ip` varchar(40) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `referer` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_reward`
--

CREATE TABLE `oc_customer_reward` (
  `customer_reward_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_search`
--

CREATE TABLE `oc_customer_search` (
  `customer_search_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category` tinyint(1) NOT NULL,
  `description` tinyint(1) NOT NULL,
  `products` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_transaction`
--

CREATE TABLE `oc_customer_transaction` (
  `customer_transaction_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_wishlist`
--

CREATE TABLE `oc_customer_wishlist` (
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field`
--

CREATE TABLE `oc_custom_field` (
  `custom_field_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `validation` varchar(255) NOT NULL,
  `location` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_customer_group`
--

CREATE TABLE `oc_custom_field_customer_group` (
  `custom_field_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_description`
--

CREATE TABLE `oc_custom_field_description` (
  `custom_field_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_value`
--

CREATE TABLE `oc_custom_field_value` (
  `custom_field_value_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_value_description`
--

CREATE TABLE `oc_custom_field_value_description` (
  `custom_field_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_download`
--

CREATE TABLE `oc_download` (
  `download_id` int(11) NOT NULL,
  `filename` varchar(160) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_download_description`
--

CREATE TABLE `oc_download_description` (
  `download_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_event`
--

CREATE TABLE `oc_event` (
  `event_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `trigger` text NOT NULL,
  `action` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_event`
--

INSERT INTO `oc_event` (`event_id`, `code`, `trigger`, `action`, `status`, `sort_order`) VALUES
(1, 'activity_customer_add', 'catalog/model/account/customer/addCustomer/after', 'event/activity/addCustomer', 1, 0),
(2, 'activity_customer_edit', 'catalog/model/account/customer/editCustomer/after', 'event/activity/editCustomer', 1, 0),
(3, 'activity_customer_password', 'catalog/model/account/customer/editPassword/after', 'event/activity/editPassword', 1, 0),
(4, 'activity_customer_forgotten', 'catalog/model/account/customer/editCode/after', 'event/activity/forgotten', 1, 0),
(5, 'activity_transaction', 'catalog/model/account/customer/addTransaction/after', 'event/activity/addTransaction', 1, 0),
(6, 'activity_customer_login', 'catalog/model/account/customer/deleteLoginAttempts/after', 'event/activity/login', 1, 0),
(7, 'activity_address_add', 'catalog/model/account/address/addAddress/after', 'event/activity/addAddress', 1, 0),
(8, 'activity_address_edit', 'catalog/model/account/address/editAddress/after', 'event/activity/editAddress', 1, 0),
(9, 'activity_address_delete', 'catalog/model/account/address/deleteAddress/after', 'event/activity/deleteAddress', 1, 0),
(10, 'activity_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'event/activity/addAffiliate', 1, 0),
(11, 'activity_affiliate_edit', 'catalog/model/account/customer/editAffiliate/after', 'event/activity/editAffiliate', 1, 0),
(12, 'activity_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'event/activity/addOrderHistory', 1, 0),
(13, 'activity_return_add', 'catalog/model/account/return/addReturn/after', 'event/activity/addReturn', 1, 0),
(14, 'mail_transaction', 'catalog/model/account/customer/addTransaction/after', 'mail/transaction', 1, 0),
(15, 'mail_forgotten', 'catalog/model/account/customer/editCode/after', 'mail/forgotten', 1, 0),
(16, 'mail_customer_add', 'catalog/model/account/customer/addCustomer/after', 'mail/register', 1, 0),
(17, 'mail_customer_alert', 'catalog/model/account/customer/addCustomer/after', 'mail/register/alert', 1, 0),
(18, 'mail_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate', 1, 0),
(19, 'mail_affiliate_alert', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate/alert', 1, 0),
(20, 'mail_voucher', 'catalog/model/checkout/order/addOrderHistory/after', 'extension/total/voucher/send', 1, 0),
(21, 'mail_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order', 1, 0),
(22, 'mail_order_alert', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order/alert', 1, 0),
(23, 'statistics_review_add', 'catalog/model/catalog/review/addReview/after', 'event/statistics/addReview', 1, 0),
(24, 'statistics_return_add', 'catalog/model/account/return/addReturn/after', 'event/statistics/addReturn', 1, 0),
(25, 'statistics_order_history', 'catalog/model/checkout/order/addOrderHistory/after', 'event/statistics/addOrderHistory', 1, 0),
(26, 'admin_mail_affiliate_approve', 'admin/model/customer/customer_approval/approveAffiliate/after', 'mail/affiliate/approve', 1, 0),
(27, 'admin_mail_affiliate_deny', 'admin/model/customer/customer_approval/denyAffiliate/after', 'mail/affiliate/deny', 1, 0),
(28, 'admin_mail_customer_approve', 'admin/model/customer/customer_approval/approveCustomer/after', 'mail/customer/approve', 1, 0),
(29, 'admin_mail_customer_deny', 'admin/model/customer/customer_approval/denyCustomer/after', 'mail/customer/deny', 1, 0),
(30, 'admin_mail_reward', 'admin/model/customer/customer/addReward/after', 'mail/reward', 1, 0),
(31, 'admin_mail_transaction', 'admin/model/customer/customer/addTransaction/after', 'mail/transaction', 1, 0),
(32, 'admin_mail_return', 'admin/model/sale/return/addReturn/after', 'mail/return', 1, 0),
(33, 'admin_mail_forgotten', 'admin/model/user/user/editCode/after', 'mail/forgotten', 1, 0),
(34, 'add order', 'admin/model/sale/order/addOrder/after', 'event/statistics/createOrder', 1, 0),
(35, 'edit order', 'admin/model/sale/order/updateProductOrder/after', 'event/statistics/editOrder', 1, 0),
(36, 'change status order', 'admin/model/sale/order/updateStatus/after', 'event/statistics/changeStatus', 1, 0),
(37, 'delete order', 'admin/model/sale/order/deleteOrderById/after', 'event/statistics/deleteOrder', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_extension`
--

CREATE TABLE `oc_extension` (
  `extension_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_extension`
--

INSERT INTO `oc_extension` (`extension_id`, `type`, `code`) VALUES
(1, 'payment', 'cod'),
(2, 'total', 'shipping'),
(3, 'total', 'sub_total'),
(4, 'total', 'tax'),
(5, 'total', 'total'),
(6, 'module', 'banner'),
(7, 'module', 'carousel'),
(8, 'total', 'credit'),
(9, 'shipping', 'flat'),
(10, 'total', 'handling'),
(11, 'total', 'low_order_fee'),
(12, 'total', 'coupon'),
(13, 'module', 'category'),
(14, 'module', 'account'),
(15, 'total', 'reward'),
(16, 'total', 'voucher'),
(17, 'payment', 'free_checkout'),
(18, 'module', 'featured'),
(19, 'module', 'slideshow'),
(21, 'dashboard', 'activity'),
(22, 'dashboard', 'sale'),
(23, 'dashboard', 'recent'),
(24, 'dashboard', 'order'),
(25, 'dashboard', 'online'),
(26, 'dashboard', 'map'),
(27, 'dashboard', 'customer'),
(28, 'dashboard', 'chart'),
(29, 'report', 'sale_coupon'),
(31, 'report', 'customer_search'),
(32, 'report', 'customer_transaction'),
(33, 'report', 'product_purchased'),
(34, 'report', 'product_viewed'),
(35, 'report', 'sale_return'),
(36, 'report', 'sale_order'),
(37, 'report', 'sale_shipping'),
(38, 'report', 'sale_tax'),
(39, 'report', 'customer_activity'),
(40, 'report', 'customer_order'),
(41, 'report', 'customer_reward'),
(42, 'module', 'theme_builder_config'),
(159, 'theme', 'adg_topal_55');

-- --------------------------------------------------------

--
-- Table structure for table `oc_extension_install`
--

CREATE TABLE `oc_extension_install` (
  `extension_install_id` int(11) NOT NULL,
  `extension_download_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_extension_path`
--

CREATE TABLE `oc_extension_path` (
  `extension_path_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter`
--

CREATE TABLE `oc_filter` (
  `filter_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter`
--

INSERT INTO `oc_filter` (`filter_id`, `filter_group_id`, `sort_order`) VALUES
(1, 1, 0),
(2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_description`
--

CREATE TABLE `oc_filter_description` (
  `filter_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter_description`
--

INSERT INTO `oc_filter_description` (`filter_id`, `language_id`, `filter_group_id`, `name`) VALUES
(1, 2, 1, 'Iphone'),
(1, 1, 1, 'Iphone'),
(2, 2, 1, 'Oppo'),
(2, 1, 1, 'Oppo');

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_group`
--

CREATE TABLE `oc_filter_group` (
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter_group`
--

INSERT INTO `oc_filter_group` (`filter_group_id`, `sort_order`) VALUES
(1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_group_description`
--

CREATE TABLE `oc_filter_group_description` (
  `filter_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter_group_description`
--

INSERT INTO `oc_filter_group_description` (`filter_group_id`, `language_id`, `name`) VALUES
(1, 1, 'filter group'),
(1, 2, 'Nhóm bộ lọc');

-- --------------------------------------------------------

--
-- Table structure for table `oc_geo_zone`
--

CREATE TABLE `oc_geo_zone` (
  `geo_zone_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_geo_zone`
--

INSERT INTO `oc_geo_zone` (`geo_zone_id`, `name`, `description`, `date_added`, `date_modified`) VALUES
(3, 'UK VAT Zone', 'UK VAT', '2009-01-06 23:26:25', '2010-02-26 22:33:24'),
(4, 'UK Shipping', 'UK Shipping Zones', '2009-06-23 01:14:53', '2010-12-15 15:18:13');

-- --------------------------------------------------------

--
-- Table structure for table `oc_information`
--

CREATE TABLE `oc_information` (
  `information_id` int(11) NOT NULL,
  `bottom` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_description`
--

CREATE TABLE `oc_information_description` (
  `information_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` mediumtext NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_to_layout`
--

CREATE TABLE `oc_information_to_layout` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_to_store`
--

CREATE TABLE `oc_information_to_store` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_language`
--

CREATE TABLE `oc_language` (
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_language`
--

INSERT INTO `oc_language` (`language_id`, `name`, `code`, `locale`, `image`, `directory`, `sort_order`, `status`) VALUES
(1, 'English', 'en-gb', 'en-US,en_US.UTF-8,en_US,en-gb,english', 'en-gb.png', 'english', 2, 0),
(2, 'Vietnamese', 'vi-vn', 'vi-VN,vi_VN.UTF-8,vi_VN,vi-vn,vietnamese', 'vi-vn.png', 'vietnamese', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout`
--

CREATE TABLE `oc_layout` (
  `layout_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_layout`
--

INSERT INTO `oc_layout` (`layout_id`, `name`) VALUES
(1, 'Home'),
(2, 'Product'),
(3, 'Category'),
(4, 'Default'),
(5, 'Manufacturer'),
(6, 'Account'),
(7, 'Checkout'),
(8, 'Contact'),
(9, 'Sitemap'),
(10, 'Affiliate'),
(11, 'Information'),
(12, 'Compare'),
(13, 'Search');

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout_module`
--

CREATE TABLE `oc_layout_module` (
  `layout_module_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `position` varchar(14) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_layout_module`
--

INSERT INTO `oc_layout_module` (`layout_module_id`, `layout_id`, `code`, `position`, `sort_order`) VALUES
(2, 4, '0', 'content_top', 0),
(3, 4, '0', 'content_top', 1),
(20, 5, '0', 'column_left', 2),
(69, 10, 'account', 'column_right', 1),
(68, 6, 'account', 'column_right', 1),
(67, 1, 'carousel.29', 'content_top', 3),
(66, 1, 'slideshow.27', 'content_top', 1),
(65, 1, 'featured.28', 'content_top', 2),
(72, 3, 'category', 'column_left', 1),
(73, 3, 'banner.30', 'column_left', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout_route`
--

CREATE TABLE `oc_layout_route` (
  `layout_route_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_layout_route`
--

INSERT INTO `oc_layout_route` (`layout_route_id`, `layout_id`, `store_id`, `route`) VALUES
(38, 6, 0, 'account/%'),
(17, 10, 0, 'affiliate/%'),
(44, 3, 0, 'product/category'),
(42, 1, 0, 'common/home'),
(20, 2, 0, 'product/product'),
(24, 11, 0, 'information/information'),
(23, 7, 0, 'checkout/%'),
(31, 8, 0, 'information/contact'),
(32, 9, 0, 'information/sitemap'),
(34, 4, 0, ''),
(45, 5, 0, 'product/manufacturer'),
(52, 12, 0, 'product/compare'),
(53, 13, 0, 'product/search');

-- --------------------------------------------------------

--
-- Table structure for table `oc_length_class`
--

CREATE TABLE `oc_length_class` (
  `length_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_length_class`
--

INSERT INTO `oc_length_class` (`length_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '10.00000000'),
(3, '0.39370000');

-- --------------------------------------------------------

--
-- Table structure for table `oc_length_class_description`
--

CREATE TABLE `oc_length_class_description` (
  `length_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_length_class_description`
--

INSERT INTO `oc_length_class_description` (`length_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Centimeter', 'cm'),
(2, 1, 'Millimeter', 'mm'),
(3, 1, 'Inch', 'in'),
(1, 2, 'Centimeter', 'cm'),
(2, 2, 'Millimeter', 'mm'),
(3, 2, 'Inch', 'in');

-- --------------------------------------------------------

--
-- Table structure for table `oc_location`
--

CREATE TABLE `oc_location` (
  `location_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `address` text NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `geocode` varchar(32) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `open` text NOT NULL,
  `comment` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_manufacturer`
--

CREATE TABLE `oc_manufacturer` (
  `manufacturer_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_manufacturer_to_store`
--

CREATE TABLE `oc_manufacturer_to_store` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_marketing`
--

CREATE TABLE `oc_marketing` (
  `marketing_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(64) NOT NULL,
  `clicks` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_modification`
--

CREATE TABLE `oc_modification` (
  `modification_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(64) NOT NULL,
  `author` varchar(64) NOT NULL,
  `version` varchar(32) NOT NULL,
  `link` varchar(255) NOT NULL,
  `xml` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_module`
--

CREATE TABLE `oc_module` (
  `module_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(32) NOT NULL,
  `setting` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_module`
--

INSERT INTO `oc_module` (`module_id`, `name`, `code`, `setting`) VALUES
(30, 'Category', 'banner', '{"name":"Category","banner_id":"6","width":"182","height":"182","status":"1"}'),
(29, 'Home Page', 'carousel', '{"name":"Home Page","banner_id":"8","width":"130","height":"100","status":"1"}'),
(28, 'Home Page', 'featured', '{"name":"Home Page","product":["43","40","42","30"],"limit":"4","width":"200","height":"200","status":"1"}'),
(27, 'Home Page', 'slideshow', '{"name":"Home Page","banner_id":"7","width":"1140","height":"380","status":"1"}'),
(31, 'Banner 1', 'banner', '{"name":"Banner 1","banner_id":"6","width":"182","height":"182","status":"1"}');

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_group_menu`
--

CREATE TABLE `oc_novaon_group_menu` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_novaon_group_menu`
--

INSERT INTO `oc_novaon_group_menu` (`id`, `parent_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 1, '2019-01-09 16:05:17', '2019-01-09 16:05:17'),
(2, 0, 1, '2019-01-09 16:06:51', '2019-01-09 16:06:51'),
(3, 0, 1, '2019-01-09 16:09:21', '2019-01-09 16:09:21'),
(4, 0, 1, '2019-01-09 16:11:17', '2019-01-09 16:11:17'),
(5, 0, 1, '2019-01-09 16:12:50', '2019-01-09 16:12:50');

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_group_menu_description`
--

CREATE TABLE `oc_novaon_group_menu_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `group_menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_novaon_group_menu_description`
--

INSERT INTO `oc_novaon_group_menu_description` (`id`, `language_id`, `name`, `group_menu_id`) VALUES
(1, 2, 'Menu chính', 1),
(2, 1, 'Menu chính', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_menu_item`
--

CREATE TABLE `oc_novaon_menu_item` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `group_menu_id` int(11) NOT NULL COMMENT 'id của bảng group_menu',
  `status` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_novaon_menu_item`
--

INSERT INTO `oc_novaon_menu_item` (`id`, `parent_id`, `group_menu_id`, `status`, `position`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 1, 0, '2019-01-09 16:05:17', '2019-01-09 16:05:17'),
(2, 0, 1, 1, 0, '2019-01-09 16:05:17', '2019-01-09 16:05:17'),
(3, 0, 1, 1, 0, '2019-01-09 16:05:18', '2019-01-09 16:05:18');

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_menu_item_description`
--

CREATE TABLE `oc_novaon_menu_item_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `menu_item_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_novaon_menu_item_description`
--

INSERT INTO `oc_novaon_menu_item_description` (`id`, `language_id`, `name`, `menu_item_id`) VALUES
(1, 2, 'Trang chủ', 1),
(2, 1, 'Trang chủ', 1),
(3, 2, 'Sản phẩm', 2),
(4, 1, 'Sản phẩm', 2),
(5, 2, 'Liên hệ', 3),
(6, 1, 'Liên hệ', 3);

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_relation_table`
--

CREATE TABLE `oc_novaon_relation_table` (
  `id` int(11) NOT NULL,
  `main_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên bảng chính(ví dụ group_menu)',
  `main_id` int(11) NOT NULL COMMENT 'id bảng chính, ví dụ group_menu_id =1)',
  `child_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên bảng con ví dụ menu_item',
  `child_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL COMMENT 'type_id =1 là sản phẩm type_id =2 là danh mục sản phẩm type_id =3 là bài viết type_id =4 là danh mục bài viết type_id =5 là url',
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `redirect` int(11) NOT NULL DEFAULT '0' COMMENT 'nếu = 1 là khi click vào sẽ redirect đến đâu, 0 là dùng dể hover vào show ra các tphan con',
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `weight_number` int(11) NOT NULL COMMENT 'trọng số hiển thị'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_novaon_relation_table`
--

INSERT INTO `oc_novaon_relation_table` (`id`, `main_name`, `main_id`, `child_name`, `child_id`, `type_id`, `url`, `redirect`, `title`, `weight_number`) VALUES
(1, 'menu_item', 1, '', 0, 5, '?route=common/home', 1, '', 0),
(2, 'menu_item', 2, '', 0, 5, '?route=common/shop', 1, '', 0),
(3, 'menu_item', 3, '', 0, 5, '?route=contact/contact', 1, '', 0),
(4, 'group_menu', 1, 'menu_item', 1, 5, '', 0, '', 0),
(5, 'group_menu', 1, 'menu_item', 2, 5, '', 0, '', 0),
(6, 'group_menu', 1, 'menu_item', 3, 5, '', 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_type`
--

CREATE TABLE `oc_novaon_type` (
  `id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `weight_number` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_novaon_type`
--

INSERT INTO `oc_novaon_type` (`id`, `status`, `weight_number`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `oc_novaon_type_description`
--

CREATE TABLE `oc_novaon_type_description` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_novaon_type_description`
--

INSERT INTO `oc_novaon_type_description` (`id`, `type_id`, `language_id`, `name`) VALUES
(1, 1, 1, 'Product'),
(2, 1, 2, 'Sản phẩm'),
(3, 2, 1, 'Category'),
(4, 2, 2, 'Danh mục sản phẩm'),
(5, 3, 1, 'Blog'),
(6, 3, 2, 'Bài viết'),
(7, 4, 1, 'Blog category'),
(8, 4, 2, 'Danh mục bài viết'),
(9, 5, 1, 'Url'),
(10, 5, 2, 'Url');

-- --------------------------------------------------------

--
-- Table structure for table `oc_option`
--

CREATE TABLE `oc_option` (
  `option_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option`
--

INSERT INTO `oc_option` (`option_id`, `type`, `sort_order`) VALUES
(1, 'radio', 1),
(2, 'checkbox', 2),
(4, 'text', 3),
(5, 'select', 4),
(6, 'textarea', 5),
(7, 'file', 6),
(8, 'date', 7),
(9, 'time', 8),
(10, 'datetime', 9),
(11, 'select', 10),
(12, 'date', 11);

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_description`
--

CREATE TABLE `oc_option_description` (
  `option_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option_description`
--

INSERT INTO `oc_option_description` (`option_id`, `language_id`, `name`) VALUES
(1, 1, 'Radio'),
(2, 1, 'Checkbox'),
(4, 1, 'Text'),
(6, 1, 'Textarea'),
(8, 1, 'Date'),
(7, 1, 'File'),
(5, 1, 'Select'),
(9, 1, 'Time'),
(10, 1, 'Date &amp; Time'),
(12, 1, 'Delivery Date'),
(11, 1, 'Size');

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_value`
--

CREATE TABLE `oc_option_value` (
  `option_value_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option_value`
--

INSERT INTO `oc_option_value` (`option_value_id`, `option_id`, `image`, `sort_order`) VALUES
(43, 1, '', 3),
(32, 1, '', 1),
(45, 2, '', 4),
(44, 2, '', 3),
(42, 5, '', 4),
(41, 5, '', 3),
(39, 5, '', 1),
(40, 5, '', 2),
(31, 1, '', 2),
(23, 2, '', 1),
(24, 2, '', 2),
(46, 11, '', 1),
(47, 11, '', 2),
(48, 11, '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_value_description`
--

CREATE TABLE `oc_option_value_description` (
  `option_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option_value_description`
--

INSERT INTO `oc_option_value_description` (`option_value_id`, `language_id`, `option_id`, `name`) VALUES
(43, 1, 1, 'Large'),
(32, 1, 1, 'Small'),
(45, 1, 2, 'Checkbox 4'),
(44, 1, 2, 'Checkbox 3'),
(31, 1, 1, 'Medium'),
(42, 1, 5, 'Yellow'),
(41, 1, 5, 'Green'),
(39, 1, 5, 'Red'),
(40, 1, 5, 'Blue'),
(23, 1, 2, 'Checkbox 1'),
(24, 1, 2, 'Checkbox 2'),
(48, 1, 11, 'Large'),
(47, 1, 11, 'Medium'),
(46, 1, 11, 'Small');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order`
--

CREATE TABLE `oc_order` (
  `order_id` int(11) NOT NULL,
  `order_code` varchar(50) DEFAULT NULL,
  `invoice_no` int(11) NOT NULL DEFAULT '0',
  `invoice_prefix` varchar(26) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `store_name` varchar(64) NOT NULL,
  `store_url` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `fullname` varchar(255) DEFAULT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `order_payment` int(11) DEFAULT NULL,
  `order_transfer` int(11) DEFAULT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `custom_field` text NOT NULL,
  `payment_firstname` varchar(32) NOT NULL,
  `payment_lastname` varchar(32) NOT NULL,
  `payment_company` varchar(60) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(10) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `payment_country_id` int(11) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_zone_id` int(11) NOT NULL,
  `payment_address_format` text NOT NULL,
  `payment_custom_field` text NOT NULL,
  `payment_method` varchar(128) NOT NULL,
  `payment_code` varchar(128) NOT NULL,
  `payment_status` tinyint(4) NOT NULL DEFAULT '0',
  `shipping_firstname` varchar(32) NOT NULL,
  `shipping_lastname` varchar(32) NOT NULL,
  `shipping_company` varchar(40) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(10) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `shipping_ward_code` varchar(50) DEFAULT NULL,
  `shipping_district_code` varchar(50) DEFAULT NULL,
  `shipping_province_code` varchar(50) DEFAULT NULL,
  `shipping_country_id` int(11) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_zone_id` int(11) NOT NULL,
  `shipping_address_format` text NOT NULL,
  `shipping_custom_field` text NOT NULL,
  `shipping_method` varchar(128) NOT NULL,
  `shipping_method_value` int(11) DEFAULT NULL,
  `shipping_fee` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `shipping_code` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `order_cancel_comment` varchar(500) DEFAULT NULL,
  `total` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `marketing_id` int(11) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL DEFAULT '1.00000000',
  `ip` varchar(40) NOT NULL,
  `forwarded_ip` varchar(40) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `accept_language` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_history`
--

CREATE TABLE `oc_order_history` (
  `order_history_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(256) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_option`
--

CREATE TABLE `oc_order_option` (
  `order_option_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_product`
--

CREATE TABLE `oc_order_product` (
  `order_product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_version_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `reward` int(8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_recurring`
--

CREATE TABLE `oc_order_recurring` (
  `order_recurring_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `recurring_name` varchar(255) NOT NULL,
  `recurring_description` varchar(255) NOT NULL,
  `recurring_frequency` varchar(25) NOT NULL,
  `recurring_cycle` smallint(6) NOT NULL,
  `recurring_duration` smallint(6) NOT NULL,
  `recurring_price` decimal(10,4) NOT NULL,
  `trial` tinyint(1) NOT NULL,
  `trial_frequency` varchar(25) NOT NULL,
  `trial_cycle` smallint(6) NOT NULL,
  `trial_duration` smallint(6) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_recurring_transaction`
--

CREATE TABLE `oc_order_recurring_transaction` (
  `order_recurring_transaction_id` int(11) NOT NULL,
  `order_recurring_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `amount` decimal(10,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_shipment`
--

CREATE TABLE `oc_order_shipment` (
  `order_shipment_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `shipping_courier_id` varchar(255) NOT NULL DEFAULT '',
  `tracking_number` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_status`
--

CREATE TABLE `oc_order_status` (
  `order_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_order_status`
--

INSERT INTO `oc_order_status` (`order_status_id`, `language_id`, `name`) VALUES
(1, 1, 'Draft'),
(2, 1, 'Processing'),
(3, 1, 'Delivering'),
(5, 1, 'Canceled'),
(4, 1, 'Complete'),
(6, 2, 'Nháp'),
(7, 2, 'Đang xử lý'),
(8, 2, 'Đang giao hàng '),
(9, 2, 'Hoàn thành'),
(10, 2, 'Đã hủy');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_tag`
--

CREATE TABLE `oc_order_tag` (
  `order_tag_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_total`
--

CREATE TABLE `oc_order_total` (
  `order_total_id` int(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_voucher`
--

CREATE TABLE `oc_order_voucher` (
  `order_voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_page_contents`
--

CREATE TABLE `oc_page_contents` (
  `page_id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `seo_title` varchar(256) DEFAULT NULL,
  `seo_description` text,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product`
--

CREATE TABLE `oc_product` (
  `product_id` int(11) NOT NULL,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `barcode` varchar(128) DEFAULT NULL,
  `upc` varchar(12) NOT NULL,
  `ean` varchar(14) NOT NULL,
  `jan` varchar(13) NOT NULL,
  `isbn` varchar(17) NOT NULL,
  `mpn` varchar(64) NOT NULL,
  `location` varchar(128) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `sale_on_out_of_stock` tinyint(4) NOT NULL DEFAULT '0',
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_alt` varchar(256) DEFAULT NULL,
  `multi_versions` tinyint(1) NOT NULL DEFAULT '0',
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `price_currency_id` int(11) NOT NULL,
  `compare_price` decimal(15,4) DEFAULT NULL,
  `c_price_currency_id` int(11) NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `tax_class_id` int(11) NOT NULL,
  `date_available` date NOT NULL DEFAULT '0000-00-00',
  `weight` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `weight_class_id` int(11) NOT NULL DEFAULT '0',
  `length` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `width` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `height` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `length_class_id` int(11) NOT NULL DEFAULT '0',
  `subtract` tinyint(1) NOT NULL DEFAULT '1',
  `minimum` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `demo` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_attribute`
--

CREATE TABLE `oc_product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_collection`
--

CREATE TABLE `oc_product_collection` (
  `product_collection_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `collection_id` int(11) NOT NULL,
  `sort_order` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_description`
--

CREATE TABLE `oc_product_description` (
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `sub_description` text,
  `seo_title` varchar(256) DEFAULT NULL,
  `seo_description` varchar(512) DEFAULT NULL,
  `tag` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_discount`
--

CREATE TABLE `oc_product_discount` (
  `product_discount_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_filter`
--

CREATE TABLE `oc_product_filter` (
  `product_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_filter`
--

INSERT INTO `oc_product_filter` (`product_id`, `filter_id`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_image`
--

CREATE TABLE `oc_product_image` (
  `product_image_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_alt` varchar(256) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_option`
--

CREATE TABLE `oc_product_option` (
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_option_value`
--

CREATE TABLE `oc_product_option_value` (
  `product_option_value_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtract` tinyint(1) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `price_prefix` varchar(1) NOT NULL,
  `points` int(8) NOT NULL,
  `points_prefix` varchar(1) NOT NULL,
  `weight` decimal(15,8) NOT NULL,
  `weight_prefix` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_recurring`
--

CREATE TABLE `oc_product_recurring` (
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_related`
--

CREATE TABLE `oc_product_related` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_reward`
--

CREATE TABLE `oc_product_reward` (
  `product_reward_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `points` int(8) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_special`
--

CREATE TABLE `oc_product_special` (
  `product_special_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_tag`
--

CREATE TABLE `oc_product_tag` (
  `product_tag_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_category`
--

CREATE TABLE `oc_product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_download`
--

CREATE TABLE `oc_product_to_download` (
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_layout`
--

CREATE TABLE `oc_product_to_layout` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_store`
--

CREATE TABLE `oc_product_to_store` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_version`
--

CREATE TABLE `oc_product_version` (
  `product_version_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `version` varchar(256) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `compare_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sku` varchar(64) NOT NULL,
  `barcode` varchar(124) NOT NULL,
  `quantity` int(7) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_recurring`
--

CREATE TABLE `oc_recurring` (
  `recurring_id` int(11) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `duration` int(10) UNSIGNED NOT NULL,
  `cycle` int(10) UNSIGNED NOT NULL,
  `trial_status` tinyint(4) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `trial_frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `trial_duration` int(10) UNSIGNED NOT NULL,
  `trial_cycle` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_recurring_description`
--

CREATE TABLE `oc_recurring_description` (
  `recurring_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_report`
--

CREATE TABLE `oc_report` (
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `report_date` date NOT NULL,
  `type` tinyint(4) NOT NULL,
  `type_description` varchar(100) NOT NULL,
  `d_field` varchar(500) NOT NULL,
  `m_value` varchar(500) NOT NULL,
  `last_updated` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return`
--

CREATE TABLE `oc_return` (
  `return_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `product` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `opened` tinyint(1) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `return_action_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `comment` text,
  `date_ordered` date NOT NULL DEFAULT '0000-00-00',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_action`
--

CREATE TABLE `oc_return_action` (
  `return_action_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_return_action`
--

INSERT INTO `oc_return_action` (`return_action_id`, `language_id`, `name`) VALUES
(1, 1, 'Refunded'),
(2, 1, 'Credit Issued'),
(3, 1, 'Replacement Sent'),
(4, 2, 'Refunded'),
(5, 2, 'Credit Issued'),
(6, 2, 'Replacement Sent');

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_history`
--

CREATE TABLE `oc_return_history` (
  `return_history_id` int(11) NOT NULL,
  `return_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_reason`
--

CREATE TABLE `oc_return_reason` (
  `return_reason_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_return_reason`
--

INSERT INTO `oc_return_reason` (`return_reason_id`, `language_id`, `name`) VALUES
(1, 1, 'Dead On Arrival'),
(2, 1, 'Received Wrong Item'),
(3, 1, 'Order Error'),
(4, 1, 'Faulty, please supply details'),
(5, 1, 'Other, please supply details'),
(6, 2, 'Dead On Arrival'),
(7, 2, 'Received Wrong Item'),
(8, 2, 'Order Error'),
(9, 2, 'Faulty, please supply details'),
(10, 2, 'Other, please supply details');

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_status`
--

CREATE TABLE `oc_return_status` (
  `return_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_return_status`
--

INSERT INTO `oc_return_status` (`return_status_id`, `language_id`, `name`) VALUES
(1, 1, 'Pending'),
(3, 1, 'Complete'),
(2, 1, 'Awaiting Products'),
(4, 2, 'Pending'),
(5, 2, 'Complete'),
(6, 2, 'Awaiting Products');

-- --------------------------------------------------------

--
-- Table structure for table `oc_review`
--

CREATE TABLE `oc_review` (
  `review_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_seo_url`
--

CREATE TABLE `oc_seo_url` (
  `seo_url_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `table_name` varchar(256) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_session`
--

CREATE TABLE `oc_session` (
  `session_id` varchar(32) NOT NULL,
  `data` text NOT NULL,
  `expire` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_setting`
--

CREATE TABLE `oc_setting` (
  `setting_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(128) NOT NULL,
  `key` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_setting`
--

INSERT INTO `oc_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(1, 0, 'config', 'config_robots', 'abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshaihulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg', 0),
(2, 0, 'config', 'config_shared', '0', 0),
(3, 0, 'config', 'config_secure', '0', 0),
(4, 0, 'voucher', 'total_voucher_sort_order', '8', 0),
(5, 0, 'voucher', 'total_voucher_status', '1', 0),
(6, 0, 'config', 'config_fraud_detection', '0', 0),
(7, 0, 'config', 'config_ftp_status', '0', 0),
(8, 0, 'config', 'config_ftp_root', '', 0),
(9, 0, 'config', 'config_ftp_password', '', 0),
(10, 0, 'config', 'config_ftp_username', '', 0),
(11, 0, 'config', 'config_ftp_port', '21', 0),
(12, 0, 'config', 'config_ftp_hostname', '', 0),
(13, 0, 'config', 'config_meta_title', 'Your Store', 0),
(14, 0, 'config', 'config_meta_description', 'My Store', 0),
(15, 0, 'config', 'config_meta_keyword', '', 0),
(5515, 0, 'config', 'config_theme', 'adg_topal_55', 0),
(17, 0, 'config', 'config_layout_id', '4', 0),
(18, 0, 'config', 'config_country_id', '222', 0),
(19, 0, 'config', 'config_zone_id', '3563', 0),
(20, 0, 'config', 'config_language', 'vi-vn', 0),
(21, 0, 'config', 'config_admin_language', 'vi-vn', 0),
(22, 0, 'config', 'config_currency', 'VND', 0),
(23, 0, 'config', 'config_currency_auto', '1', 0),
(24, 0, 'config', 'config_length_class_id', '1', 0),
(25, 0, 'config', 'config_weight_class_id', '1', 0),
(26, 0, 'config', 'config_product_count', '1', 0),
(27, 0, 'config', 'config_limit_admin', '25', 0),
(28, 0, 'config', 'config_review_status', '1', 0),
(29, 0, 'config', 'config_review_guest', '1', 0),
(30, 0, 'config', 'config_voucher_min', '1', 0),
(31, 0, 'config', 'config_voucher_max', '1000', 0),
(32, 0, 'config', 'config_tax', '1', 0),
(33, 0, 'config', 'config_tax_default', 'shipping', 0),
(34, 0, 'config', 'config_tax_customer', 'shipping', 0),
(35, 0, 'config', 'config_customer_online', '0', 0),
(36, 0, 'config', 'config_customer_activity', '0', 0),
(37, 0, 'config', 'config_customer_search', '0', 0),
(38, 0, 'config', 'config_customer_group_id', '1', 0),
(39, 0, 'config', 'config_customer_group_display', '["1"]', 1),
(40, 0, 'config', 'config_customer_price', '0', 0),
(41, 0, 'config', 'config_account_id', '3', 0),
(42, 0, 'config', 'config_invoice_prefix', 'INV-2013-00', 0),
(43, 0, 'config', 'config_api_id', '1', 0),
(44, 0, 'config', 'config_cart_weight', '1', 0),
(45, 0, 'config', 'config_checkout_guest', '1', 0),
(46, 0, 'config', 'config_checkout_id', '5', 0),
(47, 0, 'config', 'config_order_status_id', '1', 0),
(48, 0, 'config', 'config_processing_status', '["5","1","2","12","3"]', 1),
(49, 0, 'config', 'config_complete_status', '["5","3"]', 1),
(50, 0, 'config', 'config_stock_display', '0', 0),
(51, 0, 'config', 'config_stock_warning', '0', 0),
(52, 0, 'config', 'config_stock_checkout', '0', 0),
(53, 0, 'config', 'config_affiliate_approval', '0', 0),
(54, 0, 'config', 'config_affiliate_auto', '0', 0),
(55, 0, 'config', 'config_affiliate_commission', '5', 0),
(56, 0, 'config', 'config_affiliate_id', '4', 0),
(57, 0, 'config', 'config_return_id', '0', 0),
(58, 0, 'config', 'config_return_status_id', '2', 0),
(59, 0, 'config', 'config_logo', 'catalog/logo.png', 0),
(60, 0, 'config', 'config_icon', 'catalog/cart.png', 0),
(61, 0, 'config', 'config_comment', '', 0),
(62, 0, 'config', 'config_open', '', 0),
(63, 0, 'config', 'config_image', '', 0),
(64, 0, 'config', 'config_fax', '', 0),
(65, 0, 'config', 'config_telephone', '', 0),
(66, 0, 'config', 'config_email', 'demo@opencart.com', 0),
(67, 0, 'config', 'config_geocode', '', 0),
(68, 0, 'config', 'config_owner', 'Your Name', 0),
(69, 0, 'config', 'config_address', '', 0),
(70, 0, 'config', 'config_name', '', 0),
(71, 0, 'config', 'config_seo_url', '0', 0),
(72, 0, 'config', 'config_file_max_size', '300000', 0),
(73, 0, 'config', 'config_file_ext_allowed', 'zip\r\ntxt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\ndoc', 0),
(74, 0, 'config', 'config_file_mime_allowed', 'text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/gif\r\nimage/bmp\r\nimage/tiff\r\nimage/svg+xml\r\napplication/zip\r\n&quot;application/zip&quot;\r\napplication/x-zip\r\n&quot;application/x-zip&quot;\r\napplication/x-zip-compressed\r\n&quot;application/x-zip-compressed&quot;\r\napplication/rar\r\n&quot;application/rar&quot;\r\napplication/x-rar\r\n&quot;application/x-rar&quot;\r\napplication/x-rar-compressed\r\n&quot;application/x-rar-compressed&quot;\r\napplication/octet-stream\r\n&quot;application/octet-stream&quot;\r\naudio/mpeg\r\nvideo/quicktime\r\napplication/pdf', 0),
(75, 0, 'config', 'config_maintenance', '0', 0),
(76, 0, 'config', 'config_password', '1', 0),
(77, 0, 'config', 'config_encryption', '', 0),
(78, 0, 'config', 'config_compression', '0', 0),
(79, 0, 'config', 'config_error_display', '1', 0),
(80, 0, 'config', 'config_error_log', '1', 0),
(81, 0, 'config', 'config_error_filename', 'error.log', 0),
(82, 0, 'config', 'config_google_analytics', '', 0),
(83, 0, 'config', 'config_mail_engine', 'smtp', 0),
(84, 0, 'config', 'config_mail_parameter', '', 0),
(85, 0, 'config', 'config_mail_smtp_hostname', 'tls://smtp.gmail.com', 0),
(86, 0, 'config', 'config_mail_smtp_username', 'hotro@bestme.asia', 0),
(87, 0, 'config', 'config_mail_smtp_password', 'NOVAONshop@1234', 0),
(88, 0, 'config', 'config_mail_smtp_port', '587', 0),
(89, 0, 'config', 'config_mail_smtp_timeout', '15', 0),
(90, 0, 'config', 'config_mail_alert_email', '', 0),
(91, 0, 'config', 'config_mail_alert', '["order"]', 1),
(92, 0, 'config', 'config_captcha', 'basic', 0),
(93, 0, 'config', 'config_captcha_page', '["review","return","contact"]', 1),
(94, 0, 'config', 'config_login_attempts', '5', 0),
(95, 0, 'payment_free_checkout', 'payment_free_checkout_status', '1', 0),
(96, 0, 'payment_free_checkout', 'free_checkout_order_status_id', '1', 0),
(97, 0, 'payment_free_checkout', 'payment_free_checkout_sort_order', '1', 0),
(98, 0, 'payment_cod', 'payment_cod_sort_order', '5', 0),
(99, 0, 'payment_cod', 'payment_cod_total', '0.01', 0),
(100, 0, 'payment_cod', 'payment_cod_order_status_id', '1', 0),
(101, 0, 'payment_cod', 'payment_cod_geo_zone_id', '0', 0),
(102, 0, 'payment_cod', 'payment_cod_status', '1', 0),
(103, 0, 'shipping_flat', 'shipping_flat_sort_order', '1', 0),
(104, 0, 'shipping_flat', 'shipping_flat_status', '1', 0),
(105, 0, 'shipping_flat', 'shipping_flat_geo_zone_id', '0', 0),
(106, 0, 'shipping_flat', 'shipping_flat_tax_class_id', '9', 0),
(107, 0, 'shipping_flat', 'shipping_flat_cost', '5.00', 0),
(108, 0, 'total_shipping', 'total_shipping_sort_order', '3', 0),
(109, 0, 'total_sub_total', 'sub_total_sort_order', '1', 0),
(110, 0, 'total_sub_total', 'total_sub_total_status', '1', 0),
(111, 0, 'total_tax', 'total_tax_status', '1', 0),
(112, 0, 'total_total', 'total_total_sort_order', '9', 0),
(113, 0, 'total_total', 'total_total_status', '1', 0),
(114, 0, 'total_tax', 'total_tax_sort_order', '5', 0),
(115, 0, 'total_credit', 'total_credit_sort_order', '7', 0),
(116, 0, 'total_credit', 'total_credit_status', '1', 0),
(117, 0, 'total_reward', 'total_reward_sort_order', '2', 0),
(118, 0, 'total_reward', 'total_reward_status', '1', 0),
(119, 0, 'total_shipping', 'total_shipping_status', '1', 0),
(120, 0, 'total_shipping', 'total_shipping_estimator', '1', 0),
(121, 0, 'total_coupon', 'total_coupon_sort_order', '4', 0),
(122, 0, 'total_coupon', 'total_coupon_status', '1', 0),
(123, 0, 'module_category', 'module_category_status', '1', 0),
(124, 0, 'module_account', 'module_account_status', '1', 0),
(125, 0, 'theme_default', 'theme_default_product_limit', '15', 0),
(126, 0, 'theme_default', 'theme_default_product_description_length', '100', 0),
(127, 0, 'theme_default', 'theme_default_image_thumb_width', '228', 0),
(128, 0, 'theme_default', 'theme_default_image_thumb_height', '228', 0),
(129, 0, 'theme_default', 'theme_default_image_popup_width', '500', 0),
(130, 0, 'theme_default', 'theme_default_image_popup_height', '500', 0),
(131, 0, 'theme_default', 'theme_default_image_category_width', '80', 0),
(132, 0, 'theme_default', 'theme_default_image_category_height', '80', 0),
(133, 0, 'theme_default', 'theme_default_image_product_width', '228', 0),
(134, 0, 'theme_default', 'theme_default_image_product_height', '228', 0),
(135, 0, 'theme_default', 'theme_default_image_additional_width', '74', 0),
(136, 0, 'theme_default', 'theme_default_image_additional_height', '74', 0),
(137, 0, 'theme_default', 'theme_default_image_related_width', '200', 0),
(138, 0, 'theme_default', 'theme_default_image_related_height', '200', 0),
(139, 0, 'theme_default', 'theme_default_image_compare_width', '90', 0),
(140, 0, 'theme_default', 'theme_default_image_compare_height', '90', 0),
(141, 0, 'theme_default', 'theme_default_image_wishlist_width', '47', 0),
(142, 0, 'theme_default', 'theme_default_image_wishlist_height', '47', 0),
(143, 0, 'theme_default', 'theme_default_image_cart_height', '47', 0),
(144, 0, 'theme_default', 'theme_default_image_cart_width', '47', 0),
(145, 0, 'theme_default', 'theme_default_image_location_height', '50', 0),
(146, 0, 'theme_default', 'theme_default_image_location_width', '268', 0),
(147, 0, 'theme_default', 'theme_default_directory', 'default', 0),
(148, 0, 'theme_default', 'theme_default_status', '1', 0),
(149, 0, 'dashboard_activity', 'dashboard_activity_status', '1', 0),
(150, 0, 'dashboard_activity', 'dashboard_activity_sort_order', '7', 0),
(151, 0, 'dashboard_sale', 'dashboard_sale_status', '1', 0),
(152, 0, 'dashboard_sale', 'dashboard_sale_width', '3', 0),
(153, 0, 'dashboard_chart', 'dashboard_chart_status', '1', 0),
(154, 0, 'dashboard_chart', 'dashboard_chart_width', '6', 0),
(155, 0, 'dashboard_customer', 'dashboard_customer_status', '1', 0),
(156, 0, 'dashboard_customer', 'dashboard_customer_width', '3', 0),
(157, 0, 'dashboard_map', 'dashboard_map_status', '1', 0),
(158, 0, 'dashboard_map', 'dashboard_map_width', '6', 0),
(159, 0, 'dashboard_online', 'dashboard_online_status', '1', 0),
(160, 0, 'dashboard_online', 'dashboard_online_width', '3', 0),
(161, 0, 'dashboard_order', 'dashboard_order_sort_order', '1', 0),
(162, 0, 'dashboard_order', 'dashboard_order_status', '1', 0),
(163, 0, 'dashboard_order', 'dashboard_order_width', '3', 0),
(164, 0, 'dashboard_sale', 'dashboard_sale_sort_order', '2', 0),
(165, 0, 'dashboard_customer', 'dashboard_customer_sort_order', '3', 0),
(166, 0, 'dashboard_online', 'dashboard_online_sort_order', '4', 0),
(167, 0, 'dashboard_map', 'dashboard_map_sort_order', '5', 0),
(168, 0, 'dashboard_chart', 'dashboard_chart_sort_order', '6', 0),
(169, 0, 'dashboard_recent', 'dashboard_recent_status', '1', 0),
(170, 0, 'dashboard_recent', 'dashboard_recent_sort_order', '8', 0),
(171, 0, 'dashboard_activity', 'dashboard_activity_width', '4', 0),
(172, 0, 'dashboard_recent', 'dashboard_recent_width', '8', 0),
(173, 0, 'report_customer_activity', 'report_customer_activity_status', '1', 0),
(174, 0, 'report_customer_activity', 'report_customer_activity_sort_order', '1', 0),
(175, 0, 'report_customer_order', 'report_customer_order_status', '1', 0),
(176, 0, 'report_customer_order', 'report_customer_order_sort_order', '2', 0),
(177, 0, 'report_customer_reward', 'report_customer_reward_status', '1', 0),
(178, 0, 'report_customer_reward', 'report_customer_reward_sort_order', '3', 0),
(179, 0, 'report_customer_search', 'report_customer_search_sort_order', '3', 0),
(180, 0, 'report_customer_search', 'report_customer_search_status', '1', 0),
(181, 0, 'report_customer_transaction', 'report_customer_transaction_status', '1', 0),
(182, 0, 'report_customer_transaction', 'report_customer_transaction_status_sort_order', '4', 0),
(183, 0, 'report_sale_tax', 'report_sale_tax_status', '1', 0),
(184, 0, 'report_sale_tax', 'report_sale_tax_sort_order', '5', 0),
(185, 0, 'report_sale_shipping', 'report_sale_shipping_status', '1', 0),
(186, 0, 'report_sale_shipping', 'report_sale_shipping_sort_order', '6', 0),
(187, 0, 'report_sale_return', 'report_sale_return_status', '1', 0),
(188, 0, 'report_sale_return', 'report_sale_return_sort_order', '7', 0),
(189, 0, 'report_sale_order', 'report_sale_order_status', '1', 0),
(190, 0, 'report_sale_order', 'report_sale_order_sort_order', '8', 0),
(191, 0, 'report_sale_coupon', 'report_sale_coupon_status', '1', 0),
(192, 0, 'report_sale_coupon', 'report_sale_coupon_sort_order', '9', 0),
(193, 0, 'report_product_viewed', 'report_product_viewed_status', '1', 0),
(194, 0, 'report_product_viewed', 'report_product_viewed_sort_order', '10', 0),
(195, 0, 'report_product_purchased', 'report_product_purchased_status', '1', 0),
(196, 0, 'report_product_purchased', 'report_product_purchased_sort_order', '11', 0),
(197, 0, 'report_marketing', 'report_marketing_status', '1', 0),
(198, 0, 'report_marketing', 'report_marketing_sort_order', '12', 0),
(4968, 0, 'developer', 'developer_sass', '0', 0),
(201, 0, 'module_theme_builder_config', 'module_theme_builder_config_status', '1', 0),
(4700, 0, 'theme_novaon', 'theme_novaon_image_wishlist_height', '47', 0),
(4701, 0, 'theme_novaon', 'theme_novaon_image_wishlist_width', '47', 0),
(4702, 0, 'theme_novaon', 'theme_novaon_image_compare_height', '90', 0),
(4703, 0, 'theme_novaon', 'theme_novaon_image_compare_width', '90', 0),
(4704, 0, 'theme_novaon', 'theme_novaon_image_related_height', '80', 0),
(4705, 0, 'theme_novaon', 'theme_novaon_image_related_width', '80', 0),
(4706, 0, 'theme_novaon', 'theme_novaon_image_additional_height', '74', 0),
(4707, 0, 'theme_novaon', 'theme_novaon_image_additional_width', '74', 0),
(4708, 0, 'theme_novaon', 'theme_novaon_image_product_height', '228', 0),
(4709, 0, 'theme_novaon', 'theme_novaon_image_product_width', '228', 0),
(4710, 0, 'theme_novaon', 'theme_novaon_image_popup_height', '500', 0),
(4711, 0, 'theme_novaon', 'theme_novaon_image_popup_width', '500', 0),
(4712, 0, 'theme_novaon', 'theme_novaon_image_thumb_height', '228', 0),
(4713, 0, 'theme_novaon', 'theme_novaon_image_thumb_width', '228', 0),
(4714, 0, 'theme_novaon', 'theme_novaon_image_category_height', '80', 0),
(4715, 0, 'theme_novaon', 'theme_novaon_image_category_width', '80', 0),
(4716, 0, 'theme_novaon', 'theme_novaon_product_description_length', '100', 0),
(4717, 0, 'theme_novaon', 'theme_novaon_product_limit', '15', 0),
(4718, 0, 'theme_novaon', 'theme_novaon_status', '1', 0),
(4719, 0, 'theme_novaon', 'theme_novaon_directory', 'novaon', 0),
(4720, 0, 'theme_novaon', 'theme_novaon_image_cart_width', '47', 0),
(4721, 0, 'theme_novaon', 'theme_novaon_image_cart_height', '47', 0),
(4722, 0, 'theme_novaon', 'theme_novaon_image_location_width', '268', 0),
(4723, 0, 'theme_novaon', 'theme_novaon_image_location_height', '50', 0),
(4892, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_image_wishlist_height', '47', 0),
(4893, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_image_wishlist_width', '47', 0),
(4894, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_image_compare_height', '90', 0),
(4895, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_image_compare_width', '90', 0),
(4896, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_image_related_height', '80', 0),
(4897, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_image_related_width', '80', 0),
(4898, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_image_additional_height', '74', 0),
(4899, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_image_additional_width', '74', 0),
(4900, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_image_product_height', '228', 0),
(4901, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_image_product_width', '228', 0),
(4902, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_image_popup_height', '500', 0),
(4903, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_image_popup_width', '500', 0),
(4904, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_image_thumb_height', '228', 0),
(4905, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_image_thumb_width', '228', 0),
(4906, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_image_category_height', '80', 0),
(4907, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_image_category_width', '80', 0),
(4908, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_product_description_length', '100', 0),
(4909, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_product_limit', '15', 0),
(4910, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_status', '1', 0),
(4911, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_directory', 'adg_topal_55', 0),
(4912, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_image_cart_width', '47', 0),
(4913, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_image_cart_height', '47', 0),
(4914, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_image_location_width', '268', 0),
(4915, 0, 'theme_adg_topal_55', 'theme_adg_topal_55_image_location_height', '50', 0),
(4967, 0, 'developer', 'developer_theme', '0', 0);
-- --------------------------------------------------------

--
-- Table structure for table `oc_shipping_courier`
--

CREATE TABLE `oc_shipping_courier` (
  `shipping_courier_id` int(11) NOT NULL,
  `shipping_courier_code` varchar(255) NOT NULL DEFAULT '',
  `shipping_courier_name` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_statistics`
--

CREATE TABLE `oc_statistics` (
  `statistics_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `value` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_statistics`
--

INSERT INTO `oc_statistics` (`statistics_id`, `code`, `value`) VALUES
(1, 'order_sale', '0.0000'),
(2, 'order_processing', '0.0000'),
(3, 'order_complete', '0.0000'),
(4, 'order_other', '0.0000'),
(5, 'returns', '0.0000'),
(6, 'product', '0.0000'),
(7, 'review', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `oc_stock_status`
--

CREATE TABLE `oc_stock_status` (
  `stock_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_stock_status`
--

INSERT INTO `oc_stock_status` (`stock_status_id`, `language_id`, `name`) VALUES
(7, 1, 'In Stock'),
(8, 1, 'Pre-Order'),
(5, 1, 'Out Of Stock'),
(6, 1, '2-3 Days'),
(7, 2, 'In Stock'),
(8, 2, 'Pre-Order'),
(5, 2, 'Out Of Stock'),
(6, 2, '2-3 Days');

-- --------------------------------------------------------

--
-- Table structure for table `oc_store`
--

CREATE TABLE `oc_store` (
  `store_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  `ssl` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_tag`
--

CREATE TABLE `oc_tag` (
  `tag_id` int(11) NOT NULL,
  `value` varchar(500) NOT NULL,
  `sort_order` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_class`
--

CREATE TABLE `oc_tax_class` (
  `tax_class_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_class`
--

INSERT INTO `oc_tax_class` (`tax_class_id`, `title`, `description`, `date_added`, `date_modified`) VALUES
(9, 'Taxable Goods', 'Taxed goods', '2009-01-06 23:21:53', '2011-09-23 14:07:50'),
(10, 'Downloadable Products', 'Downloadable', '2011-09-21 22:19:39', '2011-09-22 10:27:36');

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rate`
--

CREATE TABLE `oc_tax_rate` (
  `tax_rate_id` int(11) NOT NULL,
  `geo_zone_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  `rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `type` char(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rate`
--

INSERT INTO `oc_tax_rate` (`tax_rate_id`, `geo_zone_id`, `name`, `rate`, `type`, `date_added`, `date_modified`) VALUES
(86, 3, 'VAT (20%)', '20.0000', 'P', '2011-03-09 21:17:10', '2011-09-22 22:24:29'),
(87, 3, 'Eco Tax (-2.00)', '2.0000', 'F', '2011-09-21 21:49:23', '2011-09-23 00:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rate_to_customer_group`
--

CREATE TABLE `oc_tax_rate_to_customer_group` (
  `tax_rate_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rate_to_customer_group`
--

INSERT INTO `oc_tax_rate_to_customer_group` (`tax_rate_id`, `customer_group_id`) VALUES
(86, 1),
(87, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rule`
--

CREATE TABLE `oc_tax_rule` (
  `tax_rule_id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `based` varchar(10) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rule`
--

INSERT INTO `oc_tax_rule` (`tax_rule_id`, `tax_class_id`, `tax_rate_id`, `based`, `priority`) VALUES
(121, 10, 86, 'payment', 1),
(120, 10, 87, 'store', 0),
(128, 9, 86, 'shipping', 1),
(127, 9, 87, 'shipping', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_theme`
--

CREATE TABLE `oc_theme` (
  `theme_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `theme` varchar(64) NOT NULL,
  `route` varchar(64) NOT NULL,
  `code` mediumtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_theme_builder_config`
--

CREATE TABLE `oc_theme_builder_config` (
  `id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `config_theme` varchar(128) NOT NULL,
  `code` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `config` mediumtext NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_theme_builder_config`
--

INSERT INTO `oc_theme_builder_config` (`id`, `store_id`, `config_theme`, `code`, `key`, `config`, `date_added`, `date_modified`) VALUES
(414, 0, 'adg_topal_55', 'config', 'config_theme_color', '{\n    "main": {\n        "main": {\n            "hex": "004EA4",\n            "visible": "1"\n        },\n        "button": {\n            "hex": "F58220",\n            "visible": "1"\n        }\n    }\n}', '2021-11-08 15:21:28', '2022-02-08 10:40:16'),
(415, 0, 'adg_topal_55', 'config', 'config_theme_text', '{\n    "all": {\n        "title": "Font Roboto",\n        "font": "Roboto",\n        "supported-fonts": [\n            "Open Sans",\n            "IBM Plex Sans",\n            "Courier New",\n            "Roboto",\n            "Nunito",\n            "Arial",\n            "DejaVu Sans",\n            "Tahoma",\n            "Time News Roman",\n            "Mulish"\n        ]\n    },\n    "heading": {\n        "font": "Tahoma",\n        "supported-fonts": [\n            "Open Sans",\n            "IBM Plex Sans",\n            "Courier New",\n            "Roboto",\n            "Nunito",\n            "Arial",\n            "DejaVu Sans",\n            "Tahoma",\n            "Time News Roman",\n            "Quicksand",\n            "Montserrat",\n            "KoHo",\n            "Newsreader",\n            "Oswald",\n            "Playfair Display",\n            "Lora",\n            "Inter"\n        ],\n        "font-type": "regular",\n        "supported-font-types": [\n            "regular",\n            "bold",\n            "italic"\n        ],\n        "font-size": 32\n    },\n    "body": {\n        "font": "Aria",\n        "supported-fonts": [\n            "Open Sans",\n            "IBM Plex Sans",\n            "Courier New",\n            "Roboto",\n            "Nunito",\n            "Arial",\n            "DejaVu Sans",\n            "Tahoma",\n            "Time News Roman",\n            "Quicksand",\n            "Montserrat",\n            "KoHo",\n            "Newsreader",\n            "Oswald",\n            "Playfair Display",\n            "Lora",\n            "Inter"\n        ],\n        "font-type": "regular",\n        "supported-font-types": [\n            "regular",\n            "bold",\n            "italic"\n        ],\n        "font-size": 14\n    }\n}', '2021-11-08 15:21:28', '2022-02-08 10:40:16'),
(416, 0, 'adg_topal_55', 'config', 'config_theme_favicon', '{\n    "image": {\n        "url": "\\/catalog\\/view\\/theme\\/default\\/image\\/favicon.ico"\n    }\n}', '2021-11-08 15:21:28', '2022-02-08 10:40:16'),
(417, 0, 'adg_topal_55', 'config', 'config_theme_social', '[\n    {\n        "name": "facebook",\n        "text": "Facebook",\n        "url": "https:\\/\\/www.facebook.com"\n    },\n    {\n        "name": "twitter",\n        "text": "Twitter",\n        "url": "https:\\/\\/www.twitter.com"\n    },\n    {\n        "name": "instagram",\n        "text": "Instagram",\n        "url": "https:\\/\\/www.instagram.com"\n    },\n    {\n        "name": "tumblr",\n        "text": "Tumblr",\n        "url": "https:\\/\\/www.tumblr.com"\n    },\n    {\n        "name": "youtube",\n        "text": "Youtube",\n        "url": "https:\\/\\/www.youtube.com"\n    },\n    {\n        "name": "googleplus",\n        "text": "Google Plus",\n        "url": "https:\\/\\/www.plus.google.com"\n    }\n]', '2021-11-08 15:21:28', '2022-02-08 10:40:16'),
(418, 0, 'adg_topal_55', 'config', 'config_theme_ecommerce', '[\n    {\n        "name": "shopee",\n        "text": "Shopee",\n        "url": "https:\\/\\/shopee.vn"\n    },\n    {\n        "name": "lazada",\n        "text": "Lazada",\n        "url": "https:\\/\\/www.lazada.vn"\n    },\n    {\n        "name": "tiki",\n        "text": "Tiki",\n        "url": "https:\\/\\/tiki.vn"\n    },\n    {\n        "name": "sendo",\n        "text": "Sendo",\n        "url": "https:\\/\\/www.sendo.vn"\n    }\n]', '2021-11-08 15:21:28', '2022-02-08 10:40:16'),
(419, 0, 'adg_topal_55', 'config', 'config_theme_override_css', '{\n    "css": "\\n   \\/* override css *\\/\\n    p.title-service {\\n        text-transform: uppercase;\\n    }\\n\\n    #header .header-search input.form-control {\\n        border-left: none;\\n    }\\n    \\n    #header .header-search .collapse-search a .icon {\\n        transform: translateY(4px);\\n    }\\n    \\n    i.icon.icon-clear {\\n        display: block;\\n    }\\n    \\n    .main-header ul.nav \\u003E li \\u003E a\\u003E.toggle {\\n        background: gray;\\n        transform: rotate(\\n    0deg);\\n        transform: translateX(3px);\\n        right: 0px !important;\\n    }\\n    \\n    .main-header ul.nav.child-nav {\\n        padding: 15px;\\n    }\\n    \\n    .main-header ul.nav.child-nav li{\\n        padding-bottom: 5px;\\n    }\\n    \\n    .main-header ul.nav.child-nav li{\\n        padding: 7px 2px;\\n        border-bottom: 1px solid #0000001c;\\n        font-size: 14px;\\n    }\\n    \\n    .main-header ul.nav.child-nav li:last-child {\\n        border: none;\\n        padding-bottom: 0px;\\n    }\\n    \\n    .main-header ul.nav.child-nav a{\\n        color: gray;\\n    }\\n    \\n    .main-header ul.nav.child-nav a:hover{\\n        color: #13203D;\\n    }\\n    .block-header #navbarMainMenu ul.nav ul.nav {\\n        border: none;\\n        transform: translateX(-19px);\\n        padding-left: 19px;\\n        box-shadow: 5px 7px 8px 1px #41464b57;\\n    }\\n    .product-item img {\\n        height: 321px !important;\\n        object-fit: cover;\\n    }\\n    .block-header li.nav-item.has-child {\\n        margin-right: 15px;\\n    }\\n    .news-box .news-item img {\\n        height: 209px !important;\\n        object-fit: cover;\\n    }\\n    .blog-category {\\n        padding-left: 0 !important;\\n    }\\n    ul#productDetailTab {\\n        margin-bottom: 0 !important;\\n    }\\n    div#productDetailTabContent {\\n        margin-top: 0 !important;\\n        padding-top: 0 !important;\\n    }\\n    a.btn.btn-primary.mt-3.contact-now-btn {\\n        margin-top: 0 !important;\\n    }\\n    .product-item .prod-title {\\n        margin-bottom: 0;\\n    }\\n    .breadcrumb-item+.breadcrumb-item {\\n        font-size: 14px;\\n    }\\n    ol.breadcrumb a {\\n        font-size: 14px;\\n    }\\n    .nav-tabs .nav-link::first-letter {\\n        text-transform: uppercase;\\n    }\\n    .project-related .project-img img {\\n        height: 238px;\\n    }\\n    .project-item img {\\n        height: 238px!important;\\n    }\\n    .about-us .col-lg-9.mb-lg-30.mb-20.text-secondary {\\n        margin-bottom: 0 !important;\\n    }\\n    footer#footer h4 {\\n        font-size: 16px !important;\\n    }\\n    .blog-category.nav-tabs .nav-link {\\n        padding: 7px;\\n    }\\n    .news-detail.news-item {\\n        padding: 0;\\n        margin: 0 !important;\\n        width: 100%;\\n    }\\n    p.title-service {\\n        font-family: Roboto;\\n        font-style: normal;\\n        font-weight: 500;\\n        font-size: 24px;\\n        line-height: 28px;\\n        color: #FFFFFF;\\n        display: block;\\n        padding-left: 43px;\\n        margin-bottom: 20px;\\n    }\\n    .collapse-search span.ms-2.d-lg-block.d-none {\\n        font-size: 14px !important;\\n    }\\n\\n.created-date {\\nfont-size: 14px;\\n}\\n\\n@media(max-width: 768px){\\n  section.mb-5.py-5.bg-sixth.full-width.bestme-block-product-related-product {\\n    margin-bottom: 0 !important;\\n  }\\n  #header .header-search.show form.search-form {\\n    visibility: visible;\\n    width: 100% !important;\\n  }\\n  .intro-award {\\n    margin-top: 35px;\\n   }\\n  .about-us .col-lg-12.col-6.mb-lg-30.mb-20 img {\\n    display: block;\\n  }\\n  img.bestme-block-banner-2 {\\n    display: none;\\n  }\\n  .why-choose {\\n    border-top: 1px solid #8080803d;\\n    padding-top: 25px;\\n    margin-top: 25px;\\n  }\\n  #header .navbar {\\n    max-width: 60%;\\n    padding-left: 0 !important;\\n  }\\n  body, #header .navbar a.nav-link:hover, #header .navbar a.nav-link.active, a, .product-item .prod-title:hover, .box-product-filter .nav a.nav-link:hover, .box-product-filter .nav a.nav-link.active, .box-product-filter .nav a.nav-link:focus, .nav-tabs .nav-link:hover, .nav-tabs .nav-link.active, .breadcrumb-item.active, .news-item .news-title {\\n    color: #27282B;\\n  }\\n  #header .navbar a.nav-link:hover, #header .navbar a.nav-link.active, .nav-tabs .nav-link:hover, .nav-tabs .nav-link.active {\\n    border: none;\\n  }\\n  .product-item img {\\n    height: auto !important;\\n    object-fit: cover;\\n  }\\n  .block-header #navbarMainMenu ul.nav ul.nav {\\n    padding-top: 0 !important;\\n    padding-bottom: 0 !important;\\n    box-shadow: none;\\n    display: none;\\n  }\\n\\n.block-header #navbarMainMenu ul.nav \\n li.active ul.nav {\\n    display: block !important;\\n}\\n  .main-header ul.nav.child-nav {\\n    padding: 0;\\n  }\\n  .copy-right br {\\n    display: none;\\n  }\\n  .copy-right {\\n    padding: 15px !important;\\n  }\\n  .service img {\\n    object-fit: cover;\\n    margin-top: 0 !important;\\n  }\\n  .main-header ul.nav.child-nav a {\\n    color: #f9fafb;\\n  }\\n  .why-choose .flex-grow-1.fw-500.fz-16.fz-lg-26 {\\n    color: #27282B;\\n  }\\n  .projects-box a.project-title {\\n    color: #27282B;\\n  }\\n  .intro-award {\\n    color: #27282B;\\n  }\\n  .main-header a.nav-link {\\n    color: white !important;\\n  }\\n  #product-category .flex-column, #product-collection .flex-column, #product-provider .flex-column, #product-tags .flex-column, #product-attribute .flex-column {\\n  \\toverflow-y: hidden !important;\\n  }\\n  .bestme-block-content-customize .flex-shrink-0.py-20.px-20.me-20.bg-eighth {\\n    padding: 5px !important;\\n  }\\n  p.title-service {\\n    text-transform: uppercase;\\n    font-size: 16px;\\n  }\\n    .block-header #navbarMainMenu ul.nav ul.nav li a {\\n    text-transform: lowercase;\\n    color: #c4c4c4;\\n  }\\n  #product-category .flex-column, #product-collection .flex-column, #product-provider .flex-column, #product-tags .flex-column, #product-attribute .flex-column {\\n    overflow-y: hidden !important;\\n  }\\n  .block-header #navbarMainMenu ul.nav ul.nav li {\\n    border: none;\\n    padding: 5px 0;\\n  }\\n  .main-header li.nav-item {\\n    padding: 5px 0;\\n  }\\n  .copy-right {\\n    font-size: 14px;\\n  }\\n  .project-item .project-img {\\n    border-radius: 0;\\n  }\\n  .product-listing b {\\n    color: #27282B;\\n  }\\n  .blog-category {\\n    padding-left: 15px !important;\\n  }\\n  .project-content .container {\\n    padding: 0;\\n  }\\n  .product-listing span.align-self-center.me-2.space-nowrap {\\n    color: #737373 !important;\\n  }\\n  .news-item .news-title {\\n    font-size: 18px;\\n  }\\n  .project-item .project-title {\\n    font-size: 18px;\\n  }\\n  .blog-category {\\n    border: none;\\n  }\\n  .blog-category li {\\n    border-bottom: 1px solid #8080804f;\\n  }\\n  .blog-category li a.active {\\n    border-bottom: 2px solid #084298 !important;\\n    width: 100%;\\n  }\\n  .intro-award .owl-stage-outer.owl-height {\\n    height: 450px !important;\\n  }\\n  .intro-award img {\\n    height: 219px !important;\\n  }\\n  #productDetailTab a.active {\\n    border-bottom: 1px solid;\\n  }\\n}\\n\\n#footer .footer-col li,#footer .footer-col li a, #footer .copy-right {\\n    color: white;\\n}\\n.header-search.ms-auto.order-2 span, #header .header-search .collapse-search span {\\n    color: var(--text-secondary);\\n}\\n\\n.about-us h2 {\\n    text-align: center;\\n}\\n\\n.about-us section.my-lg-50.my-30.about-us-home {\\n    margin-top: 0 !important;\\n    margin-bottom: 0 !important;\\n}\\n\\nsection.why-choose-us.my-lg-50.mt-30.pb-lg-50.pb-30 {\\n    margin-top: 0 !important;\\n    margin-bottom: 0 !important;\\n}\\n\\n.why-choose h2 {\\n    text-align: center;\\n}\\n\\n.projects-box section.py-50.container {\\n    padding-top: 0 !important;\\n    padding-bottom: 0 !important;\\n}\\n\\n.news-box .container.py-lg-50 {\\n    padding-top: 0 !important;\\n    padding-bottom: 0 !important;\\n}\\n\\n.news-box section.py-50.mb-lg-50.mb-30.news-home {\\n    margin-bottom: 0 !important;\\n}\\n\\n#header .menu-wrapper.show .navbar {\\n    display: block;\\n}\\n\\n#header .navbar {\\n    max-width: 60%;\\n    padding-left: 10%;\\n}\\n#header .header-search.show form.search-form {\\n    visibility: visible;\\n    width: 226px;\\n}\\n.project-content,.content-detail {\\n    color: black;\\n}\\n.header-bottom.d-none.d-lg-block.py-50 {\\n    padding: 15px 0 !important;\\n}"\n}', '2021-11-08 15:21:28', '2022-02-08 10:40:16'),
(420, 0, 'adg_topal_55', 'config', 'config_theme_checkout_page', '{\n}', '2021-11-08 15:21:28', '2021-11-08 15:21:28'),
(421, 0, 'adg_topal_55', 'config', 'config_section_sections', '{\n    "name": "Theme Config",\n    "version": "1.0",\n    "section": {\n        "header": {\n            "name": "header",\n            "text": "Header",\n            "visible": "1"\n        },\n        "slide-show": {\n            "name": "slide-show",\n            "text": "Slideshow",\n            "visible": "1"\n        },\n        "categories": {\n            "name": "categories",\n            "text": "Danh mục sản phẩm",\n            "visible": "1"\n        },\n        "hot-deals": {\n            "name": "hot-deals",\n            "text": "Sản phẩm khuyến mại",\n            "visible": "0",\n            "sort_order": "0"\n        },\n        "feature-products": {\n            "name": "feature-products",\n            "text": "Sản phẩm bán chạy",\n            "visible": "0",\n            "sort_order": "1"\n        },\n        "related-products": {\n            "name": "related-products",\n            "text": "Sản phẩm xem nhiều",\n            "visible": "1"\n        },\n        "new-products": {\n            "name": "new-products",\n            "text": "Sản phẩm mới",\n            "visible": "1",\n            "sort_order": "2"\n        },\n        "product-detail": {\n            "name": "product-details",\n            "text": "Chi tiết sản phẩm",\n            "visible": "1"\n        },\n        "banner": {\n            "name": "banner",\n            "text": "Banner",\n            "visible": "1"\n        },\n        "content_customize": {\n            "name": "content-customize",\n            "text": "Nội dung tùy chỉnh",\n            "visible": "1"\n        },\n        "partners": {\n            "name": "partners",\n            "text": "Đối tác",\n            "visible": "1"\n        },\n        "blog": {\n            "name": "blog",\n            "text": "Blog",\n            "visible": "1"\n        },\n        "rate": {\n            "name": "rate",\n            "text": "Đánh giá website",\n            "visible": "1"\n        },\n        "footer": {\n            "name": "footer",\n            "text": "Footer",\n            "visible": "1"\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-09 09:47:58'),
(422, 0, 'adg_topal_55', 'config', 'config_section_banner', '{\n    "name": "banner",\n    "text": "Banner",\n    "visible": "1",\n    "display": [\n        {\n            "image-url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/gioithieu3_2.png",\n            "url": "#",\n            "description": "Banner 1",\n            "visible": "1"\n        },\n        {\n            "image-url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/gioithieu3_3.png",\n            "url": "#",\n            "description": "Banner 2",\n            "visible": "1"\n        },\n        {\n            "image-url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/banner-4.jpg",\n            "url": "#",\n            "description": "Banner 3",\n            "visible": "1"\n        }\n    ]\n}', '2021-11-08 15:21:29', '2021-12-02 10:23:42'),
(423, 0, 'adg_topal_55', 'config', 'config_section_best_sales_product', '{\n    "name": "feature-product",\n    "text": "Sản phẩm bán chạy",\n    "visible": 1,\n    "setting": {\n        "title": "Sản phẩm bán chạy",\n        "sub_title" : "Mô tả tiêu đề",\n        "auto_retrieve_data": 1,\n        "resource_type": 1,\n        "collection_id": 1,\n        "category_id": 1,\n        "autoplay": 1,\n        "autoplay_time": 5,\n        "loop": 1\n    },\n    "display": {\n        "grid": {\n            "quantity": 4,\n            "row": 1\n        },\n        "grid_mobile": {\n            "quantity": 2,\n            "row": 1\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(424, 0, 'adg_topal_55', 'config', 'config_section_new_product', '{\n    "name": "new-product",\n    "text": "Sản phẩm mới",\n    "visible": "1",\n    "setting": {\n        "title": "Danh mục sản phẩm ",\n        "sub_title": "Mô tả tiêu đề",\n        "auto_retrieve_data": "1",\n        "resource_type": "1",\n        "collection_id": "1",\n        "category_id": "1",\n        "autoplay": "1",\n        "autoplay_time": "5",\n        "loop": "1"\n    },\n    "display": {\n        "grid": {\n            "quantity": "4",\n            "row": "1"\n        },\n        "grid_mobile": {\n            "quantity": "2",\n            "row": "1"\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-09 17:14:57'),
(425, 0, 'adg_topal_55', 'config', 'config_section_best_views_product', '{\n    "name": "related-product",\n    "text": "Sản phẩm xem nhiều",\n    "visible": 1,\n    "setting": {\n        "title": "Sản phẩm xem nhiều"\n    },\n    "display": {\n        "grid": {\n            "quantity": 10\n        },\n        "grid_mobile": {\n            "quantity": 2\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(426, 0, 'adg_topal_55', 'config', 'config_section_blog', '{\n    "name": "blog",\n    "text": "Blog",\n    "visible": 1,\n    "setting": {\n        "title": "Blogs"\n    },\n    "display": {\n        "menu": [\n            {\n                "type": "existing",\n                "menu": {\n                    "id": 12,\n                    "name": "Danh sách bài viết 1"\n                }\n            },\n            {\n                "type": "manual",\n                "entries": [\n                    {\n                        "id": 10,\n                        "name": "Entry 10"\n                    },\n                    {\n                        "id": 11,\n                        "name": "Entry 11"\n                    }\n                ]\n            }\n        ],\n        "grid": {\n            "quantity": 3,\n            "row": 1\n        },\n        "grid_mobile": {\n            "quantity": 1,\n            "row": 1\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(427, 0, 'adg_topal_55', 'config', 'config_section_detail_product', '{\n    "name": "product-detail",\n    "text": "Chi tiết sản phẩm",\n    "visible": 1,\n    "display": {\n        "name": true,\n        "description": false,\n        "price": true,\n        "price-compare": false,\n        "status": false,\n        "sale": false,\n        "rate": false\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(428, 0, 'adg_topal_55', 'config', 'config_section_footer', '{\n    "name": "footer",\n    "text": "Footer",\n    "visible": "1",\n    "contact": {\n        "title": "LIÊN HỆ CHÚNG TÔI",\n        "address": "37 Lê Văn Thiêm, Nhân Chính, Hà Nội&lt;br&gt;&lt;li&gt;Nhà máy: Km7, Đường 39, Thị trấn Yên Mỹ, Hưng Yên",\n        "phone-number": "1900 6828",\n        "email": "Austdoor@Austdoor.com",\n        "visible": "1"\n    },\n    "contact_more": [\n        {\n            "title": "Chi nhánh 1",\n            "address": "Duy Tan - Ha Noi",\n            "phone-number": "(+84)987654322",\n            "visible": "0"\n        }\n    ],\n    "collection": {\n        "title": "Bộ sưu tập",\n        "menu_id": "1",\n        "visible": "1"\n    },\n    "quick-links": {\n        "title": "ĐIỀU KHOẢN",\n        "menu_id": "16",\n        "visible": "1"\n    },\n    "subscribe": {\n        "title": "Đăng ký theo dõi",\n        "social_network": "1",\n        "visible": "1",\n        "youtube_visible": "1",\n        "facebook_visible": "1",\n        "instagram_visible": "1",\n        "shopee_visible": "1",\n        "tiki_visible": "1",\n        "sendo_visible": "1",\n        "lazada_visible": "1"\n    }\n}', '2021-11-08 15:21:29', '2021-12-24 10:26:17'),
(429, 0, 'adg_topal_55', 'config', 'config_section_header', '{\n    "name": "header",\n    "text": "Header",\n    "visible": "1",\n    "notify-bar": {\n        "name": "Thanh thông báo",\n        "visible": "1",\n        "content": "Mua sắm online thuận tiện và dễ dàng",\n        "url": "#"\n    },\n    "logo": {\n        "name": "Logo",\n        "url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/untitled-5555551.png",\n        "alt": "alt",\n        "height": ""\n    },\n    "menu": {\n        "name": "Menu",\n        "display-list": {\n            "name": "Menu chính",\n            "id": "1"\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-12-20 14:56:23'),
(430, 0, 'adg_topal_55', 'config', 'config_section_hot_product', '{\n    "name": "hot-deals",\n    "text": "Sản phẩm khuyến mại",\n    "visible": 1,\n    "setting": {\n        "title": "Sản phẩm khuyến mại",\n        "sub_title" : "Mô tả tiêu đề",\n        "auto_retrieve_data": 1,\n        "resource_type": 1,\n        "collection_id": 1,\n        "category_id": 1,\n        "autoplay": 1,\n        "autoplay_time": 5,\n        "loop": 1\n    },\n    "display": {\n        "grid": {\n            "quantity": 4,\n            "row": 1\n        },\n        "grid_mobile": {\n            "quantity": 2,\n            "row": 1\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(431, 0, 'adg_topal_55', 'config', 'config_section_list_product', '{\n    "name": "categories",\n    "text": "Danh mục sản phẩm",\n    "visible": 0,\n    "setting": {\n        "title": "Danh mục sản phẩm"\n    },\n    "display": {\n        "menu": {\n            "id": 1,\n            "name": "Danh mục sản phẩm 1"\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(432, 0, 'adg_topal_55', 'config', 'config_section_partner', '{\n    "name": "partners",\n    "text": "Đối tác",\n    "visible": 1,\n    "limit-per-line": 4,\n    "display": [\n        {\n            "image-url": "/catalog/view/theme/novaon/asset/img/partner_1.jpg",\n            "url": "#",\n            "description": "Partner 1",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/novaon/asset/img/partner_2.jpg",\n            "url": "#",\n            "description": "Partner 2",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/novaon/asset/img/partner_3.jpg",\n            "url": "#",\n            "description": "Partner 3",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/novaon/asset/img/partner_4.jpg",\n            "url": "#",\n            "description": "Partner 4",\n            "visible": 1\n        }\n    ],\n    "setting": {\n        "autoplay": 1,\n        "loop": 1,\n        "autoplay_time": 5\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(433, 0, 'adg_topal_55', 'config', 'config_section_slideshow', '{\n    "name": "slide-show",\n    "text": "Slideshow",\n    "visible": "1",\n    "setting": {\n        "transition-time": "5"\n    },\n    "display": [\n        {\n            "image-url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/banner1.png",\n            "url": "#",\n            "description": "slide_1",\n            "type": "image",\n            "video-url": "",\n            "id": "51392846"\n        },\n        {\n            "image-url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/banner2.png",\n            "url": "#",\n            "description": "slide_2",\n            "type": "image",\n            "video-url": "",\n            "id": "21020927"\n        },\n        {\n            "image-url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/banner3.png",\n            "url": "#",\n            "description": "slide_3",\n            "type": "image",\n            "video-url": "",\n            "id": "9359214"\n        },\n        {\n            "image-url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/banner4.png",\n            "description": "slide_4",\n            "id": "27897001",\n            "url": "#"\n        }\n    ]\n}', '2021-11-08 15:21:29', '2021-12-02 10:25:33'),
(434, 0, 'adg_topal_55', 'config', 'config_section_product_groups', '{\n    "name": "group products",\n    "text": "nhóm sản phẩm",\n    "visible": "1",\n    "list": [\n        {\n            "text": "Danh sách sản phẩm 1",\n            "visible": "0",\n            "setting": {\n                "title": "Danh sách sản phẩm 1",\n                "collection_id": "4",\n                "resource_type": "1",\n                "category_id": "1",\n                "sub_title": "Danh sách sản phẩm 1",\n                "autoplay": "1",\n                "autoplay_time": "5",\n                "loop": "1"\n            },\n            "display": {\n                "grid": {\n                    "quantity": "6",\n                    "row": "1"\n                },\n                "grid_mobile": {\n                    "quantity": "2",\n                    "row": "1"\n                }\n            },\n            "sort_order": "3"\n        }\n    ]\n}', '2021-11-08 15:21:29', '2021-11-09 09:47:58'),
(435, 0, 'adg_topal_55', 'config', 'config_section_content_customize', '{\n    "name": "content_customize",\n    "title": "Nội dung tuỳ chỉnh",\n    "display": [\n        {\n            "name": "ben-bi-0",\n            "title": "Bền bỉ",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/fame-1.png",\n                "title": "Bền bỉ",\n                "description": "Lấy chất lượng sản phẩm làm giá trị cốt lõi, cam kết mang đến những sản phẩm chính hãng, bảo đảm đúng chất lượng, quy cách, chủng loại, đúng theo tiêu chuẩn nhà sản xuất.",\n                "html": ""\n            }\n        },\n        {\n            "name": "da-dang-1",\n            "title": "Đa dạng",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/customer-1.png",\n                "title": "Đa dạng",\n                "description": "Không ngừng nỗ lực để thỏa mãn khách hàng. Chúng tôi quan niệm rằng chất lượng phục vụ là nền tảng để xây dựng thương hiệu vững mạnh.",\n                "html": ""\n            }\n        },\n        {\n            "name": "cong-nghe-thong-minh-2",\n            "title": "Công nghệ thông minh",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/service-1.png",\n                "title": "Công nghệ thông minh",\n                "description": "Nguồn nhân lực có nhiều kinh nghiệm, cách làm việc chuyên nghiệp đáp ứng nhu cầu khách hàng một cách hoàn hảo",\n                "html": ""\n            }\n        },\n        {\n            "name": "uy-tin-3",\n            "title": "Uy tín",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/new-product-1.png",\n                "title": "Uy tín",\n                "description": "Các sản phẩm phong phú, đa dạng. Đáp ứng đầy đủ nhu cầu của khách hàng trong xây dựng các công trình dân dụng và công nghiệp.",\n                "html": ""\n            }\n        },\n        {\n            "name": "cung-cap-4",\n            "title": "Cung cấp",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/group.png",\n                "title": "Cung cấp",\n                "description": "Dây chuyền sản xuất đồng bộ cửa cuốn Austdoor được chuyển giao từ Tập đoàn (Australia) giúp gia tăng chất lượng, đảm bảo sự chính xác và độ bền cho sản phẩm.",\n                "html": ""\n            }\n        },\n        {\n            "name": "dot-pha-5",\n            "title": "Đột phá ",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/group-1.png",\n                "title": "Đột phá ",\n                "description": "Những ứng dụng công nghệ mới nhất của Austdoor giúp cửa vận hành êm ái, chống ồn, an toàn cho người sử dụng và kiểm soát an ninh cho ngôi nhà.",\n                "html": ""\n            }\n        },\n        {\n            "name": "dich-vu-6",\n            "title": "Dịch vụ ",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/tools-1.png",\n                "title": "Dịch vụ ",\n                "description": "Sứ mệnh của Austdoor không chỉ đem tới cho ngôi nhà những tiện ích tốt nhất mà còn trở thành một người bạn chu đáo, tin cậy của mọi gia đình.",\n                "html": ""\n            }\n        },\n        {\n            "name": "niem-tin-7",\n            "title": "Niềm tin",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/guarantee-1.png",\n                "title": "Niềm tin",\n                "description": "Niềm tin của người sử dụng và sự ghi nhận của thị trường chính là động lực để Austdoor không ngừng tiến bước vì một cuộc sống tươi đẹp hơn.",\n                "html": ""\n            }\n        }\n    ]\n}', '2021-11-08 15:21:29', '2022-01-24 10:51:04'),
(437, 0, 'adg_topal_55', 'config', 'config_section_category_sections', '{\n    "name": "Theme Config",\n    "version": "1.0",\n    "section": {\n        "banner": {\n            "name": "banner",\n            "text": "Banner",\n            "visible": 1\n        },\n        "filter": {\n            "name": "filter",\n            "text": "Filter",\n            "visible": 1\n        },\n        "product_category": {\n            "name": "product_category",\n            "text": "Product Category",\n            "visible": 1\n        },\n        "product_list": {\n            "name": "product_list",\n            "text": "Product List",\n            "visible": 1\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(438, 0, 'adg_topal_55', 'config', 'config_section_category_banner', '{\n    "name": "banner",\n    "text": "Banner",\n    "visible": 1,\n    "display": [\n        {\n            "image-url": "/catalog/view/theme/default/image/banner/img-banner-06.jpg",\n            "url": "#",\n            "description": "Banner 1",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/default/image/banner/img-banner-02.png",\n            "url": "#",\n            "description": "Banner 2",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/default/image/banner/img-banner-03.png",\n            "url": "#",\n            "description": "Banner 3",\n            "visible": 1\n        }\n    ]\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(439, 0, 'adg_topal_55', 'config', 'config_section_category_filter', '{\n    "name": "filter",\n    "text": "Bộ lọc",\n    "visible": 1,\n    "setting": {\n        "title": "Bộ lọc"\n    },\n    "display": {\n        "supplier": {\n            "title": "Nhà cung cấp",\n            "visible": 1\n        },\n        "product-type": {\n            "title": "Loại sản phẩm",\n            "visible": 1\n        },\n        "collection": {\n            "title": "Bộ sưu tập",\n            "visible": 1\n        },\n        "property": {\n            "title": "Lọc theo tt - k dung",\n            "visible": 1,\n            "prop": [\n                "all",\n                "color",\n                "weight",\n                "size"\n            ]\n        },\n        "product-price": {\n            "title": "Giá sản phẩm",\n            "visible": 1,\n            "range": {\n                "from": 0,\n                "to": 100000000\n            }\n        },\n        "tag": {\n            "title": "Tag",\n            "visible": 1\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(440, 0, 'adg_topal_55', 'config', 'config_section_category_product_category', '{\n    "name": "product-category",\n    "text": "Danh mục sản phẩm",\n    "visible": 1,\n    "setting": {\n        "title": "Danh mục sản phẩm"\n    },\n    "display": {\n        "menu": {\n            "id": 1,\n            "name": "Danh mục sản phẩm 1"\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(441, 0, 'adg_topal_55', 'config', 'config_section_category_product_list', '{\n    "name": "product-list",\n    "text": "Danh sách sản phẩm",\n    "visible": 1,\n    "setting": {\n        "title": "Danh sách sản phẩm"\n    },\n    "display": {\n        "grid": {\n            "quantity": 10\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(442, 0, 'adg_topal_55', 'config', 'config_section_product_detail_sections', '{\n    "name": "Theme Config",\n    "version": "1.0",\n    "section": {\n        "related_product": {\n            "name": "related_product",\n            "text": "Related Product",\n            "visible": 1\n        },\n        "template": {\n            "name": "template",\n            "text": "Template",\n            "visible": 1\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(443, 0, 'adg_topal_55', 'config', 'config_section_product_detail_related_product', '{\n    "name": "related-product",\n    "text": "Sản phẩm liên quan",\n    "visible": "1",\n    "setting": {\n        "title": "SẢN PHẨM LIÊN QUAN",\n        "auto_retrieve_data": "0",\n        "resource_type": "2",\n        "collection_id": "1",\n        "category_id": "32",\n        "autoplay": "1",\n        "autoplay_time": "5",\n        "loop": "1"\n    },\n    "display": {\n        "grid": {\n            "quantity": "4"\n        },\n        "grid_mobile": {\n            "quantity": "2"\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-12-02 16:08:05'),
(444, 0, 'adg_topal_55', 'config', 'config_section_product_detail_template', '{\n    "name": "template",\n    "text": "Giao diện",\n    "visible": 1,\n    "display": {\n        "template": {\n            "id": 10\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(445, 0, 'adg_topal_55', 'config', 'config_section_blog_sections', '{\n    "name": "Theme Config",\n    "version": "1.0",\n    "section": {\n        "blog_category": {\n            "name": "blog_category",\n            "text": "Blog Category",\n            "visible": 1\n        },\n        "blog_list": {\n            "name": "blog_list",\n            "text": "Blog List",\n            "visible": 1\n        },\n        "latest_blog": {\n            "name": "latest_blog",\n            "text": "Latest Blog",\n            "visible": 1\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(446, 0, 'adg_topal_55', 'config', 'config_section_blog_blog_category', '{\n    "name": "blog-category",\n    "text": "Danh mục bài viết",\n    "visible": 1,\n    "setting": {\n        "title": "Danh mục bài viết"\n    },\n    "display": {\n        "menu": {\n            "id": 1,\n            "name": "Danh mục bài viết 1"\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(447, 0, 'adg_topal_55', 'config', 'config_section_blog_blog_list', '{\n    "name": "blog-list",\n    "text": "Danh sách bài viết",\n    "visible": 1,\n    "display": {\n        "grid": {\n            "quantity": 20\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(448, 0, 'adg_topal_55', 'config', 'config_section_blog_latest_blog', '{\n    "name": "latest-blog",\n    "text": "Bài viết mới nhất",\n    "visible": 1,\n    "setting": {\n        "title": "Bài viết mới nhất"\n    },\n    "display": {\n        "grid": {\n            "quantity": 10\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(449, 0, 'adg_topal_55', 'config', 'config_section_contact_sections', '{\n    "name": "Theme Config",\n    "version": "1.0",\n    "section": {\n        "map": {\n            "name": "map",\n            "text": "Bản đồ",\n            "address": "<iframe src=\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\" width=\\"600\\" height=\\"450\\" frameborder=\\"0\\" style=\\"border:0\\" allowfullscreen></iframe>",\n            "visible": 1\n        },\n        "info": {\n            "name": "info",\n            "text": "Thông tin cửa hàng",\n            "email": "contact-us@novaon.asia",\n            "phone": "0000000000",\n            "address": "address",\n            "visible": 1\n        },\n        "form": {\n            "name": "form",\n            "title": "Form liên hệ",\n            "email": "email@mail.com",\n            "visible": 1\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(450, 0, 'adg_topal_55', 'config', 'config_section_contact_map', '{   //////// CÁI NÀY KHÔNG DÙNG NỮA\n    "name": "map",\n    "text": "Bản đồ",\n    "visible": 1,\n    "setting": {\n        "title": "Bản đồ"\n    },\n    "display": {\n        "address": "<iframe src=\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\" width=\\"600\\" height=\\"450\\" frameborder=\\"0\\" style=\\"border:0\\" allowfullscreen></iframe>"\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(451, 0, 'adg_topal_55', 'config', 'config_section_contact_contact', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\n    "name": "contact",\n    "text": "Liên hệ",\n    "visible": 1,\n    "setting": {\n        "title": "Vị trí của chúng tôi"\n    },\n    "display": {\n        "store": {\n            "title": "Gian hàng",\n            "content": "Số 12 Dịch Vọng Hậu, Cầu Giấy, Hà Nội",\n            "visible": 1\n        },\n        "telephone": {\n            "title": "Điện thoại",\n            "content": "(+84) 987 654 321",\n            "visible": 1\n        },\n        "social_follow": {\n            "socials": [\n                {\n                    "name": "facebook",\n                    "text": "Facebook",\n                    "visible": 1\n                },\n                {\n                    "name": "twitter",\n                    "text": "Twitter",\n                    "visible": 1\n                },\n                {\n                    "name": "instagram",\n                    "text": "Instagram",\n                    "visible": 1\n                },\n                {\n                    "name": "tumblr",\n                    "text": "Tumblr",\n                    "visible": 1\n                },\n                {\n                    "name": "youtube",\n                    "text": "Youtube",\n                    "visible": 1\n                },\n                {\n                    "name": "googleplus",\n                    "text": "Google Plus",\n                    "visible": 1\n                }\n            ],\n            "visible": 1\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(452, 0, 'adg_topal_55', 'config', 'config_section_contact_form', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\n    "name": "form",\n    "text": "Biểu mẫu",\n    "visible": 1,\n    "setting": {\n        "title": "Liên hệ với chúng tôi"\n    },\n    "data": {\n        "email": "sale.247@xshop.com"\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(453, 0, 'adg_topal_55', 'config', 'config_section_rate', '{\n  "name": "rate",\n  "text": "Đánh giá website",\n  "visible": 1,\n  "setting": {\n    "title": "Đánh giá website",\n    "sub_title" : "Mô tả tiêu đề",\n    "auto_retrieve_data": 1,\n    "resource_type": 1,\n    "autoplay": 1,\n    "autoplay_time": 5,\n    "loop": 1\n  },\n  "display": {\n    "grid": {\n      "quantity": 3,\n      "row": 1\n    },\n    "grid_mobile": {\n      "quantity": 1,\n      "row": 1\n    }\n  }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(454, 0, 'adg_topal_55', 'config', 'config_section_customize_layout', '{\n    "config_theme_color": {\n        "main": {\n            "main": {\n                "hex": "#13203D",\n                "visible": "1"\n            },\n            "button": {\n                "hex": "#13203D",\n                "visible": "1"\n            }\n        }\n    },\n    "config_theme_text": {\n        "all": {\n            "title": "Font Roboto",\n            "font": "Roboto",\n            "supported-fonts": [\n                "Open Sans",\n                "IBM Plex Sans",\n                "Courier New",\n                "Roboto",\n                "Nunito",\n                "Arial",\n                "DejaVu Sans",\n                "Tahoma",\n                "Time News Roman",\n                "Mulish"\n            ]\n        }\n    },\n    "image_size_suggestion": {\n        "favicon": [\n            "35",\n            "35"\n        ],\n        "logo": [\n            "300",\n            "75"\n        ],\n        "home": {\n            "banner_1": [\n                "1116",\n                "214"\n            ],\n            "banner_2": [\n                "360",\n                "205"\n            ],\n            "banner_3": [\n                "555",\n                "210"\n            ],\n            "slide_show": [\n                "1442",\n                "582"\n            ]\n        },\n        "category": {\n            "banner_1": [\n                "263",\n                "307"\n            ]\n        }\n    },\n    "app_config_theme": [\n        {\n            "name": "Trang chủ",\n            "value": "home_page",\n            "position": [\n                {\n                    "name": "Đầu trang",\n                    "value": "bestme_app_after_header"\n                },\n                {\n                    "name": "Dưới slide",\n                    "value": "bestme_app_after_slide"\n                },\n                {\n                    "name": "Dưới danh sách sản phẩm",\n                    "value": "bestme_app_after_product"\n                },\n                {\n                    "name": "Trên cuối trang",\n                    "value": "bestme_app_before_footer"\n                }\n            ]\n        },\n        {\n            "name": "Sản phẩm",\n            "value": "product",\n            "position": [\n                {\n                    "name": "Đầu trang",\n                    "value": "bestme_app_after_header"\n                },\n                {\n                    "name": "Dưới danh sách sản phẩm",\n                    "value": "bestme_app_after_product"\n                },\n                {\n                    "name": "Trên cuối trang",\n                    "value": "bestme_app_before_footer"\n                }\n            ]\n        }\n    ],\n    "default_customize_layout": [\n        {\n            "name": "txt_slide_show_banner",\n            "file": "slideshow-banner"\n        },\n        {\n            "name": "txt_about_us",\n            "file": "about-us"\n        },\n        {\n            "name": "txt_why_choose",\n            "file": "why-choose"\n        },\n        {\n            "name": "txt_block_product",\n            "file": "block-products"\n        },\n        {\n            "name": "txt_block_projects",\n            "file": "projects-box"\n        },\n        {\n            "name": "txt_intro_award",\n            "file": "intro-award"\n        },\n        {\n            "name": "txt_service",\n            "file": "service"\n        },\n        {\n            "name": "txt_block_news",\n            "file": "news-box"\n        }\n    ],\n    "version": "1"\n}', '2021-12-02 16:41:24', '2022-01-24 10:49:19');

-- --------------------------------------------------------

--
-- Table structure for table `oc_translation`
--

CREATE TABLE `oc_translation` (
  `translation_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_upload`
--

CREATE TABLE `oc_upload` (
  `upload_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_user`
--

CREATE TABLE `oc_user` (
  `user_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(500) NOT NULL,
  `password` varchar(100) NOT NULL,
  `password_temp` varchar(100) DEFAULT NULL,
  `salt` varchar(9) NOT NULL,
  `salt_temp` varchar(9) DEFAULT NULL,
  `firstname` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `telephone` varchar(32) DEFAULT NULL,
  `image` varchar(1024) NOT NULL,
  `code` varchar(40) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `info` varchar(1024) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `permission` text,
  `date_added` datetime NOT NULL,
  `last_logged_in` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_user_group`
--

CREATE TABLE `oc_user_group` (
  `user_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_user_group`
--

INSERT INTO `oc_user_group` (`user_group_id`, `name`, `permission`) VALUES
(1, 'Administrator', '{"access":["app_store\\/detail","theme\\/ecommerce","settings\\/user_group","cash_flow\\/cash_flow","cash_flow\\/receipt_voucher","cash_flow\\/payment_voucher","app_store\\/my_app","extension\\/extension\\/appstore","common\\/app_config","blog\\/blog","blog\\/category","catalog\\/attribute","catalog\\/attribute_group","catalog\\/category","catalog\\/collection","catalog\\/download","catalog\\/filter","catalog\\/information","catalog\\/manufacturer","catalog\\/option","catalog\\/product","catalog\\/recurring","catalog\\/review","catalog\\/store_receipt","catalog\\/warehouse","catalog\\/store","catalog\\/cost_adjustment_receipt","catalog\\/store_take_receipt","catalog\\/store_transfer_receipt","common\\/cloudinary_upload","common\\/column_left","common\\/custom_column_left","common\\/custom_header","common\\/delivery_api","common\\/developer","common\\/filemanager","common\\/forgotten_orig","common\\/login_orig","common\\/profile","common\\/reset_orig","common\\/security","custom\\/group_menu","custom\\/menu","custom\\/menu_item","custom\\/preference","custom\\/theme","custom\\/theme_develop","customer\\/custom_field","customer\\/customer","customer\\/customer_approval","customer\\/customer_group","design\\/banner","design\\/layout","design\\/seo_url","design\\/theme","design\\/translation","discount\\/discount","discount\\/coupon","discount\\/setting","event\\/language","event\\/statistics","event\\/theme","extension\\/analytics\\/google","extension\\/captcha\\/basic","extension\\/captcha\\/google","extension\\/dashboard\\/activity","extension\\/dashboard\\/chart","extension\\/dashboard\\/customer","extension\\/dashboard\\/map","extension\\/dashboard\\/online","extension\\/dashboard\\/order","extension\\/dashboard\\/recent","extension\\/dashboard\\/sale","extension\\/extension\\/analytics","extension\\/extension\\/captcha","extension\\/extension\\/dashboard","extension\\/extension\\/feed","extension\\/extension\\/fraud","extension\\/extension\\/menu","extension\\/extension\\/module","extension\\/extension\\/payment","extension\\/extension\\/report","extension\\/extension\\/shipping","extension\\/extension\\/theme","extension\\/extension\\/total","extension\\/feed\\/google_base","extension\\/feed\\/google_sitemap","extension\\/feed\\/openbaypro","extension\\/fraud\\/fraudlabspro","extension\\/fraud\\/ip","extension\\/fraud\\/maxmind","extension\\/module\\/account","extension\\/module\\/age_restriction","extension\\/module\\/amazon_login","extension\\/module\\/amazon_pay","extension\\/module\\/banner","extension\\/module\\/bestseller","extension\\/module\\/carousel","extension\\/module\\/category","extension\\/module\\/customer_welcome","extension\\/module\\/divido_calculator","extension\\/module\\/ebay_listing","extension\\/module\\/featured","extension\\/module\\/filter","extension\\/module\\/google_hangouts","extension\\/module\\/hello_world","extension\\/module\\/html","extension\\/module\\/information","extension\\/module\\/klarna_checkout_module","extension\\/module\\/latest","extension\\/module\\/laybuy_layout","extension\\/module\\/modification_editor","extension\\/module\\/pilibaba_button","extension\\/module\\/pp_braintree_button","extension\\/module\\/pp_button","extension\\/module\\/pp_login","extension\\/module\\/recommendation","extension\\/module\\/sagepay_direct_cards","extension\\/module\\/sagepay_server_cards","extension\\/module\\/simple_blog","extension\\/module\\/simple_blog\\/article","extension\\/module\\/simple_blog\\/author","extension\\/module\\/simple_blog\\/category","extension\\/module\\/simple_blog\\/comment","extension\\/module\\/simple_blog\\/install","extension\\/module\\/simple_blog\\/report","extension\\/module\\/simple_blog_category","extension\\/module\\/slideshow","extension\\/module\\/so_categories","extension\\/module\\/so_category_slider","extension\\/module\\/so_deals","extension\\/module\\/so_extra_slider","extension\\/module\\/so_facebook_message","extension\\/module\\/so_filter_shop_by","extension\\/module\\/so_home_slider","extension\\/module\\/so_html_content","extension\\/module\\/so_latest_blog","extension\\/module\\/so_listing_tabs","extension\\/module\\/so_megamenu","extension\\/module\\/so_newletter_custom_popup","extension\\/module\\/so_onepagecheckout","extension\\/module\\/so_page_builder","extension\\/module\\/so_quickview","extension\\/module\\/so_searchpro","extension\\/module\\/so_sociallogin","extension\\/module\\/so_tools","extension\\/module\\/soconfig","extension\\/module\\/special","extension\\/module\\/store","extension\\/module\\/theme_builder_config","extension\\/openbay\\/amazon","extension\\/openbay\\/amazon_listing","extension\\/openbay\\/amazon_product","extension\\/openbay\\/amazonus","extension\\/openbay\\/amazonus_listing","extension\\/openbay\\/amazonus_product","extension\\/openbay\\/ebay","extension\\/openbay\\/ebay_profile","extension\\/openbay\\/ebay_template","extension\\/openbay\\/etsy","extension\\/openbay\\/etsy_product","extension\\/openbay\\/etsy_shipping","extension\\/openbay\\/etsy_shop","extension\\/openbay\\/fba","extension\\/payment\\/alipay","extension\\/payment\\/alipay_cross","extension\\/payment\\/amazon_login_pay","extension\\/payment\\/authorizenet_aim","extension\\/payment\\/authorizenet_sim","extension\\/payment\\/bank_transfer","extension\\/payment\\/bluepay_hosted","extension\\/payment\\/bluepay_redirect","extension\\/payment\\/cardconnect","extension\\/payment\\/cardinity","extension\\/payment\\/cheque","extension\\/payment\\/cod","extension\\/payment\\/divido","extension\\/payment\\/eway","extension\\/payment\\/firstdata","extension\\/payment\\/firstdata_remote","extension\\/payment\\/free_checkout","extension\\/payment\\/g2apay","extension\\/payment\\/globalpay","extension\\/payment\\/globalpay_remote","extension\\/payment\\/klarna_account","extension\\/payment\\/klarna_checkout","extension\\/payment\\/klarna_invoice","extension\\/payment\\/laybuy","extension\\/payment\\/liqpay","extension\\/payment\\/nochex","extension\\/payment\\/paymate","extension\\/payment\\/paypoint","extension\\/payment\\/payza","extension\\/payment\\/perpetual_payments","extension\\/payment\\/pilibaba","extension\\/payment\\/pp_braintree","extension\\/payment\\/pp_express","extension\\/payment\\/pp_payflow","extension\\/payment\\/pp_payflow_iframe","extension\\/payment\\/pp_pro","extension\\/payment\\/pp_pro_iframe","extension\\/payment\\/pp_standard","extension\\/payment\\/realex","extension\\/payment\\/realex_remote","extension\\/payment\\/sagepay_direct","extension\\/payment\\/sagepay_server","extension\\/payment\\/sagepay_us","extension\\/payment\\/securetrading_pp","extension\\/payment\\/securetrading_ws","extension\\/payment\\/skrill","extension\\/payment\\/squareup","extension\\/payment\\/twocheckout","extension\\/payment\\/web_payment_software","extension\\/payment\\/wechat_pay","extension\\/payment\\/worldpay","extension\\/report\\/customer_activity","extension\\/report\\/customer_order","extension\\/report\\/customer_reward","extension\\/report\\/customer_search","extension\\/report\\/customer_transaction","extension\\/report\\/marketing","extension\\/report\\/product_purchased","extension\\/report\\/product_viewed","extension\\/report\\/sale_coupon","extension\\/report\\/sale_order","extension\\/report\\/sale_return","extension\\/report\\/sale_shipping","extension\\/report\\/sale_tax","extension\\/shipping\\/auspost","extension\\/shipping\\/citylink","extension\\/shipping\\/ec_ship","extension\\/shipping\\/fedex","extension\\/shipping\\/flat","extension\\/shipping\\/free","extension\\/shipping\\/item","extension\\/shipping\\/parcelforce_48","extension\\/shipping\\/pickup","extension\\/shipping\\/royal_mail","extension\\/shipping\\/ups","extension\\/shipping\\/usps","extension\\/shipping\\/weight","extension\\/theme\\/default","extension\\/theme\\/dunght","extension\\/theme\\/novaon","extension\\/total\\/coupon","extension\\/total\\/credit","extension\\/total\\/handling","extension\\/total\\/klarna_fee","extension\\/total\\/low_order_fee","extension\\/total\\/reward","extension\\/total\\/shipping","extension\\/total\\/sub_total","extension\\/total\\/tax","extension\\/total\\/total","extension\\/total\\/voucher","localisation\\/country","localisation\\/currency","localisation\\/geo_zone","localisation\\/language","localisation\\/length_class","localisation\\/location","localisation\\/order_status","localisation\\/return_action","localisation\\/return_reason","localisation\\/return_status","localisation\\/stock_status","localisation\\/tax_class","localisation\\/tax_rate","localisation\\/weight_class","localisation\\/zone","mail\\/affiliate","mail\\/customer","mail\\/forgotten","mail\\/forgotten_orig","mail\\/return","mail\\/reward","mail\\/transaction","marketing\\/contact","marketing\\/coupon","marketing\\/marketing","marketplace\\/api","marketplace\\/event","marketplace\\/extension","marketplace\\/install","marketplace\\/installer","marketplace\\/marketplace","marketplace\\/modification","marketplace\\/openbay","online_store\\/contents","online_store\\/domain","online_store\\/google_shopping","report\\/online","report\\/overview","report\\/report","report\\/statistics","report\\/product","report\\/order","report\\/financial","report\\/staff","report\\/store","sale\\/order","sale\\/return_receipt","sale\\/recurring","sale\\/return","sale\\/voucher","sale\\/voucher_theme","sale_channel\\/pos_novaon","extension\\/appstore\\/mar_ons_chatbot","extension\\/appstore\\/mar_ons_onfluencer","sale_channel\\/novaonx_social","section\\/rate","rate\\/rate","section\\/blog","section\\/customize_layout","section\\/banner","section\\/best_sales_product","section\\/best_views_product","section\\/blog","section\\/content_customize","section\\/detail_product","section\\/footer","section\\/header","section\\/hot_product","section\\/list_product","section\\/new_product","section\\/partner","section\\/preview","section\\/sections","section\\/slideshow","section_blog\\/blog_category","section_blog\\/blog_list","section_blog\\/latest_blog","section_blog\\/sections","section_category\\/banner","section_category\\/filter","section_category\\/product_category","section_category\\/product_list","section_category\\/sections","section_contact\\/contact","section_contact\\/form","section_contact\\/map","section_contact\\/sections","section_product_detail\\/related_product","section_product_detail\\/sections","setting\\/setting","setting\\/store","settings\\/account","settings\\/classify","settings\\/delivery","settings\\/general","settings\\/payment","settings\\/settings","settings\\/notify","startup\\/error","startup\\/event","startup\\/login","startup\\/permission","startup\\/router","startup\\/sass","startup\\/startup","super\\/dashboard","theme\\/color","theme\\/favicon","theme\\/section_theme","theme\\/social","theme\\/text","tool\\/backup","tool\\/log","tool\\/upload","user\\/api","user\\/user","user\\/user_permission"],"modify":["app_store\\/detail","theme\\/ecommerce","settings\\/user_group","cash_flow\\/cash_flow","cash_flow\\/receipt_voucher","cash_flow\\/payment_voucher","app_store\\/my_app","extension\\/extension\\/appstore","common\\/app_config", "blog\\/blog","blog\\/category","catalog\\/attribute","catalog\\/attribute_group","catalog\\/category","catalog\\/collection","catalog\\/download","catalog\\/filter","catalog\\/information","catalog\\/manufacturer","catalog\\/option","catalog\\/product","catalog\\/recurring","catalog\\/review","catalog\\/store_receipt","catalog\\/warehouse","catalog\\/store","catalog\\/cost_adjustment_receipt","catalog\\/store_take_receipt","catalog\\/store_transfer_receipt","common\\/cloudinary_upload","common\\/column_left","common\\/custom_column_left","common\\/custom_header","common\\/delivery_api","common\\/developer","common\\/filemanager","common\\/forgotten_orig","common\\/login_orig","common\\/profile","common\\/reset_orig","common\\/security","custom\\/group_menu","custom\\/menu","custom\\/menu_item","custom\\/preference","custom\\/theme","custom\\/theme_develop","customer\\/custom_field","customer\\/customer","customer\\/customer_approval","customer\\/customer_group","design\\/banner","design\\/layout","design\\/seo_url","design\\/theme","design\\/translation","discount\\/discount","discount\\/coupon","discount\\/setting","event\\/language","event\\/statistics","event\\/theme","extension\\/analytics\\/google","extension\\/captcha\\/basic","extension\\/captcha\\/google","extension\\/dashboard\\/activity","extension\\/dashboard\\/chart","extension\\/dashboard\\/customer","extension\\/dashboard\\/map","extension\\/dashboard\\/online","extension\\/dashboard\\/order","extension\\/dashboard\\/recent","extension\\/dashboard\\/sale","extension\\/extension\\/analytics","extension\\/extension\\/captcha","extension\\/extension\\/dashboard","extension\\/extension\\/feed","extension\\/extension\\/fraud","extension\\/extension\\/menu","extension\\/extension\\/module","extension\\/extension\\/payment","extension\\/extension\\/report","extension\\/extension\\/shipping","extension\\/extension\\/theme","extension\\/extension\\/total","extension\\/feed\\/google_base","extension\\/feed\\/google_sitemap","extension\\/feed\\/openbaypro","extension\\/fraud\\/fraudlabspro","extension\\/fraud\\/ip","extension\\/fraud\\/maxmind","extension\\/module\\/account","extension\\/module\\/age_restriction","extension\\/module\\/amazon_login","extension\\/module\\/amazon_pay","extension\\/module\\/banner","extension\\/module\\/bestseller","extension\\/module\\/carousel","extension\\/module\\/category","extension\\/module\\/customer_welcome","extension\\/module\\/divido_calculator","extension\\/module\\/ebay_listing","extension\\/module\\/featured","extension\\/module\\/filter","extension\\/module\\/google_hangouts","extension\\/module\\/hello_world","extension\\/module\\/html","extension\\/module\\/information","extension\\/module\\/klarna_checkout_module","extension\\/module\\/latest","extension\\/module\\/laybuy_layout","extension\\/module\\/modification_editor","extension\\/module\\/pilibaba_button","extension\\/module\\/pp_braintree_button","extension\\/module\\/pp_button","extension\\/module\\/pp_login","extension\\/module\\/recommendation","extension\\/module\\/sagepay_direct_cards","extension\\/module\\/sagepay_server_cards","extension\\/module\\/simple_blog","extension\\/module\\/simple_blog\\/article","extension\\/module\\/simple_blog\\/author","extension\\/module\\/simple_blog\\/category","extension\\/module\\/simple_blog\\/comment","extension\\/module\\/simple_blog\\/install","extension\\/module\\/simple_blog\\/report","extension\\/module\\/simple_blog_category","extension\\/module\\/slideshow","extension\\/module\\/so_categories","extension\\/module\\/so_category_slider","extension\\/module\\/so_deals","extension\\/module\\/so_extra_slider","extension\\/module\\/so_facebook_message","extension\\/module\\/so_filter_shop_by","extension\\/module\\/so_home_slider","extension\\/module\\/so_html_content","extension\\/module\\/so_latest_blog","extension\\/module\\/so_listing_tabs","extension\\/module\\/so_megamenu","extension\\/module\\/so_newletter_custom_popup","extension\\/module\\/so_onepagecheckout","extension\\/module\\/so_page_builder","extension\\/module\\/so_quickview","extension\\/module\\/so_searchpro","extension\\/module\\/so_sociallogin","extension\\/module\\/so_tools","extension\\/module\\/soconfig","extension\\/module\\/special","extension\\/module\\/store","extension\\/module\\/theme_builder_config","extension\\/openbay\\/amazon","extension\\/openbay\\/amazon_listing","extension\\/openbay\\/amazon_product","extension\\/openbay\\/amazonus","extension\\/openbay\\/amazonus_listing","extension\\/openbay\\/amazonus_product","extension\\/openbay\\/ebay","extension\\/openbay\\/ebay_profile","extension\\/openbay\\/ebay_template","extension\\/openbay\\/etsy","extension\\/openbay\\/etsy_product","extension\\/openbay\\/etsy_shipping","extension\\/openbay\\/etsy_shop","extension\\/openbay\\/fba","extension\\/payment\\/alipay","extension\\/payment\\/alipay_cross","extension\\/payment\\/amazon_login_pay","extension\\/payment\\/authorizenet_aim","extension\\/payment\\/authorizenet_sim","extension\\/payment\\/bank_transfer","extension\\/payment\\/bluepay_hosted","extension\\/payment\\/bluepay_redirect","extension\\/payment\\/cardconnect","extension\\/payment\\/cardinity","extension\\/payment\\/cheque","extension\\/payment\\/cod","extension\\/payment\\/divido","extension\\/payment\\/eway","extension\\/payment\\/firstdata","extension\\/payment\\/firstdata_remote","extension\\/payment\\/free_checkout","extension\\/payment\\/g2apay","extension\\/payment\\/globalpay","extension\\/payment\\/globalpay_remote","extension\\/payment\\/klarna_account","extension\\/payment\\/klarna_checkout","extension\\/payment\\/klarna_invoice","extension\\/payment\\/laybuy","extension\\/payment\\/liqpay","extension\\/payment\\/nochex","extension\\/payment\\/paymate","extension\\/payment\\/paypoint","extension\\/payment\\/payza","extension\\/payment\\/perpetual_payments","extension\\/payment\\/pilibaba","extension\\/payment\\/pp_braintree","extension\\/payment\\/pp_express","extension\\/payment\\/pp_payflow","extension\\/payment\\/pp_payflow_iframe","extension\\/payment\\/pp_pro","extension\\/payment\\/pp_pro_iframe","extension\\/payment\\/pp_standard","extension\\/payment\\/realex","extension\\/payment\\/realex_remote","extension\\/payment\\/sagepay_direct","extension\\/payment\\/sagepay_server","extension\\/payment\\/sagepay_us","extension\\/payment\\/securetrading_pp","extension\\/payment\\/securetrading_ws","extension\\/payment\\/skrill","extension\\/payment\\/squareup","extension\\/payment\\/twocheckout","extension\\/payment\\/web_payment_software","extension\\/payment\\/wechat_pay","extension\\/payment\\/worldpay","extension\\/report\\/customer_activity","extension\\/report\\/customer_order","extension\\/report\\/customer_reward","extension\\/report\\/customer_search","extension\\/report\\/customer_transaction","extension\\/report\\/marketing","extension\\/report\\/product_purchased","extension\\/report\\/product_viewed","extension\\/report\\/sale_coupon","extension\\/report\\/sale_order","extension\\/report\\/sale_return","extension\\/report\\/sale_shipping","extension\\/report\\/sale_tax","extension\\/shipping\\/auspost","extension\\/shipping\\/citylink","extension\\/shipping\\/ec_ship","extension\\/shipping\\/fedex","extension\\/shipping\\/flat","extension\\/shipping\\/free","extension\\/shipping\\/item","extension\\/shipping\\/parcelforce_48","extension\\/shipping\\/pickup","extension\\/shipping\\/royal_mail","extension\\/shipping\\/ups","extension\\/shipping\\/usps","extension\\/shipping\\/weight","extension\\/theme\\/default","extension\\/theme\\/dunght","extension\\/theme\\/novaon","extension\\/total\\/coupon","extension\\/total\\/credit","extension\\/total\\/handling","extension\\/total\\/klarna_fee","extension\\/total\\/low_order_fee","extension\\/total\\/reward","extension\\/total\\/shipping","extension\\/total\\/sub_total","extension\\/total\\/tax","extension\\/total\\/total","extension\\/total\\/voucher","localisation\\/country","localisation\\/currency","localisation\\/geo_zone","localisation\\/language","localisation\\/length_class","localisation\\/location","localisation\\/order_status","localisation\\/return_action","localisation\\/return_reason","localisation\\/return_status","localisation\\/stock_status","localisation\\/tax_class","localisation\\/tax_rate","localisation\\/weight_class","localisation\\/zone","mail\\/affiliate","mail\\/customer","mail\\/forgotten","mail\\/forgotten_orig","mail\\/return","mail\\/reward","mail\\/transaction","marketing\\/contact","marketing\\/coupon","marketing\\/marketing","marketplace\\/api","marketplace\\/event","marketplace\\/extension","marketplace\\/install","marketplace\\/installer","marketplace\\/marketplace","marketplace\\/modification","marketplace\\/openbay","online_store\\/contents","online_store\\/domain","online_store\\/google_shopping","report\\/online","report\\/overview","report\\/report","report\\/statistics","report\\/product","report\\/order","report\\/financial","report\\/staff","report\\/store","sale\\/order","sale\\/return_receipt","sale\\/recurring","sale\\/return","sale\\/voucher","sale\\/voucher_theme","sale_channel\\/pos_novaon","extension\\/appstore\\/mar_ons_chatbot","extension\\/appstore\\/mar_ons_onfluencer","sale_channel\\/novaonx_social","section\\/rate","rate\\/rate","section\\/blog","section\\/customize_layout","section\\/banner","section\\/best_sales_product","section\\/best_views_product","section\\/blog","section\\/content_customize","section\\/detail_product","section\\/footer","section\\/header","section\\/hot_product","section\\/list_product","section\\/new_product","section\\/partner","section\\/preview","section\\/sections","section\\/slideshow","section_blog\\/blog_category","section_blog\\/blog_list","section_blog\\/latest_blog","section_blog\\/sections","section_category\\/banner","section_category\\/filter","section_category\\/product_category","section_category\\/product_list","section_category\\/sections","section_contact\\/contact","section_contact\\/form","section_contact\\/map","section_contact\\/sections","section_product_detail\\/related_product","section_product_detail\\/sections","setting\\/setting","setting\\/store","settings\\/account","settings\\/classify","settings\\/delivery","settings\\/general","settings\\/payment","settings\\/settings","settings\\/notify","startup\\/error","startup\\/event","startup\\/login","startup\\/permission","startup\\/router","startup\\/sass","startup\\/startup","super\\/dashboard","theme\\/color","theme\\/favicon","theme\\/section_theme","theme\\/social","theme\\/text","tool\\/backup","tool\\/log","tool\\/upload","user\\/api","user\\/user","user\\/user_permission"]}'),
(10, 'Demonstration', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher`
--

CREATE TABLE `oc_voucher` (
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_history`
--

CREATE TABLE `oc_voucher_history` (
  `voucher_history_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_theme`
--

CREATE TABLE `oc_voucher_theme` (
  `voucher_theme_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_voucher_theme`
--

INSERT INTO `oc_voucher_theme` (`voucher_theme_id`, `image`) VALUES
(8, 'catalog/demo/canon_eos_5d_2.jpg'),
(7, 'catalog/demo/gift-voucher-birthday.jpg'),
(6, 'catalog/demo/apple_logo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_theme_description`
--

CREATE TABLE `oc_voucher_theme_description` (
  `voucher_theme_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_voucher_theme_description`
--

INSERT INTO `oc_voucher_theme_description` (`voucher_theme_id`, `language_id`, `name`) VALUES
(6, 1, 'Christmas'),
(7, 1, 'Birthday'),
(8, 1, 'General');

-- --------------------------------------------------------

--
-- Table structure for table `oc_warehouse`
--

CREATE TABLE `oc_warehouse` (
  `warehouse_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_version_id` varchar(256) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_weight_class`
--

CREATE TABLE `oc_weight_class` (
  `weight_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL DEFAULT '0.00000000'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_weight_class`
--

INSERT INTO `oc_weight_class` (`weight_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '1000.00000000'),
(5, '2.20460000'),
(6, '35.27400000');

-- --------------------------------------------------------

--
-- Table structure for table `oc_weight_class_description`
--

CREATE TABLE `oc_weight_class_description` (
  `weight_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_weight_class_description`
--

INSERT INTO `oc_weight_class_description` (`weight_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Kilogram', 'kg'),
(2, 1, 'Gram', 'g'),
(5, 1, 'Pound ', 'lb'),
(6, 1, 'Ounce', 'oz'),
(1, 2, 'Kilogram', 'kg'),
(2, 2, 'Gram', 'g'),
(5, 2, 'Pound ', 'lb'),
(6, 2, 'Ounce', 'oz');

-- --------------------------------------------------------

--
-- Table structure for table `oc_zone`
--

CREATE TABLE `oc_zone` (
  `zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_zone`
--

INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1, 230, 'Hà Nội', 'HN', 1),
(2, 230, 'Hồ Chí Minh', 'HCM', 1),
(3, 230, 'An Giang', 'AG', 1),
(4, 230, 'Bà Rịa-Vũng Tàu', 'BR', 1),
(5, 230, 'Bạc Liêu', 'BL', 1),
(6, 230, 'Bắc Kạn', 'BK', 1),
(7, 230, 'Bắc Giang', 'BG', 1),
(8, 230, 'Bắc Ninh', 'BN', 1),
(9, 230, 'Bến Tre', 'BT', 1),
(10, 230, 'Bình Dương', 'BD', 1),
(11, 230, 'Bình Định', 'BĐ', 1),
(12, 230, 'Bình Phước', 'BP', 1),
(13, 230, 'Bình Thuận', 'BT', 1),
(14, 230, 'Cà Mau', 'CM', 1),
(15, 230, 'Cao Bằng', 'CB', 1),
(16, 230, 'Cần Thơ', 'CT', 1),
(17, 230, 'Đà Nẵng', 'DN', 1),
(18, 230, 'Đắk Lắk', 'DL', 1),
(19, 230, 'Đắk Nông', 'DKN', 1),
(20, 230, 'Điện Biên', 'DB', 1),
(21, 230, 'Đồng Nai', 'DNN', 1),
(22, 230, 'Đồng Tháp', 'DT', 1),
(23, 230, 'Gia Lai', 'GL', 1),
(24, 230, 'Hà Giang', 'HG', 1),
(25, 230, 'Hà Nam', 'HNN', 1),
(26, 230, 'Hà Tĩnh', 'HT', 1),
(27, 230, 'Hải Dương', 'HD', 1),
(28, 230, 'Hải Phòng', 'HP', 1),
(29, 230, 'Hòa Bình', 'HB', 1),
(30, 230, 'Hậu Giang', 'HG', 1),
(31, 230, 'Hưng Yên', 'HY', 1),
(32, 230, 'Khánh Hòa', 'KH', 1),
(33, 230, 'Kiên Giang', 'KG', 1),
(34, 230, 'Kon Tum', 'KT', 1),
(35, 230, 'Lai Châu', 'LCU', 1),
(36, 230, 'Lào Cai', 'LC', 1),
(37, 230, 'Lạng Sơn', 'LS', 1),
(38, 230, 'Lâm Đồng', 'LD', 1),
(39, 230, 'Long An', 'LA', 1),
(40, 230, 'Nam Định', 'ND', 1),
(41, 230, 'Nghệ An', 'NA', 1),
(42, 230, 'Ninh Bình', 'NB', 1),
(43, 230, 'Ninh Thuận', 'NT', 1),
(44, 230, 'Phú Thọ', 'PT', 1),
(45, 230, 'Phú Yên', 'PY', 1),
(46, 230, 'Quảng Bình', 'QB', 1),
(47, 230, 'Quảng Nam', 'QNN', 1),
(48, 230, 'Quảng Ngãi', 'QNG', 1),
(49, 230, 'Quảng Ninh', 'QN', 1),
(50, 230, 'Quảng Trị', 'QT', 1),
(51, 230, 'Sóc Trăng', 'ST', 1),
(52, 230, 'Sơn La', 'SL', 1),
(53, 230, 'Tây Ninh', 'TN', 1),
(54, 230, 'Thái Bình', 'TB', 1),
(55, 230, 'Thái Nguyên', 'TN', 1),
(56, 230, 'Thanh Hóa', 'TH', 1),
(57, 230, 'Thừa Thiên - Huế', 'TTH', 1),
(58, 230, 'Tiền Giang', 'TG', 1),
(59, 230, 'Trà Vinh', 'TV', 1),
(60, 230, 'Tuyên Quang', 'TQ', 1),
(61, 230, 'Vĩnh Long', 'VL', 1),
(62, 230, 'Vĩnh Phúc', 'VP', 1),
(63, 230, 'Yên Bái', 'YB', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_zone_to_geo_zone`
--

CREATE TABLE `oc_zone_to_geo_zone` (
  `zone_to_geo_zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `geo_zone_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_zone_to_geo_zone`
--

INSERT INTO `oc_zone_to_geo_zone` (`zone_to_geo_zone_id`, `country_id`, `zone_id`, `geo_zone_id`, `date_added`, `date_modified`) VALUES
(1, 222, 0, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 222, 3513, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 222, 3514, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 222, 3515, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 222, 3516, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 222, 3517, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 222, 3518, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 222, 3519, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 222, 3520, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 222, 3521, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 222, 3522, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 222, 3523, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 222, 3524, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 222, 3525, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 222, 3526, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 222, 3527, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 222, 3528, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 222, 3529, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 222, 3530, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 222, 3531, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 222, 3532, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 222, 3533, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 222, 3534, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 222, 3535, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 222, 3536, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 222, 3537, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 222, 3538, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 222, 3539, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 222, 3540, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 222, 3541, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 222, 3542, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 222, 3543, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 222, 3544, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 222, 3545, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 222, 3546, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 222, 3547, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 222, 3548, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 222, 3549, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 222, 3550, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 222, 3551, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 222, 3552, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 222, 3553, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 222, 3554, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 222, 3555, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 222, 3556, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 222, 3557, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 222, 3558, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 222, 3559, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 222, 3560, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 222, 3561, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 222, 3562, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 222, 3563, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 222, 3564, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 222, 3565, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 222, 3566, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 222, 3567, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 222, 3568, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 222, 3569, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 222, 3570, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 222, 3571, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 222, 3572, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 222, 3573, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 222, 3574, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 222, 3575, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 222, 3576, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 222, 3577, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 222, 3578, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 222, 3579, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 222, 3580, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 222, 3581, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 222, 3582, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 222, 3583, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 222, 3584, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 222, 3585, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 222, 3586, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 222, 3587, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 222, 3588, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 222, 3589, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 222, 3590, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 222, 3591, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 222, 3592, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 222, 3593, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 222, 3594, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 222, 3595, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 222, 3596, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 222, 3597, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 222, 3598, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 222, 3599, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 222, 3600, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 222, 3601, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 222, 3602, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 222, 3603, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 222, 3604, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 222, 3605, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 222, 3606, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 222, 3607, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 222, 3608, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 222, 3609, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 222, 3610, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 222, 3611, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 222, 3612, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 222, 3949, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 222, 3950, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 222, 3951, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 222, 3952, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 222, 3953, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 222, 3954, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 222, 3955, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 222, 3972, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `oc_timezones`
--

CREATE TABLE `oc_timezones` (
  `id` int(11) NOT NULL,
  `value` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oc_timezones`
--

INSERT INTO `oc_timezones` (`id`, `value`, `label`) VALUES
(1, '-12:00', '(GMT -12:00) Eniwetok, Kwajalein'),
(2, '-11:00', '(GMT -11:00) Midway Island, Samoa'),
(3, '-10:00', '(GMT -10:00) Hawaii'),
(4, '-09:50', '(GMT -9:30) Taiohae'),
(5, '-09:00', '(GMT -9:00) Alaska'),
(6, '-08:00', '(GMT -8:00) Pacific Time (US &amp; Canada)'),
(7, '-07:00', '(GMT -7:00) Mountain Time (US &amp; Canada)'),
(8, '-06:00', '(GMT -6:00) Central Time (US &amp; Canada), Mexico City'),
(9, '-05:00', '(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima'),
(10, '-04:50', '(GMT -4:30) Caracas'),
(11, '-04:00', '(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz'),
(12, '-03:50', '(GMT -3:30) Newfoundland'),
(13, '-03:00', '(GMT -3:00) Brazil, Buenos Aires, Georgetown'),
(14, '-02:00', '(GMT -2:00) Mid-Atlantic'),
(15, '-01:00', '(GMT -1:00) Azores, Cape Verde Islands'),
(16, '+00:00', '(GMT) Western Europe Time, London, Lisbon, Casablanca'),
(17, '+01:00', '(GMT +1:00) Brussels, Copenhagen, Madrid, Paris'),
(18, '+02:00', '(GMT +2:00) Kaliningrad, South Africa'),
(19, '+03:00', '(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg'),
(20, '+03:50', '(GMT +3:30) Tehran'),
(21, '+04:00', '(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi'),
(22, '+04:50', '(GMT +4:30) Kabul'),
(23, '+05:00', '(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent'),
(24, '+05:50', '(GMT +5:30) Bombay, Calcutta, Madras, New Delhi'),
(25, '+05:75', '(GMT +5:45) Kathmandu, Pokhar'),
(26, '+06:00', '(GMT +6:00) Almaty, Dhaka, Colombo'),
(27, '+06:50', '(GMT +6:30) Yangon, Mandalay'),
(28, '+07:00', '(GMT +7:00) Bangkok, Hanoi, Jakarta'),
(29, '+08:00', '(GMT +8:00) Beijing, Perth, Singapore, Hong Kong'),
(30, '+08:75', '(GMT +8:45) Eucla'),
(31, '+09:00', '(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk'),
(32, '+09:50', '(GMT +9:30) Adelaide, Darwin'),
(33, '+10:00', '(GMT +10:00) Eastern Australia, Guam, Vladivostok'),
(34, '+10:50', '(GMT +10:30) Lord Howe Island'),
(35, '+11:00', '(GMT +11:00) Magadan, Solomon Islands, New Caledonia'),
(36, '+11:50', '(GMT +11:30) Norfolk Island'),
(37, '+12:00', '(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka'),
(38, '+12:75', '(GMT +12:45) Chatham Islands'),
(39, '+13:00', '(GMT +13:00) Apia, Nukualofa'),
(40, '+14:00', '(GMT +14:00) Line Islands, Tokelau');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `oc_address`
--
ALTER TABLE `oc_address`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `oc_api`
--
ALTER TABLE `oc_api`
  ADD PRIMARY KEY (`api_id`);

--
-- Indexes for table `oc_api_ip`
--
ALTER TABLE `oc_api_ip`
  ADD PRIMARY KEY (`api_ip_id`);

--
-- Indexes for table `oc_api_session`
--
ALTER TABLE `oc_api_session`
  ADD PRIMARY KEY (`api_session_id`);

--
-- Indexes for table `oc_attribute`
--
ALTER TABLE `oc_attribute`
  ADD PRIMARY KEY (`attribute_id`);

--
-- Indexes for table `oc_attribute_description`
--
ALTER TABLE `oc_attribute_description`
  ADD PRIMARY KEY (`attribute_id`,`language_id`);

--
-- Indexes for table `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  ADD PRIMARY KEY (`attribute_group_id`);

--
-- Indexes for table `oc_attribute_group_description`
--
ALTER TABLE `oc_attribute_group_description`
  ADD PRIMARY KEY (`attribute_group_id`,`language_id`);

--
-- Indexes for table `oc_banner`
--
ALTER TABLE `oc_banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  ADD PRIMARY KEY (`banner_image_id`);

--
-- Indexes for table `oc_cart`
--
ALTER TABLE `oc_cart`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `cart_id` (`api_id`,`customer_id`,`session_id`,`product_id`,`recurring_id`);

--
-- Indexes for table `oc_category`
--
ALTER TABLE `oc_category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `oc_category_description`
--
ALTER TABLE `oc_category_description`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_category_filter`
--
ALTER TABLE `oc_category_filter`
  ADD PRIMARY KEY (`category_id`,`filter_id`);

--
-- Indexes for table `oc_category_path`
--
ALTER TABLE `oc_category_path`
  ADD PRIMARY KEY (`category_id`,`path_id`);

--
-- Indexes for table `oc_category_to_layout`
--
ALTER TABLE `oc_category_to_layout`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Indexes for table `oc_category_to_store`
--
ALTER TABLE `oc_category_to_store`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Indexes for table `oc_collection`
--
ALTER TABLE `oc_collection`
  ADD PRIMARY KEY (`collection_id`);

--
-- Indexes for table `oc_collection_description`
--
ALTER TABLE `oc_collection_description`
  ADD PRIMARY KEY (`collection_id`);

--
-- Indexes for table `oc_country`
--
ALTER TABLE `oc_country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `oc_coupon`
--
ALTER TABLE `oc_coupon`
  ADD PRIMARY KEY (`coupon_id`);

--
-- Indexes for table `oc_coupon_category`
--
ALTER TABLE `oc_coupon_category`
  ADD PRIMARY KEY (`coupon_id`,`category_id`);

--
-- Indexes for table `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  ADD PRIMARY KEY (`coupon_history_id`);

--
-- Indexes for table `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  ADD PRIMARY KEY (`coupon_product_id`);

--
-- Indexes for table `oc_currency`
--
ALTER TABLE `oc_currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Indexes for table `oc_customer`
--
ALTER TABLE `oc_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  ADD PRIMARY KEY (`customer_activity_id`);

--
-- Indexes for table `oc_customer_affiliate`
--
ALTER TABLE `oc_customer_affiliate`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `oc_customer_approval`
--
ALTER TABLE `oc_customer_approval`
  ADD PRIMARY KEY (`customer_approval_id`);

--
-- Indexes for table `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  ADD PRIMARY KEY (`customer_group_id`);

--
-- Indexes for table `oc_customer_group_description`
--
ALTER TABLE `oc_customer_group_description`
  ADD PRIMARY KEY (`customer_group_id`,`language_id`);

--
-- Indexes for table `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  ADD PRIMARY KEY (`customer_history_id`);

--
-- Indexes for table `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  ADD PRIMARY KEY (`customer_ip_id`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  ADD PRIMARY KEY (`customer_login_id`),
  ADD KEY `email` (`email`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `oc_customer_online`
--
ALTER TABLE `oc_customer_online`
  ADD PRIMARY KEY (`ip`);

--
-- Indexes for table `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  ADD PRIMARY KEY (`customer_reward_id`);

--
-- Indexes for table `oc_customer_search`
--
ALTER TABLE `oc_customer_search`
  ADD PRIMARY KEY (`customer_search_id`);

--
-- Indexes for table `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  ADD PRIMARY KEY (`customer_transaction_id`);

--
-- Indexes for table `oc_customer_wishlist`
--
ALTER TABLE `oc_customer_wishlist`
  ADD PRIMARY KEY (`customer_id`,`product_id`);

--
-- Indexes for table `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  ADD PRIMARY KEY (`custom_field_id`);

--
-- Indexes for table `oc_custom_field_customer_group`
--
ALTER TABLE `oc_custom_field_customer_group`
  ADD PRIMARY KEY (`custom_field_id`,`customer_group_id`);

--
-- Indexes for table `oc_custom_field_description`
--
ALTER TABLE `oc_custom_field_description`
  ADD PRIMARY KEY (`custom_field_id`,`language_id`);

--
-- Indexes for table `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  ADD PRIMARY KEY (`custom_field_value_id`);

--
-- Indexes for table `oc_custom_field_value_description`
--
ALTER TABLE `oc_custom_field_value_description`
  ADD PRIMARY KEY (`custom_field_value_id`,`language_id`);

--
-- Indexes for table `oc_download`
--
ALTER TABLE `oc_download`
  ADD PRIMARY KEY (`download_id`);

--
-- Indexes for table `oc_download_description`
--
ALTER TABLE `oc_download_description`
  ADD PRIMARY KEY (`download_id`,`language_id`);

--
-- Indexes for table `oc_event`
--
ALTER TABLE `oc_event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `oc_extension`
--
ALTER TABLE `oc_extension`
  ADD PRIMARY KEY (`extension_id`);

--
-- Indexes for table `oc_extension_install`
--
ALTER TABLE `oc_extension_install`
  ADD PRIMARY KEY (`extension_install_id`);

--
-- Indexes for table `oc_extension_path`
--
ALTER TABLE `oc_extension_path`
  ADD PRIMARY KEY (`extension_path_id`);

--
-- Indexes for table `oc_filter`
--
ALTER TABLE `oc_filter`
  ADD PRIMARY KEY (`filter_id`);

--
-- Indexes for table `oc_filter_description`
--
ALTER TABLE `oc_filter_description`
  ADD PRIMARY KEY (`filter_id`,`language_id`);

--
-- Indexes for table `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  ADD PRIMARY KEY (`filter_group_id`);

--
-- Indexes for table `oc_filter_group_description`
--
ALTER TABLE `oc_filter_group_description`
  ADD PRIMARY KEY (`filter_group_id`,`language_id`);

--
-- Indexes for table `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  ADD PRIMARY KEY (`geo_zone_id`);

--
-- Indexes for table `oc_information`
--
ALTER TABLE `oc_information`
  ADD PRIMARY KEY (`information_id`);

--
-- Indexes for table `oc_information_description`
--
ALTER TABLE `oc_information_description`
  ADD PRIMARY KEY (`information_id`,`language_id`);

--
-- Indexes for table `oc_information_to_layout`
--
ALTER TABLE `oc_information_to_layout`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Indexes for table `oc_information_to_store`
--
ALTER TABLE `oc_information_to_store`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Indexes for table `oc_language`
--
ALTER TABLE `oc_language`
  ADD PRIMARY KEY (`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_layout`
--
ALTER TABLE `oc_layout`
  ADD PRIMARY KEY (`layout_id`);

--
-- Indexes for table `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  ADD PRIMARY KEY (`layout_module_id`);

--
-- Indexes for table `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  ADD PRIMARY KEY (`layout_route_id`);

--
-- Indexes for table `oc_length_class`
--
ALTER TABLE `oc_length_class`
  ADD PRIMARY KEY (`length_class_id`);

--
-- Indexes for table `oc_length_class_description`
--
ALTER TABLE `oc_length_class_description`
  ADD PRIMARY KEY (`length_class_id`,`language_id`);

--
-- Indexes for table `oc_location`
--
ALTER TABLE `oc_location`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  ADD PRIMARY KEY (`manufacturer_id`);

--
-- Indexes for table `oc_manufacturer_to_store`
--
ALTER TABLE `oc_manufacturer_to_store`
  ADD PRIMARY KEY (`manufacturer_id`,`store_id`);

--
-- Indexes for table `oc_marketing`
--
ALTER TABLE `oc_marketing`
  ADD PRIMARY KEY (`marketing_id`);

--
-- Indexes for table `oc_modification`
--
ALTER TABLE `oc_modification`
  ADD PRIMARY KEY (`modification_id`);

--
-- Indexes for table `oc_module`
--
ALTER TABLE `oc_module`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `oc_novaon_group_menu`
--
ALTER TABLE `oc_novaon_group_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_novaon_group_menu_description`
--
ALTER TABLE `oc_novaon_group_menu_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_novaon_menu_item`
--
ALTER TABLE `oc_novaon_menu_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_novaon_menu_item_description`
--
ALTER TABLE `oc_novaon_menu_item_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_novaon_relation_table`
--
ALTER TABLE `oc_novaon_relation_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_novaon_type`
--
ALTER TABLE `oc_novaon_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_novaon_type_description`
--
ALTER TABLE `oc_novaon_type_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_option`
--
ALTER TABLE `oc_option`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `oc_option_description`
--
ALTER TABLE `oc_option_description`
  ADD PRIMARY KEY (`option_id`,`language_id`);

--
-- Indexes for table `oc_option_value`
--
ALTER TABLE `oc_option_value`
  ADD PRIMARY KEY (`option_value_id`);

--
-- Indexes for table `oc_option_value_description`
--
ALTER TABLE `oc_option_value_description`
  ADD PRIMARY KEY (`option_value_id`,`language_id`);

--
-- Indexes for table `oc_order`
--
ALTER TABLE `oc_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `oc_order_history`
--
ALTER TABLE `oc_order_history`
  ADD PRIMARY KEY (`order_history_id`);

--
-- Indexes for table `oc_order_option`
--
ALTER TABLE `oc_order_option`
  ADD PRIMARY KEY (`order_option_id`);

--
-- Indexes for table `oc_order_product`
--
ALTER TABLE `oc_order_product`
  ADD PRIMARY KEY (`order_product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  ADD PRIMARY KEY (`order_recurring_id`);

--
-- Indexes for table `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  ADD PRIMARY KEY (`order_recurring_transaction_id`);

--
-- Indexes for table `oc_order_shipment`
--
ALTER TABLE `oc_order_shipment`
  ADD PRIMARY KEY (`order_shipment_id`);

--
-- Indexes for table `oc_order_status`
--
ALTER TABLE `oc_order_status`
  ADD PRIMARY KEY (`order_status_id`,`language_id`);

--
-- Indexes for table `oc_order_tag`
--
ALTER TABLE `oc_order_tag`
  ADD PRIMARY KEY (`order_tag_id`);

--
-- Indexes for table `oc_order_total`
--
ALTER TABLE `oc_order_total`
  ADD PRIMARY KEY (`order_total_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  ADD PRIMARY KEY (`order_voucher_id`);

--
-- Indexes for table `oc_page_contents`
--
ALTER TABLE `oc_page_contents`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `oc_product`
--
ALTER TABLE `oc_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `oc_product_attribute`
--
ALTER TABLE `oc_product_attribute`
  ADD PRIMARY KEY (`product_id`,`attribute_id`,`language_id`);

--
-- Indexes for table `oc_product_collection`
--
ALTER TABLE `oc_product_collection`
  ADD PRIMARY KEY (`product_collection_id`);

--
-- Indexes for table `oc_product_description`
--
ALTER TABLE `oc_product_description`
  ADD PRIMARY KEY (`product_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  ADD PRIMARY KEY (`product_discount_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_filter`
--
ALTER TABLE `oc_product_filter`
  ADD PRIMARY KEY (`product_id`,`filter_id`);

--
-- Indexes for table `oc_product_image`
--
ALTER TABLE `oc_product_image`
  ADD PRIMARY KEY (`product_image_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_option`
--
ALTER TABLE `oc_product_option`
  ADD PRIMARY KEY (`product_option_id`);

--
-- Indexes for table `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  ADD PRIMARY KEY (`product_option_value_id`);

--
-- Indexes for table `oc_product_recurring`
--
ALTER TABLE `oc_product_recurring`
  ADD PRIMARY KEY (`product_id`,`recurring_id`,`customer_group_id`);

--
-- Indexes for table `oc_product_related`
--
ALTER TABLE `oc_product_related`
  ADD PRIMARY KEY (`product_id`,`related_id`);

--
-- Indexes for table `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  ADD PRIMARY KEY (`product_reward_id`);

--
-- Indexes for table `oc_product_special`
--
ALTER TABLE `oc_product_special`
  ADD PRIMARY KEY (`product_special_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_tag`
--
ALTER TABLE `oc_product_tag`
  ADD PRIMARY KEY (`product_tag_id`);

--
-- Indexes for table `oc_product_to_category`
--
ALTER TABLE `oc_product_to_category`
  ADD PRIMARY KEY (`product_id`,`category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `oc_product_to_download`
--
ALTER TABLE `oc_product_to_download`
  ADD PRIMARY KEY (`product_id`,`download_id`);

--
-- Indexes for table `oc_product_to_layout`
--
ALTER TABLE `oc_product_to_layout`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Indexes for table `oc_product_to_store`
--
ALTER TABLE `oc_product_to_store`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Indexes for table `oc_product_version`
--
ALTER TABLE `oc_product_version`
  ADD PRIMARY KEY (`product_version_id`);

--
-- Indexes for table `oc_recurring`
--
ALTER TABLE `oc_recurring`
  ADD PRIMARY KEY (`recurring_id`);

--
-- Indexes for table `oc_recurring_description`
--
ALTER TABLE `oc_recurring_description`
  ADD PRIMARY KEY (`recurring_id`,`language_id`);

--
-- Indexes for table `oc_report`
--
ALTER TABLE `oc_report`
  ADD PRIMARY KEY (`report_time`,`type`);

--
-- Indexes for table `oc_return`
--
ALTER TABLE `oc_return`
  ADD PRIMARY KEY (`return_id`);

--
-- Indexes for table `oc_return_history`
--
ALTER TABLE `oc_return_history`
  ADD PRIMARY KEY (`return_history_id`);

--
-- Indexes for table `oc_review`
--
ALTER TABLE `oc_review`
  ADD PRIMARY KEY (`review_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_seo_url`
--
ALTER TABLE `oc_seo_url`
  ADD PRIMARY KEY (`seo_url_id`),
  ADD KEY `query` (`query`),
  ADD KEY `keyword` (`keyword`);

--
-- Indexes for table `oc_session`
--
ALTER TABLE `oc_session`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `oc_setting`
--
ALTER TABLE `oc_setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `oc_shipping_courier`
--
ALTER TABLE `oc_shipping_courier`
  ADD PRIMARY KEY (`shipping_courier_id`);

--
-- Indexes for table `oc_statistics`
--
ALTER TABLE `oc_statistics`
  ADD PRIMARY KEY (`statistics_id`);

--
-- Indexes for table `oc_store`
--
ALTER TABLE `oc_store`
  ADD PRIMARY KEY (`store_id`);

--
-- Indexes for table `oc_tag`
--
ALTER TABLE `oc_tag`
  ADD PRIMARY KEY (`tag_id`);

--
-- Indexes for table `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  ADD PRIMARY KEY (`tax_class_id`);

--
-- Indexes for table `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  ADD PRIMARY KEY (`tax_rate_id`);

--
-- Indexes for table `oc_tax_rate_to_customer_group`
--
ALTER TABLE `oc_tax_rate_to_customer_group`
  ADD PRIMARY KEY (`tax_rate_id`,`customer_group_id`);

--
-- Indexes for table `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  ADD PRIMARY KEY (`tax_rule_id`);

--
-- Indexes for table `oc_theme`
--
ALTER TABLE `oc_theme`
  ADD PRIMARY KEY (`theme_id`);

--
-- Indexes for table `oc_theme_builder_config`
--
ALTER TABLE `oc_theme_builder_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_translation`
--
ALTER TABLE `oc_translation`
  ADD PRIMARY KEY (`translation_id`);

--
-- Indexes for table `oc_upload`
--
ALTER TABLE `oc_upload`
  ADD PRIMARY KEY (`upload_id`);

--
-- Indexes for table `oc_user`
--
ALTER TABLE `oc_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `oc_user_group`
--
ALTER TABLE `oc_user_group`
  ADD PRIMARY KEY (`user_group_id`);

--
-- Indexes for table `oc_voucher`
--
ALTER TABLE `oc_voucher`
  ADD PRIMARY KEY (`voucher_id`);

--
-- Indexes for table `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  ADD PRIMARY KEY (`voucher_history_id`);

--
-- Indexes for table `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  ADD PRIMARY KEY (`voucher_theme_id`);

--
-- Indexes for table `oc_voucher_theme_description`
--
ALTER TABLE `oc_voucher_theme_description`
  ADD PRIMARY KEY (`voucher_theme_id`,`language_id`);

--
-- Indexes for table `oc_warehouse`
--
ALTER TABLE `oc_warehouse`
  ADD PRIMARY KEY (`warehouse_id`);

--
-- Indexes for table `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  ADD PRIMARY KEY (`weight_class_id`);

--
-- Indexes for table `oc_weight_class_description`
--
ALTER TABLE `oc_weight_class_description`
  ADD PRIMARY KEY (`weight_class_id`,`language_id`);

--
-- Indexes for table `oc_zone`
--
ALTER TABLE `oc_zone`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  ADD PRIMARY KEY (`zone_to_geo_zone_id`);

--
-- Indexes for table `oc_timezones`
--
ALTER TABLE `oc_timezones`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `oc_address`
--
ALTER TABLE `oc_address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_api`
--
ALTER TABLE `oc_api`
  MODIFY `api_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_api_ip`
--
ALTER TABLE `oc_api_ip`
  MODIFY `api_ip_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_api_session`
--
ALTER TABLE `oc_api_session`
  MODIFY `api_session_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_attribute`
--
ALTER TABLE `oc_attribute`
  MODIFY `attribute_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  MODIFY `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_banner`
--
ALTER TABLE `oc_banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  MODIFY `banner_image_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_cart`
--
ALTER TABLE `oc_cart`
  MODIFY `cart_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_category`
--
ALTER TABLE `oc_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `oc_collection`
--
ALTER TABLE `oc_collection`
  MODIFY `collection_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `oc_country`
--
ALTER TABLE `oc_country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;
--
-- AUTO_INCREMENT for table `oc_coupon`
--
ALTER TABLE `oc_coupon`
  MODIFY `coupon_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  MODIFY `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  MODIFY `coupon_product_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_currency`
--
ALTER TABLE `oc_currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `oc_customer`
--
ALTER TABLE `oc_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  MODIFY `customer_activity_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_approval`
--
ALTER TABLE `oc_customer_approval`
  MODIFY `customer_approval_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  MODIFY `customer_group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  MODIFY `customer_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  MODIFY `customer_ip_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  MODIFY `customer_login_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  MODIFY `customer_reward_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_search`
--
ALTER TABLE `oc_customer_search`
  MODIFY `customer_search_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  MODIFY `customer_transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  MODIFY `custom_field_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  MODIFY `custom_field_value_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_download`
--
ALTER TABLE `oc_download`
  MODIFY `download_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_event`
--
ALTER TABLE `oc_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `oc_extension`
--
ALTER TABLE `oc_extension`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=183;
--
-- AUTO_INCREMENT for table `oc_extension_install`
--
ALTER TABLE `oc_extension_install`
  MODIFY `extension_install_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_extension_path`
--
ALTER TABLE `oc_extension_path`
  MODIFY `extension_path_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_filter`
--
ALTER TABLE `oc_filter`
  MODIFY `filter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  MODIFY `filter_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  MODIFY `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `oc_information`
--
ALTER TABLE `oc_information`
  MODIFY `information_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_language`
--
ALTER TABLE `oc_language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oc_layout`
--
ALTER TABLE `oc_layout`
  MODIFY `layout_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  MODIFY `layout_module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  MODIFY `layout_route_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `oc_length_class`
--
ALTER TABLE `oc_length_class`
  MODIFY `length_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `oc_location`
--
ALTER TABLE `oc_location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  MODIFY `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `oc_marketing`
--
ALTER TABLE `oc_marketing`
  MODIFY `marketing_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_modification`
--
ALTER TABLE `oc_modification`
  MODIFY `modification_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_module`
--
ALTER TABLE `oc_module`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `oc_novaon_group_menu`
--
ALTER TABLE `oc_novaon_group_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `oc_novaon_group_menu_description`
--
ALTER TABLE `oc_novaon_group_menu_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `oc_novaon_menu_item`
--
ALTER TABLE `oc_novaon_menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `oc_novaon_menu_item_description`
--
ALTER TABLE `oc_novaon_menu_item_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `oc_novaon_relation_table`
--
ALTER TABLE `oc_novaon_relation_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `oc_novaon_type`
--
ALTER TABLE `oc_novaon_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `oc_novaon_type_description`
--
ALTER TABLE `oc_novaon_type_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `oc_option`
--
ALTER TABLE `oc_option`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `oc_option_value`
--
ALTER TABLE `oc_option_value`
  MODIFY `option_value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `oc_order`
--
ALTER TABLE `oc_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_history`
--
ALTER TABLE `oc_order_history`
  MODIFY `order_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_option`
--
ALTER TABLE `oc_order_option`
  MODIFY `order_option_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_product`
--
ALTER TABLE `oc_order_product`
  MODIFY `order_product_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  MODIFY `order_recurring_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  MODIFY `order_recurring_transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_shipment`
--
ALTER TABLE `oc_order_shipment`
  MODIFY `order_shipment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_status`
--
ALTER TABLE `oc_order_status`
  MODIFY `order_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `oc_order_tag`
--
ALTER TABLE `oc_order_tag`
  MODIFY `order_tag_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_total`
--
ALTER TABLE `oc_order_total`
  MODIFY `order_total_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  MODIFY `order_voucher_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_page_contents`
--
ALTER TABLE `oc_page_contents`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_product`
--
ALTER TABLE `oc_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;
--
-- AUTO_INCREMENT for table `oc_product_collection`
--
ALTER TABLE `oc_product_collection`
  MODIFY `product_collection_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;
--
-- AUTO_INCREMENT for table `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  MODIFY `product_discount_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_product_image`
--
ALTER TABLE `oc_product_image`
  MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;
--
-- AUTO_INCREMENT for table `oc_product_option`
--
ALTER TABLE `oc_product_option`
  MODIFY `product_option_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  MODIFY `product_option_value_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  MODIFY `product_reward_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_product_special`
--
ALTER TABLE `oc_product_special`
  MODIFY `product_special_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_product_tag`
--
ALTER TABLE `oc_product_tag`
  MODIFY `product_tag_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_product_version`
--
ALTER TABLE `oc_product_version`
  MODIFY `product_version_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_recurring`
--
ALTER TABLE `oc_recurring`
  MODIFY `recurring_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_return`
--
ALTER TABLE `oc_return`
  MODIFY `return_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_return_history`
--
ALTER TABLE `oc_return_history`
  MODIFY `return_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_review`
--
ALTER TABLE `oc_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_seo_url`
--
ALTER TABLE `oc_seo_url`
  MODIFY `seo_url_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2204;
--
-- AUTO_INCREMENT for table `oc_setting`
--
ALTER TABLE `oc_setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5516;
--
-- AUTO_INCREMENT for table `oc_statistics`
--
ALTER TABLE `oc_statistics`
  MODIFY `statistics_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `oc_store`
--
ALTER TABLE `oc_store`
  MODIFY `store_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_tag`
--
ALTER TABLE `oc_tag`
  MODIFY `tag_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  MODIFY `tax_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  MODIFY `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  MODIFY `tax_rule_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;
--
-- AUTO_INCREMENT for table `oc_theme`
--
ALTER TABLE `oc_theme`
  MODIFY `theme_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_theme_builder_config`
--
ALTER TABLE `oc_theme_builder_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=376;
--
-- AUTO_INCREMENT for table `oc_translation`
--
ALTER TABLE `oc_translation`
  MODIFY `translation_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_upload`
--
ALTER TABLE `oc_upload`
  MODIFY `upload_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_user`
--
ALTER TABLE `oc_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_user_group`
--
ALTER TABLE `oc_user_group`
  MODIFY `user_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `oc_voucher`
--
ALTER TABLE `oc_voucher`
  MODIFY `voucher_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  MODIFY `voucher_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  MODIFY `voucher_theme_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `oc_warehouse`
--
ALTER TABLE `oc_warehouse`
  MODIFY `warehouse_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1138;
--
-- AUTO_INCREMENT for table `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  MODIFY `weight_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `oc_zone`
--
ALTER TABLE `oc_zone`
  MODIFY `zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  MODIFY `zone_to_geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT for table `oc_timezones`
--
ALTER TABLE `oc_timezones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

-- -----------------------------------------------------------
--
-- v1.4
--
-- -----------------------------------------------------------

--
-- more event for order
--
INSERT INTO `oc_event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES
('edit order 2', 'admin/model/sale/order/editOrder/after', 'event/statistics/editOrderDetail', 1, 0),
('change status order 2', 'admin/model/sale/order/editOrder/before', 'event/statistics/updateStatusOrderDetail', 1, 0);


-- -----------------------------------------------------------
--
-- v1.5
--
-- -----------------------------------------------------------

--
-- Create table `oc_novaon_api_credentials`
--
CREATE TABLE `oc_novaon_api_credentials` (
  `id` int(11) NOT NULL,
  `consumer_key` varchar(255) NOT NULL,
  `consumer_secret` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `oc_novaon_api_credentials` (`id`, `consumer_key`, `consumer_secret`, `create_date`, `status`) VALUES
(1, 'GHNHTYDSTw567', 'GHNDRTUYUKMVFYJKNBGFFGFHJJ7886GH', '2019-06-03 01:15:59', 1);

ALTER TABLE `oc_novaon_api_credentials`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `oc_novaon_api_credentials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- enable extension module NovaonApi
--
INSERT INTO `oc_extension` (`type`, `code`) VALUES
('module', 'NovaonApi');

--
-- google shopping meta config
--
INSERT INTO `oc_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'config', 'config_gg_shopping', '', 0);

--
-- add column previous_order_status_id to oc_order
--
ALTER TABLE `oc_order` ADD `previous_order_status_id` VARCHAR(50) NULL AFTER `order_status_id`;


--
-- v1.5.2 Smart loading demo data base on theme group (theme type)
-- Note: following content is copied from file "library/theme_config/demo_data/tech.sql" for default
--
-- 2020/04: More support loading demo data from special theme. If no special theme defined, use theme group as above
-- Note: following content may be copied from file "library/theme_config/demo_data/special_shop/xxx.sql" if special file defined

-- 2020/07/30: change theme demo to adg_topal_55.
-- Note: following content is copied from file "library/theme_config/demo_data/special_shop/adg_topal_55.sql"
--

-- IMPORTANT: Now move to end of file. Reason:
-- + we have some migrate sql lines below this
-- + the demo data should be latest version

-- -----------------------------------------------------------
-- --------end v1.5.2 demo data for theme---------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.5.2 tool theme----------------------------------
-- -----------------------------------------------------------

ALTER TABLE `oc_user` ADD `theme_develop` tinyint(1) NULL NULL DEFAULT '0' AFTER `permission`;

-- -----------------------------------------------------------
-- --------end v1.5.2 tool theme------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- v1.5.2 Remake menu ----------------------------------------
-- -----------------------------------------------------------

--
-- create table oc_menu
--

CREATE TABLE `oc_menu` (
  `menu_id` INT(11) NOT NULL AUTO_INCREMENT,
  `parent_id` INT(11) NULL,
  `menu_type_id` TINYINT(2) NULL,
  `target_value` VARCHAR(256) NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- create table oc_menu_description
--

CREATE TABLE `oc_menu_description` (
  `menu_id` INT(11) NOT NULL,
  `name` VARCHAR(256) NOT NULL,
  `language_id` INT(11) NOT NULL,
  PRIMARY KEY( `menu_id`, `language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- create table oc_menu_type
--

CREATE TABLE `oc_menu_type` (
  `menu_type_id` INT(11) NOT NULL AUTO_INCREMENT,
  `value` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`menu_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- create table oc_menu_type_description
--

CREATE TABLE `oc_menu_type_description` (
  `menu_type_id` INT(11) NOT NULL,
  `language_id` INT(11) NOT NULL,
  `description` VARCHAR(256) NOT NULL,
  PRIMARY KEY( `menu_type_id`, `language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- insert menu type db
--
INSERT INTO `oc_menu_type` (`menu_type_id`, `value`) VALUES
('1', 'category'),
('2', 'collection'),
('3', 'product'),
('4', 'url'),
('5', 'built-in page'),
('6', 'blog');

--
-- insert oc_menu_type_description db
--
INSERT INTO `oc_menu_type_description` (`menu_type_id`, `language_id`, `description`) VALUES
('1', '1', 'Category'),
('1', '2', 'Loại sản phẩm'),
('2', '1', 'Collection'),
('2', '2', 'Bộ sưu tập'),
('3', '1', 'Product'),
('3', '2', 'Sản phẩm'),
('4', '1', 'Url'),
('4', '2', 'Đường dẫn'),
('5', '1', 'Built-in Page'),
('5', '2', 'Trang mặc định'),
('6', '1', 'Blog'),
('6', '2', 'Bài viết');

--
-- Dumping data for table `oc_menu`
--
INSERT INTO `oc_menu` (`menu_id`, `parent_id`, `menu_type_id`, `target_value`) VALUES
(1, NULL, NULL, NULL),
(8, 1, 4, '/gioi-thieu?about-us'),
(10, 1, 5, '?route=common/shop'),
(11, 1, 4, '/bai-viet?type=du-an'),
(12, 1, 4, '/bai-viet'),
(13, 1, 5, '?route=contact/contact'),
(14, 10, 1, ''),
(15, 10, 1, ''),
(16, NULL, NULL, NULL),
(17, 16, 1, ''),
(18, 16, 1, ''),
(19, 16, 1, ''),
(20, 16, 1, ''),
(21, 16, 1, '');

--
-- Dumping data for table `oc_menu_description`
--
INSERT INTO `oc_menu_description` (`menu_id`, `name`, `language_id`) VALUES
(1, 'Menu Chính', 1),
(1, 'Menu Chính', 2),
(8, 'GIỚI THIỆU', 1),
(8, 'GIỚI THIỆU', 2),
(10, 'SẢN PHẨM', 1),
(10, 'SẢN PHẨM', 2),
(11, 'DỰ ÁN', 1),
(11, 'DỰ ÁN', 2),
(12, 'TIN TỨC', 1),
(12, 'TIN TỨC', 2),
(13, 'LIÊN HỆ', 1),
(13, 'LIÊN HỆ', 2),
(14, 'SẢN PHẨM BÁN CHẠY', 2),
(14, 'SẢN PHẨM BÁN CHẠY', 1),
(15, 'SẢN PHẨM MỚI', 2),
(15, 'SẢN PHẨM MỚI', 1),
(16, 'Điều khoản', 2),
(16, 'Điều khoản', 1),
(17, 'Hướng dẫn mua hàng', 2),
(17, 'Hướng dẫn mua hàng', 1),
(18, 'Bảo mật thông tin', 2),
(18, 'Bảo mật thông tin', 1),
(19, 'Chính sách bán hàng', 2),
(19, 'Chính sách bán hàng', 1),
(20, 'Chính sách đổi/ trả hàng', 2),
(20, 'Chính sách đổi/ trả hàng', 1),
(21, 'Chính sách bảo hành/ bảo trì', 2),
(21, 'Chính sách bảo hành/ bảo trì', 1);

-- -----------------------------------------------------------
-- --------end v1.5.2 Remake menu ----------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.5.2 default policy for gg shopping--------------
-- -----------------------------------------------------------

-- create migrate table
CREATE TABLE IF NOT EXISTS `oc_migration` (
  `migration_id` int(11) NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `run` tinyint(1) NOT NULL DEFAULT 1,
  `date_run` datetime NOT NULL,
  PRIMARY KEY (`migration_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `oc_migration` (`migration`, `run`, `date_run`) VALUES
('migration_20190718_default_policy_gg_shopping', 1, NOW());

-- default payment methods and delivery methods
INSERT INTO `oc_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'payment', 'payment_methods', '[{"payment_id":"198535319739A","name":"Thanh toán khi nhận hàng (COD)","guide":"Bạn sẽ thanh toán toàn bộ giá trị đơn hàng khi nhận hàng","status":"1"}]', 1),
(0, 'delivery', 'delivery_methods', '[{"id":"-1","name":"Giao hàng tiêu chuẩn(liên hệ)","status":"1","fee_region":"0"}]', 1);

-- default policy page and menu
INSERT IGNORE INTO `oc_page_contents` (`page_id`, `title`, `description`, `seo_title`, `seo_description`, `status`, `date_added`) VALUES
(1001, 'Hướng dẫn mua hàng', '&lt;h2&gt;Liên hệ mua sản phẩm bằng 2 cách: Gọi điện thoại tới Tổng đài &lt;b&gt;1900-6828&lt;/b&gt; hoặc lựa chọn Đại lý ở gần bạn nhất &lt;/h2&gt;', '', '', 1, '2022-02-09 09:35:07'),
(1002, 'Bảo mật thông tin', '&lt;p&gt;&lt;strong&gt;Austdoor cam kết sẽ bảo mật những thông tin mang tính riêng tư của khách hàng. Khách hàng vui lòng đọc bản “Chính sách bảo mật” dưới đây để hiểu hơn những cam kết mà Austdoor thực hiện, nhằm tôn trọng và bảo vệ quyền lợi của người truy cập.&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;1. Mục đích và phạm vi thu thập thông tin&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Để truy cập và sử dụng một số dịch vụ tại Austdoor, khách hàng có thể sẽ được yêu cầu đăng ký với Austdoor thông tin cá nhân (Email, Họ tên, Số ĐT liên lạc…) Mọi thông tin khai báo phải đảm bảo tính chính xác và hợp pháp. Austdoor không chịu mọi trách nhiệm liên quan đến pháp luật của thông tin khai báo.&lt;/p&gt;\r\n\r\n&lt;p&gt;Austdoor cũng có thể thu thập thông tin về số lần viếng thăm, bao gồm số trang khách hàng xem, số links (liên kết) khách hàng click và những thông tin khác liên quan đến việc kết nối đến Austdoor.&lt;/p&gt;\r\n\r\n&lt;p&gt;Austdoor cũng thu thập các thông tin mà ứng dụng/trình duyệt khách hàng sử dụng mỗi khi truy cập vào website.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;2. Phạm vi sử dụng thông tin&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Austdoor thu thập và sử dụng thông tin cá nhân khách hàng với mục đích phù hợp và hoàn toàn tuân thủ nội dung của “Chính sách bảo mật” này. Khi cần thiết, Austdoor có thể sử dụng những thông tin này để liên hệ trực tiếp với khách hàng dưới các hình thức như: gởi thư ngỏ, đơn đặt hàng, thư cảm ơn, sms, thông tin về kỹ thuật và bảo mật…&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;3. Thời gian lưu trữ thông tin&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Dữ liệu cá nhân của thành viên sẽ được lưu trữ cho đến khi có yêu cầu hủy bỏ hoặc tự thành viên đăng nhập và thực hiện hủy bỏ. Còn lại trong mọi trường hợp thông tin cá nhân thành viên sẽ được bảo mật trên máy chủ của Austdoor.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;4. Địa chỉ của đơn vị thu thập và quản lý thông tin cá nhân&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Công ty Cổ phần Tập đoàn Austdoor&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;ADG Tower, 37 Lê Văn Thiêm, P. Nhân Chính, Q. Thanh Xuân, Hà Nội&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;ĐT: (024) 44550088 - Fax: (04) 44550086&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;5. Phương tiện và công cụ để người dùng tiếp cận và chỉnh sửa dữ liệu cá nhân&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt; &lt;/p&gt;\r\n\r\n&lt;p&gt;Hiện website chưa triển khai chỉnh sửa thông tin cá nhân của thành viên, vì thế việc tiếp cận và chỉnh sửa dữ liệu cá nhân dựa vào yêu cầu của khách hàng bằng cách hình thức sau:&lt;/p&gt;\r\n\r\n&lt;p&gt;- Gọi điện thoại đến tổng đài chăm sóc khách hàng 1900 6828, nhân viên tổng đài sẽ hỗ trợ chỉnh sửa thay người dùng.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Để lại bình luận hoặc gửi góp ý trực tiếp từ website và ứng dụng, quản trị viên kiểm tra thông tin và xem xét nội dung bình luận phù hợp với pháp luật và chính sách của Austdoor&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;6. Cam kết bảo mật thông tin cá nhân khách hàng&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt; &lt;/p&gt;\r\n\r\n&lt;p&gt;- Thông tin cá nhân của thành viên trên website và ứng dụng được Austdoor cam kết bảo mật tuyệt đối theo chính sách bảo vệ thông tin cá nhân của công ty. Việc thu thập và sử dụng thông tin của mỗi thành viên chỉ được thực hiện khi có sự đồng ý của khách hàng đó trừ những trường hợp pháp luật có quy định khác.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Không sử dụng, không chuyển giao, cung cấp hay tiết lộ cho bên thứ 3 nào về thông tin cá nhân của thành viên khi không có sự cho phép đồng ý từ thành viên.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Trong trường hợp máy chủ lưu trữ thông tin bị hacker tấn công dẫn đến mất mát dữ liệu cá nhân thành viên, Austdoor sẽ có trách nhiệm thông báo vụ việc cho cơ quan chức năng điều tra xử lý kịp thời và thông báo cho thành viên được biết.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Ban quản lý website và ứng dụng yêu cầu các cá nhân khi đăng ký/mua hàng là thành viên, phải cung cấp đầy đủ thông tin cá nhân có liên quan như: Họ và tên, địa chỉ liên lạc, email, số chứng minh nhân dân, điện thoại, số tài khoản, số thẻ thanh toán... và chịu trách nhiệm về tính pháp lý của những thông tin trên.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Ban quản lý webtsite và ứng dụng không chịu trách nhiệm cũng như không giải quyết mọi khiếu nại có liên quan đến quyền lợi của thành viên đó nếu xét thấy tất cả thông tin cá nhân của thành viên đó cung cấp khi đăng ký ban đầu là không chính xác.&lt;/p&gt;', '', '', 1, '2022-02-09 09:35:59'),
(1003, 'Chính sách bán hàng', '&lt;p&gt;Website www.austdoor.com do công ty Cổ phần Tập đoàn Austdoor thực hiện hoạt động và vận hành nhằm giới thiệu thông tin hoạt động của doanh nghiệp, thông tin sản phẩm đến các cổ đông, nhà đầu tư, đối tác và khách hàng trong nước và quốc tế.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;1 . Nguyên tắc chung&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Thông tin, sản phẩm giới thiệu tại www.austdoor.com phải trung thực, minh bạch, đáp ứng đầy đủ các quy định của pháp luật Việt Nam.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;2. Quy định chung&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Tên Miền website www.austdoor.com do Công ty Cổ phần Tập đoàn Austdoor phát triển với tên miền giao dịch là: www.austdoor.com&lt;/p&gt;\r\n\r\n&lt;p&gt;- Đơn vị giới thiệu thông tin là Công ty Cổ phần Tập đoàn Austdoor&lt;/p&gt;\r\n\r\n&lt;p&gt;- Người tiếp nhận thông tin là các nhà đầu tư, cổ đông, đối tác và khách hàng trong và ngoài nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Nội dung bản Quy chế này tuân thủ theo các quy định hiện hành của Việt Nam. Thành viên khi tham gia website www.austdoor.com phải tự tìm hiểu trách nhiệm pháp lý của mình đối với luật pháp hiện hành của Việt Nam và cam kết thực hiện đúng những nội dung trong Quy chế này.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;3. Quy trình mua hàng (Dành cho người mua hàng các sản phẩm của AUSTDOOR)&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Bước 1:&lt;/strong&gt; Tìm sản phẩm cần mua.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Bước 2: &lt;/strong&gt;Xem giá và thông tin chi tiết sản phẩm đó, nếu quý khách đồng ý mua hàng, xin vui lòng chọn Đại lý chính hãng gần nhất của Austdoor để mua hàng.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;4. Quy định và hình thức thanh toán&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;* Thanh toán tại các Đại lý, Chi nhánh, Nhà phân phối của Công ty&lt;/p&gt;\r\n\r\n&lt;p&gt;Ngay khi Quý khách thanh toán xong, nhân viên của Đại lý, Chi nhánh và Nhà phân phối sẽ hướng dẫn các thủ tục nhận hàng.&lt;/p&gt;\r\n\r\n&lt;p&gt;* Thanh toán tại điểm giao hàng&lt;/p&gt;\r\n\r\n&lt;p&gt;- Quý khách thanh toán cho nhân viên giao nhận toàn bộ hoặc một phần (nếu đã đặt cọc) giá trị hàng hóa đã mua.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Nếu địa điểm giao hàng ngay tại nơi thanh toán, nhân viên giao hàng của Đại lý, Chi nhánh, Nhà Phân phối sẽ thu tiền khi giao hàng.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;5. Chính sách bản quyền và bảo mật&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Mọi quyền sở hữu trí tuệ (đã đăng ký hoặc chưa đăng ký), nội dung thông tin và tất cả các thiết kế, văn bản, đồ họa, phần mềm, hình ảnh, video, âm nhạc, âm thanh, biên dịch phần mềm, mã nguồn và phần mềm cơ bản đều là tài sản của Austdoor. Toàn bộ nội dung của trang web được bảo vệ bởi luật bản quyền của Việt Nam và các công ước quốc tế. Bản quyền đã được bảo lưu.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Austdoor không thu thập bất kỳ thông tin cá nhân qua www.austdoor.com.&lt;/p&gt;', '', '', 1, '2022-02-09 09:36:54'),
(1004, 'Chính sách đổi/ trả hàng', '&lt;p&gt;&lt;strong&gt;1. Quy định hàng gửi về bảo hành:&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Do đặc thù hàng hóa Công Ty chỉ đổi lại cho khách hàng những sản phẩm hoặc một bộ phận của sản phẩm bị lỗi, vì vậy những bộ phận, sản phẩm không bị lỗi Đại lý cần tháo ra (không gửi về Công Ty), sau khi Công Ty khắc phục xong thì lắp trở lại sử dụng. Ví dụ khi bảo hành hộp điều khiển Đại lý chỉ cần gửi bo mạch về Công Ty để tránh bị vỡ phần nhựa dẫn đến Công Ty phải trừ&lt;br /&gt;\r\ngiá trị sản phẩm.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;2. Quy định đổi trả hàng:&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Đại lý lập đề nghị bảo hành gửi về TTDV Austcare của Công Ty, sau khi TTDV Austcare xác nhận mới thực hiện gửi hàng hoặc theo hướng dẫn của nhân viên Công Ty.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Khi thực hiện gửi đổi hoặc trả hàng, Đại lý đính kèm phiếu giao hàng vào thùng hàng, bản sao Đề nghị nhập hàng về Công Ty (nếu có)..&lt;/p&gt;\r\n\r\n&lt;p&gt;- Dán địa chỉ người gửi, người nhận vào trước thùng hàng khi gửi.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Theo dõi và xác nhận với Công Ty để đảm bảo hàng trả về đến Công Ty.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Công Ty chỉ thực hiện nhập hàng về bảo hành, sửa chữa, nhập lại, … tại TTDV Austcare đối với các Đại lý Hà Nội hoặc tại nhà xe đối với Đại lý Tỉnh. Công Ty sẽ phản hồi về kết quả nhận hàng bảo hành trong vòng 04 giờ làm việc đối với Đại lý gửi hàng qua bến xe.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Trong vòng 04 giờ làm việc đối với các Đại lý Hà Nội hoặc 72 giờ làm việc đối với Đại lý các tỉnh sau khi Công Ty gửi trả hàng bảo hành, sửa chữa, Đại lý có trách nhiệm thông báo lại kết quả nhận hàng. Nếu Đại lý không thông báo lại thì coi như đã xác nhận đủ hàng bảo hành, sửa chữa (đối với các trường hợp gửi trả hàng qua bến xe).&lt;/p&gt;\r\n\r\n&lt;p&gt;- Các trường hợp khác ngoài quy định trên phải được sự đồng ý của Công Ty. Trên chứng từ giao hàng phải ký xác nhận, ghi rõ Họ tên, số điện thoại của bên giao và bên nhận.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;3. Quy định về bao gói đối với hàng bảo hành:&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Trường hợp Đại lý không cẩn thận trong việc bảo quản, đóng gói hàng gửi về Công Ty, để xảy ra hỏng hóc, bong tróc sơn, gẩy vỡ, … thì Công Ty sẽ không bảo hành hoặc sửa chữa có tính phí theo quy định trên&lt;/p&gt;', '', '', 1, '2022-02-09 09:37:33'),
(1005, 'Chính sách bảo hành/ bảo trì', '&lt;p&gt;&lt;strong&gt;1. THỜI HẠN BẢO HÀNH&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Mô tơ ARG, AHV, AH (không bao gồm hộp điều khiển, tay điều khiển từ xa) bảo hành 60 tháng (Chính sách trên áp dụng cho KH mua cửa cuốn từ Huế trở ra. Khu vực miền Nam khách hàng liên hệ Hotline 0923 858 858).&lt;/p&gt;\r\n\r\n&lt;p&gt;- Mô tơ AK (không bao gồm hộp điều khiển, tay điều khiển từ xa) bảo hành 24 tháng.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Ắc quy và pin tay điều khiển từ xa bảo hành 06 tháng.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Thân cửa, Hộp điều khiển, tay điều khiển từ xa và các linh kiện khác bảo hành 12 tháng.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;2. QUY ĐỊNH CHUNG VỀ BẢO HÀNH&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Toàn bộ thân cửa, hộp điều khiển, bộ tời (mô tơ), bộ lưu điện (UPS), khóa, phím bấm âm tường... của Austdoor đều có dán tem hoặc in logo Austdoor đồng thời được dán thêm tem bảo hành của Austdoor.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Khách hàng có trách nhiệm xuất trình phiếu bảo hành khi sửa chữa hoặc thay thế linh kiện trong phạm vi bảo hành.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Trường hợp có hỏng hóc, sự cố xin liên hệ với Đại lý bán hàng của Austdoor hoặc Trung tâm Dịch vụ và Chăm sóc khách hàng qua số: 1900 - 6828.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Thời hạn bảo hành: Theo đúng thời hạn của từng linh kiện đã được in trên phiếu bảo hành.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;3. SẢN PHẨM KHÔNG ĐƯỢC BẢO HÀNH TRONG NHỮNG TRƯỜNG HỢP SAU&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Thân cửa và các phụ kiện đi kèm không đồng bộ, không có tem nhãn Austdoor.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Trên thân cửa và các phụ kiện không dán tem bảo hành hoặc tem bảo hành bị rách, tẩy xóa.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Khách hàng tự ý tháo dỡ, sửa chữa trước khi nhân viên kỹ thuật của Austdoor đến bảo hành mà không được sự đồng ý của Austdoor.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Sản phẩm vận hành không đúng quy định.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Sản phẩm bị chập, cháy do nguồn điện, nước, côn trùng.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Sản phẩm bị hư hỏng do thiên tai, hỏa hoạn.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Phiếu bảo hành hết hạn, bị rách, tẩy xóa không rõ thông tin và không có dấu của Austdoor.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Phiếu bảo hành không có giá trị khi trao đổi, chuyển nhượng.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Cửa hư hỏng do lắp đặt các phụ kiện (Bộ tời, Trục, UPS) không chính hãng sẽ không được bảo hành.&lt;/p&gt;', '', '', 1, '2022-02-09 09:38:28');

INSERT IGNORE INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`, `table_name`) VALUES
(2417, 0, 1, 'page_id=1004', 'chinh-sach-doi-tra', ''),
(2416, 0, 2, 'page_id=1004', 'chinh-sach-doi-tra', ''),
(2418, 0, 2, 'page_id=1005', 'chinh-sach-bao-hanh', ''),
(2419, 0, 1, 'page_id=1005', 'chinh-sach-bao-hanh', ''),
(2410, 0, 2, 'page_id=1003', 'chinh-sach-ban-hang', ''),
(2407, 0, 1, 'page_id=1002', 'bao-mat-thong-tin', ''),
(2406, 0, 2, 'page_id=1002', 'bao-mat-thong-tin', ''),
(2402, 0, 2, 'page_id=1001', 'huong-dan-mua-hang', ''),
(2403, 0, 1, 'page_id=1001', 'huong-dan-mua-hang', ''),
(2411, 0, 1, 'page_id=1003', 'chinh-sach-ban-hang', '');

-- -----------------------------------------------------------
-- --------end v1.5.2 default policy for gg shopping----------
-- -----------------------------------------------------------


-- -----------------------------------------------------------
-- --------enable seo ----------------------------------------
-- -----------------------------------------------------------
UPDATE `oc_setting` SET `value` = '1' WHERE `code` = 'config' AND `key` = 'config_seo_url';

INSERT INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`, `table_name`) VALUES
(NULL, '0', '1', 'common/home', 'home', ''),
(NULL, '0', '2', 'common/home', 'trang-chu', ''),
(NULL, '0', '1', 'common/shop', 'products', ''),
(NULL, '0', '2', 'common/shop', 'san-pham', ''),
(NULL, '0', '1', 'contact/contact', 'contact', ''),
(NULL, '0', '2', 'contact/contact', 'lien-he', ''),
(NULL, '0', '1', 'checkout/profile', 'profile', ''),
(NULL, '0', '2', 'checkout/profile', 'tai-khoan', ''),
(NULL, '0', '1', 'account/login', 'login', ''),
(NULL, '0', '2', 'account/login', 'dang-nhap', ''),
(NULL, '0', '1', 'account/register', 'register', ''),
(NULL, '0', '2', 'account/register', 'dang-ky', ''),
(NULL, '0', '1', 'account/logout', 'logout', ''),
(NULL, '0', '2', 'account/logout', 'dang-xuat', ''),
(NULL, '0', '1', 'checkout/setting', 'setting', ''),
(NULL, '0', '2', 'checkout/setting', 'cai-dat', ''),
(NULL, '0', '1', 'checkout/my_orders', 'checkout-cart', ''),
(NULL, '0', '2', 'checkout/my_orders', 'gio-hang', ''),
(NULL, '0', '1', 'checkout/order_preview', 'payment', ''),
(NULL, '0', '2', 'checkout/order_preview', 'thanh-toan', ''),
(NULL, '0', '1', 'checkout/order_preview/orderSuccess', 'payment-success', ''),
(NULL, '0', '2', 'checkout/order_preview/orderSuccess', 'dat-hang-thanh-cong', ''),
(NULL, '0', '1', 'blog/blog', 'blog', ''),
(NULL, '0', '2', 'blog/blog', 'bai-viet', '');

-- -----------------------------------------------------------
-- --------enable seo ----------------------------------------
-- -----------------------------------------------------------


-- -----------------------------------------------------------
-- --------v1.5.3 tuning 56 key points -----------------------
-- -----------------------------------------------------------

ALTER TABLE `oc_product` ADD `common_price` decimal(15,4) DEFAULT NULL AFTER `sku`;
ALTER TABLE `oc_product` ADD `common_compare_price` decimal(15,4) DEFAULT NULL AFTER `sku`;
ALTER TABLE `oc_product` ADD `common_sku` varchar(64) DEFAULT NULL AFTER `sku`;
ALTER TABLE `oc_product` ADD `common_barcode` varchar(128) DEFAULT NULL AFTER `sku`;

-- -----------------------------------------------------------
-- --------end v1.5.3 tuning 56 key points -------------------
-- -----------------------------------------------------------


-- -----------------------------------------------------------
-- --------v1.6.1 integrate 3rd API --------------------------
-- -----------------------------------------------------------

-- add column `source` to oc_order
ALTER TABLE `oc_order`
  ADD `source` VARCHAR(50) NULL AFTER `user_id`,
  ADD `transport_name` varchar(64) DEFAULT NULL AFTER `telephone`,
  ADD `transport_order_code` varchar(64) DEFAULT NULL AFTER `telephone`,
  ADD `transport_service_name` varchar(64) DEFAULT NULL AFTER `telephone`,
  ADD `transport_money_total` VARCHAR(255) NOT NULL AFTER `transport_name`,
  ADD `transport_weight` VARCHAR(255) NOT NULL AFTER `transport_money_total`,
  ADD `transport_status` VARCHAR(255) NOT NULL AFTER `transport_weight`,
  ADD `transport_current_warehouse` VARCHAR(255) NOT NULL AFTER `transport_status`;

INSERT INTO `oc_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'config', 'config_VIETTEL_POST_token_webhook', '', 0);

-- -----------------------------------------------------------
-- --------end v1.6.1 integrate 3rd API ----------------------
-- -----------------------------------------------------------


-- -----------------------------------------------------------
-- --------v1.7.1 create report tables -----------------------
-- -----------------------------------------------------------

-- create table `oc_report_order` for report revenue, order and customer with order
CREATE TABLE IF NOT EXISTS `oc_report_order` (
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `report_date` date NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT 0,
  `total_amount` decimal(18,4) DEFAULT NULL,
  `order_status` int(11) DEFAULT 0
  -- PRIMARY KEY (`report_time`, `order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_report_product` for report product with quantity and total_amount
CREATE TABLE IF NOT EXISTS `oc_report_product` (
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `report_date` date NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_version_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(18,4) DEFAULT NULL,
  `total_amount` decimal(18,4) DEFAULT NULL
  -- PRIMARY KEY (`report_time`, `order_id`, `product_id`, `product_version_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_report_product` for report new customer
CREATE TABLE IF NOT EXISTS `oc_report_customer` (
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `report_date` date NOT NULL,
  `customer_id` int(11) DEFAULT 0
  -- PRIMARY KEY (`report_time`, `customer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_report_web_traffic` for report web traffic (then calculate conversion rate, ...)
CREATE TABLE IF NOT EXISTS `oc_report_web_traffic` (
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `report_date` date NOT NULL,
  `value` varchar(500) NOT NULL,
  `last_updated` timestamp NULL DEFAULT NULL
  -- PRIMARY KEY (`report_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- create some events to insert data to some report tables above

-- on order change event
INSERT INTO `oc_event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES
('create new order', 'admin/model/sale/order/addOrder/after', 'event/report_order/afterCreateOrder', 1, 0),
('edit order in list', 'admin/model/sale/order/updateColumnTotalOrder/after', 'event/report_order/editOrderInOrderList', 1, 0),
('change status order in order list', 'admin/model/sale/order/updateStatus/after', 'event/report_order/updateOrderStatusInOrderList', 1, 0),
('edit order in order detail', 'admin/model/sale/order/editOrder/after', 'event/report_order/editOrderInOrderDetail', 1, 0),
('create new order', 'admin/model/sale/order/addOrder/after', 'event/report_product/afterCreateOrder', 1, 0),
('edit order in list', 'admin/model/sale/order/updateProductOrder/after', 'event/report_product/editOrderInOrderList', 1, 0),
('change status order in order list', 'admin/model/sale/order/updateStatus/after', 'event/report_product/updateOrderStatusInOrderList', 1, 0),
('edit order in order detail', 'admin/model/sale/order/editOrder/after', 'event/report_product/editOrderInOrderDetail', 1, 0);

-- on customer change event
INSERT INTO `oc_event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES
('create customer', 'admin/model/customer/customer/addCustomer/after', 'event/report_customer/createCustomer', 1, 0),
('delete customer', 'admin/model/customer/customer/deleteCustomer/after', 'event/report_customer/deleteCustomer', 1, 0);

-- -----------------------------------------------------------
-- --------end v1.7.1 create report tables -------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.7.2 onboarding ---------------------------------
-- -----------------------------------------------------------

INSERT INTO `oc_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'config', 'config_onboarding_complete', '0', 0),
(0, 'config', 'config_onboarding_step_active', '', 0),
(0, 'config', 'config_onboarding_voucher', '', 0),
(0, 'config', 'config_onboarding_used_voucher', '0', 0);

-- -----------------------------------------------------------
-- --------end v1.7.2 onboarding -----------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.7.3 tuning -------------------------------------
-- -----------------------------------------------------------

-- add column `from_domain` to oc_order
ALTER TABLE `oc_order`
  ADD `from_domain` VARCHAR(500) NULL DEFAULT NULL AFTER `source`;

--
-- init first override css version
--
INSERT INTO `oc_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'config', 'builder_config_version', '1', 0);

-- -----------------------------------------------------------
-- --------end v1.7.3 tuning ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.8.1 theme api + builder + blog -----------------
-- -----------------------------------------------------------

-- create table `oc_collection_to_collection`
CREATE TABLE IF NOT EXISTS `oc_collection_to_collection` (
  `collection_to_collection_id` int(11) NOT NULL AUTO_INCREMENT,
  `collection_id` int(11) NOT NULL,
  `parent_collection_id` int(11) NOT NULL,
  `sort_order` int(11) DEFAULT '0',
  PRIMARY KEY (`collection_to_collection_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- add `type` to table `oc_collection`
ALTER TABLE `oc_collection` ADD `type` varchar(255) DEFAULT '0';

-- create table `oc_blog`
CREATE TABLE IF NOT EXISTS `oc_blog` (
  `blog_id` int(11) NOT NULL AUTO_INCREMENT,
  `author` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_publish` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`blog_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_blog`
CREATE TABLE IF NOT EXISTS `oc_blog_description` (
  `blog_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text,
  `short_content` text,
  `image` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` text,
  `alias` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`blog_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_blog_category`
CREATE TABLE IF NOT EXISTS `oc_blog_category` (
  `blog_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`blog_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table `oc_blog_category`
INSERT INTO `oc_blog_category` (`blog_category_id`, `status`, `date_added`, `date_modified`) VALUES
(1, 1, '2019-11-11 13:37:51', '2021-11-24 10:19:29'),
(5, 1, '2021-11-08 17:31:28', '2021-11-08 17:31:28'),
(3, 1, '2021-11-08 17:29:21', '2021-11-08 17:29:21'),
(4, 1, '2021-11-08 17:29:35', '2021-11-08 17:29:35'),
(6, 1, '2021-11-24 13:13:39', '2021-11-24 14:07:25'),
(7, 1, '2021-12-02 16:56:04', '2021-12-02 16:56:04');

-- create table `oc_blog_category_description`
CREATE TABLE IF NOT EXISTS `oc_blog_category_description` (
  `blog_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `alias` varchar(255) NOT NULL,
  PRIMARY KEY (`blog_category_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- create table `oc_blog_to_blog_category`
CREATE TABLE IF NOT EXISTS `oc_blog_to_blog_category` (
  `blog_id` int(11) NOT NULL,
  `blog_category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_blog_tag`
CREATE TABLE IF NOT EXISTS `oc_blog_tag` (
  `blog_tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`blog_tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- -----------------------------------------------------------
-- --------end v1.8.1 theme api + builder + blog -------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.8.2 tuning -------------------------------------
-- -----------------------------------------------------------

-- default order success text
INSERT INTO `oc_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
('0', 'config', 'config_order_success_text', 'MUA HÀNG THÀNH CÔNG!', '0');

-- -----------------------------------------------------------
-- --------end v1.8.2 tuning ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.9.2 pos ----------------------------------------
-- -----------------------------------------------------------

-- add column previous_order_status_id to table order
ALTER TABLE `oc_order` ADD `pos_id_ref` VARCHAR(50) NULL DEFAULT NULL AFTER `order_code`;

-- add column store_id and user_id to tables report. default store_id = 0 and user_id = 1
ALTER TABLE `oc_report_product`
  ADD `store_id` INT(11) NULL DEFAULT 0 AFTER `total_amount`,
  ADD `user_id` INT NULL DEFAULT 1 AFTER `store_id`;

ALTER TABLE `oc_report_order`
  ADD `store_id` INT(11) NULL DEFAULT 0 AFTER `order_status`,
  ADD `user_id` INT(11) NULL DEFAULT 1 AFTER `store_id`;

ALTER TABLE `oc_report_customer`
  ADD `store_id` INT(11) NULL DEFAULT 0 AFTER `customer_id`,
  ADD `user_id` INT(11) NULL DEFAULT 1 AFTER `store_id`;

-- add column channel to table product. null or 0 -> website, 1 -> offline, 2 -> website + offline
ALTER TABLE `oc_product` ADD `channel` INT NULL DEFAULT 2 AFTER `status`;

-- insert default store to new shop
INSERT INTO `oc_store` (`name`, `url`, `ssl`) VALUES
('Kho trung tâm', '', '');

-- IMPORTANT: force from 0, because current sql_mode is not 'NO_AUTO_VALUE_ON_ZERO'
UPDATE `oc_store` SET `store_id` = 0 WHERE `name` = 'Kho trung tâm';

-- -----------------------------------------------------------
-- --------end v1.9.2 pos ------------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v1.9.3 tuning -------------------------------------
-- -----------------------------------------------------------

-- create table `oc_images`
CREATE TABLE IF NOT EXISTS `oc_images` (
    `image_id` INT(11) NOT NULL AUTO_INCREMENT,
    `type` VARCHAR(56) NOT NULL DEFAULT 'image',
    `name` varchar(256) NULL default NULL,
    `url` VARCHAR(256) NOT NULL,
    `directory` VARCHAR(56) NULL DEFAULT NULL,
    `status` TINYINT(1) NOT NULL DEFAULT '1',
    `source` VARCHAR(56) NOT NULL DEFAULT 'Cloudinary',
    `source_id` VARCHAR (256) NULL DEFAULT  NULL,
     PRIMARY KEY (`image_id`)
)ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- add default register success text
INSERT INTO `oc_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'config', 'config_register_success_text', '<p>Chúc mừng! Tài khoản mới của bạn đã được tạo thành công!</p>\n\n<p>Bây giờ bạn có thể tận hưởng lợi ích thành viên khi tham gia trải nghiệm mua sắm cùng chúng tôi.</p>\n\n<p>Nếu bạn có câu hỏi gì về hoạt động của cửa hàng, vui lòng liên hệ với chủ cửa hàng.</p>\n\n<p>Một email xác nhận đã được gửi tới hòm thư của bạn. Nếu bạn không nhận được email này trong vòng 1 giờ, vui lòng liên hệ chúng tôi.</p>', 0);

-- -----------------------------------------------------------
-- --------end v1.9.3 tuning ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------HOT: soft deletable product -----------------------
-- -----------------------------------------------------------

ALTER TABLE `oc_product` ADD `deleted` VARCHAR(50) DEFAULT NULL AFTER `demo`;
ALTER TABLE `oc_product_version` ADD `deleted` VARCHAR(50) NULL AFTER `status`;

-- -----------------------------------------------------------
-- --------end HOT: soft deletable product -------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- v2.1 discount ------------------------------------
-- -----------------------------------------------------------

-- create table `oc_discount`
CREATE TABLE IF NOT EXISTS `oc_discount` (
  `discount_id` INT(11) NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(50) NOT NULL,
  `discount_type_id` INT(11) NOT NULL,
  `discount_status_id` INT(11) NOT NULL,
  `usage_limit` INT(11) NOT NULL DEFAULT '0',
  `times_used` INT(11) NOT NULL DEFAULT '0',
  `config` MEDIUMTEXT NULL DEFAULT NULL,
  `order_source` VARCHAR(128) NULL DEFAULT NULL,
  `customer_group` VARCHAR(128) NULL DEFAULT 'All',
  `start_at` DATETIME NOT NULL,
  `end_at` DATETIME NULL,
  `pause_reason` VARCHAR(256) NULL,
  `paused_at` DATETIME NULL,
  `delete_reason` VARCHAR(256) NULL,
  `deleted_at` DATETIME NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`discount_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_discount_description`
CREATE TABLE IF NOT EXISTS `oc_discount_description` (
  `discount_id` INT(11) NOT NULL,
  `language_id` INT(11) NOT NULL,
  `name` VARCHAR(256) NOT NULL,
  `description` VARCHAR(256) NOT NULL,
  PRIMARY KEY (discount_id, `language_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_discount_to_store`
CREATE TABLE IF NOT EXISTS `oc_discount_to_store` (
  `discount_id` INT(11) NOT NULL,
  `store_id` INT(11) NOT NULL,
  PRIMARY KEY (`discount_id`, `store_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_discount_type`
CREATE TABLE IF NOT EXISTS `oc_discount_type` (
  `discount_type_id` INT(11) NOT NULL,
  `language_id` INT(11) NOT NULL,
  `name` VARCHAR(128) NOT NULL,
  `description` VARCHAR(128) NULL,
  PRIMARY KEY (`discount_type_id`, `language_id` )
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_discount_status`
CREATE TABLE IF NOT EXISTS `oc_discount_status` (
  `discount_status_id` INT(11) NOT NULL,
  `language_id` INT(11) NOT NULL,
  `name` VARCHAR(128) NOT NULL,
  `description` VARCHAR(128) NULL,
  PRIMARY KEY (`discount_status_id`, `language_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_order_to_discount`
CREATE TABLE IF NOT EXISTS `oc_order_to_discount` (
  `order_id` INT(11) NOT NULL,
  `discount_id` INT(11) NOT NULL,
  PRIMARY KEY (`order_id`, `discount_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- add discount to table order_product
ALTER TABLE `oc_order_product` ADD `discount` decimal(18,4) NOT NULL DEFAULT '0.0000' AFTER `price`;

-- add discount to table order
ALTER TABLE `oc_order` ADD `discount` decimal(18,4) NOT NULL DEFAULT '0.0000' AFTER `order_cancel_comment`;

-- init data for table `oc_discount_status`
INSERT INTO `oc_discount_status` (`discount_status_id`, `language_id`, `name`, `description`) VALUES
('1', '2', 'Đã lập lịch', NULL),
('2', '2', 'Đang hoạt đông', NULL),
('3', '2', 'Hết hạn', NULL),
('4', '2', 'Đã xóa', NULL),
('1', '1', 'Scheduled', NULL),
('2', '1', 'Activated', NULL),
('3', '1', 'Paused', NULL),
('4', '1', 'Deleted', NULL);

-- init data for table `oc_discount_type`
INSERT INTO `oc_discount_type` (`discount_type_id`, `language_id`, `name`, `description`) VALUES
('1', '2', 'Chiết khấu theo tổng giá trị đơn hàng', NULL),
('2', '2', 'Chiết khấu theo từng sản phẩm', NULL),
('3', '2', 'Chiết khấu theo loại sản phẩm', NULL),
('4', '2', 'Chiết khấu theo nhà cung cấp', NULL),
('1', '1', 'Discount by total order value', NULL),
('2', '1', 'Discount by each product', NULL),
('3', '1', 'Discount by product type', NULL),
('4', '1', 'Discount by supplier', NULL);

-- -----------------------------------------------------------
-- --------end v2.1 discount ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------v2.2.1 api chatbot v2 -----------------------------
-- -----------------------------------------------------------

-- add discount to table order
ALTER TABLE `oc_order` ADD `customer_ref_id` VARCHAR(128) NULL DEFAULT NULL AFTER `pos_id_ref`;

-- -----------------------------------------------------------
-- --------end v2.2.1 api chatbot v2 -------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- v2.2.2-store-receipt -----------------------------
-- -----------------------------------------------------------

-- create table `oc_store_receipt`
CREATE TABLE IF NOT EXISTS `oc_store_receipt` (
  `store_receipt_id` INT(11) NOT NULL AUTO_INCREMENT,
  `store_id` INT(11) NOT NULL,
  `manufacturer_id` INT(11) NOT NULL,
  `store_receipt_code` varchar(50) NULL,
  `total` decimal(18,4) DEFAULT NULL,
  `total_to_pay` decimal(18,4) DEFAULT NULL,
  `total_paid` decimal(18,4) DEFAULT NULL,
  `discount` decimal(18,4) DEFAULT NULL,
  `allocation_criteria_type` VARCHAR(50) DEFAULT NULL,
  `allocation_criteria_total` decimal(18,4) DEFAULT NULL,
  `allocation_criteria_fees` MEDIUMTEXT NULL DEFAULT NULL,
  `owed` decimal(18,4) DEFAULT NULL,
  `comment` TEXT NULL DEFAULT NULL,
  `status` INT(4) NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`store_receipt_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- create table `oc_store_receipt_to_product`
CREATE TABLE IF NOT EXISTS `oc_store_receipt_to_product` (
  `store_receipt_product_id` INT(11) NOT NULL  AUTO_INCREMENT,
  `store_receipt_id` INT(11) NOT NULL,
  `product_id` INT(11) NOT NULL,
  `product_version_id` INT(11) NULL,
  `quantity` INT(11) NULL,
  `cost_price` decimal(18,4) DEFAULT NULL,
  `fee` decimal(18,4) DEFAULT NULL,
  `discount` decimal(18,4) DEFAULT NULL,
  `total` decimal(18,4) DEFAULT NULL,
  PRIMARY KEY (`store_receipt_product_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- new column `oc_manufacturer`
ALTER TABLE `oc_manufacturer`
  ADD `manufacturer_code` varchar(50) NULL AFTER `sort_order`,
  ADD `telephone` varchar(30) NULL AFTER `manufacturer_code`,
  ADD `email` varchar(200) NULL AFTER `telephone`,
  ADD `tax_code` varchar(200) NULL AFTER `email`,
  ADD `address` text NULL AFTER `tax_code`,
  ADD `province` varchar(50) NULL AFTER `address`,
  ADD `district` varchar(50) NULL AFTER `province`,
  ADD `status` int(4) NOT NULL DEFAULT '1' AFTER `district`,
  ADD `date_added` datetime NULL AFTER `status`,
  ADD `date_modified` datetime NULL AFTER `date_added`;

-- new column oc_product_to_store
ALTER TABLE `oc_product_to_store`
  ADD `product_version_id` int(11) NULL DEFAULT 0 AFTER `store_id`,
  ADD `quantity` int(11) NULL DEFAULT 0 AFTER `product_version_id`,
  ADD `cost_price` DECIMAL(15,4 ) NULL DEFAULT '0' AFTER `quantity`;

-- change primary key oc_product_to_store
ALTER TABLE `oc_product_to_store`
  DROP PRIMARY KEY, ADD PRIMARY KEY(`product_id`, `product_version_id`, `store_id`);

-- new column oc_product
ALTER TABLE `oc_product`
  ADD `default_store_id` INT(11) NULL DEFAULT '0' AFTER `deleted`;

-- -----------------------------------------------------------
-- --------end v2.2.2-store-receipt --------------------------
-- -----------------------------------------------------------


-- -----------------------------------------------------------
-- -------- v2.2.3-single signon + chatbot api v2 more -------
-- -----------------------------------------------------------

CREATE TABLE IF NOT EXISTS `oc_order_utm` (
  `order_utm_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `code` varchar(256) NOT NULL,
  `key` varchar(256) NOT NULL,
  `value` text NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
   PRIMARY KEY (`order_utm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- -----------------------------------------------------------
-- --------end v2.2.3-single signon + chatbot api v2 more ----
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------start v2.3.2-tuning -------------------------------
-- -----------------------------------------------------------

-- new column oc_product
ALTER TABLE `oc_product`
   ADD `common_cost_price` DECIMAL(15,4 ) NULL DEFAULT '0' AFTER `common_price`;

-- -----------------------------------------------------------
-- --------end v2.3.2-tuning ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------start v2.4.2-app-store -------------------------------
-- -----------------------------------------------------------
-- create table my_app
CREATE TABLE IF NOT EXISTS `oc_my_app` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_code` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1)  NULL,
  `expiration_date` datetime  DEFAULT NULL,
  `app_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path_asset` text COLLATE utf8_unicode_ci  NULL,
  `trial` int(2)  NULL,
  `installed` int(11) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- INSERT APP DEFAULT FOR APP
-- INSERT INTO `oc_my_app` (`app_code`, `status`, `expiration_date`, `app_name`, `path_logo`, `path_asset`, `trial`, `installed`,`date_added`,`date_modified`) VALUES
-- ('mar_ons_chatbot', 1, NULL, 'NovaonX ChatBot', 'view/image/appstore/mar_ons_chatbot.jpg', '', 0, 1, NOW(), NOW()),
-- ('mar_ons_maxlead', 1, NULL, 'AutoAds Maxlead', 'view/image/appstore/mar_ons_maxlead.jpg', '', 0, 1, NOW(), NOW()),
-- ('trans_ons_vtpost', 1, NULL, 'VietelPost', 'view/image/appstore/trans_ons_vtpost.jpg', '', 0, 1, NOW(), NOW()),
-- ('trans_ons_ghn', 1, NULL, 'Giao Hàng Nhanh', 'view/image/appstore/trans_ons_ghn.jpg', '', 0, 1, NOW(), NOW());

-- craete table appstore_setting
CREATE TABLE IF NOT EXISTS `oc_appstore_setting` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 NOT NULL,
  `code` varchar(64) CHARACTER SET utf8 NOT NULL,
  `setting` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- create table app_config_theme
CREATE TABLE IF NOT EXISTS `oc_app_config_theme` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `module_id` int(255) NOT NULL,
  `theme_value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NULL,
  `sort` int(10)  NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- -----------------------------------------------------------
-- --------end v2.4.2-app-store ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- start v2.5.2 Advance Store Management ------------
-- -----------------------------------------------------------

-- store_transfer_receipt : Table to save transfer receipt of store
CREATE TABLE IF NOT EXISTS `oc_store_transfer_receipt` (
  `store_transfer_receipt_id` INT(11) NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(128) NOT NULL,
  `export_store_id` INT(11) NOT NULL,
  `import_store_id` INT(11) NOT NULL,
  `common_quantity_transfer` INT(16) NULL,
  `total` decimal(18,4) NOT NULL,
  `status` TINYINT(2) NOT NULL DEFAULT '0' COMMENT '0: lưu nháp, 1: đã chuyển, 2: đã nhận, 9: đã hủy',
  `date_exported` DATE NOT NULL,
  `date_imported` DATE NULL,
  `date_added` DATETIME NOT NULL,
  `date_modified` DATETIME NOT NULL,
  PRIMARY KEY (`store_transfer_receipt_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- store_transfer_receipt_to_product : relationship transfer receipt with product
CREATE TABLE IF NOT EXISTS `oc_store_transfer_receipt_to_product` (
  `store_transfer_receipt_id` INT(11) NOT NULL,
  `product_id` INT(11) NOT NULL,
  `product_version_id` INT(11) NOT NULL DEFAULT '0',
  `quantity` INT(11) NOT NULL,
  `current_quantity` INT(11) NULL,
  `price` decimal(18,4) NOT NULL,
  PRIMARY KEY (`store_transfer_receipt_id`, `product_id`, `product_version_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- store_take_receipt : Table to save take receipt of store
CREATE TABLE IF NOT EXISTS `oc_store_take_receipt` (
  `store_take_receipt_id` INT(11) NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(128) NOT NULL,
  `store_id` INT(11) NOT NULL,
  `difference_amount` decimal(18,4) NOT NULL,
  `status` TINYINT(2) NOT NULL DEFAULT '0',
  `date_taked` DATE NOT NULL,
  `date_added` DATETIME NOT NULL,
  `date_modified` DATETIME NOT NULL,
  PRIMARY KEY (`store_take_receipt_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- store_take_receipt_to_product : Relationship for take receipt with product
CREATE TABLE IF NOT EXISTS `oc_store_take_receipt_to_product` ( 
  `store_take_receipt_id` INT(11) NOT NULL,
  `product_id` INT(11) NOT NULL,
  `product_version_id` INT(11) NOT NULL DEFAULT '0',
  `inventory_quantity` INT(11) NOT NULL,
  `actual_quantity` INT(11) NOT NULL,
  `difference_amount` decimal(18,4) NOT NULL,
  `reason`  VARCHAR(256),
  PRIMARY KEY (`store_take_receipt_id`, `product_id`, `product_version_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- cost_adjustment_receipt: table to save cost adjustment receipt
CREATE TABLE IF NOT EXISTS `oc_cost_adjustment_receipt` ( 
  `cost_adjustment_receipt_id` INT NOT NULL AUTO_INCREMENT,
  `cost_adjustment_receipt_code` VARCHAR(255) NULL,
  `store_id` INT(255) NOT NULL,
  `note` TEXT NULL,
  `status` tinyint(2) NULL DEFAULT 0,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`cost_adjustment_receipt_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- cost_adjustment_receipt_to_product: Relationship of cost adjustment receipt with product
CREATE TABLE IF NOT EXISTS `oc_cost_adjustment_receipt_to_product` ( 
  `cost_adjustment_receipt_id` INT NOT NULL,
  `product_id` INT(255) NOT NULL,
  `product_version_id` INT(255) NOT NULL DEFAULT '0',
  `adjustment_cost_price` DECIMAL(18,4) NULL,
  `current_cost_price` DECIMAL(18,4) NULL,
  `note` VARCHAR(255) NULL,
  PRIMARY KEY (`cost_adjustment_receipt_id`, `product_id`, `product_version_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- -----------------------------------------------------------
-- --------- end v2.5.2 Advance Store Management -------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- v2.5.4-tuning ------------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------end v2.5.4-tuning ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- start v2.6.1 Shopee connection -------------------
-- -----------------------------------------------------------

-- shopee_order
CREATE TABLE IF NOT EXISTS `oc_shopee_order` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(128) NOT NULL,
  `cod` TINYINT(1) NOT NULL,
  `currency` VARCHAR(64) NOT NULL,
  `fullname` VARCHAR(256) NULL COMMENT 'Tên người nhận',
  `phone` VARCHAR(32) NOT NULL COMMENT 'Số ',
  `town` VARCHAR(256) NULL,
  `district` VARCHAR(256) NULL,
  `city` VARCHAR(256) NULL,
  `state` VARCHAR(256) NULL,
  `country` VARCHAR(256) NULL,
  `zipcode` VARCHAR(32) NULL,
  `full_address` VARCHAR(512) NULL ,
  `actual_shipping_cost` DECIMAL(18,4) NULL,
  `total_amount` DECIMAL(18,4) NOT NULL,
  `order_status` VARCHAR(128) NOT NULL,
  `shipping_method` VARCHAR(256) NOT NULL COMMENT 'shipping_carrier',
  `payment_method` VARCHAR(256) NOT NULL,
  `payment_status` TINYINT(2) NULL DEFAULT 0,
  `note_for_shop` TEXT NULL COMMENT 'message_to_seller',
  `note` TEXT NULL,
  `create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` DATETIME NULL,
  `shopee_shop_id` BIGINT(64) NOT NULL,
  `bestme_order_id` INT(11) NULL,
  `sync_status` TINYINT(2)  NULL,
  `is_edited` TINYINT(1) NOT NULL DEFAULT 0,
  `sync_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_shopee_order_to_product
CREATE TABLE IF NOT EXISTS `oc_shopee_order_to_product` (
  `shopee_order_code` VARCHAR(128) NOT NULL COMMENT 'order code on shopee',
  `item_id` BIGINT(64) NOT NULL COMMENT '~ product_id ',
  `variation_id` BIGINT(64) NOT NULL COMMENT '~ product_version_id',
  `variation_name` VARCHAR(256) NULL ,
  `quantity` INT(11) NOT NULL COMMENT 'variation_quantity_purchased',
  `original_price` DECIMAL(18,4) NOT NULL COMMENT 'variation_original_price',
  `price` DECIMAL(18,4) NOT NULL COMMENT 'variation_discounted_price'
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_shopee_shop_config
CREATE TABLE IF NOT EXISTS `oc_shopee_shop_config` (
  `shop_id` BIGINT(64) NOT NULL,
  `shop_name` VARCHAR(256) NULL,
  `sync_interval` INT(11) NOT NULL COMMENT '0: không bao giờ',
  `connected` TINYINT(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`shop_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- -----------------------------------------------------------
-- -------- end v2.6.1 Shopee connection ---------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- Fix load demo data -------------------------------
-- -----------------------------------------------------------

-- add column `demo` to oc_blog
ALTER TABLE `oc_blog` ADD `demo` TINYINT(1) NOT NULL DEFAULT '0' AFTER `status`;

-- -----------------------------------------------------------
-- -------- Fix load demo data -------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- start v2.6.3 Lazada connection -------------------
-- -----------------------------------------------------------

-- oc_lazada_category_attributes
CREATE TABLE IF NOT EXISTS `oc_lazada_category_attributes` (
  `lazada_cateogry_id` BIGINT(32) NOT NULL ,
  `data` LONGTEXT NOT NULL ,
  PRIMARY KEY (`lazada_cateogry_id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_lazada_shop_config
CREATE TABLE IF NOT EXISTS `oc_lazada_shop_config` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `seller_id` BIGINT(32) NOT NULL ,
  `country` VARCHAR(16) NOT NULL ,
  `shop_name` VARCHAR(256) NOT NULL ,
  `access_token` VARCHAR(256) NOT NULL ,
  `expires_in` DATETIME NOT NULL ,
  `refresh_token` VARCHAR(256) NOT NULL ,
  `refresh_expires_in` DATETIME NOT NULL ,
  `sync_interval` INT(11) NOT NULL DEFAULT '0' ,
  `connected` TINYINT(2) NOT NULL DEFAULT '1' ,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_lazada_product
CREATE TABLE IF NOT EXISTS `oc_lazada_product` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `shop_id` INT(11) NOT NULL ,
  `item_id` VARCHAR(64) NOT NULL ,
  `primary_category` VARCHAR(64) NOT NULL ,
  `name` VARCHAR(256) NOT NULL ,
  `description` VARCHAR(512) NULL ,
  `short_description` VARCHAR(256) NULL ,
  `brand` VARCHAR(128) NULL ,
  `bestme_product_id` INT(11) NULL ,
  `sync_status` INT(11) NULL ,
  `created_at` DATETIME NULL ,
  `updated_at` DATETIME NULL ,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_lazada_product_version
CREATE TABLE IF NOT EXISTS `oc_lazada_product_version` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `item_id` VARCHAR(64) NOT NULL ,
  `version_name` VARCHAR(128) NULL ,
  `status` VARCHAR(26) NULL ,
  `quantity` INT(11) NULL ,
  `product_weight` DECIMAL(15,4) NULL ,
  `images` MEDIUMTEXT NULL COMMENT 'list image' ,
  `seller_sku` VARCHAR(128) NOT NULL ,
  `shop_sku` VARCHAR(128) NULL ,
  `url` VARCHAR(256) NULL ,
  `package_width` DECIMAL(10,2) NULL COMMENT 'centimet' ,
  `package_height` DECIMAL(10,2) NULL COMMENT 'centimet' ,
  `package_length` DECIMAL(10,2) NULL COMMENT 'centimet',
  `package_weight` DECIMAL(10,4) NULL COMMENT 'kilogam' ,
  `price` DECIMAL(15,4) NULL ,
  `available` INT(11) NULL ,
  `sku_id` BIGINT(32) NULL ,
  `bestme_product_id` INT(11) NULL ,
  `bestme_product_version_id` INT(11) NULL ,
  `sync_status` INT(11) NULL ,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_lazada_category_tree
CREATE TABLE IF NOT EXISTS `oc_lazada_category_tree` (
  `id` INT(11) NOT NULL ,
  `parent_id` INT(11) NOT NULL DEFAULT '0' ,
  `name` VARCHAR(256) NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_lazada_category_description
CREATE TABLE IF NOT EXISTS `oc_lazada_category_description` (
  `category_id` INT(11) NOT NULL ,
  `name` VARCHAR(256) NOT NULL
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- -----------------------------------------------------------
-- -------- end v2.6.3 Lazada connection ---------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- v2.7.1 Lazada Order ------------------------------
-- -----------------------------------------------------------

-- oc_lazada_order
CREATE TABLE IF NOT EXISTS `oc_lazada_order` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `order_id` BIGINT(64) NOT NULL ,
  `fullname` VARCHAR(256) NULL COMMENT 'address_shipping name' ,
  `phone` VARCHAR(32) NULL COMMENT 'address_shipping phone' ,
  `ward` VARCHAR(256) NULL COMMENT 'address_billing address5' ,
  `district` VARCHAR(256) NULL COMMENT 'address_billing address4' ,
  `province` VARCHAR(256) NULL COMMENT 'address_billing address3' ,
  `full_address` VARCHAR(256) NULL COMMENT 'address_billing address1' ,
  `shipping_fee_original` DECIMAL(18,4) NULL ,
  `shipping_fee` DECIMAL(18,4) NULL ,
  `shipping_method` VARCHAR(256) NULL COMMENT 'price' ,
  `total_amount` DECIMAL(18,4) NULL ,
  `order_status` VARCHAR(64) NULL ,
  `payment_method` VARCHAR(256) NULL ,
  `payment_status` TINYINT(2) NULL ,
  `voucher` DECIMAL(18,4) NULL ,
  `voucher_seller` DECIMAL(18,4) NULL ,
  `remarks` VARCHAR(512) NULL ,
  `create_time` DATETIME NULL ,
  `update_time` DATETIME NULL ,
  `lazada_shop_id` INT(11) NOT NULL ,
  `bestme_order_id` INT(11) NULL ,
  `sync_status` TINYINT(2) NULL ,
  `is_edited` TINYINT(2) NULL ,
  `sync_time` DATETIME NULL ,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_lazada_order_to_product
CREATE TABLE IF NOT EXISTS `oc_lazada_order_to_product` (
  `lazada_order_id` BIGINT(64) NOT NULL ,
  `sku` VARCHAR(128) NULL ,
  `shop_sku` VARCHAR(128) NULL ,
  `variation` VARCHAR(256) NULL ,
  `name` VARCHAR(256) NULL ,
  `quantity` INT(11) NOT NULL DEFAULT 0,
  `item_price` DECIMAL(18,4) NULL ,
  `paid_price` DECIMAL(18,4) NULL
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- -----------------------------------------------------------
-- -------- end - v2.7.1 Lazada Order ------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- v2.7.2 Tuning ------------------------------------
-- -----------------------------------------------------------

-- alt blog image
ALTER TABLE `oc_blog_description`
  ADD `alt` TEXT NULL AFTER `alias`;

-- -----------------------------------------------------------
-- -------- end - v2.7.2 Tunging -----------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- v2.7.3 New Customer Design -----------------------
-- -----------------------------------------------------------

ALTER TABLE `oc_customer`
  ADD `sex` TINYINT(2) NULL COMMENT '1: Nam, 2: Nữ, 3: Khác' AFTER `date_added`,
  ADD `birthday` DATE NULL AFTER `sex`,
  ADD `full_name` VARCHAR(255) NOT NULL AFTER `birthday`,
  ADD `website` VARCHAR(255) NULL AFTER `full_name`,
  ADD `tax_code` VARCHAR(100) NULL AFTER `website`,
  ADD `staff_in_charge` INT(11) NULL AFTER `tax_code`;

ALTER TABLE `oc_customer`
  CHANGE `firstname` `firstname` VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  CHANGE `lastname` `lastname` VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;

ALTER TABLE `oc_customer_group`
  ADD `customer_group_code` VARCHAR(50) NULL AFTER `sort_order`;

-- -----------------------------------------------------------
-- -------- end v2.7.3 New Customer Design -------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- --------------- start v2.7.4 Cash Flow --------------------
-- -----------------------------------------------------------

-- oc_receipt_voucher
CREATE TABLE IF NOT EXISTS `oc_receipt_voucher` (
    `receipt_voucher_id` INT(11) NOT NULL AUTO_INCREMENT,
    `receipt_voucher_code` VARCHAR(50) NOT NULL,
    `receipt_voucher_type_id` INT(11) NOT NULL,
    `order_id` INT(11) NULL DEFAULT NULL,
    `status` INT(11) NOT NULL,
    `in_business_report_status` TINYINT(4) DEFAULT 1,
    `amount` decimal(18,4) NOT NULL DEFAULT '0.0000',
    `store_id` INT(11) NOT NULL,
    `cash_flow_method_id` INT(11) NOT NULL,
    `cash_flow_object_id` INT(11) NULL DEFAULT NULL,
    `object_info` TEXT NULL DEFAULT NULL,
    `receipt_type_other` VARCHAR (255) NULL DEFAULT NULL,
    `note` TEXT NULL DEFAULT NULL,
    `date_added` DATETIME NOT NULL,
    `date_modified` DATETIME NOT NULL,
    PRIMARY KEY (`receipt_voucher_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_payment_voucher
CREATE TABLE IF NOT EXISTS `oc_payment_voucher` (
    `payment_voucher_id` INT(11) NOT NULL AUTO_INCREMENT,
    `payment_voucher_type_id` INT(11) NOT NULL,
    `payment_voucher_code` VARCHAR(50) NOT NULL,
    `store_receipt_id` INT(11) NULL DEFAULT NULL,
    `status` INT(11) NOT NULL,
    `in_business_report_status` TINYINT(4) DEFAULT 1,
    `amount` decimal(18,4) NOT NULL DEFAULT '0.0000',
    `store_id` INT(11) NOT NULL,
    `cash_flow_method_id` INT(11) NOT NULL,
    `cash_flow_object_id` INT(11) NULL DEFAULT NULL,
    `object_info` TEXT NULL DEFAULT NULL,
    `payment_type_other` VARCHAR (255) NULL DEFAULT NULL,
    `note` TEXT NULL DEFAULT NULL,
    `date_added` DATETIME NOT NULL,
    `date_modified` DATETIME NOT NULL,
    PRIMARY KEY (`payment_voucher_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_receipt_voucher_type
CREATE TABLE IF NOT EXISTS `oc_receipt_voucher_type` (
    `receipt_voucher_type_id` INT(11) NOT NULL,
    `language_id` TINYINT(4) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (`receipt_voucher_type_id`, `language_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_payment_voucher_type
CREATE TABLE IF NOT EXISTS `oc_payment_voucher_type` (
    `payment_voucher_type_id` INT(11) NOT NULL,
    `language_id` TINYINT(4) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (`payment_voucher_type_id`, `language_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_cash_flow_method
CREATE TABLE IF NOT EXISTS `oc_cash_flow_method` (
    `cash_flow_method_id` INT(11) NOT NULL,
    `language_id` TINYINT(4) NOT NULL,
    `name` VARCHAR (255) NOT NULL,
    `description` VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (`cash_flow_method_id`, `language_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_cash_flow_object
CREATE TABLE IF NOT EXISTS `oc_cash_flow_object` (
    `cash_flow_object_id` INT(11) NOT NULL,
    `language_id` TINYINT(4) NOT NULL,
    `name` VARCHAR (255) NOT NULL,
    `description` VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (`cash_flow_object_id`, `language_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- Thêm dữ liệu cho các bảng: oc_cash_flow_method, oc_cash_flow_object, oc_receipt_voucher_type, oc_payment_voucher_type
INSERT INTO `oc_cash_flow_method` (`cash_flow_method_id`, `language_id`, `name`, `description`) VALUES
(1, 2, 'Tiền mặt', NULL),
(1, 1, 'Cash', NULL),
(2, 2, 'Chuyển khoản', NULL),
(2, 1, 'Transfer', NULL),
(3, 2, 'COD', NULL),
(3, 1, 'COD', NULL),
(4, 2, 'Quẹt thẻ', NULL),
(4, 1, 'Card', NULL);

INSERT INTO `oc_cash_flow_object` (`cash_flow_object_id`, `language_id`, `name`, `description`) VALUES
(1, 2, 'Khách hàng', NULL),
(1, 1, 'Customer', NULL),
(2, 2, 'Nhân viên', NULL),
(2, 1, 'Staff', NULL),
(3, 2, 'Nhà cung cấp', NULL),
(3, 1, 'Supplier', NULL),
(4, 2, 'Đối tác vận chuyển', NULL),
(4, 1, 'Shipping partner', NULL),
(5, 2, 'Khác', NULL),
(5, 1, 'Other', NULL);

INSERT INTO `oc_receipt_voucher_type` (`receipt_voucher_type_id`, `language_id`, `name`, `description`) VALUES
(1, 2, 'Đối tác vận chuyển đặt cọc', NULL),
(1, 1, 'Shipping partner makes a deposit', NULL),
(2, 2, 'Thu nợ đối tác vận chuyển', NULL),
(2, 1, 'Collecting debt from shipping partner', NULL),
(3, 2, 'Thu nhập khác', NULL),
(3, 1, 'Other income', NULL),
(4, 2, 'Tiền thưởng', NULL),
(4, 1, 'Bonus/Reward', NULL),
(5, 2, 'Tiền bồi thường', NULL),
(5, 1, 'Compensation', NULL),
(6, 2, 'Cho thuê, thanh lý tài sản', NULL),
(6, 1, 'Rent and liquidation property', NULL),
(7, 2, 'Thu nợ khách hàng', NULL),
(7, 1, 'Collecting debt from customer', NULL),
(8, 2, 'Thu tự động', NULL),
(8, 1, 'Automation receipt', NULL),
(9, 2, 'Khác', NULL),
(9, 1, 'Other', NULL);

INSERT INTO `oc_payment_voucher_type` (`payment_voucher_type_id`, `language_id`, `name`, `description`) VALUES
(1, 2, 'Trả nợ đối tác vận chuyển', NULL),
(1, 1, 'Paying debt for shipping partner', NULL),
(2, 2, 'Chi phí sản xuất', NULL),
(2, 1, 'Production cost', NULL),
(3, 2, 'Chi phí nguyên - vật liệu', NULL),
(3, 1, 'Raw materials cost', NULL),
(4, 2, 'Chi phí sinh hoạt', NULL),
(4, 1, 'Living cost', NULL),
(5, 2, 'Chi phí nhân công', NULL),
(5, 1, 'Labor cost', NULL),
(6, 2, 'Chi phí bán hàng', NULL),
(6, 1, 'Selling cost', NULL),
(7, 2, 'Chi phí quản lý cửa hàng', NULL),
(7, 1, 'Store management cost', NULL),
(8, 2, 'Chi tự động', NULL),
(8, 1, 'Automation Payment', NULL),
(9, 2, 'Chi phí khác', NULL),
(9, 1, 'Other cost', NULL);

-- -----------------------------------------------------------
-- ---------------- end v2.7.4 Cash Flow ---------------------
-- -----------------------------------------------------------


-- -----------------------------------------------------------
-- ---- HOT FIX change type fields on table lazada_product ---
-- -----------------------------------------------------------

ALTER TABLE `oc_lazada_product` CHANGE
             `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
             CHANGE `short_description` `short_description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

-- -----------------------------------------------------------
-- -- end HOT FIX change type fields on table lazada_product--
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- -------- start activity log analytics ---------------------
-- -----------------------------------------------------------

-- Insert event to tracking in table oc_event
INSERT INTO `oc_event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES
('tracking_order_complete', 'admin/model/sale/order/updateStatus/after', 'event/activity_tracking/trackingOrderComplete', 1, 0),
('tracking_login', 'admin/model/user/user/editLastLoggedIn/after', 'event/activity_tracking/trackingLogin', 1, 0),
('tracking_product_category', 'admin/model/catalog/store_take_receipt/addStoreTakeReceipt/after', 'event/activity_tracking/trackingStoreTakeReceipt', 1, 0),
('tracking_new_product', 'admin/model/catalog/product/addProduct/after', 'event/activity_tracking/trackingNewProduct', 1, 0),
('tracking_transfer_order_status', 'admin/model/sale/order_history/addHistory/after', 'event/activity_tracking/trackingTransferOrderStatus', 1, 0),
('tracking_new_product_category', 'admin/model/catalog/category/addCategory/after', 'event/activity_tracking/trackingNewProductCategory', 1, 0),
('tracking_new_product_category', 'admin/model/catalog/category/addCategoryFast/after', 'event/activity_tracking/trackingNewProductCategory', 1, 0),
('tracking_delete_product_category', 'admin/model/catalog/category/deleteCategory/after', 'event/activity_tracking/trackingDeleteProductCategory', 1, 0),
('tracking_product_price', 'admin/model/catalog/product/addProduct/after', 'event/activity_tracking/trackingProductPrice', 1, 0),
('tracking_product_price', 'admin/model/catalog/product/editProduct/after', 'event/activity_tracking/trackingProductPrice', 1, 0),
('tracking_new_store_transfer_receipt', 'admin/model/catalog/store_transfer_receipt/addStoreTransferReceipt/after', 'event/activity_tracking/trackingStoreTransferReceipt', 1, 0),
('tracking_new_store_receipt', 'admin/model/catalog/store_receipt/addStoreReceipt/after', 'event/activity_tracking/trackingStoreReceipt', 1, 0),
('tracking_new_cost_adjustment_receipt', 'admin/model/catalog/cost_adjustment_receipt/addReceipt/after', 'event/activity_tracking/trackingCostAdjustmentReceipt', 1, 0),
('tracking_new_discount', 'admin/model/discount/discount/addDiscount/after', 'event/activity_tracking/trackingCountDiscount', 1, 0),
('tracking_new_order_catalog', 'catalog/model/sale/order/addOrderCommon/after', 'event/activity_tracking/trackingCountOrder', 1, 0),
('tracking_new_order_admin', 'admin/model/sale/order/addOrder/after', 'event/activity_tracking/trackingCountOrder', 1, 0),
('tracking_new_shopee', 'admin/model/catalog/shopee/addShopeeShop/after', 'event/activity_tracking/trackingCountShopee', 1, 0),
('tracking_new_shopee_product', 'admin/model/catalog/product/addProduct/after', 'event/activity_tracking/trackingCountShopeeProduct', 1, 0),
('tracking_pos_order_complete', 'catalog/model/sale/order/addOrderCommon/after', 'event/activity_tracking/trackingOrderComplete', 1, 0);

-- -----------------------------------------------------------
-- -------- end activity log analytics -----------------------
-- -----------------------------------------------------------


-- -----------------------------------------------------------
-- ---------- start v2.8.1 Shopee advance product ------------
-- -----------------------------------------------------------

-- IMPORTANT: all shopee tables SHOULD in model/sale_channel/shopee on add/remove from left menu!!!

-- oc_shopee_product_version
CREATE TABLE IF NOT EXISTS `oc_shopee_product_version` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `item_id` VARCHAR(64) NOT NULL ,
  `variation_id` BIGINT(64) NOT NULL ,
  `version_name` VARCHAR(128) NULL ,
  `reserved_stock` INT(11) NULL DEFAULT '0' ,
  `product_weight` DECIMAL(15,4) NULL ,
  `original_price` DECIMAL(15,4) NULL ,
  `price` DECIMAL(15,4) NULL ,
  `discount_id` INT NULL DEFAULT '0' ,
  `create_time` VARCHAR(64) NULL DEFAULT NULL ,
  `update_time` VARCHAR(64) NULL DEFAULT NULL ,
  `sku` VARCHAR(64) NULL ,
  `stock` INT(11) NULL DEFAULT '0' ,
  `is_set_item` BOOLEAN NULL ,
  `bestme_product_id` INT(11) NULL ,
  `bestme_product_version_id` INT(11) NULL ,
  `sync_status` INT(11) NULL ,
  `status` VARCHAR(32) NULL ,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- IMPORTANT: ALWAYS create table "shopee_product" because of below alter table sql "ALTER TABLE `oc_shopee_product`..."
CREATE TABLE IF NOT EXISTS `oc_shopee_product` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `item_id` BIGINT(64) NOT NULL ,
  `shopid` BIGINT(64) NOT NULL ,
  `sku` VARCHAR(64) NULL ,
  `stock` INT(32) NULL ,
  `status` VARCHAR(32) NULL ,
  `name` VARCHAR(255) CHARACTER SET utf8 NOT NULL ,
  `description` TEXT CHARACTER SET utf8 NOT NULL ,
  `images` TEXT NOT NULL ,
  `currency` VARCHAR(32) NOT NULL ,
  `price` FLOAT(15,4) NOT NULL ,
  `original_price` FLOAT(15,4) NOT NULL ,
  `weight` FLOAT(15,4) NOT NULL ,
  `category_id` BIGINT(64) NOT NULL ,
  `has_variation` BOOLEAN NOT NULL DEFAULT FALSE ,
  `variations` TEXT CHARACTER SET utf8 NULL ,
  `attributes` TEXT CHARACTER SET utf8 NULL ,
  `bestme_id` INT(11) NULL ,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- more columns to "oc_shopee_product"
ALTER TABLE `oc_shopee_product`
    ADD `sync_status` INT(11) NULL AFTER `updated_at`,
    ADD `bestme_product_version_id` INT(11) NULL AFTER `sync_status`;

-- -----------------------------------------------------------
-- ----------- end v2.8.1 Shopee advance product -------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------ start v2.8.2 Transport Status Management -----------
-- -----------------------------------------------------------

ALTER TABLE `oc_order` ADD `manual_update_status` TINYINT NOT NULL DEFAULT '0' AFTER `date_modified`;

-- -----------------------------------------------------------
-- ------ end v2.8.2 Transport Status Management -------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ---------------- start v2.8.3 tuning ----------------------
-- -----------------------------------------------------------

-- temp not use FULLTEXT
-- ALTER TABLE `oc_product_description`
--   ADD FULLTEXT(`name`);

ALTER TABLE `oc_blog_description`
  ADD `type` VARCHAR (255) NOT NULL DEFAULT 'image' AFTER `alt`,
  ADD `video_url` TEXT NULL AFTER `type`;

-- -----------------------------------------------------------
-- ------------------ end v2.8.3 tuning ----------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------------ start v2.8.4 Pos return goods ----------------
-- -----------------------------------------------------------

-- oc_return_receipt
CREATE TABLE IF NOT EXISTS `oc_return_receipt` (
  `return_receipt_id` INT(11) NOT NULL AUTO_INCREMENT,
  `order_id` INT(11) NOT NULL,
  `return_receipt_code` VARCHAR(50) NOT NULL,
  `return_fee` DECIMAL(18,4) NULL DEFAULT NULL,
  `total` DECIMAL(18,4) NULL DEFAULT NULL,
  `note` VARCHAR(255) NULL DEFAULT NULL,
  `date_added` DATETIME NOT NULL,
  PRIMARY KEY (`return_receipt_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- oc_return_product
CREATE TABLE IF NOT EXISTS `oc_return_product` (
  `return_product_id` INT(11) NOT NULL AUTO_INCREMENT,
  `return_receipt_id` INT(11) NOT NULL,
  `product_id` INT(11) NOT NULL,
  `product_version_id` INT(11) NULL DEFAULT '0',
  `price` DECIMAL(18,4) NULL DEFAULT NULL,
  `quantity` INT(4) NOT NULL,
  PRIMARY KEY (`return_product_id`) USING BTREE
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

-- add column return_receipt_id to table oc_payment_voucher
ALTER TABLE `oc_payment_voucher`
  ADD COLUMN `return_receipt_id` INT(11) NULL DEFAULT NULL AFTER `store_receipt_id`;

-- -----------------------------------------------------------
-- ------------- end v2.8.4 Pos return goods -----------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ---------------- start v2.9.1 Bestme package --------------
-- -----------------------------------------------------------

-- update more detail of current paid packet in oc_setting
INSERT INTO `oc_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
('0','config','config_packet_capacity','',0),
('0','config','config_packet_number_of_stores','',0),
('0','config','config_packet_number_of_staffs','',0),
('0','config','config_packet_unlimited_capacity','1',0),
('0','config','config_packet_unlimited_stores','1',0),
('0','config','config_packet_unlimited_staffs','1',0);

-- add size to table oc_images
ALTER TABLE `oc_images` ADD `size` FLOAT (5)  NOT NULL DEFAULT 0 AFTER `source_id`;

-- -----------------------------------------------------------
-- ---------------- end v2.9.1 Bestme package ----------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ---------------- start v2.10.1 image folder --------------
-- -----------------------------------------------------------

CREATE TABLE IF NOT EXISTS `oc_image_folder` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `oc_images`
    ADD COLUMN `folder_path` INT(11) NOT NULL DEFAULT 0 AFTER `size`;

-- -----------------------------------------------------------
-- ---------------- end v2.10.1 image folder --------------

-- -----------------------------------------------------------
-- ---------------- start v2.9.2 Advance Report --------------

ALTER TABLE `oc_report_product`
    ADD `cost_price` DECIMAL(18,4) NULL DEFAULT '0.0000' AFTER `total_amount`,
    ADD `discount` DECIMAL(18,4) NOT NULL DEFAULT '0.0000' AFTER `quantity`,
    ADD `return_amount` DECIMAL(18,4) NULL DEFAULT '0.0000' AFTER `discount`,
    ADD `quantity_return` INT(11) NULL DEFAULT '0' AFTER `return_amount`;

ALTER TABLE `oc_report_order`
    ADD `shipping_fee`DECIMAL(18,4) NOT NULL DEFAULT '0.0000' AFTER `total_amount`,
    ADD `discount` DECIMAL(18,4) NOT NULL DEFAULT '0.0000' AFTER `shipping_fee`,
    ADD `source` VARCHAR(50) NULL DEFAULT NULL AFTER `order_status`,
    ADD `total_return` DECIMAL(18,4) NOT NULL DEFAULT '0.0000' AFTER `total_amount`;

-- rename value 'Chi phí quản lý cửa hàng'
UPDATE `oc_payment_voucher_type` SET `name` = 'Chi phí thuế TNDN'
    WHERE `payment_voucher_type_id` = 7
    AND `language_id` = 2;

-- rename value 'Store management cost'
UPDATE `oc_payment_voucher_type` SET `name` = 'Enterprise income tax expenses'
    WHERE `payment_voucher_type_id` = 7
    AND `language_id` = 1;

INSERT INTO `oc_event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES
   ('update_report_when_create_return_receipt', 'admin/model/sale/return_receipt/addReturnReceipt/after', 'event/report_order/afterCreateReturnReceipt', 1, 0);
-- -----------------------------------------------------------
-- ---------------- end v2.9.2 Advance Report ----------------

-- -----------------------------------------------------------
-- ----------- start v2.9.2-advance-report-xon-3448 ----------

ALTER TABLE `oc_return_receipt`
  ADD `store_id` INT(11) NULL AFTER `note`;

-- -----------------------------------------------------------
-- ----------- end v2.9.2-advance-report-xon-3448 ------------

-- -----------------------------------------------------------
-- ------------ start v2.10.2 Tuning ---------------------------
-- -----------------------------------------------------------

-- -- Custom filters by product attributes
-- oc_attribute_filter
CREATE TABLE IF NOT EXISTS `oc_attribute_filter` (
    `attribute_filter_id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` TEXT NOT NULL COLLATE 'utf8_general_ci',
    `value` TEXT NOT NULL COLLATE 'utf8_general_ci',
    `status` TINYINT(1) NOT NULL DEFAULT 0,
    `language_id` INT(11) NOT NULL,
    PRIMARY KEY (`attribute_filter_id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

-- oc_product_attribute_filter
CREATE TABLE IF NOT EXISTS `oc_product_attribute_filter` (
    `product_id` INT(11) NOT NULL,
    `attribute_filter_id` INT(11) NOT NULL,
    `attribute_filter_value` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',
    PRIMARY KEY (`product_id`, `attribute_filter_id`, `attribute_filter_value`) USING BTREE
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

-- Alter oc_blog_description add seo_keywords
ALTER TABLE `oc_blog_description`
    ADD `seo_keywords` VARCHAR(500) NULL DEFAULT NULL AFTER `meta_description`;

-- -----------------------------------------------------------
-- ------------ end v2.10.2 Tuning ---------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ---------------- start v2.10.3 - shopee-upload-product ----
-- -----------------------------------------------------------

-- insert column version in talbe oc_migration
ALTER TABLE `oc_migration`
  ADD `version` INT(11) DEFAULT 1 NULL AFTER `date_run`;

-- table cache data for Shopee's categories
CREATE TABLE IF NOT EXISTS `oc_shopee_category` (
  `shopee_category_id` BIGINT(64) NOT NULL AUTO_INCREMENT ,
  `parent_id` BIGINT(64) NOT NULL ,
  `name_vi` VARCHAR(255) NULL ,
  `name_en` VARCHAR(255) NULL ,
  `has_children` BOOLEAN NOT NULL ,
  PRIMARY KEY (`shopee_category_id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- table cache data for Shopee's logistics
CREATE TABLE IF NOT EXISTS `oc_shopee_logistics` (
  `logistic_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `logistic_name` VARCHAR(255) NULL ,
  `enabled` BOOLEAN NULL ,
  `fee_type` VARCHAR(255) NULL ,
  `sizes` TEXT NULL ,
  `item_max_weight` DECIMAL(15,8) NULL ,
  `item_min_weight` DECIMAL(15,8) NULL ,
  `height` DECIMAL(15,8) NULL ,
  `width` DECIMAL(15,8) NULL ,
  `length` DECIMAL(15,8) NULL ,
  `volumetric_factor` INT(11) NULL ,
  PRIMARY KEY (`logistic_id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- table `oc_shopee_upload_product`
CREATE TABLE IF NOT EXISTS `oc_shopee_upload_product` (
   `shopee_upload_product_id` INT(11) NOT NULL AUTO_INCREMENT ,
   `bestme_product_id` INT(11) NULL ,
  `name` VARCHAR(255) NOT NULL ,
  `description` TEXT NOT NULL ,
  `category_id` BIGINT(64) NULL ,
  `version_attribute` TEXT NULL,
  `image` VARCHAR(255) NULL ,
  `price` DECIMAL(18,4) NOT NULL ,
  `stock` INT(11) NOT NULL ,
  `sku` VARCHAR(255) NULL ,
  `common_price` DECIMAL(18,4)  NULL ,
  `common_stock` INT(11) NULL ,
  `common_sku` VARCHAR(255)  NULL ,
  `weight` INT(11) NULL ,
  `length` INT(11) NULL ,
  `width` INT(11) NULL ,
  `height` INT(11) NULL ,
  `date_added` TIMESTAMP NULL ,
  `date_modify` TIMESTAMP NULL ,
  PRIMARY KEY (`shopee_upload_product_id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- table `oc_shopee_upload_product_version`
CREATE TABLE IF NOT EXISTS `oc_shopee_upload_product_version` (
  `shopee_upload_product_version_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `shopee_upload_product_id` INT(11) NOT NULL ,
  `version_name` VARCHAR(255) NOT NULL ,
  `version_price` DECIMAL(18,4) NOT NULL,
  `version_stock` INT(11) NOT NULL ,
  `version_sku` VARCHAR(255) NULL ,
  `variation_id` BIGINT(32) NULL ,
  PRIMARY KEY (`shopee_upload_product_version_id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- table `oc_shopee_upload_product_image`
CREATE TABLE IF NOT EXISTS `oc_shopee_upload_product_image` (
  `shopee_upload_product_image_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `product_id` INT(11) NOT NULL ,
  `image` VARCHAR(255) NULL ,
  `sort_order` INT(11) NULL ,
  PRIMARY KEY (`shopee_upload_product_image_id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- table `oc_shopee_upload_product_attribute`
CREATE TABLE IF NOT EXISTS `oc_shopee_upload_product_attribute` (
  `shopee_upload_product_attribute_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `product_id` INT(11) NOT NULL ,
  `attribute_id` INT(11) NOT NULL ,
  `value` VARCHAR(255) NULL ,
  PRIMARY KEY (`shopee_upload_product_attribute_id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- table `oc_shopee_upload_product_logistic`
CREATE TABLE IF NOT EXISTS `oc_shopee_upload_product_logistic` (
  `shopee_upload_product_logistic_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `logistic_id` INT(11) NOT NULL ,
  `product_id` INT(11) NOT NULL ,
  `enabled` BOOLEAN NOT NULL ,
  PRIMARY KEY (`shopee_upload_product_logistic_id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- table `oc_shopee_upload_product_shop_update_result`
CREATE TABLE IF NOT EXISTS `oc_shopee_upload_product_shop_update_result` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `shopee_upload_product_id` INT(11) NULL ,
  `shopee_shop_id` BIGINT(64) NULL ,
  `shopee_item_id` BIGINT(64) NULL ,
  `status` TINYINT(4) NULL ,
  `in_process` TINYINT(2) NULL DEFAULT '0' ,
  `error_code` VARCHAR(255) NULL ,
  `error_msg` VARCHAR(255) NULL ,
  `variation_result` TEXT NULL ,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;

-- -----------------------------------------------------------
-- ---------------- end v2.10.3-shopee-upload-product ------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------------ start v2.11.2 Tuning -------------------------
-- -----------------------------------------------------------

-- Config product version images
-- product_version
ALTER TABLE `oc_product_version`
    ADD COLUMN `image` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci' AFTER `deleted`,
    ADD COLUMN `image_alt` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci' AFTER `image`;

-- add table oc_product_description_tab
CREATE TABLE `oc_product_description_tab` (
      `product_description_tab_id` int(11) NOT NULL AUTO_INCREMENT,
      `product_id` int(11) NOT NULL,
      `title` varchar(255) COLLATE utf8_general_ci NOT NULL,
      `description` text COLLATE utf8_general_ci NOT NULL,
      PRIMARY KEY (`product_description_tab_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- add table oc_rate_website
CREATE TABLE IF NOT EXISTS `oc_rate_website` (
    `rate_id` int(11) NOT NULL AUTO_INCREMENT,
    `status` tinyint(4) NOT NULL,
    `customer_name` varchar(255) NOT NULL,
    `sub_info` text,
    `content` text,
    `image` varchar(255) DEFAULT NULL,
    `alt` varchar(255) DEFAULT NULL,
    `date_added` datetime NOT NULL,
    `date_modified` datetime NOT NULL,
    PRIMARY KEY (`rate_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- add new column to oc_shopee_shop_config
ALTER TABLE `oc_shopee_shop_config`
  ADD `order_sync_range` INT(11) NULL DEFAULT '15' AFTER `connected`,
  ADD `allow_update_stock_product` TINYINT(2) NULL DEFAULT '0' AFTER `order_sync_range`,
  ADD `allow_update_price_product` TINYINT(2) NULL DEFAULT '0' AFTER `allow_update_stock_product`,
  ADD `last_sync_time_product` TIMESTAMP NULL AFTER `allow_update_price_product`,
  ADD `last_sync_time_order` TIMESTAMP NULL AFTER `last_sync_time_product`;

-- add new column to oc_lazada_shop_config
ALTER TABLE `oc_lazada_shop_config`
  ADD `order_sync_range` INT(11) NULL DEFAULT '15' AFTER `connected`,
  ADD `allow_update_stock_product` TINYINT(2) NULL DEFAULT '0' AFTER `order_sync_range`,
  ADD `allow_update_price_product` TINYINT(2) NULL DEFAULT '0' AFTER `allow_update_stock_product`,
  ADD `last_sync_time_product` TIMESTAMP NULL AFTER `allow_update_price_product`,
  ADD `last_sync_time_order` TIMESTAMP NULL AFTER `last_sync_time_product`;

-- -----------------------------------------------------------
-- ------------ end v2.11.2 Tuning ---------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------------ start v3.1.3 Generate demo data Website ------
-- -----------------------------------------------------------

INSERT INTO `oc_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'config', 'config_use_demo_data', '1', 0),
(0, 'online_store_config', 'online_store_config_maxlead_code', '', 0);

-- -----------------------------------------------------------
-- ---------------- end v3.1.3 Generate demo data Website ----
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------------ start v3.3.1 Advance Staff Permission ------
-- -----------------------------------------------------------

-- Add table user_store
CREATE TABLE IF NOT EXISTS `oc_user_store` (
        `id` INT(11) NOT NULL AUTO_INCREMENT ,
        `user_id` INT(11) NOT NULL ,
        `store_id` VARCHAR(255) NULL ,
        PRIMARY KEY (`id`)) ENGINE = MyISAM
        CHARSET=utf8 COLLATE utf8_general_ci;

-- Add access_all column in user_group table
ALTER TABLE `oc_user_group`
	ADD COLUMN `access_all` TINYINT NULL DEFAULT '1' AFTER `permission`;

-- Add user_create_id for multi table
ALTER TABLE `oc_customer`
	ADD COLUMN `user_create_id` INT(11) NULL DEFAULT '1' AFTER `staff_in_charge`;

ALTER TABLE `oc_customer_group`
	ADD COLUMN `user_create_id` INT(11) NULL DEFAULT '1' AFTER `customer_group_code`;

ALTER TABLE `oc_manufacturer`
	ADD COLUMN `user_create_id` INT(11) NULL DEFAULT '1' AFTER `status`;

ALTER TABLE `oc_collection`
	ADD COLUMN `user_create_id` INT(11) NULL DEFAULT '1' AFTER `type`;

ALTER TABLE `oc_product`
	ADD COLUMN `user_create_id` INT(11) NULL DEFAULT '1' AFTER `default_store_id`;

ALTER TABLE `oc_return_receipt`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `note`;

ALTER TABLE `oc_payment_voucher`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `note`;

ALTER TABLE `oc_receipt_voucher`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `note`;

ALTER TABLE `oc_store_receipt`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `status`;

ALTER TABLE `oc_store_transfer_receipt`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `total`;

ALTER TABLE `oc_store_take_receipt`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `difference_amount`;

ALTER TABLE `oc_cost_adjustment_receipt`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `note`;

ALTER TABLE `oc_discount`
    ADD `user_create_id` INT(11) NULL DEFAULT '1' AFTER `end_at`;


-- -----------------------------------------------------------
-- ------------ end v3.3.1 Advance Staff Permission ------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------------ start v3.4.2 Sync bestme social --------------
-- -----------------------------------------------------------

ALTER TABLE `oc_customer` ADD `is_authenticated` TINYINT(1) NOT NULL DEFAULT '1' AFTER `customer_source`;

ALTER TABLE `oc_customer` CHANGE `customer_source` `customer_source` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '1: NovaonX Social, 2: Facebook, 3: Bestme';

ALTER TABLE `oc_customer` ADD `subscriber_id` VARCHAR(255) NULL AFTER `user_create_id`;

CREATE TABLE `oc_order_channel` (
  `order_id` INT(11) NOT NULL ,
  `channel_id` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`order_id`, `channel_id`)
) ENGINE = MyISAM;

CREATE TABLE `oc_order_campaign` (
  `order_id` INT(11) NOT NULL ,
  `campaign_id` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`order_id`, `campaign_id`)
) ENGINE = MyISAM;

CREATE TABLE `oc_campaign_voucher` (
  `campaign_voucher_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `voucher_code` VARCHAR(255) NULL DEFAULT NULL ,
  `voucher_type` INT(11) NULL COMMENT '1: percent. 2: amount' ,
  `amount` DECIMAL(18,4) NULL DEFAULT 0.000 ,
  `user_create_id` INT(11) NULL ,
  `start_at` TIMESTAMP NULL DEFAULT NULL ,
  `end_at` TIMESTAMP NULL DEFAULT NULL ,
  `date_added` TIMESTAMP NULL DEFAULT NULL ,
  `date_modified` TIMESTAMP NULL DEFAULT NULL ,
  PRIMARY KEY (`campaign_voucher_id`)
) ENGINE = MyISAM;

ALTER TABLE `oc_order_product` ADD `campaign_voucher_id` INT(11) NULL DEFAULT NULL AFTER `tax`;

ALTER TABLE `oc_campaign_voucher` ADD `order_id` INT(11) NULL DEFAULT NULL AFTER `campaign_voucher_id`;

-- -----------------------------------------------------------
-- ------------ end v3.4.2 Sync bestme social ----------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------------ start XOCF-192 -------------------------------
-- -----------------------------------------------------------

ALTER TABLE `oc_order` ADD `collection_amount` DECIMAL(18,4) NULL DEFAULT NULL AFTER `total`;

UPDATE `oc_order` SET `collection_amount` = `total` WHERE `collection_amount` IS NULL;

-- -----------------------------------------------------------
-- ------------ end XOCF-192 ---------------------------------
-- -----------------------------------------------------------

-- -----------------------------------------------------------
-- ------------ start v3.5.1 Open Api ------------------------
-- -----------------------------------------------------------

-- create table `oc_keyword_sync`
CREATE TABLE `oc_keyword_sync` (
  `keyword_id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` text COLLATE utf8_unicode_ci NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (keyword_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- add `source` column to tables: `product`, `manufacturer`, `blog`, `blog_category`, `page_contents`, `discount`, `category`
ALTER TABLE `oc_product` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `user_create_id`;
ALTER TABLE `oc_manufacturer` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `user_create_id`;
ALTER TABLE `oc_blog` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `demo`;
ALTER TABLE `oc_blog_category` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `status`;
ALTER TABLE `oc_page_contents` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `seo_description`;
ALTER TABLE `oc_discount` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `updated_at`;
ALTER TABLE `oc_category` ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `status`;

-- add `source_author_name` column to `blog_description` table
ALTER TABLE `oc_blog_description` ADD `source_author_name` VARCHAR(255) NULL DEFAULT NULL AFTER `video_url`;

-- -----------------------------------------------------------
-- ------------ end v3.5.1 Open Api ------------------------
-- -----------------------------------------------------------

-- -------------- start XOCF-264 -------------------------------
CREATE TABLE `oc_email_subscribers` (
    `email_id` int(11) NOT NULL AUTO_INCREMENT,
    `email` text COLLATE utf8_unicode_ci NOT NULL,
    `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (email_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
-- -------------- end XOCF-264 -------------------------------

-- -----------------v3.6.2-agency-brands----------------------
ALTER TABLE `oc_product` ADD `brand_shop_name` VARCHAR(255) NULL DEFAULT NULL AFTER `source`;
-- -----------------End v3.6.2-agency-brands------------------

--
-- TODO: Migrate by NOVAON from here...
--

-- -----------------------------------------------------------
-- --------Always at the end of file -------------------------
-- -----------------------------------------------------------

-- --------Load theme default data ---------------------------
-- TODO: call in cli_install?...

--
-- v1.5.2 Smart loading demo data base on theme group (theme type)
-- Note: following content is copied from file "library/theme_config/demo_data/tech.sql" for default
--
-- 2020/04: More support loading demo data from special theme. If no special theme defined, use theme group as above
-- Note: following content may be copied from file "library/theme_config/demo_data/special_shop/xxx.sql" if special file defined

-- 2020/07/30: change theme demo to adg_topal_55.
-- Note: following content is copied from file "library/theme_config/demo_data/special_shop/adg_topal_55.sql"
--

-- oc_category PRIMARY KEY (`category_id`)
INSERT IGNORE INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `source`, `date_added`, `date_modified`) VALUES
(33, NULL, 0, 0, 0, 0, 1, NULL, '2021-11-09 16:17:59', '2021-11-09 16:17:59'),
(32, NULL, 0, 0, 0, 0, 1, NULL, '2021-11-08 15:39:56', '2021-11-08 15:39:56');

-- oc_category_description PRIMARY KEY (`category_id`,`language_id`)
INSERT IGNORE INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(32, 2, 'demo', 'demo', '', '', ''),
(32, 1, 'demo', 'demo', '', '', ''),
(33, 2, 'loại 1', 'loại 1', '', '', ''),
(33, 1, 'loại 1', 'loại 1', '', '', '');

-- oc_category_path PRIMARY KEY (`category_id`,`path_id`)
INSERT IGNORE INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(33, 33, 0),
(32, 32, 0);

-- oc_category_to_store PRIMARY KEY (`category_id`,`store_id`)
INSERT IGNORE INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(32, 0),
(33, 0);

-- oc_collection PRIMARY KEY (`collection_id`)
-- INSERT IGNORE INTO `oc_collection` (`collection_id`, `title`, `sort_order`, `status`, `product_type_sort`, `type`) VALUES


-- oc_collection_description PRIMARY KEY (`collection_id`)
-- INSERT IGNORE INTO `oc_collection_description` (`collection_id`, `image`, `description`, `meta_title`, `meta_description`, `alias`) VALUES

-- oc_product PRIMARY KEY (`product_id`)
-- delete first
INSERT IGNORE INTO `oc_product` (`product_id`, `model`, `sku`, `common_barcode`, `common_sku`, `common_compare_price`, `common_price`, `common_cost_price`, `barcode`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `sale_on_out_of_stock`, `stock_status_id`, `image`, `image_alt`, `multi_versions`, `manufacturer_id`, `shipping`, `price`, `price_currency_id`, `compare_price`, `c_price_currency_id`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `channel`, `viewed`, `demo`, `deleted`, `default_store_id`, `user_create_id`, `source`, `brand_shop_name`, `date_added`, `date_modified`) VALUES
(136, '', '', '', '', 0.0000, 0.0000, 0.0000, '', '', '', '', '', '', '', 0, 1, 0, 'https://cdn.bestme.asia/images/adgtopal55/project-5.jpg', '', 0, 0, 1, 199000.0000, 1, 200000.0000, 1, 0, 0, '0000-00-00', 1.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 0, 1, 1, 0, 0, 0, 0, 1, NULL, 0, 1, 'admin', NULL, '2021-11-09 16:12:27', '2021-11-09 17:31:54'),
(135, '', '', '', '', 0.0000, 0.0000, 0.0000, '', '', '', '', '', '', '', 0, 0, 0, 'https://cdn.bestme.asia/images/adgtopalslima/product-3.jpg', '', 0, 0, 1, 0.0000, 1, 103000.0000, 1, 0, 0, '0000-00-00', 100.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, 'excel', NULL, '2021-11-08 15:39:57', '2021-12-02 15:30:00'),
(134, '', '', '', '', 0.0000, 0.0000, 0.0000, '', '', '', '', '', '', '', 0, 0, 0, 'https://cdn.bestme.asia/images/adgtopalslima/product-1.jpg', '', 0, 0, 1, 0.0000, 1, 103000.0000, 1, 0, 0, '0000-00-00', 100.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, 'excel', NULL, '2021-11-08 15:39:57', '2021-12-02 15:30:02'),
(133, '', '', '', '', 0.0000, 0.0000, 0.0000, '', '', '', '', '', '', '', 0, 0, 0, 'https://cdn.bestme.asia/images/adgtopalslima/product-4.jpg', '', 0, 0, 1, 0.0000, 1, 103000.0000, 1, 0, 0, '0000-00-00', 100.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, 'excel', NULL, '2021-11-08 15:39:57', '2021-12-02 15:28:17'),
(132, '', '', '', '', 0.0000, 0.0000, 0.0000, '', '', '', '', '', '', '', 0, 0, 0, 'https://cdn.bestme.asia/images/adgtopalslima/product-2.jpg', '', 0, 0, 1, 0.0000, 1, 103000.0000, 1, 0, 0, '0000-00-00', 100.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 0, 1, 1, 0, 1, 2, 0, 1, NULL, 0, 1, 'excel', NULL, '2021-11-08 15:39:56', '2021-12-02 15:29:59');

-- restore soft-deleted products if demo
UPDATE `oc_product` SET `deleted` = NULL
WHERE `demo` = 1
  AND `product_id` IN (
       136,
       135,
       134,
       133,
       132
    );

-- oc_product_collection PRIMARY KEY (`product_collection_id`)
-- INSERT IGNORE INTO `oc_product_collection` (`product_collection_id`, `product_id`, `collection_id`, `sort_order`) VALUES

-- oc_product_description PRIMARY KEY (`product_id`,`language_id`)
INSERT IGNORE INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `sub_description`, `seo_title`, `seo_description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(132, 2, 'Cửa nhôm Topal', '&lt;div class=&quot;tab-content py-30&quot; id=&quot;productDetailTabContent&quot;&gt;\r\n&lt;div aria-labelledby=&quot;info-tab&quot; class=&quot;tab-pane fade show active&quot; id=&quot;proDesc&quot; role=&quot;tabpanel&quot;&gt;\r\n&lt;h2 class=&quot;fz-20 mb-3 fw-500&quot;&gt;Đạt tiêu chuẩn chống cháy quốc tế&lt;/h2&gt;\r\n\r\n&lt;p&gt;Cứng vững, bền bỉ&lt;/p&gt;\r\n\r\n&lt;p&gt;Thiết kế thân cửa với bản nan to (cao 70mm) cửa được làm từ vật liệu cao cấp nhôm 6065 T5 2 lớp có độ dày lên tới 1.5mm và chân gài giúp các nan liên kết với nhau vững chắc hơn và chịu được áp lực gió lớn. Bên cạnh đó, bọ nhựa được thiết kế ở vị trí đầu nan giúp các nan cửa chống xô lệch trong quá trình đóng mở. Vận hành êm ái&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row my-3&quot;&gt;\r\n&lt;div class=&quot;col-12 col-md-6 mb-3&quot;&gt;&lt;img class=&quot;rounded-5&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/chi-tiet-sp2.jpg&quot; width=&quot;100%&quot; /&gt;&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col-12 col-md-6 mb-3&quot;&gt;&lt;img class=&quot;rounded-5&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/chi-tiet-sp3.jpg&quot; width=&quot;100%&quot; /&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;h2 class=&quot;fz-20 mb-3 fw-500&quot;&gt;Các thiết bị điều khiển chống cháy&lt;/h2&gt;\r\n\r\n&lt;p&gt;Cửa cuốn Nan nhôm Mega hạn chế tối đa tiếng ồn khi vận hành nhờ các nan PC được cải tiến có gioăng lông giảm chấn, hệ thống Pully P270 đường kính lớn với bề mặt được bọc cao su công nghiệp giúp giảm thiểu siết nan và triệt tiêu tiếng ồn. Ray nhôm sử dụng gioăng lông chống xước nan và giúp cửa vận hành êm ái hơn. Đảm bảo an toàn&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row my-3&quot;&gt;\r\n&lt;div class=&quot;col-12 col-md-6 mb-3&quot;&gt;&lt;img class=&quot;rounded-5&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/chi-tiet-sp4.png&quot; width=&quot;100%&quot; /&gt;&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col-12 col-md-6 mb-3&quot;&gt;&lt;img class=&quot;rounded-5&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/chi-tiet-sp5.png&quot; width=&quot;100%&quot; /&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;h2 class=&quot;fz-20 mb-3 fw-500&quot;&gt;Thân cửa&lt;/h2&gt;\r\n\r\n&lt;p&gt;Cửa cuốn Nan nhôm Mega được tích hợp công nghệ bảo vệ 3 cấp độ: hệ thống đảo chiều không dây, hệ thống sensor hồng ngoại và rơ le chống xổ lô đảm bảo an toàn cho người sử dụng khi cửa gặp chướng ngại vật.Điều khiển từ xa với công nghệ mã nhảy tạo ra hàng tỷ mã không lặp lại, kẻ trộm không thể sao chép mã mở cửa. Bộ cửa còn có khả năng kết nối các phụ kiện ngoại vi như: còi báo động, cảm biển khói, thiết bị kết nối smartphone… đảm bảo an ninh cho ngôi nhà.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img class=&quot;rounded-5&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/chi-tiet-sp1.jpg&quot; width=&quot;100%&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div aria-labelledby=&quot;techInfo-tab&quot; class=&quot;tab-pane fade&quot; id=&quot;techInfo&quot; role=&quot;tabpanel&quot;&gt;\r\n&lt;table class=&quot;table table-bordered table-striped&quot;&gt;\r\n	&lt;tbody&gt;\r\n		&lt;tr&gt;\r\n			&lt;th colspan=&quot;2&quot;&gt;THÔNG SỐ&lt;/th&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td width=&quot;35%&quot;&gt;Kích thước&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Màu sắc&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Chất liệu&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Bề mặt sơn&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Ray&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n	&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;', '&lt;p&gt;Với thân cửa được ghép từ các nan nhôm bản lớn sở hữu thiết kế lỗ thoáng cải tiến, cửa cuốn Nan nhôm Mega không những mang đến vẻ đẹp hiện đại cho mặt tiền mà còn đảm bảo khả năng đối lưu không khí, giúp cho tầng một ngôi nhà luôn thông thoáng.&lt;/p&gt;', '', '', '', '', '', ''),
(134, 2, 'Cửa cuốn Austdoor', '&lt;div class=&quot;tab-content py-30&quot; id=&quot;productDetailTabContent&quot;&gt;\r\n&lt;div aria-labelledby=&quot;info-tab&quot; class=&quot;tab-pane fade show active&quot; id=&quot;proDesc&quot; role=&quot;tabpanel&quot;&gt;\r\n&lt;h2 class=&quot;fz-20 mb-3 fw-500&quot;&gt;Đạt tiêu chuẩn chống cháy quốc tế&lt;/h2&gt;\r\n\r\n&lt;p&gt;Cứng vững, bền bỉ&lt;/p&gt;\r\n\r\n&lt;p&gt;Thiết kế thân cửa với bản nan to (cao 70mm) cửa được làm từ vật liệu cao cấp nhôm 6065 T5 2 lớp có độ dày lên tới 1.5mm và chân gài giúp các nan liên kết với nhau vững chắc hơn và chịu được áp lực gió lớn. Bên cạnh đó, bọ nhựa được thiết kế ở vị trí đầu nan giúp các nan cửa chống xô lệch trong quá trình đóng mở. Vận hành êm ái&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row my-3&quot;&gt;\r\n&lt;div class=&quot;col-12 col-md-6 mb-3&quot;&gt;&lt;img class=&quot;rounded-5&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/chi-tiet-sp2.jpg&quot; width=&quot;100%&quot; /&gt;&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col-12 col-md-6 mb-3&quot;&gt;&lt;img class=&quot;rounded-5&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/chi-tiet-sp3.jpg&quot; width=&quot;100%&quot; /&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;h2 class=&quot;fz-20 mb-3 fw-500&quot;&gt;Các thiết bị điều khiển chống cháy&lt;/h2&gt;\r\n\r\n&lt;p&gt;Cửa cuốn Nan nhôm Mega hạn chế tối đa tiếng ồn khi vận hành nhờ các nan PC được cải tiến có gioăng lông giảm chấn, hệ thống Pully P270 đường kính lớn với bề mặt được bọc cao su công nghiệp giúp giảm thiểu siết nan và triệt tiêu tiếng ồn. Ray nhôm sử dụng gioăng lông chống xước nan và giúp cửa vận hành êm ái hơn. Đảm bảo an toàn&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row my-3&quot;&gt;\r\n&lt;div class=&quot;col-12 col-md-6 mb-3&quot;&gt;&lt;img class=&quot;rounded-5&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/chi-tiet-sp4.png&quot; width=&quot;100%&quot; /&gt;&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col-12 col-md-6 mb-3&quot;&gt;&lt;img class=&quot;rounded-5&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/chi-tiet-sp5.png&quot; width=&quot;100%&quot; /&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;h2 class=&quot;fz-20 mb-3 fw-500&quot;&gt;Thân cửa&lt;/h2&gt;\r\n\r\n&lt;p&gt;Cửa cuốn Nan nhôm Mega được tích hợp công nghệ bảo vệ 3 cấp độ: hệ thống đảo chiều không dây, hệ thống sensor hồng ngoại và rơ le chống xổ lô đảm bảo an toàn cho người sử dụng khi cửa gặp chướng ngại vật.Điều khiển từ xa với công nghệ mã nhảy tạo ra hàng tỷ mã không lặp lại, kẻ trộm không thể sao chép mã mở cửa. Bộ cửa còn có khả năng kết nối các phụ kiện ngoại vi như: còi báo động, cảm biển khói, thiết bị kết nối smartphone… đảm bảo an ninh cho ngôi nhà.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img class=&quot;rounded-5&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/chi-tiet-sp1.jpg&quot; width=&quot;100%&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div aria-labelledby=&quot;techInfo-tab&quot; class=&quot;tab-pane fade&quot; id=&quot;techInfo&quot; role=&quot;tabpanel&quot;&gt;\r\n&lt;table class=&quot;table table-bordered table-striped&quot;&gt;\r\n	&lt;tbody&gt;\r\n		&lt;tr&gt;\r\n			&lt;th colspan=&quot;2&quot;&gt;THÔNG SỐ&lt;/th&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td width=&quot;35%&quot;&gt;Kích thước&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Màu sắc&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Chất liệu&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Bề mặt sơn&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Ray&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n	&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;', '&lt;p&gt;Với thân cửa được ghép từ các nan nhôm bản lớn sở hữu thiết kế lỗ thoáng cải tiến, cửa cuốn Nan nhôm Mega không những mang đến vẻ đẹp hiện đại cho mặt tiền mà còn đảm bảo khả năng đối lưu không khí, giúp cho tầng một ngôi nhà luôn thông thoáng.&lt;/p&gt;', '', '', '', '', '', ''),
(133, 2, 'Cửa cuốn nan nhôm', '&lt;div class=&quot;tab-content py-30&quot; id=&quot;productDetailTabContent&quot;&gt;\r\n&lt;div aria-labelledby=&quot;info-tab&quot; class=&quot;tab-pane fade show active&quot; id=&quot;proDesc&quot; role=&quot;tabpanel&quot;&gt;\r\n&lt;h2 class=&quot;fz-20 mb-3 fw-500&quot;&gt;Đạt tiêu chuẩn chống cháy quốc tế&lt;/h2&gt;\r\n\r\n&lt;p&gt;Cứng vững, bền bỉ&lt;/p&gt;\r\n\r\n&lt;p&gt;Thiết kế thân cửa với bản nan to (cao 70mm) cửa được làm từ vật liệu cao cấp nhôm 6065 T5 2 lớp có độ dày lên tới 1.5mm và chân gài giúp các nan liên kết với nhau vững chắc hơn và chịu được áp lực gió lớn. Bên cạnh đó, bọ nhựa được thiết kế ở vị trí đầu nan giúp các nan cửa chống xô lệch trong quá trình đóng mở. Vận hành êm ái&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row my-3&quot;&gt;\r\n&lt;div class=&quot;col-12 col-md-6 mb-3&quot;&gt;&lt;img class=&quot;rounded-5&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/chi-tiet-sp2.jpg&quot; width=&quot;100%&quot; /&gt;&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col-12 col-md-6 mb-3&quot;&gt;&lt;img class=&quot;rounded-5&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/chi-tiet-sp3.jpg&quot; width=&quot;100%&quot; /&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;h2 class=&quot;fz-20 mb-3 fw-500&quot;&gt;Các thiết bị điều khiển chống cháy&lt;/h2&gt;\r\n\r\n&lt;p&gt;Cửa cuốn Nan nhôm Mega hạn chế tối đa tiếng ồn khi vận hành nhờ các nan PC được cải tiến có gioăng lông giảm chấn, hệ thống Pully P270 đường kính lớn với bề mặt được bọc cao su công nghiệp giúp giảm thiểu siết nan và triệt tiêu tiếng ồn. Ray nhôm sử dụng gioăng lông chống xước nan và giúp cửa vận hành êm ái hơn. Đảm bảo an toàn&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row my-3&quot;&gt;\r\n&lt;div class=&quot;col-12 col-md-6 mb-3&quot;&gt;&lt;img class=&quot;rounded-5&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/chi-tiet-sp4.png&quot; width=&quot;100%&quot; /&gt;&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col-12 col-md-6 mb-3&quot;&gt;&lt;img class=&quot;rounded-5&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/chi-tiet-sp5.png&quot; width=&quot;100%&quot; /&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;h2 class=&quot;fz-20 mb-3 fw-500&quot;&gt;Thân cửa&lt;/h2&gt;\r\n\r\n&lt;p&gt;Cửa cuốn Nan nhôm Mega được tích hợp công nghệ bảo vệ 3 cấp độ: hệ thống đảo chiều không dây, hệ thống sensor hồng ngoại và rơ le chống xổ lô đảm bảo an toàn cho người sử dụng khi cửa gặp chướng ngại vật.Điều khiển từ xa với công nghệ mã nhảy tạo ra hàng tỷ mã không lặp lại, kẻ trộm không thể sao chép mã mở cửa. Bộ cửa còn có khả năng kết nối các phụ kiện ngoại vi như: còi báo động, cảm biển khói, thiết bị kết nối smartphone… đảm bảo an ninh cho ngôi nhà.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img class=&quot;rounded-5&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/chi-tiet-sp1.jpg&quot; width=&quot;100%&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div aria-labelledby=&quot;techInfo-tab&quot; class=&quot;tab-pane fade&quot; id=&quot;techInfo&quot; role=&quot;tabpanel&quot;&gt;\r\n&lt;table class=&quot;table table-bordered table-striped&quot;&gt;\r\n	&lt;tbody&gt;\r\n		&lt;tr&gt;\r\n			&lt;th colspan=&quot;2&quot;&gt;THÔNG SỐ&lt;/th&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td width=&quot;35%&quot;&gt;Kích thước&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Màu sắc&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Chất liệu&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Bề mặt sơn&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Ray&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n	&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;', '&lt;p&gt;Cửa cuốn chống cháy AF100 là giải pháp tối ưu cho công trình khi có sự cố hỏa hoạn. Khi xảy ra hỏa hoạn cửa cuốn chống cháy AF100 sẽ tự động đóng xuống ngăn và khoanh vùng cháy, tạo thành hành lang thoát hiểm cho con người và hàng hóa. AF100 được sản xuất theo tiêu chuẩn BS EN 16341:2000 Vương quốc Anh và được cục PCCC – Bộ Công An Việt Nam cấp chứng nhận đạt tiêu chuẩn TCXDVN 386:2007.&lt;/p&gt;', '', '', '', '', '', ''),
(135, 2, 'Cửa gỗ Huge', '&lt;div class=&quot;tab-content py-30&quot; id=&quot;productDetailTabContent&quot;&gt;\r\n&lt;div aria-labelledby=&quot;info-tab&quot; class=&quot;tab-pane fade show active&quot; id=&quot;proDesc&quot; role=&quot;tabpanel&quot;&gt;\r\n&lt;h2 class=&quot;fz-20 mb-3 fw-500&quot;&gt;Đạt tiêu chuẩn chống cháy quốc tế&lt;/h2&gt;\r\n\r\n&lt;p&gt;Cứng vững, bền bỉ&lt;/p&gt;\r\n\r\n&lt;p&gt;Thiết kế thân cửa với bản nan to (cao 70mm) cửa được làm từ vật liệu cao cấp nhôm 6065 T5 2 lớp có độ dày lên tới 1.5mm và chân gài giúp các nan liên kết với nhau vững chắc hơn và chịu được áp lực gió lớn. Bên cạnh đó, bọ nhựa được thiết kế ở vị trí đầu nan giúp các nan cửa chống xô lệch trong quá trình đóng mở. Vận hành êm ái&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row my-3&quot;&gt;\r\n&lt;div class=&quot;col-12 col-md-6 mb-3&quot;&gt;&lt;img class=&quot;rounded-5&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/chi-tiet-sp2.jpg&quot; width=&quot;100%&quot; /&gt;&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col-12 col-md-6 mb-3&quot;&gt;&lt;img class=&quot;rounded-5&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/chi-tiet-sp3.jpg&quot; width=&quot;100%&quot; /&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;h2 class=&quot;fz-20 mb-3 fw-500&quot;&gt;Các thiết bị điều khiển chống cháy&lt;/h2&gt;\r\n\r\n&lt;p&gt;Cửa cuốn Nan nhôm Mega hạn chế tối đa tiếng ồn khi vận hành nhờ các nan PC được cải tiến có gioăng lông giảm chấn, hệ thống Pully P270 đường kính lớn với bề mặt được bọc cao su công nghiệp giúp giảm thiểu siết nan và triệt tiêu tiếng ồn. Ray nhôm sử dụng gioăng lông chống xước nan và giúp cửa vận hành êm ái hơn. Đảm bảo an toàn&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row my-3&quot;&gt;\r\n&lt;div class=&quot;col-12 col-md-6 mb-3&quot;&gt;&lt;img class=&quot;rounded-5&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/chi-tiet-sp4.png&quot; width=&quot;100%&quot; /&gt;&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col-12 col-md-6 mb-3&quot;&gt;&lt;img class=&quot;rounded-5&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/chi-tiet-sp5.png&quot; width=&quot;100%&quot; /&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;h2 class=&quot;fz-20 mb-3 fw-500&quot;&gt;Thân cửa&lt;/h2&gt;\r\n\r\n&lt;p&gt;Cửa cuốn Nan nhôm Mega được tích hợp công nghệ bảo vệ 3 cấp độ: hệ thống đảo chiều không dây, hệ thống sensor hồng ngoại và rơ le chống xổ lô đảm bảo an toàn cho người sử dụng khi cửa gặp chướng ngại vật.Điều khiển từ xa với công nghệ mã nhảy tạo ra hàng tỷ mã không lặp lại, kẻ trộm không thể sao chép mã mở cửa. Bộ cửa còn có khả năng kết nối các phụ kiện ngoại vi như: còi báo động, cảm biển khói, thiết bị kết nối smartphone… đảm bảo an ninh cho ngôi nhà.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img class=&quot;rounded-5&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/chi-tiet-sp1.jpg&quot; width=&quot;100%&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div aria-labelledby=&quot;techInfo-tab&quot; class=&quot;tab-pane fade&quot; id=&quot;techInfo&quot; role=&quot;tabpanel&quot;&gt;\r\n&lt;table class=&quot;table table-bordered table-striped&quot;&gt;\r\n	&lt;tbody&gt;\r\n		&lt;tr&gt;\r\n			&lt;th colspan=&quot;2&quot;&gt;THÔNG SỐ&lt;/th&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td width=&quot;35%&quot;&gt;Kích thước&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Màu sắc&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Chất liệu&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Bề mặt sơn&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Ray&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n	&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;', '&lt;p&gt;Với thân cửa được ghép từ các nan nhôm bản lớn sở hữu thiết kế lỗ thoáng cải tiến, cửa cuốn Nan nhôm Mega không những mang đến vẻ đẹp hiện đại cho mặt tiền mà còn đảm bảo khả năng đối lưu không khí, giúp cho tầng một ngôi nhà luôn thông thoáng.&lt;/p&gt;', '', '', '', '', '', ''),
(136, 2, 'cửa sắt cuốn', '&lt;p&gt;mô tả chi tiết                         &lt;/p&gt;\r\n\r\n&lt;div aria-labelledby=&quot;techInfo-tab&quot; class=&quot;tab-pane fade active show&quot; id=&quot;techInfo&quot; role=&quot;tabpanel&quot;&gt;\r\n&lt;table class=&quot;table table-bordered table-striped&quot;&gt;\r\n	&lt;tbody&gt;\r\n		&lt;tr&gt;\r\n			&lt;th colspan=&quot;2&quot;&gt;THÔNG SỐ&lt;/th&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td width=&quot;35%&quot;&gt;Kích thước&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Màu sắc&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Chất liệu&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Bề mặt sơn&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;Ray&lt;/td&gt;\r\n			&lt;td&gt; &lt;/td&gt;\r\n		&lt;/tr&gt;\r\n	&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n&lt;br /&gt;\r\n                   &lt;/div&gt;\r\n\r\n&lt;p&gt; &lt;/p&gt;', '', '', '', '', '', '', '');

-- oc_product_image PRIMARY KEY (`product_image_id`)
-- nothing

-- oc_product_to_category PRIMARY KEY (`product_id`,`category_id`)
INSERT IGNORE INTO `oc_product_to_category` (`product_id`, `category_id`) VALUES
(132, 32),
(133, 32),
(134, 32),
(135, 32),
(136, 32),
(136, 33);

-- oc_product_to_store PRIMARY KEY (`product_id`,`store_id`)
INSERT IGNORE INTO `oc_product_to_store` (`product_id`, `store_id`, `product_version_id`, `quantity`, `cost_price`) VALUES
(136, 0, 0, 10, 0.0000),
(135, 0, 0, 100, 10000.0000),
(134, 0, 0, 100, 103000.0000),
(133, 0, 0, 100, 103000.0000),
(132, 0, 0, 100, 103000.0000);

-- oc_seo_url PRIMARY KEY (`seo_url_id`)
INSERT IGNORE INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`, `table_name`) VALUES
(2082, 0, 2, 'product_id=103', 'd-prod-giay-dup-cao-11cm', 'product'),
(2081, 0, 1, 'product_id=103', 'd-prod-giay-dup-cao-11cm', 'product'),
(2080, 0, 2, 'product_id=99', 'd-prod-giay-sandal-chien-binh-day-keo-1-ben', 'product'),
(2079, 0, 1, 'product_id=99', 'd-prod-giay-sandal-chien-binh-day-keo-1-ben', 'product'),
(2078, 0, 2, 'product_id=98', 'd-prod-dep-sandal-xoan-co-chan', 'product'),
(2077, 0, 1, 'product_id=98', 'd-prod-dep-sandal-xoan-co-chan', 'product'),
(2076, 0, 2, 'product_id=97', 'd-prod-giay-sandal-quai-ngang-anh-7-mau-1', 'product'),
(2075, 0, 1, 'product_id=97', 'd-prod-giay-sandal-quai-ngang-anh-7-mau-1', 'product'),
(2074, 0, 2, 'product_id=88', 'd-prod-giay-sandal-cao-got-quai-trong-got-kim-sa-size-34-den-40', 'product'),
(2073, 0, 1, 'product_id=88', 'd-prod-giay-sandal-cao-got-quai-trong-got-kim-sa-size-34-den-40', 'product'),
(2072, 0, 2, 'product_id=87', 'd-prod-giay-valen-vien-dinh-size-36-37-38', 'product'),
(2071, 0, 1, 'product_id=87', 'd-prod-giay-valen-vien-dinh-size-36-37-38', 'product'),
(2070, 0, 2, 'product_id=86', 'd-prod-giay-cao-got-dep-den-no', 'product'),
(2069, 0, 1, 'product_id=86', 'd-prod-giay-cao-got-dep-den-no', 'product'),
(2068, 0, 2, 'product_id=85', 'd-prod-giay-cao-got-quai-trong-phoi-dinh', 'product'),
(2067, 0, 1, 'product_id=85', 'd-prod-giay-cao-got-quai-trong-phoi-dinh', 'product'),
(2066, 0, 2, 'product_id=84', 'd-prod-giay-cao-got-got-kim-sa-xu-doc-la', 'product'),
(2065, 0, 1, 'product_id=84', 'd-prod-giay-cao-got-got-kim-sa-xu-doc-la', 'product'),
(2064, 0, 2, 'product_id=83', 'd-prod-giay-co-dien-sang-trong-mau-hong', 'product'),
(2063, 0, 1, 'product_id=83', 'd-prod-giay-co-dien-sang-trong-mau-hong', 'product'),
(2193, 0, 1, 'menu_id=13', 'menu-bo-suu-tap', ''),
(2192, 0, 2, 'menu_id=13', 'menu-bo-suu-tap', ''),
(2301, 0, 2, 'product_id=133', 'cua-cuon-nan-nhom', 'product'),
(2117, 0, 2, 'manufacturer_id=26', '', 'manufacturer'),
(2118, 0, 1, 'manufacturer_id=26', '', 'manufacturer'),
(2308, 0, 1, 'product_id=134', 'cua-cuon-austdoor', 'product'),
(2307, 0, 2, 'product_id=134', 'cua-cuon-austdoor', 'product'),
(2306, 0, 1, 'product_id=135', 'cua-go-huge', 'product'),
(2305, 0, 2, 'product_id=135', 'cua-go-huge', 'product'),
(2240, 0, 2, 'blog_category_id=2', 'thanh-tich-va-giai-thuong', 'blog_category'),
(2241, 0, 2, 'blog_category_id=3', 'projects', 'blog_category'),
(2293, 0, 2, 'blog=4', 'chuoi-du-an-vinhome-tren-toan-quoc-4000-bo-cua-cuon-nan-nhom-cua-cuon-thep-chong-chay', 'blog'),
(2298, 0, 2, 'blog=5', 'indochina-plaza-ha-noi-cua-cuon-nan-nhom-cua-cuon-trong-suot', 'blog'),
(2297, 0, 2, 'blog=6', 'nha-may-duy-tan-long-an-cua-cuon-nan-nhom-cua-nhom-topal', 'blog'),
(2296, 0, 2, 'blog=7', 'lakeside-da-nang-cua-cuon-nan-nhom-line-art-l120', 'blog'),
(2295, 0, 2, 'blog=8', 'timberland-manwah-binh-duong-cua-cuon-thep-chong-chay-cua-cuon-thep-sieu-truong-cua-cuon-khop-thoang-inox', 'blog'),
(2247, 0, 2, 'blog=9', 'du-an-thi-cong-cua-nhom-kinh-cho-he-thong-vinmart-5', 'blog'),
(2294, 0, 2, 'blog=10', 'chuoi-du-an-mapletree-toan-quoc-1000-bo-cua-cuon-thep-chong-chay-cua-cuon-thep-sieu-truong', 'blog'),
(2249, 0, 2, 'blog=11', 'du-an-thi-cong-cua-nhom-kinh-cho-he-thong-vinmart-7', 'blog'),
(2287, 0, 2, 'blog=12', 'cua-cuon-austdoor-mega-nhan-3-loi-ich-mua-ngay', 'blog'),
(2251, 0, 2, 'blog_category_id=4', 'khuyen-mai', 'blog_category'),
(2281, 0, 2, 'blog=13', '3-nhiem-vu-quan-trong-mua-cao-diem-xay-dung', 'blog'),
(2282, 0, 2, 'blog=14', 'bi-kip-lua-chon-cua-cuon-phong-thuy-austdoor', 'blog'),
(2283, 0, 2, 'blog=3', 'ung-dung-so-hoa-trong-gioi-thieu-san-pham-moi', 'blog'),
(2274, 0, 1, 'product_id=136', 'cua-sat-cuon', 'product'),
(2273, 0, 2, 'product_id=136', 'cua-sat-cuon', 'product'),
(2267, 0, 2, 'category_id=33', 'loai-1', 'category'),
(2268, 0, 1, 'category_id=33', 'loai-1', 'category'),
(2279, 0, 2, 'blog_category_id=5', 'tin-noi-bat', 'blog_category'),
(2280, 0, 2, 'blog_category_id=6', 'gioi-thieu-1', 'blog_category'),
(2284, 0, 2, 'blog=15', 'bi-kip-lua-chon-cua-cuon-phong-thuy-austdoor-1', 'blog'),
(2285, 0, 2, 'blog=16', 'ung-dung-so-hoa-trong-gioi-thieu-san-pham-moi-1', 'blog'),
(2288, 0, 2, 'blog=17', 'muon-pho-bay-net-dep-ben-trong-nhung-van-an-toan-dung-cua-cuon-gi', 'blog'),
(2289, 0, 2, 'blog=18', 'mach-ban-meo-su-dung-cua-cuon-ben-lau-mua-dich-covid-19', 'blog'),
(2290, 0, 2, 'blog=19', 'trai-nghiem-thuc-te-cua-cuon-austdoor-toc-do-cao-hs', 'blog'),
(2291, 0, 2, 'blog=20', 'austdoor-chinh-thuc-ra-mat-bo-suu-tap-cua-cuon-phong-thuy', 'blog'),
(2292, 0, 2, 'blog=21', 'dung-thuoc-nao-chuan-de-do-kich-thuoc-thong-thuy-cua-cuon', 'blog'),
(2299, 0, 2, 'menu_id=16', 'dieu-khoan', ''),
(2300, 0, 1, 'menu_id=16', 'dieu-khoan', ''),
(2302, 0, 1, 'product_id=133', 'cua-cuon-nan-nhom', 'product'),
(2304, 0, 1, 'product_id=132', 'cua-nhom-topal', 'product'),
(2303, 0, 2, 'product_id=132', 'cua-nhom-topal', 'product'),
(2228, 0, 2, 'manufacturer_id=26', '-1', 'manufacturer'),
(2229, 0, 1, 'manufacturer_id=26', '-1', 'manufacturer'),
(2230, 0, 2, 'category_id=32', 'demo', 'category'),
(2231, 0, 1, 'category_id=32', 'demo', 'category'),
(2262, 0, 2, 'blog=1', 'gioi-thieu', 'blog'),
(2286, 0, 2, 'blog=2', 'goi-y-3-cach-su-dung-tay-dieu-khien-cua-cuon-ben-lau', 'blog');

-- oc_theme_builder_config NO PRIMARY KEY
-- remove first
DELETE FROM `oc_theme_builder_config`
WHERE `store_id` = '0'
  AND `config_theme` = 'adg_topal_55'
  AND `code` = 'config';

-- insert
INSERT IGNORE INTO `oc_theme_builder_config` (`store_id`, `config_theme`, `code`, `key`, `config`, `date_added`, `date_modified`) VALUES
(0, 'adg_topal_55', 'config', 'config_theme_color', '{\n    "main": {\n        "main": {\n            "hex": "004EA4",\n            "visible": "1"\n        },\n        "button": {\n            "hex": "F58220",\n            "visible": "1"\n        }\n    }\n}', '2021-11-08 15:21:28', '2022-02-08 10:40:16'),
(0, 'adg_topal_55', 'config', 'config_theme_text', '{\n    "all": {\n        "title": "Font Roboto",\n        "font": "Roboto",\n        "supported-fonts": [\n            "Open Sans",\n            "IBM Plex Sans",\n            "Courier New",\n            "Roboto",\n            "Nunito",\n            "Arial",\n            "DejaVu Sans",\n            "Tahoma",\n            "Time News Roman",\n            "Mulish"\n        ]\n    },\n    "heading": {\n        "font": "Tahoma",\n        "supported-fonts": [\n            "Open Sans",\n            "IBM Plex Sans",\n            "Courier New",\n            "Roboto",\n            "Nunito",\n            "Arial",\n            "DejaVu Sans",\n            "Tahoma",\n            "Time News Roman",\n            "Quicksand",\n            "Montserrat",\n            "KoHo",\n            "Newsreader",\n            "Oswald",\n            "Playfair Display",\n            "Lora",\n            "Inter"\n        ],\n        "font-type": "regular",\n        "supported-font-types": [\n            "regular",\n            "bold",\n            "italic"\n        ],\n        "font-size": 32\n    },\n    "body": {\n        "font": "Aria",\n        "supported-fonts": [\n            "Open Sans",\n            "IBM Plex Sans",\n            "Courier New",\n            "Roboto",\n            "Nunito",\n            "Arial",\n            "DejaVu Sans",\n            "Tahoma",\n            "Time News Roman",\n            "Quicksand",\n            "Montserrat",\n            "KoHo",\n            "Newsreader",\n            "Oswald",\n            "Playfair Display",\n            "Lora",\n            "Inter"\n        ],\n        "font-type": "regular",\n        "supported-font-types": [\n            "regular",\n            "bold",\n            "italic"\n        ],\n        "font-size": 14\n    }\n}', '2021-11-08 15:21:28', '2022-02-08 10:40:16'),
(0, 'adg_topal_55', 'config', 'config_theme_favicon', '{\n    "image": {\n        "url": "\\/catalog\\/view\\/theme\\/default\\/image\\/favicon.ico"\n    }\n}', '2021-11-08 15:21:28', '2022-02-08 10:40:16'),
(0, 'adg_topal_55', 'config', 'config_theme_social', '[\n    {\n        "name": "facebook",\n        "text": "Facebook",\n        "url": "https:\\/\\/www.facebook.com"\n    },\n    {\n        "name": "twitter",\n        "text": "Twitter",\n        "url": "https:\\/\\/www.twitter.com"\n    },\n    {\n        "name": "instagram",\n        "text": "Instagram",\n        "url": "https:\\/\\/www.instagram.com"\n    },\n    {\n        "name": "tumblr",\n        "text": "Tumblr",\n        "url": "https:\\/\\/www.tumblr.com"\n    },\n    {\n        "name": "youtube",\n        "text": "Youtube",\n        "url": "https:\\/\\/www.youtube.com"\n    },\n    {\n        "name": "googleplus",\n        "text": "Google Plus",\n        "url": "https:\\/\\/www.plus.google.com"\n    }\n]', '2021-11-08 15:21:28', '2022-02-08 10:40:16'),
(0, 'adg_topal_55', 'config', 'config_theme_ecommerce', '[\n    {\n        "name": "shopee",\n        "text": "Shopee",\n        "url": "https:\\/\\/shopee.vn"\n    },\n    {\n        "name": "lazada",\n        "text": "Lazada",\n        "url": "https:\\/\\/www.lazada.vn"\n    },\n    {\n        "name": "tiki",\n        "text": "Tiki",\n        "url": "https:\\/\\/tiki.vn"\n    },\n    {\n        "name": "sendo",\n        "text": "Sendo",\n        "url": "https:\\/\\/www.sendo.vn"\n    }\n]', '2021-11-08 15:21:28', '2022-02-08 10:40:16'),
(0, 'adg_topal_55', 'config', 'config_theme_override_css', '{\n    "css": "\\n   \\/* override css *\\/\\n    p.title-service {\\n        text-transform: uppercase;\\n    }\\n\\n    #header .header-search input.form-control {\\n        border-left: none;\\n    }\\n    \\n    #header .header-search .collapse-search a .icon {\\n        transform: translateY(4px);\\n    }\\n    \\n    i.icon.icon-clear {\\n        display: block;\\n    }\\n    \\n    .main-header ul.nav \\u003E li \\u003E a\\u003E.toggle {\\n        background: gray;\\n        transform: rotate(\\n    0deg);\\n        transform: translateX(3px);\\n        right: 0px !important;\\n    }\\n    \\n    .main-header ul.nav.child-nav {\\n        padding: 15px;\\n    }\\n    \\n    .main-header ul.nav.child-nav li{\\n        padding-bottom: 5px;\\n    }\\n    \\n    .main-header ul.nav.child-nav li{\\n        padding: 7px 2px;\\n        border-bottom: 1px solid #0000001c;\\n        font-size: 14px;\\n    }\\n    \\n    .main-header ul.nav.child-nav li:last-child {\\n        border: none;\\n        padding-bottom: 0px;\\n    }\\n    \\n    .main-header ul.nav.child-nav a{\\n        color: gray;\\n    }\\n    \\n    .main-header ul.nav.child-nav a:hover{\\n        color: #13203D;\\n    }\\n    .block-header #navbarMainMenu ul.nav ul.nav {\\n        border: none;\\n        transform: translateX(-19px);\\n        padding-left: 19px;\\n        box-shadow: 5px 7px 8px 1px #41464b57;\\n    }\\n    .product-item img {\\n        height: 321px !important;\\n        object-fit: cover;\\n    }\\n    .block-header li.nav-item.has-child {\\n        margin-right: 15px;\\n    }\\n    .news-box .news-item img {\\n        height: 209px !important;\\n        object-fit: cover;\\n    }\\n    .blog-category {\\n        padding-left: 0 !important;\\n    }\\n    ul#productDetailTab {\\n        margin-bottom: 0 !important;\\n    }\\n    div#productDetailTabContent {\\n        margin-top: 0 !important;\\n        padding-top: 0 !important;\\n    }\\n    a.btn.btn-primary.mt-3.contact-now-btn {\\n        margin-top: 0 !important;\\n    }\\n    .product-item .prod-title {\\n        margin-bottom: 0;\\n    }\\n    .breadcrumb-item+.breadcrumb-item {\\n        font-size: 14px;\\n    }\\n    ol.breadcrumb a {\\n        font-size: 14px;\\n    }\\n    .nav-tabs .nav-link::first-letter {\\n        text-transform: uppercase;\\n    }\\n    .project-related .project-img img {\\n        height: 238px;\\n    }\\n    .project-item img {\\n        height: 238px!important;\\n    }\\n    .about-us .col-lg-9.mb-lg-30.mb-20.text-secondary {\\n        margin-bottom: 0 !important;\\n    }\\n    footer#footer h4 {\\n        font-size: 16px !important;\\n    }\\n    .blog-category.nav-tabs .nav-link {\\n        padding: 7px;\\n    }\\n    .news-detail.news-item {\\n        padding: 0;\\n        margin: 0 !important;\\n        width: 100%;\\n    }\\n    p.title-service {\\n        font-family: Roboto;\\n        font-style: normal;\\n        font-weight: 500;\\n        font-size: 24px;\\n        line-height: 28px;\\n        color: #FFFFFF;\\n        display: block;\\n        padding-left: 43px;\\n        margin-bottom: 20px;\\n    }\\n    .collapse-search span.ms-2.d-lg-block.d-none {\\n        font-size: 14px !important;\\n    }\\n\\n.created-date {\\nfont-size: 14px;\\n}\\n\\n@media(max-width: 768px){\\n  section.mb-5.py-5.bg-sixth.full-width.bestme-block-product-related-product {\\n    margin-bottom: 0 !important;\\n  }\\n  #header .header-search.show form.search-form {\\n    visibility: visible;\\n    width: 100% !important;\\n  }\\n  .intro-award {\\n    margin-top: 35px;\\n   }\\n  .about-us .col-lg-12.col-6.mb-lg-30.mb-20 img {\\n    display: block;\\n  }\\n  img.bestme-block-banner-2 {\\n    display: none;\\n  }\\n  .why-choose {\\n    border-top: 1px solid #8080803d;\\n    padding-top: 25px;\\n    margin-top: 25px;\\n  }\\n  #header .navbar {\\n    max-width: 60%;\\n    padding-left: 0 !important;\\n  }\\n  body, #header .navbar a.nav-link:hover, #header .navbar a.nav-link.active, a, .product-item .prod-title:hover, .box-product-filter .nav a.nav-link:hover, .box-product-filter .nav a.nav-link.active, .box-product-filter .nav a.nav-link:focus, .nav-tabs .nav-link:hover, .nav-tabs .nav-link.active, .breadcrumb-item.active, .news-item .news-title {\\n    color: #27282B;\\n  }\\n  #header .navbar a.nav-link:hover, #header .navbar a.nav-link.active, .nav-tabs .nav-link:hover, .nav-tabs .nav-link.active {\\n    border: none;\\n  }\\n  .product-item img {\\n    height: auto !important;\\n    object-fit: cover;\\n  }\\n  .block-header #navbarMainMenu ul.nav ul.nav {\\n    padding-top: 0 !important;\\n    padding-bottom: 0 !important;\\n    box-shadow: none;\\n    display: none;\\n  }\\n\\n.block-header #navbarMainMenu ul.nav \\n li.active ul.nav {\\n    display: block !important;\\n}\\n  .main-header ul.nav.child-nav {\\n    padding: 0;\\n  }\\n  .copy-right br {\\n    display: none;\\n  }\\n  .copy-right {\\n    padding: 15px !important;\\n  }\\n  .service img {\\n    object-fit: cover;\\n    margin-top: 0 !important;\\n  }\\n  .main-header ul.nav.child-nav a {\\n    color: #f9fafb;\\n  }\\n  .why-choose .flex-grow-1.fw-500.fz-16.fz-lg-26 {\\n    color: #27282B;\\n  }\\n  .projects-box a.project-title {\\n    color: #27282B;\\n  }\\n  .intro-award {\\n    color: #27282B;\\n  }\\n  .main-header a.nav-link {\\n    color: white !important;\\n  }\\n  #product-category .flex-column, #product-collection .flex-column, #product-provider .flex-column, #product-tags .flex-column, #product-attribute .flex-column {\\n  \\toverflow-y: hidden !important;\\n  }\\n  .bestme-block-content-customize .flex-shrink-0.py-20.px-20.me-20.bg-eighth {\\n    padding: 5px !important;\\n  }\\n  p.title-service {\\n    text-transform: uppercase;\\n    font-size: 16px;\\n  }\\n    .block-header #navbarMainMenu ul.nav ul.nav li a {\\n    text-transform: lowercase;\\n    color: #c4c4c4;\\n  }\\n  #product-category .flex-column, #product-collection .flex-column, #product-provider .flex-column, #product-tags .flex-column, #product-attribute .flex-column {\\n    overflow-y: hidden !important;\\n  }\\n  .block-header #navbarMainMenu ul.nav ul.nav li {\\n    border: none;\\n    padding: 5px 0;\\n  }\\n  .main-header li.nav-item {\\n    padding: 5px 0;\\n  }\\n  .copy-right {\\n    font-size: 14px;\\n  }\\n  .project-item .project-img {\\n    border-radius: 0;\\n  }\\n  .product-listing b {\\n    color: #27282B;\\n  }\\n  .blog-category {\\n    padding-left: 15px !important;\\n  }\\n  .project-content .container {\\n    padding: 0;\\n  }\\n  .product-listing span.align-self-center.me-2.space-nowrap {\\n    color: #737373 !important;\\n  }\\n  .news-item .news-title {\\n    font-size: 18px;\\n  }\\n  .project-item .project-title {\\n    font-size: 18px;\\n  }\\n  .blog-category {\\n    border: none;\\n  }\\n  .blog-category li {\\n    border-bottom: 1px solid #8080804f;\\n  }\\n  .blog-category li a.active {\\n    border-bottom: 2px solid #084298 !important;\\n    width: 100%;\\n  }\\n  .intro-award .owl-stage-outer.owl-height {\\n    height: 450px !important;\\n  }\\n  .intro-award img {\\n    height: 219px !important;\\n  }\\n  #productDetailTab a.active {\\n    border-bottom: 1px solid;\\n  }\\n}\\n\\n#footer .footer-col li,#footer .footer-col li a, #footer .copy-right {\\n    color: white;\\n}\\n.header-search.ms-auto.order-2 span, #header .header-search .collapse-search span {\\n    color: var(--text-secondary);\\n}\\n\\n.about-us h2 {\\n    text-align: center;\\n}\\n\\n.about-us section.my-lg-50.my-30.about-us-home {\\n    margin-top: 0 !important;\\n    margin-bottom: 0 !important;\\n}\\n\\nsection.why-choose-us.my-lg-50.mt-30.pb-lg-50.pb-30 {\\n    margin-top: 0 !important;\\n    margin-bottom: 0 !important;\\n}\\n\\n.why-choose h2 {\\n    text-align: center;\\n}\\n\\n.projects-box section.py-50.container {\\n    padding-top: 0 !important;\\n    padding-bottom: 0 !important;\\n}\\n\\n.news-box .container.py-lg-50 {\\n    padding-top: 0 !important;\\n    padding-bottom: 0 !important;\\n}\\n\\n.news-box section.py-50.mb-lg-50.mb-30.news-home {\\n    margin-bottom: 0 !important;\\n}\\n\\n#header .menu-wrapper.show .navbar {\\n    display: block;\\n}\\n\\n#header .navbar {\\n    max-width: 60%;\\n    padding-left: 10%;\\n}\\n#header .header-search.show form.search-form {\\n    visibility: visible;\\n    width: 226px;\\n}\\n.project-content,.content-detail {\\n    color: black;\\n}\\n.header-bottom.d-none.d-lg-block.py-50 {\\n    padding: 15px 0 !important;\\n}"\n}', '2021-11-08 15:21:28', '2022-02-08 10:40:16'),
(0, 'adg_topal_55', 'config', 'config_theme_checkout_page', '{\n}', '2021-11-08 15:21:28', '2021-11-08 15:21:28'),
(0, 'adg_topal_55', 'config', 'config_section_sections', '{\n    "name": "Theme Config",\n    "version": "1.0",\n    "section": {\n        "header": {\n            "name": "header",\n            "text": "Header",\n            "visible": "1"\n        },\n        "slide-show": {\n            "name": "slide-show",\n            "text": "Slideshow",\n            "visible": "1"\n        },\n        "categories": {\n            "name": "categories",\n            "text": "Danh mục sản phẩm",\n            "visible": "1"\n        },\n        "hot-deals": {\n            "name": "hot-deals",\n            "text": "Sản phẩm khuyến mại",\n            "visible": "0",\n            "sort_order": "0"\n        },\n        "feature-products": {\n            "name": "feature-products",\n            "text": "Sản phẩm bán chạy",\n            "visible": "0",\n            "sort_order": "1"\n        },\n        "related-products": {\n            "name": "related-products",\n            "text": "Sản phẩm xem nhiều",\n            "visible": "1"\n        },\n        "new-products": {\n            "name": "new-products",\n            "text": "Sản phẩm mới",\n            "visible": "1",\n            "sort_order": "2"\n        },\n        "product-detail": {\n            "name": "product-details",\n            "text": "Chi tiết sản phẩm",\n            "visible": "1"\n        },\n        "banner": {\n            "name": "banner",\n            "text": "Banner",\n            "visible": "1"\n        },\n        "content_customize": {\n            "name": "content-customize",\n            "text": "Nội dung tùy chỉnh",\n            "visible": "1"\n        },\n        "partners": {\n            "name": "partners",\n            "text": "Đối tác",\n            "visible": "1"\n        },\n        "blog": {\n            "name": "blog",\n            "text": "Blog",\n            "visible": "1"\n        },\n        "rate": {\n            "name": "rate",\n            "text": "Đánh giá website",\n            "visible": "1"\n        },\n        "footer": {\n            "name": "footer",\n            "text": "Footer",\n            "visible": "1"\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-09 09:47:58'),
(0, 'adg_topal_55', 'config', 'config_section_banner', '{\n    "name": "banner",\n    "text": "Banner",\n    "visible": "1",\n    "display": [\n        {\n            "image-url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/gioithieu3_2.png",\n            "url": "#",\n            "description": "Banner 1",\n            "visible": "1"\n        },\n        {\n            "image-url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/gioithieu3_3.png",\n            "url": "#",\n            "description": "Banner 2",\n            "visible": "1"\n        },\n        {\n            "image-url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/banner-4.jpg",\n            "url": "#",\n            "description": "Banner 3",\n            "visible": "1"\n        }\n    ]\n}', '2021-11-08 15:21:29', '2021-12-02 10:23:42'),
(0, 'adg_topal_55', 'config', 'config_section_best_sales_product', '{\n    "name": "feature-product",\n    "text": "Sản phẩm bán chạy",\n    "visible": 1,\n    "setting": {\n        "title": "Sản phẩm bán chạy",\n        "sub_title" : "Mô tả tiêu đề",\n        "auto_retrieve_data": 1,\n        "resource_type": 1,\n        "collection_id": 1,\n        "category_id": 1,\n        "autoplay": 1,\n        "autoplay_time": 5,\n        "loop": 1\n    },\n    "display": {\n        "grid": {\n            "quantity": 4,\n            "row": 1\n        },\n        "grid_mobile": {\n            "quantity": 2,\n            "row": 1\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_new_product', '{\n    "name": "new-product",\n    "text": "Sản phẩm mới",\n    "visible": "1",\n    "setting": {\n        "title": "Danh mục sản phẩm ",\n        "sub_title": "Mô tả tiêu đề",\n        "auto_retrieve_data": "1",\n        "resource_type": "1",\n        "collection_id": "1",\n        "category_id": "1",\n        "autoplay": "1",\n        "autoplay_time": "5",\n        "loop": "1"\n    },\n    "display": {\n        "grid": {\n            "quantity": "4",\n            "row": "1"\n        },\n        "grid_mobile": {\n            "quantity": "2",\n            "row": "1"\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-09 17:14:57'),
(0, 'adg_topal_55', 'config', 'config_section_best_views_product', '{\n    "name": "related-product",\n    "text": "Sản phẩm xem nhiều",\n    "visible": 1,\n    "setting": {\n        "title": "Sản phẩm xem nhiều"\n    },\n    "display": {\n        "grid": {\n            "quantity": 10\n        },\n        "grid_mobile": {\n            "quantity": 2\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_blog', '{\n    "name": "blog",\n    "text": "Blog",\n    "visible": 1,\n    "setting": {\n        "title": "Blogs"\n    },\n    "display": {\n        "menu": [\n            {\n                "type": "existing",\n                "menu": {\n                    "id": 12,\n                    "name": "Danh sách bài viết 1"\n                }\n            },\n            {\n                "type": "manual",\n                "entries": [\n                    {\n                        "id": 10,\n                        "name": "Entry 10"\n                    },\n                    {\n                        "id": 11,\n                        "name": "Entry 11"\n                    }\n                ]\n            }\n        ],\n        "grid": {\n            "quantity": 3,\n            "row": 1\n        },\n        "grid_mobile": {\n            "quantity": 1,\n            "row": 1\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_detail_product', '{\n    "name": "product-detail",\n    "text": "Chi tiết sản phẩm",\n    "visible": 1,\n    "display": {\n        "name": true,\n        "description": false,\n        "price": true,\n        "price-compare": false,\n        "status": false,\n        "sale": false,\n        "rate": false\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_footer', '{\n    "name": "footer",\n    "text": "Footer",\n    "visible": "1",\n    "contact": {\n        "title": "LIÊN HỆ CHÚNG TÔI",\n        "address": "37 Lê Văn Thiêm, Nhân Chính, Hà Nội&lt;br&gt;&lt;li&gt;Nhà máy: Km7, Đường 39, Thị trấn Yên Mỹ, Hưng Yên",\n        "phone-number": "1900 6828",\n        "email": "Austdoor@Austdoor.com",\n        "visible": "1"\n    },\n    "contact_more": [\n        {\n            "title": "Chi nhánh 1",\n            "address": "Duy Tan - Ha Noi",\n            "phone-number": "(+84)987654322",\n            "visible": "0"\n        }\n    ],\n    "collection": {\n        "title": "Bộ sưu tập",\n        "menu_id": "1",\n        "visible": "1"\n    },\n    "quick-links": {\n        "title": "ĐIỀU KHOẢN",\n        "menu_id": "16",\n        "visible": "1"\n    },\n    "subscribe": {\n        "title": "Đăng ký theo dõi",\n        "social_network": "1",\n        "visible": "1",\n        "youtube_visible": "1",\n        "facebook_visible": "1",\n        "instagram_visible": "1",\n        "shopee_visible": "1",\n        "tiki_visible": "1",\n        "sendo_visible": "1",\n        "lazada_visible": "1"\n    }\n}', '2021-11-08 15:21:29', '2021-12-24 10:26:17'),
(0, 'adg_topal_55', 'config', 'config_section_header', '{\n    "name": "header",\n    "text": "Header",\n    "visible": "1",\n    "notify-bar": {\n        "name": "Thanh thông báo",\n        "visible": "1",\n        "content": "Mua sắm online thuận tiện và dễ dàng",\n        "url": "#"\n    },\n    "logo": {\n        "name": "Logo",\n        "url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/untitled-5555551.png",\n        "alt": "alt",\n        "height": ""\n    },\n    "menu": {\n        "name": "Menu",\n        "display-list": {\n            "name": "Menu chính",\n            "id": "1"\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-12-20 14:56:23'),
(0, 'adg_topal_55', 'config', 'config_section_hot_product', '{\n    "name": "hot-deals",\n    "text": "Sản phẩm khuyến mại",\n    "visible": 1,\n    "setting": {\n        "title": "Sản phẩm khuyến mại",\n        "sub_title" : "Mô tả tiêu đề",\n        "auto_retrieve_data": 1,\n        "resource_type": 1,\n        "collection_id": 1,\n        "category_id": 1,\n        "autoplay": 1,\n        "autoplay_time": 5,\n        "loop": 1\n    },\n    "display": {\n        "grid": {\n            "quantity": 4,\n            "row": 1\n        },\n        "grid_mobile": {\n            "quantity": 2,\n            "row": 1\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_list_product', '{\n    "name": "categories",\n    "text": "Danh mục sản phẩm",\n    "visible": 0,\n    "setting": {\n        "title": "Danh mục sản phẩm"\n    },\n    "display": {\n        "menu": {\n            "id": 1,\n            "name": "Danh mục sản phẩm 1"\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_partner', '{\n    "name": "partners",\n    "text": "Đối tác",\n    "visible": 1,\n    "limit-per-line": 4,\n    "display": [\n        {\n            "image-url": "/catalog/view/theme/novaon/asset/img/partner_1.jpg",\n            "url": "#",\n            "description": "Partner 1",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/novaon/asset/img/partner_2.jpg",\n            "url": "#",\n            "description": "Partner 2",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/novaon/asset/img/partner_3.jpg",\n            "url": "#",\n            "description": "Partner 3",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/novaon/asset/img/partner_4.jpg",\n            "url": "#",\n            "description": "Partner 4",\n            "visible": 1\n        }\n    ],\n    "setting": {\n        "autoplay": 1,\n        "loop": 1,\n        "autoplay_time": 5\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_slideshow', '{\n    "name": "slide-show",\n    "text": "Slideshow",\n    "visible": "1",\n    "setting": {\n        "transition-time": "5"\n    },\n    "display": [\n        {\n            "image-url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/banner1.png",\n            "url": "#",\n            "description": "slide_1",\n            "type": "image",\n            "video-url": "",\n            "id": "51392846"\n        },\n        {\n            "image-url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/banner2.png",\n            "url": "#",\n            "description": "slide_2",\n            "type": "image",\n            "video-url": "",\n            "id": "21020927"\n        },\n        {\n            "image-url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/banner3.png",\n            "url": "#",\n            "description": "slide_3",\n            "type": "image",\n            "video-url": "",\n            "id": "9359214"\n        },\n        {\n            "image-url": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/banner4.png",\n            "description": "slide_4",\n            "id": "27897001",\n            "url": "#"\n        }\n    ]\n}', '2021-11-08 15:21:29', '2021-12-02 10:25:33'),
(0, 'adg_topal_55', 'config', 'config_section_product_groups', '{\n    "name": "group products",\n    "text": "nhóm sản phẩm",\n    "visible": "1",\n    "list": [\n        {\n            "text": "Danh sách sản phẩm 1",\n            "visible": "0",\n            "setting": {\n                "title": "Danh sách sản phẩm 1",\n                "collection_id": "4",\n                "resource_type": "1",\n                "category_id": "1",\n                "sub_title": "Danh sách sản phẩm 1",\n                "autoplay": "1",\n                "autoplay_time": "5",\n                "loop": "1"\n            },\n            "display": {\n                "grid": {\n                    "quantity": "6",\n                    "row": "1"\n                },\n                "grid_mobile": {\n                    "quantity": "2",\n                    "row": "1"\n                }\n            },\n            "sort_order": "3"\n        }\n    ]\n}', '2021-11-08 15:21:29', '2021-11-09 09:47:58'),
(0, 'adg_topal_55', 'config', 'config_section_content_customize', '{\n    "name": "content_customize",\n    "title": "Nội dung tuỳ chỉnh",\n    "display": [\n        {\n            "name": "ben-bi-0",\n            "title": "Bền bỉ",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/fame-1.png",\n                "title": "Bền bỉ",\n                "description": "Lấy chất lượng sản phẩm làm giá trị cốt lõi, cam kết mang đến những sản phẩm chính hãng, bảo đảm đúng chất lượng, quy cách, chủng loại, đúng theo tiêu chuẩn nhà sản xuất.",\n                "html": ""\n            }\n        },\n        {\n            "name": "da-dang-1",\n            "title": "Đa dạng",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/customer-1.png",\n                "title": "Đa dạng",\n                "description": "Không ngừng nỗ lực để thỏa mãn khách hàng. Chúng tôi quan niệm rằng chất lượng phục vụ là nền tảng để xây dựng thương hiệu vững mạnh.",\n                "html": ""\n            }\n        },\n        {\n            "name": "cong-nghe-thong-minh-2",\n            "title": "Công nghệ thông minh",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/service-1.png",\n                "title": "Công nghệ thông minh",\n                "description": "Nguồn nhân lực có nhiều kinh nghiệm, cách làm việc chuyên nghiệp đáp ứng nhu cầu khách hàng một cách hoàn hảo",\n                "html": ""\n            }\n        },\n        {\n            "name": "uy-tin-3",\n            "title": "Uy tín",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/new-product-1.png",\n                "title": "Uy tín",\n                "description": "Các sản phẩm phong phú, đa dạng. Đáp ứng đầy đủ nhu cầu của khách hàng trong xây dựng các công trình dân dụng và công nghiệp.",\n                "html": ""\n            }\n        },\n        {\n            "name": "cung-cap-4",\n            "title": "Cung cấp",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/group.png",\n                "title": "Cung cấp",\n                "description": "Dây chuyền sản xuất đồng bộ cửa cuốn Austdoor được chuyển giao từ Tập đoàn (Australia) giúp gia tăng chất lượng, đảm bảo sự chính xác và độ bền cho sản phẩm.",\n                "html": ""\n            }\n        },\n        {\n            "name": "dot-pha-5",\n            "title": "Đột phá ",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/group-1.png",\n                "title": "Đột phá ",\n                "description": "Những ứng dụng công nghệ mới nhất của Austdoor giúp cửa vận hành êm ái, chống ồn, an toàn cho người sử dụng và kiểm soát an ninh cho ngôi nhà.",\n                "html": ""\n            }\n        },\n        {\n            "name": "dich-vu-6",\n            "title": "Dịch vụ ",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/tools-1.png",\n                "title": "Dịch vụ ",\n                "description": "Sứ mệnh của Austdoor không chỉ đem tới cho ngôi nhà những tiện ích tốt nhất mà còn trở thành một người bạn chu đáo, tin cậy của mọi gia đình.",\n                "html": ""\n            }\n        },\n        {\n            "name": "niem-tin-7",\n            "title": "Niềm tin",\n            "type": "fixed",\n            "content": {\n                "icon": "https:\\/\\/cdn.bestme.asia\\/images\\/adgtopal55\\/guarantee-1.png",\n                "title": "Niềm tin",\n                "description": "Niềm tin của người sử dụng và sự ghi nhận của thị trường chính là động lực để Austdoor không ngừng tiến bước vì một cuộc sống tươi đẹp hơn.",\n                "html": ""\n            }\n        }\n    ]\n}', '2021-11-08 15:21:29', '2022-01-24 10:51:04'),
(0, 'adg_topal_55', 'config', 'config_section_category_sections', '{\n    "name": "Theme Config",\n    "version": "1.0",\n    "section": {\n        "banner": {\n            "name": "banner",\n            "text": "Banner",\n            "visible": 1\n        },\n        "filter": {\n            "name": "filter",\n            "text": "Filter",\n            "visible": 1\n        },\n        "product_category": {\n            "name": "product_category",\n            "text": "Product Category",\n            "visible": 1\n        },\n        "product_list": {\n            "name": "product_list",\n            "text": "Product List",\n            "visible": 1\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_category_banner', '{\n    "name": "banner",\n    "text": "Banner",\n    "visible": 1,\n    "display": [\n        {\n            "image-url": "/catalog/view/theme/default/image/banner/img-banner-06.jpg",\n            "url": "#",\n            "description": "Banner 1",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/default/image/banner/img-banner-02.png",\n            "url": "#",\n            "description": "Banner 2",\n            "visible": 1\n        },\n        {\n            "image-url": "/catalog/view/theme/default/image/banner/img-banner-03.png",\n            "url": "#",\n            "description": "Banner 3",\n            "visible": 1\n        }\n    ]\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_category_filter', '{\n    "name": "filter",\n    "text": "Bộ lọc",\n    "visible": 1,\n    "setting": {\n        "title": "Bộ lọc"\n    },\n    "display": {\n        "supplier": {\n            "title": "Nhà cung cấp",\n            "visible": 1\n        },\n        "product-type": {\n            "title": "Loại sản phẩm",\n            "visible": 1\n        },\n        "collection": {\n            "title": "Bộ sưu tập",\n            "visible": 1\n        },\n        "property": {\n            "title": "Lọc theo tt - k dung",\n            "visible": 1,\n            "prop": [\n                "all",\n                "color",\n                "weight",\n                "size"\n            ]\n        },\n        "product-price": {\n            "title": "Giá sản phẩm",\n            "visible": 1,\n            "range": {\n                "from": 0,\n                "to": 100000000\n            }\n        },\n        "tag": {\n            "title": "Tag",\n            "visible": 1\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_category_product_category', '{\n    "name": "product-category",\n    "text": "Danh mục sản phẩm",\n    "visible": 1,\n    "setting": {\n        "title": "Danh mục sản phẩm"\n    },\n    "display": {\n        "menu": {\n            "id": 1,\n            "name": "Danh mục sản phẩm 1"\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_category_product_list', '{\n    "name": "product-list",\n    "text": "Danh sách sản phẩm",\n    "visible": 1,\n    "setting": {\n        "title": "Danh sách sản phẩm"\n    },\n    "display": {\n        "grid": {\n            "quantity": 10\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_product_detail_sections', '{\n    "name": "Theme Config",\n    "version": "1.0",\n    "section": {\n        "related_product": {\n            "name": "related_product",\n            "text": "Related Product",\n            "visible": 1\n        },\n        "template": {\n            "name": "template",\n            "text": "Template",\n            "visible": 1\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_product_detail_related_product', '{\n    "name": "related-product",\n    "text": "Sản phẩm liên quan",\n    "visible": "1",\n    "setting": {\n        "title": "SẢN PHẨM LIÊN QUAN",\n        "auto_retrieve_data": "0",\n        "resource_type": "2",\n        "collection_id": "1",\n        "category_id": "32",\n        "autoplay": "1",\n        "autoplay_time": "5",\n        "loop": "1"\n    },\n    "display": {\n        "grid": {\n            "quantity": "4"\n        },\n        "grid_mobile": {\n            "quantity": "2"\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-12-02 16:08:05'),
(0, 'adg_topal_55', 'config', 'config_section_product_detail_template', '{\n    "name": "template",\n    "text": "Giao diện",\n    "visible": 1,\n    "display": {\n        "template": {\n            "id": 10\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_blog_sections', '{\n    "name": "Theme Config",\n    "version": "1.0",\n    "section": {\n        "blog_category": {\n            "name": "blog_category",\n            "text": "Blog Category",\n            "visible": 1\n        },\n        "blog_list": {\n            "name": "blog_list",\n            "text": "Blog List",\n            "visible": 1\n        },\n        "latest_blog": {\n            "name": "latest_blog",\n            "text": "Latest Blog",\n            "visible": 1\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_blog_blog_category', '{\n    "name": "blog-category",\n    "text": "Danh mục bài viết",\n    "visible": 1,\n    "setting": {\n        "title": "Danh mục bài viết"\n    },\n    "display": {\n        "menu": {\n            "id": 1,\n            "name": "Danh mục bài viết 1"\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_blog_blog_list', '{\n    "name": "blog-list",\n    "text": "Danh sách bài viết",\n    "visible": 1,\n    "display": {\n        "grid": {\n            "quantity": 20\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_blog_latest_blog', '{\n    "name": "latest-blog",\n    "text": "Bài viết mới nhất",\n    "visible": 1,\n    "setting": {\n        "title": "Bài viết mới nhất"\n    },\n    "display": {\n        "grid": {\n            "quantity": 10\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_contact_sections', '{\n    "name": "Theme Config",\n    "version": "1.0",\n    "section": {\n        "map": {\n            "name": "map",\n            "text": "Bản đồ",\n            "address": "<iframe src=\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\" width=\\"600\\" height=\\"450\\" frameborder=\\"0\\" style=\\"border:0\\" allowfullscreen></iframe>",\n            "visible": 1\n        },\n        "info": {\n            "name": "info",\n            "text": "Thông tin cửa hàng",\n            "email": "contact-us@novaon.asia",\n            "phone": "0000000000",\n            "address": "address",\n            "visible": 1\n        },\n        "form": {\n            "name": "form",\n            "title": "Form liên hệ",\n            "email": "email@mail.com",\n            "visible": 1\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_contact_map', '{   //////// CÁI NÀY KHÔNG DÙNG NỮA\n    "name": "map",\n    "text": "Bản đồ",\n    "visible": 1,\n    "setting": {\n        "title": "Bản đồ"\n    },\n    "display": {\n        "address": "<iframe src=\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.043949938738!2d105.78184861424786!3d21.03092738599717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab23dbae9cef%3A0x26c5664dafc0fbd0!2sNovaon+Ads!5e0!3m2!1sen!2s!4v1544096628331\\" width=\\"600\\" height=\\"450\\" frameborder=\\"0\\" style=\\"border:0\\" allowfullscreen></iframe>"\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_contact_contact', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\n    "name": "contact",\n    "text": "Liên hệ",\n    "visible": 1,\n    "setting": {\n        "title": "Vị trí của chúng tôi"\n    },\n    "display": {\n        "store": {\n            "title": "Gian hàng",\n            "content": "Số 12 Dịch Vọng Hậu, Cầu Giấy, Hà Nội",\n            "visible": 1\n        },\n        "telephone": {\n            "title": "Điện thoại",\n            "content": "(+84) 987 654 321",\n            "visible": 1\n        },\n        "social_follow": {\n            "socials": [\n                {\n                    "name": "facebook",\n                    "text": "Facebook",\n                    "visible": 1\n                },\n                {\n                    "name": "twitter",\n                    "text": "Twitter",\n                    "visible": 1\n                },\n                {\n                    "name": "instagram",\n                    "text": "Instagram",\n                    "visible": 1\n                },\n                {\n                    "name": "tumblr",\n                    "text": "Tumblr",\n                    "visible": 1\n                },\n                {\n                    "name": "youtube",\n                    "text": "Youtube",\n                    "visible": 1\n                },\n                {\n                    "name": "googleplus",\n                    "text": "Google Plus",\n                    "visible": 1\n                }\n            ],\n            "visible": 1\n        }\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_contact_form', '{  //////// CÁI NÀY KHÔNG DÙNG NỮA\n    "name": "form",\n    "text": "Biểu mẫu",\n    "visible": 1,\n    "setting": {\n        "title": "Liên hệ với chúng tôi"\n    },\n    "data": {\n        "email": "sale.247@xshop.com"\n    }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_rate', '{\n  "name": "rate",\n  "text": "Đánh giá website",\n  "visible": 1,\n  "setting": {\n    "title": "Đánh giá website",\n    "sub_title" : "Mô tả tiêu đề",\n    "auto_retrieve_data": 1,\n    "resource_type": 1,\n    "autoplay": 1,\n    "autoplay_time": 5,\n    "loop": 1\n  },\n  "display": {\n    "grid": {\n      "quantity": 3,\n      "row": 1\n    },\n    "grid_mobile": {\n      "quantity": 1,\n      "row": 1\n    }\n  }\n}', '2021-11-08 15:21:29', '2021-11-08 15:21:29'),
(0, 'adg_topal_55', 'config', 'config_section_customize_layout', '{\n    "config_theme_color": {\n        "main": {\n            "main": {\n                "hex": "#13203D",\n                "visible": "1"\n            },\n            "button": {\n                "hex": "#13203D",\n                "visible": "1"\n            }\n        }\n    },\n    "config_theme_text": {\n        "all": {\n            "title": "Font Roboto",\n            "font": "Roboto",\n            "supported-fonts": [\n                "Open Sans",\n                "IBM Plex Sans",\n                "Courier New",\n                "Roboto",\n                "Nunito",\n                "Arial",\n                "DejaVu Sans",\n                "Tahoma",\n                "Time News Roman",\n                "Mulish"\n            ]\n        }\n    },\n    "image_size_suggestion": {\n        "favicon": [\n            "35",\n            "35"\n        ],\n        "logo": [\n            "300",\n            "75"\n        ],\n        "home": {\n            "banner_1": [\n                "1116",\n                "214"\n            ],\n            "banner_2": [\n                "360",\n                "205"\n            ],\n            "banner_3": [\n                "555",\n                "210"\n            ],\n            "slide_show": [\n                "1442",\n                "582"\n            ]\n        },\n        "category": {\n            "banner_1": [\n                "263",\n                "307"\n            ]\n        }\n    },\n    "app_config_theme": [\n        {\n            "name": "Trang chủ",\n            "value": "home_page",\n            "position": [\n                {\n                    "name": "Đầu trang",\n                    "value": "bestme_app_after_header"\n                },\n                {\n                    "name": "Dưới slide",\n                    "value": "bestme_app_after_slide"\n                },\n                {\n                    "name": "Dưới danh sách sản phẩm",\n                    "value": "bestme_app_after_product"\n                },\n                {\n                    "name": "Trên cuối trang",\n                    "value": "bestme_app_before_footer"\n                }\n            ]\n        },\n        {\n            "name": "Sản phẩm",\n            "value": "product",\n            "position": [\n                {\n                    "name": "Đầu trang",\n                    "value": "bestme_app_after_header"\n                },\n                {\n                    "name": "Dưới danh sách sản phẩm",\n                    "value": "bestme_app_after_product"\n                },\n                {\n                    "name": "Trên cuối trang",\n                    "value": "bestme_app_before_footer"\n                }\n            ]\n        }\n    ],\n    "default_customize_layout": [\n        {\n            "name": "txt_slide_show_banner",\n            "file": "slideshow-banner"\n        },\n        {\n            "name": "txt_about_us",\n            "file": "about-us"\n        },\n        {\n            "name": "txt_why_choose",\n            "file": "why-choose"\n        },\n        {\n            "name": "txt_block_product",\n            "file": "block-products"\n        },\n        {\n            "name": "txt_block_projects",\n            "file": "projects-box"\n        },\n        {\n            "name": "txt_intro_award",\n            "file": "intro-award"\n        },\n        {\n            "name": "txt_service",\n            "file": "service"\n        },\n        {\n            "name": "txt_block_news",\n            "file": "news-box"\n        }\n    ],\n    "version": "1"\n}', '2021-12-02 16:41:24', '2022-01-24 10:49:19');

-- oc_blog PRIMARY KEY (`blog_id`)
INSERT IGNORE INTO `oc_blog` (`blog_id`, `author`, `status`, `demo`, `source`, `date_publish`, `date_added`, `date_modified`) VALUES
(1, 1, 1, 1, NULL, '2020-06-10 11:51:00', '2020-06-10 11:51:00', '2021-12-06 15:49:20'),
(2, 1, 1, 1, NULL, '2020-06-10 11:55:33', '2020-06-10 11:55:33', '2021-12-02 15:03:39'),
(3, 1, 1, 1, NULL, '2020-06-10 12:05:27', '2020-06-10 12:05:27', '2021-12-06 15:12:23'),
(4, 1, 1, 1, NULL, '2021-11-08 16:06:45', '2021-11-08 16:06:45', '2021-12-02 16:51:35'),
(5, 1, 1, 1, NULL, '2021-11-08 16:07:22', '2021-11-08 16:07:22', '2021-12-02 15:18:40'),
(6, 1, 1, 1, NULL, '2021-11-08 16:07:52', '2021-11-08 16:07:52', '2021-12-02 15:18:38'),
(7, 1, 1, 1, NULL, '2021-11-08 16:08:09', '2021-11-08 16:08:09', '2021-12-02 15:18:08'),
(8, 1, 1, 1, NULL, '2021-11-08 16:08:24', '2021-11-08 16:08:24', '2021-12-04 11:18:23'),
(9, 1, 1, 1, NULL, '2021-11-08 16:08:46', '2021-11-08 16:08:46', '2021-12-02 15:40:27'),
(10, 1, 1, 1, NULL, '2021-11-08 16:09:14', '2021-11-08 16:09:14', '2021-12-02 15:17:28'),
(11, 1, 1, 1, NULL, '2021-11-08 16:09:46', '2021-11-08 16:09:46', '2021-12-02 15:40:26'),
(12, 1, 1, 1, NULL, '2021-11-08 16:12:42', '2021-11-08 16:12:42', '2021-12-06 15:16:46'),
(13, 1, 1, 1, NULL, '2021-11-08 16:16:50', '2021-11-08 16:16:50', '2021-12-02 14:56:19'),
(14, 1, 1, 1, NULL, '2021-11-08 16:17:21', '2021-11-08 16:17:21', '2021-12-02 14:57:21'),
(15, 1, 1, 1, NULL, '2021-12-02 14:59:22', '2021-12-02 14:59:22', '2021-12-02 16:50:42'),
(16, 1, 1, 1, NULL, '2021-12-02 15:00:11', '2021-12-02 15:00:11', '2021-12-06 15:28:02'),
(17, 1, 1, 1, NULL, '2021-12-02 15:06:19', '2021-12-02 15:06:19', '2021-12-06 15:15:34'),
(18, 1, 1, 1, NULL, '2021-12-02 15:06:56', '2021-12-02 15:06:56', '2021-12-06 15:16:12'),
(19, 1, 1, 1, NULL, '2021-12-02 15:07:40', '2021-12-02 15:07:40', '2021-12-02 16:51:43'),
(20, 1, 1, 1, NULL, '2021-12-02 15:08:20', '2021-12-02 15:08:20', '2021-12-06 15:16:07'),
(21, 1, 1, 1, NULL, '2021-12-02 15:08:51', '2021-12-02 15:08:51', '2021-12-06 15:15:54');

-- oc_blog_category PRIMARY KEY (`blog_category_id`)
INSERT IGNORE INTO `oc_blog_category` (`blog_category_id`, `status`, `source`, `date_added`, `date_modified`) VALUES
(1, 1, NULL, '2019-11-11 13:37:51', '2021-11-08 16:14:33'),
(2, 1, NULL, '2021-11-08 16:02:38', '2021-11-08 16:02:38'),
(3, 1, NULL, '2021-11-08 16:05:57', '2021-11-08 16:05:57'),
(4, 1, NULL, '2021-11-08 16:14:45', '2021-11-08 16:14:45'),
(5, 1, NULL, '2021-12-01 13:29:00', '2021-12-01 13:29:00'),
(6, 1, NULL, '2021-12-02 11:32:33', '2021-12-02 11:32:33');

-- oc_blog_category_description PRIMARY KEY (`blog_category_id`,`language_id`)
INSERT IGNORE INTO `oc_blog_category_description` (`blog_category_id`, `language_id`, `title`, `meta_title`, `meta_description`, `alias`) VALUES
(1, 1, 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'danh-muc-mac-dinh-uncategorized'),
(1, 2, 'Tin tức Đại lý', 'Mặc định (Uncategorized)', 'Mặc định (Uncategorized)', 'danh-muc-mac-dinh-uncategorized'),
(2, 2, 'thành tích và giải thưởng', '', '', 'thanh-tich-va-giai-thuong'),
(3, 2, 'projects', '', '', 'projects'),
(4, 2, 'Khuyến mại', '', '', 'khuyen-mai'),
(5, 2, 'tin nổi bật', '', '', 'tin-noi-bat'),
(6, 2, 'giới thiệu', '', '', 'gioi-thieu-1');

-- oc_blog_description PRIMARY KEY (`blog_id`,`language_id`)
INSERT IGNORE INTO `oc_blog_description` (`blog_id`, `language_id`, `title`, `content`, `short_content`, `image`, `meta_title`, `meta_description`, `seo_keywords`, `alias`, `alt`, `type`, `video_url`, `source_author_name`) VALUES
(1, 2, 'Thành lập từ năm 2003, Tập đoàn Austdoor sở hữu các thương hiệu nổi tiếng trong ngành cửa và vật liệu xây dựng như cửa cuốn AUSTDOOR', '&lt;div class=&quot;page-content&quot;&gt;\r\n&lt;section class=&quot;mb-lg-40 mb-10 mt-20&quot;&gt;\r\n&lt;h1 class=&quot;page-title mb-lg-20 text-center text-uppercase&quot; style=&quot;\r\n    margin-bottom: 0;\r\n&quot;&gt;GIỚI THIỆU VỀ CHÚNG TÔI&lt;/h1&gt;\r\n\r\n&lt;div class=&quot;line-percent mb-lg-40&quot; style=&quot;margin-bottom: 10px;&quot;&gt;&lt;/div&gt;\r\n\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-9 mb-lg-30 mb-20&quot;&gt;&lt;img src=&quot;https://nguyenductan.net/novaon/bestme/theme-6/img/banner-1.jpg&quot;&gt;&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col-lg-3&quot;&gt;\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-12 col-6 mb-lg-30 mb-20&quot;&gt;&lt;img src=&quot;https://nguyenductan.net/novaon/bestme/theme-6/img/banner-2.jpg&quot;&gt;&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col-lg-12 col-6 mb-lg-30 mb-20&quot;&gt;&lt;img src=&quot;https://nguyenductan.net/novaon/bestme/theme-6/img/banner-3.jpg&quot;&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col-lg-9 mx-auto text-secondary text-lg-center&quot; style=&quot;line-height: 30px&quot;&gt;\r\n&lt;p&gt;Năm 2003 đánh dấu sự xuất hiện của Austdoor trên thị trường Việt Nam. Trải qua gần 20 năm phát triển, Austdoor ngày càng lớn mạnh và khẳng định vị trí dẫn đầu toàn quốc trong lĩnh vực sản xuất, kinh doanh cửa cuốn chất lượng cao. Đến nay, cửa cuốn Austdoor thị phần số 1 Việt Nam, tự hào là đơn vị đầu tiên và duy nhất đạt Thương hiệu Quốc gia 2 lần liên tiếp, ghi dấu ấn thương hiệu tới 63 tỉnh thành trên cả nước và không ngừng mở rộng hoạt động tại các thị trường quốc tế. Trong suốt quá trình phát triển, Austdoor luôn khơi dậy, làm mới niềm đam mê, sự sáng tạo để chinh phục và đem lại những giá trị đỉnh cao cho cuộc sống.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/section&gt;\r\n\r\n&lt;div class=&quot;row pt-lg-15 mb-lg-40 mt-lg-30 mb-10 mt-20&quot;&gt;\r\n&lt;div class=&quot;col-lg-6 d-flex flex-column justify-content-center&quot;&gt;\r\n&lt;h2 class=&quot;mb-lg-30 mb-15 title-left-through d-lg-block d-none&quot;&gt;TẦM NHÌN&lt;/h2&gt;\r\n\r\n&lt;div class=&quot;d-lg-none&quot;&gt;\r\n&lt;h2 class=&quot;text-center text-uppercase&quot;&gt;TẦM NHÌN&lt;/h2&gt;\r\n\r\n&lt;div class=&quot;line-percent mb-10&quot;&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;p class=&quot;text-secondary&quot;&gt; Trở thành Nhà sản xuất &amp;amp; cung cấp các sản phẩm cửa cuốn an toàn &amp;amp; thông minh hàng đầu Đông Nam Á &lt;/p&gt;&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col-lg-6&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://nguyenductan.net/novaon/bestme/theme-6/img/about-1.jpg&quot; width=&quot;100%&quot;&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;section class=&quot;mb-lg-40 mt-lg-30 mb-10 mt-20&quot;&gt;\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-6 d-flex flex-column justify-content-center&quot;&gt;\r\n&lt;h2 class=&quot;mb-lg-30 mb-15 title-left-through d-lg-block d-none&quot;&gt;SỨ MỆNH&lt;/h2&gt;\r\n\r\n&lt;div class=&quot;d-lg-none&quot;&gt;\r\n&lt;h2 class=&quot;text-center text-uppercase&quot;&gt;SỨ MỆNH&lt;/h2&gt;\r\n\r\n&lt;div class=&quot;line-percent mb-10&quot;&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;p class=&quot;style-dark text-secondary&quot;&gt;Mang các sản phẩm cửa cuốn an toàn &amp;amp; thông minh bảo vệ cho hàng triệu ngôi nhà, cùng những trải nghiệm đỉnh cao cho người dùng&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col-lg-6&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://nguyenductan.net/novaon/bestme/theme-6/img/about-2.jpg&quot; width=&quot;100%&quot;&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!--End row--&gt;&lt;/section&gt;\r\n\r\n&lt;div class=&quot;row mt-30&quot;&gt;\r\n&lt;div class=&quot;col-lg-10 mx-auto text-secondary text-center&quot; style=&quot;line-height: 30px&quot;&gt;\r\n&lt;h2 class=&quot;text-center text-uppercase&quot;&gt;NHÀ MÁY SẢN XUẤT&lt;/h2&gt;\r\n\r\n&lt;div class=&quot;line-percent mb-lg-40&quot; style=&quot;\r\n    margin-bottom: 10px;\r\n&quot;&gt;&lt;/div&gt;\r\n\r\n&lt;p&gt;&lt;img src=&quot;https://nguyenductan.net/novaon/bestme/theme-6/img/about-3.jpg&quot;&gt;&lt;/p&gt;\r\n\r\n&lt;p class=&quot;px-lg-30&quot; style=&quot;\r\n    text-align: justify;\r\n&quot;&gt;Sở hữu hệ thống nhà máy sản xuất cửa cuốn lớn nhất Việt Nam, Austdoor đảm bảo tốc độ sản xuất vượt trội và năng lực cung ứng số lượng lớn cho khách hàng trong nước và quốc tế. Hệ thống nhà máy trải dài cả ba miền Bắc - Trung - Nam cho phép Austdoor cung ứng nhanh chóng, thuận tiện cho thị trường toàn quốc, giảm chi phí vận chuyển và rút ngắn thời gian giao hàng.&lt;/p&gt;\r\n\r\n&lt;ul style=&quot;\r\n    text-align: left;\r\n    padding-left: 48px;\r\n&quot;&gt;\r\n	&lt;li&gt;NHÀ MÁY SỐ 1: AUSTDOOR HƯNG YÊN Km 7, Quốc lộ 39, TT. Yên Mỹ, Tỉnh Hưng Yên Quy mô: 30.000m2&lt;/li&gt;\r\n	&lt;li&gt;NHÀ MÁY SỐ 2: AUSTDOOR NGHỆ AN Lô 14, KCN Nghi Phú,TP. Vinh, Nghệ An Quy mô: 5.000m2&lt;/li&gt;\r\n	&lt;li&gt;NHÀ MÁY SỐ 3: AUSTDOOR NHƠN TRẠCH Đường số 3, KCN Nhơn Trạch 1, Phước Thiền, Nhơn Trạch, Đồng Nai Quy mô: 17.000m&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;', '&lt;p&gt;Năm 2003 đánh dấu sự xuất hiện của Austdoor trên thị trường Việt Nam. Trải qua gần 20 năm phát triển, Austdoor ngày càng lớn mạnh và khẳng định vị trí dẫn đầu toàn quốc trong lĩnh vực sản xuất, kinh doanh cửa cuốn chất lượng cao. Đến nay, cửa cuốn Austdoor thị phần số 1 Việt Nam, tự hào là đơn vị đầu tiên và duy nhất đạt Thương hiệu Quốc gia 2 lần liên tiếp, ghi dấu ấn thương hiệu tới 63 tỉnh thành trên cả nước và không ngừng mở rộng hoạt động tại các thị trường quốc tế. Trong suốt quá trình phát triển, Austdoor luôn khơi dậy, làm mới niềm đam mê, sự sáng tạo để chinh phục và đem lại những giá trị đỉnh cao cho cuộc sống.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopal55/gioithieu3_1.png', '', '', '', 'gioi-thieu', '', 'image', '', NULL),
(2, 2, 'Gợi ý 3 cách sử dụng tay điều khiển cửa cuốn bền lâu', '&lt;p&gt;Là phụ kiện không thể thiếu giúp người dùng có thể vận hành cửa cuốn nhanh chóng, thuận tiện. Thế nhưng, bạn đã biết làm sao để sử dụng tay điều khiển bền lâu chưa? Hãy cùng Austdoor lưu lại ngay 3 cách sau: &lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;1. Tránh nơi ẩm thấp, có nhiệt độ cao &lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Tay điều khiển cửa cuốn sử dụng pin để có thể hoạt động được, chính vì vậy, khi sử dụng cần tránh những nơi ẩm thấp, trũng nước và những nơi có nhiệt độ quá cao dễ gây hỏng tay điều khiển.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;2. Nên thay pin mới cho tay điều khiển 12 tháng/lần&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Tay điều khiển dùng pin để vận hành, chính vì vậy, nên định kỳ thay pin cho tay điều khiển để hoạt động được trơn tru. &lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;3. Phạm vi sử dụng hiệu quả không quá 50m tới hộp điều khiển&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Ðiều khiển từ xa chỉ sử dụng hiệu quả trong phạm vi khoảng cách &lt;= 50m tới hộp điều khiển.&lt;/p&gt;\r\n\r\n&lt;p&gt;Lưu ý:&lt;/p&gt;\r\n\r\n&lt;p&gt;- Các vật cản như tường bê tông, vách thạch cao hay vách ngăn nhựa… cũng làm giảm hiệu quả sử dụng của điều khiển từ xa.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Luôn để ăng ten trên hộp điểu khiển (dây điện màu nâu trên hộp điều khiển) hướng chếch ra ngoài để đón sóng của điều khiển từ xa.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Dây ăng ten nên được để dưới dạng xoắn nhằm đảm bảo chất lượng bắt sóng tốt nhất.&lt;/p&gt;\r\n\r\n&lt;p&gt;- Tuyệt đối không được để dây ăng ten bị đứt hoặc bám bẩn bởi hồ, vữa, bụi…&lt;/p&gt;\r\n\r\n&lt;p&gt;Tay điều khiển cửa cuốn nhà bạn vẫn dùng tốt chứ và bao lâu rồi chưa được bảo dưỡng? Kiểm tra ngay và nếu có câu hỏi nào hãy nói cho Austdoor biết nhé!&lt;/p&gt;\r\n\r\n&lt;p&gt;Và hãy nhớ, Austdoor - cửa cuốn an toàn và thông minh là lựa chọn an tâm cho gia đình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Mọi thắc mắc về sản phẩm, Quý Khách hàng vui lòng liên hệ:&lt;/p&gt;\r\n\r\n&lt;p&gt;- Hotline: 1900 6828&lt;/p&gt;\r\n\r\n&lt;p&gt;- Website: &lt;a href=&quot;https://l.facebook.com/l.php?u=http%3A%2F%2Faustdoor.com%2F%3Ffbclid%3DIwAR3I9Iyf_B6sHXgjPrEguFWBvoUy93HmDOTOFXSzy8B7OYa0fqtt8MybdBI&amp;amp;h=AT0wLmRG_PsqAuOmSZaLbp0m3HFV4AGrMVE1ERsERhTeqS6fz7ffXoA-3Ot1ipdJRi0ExXYtbCN4nxJowuzzF-_kXAK3-t7xhD4vUQR7OL9s0hTU-PRVqb99AFavAKiXV4FM&amp;amp;__tn__=-UK-R&amp;amp;c%5b0%5d=AT26pM4GYsoR9W3ME_0NSSx2WvhJ7NcaNujRJYbYqk_nvYiZ0yzu1clV_N7Z9jY_aBDtZK7VdjpghOCOgsPY2rWvu-6WrYvAaU5RiVg_sVQlOPiNEvmtQFqSOaxuJWwH3xGix5Mf3fnU0zeRYcA1R4Z9IRUgN3ffiuxKHC4Jr7a_4jN2IC66Uwz0RPC9b7uDABjkk5Tr&quot; target=&quot;_blank&quot;&gt;austdoor.com&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Youtube: &lt;a href=&quot;https://www.youtube.com/cuacuonaustdoor?fbclid=IwAR3E0deQvQ-vpEUk8Tdl0OgzjNHaj75tX1wG0DVCnCR33Sz5hrTTvnGkeuY&quot; target=&quot;_blank&quot;&gt;https://www.youtube.com/cuacuonaustdoor&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Fanpage: &lt;a href=&quot;https://www.facebook.com/austdoorofficial&quot;&gt;https://www.facebook.com/austdoorofficial&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;AUSTDOOR - CỬA CUỐN AN TOÀN &amp; THÔNG MINH HÀNG ĐẦU VIỆT NAM!&lt;/p&gt;', '&lt;p&gt;Là phụ kiện không thể thiếu giúp người dùng có thể vận hành cửa cuốn nhanh chóng, thuận tiện. Thế nhưng, bạn đã biết làm sao để sử dụng tay điều khiển bền lâu chưa? Hãy cùng Austdoor lưu lại ngay 3 cách sau&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopal55/tintuc12.jpg', '', '', '', 'goi-y-3-cach-su-dung-tay-dieu-khien-cua-cuon-ben-lau', '', 'image', '', NULL),
(4, 2, 'Chuỗi Dự án Vinhome trên toàn quốc (4000 bộ) Cửa cuốn Nan nhôm, Cửa cuốn thép Chống cháy', '&lt;div class=&quot;container&quot;&gt;\r\n&lt;div class=&quot;project-content py-3 mb-4&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/1_AcsfwoB.png&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Vừa qua, Phú Gia An vinh dự khi là đối tác trong mảng thi công lắp đặt cửa nhôm kính cho toàn bộ hệ thống khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh. Chúng tôi luôn tự tin mang lại sự hài lòng cho mọi khách hàng từ sản phẩm cho đến dịch vụ của mình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh vừa chính thức đi vào hoạt động những ngày qua, đây chính là một trong những đối tác quan trọng mà Phú Gia An hợp tác trong hạng mục thi công lắp đặt cửa nhôm kính. Toàn bộ hệ thống khu nghỉ dưỡng đều sử dụng các sản phẩm cửa nhôm kính do Phú Gia An cung cấp.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia bao gồm 2 toà nhà chính với mật độ xây dựng cửa nhôm kính lên tới 75%. Cùng tham khảo một số hình ảnh đã hoàn thiện của công trình này dưới đây:&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-6&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/2_Rk3ffU4.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/3_SCVHjaX.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n\r\n&lt;p class=&quot;text-center&quot;&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/4_rCSW4ec.png&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End project content --&gt;&lt;/div&gt;', '&lt;p&gt;Mừng sinh nhật lần thứ 3 nhôm Topal - Những dấu mốc đáng nhớ&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopal55/du-an-1.jpg', '', '', '', 'chuoi-du-an-vinhome-tren-toan-quoc-4000-bo-cua-cuon-nan-nhom-cua-cuon-thep-chong-chay', '', 'image', '', NULL),
(5, 2, 'Indochina Plaza - Hà Nội Cửa cuốn Nan nhôm, Cửa cuốn Trong suốt', '&lt;div class=&quot;container&quot;&gt;\r\n&lt;div class=&quot;project-content py-3 mb-4&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/1_AcsfwoB.png&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Vừa qua, Phú Gia An vinh dự khi là đối tác trong mảng thi công lắp đặt cửa nhôm kính cho toàn bộ hệ thống khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh. Chúng tôi luôn tự tin mang lại sự hài lòng cho mọi khách hàng từ sản phẩm cho đến dịch vụ của mình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh vừa chính thức đi vào hoạt động những ngày qua, đây chính là một trong những đối tác quan trọng mà Phú Gia An hợp tác trong hạng mục thi công lắp đặt cửa nhôm kính. Toàn bộ hệ thống khu nghỉ dưỡng đều sử dụng các sản phẩm cửa nhôm kính do Phú Gia An cung cấp.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia bao gồm 2 toà nhà chính với mật độ xây dựng cửa nhôm kính lên tới 75%. Cùng tham khảo một số hình ảnh đã hoàn thiện của công trình này dưới đây:&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-6&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/2_Rk3ffU4.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/3_SCVHjaX.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n\r\n&lt;p class=&quot;text-center&quot;&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/4_rCSW4ec.png&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End project content --&gt;&lt;/div&gt;', '', 'https://cdn.bestme.asia/images/adgtopal55/du-an-6.jpg', '', '', '', 'indochina-plaza-ha-noi-cua-cuon-nan-nhom-cua-cuon-trong-suot', '', 'image', '', NULL),
(6, 2, 'Nhà máy Duy Tân - Long An Cửa cuốn Nan nhôm, cửa nhôm Topal', '&lt;div class=&quot;container&quot;&gt;\r\n&lt;div class=&quot;project-content py-3 mb-4&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/1_AcsfwoB.png&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Vừa qua, Phú Gia An vinh dự khi là đối tác trong mảng thi công lắp đặt cửa nhôm kính cho toàn bộ hệ thống khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh. Chúng tôi luôn tự tin mang lại sự hài lòng cho mọi khách hàng từ sản phẩm cho đến dịch vụ của mình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh vừa chính thức đi vào hoạt động những ngày qua, đây chính là một trong những đối tác quan trọng mà Phú Gia An hợp tác trong hạng mục thi công lắp đặt cửa nhôm kính. Toàn bộ hệ thống khu nghỉ dưỡng đều sử dụng các sản phẩm cửa nhôm kính do Phú Gia An cung cấp.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia bao gồm 2 toà nhà chính với mật độ xây dựng cửa nhôm kính lên tới 75%. Cùng tham khảo một số hình ảnh đã hoàn thiện của công trình này dưới đây:&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-6&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/2_Rk3ffU4.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/3_SCVHjaX.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n\r\n&lt;p class=&quot;text-center&quot;&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/4_rCSW4ec.png&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End project content --&gt;&lt;/div&gt;', '', 'https://cdn.bestme.asia/images/adgtopal55/du-an-5.jpg', '', '', '', 'nha-may-duy-tan-long-an-cua-cuon-nan-nhom-cua-nhom-topal', '', 'image', '', NULL),
(7, 2, 'LakeSide - Đà Nẵng Cửa cuốn Nan nhôm Line-Art L120', '&lt;div class=&quot;container&quot;&gt;\r\n&lt;div class=&quot;project-content py-3 mb-4&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/1_AcsfwoB.png&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Vừa qua, Phú Gia An vinh dự khi là đối tác trong mảng thi công lắp đặt cửa nhôm kính cho toàn bộ hệ thống khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh. Chúng tôi luôn tự tin mang lại sự hài lòng cho mọi khách hàng từ sản phẩm cho đến dịch vụ của mình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh vừa chính thức đi vào hoạt động những ngày qua, đây chính là một trong những đối tác quan trọng mà Phú Gia An hợp tác trong hạng mục thi công lắp đặt cửa nhôm kính. Toàn bộ hệ thống khu nghỉ dưỡng đều sử dụng các sản phẩm cửa nhôm kính do Phú Gia An cung cấp.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia bao gồm 2 toà nhà chính với mật độ xây dựng cửa nhôm kính lên tới 75%. Cùng tham khảo một số hình ảnh đã hoàn thiện của công trình này dưới đây:&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-6&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/2_Rk3ffU4.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/3_SCVHjaX.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n\r\n&lt;p class=&quot;text-center&quot;&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/4_rCSW4ec.png&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End project content --&gt;&lt;/div&gt;', '', 'https://cdn.bestme.asia/images/adgtopal55/du-an-4.jpg', '', '', '', 'lakeside-da-nang-cua-cuon-nan-nhom-line-art-l120', '', 'image', '', NULL),
(8, 2, 'TIMBERLAND MANWAH - Bình Dương Cửa cuốn thép Chống cháy, cửa cuốn thép Siêu trường, cửa cuốn Khớp thoáng inox', '&lt;div class=&quot;container&quot;&gt;\r\n&lt;div class=&quot;project-content py-3 mb-4&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/1_AcsfwoB.png&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Vừa qua, Phú Gia An vinh dự khi là đối tác trong mảng thi công lắp đặt cửa nhôm kính cho toàn bộ hệ thống khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh. Chúng tôi luôn tự tin mang lại sự hài lòng cho mọi khách hàng từ sản phẩm cho đến dịch vụ của mình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh vừa chính thức đi vào hoạt động những ngày qua, đây chính là một trong những đối tác quan trọng mà Phú Gia An hợp tác trong hạng mục thi công lắp đặt cửa nhôm kính. Toàn bộ hệ thống khu nghỉ dưỡng đều sử dụng các sản phẩm cửa nhôm kính do Phú Gia An cung cấp.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia bao gồm 2 toà nhà chính với mật độ xây dựng cửa nhôm kính lên tới 75%. Cùng tham khảo một số hình ảnh đã hoàn thiện của công trình này dưới đây:&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-6&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/2_Rk3ffU4.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/3_SCVHjaX.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n\r\n&lt;p class=&quot;text-center&quot;&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/4_rCSW4ec.png&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End project content --&gt;&lt;/div&gt;', '', 'https://cdn.bestme.asia/images/adgtopal55/du-an-3.jpg', '', '', '', 'timberland-manwah-binh-duong-cua-cuon-thep-chong-chay-cua-cuon-thep-sieu-truong-cua-cuon-khop-thoang-inox', '', 'image', '', NULL),
(9, 2, 'Dự án thi công cửa nhôm kính cho hệ thống Vinmart', '&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-12 col-lg-8 news-detail news-item mx-auto my-0 my-lg-3&quot;&gt;\r\n&lt;div class=&quot;content-detail&quot;&gt;\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Vừa ra mắt, các sản phẩm cửa cuốn phong thủy của Austdoor đang khiến người tiêu dùng quan tâm và thích thú tìm hiểu. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Theo đó, bộ sưu tập cửa cuốn phong thủy Austdoor sẽ có 5 loại cửa ứng với 5 loại kích thước thuộc cung đẹp trong phong thủy như Hoạch Tài, Phát Đạt, Văn Chương, Thi Thơ, Đại Tài; với nhiều mẫu mã đa dạng, giúp khách hàng dễ dàng lựa chọn theo mong muốn. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Phong thủy cửa chính, đón lộc vào nhà &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Khoa học phong thuỷ trong lĩnh vực xây dựng, kiến trúc dựa trên sự hình thành, phát triển và các quy luật về tự nhiên, tập trung vào các yếu tố phương hướng, địa lý, kích thước... Những công trình tốt không chỉ đảm bảo sự tiện nghi, thẩm mỹ, đáp ứng tốt nhu cầu sử dụng của gia chủ; mà còn được thiết kế, xây dựng phù hợp với các điều kiện ngoại cảnh, bên cạnh đó, chính là sự hoà hợp với thiên nhiên trời đất, hợp với tuổi, mệnh... của gia chủ. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Khi đã xác định được vị trí, hướng cửa, thì kích thước cửa chính là một tiêu chí quan trọng trong phong thủy hiện đại. Chúng vừa quyết định yếu tố công năng, thẩm mỹ, vừa có ý nghĩa phong thủy của toàn bộ ngôi nhà; vì vậy cần có sự tính toán tỉ mỉ, cẩn thận trước khi lựa chọn số đo cửa để đảm bảo phù hợp, tương xứng với công trình. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;&lt;span style=&quot;&quot;&gt;&lt;img alt=&quot;Text\r\n\r\nDescription automatically generated&quot; height=&quot;269&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/1.png&quot; style=&quot;margin-left:0px;margin-top:0px;&quot; width=&quot;478&quot; /&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-style: italic; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Phong thủy cửa chính, mối quan tâm tài lộc của nhiều người&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Với tầm quan trọng đó, rất nhiều khách hàng hiện nay mong muốn lựa chọn các loại cửa phong thuỷ để mang lại tài lộc, may mắn cho gia đình. Nếu trước đây, sản phẩm cửa cuốn chỉ được người dùng quan tâm lựa chọn về công nghệ hiện đại, an toàn, mẫu mã, màu sắc đẹp thì nay, kích thước phù hợp phong thuỷ, bản mệnh chủ nhà là một trong những tiêu chí quan trọng. Nắm bắt được xu hướng, Austdoor đã tiên phong nghiên cứu và giới thiệu tới khách hàng Bộ sưu tập cửa cuốn phong thủy, không chỉ có nền tảng công nghệ thông minh, thẩm mỹ sẵn có mà còn mang đến những mẫu cửa thiết kế riêng với kích thước đẹp, đón tài lộc may mắn.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Nghênh phúc đón tài cùng Austdoor &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Lấy cảm hứng từ chính nhu cầu của người dân Việt Nam và quan niệm phong thủy hiện đại, bộ sưu tập cửa cuốn lần này của Austdoor đã lựa chọn ra những kích thước cửa đẹp với số đo chuẩn theo phong thủy, với nhiều ý nghĩa may mắn khác nhau. Khi lắp đặt những cửa cuốn này, gia chủ cần xác định mong muốn của mình về gia đạo, tài lộc hay con cái… từ đó sẽ có những loại cửa phong thủy với kích thước phù hợp.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Nếu gia chủ mong muốn mọi sự hanh thông, con cái tấn tài danh thì cửa cuốn Đại Tài chính là lựa chọn phù hợp nhất. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;&lt;span style=&quot;&quot;&gt;&lt;img height=&quot;718&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/2.jpg&quot; style=&quot;margin-left:0px;margin-top:0px;&quot; width=&quot;498&quot; /&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-style: italic; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Cửa cuốn Đại Tài, 1 trong 5 lựa chọn cửa phong thủy của Austdoor&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Hay đối với những hộ kinh doanh&lt;/span&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt; &lt;/span&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;mong muốn tài lộc, làm ăn phát đạt, tài lộc vào nhà thì cửa cuốn Phát Đạt là gợi ý sáng giá. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Còn đối với gia đình mong muốn con cái thông minh, hiếu học, gia đạo yên vui, cửa cuốn Văn Chương là lựa chọn phù hợp…&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Để giúp cho khách hàng tiếp cận trực quan và dễ dàng hơn với bộ sưu tập mới, Austdoor cũng đã thiết kế các &lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;tài liệu bán hàng trực tuyến e-catalogue, hay các phiên bản giới thiệu sản phẩm ứng dụng trí tuệ nhân tạo (Ai), công cụ hướng dẫn sử dụng thước Lỗ Ban 52,2cm phiên bản “online”….  Đây là những ứng dụng số hóa hiện đại mà thương hiệu này tích cực triển khai ứng dụng trong thời gian qua, hứa hẹn mang lại nhiều trải nghiệm mới cho khách hàng. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;&lt;span style=&quot;&quot;&gt;&lt;img height=&quot;499&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/3.png&quot; style=&quot;margin-left:0px;margin-top:0px;&quot; width=&quot;&quot; /&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-style: italic; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Austdoor ứng dụng công nghệ trí tuệ nhân tạo AI vào giới thiệu sản phẩm&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Là thương hiệu cửa cuốn an toàn và thông minh hàng đầu Việt Nam, Austdoor luôn nỗ lực tìm hiểu, không ngừng nghiên cứu để tạo ra những bộ cửa cuốn thẩm mỹ hiện đại, sử dụng an toàn, an ninh hàng đầu Việt Nam. Bộ sưu tập cửa cuốn phong thủy – Nghênh phúc đón tài lần này sẽ giúp khách hàng có thêm nhiều lựa chọn, đặc biệt hữu ích với các chủ đầu tư, hộ gia đình, kinh doanh chuẩn bị xây dựng công trình, nhà mới; mang may mắn, tài lộc, đặc biệt trong thời điểm Xuân Nhâm Dần 2022 đang tới gần. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Sản phẩm sẽ ra mắt trước tại thị trường phía Bắc, Trung trước khi cung ứng cho toàn quốc. Để sở hữu sản phẩm của Bộ sưu tập cửa cuốn phong thủy, vui lòng liên hệ Hotline 1900 6828, hoặc truy cập website: austdoor.com để tìm hiểu chi tiết. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;---------------&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;Và hãy nhớ, Austdoor - cửa cuốn an toàn và thông minh là lựa chọn an tâm cho gia đình&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;- Hotline: 1900 6828&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;- Website: &lt;/span&gt;&lt;a href=&quot;https://l.facebook.com/l.php?u=http%3A%2F%2Faustdoor.com%2F%3Ffbclid%3DIwAR3I9Iyf_B6sHXgjPrEguFWBvoUy93HmDOTOFXSzy8B7OYa0fqtt8MybdBI&amp;amp;h=AT0wLmRG_PsqAuOmSZaLbp0m3HFV4AGrMVE1ERsERhTeqS6fz7ffXoA-3Ot1ipdJRi0ExXYtbCN4nxJowuzzF-_kXAK3-t7xhD4vUQR7OL9s0hTU-PRVqb99AFavAKiXV4FM&amp;amp;__tn__=-UK-R&amp;amp;c%5b0%5d=AT26pM4GYsoR9W3ME_0NSSx2WvhJ7NcaNujRJYbYqk_nvYiZ0yzu1clV_N7Z9jY_aBDtZK7VdjpghOCOgsPY2rWvu-6WrYvAaU5RiVg_sVQlOPiNEvmtQFqSOaxuJWwH3xGix5Mf3fnU0zeRYcA1R4Z9IRUgN3ffiuxKHC4Jr7a_4jN2IC66Uwz0RPC9b7uDABjkk5Tr&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;austdoor.com&lt;/span&gt;&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;- Youtube: &lt;/span&gt;&lt;a href=&quot;https://www.youtube.com/cuacuonaustdoor?fbclid=IwAR3E0deQvQ-vpEUk8Tdl0OgzjNHaj75tX1wG0DVCnCR33Sz5hrTTvnGkeuY&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;https://www.youtube.com/cuacuonaustdoor&lt;/span&gt;&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;- Fanpage: &lt;/span&gt;&lt;a href=&quot;https://www.facebook.com/austdoorofficial&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;https://www.facebook.com/austdoorofficial&lt;/span&gt;&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;AUSTDOOR - CỬA CUỐN AN TOÀN &amp; THÔNG MINH HÀNG ĐẦU VIỆT NAM!&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End blog detail --&gt;&lt;/div&gt;', '&lt;p&gt;Cửa cuốn Austdoor tự hào là đơn vị duy nhất đạt Thương hiệu Quốc gia 2 năm liên tiếp 2018, 2020&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopal55/banner-5.jpg', '', '', '', 'du-an-thi-cong-cua-nhom-kinh-cho-he-thong-vinmart-5', '', 'image', '', NULL),
(10, 2, 'Chuỗi dự án Mapletree toàn quốc (1000 bộ) Cửa cuốn thép Chống cháy, cửa cuốn thép Siêu trường', '&lt;div class=&quot;container&quot;&gt;\r\n&lt;div class=&quot;project-content py-3 mb-4&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/1_AcsfwoB.png&quot; style=&quot;max-width: 100%;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Vừa qua, Phú Gia An vinh dự khi là đối tác trong mảng thi công lắp đặt cửa nhôm kính cho toàn bộ hệ thống khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh. Chúng tôi luôn tự tin mang lại sự hài lòng cho mọi khách hàng từ sản phẩm cho đến dịch vụ của mình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia tại Nghi Xuân – Hà Tĩnh vừa chính thức đi vào hoạt động những ngày qua, đây chính là một trong những đối tác quan trọng mà Phú Gia An hợp tác trong hạng mục thi công lắp đặt cửa nhôm kính. Toàn bộ hệ thống khu nghỉ dưỡng đều sử dụng các sản phẩm cửa nhôm kính do Phú Gia An cung cấp.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khu nghỉ dưỡng Phú Minh Gia bao gồm 2 toà nhà chính với mật độ xây dựng cửa nhôm kính lên tới 75%. Cùng tham khảo một số hình ảnh đã hoàn thiện của công trình này dưới đây:&lt;/p&gt;\r\n\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-lg-6&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/2_Rk3ffU4.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col&quot;&gt;\r\n&lt;p&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/3_SCVHjaX.png&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n\r\n&lt;p class=&quot;text-center&quot;&gt;&lt;img src=&quot;https://cdn.bestme.asia/images/adgtopalprima/4_rCSW4ec.png&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Sự hợp tác giữa Phú Gia An và khu nghỉ dưỡng Phú Minh Gia là minh chứng cho sự tin tưởng mà khách hàng đã đặt trọn cho chúng tôi. Không chỉ là đơn vị thi công lắp đặt cửa nhôm tại Nghệ An chuyên nghiệp và uy tín nhất hiện nay mà Phú Gia An còn là sự lựa chọn của rất nhiều khách hàng ở các vùng lân cận và khắp cả nước.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với bề dày kinh nghiệm về sản xuất và thi công các hạng mục cửa nhôm kính, cửa cuốn, cửa nhựa lõi thép,… chất lượng cao tại khu vực Bắc Miền Trung, chúng tôi tự tin mang lại sự hài lòng cho mọi khách hàng từ chất lượng sản phẩm đến chất lượng dịch vụ.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End project content --&gt;&lt;/div&gt;', '', 'https://cdn.bestme.asia/images/adgtopal55/du-an-2.jpg', '', '', '', 'chuoi-du-an-mapletree-toan-quoc-1000-bo-cua-cuon-thep-chong-chay-cua-cuon-thep-sieu-truong', '', 'image', '', NULL),
(11, 2, 'Dự án thi công cửa nhôm kính cho hệ thống Vinmart', '&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-12 col-lg-8 news-detail news-item mx-auto my-0 my-lg-3&quot;&gt;\r\n&lt;div class=&quot;content-detail&quot;&gt;\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Vừa ra mắt, các sản phẩm cửa cuốn phong thủy của Austdoor đang khiến người tiêu dùng quan tâm và thích thú tìm hiểu. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Theo đó, bộ sưu tập cửa cuốn phong thủy Austdoor sẽ có 5 loại cửa ứng với 5 loại kích thước thuộc cung đẹp trong phong thủy như Hoạch Tài, Phát Đạt, Văn Chương, Thi Thơ, Đại Tài; với nhiều mẫu mã đa dạng, giúp khách hàng dễ dàng lựa chọn theo mong muốn. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Phong thủy cửa chính, đón lộc vào nhà &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Khoa học phong thuỷ trong lĩnh vực xây dựng, kiến trúc dựa trên sự hình thành, phát triển và các quy luật về tự nhiên, tập trung vào các yếu tố phương hướng, địa lý, kích thước... Những công trình tốt không chỉ đảm bảo sự tiện nghi, thẩm mỹ, đáp ứng tốt nhu cầu sử dụng của gia chủ; mà còn được thiết kế, xây dựng phù hợp với các điều kiện ngoại cảnh, bên cạnh đó, chính là sự hoà hợp với thiên nhiên trời đất, hợp với tuổi, mệnh... của gia chủ. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Khi đã xác định được vị trí, hướng cửa, thì kích thước cửa chính là một tiêu chí quan trọng trong phong thủy hiện đại. Chúng vừa quyết định yếu tố công năng, thẩm mỹ, vừa có ý nghĩa phong thủy của toàn bộ ngôi nhà; vì vậy cần có sự tính toán tỉ mỉ, cẩn thận trước khi lựa chọn số đo cửa để đảm bảo phù hợp, tương xứng với công trình. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;&lt;span style=&quot;&quot;&gt;&lt;img alt=&quot;Text\r\n\r\nDescription automatically generated&quot; height=&quot;269&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/1.png&quot; style=&quot;margin-left:0px;margin-top:0px;&quot; width=&quot;478&quot; /&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-style: italic; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Phong thủy cửa chính, mối quan tâm tài lộc của nhiều người&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Với tầm quan trọng đó, rất nhiều khách hàng hiện nay mong muốn lựa chọn các loại cửa phong thuỷ để mang lại tài lộc, may mắn cho gia đình. Nếu trước đây, sản phẩm cửa cuốn chỉ được người dùng quan tâm lựa chọn về công nghệ hiện đại, an toàn, mẫu mã, màu sắc đẹp thì nay, kích thước phù hợp phong thuỷ, bản mệnh chủ nhà là một trong những tiêu chí quan trọng. Nắm bắt được xu hướng, Austdoor đã tiên phong nghiên cứu và giới thiệu tới khách hàng Bộ sưu tập cửa cuốn phong thủy, không chỉ có nền tảng công nghệ thông minh, thẩm mỹ sẵn có mà còn mang đến những mẫu cửa thiết kế riêng với kích thước đẹp, đón tài lộc may mắn.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Nghênh phúc đón tài cùng Austdoor &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Lấy cảm hứng từ chính nhu cầu của người dân Việt Nam và quan niệm phong thủy hiện đại, bộ sưu tập cửa cuốn lần này của Austdoor đã lựa chọn ra những kích thước cửa đẹp với số đo chuẩn theo phong thủy, với nhiều ý nghĩa may mắn khác nhau. Khi lắp đặt những cửa cuốn này, gia chủ cần xác định mong muốn của mình về gia đạo, tài lộc hay con cái… từ đó sẽ có những loại cửa phong thủy với kích thước phù hợp.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Nếu gia chủ mong muốn mọi sự hanh thông, con cái tấn tài danh thì cửa cuốn Đại Tài chính là lựa chọn phù hợp nhất. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;&lt;span style=&quot;&quot;&gt;&lt;img height=&quot;718&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/2.jpg&quot; style=&quot;margin-left:0px;margin-top:0px;&quot; width=&quot;498&quot; /&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-style: italic; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Cửa cuốn Đại Tài, 1 trong 5 lựa chọn cửa phong thủy của Austdoor&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Hay đối với những hộ kinh doanh&lt;/span&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt; &lt;/span&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;mong muốn tài lộc, làm ăn phát đạt, tài lộc vào nhà thì cửa cuốn Phát Đạt là gợi ý sáng giá. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Còn đối với gia đình mong muốn con cái thông minh, hiếu học, gia đạo yên vui, cửa cuốn Văn Chương là lựa chọn phù hợp…&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Để giúp cho khách hàng tiếp cận trực quan và dễ dàng hơn với bộ sưu tập mới, Austdoor cũng đã thiết kế các &lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;tài liệu bán hàng trực tuyến e-catalogue, hay các phiên bản giới thiệu sản phẩm ứng dụng trí tuệ nhân tạo (Ai), công cụ hướng dẫn sử dụng thước Lỗ Ban 52,2cm phiên bản “online”….  Đây là những ứng dụng số hóa hiện đại mà thương hiệu này tích cực triển khai ứng dụng trong thời gian qua, hứa hẹn mang lại nhiều trải nghiệm mới cho khách hàng. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;&lt;span style=&quot;&quot;&gt;&lt;img height=&quot;499&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/3.png&quot; style=&quot;margin-left:0px;margin-top:0px;&quot; width=&quot;&quot; /&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-style: italic; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Austdoor ứng dụng công nghệ trí tuệ nhân tạo AI vào giới thiệu sản phẩm&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Là thương hiệu cửa cuốn an toàn và thông minh hàng đầu Việt Nam, Austdoor luôn nỗ lực tìm hiểu, không ngừng nghiên cứu để tạo ra những bộ cửa cuốn thẩm mỹ hiện đại, sử dụng an toàn, an ninh hàng đầu Việt Nam. Bộ sưu tập cửa cuốn phong thủy – Nghênh phúc đón tài lần này sẽ giúp khách hàng có thêm nhiều lựa chọn, đặc biệt hữu ích với các chủ đầu tư, hộ gia đình, kinh doanh chuẩn bị xây dựng công trình, nhà mới; mang may mắn, tài lộc, đặc biệt trong thời điểm Xuân Nhâm Dần 2022 đang tới gần. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Sản phẩm sẽ ra mắt trước tại thị trường phía Bắc, Trung trước khi cung ứng cho toàn quốc. Để sở hữu sản phẩm của Bộ sưu tập cửa cuốn phong thủy, vui lòng liên hệ Hotline 1900 6828, hoặc truy cập website: austdoor.com để tìm hiểu chi tiết. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;---------------&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;Và hãy nhớ, Austdoor - cửa cuốn an toàn và thông minh là lựa chọn an tâm cho gia đình&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;- Hotline: 1900 6828&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;- Website: &lt;/span&gt;&lt;a href=&quot;https://l.facebook.com/l.php?u=http%3A%2F%2Faustdoor.com%2F%3Ffbclid%3DIwAR3I9Iyf_B6sHXgjPrEguFWBvoUy93HmDOTOFXSzy8B7OYa0fqtt8MybdBI&amp;amp;h=AT0wLmRG_PsqAuOmSZaLbp0m3HFV4AGrMVE1ERsERhTeqS6fz7ffXoA-3Ot1ipdJRi0ExXYtbCN4nxJowuzzF-_kXAK3-t7xhD4vUQR7OL9s0hTU-PRVqb99AFavAKiXV4FM&amp;amp;__tn__=-UK-R&amp;amp;c%5b0%5d=AT26pM4GYsoR9W3ME_0NSSx2WvhJ7NcaNujRJYbYqk_nvYiZ0yzu1clV_N7Z9jY_aBDtZK7VdjpghOCOgsPY2rWvu-6WrYvAaU5RiVg_sVQlOPiNEvmtQFqSOaxuJWwH3xGix5Mf3fnU0zeRYcA1R4Z9IRUgN3ffiuxKHC4Jr7a_4jN2IC66Uwz0RPC9b7uDABjkk5Tr&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;austdoor.com&lt;/span&gt;&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;- Youtube: &lt;/span&gt;&lt;a href=&quot;https://www.youtube.com/cuacuonaustdoor?fbclid=IwAR3E0deQvQ-vpEUk8Tdl0OgzjNHaj75tX1wG0DVCnCR33Sz5hrTTvnGkeuY&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;https://www.youtube.com/cuacuonaustdoor&lt;/span&gt;&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;- Fanpage: &lt;/span&gt;&lt;a href=&quot;https://www.facebook.com/austdoorofficial&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;https://www.facebook.com/austdoorofficial&lt;/span&gt;&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;AUSTDOOR - CỬA CUỐN AN TOÀN &amp; THÔNG MINH HÀNG ĐẦU VIỆT NAM!&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End blog detail --&gt;&lt;/div&gt;', '&lt;p&gt;Thủ tướng Chính phủ đã phê duyệt và giao Bộ Công Thương chủ trì việc thực hiện triển khai đề án bình chọn và trao tặng giải thưởng “Thương hiệu nổi tiếng quốc gia”.&lt;br /&gt;\r\n &lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopal55/thanhtichgiaithuong3_1.png', '', '', '', 'du-an-thi-cong-cua-nhom-kinh-cho-he-thong-vinmart-7', '', 'image', '', NULL),
(12, 2, 'Cửa cuốn Austdoor Mega - Nhân 3 lợi ích (mua ngay)', '&lt;div class=&quot;content-detail&quot;&gt;\r\n&lt;p&gt;Mega chính là hiện thân của dòng sản phẩm cửa cuốn cao cấp – hiện đại – thông thoáng được khách hàng lựa chọn nhiều nhất của Austdoor.&lt;/p&gt;\r\n\r\n&lt;div class=&quot;content-detail&quot;&gt;\r\n&lt;p&gt;Với thiết kế hiện đại, cửa cuốn MEGA đáp ứng tối đa nhu cầu sử dụng với 3 tính năng nổi trội:&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;&lt;strong&gt;Nan cửa bản lớn, phẳng hiện đại&lt;/strong&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;​&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/M2.jpg&quot; style=&quot;width: 700px; height: 700px;&quot; /&gt;&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;&lt;strong&gt;Thiết kế ô thoáng PC kích thước lớn giúp đối lưu không khí, kết hợp gioăng giảm chấn giúp cửa vận hành êm ái&lt;/strong&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;​&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/M4.jpg&quot; style=&quot;width: 700px; height: 700px;&quot; /&gt;&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;&lt;strong&gt;Thiết kế chân gài cứng vững, chắc chắn, đảm bảo các nan cửa vận hành bền bỉ, an toàn &lt;/strong&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;​&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/M3.jpg&quot; style=&quot;width: 700px; height: 700px;&quot; /&gt;&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Bên cạnh đó, Mega được tích hợp nhiều tính năng an toàn như: Hệ thống bảo vệ kép Dual Safe tự động đảo chiều khi gặp vật cản, còi báo động, cảm biến khói... mang lại sự an tâm khi sử dụng.&lt;/p&gt;\r\n\r\n&lt;p&gt;Với cửa cuốn MEGA, gia đình không chỉ được bảo vệ an toàn mà còn mang lại vẻ đẹp hiện đại cho ngôi nhà.&lt;/p&gt;\r\n\r\n&lt;p&gt;Và hãy nhớ, Austdoor - cửa cuốn an toàn và thông minh là lựa chọn an tâm cho gia đình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Mọi thắc mắc về sản phẩm, Quý Khách hàng vui lòng liên hệ:&lt;/p&gt;\r\n\r\n&lt;p&gt;- Hotline: 1900 6828&lt;/p&gt;\r\n\r\n&lt;p&gt;- Website: &lt;a href=&quot;https://l.facebook.com/l.php?u=http%3A%2F%2Faustdoor.com%2F%3Ffbclid%3DIwAR3I9Iyf_B6sHXgjPrEguFWBvoUy93HmDOTOFXSzy8B7OYa0fqtt8MybdBI&amp;amp;h=AT0wLmRG_PsqAuOmSZaLbp0m3HFV4AGrMVE1ERsERhTeqS6fz7ffXoA-3Ot1ipdJRi0ExXYtbCN4nxJowuzzF-_kXAK3-t7xhD4vUQR7OL9s0hTU-PRVqb99AFavAKiXV4FM&amp;amp;__tn__=-UK-R&amp;amp;c%5b0%5d=AT26pM4GYsoR9W3ME_0NSSx2WvhJ7NcaNujRJYbYqk_nvYiZ0yzu1clV_N7Z9jY_aBDtZK7VdjpghOCOgsPY2rWvu-6WrYvAaU5RiVg_sVQlOPiNEvmtQFqSOaxuJWwH3xGix5Mf3fnU0zeRYcA1R4Z9IRUgN3ffiuxKHC4Jr7a_4jN2IC66Uwz0RPC9b7uDABjkk5Tr&quot; target=&quot;_blank&quot;&gt;austdoor.com&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Youtube: &lt;a href=&quot;https://www.youtube.com/cuacuonaustdoor?fbclid=IwAR3E0deQvQ-vpEUk8Tdl0OgzjNHaj75tX1wG0DVCnCR33Sz5hrTTvnGkeuY&quot; target=&quot;_blank&quot;&gt;https://www.youtube.com/cuacuonaustdoor&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Fanpage: &lt;a href=&quot;https://www.facebook.com/austdoorofficial&quot;&gt;https://www.facebook.com/austdoorofficial&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;AUSTDOOR - CỬA CUỐN AN TOÀN &amp; THÔNG MINH HÀNG ĐẦU VIỆT NAM!&lt;/p&gt;\r\n\r\n&lt;p&gt;Tham khảo 1 số công trình hiện đại đã sử dụng cửa cuốn Mega:&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/%C4%90L%20Ho%C3%A0ng%20V%C5%A9%20-%20Tuy%C3%AAn%20Quan.jpg&quot; style=&quot;width: 700px; height: 933px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/%C4%90L%20Thi%C3%AAn%20T%C3%A2m%20-%20Nha%20Trang.jpg&quot; style=&quot;width: 700px; height: 525px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/%C4%90L%20Tr%E1%BB%8Dng%20Ho%C3%A0ng%20H%C6%B0ng%20-%20Nha%20Trang.jpg&quot; style=&quot;width: 700px; height: 525px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/z1487773421489_71259909ac40bf7ada6fee4a540ed0e9%20(1).jpg&quot; style=&quot;width: 700px; height: 585px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/shutterstock_188107259%202.jpg&quot; style=&quot;width: 700px; height: 509px;&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;', '&lt;p&gt;Mega chính là hiện thân của dòng sản phẩm cửa cuốn cao cấp – hiện đại – thông thoáng được khách hàng lựa chọn nhiều nhất của Austdoor.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopal55/tintuc11.jpg', '', '', '', 'cua-cuon-austdoor-mega-nhan-3-loi-ich-mua-ngay', '', 'image', '', NULL),
(13, 2, '3 Nhiệm vụ chính mùa cao điểm xây dựng', '&lt;div class=&quot;content-detail&quot;&gt;\r\n&lt;p&gt;Mùa cao điểm xây dựng đang bắt đầu, nhu cầu về cửa cuốn đang tăng cao hơn bao giờ hết. Austdoor vẫn đang thực hiện tốt 3 NHIỆM VỤ CHÍNH để phục vụ khách hàng:&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;&lt;strong&gt;CUNG CẤP SẢN PHẨM AN TOÀN&lt;/strong&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/N2.jpg&quot; style=&quot;width: 500px; height: 500px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;Sở hữu dây chuyền sản xuất hiện đại bậc nhất Việt Nam, Austdoor luôn mang đến cho người tiêu dùng những sản phẩm chất lượng cao, an toàn và bền bỉ.&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;&lt;strong&gt;TIÊN PHONG CÔNG NGHỆ THÔNG MINH&lt;/strong&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/N3.jpg&quot; style=&quot;width: 500px; height: 500px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;Austdoor tự hào là đơn vị tiên phong nghiên cứu và đưa ra những công nghệ cửa cuốn hiện đại nhất, giúp quá trình sử dụng của khách hàng được thuận tiện, dễ dàng, cùng những trải nghiệm đỉnh cao hàng đầu.&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;&lt;strong&gt;ĐẨY LÙI HÀNG GIẢ, HÀNG NHÁI&lt;/strong&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/N4.jpg&quot; style=&quot;width: 500px; height: 500px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;Không chỉ nỗ lực mang đến cho người dùng những sản phẩm an toàn và thông minh hàng đầu Việt Nam, Austdoor còn thường xuyên kiểm tra và xử lý những trường hợp làm giả, nhái cửa cuốn Austdoor gây ảnh hưởng đến thương hiệu và sự an toàn của khách hàng.&lt;/p&gt;\r\n\r\n&lt;p&gt;Bảo vệ quyền lợi người tiêu dùng là mục tiêu quan trọng nhất, là kim chỉ nam cho mọi hoạt động, Austdoor luôn mang đến những sản phẩm chất lượng cùng những trải nghiệm đỉnh cao, làm hài lòng hàng triệu khách hàng Việt.&lt;/p&gt;\r\n\r\n&lt;p&gt;-----------&lt;/p&gt;\r\n\r\n&lt;p&gt;Và hãy nhớ, Austdoor - cửa cuốn an toàn và thông minh là lựa chọn an tâm cho gia đình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Mọi thắc mắc về sản phẩm, Quý Khách hàng vui lòng liên hệ:&lt;/p&gt;\r\n\r\n&lt;p&gt;- Hotline: 1900 6828&lt;/p&gt;\r\n\r\n&lt;p&gt;- Website: &lt;a href=&quot;https://l.facebook.com/l.php?u=http%3A%2F%2Faustdoor.com%2F%3Ffbclid%3DIwAR3I9Iyf_B6sHXgjPrEguFWBvoUy93HmDOTOFXSzy8B7OYa0fqtt8MybdBI&amp;amp;h=AT0wLmRG_PsqAuOmSZaLbp0m3HFV4AGrMVE1ERsERhTeqS6fz7ffXoA-3Ot1ipdJRi0ExXYtbCN4nxJowuzzF-_kXAK3-t7xhD4vUQR7OL9s0hTU-PRVqb99AFavAKiXV4FM&amp;amp;__tn__=-UK-R&amp;amp;c%5b0%5d=AT26pM4GYsoR9W3ME_0NSSx2WvhJ7NcaNujRJYbYqk_nvYiZ0yzu1clV_N7Z9jY_aBDtZK7VdjpghOCOgsPY2rWvu-6WrYvAaU5RiVg_sVQlOPiNEvmtQFqSOaxuJWwH3xGix5Mf3fnU0zeRYcA1R4Z9IRUgN3ffiuxKHC4Jr7a_4jN2IC66Uwz0RPC9b7uDABjkk5Tr&quot; target=&quot;_blank&quot;&gt;austdoor.com&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Youtube: &lt;a href=&quot;https://www.youtube.com/cuacuonaustdoor?fbclid=IwAR3E0deQvQ-vpEUk8Tdl0OgzjNHaj75tX1wG0DVCnCR33Sz5hrTTvnGkeuY&quot; target=&quot;_blank&quot;&gt;https://www.youtube.com/cuacuonaustdoor&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Fanpage: &lt;a href=&quot;https://www.facebook.com/austdoorofficial&quot;&gt;https://www.facebook.com/austdoorofficial&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;AUSTDOOR - CỬA CUỐN AN TOÀN &amp; THÔNG MINH HÀNG ĐẦU VIỆT NAM!&lt;/p&gt;\r\n&lt;/div&gt;', '&lt;p&gt;Mùa cao điểm xây dựng đang bắt đầu, nhu cầu về cửa cuốn đang tăng cao hơn bao giờ hết. Austdoor vẫn đang thực hiện tốt 3 NHIỆM VỤ CHÍNH để phục vụ khách hàng:&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopal55/tintuc5.png', '', '', '', '3-nhiem-vu-quan-trong-mua-cao-diem-xay-dung', '', 'image', '', NULL),
(14, 2, 'Bí kíp lựa chọn cửa cuốn phong thủy Austdoor', '&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-12 col-lg-8 news-detail news-item mx-auto my-0 my-lg-3&quot;&gt;\r\n&lt;div class=&quot;content-detail&quot;&gt;\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Vừa ra mắt, các sản phẩm cửa cuốn phong thủy của Austdoor đang khiến người tiêu dùng quan tâm và thích thú tìm hiểu. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Theo đó, bộ sưu tập cửa cuốn phong thủy Austdoor sẽ có 5 loại cửa ứng với 5 loại kích thước thuộc cung đẹp trong phong thủy như Hoạch Tài, Phát Đạt, Văn Chương, Thi Thơ, Đại Tài; với nhiều mẫu mã đa dạng, giúp khách hàng dễ dàng lựa chọn theo mong muốn. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Phong thủy cửa chính, đón lộc vào nhà &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Khoa học phong thuỷ trong lĩnh vực xây dựng, kiến trúc dựa trên sự hình thành, phát triển và các quy luật về tự nhiên, tập trung vào các yếu tố phương hướng, địa lý, kích thước... Những công trình tốt không chỉ đảm bảo sự tiện nghi, thẩm mỹ, đáp ứng tốt nhu cầu sử dụng của gia chủ; mà còn được thiết kế, xây dựng phù hợp với các điều kiện ngoại cảnh, bên cạnh đó, chính là sự hoà hợp với thiên nhiên trời đất, hợp với tuổi, mệnh... của gia chủ. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Khi đã xác định được vị trí, hướng cửa, thì kích thước cửa chính là một tiêu chí quan trọng trong phong thủy hiện đại. Chúng vừa quyết định yếu tố công năng, thẩm mỹ, vừa có ý nghĩa phong thủy của toàn bộ ngôi nhà; vì vậy cần có sự tính toán tỉ mỉ, cẩn thận trước khi lựa chọn số đo cửa để đảm bảo phù hợp, tương xứng với công trình. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;&lt;span style=&quot;&quot;&gt;&lt;img alt=&quot;Text\r\n\r\nDescription automatically generated&quot; height=&quot;269&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/1.png&quot; style=&quot;margin-left:0px;margin-top:0px;&quot; width=&quot;478&quot; /&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-style: italic; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Phong thủy cửa chính, mối quan tâm tài lộc của nhiều người&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Với tầm quan trọng đó, rất nhiều khách hàng hiện nay mong muốn lựa chọn các loại cửa phong thuỷ để mang lại tài lộc, may mắn cho gia đình. Nếu trước đây, sản phẩm cửa cuốn chỉ được người dùng quan tâm lựa chọn về công nghệ hiện đại, an toàn, mẫu mã, màu sắc đẹp thì nay, kích thước phù hợp phong thuỷ, bản mệnh chủ nhà là một trong những tiêu chí quan trọng. Nắm bắt được xu hướng, Austdoor đã tiên phong nghiên cứu và giới thiệu tới khách hàng Bộ sưu tập cửa cuốn phong thủy, không chỉ có nền tảng công nghệ thông minh, thẩm mỹ sẵn có mà còn mang đến những mẫu cửa thiết kế riêng với kích thước đẹp, đón tài lộc may mắn.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Nghênh phúc đón tài cùng Austdoor &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Lấy cảm hứng từ chính nhu cầu của người dân Việt Nam và quan niệm phong thủy hiện đại, bộ sưu tập cửa cuốn lần này của Austdoor đã lựa chọn ra những kích thước cửa đẹp với số đo chuẩn theo phong thủy, với nhiều ý nghĩa may mắn khác nhau. Khi lắp đặt những cửa cuốn này, gia chủ cần xác định mong muốn của mình về gia đạo, tài lộc hay con cái… từ đó sẽ có những loại cửa phong thủy với kích thước phù hợp.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Nếu gia chủ mong muốn mọi sự hanh thông, con cái tấn tài danh thì cửa cuốn Đại Tài chính là lựa chọn phù hợp nhất. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;&lt;span style=&quot;&quot;&gt;&lt;img height=&quot;718&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/2.jpg&quot; style=&quot;margin-left:0px;margin-top:0px;&quot; width=&quot;498&quot; /&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-style: italic; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Cửa cuốn Đại Tài, 1 trong 5 lựa chọn cửa phong thủy của Austdoor&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Hay đối với những hộ kinh doanh&lt;/span&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt; &lt;/span&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;mong muốn tài lộc, làm ăn phát đạt, tài lộc vào nhà thì cửa cuốn Phát Đạt là gợi ý sáng giá. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Còn đối với gia đình mong muốn con cái thông minh, hiếu học, gia đạo yên vui, cửa cuốn Văn Chương là lựa chọn phù hợp…&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Để giúp cho khách hàng tiếp cận trực quan và dễ dàng hơn với bộ sưu tập mới, Austdoor cũng đã thiết kế các &lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;tài liệu bán hàng trực tuyến e-catalogue, hay các phiên bản giới thiệu sản phẩm ứng dụng trí tuệ nhân tạo (Ai), công cụ hướng dẫn sử dụng thước Lỗ Ban 52,2cm phiên bản “online”….  Đây là những ứng dụng số hóa hiện đại mà thương hiệu này tích cực triển khai ứng dụng trong thời gian qua, hứa hẹn mang lại nhiều trải nghiệm mới cho khách hàng. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;&lt;span style=&quot;&quot;&gt;&lt;img height=&quot;499&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/3.png&quot; style=&quot;margin-left:0px;margin-top:0px;&quot; width=&quot;&quot; /&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-style: italic; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Austdoor ứng dụng công nghệ trí tuệ nhân tạo AI vào giới thiệu sản phẩm&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Là thương hiệu cửa cuốn an toàn và thông minh hàng đầu Việt Nam, Austdoor luôn nỗ lực tìm hiểu, không ngừng nghiên cứu để tạo ra những bộ cửa cuốn thẩm mỹ hiện đại, sử dụng an toàn, an ninh hàng đầu Việt Nam. Bộ sưu tập cửa cuốn phong thủy – Nghênh phúc đón tài lần này sẽ giúp khách hàng có thêm nhiều lựa chọn, đặc biệt hữu ích với các chủ đầu tư, hộ gia đình, kinh doanh chuẩn bị xây dựng công trình, nhà mới; mang may mắn, tài lộc, đặc biệt trong thời điểm Xuân Nhâm Dần 2022 đang tới gần. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Sản phẩm sẽ ra mắt trước tại thị trường phía Bắc, Trung trước khi cung ứng cho toàn quốc. Để sở hữu sản phẩm của Bộ sưu tập cửa cuốn phong thủy, vui lòng liên hệ Hotline 1900 6828, hoặc truy cập website: austdoor.com để tìm hiểu chi tiết. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;---------------&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;Và hãy nhớ, Austdoor - cửa cuốn an toàn và thông minh là lựa chọn an tâm cho gia đình&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;- Hotline: 1900 6828&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;- Website: &lt;/span&gt;&lt;a href=&quot;https://l.facebook.com/l.php?u=http%3A%2F%2Faustdoor.com%2F%3Ffbclid%3DIwAR3I9Iyf_B6sHXgjPrEguFWBvoUy93HmDOTOFXSzy8B7OYa0fqtt8MybdBI&amp;amp;h=AT0wLmRG_PsqAuOmSZaLbp0m3HFV4AGrMVE1ERsERhTeqS6fz7ffXoA-3Ot1ipdJRi0ExXYtbCN4nxJowuzzF-_kXAK3-t7xhD4vUQR7OL9s0hTU-PRVqb99AFavAKiXV4FM&amp;amp;__tn__=-UK-R&amp;amp;c%5b0%5d=AT26pM4GYsoR9W3ME_0NSSx2WvhJ7NcaNujRJYbYqk_nvYiZ0yzu1clV_N7Z9jY_aBDtZK7VdjpghOCOgsPY2rWvu-6WrYvAaU5RiVg_sVQlOPiNEvmtQFqSOaxuJWwH3xGix5Mf3fnU0zeRYcA1R4Z9IRUgN3ffiuxKHC4Jr7a_4jN2IC66Uwz0RPC9b7uDABjkk5Tr&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;austdoor.com&lt;/span&gt;&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;- Youtube: &lt;/span&gt;&lt;a href=&quot;https://www.youtube.com/cuacuonaustdoor?fbclid=IwAR3E0deQvQ-vpEUk8Tdl0OgzjNHaj75tX1wG0DVCnCR33Sz5hrTTvnGkeuY&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;https://www.youtube.com/cuacuonaustdoor&lt;/span&gt;&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;- Fanpage: &lt;/span&gt;&lt;a href=&quot;https://www.facebook.com/austdoorofficial&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;https://www.facebook.com/austdoorofficial&lt;/span&gt;&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;AUSTDOOR - CỬA CUỐN AN TOÀN &amp; THÔNG MINH HÀNG ĐẦU VIỆT NAM!&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End blog detail --&gt;&lt;/div&gt;', '&lt;p&gt;Vừa ra mắt, các sản phẩm cửa cuốn phong thủy của Austdoor đang khiến người tiêu dùng quan tâm và thích thú tìm hiểu. &lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopal55/tintuc6.jpg', '', '', '', 'bi-kip-lua-chon-cua-cuon-phong-thuy-austdoor', '', 'image', '', NULL),
(3, 2, 'Ứng dụng số hóa trong giới thiệu sản phẩm mới', '&lt;p&gt;Bộ sưu tập cửa cuốn phong thủy - Nghênh phúc đón tài của Austdoor, mang dấu ấn riêng bản khi được ứng dụng các công cụ số hóa mạnh mẽ trong công tác bán hàng, giới thiệu sản phẩm nhằm hỗ trợ tối ưu cho Quý Đại lý và người tiêu dùng tiếp cận nhanh chóng, trực quan và sinh động hơn với sản phẩm của Austdoor.&lt;/p&gt;\r\n\r\n&lt;div class=&quot;content-detail&quot;&gt;\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com//upload_images/images/790f5ed4014ec910905f.jpg&quot; style=&quot;width: 700px; height: 394px;&quot; /&gt;&lt;br /&gt;\r\n&lt;br /&gt;\r\n&lt;em&gt;Tài liệu bán hàng Online, tiện dụng, dễ dàng, mọi lúc, mọi nơi&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Có thể kể đến như: Tài liệu bán hàng trực tuyến e-catalogue, hay các phiên bản giới thiệu sản phẩm ứng dụng trí tuệ nhân tạo (Ai), cùng đầy đủ công cụ hướng dẫn sử dụng thước Lỗ Ban 52,2cm…&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com//upload_images/images/c60d4106189cd0c2898d.jpg&quot; style=&quot;width: 700px; height: 394px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;em&gt;Số hóa các tài liệu bán hàng của Đại lý&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com//upload_images/images/bd0528cc8a6942371b78.jpg&quot; style=&quot;width: 700px; height: 394px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;em&gt;MC AI - công nghệ trí tuệ nhân tạo&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;em&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com//upload_images/images/ac37e74e39ebf1b5a8fa.jpg&quot; style=&quot;width: 700px; height: 394px;&quot; /&gt;&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;i&gt;Web_Hub đồng bộ giữa mọi nền tảng&lt;/i&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;i&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com//upload_images/images/f93e6baeb20b7a55231a.jpg&quot; style=&quot;width: 700px; height: 394px;&quot; /&gt;&lt;/i&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;i&gt;Tài liệu hướng dẫn trực quan, sinh động&lt;/i&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;Chắc chắn rằng, với việc tiên phong ứng dụng số hóa để thích hợp với xu hướng phát triển của hiện tại &amp; tương lai, bộ sản phẩm mới của Austdoor sẽ tiếp tục ghi dấu ấn mạnh mẽ và chiếm được lòng tin yêu của đông đảo khách hàng.&lt;/p&gt;\r\n\r\n&lt;p&gt;Xem thêm &lt;a href=&quot;https://www.youtube.com/watch?v=HV72p9m4BAc&quot;&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;strong&gt;tại đây&lt;/strong&gt;&lt;/span&gt;&lt;/a&gt; &lt;/p&gt;\r\n\r\n&lt;p&gt; -----------&lt;/p&gt;\r\n\r\n&lt;p&gt;Mọi thắc mắc về sản phẩm, Quý Khách hàng vui lòng liên hệ:&lt;/p&gt;\r\n\r\n&lt;p&gt;✅ Hotline: 1900 6828&lt;/p&gt;\r\n\r\n&lt;p&gt;✅ Website: austdoor.com&lt;/p&gt;\r\n\r\n&lt;p&gt;✅ Youtube: &lt;a href=&quot;https://www.youtube.com/cuacuonaustdoor&quot; spellcheck=&quot;false&quot;&gt;https://www.youtube.com/cuacuonaustdoor&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;✅ Fanpage: &lt;a href=&quot;https://www.facebook.com/austdoorofficial&quot;&gt;https://www.facebook.com/austdoorofficial&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;AUSTDOOR - CỬA CUỐN AN TOÀN &amp; THÔNG MINH HÀNG ĐẦU VIỆT NAM!&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;a href=&quot;https://www.youtube.com/hashtag/austdoor&quot;&gt;#austdoor&lt;/a&gt; ​​ &lt;a href=&quot;https://www.youtube.com/hashtag/cuacuon&quot; spellcheck=&quot;false&quot;&gt;#cuacuon&lt;/a&gt; ​ &lt;a href=&quot;https://www.youtube.com/hashtag/antoan&quot; spellcheck=&quot;false&quot;&gt;#antoan&lt;/a&gt; ​ &lt;a href=&quot;https://www.youtube.com/hashtag/thongminh&quot; spellcheck=&quot;false&quot;&gt;#thongminh&lt;/a&gt; &lt;a href=&quot;https://www.youtube.com/hashtag/phongthuy&quot; spellcheck=&quot;false&quot;&gt;#phongthuy&lt;/a&gt;&lt;/p&gt;\r\n&lt;/div&gt;', '&lt;p&gt;Bộ sưu tập cửa cuốn phong thủy - Nghênh phúc đón tài của Austdoor, mang dấu ấn riêng bản khi được ứng dụng các công cụ số hóa mạnh mẽ trong công tác bán hàng, giới thiệu sản phẩm nhằm hỗ trợ tối ưu cho Quý Đại lý và người tiêu dùng tiếp cận nhanh chóng, trực quan và sinh động hơn với sản phẩm của Austdoor.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopal55/tintuc7.jpg', '', '', '', 'ung-dung-so-hoa-trong-gioi-thieu-san-pham-moi', '', 'image', '', NULL),
(15, 2, 'Bí kíp lựa chọn cửa cuốn phong thủy Austdoor', '&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-12 col-lg-8 news-detail news-item mx-auto my-0 my-lg-3&quot;&gt;\r\n&lt;div class=&quot;content-detail&quot;&gt;\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Vừa ra mắt, các sản phẩm cửa cuốn phong thủy của Austdoor đang khiến người tiêu dùng quan tâm và thích thú tìm hiểu. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Theo đó, bộ sưu tập cửa cuốn phong thủy Austdoor sẽ có 5 loại cửa ứng với 5 loại kích thước thuộc cung đẹp trong phong thủy như Hoạch Tài, Phát Đạt, Văn Chương, Thi Thơ, Đại Tài; với nhiều mẫu mã đa dạng, giúp khách hàng dễ dàng lựa chọn theo mong muốn. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Phong thủy cửa chính, đón lộc vào nhà &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Khoa học phong thuỷ trong lĩnh vực xây dựng, kiến trúc dựa trên sự hình thành, phát triển và các quy luật về tự nhiên, tập trung vào các yếu tố phương hướng, địa lý, kích thước... Những công trình tốt không chỉ đảm bảo sự tiện nghi, thẩm mỹ, đáp ứng tốt nhu cầu sử dụng của gia chủ; mà còn được thiết kế, xây dựng phù hợp với các điều kiện ngoại cảnh, bên cạnh đó, chính là sự hoà hợp với thiên nhiên trời đất, hợp với tuổi, mệnh... của gia chủ. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Khi đã xác định được vị trí, hướng cửa, thì kích thước cửa chính là một tiêu chí quan trọng trong phong thủy hiện đại. Chúng vừa quyết định yếu tố công năng, thẩm mỹ, vừa có ý nghĩa phong thủy của toàn bộ ngôi nhà; vì vậy cần có sự tính toán tỉ mỉ, cẩn thận trước khi lựa chọn số đo cửa để đảm bảo phù hợp, tương xứng với công trình. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;&lt;span style=&quot;&quot;&gt;&lt;img alt=&quot;Text\r\n\r\nDescription automatically generated&quot; height=&quot;269&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/1.png&quot; style=&quot;margin-left:0px;margin-top:0px;&quot; width=&quot;478&quot; /&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-style: italic; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Phong thủy cửa chính, mối quan tâm tài lộc của nhiều người&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Với tầm quan trọng đó, rất nhiều khách hàng hiện nay mong muốn lựa chọn các loại cửa phong thuỷ để mang lại tài lộc, may mắn cho gia đình. Nếu trước đây, sản phẩm cửa cuốn chỉ được người dùng quan tâm lựa chọn về công nghệ hiện đại, an toàn, mẫu mã, màu sắc đẹp thì nay, kích thước phù hợp phong thuỷ, bản mệnh chủ nhà là một trong những tiêu chí quan trọng. Nắm bắt được xu hướng, Austdoor đã tiên phong nghiên cứu và giới thiệu tới khách hàng Bộ sưu tập cửa cuốn phong thủy, không chỉ có nền tảng công nghệ thông minh, thẩm mỹ sẵn có mà còn mang đến những mẫu cửa thiết kế riêng với kích thước đẹp, đón tài lộc may mắn.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Nghênh phúc đón tài cùng Austdoor &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Lấy cảm hứng từ chính nhu cầu của người dân Việt Nam và quan niệm phong thủy hiện đại, bộ sưu tập cửa cuốn lần này của Austdoor đã lựa chọn ra những kích thước cửa đẹp với số đo chuẩn theo phong thủy, với nhiều ý nghĩa may mắn khác nhau. Khi lắp đặt những cửa cuốn này, gia chủ cần xác định mong muốn của mình về gia đạo, tài lộc hay con cái… từ đó sẽ có những loại cửa phong thủy với kích thước phù hợp.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Nếu gia chủ mong muốn mọi sự hanh thông, con cái tấn tài danh thì cửa cuốn Đại Tài chính là lựa chọn phù hợp nhất. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;&lt;span style=&quot;&quot;&gt;&lt;img height=&quot;718&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/2.jpg&quot; style=&quot;margin-left:0px;margin-top:0px;&quot; width=&quot;498&quot; /&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-style: italic; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Cửa cuốn Đại Tài, 1 trong 5 lựa chọn cửa phong thủy của Austdoor&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Hay đối với những hộ kinh doanh&lt;/span&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt; &lt;/span&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;mong muốn tài lộc, làm ăn phát đạt, tài lộc vào nhà thì cửa cuốn Phát Đạt là gợi ý sáng giá. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Còn đối với gia đình mong muốn con cái thông minh, hiếu học, gia đạo yên vui, cửa cuốn Văn Chương là lựa chọn phù hợp…&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Để giúp cho khách hàng tiếp cận trực quan và dễ dàng hơn với bộ sưu tập mới, Austdoor cũng đã thiết kế các &lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;tài liệu bán hàng trực tuyến e-catalogue, hay các phiên bản giới thiệu sản phẩm ứng dụng trí tuệ nhân tạo (Ai), công cụ hướng dẫn sử dụng thước Lỗ Ban 52,2cm phiên bản “online”….  Đây là những ứng dụng số hóa hiện đại mà thương hiệu này tích cực triển khai ứng dụng trong thời gian qua, hứa hẹn mang lại nhiều trải nghiệm mới cho khách hàng. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;&lt;span style=&quot;&quot;&gt;&lt;img height=&quot;499&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/3.png&quot; style=&quot;margin-left:0px;margin-top:0px;&quot; width=&quot;&quot; /&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: center;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-style: italic; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Austdoor ứng dụng công nghệ trí tuệ nhân tạo AI vào giới thiệu sản phẩm&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Là thương hiệu cửa cuốn an toàn và thông minh hàng đầu Việt Nam, Austdoor luôn nỗ lực tìm hiểu, không ngừng nghiên cứu để tạo ra những bộ cửa cuốn thẩm mỹ hiện đại, sử dụng an toàn, an ninh hàng đầu Việt Nam. Bộ sưu tập cửa cuốn phong thủy – Nghênh phúc đón tài lần này sẽ giúp khách hàng có thêm nhiều lựa chọn, đặc biệt hữu ích với các chủ đầu tư, hộ gia đình, kinh doanh chuẩn bị xây dựng công trình, nhà mới; mang may mắn, tài lộc, đặc biệt trong thời điểm Xuân Nhâm Dần 2022 đang tới gần. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.295;text-align: justify;margin-top:0pt;margin-bottom:8pt;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-605bbff0-7fff-562a-65ac-d55c6c5d8a8e&quot;&gt;&lt;span style=&quot;font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;&quot;&gt;Sản phẩm sẽ ra mắt trước tại thị trường phía Bắc, Trung trước khi cung ứng cho toàn quốc. Để sở hữu sản phẩm của Bộ sưu tập cửa cuốn phong thủy, vui lòng liên hệ Hotline 1900 6828, hoặc truy cập website: austdoor.com để tìm hiểu chi tiết. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;---------------&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;Và hãy nhớ, Austdoor - cửa cuốn an toàn và thông minh là lựa chọn an tâm cho gia đình&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;- Hotline: 1900 6828&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;- Website: &lt;/span&gt;&lt;a href=&quot;https://l.facebook.com/l.php?u=http%3A%2F%2Faustdoor.com%2F%3Ffbclid%3DIwAR3I9Iyf_B6sHXgjPrEguFWBvoUy93HmDOTOFXSzy8B7OYa0fqtt8MybdBI&amp;amp;h=AT0wLmRG_PsqAuOmSZaLbp0m3HFV4AGrMVE1ERsERhTeqS6fz7ffXoA-3Ot1ipdJRi0ExXYtbCN4nxJowuzzF-_kXAK3-t7xhD4vUQR7OL9s0hTU-PRVqb99AFavAKiXV4FM&amp;amp;__tn__=-UK-R&amp;amp;c%5b0%5d=AT26pM4GYsoR9W3ME_0NSSx2WvhJ7NcaNujRJYbYqk_nvYiZ0yzu1clV_N7Z9jY_aBDtZK7VdjpghOCOgsPY2rWvu-6WrYvAaU5RiVg_sVQlOPiNEvmtQFqSOaxuJWwH3xGix5Mf3fnU0zeRYcA1R4Z9IRUgN3ffiuxKHC4Jr7a_4jN2IC66Uwz0RPC9b7uDABjkk5Tr&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;austdoor.com&lt;/span&gt;&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;- Youtube: &lt;/span&gt;&lt;a href=&quot;https://www.youtube.com/cuacuonaustdoor?fbclid=IwAR3E0deQvQ-vpEUk8Tdl0OgzjNHaj75tX1wG0DVCnCR33Sz5hrTTvnGkeuY&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;https://www.youtube.com/cuacuonaustdoor&lt;/span&gt;&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;- Fanpage: &lt;/span&gt;&lt;a href=&quot;https://www.facebook.com/austdoorofficial&quot;&gt;&lt;span style=&quot;color:#000000;&quot;&gt;https://www.facebook.com/austdoorofficial&lt;/span&gt;&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#000000;&quot;&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;AUSTDOOR - CỬA CUỐN AN TOÀN &amp; THÔNG MINH HÀNG ĐẦU VIỆT NAM!&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- End blog detail --&gt;&lt;/div&gt;', '&lt;p&gt;Vừa ra mắt, các sản phẩm cửa cuốn phong thủy của Austdoor đang khiến người tiêu dùng quan tâm và thích thú tìm hiểu. &lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopal55/tintuc6.jpg', '', '', '', 'bi-kip-lua-chon-cua-cuon-phong-thuy-austdoor-1', '', 'image', '', NULL),
(16, 2, 'Ứng dụng số hóa trong giới thiệu sản phẩm mới', '&lt;h1 class=&quot;title-page&quot; style=&quot;text-align: center;&quot;&gt;Ứng dụng số hóa trong giới thiệu sản phẩm mới&lt;/h1&gt;\r\n\r\n&lt;p&gt; &lt;/p&gt;\r\n\r\n&lt;p&gt;Bộ sưu tập cửa cuốn phong thủy - Nghênh phúc đón tài của Austdoor, mang dấu ấn riêng bản khi được ứng dụng các công cụ số hóa mạnh mẽ trong công tác bán hàng, giới thiệu sản phẩm nhằm hỗ trợ tối ưu cho Quý Đại lý và người tiêu dùng tiếp cận nhanh chóng, trực quan và sinh động hơn với sản phẩm của Austdoor.&lt;/p&gt;\r\n\r\n&lt;div class=&quot;content-detail&quot;&gt;\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com//upload_images/images/790f5ed4014ec910905f.jpg&quot; style=&quot;width: 700px; height: 394px;&quot; /&gt;&lt;br /&gt;\r\n&lt;br /&gt;\r\n&lt;em&gt;Tài liệu bán hàng Online, tiện dụng, dễ dàng, mọi lúc, mọi nơi&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Có thể kể đến như: Tài liệu bán hàng trực tuyến e-catalogue, hay các phiên bản giới thiệu sản phẩm ứng dụng trí tuệ nhân tạo (Ai), cùng đầy đủ công cụ hướng dẫn sử dụng thước Lỗ Ban 52,2cm…&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com//upload_images/images/c60d4106189cd0c2898d.jpg&quot; style=&quot;width: 700px; height: 394px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;em&gt;Số hóa các tài liệu bán hàng của Đại lý&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com//upload_images/images/bd0528cc8a6942371b78.jpg&quot; style=&quot;width: 700px; height: 394px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;em&gt;MC AI - công nghệ trí tuệ nhân tạo&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;em&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com//upload_images/images/ac37e74e39ebf1b5a8fa.jpg&quot; style=&quot;width: 700px; height: 394px;&quot; /&gt;&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;i&gt;Web_Hub đồng bộ giữa mọi nền tảng&lt;/i&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;i&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com//upload_images/images/f93e6baeb20b7a55231a.jpg&quot; style=&quot;width: 700px; height: 394px;&quot; /&gt;&lt;/i&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;i&gt;Tài liệu hướng dẫn trực quan, sinh động&lt;/i&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;Chắc chắn rằng, với việc tiên phong ứng dụng số hóa để thích hợp với xu hướng phát triển của hiện tại &amp; tương lai, bộ sản phẩm mới của Austdoor sẽ tiếp tục ghi dấu ấn mạnh mẽ và chiếm được lòng tin yêu của đông đảo khách hàng.&lt;/p&gt;\r\n\r\n&lt;p&gt;Xem thêm &lt;a href=&quot;https://www.youtube.com/watch?v=HV72p9m4BAc&quot;&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;strong&gt;tại đây&lt;/strong&gt;&lt;/span&gt;&lt;/a&gt; &lt;/p&gt;\r\n\r\n&lt;p&gt; -----------&lt;/p&gt;\r\n\r\n&lt;p&gt;Mọi thắc mắc về sản phẩm, Quý Khách hàng vui lòng liên hệ:&lt;/p&gt;\r\n\r\n&lt;p&gt;✅ Hotline: 1900 6828&lt;/p&gt;\r\n\r\n&lt;p&gt;✅ Website: austdoor.com&lt;/p&gt;\r\n\r\n&lt;p&gt;✅ Youtube: &lt;a href=&quot;https://www.youtube.com/cuacuonaustdoor&quot; spellcheck=&quot;false&quot;&gt;https://www.youtube.com/cuacuonaustdoor&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;✅ Fanpage: &lt;a href=&quot;https://www.facebook.com/austdoorofficial&quot;&gt;https://www.facebook.com/austdoorofficial&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;AUSTDOOR - CỬA CUỐN AN TOÀN &amp; THÔNG MINH HÀNG ĐẦU VIỆT NAM!&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;a href=&quot;https://www.youtube.com/hashtag/austdoor&quot;&gt;#austdoor&lt;/a&gt; ​​ &lt;a href=&quot;https://www.youtube.com/hashtag/cuacuon&quot; spellcheck=&quot;false&quot;&gt;#cuacuon&lt;/a&gt; ​ &lt;a href=&quot;https://www.youtube.com/hashtag/antoan&quot; spellcheck=&quot;false&quot;&gt;#antoan&lt;/a&gt; ​ &lt;a href=&quot;https://www.youtube.com/hashtag/thongminh&quot; spellcheck=&quot;false&quot;&gt;#thongminh&lt;/a&gt; &lt;a href=&quot;https://www.youtube.com/hashtag/phongthuy&quot; spellcheck=&quot;false&quot;&gt;#phongthuy&lt;/a&gt;&lt;/p&gt;\r\n&lt;/div&gt;', '&lt;p&gt;Bộ sưu tập cửa cuốn phong thủy - Nghênh phúc đón tài của Austdoor, mang dấu ấn riêng bản khi được ứng dụng các công cụ số hóa mạnh mẽ trong công tác bán hàng, giới thiệu sản phẩm nhằm hỗ trợ tối ưu cho Quý Đại lý và người tiêu dùng tiếp cận nhanh chóng, trực quan và sinh động hơn với sản phẩm của Austdoor.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopal55/tintuc7.jpg', '', '', '', 'ung-dung-so-hoa-trong-gioi-thieu-san-pham-moi-1', '', 'image', '', NULL),
(17, 2, 'Muốn phô bày nét đẹp bên trong nhưng vẫn an toàn dùng cửa cuốn gì', '&lt;div class=&quot;content-detail&quot;&gt;\r\n&lt;p&gt;CỬA CUỐN AUSTVISION - TRỌN VẸN LÔI CUỐN TỪ BÊN TRONG&lt;/p&gt;\r\n\r\n&lt;p&gt;Austvison là dòng sản phẩm cửa cuốn đặc biệt ấn tượng, có kết cấu thân cửa được tạo nên từ các nan nhôm hợp kim cao cấp và tấm Polycarbonate TRONG SUỐT có độ bền cao.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/z2839067895164_77a0607027669a6f49fab74ff1b6422b.jpg&quot; style=&quot;width: 700px; height: 352px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Sản phẩm phù hợp cho GIAN HÀNG tại trung tâm thương mại, những không gian mở như bể bơi, cửa hàng thời trang với mong muốn phô bày được vẻ đẹp của hàng hóa bên trong ngay cả khi đóng cửa mà vẫn đảm bảo an toàn tuyệt đối.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/z2839067886436_f7b7fbe67c92e54225730a90e95e836f.jpg&quot; style=&quot;width: 350px; height: 350px;&quot; /&gt; &lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/z2839067908361_987ac72ec9cf1410922b0312a80e9108.jpg&quot; style=&quot;width: 350px; height: 350px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Bên cạnh đó, Austvision tích hợp hhiều tính năng AN TOÀN: Austmatic, ARC, còi báo động, UPS, mạch báo sáng... người dùng luôn được bảo vệ an toàn.&lt;/p&gt;\r\n\r\n&lt;p&gt;Và hãy nhớ, Austdoor - cửa cuốn an toàn và thông minh là lựa chọn an tâm cho gia đình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Mọi thắc mắc về sản phẩm, Quý Khách hàng vui lòng liên hệ:&lt;/p&gt;\r\n\r\n&lt;p&gt;- Hotline: 1900 6828&lt;/p&gt;\r\n\r\n&lt;p&gt;- Website: &lt;a href=&quot;https://l.facebook.com/l.php?u=http%3A%2F%2Faustdoor.com%2F%3Ffbclid%3DIwAR3I9Iyf_B6sHXgjPrEguFWBvoUy93HmDOTOFXSzy8B7OYa0fqtt8MybdBI&amp;amp;h=AT0wLmRG_PsqAuOmSZaLbp0m3HFV4AGrMVE1ERsERhTeqS6fz7ffXoA-3Ot1ipdJRi0ExXYtbCN4nxJowuzzF-_kXAK3-t7xhD4vUQR7OL9s0hTU-PRVqb99AFavAKiXV4FM&amp;amp;__tn__=-UK-R&amp;amp;c%5b0%5d=AT26pM4GYsoR9W3ME_0NSSx2WvhJ7NcaNujRJYbYqk_nvYiZ0yzu1clV_N7Z9jY_aBDtZK7VdjpghOCOgsPY2rWvu-6WrYvAaU5RiVg_sVQlOPiNEvmtQFqSOaxuJWwH3xGix5Mf3fnU0zeRYcA1R4Z9IRUgN3ffiuxKHC4Jr7a_4jN2IC66Uwz0RPC9b7uDABjkk5Tr&quot; target=&quot;_blank&quot;&gt;austdoor.com&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Youtube: &lt;a href=&quot;https://www.youtube.com/cuacuonaustdoor?fbclid=IwAR3E0deQvQ-vpEUk8Tdl0OgzjNHaj75tX1wG0DVCnCR33Sz5hrTTvnGkeuY&quot; target=&quot;_blank&quot;&gt;https://www.youtube.com/cuacuonaustdoor&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Fanpage: &lt;a href=&quot;https://www.facebook.com/austdoorofficial&quot;&gt;https://www.facebook.com/austdoorofficial&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;AUSTDOOR - CỬA CUỐN AN TOÀN &amp; THÔNG MINH HÀNG ĐẦU VIỆT NAM!&lt;/p&gt;\r\n\r\n&lt;p&gt;Cùng tham khảo 1 số hình ảnh tại các công trình:&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/screenshot_1626322559.png&quot; style=&quot;width: 700px; height: 668px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/5(4).jpg&quot; style=&quot;width: 700px; height: 511px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/z1489262590468_522d2f99b32578bbf6a4ef9c93c953a0.jpg&quot; style=&quot;width: 700px; height: 525px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/z1487771850453_9b52d79eadd02bb11a3d22cc6bb9a7f4.jpg&quot; style=&quot;width: 700px; height: 467px;&quot; /&gt;&lt;/p&gt;\r\n&lt;/div&gt;', '&lt;p&gt;Cửa cuốn Austvision - trọn vẹn lôi cuốn từ bên trong&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopal55/tintuc10.png', '', '', '', 'muon-pho-bay-net-dep-ben-trong-nhung-van-an-toan-dung-cua-cuon-gi', '', 'image', '', NULL),
(18, 2, 'Mách bạn mẹo sử dụng cửa cuốn bền lâu mùa dịch covid-19', '&lt;div class=&quot;content-detail&quot;&gt;\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;Nghỉ dịch ở nhà, dù không ra ngoài nhưng đừng quên rằng, cửa cuốn cũng cần “vận động” để luôn ổn định và an toàn. Hãy chuẩn bị những kiến thức cần thiết về việc sử dụng và vệ sinh cửa cuốn để bảo vệ tổ ấm của mình. Austdoor mách bạn 3 “việc” cần làm trong lúc giãn cách sau đây.&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li&gt;&lt;strong&gt;Vệ sinh cửa cuốn&lt;/strong&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;Hãy nhớ rằng, cửa cuốn cũng như mọi đồ dùng khác, muốn bền đẹp thì cần được quan tâm vệ sinh thường xuyên, đối với bề mặt thân cửa ít nhất 3 tháng/lần, bề mặt trong của ray dẫn hưỡng nên vệ sinh định kỳ 6 tháng/lần.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/z2771678884866_3f22d565bba5d377c7b5f88f2bbfd906.jpg&quot; style=&quot;width: 600px; height: 600px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;Đối với những thao tác khó và yêu cầu kỹ thuật cao cần liên hệ dịch vụ bảo hành cửa cuốn của hãng để được tư vấn chính xác.&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li value=&quot;2&quot;&gt;&lt;strong&gt;Sử dụng cửa cuốn&lt;/strong&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;Không chỉ người lớn mới cần biết cách sử dụng cửa cuốn, trẻ em là đối tượng cần trang bị những kiến thức cơ bản về sử dụng cửa cuốn an toàn.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/z2770118063799_23ff429ebc51539d46301c5d9dcbc4eb.jpg&quot; style=&quot;width: 600px; height: 600px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;Tranh thủ thời gian nghỉ dịch ở nhà, bố con được gần gũi nhau hơn, hãy dạy con những kiến thức hữu ích về cửa cuốn để cả nhà mình được an tâm.&lt;/p&gt;\r\n\r\n&lt;ol&gt;\r\n	&lt;li value=&quot;3&quot;&gt;&lt;strong&gt;Vận hành cửa cuốn &lt;/strong&gt;&lt;/li&gt;\r\n&lt;/ol&gt;\r\n\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;Bạn nghĩ rằng ở yên trong nhà thì cửa cuốn cũng phải “ở yên” sao? Không phải như vậy, cửa cuốn nên được thường xuyên vận hành mỗi ngày dù chúng ta không có nhu cầu đi ra ngoài.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/z2771678868413_818e88e43fd9e0476acb371f5785159d.jpg&quot; style=&quot;width: 600px; height: 600px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;Hãy khởi động cửa cuốn mở lên/đóng xuống ít nhất 1 ngày 3 lần để máy móc được ổn định và bền lâu.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;Dịch covid_19 vẫn đang từng bước được khống chế, chúng ta hãy nâng cao tinh thần tập thể, ai ở đâu ở yên đó để đảm bảo an toàn tuyệt đối cho bản thân và cả cộng đồng, để sớm quay trở lại với công việc.  &lt;/p&gt;\r\n\r\n&lt;p&gt;Và hãy nhớ, Austdoor - cửa cuốn an toàn và thông minh là lựa chọn an tâm cho gia đình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Mọi thắc mắc về sản phẩm, Quý Khách hàng vui lòng liên hệ:&lt;/p&gt;\r\n\r\n&lt;p&gt;- Hotline: 1900 6828&lt;/p&gt;\r\n\r\n&lt;p&gt;- Website: &lt;a href=&quot;https://l.facebook.com/l.php?u=http%3A%2F%2Faustdoor.com%2F%3Ffbclid%3DIwAR3I9Iyf_B6sHXgjPrEguFWBvoUy93HmDOTOFXSzy8B7OYa0fqtt8MybdBI&amp;amp;h=AT0wLmRG_PsqAuOmSZaLbp0m3HFV4AGrMVE1ERsERhTeqS6fz7ffXoA-3Ot1ipdJRi0ExXYtbCN4nxJowuzzF-_kXAK3-t7xhD4vUQR7OL9s0hTU-PRVqb99AFavAKiXV4FM&amp;amp;__tn__=-UK-R&amp;amp;c%5b0%5d=AT26pM4GYsoR9W3ME_0NSSx2WvhJ7NcaNujRJYbYqk_nvYiZ0yzu1clV_N7Z9jY_aBDtZK7VdjpghOCOgsPY2rWvu-6WrYvAaU5RiVg_sVQlOPiNEvmtQFqSOaxuJWwH3xGix5Mf3fnU0zeRYcA1R4Z9IRUgN3ffiuxKHC4Jr7a_4jN2IC66Uwz0RPC9b7uDABjkk5Tr&quot; target=&quot;_blank&quot;&gt;austdoor.com&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Youtube: &lt;a href=&quot;https://www.youtube.com/cuacuonaustdoor?fbclid=IwAR3E0deQvQ-vpEUk8Tdl0OgzjNHaj75tX1wG0DVCnCR33Sz5hrTTvnGkeuY&quot; target=&quot;_blank&quot;&gt;https://www.youtube.com/cuacuonaustdoor&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Fanpage: &lt;a href=&quot;https://www.facebook.com/austdoorofficial&quot;&gt;https://www.facebook.com/austdoorofficial&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;AUSTDOOR - CỬA CUỐN AN TOÀN &amp; THÔNG MINH HÀNG ĐẦU VIỆT NAM!&lt;/p&gt;\r\n&lt;/div&gt;', '&lt;p&gt;Nghỉ dịch ở nhà, dù không ra ngoài nhưng đừng quên rằng, cửa cuốn cũng cần “vận động” để luôn ổn định và an toàn. Hãy chuẩn bị những kiến thức cần thiết về việc sử dụng và vệ sinh cửa cuốn để bảo vệ tổ ấm của mình. Austdoor mách bạn 3 “việc” cần làm trong lúc giãn cách sau đây.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopal55/tintuc9.jpg', '', '', '', 'mach-ban-meo-su-dung-cua-cuon-ben-lau-mua-dich-covid-19', '', 'image', '', NULL),
(19, 2, 'Trải nghiệm thực tế cửa cuốn Austdoor tốc độ cao HS', '&lt;div class=&quot;content-detail&quot;&gt;\r\n&lt;p&gt;&lt;strong&gt;Trải nghiệm thực tế cửa cuốn Austdoor tốc độ cao HS!&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;TỐC ĐỘ đóng/mở NHANH lên tới 3m/s, HS mang đến cho người dùng những trải nghiệm vô cùng bất ngờ và thích thú.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/Untitled1(1).png&quot; style=&quot;width: 722px; height: 223px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;em&gt;Cửa cuốn tốc độ cao HS với nhiều lựa chọn thân cửa&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Không những thế, HS hoạt động dựa trên nguyên lý cảm biến, khi có chướng ngại vật cửa sẽ tự động đảo chiều đi lên, mang đến an toàn tuyệt đối cho người sử dụng.&lt;/p&gt;\r\n\r\n&lt;p&gt;Xem thực tế trải nghiệm tại: &lt;a href=&quot;https://www.youtube.com/watch?v=_87Xfik2mXE&quot;&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;https://www.youtube.com/watch?v=_87Xfik2mXE&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Thông tin dự án sử dụng cửa cuốn tốc độ cao HS của Austdoor:&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;SHOWROOM POSCHER&lt;/li&gt;\r\n	&lt;li&gt;Địa chỉ: đường số 7, KCX Tân Thuận, Tân Thuận Đông, Q7, TP. HCM&lt;/li&gt;\r\n	&lt;li&gt;Quy mô dự án: 6 bộ cửa HS kích thước lớn&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p&gt;-----------&lt;/p&gt;\r\n\r\n&lt;p&gt;Và hãy nhớ, Austdoor - cửa cuốn an toàn và thông minh là lựa chọn an tâm cho gia đình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Mọi thắc mắc về sản phẩm, Quý Khách hàng vui lòng liên hệ:&lt;/p&gt;\r\n\r\n&lt;p&gt;- Hotline: 1900 6828&lt;/p&gt;\r\n\r\n&lt;p&gt;- Website: &lt;a href=&quot;https://l.facebook.com/l.php?u=http%3A%2F%2Faustdoor.com%2F%3Ffbclid%3DIwAR3I9Iyf_B6sHXgjPrEguFWBvoUy93HmDOTOFXSzy8B7OYa0fqtt8MybdBI&amp;amp;h=AT0wLmRG_PsqAuOmSZaLbp0m3HFV4AGrMVE1ERsERhTeqS6fz7ffXoA-3Ot1ipdJRi0ExXYtbCN4nxJowuzzF-_kXAK3-t7xhD4vUQR7OL9s0hTU-PRVqb99AFavAKiXV4FM&amp;amp;__tn__=-UK-R&amp;amp;c%5b0%5d=AT26pM4GYsoR9W3ME_0NSSx2WvhJ7NcaNujRJYbYqk_nvYiZ0yzu1clV_N7Z9jY_aBDtZK7VdjpghOCOgsPY2rWvu-6WrYvAaU5RiVg_sVQlOPiNEvmtQFqSOaxuJWwH3xGix5Mf3fnU0zeRYcA1R4Z9IRUgN3ffiuxKHC4Jr7a_4jN2IC66Uwz0RPC9b7uDABjkk5Tr&quot; target=&quot;_blank&quot;&gt;austdoor.com&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Youtube: &lt;a href=&quot;https://www.youtube.com/cuacuonaustdoor?fbclid=IwAR3E0deQvQ-vpEUk8Tdl0OgzjNHaj75tX1wG0DVCnCR33Sz5hrTTvnGkeuY&quot; target=&quot;_blank&quot;&gt;https://www.youtube.com/cuacuonaustdoor&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Fanpage: &lt;a href=&quot;https://www.facebook.com/austdoorofficial&quot;&gt;https://www.facebook.com/austdoorofficial&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;AUSTDOOR - CỬA CUỐN AN TOÀN &amp; THÔNG MINH HÀNG ĐẦU VIỆT NAM!&lt;/p&gt;\r\n&lt;/div&gt;', '&lt;p&gt;Trải nghiệm thực tế cửa cuốn Austdoor tốc độ cao HS!&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopal55/tintuc8.png', '', '', '', 'trai-nghiem-thuc-te-cua-cuon-austdoor-toc-do-cao-hs', '', 'image', '', NULL),
(20, 2, 'Austdoor chính thức ra mắt bộ sưu tập cửa cuốn phong thủy', '&lt;div class=&quot;content-detail&quot;&gt;\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;&lt;strong&gt;Ngày 8/11, Austdoor chính thức giới thiệu tới khách hàng bộ sưu tập cửa cuốn phong thủy - Nghênh phúc đón tài, chào Xuân Nhâm Dần 2022.&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;Theo văn hóa và quan niệm của người Việt; cửa chính, cửa mặt tiền được coi là nơi giao thoa giữa luồng không khí bên trong và bên ngoài của ngôi nhà, là khu vực đón tài lộc, đón vượng khí tốt nhất. Chính vì vậy, lựa chọn những bộ cửa phong thủy tốt không chỉ để bảo vệ sự an toàn, thẩm mỹ cho ngôi nhà mà còn mang ý nghĩa may mắn, bình an và tài lộc cho gia chủ.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;Thực tế, các nghiên cứu khoa học đã chứng minh rằng phong thủy tác động một phần không nhỏ đến sức khỏe, hạnh phúc, công danh, tài lộc của con người. Có nghĩa là, khi chúng ta biết vận dụng phong thủy một cách khoa học, có chừng mực thì vận may, những điều tốt lành sẽ tới. &lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Xu hướng cửa phong thủy&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;Nguồn gốc ban đầu phong thủy được hiểu là sự lý giải về thế giới tự nhiên nhằm phục vụ cho nhu cầu sản xuất và canh tác nông nghiệp. Theo dòng thời gian, phong thủy còn có vai trò trong việc giúp con người xác định sự vận động của quỹ thời gian. Ngày nay, cùng với sự phát triển của nhận thức, phong thủy được xem như là một bộ môn khoa học tổng hợp từ các môn khoa học thực nghiệm và lý thuyết khác như vật lý địa cầu, thủy văn địa chất, vũ trụ tinh thể học, khí tượng học, môi trường học và kiến trúc.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;Khoa học &lt;strong&gt;phong thủy trong lĩnh vực xây dựng, kiến trúc&lt;/strong&gt; dựa trên sự hình thành, phát triển và các quy luật về tự nhiên, tập trung vào các yếu tố phương hướng, địa lý, kích thước... Những công trình tốt không chỉ đảm bảo sự tiện nghi, thẩm mỹ, đáp ứng tốt nhu cầu sử dụng của gia chủ; mà còn được thiết kế, xây dựng phù hợp với các điều kiện ngoại cảnh, bên cạnh đó, chính là sự hòa hợp với thiên nhiên trời đất, hợp với tuổi, mệnh... của gia chủ.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;Với tầm quan trọng đó, rất nhiều khách hàng hiện nay quan tâm và mong muốn lựa chọn các loại cửa phong thủy để mang lại tài lộc, may mắn cho gia đình. Nếu trước đây, sản phẩm cửa cuốn chỉ được người dùng quan tâm lựa chọn về công nghệ hiện đại, an toàn, mẫu mã, màu sắc đẹp thì nay, kích thước phù hợp phong thủy, bản mệnh chủ nhà là một trong những tiêu chí quan trọng. Nắm bắt được xu hướng, Austdoor đã tiên phong nghiên cứu và giới thiệu tới khách hàng Bộ sưu tập cửa cuốn phong thủy, không chỉ có nền tảng công nghệ thông minh, thẩm mỹ sẵn có mà còn mang đến cho khách hàng những mẫu cửa thiết kế riêng với kích thước đẹp, thuộc vào các cung phúc, lộc theo quan niệm phong thủy. Đây được xem là một &quot;luồng gió&quot; mới, mang dấu ấn riêng chỉ có ở Austdoor.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/z2910664111151_e3320db4c895f3288ca9189cd202c165.jpg&quot; style=&quot;width: 700px; height: 525px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p align=&quot;center&quot;&gt;&lt;em&gt;Bộ sưu tập cửa cuốn phong thủy Austdoor - Nghênh phúc đón tài.&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;Là thương hiệu cửa cuốn an toàn và thông minh hàng đầu Việt Nam, Austdoor luôn nỗ lực tìm hiểu, không ngừng nghiên cứu để tạo ra những bộ cửa cuốn thẩm mỹ hiện đại, sử dụng an toàn, an ninh; và mới nhất là bộ sưu tập cửa cuốn phong thủy, đa dạng mẫu mã, kích thước giúp khách hàng có thêm nhiều lựa chọn, đặc biệt hữu ích với các chủ đầu tư, hộ gia đình, kinh doanh chuẩn bị xây dựng công trình, nhà mới.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Giúp khách hàng lựa chọn cửa cuốn phù hợp&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;Phong thủy ứng dụng trong lĩnh vực xây dựng, kiến trúc là việc xem xét tổng hòa các đối tượng tác động với những đặc tính cơ bản bao gồm tính tổng hợp, tính linh hoạt, tính cân bằng, tính ổn định và tính văn hóa. Bởi vậy, để có một công trình thực sự &quot;hợp phong thủy&quot;, chủ công trình, kiến trúc sư và nhà thầu xây dựng cần xem xét và quan tâm tới rất nhiều yếu tố. Trong đó, vị trí, hướng, kích thước, màu sắc, phối cảnh công trình và phong thủy cửa chính rất được chú trọng.&lt;/p&gt;\r\n\r\n&lt;p&gt;Khi đã xác định được vị trí, hướng cửa, thì kích thước cửa chính là một tiêu chí quan trọng trong phong thủy hiện đại. Chúng vừa quyết định yếu tố công năng, thẩm mỹ, vừa có ý nghĩa phong thủy của toàn bộ ngôi nhà; vì vậy cần có sự tính toán tỉ mỉ, cẩn thận trước khi lựa chọn số đo cửa để đảm bảo phù hợp, tương xứng với công trình.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://austdoor.com/upload_images/images/z2910664127468_55c7dd59afec3307cb8b87ec5abcd9eb.jpg&quot; style=&quot;width: 700px; height: 525px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p align=&quot;center&quot;&gt;&lt;em&gt;Thước Lỗ Ban 52,2cm dùng để đo khoảng thông thủy của cửa chính.&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: justify;&quot;&gt;Để xác định chính xác kích thước cửa đẹp theo phong thủy, ngày nay, các nhà thiết kế thường sử dụng thước Lỗ Ban 52,2cm. Trên thước này, các chuyên gia phong thủy đã giúp xác định những thông số vàng cho kích thước cửa để mang đến những điều tốt đẹp nhất cho gia chủ. Với việc ứng dụng công nghệ và nền tảng trực tuyến, thước Lỗ Ban 52,2cm hiện có phiên bản &quot;online&quot; giúp khách hàng dễ dàng tra cứu và sử dụng. Một &quot;quy tắc&quot; quan trọng được đúc kết để người dùng dễ nhớ khi đo kích thước cửa chính đẹp theo phong thủy đó là &quot;đen bỏ, đỏ dùng&quot;. Theo đó, để có được một bộ cửa đẹp, khách hàng nên lựa chọn kích thước thông thủy và phủ bì theo cả chiều dài và chiều rộng của cửa rơi vào các cung mang ý nghĩa tốt (có màu đỏ trên thước Lỗ Ban 52,2cm).&lt;/p&gt;\r\n\r\n&lt;p&gt;Dựa trên quy tắc này, bộ sưu tập cửa cuốn phong thủy của Austdoor mang đến cho khách hàng những sản phẩm cửa cuốn có kích thước cửa thuộc &quot;cung đẹp&quot; và đa dạng màu sắc để khách hàng dễ dàng lựa chọn. Trong sự kiện trực tuyến giới thiệu Bộ sưu tập cửa cuốn phong thủy dành cho các đại lý, đối tác, Austdoor cũng sẽ giới thiệu chi tiết các dòng sản phẩm và đưa ra bộ tài liệu hướng dẫn sử dụng, cách đọc thước Lỗ Ban 52,2cm phiên bản Online để hỗ trợ quá trình tư vấn của đại lý tới người tiêu dùng.&lt;/p&gt;\r\n\r\n&lt;p&gt;Việc ra mắt bộ sưu tập cửa cuốn phong thủy của Austdoor sẽ góp phần giải đáp những thắc mắc, trăn trở và đưa ra gợi ý hữu hiệu cho các khách hàng quan tâm đến phong thủy hiện đại.&lt;/p&gt;\r\n\r\n&lt;p&gt;Để sở hữu sản phẩm của Bộ sưu tập cửa cuốn phong thủy, vui lòng liên hệ hotline 1900 6828 để được tư vấn hỗ trợ.&lt;/p&gt;\r\n\r\n&lt;table border=&quot;1&quot; cellpadding=&quot;1&quot; cellspacing=&quot;1&quot; style=&quot;width:1050px;&quot;&gt;\r\n	&lt;tbody&gt;\r\n		&lt;tr&gt;\r\n			&lt;td&gt;\r\n			&lt;p&gt;&lt;em&gt;Gợi ý cho chủ nhà &amp; các chuyên gia khi lắp đặt cửa chính hợp phong thủy:&lt;/em&gt;&lt;/p&gt;\r\n\r\n			&lt;p style=&quot;text-align: justify;&quot;&gt;- Bên cạnh kích thước cửa thì màu sắc, hướng cửa cũng rất quan trọng trong phong thủy hiện đại.&lt;/p&gt;\r\n\r\n			&lt;p&gt;- Đừng bỏ qua nền tảng sản phẩm chất lượng và chính hãng để sở hữu những công nghệ an ninh, bền bỉ, đảm bảo công năng và sự an toàn tối đa cho gia đình.&lt;/p&gt;\r\n\r\n			&lt;p&gt;- Luôn giữ cho khu vực xung quanh nhà cửa được sạch sẽ, thông thoáng.&lt;/p&gt;\r\n			&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n	&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n\r\n&lt;p&gt; &lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: right;&quot;&gt;&lt;em&gt;Nguồn: Dantri: &lt;a href=&quot;https://dantri.com.vn/kinh-doanh/austdoor-chinh-thuc-ra-mat-bo-suu-tap-cua-cuon-phong-thuy-20211108121014057.htm&quot;&gt;https://dantri.com.vn/kinh-doanh/austdoor-chinh-thuc-ra-mat-bo-suu-tap-cua-cuon-phong-thuy-20211108121014057.htm&lt;/a&gt;&lt;/em&gt;&lt;/p&gt;\r\n&lt;/div&gt;', '&lt;p&gt;Ngày 8/11, Austdoor chính thức giới thiệu tới khách hàng bộ sưu tập cửa cuốn phong thủy - Nghênh phúc đón tài, chào Xuân Nhâm Dần 2022.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopal55/tintuc4.jpg', '', '', '', 'austdoor-chinh-thuc-ra-mat-bo-suu-tap-cua-cuon-phong-thuy', '', 'image', '', NULL),
(21, 2, 'Dùng thước nào chuẩn để đo kích thước thông thủy cửa cuốn', '&lt;div class=&quot;container&quot;&gt;\r\n&lt;div class=&quot;content-module&quot;&gt;\r\n&lt;div class=&quot;box-content&quot;&gt;\r\n&lt;h1 class=&quot;title-page&quot;&gt;Dùng thước nào chuẩn để đo kích thước thông thủy cửa cuốn&lt;/h1&gt;\r\n\r\n&lt;div class=&quot;content-detail&quot;&gt;\r\n&lt;p&gt;&lt;strong&gt;Muốn đo kích thước cửa đẹp, dùng thước nào? Đây là câu hỏi rất nhiều khách hàng đang băn khoăn, quý vị cùng theo dõi bài viết dưới đây để có câu trả lời chính xác.&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Hiện nay, người dùng hay thậm chí là thợ lắp đặt vẫn đang sử dụng thước Lỗ Ban thông dụng trên thị trường để đo kích thước, sau đó tìm ra cung đẹp để lựa chọn, thế nhưng lại chưa hiểu hết về ý nghĩa và phạm vi sử dụng của từng loại thước, chính vì vậy là kết quả thường có sự sai khác.&lt;/p&gt;\r\n\r\n&lt;p&gt;Thước Lỗ Ban là một loại thước thông dụng trong xây dựng, giúp khách hàng lựa chọn được kích thước đẹp nên dùng, kích thước xấu nên tránh trong xây dựng các công trình.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;https://cdn.bestme.asia/images/adgtopalvp/dung-thuoc-do.jpg&quot; style=&quot;width: 700px; height: 525px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;em&gt;Thước Lỗ Ban 52,2cm dùng để đo chính xác kích thước cửa cuốn phong thủy&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Thước Lỗ Ban gồm 3 loại:&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;• Thước Lỗ Ban 52,2cm (hay thước 52):&lt;/strong&gt; Để đo khoảng không (đo lọt lòng), các khoảng thông thủy trong nhà như: cửa chính, cửa đi,…&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;• Thước Lỗ Ban 42,9cm (hay thước 43):&lt;/strong&gt; Để đo kích thước dương trạch, khối đặc, đồ đạc nội thất: kích thước giường tủ, bệ bếp, bậc…&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;• Thước Lỗ Ban 38,8cm (hay thước 39):&lt;/strong&gt; Để đo kích thước âm phần: bàn thờ, mộ phần…&lt;/p&gt;\r\n\r\n&lt;p&gt;Trong xây dựng, kích thước thông thủy của cửa chính rất quan trọng bởi đây là nơi:&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;Nạp khí cho toàn bộ cấu trúc ngôi nhà&lt;/li&gt;\r\n	&lt;li&gt;Quyết định lớn đến khí trường của ngôi nhà&lt;/li&gt;\r\n	&lt;li&gt;Quyết định sự hưng vượng của ngôi nhà&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p&gt; &lt;/p&gt;\r\n\r\n&lt;p&gt;Chính vì vậy, lựa chọn kích thước thông thủy đẹp góp phần rất lớn vào việc mang vận khí may mắn cho gia chủ. Với sản phẩm cửa nói chung, cửa cuốn nói riêng, kích thước thông thủy sẽ được đo bằng thước Lỗ Ban 52,2cm.&lt;/p&gt;\r\n\r\n&lt;p&gt;Xem chi tiết hướng dẫn sử dụng thước Lỗ Ban 52,2cm &lt;a href=&quot;https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DwRiKXSVs630%26fbclid%3DIwAR3qorNv62fT4nyYRtcvZigF3bLO5j4t1jR6MSCTuCO7_2Ba9CJiK5BNNC0&amp;amp;h=AT2O-59yuVEa-URTtnPMFFE5t14zMTAm6TzZUyKcZDt9GBe9D5BR_IPHMi943iXWIldg4G_z5n0ZfWNFFqnnMuK4zikBlc8nInG8mGZlfqZwvrGa11F-aqF7JJO57JNa9-kM&amp;amp;__tn__=R]-R&amp;amp;c[0]=AT2Ug5KrOMZxHrNRqZnu71tObXAHl8SPkpStqOtfX-radINNHBLRNOfTL77RiDftOW_kCX2nhC8WZHNoxiRCjcC2lJGsLGkyB-PdFebcp9AuBxxOGvpGQ73_7p8lv3FzJAsT5UxA22iKIL6XM-tBjiv-howTfLu954op7sl1aVtMnqzgbCeU8WldQSgQDaJX1jpUUHtouQbVdayqYugV&quot;&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;strong&gt;tại đây&lt;/strong&gt;&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Hãy lựa chọn đúng kích thước thông thủy cho cửa cuốn bởi đúng loại thước và chức năng của nó. Và cùng Nghênh phúc đón tài với Bộ sưu tập cửa cuốn phong thủy Austdoor!&lt;/p&gt;\r\n\r\n&lt;p&gt;----------&lt;/p&gt;\r\n\r\n&lt;p&gt;Và hãy nhớ, Austdoor - cửa cuốn an toàn và thông minh là lựa chọn an tâm cho gia đình.&lt;/p&gt;\r\n\r\n&lt;p&gt;Mọi thắc mắc về sản phẩm, Quý Khách hàng vui lòng liên hệ:&lt;/p&gt;\r\n\r\n&lt;p&gt;- Hotline: 1900 6828&lt;/p&gt;\r\n\r\n&lt;p&gt;- Website: &lt;a href=&quot;https://l.facebook.com/l.php?u=http%3A%2F%2Faustdoor.com%2F%3Ffbclid%3DIwAR3I9Iyf_B6sHXgjPrEguFWBvoUy93HmDOTOFXSzy8B7OYa0fqtt8MybdBI&amp;amp;h=AT0wLmRG_PsqAuOmSZaLbp0m3HFV4AGrMVE1ERsERhTeqS6fz7ffXoA-3Ot1ipdJRi0ExXYtbCN4nxJowuzzF-_kXAK3-t7xhD4vUQR7OL9s0hTU-PRVqb99AFavAKiXV4FM&amp;amp;__tn__=-UK-R&amp;amp;c%5b0%5d=AT26pM4GYsoR9W3ME_0NSSx2WvhJ7NcaNujRJYbYqk_nvYiZ0yzu1clV_N7Z9jY_aBDtZK7VdjpghOCOgsPY2rWvu-6WrYvAaU5RiVg_sVQlOPiNEvmtQFqSOaxuJWwH3xGix5Mf3fnU0zeRYcA1R4Z9IRUgN3ffiuxKHC4Jr7a_4jN2IC66Uwz0RPC9b7uDABjkk5Tr&quot; target=&quot;_blank&quot;&gt;austdoor.com&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Youtube: &lt;a href=&quot;https://www.youtube.com/cuacuonaustdoor?fbclid=IwAR3E0deQvQ-vpEUk8Tdl0OgzjNHaj75tX1wG0DVCnCR33Sz5hrTTvnGkeuY&quot; target=&quot;_blank&quot;&gt;https://www.youtube.com/cuacuonaustdoor&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;- Fanpage: &lt;a href=&quot;https://www.facebook.com/austdoorofficial&quot;&gt;https://www.facebook.com/austdoorofficial&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;AUSTDOOR - CỬA CUỐN AN TOÀN &amp; THÔNG MINH HÀNG ĐẦU VIỆT NAM!&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;!--End content-detail--&gt;&lt;!--End body-wrapper--&gt;&lt;!--End body-module--&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;', '&lt;p&gt;Muốn đo kích thước cửa đẹp, dùng thước nào? Đây là câu hỏi rất nhiều khách hàng đang băn khoăn, quý vị cùng theo dõi bài viết dưới đây để có câu trả lời chính xác.&lt;/p&gt;', 'https://cdn.bestme.asia/images/adgtopal55/tintuc2.jpg', '', '', '', 'dung-thuoc-nao-chuan-de-do-kich-thuoc-thong-thuy-cua-cuon', '', 'image', '', NULL);

-- oc_blog_to_blog_category NO PRIMARY KEY
-- remove first
DELETE FROM `oc_blog_to_blog_category`
WHERE `blog_id` IN (1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 3, 15, 16, 17, 18, 19, 20, 21)
  AND `blog_category_id` = 1;

-- insert
INSERT IGNORE INTO `oc_blog_to_blog_category` (`blog_id`, `blog_category_id`) VALUES
(1, 6),
(2, 4),
(3, 5),
(4, 3),
(5, 3),
(6, 3),
(7, 3),
(8, 3),
(9, 2),
(10, 3),
(11, 2),
(12, 1),
(13, 5),
(14, 5),
(15, 1),
(16, 1),
(17, 1),
(18, 4),
(19, 4),
(20, 1),
(21, 4);

-- --------End - Load theme default data ---------------------

-- --------Add migrated versions for new shops ---------------

-- Dumping data for table `oc_migration`
INSERT INTO `oc_migration` (`migration`, `run`, `date_run`) VALUES
-- ('migration_20190718_default_policy_gg_shopping', 1, NOW()), -- already above when first time add migration feature
('migration_20190729_enable_seo', 1, NOW()),
('migration_20190729_2_fix_demohttp://x2.bestme.test/du-an-thi-cong-cua-nhom-kinh-cho-he-thong-vinmart?type=du-an_data_sql', 1, NOW()),
('migration_20190731_remake_menu', 1, NOW()),
('migration_20190810_staff_permission', 1, NOW()),
('migration_20190822_product_multi_version', 1, NOW()),
('migration_20190828_chatbot', 1, NOW()),
('migration_20190828_transport', 1, NOW()),
('migration_20190903_sync_staffs_to_welcome', 1, NOW()),
('migration_20190904_keep_edited_demo_data', 1, NOW()),
('migration_20190930_increase_decimal_order', 1, NOW()),
('migration_20191015_new_report_tables_and_events', 1, NOW()),
('migration_20191016_new_report_data', 1, NOW()),
('migration_20191023_new_report_events', 1, NOW()),
('migration_20191024_report_order_from_shop', 1, NOW()),
('migration_20191028_add_field_from_domain_order', 1, NOW()),
('migration_20191029_default_delivery_method', 1, NOW()),
('migration_20191111_blog_permission_and_seo', 1, NOW()),
('migration_20191115_order_success_seo_and_text', 1, NOW()),
('migration_20191116_add_settings_classify_permission', 1, NOW()),
('migration_20191127_sync_onboading_to_welcome', 1, NOW()),
('migration_20191209_default_store', 1, NOW()),
('migration_20191210_add_columns_for_pos', 1, NOW()),
('migration_20191223_save_image_url_to_database', 1, NOW()),
('migration_20191225_builder_product_per_row_mobile', 1, NOW()),
('migration_20191226_register_success_text', 1, NOW()),
('migration_20200103_soft_delete_product', 1, NOW()),
('migration_20200117_add_settings_notify_permission', 1, NOW()),
('migration_20200212_discount', 1, NOW()),
('migration_20200314_chatbot_v2', 1, NOW()),
('migration_20200312_store_receipt', 1, NOW()),
('migration_20200410_sync_report_order_to_cms', 1, NOW()),
('migration_20200420_add_table_order_utm', 1, NOW()),
('migration_20200420_store_permission', 1, NOW()),
('migration_20200421_fill_product_to_store_with_default_store', 1, NOW()),
('migration_20200422_add_column_table_product', 1, NOW()),
('migration_20200520_appstore_version', 1, NOW()),
('migration_20200626_advance_store_manager_version', 1, NOW()),
('migration_20200717_shopee_connection', 1, NOW()),
('migration_20200731_add_column_table_shopee_order', 1, NOW()),
('migration_20200811_novaonx_social', 1, NOW()),
('migration_20200816_add_column_demo_blog_table', 1, NOW()),
('migration_20200812_lazada_connection', 1, NOW()),
('migration_20200817_add_column_alt_image_blog_table', 1, NOW()),
('migration_20200817_move_chatbot_to_app_store', 1, NOW()),
('migration_20200818_onfluencer_app', 1, NOW()),
('migration_20200821_lazada_sync_order', 1, NOW()),
('migration_20200819_add_columns_for_customer', 1, NOW()),
('migration_20200904_cash_flow', 1, NOW()),
('migration_20200821_lazada_sync_order_again', 1, NOW()),
('migration_20200904_activity_log_analytics', 1, NOW()),
('migration_20200921_lazada_product_short_description', 1, NOW()),
('migration_20200915_shopee_advance_product', 1, NOW()),
('migration_20200916_pull_image_from_cloudinary', 1, NOW()),
('migration_20200922_transport_status_management', 1, NOW()),
('migration_20201007_tracking_pos_order_complete', 1, NOW()),
-- ('migration_20200924_full_text_search_product_name', 1, NOW()), -- temp not use
('migration_20201002_add_video_to_blog', 1, NOW()),
('migration_20201021_pos_return_goods', 1, NOW()),
('migration_20201019_bestme_package', 1, NOW()),
('migration_20201023_update_image_size', 1, NOW()),
('migration_20201103_create_image_folder', 1, NOW()),
('migration_20201106_add_columns_for_report', 1, NOW()),
('migration_20201118_add_columns_keyword_for_blog', 1, NOW()),
('migration_20201118_create_attribute_filter_table', 1, NOW()),
('migration_20201130_add_permission_customize_layout', 1, NOW()),
('migration_20201201_shopee_category_and_logistics', 0, NOW()), -- SET RUN=0 to keep running for new shop for loading data from file to shopee_category and shoppe_logistics table!
('migration_20201214_shopee_upload_add_tables', 1, NOW()),
('migration_20210107_tuning_2_11_2', 1, NOW()),
('migration_20210310_v3_3_1_advance_permission', 1, NOW()),
('migration_20210329_3_3_3_finetune', 1, NOW()),
('migration_20210409_add_columns_collection_amount_for_order', 1, NOW()),
('migration_20201125_add_column_store_id_for_return_receipt', 1, NOW()),
('migration_20210422_add_column_store_id_for_return_receipt_if_not_exists', 1, NOW()),
('migration_20210409_add_columns_collection_amount_for_order', 1, NOW()),
('migration_20210504_v3_4_2_sync_bestme_social', 1, NOW()),
('migration_20210517_add_column_order_id_on_campaign_voucher', 1, NOW()),
('migration_20210525_update_migrate_v3_4_2_add_new_table_order_channel', 1, NOW()),
('migration_20210616_v3_5_1_open_api', 1, NOW()),
('migration_20210812_create_keyword_sync_table', 1, NOW()),
('migration_20210817_add_column_source_sync', 1, NOW()),
('migration_20210914_add_email_subscribers', 1, NOW()),
('migration_20211012_add_new_column_to_table_product', 1, NOW());

-- --------end Add migrated versions for new shops -----------

-- -----------------------------------------------------------