<?php
// show error. TODO: remove on production
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Version
define('VERSION', '1.0');

// Configuration
if (is_file('config.php')) {
	require_once('config.php');
}

// Configuration
if (is_file('multi.config.php')) {
    require_once('multi.config.php');
}

// Install
if (!defined('DIR_APPLICATION')) {
	header('Location: install/index.php');
	exit;
}

// Startup
require_once(DIR_SYSTEM . 'startup.php');

start('catalog');
