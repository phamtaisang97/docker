-- -----------------------------------------------------------
--
-- By Novaon - Migrate db
--
-- -----------------------------------------------------------

SET sql_mode = '';

-- -----------------------------------------------------------
--
-- v1.4
--
-- -----------------------------------------------------------

--
-- more event for order
--
INSERT INTO `oc_event` (`code`, `trigger`, `action`, `status`, `sort_order`) VALUES
('edit order 2', 'admin/model/sale/order/editOrder/after', 'event/statistics/editOrderDetail', 1, 0),
('change status order 2', 'admin/model/sale/order/editOrder/before', 'event/statistics/updateStatusOrderDetail', 1, 0);


-- -----------------------------------------------------------
--
-- v1.5
--
-- -----------------------------------------------------------

--
-- Create table `oc_novaon_api_credentials`
--
CREATE TABLE `oc_novaon_api_credentials` (
  `id` int(11) NOT NULL,
  `consumer_key` varchar(255) NOT NULL,
  `consumer_secret` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `oc_novaon_api_credentials` (`id`, `consumer_key`, `consumer_secret`, `create_date`, `status`) VALUES
(1, 'GHNHTYDSTw567', 'GHNDRTUYUKMVFYJKNBGFFGFHJJ7886GH', '2019-06-03 01:15:59', 1);

ALTER TABLE `oc_novaon_api_credentials`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `oc_novaon_api_credentials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- enable extension module NovaonApi
--
INSERT INTO `oc_extension` (`type`, `code`) VALUES
('module', 'NovaonApi');

--
-- google shopping meta config
--
INSERT INTO `oc_setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES
(0, 'config', 'config_gg_shopping', '', 0);

--
-- add column previous_order_status_id to oc_order
--
ALTER TABLE `oc_order` ADD `previous_order_status_id` VARCHAR(50) NULL AFTER `order_status_id`;

--
-- TODO: Migrate by NOVAON from here...
--