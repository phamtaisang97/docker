<?php

class ControllerProductProductFlashsale extends Controller
{
    use Flash_Sale_Trait;

    public function index()
    {
        $data = $this->flashSale();
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        $this->response->setOutput($this->load->view('product/product_flashsale', $data));
    }
}