<?php

class ControllerProductProduct extends Controller
{
    use Theme_Config_Util;
    use Device_Util;
    use Product_Util_Trait;
    use Flash_Sale_Trait;

    /*
     * Important: this const is used to avoid too much memory used and reduce loading time
     * Current set 32 products to satisfied with grid config 2, 4, 6 with 1, 2 product lines
     * TODO: admin config or choose other satisfied number...
     */
    const LIMIT_PRODUCT_IN_BLOCK = 32;

    const IS_BUY_NOW_INSTEAD_OF_INSTALLMENT_CONFIG_KEY = 'config_is_buy_now_instead_of_installment';

    const URL_BESTME = "https://bestme.vn/products";

    public function index()
    {
        $data = [];

        $data['theme_directory'] = '/catalog/view/theme/' . $this->getCurrentThemeDir();

        $data['is_on_mobile'] = $this->isMobile() ? 1 : 0;

        $data['is_ios'] = $this->isIosDevice() ? 1 : 0;

        $this->load->language('product/product_detail');
        $this->document->setTitle($this->language->get('text_product_detail_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('product_detail_title'),
            'href' => $this->url->link('common/shop')
        );

        /* get product detail due to id */
        $this->load->model('catalog/product');

        $product_id = $this->request->get['product_id'];
        $product_variant_id = !empty($this->request->get['variant']) ? $this->request->get['variant'] : '';
        $product = $this->model_catalog_product->getProduct($product_id);
        $productCategories = array_reverse($this->model_catalog_product->getCategoriesAndParentsForBradcrumd($product_id));
        $data['original_price'] = isset($product['compare_price_master']) ? $product['compare_price_master'] : '';
        $not_found = false;
        if (empty($product) || $product['product_id'] == null) {
            $not_found = true;
            $this->load->language('error/not_found');
        }

        if ($product) {
            if (!empty($productCategories)) {
                foreach ($productCategories as $category) {
                    $data['breadcrumbs'][] = array(
                        'text' => $category['name'],
                        'href' => $category['href']
                    );
                }
            }

            $data['breadcrumbs'][] = array(
                'text' => $product['name'],
                'href' => $this->url->link('product/product', '&product_id=' . $this->request->get['product_id']),
                'active' => true,
            );
        }

        $this->load->model('tool/image');
        if ($product['multi_versions'] == 0) {
            $product['schema_price'] = !empty($product['price_master']) && intval($product['price_master']) ? intval($product['price_master']) : intval($product['compare_price_master']);
        } else {
            $productVersions = $this->model_catalog_product->getProductVersions($product_id);
            if (!empty($productVersions)) {
                $length = count($productVersions);
                usort($productVersions, function ($item1, $item2) {
                    return $item1['compare_price'] <=> $item2['compare_price'];
                });

                $product['schema_price_min'] = intval($productVersions[0]['compare_price']);
                $product['schema_price_max'] = intval($productVersions[$length - 1]['compare_price']);
            }
        }



        $data['is_product_existing'] = true;
        if (empty($product) || !is_array($product)) {
            $data['is_product_existing'] = false;
        } else {
            /* resize image */
            $product['image'] = $product['image'] ? $this->resizeImage($product['image'], 2000, 2000) : $product['image']; // Avoid breaking when zooming in, TODO ...
            $product['image'] = $this->changeUrl($product['image']);
            $product_image_empty = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height'));
            if (empty($product['image'])) {
                $product['image'] = $product_image_empty;
            }
            /* get description tab  */
            $product_ingredient = $this->model_catalog_product->getIngredientOfProductByProductId($product_id);

            /* get description tab  */
            $description_tabs = $this->model_catalog_product->getProductDescriptionTab($product_id);
            $this->load->model('custom/common');
            foreach ($description_tabs as &$description_tab) {
                $description_tab['slug'] =  $this->model_custom_common->createSlug($description_tab['title'], '-');
            }

            $data['description_tabs'] = $description_tabs;
            $data['product_ingredient'] = $product_ingredient;

            /* convert data product */
            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $price_master = $this->currency->formatCustom($this->tax->calculate($product['price_master'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $price_master = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $compare_price_master = $this->currency->formatCustom($this->tax->calculate($product['compare_price_master'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $compare_price_master = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $min_price_version = $this->currency->formatCustom($this->tax->calculate($product['min_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $min_price_version = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $max_price_version = $this->currency->formatCustom($this->tax->calculate($product['max_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $max_price_version = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $min_compare_price_version = $this->currency->formatCustom($this->tax->calculate($product['min_compare_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $min_compare_price_version = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $max_compare_price_version = $this->currency->formatCustom($this->tax->calculate($product['max_compare_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $max_compare_price_version = false;
            }
            if (empty($product['product_version_id'])) {
                $product['price_master_real'] = ((int)$product['price_master'] != 0) ? 1 : 0;
                $product['max_price_version_real'] = 1;
            }
            if (!empty($product['product_version_id'])) {
                $product['max_price_version_real'] = ((int)$product['price_version_check_null'] != 0) ? 1 : 0;
                $product['price_master_real'] = 1;
            }
            $product['percent_sale'] = empty($product['max_price_version']) ? getPercent($product['price_master'], $product['compare_price_master']) : getPercent($product['min_price_version'], $product['min_compare_price_version'], $product['max_percent_pv']);
            $product['percent_sale_value'] = empty($product['max_price_version'])
                ? getPercent($product['price_master'], $product['compare_price_master'], 0, "")
                : getPercent($product['min_price_version'], $product['min_compare_price_version'], $product['max_percent_pv'], "");
            $product['price_master'] = $price_master;
            $product['compare_price_master'] = $compare_price_master;
            $product['min_price_version'] = $min_price_version;
            $product['max_price_version'] = empty($product['max_price_version']) ? $product['max_price_version'] : $max_price_version;
            $product['min_compare_price_version'] = $min_compare_price_version;
            $product['max_compare_price_version'] = empty($product['max_compare_price_version']) ? $product['max_compare_price_version'] : $max_compare_price_version;
            $product_version_id = (int)$product['product_version_id'];
            $product['product_is_stock'] = $this->model_catalog_product->isStock($product_id, $product_version_id);
            /* TODO: remove if check is_stock ok */
            //        $product['product_is_stock'] = false;
            //        if ($product['product_version_status_max'] !== "0" && $product['sale_on_out_stock'] == 1) {
            //            $product['product_is_stock'] = true;
            //        } elseif (empty($product['product_version_id']) && (int)$product['product_master_quantity'] > 0) {
            //            $product['product_is_stock'] = true;
            //        } elseif ($product['product_version_status_max'] !== "0" && !empty($product['product_version_id']) && (int)$product['product_version_quantity'] > 0) {
            //            $product['product_is_stock'] = true;
            //        }
            $product['product_mas_ver_quantity'] = empty($product['product_version_id']) ? (int)$product['product_master_quantity'] : (int)$product['product_version_quantity'];
            $product['sale_on_out_stock_check'] = true;
            if (empty($product['product_version_id']) && $product['product_master_quantity'] < 1 && $product['sale_on_out_stock'] == 0) {
                $product['sale_on_out_stock_check'] = false;
            }
            $product['multi_versions'] = $product['multi_versions'];
            $product['is_contact_for_price'] = $this->isContactForPrice($product['multi_versions'], $product['compare_price_master'], $product['min_compare_price_version']);
        }

        $price_2_text = $this->currency->formatCustom($this->tax->calculate((float)$product['price_2'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
        $compare_price_2_text = $this->currency->formatCustom($this->tax->calculate((float)$product['compare_price_2'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
        $product['price_2_text'] = isset($price_2_text) ? $price_2_text : 0;
        $product['compare_price_2_text'] = isset($compare_price_2_text) ? $compare_price_2_text : 0;

        if ((float)$product['price_discount'] == 0 && !empty($product['discount_id'])) {
            $product['percent_sale'] = -100;
            $product['percent_sale_value'] = '-100%';
        }

        // Add-on Deal
        $this->load->model('discount/add_on_deal');
        $product['has_deal'] = false;
        $product['deal_data'] = $this->model_discount_add_on_deal->getAddOnDealByProductId($product['product_id']);

        if (!empty($product['deal_data'])) {
            $product['has_deal'] = true;
            $product['add_on_products'] = $this->addOnProductListAndChecked($product['deal_data']['add_on_deal_id'], $product['product_id'], 0);
        }

        $data['product'] = $product;
        // for share social
        $data['current_url'] = $this->url->link('product/product', 'product_id=' . $this->request->get['product_id']);
        // for product url click
        $data['current_url_href'] = $this->url->link('product/product', 'product_id=' . $this->request->get['product_id']);
        $url_dhc = HTTPS_SERVER."san-pham";
        $data['current_url_bestme'] = str_replace($url_dhc, self::URL_BESTME, $data['current_url_href']);
        /* get product attribute */
        $data['attributes'] = [];
        $attributes = $this->model_catalog_product->getProductAttributeCustom($this->request->get['product_id']);
        if ($attributes) {
            foreach ($attributes as $attribute) {
                $data['attributes'][] = [
                    'product_id' => $attribute['product_id'],
                    'attribute_name' => $attribute['name'],
                    // 'sale_on_out_of_stock' => strval((int)$this->model_catalog_product->isStock($this->request->get['product_id'], $attribute['product_version_id'])),
                    'attribute_text' => explode(',', $attribute['text']),
                ];
            }
        }
        $data['versions'] = [];
        if (isset($productVersions)) {
            foreach ($productVersions as $key => $productVersion) {
                $has_deal = false;
                $deal = $this->model_discount_add_on_deal->getAddOnDealByProductId($this->request->get['product_id'], $productVersion['product_version_id']);
                $is_stock = $this->model_catalog_product->isStock($this->request->get['product_id'], $productVersion['product_version_id']);


                if (!empty($deal)) {
                    $has_deal = true;
                }

                $checked_version = false;
                if (!empty($product_variant_id) && $is_stock &&
                    ($product_variant_id == $productVersion['product_version_id'])
                    ) {
                    $checked_version = true;
                }

                if ($is_stock && $checked_version) {
                    $data['index_stock_version'] = $key;
                } else {
                    if ($is_stock && !isset($data['index_stock_version'])) {
                        $data['index_stock_version'] = $key;
                    }
                }

                $data['versions'][] = [
                    'version' => $productVersion['version'],
                    'is_stock' => $is_stock,
                    'has_deal' => $has_deal,
                ];
            }
            if (!isset($data['index_stock_version'])) {
                $data['index_stock_version'] = 0;
            }
        }

        /* v3.4.3-Finetune */
        $cookie_name = '_viewed_products';
        $date_now = new DateTime(date("Y-m-d"));
        if (isset($_COOKIE)) {
            $viewed_products_cookie = isset($_COOKIE[$cookie_name]) ? $_COOKIE[$cookie_name] : '[]';
            $viewed_products_cookie = json_decode($viewed_products_cookie, true);

            if (empty($viewed_products_cookie)) {
                $viewed_products_cookie[] = [
                    'product_id' => $product_id,
                    'time' => strtotime(date("Y-m-d H:i:s"))
                ];
            } else {
                foreach ($viewed_products_cookie as $key => $item) {
                    $date_value = new DateTime(date('Y-m-d', $item['time']));
                    $interval = $date_now->diff($date_value);
                    if ($interval->days > 3) {
                        //remove item of $viewed_products_cookie
                        unset($viewed_products_cookie[$key]);
                        continue;
                    }
                    if ($key == 7) {
                        unset($viewed_products_cookie[0]);
                        continue;
                    }
                    $viewed_products_cookie[] = [
                        'product_id' => $product_id,
                        'time' => strtotime(date("Y-m-d H:i:s"))
                    ];
                }
            }
            $viewed_products_cookie = json_encode($this->unique_multidim_array($viewed_products_cookie, 'product_id'));
            setcookie($cookie_name, $viewed_products_cookie, time() + (7 * 24 * 60 * 60), "/");
            // note : time() + (7 * 24 * 60 * 60) => 7 days; 24 hours; 60 mins; 60 secs
        }

        if (isset($_COOKIE['_viewed_products'])) {
            $viewed_products_ids = array_map(function ($p) {
                return $p->product_id;
            }, json_decode($_COOKIE['_viewed_products']));
        }

        if (!empty($viewed_products_ids)) {
            $data['products_viewed'] = $this->getProductsByID($viewed_products_ids);
        }

        $data['product_viewed_title'] = $this->language->get('text_product_viewed_title');

        /* end v3.4.3-Finetune */

        /* get product category */
        $data['categories'] = [];
        $categories = $this->model_catalog_product->getCategoriesCustom($this->request->get['product_id']);
        if ($categories) {
            foreach ($categories as $category) {
                $data['categories'][] = [
                    'name' => $category['name'],
                    'href' => $this->url->link('common/shop', 'path=' . $category['category_id'], true),
                ];
            }
        }
        $data['tags'] = [];
        $this->load->model('catalog/tag');
        $tags = $this->model_catalog_tag->getTagsByProductId($this->request->get['product_id']);
        if ($tags) {
            foreach ($tags as $tag) {
                $data['tags'][] = [
                    'name' => $tag['title'],
                    'href' => $this->url->link('common/shop', 'nametag=' . $tag['id'], true),
                ];
            }
        }

        /* get product images */
        $this->load->model('catalog/product');
        $product_images = $this->model_catalog_product->getProductImages($product_id);
        $product_images = empty($product_images) || !is_array($product_images) ? [] : $product_images;

        foreach ($product_images as &$product_image) {
            $image = $this->resizeImage($product_image['image'], 2000, 2000); // Avoid breaking when zooming in, TODO ...
            $product_image['image_thump'] = $this->changeUrl($image, true, 'product_detail_thump');
            $product_image['image'] = $this->changeUrl($image, true, 'product_detail');
        }
        unset($product_image);
        $base_image_alt = $this->model_catalog_product->getBaseImageAlt($product_id);
        //add image default
        if ($product['image']) {
            $product_image_default = [
                'image' => $this->changeUrl($product['image'], true, 'product_detail'),
                'image_thump' => $this->changeUrl($product['image'], true, 'product_detail_thump'),
                'product_id' => $this->request->get['product_id'],
                'image_alt' => $base_image_alt
            ];
            array_unshift($product_images, $product_image_default);
        }

        $data['product_images'] = $product_images;
        $data['product_empty'] = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height'));

        $data['manufacturer'] = $product['manufacturer'];

        /* get currency symbol */
        $data['currency'] = $this->currency->getSymbolRight($this->session->data['currency']);
        if (empty($data['currency'])) {
            $data['currency'] = $this->currency->getSymbolLeft($this->session->data['currency']);
        }
        $data['currency'] = 'đ';

        /* get social config */
        $this->load->model('extension/module/theme_builder_config');
        $social_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_SOCIAL);
        $social_config = json_decode($social_config, true);

        // map
        $social_config_map = [];
        foreach ($social_config as $social) {
            $social_config_map[$social['name']] = $social['url'];
        }

        $data['social_config'] = $social_config_map;

        /* button add to cart */
        $data['action'] = $this->url->link('checkout/my_orders', '', true);

        /* related products (key: related_products) */
        $limit = $this->getLimitProductInBlock();
        $related_products = $this->getRelatedProducts($product_id, $limit);
        $data = array_merge($data, $related_products);

        /* hot products (key: hot_products) */
        $hot_products = $this->getHotProduct();
        $data = array_merge($data, $hot_products);

        /* new products (key: new_products) */
        $hot_products = $this->getHotProduct();
        $data = array_merge($data, $hot_products);

        /* get one banner */
        $one_banner = $this->getBanner();
        $data['banner'] = (is_array($one_banner) ? $one_banner : '');

        /* get all banners */
        $banners = $this->getBanners();
        $data['banners'] = (is_array($banners) ? $banners : '');

        $data['common_shop_href'] = $this->url->link('common/shop');

        $result_new_product = $this->getNewProducts($product_id);
        $data['is_new_product'] = $result_new_product['is_new_product'];


        /** total money in cart */
        $this->load->model('catalog/product');
        $key_for_current_order = "current_order_" . $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());
        $current_order = isset($this->session->data[$key_for_current_order]) ? $this->session->data[$key_for_current_order] : [];
        $total_into_money = 0;
        $total_product_cart = 0;
        foreach ($current_order as $c_order) {
            if (!isset($c_order['product_id']) || !isset($c_order['quantity'])) {
                continue;
            }

            // find product
            $product_in_order_id = $c_order['product_id'];
            $product_in_order = $this->model_catalog_product->getProduct($product_in_order_id);
            $data['is_product_existing'] = true;
            if (empty($product_in_order) || !is_array($product_in_order)) {
                $data['is_product_existing'] = false;
            }

            if (isset($c_order['attribute']) && $c_order['attribute'] && isset($c_order['product_version']['price'])) {
                if ((int)$c_order['product_version']['price'] == 0) $c_order['product_version']['price'] = $c_order['product_version']['compare_price'];
                $price = (float)$c_order['product_version']['price'];
            } else {
                if ((int)$product_in_order['price'] == 0) $product_in_order['price'] = $product_in_order['compare_price_master'];
                $price = (float)$product_in_order['price'];
            }

            $total = $price * $c_order['quantity'];
            $total_into_money += $total;
            $total_product_cart += $c_order['quantity'];
        }
        $data['order_total_money'] = $this->currency->formatCustom($total_into_money, $this->session->data['currency']);
        $data['total_product_cart'] = $total_product_cart;

        /* get hotline */
        $hotline = $this->model_catalog_product->getHotlineValue();
        if (!empty($hotline)) {
            $data['hotline'] = $hotline['value'];
        }

        /* get header config */
        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }
        $this->load->model('setting/extension');
        $this->load->model('extension/module/theme_builder_config');
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;
        $header_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HEADER);
        $header_config = json_decode($header_config, true);

        /* display logo */
//        if ($this->model_tool_image->imageIsExist($header_config['logo']['url'])) {
//            $data['logo'] = $header_config['logo']['url'];
//        } elseif (isset($header_config['logo']['url']) && is_file(str_replace($server, '', $header_config['logo']['url']))) {
//            $data['logo'] = $server . str_replace($server, '', $header_config['logo']['url']);
//        } elseif (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
//            $data['logo'] = $server . 'image/' . $this->config->get('config_logo');
//        } else {
//            $data['logo'] = '';
//        }

        //add schema
        $product_description = empty($description_tabs[0]) ? $data['product']['description'] : $description_tabs[0]['description'];
        $schema = array(
            "@context" => "https://schema.org/",
            "@type" => "Product",
            "name" => $data['product']['name'],
            "image" => array($data['product']['image']),
            "description" => strip_tags(html_entity_decode($product_description)),
            "sku" => $data['product']['sku'],
            "brand" => [
                "@type" => "Brand",
                "name" => !empty($data['product']['manufacturer']) ? $data['product']['manufacturer'] : "DHC"
            ],
            "offers" => []
        );

        if ($product['multi_versions'] == 0) {
            $offer = [
                "@type" => "Offer",
                "url" => $this->url->link('product/product', '&product_id=' . $product['product_id']),
                "priceCurrency" => "VND",
                "price" => intval($data['product']['schema_price']),
                "itemCondition" => "https://schema.org/NewCondition",
                "availability" => "https://schema.org/InStock",
                "seller" => [
                    "@id" => HTTPS_SERVER . "#Organization",
                ]
            ];
        } else {
            $offer = [
                "@type" => "Offer",
                "url" => $this->url->link('product/product', '&product_id=' . $product['product_id']),
                "priceCurrency" => "VND",
                "price" => intval($data['product']['schema_price_min']),
                //"lowPrice" => intval($data['product']['schema_price_min']),
                //"highPrice" => intval($data['product']['schema_price_max']),
                "itemCondition" => "https://schema.org/NewCondition",
                "availability" => "https://schema.org/InStock",
                "seller" => [
                    "@id" => HTTPS_SERVER . "#Organization",
                ]
            ];
        }

        $schema['offers'] = $offer;

        $schema = json_encode($schema, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        $data['logo'] = isset($header_config['logo']['url']) ? cloudinary_change_http_to_https($header_config['logo']['url']) : 'image/no_image.png';

        /* display main menu */
        $menu_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HEADER);
        $menu_config = json_decode($menu_config, true);

        $mainMenuId = isset($menu_config['menu']['display-list']['id']) ? $menu_config['menu']['display-list']['id'] : -1;
        //menu ngang
        $this->load->model('custom/common');
        $mainMenu = $this->model_custom_common->getListMenuItemByGroupMenuId($mainMenuId);
        // default if empty
        // $mainMenu = [];
        $mainMenu = !empty($mainMenu) ? $mainMenu : [
            [
                'text' => 'Trang chủ',
                'url' => 'index.php?route=common/home'
            ],
        ];
        $data['main_menu'] = $mainMenu;

        /** Login */
        $data['logged_in'] = $this->customer->isLogged();

        /** link order */
        $data['href_my_orders'] = $this->url->link('checkout/my_orders', '', true);

        $this->load->model('setting/setting');
        $is_buy_now_instead_of_installment = $this->model_setting_setting->getSettingValue(self::IS_BUY_NOW_INSTEAD_OF_INSTALLMENT_CONFIG_KEY);
        $data['is_buy_now_instead_of_installment'] = $is_buy_now_instead_of_installment == 1 ? 1 : 0;

        $flash_sale = $this->flashSale();
        $data['is_product_flash_sale'] = false;
        if (!empty($flash_sale['products_flash_sale'])) {
            foreach ($flash_sale['products_flash_sale'] as $product_flash_sale) {
                if ($product_flash_sale['product_id'] == $data['product']['product_id']) {
                    $data['is_product_flash_sale'] = true;
                }
            }
        }
        $data['future_time'] = $flash_sale['future_time'];
        $data['time_flashsale_happening'] = isset($flash_sale['time_flashsale_happening']) ? $flash_sale['time_flashsale_happening'] : null;

        /* google analytic 4 view_item event */
        $google_analytics_code = $this->config->get('online_store_config_google_code');
        if (empty($google_analytics_code) || '1' == $product['multi_versions']) {
            $data['ga4_view_item_detail'] = null;
        } else {
            $ga4_items = [
                'item_id' => $product['sku'],
                'item_name' => $product['name'],
                'currency' => "VND",
                'index' => 0,
                'item_brand' => $product['manufacturer']
            ];
            foreach ($product['categories'] as $category_key => $category) {
                $ga4_items['item_category' . (0 == $category_key ? '' : $category_key)] = $category['name'];
            }
            $product_price = $this->calculateProductPrice($product);
            $ga4_items['price'] = (float)$product_price['price'];

            $data['ga4_view_item_detail'] = [
                'event' => 'view_item',
                'items' => [$ga4_items]
            ];
            $data['ga4_view_item_detail'] = json_encode($data['ga4_view_item_detail'], JSON_HEX_APOS);
        }

        /* base layout */
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        $data['header'] .= '<script type="application/ld+json">' . $schema . '</script>' . "\n";
        $data['header'] .= '<script type="application/ld+json">' .
        $this->schemaBreadcrumbList($this->url->link('common/shop'), "Sản phẩm, Products", $productCategories, $data['product']['name']) . '</script>' . "\n";
        $data['header'] .= '<script type="application/ld+json">' . $this->schemaOrganization() . '</script>' . "\n";

        // placeholder image
        $data['placeholder_image_url'] = $this->session->data['placeholder_image_url'];

        if ($not_found) {
            $this->response->setOutput($this->load->view('error/not_found', $data));
        } else {
            $this->response->setOutput($this->load->view('product/product_detail', $data));
        }
    }

    /**
     * get one banner (for product detail)
     *
     * @return array
     */
    private function getBanner()
    {
        $data = [];

        // current get banners config from product list page. TODO: add config for product detail page?...
        $banners = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CATEGORY_BANNER);
        $banners = json_decode($banners, true);
        if ($banners['display'][0]['visible'] == 1) {
            $this->load->model('tool/image');
            $data = [
                'url_banner' => $banners['display'][0]['url'],
                'url_image' => empty($banners['display'][0]['image-url']) ? $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height')) : cloudinary_change_http_to_https($banners['display'][0]['image-url']),
                'visible_banner' => $banners['display'][0]['visible'],
            ];
        }

        return $data;
    }

    /**
     * get all banners (for product detail)
     *
     * @return array
     */
    private function getBanners()
    {
        // current get banners config from product list page. TODO: add config for product detail page?...
        $banners = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CATEGORY_BANNER);
        $banners = json_decode($banners, true);
        if (isset($banners['display']) && is_array($banners['display'])) {
            foreach ($banners['display'] as $key => $banner) {
                if (isset($banner['image-url'])) {
                    if (empty($banner['image-url'])) {
                        $banners['display'][$key]['visible'] = 0;
                    } else {
                        $banners['display'][$key]['image-url'] = $this->model_tool_image->cloudinary_auto_optimize($banners['display'][$key]['image-url']);
                        $banners['display'][$key]['image-url'] = cloudinary_change_http_to_https($banners['display'][$key]['image-url']);
                    }
                }
            }

            return $banners['display'];
        }

        return [];
    }

    private function getNewProducts($product_id)
    {
        $data = array();
        $this->load->model('catalog/product');
        $new_product_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_NEW_PRODUCT);
        $new_product_config = json_decode($new_product_config, true);

        $data['hot_product_title'] = '';
        if (isset($new_product_config['setting']['title'])) {
            $data['hot_product_title'] = $new_product_config['setting']['title'];
        }
        $limit = 10;
        if ($new_product_config['setting']['auto_retrieve_data'] == 0) {
            $filter['filter_collection_id'] = $new_product_config['setting']['collection_id'];
            $filter['start'] = 0;
            $filter['limit'] = $limit;
            $new_products = $this->model_catalog_product->getProductsByColection($filter);
        } else {
            $new_products = $this->model_catalog_product->getLatestProducts($limit);
        }

        $arr_new_product = array();
        foreach ($new_products as $product) {
            $arr_new_product[] = (int)$product['product_id'];
        }
        if (in_array($product_id, $arr_new_product)) {
            $data['is_new_product'] = true;
        } else {
            $data['is_new_product'] = false;
        }
        return $data;
    }

    private function getHotProduct()
    {
        $data = array();

        $hot_product_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HOT_PRODUCT);
        $hot_product_config = json_decode($hot_product_config, true);

        $data['hot_product_title'] = '';
        if (isset($hot_product_config['setting']['title'])) {
            $data['hot_product_title'] = $hot_product_config['setting']['title'];
        }

        $limit = 10;

        if (isset($hot_product_config['display']['grid']['quantity'])) {
            $limit = $hot_product_config['display']['grid']['quantity'];
        }

        $config_menu = array();

        if (isset($hot_product_config['display']['menu'])) {
            $config_menu = $hot_product_config['display']['menu'];
        }

        $hot_products = [];
        foreach ($config_menu as $menu) {
            if (isset($menu['checked']) && $menu['checked'] == '1') {
                $this->load->model('catalog/product');
                if ($menu['type'] == 'existing') {
                    $filter['filter_category_id'] = $menu['menu']['id'];
                    $filter['start'] = 0;
                    $filter['limit'] = $limit;
                    $hot_products = $this->model_catalog_product->getProducts($filter);
                } else {
                    $product_id = array_map(function ($product) {
                        return $product['id'];
                    }, $menu['products']);
                    $product_id = array_slice($product_id, 0, $limit);
                    $hot_products = $this->model_catalog_product->getProductByIds($product_id);
                }
            }
        }

        $this->load->model('tool/image');
        $hot_product_data = array();
        foreach ($hot_products as $product) {
            $image = $this->resizeImage($product['image']);

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $price = false;
            }

            if ((float)$product['special']) {
                $special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $special = false;
            }

            if ($this->config->get('config_tax')) {
                $tax = $this->currency->format((float)$product['special'] ? $product['special'] : $product['price'], $this->session->data['currency']);
            } else {
                $tax = false;
            }

            if ($this->config->get('config_review_status')) {
                $rating = (int)$product['rating'];
            } else {
                $rating = false;
            }

            $hot_product_data[] = array(
                'product_id' => $product['product_id'],
                'thumb' => $this->changeUrl($image),
                'name' => $product['name'],
                'description' => utf8_substr(trim(strip_tags(html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                'price' => $price,
                'special' => $special,
                'tax' => $tax,
                'minimum' => $product['minimum'] > 0 ? $product['minimum'] : 1,
                'rating' => $rating,
                'href' => $this->url->link('product/product', 'product_id=' . $product['product_id'])
            );
        }

        $data['hot_products'] = $hot_product_data;

        return $data;
    }

    private function resizeImage($image, $width = null, $height = null)
    {
        if ($image) {
            if ($width == null) {
                $width = $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width');
            }
            if ($height == null) {
                $height = $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height');
            }
            $image = $this->model_tool_image->resize($image, $width, $height);
        } else {
            $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height'));
        }

        return $image;
    }

    public function updateCartWhenChangeVersion()
    {
        $json = [
            'success' => false,
            'message' => "Không tồn tại version sản phẩm"
        ];

        $product_info = $this->request->post;
        if (isset($product_info['attribute']) && is_array($product_info['attribute']) && isset($product_info['product_id'])) {
            $this->load->model('catalog/product');
            $version = [];
            foreach ($product_info['attribute'] as $key => $attribute) {
                $version[] = trim($attribute);
            }

            $product_version = $this->model_catalog_product->getProductByVersionValue($product_info['product_id'], $version);
            if (isset($product_version['product_id'])) {
                $product_version_id = getValueByKey($product_version, 'product_version_id', null);
                $json['sale_on_out_of_stock'] = strval((int)$this->model_catalog_product->isStock($product_info['product_id'], $product_version_id));
                $json['product_version'] = $product_version;

                // Add-on Deal
                $this->load->model('discount/add_on_deal');
                $json['has_deal'] = false;
                $json['deal_data'] = $this->model_discount_add_on_deal->getAddOnDealByProductId($product_info['product_id'], $product_version_id);

                if (!empty($json['deal_data'])) {
                    $json['has_deal'] = true;
                    $json['add_on_products'] = $this->addOnProductListAndChecked($json['deal_data']['add_on_deal_id'], $product_info['product_id'], $product_version_id);
                }

                $json['success'] = true;
                $json['message'] = "Tồn tại version sản phẩm";
                $json['product_version']['percent_sale'] = getPercent($product_version['price'], $product_version['compare_price']);
                //TODO tax 0
                $json['product_version']['price_format'] = $this->currency->formatCustom($this->tax->calculate($product_version['price'], 0, $this->config->get('config_tax')), $this->session->data['currency']);
                $json['product_version']['compare_price_format'] = $this->currency->formatCustom($this->tax->calculate($product_version['compare_price'], 0, $this->config->get('config_tax')), $this->session->data['currency']);
                $json['product_version']['is_contact_for_price'] = (int)$product_version['compare_price'] == 0 ? true : false;

                /* google analytic 4 view_item event */
                $google_analytics_code = $this->config->get('online_store_config_google_code');
                if (empty($google_analytics_code)) {
                    $json['ga4_view_item_detail'] = null;
                } else {
                    $product = $this->model_catalog_product->getProduct($product_info['product_id']);
                    $ga4_items = [
                        'item_id' => $product['sku'],
                        'item_name' => $product['name'],
                        'currency' => "VND",
                        'index' => 0,
                        'item_brand' => $product['manufacturer'],
                        'item_variant' => $product_version['version']
                    ];
                    foreach ($product['categories'] as $category_key => $category) {
                        $ga4_items['item_category' . (0 == $category_key ? '' : $category_key)] = $category['name'];
                    }
                    $product_price = $this->calculateProductPrice($product);
                    $ga4_items['price'] = (float)$product_price['price'];

                    $json['ga4_view_item_detail'] = [
                        'event' => 'view_item',
                        'items' => [$ga4_items]
                    ];
                }
                $json['product_version']['product_attribute'] = $product_info['attribute'];
            } else {
                $json['success'] = false;
                $json['message'] = "Không tồn tại version sản phẩm";
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function addOnProductListAndChecked($deal_id, $main_product_id, $main_product_version_id)
    {
        // cart
        $key_for_current_order = "current_order_". $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());
        $current_order = isset($this->session->data[$key_for_current_order]) ? $this->session->data[$key_for_current_order] : [];

        // getFullAddOnProduct
        $addOnProducts = $this->getFullAddOnProduct($deal_id);

        $products = [];
        if (!empty($addOnProducts) && !empty($current_order)) {
            foreach ($addOnProducts as $add_on_product) {
                foreach ($current_order as $product_order) {
                    if (empty($product_order['add_on_products'])) {
                        continue;
                    }

                    if ($product_order['product_id'] != $main_product_id) {
                        continue;
                    }

                    if ($main_product_version_id != 0) {
                        if (empty($product_order['product_version'])) {
                            continue;
                        }

                        if ($product_order['product_version']['product_id'] != $main_product_id ||
                            $product_order['product_version']['product_version_id'] != $main_product_version_id) {
                            continue;
                        }
                    }

                    if (!empty($product_order['chooseAddOnProduct'])) {
                        if (in_array($add_on_product['id'], $product_order['chooseAddOnProduct'])) {
                            $add_on_product['checked'] = true;
                        }
                    }
                }

                $products[] = $add_on_product;
            }
        } else {
            return $addOnProducts;
        }

        return $products;
    }

    public function unique_multidim_array($array, $key) {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }

    public function getProductsByID($ids) {
        $this->load->model('catalog/product');

        $products_viewed_data = [];
        if (!count($ids)) {
            return $products_viewed_data;
        }

        foreach ($ids as $product_id) {
            if (!$product_id) {
                continue;
            }

            $product = $this->model_catalog_product->getProduct($product_id);
            if(!isset($product['price_master'])
                && !isset($product['compare_price_master'])
                && !isset($product['tax_class_id'])
                && !isset($product['min_price_version'])
                && !isset($product['product_version_status_max'])
                && !isset($product['product_master_quantity'])
                && !isset($product['sale_on_out_stock'])
            ) {
                $products_viewed_data = [];
            }
            else
            {
                /* convert data product */
                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $price_master = $this->currency->formatCustom($this->tax->calculate($product['price_master'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price_master = false;
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $compare_price_master = $this->currency->formatCustom($this->tax->calculate($product['compare_price_master'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $compare_price_master = false;
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $min_price_version = $this->currency->formatCustom($this->tax->calculate($product['min_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $min_price_version = false;
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $max_price_version = $this->currency->formatCustom($this->tax->calculate($product['max_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $max_price_version = false;
                }

                if (($this->customer->isLogged() || !$this->config->get('config_customer_price')) && isset($product['compare_price_2'])) {
                    $compare_price_2 = $this->currency->formatCustom($this->tax->calculate($product['compare_price_2'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $compare_price_2 = false;
                }

                if (($this->customer->isLogged() || !$this->config->get('config_customer_price')) && isset($product['price_2'])) {
                    $price_2 = $this->currency->formatCustom($this->tax->calculate($product['price_2'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price_2 = false;
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $min_compare_price_version = $this->currency->formatCustom($this->tax->calculate($product['min_compare_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $min_compare_price_version = false;
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $max_compare_price_version = $this->currency->formatCustom($this->tax->calculate($product['max_compare_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $max_compare_price_version = false;
                }

                if (empty($product['product_version_id'])) {
                    $product['price_master_real'] = ((int)$product['price_master'] != 0) ? 1 : 0;
                    $product['max_price_version_real'] = 1;
                }

                if (!empty($product['product_version_id'])) {
                    $product['max_price_version_real'] = ((int)$product['price_version_check_null'] != 0) ? 1 : 0;
                    $product['price_master_real'] = 1;
                }

                $product['percent_sale'] = empty($product['max_price_version']) ? getPercent($product['price_master'], $product['compare_price_master']) : getPercent($product['min_price_version'], $product['min_compare_price_version'], $product['max_percent_pv']);
                $product['percent_sale_value'] = empty($product['max_price_version'])
                    ? getPercent($product['price_master'], $product['compare_price_master'], 0, "")
                    : getPercent($product['min_price_version'], $product['min_compare_price_version'], $product['max_percent_pv'], "");

                if ((float)$product['price_discount'] == 0 && !empty($product['discount_id'])) {
                    $product['percent_sale'] = -100;
                    $product['percent_sale_value'] = '-100%';
                }

                // Add-on Deal
                $this->load->model('discount/add_on_deal');
                $product['has_deal'] = false;
                $product['deal_name'] = $this->model_discount_add_on_deal->getAddOnDealNameByProductId($product['product_id']);

                if ($product['deal_name']) {
                    $product['has_deal'] = true;
                }

                $product['price_master'] = $price_master;
                $product['compare_price_master'] = $compare_price_master;
                $product['min_price_version'] = $min_price_version;
                $product['max_price_version'] = empty($product['max_price_version']) ? $product['max_price_version'] : $max_price_version;
                $product['min_compare_price_version'] = $min_compare_price_version;
                $product['compare_price_2_text'] = $compare_price_2;
                $product['price_2_text'] = $price_2;
                $product['compare_price_2'] = $product['compare_price_2'];
                $product['price_2'] = $product['price_2'];
                $product['max_compare_price_version'] = empty($product['max_compare_price_version']) ? $product['max_compare_price_version'] : $max_compare_price_version;
                $product_version_id = (int)$product['product_version_id'];
                $product['product_is_stock'] = $this->model_catalog_product->isStock($product['product_id'], $product_version_id);
                $product['product_is_stock'] = false;
                if ($product['product_version_status_max'] !== "0" && $product['sale_on_out_stock'] == 1) {
                    $product['product_is_stock'] = true;
                } elseif (empty($product['product_version_id']) && (int)$product['product_master_quantity'] > 0) {
                    $product['product_is_stock'] = true;
                } elseif ($product['product_version_status_max'] !== "0" && !empty($product['product_version_id']) && (int)$product['product_version_quantity'] > 0) {
                    $product['product_is_stock'] = true;
                }
                $product['product_mas_ver_quantity'] = empty($product['product_version_id']) ? (int)$product['product_master_quantity'] : (int)$product['product_version_quantity'];
                $product['sale_on_out_stock_check'] = true;
                if (empty($product['product_version_id']) && $product['product_master_quantity'] < 1 && $product['sale_on_out_stock'] == 0) {
                    $product['sale_on_out_stock_check'] = false;
                }
                $product['multi_versions'] = $product['multi_versions'];
                $product['is_contact_for_price'] = $this->isContactForPrice($product['multi_versions'], $product['compare_price_master'], $product['min_compare_price_version']);
            }
            if(isset($product['product_id'])) {
                $image = $this->resizeImage($product['image']);

                $pv_id = '';
                if (!empty($product['product_version_id'])) {
                    $pv_id = $product['product_version_id'];
                }

                if (!empty($product['product_version_id_2'])) {
                    $pv_id = $product['product_version_id_2'];
                }

                $param_text = !empty($pv_id) ? "&variant={$pv_id}" : '';

                $product['thumb'] = $this->changeUrl($image, true, 'product');
                $product['href'] = $this->url->link('product/product', 'product_id=' . $product['product_id'] . $param_text);
                $products_viewed_data[] = $product;
            }
        }

        return $products_viewed_data;
    }

    public function getProductBySku()
    {
        $this->load->model('tool/image');
        $product_sku = $this->request->get['product_sku'];
        $this->load->model('catalog/product');

        $product = $this->model_catalog_product->getProductBySku($product_sku);
        if(!isset($product['price_master'])
            && !isset($product['compare_price_master'])
            && !isset($product['tax_class_id'])
            && !isset($product['min_price_version'])
            && !isset($product['product_version_status_max'])
            && !isset($product['product_master_quantity'])
            && !isset($product['sale_on_out_stock'])
        ) {
            $product = [];
        }
        else
        {
            /* convert data product */
            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $price_master = $this->currency->formatCustom($this->tax->calculate($product['price_master'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $price_master = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price') && isset($product['price'])) {
                $price = $this->currency->formatCustom($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $price = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price') && isset($product['compare_price'])) {
                $compare_price = $this->currency->formatCustom($this->tax->calculate($product['compare_price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $compare_price = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $compare_price_master = $this->currency->formatCustom($this->tax->calculate($product['compare_price_master'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $compare_price_master = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $min_price_version = $this->currency->formatCustom($this->tax->calculate($product['min_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $min_price_version = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $max_price_version = $this->currency->formatCustom($this->tax->calculate($product['max_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $max_price_version = false;
            }

            if (($this->customer->isLogged() || !$this->config->get('config_customer_price')) && isset($product['compare_price_2'])) {
                $compare_price_2 = $this->currency->formatCustom($this->tax->calculate($product['compare_price_2'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $compare_price_2 = false;
            }

            if (($this->customer->isLogged() || !$this->config->get('config_customer_price')) && isset($product['price_2'])) {
                $price_2 = $this->currency->formatCustom($this->tax->calculate($product['price_2'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $price_2 = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $min_compare_price_version = $this->currency->formatCustom($this->tax->calculate($product['min_compare_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $min_compare_price_version = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $max_compare_price_version = $this->currency->formatCustom($this->tax->calculate($product['max_compare_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $max_compare_price_version = false;
            }

            if (empty($product['product_version_id'])) {
                $product['price_master_real'] = ((int)$product['price_master'] != 0) ? 1 : 0;
                $product['max_price_version_real'] = 1;
            }

            if (!empty($product['product_version_id'])) {
                $product['max_price_version_real'] = ((int)$product['price_version_check_null'] != 0) ? 1 : 0;
                $product['price_master_real'] = 1;
            }

            $product['percent_sale'] = empty($product['max_price_version']) ? getPercent($product['price_master'], $product['compare_price_master']) : getPercent($product['min_price_version'], $product['min_compare_price_version'], $product['max_percent_pv']);
            $product['percent_sale_version'] = isset($product['price']) && isset($product['compare_price']) ? getPercent($product['price'], $product['compare_price']) : '';
            $product['percent_sale_value'] = empty($product['max_price_version'])
                ? getPercent($product['price_master'], $product['compare_price_master'], 0, "")
                : getPercent($product['min_price_version'], $product['min_compare_price_version'], $product['max_percent_pv'], "");
            $product['price_master'] = $price_master;
            $product['compare_price_master'] = $compare_price_master;
            $product['min_price_version'] = $min_price_version;
            $product['max_price_version'] = empty($product['max_price_version']) ? $product['max_price_version'] : $max_price_version;
            $product['min_compare_price_version'] = $min_compare_price_version;
            $product['compare_price_2_text'] = $compare_price_2;
            $product['price_2_text'] = $price_2;
            $product['max_compare_price_version'] = empty($product['max_compare_price_version']) ? $product['max_compare_price_version'] : $max_compare_price_version;
            $product_version_id = (int)$product['product_version_id'];
            $product['product_is_stock'] = false;
            if ($product['product_version_status_max'] !== "0" && $product['sale_on_out_stock'] == 1) {
                $product['product_is_stock'] = true;
            } elseif (empty($product['product_version_id']) && (int)$product['product_master_quantity'] > 0) {
                $product['product_is_stock'] = true;
            } elseif ($product['product_version_status_max'] !== "0" && !empty($product['product_version_id']) && (int)$product['product_version_quantity'] > 0) {
                $product['product_is_stock'] = true;
            }
            $product['product_mas_ver_quantity'] = empty($product['product_version_id']) ? (int)$product['product_master_quantity'] : (int)$product['product_version_quantity'];
            $product['sale_on_out_stock_check'] = true;
            $product['price'] = $price;
            $product['compare_price'] = $compare_price;
            if (empty($product['product_version_id']) && $product['product_master_quantity'] < 1 && $product['sale_on_out_stock'] == 0) {
                $product['sale_on_out_stock_check'] = false;
            }
            $product['is_contact_for_price'] = $this->isContactForPrice($product['multi_versions'], $product['compare_price_master'], $product['min_compare_price_version']);
        }
        if(isset($product['product_id'])) {
            $image = $this->resizeImage($product['image']);
            $product['thumb'] = $this->changeUrl($image, true, 'product');
            $product['href'] = $this->url->link('product/product', 'product_id=' . $product['product_id']);
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($product));
    }

    private function getFullAddOnProduct($deal_id)
    {
        $this->load->model('tool/image');

        $products = [];
        $results = $this->model_discount_add_on_deal->getFullAddOnProductById($deal_id);

        foreach ($results as $item) {
            $product_id = !empty($item['product_id']) ? $item['product_id'] : 0;
            $product_version_id = !empty($item['product_version_id']) ? $item['product_version_id'] : 0;

            if (empty($product_id)) {
                continue;
            }

            $p_v_id = $product_id;
            if ($product_version_id) {
                $p_v_id = $product_id . '-' . $product_version_id;
            }

            $version_name = implode(' • ', explode(',', $item['version']));
            $version_name = $version_name != '' ? $version_name : '';

            if ($item['image'] != '') {
                $image = $this->model_tool_image->resize($item['image'], 100, 100);
            } else {
                $image = $this->model_tool_image->resize('no_image.png', 100, 100);
            }

            $sale_price = (float)$item['discount_price'];
            $original_price = empty($item['multi_versions']) ? (float)$item['p_compare_price'] : (float)$item['pv_compare_price'];
            $sku = empty($item['multi_versions']) ? $item['p_sku'] : $item['pv_sku'];

            $percent = round(($original_price - $sale_price) / $original_price * 100);

            if ($sale_price == 0) {
                $percent = 100;
            }

            $is_stock = true;
            if ($item['current_quantity'] < 1 || $item['pts_quantity'] < 1) {
                $is_stock = false;
            }

            $products[] = [
                "id" => $p_v_id,
                "product_id" => $item['product_id'],
                "product_version_id" => $item['product_version_id'],
                "version" => $version_name,
                "product_name" => $item['name'],
                "image" => $image,
                "sale_price" => $sale_price,
                "original_price" => $original_price,
                "sale_price_format" => $this->currency->formatCustom($this->tax->calculate($sale_price, $item['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
                "original_price_format" => $this->currency->formatCustom($this->tax->calculate($original_price, $item['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
                "quantity" => (float)$item['pts_quantity'],
                "sku" => $sku,
                "percent_reduce" => $percent,
                "checked" => false,
                "is_stock" => $is_stock
            ];
        }

        return $products;
    }
}