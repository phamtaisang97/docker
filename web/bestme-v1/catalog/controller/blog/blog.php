<?php

class ControllerBlogBlog extends Controller
{
    use Theme_Config_Util;
    use Device_Util;
    use Setting_Util_Trait;
    use Product_Util_Trait;

    public function index()
    {
        $this->load->language('blog/blog');
        $this->document->setTitle($this->config->get('config_meta_title'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->setKeywords($this->config->get('config_meta_keyword'));

        $this->load->model('blog/blog');
        $this->load->model('blog/category');
        $this->load->model('tool/image');
        $this->load->model('extension/module/theme_builder_config');

        $data['theme_directory'] = '/catalog/view/theme/' . $this->getCurrentThemeDir();

        //breadcrumbs
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => 'Trang chủ',
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => 'Tin tức',
            'href' => $this->url->link('blog/blog')
        );

        //data filter
        $url = '';
        $data_filter = ['not_load_tag' => true];

        if (isset($this->request->get['blog_category_id'])) {
            $url .= "&blog_category_id=" . $this->request->get['blog_category_id'];
            $blog_category_id = $this->request->get['blog_category_id'];
            $data['titleCategory'] = $this->model_blog_blog->getTitleCategory($this->request->get['blog_category_id']);
        }

        if (isset($this->request->get['tag_id'])) {
            $url .= "&tag_id=" . $this->request->get['tag_id'];
            $data_filter['tag_id'] = $this->request->get['tag_id'];
            $data_filter['not_load_tag'] = false;
        }

        if (isset($this->request->get['type'])) {
            $url .= "&type=" . $this->request->get['type'];
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $limit = $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit') ? $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit') : $this->config->get('config_limit_admin');
        $data_filter['start'] = ($page - 1) * (int)$limit;
        $data_filter['limit'] = $limit;
        $data['limit'] = $limit;

        // v3.6.1.2-adg-themes
        if (isset($this->request->get['type'])) {
            $data['page_projects'] = true;
            $data_filter['blog_category_title'] = 'projects';
        }
        //end v3.6.1.2-adg-themes

        $blogFull = !empty($this->request->get['blog_category_id']) ?
            array_reverse($this->model_blog_blog->getAllParentsBlogCategoryById($this->request->get['blog_category_id'])) : [];

        if (!empty($blogFull)) {
            $length = count($blogFull);
            foreach ($blogFull as $key => $item) {
                if ($key == $length - 1) {
                    $data['breadcrumbs'][] = array(
                        'text' => $item['name'],
                        'active' => true
                    );

                    break;
                }

                $data['breadcrumbs'][] = array(
                    'text' => $item['name'],
                    'href' => $item['href']
                );
            }
        }

        //data blog
        // todo: load more tags
        $data['tags'] = []; // $this->model_blog_blog->getAllBlogTag();
        $data['blog_category'] = $this->model_blog_blog->getALlBlogCategory();
        $data['blog_full_categories'] = $this->model_blog_blog->getFullBlogCategories();
        if (isset($blog_category_id)) {
            $category_ids = $this->model_blog_category->getFullChildCategoryIds($blog_category_id);
            array_push($category_ids, $blog_category_id);
            $data_filter['categories_ids'] = $category_ids;
        }
        $data['blog_list'] = $this->convertBlogList($this->model_blog_blog->getBlogList($data_filter));
        $data['blog_list_new'] = $this->convertBlogList($this->model_blog_blog->getBlogList(array('date_time' => 30, 'start' => 0, 'limit' => $limit, 'not_load_tag' => true)));
        $data['all'] = $this->url->link('blog/blog');

        $one_banner = $this->getBanner();
        $data['banners'] = (is_array($one_banner) ? $one_banner : '');
        
        $data['social'] = $this->getSocialShareForBlogHomePage();

        $pagination = new CustomPaginate();
        $pagination->total = $this->model_blog_blog->getBlogListCount($data_filter);
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->url = $this->url->link('blog/blog', $url . '&page={page}', true);
        $data['customer_total'] = $this->model_blog_blog->getBlogListCount($data_filter);
        $data['pagination'] = $pagination->render();

        foreach ($data['blog_category'] as &$category) {
            $url_blog_category = '';
            if (isset($this->request->get['tag_id'])) {
                $url_blog_category .= "&tag_id=" . $this->request->get['tag_id'];
            }
            $category['link'] = $this->url->link("blog/blog", 'blog_category_id=' . $category['blog_category_id'] . $url_blog_category);
            if (isset($data_filter['blog_category_id']) && $data_filter['blog_category_id'] == $category['blog_category_id']) {
                $category['link'] = $this->url->link("blog/blog", $url_blog_category);
            }
            $category['count_blog'] = $this->model_blog_blog->countBlogOfCategory($category['blog_category_id']);
        }
        unset($category);

        foreach ($data['tags'] as &$tag) {
            $url_blog_tag = '';
            if (isset($this->request->get['blog_category_id'])) {
                $url_blog_tag .= "&blog_category_id=" . $this->request->get['blog_category_id'];
            }
            $tag['link'] = $this->url->link("blog/blog", 'tag_id=' . $tag['tag_id'] . $url_blog_tag);
            if (isset($data_filter['tag_id']) && $data_filter['tag_id'] == $tag['tag_id']) {
                $tag['link'] = $this->url->link("blog/blog", $url_blog_tag);
            }
        }
        unset($tag);
        $data['is_on_mobile'] = $this->isMobile() ? 1 : 0;
        $data['filter'] = $data_filter;

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        $data['header'] .= '<script type="application/ld+json">' . $this->schemaOrganization() . '</script>' . "\n";
        $data['header'] .= '<script type="application/ld+json">' .
            $this->schemaBreadcrumbList($this->url->link("blog/blog"), "Bài viết, Blogs", $blogFull) . '</script>' . "\n";

        // placeholder image
        $data['placeholder_image_url'] = $this->session->data['placeholder_image_url'];

        $this->response->setOutput($this->load->view('blog/blog', $data));
    }

    public function detail()
    {
        $this->load->language('blog/blog_detail');
        $this->document->setTitle($this->config->get('config_meta_title'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->setKeywords($this->config->get('config_meta_keyword'));

        //bre
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => 'Trang chủ',
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => 'Tin tức',
            'href' => $this->url->link('blog/blog')
        );

        // v3.6.1.2-adg-themes
        if (isset($this->request->get['type'])) {
            $data['page_projects'] = true;
        }
        if (isset($this->request->get['about-us'])) {
            $data['page_about_us'] = true;
        }
        //end v3.6.1.2-adg-themes

        //data filter
        $data_filter = ['not_load_tag' => true, 'limit' => 1];
        // TODO: remove... set 'blog_category_id' as bellow...
        /*if (isset($this->request->get['blog_id'])) {
            $data_filter['blog_id'] = $this->request->get['blog_id'];
        }*/

        //data blog
        $this->load->model('blog/blog');
        $this->load->model('tool/image');
        $this->load->model('extension/module/theme_builder_config');
        // todo: load more tags
        $data['tags'] = []; // $this->convertTags($this->model_blog_blog->getTagsByBlog($this->request->get['blog_id']));
        $data['blog_category'] = $this->convertCategory($this->model_blog_blog->getALlBlogCategory());

        foreach ($data['blog_category'] as &$category) {
            $category['count_blog'] = $this->model_blog_blog->countBlogOfCategory($category['blog_category_id']);
        }

        if (isset($this->request->get['blog_id'])) {
            $data_filter['blog_category_id'] = $this->model_blog_blog->getCategoryIdByBlog($this->request->get['blog_id']);
        }

        $data['blog_list'] = $this->convertBlogList($this->model_blog_blog->getBlogList($data_filter));
        $limit = 0; // unlimited relate blogs. No need use: $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit') ? $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit') : $this->config->get('config_limit_admin');
        $data['blog_list_new'] = $this->convertBlogList($this->model_blog_blog->getBlogList(array('date_time' => 30, 'start' => 0, 'limit' => $limit, 'not_load_tag' => true)));

        $data['theme_directory'] = '/catalog/view/theme/' . $this->config->get('config_theme');
        $data['current_theme'] = $this->config->get('config_theme');

//        if (isset($data['blog_list'][0]['blog_category_id'])) {
//            $data['category_blog'] = $this->model_blog_blog->getTitleCategory($data['blog_list'][0]['blog_category_id']);
//        }

        $data['blog'] = $this->model_blog_blog->getBlog($this->request->get['blog_id']);

        $blogFull = !empty($data['blog_list'][0]['blog_category_id']) ?
            array_reverse($this->model_blog_blog->getAllParentsBlogCategoryById($data['blog_list'][0]['blog_category_id'])) : [];

        if (!empty($blogFull)) {
            foreach ($blogFull as $key => $item) {
                $data['breadcrumbs'][] = array(
                    'text' => $item['name'],
                    'href' => $item['href']
                );
            }
        }


        if($data['blog']['type'] == 'video' and !$data['blog']['video_url']) {
            $data['blog']['type'] = 'image';
            $data['blog']['image'] = '';
        }

        $data['blog_relate'] = [];
        if (isset($data['blog']['blog_categories']) && !empty($data['blog']['blog_categories'])) {
            $related_blogs = $this->model_blog_blog->getBlogList([
                'start' => 0,
                'limit' => $limit,
                'out_blog_id' => $data['blog'],
                'not_load_tag' => true
            ]);
            $data['blog_relate'] = $this->convertBlogList($related_blogs);
        }

        $data['blog']['content'] = $this->replaceContentBlog($this->request->get['blog_id'], $data['blog']['content']);

//        $data['blog_relate_project'] = [];
//        $related_blogs_projects = $this->model_blog_blog->getBlogList([
//            'start' => 0,
//            'limit' => $limit,
//            'blog_category_title' => "projects",
//            'not_load_tag' => true
//        ]);
//        $data['blog_relate_project'] = $this->convertBlogList($related_blogs_projects);

        $data['breadcrumbs'][] = array(
            'text' => isset($data['blog']['title']) ? $data['blog']['title'] : '',
        );
        $data['social'] = $this->getSocialShareForBlogPost($this->request->get['blog_id']);

        $one_banner = $this->getBanner();
        $data['banners'] = (is_array($one_banner) ? $one_banner : '');
        $data['blog_full_categories'] = $this->model_blog_blog->getFullBlogCategories();
        $data['is_on_mobile'] = $this->isMobile() ? 1 : 0;
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');

        $keywords = $data['blog']['seo_keywords'];
        if (empty($keywords)) {
            $title = $data['blog']['meta_title'];
            $main_domain = $this->getMainDomain();
            $keywords = "{$title},{$main_domain}";
        }
        //var_dump($data['blog']['blog_categories']);;die;

        $schema = array(
            "@context" => "http://schema.org",
            "@type" => "Article",
            "name"=> $data['blog']['title'],
            "author"=> $data['blog']['lastname'].$data['blog']['firstname'],
            "articleSection"=> isset($data['category_blog']['title']) ? $data['category_blog']['title'] : "",
            "description"=> strip_tags(html_entity_decode($data['blog']['short_content'])),
            "abstract"=> strip_tags(html_entity_decode($data['blog']['short_content'])),
            "articleBody"=> strip_tags(html_entity_decode($data['blog']['content'])),
            "dateCreated"=> $data['blog']['date_added'],
            "dateModified"=> $data['blog']['date_modified'],
            "datePublished"=> $data['blog']['date_added'],
            "thumbnailUrl" => $data['blog']['image'],
            "keywords" => $keywords,
            //"genre" => $data['category_blog']['title'],
            "url" => $this->url->link('blog/blog/detail', '&blog_id=' . $data['blog']['blog_id']),
        );
        $schema =  json_encode($schema, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        $data['header'] = $this->load->controller('common/header');
        $data['header'] .= '<script type="application/ld+json">' . $schema . '</script>' . "\n";
        $data['header'] .= '<script type="application/ld+json">' . $this->schemaOrganization() . '</script>' . "\n";
        $data['header'] .= '<script type="application/ld+json">' .
            $this->schemaBreadcrumbList($this->url->link("blog/blog"), "Bài viết, Blogs", $blogFull, $data['blog']['title']) . '</script>' . "\n";

        $data['blog_url'] = $this->url->link('blog/blog');

        // placeholder image
        $data['placeholder_image_url'] = $this->session->data['placeholder_image_url'];
        
        $this->response->setOutput($this->load->view('blog/blog_detail', $data));
    }

    private function convertTags($tags)
    {
        foreach ($tags as &$tag) {
            $tag['link'] = $this->url->link("blog/blog", 'tag_id=' . $tag['tag_id']);
        }
        unset($tag);
        return $tags;
    }

    private function convertCategory($categories)
    {
        foreach ($categories as &$category) {
            $category['link'] = $this->url->link("blog/blog", 'blog_category_id=' . $category['blog_category_id']);
        }
        unset($category);
        return $categories;
    }

    private function convertBlogList($blog_list)
    {
        foreach ($blog_list as $key => &$blog) {
            $blog['link'] = $this->url->link("blog/blog/detail", 'blog_id=' . $blog['blog_id']);
            $blog['category'] = empty($blog['blog_category_id']) ? '' : $this->model_blog_blog->getTitleCategory($blog['blog_category_id']);
            if (!$blog['image']) {
                $blog_image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
                $blog['image'] = $this->changeUrl($blog_image);
            }
            else {
                if ($key === 0) {
                    $blog['image'] = $this->changeUrl($blog['image'], true, 'blog_list_first');
                } else {
                    $blog['image'] = $this->changeUrl($blog['image'], true, 'blog_list');
                }
            }
            $blog['blog_categories'] = $this->convertCategory($this->model_blog_blog->getCategoriesByBlog($blog['blog_id']));
            $blog['blog_tags'] = $this->convertTags($this->model_blog_blog->getTagsByBlog($blog['blog_id']));
            // TODO: tienhm...
            //$blog['social'] = $this->getSocial($blog['blog_id']);
        }
        unset($blog);

        return $blog_list;
    }

    private function getSocialShareForBlogPost($blog_id)
    {
        $blog_post_url = $this->url->link('blog/blog/detail', 'blog_id=' . $blog_id);

        return $this->getSocialShare($blog_post_url);
    }

    private function getSocialShareForBlogHomePage()
    {
        $blog_homepage_url = $this->url->link('blog/blog');

        return $this->getSocialShare($blog_homepage_url);
    }

    private function getSocialShare($url)
    {
        return array(
            'facebook' => [
                'href' => 'https://www.facebook.com/sharer/sharer.php?u=' . ($url),
                'content' => 'fa fa-facebook-official'
            ],
            'google' => [
                'href' => 'https://plus.google.com/share?url=' . ($url),
                'content' => 'fa fa-google-plus'
            ],
            'twitter' => [
                'href' => 'https://twitter.com/share?url=' . ($url),
                'content' => 'fa fa-twitter'
            ]
        );
    }

    /**
     * get one banner (for blog detail, blog list...)
     *
     * @return array
     */
    private function getBanner()
    {
        $data = [];

        // current get banners config from product list page. TODO: add config for blog page?...
        $banners = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CATEGORY_BANNER);
        $banners = json_decode($banners, true);
        if ($banners['display'][0]['visible'] == 1) {
            $this->load->model('tool/image');
            $data = [
                'url_banner' => $banners['display'][0]['url'],
                'url_image' => empty($banners['display'][0]['image-url']) ? $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height')) : cloudinary_change_http_to_https($banners['display'][0]['image-url']),
                'visible_banner' => $banners['display'][0]['visible'],
            ];
        }

        return $data;
    }



    public function replaceContentBlog($blog_id, $content)
    {
        $link = "href=&quot;".$this->url->link("blog/blog/detail", 'blog_id=' . $blog_id)."/#";
        $c = str_replace('href=&quot;#',$link, $content);
        return $c;
    }
}
