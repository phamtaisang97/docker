<?php

class ControllerAccountReset extends Controller
{
    use Theme_Config_Util;

    private $error = array();

    public function index()
    {
        $this->load->language('account/reset');

        if ($this->customer->isLogged()) {
            $this->response->redirect($this->url->link('account/account', '', true));
        }

        /*NOT SUPPORTED as in admin dashboard: if (!$this->config->get('config_password')) {
            $data['text_notify'] = $this->language->get('error_disabled_forgotten');
        }*/

        if (isset($this->request->get['code'])) {
            $code = $this->request->get['code'];
        } else {
            $code = '';
        }

        $this->load->model('account/customer');

        $data['theme_directory'] = '/catalog/view/theme/' . $this->getCurrentThemeDir();

        $customer_info = $this->model_account_customer->getCustomerByCode($code);

        // $this->document->setTitle($this->language->get('heading_title'));

        if ($customer_info) {
            if ($this->validate()) {
                /* set password_tmp value as password */
                $this->model_account_customer->editPasswordActivationSuccess($customer_info['customer_id']);

                $this->session->data['success'] = $this->language->get('text_success');

                $this->response->redirect($this->url->link('account/login', '', true));
            } else {
                $data['text_notify'] = $this->language->get('error_invalid_activation_link');
            }
        } else {
            $data['text_notify'] = $this->language->get('error_invalid_activation_link');
        }

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('account/reset', $data));
    }

    protected function validate()
    {
        return !$this->error;
    }
}