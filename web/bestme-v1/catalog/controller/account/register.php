<?php

class ControllerAccountRegister extends Controller
{
    use Theme_Config_Util;

    private $error = array();

    public function index()
    {
        if ($this->customer->isLogged()) {
            $this->response->redirect($this->url->link('checkout/profile', '', true));
        }

        $this->load->language('account/register');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

        $this->load->model('account/customer');

        $data['theme_directory'] = '/catalog/view/theme/' . $this->getCurrentThemeDir();

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $customer_id = $this->model_account_customer->addCustomer($this->request->post);

            // Clear any previous login attempts for unregistered accounts.
            $this->model_account_customer->deleteLoginAttempts(trim($this->request->post['email']));

            $this->customer->login(trim($this->request->post['email']), $this->request->post['password']);

            unset($this->session->data['guest']);

            //pass session after login
            $key_for_current_order = "current_order_". $this->session->getId() . "_" . 0;
            if (isset($this->session->data[$key_for_current_order])) {
                $data_current_order_none_user = $this->session->data[$key_for_current_order];
                unset($this->session->data[$key_for_current_order]);
                $key_for_current_order = "current_order_". $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());
                if (!isset($this->session->data[$key_for_current_order])) {
                    $this->session->data[$key_for_current_order] = [];
                }
                $this->session->data[$key_for_current_order] = array_merge($this->session->data[$key_for_current_order], $data_current_order_none_user);
            }

            $this->response->redirect($this->url->link('account/success'));
        }

        /*$data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_register'),
            'href' => $this->url->link('account/register', '', true)
        );*/

        $data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('account/login', '', true));

        $data['href_login'] = $this->url->link('account/login', '', true);

        $error = '';
       foreach ($this->error as $er){
           $error .= $er .'<br/>';
       }
        $data['error'] = $error;

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['firstname'])) {
            $data['error_firstname'] = $this->error['firstname'];
        } else {
            $data['error_firstname'] = '';
        }

        if (isset($this->error['lastname'])) {
            $data['error_lastname'] = $this->error['lastname'];
        } else {
            $data['error_lastname'] = '';
        }

        if (isset($this->error['fullname'])) {
            $data['error_fullname'] = $this->error['fullname'];
        } else {
            $data['error_fullname'] = '';
        }

        if (isset($this->error['email_register'])) {
            $data['error_email_register'] = $this->error['email_register'];
        } else {
            $data['error_email_register'] = '';
        }

        if (isset($this->error['email'])) {
            $data['error_email'] = $this->error['email'];
        } else {
            $data['error_email'] = '';
        }

        if (isset($this->error['telephone'])) {
            $data['error_telephone'] = $this->error['telephone'];
        } else {
            $data['error_telephone'] = '';
        }

        if (isset($this->error['custom_field'])) {
            $data['error_custom_field'] = $this->error['custom_field'];
        } else {
            $data['error_custom_field'] = array();
        }

        if (isset($this->error['password'])) {
            $data['error_password'] = $this->error['password'];
        } else {
            $data['error_password'] = '';
        }

        /*if (isset($this->error['confirm'])) {
            $data['error_confirm'] = $this->error['confirm'];
        } else {
            $data['error_confirm'] = '';
        }*/

        $data['home'] = $this->url->link('common/home', '', true);
        $data['action'] = $this->url->link('account/register', '', true);
        $data['urllogin'] = $this->url->link('account/login', '', true);
        $data['action_login'] = $this->url->link('account/login', '', true);
        $data['action_register'] = $this->url->link('account/register', '', true);
        $data['forgotten'] = $this->url->link('account/forgotten', '', true);
        $data['customer_groups'] = array();

        if (is_array($this->config->get('config_customer_group_display'))) {
            $this->load->model('account/customer_group');

            $customer_groups = $this->model_account_customer_group->getCustomerGroups();

            foreach ($customer_groups as $customer_group) {
                if (in_array($customer_group['customer_group_id'], $this->config->get('config_customer_group_display'))) {
                    $data['customer_groups'][] = $customer_group;
                }
            }
        }

        if (isset($this->request->post['customer_group_id'])) {
            $data['customer_group_id'] = $this->request->post['customer_group_id'];
        } else {
            $data['customer_group_id'] = $this->config->get('config_customer_group_id');
        }

        if (!isset($this->request->post['fullname'])) {
            if (isset($this->request->post['firstname'])) {
                $data['firstname'] = $this->request->post['firstname'];
            } else {
                $data['firstname'] = '';
            }

            if (isset($this->request->post['lastname'])) {
                $data['lastname'] = $this->request->post['lastname'];
            } else {
                $data['lastname'] = '';
            }
        }
        else{
            $data['fullname'] = $this->request->post['fullname'];
        }

        if (isset($this->request->post['email'])) {
            $data['email'] = $this->request->post['email'];
        } else {
            $data['email'] = '';
        }

        if (isset($this->request->post['email_register'])) {
            $data['email_register'] = $this->request->post['email_register'];
        } else {
            $data['email_register'] = '';
        }
        if (isset($this->request->post['telephone'])) {
            $data['telephone'] = $this->request->post['telephone'];
        } else {
            $data['telephone'] = '';
        }

        // Custom Fields
        $data['custom_fields'] = array();

        $this->load->model('account/custom_field');

        $custom_fields = $this->model_account_custom_field->getCustomFields();

        foreach ($custom_fields as $custom_field) {
            if ($custom_field['location'] == 'account') {
                $data['custom_fields'][] = $custom_field;
            }
        }

        if (isset($this->request->post['custom_field']['account'])) {
            $data['register_custom_field'] = $this->request->post['custom_field']['account'];
        } else {
            $data['register_custom_field'] = array();
        }

        if (isset($this->request->post['password'])) {
            $data['password'] = $this->request->post['password'];
        } else {
            $data['password'] = '';
        }

        /*if (isset($this->request->post['confirm'])) {
            $data['confirm'] = $this->request->post['confirm'];
        } else {
            $data['confirm'] = '';
        }*/

        if (isset($this->request->post['newsletter'])) {
            $data['newsletter'] = $this->request->post['newsletter'];
        } else {
            $data['newsletter'] = '';
        }

        // Captcha
        if ($this->config->get('captcha_' . $this->config->get('config_captcha') . '_status') && in_array('register', (array)$this->config->get('config_captcha_page'))) {
            $data['captcha'] = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha'), $this->error);
        } else {
            $data['captcha'] = '';
        }

        if ($this->config->get('config_account_id')) {
            $this->load->model('catalog/information');

            $information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

            if ($information_info) {
                $data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_account_id'), true), $information_info['title'], $information_info['title']);
            } else {
                $data['text_agree'] = '';
            }
        } else {
            $data['text_agree'] = '';
        }

        if (isset($this->request->post['agree'])) {
            $data['agree'] = $this->request->post['agree'];
        } else {
            $data['agree'] = false;
        }

        if(isset($_COOKIE["email"])) {
            $data['cookie_email'] = $_COOKIE["email"];
        } else {
            $data['cookie_email'] = '';
        }

        if (isset($_COOKIE["password"])) {
            $data['password'] = $_COOKIE["password"];
        } else {
            $data['password'] = '';
        }

        $data['banners'] =  (isset($this->getBanner()['banners_visible']) ? $this->getBanner()['banners_visible'] : '');
        // $data['column_left'] = $this->load->controller('common/column_left');
        // $data['column_right'] = $this->load->controller('common/column_right');
        // $data['content_top'] = $this->load->controller('common/content_top');
        // $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('account/register', $data));
    }
    private function getBanner()
    {
        $this->load->model('extension/module/theme_builder_config');
        $data = array();
        $banners = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CATEGORY_BANNER);
        $banners = json_decode($banners, true);
        if ($banners['display'][0]['visible'] == 1) {
            $data['banners_visible'] = array(
                'url_banner' => $banners['display'][0]['url'],
                'url_image' => $banners['display'][0]['image-url'],
                'visible_banner' => $banners['display'][0]['visible'],
            );
        }
        return $data;
    }
    private function validate()
    {
        if (!isset($this->request->post['fullname'])) {
            if (!isset($this->request->post['firstname']) || (utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
                $this->error['firstname'] = $this->language->get('error_firstname');
            }

            if (!isset($this->request->post['lastname']) || (utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
                $this->error['lastname'] = $this->language->get('error_lastname');
            }
        } else {
            if ((utf8_strlen(trim($this->request->post['fullname'])) < 1) || (utf8_strlen(trim($this->request->post['fullname'])) > 64)) {
                $this->error['fullname'] = $this->language->get('error_fullname');
            }
        }

        if(isset($this->request->post['email_register'])){
            if (!isset($this->request->post['email_register']) || (utf8_strlen(trim($this->request->post['email_register'])) > 96) || !filter_var(trim($this->request->post['email_register']), FILTER_VALIDATE_EMAIL)) {
                $this->error['email_register'] = $this->language->get('error_email');
            }
            if ($this->model_account_customer->getTotalCustomersByEmail(trim($this->request->post['email_register']))) {
                $this->error['warning'] = $this->language->get('error_exists');
            }
        }
        if(isset($this->request->post['email'])) {
            if (!isset($this->request->post['email']) || (utf8_strlen(trim($this->request->post['email'])) > 96) || !filter_var(trim($this->request->post['email']), FILTER_VALIDATE_EMAIL)) {
                $this->error['email'] = $this->language->get('error_email');
            }
            if ($this->model_account_customer->getTotalCustomersByEmail(trim($this->request->post['email']))) {
                $this->error['warning'] = $this->language->get('error_exists');
            }
        }

        //if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
            //$this->error['telephone'] = $this->language->get('error_telephone');
        //}

        // Customer Group
        if (isset($this->request->post['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->post['customer_group_id'], $this->config->get('config_customer_group_display'))) {
            $customer_group_id = $this->request->post['customer_group_id'];
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        // Custom field validation
        $this->load->model('account/custom_field');

        $custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

        foreach ($custom_fields as $custom_field) {
            if ($custom_field['location'] == 'account') {
                if ($custom_field['required'] && empty($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']])) {
                    $this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
                } elseif (($custom_field['type'] == 'text') && !empty($custom_field['validation']) && !filter_var($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']], FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => $custom_field['validation'])))) {
                    $this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
                }
            }
        }

        if (!isset($this->request->post['password']) || (utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) < 6) || (utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) > 40)
        || !preg_match("/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/i", $this->request->post['password'])
        ) {
            $this->error['password'] = $this->language->get('error_password');
        }

        /*if ($this->request->post['confirm'] != $this->request->post['password']) {
            $this->error['confirm'] = $this->language->get('error_confirm');
        }*/

        // confirm password
        if (isset($this->request->post['confirm_password']) && $this->request->post['confirm_password'] != $this->request->post['password']) {
            $this->error['confirm_password'] = 'Mật khẩu và mật khẩu nhập lại không giống nhau.';
        }

        //
        if(isset($this->request->post['policies']) && $this->request->post['policies'] != 1) {
            $this->error['policies'] = 'Bạn chưa đồng ý với điền khoản.';
        }

        // Captcha
        if ($this->config->get('captcha_' . $this->config->get('config_captcha') . '_status') && in_array('register', (array)$this->config->get('config_captcha_page'))) {
            $captcha = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha') . '/validate');

            if ($captcha) {
                $this->error['captcha'] = $captcha;
            }
        }

        // Agree to terms
        $this->request->post['agree'] = true;   // temp: default = true
        if ($this->config->get('config_account_id')) {
            $this->load->model('catalog/information');

            $information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

            if ($information_info && !isset($this->request->post['agree'])) {
                $this->error['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
            }
        }

        return !$this->error;
    }

    public function customfield()
    {
        $json = array();

        $this->load->model('account/custom_field');

        // Customer Group
        if (isset($this->request->get['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->get['customer_group_id'], $this->config->get('config_customer_group_display'))) {
            $customer_group_id = $this->request->get['customer_group_id'];
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        $custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

        foreach ($custom_fields as $custom_field) {
            $json[] = array(
                'custom_field_id' => $custom_field['custom_field_id'],
                'required' => $custom_field['required']
            );
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}