<?php

class ControllerAccountLogin extends Controller
{
    use Theme_Config_Util;

    private $error = array();

    public function index()
    {
        $this->load->model('account/customer');

        // Login override for admin users
        if (!empty($this->request->get['token'])) {
            $this->customer->logout();
            $this->cart->clear();

            unset($this->session->data['order_id']);
            unset($this->session->data['payment_address']);
            unset($this->session->data['payment_method']);
            unset($this->session->data['payment_methods']);
            unset($this->session->data['shipping_address']);
            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
            unset($this->session->data['comment']);
            unset($this->session->data['coupon']);
            unset($this->session->data['reward']);
            unset($this->session->data['voucher']);
            unset($this->session->data['vouchers']);

            $customer_info = $this->model_account_customer->getCustomerByToken($this->request->get['token']);

            if ($customer_info && $this->customer->login($customer_info['email'], '', true)) {
                // Default Addresses
                $this->load->model('account/address');

                if ($this->config->get('config_tax_customer') == 'payment') {
                    $this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
                }

                if ($this->config->get('config_tax_customer') == 'shipping') {
                    $this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
                }

                $this->response->redirect($this->url->link('common/home', '', true));
            }
        }

        if ($this->customer->isLogged()) {
            $this->response->redirect($this->url->link('common/home', '', true));
        }

        $this->load->language('account/login');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['theme_directory'] = '/catalog/view/theme/' . $this->getCurrentThemeDir();

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            // Unset guest
            unset($this->session->data['guest']);

            // Default Shipping Address
            $this->load->model('account/address');

            if ($this->config->get('config_tax_customer') == 'payment') {
                $this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
            }

            if ($this->config->get('config_tax_customer') == 'shipping') {
                $this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
            }
            //pass session after login
            $key_for_current_order = "current_order_". $this->session->getId() . "_" . 0;
            if (isset($this->session->data[$key_for_current_order])) {
                $data_current_order_none_user = $this->session->data[$key_for_current_order];
                unset($this->session->data[$key_for_current_order]);
                $key_for_current_order = "current_order_". $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());
                if (!isset($this->session->data[$key_for_current_order])) {
                    $this->session->data[$key_for_current_order] = [];
                }
                $this->session->data[$key_for_current_order] = $this->mergeCartInfo(array_merge($this->session->data[$key_for_current_order], $data_current_order_none_user));
            }

            // Wishlist
            if (isset($this->session->data['wishlist']) && is_array($this->session->data['wishlist'])) {
                $this->load->model('account/wishlist');

                foreach ($this->session->data['wishlist'] as $key => $product_id) {
                    $this->model_account_wishlist->addWishlist($product_id);

                    unset($this->session->data['wishlist'][$key]);
                }
            }
            // save password
            $data_post = $this->request->post;
            if (isset($data_post['remember_password'])) {
                setcookie ("email",$data_post['email'],time()+ 3600);
                setcookie ("password",$data_post['password'],time()+ 3600);
            }
            // Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)
            if (isset($this->request->post['redirect']) && $this->request->post['redirect'] != $this->url->link('account/logout', '', true) && (strpos($this->request->post['redirect'], $this->config->get('config_url')) !== false || strpos($this->request->post['redirect'], $this->config->get('config_ssl')) !== false)) {
                $this->response->redirect(str_replace('&amp;', '&', $this->request->post['redirect']));
            } else {
                $this->response->redirect($this->url->link('common/home', '', true));
            }
        }
        $error = '';
        foreach ($this->error as $er){
            $error .= $er .'<br/>';
        }
        $data['error'] = $error;
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('checkout/profile', '', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_login'),
            'href' => $this->url->link('account/login', '', true)
        );

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];

            unset($this->session->data['error']);
        } elseif (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['home'] = $this->url->link('common/home', '', true);
        $data['action'] = $this->url->link('account/login', '', true);
        $data['register'] = $this->url->link('account/register', '', true);
        $data['forgotten'] = $this->url->link('account/forgotten', '', true);
        $data['action_login'] = $this->url->link('account/login', '', true);
        $data['action_register'] = $this->url->link('account/register', '', true);

        // Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)
        if (isset($this->request->post['redirect']) && (strpos($this->request->post['redirect'], $this->config->get('config_url')) !== false || strpos($this->request->post['redirect'], $this->config->get('config_ssl')) !== false)) {
            $data['redirect'] = $this->request->post['redirect'];
        } elseif (isset($this->session->data['redirect'])) {
            $data['redirect'] = $this->session->data['redirect'];

            unset($this->session->data['redirect']);
        } else {
            $data['redirect'] = '';
        }

        if (isset($this->request->get['redirect'])){
            if ($this->request->get['redirect'] == 'checkout'){
                $data['redirect'] =  $this->url->link('checkout/order_preview', '', true);
            }
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['email'])) {
            $data['email'] = $this->request->post['email'];
        } elseif(isset($_COOKIE["email"])) {
            $data['email'] = $_COOKIE["email"];
        } else {
            $data['email'] = '';
        }

        if (isset($this->request->post['password'])) {
            $data['password'] = $this->request->post['password'];
        } elseif (isset($_COOKIE["password"])) {
            $data['password'] = $_COOKIE["password"];
        } else {
            $data['password'] = '';
        }
        $data['banners'] =  (isset($this->getBanner()['banners_visible']) ? $this->getBanner()['banners_visible'] : '');
        // $data['column_left'] = $this->load->controller('common/column_left');
        // $data['column_right'] = $this->load->controller('common/column_right');
        // $data['content_top'] = $this->load->controller('common/content_top');
        // $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['urlregister'] = $this->url->link('account/register', '', true);
        $data['urlforgotten'] = $this->url->link('account/forgotten', '', true);
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('account/login', $data));
    }
    private function getBanner()
    {
        $this->load->model('extension/module/theme_builder_config');
        $data = array();
        $banners = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CATEGORY_BANNER);
        $banners = json_decode($banners, true);
        if ($banners['display'][0]['visible'] == 1) {
            $data['banners_visible'] = array(
                'url_banner' => $banners['display'][0]['url'],
                'url_image' => $banners['display'][0]['image-url'],
                'visible_banner' => $banners['display'][0]['visible'],
            );
        }
        return $data;
    }
    protected function validate()
    {
        // Check how many login attempts have been made.
        $login_info = $this->model_account_customer->getLoginAttempts($this->request->post['email']);

        if ($login_info && ($login_info['total'] >= $this->config->get('config_login_attempts')) && strtotime('-1 hour') < strtotime($login_info['date_modified'])) {
            $this->error['warning'] = $this->language->get('error_attempts');
        }

        // Check if customer has been approved.
        $customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

        if ($customer_info && !$customer_info['status']) {
            $this->error['warning'] = $this->language->get('error_approved');
        }

        if (!$this->error) {
            if (!$this->customer->login($this->request->post['email'], $this->request->post['password'])) {
                $this->error['warning'] = $this->language->get('error_login');

                $this->model_account_customer->addLoginAttempt($this->request->post['email']);
            } else {
                $this->model_account_customer->deleteLoginAttempts($this->request->post['email']);
            }
        }

        return !$this->error;
    }

    private function mergeCartInfo($cart_data)
    {
        $merged_cart_data = [];
        if (!is_array($cart_data)) {
            return $merged_cart_data;
        }

        foreach ($cart_data as $cart_item) {
            if (!isset($cart_item['product_id']) || !isset($cart_item['quantity'])) {
                continue;
            }

            $product_version_id = isset($cart_item['product_version']['product_version_id']) ? $cart_item['product_version']['product_version_id'] : '';
            $product_and_version = sprintf('%s-%s', $cart_item['product_id'], $product_version_id);
            if (!array_key_exists($product_and_version, $merged_cart_data)) {
                $merged_cart_data[$product_and_version] = $cart_item;
                continue;
            }

            // merge quantity of same product id
            $merged_cart_data[$product_and_version]['quantity'] += $cart_item['quantity'];
        }
        return array_values($merged_cart_data);
    }
}
