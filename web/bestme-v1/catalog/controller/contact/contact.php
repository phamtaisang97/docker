<?php

class ControllerContactContact extends Controller
{
    use Theme_Config_Util;
    use RabbitMq_Trait;

    private $error = array();

    /**
     * load contact view or return contact data array
     * @param false $load_from_home
     * @return array
     */
    public function index($load_from_home = false)
    {
        $this->load->language('contact/contact');
        //$this->load->library('mail/mail');
        //$this->load->library('mail/smtp');
        $this->load->model('extension/module/theme_builder_config');
        /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;

        $data = [];

        $data['theme_directory'] = 'catalog/view/theme/' . $this->getCurrentThemeDir();

        $data['customer_info']['firstname'] = $this->customer->getFirstName();
        $data['customer_info']['lastname'] = $this->customer->getLastName();
        $data['customer_info']['fullname'] = $data['customer_info']['lastname'] . ' ' . $data['customer_info']['firstname'];
        $data['customer_info']['email'] = $this->customer->getEmail();
        $data['customer_info']['phone'] = $this->customer->getTelephone();

        $data['href_submit_form'] = $this->url->link('contact/contact', '', true);

        /* get contact config */
        $contact_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CONTACT_SECTIONS);  //       CONFIG_KEY_SECTION_CONTACT_SECTIONS
        $contact_config = json_decode($contact_config, true);
        $data['visible_map'] = $contact_config['section']['map']['visible'];
        $data['visible_info'] = isset($contact_config['section']['info']['visible']) ? $contact_config['section']['info']['visible'] : '';
        $data['visible_form'] = $contact_config['section']['form']['visible'];

        /* contact info */
        $key = 'info';
        $data['contacts_info'] = [];
        if (isset($contact_config['section'][$key]['visible']) && $contact_config['section'][$key]['visible'] == 1) {
            $data['contacts_info'] = [
                'name' => (!empty($contact_config['section'][$key]['name']) ? $contact_config['section'][$key]['name'] : ''),
                'text' => (!empty($contact_config['section'][$key]['text']) ? $contact_config['section'][$key]['text'] : ''),
                'phone' => (!empty($contact_config['section'][$key]['phone']) ? $contact_config['section'][$key]['phone'] : ''),
                'email' => (!empty($contact_config['section'][$key]['email']) ? $contact_config['section'][$key]['email'] : ''),
                'address' => (!empty($contact_config['section'][$key]['address']) ? $contact_config['section'][$key]['address'] : ''),
                'title' => (!empty($contact_config['section'][$key]['title']) ? $contact_config['section'][$key]['title'] : ''),
            ];
        }

        /* contact map */
        $key_map = 'map';
        $data['contacts_map'] = [];
        if ($contact_config['section'][$key_map]['visible'] == 1) {
            $data['contacts_map'] = array(
                'address' => (!empty($contact_config['section'][$key_map]['address']) ? $contact_config['section'][$key_map]['address'] : ''),
                'visible' => 1,
            );
        }

        $social_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_SOCIAL);
        $data['social_config'] = json_decode($social_config, true);
        // filter social enabled
        foreach ($data['social_config'] as $social) {
            $data['social_configs'][] = array(
                'name' => (!empty($social['name']) ? $social['text'] : ''),
                'url' => (!empty($social['name']) ? $social['url'] : ''),
            );
        }

        /* contact form */
        $key_form = 'form';
        $data['contact_config'] = $contact_config['section'][$key_form];
        $data['contacts_email'] = [];
        if ($contact_config['section'][$key_form]['visible'] == 1) {
            $data['contacts_email'] = [
                'title' => (!empty($contact_config['section'][$key_form]['title']) ? $contact_config['section'][$key_form]['title'] : ''),
                'address' => (!empty($contact_config['section'][$key_form]['email']) ? $contact_config['section'][$key_form]['email'] : ''),
                'visible' => 1,
            ];
        }

        /* if request is POST from submit form */
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $mail = new Mail($this->config->get('config_mail_engine'));
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = MAIL_SMTP_HOST_NAME;
            $mail->smtp_username = MAIL_SMTP_USERNAME;
            $mail->smtp_password = html_entity_decode(MAIL_SMTP_PASSWORD, ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = MAIL_SMTP_PORT;
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setTo($data['contacts_email']['address']);

            $mail->setFrom($this->config->get('config_mail_smtp_username'));

            $sender = html_entity_decode($this->request->post['fullname'], ENT_QUOTES, 'UTF-8');
            $mail->setSender($sender);

            $subject = sprintf($this->language->get('email_subject'), $sender);
            $mail->setSubject($subject);
            $phone = isset($this->request->post['phone']) ? $this->request->post['phone'] : '';
            $body_text = sprintf($this->language->get('email_body_text'), $this->request->post['fullname'], $this->request->post['email'], $phone, $this->request->post['your_mind']);
            $mail->setText($body_text);

            try {
                $this->log->write("contact sends mail: processing... ($body_text)");
                $mail->send();
            } catch (Exception $e) {
                $this->log->write('contact sends mail got error: ' . $e->getMessage());
            }

            $this->session->data['success'] = $this->language->get('text_success');

            // redirect to home in case of having error
            $load_from_home = isset($this->request->post['is_get_data']) && '1' == $this->request->post['is_get_data'];
            if ($load_from_home) {
                // save session error message to data array
                $data['success'] = isset($this->session->data['success']) ? $this->session->data['success'] : '';
                $data['error_warning'] = isset($this->session->data['error_warning']) ? $this->session->data['error_warning'] : '';

                return $data;
            } else {
                $this->response->redirect($this->url->link('contact/contact', '', true));
            }
        }

        $this->document->setTitle($this->language->get('heading'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->setKeywords($this->config->get('config_meta_keyword'));

        if (isset($this->request->get['route'])) {
            $this->document->addLink($this->config->get('config_url'), 'canonical');
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            $this->session->data['success'] = '';
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->session->data['error_warning'])) {
            $data['error_warning'] = $this->session->data['error_warning'];
            $this->session->data['error_warning'] = '';
            unset($this->session->data['error_warning']);
        } else {
            $data['error_warning'] = '';
        }

        /* display logo */
        $this->load->model('tool/image');
        $this->load->model('setting/extension');
        $this->load->model('extension/module/theme_builder_config');
        /* get header config */
        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        $header_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HEADER);
        $header_config = json_decode($header_config, true);

        $data['logo'] = isset($header_config['logo']['url']) ? cloudinary_change_http_to_https($header_config['logo']['url']) : 'image/no_image.png';

        /* get footer config */
        $footer_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_FOOTER);
        $footer_config = json_decode($footer_config, true);
        if(isset($footer_config['contact']['visible']) && $footer_config['contact']['visible'] == 1){
            $data['contact'] = array(
                'title'  => $footer_config['contact']['title'],
                'address'  => $footer_config['contact']['address'],
                'phone'  => $footer_config['contact']['phone-number'],
                'email'  => $footer_config['contact']['email'],
                'visible'  => $footer_config['contact']['visible']
            );
        }

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if ($load_from_home) {
            return $data;
        } else {
            $this->response->setOutput($this->load->view('contact/contact', $data));
        }
    }

    protected function validateForm()
    {
        if (!isset($this->request->post['your_mind']) || trim($this->request->post['your_mind']) == '') {
            $this->error['warning'] = $this->language->get('error_warning');
            $this->session->data['error_warning'] = $this->language->get('alert_fail');
        }

        return !$this->error;
    }

    public function installment()
    {
        $this->load->language('contact/contact');
        if ($this->request->post['fullname'] == '') {
            echo json_encode(array('error_code' => 1, 'message' => $this->language->get('form_full_name_placeholder')));
            exit();
        }
        if ($this->request->post['phone'] == '') {
            echo json_encode(array('error_code' => 1, 'message' => $this->language->get('form_phone_number_placeholder')));
            exit();
        }
        if ($this->request->post['your_mind'] == '') {
            echo json_encode(array('error_code' => 1, 'message' => $this->language->get('form_your_mind')));
            exit();
        }
        $this->load->model('extension/module/theme_builder_config');
        /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;
        $data = [];
        $contact_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CONTACT_SECTIONS);  //       CONFIG_KEY_SECTION_CONTACT_SECTIONS
        $contact_config = json_decode($contact_config, true);
        $key_form = 'form';
        $data['contact_config'] = $contact_config['section'][$key_form];

        $data['contacts_email'] = [
            'title' => '',
            'address' => '',
        ];
        if ($contact_config['section'][$key_form]['visible'] == 1) {
            $data['contacts_email'] = array(
                'title' => (!empty($contact_config['section'][$key_form]['title']) ? $contact_config['section'][$key_form]['title'] : ''),
                'address' => (!empty($contact_config['section'][$key_form]['email']) ? $contact_config['section'][$key_form]['email'] : ''),
            );
        }

        $mail = new Mail($this->config->get('config_mail_engine'));
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = MAIL_SMTP_HOST_NAME;
        $mail->smtp_username = MAIL_SMTP_USERNAME;
        $mail->smtp_password = html_entity_decode(MAIL_SMTP_PASSWORD, ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = MAIL_SMTP_PORT;
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($data['contacts_email']['address']);

        $mail->setFrom($this->config->get('config_mail_smtp_username'));

        $sender = html_entity_decode($this->request->post['fullname'], ENT_QUOTES, 'UTF-8');
        $mail->setSender($sender);

        $subject = sprintf($this->language->get('installment_email_subject'), $sender);
        $mail->setSubject($subject);
        $phone = isset($this->request->post['phone']) ? $this->request->post['phone'] : '';
        $productName = (isset($this->request->post['product_attribute']) && $this->request->post['product_attribute'] != '') ? $this->request->post['product_name'] .' - '. $this->request->post['product_attribute'] : $this->request->post['product_name'];
        $body_text = sprintf($this->language->get('installment_email_body_text'), $this->request->post['fullname'], $phone, $productName,  $this->request->post['your_mind']);
        $mail->setText($body_text);
        try {
            $mail->send();
            echo json_encode(array('error_code' => 0, 'message' => ''));
            exit();
        } catch (Exception $e) {
            echo json_encode(array('error_code' => 1, 'message' => 'Không gửi được email'));
            exit();
        }
    }

    public function contactBuyProduct()
    {
        $this->load->language('contact/contact');
        if ($this->request->post['fullname'] == '') {
            echo json_encode(array('error_code' => 1, 'message' => $this->language->get('form_full_name_placeholder')));
            exit();
        }
        if ($this->request->post['phone'] == '') {
            echo json_encode(array('error_code' => 1, 'message' => $this->language->get('form_phone_number_placeholder')));
            exit();
        }
        if ($this->request->post['your_mind'] == '') {
            echo json_encode(array('error_code' => 1, 'message' => $this->language->get('form_your_mind')));
            exit();
        }

        if (!empty($this->request->post['validate_address'])) {
            if (empty($this->request->post['city_delivery'])) {
                echo json_encode(array('error_code' => 1, 'message' => $this->language->get('province_not_null')));
                exit();
            }

            if (empty($this->request->post['district_delivery'])) {
                echo json_encode(array('error_code' => 1, 'message' => $this->language->get('district_not_null')));
                exit();
            }

            if (empty($this->request->post['ward_delivery'])) {
                echo json_encode(array('error_code' => 1, 'message' => $this->language->get('ward_not_null')));
                exit();
            }

            if (empty($this->request->post['address'])) {
                echo json_encode(array('error_code' => 1, 'message' => $this->language->get('address_not_null')));
                exit();
            }
        }

        $this->load->model('extension/module/theme_builder_config');
        /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;
        $data = [];
        $contact_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CONTACT_SECTIONS);  //       CONFIG_KEY_SECTION_CONTACT_SECTIONS
        $contact_config = json_decode($contact_config, true);
        $key_form = 'form';
        $data['contact_config'] = $contact_config['section'][$key_form];

        $data['contacts_email'] = [
            'title' => '',
            'address' => '',
        ];
        if ($contact_config['section'][$key_form]['visible'] == 1) {
            $data['contacts_email'] = [
                'title' => (!empty($contact_config['section'][$key_form]['title']) ? $contact_config['section'][$key_form]['title'] : ''),
                'address' => (!empty($contact_config['section'][$key_form]['email']) ? $contact_config['section'][$key_form]['email'] : ''),
            ];
        }

        $mail = new Mail($this->config->get('config_mail_engine'));
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = MAIL_SMTP_HOST_NAME;
        $mail->smtp_username = MAIL_SMTP_USERNAME;
        $mail->smtp_password = html_entity_decode(MAIL_SMTP_PASSWORD, ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = MAIL_SMTP_PORT;
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($data['contacts_email']['address']);

        $mail->setFrom($this->config->get('config_mail_smtp_username'));

        $sender = html_entity_decode($this->request->post['fullname'], ENT_QUOTES, 'UTF-8');
        $mail->setSender($sender);

        $subject = sprintf($this->language->get('contact_by_product_email_subject'), $sender);
        $mail->setSubject($subject);
        $phone = isset($this->request->post['phone']) ? $this->request->post['phone'] : '';
        $productName = (isset($this->request->post['product_attribute']) && $this->request->post['product_attribute'] != '') ? $this->request->post['product_name'] .' - '. $this->request->post['product_attribute'] : $this->request->post['product_name'];
        $body_text = sprintf($this->language->get('contact_by_product_email_body_text'), $this->request->post['fullname'], $phone, $productName,  $this->request->post['your_mind']);
        $mail->setText($body_text);

        // v3.6.1.2
        $province_name = '';
        $district_name = '';
        $ward_name = '';
        $this->load->model('localisation/vietnam_administrative');
        if (!empty($this->request->post['city_delivery'])) {
            $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($this->request->post['city_delivery']);
            $province_name = $province['name'];
        }

        if (!empty($this->request->post['district_delivery'])) {
            $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($this->request->post['district_delivery']);
            $district_name = $district['name'];
        }

        if (!empty($this->request->post['ward_delivery'])) {
            $ward = $this->model_localisation_vietnam_administrative->getWardByCode($this->request->post['ward_delivery']);
            $ward_name = $ward['name'];
        }

        $address = !empty($this->request->post['address']) ? $this->request->post['address'] : '';
        // End v3.6.1.2

        // message for rabbitmq
        try {
            $message = [
                'shop_name' => $this->config->get('shop_name'),
                'contact' => [
                    'name' => isset($this->request->post['fullname']) ? $this->request->post['fullname'] : '',
                    'phone' => $phone,
                    'note' => isset($this->request->post['your_mind']) ? $this->request->post['your_mind'] : '',
                    'address' => $address,
                    'province_name' => $province_name,
                    'district_name' => $district_name,
                    'ward_name' => $ward_name,
                ],
                'product' => []
            ];

            $product_id = (isset($this->request->post['product_id']) && !empty($this->request->post['product_id'])) ? $this->request->post['product_id'] : '';
            if (!empty($product_id)) {
                $this->load->model('catalog/product');
                $product = $this->model_catalog_product->getProductBasic($product_id);
                if (!empty($product) && !empty($product['source']) && $product['source'] == 'omihub') {
                    $this->load->model('sale/order');
                    $product_version_id = empty($this->request->post['product_version_id']) ? '' : $this->request->post['product_id'];
                    $product_version = $this->model_sale_order->getProductVersionInfo($product_id, $product_version_id);
                    $message['product'] = [
                        'name' => $product['name'],
                        'product_attribute' => isset($this->request->post['product_attribute']) ? $this->request->post['product_attribute'] : '',
                        'name_interested' => $productName,
                        'sku' => empty($product_version['sku']) ? $product['sku'] : $product_version['sku'],
                        'common_sku' => $product['common_sku'],
                        'brand_shop_name' => isset($product['brand_shop_name']) ? $product['brand_shop_name'] : null
                    ];

                    $this->sendMessage(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER_LOGIN, RABBITMQ_USER_PASSWORD, 'request_product', '', 'request_product', json_encode($message));
                }
            }
        } catch (Exception $e) {
            $this->log->write('Send message to rabbitmq error: ' . $e->getMessage());
        }

        try {
            $mail->send();
            echo json_encode(array('error_code' => 0, 'message' => ''));
            exit();
        } catch (Exception $e) {
//            echo json_encode(array('error_code' => 1, 'message' => 'Không gửi được email'));
            echo json_encode(array('error_code' => 0, 'message' => ''));
            exit();
        }
    }
}
