<?php
use App_Api\App_Setting;
class ControllerDesignAppConfigTheme extends Controller
{
    public function getConfigApp()
    {
        // Lấy page hiện tại
        $page = $this->request->post['page'];
        // Lấy theme_value đang sử dụng
        $theme_value = $this->config->get('config_theme');

        $apps = $this->getAppOfPage($theme_value, $page);

        if(empty($apps)){
            echo json_encode(array('error_code' => 1, 'message' => "No App installed for page"));
            exit();
        }else{
            $this->load->model('setting/appstore_setting');
            $app_enable = array();
            foreach($apps as $app){
                $setting = $this->model_setting_appstore_setting->getModule($app['module_id']);
                if(isset($setting['status']) && $setting['status'] == 1){
                    $module_code = $this->model_setting_appstore_setting->getModuleCode($app['module_id']);
                    $app['code'] = $module_code;
                    array_push($app_enable,$app);
                }
            }
            echo json_encode(array('error_code' => 0, 'message' => $app_enable));
            exit();
        }
    }

    public function getAppOfPage($theme, $page)
    {
        $this->load->model('design/app_config_theme');
        $app = $this->model_design_app_config_theme->getAppFromTheme($theme,$page);
        return $app;
    }
    public function getAsset(){
        // Lấy App_code
        $setting = new App_Setting($this->registry);
        $asset = $setting->getAsset();
        echo json_encode(array('error_code' => 0, 'result' => $asset));
        exit();
    }

}