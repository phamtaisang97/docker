<?php

class ControllerToolVietnamAdministrative extends Controller
{
    public function getProvinces()
    {
        $this->load->model('localisation/vietnam_administrative');
        $provinces = $this->model_localisation_vietnam_administrative->getProvinces();

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($provinces));
    }

    public function getProvinceByCode()
    {
        $this->load->model('localisation/vietnam_administrative');
        $code = isset($this->request->get['province_code']) ? $this->request->get['province_code'] : '';
        $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($code);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($province));
    }

    public function getProvinceByName()
    {
        $this->load->model('localisation/vietnam_administrative');
        $name = isset($this->request->get['province_name']) ? $this->request->get['province_name'] : '';
        $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($name);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($province));
    }

    public function getDistrictsByProvinceCode()
    {
        $this->load->model('localisation/vietnam_administrative');
        $provinceCode = isset($this->request->get['province_code']) ? $this->request->get['province_code'] : '';
        $districts = $this->model_localisation_vietnam_administrative->getDistrictsByProvinceCode($provinceCode);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($districts));
    }

    public function getWardsByDistrictCode()
    {
        $this->load->model('localisation/vietnam_administrative');
        $districtCode = isset($this->request->get['district_code']) ? $this->request->get['district_code'] : '';
        $wards = $this->model_localisation_vietnam_administrative->getWardsByDistrictCode($districtCode);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($wards));
    }
}
