<?php

class ControllerApiOpenApiV2 extends Controller
{
    const ORDER_STATUS_DRAFT = 6; // DO NOT HARD CODE HERE. TODO: get from Catalog/Order
    const CONFIG_OPEN_API_KEY = 'config_open_api_api_key';

    /*
     * first implement open_api_v2
     */
    const VER_2_0 = '2.0';

    /*
     * get products with new format
     */
    const VER_2_1 = '2.1';

    private static $SUPPORTED_VERSIONS = [
        self::VER_2_0,
        self::VER_2_1,
    ];

    const DELIVERY_METHOD_DEFAULT = -1;
    const DELIVERY_METHOD_CUSTOM = -2;
    const DELIVERY_METHOD_FREE = -3;

    /**
     * get products list
     * get product detail
     *
     * Notice: support param "&ver=xxx" for new version if has.
     * If no param "&ver=xxx" provided, default ver=1.0
     */
    public function products()
    {
        // check parameter
        if (!$this->validateTokenGetProducts()) {
            $json = [
                'code' => 400,
                'message' => 'invalid token'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));

            return;
        }

        /* get and validate version */
        $ver = $this->getValueFromRequest('GET', 'ver');
        if (empty($ver)) {
            $ver = self::VER_2_0;
        }

        if (!$this->isSupportVersion($ver)) {
            $json = [
                'code' => 400,
                'message' => 'not supported version. Current supported versions: ' . implode(' | ', self::$SUPPORTED_VERSIONS)
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));

            return;
        }

        /* do getting data */
        $this->load->model('api/open_api_v2');
        switch ($ver) {
            case self::VER_2_0:
                $json = $this->model_api_open_api_v2->getProductsListData_v1_0($this->request->get);
                break;

            case self::VER_2_1:
                $json = $this->model_api_open_api_v2->getProductsListData_v1_1($this->request->get);
                break;

            default:
                // not reach this case if $this->isSupportVersion($ver) works properly!
                $json = [
                    'code' => 400,
                    'message' => 'not supported version. Current supported versions: ' . implode(' | ', self::$SUPPORTED_VERSIONS)
                ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * get orders list
     * get order detail
     * create order
     * edit order
     */
    public function orders()
    {
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            // get post data from php://input
            // notice: could not get directly from $this->request->post or $_POST, TODO: why?...
            $this->request->post = json_decode(file_get_contents('php://input'), true);

            if (!isset($this->request->post['data']['order_id']) || empty($this->request->post['data']['order_id'])) {
                return $this->createOrder();
            } else {
                return $this->editOrder();
            }
        }

        if (($this->request->server['REQUEST_METHOD'] == 'GET')) {
            if (!isset($this->request->get['id']) && !isset($this->request->get['order_code'])) {
                return $this->getOrdersList();
            } else {
                return $this->getOrderDetail();
            }
        }

        $result = [
            'code' => 101,
            'message' => 'customer info invalid'
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    public function verify()
    {
        $this->load->language('api/open_api_v1');
        $json = [
            'code' => 1,
            'message' => 'credential (api key) is verified'
        ];

        // check parameter
        if (!$this->validateTokenVerify()) {
            $json = [
                'code' => 400,
                'message' => 'invalid token'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function provinces()
    {
        $this->load->model('localisation/vietnam_administrative');
        $provinces = $this->model_localisation_vietnam_administrative->getProvinces();

        // maintain key
        $provinces = array_values($provinces);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($provinces));
    }

    public function districts()
    {
        $provinceCode = '';
        if (isset($this->request->get['province'])) {
            $provinceCode = $this->request->get['province'];
        }

        if (!empty($provinceCode)) {
            $this->load->model('localisation/vietnam_administrative');
            $districts = $this->model_localisation_vietnam_administrative->getDistrictsByProvinceCode($provinceCode);

            // maintain key
            $districts = array_values($districts);
        } else {
            $districts = [];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($districts));
    }

    public function wards()
    {
        $districtCode = '';
        if (isset($this->request->get['district'])) {
            $districtCode = $this->request->get['district'];
        }

        if (!empty($districtCode)) {
            $this->load->model('localisation/vietnam_administrative');
            $wards = $this->model_localisation_vietnam_administrative->getWardsByDistrictCode($districtCode);

            // maintain key
            $wards = array_values($wards);
        } else {
            $wards = [];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($wards));
    }

    public function deliveries()
    {
        $delivery_methods_config = $this->getDeliveryConfig();

        // TODO: pagination...

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($delivery_methods_config));
    }

    public function delivery_fee()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        // get post data from php://input
        // notice: could not get directly from $this->request->post or $_POST, TODO: why?...
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('[open_api_v1] delivery_fee(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        $data_post = $this->request->post;

        // test data
        /*$data_post = [
            'products' => [
                [
                    'id' => 135,
                    'quantity' => 1
                ]
            ],
            'province' => 89,
            'into_money' => 200004,
            'delivery_id' => "",
        ];*/

        /* calc delivery fee */
        $result = $this->calcDeliveryFee($data_post);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    /* === local functions === */

    /**
     * createOrder
     *
     * @return array|void
     */
    private function createOrder()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $error = [];

        // get post data from php://input
        // notice: could not get directly from $this->request->post or $_POST, TODO: why?...
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('[open_api_v1] orders(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        // sample data
        /*$this->request->post = [
            'token' => 'd1133ac9eddba6d491219f070affc5d2',
            'data' => [
                'customer' => [
                    'name' => 'Nguyen Thuy Linh',
                    'phone_number' => '0987654321',
                    'email' => 'nguyenthuylinh@gmail.com',
                    'delivery_addr' => 'So 1 Ngo 2 Duy Tan',
                    'ward' => 'Dich Vong Hau',
                    'district' => 'Cau Giay',
                    'city' => 'Ha Noi',
                    'note' => 'Goi hang can than va du so luong nhe shop. Thanks!'
                ],
                "status" => 6,
                "customer_ref_id" => "",
                "order_id" => null,
                "tags" => "tag1, tag2",
                "utm" => [
                    {
                        "key" => "campaign",
                        "value" => "Campaign 1"
                    },
                    {
                        "key" => "campaign",
                        "value" => "Campaign 2"
                    },
                    ...
                ],
                'products' => [
                    [
                        'id' => 83,
                        'quantity' => 2
                    ],
                    [
                        'id' => 104,
                        'quantity' => 3,
                        'product_version_id' => 1
                    ]
                ],
                'payment_trans' => "-2",
                'payment_trans_custom_name' => "Shop tự vận chuyển",
                'payment_trans_custom_fee' => 25000
            ]
        ];*/

        $valid_orders = $this->validateOrdersPostData();
        if ($valid_orders) {
            $error = $valid_orders;

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        try {
            // add order
            $this->load->model('account/address');
            $this->load->model('customer/customer');
            $this->load->model('account/customer');
            $this->load->model('localisation/vietnam_administrative');
            // pass data post
            $data['telephone'] = $this->request->post['data']['customer']['phone_number'];

            // extract first name, last name
            $full_name = isset($this->request->post['data']['customer']['name']) ? $this->request->post['data']['customer']['name'] : '';
            $full_name = trim($full_name);
            $first_and_last_name = explode(" ", $full_name);
            $first_name = array_pop($first_and_last_name);
            $last_name = implode(" ", $first_and_last_name);
            $data['fullname'] = $full_name;
            $data['lastname'] = trim($last_name);
            $data['firstname'] = trim($first_name);

            $data['city'] = isset($this->request->post['data']['customer']['city']) ? $this->request->post['data']['customer']['city'] : '';
            $data['email'] = isset($this->request->post['data']['customer']['email']) ? $this->request->post['data']['customer']['email'] : '';
            $data['district'] = isset($this->request->post['data']['customer']['district']) ? $this->request->post['data']['customer']['district'] : '';
            $data['wards'] = isset($this->request->post['data']['customer']['ward']) ? $this->request->post['data']['customer']['ward'] : '';
            $data['address'] = isset($this->request->post['data']['customer']['delivery_addr']) ? $this->request->post['data']['customer']['delivery_addr'] : '';
            $customer = $this->model_customer_customer->getCustomerByTelephone($data['telephone']);
            if ($customer) {
                $customer_id = $customer['customer_id'];
            } else {
                $customer_id = $this->createNewCustomer($data);
            }
            $data['customer_id'] = $customer_id;

            $data['customer_group_id'] = $this->config->get('config_customer_group_id');
            $data['order_payment'] = 1;

            $order_status = isset($this->request->post['data']['status']) ? $this->request->post['data']['status'] : self::ORDER_STATUS_DRAFT;
            $data['order_transfer'] = $order_status;

            $data['order_note'] = isset($this->request->post['data']['customer']['note']) ? $this->request->post['data']['customer']['note'] : '';
            $data['payment_firstname'] = $data['firstname'];
            $data['payment_lastname'] = $data['lastname'];
            $data['payment_company'] = '';
            $data['payment_address_1'] = $data['address'];
            $data['payment_address_2'] = sprintf('%s, %s, %s, %s', $data['address'], $data['wards'], $data['district'], $data['city']);
            $data['payment_city'] = $data['city'];
            $data['payment_postcode'] = '';
            $data['payment_country_id'] = '';
            $data['payment_zone_id'] = '';
            $data['payment_custom_field'] = array();
            $data['payment_code'] = '';
            $data['payment_status'] = getValueByKey($this->request->post['data'], 'payment_status') == 1 ? 1 : 0;
            $data['shipping_firstname'] = $data['firstname'];
            $data['shipping_lastname'] = $data['lastname'];
            $data['shipping_company'] = '';
            $data['shipping_address_1'] = $data['address'];
            $data['shipping_address_2'] = sprintf('%s, %s, %s, %s', $data['address'], $data['wards'], $data['district'], $data['city']);
            $data['shipping_city'] = $data['city'];
            $data['shipping_postcode'] = '';
            $data['shipping_country_id'] = '';
            $data['shipping_zone_id'] = '';
            $data['shipping_custom_field'] = array();
            $data['shipping_method'] = '';
            $data['shipping_method_value'] = ''; // phương thức vẫn chuyển mặc định để rõng
            $data['shipping_code'] = '';
            $data['shipping_ward_code'] = $data['wards'];
            $data['shipping_district_code'] = $data['district'];
            $data['shipping_province_code'] = $data['city'];

            $data['order_products'] = array();
            $data['order_vouchers'] = array();
            $data['order_totals'] = array();

            $data['order_status_id'] = $order_status;
            $data['order_status'] = $order_status;  /////Đang xử lý; @@
            $data['currency_code'] = $this->config->get('config_currency');
            $data['from_domain'] = ''; // auto from main domain.

            $data['coupon'] = '';
            $data['voucher'] = '';
            $data['reward'] = '';

            $data['tags'] = isset($this->request->post['data']['tags']) ? $this->request->post['data']['tags'] : '';

            // for making 3rd party customer with bestme customer
            $data['customer_ref_id'] = isset($this->request->post['data']['customer_ref_id']) ? $this->request->post['data']['customer_ref_id'] : '';

            // 20200420: support order utm
            $data['utm'] = isset($this->request->post['data']['utm']) ? $this->request->post['data']['utm'] : [];

            $products = $this->request->post['data']['products'];
            // merge products quantity if same id
            $grouped_products = [];
            foreach ($products as $product) {
                if (!isset($product['id']) || !isset($product['quantity']) || (int)$product['quantity'] == 0) {
                    continue;
                }

                $key = $product['id'];
                if (isset($product['product_version_id']) && !empty($product['product_version_id'])) {
                    $key = sprintf('%s-%s', $product['id'], $product['product_version_id']);
                }

                // new if not exist
                if (!array_key_exists($key, $grouped_products)) {
                    $grouped_products[$key] = $product;
                    continue;
                }

                // or sum quantity if existed
                $grouped_products[$key]['quantity'] += $product['quantity'];
            }

            $this->load->model('catalog/product');
            $this->load->model('api/open_api_v2');
            $total_amount = 0;
            $total_into_money = 0;
            $not_available_products = [];
            foreach ($grouped_products as &$product) {
                if (!isset($product['id']) || !isset($product['quantity']) || (int)$product['quantity'] == 0) {
                    $error = [
                        'code' => 102,
                        'message' => 'products info invalid. Missing key either "id" or "quantity"'
                    ];
                    break;
                }

                // get product info same as api get product detail
                $product_info = $this->model_api_open_api_v2->getProductsListData_v1_1([
                    'id' => $product['id'],
                    'product_version_id' => isset($product['product_version_id']) ? $product['product_version_id'] : ''
                ], $all_statuses = true, $more_return_fields = ['status', 'deleted', 'quantity_in_stock', 'sale_on_out_of_stock']);

                // Notice: validate production not available (out_of_stock, deleted, disabled...) if draft for processing
                if (empty($order_status) || in_array($order_status, [
                        ModelSaleOrder::ORDER_STATUS_ID_DRAFT,
                        ModelSaleOrder::ORDER_STATUS_ID_PROCESSING
                    ])
                ) {
                    if (!$product_info ||
                        (isset($product_info['deleted']) && !empty($product_info['deleted'])) ||
                        (isset($product_info['status']) && $product_info['status'] != 1)
                    ) {
                        $product_info['error_code'] = 1042;
                        unset($product_info['status'], $product_info['deleted'], $product_info['quantity_in_stock'], $product_info['sale_on_out_of_stock']);
                        //$product_info['quantity'] = $product['quantity'];
                        $not_available_products[] = $product_info;
                        continue;
                    }

                    if (isset($product_info['quantity_in_stock']) && $product_info['quantity_in_stock'] <= 0 &&
                        isset($product_info['sale_on_out_of_stock']) && $product_info['sale_on_out_of_stock'] != 1
                    ) {
                        $product_info['error_code'] = 1041;
                        unset($product_info['status'], $product_info['deleted'], $product_info['quantity_in_stock'], $product_info['sale_on_out_of_stock']);
                        //$product_info['quantity'] = $product['quantity'];
                        $not_available_products[] = $product_info;
                        continue;
                    }
                }

                // TODO: use above product info?
                $product_detail = $this->model_catalog_product->getProduct($product['id']);

                $product_quantity_max = $this->getMaxQuantityProduct($product_detail, $product);
                $product_version = [];
                if (!$product_detail || !isset($product_detail['product_id']) || ($product_detail['multi_versions'] == 1 && count($product_version = $this->model_catalog_product->getProductByVersionId($product_detail['product_id'], $product['product_version_id'])) == 0)) {
                    $error = [
                        'code' => 201,
                        'message' => 'error product order'
                    ];
                    break;
                }

                if (!$product_quantity_max) {
                    /*$error = [
                        'code' => 201,
                        'message' => 'error product order'
                    ];
                    break;*/
                }

                $product['name'] = $product_detail['name'];
                $product['model'] = $product_detail['model'];
                $product['price'] = $product_detail['price'];
                $product['product_id'] = $product['id'];
                if (isset($product['product_version_id']) && !empty($product['product_version_id'])) {
                    if (!isset($product_version['price']) || (int)$product_version['price'] == 0) {
                        $product_version['price'] = isset($product_version['compare_price']) ? (int)$product_version['compare_price'] : null;
                    }
                    $total = (float)$product_version['price'] * (int)$product['quantity'];
                    $product['product_version'] = $product_version;
                } else {
                    if ((int)$product_detail['price'] == 0) $product_detail['price'] = $product_detail['compare_price_master'];
                    $total = (float)$product_detail['price'] * (int)$product['quantity'];
                }
                $total_amount += $product['quantity'];
                $total_into_money += $total;
            }
            unset($product);

            if (!empty($not_available_products)) {
                $error = [
                    "code" => 104,
                    "message" => "create order failed: products are out of stock or do not existed",
                    "products" => $not_available_products
                ];
            }

            $data['shipping_fee'] = 0;
            $data['shipping_method'] = '';
            $data['shipping_method_value'] = '';
            // 20200612: support shipping fee
            $delivery_id = isset($this->request->post['data']['payment_trans']) ? $this->request->post['data']['payment_trans'] : null;
            switch ($delivery_id) {
                case self::DELIVERY_METHOD_CUSTOM:
                    if (!isset($this->request->post['data']['payment_trans_custom_name']) ||
                        !isset($this->request->post['data']['payment_trans_custom_fee'])
                    ) {
                        $error = [
                            "code" => 400,
                            "message" => "create order failed: missing neither payment_trans custom_name nor custom_fee"
                        ];

                        break;
                    }

                    $data['shipping_fee'] = (int)$this->request->post['data']['payment_trans_custom_fee'];
                    $data['shipping_method'] = $this->request->post['data']['payment_trans_custom_name'];
                    $data['shipping_method_value'] = self::DELIVERY_METHOD_CUSTOM;

                    break;

                case self::DELIVERY_METHOD_FREE:
                    $data['shipping_fee'] = 0;
                    $data['shipping_method'] = 'Miễn phí vận chuyển';
                    $data['shipping_method_value'] = self::DELIVERY_METHOD_FREE;

                    break;

                default:
                    if (is_null($delivery_id)) {
                        break;
                    }

                    // get shipping method name
                    $deliveries = $this->getDeliveryConfig();
                    foreach ($deliveries as $delivery) {
                        if (!isset($delivery['id']) || !isset($delivery['name']) || $delivery['id'] != $delivery_id) {
                            continue;
                        }

                        if ($delivery['id'] == $delivery_id) {
                            $data['shipping_method'] = $delivery['name'];
                            $data['shipping_method_value'] = $delivery['id'];
                            break;
                        }
                    }

                    // calc delivery fee
                    $data_calc_delivery_fee = [
                        'products' => [
                            /*[
                                'id' => 135,
                                'quantity' => 1
                            ]*/
                        ],
                        'province' => $data['city'],
                        'into_money' => $total_into_money,
                        'delivery_id' => $delivery_id,
                    ];

                    foreach ($products as $product) {
                        if (!isset($product['id']) || !isset($product['quantity']) || (int)$product['quantity'] == 0) {
                            continue;
                        }

                        $data_calc_delivery_fee['products'][] = [
                            'id' => $product['id'],
                            'quantity' => $product['quantity']
                        ];
                    }

                    $delivery_fees = $this->calcDeliveryFee($data_calc_delivery_fee);

                    // error
                    if (isset($delivery_fees['error']) && !empty($delivery_fees['error'])) {
                        $error = [
                            "code" => 104,
                            "message" => "create order failed: calculate delivery fee failed (may products are out of stock or do not existed)",
                            "products" => $not_available_products
                        ];

                        break;
                    }

                    // success
                    if (isset($delivery_fees['delivery_fees'][0]['fee'])) {
                        $data['shipping_fee'] = (int)$delivery_fees['delivery_fees'][0]['fee'];
                        $_delivery = getValueByKey($delivery_fees['delivery_fees'][0], 'delivery', [
                            'id' => '',
                            'name' => '',
                        ]);
                        $data['shipping_method'] = $_delivery['name'];
                        $data['shipping_method_value'] = $_delivery['id'];
                    }

                    break;
            }

            $data['total'] = $data['shipping_fee'] + $total_into_money;
            if (!$error) {
                $this->load->model('sale/order');
                $order_id = $this->model_sale_order->addOrderCommon($data, $data['customer_id']);
                // confix Prefix, suffix in order_code
                $this->load->model('setting/setting');

                $config_id_prefix = $this->model_setting_setting->getSettingByKey('config_id_prefix');
                $config_id_prefix = isset($config_id_prefix['value']) ? $config_id_prefix['value'] : '';

                $config_id_suffix = $this->model_setting_setting->getSettingByKey('config_id_suffix');
                $config_id_suffix = isset($config_id_suffix['value']) ? $config_id_suffix['value'] : '';

                $max_order_id = 99999999999;
                $value_order = $max_order_id - ($max_order_id - $order_id);
                $strlen_value = strlen($value_order);
                $number_for = 11 - $strlen_value;
                $number_zero = '0';
                $result = '';

                for ($i = 1; $i <= $number_for; $i++) {
                    $result .= $number_zero;
                }
                $order_code = $config_id_prefix . $result . $order_id . $config_id_suffix;
                // TODO: set "source=xxx"?
                $this->db->query("UPDATE " . DB_PREFIX . "order SET order_code = '" . $this->db->escape($order_code) . "' WHERE order_id = " . (int)$order_id);

                $this->model_sale_order->addOrderProduct($grouped_products, $order_id);
                $this->model_sale_order->addReport($order_id);
                $error = [
                    'code' => 0,
                    'message' => 'ORDER_CREATED',
                    'order_id' => $order_id,
                    'order_code' => $order_code
                ];
            }
        } catch (Exception $e) {
            $error = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($error));
    }

    /**
     * editOrder
     *
     * @return array|void
     */
    private function editOrder()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $error = array();

        // get post data from php://input
        // notice: could not get directly from $this->request->post or $_POST, TODO: why?...
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('[open_api_v1] orders(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        // check if has order_id
        if (!isset($this->request->post['data']['order_id']) || empty($this->request->post['data']['order_id'])) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Missing "order_id"!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $order_id = $this->request->post['data']['order_id'];

        // sample data
        /*$this->request->post = [
            'token' => 'd1133ac9eddba6d491219f070affc5d2',
            'data' => [
                'customer' => [
                    'name' => 'Nguyen Thuy Linh',
                    'phone_number' => '0987654321',
                    'email' => 'nguyenthuylinh@gmail.com',
                    'delivery_addr' => 'So 1 Ngo 2 Duy Tan',
                    'ward' => 'Dich Vong Hau',
                    'district' => 'Cau Giay',
                    'city' => 'Ha Noi',
                    'note' => 'Goi hang can than va du so luong nhe shop. Thanks!'
                ],
                "status" => 6,
                "customer_ref_id" => "",
                "order_id" => 213,
                "tags" => "tag1, tag2",
                'products' => [
                    [
                        'id' => 83,
                        'quantity' => 2
                    ],
                    [
                        'id' => 104,
                        'quantity' => 3,
                        'product_version_id' => 1
                    ]
                ]
            ]
        ];*/

        $valid_orders = $this->validateOrdersPostData();
        if ($valid_orders) {
            $error = $valid_orders;

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        try {
            $this->load->model('account/address');
            $this->load->model('customer/customer');
            $this->load->model('account/customer');
            $this->load->model('localisation/vietnam_administrative');

            // pass data post
            $data['telephone'] = $this->request->post['data']['customer']['phone_number'];

            // extract first name, last name
            $full_name = isset($this->request->post['data']['customer']['name']) ? $this->request->post['data']['customer']['name'] : '';
            $full_name = trim($full_name);
            $first_and_last_name = explode(" ", $full_name);
            $first_name = array_pop($first_and_last_name);
            $last_name = implode(" ", $first_and_last_name);
            $data['fullname'] = $full_name;
            $data['lastname'] = trim($last_name);
            $data['firstname'] = trim($first_name);

            $data['city'] = isset($this->request->post['data']['customer']['city']) ? $this->request->post['data']['customer']['city'] : '';
            $data['email'] = isset($this->request->post['data']['customer']['email']) ? $this->request->post['data']['customer']['email'] : '';
            $data['district'] = isset($this->request->post['data']['customer']['district']) ? $this->request->post['data']['customer']['district'] : '';
            $data['wards'] = isset($this->request->post['data']['customer']['ward']) ? $this->request->post['data']['customer']['ward'] : '';
            $data['address'] = isset($this->request->post['data']['customer']['delivery_addr']) ? $this->request->post['data']['customer']['delivery_addr'] : '';
            $customer = $this->model_customer_customer->getCustomerByTelephone($data['telephone']);
            if ($customer) {
                $customer_id = $customer['customer_id'];
            } else {
                $customer_id = $this->createNewCustomer($data);
            }
            $data['customer_id'] = $customer_id;

            $data['customer_group_id'] = $this->config->get('config_customer_group_id');
            $data['order_payment'] = 1;

            $order_status = isset($this->request->post['data']['status']) ? $this->request->post['data']['status'] : self::ORDER_STATUS_DRAFT;
            $data['order_transfer'] = $order_status;

            $data['order_note'] = isset($this->request->post['data']['customer']['note']) ? $this->request->post['data']['customer']['note'] : '';
            $data['payment_firstname'] = $data['firstname'];
            $data['payment_lastname'] = $data['lastname'];
            $data['payment_company'] = '';
            $data['payment_address_1'] = $data['address'];
            $data['payment_address_2'] = sprintf('%s, %s, %s, %s', $data['address'], $data['wards'], $data['district'], $data['city']);
            $data['payment_city'] = $data['city'];
            $data['payment_postcode'] = '';
            $data['payment_country_id'] = '';
            $data['payment_zone_id'] = '';
            $data['payment_custom_field'] = array();
            $data['payment_code'] = '';
            $data['payment_status'] = getValueByKey($this->request->post['data'], 'payment_status') == 1 ? 1 : 0;
            $data['shipping_firstname'] = $data['firstname'];
            $data['shipping_lastname'] = $data['lastname'];
            $data['shipping_company'] = '';
            $data['shipping_address_1'] = $data['address'];
            $data['shipping_address_2'] = sprintf('%s, %s, %s, %s', $data['address'], $data['wards'], $data['district'], $data['city']);
            $data['shipping_city'] = $data['city'];
            $data['shipping_postcode'] = '';
            $data['shipping_country_id'] = '';
            $data['shipping_zone_id'] = '';
            $data['shipping_custom_field'] = array();
            //$data['shipping_method'] = '';
            $data['shipping_method_value'] = ''; // phương thức vẫn chuyển mặc định để rõng
            $data['shipping_code'] = '';
            $data['shipping_ward_code'] = $data['wards'];
            $data['shipping_district_code'] = $data['district'];
            $data['shipping_province_code'] = $data['city'];

            $data['order_products'] = array();
            $data['order_vouchers'] = array();
            $data['order_totals'] = array();

            $data['order_status_id'] = $order_status;
            $data['order_status'] = $order_status;
            $data['currency_code'] = $this->config->get('config_currency');
            $data['from_domain'] = ''; // auto from main domain.

            $data['coupon'] = '';
            $data['voucher'] = '';
            $data['reward'] = '';

            $data['tags'] = isset($this->request->post['data']['tags']) ? $this->request->post['data']['tags'] : '';

            // for making 3rd party customer with bestme customer
            $data['customer_ref_id'] = isset($this->request->post['data']['customer_ref_id']) ? $this->request->post['data']['customer_ref_id'] : '';

            $products = $this->request->post['data']['products'];
            // merge products quantity if same id
            $grouped_products = [];
            foreach ($products as $product) {
                if (!isset($product['id']) || !isset($product['quantity']) || (int)$product['quantity'] == 0) {
                    continue;
                }

                // new if not exist
                if (!array_key_exists($product['id'], $grouped_products)) {
                    $grouped_products[$product['id']] = $product;
                    continue;
                }

                // or sum quantity if existed
                $grouped_products[$product['id']]['quantity'] += $product['quantity'];
            }

            $this->load->model('catalog/product');
            $this->load->model('api/open_api_v2');
            $total_amount = 0;
            $total_into_money = 0;
            $not_available_products = [];
            foreach ($grouped_products as &$product) {
                if (!isset($product['id']) || !isset($product['quantity']) || (int)$product['quantity'] == 0) {
                    $error = [
                        'code' => 102,
                        'message' => 'products info invalid. Missing key either "id" or "quantity"'
                    ];
                    break;
                }

                // get product info same as api get product detail
                $product_info = $this->model_api_open_api_v2->getProductsListData_v1_1([
                    'id' => $product['id'],
                    'product_version_id' => isset($product['product_version_id']) ? $product['product_version_id'] : ''
                ], $all_statuses = true, $more_return_fields = ['status', 'deleted', 'quantity_in_stock', 'sale_on_out_of_stock']);

                // Notice: validate production not available (out_of_stock, deleted, disabled...) if draft for processing
                if (empty($order_status) || in_array($order_status, [
                        ModelSaleOrder::ORDER_STATUS_ID_DRAFT,
                        ModelSaleOrder::ORDER_STATUS_ID_PROCESSING
                    ])
                ) {
                    if (!$product_info ||
                        (isset($product_info['deleted']) && !empty($product_info['deleted'])) ||
                        (isset($product_info['status']) && $product_info['status'] != 1)
                    ) {
                        $product_info['error_code'] = 1042;
                        unset($product_info['status'], $product_info['deleted'], $product_info['quantity_in_stock'], $product_info['sale_on_out_of_stock']);
                        //$product_info['quantity'] = $product['quantity'];
                        $not_available_products[] = $product_info;
                        continue;
                    }

                    if (isset($product_info['quantity_in_stock']) && $product_info['quantity_in_stock'] <= 0 &&
                        isset($product_info['sale_on_out_of_stock']) && $product_info['sale_on_out_of_stock'] != 1
                    ) {
                        $product_info['error_code'] = 1041;
                        unset($product_info['status'], $product_info['deleted'], $product_info['quantity_in_stock'], $product_info['sale_on_out_of_stock']);
                        //$product_info['quantity'] = $product['quantity'];
                        $not_available_products[] = $product_info;
                        continue;
                    }
                }

                // TODO: use above product info?
                $product_detail = $this->model_catalog_product->getProduct($product['id']);

                $product_quantity_max = $this->getMaxQuantityProduct($product_detail, $product);
                $product_version = [];
                if (!$product_detail || ($product_detail['multi_versions'] == 1 && count($product_version = $this->model_catalog_product->getProductByVersionId($product_detail['product_id'], $product['product_version_id'])) == 0)) {
                    /*$error = [
                        'code' => 201,
                        'message' => 'error product order'
                    ];
                    break;*/
                }

                if (!$product_quantity_max) {
                    /*$error = [
                        'code' => 201,
                        'message' => 'error product order'
                    ];
                    break;*/
                }

                $product['name'] = $product_detail['name'];
                $product['model'] = $product_detail['model'];
                $product['price'] = $product_detail['price'];
                $product['product_id'] = $product['id'];
                if (isset($product['product_version_id']) && !empty($product['product_version_id'])) {
                    if ((int)$product_version['price'] == 0) $product_version['price'] = $product_version['compare_price'];
                    $total = (float)$product_version['price'] * (int)$product['quantity'];
                    $product['product_version'] = $product_version;
                } else {
                    if ((int)$product_detail['price'] == 0) $product_detail['price'] = $product_detail['compare_price_master'];
                    $total = (float)$product_detail['price'] * (int)$product['quantity'];
                }
                $total_amount += $product['quantity'];
                $total_into_money += $total;
            }
            unset($product);

            if (!empty($not_available_products)) {
                $error = [
                    "code" => 104,
                    "message" => "create order failed: products are out of stock or do not existed",
                    "products" => $not_available_products
                ];
            }

            $data['shipping_fee'] = 0;
            $data['shipping_method'] = '';
            // 20200612: support shipping fee
            $delivery_id = isset($this->request->post['data']['payment_trans']) ? $this->request->post['data']['payment_trans'] : null;
            switch ($delivery_id) {
                case self::DELIVERY_METHOD_CUSTOM:
                    if (!isset($this->request->post['data']['payment_trans_custom_name']) ||
                        !isset($this->request->post['data']['payment_trans_custom_fee'])
                    ) {
                        $error = [
                            "code" => 400,
                            "message" => "create order failed: missing neither payment_trans custom_name nor custom_fee"
                        ];

                        break;
                    }

                    $data['shipping_method'] = $this->request->post['data']['payment_trans_custom_name'];
                    $data['shipping_fee'] = (int)$this->request->post['data']['payment_trans_custom_fee'];

                    break;

                case self::DELIVERY_METHOD_FREE:
                    $data['shipping_method'] = 'Miễn phí vận chuyển';
                    $data['shipping_fee'] = 0;

                    break;

                default:
                    if (is_null($delivery_id)) {
                        break;
                    }

                    // get shipping method name
                    $deliveries = $this->getDeliveryConfig();
                    foreach ($deliveries as $delivery) {
                        if (!isset($delivery['id']) || !isset($delivery['name']) || $delivery['id'] != $delivery_id) {
                            continue;
                        }

                        if ($delivery['id'] == $delivery_id) {
                            $data['shipping_method'] = $delivery['name'];
                            break;
                        }
                    }

                    // calc delivery fee
                    $data_calc_delivery_fee = [
                        'products' => [
                            /*[
                                'id' => 135,
                                'quantity' => 1
                            ]*/
                        ],
                        'province' => $data['city'],
                        'into_money' => $total_into_money,
                        'delivery_id' => $delivery_id,
                    ];

                    foreach ($products as $product) {
                        if (!isset($product['id']) || !isset($product['quantity']) || (int)$product['quantity'] == 0) {
                            continue;
                        }

                        $data_calc_delivery_fee['products'][] = [
                            'id' => $product['id'],
                            'quantity' => $product['quantity']
                        ];
                    }

                    $delivery_fees = $this->calcDeliveryFee($data_calc_delivery_fee);

                    // error
                    if (isset($delivery_fees['error']) && !empty($delivery_fees['error'])) {
                        $error = [
                            "code" => 104,
                            "message" => "create order failed: calculate delivery fee failed (may products are out of stock or do not existed)",
                            "products" => $not_available_products
                        ];

                        break;
                    }

                    // success
                    if (isset($delivery_fees['delivery_fees'][0]['fee'])) {
                        $data['shipping_fee'] = (int)$delivery_fees['delivery_fees'][0]['fee'];
                        $_delivery = getValueByKey($delivery_fees['delivery_fees'][0], 'delivery', [
                            'id' => '',
                            'name' => '',
                        ]);
                        $data['shipping_method'] = $_delivery['name'];
                        $data['shipping_method_value'] = $_delivery['id'];
                    }

                    break;
            }

            $data['total'] = $data['shipping_fee'] + $total_into_money;
            if (!$error) {
                $this->load->model('sale/order');
                $this->model_sale_order->editOrderCommon($order_id, $data, $customer_id);

                $this->db->query("DELETE FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
                $this->model_sale_order->addOrderProduct($grouped_products, $order_id);
                $this->model_sale_order->addReport($order_id);

                $error = [
                    'code' => 0,
                    'message' => 'ORDER_EDITED'
                ];
            }
        } catch (Exception $e) {
            $error = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($error));
    }

    /**
     * getOrdersList
     */
    private function getOrdersList()
    {
        $this->load->language('api/open_api_v1');

        // check parameter
        if (!$this->validateTokenGetOrdersList()) {
            $json = [
                'code' => 400,
                'message' => 'invalid token'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));

            return;
        }

        $data_filter = [];
        if (isset($this->request->get['page']) && !empty($this->request->get['page'])) {
            $data_filter = [
                'start' => ((int)$this->request->get['page'] - 1) * ((int)isset($this->request->get['limit']) ? $this->request->get['limit'] : 15),
                'limit' => isset($this->request->get['limit']) ? $this->request->get['limit'] : 15
            ];
        }

        $data_filter['customer_ref_id'] = isset($this->request->get['customer_ref_id']) ? $this->request->get['customer_ref_id'] : '';
        $data_filter['order_ids'] = isset($this->request->get['order_ids']) ? $this->request->get['order_ids'] : '';
        $data_filter['order_tag'] = isset($this->request->get['order_tag']) ? $this->request->get['order_tag'] : '';
        $data_filter['order_utm_key'] = isset($this->request->get['order_utm_key']) ? $this->request->get['order_utm_key'] : '';
        $data_filter['order_utm_value'] = isset($this->request->get['order_utm_value']) ? $this->request->get['order_utm_value'] : '';

        $this->load->model('api/open_api_v2');
        $json = $this->model_api_open_api_v2->getOrdersListData($data_filter);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function getOrderDetail()
    {
        $this->load->language('api/open_api_v1');

        // check parameter
        if (!$this->validateTokenGetOrderDetail()) {
            $json = [
                'code' => 400,
                'message' => 'invalid token'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));

            return;
        }

        $order_id = isset($this->request->get['id']) ? $this->request->get['id'] : '';
        $order_code = isset($this->request->get['order_code']) ? $this->request->get['order_code'] : '';
        if (empty($order_id) && empty($order_code)) {
            $json = [];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));

            return;
        }

        // do getting order detail
        $data_filter = [];
        $data_filter['customer_ref_id'] = isset($this->request->get['customer_ref_id']) ? $this->request->get['customer_ref_id'] : '';
        if (!empty($order_id)) {
            // prefer filter order id than order_code!
            $data_filter['order_ids'] = $order_id;
        } else {
            $data_filter['order_code'] = $order_code;
        }

        $this->load->model('api/open_api_v2');
        $json = $this->model_api_open_api_v2->getOrdersListData($data_filter);
        $order_detail = [];
        if (isset($json['records']) && is_array($json['records']) && count($json['records']) > 0) {
            $order_detail = $json['records'][0];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($order_detail));
    }

    private function validateOrdersPostData()
    {
        $this->load->model('sale/order');

        if (!isset($this->request->post['token'])) {
            return [
                'code' => 400,
                'message' => 'invalid token'
            ];
        }

        if ($this->getMd5TokenPost() != $this->request->post['token']) {
            return [
                'code' => 400,
                'message' => 'invalid token'
            ];
        }

        $phone = isset($this->request->post['data']['customer']['phone_number']) ? $this->request->post['data']['customer']['phone_number'] : '';
        if (!$phone || (utf8_strlen($phone) < 3) || (utf8_strlen($phone) > 32)) {
            return [
                'code' => 101,
                'message' => 'customer info invalid. phone_number length must be 3-32'
            ];
        }

        // NO NEED validate address if:
        // - does not have order status (v1) (known as status = 6 (draft) for default)
        // - has order status (v2) and status = 6 (draft)
        $order_status = isset($this->request->post['data']['status']) ? $this->request->post['data']['status'] : '';
        if (empty($order_status) || $order_status != ModelSaleOrder::ORDER_STATUS_ID_DRAFT) {
            $address = isset($this->request->post['data']['customer']['delivery_addr']) ? $this->request->post['data']['customer']['delivery_addr'] : '';
            if (empty($address)) {
                return [
                    'code' => 101,
                    'message' => 'customer info invalid. delivery_addr could not be empty with order status "draft" (' . ModelSaleOrder::ORDER_STATUS_ID_DRAFT . ')'
                ];
            }
        }

        $products = isset($this->request->post['data']['products']) ? $this->request->post['data']['products'] : '';
        if (!$products || !is_array($products)) {
            return [
                'code' => 102,
                'message' => 'products info invalid. products must be array and could not be empty'
            ];
        }

        foreach ($products as $p) {
            if (!isset($p['id']) || !isset($p['quantity'])) {
                return [
                    'code' => 102,
                    'message' => 'products info invalid. Missing key either "id" or "quantity"'
                ];
            }
        }

        return;
    }

    private function validateToken()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }

        if ($this->getMd5TokenGet() !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    private function validateTokenGetProducts()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }


        if ($this->getMd5TokenGetProductsList() !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    private function validateTokenGetOrdersList()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }

        if ($this->getMd5TokenGetOrdersList() !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    private function validateTokenGetOrderDetail()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }

        if ($this->getMd5TokenGetOrderDetail() !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    private function validateTokenVerify()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }

        if ($this->getMd5TokenVerify() !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    private function getMd5TokenGet()
    {
        $apiKey = $this->getApiKey();
        $token_string = isset($this->request->get['page']) ? $this->request->get['page'] : '';
        $token_string .= isset($this->request->get['limit']) ? $this->request->get['limit'] : '';
        $token_string .= urlencode(isset($this->request->get['searchKey']) ? $this->request->get['searchKey'] : '');
        $token_string .= isset($this->request->get['searchField']) ? $this->request->get['searchField'] : '';
        $token_string .= isset($this->request->get['id']) ? $this->request->get['id'] : '';
        $token_string .= isset($this->request->get['product_version_id']) ? $this->request->get['product_version_id'] : '';
        $token_string .= $apiKey;
        return md5($token_string);
    }

    private function getMd5TokenGetOrdersList()
    {
        $data = [];
        $data[] = isset($this->request->get['page']) ? $this->request->get['page'] : '';
        $data[] = isset($this->request->get['limit']) ? $this->request->get['limit'] : '';
        $data[] = urlencode(isset($this->request->get['customer_ref_id']) ? $this->request->get['customer_ref_id'] : '');
        $data[] = isset($this->request->get['order_ids']) ? $this->request->get['order_ids'] : '';
        $data[] = urlencode(isset($this->request->get['order_tag']) ? $this->request->get['order_tag'] : '');
        $data[] = urlencode(isset($this->request->get['order_utm_key']) ? $this->request->get['order_utm_key'] : '');
        $data[] = urlencode(isset($this->request->get['order_utm_value']) ? $this->request->get['order_utm_value'] : '');
        $data[] = $this->getApiKey();

        return $this->getToken($data);
    }

    private function getMd5TokenGetOrderDetail()
    {
        $data = [];
        $data[] = urlencode(isset($this->request->get['customer_ref_id']) ? $this->request->get['customer_ref_id'] : '');
        $data[] = isset($this->request->get['id']) ? $this->request->get['id'] : '';
        $data[] = urlencode(isset($this->request->get['order_code']) ? $this->request->get['order_code'] : '');
        $data[] = $this->getApiKey();

        return $this->getToken($data);
    }

    private function getMd5TokenGetProductsList()
    {
        $data = [];
        $data[] = isset($this->request->get['page']) ? $this->request->get['page'] : '';
        $data[] = isset($this->request->get['limit']) ? $this->request->get['limit'] : '';
        $data[] = urlencode(isset($this->request->get['searchKey']) ? $this->request->get['searchKey'] : '');
        $data[] = isset($this->request->get['searchField']) ? $this->request->get['searchField'] : '';
        $data[] = isset($this->request->get['product_ids']) ? $this->request->get['product_ids'] : '';
        $data[] = isset($this->request->get['except_product_ids']) ? $this->request->get['except_product_ids'] : '';
        $data[] = isset($this->request->get['id']) ? $this->request->get['id'] : '';
        $data[] = isset($this->request->get['product_version_id']) ? $this->request->get['product_version_id'] : '';
        $data[] = $this->getApiKey();

        return $this->getToken($data);
    }

    private function getToken(array $data)
    {
        $token_string = implode('', $data);
        return md5($token_string);
    }

    private function getMd5TokenPost()
    {
        $apiKey = $this->getApiKey();
        $body_date_expert_token = $this->request->post;
        unset($body_date_expert_token['token']);
        $token_string = json_encode($body_date_expert_token);
        $token_string .= $apiKey;
        return md5($token_string);
    }

    private function getMd5TokenVerify()
    {
        $apiKey = $this->getApiKey();
        $token_string = isset($this->request->get['data']) ? $this->request->get['data'] : '';
        if (empty($token_string)) {
            return null;
        }

        $token_string .= $apiKey;

        return md5($token_string);
    }

    private function createNewCustomer($data)
    {
        $this->load->model('account/customer');
        $this->load->model('account/address');

        $customer = [
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'telephone' => $data['telephone'],
            'password' => token(9)
        ];

        $customer_id = $this->model_account_customer->addCustomer($customer);

        $address_data = array(
            'customer_id' => $customer_id,
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'company' => '',
            'city' => $data['city'],
            'district' => $data['district'],
            'wards' => $data['wards'],
            'postcode' => '',
            'address' => $data['address'],
            'phone' => $data['telephone'],
            'default' => true
        );

        $this->model_account_address->addAddressNew($customer_id, $address_data);

        return $customer_id;
    }

    private function getApiKey()
    {
        $this->load->model('setting/setting');

        return $this->model_setting_setting->getSettingValue(self::CONFIG_OPEN_API_KEY);
    }

    public function getMaxQuantityProduct($product, $order)
    {
        $this->load->model('catalog/product');
        if ($product['multi_versions'] == 1) {
            $product_version = $this->model_catalog_product->getProductByVersionId($product['product_id'], $order['product_version_id']);
            if ($product_version && $product['status'] == 1 && $product_version['deleted'] != 1 && $product_version['status'] == 1 && ($product_version['quantity'] > 0 || $product['sale_on_out_stock'] == 1)) {
                return true;
            } else {
                return false;
            }
        }

        if ($product['multi_versions'] == 0 && $product['deleted'] != 1 && $product['status'] == 1 && ($product['product_master_quantity'] > 0 || $product['sale_on_out_stock'] == 1)) {
            return true;
        }

        return false;
    }

    private function getDeliveryConfig()
    {
        /* get delivery methods setting from db */
        $this->load->model('setting/setting');
        $delivery_methods_config = $this->model_setting_setting->getSettingValue('delivery_methods');
        $delivery_methods_config = json_decode($delivery_methods_config, true);

        // test data
        /*$DEBUG = false;
        if ($DEBUG) {
            $delivery_methods_config = json_decode(
                '[
                    {
                        "id": "-1",
                        "name": "Giao hàng tiêu chuẩn",
                        "status": "1",
                        "fee_region": "41,000"
                    },
                    {
                        "id": 1,
                        "name": "Miễn phí vận chuyển Hà Nội",
                        "status": "1",
                        "fee_region": "0",
                        "fee_regions": [
                            {
                                "id": 1,
                                "name": "Mien phi HN",
                                "provinces": [
                                    "Hà Nội"
                                ],
                                "steps": "weight",
                                "weight_steps": [
                                    {
                                        "type": "FROM",
                                        "from": "0",
                                        "to": "1,500",
                                        "price": "0"
                                    },
                                    {
                                        "type": "FROM",
                                        "from": "1,500",
                                        "to": "2,000",
                                        "price": "0"
                                    }
                                ],
                                "price_steps": [
                                    {
                                        "type": "ABOVE",
                                        "from": "0",
                                        "to": "500,000",
                                        "price": "0"
                                    },
                                    {
                                        "type": "ABOVE",
                                        "from": "500,000",
                                        "to": "",
                                        "price": "0"
                                    }
                                ]
                            }
                        ],
                        "fee_cod": {
                            "fixed": {
                                "status": "1",
                                "value": "25,000"
                            },
                            "dynamic": {
                                "status": "0",
                                "value": "13.5"
                            }
                        }
                    }
                ]',
                true
            );
        }*/

        /* filter by status */
        $delivery_methods_config = $this->filterDeliveryConfig($delivery_methods_config, $status = 1);

        /* format number in config */
        $delivery_methods_config = $this->formatNumberForDeliveryConfig($delivery_methods_config);

        return is_array($delivery_methods_config) ? $delivery_methods_config : [];
    }

    /**
     * @param array $data format as:
     * [
     *     'products' => [
     *         [
     *             'id' => 135,
     *             'quantity' => 1
     *         ]
     *     ],
     *     'province' => 89,
     *     'into_money' => 200004,
     *     'delivery_id' => "",
     * ]
     * @return array format as:
     * if error:
     * [
     *     'code' => 201,
     *     'message' => 'Bad request. Missing products (array)!'
     * ]
     *
     * if success:
     * [
     *     'order_info' => [],
     *     'delivery_fees' => [],
     *     'error' => '',
     * ]
     */
    private function calcDeliveryFee(array $data)
    {
        // test data
        /*$data_post = [
            'products' => [
                [
                    'id' => 135,
                    'quantity' => 1
                ]
            ],
            'province' => 89,
            'into_money' => 200004,
            'delivery_id' => "",
        ];*/

        $result = [
            'order_info' => [],
            'delivery_fees' => [],
            'error' => '',
        ];

        /* validate */
        if (!isset($data['products']) || !is_array($data['products']) || empty($data['products'])) {
            return [
                'code' => 201,
                'message' => 'Bad request. Missing products (array)!'
            ];
        }

        /* data address */
        $data['province'] = isset($data['province']) ? $data['province'] : '';
        $data['into_money'] = isset($data['into_money']) ? (float)$data['into_money'] : 0;

        /* orders */
        $this->load->model('catalog/product');
        $products = $data['products'];
        $total_amount = 0;
        $total_into_money = 0;
        $total_weight = 0;
        $error_product = false;

        foreach ($products as &$c_product) {
            if (!isset($c_product['id']) || !isset($c_product['quantity'])) {
                $c_product['is_existing'] = false;
                $error_product = !$c_product['is_existing'];

                continue;
            }

            // find product
            $product_id = $c_product['id'];
            $product = $this->model_catalog_product->getProduct($product_id);
            $c_product['is_existing'] = !empty($product) && is_array($product) && isset($product['product_id']) && !empty($product['product_id']);
            if (!$error_product) {
                $error_product = !$c_product['is_existing'];
            }

            // format
            if (isset($c_product['attribute']) && $c_product['attribute']) {
                if ((int)$c_product['product_version']['price'] == 0) $c_product['product_version']['price'] = $c_product['product_version']['compare_price'];
                $total = (float)$c_product['product_version']['price'] * (int)$c_product['quantity'];
            } else {
                if ((int)$product['price'] == 0) $product['price'] = $product['compare_price_master'];
                $total = (float)$product['price'] * (int)$c_product['quantity'];
            }

            // for overall order
            $total_amount += $c_product['quantity'];
            $total_into_money += $total;
            $total_weight = ($total_amount * $product['weight']);
        }
        unset($c_product);

        $total_into_money = $data['into_money']; // tổng này được truyền lên sau khi trừ khuyễn mãi, giá tính bên trên chưa được tính khuyến mãi

        /* deliver fee */
        $transport_fee = 0;

        /* get satisfied delivery methods due to order info (weight, city) */
        $satisfied_delivery_methods = $this->getSatisfiedDeliveryMethods($data['province'], $total_weight, $total_into_money);

        // filter by transport_id if has
        $delivery_id = isset($data['delivery_id']) ? $data['delivery_id'] : null;
        if ($delivery_id != null) {
            $satisfied_delivery_methods = array_filter($satisfied_delivery_methods, function ($satisfied_delivery_method) use ($delivery_id) {
                return isset($satisfied_delivery_method['id']) &&
                    ($satisfied_delivery_method['id'] == $delivery_id ||
                        $satisfied_delivery_method['id'] == self::DELIVERY_METHOD_DEFAULT // always return default
                    );
            });
        }

        $result['order_info'] = [
            'products' => $products,
            'amount' => $total_amount,
            'into_money' => $total_into_money,
            'into_money_format' => $this->currency->format($this->tax->calculate($total_into_money, 0, $this->config->get('config_tax')), $this->session->data['currency']),
            'total_money' => $this->currency->format($this->tax->calculate($total_into_money + (str_replace(',', '', $transport_fee)), 0, $this->config->get('config_tax')), $this->session->data['currency']),
        ];

        if ($error_product) {
            $result['delivery_fees'] = [];
            $result['error'] = 'Products error!';

            return $result;
        }

        $default_delivery_method = [];
        foreach ($satisfied_delivery_methods as $satisfied_delivery_method) {
            // backup default delivery method
            if ($satisfied_delivery_method['id'] == self::DELIVERY_METHOD_DEFAULT) {
                $default_delivery_method = $satisfied_delivery_method;
            }

            // important: skip default delivery method if has other satisfied methods
            if (count($satisfied_delivery_methods) > 1 && $satisfied_delivery_method['id'] == self::DELIVERY_METHOD_DEFAULT) {
                continue;
            }

            // format fee
            $satisfied_delivery_method['fee_format'] = $this->currency->format($this->tax->calculate($satisfied_delivery_method['fee'], 0, $this->config->get('config_tax')), $this->session->data['currency']);

            $transport_fee = $satisfied_delivery_method['fee'];
            $result['delivery_fees'][] = [
                'delivery' => $satisfied_delivery_method,
                'fee' => $transport_fee,
                'fee_format' => $this->currency->format($this->tax->calculate((str_replace(',', '', $transport_fee)), 0, $this->config->get('config_tax')), $this->session->data['currency']),
            ];
        }

        // MAY NEVER REACH HERE: always return default delivery method if no satisfied delivery method found
        if (empty($result['delivery_fees'])) {
            $result['delivery_fees'][] = $default_delivery_method;
        }

        return $result;
    }

    /**
     * get Satisfied Delivery Methods due to city, weight
     *
     * @param string $city
     * @param string $total_weight
     * @param string $total_order
     * @return array format as
     * [
     *     [
     *         'id' => '1245643122', // transport id,
     *         'name' => 'HA NOI va cac tinh lan can', // $name_transport,
     *         'delivery_method' => [...], // $delivery_method_config,
     *         'fee_region' => [...], // $found_region,
     *         'fee_weight' => 34000, // $fee_price + $value_fee_cod,\
     *         'fee_cod' => 15000, // fee cod
     *         'fee' => 49000, // total fee, = fee_weight + $value_fee_cod
     *         'error' => ''
     *     ],
     *     ...
     * ]
     */
    private function getSatisfiedDeliveryMethods($city = '', $total_weight = '', $total_order = '')
    {
        /* get city name by code */
        $this->load->model('localisation/vietnam_administrative');
        $cityArr = $this->model_localisation_vietnam_administrative->getProvinceByCode($city);
        $city = '';
        if (isset($cityArr['name'])) {
            $city = $cityArr['name'];
        }

        /* get delivery methods setting from db */
        $delivery_methods_config = $this->getDeliveryConfig();

        /* get delivery fee due to city, total_weight, total_order from each delivery method */
        $value_fee_cod = 0;
        $fee_weight = 0;
        $found_delivery_methods = [];
        foreach ($delivery_methods_config as $key => $delivery_method_config) {
            if (!isset($delivery_method_config['fee_regions'])) {
                continue;
            }

            if (!isset($delivery_method_config['status']) || $delivery_method_config['status'] != '1') {
                continue;
            }

            // get delivery fee from each region
            $found_region = false;
            foreach ($delivery_method_config['fee_regions'] as $fee_region_config) {
                // find matched region due to city
                if (!in_array(trim($city), $fee_region_config['provinces'])) {
                    continue;
                }

                if (isset($fee_region_config['steps']) && $fee_region_config['steps'] == 'price') {
                    foreach ($fee_region_config['price_steps'] as $price_step_config) {
                        if (convertWeight($price_step_config['from']) <= $total_order && convertWeight($price_step_config['to']) >= $total_order) {
                            // force remove "," due to json from cb. TODO: DO NOT save value such as 30,000, save 30000 only...
                            $fee_weight = convertWeight($price_step_config['price']); // Gia thuoc tinh thanh
                            $found_region = $fee_region_config;
                            break;
                        }
                    }
                } else {
                    // find matched region due to weight
                    foreach ($fee_region_config['weight_steps'] as $weight_step_config) {
                        if (convertWeight($weight_step_config['from']) <= $total_weight && convertWeight($weight_step_config['to']) >= $total_weight) {
                            // force remove "," due to json from cb. TODO: DO NOT save value such as 30,000, save 30000 only...
                            $fee_weight = convertWeight($weight_step_config['price']); // Gia thuoc tinh thanh
                            $found_region = $fee_region_config;
                            break;
                        }
                    }
                }


                if ($found_region) {
                    break;
                }
            }

            // skip to next delivery_method_config
            if (!$found_region) {
                continue;
            }

            // more fee about COD (fixed or per order total revenue)
            foreach ($delivery_method_config['fee_cod'] as $key_cod => $fee_cod) {
                $arr_fee_status = ($delivery_method_config['fee_cod'][$key_cod]);
                if ($arr_fee_status['status'] == 1) {
                    // force remove "," due to json from cb. TODO: DO NOT save value such as 30,000, save 30000 only...
                    $arr_fee_status = str_replace(',', '', $arr_fee_status['value']);
                    if ($key_cod === 'dynamic') {
                        if ($total_order != 0) {
                            $value_fee_cod = ($arr_fee_status / 100) * $total_order;
                        } else {
                            $value_fee_cod = 0;
                        }
                    } elseif ($key_cod === 'fixed') {
                        $value_fee_cod = $arr_fee_status;
                    }
                }
            }

            // add to list
            $found_delivery_methods[] = [
                'id' => $delivery_method_config['id'],
                'name' => $delivery_method_config['name'],
                'delivery_method' => $delivery_method_config,
                'fee_region' => $found_region,
                'fee_weight' => $fee_weight,
                'fee_cod' => $value_fee_cod,
                'fee' => $fee_weight + $value_fee_cod, // total fee
                'error' => '',
            ];
        }

        $ALWAYS_RETURN_DEFAULT_DELIVERY_METHOD = true;
        if ($ALWAYS_RETURN_DEFAULT_DELIVERY_METHOD || empty($found_delivery_methods)) {
            $delivery_method_config_default = $delivery_methods_config[0];
            $found_delivery_methods[] = [
                'id' => $delivery_method_config_default['id'],
                'name' => $delivery_method_config_default['name'],
                'delivery_method' => $delivery_method_config_default,
                'fee_region' => $delivery_method_config_default['fee_region'],
                'fee_weight' => 0,
                'fee_cod' => 0,
                'fee' => $delivery_method_config_default['fee_region'], // total fee
                'error' => '',
            ];
        }

        return $found_delivery_methods;
    }

    /**
     * check if support api version
     *
     * @param string $ver
     * @return bool
     */
    private function isSupportVersion($ver)
    {
        return in_array($ver, self::$SUPPORTED_VERSIONS);
    }

    /**
     * @param array $delivery_methods_config
     * @param int|null $status 0 | 1 | null (for both 0 and 1)
     * @return mixed
     */
    private function filterDeliveryConfig(array $delivery_methods_config, $status = 1)
    {
        if (is_null($status)) {
            return $delivery_methods_config;
        }

        /* filter by status */
        $result = array_filter($delivery_methods_config, function ($dmc) use ($status) {
            return isset($dmc['status']) && $dmc['status'] == $status;
        });

        /* force array */
        $result = array_values($result);

        return $result;
    }

    /**
     * @param array $delivery_methods_config
     * @return array
     */
    private function formatNumberForDeliveryConfig(array $delivery_methods_config)
    {
        // force format number. TODO: no need if save to db already number...
        $NEED_FORMAT_NUMBER = true;
        if ($NEED_FORMAT_NUMBER) {
            foreach ($delivery_methods_config as &$dmc) {
                // fee_region
                if (isset($dmc['fee_region'])) {
                    $dmc['fee_region'] = extract_number($dmc['fee_region']);
                }

                // fee_regions
                if (isset($dmc['fee_regions']) && is_array($dmc['fee_regions'])) {
                    foreach ($dmc['fee_regions'] as &$fee_region) {
                        // weight steps
                        if (isset($fee_region['weight_steps']) && is_array($fee_region['weight_steps'])) {
                            foreach ($fee_region['weight_steps'] as &$wst) {
                                if (isset($wst['from'])) {
                                    $wst['from'] = extract_number($wst['from']);
                                }

                                if (isset($wst['to'])) {
                                    $wst['to'] = extract_number($wst['to']);
                                }

                                if (isset($wst['price'])) {
                                    $wst['price'] = extract_number($wst['price']);
                                }
                            }
                            unset($wst);
                        }

                        // price steps
                        if (isset($fee_region['price_steps']) && is_array($fee_region['price_steps'])) {
                            foreach ($fee_region['price_steps'] as &$pst) {
                                if (isset($pst['from'])) {
                                    $pst['from'] = extract_number($pst['from']);
                                }

                                if (isset($pst['to'])) {
                                    $pst['to'] = extract_number($pst['to']);
                                }

                                if (isset($pst['price'])) {
                                    $pst['price'] = extract_number($pst['price']);
                                }
                            }
                            unset($pst);
                        }
                    }
                    unset($fee_region);
                }

                // fee_cod
                if (isset($dmc['fee_cod']['fixed']['value'])) {
                    $dmc['fee_cod']['fixed']['value'] = extract_number($dmc['fee_cod']['fixed']['value']);
                }

                if (isset($dmc['fee_cod']['dynamic']['value'])) {
                    $dmc['fee_cod']['dynamic']['value'] = extract_number($dmc['fee_cod']['dynamic']['value']);
                }
            }
            unset($dmc);
        }

        return $delivery_methods_config;
    }
}