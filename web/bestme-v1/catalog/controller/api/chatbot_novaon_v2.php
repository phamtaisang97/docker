<?php

class ControllerApiChatbotNovaonV2 extends Controller
{
    use AUTO_CREATE_RECEIPT_VOUCHER_UTIL_TRAIT;
    use RabbitMq_Trait;

    const ORDER_STATUS_DRAFT = 6; // DO NOT HARD CODE HERE. TODO: get from Catalog/Order
    const CONFIG_CHATBOT_NOVAON_API_KEY = 'config_chatbot_novaon';

    /*
     * first implement chatbot_novaon_v2
     */
    const VER_1_0 = '1.0';

    /*
     * get products with new format
     */
    const VER_1_1 = '1.1';

    private static $SUPPORTED_VERSIONS = [
        self::VER_1_0,
        self::VER_1_1,
    ];

    const DELIVERY_METHOD_DEFAULT = -1;
    const DELIVERY_METHOD_CUSTOM = -2;
    const DELIVERY_METHOD_FREE = -3;

    /*
     * Staff manager
     */
    const SUPPORTED_PERMISSIONS = [
        'home_permission',
        'order_permission',
        'product_permission',
        'customer_permission',
        'setting_permission',
        'interface_permission',
        'menu_permission',
        'content_permission',
        'domain_permission',
    ];

    /* dashboard */
    const REVENUE_ID = 1;

    /**
     * get products list
     * get product detail
     *
     * Notice: support param "&ver=xxx" for new version if has.
     * If no param "&ver=xxx" provided, default ver=1.0
     */
    public function products()
    {
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            // get post data from php://input
            // notice: could not get directly from $this->request->post or $_POST, TODO: why?...
            $this->request->post = json_decode(file_get_contents('php://input'), true);

            if (!isset($this->request->post['data']['product_id']) || empty($this->request->post['data']['product_id'])) {
                return $this->createProduct();
            } else {
                if (isset($this->request->post['data']['action']) && $this->request->post['data']['action'] == 'delete') {
                    return $this->deleteProduct();
                }
                return $this->editProduct();
            }
        }

        if (($this->request->server['REQUEST_METHOD'] == 'GET')) {
            // check parameter
            if (!$this->validateTokenGetProducts()) {
                $json = [
                    'code' => 400,
                    'message' => 'invalid token'
                ];

                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json));

                return;
            }

            /* get and validate version */
            $ver = $this->getValueFromRequest('GET', 'ver');
            if (empty($ver)) {
                $ver = self::VER_1_0;
            }

            if (!$this->isSupportVersion($ver)) {
                $json = [
                    'code' => 400,
                    'message' => 'not supported version. Current supported versions: ' . implode(' | ', self::$SUPPORTED_VERSIONS)
                ];

                $this->responseJson($json);
                return;
            }

            /* do getting data */
            $this->load->model('api/chatbox_novaon_v2');
            switch ($ver) {
                case self::VER_1_0:
                    $json = $this->model_api_chatbox_novaon_v2->getProductsListData_v1_0($this->request->get);
                    break;

                case self::VER_1_1:
                    $json = $this->model_api_chatbox_novaon_v2->getProductsListData_v1_1($this->request->get);
                    break;

                default:
                    // not reach this case if $this->isSupportVersion($ver) works properly!
                    $json = [
                        'code' => 400,
                        'message' => 'not supported version. Current supported versions: ' . implode(' | ', self::$SUPPORTED_VERSIONS)
                    ];
            }

            $this->responseJson($json);
        }
        return;
    }

    /**
     * create product
     */
    private function createProduct()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->log->write('createProduct(): resp: ' . 'Bad request. Use POST only!');

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        // get post data from php://input
        // notice: could not get directly from $this->request->post or $_POST, TODO: why?...
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('[chatbot_novaon_v2.0] product(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        $validate_result = $this->validateProductPostData();
        if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
            $error = is_array($validate_result) ? $validate_result : [];
            $this->responseJson($error);
            return;
        }

        try {
            /*build data to add product*/
            $data_to_save = $this->request->post['data'];

            // category
            $data_to_save['category'] = [];
            $category_names = isset($this->request->post['data']['category']) ? $this->request->post['data']['category'] : [];
            $this->load->model('catalog/category');
            $categories = $this->model_catalog_category->getCategoriesByNames($category_names);
            $category_name_to_id = [];
            foreach ($categories as $cat) {
                if (!isset($cat['category_id']) || !isset($cat['name'])) {
                    continue;
                }

                $category_name_to_id[mb_strtoupper($cat['name'])] = $cat['category_id'];
            }

            foreach ($category_names as $category_name) {
                if (array_key_exists(mb_strtoupper($category_name), $category_name_to_id)) {
                    $data_to_save['category'][] = $category_name_to_id[mb_strtoupper($category_name)];
                } else {
                    $data_to_save['category'][] = "add_new_{$category_name}";
                }
            }

            // manufacturer
            $data_to_save['manufacturer'] = '';
            $manufacturer_name = isset($this->request->post['data']['manufacturer']) ? $this->request->post['data']['manufacturer'] : '';
            $this->load->model('catalog/manufacturer');
            $manufacturer = $this->model_catalog_manufacturer->getManufacturerByName($filter = ['name' => $manufacturer_name]);
            if (isset($manufacturer['manufacturer_id'])) {
                $data_to_save['manufacturer'] = $manufacturer['manufacturer_id'];
            } else {
                $data_to_save['manufacturer'] = "add_new_{$manufacturer_name}";
            }

            // collection
            $data_to_save['collection_list'] = [];
            $collection_titles = isset($this->request->post['data']['collection_list']) ? $this->request->post['data']['collection_list'] : [];
            $this->load->model('catalog/collection');
            $collections = $this->model_catalog_collection->getCollectionsByTitles($collection_titles);
            $collection_title_to_id = [];
            foreach ($collections as $collection) {
                if (!isset($collection['collection_id']) || !isset($collection['title'])) {
                    continue;
                }

                $collection_title_to_id[mb_strtoupper($collection['title'])] = $collection['collection_id'];
            }

            foreach ($collection_titles as $collection_title) {
                if (array_key_exists(mb_strtoupper($collection_title), $collection_title_to_id)) {
                    $data_to_save['collection_list'][] = $collection_title_to_id[mb_strtoupper($collection_title)];
                } else {
                    $data_to_save['collection_list'][] = "add_new_{$collection_title}";
                }
            }

            // tag
            $data_to_save['tag_list'] = [];
            $tag_values = isset($this->request->post['data']['tag_list']) ? $this->request->post['data']['tag_list'] : [];
            $this->load->model('catalog/tag');
            $tags = $this->model_catalog_tag->getTagsByValues($tag_values);
            $tag_value_to_id = [];
            foreach ($tags as $tag) {
                if (!isset($tag['tag_id']) || !isset($tag['value'])) {
                    continue;
                }

                $tag_value_to_id[mb_strtoupper($tag['value'])] = $tag['tag_id'];
            }

            foreach ($tag_values as $tag_value) {
                if (array_key_exists(mb_strtoupper($tag_value), $tag_value_to_id)) {
                    $data_to_save['tag_list'][] = $tag_value_to_id[mb_strtoupper($tag_value)];
                } else {
                    $data_to_save['tag_list'][] = "add_new_{$tag_value}";
                }
            }

            /*add product*/
            $this->load->model('catalog/product');
            $product_id = $this->model_catalog_product->addProduct($data_to_save);
            $error = [
                'code' => 0,
                'message' => 'PRODUCT_CREATED',
                'product_id' => $product_id
            ];
        } catch (Exception $e) {
            $error = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->log->write('createProduct(): final resp: ' . json_encode($error));

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($error));

    }

    /**
     * edit product
     */
    private function editProduct()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $error = array();

        // get post data from php://input
        // notice: could not get directly from $this->request->post or $_POST, TODO: why?...
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('[chatbot_novaon_v2.0] product(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        // check if has product_id
        if (!isset($this->request->post['data']['product_id']) || empty($this->request->post['data']['product_id'])) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Missing "product_id"!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $product_id = $this->request->post['data']['product_id'];
        // check exist product_id in database
        $this->load->model('catalog/product');
        $exist = $this->model_catalog_product->getProduct($product_id);
        if (!$exist) {
            $error = [
                'code' => 506,
                'message' => 'product (with product_id) does not existed'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }
        $valid_product = $this->validateProductPostData();
        if ($valid_product) {
            $error = $valid_product;

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        try {
            // edit order

            $data_to_save = $this->request->post['data'];
            $data_to_save['seo_title'] = '';
            $data_to_save['seo_desciption'] = '';
            $product_id = $this->model_catalog_product->editProduct($product_id, $data_to_save);
            $error = [
                'code' => 0,
                'message' => 'product edited successfully'
            ];
        } catch (Exception $e) {
            $error = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->log->write('createProduct(): final resp: ' . json_encode($error));

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($error));
    }

    private function deleteProduct()
    {
        $error = $this->validateProductDeleteData();
        if ($error) {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        try {
            $product_id = $this->request->post['data']['product_id'];
            $this->load->model('catalog/product');
            $this->model_catalog_product->deleteProduct($product_id);

            $error = [
                'code' => 0,
                'message' => 'product deleted successfully'
            ];
        } catch (Exception $e) {
            $error = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($error));
    }

    /**
     * get orders list
     * get order detail
     * create order
     * edit order
     */
    public function orders()
    {
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            // get post data from php://input
            // notice: could not get directly from $this->request->post or $_POST, TODO: why?...
            $this->request->post = json_decode(file_get_contents('php://input'), true);

            if (!isset($this->request->post['data']['order_id']) || empty($this->request->post['data']['order_id'])) {
                return $this->createOrder();
            } else {
                if (isset($this->request->post['data']['order_id']) && isset($this->request->post['data']['order_status_id'])) {
                    return $this->updateOrderStatus();
                } else {
                    return $this->editOrder();
                }
            }
        }

        if (($this->request->server['REQUEST_METHOD'] == 'GET')) {
            if (!isset($this->request->get['id'])) {
                return $this->getOrdersList();
            } else {
                return $this->getOrderDetail();
            }
        }

        $result = [
            'code' => 101,
            'message' => 'customer info invalid'
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    public function verify()
    {
        $this->load->language('api/chatbot_novaon');
        $json = [
            'code' => 1,
            'message' => 'credential (api key) is verified'
        ];

        // check parameter
        if (!$this->validateTokenVerify()) {
            $json = [
                'code' => 400,
                'message' => 'invalid token'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function provinces()
    {
        $this->load->model('localisation/vietnam_administrative');
        $provinces = $this->model_localisation_vietnam_administrative->getProvinces();

        // maintain key
        $provinces = array_values($provinces);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($provinces));
    }

    public function districts()
    {
        $provinceCode = '';
        if (isset($this->request->get['province'])) {
            $provinceCode = $this->request->get['province'];
        }

        if (!empty($provinceCode)) {
            $this->load->model('localisation/vietnam_administrative');
            $districts = $this->model_localisation_vietnam_administrative->getDistrictsByProvinceCode($provinceCode);

            // maintain key
            $districts = array_values($districts);
        } else {
            $districts = [];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($districts));
    }

    public function wards()
    {
        $districtCode = '';
        if (isset($this->request->get['district'])) {
            $districtCode = $this->request->get['district'];
        }

        if (!empty($districtCode)) {
            $this->load->model('localisation/vietnam_administrative');
            $wards = $this->model_localisation_vietnam_administrative->getWardsByDistrictCode($districtCode);

            // maintain key
            $wards = array_values($wards);
        } else {
            $wards = [];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($wards));
    }

    public function deliveries()
    {
        $delivery_methods_config = $this->getDeliveryConfig();

        // TODO: pagination...

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($delivery_methods_config));
    }

    public function delivery_fee()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        // get post data from php://input
        // notice: could not get directly from $this->request->post or $_POST, TODO: why?...
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('[chatbot_novaon_v2.0] delivery_fee(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        $data_post = $this->request->post;

        // test data
        /*$data_post = [
            'products' => [
                [
                    'id' => 135,
                    'quantity' => 1
                ]
            ],
            'province' => 89,
            'into_money' => 200004,
            'delivery_id' => "",
        ];*/

        /* calc delivery fee */
        $result = $this->calcDeliveryFee($data_post);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    /**
     * get staffs list
     * get staff detail
     * create staff
     * edit staff
     * delete staff
     */
    public function staffs()
    {
        if ('GET' == $this->request->server['REQUEST_METHOD']) {
            if (!isset($this->request->get['id'])) {
                $this->getStaffsList();
            } else {
                $this->getStaffDetail();
            }
        } else {
            if ('POST' == $this->request->server['REQUEST_METHOD']) {
                // create staff
                $this->createStaff();
            } elseif ('PUT' == $this->request->server['REQUEST_METHOD']) {
                // edit staff
//                $this->editStaff();
            } elseif ('DELETE' == $this->request->server['REQUEST_METHOD']) {
                // delete staff
//                $this->deleteStaff();
            }
        }
    }

    /**
     * get categories list
     * get category detail
     * create category
     * edit category
     * delete category
     */
    public function categories()
    {
        $NOT_SUPPORTED = true;
        if ($NOT_SUPPORTED) {
            $result = [
                'code' => 404,
                'message' => 'not yet supported api categories'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($result));

            return;
        }

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            // get post data from php://input
            // notice: could not get directly from $this->request->post or $_POST, TODO: why?...
            $this->request->post = json_decode(file_get_contents('php://input'), true);

            if (!isset($this->request->post['data']['category_id']) || empty($this->request->post['data']['category_id'])) {
                return $this->createCategory();
            } else {
                if (isset($this->request->post['data']['action']) && $this->request->post['data']['action'] == "delete") {
                    return $this->deleteCategory();
                }

                return $this->editCategory();
            }
        }

        if (($this->request->server['REQUEST_METHOD'] == 'GET')) {
            if (!isset($this->request->get['id'])) {
                return $this->getCategoriesList();
            } else {
                return $this->getCategoryDetail();
            }
        }

        $result = [
            'code' => 101,
            'message' => 'category info invalid'
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    /**
     * get manufacturers list
     * get manufacturer detail
     * create manufacturer
     * edit manufacturer
     * delete manufacturer
     */
    public function manufacturers()
    {
        $NOT_SUPPORTED = true;
        if ($NOT_SUPPORTED) {
            $result = [
                'code' => 404,
                'message' => 'not yet supported api manufacturers'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($result));

            return;
        }

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            // get post data from php://input
            // notice: could not get directly from $this->request->post or $_POST, TODO: why?...
            $this->request->post = json_decode(file_get_contents('php://input'), true);

            if (!isset($this->request->post['data']['manufacturer_id']) || empty($this->request->post['data']['manufacturer_id'])) {
                return $this->createManufacturer();
            } else {
                if (isset($this->request->post['data']['action']) && $this->request->post['data']['action'] == "delete") {
                    return $this->deleteManufacturer();
                }

                return $this->editManufacturer();
            }
        }

        if (($this->request->server['REQUEST_METHOD'] == 'GET')) {
            if (!isset($this->request->get['id'])) {
                return $this->getManufacturersList();
            } else {
                return $this->getManufacturerDetail();
            }
        }

        $result = [
            'code' => 101,
            'message' => 'manufacturer info invalid'
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    /* === local functions === */

    /**
     * createOrder
     *
     * @return array|void
     */
    private function createOrder()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->log->write('createOrder(): resp: ' . 'Bad request. Use POST only!');

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $error = [];

        // get post data from php://input
        // notice: could not get directly from $this->request->post or $_POST, TODO: why?...
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('[chatbot_novaon_v2.0] orders(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        // sample data
        /*$this->request->post = [
            'token' => 'd1133ac9eddba6d491219f070affc5d2',
            'data' => [
                'customer' => [
                    'name' => 'Nguyen Thuy Linh',
                    'phone_number' => '0987654321',
                    'email' => 'nguyenthuylinh@gmail.com',
                    'delivery_addr' => 'So 1 Ngo 2 Duy Tan',
                    'ward' => 'Dich Vong Hau',
                    'district' => 'Cau Giay',
                    'city' => 'Ha Noi',
                    'note' => 'Goi hang can than va du so luong nhe shop. Thanks!'
                ],
                "status" => 6,
                "customer_ref_id" => "",
                "order_id" => null,
                "tags" => "tag1, tag2",
                "utm" => [
                    {
                        "key" => "campaign",
                        "value" => "Campaign 1"
                    },
                    {
                        "key" => "campaign",
                        "value" => "Campaign 2"
                    },
                    ...
                ],
                'products' => [
                    [
                        'id' => 83,
                        'quantity' => 2,
                        'product_version_id' => null
                    ],
                    [
                        'id' => 104,
                        'quantity' => 3,
                        'product_version_id' => 1
                    ]
                ],
                'vouchers' => [
                    'voucher_code' => 'ABCDEFGH99',
                    'voucher_type' => 1, //1:  percent / 2: amount
                    'amount' => 20000,
                    'products' => [
                        [
                            'id' => 83,
                            'product_version_id' => null
                        ],
                        [
                            'id' => 104,
                            'product_version_id' => 1
                        ]
                    ],
                    'start_at' => "2021-04-22 13:41:00",
                    'end_at' => "2021-04-22 13:41:59",
                    'user_create_id' => "10"
                ],
                'payment_trans' => "-2",
                'payment_trans_custom_name' => "Shop tự vận chuyển",
                'payment_trans_custom_fee' => 25000
            ]
        ];*/

        $valid_orders = $this->validateOrdersPostData();
        if ($valid_orders) {
            $error = is_array($valid_orders) ? $valid_orders : [];

            $this->log->write('createOrder(): validateOrdersPostData resp: ' . json_encode($error));

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        try {
            // add order
            $this->load->model('account/address');
            $this->load->model('customer/customer');
            $this->load->model('account/customer');
            $this->load->model('localisation/vietnam_administrative');
            // pass data post
            $data['telephone'] = $this->request->post['data']['customer']['phone_number'];

            // extract first name, last name
            $full_name = isset($this->request->post['data']['customer']['name']) ? $this->request->post['data']['customer']['name'] : '';
            $full_name = trim($full_name);
            $first_and_last_name = explode(" ", $full_name);
            $first_name = array_pop($first_and_last_name);
            $last_name = implode(" ", $first_and_last_name);
            $data['fullname'] = $full_name;
            $data['lastname'] = trim($last_name);
            $data['firstname'] = trim($first_name);

            $data['city'] = isset($this->request->post['data']['customer']['city']) ? $this->request->post['data']['customer']['city'] : '';
            $data['email'] = isset($this->request->post['data']['customer']['email']) ? $this->request->post['data']['customer']['email'] : '';
            $data['district'] = isset($this->request->post['data']['customer']['district']) ? $this->request->post['data']['customer']['district'] : '';
            $data['wards'] = isset($this->request->post['data']['customer']['ward']) ? $this->request->post['data']['customer']['ward'] : '';
            $data['address'] = isset($this->request->post['data']['customer']['delivery_addr']) ? $this->request->post['data']['customer']['delivery_addr'] : '';
            // 20210429: sp social
            $data['subscriber_id'] = isset($this->request->post['data']['customer']['subscriber_id']) ? $this->request->post['data']['customer']['subscriber_id'] : '';

            $customer = $this->model_customer_customer->getCustomerByTelephone($data['telephone']);
            if ($customer) {
                $customer_id = $customer['customer_id'];
            } else {
                $customer_id = $this->createNewCustomer($data);
            }
            $data['customer_id'] = $customer_id;

            $data['customer_group_id'] = $this->config->get('config_customer_group_id');
            $data['order_payment'] = 1;

            $order_status = isset($this->request->post['data']['status']) ? $this->request->post['data']['status'] : self::ORDER_STATUS_DRAFT;
            $data['order_transfer'] = $order_status;

            $data['order_note'] = isset($this->request->post['data']['customer']['note']) ? $this->request->post['data']['customer']['note'] : '';
            $data['payment_firstname'] = $data['firstname'];
            $data['payment_lastname'] = $data['lastname'];
            $data['payment_company'] = '';
            $data['payment_address_1'] = $data['address'];
            $data['payment_address_2'] = sprintf('%s, %s, %s, %s', $data['address'], $data['wards'], $data['district'], $data['city']);
            $data['payment_city'] = $data['city'];
            $data['payment_postcode'] = '';
            $data['payment_country_id'] = '';
            $data['payment_zone_id'] = '';
            $data['payment_custom_field'] = array();
            $data['payment_code'] = '';
            $data['payment_status'] = getValueByKey($this->request->post['data'], 'payment_status') == 1 ? 1 : 0;
            $data['shipping_firstname'] = $data['firstname'];
            $data['shipping_lastname'] = $data['lastname'];
            $data['shipping_company'] = '';
            $data['shipping_address_1'] = $data['address'];
            $data['shipping_address_2'] = sprintf('%s, %s, %s, %s', $data['address'], $data['wards'], $data['district'], $data['city']);
            $data['shipping_city'] = $data['city'];
            $data['shipping_postcode'] = '';
            $data['shipping_country_id'] = '';
            $data['shipping_zone_id'] = '';
            $data['shipping_custom_field'] = array();
            $data['shipping_method'] = '';
            $data['shipping_method_value'] = ''; // phương thức vẫn chuyển mặc định để rõng
            $data['shipping_code'] = '';
            $data['shipping_ward_code'] = $data['wards'];
            $data['shipping_district_code'] = $data['district'];
            $data['shipping_province_code'] = $data['city'];

            $data['order_products'] = array();
            $data['order_vouchers'] = array();
            $data['order_totals'] = array();

            $data['order_status_id'] = $order_status;
            $data['order_status'] = $order_status;  /////Đang xử lý; @@
            $data['currency_code'] = $this->config->get('config_currency');
            $data['from_domain'] = ''; // auto from main domain. TODO: from "chatbot"?...

            $data['coupon'] = '';
            $data['voucher'] = '';
            $data['reward'] = '';

            $data['tags'] = isset($this->request->post['data']['tags']) ? $this->request->post['data']['tags'] : '';

            // for chatbot only
            $data['customer_ref_id'] = isset($this->request->post['data']['customer_ref_id']) ? $this->request->post['data']['customer_ref_id'] : '';

            // 20200420: support order utm
            $data['utm'] = isset($this->request->post['data']['utm']) ? $this->request->post['data']['utm'] : [];

            // 20210427: support campaign_voucher
            $data['voucher_product'] = isset($this->request->post['data']['voucher_product']) ? $this->request->post['data']['voucher_product'] : [];

            if (isset($data['voucher_product']) && !empty($data['voucher_product'])) {
                if (!isset($data['voucher_product']['voucher_code']) || empty($data['voucher_product']['voucher_code'])) {
                    $error = [
                        'code' => 102,
                        'message' => 'Voucher product invalid. Key "voucher_code" is not null'
                    ];
                }
            }

            // 20210429: sp social
            $data['campaign_id'] = isset($this->request->post['data']['campaign_id']) ? $this->request->post['data']['campaign_id'] : "";
            $data['channel_id'] = isset($this->request->post['data']['channel_id']) ? $this->request->post['data']['channel_id'] : "";

            $data['payment_method'] = isset($this->request->post['data']['payment_method']) ? $this->request->post['data']['payment_method'] : "";
//            if (!isset($data['payment_method']) || empty($data['payment_method'])) {
//                $error = [
//                    'code' => 102,
//                    'message' => 'payment_method invalid. Key "payment_method" is not null'
//                ];
//            }

            $products = $this->request->post['data']['products'];
            // merge products quantity if same id
            $grouped_products = [];
            foreach ($products as $product) {
                if (!isset($product['id']) || !isset($product['quantity']) || (int)$product['quantity'] == 0) {
                    continue;
                }

                $key = $product['id'];
                if (isset($product['product_version_id']) && !empty($product['product_version_id'])) {
                    $key = $product['product_version_id'];
                }

                // new if not exist
                if (!array_key_exists($key, $grouped_products)) {
                    $grouped_products[$key] = $product;
                    continue;
                }

                // or sum quantity if existed
                $grouped_products[$key]['quantity'] += $product['quantity'];
            }

            $this->load->model('catalog/product');
            $this->load->model('api/chatbox_novaon_v2');
            $total_amount = 0;
            $total_into_money = 0;
            $not_available_products = [];
            foreach ($grouped_products as &$product) {
                if (!isset($product['id']) || !isset($product['quantity']) || (int)$product['quantity'] == 0) {
                    $error = [
                        'code' => 102,
                        'message' => 'products info invalid. Missing key either "id" or "quantity"'
                    ];
                    break;
                }

                // get product info same as api get product detail
                $product_info = $this->model_api_chatbox_novaon_v2->getProductsListData_v1_1([
                    'id' => $product['id'],
                    'product_version_id' => isset($product['product_version_id']) ? $product['product_version_id'] : ''
                ], $all_statuses = true, $more_return_fields = ['status', 'deleted', 'quantity_in_stock', 'sale_on_out_of_stock']);

                // Notice: validate production not available (out_of_stock, deleted, disabled...) if draft for processing
                if (empty($order_status) || in_array($order_status, [
                        ModelSaleOrder::ORDER_STATUS_ID_DRAFT,
                        ModelSaleOrder::ORDER_STATUS_ID_PROCESSING
                    ])
                ) {
                    if (!$product_info ||
                        (isset($product_info['deleted']) && !empty($product_info['deleted'])) ||
                        (isset($product_info['status']) && $product_info['status'] != 1)
                    ) {
                        $product_info['error_code'] = 1042;
                        unset($product_info['status'], $product_info['deleted'], $product_info['quantity_in_stock'], $product_info['sale_on_out_of_stock']);
                        //$product_info['quantity'] = $product['quantity'];
                        $not_available_products[] = $product_info;
                        continue;
                    }

                    if (isset($product_info['quantity_in_stock']) && $product_info['quantity_in_stock'] <= 0 &&
                        isset($product_info['sale_on_out_of_stock']) && $product_info['sale_on_out_of_stock'] != 1
                    ) {
                        $product_info['error_code'] = 1041;
                        unset($product_info['status'], $product_info['deleted'], $product_info['quantity_in_stock'], $product_info['sale_on_out_of_stock']);
                        //$product_info['quantity'] = $product['quantity'];
                        $not_available_products[] = $product_info;
                        continue;
                    }
                }

                // TODO: use above product info?
                $product_detail = $this->model_catalog_product->getProduct($product['id']);

                $product_quantity_max = $this->getMaxQuantityProduct($product_detail, $product);
                $product_version = [];
                if (!$product_detail || !isset($product_detail['product_id']) || ($product_detail['multi_versions'] == 1 && count($product_version = $this->model_catalog_product->getProductByVersionId($product_detail['product_id'], $product['product_version_id'])) == 0)) {
                    $error = [
                        'code' => 201,
                        'message' => 'error product order'
                    ];
                    break;
                }

                if (!$product_quantity_max) {
                    /*$error = [
                        'code' => 201,
                        'message' => 'error product order'
                    ];
                    break;*/
                }

                $product['name'] = $product_detail['name'];
                $product['model'] = $product_detail['model'];
                $product['price'] = $product_detail['price'];
                $product['product_id'] = $product['id'];
                if (isset($product['product_version_id']) && !empty($product['product_version_id'])) {
                    if (!isset($product_version['price']) || (int)$product_version['price'] == 0) {
                        $product_version['price'] = isset($product_version['compare_price']) ? (int)$product_version['compare_price'] : null;
                    }
                    $total = (float)$product_version['price'] * (int)$product['quantity'];
                    $product['product_version'] = $product_version;
                } else {
                    if ((int)$product_detail['price'] == 0) $product_detail['price'] = $product_detail['compare_price_master'];
                    $total = (float)$product_detail['price'] * (int)$product['quantity'];
                }
                $total_amount += $product['quantity'];
                $total_into_money += $total;
            }
            unset($product);

            if (!empty($not_available_products)) {
                $error = [
                    "code" => 104,
                    "message" => "create order failed: products are out of stock or do not existed",
                    "products" => $not_available_products
                ];
            }

            $data['shipping_fee'] = 0;
            $data['shipping_method'] = '';
            $data['shipping_method_value'] = '';
            // 20200612: support shipping fee
            $delivery_id = isset($this->request->post['data']['payment_trans']) ? $this->request->post['data']['payment_trans'] : null;
            switch ($delivery_id) {
                case self::DELIVERY_METHOD_CUSTOM:
                    if (!isset($this->request->post['data']['payment_trans_custom_name']) ||
                        !isset($this->request->post['data']['payment_trans_custom_fee'])
                    ) {
                        $error = [
                            "code" => 400,
                            "message" => "create order failed: missing neither payment_trans custom_name nor custom_fee"
                        ];

                        break;
                    }

                    $data['shipping_fee'] = (int)$this->request->post['data']['payment_trans_custom_fee'];
                    $data['shipping_method'] = $this->request->post['data']['payment_trans_custom_name'];
                    $data['shipping_method_value'] = self::DELIVERY_METHOD_CUSTOM;

                    break;

                case self::DELIVERY_METHOD_FREE:
                    $data['shipping_fee'] = 0;
                    $data['shipping_method'] = 'Miễn phí vận chuyển';
                    $data['shipping_method_value'] = self::DELIVERY_METHOD_FREE;

                    break;

                default:
                    if (is_null($delivery_id)) {
                        break;
                    }

                    // get shipping method name
                    $deliveries = $this->getDeliveryConfig();
                    foreach ($deliveries as $delivery) {
                        if (!isset($delivery['id']) || !isset($delivery['name']) || $delivery['id'] != $delivery_id) {
                            continue;
                        }

                        if ($delivery['id'] == $delivery_id) {
                            $data['shipping_method'] = $delivery['name'];
                            $data['shipping_method_value'] = $delivery['id'];
                            break;
                        }
                    }

                    // calc delivery fee
                    $data_calc_delivery_fee = [
                        'products' => [
                            /*[
                                'id' => 135,
                                'quantity' => 1
                            ]*/
                        ],
                        'province' => $data['city'],
                        'into_money' => $total_into_money,
                        'delivery_id' => $delivery_id,
                    ];

                    foreach ($products as $product) {
                        if (!isset($product['id']) || !isset($product['quantity']) || (int)$product['quantity'] == 0) {
                            continue;
                        }

                        $data_calc_delivery_fee['products'][] = [
                            'id' => $product['id'],
                            'quantity' => $product['quantity']
                        ];
                    }

                    $delivery_fees = $this->calcDeliveryFee($data_calc_delivery_fee);

                    // error
                    if (isset($delivery_fees['error']) && !empty($delivery_fees['error'])) {
                        $error = [
                            "code" => 104,
                            "message" => "create order failed: calculate delivery fee failed (may products are out of stock or do not existed)",
                            "products" => $not_available_products
                        ];

                        break;
                    }

                    // success
                    if (isset($delivery_fees['delivery_fees'][0]['fee'])) {
                        $data['shipping_fee'] = (int)$delivery_fees['delivery_fees'][0]['fee'];
                        $_delivery = getValueByKey($delivery_fees['delivery_fees'][0], 'delivery', [
                            'id' => '',
                            'name' => '',
                        ]);
                        $data['shipping_method'] = $_delivery['name'];
                        $data['shipping_method_value'] = $_delivery['id'];
                    }

                    break;
            }

            $data['total'] = isset($this->request->post['data']['total']) ? $this->request->post['data']['total'] : 0; //$data['shipping_fee'] + $total_into_money;
            if (!$error) {
                $this->load->model('sale/order');
                $order_id = $this->model_sale_order->addOrderCommon($data, $data['customer_id']);
                // confix Prefix, suffix in order_code
                $this->load->model('setting/setting');

                if (isset($data['campaign_id']) && !empty($data['campaign_id'])) {
                    $this->load->model('sale/order_campaign');
                    $this->model_sale_order_campaign->addOrderCampaign($order_id, $data['campaign_id']);
                }

                if (isset($data['channel_id']) && !empty($data['channel_id'])) {
                    $this->load->model('sale/order_channel');
                    $this->model_sale_order_channel->addOrderChannel($order_id, $data['channel_id']);
                }

                $config_id_prefix = $this->model_setting_setting->getSettingByKey('config_id_prefix');
                $config_id_prefix = isset($config_id_prefix['value']) ? $config_id_prefix['value'] : '';

                $config_id_suffix = $this->model_setting_setting->getSettingByKey('config_id_suffix');
                $config_id_suffix = isset($config_id_suffix['value']) ? $config_id_suffix['value'] : '';

                if (isset($data['voucher_product']) && !empty($data['voucher_product'])) {
                    $this->load->model('sale/campaign_voucher');
                    $campaign_voucher_id = $this->model_sale_campaign_voucher->addCampaignVoucher($order_id, $data['voucher_product']);
                    $amount_voucher = isset($data['voucher_product']['amount']) ? (float)$data['voucher_product']['amount'] : 0;
                    $voucher_products = (isset($data['voucher_product']['products']) && is_array($data['voucher_product']['products']) && !empty($data['voucher_product']['products'])) ? $data['voucher_product']['products'] : [];

                    if ($campaign_voucher_id && !empty($voucher_products)) {
                        // product-discount
                        foreach ($voucher_products as $voucher_product) {
                            foreach ($grouped_products as &$grouped_product) {
                                if ($grouped_product['id'] == $voucher_product['id'] &&
                                    ( !$grouped_product['product_version_id']
                                        || $grouped_product['product_version_id']
                                        && $grouped_product['product_version_id'] == $voucher_product['product_version_id'])) {
                                    $grouped_product['product-discount'] = $amount_voucher;
                                    $grouped_product['campaign_voucher_id'] = $campaign_voucher_id;
                                }
                            }
                        }
                    }
                }

                $max_order_id = 99999999999;
                $value_order = $max_order_id - ($max_order_id - $order_id);
                $strlen_value = strlen($value_order);
                $number_for = 11 - $strlen_value;
                $number_zero = '0';
                $result = '';

                for ($i = 1; $i <= $number_for; $i++) {
                    $result .= $number_zero;
                }
                $order_code = $config_id_prefix . $result . $order_id . $config_id_suffix;
                $this->db->query("UPDATE " . DB_PREFIX . "order SET order_code = '" . $this->db->escape($order_code) . "', `source` = 'chatbot' WHERE order_id = " . (int)$order_id);

                $this->model_sale_order->addOrderProduct($grouped_products, $order_id);
                $this->model_sale_order->addReport($order_id);

                $error = [
                    'code' => 0,
                    'message' => 'ORDER_CREATED',
                    'order_id' => $order_id,
                    'order_code' => $order_code
                ];
            }
        } catch (Exception $e) {
            $error = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->log->write('createOrder(): final resp: ' . json_encode($error));

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($error));
    }

    /**
     * editOrder
     *
     * @return array|void
     */
    private function editOrder()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $error = array();

        // get post data from php://input
        // notice: could not get directly from $this->request->post or $_POST, TODO: why?...
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('[chatbot_novaon_v2.0] orders(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        // check if has order_id
        if (!isset($this->request->post['data']['order_id']) || empty($this->request->post['data']['order_id'])) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Missing "order_id"!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $order_id = $this->request->post['data']['order_id'];

        // sample data
        /*$this->request->post = [
            'token' => 'd1133ac9eddba6d491219f070affc5d2',
            'data' => [
                'customer' => [
                    'name' => 'Nguyen Thuy Linh',
                    'phone_number' => '0987654321',
                    'email' => 'nguyenthuylinh@gmail.com',
                    'delivery_addr' => 'So 1 Ngo 2 Duy Tan',
                    'ward' => 'Dich Vong Hau',
                    'district' => 'Cau Giay',
                    'city' => 'Ha Noi',
                    'note' => 'Goi hang can than va du so luong nhe shop. Thanks!'
                ],
                "status" => 6,
                "customer_ref_id" => "",
                "order_id" => 213,
                "tags" => "tag1, tag2",
                'products' => [
                    [
                        'id' => 83,
                        'quantity' => 2
                    ],
                    [
                        'id' => 104,
                        'quantity' => 3,
                        'product_version_id' => 1
                    ]
                ]
            ]
        ];*/

        $valid_orders = $this->validateOrdersPostData();
        if ($valid_orders) {
            $error = $valid_orders;

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        try {
            $this->load->model('account/address');
            $this->load->model('customer/customer');
            $this->load->model('account/customer');
            $this->load->model('localisation/vietnam_administrative');

            // pass data post
            $data['telephone'] = $this->request->post['data']['customer']['phone_number'];

            // extract first name, last name
            $full_name = isset($this->request->post['data']['customer']['name']) ? $this->request->post['data']['customer']['name'] : '';
            $full_name = trim($full_name);
            $first_and_last_name = explode(" ", $full_name);
            $first_name = array_pop($first_and_last_name);
            $last_name = implode(" ", $first_and_last_name);
            $data['fullname'] = $full_name;
            $data['lastname'] = trim($last_name);
            $data['firstname'] = trim($first_name);

            $data['city'] = isset($this->request->post['data']['customer']['city']) ? $this->request->post['data']['customer']['city'] : '';
            $data['email'] = isset($this->request->post['data']['customer']['email']) ? $this->request->post['data']['customer']['email'] : '';
            $data['district'] = isset($this->request->post['data']['customer']['district']) ? $this->request->post['data']['customer']['district'] : '';
            $data['wards'] = isset($this->request->post['data']['customer']['ward']) ? $this->request->post['data']['customer']['ward'] : '';
            $data['address'] = isset($this->request->post['data']['customer']['delivery_addr']) ? $this->request->post['data']['customer']['delivery_addr'] : '';
            // 20210429: sp social
            $data['subscriber_id'] = isset($this->request->post['data']['customer']['subscriber_id']) ? $this->request->post['data']['customer']['subscriber_id'] : '';

            $customer = $this->model_customer_customer->getCustomerByTelephone($data['telephone']);
            if ($customer) {
                $customer_id = $customer['customer_id'];
            } else {
                $customer_id = $this->createNewCustomer($data);
            }
            $data['customer_id'] = $customer_id;

            $data['customer_group_id'] = $this->config->get('config_customer_group_id');
            $data['order_payment'] = 1;

            $order_status = isset($this->request->post['data']['status']) ? $this->request->post['data']['status'] : self::ORDER_STATUS_DRAFT;
            $data['order_transfer'] = $order_status;

            $data['order_note'] = isset($this->request->post['data']['customer']['note']) ? $this->request->post['data']['customer']['note'] : '';
            $data['payment_firstname'] = $data['firstname'];
            $data['payment_lastname'] = $data['lastname'];
            $data['payment_company'] = '';
            $data['payment_address_1'] = $data['address'];
            $data['payment_address_2'] = sprintf('%s, %s, %s, %s', $data['address'], $data['wards'], $data['district'], $data['city']);
            $data['payment_city'] = $data['city'];
            $data['payment_postcode'] = '';
            $data['payment_country_id'] = '';
            $data['payment_zone_id'] = '';
            $data['payment_custom_field'] = array();
            $data['payment_code'] = '';
            $data['payment_status'] = getValueByKey($this->request->post['data'], 'payment_status') == 1 ? 1 : 0;
            $data['shipping_firstname'] = $data['firstname'];
            $data['shipping_lastname'] = $data['lastname'];
            $data['shipping_company'] = '';
            $data['shipping_address_1'] = $data['address'];
            $data['shipping_address_2'] = sprintf('%s, %s, %s, %s', $data['address'], $data['wards'], $data['district'], $data['city']);
            $data['shipping_city'] = $data['city'];
            $data['shipping_postcode'] = '';
            $data['shipping_country_id'] = '';
            $data['shipping_zone_id'] = '';
            $data['shipping_custom_field'] = array();
            //$data['shipping_method'] = '';
            $data['shipping_method_value'] = ''; // phương thức vẫn chuyển mặc định để rõng
            $data['shipping_code'] = '';
            $data['shipping_ward_code'] = $data['wards'];
            $data['shipping_district_code'] = $data['district'];
            $data['shipping_province_code'] = $data['city'];

            $data['order_products'] = array();
            $data['order_vouchers'] = array();
            $data['order_totals'] = array();

            $data['order_status_id'] = $order_status;
            $data['order_status'] = $order_status;
            $data['currency_code'] = $this->config->get('config_currency');
            $data['from_domain'] = ''; // auto from main domain. TODO: from "chatbot"?...

            $data['coupon'] = '';
            $data['voucher'] = '';
            $data['reward'] = '';

            $data['tags'] = isset($this->request->post['data']['tags']) ? $this->request->post['data']['tags'] : '';

            // for chatbot only
            $data['customer_ref_id'] = isset($this->request->post['data']['customer_ref_id']) ? $this->request->post['data']['customer_ref_id'] : '';

            // 20210427: support campaign_voucher
            $data['voucher_product'] = isset($this->request->post['data']['voucher_product']) ? $this->request->post['data']['voucher_product'] : [];
            if (isset($data['voucher_product']) && !empty($data['voucher_product'])) {
                if (!isset($data['voucher_product']['voucher_code']) || empty($data['voucher_product']['voucher_code'])) {
                    $error = [
                        'code' => 102,
                        'message' => 'Voucher product invalid. Key "voucher_code" is not null'
                    ];
                }
            }

            // 20210429: sp social
            $data['campaign_id'] = isset($this->request->post['data']['campaign_id']) ? $this->request->post['data']['campaign_id'] : "";
            $data['channel_id'] = isset($this->request->post['data']['channel_id']) ? $this->request->post['data']['channel_id'] : "";

            $data['payment_method'] = isset($this->request->post['data']['payment_method']) ? $this->request->post['data']['payment_method'] : "";
//            if (!isset($data['payment_method']) || empty($data['payment_method'])) {
//                $error = [
//                    'code' => 102,
//                    'message' => 'payment_method invalid. Key "payment_method" is not null'
//                ];
//            }

            $products = $this->request->post['data']['products'];
            // merge products quantity if same id
            $grouped_products = [];
            foreach ($products as $product) {
                if (!isset($product['id']) || !isset($product['quantity']) || (int)$product['quantity'] == 0) {
                    continue;
                }

                $key = $product['id'];
                if (isset($product['product_version_id']) && !empty($product['product_version_id'])) {
                    $key = $product['product_version_id'];
                }

                // new if not exist
                if (!array_key_exists($key, $grouped_products)) {
                    $grouped_products[$key] = $product;
                    continue;
                }

                // new if not exist
                // if (!array_key_exists($product['id'], $grouped_products)) {
                    // $grouped_products[$product['id']] = $product;
                    // continue;
                // }

                // or sum quantity if existed
                $grouped_products[$product['id']]['quantity'] += $product['quantity'];
            }

            $this->load->model('catalog/product');
            $this->load->model('api/chatbox_novaon_v2');
            $total_amount = 0;
            $total_into_money = 0;
            $not_available_products = [];
            foreach ($grouped_products as &$product) {
                if (!isset($product['id']) || !isset($product['quantity']) || (int)$product['quantity'] == 0) {
                    $error = [
                        'code' => 102,
                        'message' => 'products info invalid. Missing key either "id" or "quantity"'
                    ];
                    break;
                }

                // get product info same as api get product detail
                $product_info = $this->model_api_chatbox_novaon_v2->getProductsListData_v1_1([
                    'id' => $product['id'],
                    'product_version_id' => isset($product['product_version_id']) ? $product['product_version_id'] : ''
                ], $all_statuses = true, $more_return_fields = ['status', 'deleted', 'quantity_in_stock', 'sale_on_out_of_stock']);

                // Notice: validate production not available (out_of_stock, deleted, disabled...) if draft for processing
                if (empty($order_status) || in_array($order_status, [
                        ModelSaleOrder::ORDER_STATUS_ID_DRAFT,
                        ModelSaleOrder::ORDER_STATUS_ID_PROCESSING
                    ])
                ) {
                    if (!$product_info ||
                        (isset($product_info['deleted']) && !empty($product_info['deleted'])) ||
                        (isset($product_info['status']) && $product_info['status'] != 1)
                    ) {
                        $product_info['error_code'] = 1042;
                        unset($product_info['status'], $product_info['deleted'], $product_info['quantity_in_stock'], $product_info['sale_on_out_of_stock']);
                        //$product_info['quantity'] = $product['quantity'];
                        $not_available_products[] = $product_info;
                        continue;
                    }

                    if (isset($product_info['quantity_in_stock']) && $product_info['quantity_in_stock'] <= 0 &&
                        isset($product_info['sale_on_out_of_stock']) && $product_info['sale_on_out_of_stock'] != 1
                    ) {
                        $product_info['error_code'] = 1041;
                        unset($product_info['status'], $product_info['deleted'], $product_info['quantity_in_stock'], $product_info['sale_on_out_of_stock']);
                        //$product_info['quantity'] = $product['quantity'];
                        $not_available_products[] = $product_info;
                        continue;
                    }
                }

                // TODO: use above product info?
                $product_detail = $this->model_catalog_product->getProduct($product['id']);

                $product_quantity_max = $this->getMaxQuantityProduct($product_detail, $product);
                $product_version = [];
                if (!$product_detail || ($product_detail['multi_versions'] == 1 && count($product_version = $this->model_catalog_product->getProductByVersionId($product_detail['product_id'], $product['product_version_id'])) == 0)) {
                    /*$error = [
                        'code' => 201,
                        'message' => 'error product order'
                    ];
                    break;*/
                }

                if (!$product_quantity_max) {
                    /*$error = [
                        'code' => 201,
                        'message' => 'error product order'
                    ];
                    break;*/
                }

                $product['name'] = $product_detail['name'];
                $product['model'] = $product_detail['model'];
                $product['price'] = $product_detail['price'];
                $product['product_id'] = $product['id'];
                if (isset($product['product_version_id']) && !empty($product['product_version_id'])) {
                    if ((int)$product_version['price'] == 0) $product_version['price'] = $product_version['compare_price'];
                    $total = (float)$product_version['price'] * (int)$product['quantity'];
                    $product['product_version'] = $product_version;
                } else {
                    if ((int)$product_detail['price'] == 0) $product_detail['price'] = $product_detail['compare_price_master'];
                    $total = (float)$product_detail['price'] * (int)$product['quantity'];
                }
                $total_amount += $product['quantity'];
                $total_into_money += $total;
            }
            unset($product);

            if (!empty($not_available_products)) {
                $error = [
                    "code" => 104,
                    "message" => "create order failed: products are out of stock or do not existed",
                    "products" => $not_available_products
                ];
            }

            $data['shipping_fee'] = 0;
            $data['shipping_method'] = '';
            // 20200612: support shipping fee
            $delivery_id = isset($this->request->post['data']['payment_trans']) ? $this->request->post['data']['payment_trans'] : null;
            switch ($delivery_id) {
                case self::DELIVERY_METHOD_CUSTOM:
                    if (!isset($this->request->post['data']['payment_trans_custom_name']) ||
                        !isset($this->request->post['data']['payment_trans_custom_fee'])
                    ) {
                        $error = [
                            "code" => 400,
                            "message" => "create order failed: missing neither payment_trans custom_name nor custom_fee"
                        ];

                        break;
                    }

                    $data['shipping_method'] = $this->request->post['data']['payment_trans_custom_name'];
                    $data['shipping_fee'] = (int)$this->request->post['data']['payment_trans_custom_fee'];

                    break;

                case self::DELIVERY_METHOD_FREE:
                    $data['shipping_method'] = 'Miễn phí vận chuyển';
                    $data['shipping_fee'] = 0;

                    break;

                default:
                    if (is_null($delivery_id)) {
                        break;
                    }

                    // get shipping method name
                    $deliveries = $this->getDeliveryConfig();
                    foreach ($deliveries as $delivery) {
                        if (!isset($delivery['id']) || !isset($delivery['name']) || $delivery['id'] != $delivery_id) {
                            continue;
                        }

                        if ($delivery['id'] == $delivery_id) {
                            $data['shipping_method'] = $delivery['name'];
                            break;
                        }
                    }

                    // calc delivery fee
                    $data_calc_delivery_fee = [
                        'products' => [
                            /*[
                                'id' => 135,
                                'quantity' => 1
                            ]*/
                        ],
                        'province' => $data['city'],
                        'into_money' => $total_into_money,
                        'delivery_id' => $delivery_id,
                    ];

                    foreach ($products as $product) {
                        if (!isset($product['id']) || !isset($product['quantity']) || (int)$product['quantity'] == 0) {
                            continue;
                        }

                        $data_calc_delivery_fee['products'][] = [
                            'id' => $product['id'],
                            'quantity' => $product['quantity']
                        ];
                    }

                    $delivery_fees = $this->calcDeliveryFee($data_calc_delivery_fee);

                    // error
                    if (isset($delivery_fees['error']) && !empty($delivery_fees['error'])) {
                        $error = [
                            "code" => 104,
                            "message" => "create order failed: calculate delivery fee failed (may products are out of stock or do not existed)",
                            "products" => $not_available_products
                        ];

                        break;
                    }

                    // success
                    if (isset($delivery_fees['delivery_fees'][0]['fee'])) {
                        $data['shipping_fee'] = (int)$delivery_fees['delivery_fees'][0]['fee'];
                        $_delivery = getValueByKey($delivery_fees['delivery_fees'][0], 'delivery', [
                            'id' => '',
                            'name' => '',
                        ]);
                        $data['shipping_method'] = $_delivery['name'];
                        $data['shipping_method_value'] = $_delivery['id'];
                    }

                    break;
            }

            $data['total'] = isset($this->request->post['data']['total']) ? $this->request->post['data']['total'] : 0; //$data['shipping_fee'] + $total_into_money;
            if (!$error) {
                $this->load->model('sale/order');
                $this->model_sale_order->editOrderCommon($order_id, $data, $customer_id);

                if (isset($data['campaign_id']) && !empty($data['campaign_id'])) {
                    $this->load->model('sale/order_campaign');
                    $this->model_sale_order_campaign->addOrderCampaign($order_id, $data['campaign_id']);
                }

                if (isset($data['channel_id']) && !empty($data['channel_id'])) {
                    $this->load->model('sale/order_channel');
                    $this->model_sale_order_channel->addOrderChannel($order_id, $data['channel_id']);
                }

                if (isset($data['voucher_product']) && !empty($data['voucher_product'])) {
                    $this->load->model('sale/campaign_voucher');
                    $campaign_voucher_id = $this->model_sale_campaign_voucher->addCampaignVoucher($order_id, $data['voucher_product']);
                    $amount_voucher = isset($data['voucher_product']['amount']) ? (float)$data['voucher_product']['amount'] : 0;
                    $voucher_products = (isset($data['voucher_product']['products']) && is_array($data['voucher_product']['products']) && !empty($data['voucher_product']['products'])) ? $data['voucher_product']['products'] : [];

                    if ($campaign_voucher_id && !empty($voucher_products)) {
                        // product-discount
                        foreach ($voucher_products as $voucher_product) {
                            foreach ($grouped_products as &$grouped_product) {
                                if ($grouped_product['id'] == $voucher_product['id'] &&
                                    ( !$grouped_product['product_version_id']
                                        || $grouped_product['product_version_id']
                                        && $grouped_product['product_version_id'] == $voucher_product['product_version_id'])) {
                                    $grouped_product['product-discount'] = $amount_voucher;
                                    $grouped_product['campaign_voucher_id'] = $campaign_voucher_id;
                                }
                            }
                        }
                    }
                }

                $this->db->query("DELETE FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
                $this->model_sale_order->addOrderProduct($grouped_products, $order_id);
                $this->model_sale_order->addReport($order_id);

                $error = [
                    'code' => 0,
                    'message' => 'order edited successfully'
                ];
            }
        } catch (Exception $e) {
            $error = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($error));
    }

    /**
     * updateOrderStatus
     *
     */
    private function updateOrderStatus()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 400,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        // get post data from php://input
        // notice: could not get directly from $this->request->post or $_POST, TODO: why?...
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('[chatbot_novaon_v2.0] orders(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        // check if has order_id
        if (!isset($this->request->post['data']['order_id']) || empty($this->request->post['data']['order_id']) ||
            !isset($this->request->post['data']['order_status_id']) || empty($this->request->post['data']['order_status_id'])) {
            $error = [
                'code' => 400,
                'message' => 'Bad request. Missing "order_id" or "order_status_id"!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $valid_orders = $this->validateUpdateOrderStatusPostData();
        if ($valid_orders) {
            $error = $valid_orders;

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $order_id = $this->request->post['data']['order_id'];
        $order_status_id = $this->request->post['data']['order_status_id'];

        try {
            $this->load->model('sale/order');
            $order = $this->model_sale_order->getOrder($order_id);

            if (isset($order['order_status_id'])) {
                if ($order_status_id == 9) {
                    $orderDetail = $this->model_sale_order->getOrder($order_id);

                    if($orderDetail['payment_status'] != 1){
                        $customer_info = json_encode(['id' => isset($orderDetail['customer_id']) ? $orderDetail['customer_id'] : null ,
                                'name' => isset($orderDetail['fullname']) ? $orderDetail['fullname'] : null ]
                        );
                        $orderDetail['customer_info'] = $customer_info;
                        $orderDetail['payment_status'] = 1;
                        $orderDetail['total_pay'] = $orderDetail['total'];
                        $orderDetail['user_create_id'] = 1; // 1 is admin $this->user->getId();
                        $receipt_voucher = $this->autoUpdateFromOrder($order_id, $orderDetail);
                    }

                    $this->model_sale_order->updateOrderPaymentStatusFromAdmin($order_id, 1);
                }

                $this->model_sale_order->updateStatus(['order_id' => $order_id, 'order_status_id' => $order_status_id], $order['order_status_id']);
            }
        } catch (Exception $e) {
            //
        }

        $result = [
            'code' => 200,
            'message' => 'Update status success'
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    /**
     * getOrdersList
     */
    private function getOrdersList()
    {
        $this->load->language('api/chatbot_novaon');

        // check parameter
        if (!$this->validateTokenGetOrdersList()) {
            $json = [
                'code' => 400,
                'message' => 'invalid token'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));

            return;
        }

        $data_filter = [];
        if (isset($this->request->get['page']) && !empty($this->request->get['page'])) {
            $data_filter = [
                'start' => ((int)$this->request->get['page'] - 1) * ((int)isset($this->request->get['limit']) ? $this->request->get['limit'] : 15),
                'limit' => isset($this->request->get['limit']) ? $this->request->get['limit'] : 15
            ];
        }

        /*filter :
            giá trị đơn: order_total_type
            nguồn : order_campaign
            kênh : order_channel
            trạng thái thanh toán : order_payment_status
            đơn hàng nháp : order_status
            sản phẩm : lọc theo tên sản phẩm trong đơn hàng
            search_order_code

            order_customer_ids
            order_promotion_code
            order_subscriber_ids
        */

        $data_filter['order_status'] = isset($this->request->get['order_status']) ? $this->request->get['order_status'] : '';
        $data_filter['order_payment'] = isset($this->request->get['order_payment']) ? $this->request->get['order_payment'] : '';
        $data_filter['order_total_type'] = isset($this->request->get['order_total_type']) ? $this->request->get['order_total_type'] : '';
        $data_filter['order_code'] = isset($this->request->get['order_code']) ? $this->request->get['order_code'] : '';
        $data_filter['order_ids'] = isset($this->request->get['order_ids']) ? $this->request->get['order_ids'] : '';

        $data_filter['order_campaign'] = isset($this->request->get['order_campaign']) ? $this->request->get['order_campaign'] : '';
        $data_filter['order_channel'] = isset($this->request->get['order_channel']) ? $this->request->get['order_channel'] : '';
        $data_filter['order_product'] = isset($this->request->get['order_product']) ? $this->request->get['order_product'] : '';

        $data_filter['order_customer_ids'] = isset($this->request->get['order_customer_ids']) ? $this->request->get['order_customer_ids'] : '';
        $data_filter['order_promotion_code'] = isset($this->request->get['order_promotion_code']) ? $this->request->get['order_promotion_code'] : '';

        $data_filter['order_subscriber_ids'] = isset($this->request->get['order_subscriber_ids']) ? $this->request->get['order_subscriber_ids'] : '';

       /* $data_filter['customer_ref_id'] = isset($this->request->get['customer_ref_id']) ? $this->request->get['customer_ref_id'] : '';
        $data_filter['order_ids'] = isset($this->request->get['order_ids']) ? $this->request->get['order_ids'] : '';
        $data_filter['order_tag'] = isset($this->request->get['order_tag']) ? $this->request->get['order_tag'] : '';
        $data_filter['order_utm_key'] = isset($this->request->get['order_utm_key']) ? $this->request->get['order_utm_key'] : '';
        $data_filter['order_utm_value'] = isset($this->request->get['order_utm_value']) ? $this->request->get['order_utm_value'] : '';*/

        $this->load->model('api/chatbox_novaon_v2');
        $json = $this->model_api_chatbox_novaon_v2->getOrdersListData($data_filter);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function getOrderDetail()
    {
        $this->load->language('api/chatbot_novaon');

        // check parameter
        if (!$this->validateTokenGetOrderDetail()) {
            $json = [
                'code' => 400,
                'message' => 'invalid token'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));

            return;
        }

        $order_id = isset($this->request->get['id']) ? $this->request->get['id'] : '';
        if (empty($order_id)) {
            $json = [];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));

            return;
        }

        // do getting order detail
        $data_filter = [];
        // $data_filter['customer_ref_id'] = isset($this->request->get['customer_ref_id']) ? $this->request->get['customer_ref_id'] : '';
        $data_filter['order_ids'] = $order_id;

        $this->load->model('api/chatbox_novaon_v2');
        $json = $this->model_api_chatbox_novaon_v2->getOrdersListData($data_filter);
        $order_detail = [];
        if (isset($json['records']) && is_array($json['records']) && count($json['records']) > 0) {
            $order_detail = $json['records'][0];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($order_detail));
    }

    private function validateProductPostData()
    {
        $bad_request_code = 400;

        $this->load->model('catalog/product');
        if (!isset($this->request->post['token'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing token'
            ];
        }

        if ($this->getMd5TokenPost() != $this->request->post['token']) {
            return [
                'code' => $bad_request_code,
                'message' => 'invalid token'
            ];
        }

        $product_id = isset($this->request->post['data']['product_id']) ? $this->request->post['data']['product_id'] : '';


        $name = isset($this->request->post['data']['name']) ? $this->request->post['data']['name'] : '';
        if (!$name) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing or invalid name'
            ];
        } else {
            if ($this->model_catalog_product->checkExistingProductByName($this->request->post['data']['name'], $product_id) != false) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'error name exist'
                ];
            }
        }

        $description = isset($this->request->post['data']['description']) ? $this->request->post['data']['description'] : '';
        if (!$description) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing or invalid description'
            ];
        }

        $summary = isset($this->request->post['data']['summary']) ? $this->request->post['data']['summary'] : '';
        if (!$summary) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing or invalid summary'
            ];
        }

        if (!isset($this->request->post['data']['price'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing price'
            ];
        }

        if (!isset($this->request->post['data']['promotion_price'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing promotion_price'
            ];
        }

        if (!isset($this->request->post['data']['weight'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing weight'
            ];
        }

        if (!isset($this->request->post['data']['sku'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing sku'
            ];
        }

        if (!isset($this->request->post['data']['barcode'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing barcode'
            ];
        }

        if (!isset($this->request->post['data']['common_compare_price'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing common_compare_price'
            ];
        }

        if (!isset($this->request->post['data']['common_price'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing common_price'
            ];
        }

        if (!isset($this->request->post['data']['common_weight'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing common_weight'
            ];
        }

        if (!isset($this->request->post['data']['common_sku'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing common_sku'
            ];
        }

        if (!isset($this->request->post['data']['common_barcode'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing common_barcode'
            ];
        }

        if (!isset($this->request->post['data']['common_cost_price'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing common_cost_price'
            ];
        }

        $product_version = isset($this->request->post['data']['product_version']) ? $this->request->post['data']['product_version'] : '';
        if (!isset($product_version)) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing or invalid product_version'
            ];
        } elseif ($product_version == '1') {
            $common_compare_price = isset($this->request->post['data']['common_compare_price']) ? $this->request->post['data']['common_compare_price'] : '';
            if (!$common_compare_price || floatval($common_compare_price) > 99999999999) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid common_compare_price , max 99,999,999,999'
                ];
            }

            $common_price = isset($this->request->post['data']['common_price']) ? $this->request->post['data']['common_price'] : '';
            if (!$common_price || floatval($common_price) > 99999999999) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid common_price , max 99,999,999,999'
                ];
            }

            $attribute_name = isset($this->request->post['data']['attribute_name']) ? $this->request->post['data']['attribute_name'] : '';
            if (!$attribute_name || !is_array($attribute_name)) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid attribute_name'
                ];
            }

            foreach ($attribute_name as $att) {
                if (utf8_strlen($att) > 30) {
                    return [
                        'code' => $bad_request_code,
                        'message' => 'invalid attribute_name. each name max length 30 '
                    ];
                }
            }

            $attribute_values = isset($this->request->post['data']['attribute_values']) ? $this->request->post['data']['attribute_values'] : '';
            if (!$attribute_values || !is_array($attribute_values) || count($attribute_values) > 100) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid attribute_values or total max 100 values'
                ];
            }

            foreach ($attribute_values as $attrs) {
                foreach ($attrs as $attr) {
                    if (utf8_strlen($attr) > 30) {
                        return [
                            'code'    => $bad_request_code,
                            'message' => 'invalid attribute_name. each value max length 30 '
                        ];
                    }
                }
            }

            $product_prices = isset($this->request->post['data']['product_prices']) ? $this->request->post['data']['product_prices'] : '';
            if (!$product_prices || !is_array($product_prices)) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid product_prices'
                ];
            }

            foreach ($product_prices as $price) {
                if (floatval($price) > 99999999999) {
                    return [
                        'code' => $bad_request_code,
                        'message' => 'missing or invalid product_prices max 99,999,999,999'
                    ];
                }
            }

            $product_promotion_prices = isset($this->request->post['data']['product_promotion_prices']) ? $this->request->post['data']['product_promotion_prices'] : '';
            if (!$product_promotion_prices || !is_array($product_promotion_prices)) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid product_promotion_prices'
                ];
            }

            foreach ($product_promotion_prices as $price) {
                if (floatval($price) > 99999999999) {
                    return [
                        'code' => $bad_request_code,
                        'message' => 'missing or invalid product_promotion_prices max 99,999,999,999'
                    ];
                }
            }

            $product_quantities = isset($this->request->post['data']['product_quantities']) ? $this->request->post['data']['product_quantities'] : '';
            if (!$product_quantities || !is_array($product_quantities)) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid product_quantities'
                ];
            }

            foreach ($product_quantities as $quantity) {
                if (floatval($quantity) > 9999999) {
                    return [
                        'code' => $bad_request_code,
                        'message' => 'missing or invalid product_quantities max 9,999,999'
                    ];
                }
            }

            $product_skus = isset($this->request->post['data']['product_skus']) ? $this->request->post['data']['product_skus'] : '';
            if (!$product_skus || !is_array($product_skus)) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid product_skus'
                ];
            } else {
                foreach ($product_skus as $product_sku) {
                    if (!empty($product_sku) && $this->model_catalog_product->searchProductAndVersionBySKU($product_sku, $product_id) == false) {
                        return [
                            'code' => $bad_request_code,
                            'message' => 'error sku exist'
                        ];
                    }
                }
            }

            $product_barcodes = isset($this->request->post['data']['product_barcodes']) ? $this->request->post['data']['product_barcodes'] : '';
            if (!$product_barcodes || !is_array($product_barcodes)) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid product_barcodes'
                ];
            }

            $common_weight = isset($this->request->post['data']['common_weight']) ? $this->request->post['data']['common_weight'] : '';
            if (!$common_weight || floatval($common_weight) > 9999999) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid common_weight, max 9,999,999'
                ];
            }

            $common_sku = isset($this->request->post['data']['common_sku']) ? $this->request->post['data']['common_sku'] : '';
            if (!$common_sku) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid common_sku'
                ];
            }

            $common_barcode = isset($this->request->post['data']['common_barcode']) ? $this->request->post['data']['common_barcode'] : '';
            if (!$common_barcode) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid common_barcode'
                ];
            }
        } elseif (0 == $product_version) {
            $price = isset($this->request->post['data']['price']) ? $this->request->post['data']['price'] : '';
            if (!$price || floatval($price) > 99999999999) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid price , max 99,999,999,999'
                ];
            }

            $promotion_price = isset($this->request->post['data']['promotion_price']) ? $this->request->post['data']['promotion_price'] : '';
            if (!$promotion_price || floatval($promotion_price) > 99999999999) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid promotion_price, max 99,999,999,999'
                ];
            }

            if (!isset($this->request->post['data']['stores_default_id'])) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing stores_default_id'
                ];
            }

            $original_inventory = isset($this->request->post['data']['original_inventory']) ? $this->request->post['data']['original_inventory'] : '';
            if (!$original_inventory || floatval($original_inventory) > 9999999) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid original_inventory'
                ];
            }

            $weight = isset($this->request->post['data']['weight']) ? $this->request->post['data']['weight'] : '';
            if (!$weight || floatval($weight) > 9999999) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid weight, max 9,999,999'
                ];
            }

            $sku = isset($this->request->post['data']['sku']) ? $this->request->post['data']['sku'] : '';
            if (!$sku) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid sku'
                ];
            } else {
                if ($this->model_catalog_product->searchProductAndVersionBySKU($this->request->post['data']['sku'], $product_id) == false) {
                    return [
                        'code' => $bad_request_code,
                        'message' => 'error sku exist'
                    ];
                }
            }
        }

        $sale_on_out_of_stock = isset($this->request->post['data']['sale_on_out_of_stock']) ? $this->request->post['data']['sale_on_out_of_stock'] : '';
        if (is_null($sale_on_out_of_stock) || $sale_on_out_of_stock === '' || !in_array($sale_on_out_of_stock, [0, 1])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing or invalid sale_on_out_of_stock'
            ];
        }


        $images = isset($this->request->post['data']['images']) ? $this->request->post['data']['images'] : '';
        if (!$images || !is_array($images)) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing or invalid images'
            ];
        }

        foreach ($images as $img) {
            if (utf8_strlen($img) > 255) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid images, each url max length 255'
                ];
            }
        }

        $image_alts = isset($this->request->post['data']['image-alts']) ? $this->request->post['data']['image-alts'] : '';
        if (!$image_alts || !is_array($image_alts)) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing or invalid image-alts'
            ];
        }

        $status = isset($this->request->post['data']['status']) ? $this->request->post['data']['status'] : '';
        if (!$status) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing or invalid status'
            ];
        }

        $channel = isset($this->request->post['data']['channel']) ? $this->request->post['data']['channel'] : '';
        if (!isset($channel)) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing or invalid channel'
            ];
        }

        $categories = isset($this->request->post['data']['category']) ? $this->request->post['data']['category'] : '';
        if (!empty($categories) && !is_array($categories)) {
            return [
                'code' => $bad_request_code,
                'message' => 'invalid categories (expected: array)'
            ];
        }

        return [
            'code' => 200
        ];
    }

    private function validateProductDeleteData()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            return [
                'code' => 201,
                'message' => 'Bad request. Use POST only!'
            ];
        }

        // get post data from php://input
        // notice: could not get directly from $this->request->post or $_POST, TODO: why?...
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('[chatbot_novaon_v2.0] delete(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        if (!isset($this->request->post['token'])) {
            return [
                'code' => 400,
                'message' => 'invalid token'
            ];
        }

        $token = $this->getMd5TokenPost();
        //return $token;
        if ($token != $this->request->post['token']) {
            return [
                'code' => 400,
                'message' => 'invalid token'
            ];
        }

        $product_id = getValueByKey($this->request->post['data'], 'product_id');
        $action = getValueByKey($this->request->post['data'], 'action');

        // if delete
        if (!is_null($action)) {
            if ($action == 'delete') {
                if ($product_id <= 0) {
                    return [
                        'code' => 501,
                        'message' => 'product id invalid (staff id)'
                    ];
                }
                $this->load->model('catalog/product');
                $exist = $this->model_catalog_product->getProduct($product_id);
                if (!$exist) {
                    $error = [
                        'code' => 506,
                        'message' => 'product (with product_id) does not existed'
                    ];
                    return $error;
                }

                return;
            } else {
                return [
                    'code' => 501,
                    'message' => 'not supported action'
                ];
            }
        }

    }

    private function validateOrdersPostData()
    {
        $this->load->model('sale/order');

        if (!isset($this->request->post['token'])) {
            return [
                'code' => 400,
                'message' => 'invalid token'
            ];
        }

        if ($this->getMd5TokenPost() != $this->request->post['token']) {
            return [
                'code' => 400,
                'message' => 'invalid token'
            ];
        }

        $order_status = isset($this->request->post['data']['status']) ? $this->request->post['data']['status'] : '';

        if (empty($order_status) || !in_array($order_status, [ModelSaleOrder::ORDER_STATUS_ID_DRAFT, ModelSaleOrder::ORDER_STATUS_ID_PROCESSING, ModelSaleOrder::ORDER_STATUS_ID_DELIVERING, ModelSaleOrder::ORDER_STATUS_ID_COMPLETED, ModelSaleOrder::ORDER_STATUS_ID_CANCELLED])) {
            return [
                'code' => 101,
                'message' => 'order status invalid.'
            ];
        }

        $phone = isset($this->request->post['data']['customer']['phone_number']) ? $this->request->post['data']['customer']['phone_number'] : '';

        if ($order_status != ModelSaleOrder::ORDER_STATUS_ID_DRAFT) {
            if (!$phone || (utf8_strlen($phone) < 3) || (utf8_strlen($phone) > 32)) {
                return [
                    'code' => 101,
                    'message' => 'customer info invalid. phone_number length must be 3-32'
                ];
            }
        }

        // NO NEED validate address if:
        // - does not have order status (v1) (known as status = 6 (draft) for default)
        // - has order status (v2) and status = 6 (draft)
        //$order_status = isset($this->request->post['data']['status']) ? $this->request->post['data']['status'] : '';
        if (empty($order_status) || $order_status != ModelSaleOrder::ORDER_STATUS_ID_DRAFT) {
            $address = isset($this->request->post['data']['customer']['delivery_addr']) ? $this->request->post['data']['customer']['delivery_addr'] : '';
            if (empty($address)) {
                return [
                    'code' => 101,
                    'message' => 'customer info invalid. delivery_addr could not be empty with order status "draft" (' . ModelSaleOrder::ORDER_STATUS_ID_DRAFT . ')'
                ];
            }
        }

        $products = isset($this->request->post['data']['products']) ? $this->request->post['data']['products'] : '';
        if (!$products || !is_array($products)) {
            return [
                'code' => 102,
                'message' => 'products info invalid. products must be array and could not be empty'
            ];
        }

        foreach ($products as $p) {
            if (!isset($p['id']) || !isset($p['quantity'])) {
                return [
                    'code' => 102,
                    'message' => 'products info invalid. Missing key either "id" or "quantity"'
                ];
            }
        }

        return;
    }

    private function validateUpdateOrderStatusPostData()
    {
        if (!isset($this->request->post['token'])) {
            return [
                'code' => 400,
                'message' => 'Bad request. Invalid token'
            ];
        }

        if ($this->getMd5TokenPost() != $this->request->post['token']) {
            return [
                'code' => 400,
                'message' => 'Bad request. Invalid token'
            ];
        }

        $this->load->model('sale/order');
        $CHANGEABLE_ORDER_STATUS = [
            // draft
            ModelSaleOrder::ORDER_STATUS_ID_DRAFT => [
                ModelSaleOrder::ORDER_STATUS_ID_PROCESSING
            ],
            // processing
            ModelSaleOrder::ORDER_STATUS_ID_PROCESSING => [
                ModelSaleOrder::ORDER_STATUS_ID_DELIVERING, ModelSaleOrder::ORDER_STATUS_ID_COMPLETED, ModelSaleOrder::ORDER_STATUS_ID_CANCELLED
            ],
            // delivering
            ModelSaleOrder::ORDER_STATUS_ID_DELIVERING => [
                ModelSaleOrder::ORDER_STATUS_ID_COMPLETED, ModelSaleOrder::ORDER_STATUS_ID_CANCELLED
            ],
            // complete
            ModelSaleOrder::ORDER_STATUS_ID_COMPLETED => [
                ModelSaleOrder::ORDER_STATUS_ID_CANCELLED
            ],
            // cancelled
            ModelSaleOrder::ORDER_STATUS_ID_CANCELLED => []
        ];

        $order_id = $this->request->post['data']['order_id'];
        $order = $this->model_sale_order->getOrder($order_id);

        if ($order) {
            $order_status_id = $this->request->post['data']['order_status_id'];

            if(!in_array($order_status_id, [6,7,8,9,10])) {
                return [
                    'code' => 400,
                    'message' => 'Bad request: Does not support status'
                ];
            }

            if (!in_array($order_status_id, $CHANGEABLE_ORDER_STATUS[$order['order_status_id']])) {
                return [
                    'code' => 403,
                    'message' => 'Status invalid'
                ];
            }

        } else {
            return [
                'code' => 404,
                'message' => 'Order not found'
            ];
        }

        return;
    }

    private function validateCreateOrUpdateStaffData()
    {
        if (!isset($this->request->post['token'])) {
            return [
                'code' => 400,
                'message' => 'invalid token'
            ];
        }

        $token = $this->getMd5TokenPost();
        if ($token != $this->request->post['token']) {
            return [
                'code' => 400,
                'message' => 'invalid token'
            ];
        }

        $staff_id = getValueByKey($this->request->post['data'], 'staff_id');
        $action = getValueByKey($this->request->post['data'], 'action');

        // if delete
        if (!is_null($action)) {
            if ($action == 'delete') {
                if ($staff_id <= 0) {
                    return [
                        'code' => 501,
                        'message' => 'staff id invalid (staff id)'
                    ];
                }

                return $this->validateStaffsPostDataForDelete();
            } else {
                return [
                    'code' => 501,
                    'message' => 'not supported action'
                ];
            }
        }

        return $this->validateStaffsPostDataForCreateOrEdit();
    }

    private function validateStaffsPostDataForCreateOrEdit()
    {
        $info = getValueByKey($this->request->post['data'], 'info');
        if (empty($info)) {
            return [
                'code' => 101,
                'message' => 'missing info'
            ];
        }

        if (empty(getValueByKey($info, 'email'))) {
            return [
                'code' => 501,
                'message' => 'staff info invalid (email)'
            ];
        }

        $staff_id = getValueByKey($this->request->post['data'], 'staff_id');

        // password on creating
        $is_editing = $staff_id > 0;
        if (!$is_editing) {
            if (empty(getValueByKey($info, 'password'))) {
                return [
                    'code' => 501,
                    'message' => 'staff info invalid (password missing)'
                ];
            }
        }

        // validate email existing
        $this->load->model('user/user');
        if (!$is_editing) {
            // on creating: staff MUST not existed by email
            $staff = $this->model_user_user->getUserByEmail($info['email']);
            if (isset($staff['user_id'])) {
                return [
                    'code' => 507,
                    'message' => 'staff (with email) already existed',
                    'staff_id' => $staff['user_id']
                ];
            }
        } else {
            // on editing: staff MUST existed by email and staff_id
            $staff = $this->model_user_user->getUser($staff_id);
            if (!isset($staff['user_id'])) {
                return [
                    'code' => 506,
                    'message' => 'staff (with staff_id) does not existed'
                ];
            }
        }

        $status = getValueByKey($this->request->post['data'], 'status');
        if (is_null($status) || !in_array((int)$status, [0, 1])) {
            return [
                'code' => 502,
                'message' => 'invalid status, supported 0|1'
            ];
        }

        $is_all_permissions = getValueByKey($this->request->post['data'], 'is_all_permissions');
        if (is_null($is_all_permissions) || !in_array((int)$is_all_permissions, [0, 1])) {
            return [
                'code' => 503,
                'message' => 'is_all_permissions invalid, supported 0|1'
            ];
        }

        $permission = getValueByKey($this->request->post['data'], 'permission');
        if ((empty($permission) || !is_array($permission)) && $is_all_permissions != 1) {
            return [
                'code' => 504,
                'message' => 'permission invalid'
            ];

            foreach ($permission as $per) {
                if (!in_array($per, self::SUPPORTED_PERMISSIONS)) {
                    return [
                        'code' => 504,
                        'message' => 'permission invalid'
                    ];
                }
            }
        }

        return;
    }

    private function validateProductPostDataForDelete()
    {
        $this->load->model('user/user');

        // on deleting: staff MUST existed by email and staff_id
        $staff_id = getValueByKey($this->request->post['data'], 'staff_id');
        $staff = $this->model_user_user->getUser($staff_id);
        if (!isset($staff['user_id'])) {
            return [
                'code' => 506,
                'message' => 'staff (with staff_id) does not existed'
            ];
        }
        return;
    }

    private function validateStaffsPostDataForDelete()
    {
        $this->load->model('user/user');

        // on deleting: staff MUST existed by email and staff_id
        $staff_id = getValueByKey($this->request->post['data'], 'staff_id');
        $staff = $this->model_user_user->getUser($staff_id);
        if (!isset($staff['user_id'])) {
            return [
                'code' => 506,
                'message' => 'staff (with staff_id) does not existed'
            ];
        }
        return;
    }

    private function validateAdminPassword($admin_password)
    {
        if (empty($admin_password)) {
            return false;
        }

        $this->load->model('user/user');
        $user_info = $this->model_user_user->getUser(1);
        if (!isset($user_info['type']) || ($user_info['type'] != 'Admin') ||
            !isset($user_info['password']) || !isset($user_info['salt'])
        ) {
            return false;
        }

        $password = sha1($admin_password);
        $password = sha1($user_info['salt'] . $password);
        $password = sha1($user_info['salt'] . $password);
        if ($password != $user_info['password']) {
            return false;
        }

        return true;
    }

    private function validateToken()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }

        if ($this->getMd5TokenGet() !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    private function validateTokenGetProducts()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }


        if ($this->getMd5TokenGetProductsList() !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    private function validateTokenGetOrdersList()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }

        if ($this->getMd5TokenGetOrdersList() !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    private function validateTokenGetOrderDetail()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }

        if ($this->getMd5TokenGetOrderDetail() !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    private function validateTokenGetStaffsList()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }

        if ($this->getMd5TokenGetStaffsList() !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    private function validateTokenGetCategoriesList()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }

        if ($this->getMd5TokenGetCategoriesList() !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    private function validateTokenGetManufacturersList()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }

        if ($this->getMd5TokenGetManufacturersList() !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    private function validateTokenGetStaffDetail()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }

        if ($this->getMd5TokenGetStaffDetail() !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    private function validateTokenVerify()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }

        if ($this->getMd5TokenVerify() !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    private function validateTokenGetCustomer()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }


        if ($this->getMd5TokenGetCustomerList() !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    private function getMd5TokenGet()
    {
        $apiKey = $this->getApiKey();
        $token_string = isset($this->request->get['page']) ? $this->request->get['page'] : '';
        $token_string .= isset($this->request->get['limit']) ? $this->request->get['limit'] : '';
        $token_string .= urlencode(isset($this->request->get['searchKey']) ? $this->request->get['searchKey'] : '');
        $token_string .= isset($this->request->get['searchField']) ? $this->request->get['searchField'] : '';
        $token_string .= isset($this->request->get['id']) ? $this->request->get['id'] : '';
        $token_string .= isset($this->request->get['product_version_id']) ? $this->request->get['product_version_id'] : '';
        $token_string .= $apiKey;
        return md5($token_string);
    }

    private function getMd5TokenGetOrdersList()
    {
        $data = [];
        $data[] = isset($this->request->get['page']) ? $this->request->get['page'] : '';
        $data[] = isset($this->request->get['limit']) ? $this->request->get['limit'] : '';

        // $data[] = urlencode(isset($this->request->get['customer_ref_id']) ? $this->request->get['customer_ref_id'] : '');
        // $data[] = urlencode(isset($this->request->get['order_tag']) ? $this->request->get['order_tag'] : '');
        // $data[] = urlencode(isset($this->request->get['order_utm_key']) ? $this->request->get['order_utm_key'] : '');
        // $data[] = urlencode(isset($this->request->get['order_utm_value']) ? $this->request->get['order_utm_value'] : '');

        $data[] = isset($this->request->get['order_ids']) ? $this->request->get['order_ids'] : '';

        $data[] = isset($this->request->get['order_status']) ? $this->request->get['order_status'] : '';
        $data[] = isset($this->request->get['order_payment']) ? $this->request->get['order_payment'] : '';
        $data[] = urlencode(isset($this->request->get['order_total_type']) ? $this->request->get['order_total_type'] : '');
        $data[] = urlencode(isset($this->request->get['order_code']) ? $this->request->get['order_code'] : '');

        $data[] = isset($this->request->get['order_campaign']) ? $this->request->get['order_campaign'] : '';
        $data[] = isset($this->request->get['order_channel']) ? $this->request->get['order_channel'] : '';
        $data[] = urlencode(isset($this->request->get['order_product']) ? $this->request->get['order_product'] : '');

        $data[] = isset($this->request->get['order_customer_ids']) ? $this->request->get['order_customer_ids'] : '';
        $data[] = isset($this->request->get['order_promotion_code']) ? $this->request->get['order_promotion_code'] : '';

        $data[] = isset($this->request->get['order_subscriber_ids']) ? $this->request->get['order_subscriber_ids'] : '';

        $data[] = $this->getApiKey();

        return $this->getToken($data);
    }

    private function getMd5TokenGetOrderDetail()
    {
        $data = [];
        // $data[] = urlencode(isset($this->request->get['customer_ref_id']) ? $this->request->get['customer_ref_id'] : '');
        $data[] = isset($this->request->get['id']) ? $this->request->get['id'] : '';
        $data[] = $this->getApiKey();

        return $this->getToken($data);
    }

    private function getMd5TokenGetStaffsList()
    {
        $data = [];
        $data[] = isset($this->request->get['page']) ? $this->request->get['page'] : '';
        $data[] = isset($this->request->get['limit']) ? $this->request->get['limit'] : '';
        $data[] = isset($this->request->get['sort']) ? $this->request->get['sort'] : '';
        $data[] = isset($this->request->get['order']) ? $this->request->get['order'] : '';
        $data[] = $this->getApiKey();

        return $this->getToken($data);
    }

    private function getMd5TokenGetCategoriesList()
    {
        $data = [];
        $data[] = isset($this->request->get['page']) ? $this->request->get['page'] : '';
        $data[] = isset($this->request->get['limit']) ? $this->request->get['limit'] : '';
        $data[] = isset($this->request->get['category_ids']) ? $this->request->get['category_ids'] : '';
        $data[] = $this->getApiKey();

        return $this->getToken($data);
    }

    private function getMd5TokenGetManufacturersList()
    {
        $data = [];
        $data[] = isset($this->request->get['page']) ? $this->request->get['page'] : '';
        $data[] = isset($this->request->get['limit']) ? $this->request->get['limit'] : '';
        $data[] = isset($this->request->get['manufacturer_ids']) ? $this->request->get['manufacturer_ids'] : '';
        $data[] = $this->getApiKey();

        return $this->getToken($data);
    }

    private function getMd5TokenGetStaffDetail()
    {
        $data = [];
        $data[] = isset($this->request->get['id']) ? $this->request->get['id'] : '';
        $data[] = $this->getApiKey();

        return $this->getToken($data);
    }

    private function getMd5TokenGetProductsList()
    {
        $data = [];
        $data[] = isset($this->request->get['page']) ? $this->request->get['page'] : '';
        $data[] = isset($this->request->get['limit']) ? $this->request->get['limit'] : '';
        $data[] = urlencode(isset($this->request->get['searchKey']) ? $this->request->get['searchKey'] : '');
        $data[] = isset($this->request->get['searchField']) ? $this->request->get['searchField'] : '';
        $data[] = urlencode(isset($this->request->get['quantity']) ? $this->request->get['quantity'] : '');
        $data[] = urlencode(isset($this->request->get['category']) ? $this->request->get['category'] : '');
        $data[] = urlencode(isset($this->request->get['manufacturer']) ? $this->request->get['manufacturer'] : '');
        $data[] = isset($this->request->get['product_ids']) ? $this->request->get['product_ids'] : '';
        $data[] = isset($this->request->get['except_product_ids']) ? $this->request->get['except_product_ids'] : '';
        $data[] = isset($this->request->get['is_all']) ? $this->request->get['is_all'] : '';
        $data[] = isset($this->request->get['status']) ? $this->request->get['status'] : '';
        $data[] = isset($this->request->get['id']) ? $this->request->get['id'] : '';
        $data[] = isset($this->request->get['product_version_id']) ? $this->request->get['product_version_id'] : '';
        $data[] = $this->getApiKey();

        return $this->getToken($data);
    }

    private function getMd5TokenGetCustomerList()
    {
        $data = [];
        $data[] = isset($this->request->get['page']) ? $this->request->get['page'] : '';
        $data[] = isset($this->request->get['limit']) ? $this->request->get['limit'] : '';
        $data[] = urlencode(isset($this->request->get['filter_search']) ? $this->request->get['filter_search'] : '');
        $data[] = urlencode(isset($this->request->get['filter_type']) ? $this->request->get['filter_type'] : '');
        $data[] = urlencode(isset($this->request->get['phone']) ? $this->request->get['phone'] : '');
        $data[] = urlencode(isset($this->request->get['phone_operator']) ? $this->request->get['phone_operator'] : '');
        $data[] = urlencode(isset($this->request->get['order']) ? $this->request->get['order'] : '');
        $data[] = urlencode(isset($this->request->get['order_operator']) ? $this->request->get['order_operator'] : '');
        $data[] = urlencode(isset($this->request->get['order_value']) ? $this->request->get['order_value'] : '');
        $data[] = urlencode(isset($this->request->get['order_latest']) ? $this->request->get['order_latest'] : '');
        $data[] = urlencode(isset($this->request->get['order_latest_operator']) ? $this->request->get['order_latest_operator'] : '');
        $data[] = urlencode(isset($this->request->get['order_latest_value']) ? $this->request->get['order_latest_value'] : '');
        $data[] = urlencode(isset($this->request->get['amount']) ? $this->request->get['amount'] : '');
        $data[] = urlencode(isset($this->request->get['amount_operator']) ? $this->request->get['amount_operator'] : '');
        $data[] = urlencode(isset($this->request->get['amount_value']) ? $this->request->get['amount_value'] : '');
        $data[] = urlencode(isset($this->request->get['source']) ? $this->request->get['source'] : '');
        $data[] = urlencode(isset($this->request->get['source_value']) ? $this->request->get['source_value'] : '');
        $data[] = urlencode(isset($this->request->get['subscriber_id']) ? $this->request->get['subscriber_id'] : '');
        $data[] = $this->getApiKey();

        return $this->getToken($data);
    }

    private function getToken(array $data)
    {
        $token_string = implode('', $data);
        return md5($token_string);
    }

    private function getMd5TokenPost()
    {
        $apiKey = $this->getApiKey();
        $body_date_expert_token = $this->request->post;
        unset($body_date_expert_token['token']);
        $token_string = json_encode($body_date_expert_token);
        $token_string .= $apiKey;
        return md5($token_string);
    }

    private function getMd5TokenVerify()
    {
        $apiKey = $this->getApiKey();
        $token_string = isset($this->request->get['data']) ? $this->request->get['data'] : '';
        if (empty($token_string)) {
            return null;
        }

        $token_string .= $apiKey;

        return md5($token_string);
    }

    private function createNewCustomer($data)
    {
        $this->load->model('account/customer');
        $this->load->model('account/address');

        $customer = [
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'telephone' => $data['telephone'],
            'password' => token(9),
            'subscriber_id' => isset($data['subscriber_id']) ? $data['subscriber_id'] : ''
        ];

        $customer_id = $this->model_account_customer->addCustomer($customer);

        $address_data = array(
            'customer_id' => $customer_id,
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'company' => '',
            'city' => $data['city'],
            'district' => $data['district'],
            'wards' => $data['wards'],
            'postcode' => '',
            'address' => $data['address'],
            'phone' => $data['telephone'],
            'default' => true
        );

        $this->model_account_address->addAddressNew($customer_id, $address_data);

        return $customer_id;
    }

    private function getApiKey()
    {
        $this->load->model('setting/setting');

        return $this->model_setting_setting->getSettingValue(self::CONFIG_CHATBOT_NOVAON_API_KEY);
    }

    public function getMaxQuantityProduct($product, $order)
    {
        $this->load->model('catalog/product');
        if ($product['multi_versions'] == 1) {
            $product_version = $this->model_catalog_product->getProductByVersionId($product['product_id'], $order['product_version_id']);
            if ($product_version && $product['status'] == 1 && $product_version['deleted'] != 1 && $product_version['status'] == 1 && ($product_version['quantity'] > 0 || $product['sale_on_out_stock'] == 1)) {
                return true;
            } else {
                return false;
            }
        }

        if ($product['multi_versions'] == 0 && $product['deleted'] != 1 && $product['status'] == 1 && ($product['product_master_quantity'] > 0 || $product['sale_on_out_stock'] == 1)) {
            return true;
        }

        return false;
    }

    private function getDeliveryConfig()
    {
        /* get delivery methods setting from db */
        $this->load->model('setting/setting');
        $delivery_methods_config = $this->model_setting_setting->getSettingValue('delivery_methods');
        $delivery_methods_config = json_decode($delivery_methods_config, true);

        // test data
        /*$DEBUG = false;
        if ($DEBUG) {
            $delivery_methods_config = json_decode(
                '[
                    {
                        "id": "-1",
                        "name": "Giao hàng tiêu chuẩn",
                        "status": "1",
                        "fee_region": "41,000"
                    },
                    {
                        "id": 1,
                        "name": "Miễn phí vận chuyển Hà Nội",
                        "status": "1",
                        "fee_region": "0",
                        "fee_regions": [
                            {
                                "id": 1,
                                "name": "Mien phi HN",
                                "provinces": [
                                    "Hà Nội"
                                ],
                                "steps": "weight",
                                "weight_steps": [
                                    {
                                        "type": "FROM",
                                        "from": "0",
                                        "to": "1,500",
                                        "price": "0"
                                    },
                                    {
                                        "type": "FROM",
                                        "from": "1,500",
                                        "to": "2,000",
                                        "price": "0"
                                    }
                                ],
                                "price_steps": [
                                    {
                                        "type": "ABOVE",
                                        "from": "0",
                                        "to": "500,000",
                                        "price": "0"
                                    },
                                    {
                                        "type": "ABOVE",
                                        "from": "500,000",
                                        "to": "",
                                        "price": "0"
                                    }
                                ]
                            }
                        ],
                        "fee_cod": {
                            "fixed": {
                                "status": "1",
                                "value": "25,000"
                            },
                            "dynamic": {
                                "status": "0",
                                "value": "13.5"
                            }
                        }
                    }
                ]',
                true
            );
        }*/

        /* filter by status */
        $delivery_methods_config = $this->filterDeliveryConfig($delivery_methods_config, $status = 1);

        /* format number in config */
        $delivery_methods_config = $this->formatNumberForDeliveryConfig($delivery_methods_config);

        return is_array($delivery_methods_config) ? $delivery_methods_config : [];
    }

    /**
     * @param array $data format as:
     * [
     *     'products' => [
     *         [
     *             'id' => 135,
     *             'quantity' => 1
     *         ]
     *     ],
     *     'province' => 89,
     *     'into_money' => 200004,
     *     'delivery_id' => "",
     * ]
     * @return array format as:
     * if error:
     * [
     *     'code' => 201,
     *     'message' => 'Bad request. Missing products (array)!'
     * ]
     *
     * if success:
     * [
     *     'order_info' => [],
     *     'delivery_fees' => [],
     *     'error' => '',
     * ]
     */
    private function calcDeliveryFee(array $data)
    {
        // test data
        /*$data_post = [
            'products' => [
                [
                    'id' => 135,
                    'quantity' => 1
                ]
            ],
            'province' => 89,
            'into_money' => 200004,
            'delivery_id' => "",
        ];*/

        $result = [
            'order_info' => [],
            'delivery_fees' => [],
            'error' => '',
        ];

        /* validate */
        if (!isset($data['products']) || !is_array($data['products']) || empty($data['products'])) {
            return [
                'code' => 201,
                'message' => 'Bad request. Missing products (array)!'
            ];
        }

        /* data address */
        $data['province'] = isset($data['province']) ? $data['province'] : '';
        $data['into_money'] = isset($data['into_money']) ? (float)$data['into_money'] : 0;

        /* orders */
        $this->load->model('catalog/product');
        $products = $data['products'];
        $total_amount = 0;
        $total_into_money = 0;
        $total_weight = 0;
        $error_product = false;

        foreach ($products as &$c_product) {
            if (!isset($c_product['id']) || !isset($c_product['quantity'])) {
                $c_product['is_existing'] = false;
                $error_product = !$c_product['is_existing'];

                continue;
            }

            // find product
            $product_id = $c_product['id'];
            $product = $this->model_catalog_product->getProduct($product_id);
            $c_product['is_existing'] = !empty($product) && is_array($product) && isset($product['product_id']) && !empty($product['product_id']);
            if (!$error_product) {
                $error_product = !$c_product['is_existing'];
            }

            // format
            if (isset($c_product['attribute']) && $c_product['attribute']) {
                if ((int)$c_product['product_version']['price'] == 0) $c_product['product_version']['price'] = $c_product['product_version']['compare_price'];
                $total = (float)$c_product['product_version']['price'] * (int)$c_product['quantity'];
            } else {
                if ((int)$product['price'] == 0) $product['price'] = $product['compare_price_master'];
                $total = (float)$product['price'] * (int)$c_product['quantity'];
            }

            // for overall order
            $total_amount += $c_product['quantity'];
            $total_into_money += $total;
            $total_weight = ($total_amount * $product['weight']);
        }
        unset($c_product);

        $total_into_money = $data['into_money']; // tổng này được truyền lên sau khi trừ khuyễn mãi, giá tính bên trên chưa được tính khuyến mãi

        /* deliver fee */
        $transport_fee = 0;

        /* get satisfied delivery methods due to order info (weight, city) */
        $satisfied_delivery_methods = $this->getSatisfiedDeliveryMethods($data['province'], $total_weight, $total_into_money);

        // filter by transport_id if has
        $delivery_id = isset($data['delivery_id']) ? $data['delivery_id'] : null;
        if ($delivery_id != null) {
            $satisfied_delivery_methods = array_filter($satisfied_delivery_methods, function ($satisfied_delivery_method) use ($delivery_id) {
                return isset($satisfied_delivery_method['id']) &&
                    ($satisfied_delivery_method['id'] == $delivery_id ||
                        $satisfied_delivery_method['id'] == self::DELIVERY_METHOD_DEFAULT // always return default
                    );
            });
        }

        $result['order_info'] = [
            'products' => $products,
            'amount' => $total_amount,
            'into_money' => $total_into_money,
            'into_money_format' => $this->currency->format($this->tax->calculate($total_into_money, 0, $this->config->get('config_tax')), $this->session->data['currency']),
            'total_money' => $this->currency->format($this->tax->calculate($total_into_money + (str_replace(',', '', $transport_fee)), 0, $this->config->get('config_tax')), $this->session->data['currency']),
        ];

        if ($error_product) {
            $result['delivery_fees'] = [];
            $result['error'] = 'Products error!';

            return $result;
        }

        $default_delivery_method = [];
        foreach ($satisfied_delivery_methods as $satisfied_delivery_method) {
            // backup default delivery method
            if ($satisfied_delivery_method['id'] == self::DELIVERY_METHOD_DEFAULT) {
                $default_delivery_method = $satisfied_delivery_method;
            }

            // important: skip default delivery method if has other satisfied methods
            if (count($satisfied_delivery_methods) > 1 && $satisfied_delivery_method['id'] == self::DELIVERY_METHOD_DEFAULT) {
                continue;
            }

            // format fee
            $satisfied_delivery_method['fee_format'] = $this->currency->format($this->tax->calculate($satisfied_delivery_method['fee'], 0, $this->config->get('config_tax')), $this->session->data['currency']);

            $transport_fee = $satisfied_delivery_method['fee'];
            $result['delivery_fees'][] = [
                'delivery' => $satisfied_delivery_method,
                'fee' => $transport_fee,
                'fee_format' => $this->currency->format($this->tax->calculate((str_replace(',', '', $transport_fee)), 0, $this->config->get('config_tax')), $this->session->data['currency']),
            ];
        }

        // MAY NEVER REACH HERE: always return default delivery method if no satisfied delivery method found
        if (empty($result['delivery_fees'])) {
            $result['delivery_fees'][] = $default_delivery_method;
        }

        return $result;
    }

    /**
     * get Satisfied Delivery Methods due to city, weight
     *
     * @param string $city
     * @param string $total_weight
     * @param string $total_order
     * @return array format as
     * [
     *     [
     *         'id' => '1245643122', // transport id,
     *         'name' => 'HA NOI va cac tinh lan can', // $name_transport,
     *         'delivery_method' => [...], // $delivery_method_config,
     *         'fee_region' => [...], // $found_region,
     *         'fee_weight' => 34000, // $fee_price + $value_fee_cod,\
     *         'fee_cod' => 15000, // fee cod
     *         'fee' => 49000, // total fee, = fee_weight + $value_fee_cod
     *         'error' => ''
     *     ],
     *     ...
     * ]
     */
    private function getSatisfiedDeliveryMethods($city = '', $total_weight = '', $total_order = '')
    {
        /* get city name by code */
        $this->load->model('localisation/vietnam_administrative');
        $cityArr = $this->model_localisation_vietnam_administrative->getProvinceByCode($city);
        $city = '';
        if (isset($cityArr['name'])) {
            $city = $cityArr['name'];
        }

        /* get delivery methods setting from db */
        $delivery_methods_config = $this->getDeliveryConfig();

        /* get delivery fee due to city, total_weight, total_order from each delivery method */
        $value_fee_cod = 0;
        $fee_weight = 0;
        $found_delivery_methods = [];
        foreach ($delivery_methods_config as $key => $delivery_method_config) {
            if (!isset($delivery_method_config['fee_regions'])) {
                continue;
            }

            if (!isset($delivery_method_config['status']) || $delivery_method_config['status'] != '1') {
                continue;
            }

            // get delivery fee from each region
            $found_region = false;
            foreach ($delivery_method_config['fee_regions'] as $fee_region_config) {
                // find matched region due to city
                if (!in_array(trim($city), $fee_region_config['provinces'])) {
                    continue;
                }

                if (isset($fee_region_config['steps']) && $fee_region_config['steps'] == 'price') {
                    foreach ($fee_region_config['price_steps'] as $price_step_config) {
                        if (convertWeight($price_step_config['from']) <= $total_order && convertWeight($price_step_config['to']) >= $total_order) {
                            // force remove "," due to json from cb. TODO: DO NOT save value such as 30,000, save 30000 only...
                            $fee_weight = convertWeight($price_step_config['price']); // Gia thuoc tinh thanh
                            $found_region = $fee_region_config;
                            break;
                        }
                    }
                } else {
                    // find matched region due to weight
                    foreach ($fee_region_config['weight_steps'] as $weight_step_config) {
                        if (convertWeight($weight_step_config['from']) <= $total_weight && convertWeight($weight_step_config['to']) >= $total_weight) {
                            // force remove "," due to json from cb. TODO: DO NOT save value such as 30,000, save 30000 only...
                            $fee_weight = convertWeight($weight_step_config['price']); // Gia thuoc tinh thanh
                            $found_region = $fee_region_config;
                            break;
                        }
                    }
                }


                if ($found_region) {
                    break;
                }
            }

            // skip to next delivery_method_config
            if (!$found_region) {
                continue;
            }

            // more fee about COD (fixed or per order total revenue)
            foreach ($delivery_method_config['fee_cod'] as $key_cod => $fee_cod) {
                $arr_fee_status = ($delivery_method_config['fee_cod'][$key_cod]);
                if ($arr_fee_status['status'] == 1) {
                    // force remove "," due to json from cb. TODO: DO NOT save value such as 30,000, save 30000 only...
                    $arr_fee_status = str_replace(',', '', $arr_fee_status['value']);
                    if ($key_cod === 'dynamic') {
                        if ($total_order != 0) {
                            $value_fee_cod = ($arr_fee_status / 100) * $total_order;
                        } else {
                            $value_fee_cod = 0;
                        }
                    } elseif ($key_cod === 'fixed') {
                        $value_fee_cod = $arr_fee_status;
                    }
                }
            }

            // add to list
            $found_delivery_methods[] = [
                'id' => $delivery_method_config['id'],
                'name' => $delivery_method_config['name'],
                'delivery_method' => $delivery_method_config,
                'fee_region' => $found_region,
                'fee_weight' => $fee_weight,
                'fee_cod' => $value_fee_cod,
                'fee' => $fee_weight + $value_fee_cod, // total fee
                'error' => '',
            ];
        }

        $ALWAYS_RETURN_DEFAULT_DELIVERY_METHOD = true;
        if ($ALWAYS_RETURN_DEFAULT_DELIVERY_METHOD || empty($found_delivery_methods)) {
            $delivery_method_config_default = $delivery_methods_config[0];
            $found_delivery_methods[] = [
                'id' => $delivery_method_config_default['id'],
                'name' => $delivery_method_config_default['name'],
                'delivery_method' => $delivery_method_config_default,
                'fee_region' => $delivery_method_config_default['fee_region'],
                'fee_weight' => 0,
                'fee_cod' => 0,
                'fee' => $delivery_method_config_default['fee_region'], // total fee
                'error' => '',
            ];
        }

        return $found_delivery_methods;
    }

    /**
     * check if support api version
     *
     * @param string $ver
     * @return bool
     */
    private function isSupportVersion($ver)
    {
        return in_array($ver, self::$SUPPORTED_VERSIONS);
    }

    /**
     * @param array $delivery_methods_config
     * @param int|null $status 0 | 1 | null (for both 0 and 1)
     * @return mixed
     */
    private function filterDeliveryConfig(array $delivery_methods_config, $status = 1)
    {
        if (is_null($status)) {
            return $delivery_methods_config;
        }

        /* filter by status */
        $result = array_filter($delivery_methods_config, function ($dmc) use ($status) {
            return isset($dmc['status']) && $dmc['status'] == $status;
        });

        /* force array */
        $result = array_values($result);

        return $result;
    }

    /**
     * @param array $delivery_methods_config
     * @return array
     */
    private function formatNumberForDeliveryConfig(array $delivery_methods_config)
    {
        // force format number. TODO: no need if save to db already number...
        $NEED_FORMAT_NUMBER = true;
        if ($NEED_FORMAT_NUMBER) {
            foreach ($delivery_methods_config as &$dmc) {
                // fee_region
                if (isset($dmc['fee_region'])) {
                    $dmc['fee_region'] = extract_number($dmc['fee_region']);
                }

                // fee_regions
                if (isset($dmc['fee_regions']) && is_array($dmc['fee_regions'])) {
                    foreach ($dmc['fee_regions'] as &$fee_region) {
                        // weight steps
                        if (isset($fee_region['weight_steps']) && is_array($fee_region['weight_steps'])) {
                            foreach ($fee_region['weight_steps'] as &$wst) {
                                if (isset($wst['from'])) {
                                    $wst['from'] = extract_number($wst['from']);
                                }

                                if (isset($wst['to'])) {
                                    $wst['to'] = extract_number($wst['to']);
                                }

                                if (isset($wst['price'])) {
                                    $wst['price'] = extract_number($wst['price']);
                                }
                            }
                            unset($wst);
                        }

                        // price steps
                        if (isset($fee_region['price_steps']) && is_array($fee_region['price_steps'])) {
                            foreach ($fee_region['price_steps'] as &$pst) {
                                if (isset($pst['from'])) {
                                    $pst['from'] = extract_number($pst['from']);
                                }

                                if (isset($pst['to'])) {
                                    $pst['to'] = extract_number($pst['to']);
                                }

                                if (isset($pst['price'])) {
                                    $pst['price'] = extract_number($pst['price']);
                                }
                            }
                            unset($pst);
                        }
                    }
                    unset($fee_region);
                }

                // fee_cod
                if (isset($dmc['fee_cod']['fixed']['value'])) {
                    $dmc['fee_cod']['fixed']['value'] = extract_number($dmc['fee_cod']['fixed']['value']);
                }

                if (isset($dmc['fee_cod']['dynamic']['value'])) {
                    $dmc['fee_cod']['dynamic']['value'] = extract_number($dmc['fee_cod']['dynamic']['value']);
                }
            }
            unset($dmc);
        }

        return $delivery_methods_config;
    }

    /**
     * create staff
     * @return bool
     */
    private function createStaff()
    {
        // get post data from php://input
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        $validate_result = $this->validateStaffsPostData();
        if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
            $error = is_array($validate_result) ? $validate_result : [];
            $this->responseJson($error);
            return false;
        }

        $result = [
            'code' => 500,
            'message' => 'An error has occurred!'
        ];

        try {
            $data = $this->request->post['data'];
            $password = $this->generateRandomString();
            $user = array(
                'user_group_id' => $data['user_group'],
                'username' => $data['email'],
                'password' => $password,
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'email' => $data['email'],
                'telephone' => $data['telephone'],
                'image' => isset($data['avatar']) ? $data['avatar'] : '',
                'status' => 1,
                'type' => 'Staff',
                'permission' => [],
                'info' => isset($data['info']) ? $data['info'] : ''
            );

            $this->load->language('settings/account');
            $this->load->model('user/user');
            $user_id = $this->model_user_user->addUser($user);
            $this->publishCustomerStaff($user);

            // Save user_store, default is 0 (Kho trung tam)
            $user_store = array(
                'user_id' => $user_id,
                'store_id' => 0
            );
            $this->model_user_user->addUserStore($user_store);

            $result = [
                'code' => 200,
                'message' => 'Add staff successful!'
            ];
        } catch (Exception $e) {
            $result = [
                'code' => 500,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->responseJson($result);
        return true;
    }

    /**
     * edit staff
     */
    private function editStaff()
    {
        $error = $this->preHandleAndValidateStaffPostRequest();
        if ($error) {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        try {
            /* copied from controller/settings/account.php. TODO: refactor reusable code... */

            $this->load->model('user/user');
            $this->load->model('user/user_group');
            $this->load->language('settings/account');

            /* get staff by staff_id */
            $staff = $this->model_user_user->getUser($this->request->post['data']['staff_id']);
            if (!$staff) {
                $error = [
                    'code' => 506,
                    'message' => 'staff (with staff_id) does not existed'
                ];

                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($error));

                return;
            }

            /* edit group permission if need */
            $request_data = $this->request->post['data'];
            $info = getValueByKey($request_data, 'info', []);

            $is_all_permissions = getValueByKey($request_data, 'is_all_permissions', 0);
            if ($is_all_permissions == 0) {
                $permissions_data = getValueByKey($request_data, 'permission', []);
            } else {
                $permissions_data = array_merge(['allPermission'], self::SUPPORTED_PERMISSIONS);
            }

            if (!empty($permissions_data)) {
                $permission = $this->buildPermission($permissions_data);
                $permission_raw = $this->buildPermissionRaw($permissions_data);
                $user_group = [
                    'name' => $info['phone_number'],
                    'permission' => $permission
                ];
                $this->model_user_user_group->editUserGroup($staff['user_group_id'], $user_group);
            } else {
                $permission_raw = $staff['permission'];
            }

            /* edit staff */
            $staff = [
                'user_group_id' => $staff['user_group_id'],
                'username' => $staff['email'], // no change!
                'firstname' => $info['first_name'],
                'lastname' => $info['last_name'],
                'email' => $staff['email'], // no change!
                'telephone' => $info['phone_number'],
                'image' => '',
                'status' => $request_data['status'],
                'type' => 'Staff',
                'permission' => $permission_raw,
                'info' => $info['description']
            ];

            if (!empty($info['password'])) {
                $staff['password'] = $info['password'];
            }

            $this->model_user_user->editUser($request_data['staff_id'], $staff);

            $error = [
                'code' => 0,
                'message' => 'staff edited successfully'
            ];
        } catch (Exception $e) {
            $error = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($error));
    }

    /**
     * edit staff
     */
    private function deleteStaff()
    {
        $error = $this->preHandleAndValidateStaffPostRequest();
        if ($error) {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        try {
            $this->load->model('user/user');
            $this->load->model('user/user_group');

            /* get staff by staff_id */
            $staff_id = getValueByKey($this->request->post['data'], 'staff_id');
            $staff = $this->model_user_user->getUser($staff_id);
            if (!$staff) {
                $error = [
                    'code' => 506,
                    'message' => 'staff (with staff_id) does not existed'
                ];

                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($error));

                return;
            }

            /* delete staff */
            $this->model_user_user->deleteUser($staff_id);

            /* delete user group for staff */
            $user_group_id = $staff['user_group_id'];
            if (!is_null($user_group_id)) {
                $this->model_user_user_group->deleteUserGroup($user_group_id);
            }

            $this->publishCustomerStaff($staff, 'delete');

            $error = [
                'code' => 0,
                'message' => 'staff deleted successfully'
            ];
        } catch (Exception $e) {
            $error = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($error));
    }

    /**
     * get staff list
     * @return bool
     */
    private function getStaffsList()
    {
        $this->load->language('api/chatbot_novaon');

        // check parameter
        if (!$this->validateTokenGetStaffsList()) {
            $result = [
                'code' => 400,
                'message' => 'invalid token'
            ];

            $this->responseJson($result);
            return false;
        }

        $data_filter = [];
        if (isset($this->request->get['page']) && !empty($this->request->get['page'])) {
            $data_filter = [
                'start' => ((int)$this->request->get['page'] - 1) * ((int)isset($this->request->get['limit']) ? $this->request->get['limit'] : 15),
                'limit' => isset($this->request->get['limit']) ? $this->request->get['limit'] : 15
            ];
        }

        if (isset($this->request->get['sort'])) {
            $data_filter[] = $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $data_filter[] = $this->request->get['order'];
        }

        $this->load->model('api/chatbox_novaon_v2');
        $result = $this->model_api_chatbox_novaon_v2->getStaffsListData($data_filter);

        $this->responseJson($result);
        return true;
    }

    /**
     * getStaffDetail
     */
    private function getStaffDetail()
    {
        // check parameter
        if (!$this->validateTokenGetStaffDetail()) {
            $result = [
                'code' => 400,
                'message' => 'invalid token'
            ];

            $this->responseJson($result);
            return false;
        }

        $staff_id = $this->request->get['id'];
        if (empty($staff_id)) {
            $result = [
                'code' => 400,
                'message' => 'missing staff id'
            ];

            $this->responseJson($result);
            return false;
        }

        // do getting staff detail
        $data_filter = [];
        $data_filter['staff_ids'] = $staff_id;

        $this->load->language('api/chatbot_novaon');
        $this->load->model('api/chatbox_novaon_v2');
        $staff_data = $this->model_api_chatbox_novaon_v2->getStaffsListData($data_filter);
        $result = [
            'code' => 200,
            'data' => new stdClass()
        ];
        if (isset($staff_data['records'][0])) {
            $result['data'] = $staff_data['records'][0];
        }

        $this->responseJson($result);
        return true;
    }

    /**
     * validate staff post data
     * @return array|int[]
     */
    private function validateStaffsPostData()
    {
        // validate token
        if (!isset($this->request->post['token'])) {
            return [
                'code'    => 400,
                'message' => 'missing token'
            ];
        }
        if ($this->getMd5TokenPost() != $this->request->post['token']) {
            return [
                'code'    => 400,
                'message' => 'invalid token'
            ];
        }

        $this->load->model('user/user');

        /* Validate limit staff for shop */
        $config_packet_unlimited_staffs = $this->config->get('config_packet_unlimited_staffs');
        if (isset($config_packet_unlimited_staffs) && $config_packet_unlimited_staffs != '1') {
            $config_packet_number_of_staffs = (int)$this->config->get('config_packet_number_of_staffs');
            $count_staff = $this->model_user_user->getTotalStaff();
            if (isset($config_packet_number_of_staffs)) {
                if ($count_staff >= $config_packet_number_of_staffs) {
                    return [
                        'code' => 403,
                        'message' => 'Excess limit to allow creating staff accounts'
                    ];
                }
            } else {
                return [
                    'code' => 403,
                    'message' => 'Excess limit to allow creating staff accounts'
                ];
            }
        }

        // validate body params
        $this->load->language('api/account_staff_form');

        $firstname = isset($this->request->post['data']['firstname']) ? $this->request->post['data']['firstname'] : '';
        if ($firstname) {
            if (utf8_strlen($firstname) < 1 || utf8_strlen(trim($firstname)) > 32) {
                return [
                    'code' => 400,
                    'message' => $this->language->get('error_firstname')
                ];
            }
        } else {
            return [
                'code' => 400,
                'message' => 'missing or invalid firstname'
            ];
        }

        $lastname = isset($this->request->post['data']['lastname']) ? $this->request->post['data']['lastname'] : '';
        if ($lastname) {
            if (utf8_strlen($lastname) < 1 || utf8_strlen(trim($lastname)) > 32) {
                return [
                    'code' => 400,
                    'message' => $this->language->get('error_lastname')
                ];
            }
        } else {
            return [
                'code' => 400,
                'message' => 'missing or invalid lastname'
            ];
        }

        $email = isset($this->request->post['data']['email']) ? $this->request->post['data']['email'] : '';
        if ($email) {
            if (utf8_strlen($email) > 96 || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
                return [
                    'code' => 400,
                    'message' => $this->language->get('error_email')
                ];
            }

            $user_info = $this->model_user_user->getUserByEmail($email);
            if ($user_info) {
                return [
                    'code' => 403,
                    'message' => $this->language->get('error_exists')
                ];
            }
        } else {
            return [
                'code' => 400,
                'message' => 'missing or invalid email'
            ];
        }

        $telephone = isset($this->request->post['data']['telephone']) ? $this->request->post['data']['telephone'] : '';
        if ($telephone) {
            if (utf8_strlen($telephone) != 10) {
                return [
                    'code' => 400,
                    'message' => $this->language->get('error_phone')
                ];
            }
        } else {
            return [
                'code' => 400,
                'message' => 'missing or invalid telephone'
            ];
        }

        $user_group_id = isset($this->request->post['data']['user_group']) ? $this->request->post['data']['user_group'] : '';
        if ($user_group_id) {
            $this->load->model('user/user_group');
            $user_group = $this->model_user_user_group->getUserGroup($user_group_id);
            if (empty($user_group['name'])) {
                return [
                    'code' => 400,
                    'message' => 'invalid user group id'
                ];
            }
        } else {
            return [
                'code' => 400,
                'message' => 'missing user group id'
            ];
        }

        return [
            'code' => 200
        ];
    }

    /**
     * generate a random string
     * @param int $length
     * @return false|string
     */
    private function generateRandomString($length = 10) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }

    /**
     * getCategoriesList
     */
    private function getCategoriesList()
    {
        $this->load->language('api/chatbot_novaon');

        // check parameter
        if (!$this->validateTokenGetCategoriesList()) {
            $json = [
                'code' => 400,
                'message' => 'invalid token'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));

            return;
        }

        $data_filter = [];
        if (isset($this->request->get['page']) && !empty($this->request->get['page'])) {
            $data_filter = [
                'start' => ((int)$this->request->get['page'] - 1) * ((int)isset($this->request->get['limit']) ? $this->request->get['limit'] : 15),
                'limit' => isset($this->request->get['limit']) ? $this->request->get['limit'] : 15
            ];
        }

        $data_filter['category_ids'] = isset($this->request->get['category_ids']) ? $this->request->get['category_ids'] : '';

        $this->load->model('api/chatbox_novaon_v2');
        $json = $this->model_api_chatbox_novaon_v2->getCategoriesListData($data_filter);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * getCategoryDetail
     */
    private function getCategoryDetail()
    {
        $result = [
            'code' => 404,
            'message' => 'not yet supported get detail category'
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    /**
     * createCategory
     */
    private function createCategory()
    {
        $result = [
            'code' => 404,
            'message' => 'not yet supported create category'
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    /**
     * editCategory
     */
    private function editCategory()
    {
        $result = [
            'code' => 404,
            'message' => 'not yet supported edit category'
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    /**
     * deleteCategory
     */
    private function deleteCategory()
    {
        $result = [
            'code' => 404,
            'message' => 'not yet supported delete category'
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    /**
     * getManufacturersList
     */
    private function getManufacturersList()
    {
        $this->load->language('api/chatbot_novaon');

        // check parameter
        if (!$this->validateTokenGetManufacturersList()) {
            $json = [
                'code' => 400,
                'message' => 'invalid token'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));

            return;
        }

        $data_filter = [];
        if (isset($this->request->get['page']) && !empty($this->request->get['page'])) {
            $data_filter = [
                'start' => ((int)$this->request->get['page'] - 1) * ((int)isset($this->request->get['limit']) ? $this->request->get['limit'] : 15),
                'limit' => isset($this->request->get['limit']) ? $this->request->get['limit'] : 15
            ];
        }

        $data_filter['manufacturer_ids'] = isset($this->request->get['manufacturer_ids']) ? $this->request->get['manufacturer_ids'] : '';

        $this->load->model('api/chatbox_novaon_v2');
        $json = $this->model_api_chatbox_novaon_v2->getManufacturersListData($data_filter);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * getManufacturerDetail
     */
    private function getManufacturerDetail()
    {
        $result = [
            'code' => 404,
            'message' => 'not yet supported get detail manufacturer'
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    /**
     * createManufacturer
     */
    private function createManufacturer()
    {
        $result = [
            'code' => 404,
            'message' => 'not yet supported create manufacturer'
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    /**
     * editManufacturer
     */
    private function editManufacturer()
    {
        $result = [
            'code' => 404,
            'message' => 'not yet supported edit manufacturer'
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    /**
     * deleteManufacturer
     */
    private function deleteManufacturer()
    {
        $result = [
            'code' => 404,
            'message' => 'not yet supported delete manufacturer'
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    /**
     * @return array|string
     */
    private function preHandleAndValidateStaffPostRequest()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            return [
                'code' => 201,
                'message' => 'Bad request. Use POST only!'
            ];
        }

        // get post data from php://input
        // notice: could not get directly from $this->request->post or $_POST, TODO: why?...
        $this->request->post = json_decode(file_get_contents('php://input'), true);
        try {
            $this->log->write('[chatbot_novaon_v2.0] orders(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        // sample data. note: staff_id for ONLY edit
        /*$this->request->post = [
            'token' => 'd1133ac9eddba6d491219f070affc5d2',
            'data' => json_decode(
                '{
                    "token": "ASDFGHJKL1234567890"
                    "data": {
                        "info": {
                            "first_name": "Linh",
                            "last_name": "Nguyen Thuy",
                            "password": "12345678",
                            "phone_number": "0987654321",
                            "email": "nguyenthuylinh@gmail.com",
                            "description": "this staff for product management only"
                        },
                        "status": 1,
                        "staff_id": 6,
                        "is_all_permissions": 0,
                        "permission": [
                            "product_permission"
                        ],
                        "admin_password": "123456"
                    }
                }'
            )
        ];*/

        $valid_staff = $this->validateCreateOrUpdateStaffData();
        if ($valid_staff) {
            return $valid_staff;
        }

        return '';
    }

    /**
     * build Permission
     *
     * @param array $permissions_data format
     * [
     *     'home_permission',
     *     ...
     * ]
     * @return array format
     * [
     *     'access' => $permission,
     *     'modify' => $permission
     * ]
     */
    private function buildPermission(array $permissions_data)
    {
        $permission = [];

        if (in_array('home_permission', $permissions_data)) {
            $home_persmission = [
                'common/dashboard',
                'common/filemanager'
            ];
            $permission = array_merge($permission, $home_persmission);
        }

        if (in_array('order_permission', $permissions_data)) {
            $order_permission = [
                'sale/order',
                'common/filemanager'
            ];
            $permission = array_merge($permission, $order_permission);
        }

        if (in_array('product_permission', $permissions_data)) {
            $product_permission = [
                'catalog/product',
                'catalog/category',
                'catalog/collection',
                'catalog/manufacturer',
                'catalog/warehouse',
                'common/filemanager'
            ];
            $permission = array_merge($permission, $product_permission);
        }

        if (in_array('customer_permission', $permissions_data)) {
            $customer_permission = [
                'customer/custom_field',
                'customer/customer',
                'customer/customer_approval',
                'customer/customer_group',
                'common/filemanager'
            ];
            $permission = array_merge($permission, $customer_permission);
        }

        if (in_array('setting_permission', $permissions_data)) {
            $setting_permission = [
                'settings/account',
                'settings/classify',
                'settings/delivery',
                'settings/general',
                'settings/payment',
                'settings/notify',
                'settings/settings',
                'setting/setting',
                'common/filemanager'
            ];
            $permission = array_merge($permission, $setting_permission);
        }

        if (in_array('interface_permission', $permissions_data)) {
            $interface_permission = [
                // theme builder extension
                'extension/module/theme_builder_config',
                // theme builder start point
                'custom/theme',
                // section setting in theme builder
                'section/banner',
                'section/best_sales_product',
                'section/best_views_product',
                'section/blog',
                'section/detail_product',
                'section/footer',
                'section/header',
                'section/hot_product',
                'section/list_product',
                'section/new_product',
                'section/partner',
                'theme/common/preview',
                'section/sections',
                'section/slideshow',
                'section_blog/blog_category',
                'section_blog/blog_list',
                'section_blog/sections',
                'section_blog/latest_blog',
                'section_category/banner',
                'section_category/filter',
                'section_category/product_category',
                'section_category/product_list',
                'section_category/sections',
                'section_contact/contact',
                'section_contact/form',
                'section_contact/map',
                'section_contact/sections',
                'section_product_detail/related_product',
                'section_product_detail/sections',
                // theme (general) setting in theme builder
                'theme/color',
                'theme/favicon',
                'theme/preview',
                'theme/section_theme',
                'theme/social',
                'theme/text',
                // image manager (for setting favicon, banner, ...)
                'common/filemanager'
            ];
            $permission = array_merge($permission, $interface_permission);
        }

        if (in_array('menu_permission', $permissions_data)) {
            $menu_permission = [
                'custom/menu',
                'custom/group_menu', // old. TODO: remove...
                'custom/menu_item', // old. TODO: remove...
                'common/filemanager'
            ];
            $permission = array_merge($permission, $menu_permission);
        }

        if (in_array('content_permission', $permissions_data)) {
            $content_permission = [
                'online_store/contents',
                'blog/blog'
            ];
            $permission = array_merge($permission, $content_permission);
        }

        if (in_array('domain_permission', $permissions_data)) {
            $domain_permission = [
                'online_store/domain',
                'common/filemanager'
            ];
            $permission = array_merge($permission, $domain_permission);
        }

        return [
            'access' => $permission,
            'modify' => $permission
        ];
    }

    /**
     * build Permission Raw
     *
     * @param array $permissions_data format
     * [
     *     'home_permission',
     *     ...
     * ]
     * @return array
     */
    private function buildPermissionRaw(array $permissions_data)
    {
        $permission = [];

        if (in_array('allPermission', $permissions_data)) {
            $permission[] = 'allPermission';
        }

        foreach (self::SUPPORTED_PERMISSIONS as $per) {
            if (!in_array($per, $permissions_data)) {
                continue;
            }

            $permission[] = $per;
        }

        return $permission;
    }

    private function publishCustomerStaff($user, $action = 'add')
    {
        $this->load->model('setting/setting');
        $data['email'] = $user['email'];
        $data['firstname'] = $user['firstname'];
        $data['lastname'] = $user['lastname'];
        $data['shopname'] = $this->model_setting_setting->getShopName();
        $data['phone'] = $user['telephone'];
        $data['action'] = $action;
        $data_json = json_encode($data);

        $redis = $this->redis_connect();
        $redis->publish(MUL_REDIS_CUSTOMER_STAFF_PUBLISH_CHANEL, $data_json);
        $redis->close();
    }

    private function redis_connect()
    {
        $redis = new \Redis();
        $redis->connect(MUL_REDIS_HOST, MUL_REDIS_PORT);
        $redis->select(MUL_REDIS_DB);

        //check whether server is running or not
        //echo "Server is running: " . $redis->ping();

        return $redis;
    }

    public function customers()
    {
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $this->request->post = json_decode(file_get_contents('php://input'), true);
            if (!isset($this->request->post['data']['customer_id']) || empty($this->request->post['data']['customer_id'])) {
                return $this->createCustomer();
            }

        } else if (($this->request->server['REQUEST_METHOD'] == 'GET')) {
            // check parameter
            if (!$this->validateTokenGetCustomer()) {
                $json = [
                    'code' => 400,
                    'message' => 'invalid token'
                ];

                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json));

                return;
            }
            /* do getting data */
            $this->load->model('api/chatbox_novaon_v2');
               $json = $this->model_api_chatbox_novaon_v2->getCustomersListData($this->request->get);

        }

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
    }
    public function createCustomer() {
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->log->write('createCustomer(): resp: ' . 'Bad request. Use POST only!');

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $error = [];

        // get post data from php://input
        // notice: could not get directly from $this->request->post or $_POST, TODO: why?...
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('customer(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        $valid_customer = $this->validateCustomerPostData();
        if ($valid_customer) {
            $error = is_array($valid_customer) ? $valid_customer : [];

            $this->log->write('createCustomer(): validateCustomerPostData resp: ' . json_encode($error));

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $this->load->model('account/address');
        $this->load->model('customer/customer');
        $this->load->model('account/customer');
        $this->load->model('localisation/vietnam_administrative');

        // pass data post
        $data['telephone'] = isset($this->request->post['data']['customer']['phone_number']) ? $this->request->post['data']['customer']['phone_number'] : '';
        $data['customer_source'] = isset($this->request->post['data']['customer']['customer_source']) ? $this->request->post['data']['customer']['customer_source'] : '';
        $data['is_authenticated'] = isset($this->request->post['data']['customer']['is_authenticated']) ? $this->request->post['data']['customer']['is_authenticated'] : '';

        // extract first name, last name
        $full_name = isset($this->request->post['data']['customer']['name']) ? $this->request->post['data']['customer']['name'] : '';
        $full_name = trim($full_name);
        $first_and_last_name = explode(" ", $full_name);
        $first_name = array_pop($first_and_last_name);
        $last_name = implode(" ", $first_and_last_name);
        $data['fullname'] = $full_name;
        $data['lastname'] = trim($last_name);
        $data['firstname'] = trim($first_name);

        // mapping address
        $data['city'] = isset($this->request->post['data']['customer']['city']) ? $this->request->post['data']['customer']['city'] : '';
        $data['email'] = isset($this->request->post['data']['customer']['email']) ? $this->request->post['data']['customer']['email'] : '';
        $data['district'] = isset($this->request->post['data']['customer']['district']) ? $this->request->post['data']['customer']['district'] : '';
        $data['wards'] = isset($this->request->post['data']['customer']['ward']) ? $this->request->post['data']['customer']['ward'] : '';
        $data['address'] = isset($this->request->post['data']['customer']['delivery_addr']) ? $this->request->post['data']['customer']['delivery_addr'] : '';
        $data['customer_group_id'] = isset($this->request->post['data']['customer']['group_id']) ? $this->request->post['data']['customer']['group_id'] : $this->config->get('config_customer_group_id');
        $data['subscriber_id'] = isset($this->request->post['data']['customer']['subscriber_id']) ? $this->request->post['data']['customer']['subscriber_id'] : '';

        // create customer

        $customer = [
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'telephone' => $data['telephone'],
            'customer_source' => $data['customer_source'],
            'is_authenticated' => $data['is_authenticated'],
            'customer_group_id' => $data['customer_group_id'],
            'password' => token(9),
            'not_send_mail' => true,
            'subscriber_id' => $data['subscriber_id']
        ];

        $customer_id = $this->model_account_customer->addCustomer($customer);

        $address_data = array(
            'customer_id' => $customer_id,
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'company' => '',
            'city' => $data['city'],
            'district' => $data['district'],
            'wards' => $data['wards'],
            'postcode' => '',
            'address' => $data['address'],
            'phone' => $data['telephone'],
            'default' => true
        );

        $this->model_account_address->addAddressNew($customer_id, $address_data);
        $json = [
            'code' => 200,
            'message' => 'Success',
        ];
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }

    public function validateCustomerPostData()
    {
        $this->load->model('api/chatbox_novaon_v2');
        if (!isset($this->request->post['token'])) {
            return [
                'code' => 400,
                'message' => 'invalid token'
            ];
        }

        if ($this->getMd5TokenPost() != $this->request->post['token']) {
            return [
                'code' => 400,
                'message' => 'invalid token'
            ];
        }

        $customer_id = isset($this->request->post['data']['customer']['customer_id']) ? $this->request->post['data']['customer']['customer_id'] : '';

        $name = isset($this->request->post['data']['customer']['name']) ? $this->request->post['data']['customer']['name'] : '';
        if (!$name) {
            return [
                'code' => 101,
                'message' => 'missing or invalid name'
            ];
        }
        else {
            if ($this->model_api_chatbox_novaon_v2->checkPhoneExist($this->request->post['data']['customer']['phone_number'], $customer_id) != false) {
                return [
                    'code' => 125,
                    'message' => 'error phone exist'
                ];
            }
        else {
            if ($this->model_api_chatbox_novaon_v2->checkEmailExist($this->request->post['data']['customer']['email'], $customer_id) != false) {
                return [
                    'code' => 125,
                    'message' => 'error email exist'
                ];
            }
        }
        }
        return;
    }

    /**
     * api get payment method list
     */
    public function payment_methods() {
        if ($this->request->server['REQUEST_METHOD'] == 'GET') {
            // check parameter
            if (!$this->validateTokenGetPaymentMethods()) {
                $json = [
                    'code' => 400,
                    'message' => 'invalid token'
                ];

                $this->responseJson($json);
                return;
            }

            /* do getting data */
            $this->load->model('api/chatbox_novaon_v2');
            $json = [
                'code' => 200,
                'message' => 'Lấy danh sách phương thức thanh toán thành công',
                'data' => $this->model_api_chatbox_novaon_v2->getPaymentMethods()
            ];

            $this->responseJson($json);
        }
    }

    private function validateTokenGetPaymentMethods()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }

        if ($this->getPaymentMethodListMd5Token() !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    private function getPaymentMethodListMd5Token()
    {
        $data = [];
        $data[] = $this->getApiKey();

        return $this->getToken($data);
    }

    /**
     * get shipping service list
     * @return bool
     */
    public function shipping_services() {
        if ('GET' == $this->request->server['REQUEST_METHOD']) {
            // check parameter
            if (!$this->validateTokenGetShippingServices()) {
                $json = [
                    'code' => 400,
                    'message' => 'invalid token'
                ];

                $this->responseJson($json);
                return false;
            }

            $data = $this->request->get;

            $result = [];
            $json = [
                'code' => 200,
                'data' => $result
            ];

            try {
                $data['transport_list_active'] = $this->config->get('config_transport_active');
                if (!empty($data['transport_list_active'])) {
                    $data['return_json'] = true;
                    $this->load->model('appstore/my_app');
                    $transport_list = explode(',', $data['transport_list_active']);
                    foreach ($transport_list as $key => $transport_name) {
                        $trans_app_code = $transport_name;
                        if ($transport_name == 'Ghn') {
                            $trans_app_code = ModelAppstoreMyApp::APP_GHN;
                        }

                        if ($transport_name == 'viettel_post') {
                            $trans_app_code = ModelAppstoreMyApp::APP_VIETTEL_POST;
                        }

                        if (!$this->model_appstore_my_app->checkAppActive($trans_app_code)) {
                            unset($transport_list[$key]);
                        }
                    }

                    foreach ($transport_list as $delivery_name) {
                        if (in_array($delivery_name, ['Ghn', 'viettel_post'])) {
                            $data['token'] = $this->config->get('config_' . strtoupper($delivery_name) . '_token');
                        } else {
                            $data['token'] = $this->config->get('config_' . $delivery_name . '_token');
                        }
                        $transport = new Transport($delivery_name, $data, $this);
                        if (isset($data['token']) && !empty($data['token'])) {
                            $result[] = $transport->getListService($data);
                        }
                    }

                    $json = [
                        'code' => 200,
                        'data' => $result
                    ];
                }
            } catch (Exception $e) {
                $json = [
                    'code' => 500,
                    'message' => $this->language->get('error')
                ];
            }

            $this->responseJson($json);
        }

        return true;
    }

    private function validateTokenGetShippingServices()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }

        if ($this->getShippingServiceListMd5Token() !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    private function getShippingServiceListMd5Token()
    {
        $data = [];
        $data[] = isset($this->request->get['total_weight']) ? $this->request->get['total_weight'] : 0;
        $data[] = isset($this->request->get['package_length']) ? $this->request->get['package_length'] : 0;
        $data[] = isset($this->request->get['package_width']) ? $this->request->get['package_width'] : 0;
        $data[] = isset($this->request->get['package_height']) ? $this->request->get['package_height'] : 0;
        $data[] = isset($this->request->get['from_district_id_ghn']) ? $this->request->get['from_district_id_ghn'] : '';
        $data[] = isset($this->request->get['total_pay']) ? $this->request->get['total_pay'] : 0;
        $data[] = urlencode(isset($this->request->get['transport_shipping_full_address']) ? $this->request->get['transport_shipping_full_address'] : '');
        $data[] = isset($this->request->get['transport_shipping_wards']) ? $this->request->get['transport_shipping_wards'] : '';
        $data[] = isset($this->request->get['transport_shipping_district']) ? $this->request->get['transport_shipping_district'] : '';
        $data[] = isset($this->request->get['transport_shipping_province']) ? $this->request->get['transport_shipping_province'] : '';
        $data[] = $this->getApiKey();

        return $this->getToken($data);
    }

    /**
     * create delivery order
     * cancel delivery order
     * @return bool
     */
    public function delivery_order() {
        $this->load->language('common/delivery_api');

        if ('POST' == $this->request->server['REQUEST_METHOD']) {
            // get post data from php://input
            $this->request->post = json_decode(file_get_contents('php://input'), true);

            $validate_result = $this->validateDeliveryOrderCreateData();
            if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
                $error = is_array($validate_result) ? $validate_result : [];
                $this->responseJson($error);
                return false;
            }

            $json = [
                'code' => 400,
                'message' => 'Error selecting service!'
            ];
            try {
                $data = $this->request->post['data'];

                if (isset($data['transport_method']) && !empty($data['transport_method'])) {
                    $data['token'] = $this->config->get('config_' . strtoupper($data['transport_method']) . '_token');
                    $transport = new Transport($data['transport_method'], $data, $this);
                    if (!$transport->getAdaptor() instanceof \Transport\Abstract_Transport) {
                        $this->responseJson($json);
                        return false;
                    }

                    $result = $transport->getAdaptor()->createOrderDelivery($data);
                    if ($result['status']) {
                        $json = [
                            'code' => 200,
                            'message' => isset($result['msg']) ? $result['msg'] : 'Create order successfully'
                        ];
                        //edit order
                        $data['order_status'] = ModelSaleOrder::ORDER_STATUS_ID_DELIVERING;
                        $data['is_create_order_delivery'] = true;
                        $this->load->model('sale/order');
                        $this->model_sale_order->editOrder($data['order_id'], $data);

                        if (isset($data['collection_amount']) && isset($data['order_id'])) {
                            $this->model_sale_order->updateCollectionAmount($data['order_id'], $data['collection_amount']);
                        }

                        // return data
                        $this->load->model('customer/customer');
                        $customer = $this->model_customer_customer->getCustomer($data['customer']);

                        $return_data = [];
                        $return_data['customer_id'] = $data['customer'];
                        $return_data['firstname'] = $customer['firstname'];
                        $return_data['lastname'] = $customer['lastname'];
                        $return_data['fullname'] = $data['customer_full_name'];
                        $return_data['email'] = $data['customer_email'];
                        $return_data['telephone'] = $data['customer_phone'];
                        $return_data['transport_order_code'] = $result['order_code'];
                        $return_data['transport_name'] = $data['transport_method'];
                        $return_data['transport_service_name'] = $data['service_name'];
                        $return_data['transport_display_name'] = $transport->getAdaptor()->getDisplayName();
                        $return_data['transport_collection_amount'] = $data['collection_amount'];
                        $return_data['transport_order_active'] = true;
                        $return_data['transport_order_detail'] = [
                            'PickWarehouseName' => $result['delivery_address'],
                            'OrderCode' => $result['order_code'],
                            'CurrentStatus' => $result['transport_status'],
                            'CurrentStatusLanguage' => $transport->getAdaptor()->getTransportStatusMessage($result['transport_status']),
                            'Cancelable' => $transport->getAdaptor()->isCancelableOrder($result['transport_status']) ? 1 : 0,
                            'CollectionAmount' => $data['collection_amount']
                        ];

                        $json['return_data'] = $return_data;
                    } else {
                        $json = [
                            'code' => 500,
                            'message' => isset($result['msg']) ? $result['msg'] : 'An unknown error!'
                        ];
                    }
                } else {
                    $json = [
                        'code' => 400,
                        'message' => 'missing transport method'
                    ];
                }
            } catch (Exception $e) {
                $json = [
                    'code' => 500,
                    'message' => 'An unknown error!'
                ];
            }

            $this->responseJson($json);
        }

        if ('PUT' == $this->request->server['REQUEST_METHOD']) {
            // get post data from php://input
            $this->request->post = json_decode(file_get_contents('php://input'), true);

            $validate_result = $this->validateDeliveryOrderCancelData();
            if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
                $error = is_array($validate_result) ? $validate_result : [];
                $this->responseJson($error);
                return false;
            }

            $json = [
                'code' => 400,
                'message' => 'Error!'
            ];
            try {
                $data = $this->request->post['data'];

                if (isset($data['transport_method']) && !empty($data['transport_method'])) {
                    $data['token'] = $this->config->get('config_' . strtoupper($data['transport_method']) . '_token');
                    $transport = new Transport($data['transport_method'], $data, $this);
                    $result = $transport->cancelOrder($data);
                    $json = [
                        'code' => $result['status'] ? 200 : 500,
                        'message' => isset($result['msg']) ? $result['msg'] : ($result['status'] ? 'Cancel delivery order successfully' : $this->language->get('text_error_unknown'))
                    ];
                }

            } catch (Exception $e) {
                $json = [
                    'code' => 500,
                    'message' => 'An unknown error!'
                ];
            }

            $this->responseJson($json);
        }

        return true;
    }

    private function validateDeliveryOrderCreateData()
    {
        // validate token
        if (!isset($this->request->post['token'])) {
            return [
                'code' => 400,
                'message' => 'missing token'
            ];
        }
        if ($this->getMd5TokenPost() != $this->request->post['token']) {
            return [
                'code' => 400,
                'message' => 'invalid token'
            ];
        }

        // validate body params
        if (empty($this->request->post['data']['total_weight'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid total weight (must be greater than 0)'
            ];
        }

        if (!isset($this->request->post['data']['shipping_fee'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid shipping_fee'
            ];
        }

        if (!isset($this->request->post['data']['shipping_method'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid shipping_method'
            ];
        }

        if (empty($this->request->post['data']['total_pay'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid total_pay'
            ];
        }

        if (empty($this->request->post['data']['order_source'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid order_source'
            ];
        }

        if (empty($this->request->post['data']['payment_method'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid payment_method'
            ];
        }

        if (!isset($this->request->post['data']['payment_status'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid payment_status'
            ];
        }

        if (empty($this->request->post['data']['shipping_district_code'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid shipping_district_code'
            ];
        }

        $order_id = isset($this->request->post['data']['order_id']) ? $this->request->post['data']['order_id'] : '';
        if ($order_id) {
            $this->load->model('sale/order');
            $order = $this->model_sale_order->getOrder($order_id);
            if (empty($order)) {
                return [
                    'code' => 400,
                    'message' => 'order does not exist'
                ];
            }
        } else {
            return [
                'code' => 400,
                'message' => 'missing or invalid order_id'
            ];
        }

        $transport_method = isset($this->request->post['data']['transport_method']) ? $this->request->post['data']['transport_method'] : '';
        if ($transport_method) {
            $transport_list = explode(',', $this->config->get('config_transport_active'));
            if (in_array($transport_method, $transport_list)) {
                if ('Ghn' == $transport_method) {
                    if (empty($this->request->post['data']['ghn_district_hub'])) {
                        return [
                            'code' => 400,
                            'message' => 'missing or invalid ghn_district_hub'
                        ];
                    }
                }
            } else {
                return [
                    'code' => 400,
                    'message' => 'not support transport_method'
                ];
            }
        } else {
            return [
                'code' => 400,
                'message' => 'missing or invalid transport_method'
            ];
        }

        if (!isset($this->request->post['data']['service_id'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid service_id'
            ];
        }

        if (empty($this->request->post['data']['service_name'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid service_name'
            ];
        }

        if (empty($this->request->post['data']['transport_shipping_fullname'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid transport_shipping_fullname'
            ];
        }

        if (empty($this->request->post['data']['transport_shipping_phone'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid transport_shipping_phone'
            ];
        }

        if (empty($this->request->post['data']['transport_shipping_address'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid transport_shipping_address'
            ];
        }

        if (!isset($this->request->post['data']['transport_shipping_wards'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid transport_shipping_wards'
            ];
        }

        if (!isset($this->request->post['data']['transport_shipping_district'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid transport_shipping_district'
            ];
        }

        if (!isset($this->request->post['data']['transport_shipping_province'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid transport_shipping_province'
            ];
        }

        if (!isset($this->request->post['data']['collection_amount'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid collection_amount'
            ];
        }

        if (empty($this->request->post['data']['package_length'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid package_length'
            ];
        }

        if (empty($this->request->post['data']['package_width'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid package_width'
            ];
        }

        if (empty($this->request->post['data']['package_height'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid package_height'
            ];
        }

        if (!isset($this->request->post['data']['transport_note'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid transport_note'
            ];
        }

        if (!isset($this->request->post['data']['order_status'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid order_status'
            ];
        }

        if (!isset($this->request->post['data']['old_order_status_id'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid old_order_status_id'
            ];
        }

        if (!isset($this->request->post['data']['user_id'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid user_id'
            ];
        }

        if (!isset($this->request->post['data']['customer'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid customer'
            ];
        }

        if (!isset($this->request->post['data']['customer_group_id'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid customer_group_id'
            ];
        }

        if (empty($this->request->post['data']['shipping_fullname'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid shipping_fullname'
            ];
        }

        if (!isset($this->request->post['data']['shipping_phone'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid shipping_phone'
            ];
        }

        if (!isset($this->request->post['data']['shipping_address'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid shipping_address'
            ];
        }

        if (!isset($this->request->post['data']['shipping_wards'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid shipping_wards'
            ];
        }

        if (!isset($this->request->post['data']['shipping_district'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid shipping_district'
            ];
        }

        if (!isset($this->request->post['data']['shipping_province'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid shipping_province'
            ];
        }

        if (!isset($this->request->post['data']['shipping_bill'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid shipping_bill'
            ];
        }

        if (!isset($this->request->post['data']['note'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid note'
            ];
        }

        if (!isset($this->request->post['data']['delivery'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid delivery'
            ];
        }

        if (!isset($this->request->post['data']['customer_full_name'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid customer_full_name'
            ];
        }

        if (!isset($this->request->post['data']['customer_phone'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid customer_phone'
            ];
        }

        if (!isset($this->request->post['data']['customer_email'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid customer_email'
            ];
        }

        if (!isset($this->request->post['data']['customer_province'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid customer_province'
            ];
        }

        if (!isset($this->request->post['data']['customer_address'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid customer_address'
            ];
        }

        return [
            'code' => 200
        ];
    }

    private function validateDeliveryOrderCancelData()
    {
        // validate token
        if (!isset($this->request->post['token'])) {
            return [
                'code'    => 400,
                'message' => 'missing token'
            ];
        }
        if ($this->getMd5TokenPost() != $this->request->post['token']) {
            return [
                'code'    => 400,
                'message' => 'invalid token'
            ];
        }

        // validate body params
        $transport_method = isset($this->request->post['data']['transport_method']) ? $this->request->post['data']['transport_method'] : '';
        if ($transport_method) {
            $transport_list = explode(',', $this->config->get('config_transport_active'));
            if (!in_array($transport_method, $transport_list)) {
                return [
                    'code' => 400,
                    'message' => 'not support transport_method'
                ];
            }
        } else {
            return [
                'code' => 400,
                'message' => 'missing or invalid transport_method'
            ];
        }

        if (empty($this->request->post['data']['transport_order_code'])) {
            return [
                'code' => 400,
                'message' => 'missing or invalid transport_order_code'
            ];
        }

        $order_id = isset($this->request->post['data']['data_order_id']) ? $this->request->post['data']['data_order_id'] : '';
        if ($order_id) {
            $this->load->model('sale/order');
            $order = $this->model_sale_order->getOrder($order_id);
            if (empty($order)) {
                return [
                    'code' => 400,
                    'message' => 'order does not exist'
                ];
            }
        } else {
            return [
                'code' => 400,
                'message' => 'missing or invalid order_id'
            ];
        }

        return [
            'code' => 200
        ];
    }

    /**
     * get user group list
     * @return bool
     */
    public function user_groups() {
        if ('GET' == $this->request->server['REQUEST_METHOD']) {
            // check parameter
            if (!$this->validateTokenGetUserGroups()) {
                $json = [
                    'code' => 400,
                    'message' => 'invalid token'
                ];

                $this->responseJson($json);
                return false;
            }

            $json = [
                'code' => 200,
                'data' => []
            ];

            try {
                $this->load->model('user/user_group');
                $user_groups = $this->model_user_user_group->getUserGroupWithCountStaff();
                if (empty($user_groups)) {
                    // create a user group with default permissions
                    $permission = array (
                        'access' =>
                            array (
                                0 => 'sale/order',
                                1 => 'sale/return_receipt',
                                2 => 'catalog/product',
                                3 => 'catalog/warehouse',
                                4 => 'catalog/collection',
                                5 => 'catalog/manufacturer',
                                6 => 'catalog/store_receipt',
                                7 => 'catalog/store_transfer_receipt',
                                8 => 'catalog/store_take_receipt',
                                9 => 'catalog/cost_adjustment_receipt',
                                10 => 'catalog/store',
                                11 => 'customer/customer',
                                12 => 'customer/customer_group',
                                13 => 'cash_flow/cash_flow',
                                14 => 'cash_flow/receipt_voucher',
                                15 => 'cash_flow/payment_voucher',
                                16 => 'report/overview',
                                17 => 'report/order',
                                18 => 'report/store',
                                19 => 'report/product',
                                20 => 'report/staff',
                                21 => 'report/financial',
                                22 => 'discount/discount',
                                23 => 'discount/setting',
                                24 => 'discount/coupon',
                                25 => 'section/sections',
                                26 => 'custom/theme',
                                27 => 'blog/blog',
                                28 => 'blog/category',
                                29 => 'online_store/contents',
                                30 => 'custom/menu',
                                31 => 'online_store/domain',
                                32 => 'custom/preference',
                                33 => 'common/filemanager',
                                34 => 'extension/module/theme_builder_config',
                                35 => 'rate/rate',
                                36 => 'sale_channel/pos_novaon',
                                37 => 'setting/setting',
                                38 => 'settings/account',
                                39 => 'settings/classify',
                                40 => 'settings/delivery',
                                41 => 'settings/general',
                                42 => 'settings/payment',
                                43 => 'settings/notify',
                                44 => 'settings/settings'
                            ),
                        'modify' =>
                            array (
                                0 => 'sale/order',
                                1 => 'sale/return_receipt',
                                2 => 'catalog/product',
                                3 => 'catalog/warehouse',
                                4 => 'catalog/collection',
                                5 => 'catalog/manufacturer',
                                6 => 'catalog/store_receipt',
                                7 => 'catalog/store_transfer_receipt',
                                8 => 'catalog/store_take_receipt',
                                9 => 'catalog/cost_adjustment_receipt',
                                10 => 'catalog/store',
                                11 => 'customer/customer',
                                12 => 'customer/customer_group',
                                13 => 'cash_flow/cash_flow',
                                14 => 'cash_flow/receipt_voucher',
                                15 => 'cash_flow/payment_voucher',
                                16 => 'report/overview',
                                17 => 'report/order',
                                18 => 'report/store',
                                19 => 'report/product',
                                20 => 'report/staff',
                                21 => 'report/financial',
                                22 => 'discount/discount',
                                23 => 'discount/setting',
                                24 => 'discount/coupon',
                                25 => 'section/sections',
                                26 => 'custom/theme',
                                27 => 'blog/blog',
                                28 => 'blog/category',
                                29 => 'online_store/contents',
                                30 => 'custom/menu',
                                31 => 'online_store/domain',
                                32 => 'custom/preference',
                                33 => 'common/filemanager',
                                34 => 'extension/module/theme_builder_config',
                                35 => 'rate/rate',
                                36 => 'sale_channel/pos_novaon',
                                37 => 'setting/setting',
                                38 => 'settings/account',
                                39 => 'settings/classify',
                                40 => 'settings/delivery',
                                41 => 'settings/general',
                                42 => 'settings/payment',
                                43 => 'settings/notify',
                                44 => 'settings/settings'
                            )
                    );
                    $user_group = array(
                        'name' => 'Nhóm nhân viên mặc định',
                        'permission' => $permission,
                        'access_all' => 0
                    );

                    $this->model_user_user_group->addUserGroup($user_group);
                    $user_groups = $this->model_user_user_group->getUserGroupWithCountStaff();
                    $json['data'] = $user_groups;
                } else {
                    $json['data'] = $user_groups;
                }
            } catch (Exception $e) {
                $json = [
                    'code' => 500,
                    'message' => 'An unknown error!'
                ];
            }

            $this->responseJson($json);
        }

        return true;
    }

    private function validateTokenGetUserGroups()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }

        if ($this->getUserGroupListMd5Token() !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    private function getUserGroupListMd5Token()
    {
        $data = [];
        $data[] = $this->getApiKey();

        return $this->getToken($data);
    }

    /**
     * get dashboard info filter by date range
     *
     * @return bool
     */
    public function dashboard() {
        if ('GET' == $this->request->server['REQUEST_METHOD']) {
            // check parameter
            if (!$this->validateTokenGetDashboard()) {
                $json = [
                    'code' => 400,
                    'message' => 'invalid token'
                ];

                $this->responseJson($json);
                return false;
            }

            $json = [
                'code' => 200,
                'data' => []
            ];

            try {
                $filter = $this->request->get;

                // default params
                if (empty($this->request->get['start']) || empty($this->request->get['end'])) {
                    $timezone = ini_get('date.timezone') ? ini_get('date.timezone') : date_default_timezone_get();
                    $now = new DateTime('now', new DateTimeZone($timezone));
                    $filter['end'] = $now->format('Y-m-d');
                    $filter['start'] = $now->modify('- 7 day')->format('Y-m-d');
                }

                if (isset($this->request->get['user_id'])) {
                    $user_id = preg_replace('/[^0-9]/', '', $this->request->get['user_id']);
                    if ($user_id !== '') {
                        $filter['user_id'] = $user_id;
                    }
                }
                if (isset($this->request->get['store_id'])) {
                    $store_id = preg_replace('/[^0-9]/', '', $this->request->get['store_id']);
                    if ($store_id !== '') {
                        $filter['store_id'] = $store_id;
                    }
                }

                $filter['source'] = 'chatbot';

                $json['data'] = [
                    'revenue' => $this->getDashboardRevenue($filter),
                    'customer' => $this->getDashboardCustomer($filter),
                    'order' => $this->getDashboardOrder($filter)
                ];

            } catch (Exception $e) {
                $json = [
                    'code' => 500,
                    'message' => 'An unknown error!'
                ];
            }

            $this->responseJson($json);
        }

        return true;
    }

    private function getDashboardRevenue($filter)
    {
        $this->load->model('report/overview');

        $result = $this->model_report_overview->getRevenue($filter, self::REVENUE_ID);

        return $result;
    }

    private function getDashboardOrder($filter)
    {
        $this->load->model('report/order');

        /* Get count order*/
        return $this->model_report_order->getOrdersByDate($filter);
    }

    private function getDashboardCustomer($filter)
    {
        $this->load->model('report/order');

        /* Handle count customer */
        return $this->model_report_order->getCustomersByDate($filter);
    }

    private function validateTokenGetDashboard()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }

        if ($this->getDashboardMd5Token() !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    private function getDashboardMd5Token()
    {
        $data = [];
        $data[] = urlencode(isset($this->request->get['start']) ? $this->request->get['start'] : '');
        $data[] = urlencode(isset($this->request->get['end']) ? $this->request->get['end'] : '');
        $data[] = $this->getApiKey();

        return $this->getToken($data);
    }
}