<?php

/**
 * base url: /api/pos_v1_product/xxx
 * Class ControllerApiV1Product
 */
class ControllerApiPosV1 extends Controller
{
    use Setting_Util_Trait;

    const CONFIG_BESTME_POS_API_KEY = 'config_pos_api_key';

    /* ----- api ----- */
    /**
     * verify api key
     */
    public function verify()
    {
        $json = [
            'code' => 1,
            'message' => 'credential (api key) is verified'
        ];

        // check parameter
        if (!$this->validateTokenVerify()) {
            $json = [
                'code' => 400,
                'message' => 'invalid token'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * get list isPOSEnabled
     */
    public function isPOSEnabled()
    {
        $result = $this->isEnableModule(self::$MODULE_POS);

        $json = [
            'result' => $result == 1
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    // TODO: add verify token to all get methods...

    /**
     * get list products. TODO: complete...
     */
    public function products()
    {
        $this->load->model('catalog/collection');
        $this->load->language('common/shop');
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('discount/discount');
        $this->load->model('tool/image');
        $this->document->setTitle($this->language->get('text_shop'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->setKeywords($this->config->get('config_meta_keyword'));
        $this->load->model('extension/module/theme_builder_config');
        try {
            if (isset($this->request->get['route'])) {
                $this->document->addLink($this->config->get('config_url'), 'canonical');
            }

            // banner
            $shop_banner_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CATEGORY_BANNER);
            $shop_banner_config = json_decode($shop_banner_config, true);
            if (isset($shop_banner_config['display'])) {
                $data['banner'] = reset($shop_banner_config['display']);
            }

            //category
            $product_category_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CATEGORY_PRODUCT_CATEGORY);
            $product_category_config = json_decode($product_category_config, true);

            if (isset($product_category_config['setting']['title'])) {
                $data['category_title'] = $product_category_config['setting']['title'];
            }

            if (isset($product_category_config['display']['menu']['id'])) {
                $this->load->model('custom/common');
                $category_lists = $this->model_custom_common->getListMenuItemByGroupMenuId($product_category_config['display']['menu']['id']);
                $data['category_lists'] = $this->categoryMapList($category_lists);
            }

            // manufacture, prime  (category filter)
            $category_filter_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CATEGORY_FILTER);
            $category_filter_config = json_decode($category_filter_config, true);

            if (isset($category_filter_config['display']['supplier']['title'])) {
                $data['manufacture_title'] = $category_filter_config['display']['supplier']['title'];
            }

            $data['manufacture_visible'] = $category_filter_config['display']['supplier']['visible'];
            $manufactures = $this->model_catalog_product->getManufacturer();
            foreach ($manufactures as &$manufacture) {
                $manufacture['url'] = $this->url->link('common/shop', 'manufacture=' . $manufacture['manufacturer_id'], true);
            }
            $data['manufactures'] = $manufactures;
            unset($manufactures);

            if (isset($category_filter_config['display']['product-price']['title'])) {
                $data['prime_title'] = $category_filter_config['display']['product-price']['title'];
            }

            $filter['channel'] = implode(',', ModelCatalogProduct::$CHANNELS_FOR_POS);

            if (isset($this->request->get['store_id'])) {
                $filter['store_id'] = $this->request->get['store_id'];
            }

            //product
            if (isset($this->request->get['page'])) {
                $page = $this->request->get['page'];
            } else {
                $page = 1;
            }

            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = 1e6; // TODO: set 1e2, 1e3 instead of 1e6?...
            }

            $filter['start'] = ($page - 1) * (int)$limit;
            $filter['limit'] = $limit;

            $products = $this->model_catalog_product->getAPIProducts($filter);
            $customer_total = $this->model_catalog_product->getAPIProductsTotal($filter);
            $data['total_product'] = $customer_total;
            $data['currency'] = $this->session->data['currency'];

            $data['products'] = [];
            foreach ($products as $product) {
                // skip product disabled. TODO: do by sql...
                if (!array_key_exists('status', $product) || (!is_null($product['status']) && $product['status'] != 1)) {
                    continue;
                }

                if ($product['image']) {
                    $image = $this->model_tool_image->resize($product['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
                }

                if ($product['multi_versions'] == 1) {
                    $SumProduct_version = $this->model_catalog_product->getSumProduct_version($product['product_id']);
                } else {
                    $SumProduct_version = $product['product_master_quantity'];
                }

                $categories = [];
                $cats = $this->model_catalog_product->getCategoriesCustomForPos($product['product_id']);

                if (isset($cats) && $cats) {
                    foreach ($cats as $cat) {
                        if (!isset($cat) || !is_array($cat) || count($cat) < 1) {
                            continue;
                        }

                        foreach ($cat as $key => $item) {
                            if (!array_search($key, $categories)) {
                                array_push($categories, (string)$key);
                            }
                        }

                        //array_push($categories, $cat['category_id']);
//                        $categories[] = [
//                            'id' => $cat['category_id'],
//                            'name' => $cat['name'],
//                            'href' => $this->url->link('common/shop', 'category=' . $cat['category_id'], true),
//                        ];
                    }
                }

                /*$data['products'][] = array(
                    'product_id' => $product['product_id'],
                    'image' => $image,
                    'name' => $product['name'],
                    'multi_versions' => $product['multi_versions'],
                    'quantity' => ($product['multi_versions'] == 1) ? $product['product_version_quantity'] : $product['product_master_quantity'],
                    'description' => utf8_substr(trim(strip_tags(html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                    'sub_description' => $product['sub_description'],
                    'price_compare_zero' => $product['price'],
                    'price' => $this->currency->formatCustom($product['price_master'], $this->session->data['currency']),
                    'compare_price' => $this->currency->formatCustom($product['compare_price_master'], $this->session->data['currency']),
                    'href' => $this->url->link('product/product', 'product_id=' . $product['product_id']),
                    'categories' => $categories
                );*/

                $collections = $this->model_catalog_product->getCollectionByProductId($product['product_id']);
                $collections_result = [];
                if ($collections) {
                    foreach ($collections as $collection) {
                        if (!isset($collection['id'])) {
                            continue;
                        }

                        array_push($collections_result, $collection['id']);
                    }
                }

                // manufacturer
                $manufacturers = empty($product['manufacturer_id']) ? [] : [$product['manufacturer_id']];

                // store_id. TODO: get Store Ids By ProductId and ProductVersionId?...
                $store_ids = [];
                $p_to_stores = $this->model_catalog_product->getStoreIdsByProductId($product['product_id']);
                if ($p_to_stores) {
                    foreach ($p_to_stores as $p_to_store) {
                        if (!isset($p_to_store['store_id'])) {
                            continue;
                        }

                        array_push($store_ids, $p_to_store['store_id']);
                    }
                }
                $store_ids = array_values(array_unique($store_ids));

                // version
                $versions_to_return = [];
                if (!empty($product['name_version']) && is_string($product['name_version'])) {
                    $versions = explode(',', $product['name_version']);
                    if (is_array($versions)) {
                        foreach ($versions as $idx => $version) {
                            $versions_to_return["Thuộc tính $idx"] = trim($version);
                        }
                    }
                }

                // price
                $sale_price = ($product['multi_versions'] == 1) ? $product['price'] : $product['price_master'];
                $price = ($product['multi_versions'] == 1) ? $product['compare_price'] : $product['compare_price_master'];
                if ($price < 1) {
                    // skip product price = 0 (feature: contact for order)
                    continue;
                }

                $sale_price = empty(number_format($sale_price)) ? $price : $sale_price;

                // quantity_with_stores
                $quantity_by_store = [];
                foreach ($store_ids as $store_id) {
                    $quantity_by_store[sprintf('%s', (int)$store_id)] = $this->model_catalog_product->getProductQuantityByStore($product['product_id'], $product['product_version_id'], $store_id);
                }

                // discounts
                $discounts = [];

                $data['products'][] = [
                    'id' => $product['product_id'],
                    'name' => $product['name'],
                    'image' => $image,
                    'sku' => $product['sku'],
                    'barcode' => $product['barcode'],
                    'version' => $versions_to_return,
                    'version_id' => empty($product['product_version_id']) ? null : $product['product_version_id'],
                    'sale_price' => intval($sale_price),
                    'price' => intval($price),
                    //'quantity' => ($product['multi_versions'] == 1) ? $product['product_version_quantity'] : $product['product_master_quantity'],
                    'quantity' => 0, // will be set later in POS due to quantity_by_store and store_id
                    'quantity_by_store' => $quantity_by_store,
                    'category' => $categories,
                    'collection' => $collections_result,
                    'manufacturer' => $manufacturers,
                    'store_id' => $store_ids,
                    // new for discount feature
                    'discounts' => array_keys($discounts), // NOTICE: related discounts, not satisfied or selected discounts
                    'selected_discounts' => []
                ];
            }

            $json = [
                'result' => $data['products']
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request',
                '__error' => $e->getMessage()
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * get list categories
     */
    public function categories()
    {
        /* get header config */
        $this->load->model('catalog/category');
        try {
            /* display main menu */
            $related_category = $this->model_catalog_category->getCategoriesForClassify([]);

            $result = [];
            if ($related_category) {
                foreach ($related_category as $category) {
                    $result[] = [
                        'id' => $category['id'],
                        'name' => $category['text'],
                        'sub_categories' => $this->getCategoryInfo($category)
                    ];
                }
            }

            $json = [
                'result' => $result
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * get list suppliers (manufacturers)
     */
    public function suppliers()
    {
        $this->load->model('catalog/manufacturer');
        try {
            $url = '';
            $result = [];
            $filter = [
                'start' => 0,
                'limit' => 1e6
            ];
            $related_manufacturer = $this->model_catalog_manufacturer->getManufacturers($filter);
            if ($related_manufacturer) {
                foreach ($related_manufacturer as $manufacturer) {
                    if (isset($this->request->get['manufacture']) && $this->request->get['manufacture'] == $manufacturer['manufacturer_id']) {
                        $manufacturer_url = $this->url->link('common/shop', modQuery($url, 'manufacture'), true);
                        $result['manufacturer_selected_name'] = $manufacturer['name'];
                    } else {
                        $manufacturer_url = $this->url->link('common/shop', modQuery($url, 'manufacture') . '&manufacture=' . $manufacturer['manufacturer_id'], true);
                    }
                    /*$result[] = array(
                        'supplier_id' => $manufacturer['manufacturer_id'],
                        'title' => $manufacturer['name'],
                        'url' => $manufacturer_url,
                    );*/
                    $result[] = array(
                        'id' => $manufacturer['manufacturer_id'],
                        'name' => $manufacturer['name']
                    );
                }
            }

            $json = [
                'result' => $result
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * get list suppliers (manufacturers)
     * alias of api method suppliers
     */
    public function manufacturers()
    {
        return $this->suppliers();
    }

    /**
     * get list collections
     */
    public function collections()
    {
        $this->load->model('catalog/collection');
        try {
            $url = '';
            $result = [];
            $related_collection = $this->model_catalog_collection->getAllcollection();
            if ($related_collection) {
                foreach ($related_collection as $collection) {
                    if (isset($this->request->get['collection']) && $this->request->get['collection'] == $collection['collection_id']) {
                        $collection_url = $this->url->link('common/shop', modQuery($url, 'collection'), true);
                        $result['collection_selected_name'] = $collection['title'];
                    } else {
                        $collection_url = $this->url->link('common/shop', modQuery($url, 'collection') . '&collection=' . $collection['collection_id'], true);
                    }
                    /*$result['collection'][] = [
                        'collection_id' => $collection['collection_id'],
                        'title' => $collection['title'],
                        'image' => $collection['image'],
                        'amoutProduct' => $this->model_catalog_collection->getCountProductCollections($collection['collection_id']),
                        'url' => $collection_url,
                    ];*/
                    $result[] = [
                        'id' => $collection['collection_id'],
                        'name' => $collection['title']
                    ];
                }
            }

            $json = [
                'result' => $result
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * get list stores. TODO: run db alter...
     */
    public function stores()
    {
        $this->load->model('setting/store');
        try {
            $result = [];
            $list_store = $this->model_setting_store->getStores();
            if ($list_store) {
                foreach ($list_store as $value) {
                    /*$result['stores'][] = [
                        'store_id' => $value['store_id'],
                        'name' => $value['name'],
                        'description' => $value['description']
                    ];*/
                    $result[] = [
                        'id' => $value['store_id'],
                        'name' => $value['name']
                    ];
                }
            }

            $json = [
                'result' => $result
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * get list staffs
     */
    public function staffs()
    {
        $this->load->model('user/user');
        try {
            $result = [];
            $list_user = $this->model_user_user->getUsers();
            if ($list_user) {
                foreach ($list_user as $value) {
                    /*$result['staffs'][] = [
                        'staff_id' => $value['user_id'],
                        'lastname' => $value['lastname'],
                        'firstname' => $value['firstname'],
                        'email' => $value['email'],
                        'phone' => $value['telephone']
                    ];*/
                    $result[] = [
                        'id' => $value['user_id'],
                        'name' => $value['lastname'] . ' ' . $value['firstname']
                    ];
                }
            }

            $json = [
                'result' => $result
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * get list customers
     */
    public function customers()
    {
        $this->load->model('customer/customer');
        $this->load->model('localisation/vietnam_administrative');
        try {
            $result = [];
            $list_customers = $this->model_customer_customer->getAllCustomers();
            if ($list_customers) {
                foreach ($list_customers as $value) {
                    $address = $this->model_customer_customer->getAddress($value['address_id']);
                    $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($address['city']);
                    $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($address['district']);
                    $ward = $this->model_localisation_vietnam_administrative->getWardByCode($address['wards']);

                    $addressArr = [];

                    $address_tmp = isset($address['address']) ? $address['address'] : '';

                    if (trim($address_tmp) != '') {
                        $addressArr[] = $address_tmp;
                    }

                    if (trim($ward['name']) != '') {
                        $addressArr[] = $ward['name'];
                    }
                    if (trim($district['name']) != '') {
                        $addressArr[] = $district['name'];
                    }
                    if (trim($province['name']) != '') {
                        $addressArr[] = $province['name'];
                    }

                    /*$result[] = array(
                        'customer_id' => $value['customer_id'],
                        'name' => $value['lastname'] . ' ' . $value['firstname'],
                        'email' => $value['email'],
                        'phone' => $value['telephone'],
                        'address' => implode(', ', $addressArr)
                    );*/
                    $result[] = [
                        'id' => $value['customer_id'],
                        'name' => $value['lastname'] . ' ' . $value['firstname'],
                        'gender' => 'male', // male or female
                        'birth' => '',
                        'phone' => $value['telephone'],
                        'email' => $value['email'],
                        'group_id' => $value['customer_group_id'],
                        'city' => isset($province['code']) ? $province['code'] : '',
                        'district' => isset($district['code']) ? $district['code'] : '',
                        'ward' => isset($ward['code']) ? $ward['code'] : '',
                        'address' => !empty($addressArr) ? implode(', ', $addressArr) : (isset($address['address']) ? $address['address'] : '')
                    ];
                }
            }

            $json = [
                'result' => $result
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * get list customer_group
     */
    public function customer_groups()
    {
        $this->load->model('customer/customer');

        try {
            $result = [];
            $list_customers = $this->model_customer_customer->getAllCustomerGroups();
            if ($list_customers) {
                foreach ($list_customers as $value) {
                    $result[] = [
                        'id' => $value['customer_group_id'],
                        'name' => $value['name'],
                        'code' => $value['customer_group_code']
                    ];
                }
            }

            $json = [
                'result' => $result
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * get list provinces
     */
    public function provinces()
    {
        $result = [];

        $this->load->model('localisation/vietnam_administrative');
        $provinces = $this->model_localisation_vietnam_administrative->getProvinces();

        // maintain key
        $provinces = array_values($provinces);
        if ($provinces && is_array($provinces)) {
            foreach ($provinces as $province) {
                if (!array_key_exists('code', $province) ||
                    !array_key_exists('name', $province) ||
                    !array_key_exists('name_with_type', $province)
                ) {
                    continue;
                }

                $result[] = [
                    'id' => $province['code'],
                    'code' => $province['code'],
                    'name' => $province['name'],
                    'name_with_type' => $province['name_with_type'],
                ];
            }
        }

        $json = [
            'result' => $result
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * get list districts
     */
    public function districts()
    {
        $result = [];

        $this->load->model('localisation/vietnam_administrative');
        $districts = $this->model_localisation_vietnam_administrative->getDistricts();

        // maintain key
        $districts = array_values($districts);
        if ($districts && is_array($districts)) {
            foreach ($districts as $district) {
                if (!array_key_exists('code', $district) ||
                    !array_key_exists('name', $district) ||
                    !array_key_exists('name_with_type', $district)
                ) {
                    continue;
                }

                $result[] = [
                    'id' => $district['code'],
                    'code' => $district['code'],
                    'parent_code' => $district['parent_code'],
                    'name' => $district['name'],
                    'name_with_type' => $district['name_with_type'],
                ];
            }
        }

        $json = [
            'result' => $result
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * get list wards
     */
    public function wards()
    {
        $result = [];

        $this->load->model('localisation/vietnam_administrative');
        $wards = $this->model_localisation_vietnam_administrative->getWards();

        // maintain key
        $wards = array_values($wards);

        // TODO: use foreach $wards
        if ($wards) {
            $result = $wards;
        }

        $json = [
            'result' => $result
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * get list settings
     */
    public function settings()
    {
        $result = [];

        $this->load->model('setting/setting');

        $result[] = [
            'key' => 'shop_name',
            'value' => $this->model_setting_setting->getShopName()
        ];

        $result[] = [
            'key' => 'discount_auto_apply',
            'value' => $this->model_setting_setting->getSettingValue('discount_auto_apply') == 1 ? 1 : 0
        ];

        $result[] = [
            'key' => 'discount_combine',
            'value' => $this->model_setting_setting->getSettingValue('discount_combine') == 1 ? 1 : 0
        ];

        $result[] = [
            'key' => 'image_qr_code',
            'value' => $this->model_setting_setting->getSettingValue('config_image_qr_code')
        ];

        $json = [
            'result' => $result
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * get list discounts
     */
    public function discounts()
    {
        $this->load->model('discount/discount');
        try {
            $result = [];
            $list_discounts = $this->model_discount_discount->getActivatedDiscounts();
            if ($list_discounts) {
                $list_discounts = $this->filterDiscountsByOrderSource($list_discounts, ModelDiscountDiscount::$SOURCE_POS);

                foreach ($list_discounts as $discount) {
                    // store_id
                    $store_ids = [];
                    $d_to_stores = $this->model_discount_discount->getStoreIdsByDiscountId($discount['discount_id']);
                    if ($d_to_stores) {
                        foreach ($d_to_stores as $d_to_store) {
                            if (!isset($d_to_store['store_id'])) {
                                continue;
                            }

                            array_push($store_ids, $d_to_store['store_id']);
                        }
                    }

                    $result[] = [
                        "id" => $discount['discount_id'],
                        "name" => $discount['name'],
                        "code" => $discount['code'],
                        "discount_type_id" => $discount['discount_type_id'],
                        "config" => json_decode($discount['config'], true),
                        "start_at" => $discount['start_at'],
                        "end_at" => $discount['end_at'],
                        "usage_limit" => (int)$discount['usage_limit'],
                        "times_used" => (int)$discount['times_used'],
                        "store_id" => $store_ids
                    ];
                }
            }

            $json = [
                'result' => $result
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * create orders
     */
    public function orders()
    {
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $this->createOrder();
            return;
        } elseif (($this->request->server['REQUEST_METHOD'] == 'GET')) {
            $this->getOrders();
        }
    }

    public function reports()
    {
        $this->load->model('sale/order');
        $data_filter = ['order_source' => 'pos',];
        $records = [];

        try {
            $orders = $this->model_sale_order->getDataTotalReportByOrders($data_filter);
            $orders_total = $this->model_sale_order->getOrdersListTotal($data_filter);

            foreach ($orders as $order) {
                $record = [
                    'id' => !empty($order['pos_id_ref']) ? $order['pos_id_ref'] : $order['order_id_result'],
                    'order_id' => $order['order_id_result'],
                    'created_date' => $order['date_added'],
                    'created_date_int' => strtotime($order['date_added']) * 1000,
                    'customer_name' => $order['customer_full_name'] != '' ? $order['customer_full_name'] : $order['order_full_name'],
                    'total_pay' => (float)$order['total_amount'],
                    'store_id' => $order['store_id'],
                    'discount' => (float)$order['rp_discount'],
                    'order_code' => $order['order_code'],
                ];

                $records[] = $record;
            }

            $json = [
                'code' => 200,
                'message' => 'Success',
                'total' => $orders_total,
                'records' => $records,
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request',
                '__error' => $e->getMessage()
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /* ----- local function ----- */
    /**
     * create orders
     */
    private function createOrder()
    {
        $json = [];

        // get post data from php://input
        // notice: could not get directly from $this->request->post or $_POST, TODO: why?...
        $this->request->post = json_decode(file_get_contents('php://input'), true);
        if (empty($this->request->post['data'])) {
            $json = [
                'code' => 400,
                'message' => 'Expected post data is not empty!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }

        try {
            $this->log->write('[bestme_pos] orders(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
            $this->log->write('[bestme_pos] orders(): could not log post data');
        }

        // sample data
        /*$this->request->post = [
            'token' => 'd1133ac9eddba6d491219f070affc5d2',
            'data' => [
                'customer' => [
                    'id' => '',
                    'name' => 'Nguyen Thuy Linh',
                    'phone_number' => '0987654321',
                    'email' => 'nguyenthuylinh@gmail.com',
                    'city' => '81',
                    'district' => '818',
                    'ward' => '81562',
                    'delivery_addr' => 'So 1 Ngo 2 Duy Tan',
                ],
                'products' => [
                    [
                        'id' => 83,
                        'quantity' => 2,
                        'product_version_id' => null,
                        'discount' => 3000,
                        'discount_ids' => []
                    ],
                    [
                        'id' => 104,
                        'quantity' => 3,
                        'product_version_id' => 1,
                        'discount' => 7000,
                        'discount_ids' => [1,2]
                    ]
                ],
                'order_discount_ids' => []
                'discount' => 10000,
                'total_pay' => 80000,
                'pos_id_ref' => 'xxxxx_12354',
                'store_id' => 1,
                'user_id' => 5,
                'note' => 'Khach mua hang tai POS!'
            ],
        ];*/

        /* validate post data */
        $valid_orders = $this->validateOrders();
        if (!is_array($valid_orders) || !isset($valid_orders['code']) || $valid_orders['code'] != 0) {
            $json = $valid_orders;

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }

        /* check if order existing */
        $this->load->model('sale/order');
        $order_by_post_ref_id = $this->model_sale_order->getOrderByPosRef(isset($this->request->post['data']['pos_id_ref']) ? $this->request->post['data']['pos_id_ref'] : '');
        if ($order_by_post_ref_id) {
            $json = [
                'code' => 0,
                'message' => 'Order with id reference has already existed'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }

        /* create order, customer, ... */
        try {
            $this->load->model('account/address');
            $this->load->model('customer/customer');
            $this->load->model('account/customer');
            $this->load->model('localisation/vietnam_administrative');

            // pass data post
            $data['telephone'] = isset($this->request->post['data']['customer']['phone_number']) ? $this->request->post['data']['customer']['phone_number'] : '';

            // extract first name, last name
            $full_name = isset($this->request->post['data']['customer']['name']) ? $this->request->post['data']['customer']['name'] : '';
            $full_name = trim($full_name);
            $first_and_last_name = explode(" ", $full_name);
            $first_name = array_pop($first_and_last_name);
            $last_name = implode(" ", $first_and_last_name);
            $data['fullname'] = $full_name;
            $data['lastname'] = trim($last_name);
            $data['firstname'] = trim($first_name);

            // mapping address
            $data['city'] = isset($this->request->post['data']['customer']['city']) ? $this->request->post['data']['customer']['city'] : '';
            $data['email'] = isset($this->request->post['data']['customer']['email']) ? $this->request->post['data']['customer']['email'] : '';
            $data['district'] = isset($this->request->post['data']['customer']['district']) ? $this->request->post['data']['customer']['district'] : '';
            $data['wards'] = isset($this->request->post['data']['customer']['ward']) ? $this->request->post['data']['customer']['ward'] : '';
            $data['address'] = isset($this->request->post['data']['customer']['delivery_addr']) ? $this->request->post['data']['customer']['delivery_addr'] : '';
            $data['customer_group_id'] = isset($this->request->post['data']['customer']['group_id']) ? $this->request->post['data']['customer']['group_id'] : $this->config->get('config_customer_group_id');

            // create customer
            $customer = $this->model_customer_customer->getCustomerByTelephone($data['telephone']);
            if ($customer) {
                $customer_id = $customer['customer_id'];
            } else {
                $customer_id = $this->createNewCustomer($data);
            }
            $data['customer_id'] = $customer_id;

            // bind more data to order model
            $data['order_payment'] = 1;
            $data['order_transfer'] = ModelSaleOrder::ORDER_STATUS_ID_COMPLETED;
            $data['order_note'] = isset($this->request->post['data']['note']) ? $this->request->post['data']['note'] : '';
            $data['payment_firstname'] = $data['firstname'];
            $data['payment_lastname'] = $data['lastname'];
            $data['payment_company'] = '';
            $data['payment_address_1'] = $data['address'];
            $data['payment_address_2'] = sprintf('%s, %s, %s, %s', $data['address'], $data['wards'], $data['district'], $data['city']);
            $data['payment_city'] = $data['city'];
            $data['payment_postcode'] = '';
            $data['payment_country_id'] = '';
            $data['payment_zone_id'] = '';
            $data['payment_custom_field'] = array();
            $data['payment_code'] = '';
            $data['payment_status'] = 1; // customer already paid. Future: support COD?...
            $data['shipping_firstname'] = $data['firstname'];
            $data['shipping_lastname'] = $data['lastname'];
            $data['shipping_company'] = '';
            $data['shipping_address_1'] = $data['address'];
            $data['shipping_address_2'] = sprintf('%s, %s, %s, %s', $data['address'], $data['wards'], $data['district'], $data['city']);
            $data['shipping_city'] = $data['city'];
            $data['shipping_postcode'] = '';
            $data['shipping_country_id'] = '';
            $data['shipping_zone_id'] = '';
            $data['shipping_custom_field'] = array();
            $data['shipping_method'] = '';
            $data['shipping_method_value'] = ''; // phương thức vẫn chuyển mặc định để rõng
            $data['shipping_code'] = '';
            $data['shipping_ward_code'] = $data['wards'];
            $data['shipping_district_code'] = $data['district'];
            $data['shipping_province_code'] = $data['city'];

            $data['order_products'] = array();
            $data['order_vouchers'] = array();
            $data['order_totals'] = array();

            $data['order_status_id'] = ModelSaleOrder::ORDER_STATUS_ID_COMPLETED;
            $data['order_status'] = ModelSaleOrder::ORDER_STATUS_ID_COMPLETED;  /////Đang xử lý; @@
            $data['currency_code'] = $this->config->get('config_currency');
            $data['from_domain'] = ''; // auto from main domain. TODO: from "bestme pos"?...
            $data['pos_id_ref'] = isset($this->request->post['data']['pos_id_ref']) ? $this->request->post['data']['pos_id_ref'] : '';
            $data['store_id'] = isset($this->request->post['data']['store_id']) ? $this->request->post['data']['store_id'] : '';
            $data['user_id'] = isset($this->request->post['data']['user_id']) ? $this->request->post['data']['user_id'] : '';

            $data['coupon'] = '';
            $data['voucher'] = '';
            $data['reward'] = '';

            /* check and build products data */
            $products = $this->request->post['data']['products'];
            // merge products quantity if same id
            $grouped_products = [];
            foreach ($products as $product) {
                if (!isset($product['id']) || !isset($product['quantity']) || (int)$product['quantity'] == 0) {
                    continue;
                }

                // maintain key "discount"
                $product['product-discount'] = isset($product['discount']) ? (int)$product['discount'] : 0;

                $key = $product['id'];
                if (isset($product['product_version_id']) && !empty($product['product_version_id'])) {
                    $key = sprintf('%s-%s', $product['id'], $product['product_version_id']);
                }

                // new if not exist
                if (!array_key_exists($key, $grouped_products)) {
                    $grouped_products[$key] = $product;
                    continue;
                }

                // or sum quantity if existed
                $grouped_products[$product['id']]['quantity'] += $product['quantity'];
            }

            $this->load->model('catalog/product');
            $this->load->model('discount/discount');
            $total_amount = 0;
            $total_into_money = 0;
            $list_discounts = [];
            foreach ($grouped_products as &$product) {
                if (!isset($product['id']) || !isset($product['quantity']) || (int)$product['quantity'] == 0) {
                    $json = [
                        'code' => 101,
                        'message' => 'products info invalid'
                    ];
                    break;
                }

                $product_id = $product['id'];
                $product_version_id = isset($product['product_version_id']) ? $product['product_version_id'] : '';

                $product['model'] = '';
                $product['price'] = (isset($product['sale_price']) && $product['sale_price']) ? $product['sale_price'] : $product['price'];
                $product['product_id'] = $product_id;
                $product['product_version_id'] = $product_version_id;

                $total = (float)$product['price'] * (int)$product['quantity'];


                // Now: use total from
                // TODO: add column discount, save db, calculate total amount with discount?...
                $total_amount += $product['quantity'];
                $total_into_money += $total;

                // update discount ids list
                $discount_ids = isset($product['discount_ids']) ? $product['discount_ids'] : [];
                if (!empty($discount_ids) && is_array($discount_ids)) {
                    $list_discounts = array_merge($list_discounts, $discount_ids);
                }
            }
            unset($product); // unset reference from above for-loop

            // update discount ids list
            $order_discount_ids = isset($this->request->post['data']['order_discount_ids']) ? $this->request->post['data']['order_discount_ids'] : [];
            if (!empty($order_discount_ids) && is_array($order_discount_ids)) {
                $list_discounts = array_merge($list_discounts, $order_discount_ids);
            }

            // unique ids
            $list_discounts = array_values(array_unique($list_discounts));

            // no shopping fee
            $data['shipping_fee'] = 0;

            $data['order-discount'] = isset($this->request->post['data']['discount']) ? (int)$this->request->post['data']['discount'] : 0;

            $data['total'] = $data['shipping_fee'] + $total_into_money;
            /*TODO override total order*/
            $data['total'] = isset($this->request->post['data']['total_pay']) ? $this->request->post['data']['total_pay'] : 0;
            $data['created_date'] = isset($this->request->post['data']['created_date']) ? $this->request->post['data']['created_date'] : '';

            // finally, create order
            if (!$json) {
                $order_id = $this->model_sale_order->addOrderCommon($data, $data['customer_id']);
                $this->model_discount_discount->addOrderDiscount($order_id, $list_discounts);

                // confix Prefix, suffix in order_code
                $this->load->model('setting/setting');

                $config_id_prefix = $this->model_setting_setting->getSettingByKey('config_id_prefix');
                $config_id_prefix = isset($config_id_prefix['value']) ? $config_id_prefix['value'] : '';

                $config_id_suffix = $this->model_setting_setting->getSettingByKey('config_id_suffix');
                $config_id_suffix = isset($config_id_suffix['value']) ? $config_id_suffix['value'] : '';

                // TODO: ...
                $order_code = $this->generateOrderCode($order_id, $config_id_prefix, $config_id_suffix);
                $this->db->query("UPDATE " . DB_PREFIX . "order SET order_code = '" . $this->db->escape($order_code) . "', `source` = 'pos' WHERE order_id = " . (int)$order_id);

                $this->model_sale_order->addOrderProductForPos($grouped_products, $order_id, $data['store_id']);
                $this->model_sale_order->addReport($order_id, $data['store_id'], $data['user_id']);

                $json = [
                    'code' => 0,
                    'message' => 'ORDER_CREATED'
                ];
            }
        } catch (Exception $e) {
            $json = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function getOrders()
    {
        $json = [];
        $data_filter = [];

        $this->load->model('sale/order');

        if (isset($this->request->get['page']) && !empty($this->request->get['page'])) {
            $data_filter = [
                'start' => ((int)$this->request->get['page'] - 1) * ((int)isset($this->request->get['limit']) ? $this->request->get['limit'] : 10),
                'limit' => isset($this->request->get['limit']) ? $this->request->get['limit'] : 10
            ];
        }

        $data_filter['order_code'] = isset($this->request->get['order_code']) ? $this->request->get['order_code'] : '';
        $data_filter['order_source'] = isset($this->request->get['order_source']) ? $this->request->get['order_source'] : '';
        $data_filter['time_from'] = isset($this->request->get['start']) ? $this->request->get['start'] : '';
        $data_filter['time_to'] = isset($this->request->get['end']) ? $this->request->get['end'] : '';
        $data_filter['page'] = isset($this->request->get['page']) ? $this->request->get['page'] : 1;

        try {
            $orders = $this->model_sale_order->getOrdersListData($data_filter);
            $orders_total = $this->model_sale_order->getOrdersListTotal($data_filter);

            $json = [
                'code' => 200,
                'message' => 'Success',
                'total' => $orders_total,
                'records' => $orders,
                'page' => (isset($this->request->get['page']) && !empty($this->request->get['page'])) ? $this->request->get['page'] : '',
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request',
                '__error' => $e->getMessage()
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * validateTokenVerify
     *
     * @return bool
     */
    private function validateTokenVerify()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }

        $token_string = isset($this->request->get['data']) ? $this->request->get['data'] : '';
        $query = [$token_string];

        $calculated_token = $this->getTokenForRequestGet($query);
        if ($calculated_token !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    /**
     * get Token For Request Get
     *
     * @param array $query
     * @return string
     */
    private function getTokenForRequestGet(array $query)
    {
        $apiKey = $this->getApiKey();
        $token_string = implode('', $query);
        $token_string .= $apiKey;

        return $this->getToken($token_string);
    }

    /**
     * get Token For Request Post
     *
     * @param array $data key 'token' will be auto removed
     * @return string
     */
    private function getTokenForRequestPost(array $data)
    {
        $apiKey = $this->getApiKey();
        $data_except_token = $data;
        unset($data_except_token['token']);
        $token_string = json_encode($data_except_token);
        $token_string .= $apiKey;

        return $this->getToken($token_string);
    }

    /**
     * get Token from data
     *
     * @param string $data
     * @return string
     */
    private function getToken($data)
    {
        if (!is_string($data)) {
            return '';
        }

        return md5($data);
    }

    /**
     * get Api Key
     *
     * @return mixed
     */
    private function getApiKey()
    {
        /*$this->load->model('setting/setting');

        return $this->model_setting_setting->getSettingValue(self::CONFIG_BESTME_POS_API_KEY);*/

        return BESTME_POS_SECURE_KEY;
    }

    /**
     * categoryMapList
     *
     * @param $category_lists
     * @return array
     */
    private function categoryMapList($category_lists)
    {
        $categories = array();
        foreach ($category_lists as $category) {
            $name = $category['text'];
            $path = $category['id'];
            $filter['filter_category_id'] = (int)$category['id'];
            $filter['filter_sub_category'] = true;
            $count = $this->model_catalog_product->getTotalProducts($filter);
            $categories[] = array(
                'name' => $name,
                'count' => (int)$count,
                'href' => $this->url->link('common/shop', 'path=' . $path, true)
            );
        }

        return $categories;
    }

    /**
     * validate Orders
     *
     * @return array|bool
     */
    private function validateOrders()
    {
        if (!isset($this->request->post['token'])) {
            return [
                'code' => 400,
                'message' => 'Expected param "token" and token is not empty!'
            ];
        }

        $post_body = $this->request->post;
        if (!is_array($post_body) || empty($post_body)) {
            return [
                'code' => 400,
                'message' => 'Expected post data is not empty array'
            ];
        }

        if ($this->getTokenForRequestPost($post_body) != $this->request->post['token']) {
            return [
                'code' => 400,
                'message' => 'Invalid token'
            ];
        }

        // No need validate customer info for POS
        /*$phone = isset($this->request->post['data']['customer']['phone_number']) ? $this->request->post['data']['customer']['phone_number'] : '';
        if (!$phone || (utf8_strlen($phone) < 3) || (utf8_strlen($phone) > 32)) {
            return [
                'code' => 101,
                'message' => 'Expected customer phone is not empty and length in 3-32 characters!'
            ];
        }*/

        // No need validate customer info for POS
        /*$address = isset($this->request->post['data']['customer']['delivery_addr']) ? $this->request->post['data']['customer']['delivery_addr'] : '';
        if (empty($address)) {
            return [
                'code' => 101,
                'message' => 'Expected customer address is not empty!'
            ];
        }*/

        $products = isset($this->request->post['data']['products']) ? $this->request->post['data']['products'] : [];
        if (empty($products) || !is_array($products)) {
            return [
                'code' => 101,
                'message' => 'Expected products is not empty array!'
            ];
        }

        return [
            'code' => 0,
            'message' => 'Products info valid'
        ];
    }

    private function createNewCustomer($data)
    {
        $this->load->model('account/customer');
        $this->load->model('account/address');

        $customer = [
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'telephone' => $data['telephone'],
            'customer_group_id' => $data['customer_group_id'],
            'password' => token(9),
            'not_send_mail' => true
        ];

        $customer_id = $this->model_account_customer->addCustomer($customer);

        $address_data = array(
            'customer_id' => $customer_id,
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'company' => '',
            'city' => $data['city'],
            'district' => $data['district'],
            'wards' => $data['wards'],
            'postcode' => '',
            'address' => $data['address'],
            'phone' => $data['telephone'],
            'default' => true
        );

        $this->model_account_address->addAddressNew($customer_id, $address_data);

        return $customer_id;
    }

    private function getMaxQuantityProduct($product, $order)
    {
        $this->load->model('catalog/product');
        if ($product['multi_versions'] == 1) {
            $product_version = $this->model_catalog_product->getProductByVersionId($product['product_id'], $order['product_version_id']);
            if ($product_version && $product['status'] == 1 && $product_version['deleted'] != 1 && $product_version['status'] == 1 && ($product_version['quantity'] > 0 || $product['sale_on_out_stock'] == 1)) {
                return true;
            } else {
                return false;
            }
        }
        if ($product['multi_versions'] == 0 && $product['status'] == 1 && ($product['product_master_quantity'] > 0 || $product['sale_on_out_stock'] == 1)) {
            return true;
        }
        return false;
    }

    /**
     * generate Order Code
     *
     * @param $order_id
     * @param $config_id_prefix
     * @param $config_id_suffix
     * @return string format as PREFIX[11-digits-with-padding-0]SUFFIX. e.g PR00000000123SF
     */
    private function generateOrderCode($order_id, $config_id_prefix, $config_id_suffix)
    {
        $max_order_id = 99999999999;
        $value_order = $max_order_id - ($max_order_id - $order_id);
        $strlen_value = strlen($value_order);
        $number_for = 11 - $strlen_value;
        $number_zero = '0';
        $result = '';

        for ($i = 1; $i <= $number_for; $i++) {
            $result .= $number_zero;
        }
        $order_code = $config_id_prefix . $result . $order_id . $config_id_suffix;

        return $order_code;
    }

    /**
     * @param array $category
     * @return array format as:
     * [
     *     "id" => 12,
     *     "name" => "cat12",
     *     "sub_categories" => [
     *         "id" => 13,
     *         "name" => "cat13",
     *         "sub_categories" => []
     *     ],
     * ]
     */
    private function getCategoryInfo(array $category)
    {
        if (!isset($category['children']) || !is_array($category['children']) || empty($category['children'])) {
            return [];
        }

        $result = [];
        foreach ($category['children'] as $child) {
            $result[] = [
                'id' => $child['id'],
                'name' => $child['text'],
                'sub_categories' => $this->getCategoryInfo($child)
            ];
        }

        return $result;
    }
}