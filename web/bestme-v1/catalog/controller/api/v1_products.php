<?php

/**
 * base url: /api/v1_product/xxx
 * Class ControllerApiV1Product
 */
class Controllerapiv1products extends Controller
{
    use Product_Util_Trait;
    use Device_Util;

    public function categories()
    {
        $this->load->model('catalog/category');
        try {
            $all_categories_with_tree = $this->model_catalog_category->getCategoriesTree();

            $result = [];
            if ($all_categories_with_tree) {
                foreach ($all_categories_with_tree as $category) {
                    $result[] = [
                        'category_id' => $category['category_id'],
                        'name' => $category['name'],
                        'image' => $category['image'],
                        'url' => $this->url->link('common/shop', '&path=' . $category['category_id'], true),
                        'sub_categories' => $this->buildSubCategories($category)
                    ];
                }
            }

            $json = [
                'result' => $result
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function suppliers()
    {
        $this->load->model('catalog/manufacturer');
        try {
            $url = '';
            $result = [];
            $related_manufacturer = $this->model_catalog_manufacturer->getManufacturersHadProduct();
            if ($related_manufacturer) {
                foreach ($related_manufacturer as $manufacturer) {
                    if (isset($this->request->get['manufacture']) && $this->request->get['manufacture'] == $manufacturer['manufacturer_id']) {
                        $manufacturer_url = $this->url->link('common/shop', modQuery($url, 'manufacture'), true);
                        $result['manufacturer_selected_name'] = $manufacturer['name'];
                    } else {
                        $manufacturer_url = $this->url->link('common/shop', modQuery($url, 'manufacture') . '&manufacture=' . $manufacturer['manufacturer_id'], true);
                    }
                    $result['suppliers'][] = array(
                        'supplier_id' => $manufacturer['manufacturer_id'],
                        'title' => $manufacturer['name'],
                        'url' => $manufacturer_url,
                    );
                }

                $json = [
                    'result' => $result
                ];
            } else {
                $json = [
                    'code' => 204,
                    'message' => 'No Content',
                ];
            }
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function collections()
    {
        $this->load->model('catalog/collection');
        try {
            $url = '';
            $result = [];
            $related_collection = $this->model_catalog_collection->getAllcollection();
            if ($related_collection) {
                foreach ($related_collection as $collection) {
                    if (isset($this->request->get['collection']) && $this->request->get['collection'] == $collection['collection_id']) {
                        $collection_url = $this->url->link('common/shop', modQuery($url, 'collection'), true);
                        $result['collection_selected_name'] = $collection['title'];
                    } else {
                        $collection_url = $this->url->link('common/shop', modQuery($url, 'collection') . '&collection=' . $collection['collection_id'], true);
                    }
                    $result['collection'][] = array(
                        'collection_id' => $collection['collection_id'],
                        'title' => $collection['title'],
                        'image' => $collection['image'],
                        'amoutProduct' => $this->model_catalog_collection->getCountProductCollections($collection['collection_id']),
                        'url' => $collection_url,
                    );
                }

                $json = [
                    'result' => $result
                ];
            } else {
                $json = [
                    'code' => 204,
                    'message' => 'No Content',
                ];
            }
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function product_banner()
    {
        $this->load->model('extension/module/theme_builder_config');
        $banners = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CATEGORY_BANNER);
        $banners = json_decode($banners, true);
        try {
            if ($banners['visible'] == 1) {
                foreach ($banners['display'] as $key => $vl) {
                    $result[] = [
                        "image-url" => $vl['image-url'],
                        "url" => $vl['url'],
                        "description" => $vl['description'],
                        "visible" => $vl['visible']
                    ];
                }

                $json = [
                    'result' => $result
                ];
            } else {
                $json = [
                    'code' => 204,
                    'message' => 'No Content',
                ];
            }
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function products()
    {
        $this->load->model('catalog/collection');
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('tool/image');
        $this->load->model('extension/module/theme_builder_config');
        try {
            // products
            $filter = [];
            $valueoptiton_post = (isset($this->request->get['valueoptiton']) ? $this->request->get['valueoptiton'] : '');
            if (isset($this->request->get['valueoptiton'])) {
                $filter['valueoptiton'] = $valueoptiton_post;
            }

            if (isset($this->request->get['search'])) {
                $txtSearch = trim($this->request->get['search']);
                $filter['search'] = $txtSearch;
            }

            if (isset($this->request->get['path'])) {
                $parts = explode('_', $this->request->get['path']);
                $filter['filter_category_id'] = (int)end($parts);
                $filter['filter_sub_category'] = true;
            }

            if (isset($this->request->get['manufacture'])) {
                $filter['filter_manufacturer_id'] = $this->request->get['manufacture'];
            }

            if (isset($this->request->get['collection'])) {
                $filter['filter_collection_id'] = $this->request->get['collection'];
            }

            if (isset($this->request->get['nametag'])) {
                $filter['filter_nametag'] = $this->request->get['nametag'];
            }

            if (isset($this->request->get['min'])) {
                $filter['price_min'] = $this->request->get['min'];
            }

            if (isset($this->request->get['max'])) {
                $filter['price_max'] = $this->request->get['max'];
            }

            if (isset($this->request->get['products_block_name'])) {
                $productsInProductBlock = $this->getProductsInProductBlock($this->request->get['products_block_name']);
                $filter['products_block_name'] = $productsInProductBlock['products_block_name'];
            }

            if (isset($this->request->get['page'])) {
                $page = $this->request->get['page'];
            } else {
                $page = 1;
            }

            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit') ? $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit') : $this->config->get('config_limit_admin');
            }

            $filter['start'] = ($page - 1) * (int)$limit;
            $filter['limit'] = $limit;

            $products = $this->getProductsByFilter($filter);

            $json = [
                'result' => $products
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function categoryMapList($category_lists)
    {
        $categories = array();
        foreach ($category_lists as $category) {
            $name = $category['text'];
            $path = $category['id'];
            $filter['filter_category_id'] = (int)$category['id'];
            $filter['filter_sub_category'] = true;
            $count = $this->model_catalog_product->getTotalProducts($filter);
            $categories[] = array(
                'name' => $name,
                'count' => (int)$count,
                'href' => $this->url->link('common/shop', 'path=' . $path, true)
            );
        }

        return $categories;
    }

    public function product_detail()
    {
        /* get product detail due to id */
        $this->load->model('catalog/product');

        if (isset($this->request->get['product_id']) && !empty($this->request->get['product_id'])) {
            $product_id = $this->request->get['product_id'];
            $product = $this->model_catalog_product->getProduct($product_id);
            $not_found = false;
            if (empty($product) || $product['product_id'] == null) {
                $not_found = true;
            }

            /* resize image */
            $this->load->model('tool/image');
            $product['image'] = $this->resizeImage($product['image'], 2000, 2000); // Avoid breaking when zooming in, TODO ...

            /* convert data product */
            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $price_master = $this->currency->formatCustom($this->tax->calculate($product['price_master'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $price_master = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $compare_price_master = $this->currency->formatCustom($this->tax->calculate($product['compare_price_master'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $compare_price_master = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $min_price_version = $this->currency->formatCustom($this->tax->calculate($product['min_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $min_price_version = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $max_price_version = $this->currency->formatCustom($this->tax->calculate($product['max_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $max_price_version = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $min_compare_price_version = $this->currency->formatCustom($this->tax->calculate($product['min_compare_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $min_compare_price_version = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $max_compare_price_version = $this->currency->formatCustom($this->tax->calculate($product['max_compare_price_version'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $max_compare_price_version = false;
            }
            if (empty($product['product_version_id'])) {
                $product['price_master_real'] = ((int)$product['price_master'] != 0) ? 1 : 0;
                $product['max_price_version_real'] = 1;
            }
            if (!empty($product['product_version_id'])) {
                $product['max_price_version_real'] = ((int)$product['price_version_check_null'] != 0) ? 1 : 0;
                $product['price_master_real'] = 1;
            }
            $product['percent_sale'] = empty($product['max_price_version']) ? getPercent($product['price_master'], $product['compare_price_master']) : getPercent($product['min_price_version'], $product['min_compare_price_version'], $product['max_percent_pv']);

            if ((float)$product['price_discount'] == 0 && !empty($product['discount_id'])) {
                $product['percent_sale'] = -100;
                $product['percent_sale_value'] = '-100%';
            }

            $product['price_master'] = $price_master;
            $product['compare_price_master'] = $compare_price_master;
            $product['min_price_version'] = $min_price_version;
            $product['max_price_version'] = empty($product['max_price_version']) ? $product['max_price_version'] : $max_price_version;
            $product['min_compare_price_version'] = $min_compare_price_version;
            $product['max_compare_price_version'] = empty($product['max_compare_price_version']) ? $product['max_compare_price_version'] : $max_compare_price_version;
            $product['product_is_stock'] = $this->model_catalog_product->isStock($product_id); // $product_version_id null for all versions
            $product['product_mas_ver_quantity'] = empty($product['product_version_id']) ? $product['product_master_quantity'] : $product['product_version_quantity'];
            $product['sale_on_out_stock_check'] = true;
            if (empty($product['product_version_id']) && $product['product_master_quantity'] < 1 && $product['sale_on_out_stock'] == 0) {
                $product['sale_on_out_stock_check'] = false;
            }
            $data['product'] = $product;
            $data['current_url'] = $this->url->link('product/product', 'product_id=' . $this->request->get['product_id']);

            /* get product attribute */
            $data['attributes'] = [];
            $attributes = $this->model_catalog_product->getProductAttributeCustom($this->request->get['product_id']);
            if ($attributes) {
                foreach ($attributes as $attribute) {
                    $data['attributes'][] = [
                        'product_id' => $attribute['product_id'],
                        'attribute_name' => $attribute['name'],
                        'attribute_text' => explode(',', $attribute['text']),
                    ];
                }
            }

            /* get product category */
            $data['categories'] = [];
            $categories = $this->model_catalog_product->getCategoriesCustom($this->request->get['product_id']);
            if ($categories) {
                foreach ($categories as $category) {
                    $data['categories'][] = [
                        'name' => $category['name'],
                        'image' => $category['image'],
                        'href' => $this->url->link('common/shop', 'path=' . $category['category_id'], true),
                    ];
                }
            }
            $data['tags'] = [];
            $this->load->model('catalog/tag');
            $tags = $this->model_catalog_tag->getTagsByProductId($this->request->get['product_id']);
            if ($tags) {
                foreach ($tags as $tag) {
                    $data['tags'][] = [
                        'name' => $tag['title'],
                        'href' => $this->url->link('common/shop', 'nametag=' . $tag['id'], true),
                    ];
                }
            }

            /* get product images */
            $this->load->model('catalog/product');
            $product_images = $this->model_catalog_product->getProductImages($product_id);
            $product_images = empty($product_images) || !is_array($product_images) ? [] : $product_images;
            foreach ($product_images as &$product_image) {
                $product_image['image'] = $this->resizeImage($product_image['image'], 2000, 2000); // Avoid breaking when zooming in, TODO ...
            }
            unset($product_image);
            //add image default
            $product_image_default = [
                'image' => $product['image'],
                'product_id' => $this->request->get['product_id'],
                'image_alt' => ''
            ];
            array_unshift($product_images, $product_image_default);
            $data['product_images'] = $product_images;
            $data['product_empty'] = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height'));

            $json = $data;
        } else {
            $json = [
                'code' => 204,
                'message' => 'No Content',
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function resizeImage($image, $width = null, $height = null)
    {
        if ($image) {
            if ($width == null) {
                $width = $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width');
            }
            if ($height == null) {
                $height = $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height');
            }
            $image = $this->model_tool_image->resize($image, $width, $height);
        } else {
            $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height'));
        }

        return $image;
    }

    public function product_version()
    {
        $json = array();
        $product = $this->request->get;
        if (isset($product['attribute'])) {
            $this->load->model('catalog/product');
            $version = [];
            foreach ($product['attribute'] as $key => $attribute) {
                $version[] = $attribute;
            }
            $product_version = $this->model_catalog_product->getProductByVersionValue($product['product_id'], $version);
            if ($product_version) {
                $json['sale_on_out_of_stock'] = $this->model_catalog_product->getSaleOnOutOfStockProduct($product['product_id']);
                $json['product_version'] = $product_version;
                $json['success'] = true;
                $json['product_version']['percent_sale'] = getPercent($product_version['price'], $product_version['compare_price']);
                //TODO tax 0
                $json['product_version']['price_format'] = $this->currency->formatCustom($this->tax->calculate($product_version['price'], 0, $this->config->get('config_tax')), $this->session->data['currency']);
                $json['product_version']['compare_price_format'] = $this->currency->formatCustom($this->tax->calculate($product_version['compare_price'], 0, $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $json['success'] = false;
                $json['message'] = "Không tồn tại version sản phẩm";
            }
        } else {
            $json = [
                'code' => 204,
                'message' => 'No Content',
            ];
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function products_block_list()
    {
         $this->load->model('extension/module/theme_builder_config');
        // /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
        // $sections_hot_product_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HOT_PRODUCT);
        // $sections_new_product_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_NEW_PRODUCT);
        // $sections_best_sale_product_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_BEST_SALES_PRODUCT);
        //
        // $hot_product_config = json_decode($sections_hot_product_config, true);
        // $new_product_config = json_decode($sections_new_product_config, true);
        // $best_sale_product_config = json_decode($sections_best_sale_product_config, true);
        //
        // $json = array($hot_product_config, $new_product_config, $best_sale_product_config);
        // $this->response->addHeader('Content-Type: application/json');
        // $this->response->setOutput(json_encode($json));

        // get sections config
        /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;
        $sections_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_SECTIONS);
        $sections_config = json_decode($sections_config, true);
        $sections_config = is_array($sections_config) ? $sections_config : [];
        $sections = array();

        if (isset($sections_config['section']) && is_array($sections_config['section'])) {
            foreach ($sections_config['section'] as $section) {
                if (!array_key_exists('name', $section) || !array_key_exists('visible', $section)) {
                    continue;
                }
                $sections[$section['name']] = $section['visible'];
            }
            unset($sections_config, $section);
        }

        $group_block = $this->getGroupBlock($model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_PRODUCT_GROUPS));
        $json = $this->getProductBlocksList($group_block, $sections);
        $this->responseJson($json);
    }

    /**
     * get Best Sales Products
     */
    public function best_sale_products()
    {
        $result = $this->getBestSalesProducts();

        $json = [
            'result' => $result
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * get Hot Product
     */
    public function hot_products()
    {
        $result = $this->getHotProduct();

        $json = [
            'result' => $result
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * get New Products
     */
    public function new_products()
    {
        $result = $this->getNewProducts();

        $json = [
            'result' => $result
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * get Best views Products
     */
    public function best_view_products()
    {
        $result = $this->getBestviewsProducts();

        $json = [
            'result' => $result
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /* == private functions == */
    private function getProductsInProductBlock($product_block_name)
    {
        $this->load->model('extension/module/theme_builder_config');
        $BUILT_IN_PRODUCT_LIST = [
            "hot-deals",
            "feature-product",
            "new-product"
        ];
        if (in_array($product_block_name, $BUILT_IN_PRODUCT_LIST)) {
            $this->load->model('extension/module/theme_builder_config');
            switch ($this->request->get['products_block_name']) {
                case 'hot-deals':
                    $sections_product_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HOT_PRODUCT);
                    break;
                case 'new-product':
                    $sections_product_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_NEW_PRODUCT);
                    break;
                case 'feature-product':
                    $sections_product_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_BEST_SALES_PRODUCT);
                    break;
            }

            $new_product_config = json_decode($sections_product_config, true);
            $data = [];
            if (isset($new_product_config['visible']) && $new_product_config['visible'] == 1) {
                if ($new_product_config['setting']['auto_retrieve_data'] == 0) {
                    $data['products_block_name'] = $new_product_config['setting']['collection_id'];
                }
            }
            return $data;
        } else {
            return false;
        }
    }

    /**
     * @param array $category
     * @return array format as:
     * [
     *     "id" => 12,
     *     "name" => "cat12",
     *     "sub_categories" => [
     *         "id" => 13,
     *         "name" => "cat13",
     *         "sub_categories" => []
     *     ],
     * ]
     */
    private function buildSubCategories(array $category)
    {
        if (!isset($category['sub_categories']) || !is_array($category['sub_categories']) || empty($category['sub_categories'])) {
            return [];
        }

        $result = [];
        foreach ($category['sub_categories'] as $child) {
            $result[] = [
                'id' => $child['category_id'],
                'name' => $child['name'],
                'image' => $child['image'],
                'url' => $this->url->link('common/shop', '&path=' . $child['category_id'], true),
                'sub_categories' => $this->buildSubCategories($child)
            ];
        }

        return $result;
    }

    /**
     * @param $multi_versions
     * @param $compare_price_master
     * @param $min_compare_price_version
     * @return bool
     */
    private function isContactForPrice($multi_versions, $compare_price_master, $min_compare_price_version)
    {
        $is_multi_versions = isset($multi_versions) && $multi_versions == 1;
        if (!$is_multi_versions) {
            if ((int)($compare_price_master) == 0) {
                return true;
            }
        } else {
            if ((int)($min_compare_price_version) == 0) {
                return true;
            }
        }

        return false;
    }

    private function getGroupBlock($config)
    {
        $config = json_decode($config, true);
        $groups = isset($config['list']) ? $config['list'] : [];

        foreach ($groups as $key => $group) {
            if ($group['visible'] != 1) {
                unset($groups[$key]);
            }
        }

        return $groups;
    }
}