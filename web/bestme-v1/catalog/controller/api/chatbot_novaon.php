<?php

class ControllerApiChatbotNovaon extends Controller
{
    const ORDER_STATUS_DRAFT = 6; // DO NOT HARD CODE HERE. TODO: get from Catalog/Order
    const CONFIG_CHATBOT_NOVAON_API_KEY = 'config_chatbot_novaon';

    public function products()
    {
        $this->load->language('api/chatbot_novaon');
        $json = array();
        // check parameter
        if (!$this->validateToken()) {
            $json = [
                'code' => 400,
                'message' => 'invalid token'
            ];
        } else {
            $data_filter = [];
            if (isset($this->request->get['page']) && !empty($this->request->get['page'])) {
                $data_filter = [
                    'start' => ((int)$this->request->get['page'] - 1) * ((int)isset($this->request->get['limit']) ? $this->request->get['limit'] : 15),
                    'limit' => isset($this->request->get['limit']) ? $this->request->get['limit'] : 15
                ];
            }
            if (isset($this->request->get['searchKey']) && isset($this->request->get['searchField'])) {
                $data_filter = array_merge($data_filter, [
                    'filter_' . $this->request->get['searchField'] => $this->request->get['searchKey'],
                ]);
            }
            if (isset($this->request->get['id'])) {
                $data_filter = [
                    'filter_product_id' => $this->request->get['id'],
                    'filter_product_version_id' => isset($this->request->get['product_version_id']) ? $this->request->get['product_version_id'] : '',
                ];
            }
            try {
                $this->load->model('api/chatbox_novaon');
                $products = $this->model_api_chatbox_novaon->getAPIProducts($data_filter);
                $products_total = $this->model_api_chatbox_novaon->getAPIProductsTotal($data_filter);
                $records = [];
                foreach ($products as $product) {
                    $records[] = [
                        "id" => $product['product_id'],
                        "name" => $product['name'],
                        "description" => $product['description'],
                        "short_description" => $product['sub_description'],
                        "original_price" => $product['compare_price_version'],
                        "sale_price" => $product['price_version'],
                        "image" => $product['image'],
                        "version" => $product["name_version"],
                        "product_version_id" => $product["product_version_id"],
                    ];
                }
                if (!isset($this->request->get['id'])) {
                    $json = [
                        'total' => $products_total,
                        'page' => (isset($this->request->get['page']) && !empty($this->request->get['page'])) ? $this->request->get['page'] : '',
                        'records' => $records,
                    ];
                } else {
                    if ($records) {
                        $json = $records[0];
                    } else {
                        $json = [
                            'code' => 102,
                            'message' => 'products info invalid'
                        ];
                    }
                }
            } catch (Exception $e) {
                $json = [
                    'code' => 201,
                    'message' => 'unknown error'
                ];
            }
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function orders()
    {
        $json = array();

        // get post data from php://input
        // notice: could not get directly from $this->request->post or $_POST, TODO: why?...
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('[chatbot_novaon] orders(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        // sample data
        /*$this->request->post = array(
            'token' => 'd1133ac9eddba6d491219f070affc5d2',
            'data' =>
                array(
                    'customer' =>
                        array(
                            'name' => 'Nguyen Thuy Linh',
                            'phone_number' => '0987654321',
                            'email' => 'nguyenthuylinh@gmail.com',
                            'delivery_addr' => 'So 1 Ngo 2 Duy Tan',
                            'ward' => 'Dich Vong Hau',
                            'district' => 'Cau Giay',
                            'city' => 'Ha Noi',
                            'note' => 'Goi hang can than va du so luong nhe shop. Thanks!',
                        ),
                    'products' =>
                        array(
                            0 =>
                                array(
                                    'id' => 83,
                                    'quantity' => 2,
                                ),
                            1 =>
                                array(
                                    'id' => 104,
                                    'quantity' => 3,
                                    'product_version_id' => 1,
                                ),
                        ),
                ),
        );*/

        $valid_orders = $this->validateOrders();
        if ($valid_orders) {
            $json = $valid_orders;
        } else {
            try {
                // add order
                $this->load->model('account/address');
                $this->load->model('customer/customer');
                $this->load->model('account/customer');
                $this->load->model('localisation/vietnam_administrative');
                // pass data post
                $data['telephone'] = $this->request->post['data']['customer']['phone_number'];

                // extract first name, last name
                $full_name = isset($this->request->post['data']['customer']['name']) ? $this->request->post['data']['customer']['name'] : '';
                $full_name = trim($full_name);
                $first_and_last_name = explode(" ", $full_name);
                $first_name = array_pop($first_and_last_name);
                $last_name = implode(" ", $first_and_last_name);
                $data['fullname'] = $full_name;
                $data['lastname'] = trim($last_name);
                $data['firstname'] = trim($first_name);

                $data['city'] = isset($this->request->post['data']['customer']['city']) ? $this->request->post['data']['customer']['city'] : '';
                $data['email'] = isset($this->request->post['data']['customer']['email']) ? $this->request->post['data']['customer']['email'] : '';
                $data['district'] = isset($this->request->post['data']['customer']['district']) ? $this->request->post['data']['customer']['district'] : '';
                $data['wards'] = isset($this->request->post['data']['customer']['ward']) ? $this->request->post['data']['customer']['ward'] : '';
                $data['address'] = isset($this->request->post['data']['customer']['delivery_addr']) ? $this->request->post['data']['customer']['delivery_addr'] : '';
                $customer = $this->model_customer_customer->getCustomerByTelephone($data['telephone']);
                if ($customer) {
                    $customer_id = $customer['customer_id'];
                } else {
                    $customer_id = $this->createNewCustomer($data);
                }
                $data['customer_id'] = $customer_id;
                $data['customer_group_id'] = $this->config->get('config_customer_group_id');
                $data['order_payment'] = 1;
                $data['order_transfer'] = self::ORDER_STATUS_DRAFT;
                $data['order_note'] = isset($this->request->post['data']['customer']['note']) ? $this->request->post['data']['customer']['note'] : '';
                $data['payment_firstname'] = $data['firstname'];
                $data['payment_lastname'] = $data['lastname'];
                $data['payment_company'] = '';
                $data['payment_address_1'] = $data['address'];
                $data['payment_address_2'] = sprintf('%s, %s, %s, %s', $data['address'], $data['wards'], $data['district'], $data['city']);
                $data['payment_city'] = $data['city'];
                $data['payment_postcode'] = '';
                $data['payment_country_id'] = '';
                $data['payment_zone_id'] = '';
                $data['payment_custom_field'] = array();
                $data['payment_code'] = '';
                $data['payment_status'] = 0;
                $data['shipping_firstname'] = $data['firstname'];
                $data['shipping_lastname'] = $data['lastname'];
                $data['shipping_company'] = '';
                $data['shipping_address_1'] = $data['address'];
                $data['shipping_address_2'] = sprintf('%s, %s, %s, %s', $data['address'], $data['wards'], $data['district'], $data['city']);
                $data['shipping_city'] = $data['city'];
                $data['shipping_postcode'] = '';
                $data['shipping_country_id'] = '';
                $data['shipping_zone_id'] = '';
                $data['shipping_custom_field'] = array();
                $data['shipping_method'] = '';
                $data['shipping_method_value'] = ''; // phương thức vẫn chuyển mặc định để rõng
                $data['shipping_code'] = '';
                $data['shipping_ward_code'] = $data['wards'];
                $data['shipping_district_code'] = $data['district'];
                $data['shipping_province_code'] = $data['city'];

                $data['order_products'] = array();
                $data['order_vouchers'] = array();
                $data['order_totals'] = array();

                $data['order_status_id'] = self::ORDER_STATUS_DRAFT;
                $data['order_status'] = self::ORDER_STATUS_DRAFT;  /////Đang xử lý; @@
                $data['currency_code'] = $this->config->get('config_currency');
                $data['from_domain'] = ''; // auto from main domain. TODO: from "chatbot"?...

                $data['coupon'] = '';
                $data['voucher'] = '';
                $data['reward'] = '';

                $products = $this->request->post['data']['products'];
                // merge products quantity if same id
                $grouped_products = [];
                foreach ($products as $product) {
                    if (!isset($product['id']) || !isset($product['quantity']) || (int)$product['quantity'] == 0) {
                        continue;
                    }

                    $key = $product['id'];
                    if (isset($product['product_version_id']) && !empty($product['product_version_id'])) {
                        $key = sprintf('%s-%s', $product['id'], $product['product_version_id']);
                    }

                    // new if not exist
                    if (!array_key_exists($key, $grouped_products)) {
                        $grouped_products[$key] = $product;
                        continue;
                    }

                    // or sum quantity if existed
                    $grouped_products[$key]['quantity'] += $product['quantity'];
                }

                $this->load->model('catalog/product');
                $total_amount = 0;
                $total_into_money = 0;
                foreach ($grouped_products as &$product) {
                    if (!isset($product['id']) || !isset($product['quantity']) || (int)$product['quantity'] == 0) {
                        $json = [
                            'code' => 101,
                            'message' => 'products info invalid'
                        ];
                        break;
                    }
                    $product_detail = $this->model_catalog_product->getProduct($product['id']);

                    $product_quantity_max = $this->getMaxQuantityProduct($product_detail, $product);
                    $product_version = [];
                    if (!$product_detail || ($product_detail['multi_versions'] == 1 && count($product_version = $this->model_catalog_product->getProductByVersionId($product_detail['product_id'], $product['product_version_id'])) == 0)) {
                        $json = [
                            'code' => 201,
                            'message' => 'error product order'
                        ];
                        break;
                    }

                    if (!$product_quantity_max) {
                        $json = [
                            'code' => 201,
                            'message' => 'error product order'
                        ];
                        break;
                    }

                    $product['name'] = $product_detail['name'];
                    $product['model'] = $product_detail['model'];
                    $product['price'] = $product_detail['price'];
                    $product['product_id'] = $product['id'];
                    if (isset($product['product_version_id']) && !empty($product['product_version_id'])) {
                        if ((int)$product_version['price'] == 0) $product_version['price'] = $product_version['compare_price'];
                        $total = (float)$product_version['price'] * (int)$product['quantity'];
                        $product['product_version'] = $product_version;
                    } else {
                        if ((int)$product_detail['price'] == 0) $product_detail['price'] = $product_detail['compare_price_master'];
                        $total = (float)$product_detail['price'] * (int)$product['quantity'];
                    }
                    $total_amount += $product['quantity'];
                    $total_into_money += $total;
                }
                unset($product);

                $data['shipping_fee'] = 0;
                $data['total'] = $data['shipping_fee'] + $total_into_money;
                if (!$json) {
                    $this->load->model('sale/order');
                    $order_id = $this->model_sale_order->addOrderCommon($data, $data['customer_id']);
                    // confix Prefix, suffix in order_code
                    $this->load->model('setting/setting');

                    $config_id_prefix = $this->model_setting_setting->getSettingByKey('config_id_prefix');
                    $config_id_prefix = isset($config_id_prefix['value']) ? $config_id_prefix['value'] : '';

                    $config_id_suffix = $this->model_setting_setting->getSettingByKey('config_id_suffix');
                    $config_id_suffix = isset($config_id_suffix['value']) ? $config_id_suffix['value'] : '';

                    $max_order_id = 99999999999;
                    $value_order = $max_order_id - ($max_order_id - $order_id);
                    $strlen_value = strlen($value_order);
                    $number_for = 11 - $strlen_value;
                    $number_zero = '0';
                    $result = '';

                    for ($i = 1; $i <= $number_for; $i++) {
                        $result .= $number_zero;
                    }
                    $order_code = $config_id_prefix . $result . $order_id . $config_id_suffix;
                    $this->db->query("UPDATE " . DB_PREFIX . "order SET order_code = '" . $this->db->escape($order_code) . "', `source` = 'chatbot' WHERE order_id = " . (int)$order_id);

                    $this->model_sale_order->addOrderProduct($grouped_products, $order_id);
                    $this->model_sale_order->addReport($order_id);
                    $json = [
                        'code' => 0,
                        'message' => 'ORDER_CREATED'
                    ];
                }
            } catch (Exception $e) {
                $json = [
                    'code' => 201,
                    'message' => 'unknown error: ' . $e->getMessage()
                ];
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function verify()
    {
        $this->load->language('api/chatbot_novaon');
        $json = [
            'code' => 1,
            'message' => 'credential (api key) is verified'
        ];

        // check parameter
        if (!$this->validateTokenVerify()) {
            $json = [
                'code' => 400,
                'message' => 'invalid token'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function provinces()
    {
        $this->load->model('localisation/vietnam_administrative');
        $provinces = $this->model_localisation_vietnam_administrative->getProvinces();

        // maintain key
        $provinces = array_values($provinces);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($provinces));
    }

    public function districts()
    {
        $provinceCode = '';
        if (isset($this->request->get['province'])) {
            $provinceCode = $this->request->get['province'];
        }

        if (!empty($provinceCode)) {
            $this->load->model('localisation/vietnam_administrative');
            $districts = $this->model_localisation_vietnam_administrative->getDistrictsByProvinceCode($provinceCode);

            // maintain key
            $districts = array_values($districts);
        } else {
            $districts = [];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($districts));
    }

    public function wards()
    {
        $districtCode = '';
        if (isset($this->request->get['district'])) {
            $districtCode = $this->request->get['district'];
        }

        if (!empty($districtCode)) {
            $this->load->model('localisation/vietnam_administrative');
            $wards = $this->model_localisation_vietnam_administrative->getWardsByDistrictCode($districtCode);

            // maintain key
            $wards = array_values($wards);
        } else {
            $wards = [];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($wards));
    }

    private function validateOrders()
    {
        if (!isset($this->request->post['token'])) {
            return [
                'code' => 400,
                'message' => 'invalid token'
            ];
        }

        if ($this->getMd5TokenPost() != $this->request->post['token']) {
            return [
                'code' => 400,
                'message' => 'invalid token'
            ];
        }

        $phone = isset($this->request->post['data']['customer']['phone_number']) ? $this->request->post['data']['customer']['phone_number'] : '';
        if (!$phone || (utf8_strlen($phone) < 3) || (utf8_strlen($phone) > 32)) {
            return [
                'code' => 101,
                'message' => 'customer info invalid'
            ];
        }

        $address = isset($this->request->post['data']['customer']['delivery_addr']) ? $this->request->post['data']['customer']['delivery_addr'] : '';
        if (empty($address)) {
            return [
                'code' => 101,
                'message' => 'customer info invalid'
            ];
        }

        $products = isset($this->request->post['data']['products']) ? $this->request->post['data']['products'] : '';
        if (!$products) {
            return [
                'code' => 101,
                'message' => 'products info invalid'
            ];
        }

        return;
    }

    private function validateToken()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }

        if ($this->getMd5TokenGet() !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    private function validateTokenVerify()
    {
        if (!isset($this->request->get['token'])) {
            return false;
        }

        if ($this->getMd5TokenVerify() !== $this->request->get['token']) {
            return false;
        }

        return true;
    }

    private function getMd5TokenGet()
    {
        $apiKey = $this->getApiKey();
        $token_string = isset($this->request->get['page']) ? $this->request->get['page'] : '';
        $token_string .= isset($this->request->get['limit']) ? $this->request->get['limit'] : '';
        $token_string .= urlencode(isset($this->request->get['searchKey']) ? $this->request->get['searchKey'] : '');
        $token_string .= isset($this->request->get['searchField']) ? $this->request->get['searchField'] : '';
        $token_string .= isset($this->request->get['id']) ? $this->request->get['id'] : '';
        $token_string .= isset($this->request->get['product_version_id']) ? $this->request->get['product_version_id'] : '';
        $token_string .= $apiKey;
        return md5($token_string);
    }

    private function getMd5TokenPost()
    {
        $apiKey = $this->getApiKey();
        $body_date_expert_token = $this->request->post;
        unset($body_date_expert_token['token']);
        $token_string = json_encode($body_date_expert_token);
        $token_string .= $apiKey;
        return md5($token_string);
    }

    private function getMd5TokenVerify()
    {
        $apiKey = $this->getApiKey();
        $token_string = isset($this->request->get['data']) ? $this->request->get['data'] : '';
        if (empty($token_string)) {
            return null;
        }

        $token_string .= $apiKey;

        return md5($token_string);
    }

    private function createNewCustomer($data)
    {
        $this->load->model('account/customer');
        $this->load->model('account/address');

        $customer = [
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'telephone' => $data['telephone'],
            'password' => token(9)
        ];

        $customer_id = $this->model_account_customer->addCustomer($customer);

        $address_data = array(
            'customer_id' => $customer_id,
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'company' => '',
            'city' => $data['city'],
            'district' => $data['district'],
            'wards' => $data['wards'],
            'postcode' => '',
            'address' => $data['address'],
            'phone' => $data['telephone'],
            'default' => true
        );

        $this->model_account_address->addAddressNew($customer_id, $address_data);

        return $customer_id;
    }

    private function getApiKey()
    {
        $this->load->model('setting/setting');

        return $this->model_setting_setting->getSettingValue(self::CONFIG_CHATBOT_NOVAON_API_KEY);
    }

    public function getMaxQuantityProduct($product, $order)
    {
        $this->load->model('catalog/product');
        if ($product['multi_versions'] == 1) {
            $product_version = $this->model_catalog_product->getProductByVersionId($product['product_id'], $order['product_version_id']);
            if ($product_version && $product['status'] == 1 && $product_version['deleted'] != 1 && $product_version['status'] == 1 && ($product_version['quantity'] > 0 || $product['sale_on_out_stock'] == 1)) {
                return true;
            } else {
                return false;
            }
        }
        if ($product['multi_versions'] == 0 && $product['deleted'] != 1 && $product['status'] == 1 && ($product['product_master_quantity'] > 0 || $product['sale_on_out_stock'] == 1)) {
            return true;
        }
        return false;
    }
}