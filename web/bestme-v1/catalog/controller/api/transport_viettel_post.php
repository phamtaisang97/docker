<?php

class ControllerApiTransportViettelPost extends Controller
{
    public static $VALID_VIETTEL_POST_ORDER_STATUES = [
        '501' => 'delivery_successful', // Giao thành công
        '503' => 'destroyed_spot',  // Tiêu hủy tại chỗ
        '504' => 'refund_successful', // Hoàn trả thành công
        '201' => 'cancel_ballot',  // Hủy phiếu gửi
        '107' => 'cancel_bill', // Hủy vận đơn
        '-15' => 'cancel_bill'  // Hủy vận đơn
    ];

    private $error = [];

    public function webhook()
    {
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $data = $this->request->post['DATA'];
            $token = $this->request->post['TOKEN'];

            if (array_key_exists($data['ORDER_STATUS'], self::$VALID_VIETTEL_POST_ORDER_STATUES)) {
                foreach (self::$VALID_VIETTEL_POST_ORDER_STATUES as $key => $value) {
                    if ($key == $data['ORDER_STATUS']) {
                        $this->load->model('sale/order');
                        $this->model_sale_order->updateStatusTransportDeliveryApi($data['ORDER_NUMBER'], $value);
                    }
                }
            }
            $json = [
                'code' => 200,
                'message' => 'successful'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));

            return;
        }

        $json = [
            'code' => 400,
            'message' => json_encode($this->error, JSON_PRETTY_PRINT)
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    protected function validateForm()
    {
        if (!isset($this->request->post['DATA'])) {
            $this->error[] = 'Missing "DATA"';
        }

        $data = $this->request->post['DATA'];
        if (!isset($data['ORDER_STATUS'])) {
            $this->error[] = 'Invalid "ORDER_STATUS"';
        }

        if (!array_key_exists($data['ORDER_STATUS'], self::$VALID_VIETTEL_POST_ORDER_STATUES)) {
            $this->error[] = 'Not support "ORDER_STATUS": ' . $data['ORDER_STATUS'];
        }

        if (!isset($this->request->post['TOKEN'])) {
            $this->error[] = 'Missing "TOKEN"';
        }
        // Get config token from db...
        $config_token = $this->config->get('config_VIETTEL_POST_token_webhook');
        if ($this->request->post['TOKEN'] != $config_token) {
            $this->error[] = 'Invalid "TOKEN"';
        }

        return !$this->error;
    }
}