<?php

class ControllerApiOpenApiManufacturers extends Controller
{
    use Open_Api;

    const PERMISSION = "manufacturer";

    /**
     * get order list
     *
     * @return bool
     */
    public function index()
    {
        if ($this->request->server['REQUEST_METHOD'] !== 'GET') {
            $error = [
                'code' => $this->STATUS_CODE['method_not_allowed'],
                'message' => 'Bad request. Use GET only!'
            ];

            $this->responseJson($error, $this->STATUS_CODE['method_not_allowed']);
            return false;
        }

        $json = [
            'code' => $this->STATUS_CODE['forbidden'],
            'message' => 'Not permission'
        ];

        if (!$this->validate(__FUNCTION__)) {
            $this->responseJson($json, $this->STATUS_CODE['forbidden']);
            return false;
        }

        /* do getting data */
        $data_filter = [];
        if (isset($this->request->get['page']) && !empty($this->request->get['page'])) {
            $data_filter = [
                'start' => ((int)$this->request->get['page'] - 1) * ((int)isset($this->request->get['limit']) ? $this->request->get['limit'] : 15),
                'limit' => isset($this->request->get['limit']) ? $this->request->get['limit'] : 15
            ];
        }

        $data_filter['manufacturer_ids'] = isset($this->request->get['manufacturer_ids']) ? $this->request->get['manufacturer_ids'] : '';

        $this->load->model('api/chatbox_novaon_v2');
        $manufacturers = $this->model_api_chatbox_novaon_v2->getManufacturersListData($data_filter);

        $this->responseJson($manufacturers);
        return true;
    }

    /**
     * get manufacture detail by id
     *
     * @return bool
     */
    public function show() {
        if ($this->request->server['REQUEST_METHOD'] !== 'GET') {
            $error = [
                'code' => $this->STATUS_CODE['method_not_allowed'],
                'message' => 'Bad request. Use GET only!'
            ];

            $this->responseJson($error, $this->STATUS_CODE['method_not_allowed']);
            return false;
        }

        if (empty($this->request->id)) {
            $response = [
                'code' => $this->STATUS_CODE['bad_request'],
                'message' => 'Missing id parameter'
            ];

            $this->responseJson($response, $this->STATUS_CODE['bad_request']);
        }

        $json = [
            'code' => $this->STATUS_CODE['forbidden'],
            'message' => 'Not permission'
        ];

        if (!$this->validate(__FUNCTION__)) {
            $this->responseJson($json, $this->STATUS_CODE['forbidden']);
            return false;
        }

        /* do getting data */
        $data_filter = [];
        $data_filter['manufacturer_ids'] = $this->request->get['id'];

        $this->load->model('api/chatbox_novaon_v2');
        $manufacturers = $this->model_api_chatbox_novaon_v2->getManufacturersList($data_filter);
        $manufacturer = empty($manufacturers[0]) ? [] : $manufacturers[0];

        $this->responseJson($manufacturer);
        return true;
    }

    /**
     * create a manufacturer
     *
     * @return bool
     */
    public function store() {
        if ($this->request->server['REQUEST_METHOD'] !== 'POST') {
            $error = [
                'code' => $this->STATUS_CODE['method_not_allowed'],
                'message' => 'Bad request. Use POST only!'
            ];

            $this->responseJson($error, $this->STATUS_CODE['method_not_allowed']);
            return false;
        }

        if (!$this->validate(__FUNCTION__)) {
            $response = [
                'code' => $this->STATUS_CODE['forbidden'],
                'message' => 'Not permission'
            ];

            $this->responseJson($response, $this->STATUS_CODE['forbidden']);
            return false;
        }

        $response = [
            'message' => 'Developing function'
        ];

        $this->responseJson($response);
        return true;
    }

    /**
     * update a manufacturer
     *
     * @return bool
     */
    public function update() {
        if ($this->request->server['REQUEST_METHOD'] !== 'PUT') {
            $error = [
                'code' => $this->STATUS_CODE['method_not_allowed'],
                'message' => 'Bad request. Use PUT only!'
            ];

            $this->responseJson($error, $this->STATUS_CODE['method_not_allowed']);
            return false;
        }

        if (!$this->validate(__FUNCTION__)) {
            $response = [
                'code' => $this->STATUS_CODE['forbidden'],
                'message' => 'Not permission'
            ];

            $this->responseJson($response, $this->STATUS_CODE['forbidden']);
            return false;
        }

        $response = [
            'message' => 'Developing function'
        ];

        $this->responseJson($response);
        return true;
    }

    /**
     * delete a manufacturer
     *
     * @return bool
     */
    public function delete()
    {
        if ($this->request->server['REQUEST_METHOD'] !== 'DELETE') {
            $error = [
                'code' => $this->STATUS_CODE['method_not_allowed'],
                'message' => 'Bad request. Use DELETE only!'
            ];

            $this->responseJson($error, $this->STATUS_CODE['method_not_allowed']);
            return false;
        }

        if (!$this->validate(__FUNCTION__)) {
            $response = [
                'code' => $this->STATUS_CODE['forbidden'],
                'message' => 'Not permission'
            ];

            $this->responseJson($response, $this->STATUS_CODE['forbidden']);
            return false;
        }

        // get post data from php://input
        $this->request->post = json_decode(file_get_contents('php://input'), true);
        if (!isset($this->request->post['data']['name']) || empty($this->request->post['data']['name'])) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Missing "name"!'
            ];

            $this->responseJson($error);
            return false;
        }

        try {
            $this->load->model('catalog/manufacturer');
            $manufacturer_name = $this->request->post['data']['name'];
            $manufacturer_status = isset($this->request->post['data']['status']) ? (int)$this->request->post['data']['status'] : 0;

            if (!in_array($manufacturer_status, [1, 2, 99])) {
                $json = [
                    'code' => 403,
                    'message' => 'Status not support!'
                ];

                $this->responseJson($json);
                return false;
            }

            $manufacturer_id = $this->model_catalog_manufacturer->getManufacturerIdByName($manufacturer_name);

            if (empty($manufacturer_id)) {
                $json = [
                    'code' => 404,
                    'message' => 'Not found manufacturer'
                ];
            } else {
                $this->model_catalog_manufacturer->updateManufacturerStatus($manufacturer_id, $manufacturer_status);

                $json = [
                    'code' => 200,
                    'manufacturer_id' => $manufacturer_id,
                    'message' => 'Manufacturer deleted successfully'
                ];
            }
        } catch (Exception $e) {
            $json = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->responseJson($json);
        return true;
    }

    public function sync()
    {
        if ($this->request->server['REQUEST_METHOD'] !== 'POST') {
            $json = [
                'message' => 'Bad request. Use POST only!'
            ];
            $this->responseJson($json, 400);
            return false;
        }

        $json = [
            'message' => 'Not permission'
        ];

        if (!$this->validate("sync", "modify")) {
            $this->responseJson($json, 400);
            return false;
        }

        try {
            $this->request->post = json_decode(file_get_contents('php://input'), true);
            $validate_result = $this->validateSyncManufacture();
            if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
                $error = is_array($validate_result) ? $validate_result : [];
                $this->responseJson($error);
                return;
            }

            /*build data to add product*/
            /*
             * [
             *  'name' => '123',
             *  'old_name' => '123 5',
             *  'source' => 'omihub',
             * ]
             * */
            $data_to_save = $this->request->post['data'];
            $this->load->model('catalog/manufacturer');
            $source = isset($data_to_save['source']) ? $data_to_save['source'] : '';
            $old_name = isset($data_to_save['old_name']) ? $data_to_save['old_name'] : '';

            if (!empty($old_name)) {
                $manufacture_id = $this->model_catalog_manufacturer->getManufacturerIdByName($data_to_save['old_name']);
            } else {
                $manufacture_id = $this->model_catalog_manufacturer->getManufacturerIdByName($data_to_save['name']);
            }

            if (isset($manufacture_id) && !empty($manufacture_id)) {
                $manufacture_id = $this->model_catalog_manufacturer->updateManufacturerFast($manufacture_id, $data_to_save['name'], $source);
            } else {
                $manufacture_id = $this->model_catalog_manufacturer->addManufacturerFast($data_to_save['name'], $source);
            }

            $json = [
                'code' => 200,
                'message' => 'Sync success!',
                'manufacture_id' => $manufacture_id
            ];
        } catch (Exception $exception) {
            $json = [
                'code' => 201,
                'message' => 'unknown error: ' . $exception->getMessage()
            ];
        }

        $this->responseJson($json);
        return;
    }

    public function validateSyncManufacture()
    {
        $bad_request_code = 400;
        $manufacture_name = isset($this->request->post['data']['name']) ? $this->request->post['data']['name'] : '';
        if ($manufacture_name == "") {
            return [
                'code' => $bad_request_code,
                'message' => 'missing or invalid manufacture name'
            ];
        }

        return [
            'code' => 200
        ];
    }
}