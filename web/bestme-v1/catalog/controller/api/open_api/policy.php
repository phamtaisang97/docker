<?php

class ControllerApiOpenApiPolicy extends Controller
{
    use Open_Api;
    const PERMISSION = "blog";

    public function index()
    {
        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("index")) {
            $this->responseJson($json);
            return;
        }
        /* do getting data */
        $this->load->model('online_store/contents');
        $json = $this->model_online_store_contents->getContentsAPI($this->request->get);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * @return bool
     */
    public function store()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->log->write('createPolicy(): resp: ' . 'Bad request. Use POST only!');

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("store")) {
            $this->responseJson($json);
            return;
        }

        $error = [];
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('createPolicy(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }
        // pass data post
        $data['title'] = isset($this->request->post['data']['title']) ? $this->request->post['data']['title'] : '';
        $data['description'] = isset($this->request->post['data']['description']) ? $this->request->post['data']['description'] : '';
        $data['seo_title'] = isset($this->request->post['data']['seo_title']) ? $this->request->post['data']['seo_title'] : '';
        $data['seo_description'] = isset($this->request->post['data']['seo_description']) ? $this->request->post['data']['seo_description'] : '';
        $data['status'] = 1;
        $data['source'] = "omihub";
        $data['alias'] = isset($this->request->post['data']['alias']) ? $this->request->post['data']['alias'] : '';
        $this->load->model('online_store/contents');
        $validate_result = $this->validatePolicyPostData();
        if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
            $this->model_online_store_contents->editContent($validate_result['policy_id'], $data);
            $json = [
                'code' => 200,
                'message' => 'policy exist => update for policy'
            ];
            $this->responseJson($json);
            return;
        }

        $this->model_online_store_contents->addContent($data);

        $json = [
            'code' => 200,
            'message' => 'Create policy success !',
        ];
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function delete()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'DELETE')) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Use DELETE only!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("delete")) {
            $this->responseJson($json);
            return;
        }

        $this->request->post = json_decode(file_get_contents('php://input'), true);

        // check if has policy_title
        if (!isset($this->request->post['data']['title']) || empty($this->request->post['data']['title'])) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Missing "title"!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));
            return;
        }
        $title = $this->request->post['data']['title'];
        // check exist blog_id in database
        $this->load->model('online_store/contents');
        $policy_id = $this->model_online_store_contents->checkPolicyExist($title);

        if (!$policy_id) {
            $error = [
                'code' => 506,
                'message' => 'Policy (with title) does not existed'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));
            return;
        }
        $this->model_online_store_contents->deleteContent($policy_id);

        $json = [
            'code' => 200,
            'message' => 'Delete policy success !',
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function validatePolicyPostData()
    {
        $this->load->model('online_store/contents');

        $name = isset($this->request->post['data']['title']) ? $this->request->post['data']['title'] : '';

        if (!$name) {
            return [
                'code' => 101,
                'message' => 'missing or invalid policy'
            ];
        }

        $exits = $this->model_online_store_contents->checkPolicyExist($this->request->post['data']['title']);

        if ($exits != false) {
            return [
                'code' => 125,
                'message' => 'error policy exist',
                'policy_id' => $exits
            ];
        }

        return [
            'code' => 200
        ];
    }
}