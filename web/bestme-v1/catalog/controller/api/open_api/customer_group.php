<?php

class ControllerApiOpenApiCustomerGroup extends Controller
{
    use Open_Api;
    const PERMISSION = "customer_group";
    // api/open_api/customer group
    public function index()
    {
        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("index")) {
            $this->responseJson($json);
            return;
        }
        /* do getting data */
        $this->load->model('customer/customer');
        $json = $this->model_customer_customer->getCustomerGroups($this->request->get);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function create()
    {
        $json = [];
        $this->responseJson($json);
        return;
    }

    public function store()
    {

        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->log->write('createCustomerGroup(): resp: ' . 'Bad request. Use POST only!');

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("store")) {
            $this->responseJson($json);
            return;
        }
        $error = [];
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('customerGroup(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        $validate_result = $this->validateCustomerGroupPostData();
        if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
            $error = is_array($validate_result) ? $validate_result : [];
            $this->responseJson($error);
            return;
        }

        $this->load->model('customer/customer');

        // pass data post
        $data['name_group'] = isset($this->request->post['name_group']) ? $this->request->post['name_group'] : '';
        $data['description'] = isset($this->request->post['description']) ? $this->request->post['description'] : '';
        // create customer

        $customer_group = [
            'name_group' => $data['name_group'],
            'description' => $data['description'],
        ];

        $this->model_customer_customer->addCustomerGroup($customer_group);

        $json = [
            'code' => 200,
            'message' => 'Create customer group success !',
        ];
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function show()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'GET')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use GET only!'
            ];

            $this->log->write('showCustomer(): resp: ' . 'Bad request. Use GET only!');

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("show")) {
            $this->responseJson($json);
            return;
        }

        // check if has customer_group_id
        if (!isset($this->request->get['customer_group_id']) || empty($this->request->get['customer_group_id'])) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Missing "customer_group_id"!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $customer_group_id = $this->request->get['customer_group_id'];
        $this->load->model('customer/customer');
        $result = $this->model_customer_customer->getCustomerGroup($customer_group_id);
        if (!$result) {
            $error = [
                'code' => 506,
                'message' => 'customer (with customer_group_id) does not existed'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    public function update()
    {
        $this->load->model('customer/customer');
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->log->write('updateCustomerGroup(): resp: ' . 'Bad request. Use POST only!');

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("store")) {
            $this->responseJson($json);
            return;
        }
        $this->request->post = json_decode(file_get_contents('php://input'), true);
        // check if has customer_group_id
        if (!isset($this->request->post['customer_group_id']) || empty($this->request->post['customer_group_id'])) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Missing "customer_group_id"!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $customer_group_id = $this->request->post['customer_group_id'];

        $exist = $this->model_customer_customer->getCustomerGroup($customer_group_id);
        if (!$exist) {
            $error = [
                'code' => 506,
                'message' => 'customer (with customer_group_id) does not existed'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $validate_result = $this->validateCustomerGroupPostData($exist);
        if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
            $error = is_array($validate_result) ? $validate_result : [];
            $this->responseJson($error);
            return;
        }

        // pass data post
        $data['name_group'] = isset($this->request->post['name_group']) ? $this->request->post['name_group'] : '';
        $data['description'] = isset($this->request->post['description']) ? $this->request->post['description'] : '';
        // create customer

        $customer_group = [
            'name_group' => $data['name_group'],
            'description' => $data['description'],
        ];

        $this->model_customer_customer->editCustomerGroup($customer_group_id, $customer_group);

        $json = [
            'code' => 200,
            'message' => 'Edit customer group success !',
        ];
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function delete()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'DELETE')) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Use DELETE only!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("delete")) {
            $this->responseJson($json);
            return;
        }
        $this->request->post = json_decode(file_get_contents('php://input'), true);
        // check if has customer_group_id
        if (!isset($this->request->post['customer_group_id']) || empty($this->request->post['customer_group_id'])) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Missing "customer_group_id"!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }
        $customer_group_id = $this->request->post['customer_group_id'];
        // check exist customer_id in database
        $this->load->model('customer/customer');
        $exist = $this->model_customer_customer->getCustomerGroup($customer_group_id);
        if (!$exist) {
            $error = [
                'code' => 506,
                'message' => 'customer (with customer_id) does not existed'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }
        $this->model_customer_customer->deleteCustomerGroup($customer_group_id);
        $json = [
            'code' => 200,
            'message' => 'Delete customer success !',
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function validateCustomerGroupPostData($exist = null)
    {
        $this->load->model('customer/customer');

        $customer_group_id = isset($this->request->post['customer_group_id']) ? $this->request->post['customer_group_id'] : '';

        $name = isset($this->request->post['name_group']) ? $this->request->post['name_group'] : '';

        if (!$name) {
            return [
                'code' => 101,
                'message' => 'missing or invalid name group'
            ];
        }
        if (isset($customer_group_id) && !empty($exist)) {

            if ($this->request->post['name_group'] != $exist['name']) {
                if ($this->model_customer_customer->checkNameGroupExist($this->request->post['name_group'], $customer_group_id) != false) {
                    return [
                        'code' => 125,
                        'message' => 'error name group exist'
                    ];
                }
            }

        } else {

            if ($this->request->post['name_group'] != $exist['name_group']) {
                if ($this->model_customer_customer->checkNameGroupExist($this->request->post['name_group'], $customer_group_id) != false) {
                    return [
                        'code' => 125,
                        'message' => 'error name group exist'
                    ];
                }
            }

        }

        return [
            'code' => 200
        ];
    }
}