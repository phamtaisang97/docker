<?php

class ControllerApiOpenApiCustomers extends Controller
{
    use Open_Api;
    const PERMISSION = "customer";
    // api/open_api/customers
    public function index()
    {
        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("index")) {
            $this->responseJson($json);
            return;
        }
        /* do getting data */
        $this->load->model('api/chatbox_novaon_v2');
        $json = $this->model_api_chatbox_novaon_v2->getCustomersListData($this->request->get);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function create()
    {
        $json = [];
        $this->responseJson($json);
        return;
    }

    public function store()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->log->write('createCustomer(): resp: ' . 'Bad request. Use POST only!');

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("store")) {
            $this->responseJson($json);
            return;
        }

        $error = [];
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('customer(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        $validate_result = $this->validateCustomerPostData();
        if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
            $error = is_array($validate_result) ? $validate_result : [];
            $this->responseJson($error);
            return;
        }

        $this->load->model('account/address');
        $this->load->model('customer/customer');
        $this->load->model('account/customer');
        $this->load->model('localisation/vietnam_administrative');

        // pass data post
        $data['telephone'] = isset($this->request->post['data']['customer']['phone_number']) ? $this->request->post['data']['customer']['phone_number'] : '';
        $data['customer_source'] = isset($this->request->post['data']['customer']['customer_source']) ? $this->request->post['data']['customer']['customer_source'] : '';
        $data['is_authenticated'] = isset($this->request->post['data']['customer']['is_authenticated']) ? $this->request->post['data']['customer']['is_authenticated'] : '';

        // extract first name, last name
        $full_name = isset($this->request->post['data']['customer']['name']) ? $this->request->post['data']['customer']['name'] : '';
        $full_name = trim($full_name);
        $first_and_last_name = explode(" ", $full_name);
        $first_name = array_pop($first_and_last_name);
        $last_name = implode(" ", $first_and_last_name);
        $data['fullname'] = $full_name;
        $data['lastname'] = trim($last_name);
        $data['firstname'] = trim($first_name);

        // mapping address
        $data['city'] = isset($this->request->post['data']['customer']['city']) ? $this->request->post['data']['customer']['city'] : '';
        $data['email'] = isset($this->request->post['data']['customer']['email']) ? $this->request->post['data']['customer']['email'] : '';
        $data['district'] = isset($this->request->post['data']['customer']['district']) ? $this->request->post['data']['customer']['district'] : '';
        $data['wards'] = isset($this->request->post['data']['customer']['ward']) ? $this->request->post['data']['customer']['ward'] : '';
        $data['address'] = isset($this->request->post['data']['customer']['delivery_addr']) ? $this->request->post['data']['customer']['delivery_addr'] : '';
        $data['customer_group_id'] = isset($this->request->post['data']['customer']['group_id']) ? $this->request->post['data']['customer']['group_id'] : $this->config->get('config_customer_group_id');
        $data['subscriber_id'] = isset($this->request->post['data']['customer']['subscriber_id']) ? $this->request->post['data']['customer']['subscriber_id'] : '';

        // create customer

        $customer = [
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'telephone' => $data['telephone'],
            'customer_source' => $data['customer_source'],
            'is_authenticated' => $data['is_authenticated'],
            'customer_group_id' => $data['customer_group_id'],
            'password' => token(9),
            'not_send_mail' => true,
            'subscriber_id' => $data['subscriber_id']
        ];

        $customer_id = $this->model_account_customer->addCustomer($customer);

        $address_data = array(
            'customer_id' => $customer_id,
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'company' => '',
            'city' => $data['city'],
            'district' => $data['district'],
            'wards' => $data['wards'],
            'postcode' => '',
            'address' => $data['address'],
            'phone' => $data['telephone'],
            'default' => true
        );

        $this->model_account_address->addAddressNew($customer_id, $address_data);
        $json = [
            'code' => 200,
            'message' => 'Create customer success !',
        ];
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function show()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'GET')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use GET only!'
            ];

            $this->log->write('showCustomer(): resp: ' . 'Bad request. Use GET only!');

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("show")) {
            $this->responseJson($json);
            return;
        }

        $customer_id = isset($this->request->get['customer_id']) ? $this->request->get['customer_id'] : '';
        $phone = isset($this->request->get['phone']) ? $this->request->get['phone'] : '';
        $email = isset($this->request->get['email']) ? $this->request->get['email'] : '';
        // check exist customer_id in database
        $this->load->model('customer/customer');

        if ($customer_id) {
            $exist = $this->model_customer_customer->getCustomer($customer_id);
            if (!$exist) {
                $error = [
                    'code' => 506,
                    'message' => 'customer (with customer_id) does not existed'
                ];

                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($error));

                return;
            }
            $result = $this->model_customer_customer->getCustomerById($customer_id);
        }else if ($phone) {
             $result = $this->model_customer_customer->getCustomerByTelephone($phone);
         }else if ($email) {
             $result = $this->model_customer_customer->getCustomerByEmail($email);
         }else {
            $result = [
                'code' => 201,
                'message' => 'Bad request. Missing "customer_id" !! '
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    public function update()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("update")) {
            $this->responseJson($json);
            return;
        }

        $error = array();

        // get post data from php://input
        // notice: could not get directly from $this->request->post or $_POST, TODO: why?...
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('[chatbot_novaon_v2.0] product(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        // check if has customer_id
        if (!isset($this->request->post['data']['customer']['customer_id']) || empty($this->request->post['data']['customer']['customer_id'])) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Missing "customer_id"!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $customer_id = $this->request->post['data']['customer']['customer_id'];
        // check exist customer_id in database
        $this->load->model('customer/customer');
        $exist = $this->model_customer_customer->getCustomer($customer_id);
        if (!$exist) {
            $error = [
                'code' => 506,
                'message' => 'customer (with customer_id) does not existed'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }
        $validate_result = $this->validateCustomerPostData($exist);
        if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
            $error = is_array($validate_result) ? $validate_result : [];
            $this->responseJson($error);
            return;
        }
        $this->load->model('account/address');
        $this->load->model('customer/customer');
        $this->load->model('account/customer');
        $this->load->model('localisation/vietnam_administrative');

        // pass data post
        $data['telephone'] = isset($this->request->post['data']['customer']['phone_number']) ? $this->request->post['data']['customer']['phone_number'] : '';
        $data['customer_source'] = isset($this->request->post['data']['customer']['customer_source']) ? $this->request->post['data']['customer']['customer_source'] : '';
        $data['is_authenticated'] = isset($this->request->post['data']['customer']['is_authenticated']) ? $this->request->post['data']['customer']['is_authenticated'] : '';

        // extract first name, last name
        $full_name = isset($this->request->post['data']['customer']['name']) ? $this->request->post['data']['customer']['name'] : '';
        $full_name = trim($full_name);
        $first_and_last_name = explode(" ", $full_name);
        $first_name = array_pop($first_and_last_name);
        $last_name = implode(" ", $first_and_last_name);
        $data['fullname'] = $full_name;
        $data['lastname'] = trim($last_name);
        $data['firstname'] = trim($first_name);

        // mapping address
        $data['city'] = isset($this->request->post['data']['customer']['city']) ? $this->request->post['data']['customer']['city'] : '';
        $data['email'] = isset($this->request->post['data']['customer']['email']) ? $this->request->post['data']['customer']['email'] : '';
        $data['district'] = isset($this->request->post['data']['customer']['district']) ? $this->request->post['data']['customer']['district'] : '';
        $data['wards'] = isset($this->request->post['data']['customer']['ward']) ? $this->request->post['data']['customer']['ward'] : '';
        $data['address'] = isset($this->request->post['data']['customer']['delivery_addr']) ? $this->request->post['data']['customer']['delivery_addr'] : '';
        $data['customer_group_id'] = isset($this->request->post['data']['customer']['group_id']) ? $this->request->post['data']['customer']['group_id'] : $this->config->get('config_customer_group_id');
        $data['subscriber_id'] = isset($this->request->post['data']['customer']['subscriber_id']) ? $this->request->post['data']['customer']['subscriber_id'] : '';

        // create customer
        $address_data = array(
            'customer_id' => $customer_id,
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'company' => '',
            'city' => $data['city'],
            'district' => $data['district'],
            'wards' => $data['wards'],
            'postcode' => '',
            'address' => $data['address'],
            'phone' => $data['telephone'],
            'default' => true
        );

        $customer = [
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'telephone' => $data['telephone'],
            'customer_source' => $data['customer_source'],
            'is_authenticated' => $data['is_authenticated'],
            'customer_group_id' => $data['customer_group_id'],
            'password' => token(9),
            'not_send_mail' => true,
            'subscriber_id' => $data['subscriber_id'],
        ];

        $this->model_customer_customer->editCustomer($customer_id, $customer);
        $this->model_account_address->editAddressNew($customer_id, $address_data);
        $json = [
            'code' => 200,
            'message' => 'Edit customer success !',
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function delete()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'DELETE')) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Use DELETE only!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("delete")) {
            $this->responseJson($json);
            return;
        }
        $this->request->post = json_decode(file_get_contents('php://input'), true);
        // check if has customer_id
        if (!isset($this->request->post['customer_id']) || empty(['customer_id'])) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Missing "customer_id"!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $customer_id = $this->request->post['customer_id'];
        // check exist customer_id in database
        $this->load->model('customer/customer');
        $exist = $this->model_customer_customer->getCustomer($customer_id);
        if (!$exist) {
            $error = [
                'code' => 506,
                'message' => 'customer (with customer_id) does not existed'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }
        $this->model_customer_customer->deleteCustomer($customer_id);
        $json = [
            'code' => 200,
            'message' => 'Delete customer success !',
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function orders() {
        if (($this->request->server['REQUEST_METHOD'] !== 'GET')) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Use GET only!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("index")) {
            $this->responseJson($json);
            return;
        }
        $data['customer_id'] = $this->request->get['customer_id'];
        $this->load->model('customer/customer');
        $result = $this->model_customer_customer->getOrderByCustomer($data);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    public function validateCustomerPostData($exist = null)
    {
        $this->load->model('api/chatbox_novaon_v2');

        $customer_id = isset($this->request->post['data']['customer']['customer_id']) ? $this->request->post['data']['customer']['customer_id'] : '';

        $name = isset($this->request->post['data']['customer']['name']) ? $this->request->post['data']['customer']['name'] : '';

        if (!$name) {
            return [
                'code' => 101,
                'message' => 'missing or invalid name'
            ];
        }

        //validate edit customer
        if (isset($customer_id) && !empty($exist)) {

            if ($this->request->post['data']['customer']['phone_number'] != $exist['telephone']) {
                if ($this->model_api_chatbox_novaon_v2->checkPhoneExist($this->request->post['data']['customer']['phone_number'], $customer_id) != false) {
                    return [
                        'code' => 125,
                        'message' => 'error phone exist'
                    ];
                }
            }

            if ($this->request->post['data']['customer']['email'] != $exist['email']) {
                if ($this->model_api_chatbox_novaon_v2->checkEmailExist($this->request->post['data']['customer']['email'], $customer_id) != false) {
                    return [
                        'code' => 125,
                        'message' => 'error email exist'
                    ];
                }
            }

        } else {
            if ($this->model_api_chatbox_novaon_v2->checkPhoneExist($this->request->post['data']['customer']['phone_number'], $customer_id) != false) {
                return [
                    'code' => 125,
                    'message' => 'error phone exist'
                ];
            }

            if ($this->model_api_chatbox_novaon_v2->checkEmailExist($this->request->post['data']['customer']['email'], $customer_id) != false) {
                return [
                    'code' => 125,
                    'message' => 'error email exist'
                ];
            }
        }
        return [
            'code' => 200
        ];
    }
}