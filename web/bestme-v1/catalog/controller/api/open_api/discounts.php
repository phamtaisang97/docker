<?php

class ControllerApiOpenApiDiscounts extends Controller
{
    use Open_Api;

    const PERMISSION = "discount";

    /**
     * @return bool
     */
    public function index()
    {
        if ($this->request->server['REQUEST_METHOD'] !== 'GET') {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Use GET only!'
            ];

            $this->responseJson($error, 400);
            return false;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("index")) {
            $this->responseJson($json, 400);
            return false;
        }

        /* do getting data */
        $response_code = 200;
        try {$data_filter = [];
            $query_params = $this->request->get;

            if (isset($query_params['page']) && !empty($query_params['page'])) {
                $data_filter = [
                    'start' => ((int)$query_params['page'] - 1) * ((int)isset($query_params['limit']) ? $query_params['limit'] : 15),
                    'limit' => isset($query_params['limit']) ? $query_params['limit'] : 15
                ];
            }

            if (isset($query_params['name'])) {
                $data_filter = array_merge($data_filter, [
                    'filter_name' => $query_params['name']
                ]);
            }

            if (isset($query_params['discount_type_id'])) {
                $data_filter = array_merge($data_filter, [
                    'discount_type_id' => $query_params['discount_type_id']
                ]);
            }

            if (isset($query_params['source'])) {
                $data_filter = array_merge($data_filter, [
                    'source' => $query_params['source']
                ]);
            }

            $this->load->model('discount/discount');
            $json = $this->model_discount_discount->getDiscountsResult($data_filter);

            if (empty($json)) {
                $json = [
                    'code' => 201,
                    'message' => 'No data'
                ];
            }

        } catch (Exception $e) {
            $json = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
            $response_code = 500;
        }

        $this->responseJson($json, $response_code);
        return true;
    }

    public function create()
    {
        return;
    }

    /**
     * @return bool
     */
    public function store()
    {
        try {
            if ($this->request->server['REQUEST_METHOD'] !== 'POST') {
                $json = [
                    'message' => 'Bad request. Use POST only!'
                ];
                $this->responseJson($json, 400);
                return false;
            }

            $json = [
                'message' => 'Not permission'
            ];

            if (!$this->validate("store")) {
                $this->responseJson($json, 400);
                return false;
            }

            $this->request->post = json_decode(file_get_contents('php://input'), true);

            $validate_result = $this->validateDiscountInputData();
            if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
                $error = is_array($validate_result) ? $validate_result : [];
                $this->responseJson($error);
                return false;
            }

            $data = $this->request->post;
            // convert config data
            $data['config'] = $this->modifyConfig($data['config'], $data['discount_type_id']);

            $this->load->model('discount/discount');
            $this->model_discount_discount->addDiscount($data);
            $json = [
                'message' => 'Create discount success!',
            ];

            $this->responseJson($json);
            return true;
        } catch (Exception $e) {
            $this->responseJson(['message' => $e->getMessage()], 500);
            return false;
        }
    }

    /**
     *
     */
    public function show()
    {
        try {
            if ($this->request->server['REQUEST_METHOD'] !== 'GET') {
                $error = [
                    'code' => 101,
                    'message' => 'Bad request. Use GET only!'
                ];

                $this->responseJson($error);
                return;
            }

            $json = [
                'code' => 201,
                'message' => 'Not permission'
            ];

            if (!$this->validate("index")) {
                $this->responseJson($json);
                return;
            }

            $data_filter = [];
            $query_params = $this->request->get;

            if (empty($query_params['discount_id']) || $query_params['discount_id'] == "") {
                $json = [
                    'code' => 201,
                    'message' => 'Missing param discount_id or not empty'
                ];

                $this->responseJson($json);
                return;
            }

            if ( filter_var($query_params['discount_id'], FILTER_VALIDATE_INT) === false ) {
                $json = [
                    'code' => 201,
                    'message' => 'Discount_id is not int'
                ];

                $this->responseJson($json);
                return;
            }

            if (isset($query_params['discount_id'])) {
                $data_filter = array_merge($data_filter, [
                    'filter_discount_id' => $query_params['discount_id']
                ]);
            }

            $this->load->model('discount/discount');
            $json = $this->model_discount_discount->getDiscountsResult($data_filter);

            if (empty($json)) {
                $json = [
                    'code' => 201,
                    'message' => 'No data'
                ];

                $this->responseJson($json);
                return;
            }

        } catch (Exception $e) {
            $json = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];

            $this->responseJson($json);
            return;
        }

        $discount_detail = [];
        if (isset($json['records']) && is_array($json['records']) && count($json['records']) > 0) {
            $discount_detail = $json['records'][0];
        }

        $this->responseJson($discount_detail);
        return;
    }

    /**
     * @return bool
     */
    public function update()
    {
        try {
            if ($this->request->server['REQUEST_METHOD'] !== 'PUT') {
                $json = [
                    'message' => 'Bad request. Use PUT only!'
                ];
                $this->responseJson($json, 400);
                return false;
            }

            $json = [
                'message' => 'Not permission'
            ];

            if (!$this->validate("store")) {
                $this->responseJson($json, 400);
                return false;
            }

            $this->request->post = json_decode(file_get_contents('php://input'), true);

            $validate_result = $this->validateDiscountInputData(true);
            if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
                $error = is_array($validate_result) ? $validate_result : [];
                $this->responseJson($error);
                return false;
            }

            $data = $this->request->post;
            // convert config data
            $data['config'] = $this->modifyConfig($data['config'], $data['discount_type_id']);

            $this->load->model('discount/discount');
            $this->model_discount_discount->editDiscount($data['discount_id'], $data);
            $json = [
                'message' => 'Update discount success!',
            ];

            $this->responseJson($json);
            return true;
        } catch (Exception $e) {
            $this->responseJson(['message' => $e->getMessage()], 500);
            return false;
        }
    }

    public function delete()
    {
        return;
    }

    /**
     * @return bool
     */
    public function sync()
    {
        try {
            if ($this->request->server['REQUEST_METHOD'] !== 'POST') {
                $json = [
                    'message' => 'Bad request. Use POST only!'
                ];
                $this->responseJson($json, 400);
                return false;
            }

            $json = [
                'message' => 'Not permission'
            ];

            if (!$this->validate("store")) {
                $this->responseJson($json, 400);
                return false;
            }

            $this->request->post = json_decode(file_get_contents('php://input'), true);

            $validate_result = $this->validateDiscountInputData();
            if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
                $error = is_array($validate_result) ? $validate_result : [];
                $this->responseJson($error);
                return false;
            }

            $data = $this->request->post;
            // convert config data
            $data['config'] = $this->modifyConfig($data['config'], $data['discount_type_id'], false);

            $this->load->model('discount/discount');

            // check if discount is exists
            $discounts = $this->model_discount_discount->getDiscountByName(empty($data['old_discount_code']) ? $data['discount_code'] : $data['old_discount_code']);
            $data['discount_id'] = empty($discounts[0]['discount_id']) ? 0 : $discounts[0]['discount_id'];



            $data['config'] = json_encode($data['config']);
            if (empty($discounts[0])) {
                $this->model_discount_discount->addDiscount($data);
            } else {
                $this->model_discount_discount->editDiscount($discounts[0]['discount_id'], $data);
            }
            $json = [
                'message' => 'Sync discount success!',
            ];

            $this->responseJson($json);
            return true;
        } catch (Exception $e) {
            $this->responseJson(['message' => $e->getMessage()], 500);
            return false;
        }
    }

    /**
     * @param false $is_update
     * @return array|int[]
     */
    private function validateDiscountInputData($is_update = false)
    {
        $error_code = 400;

        if ($is_update) {
            if (empty($this->request->post['discount_id'])) {
                return [
                    'code' => $error_code,
                    'message' => 'missing discount id'
                ];
            }

            $discount_id = $this->request->post['discount_id'];
            $this->load->model('discount/discount');
            $discount = $this->model_discount_discount->getDiscountById($discount_id);
            if (empty($discount)) {
                return [
                    'code' => $error_code,
                    'message' => 'invalid discount id'
                ];
            }
        }

        if (!isset($this->request->post['discount_code'])) {
            return [
                'code' => $error_code,
                'message' => 'missing discount_code'
            ];
        }

        if (!isset($this->request->post['old_discount_code'])) {
            return [
                'code' => $error_code,
                'message' => 'missing old discount code'
            ];
        }

        if (!isset($this->request->post['config'])) {
            return [
                'code' => $error_code,
                'message' => 'missing config'
            ];
        }

        if (!isset($this->request->post['name'])) {
            return [
                'code' => $error_code,
                'message' => 'missing name'
            ];
        }

        return [
            'code' => 200
        ];
    }

    /**
     * @param $configs
     * @return array
     */
    private function modifyProductConfig($configs) {
        $this->load->model('catalog/product');
        foreach ($configs as $key => $config) {
            if (0 == $config['all_product']) {
                if (empty($config['product_version_ids'])) {
                    // product has one version
                    $product = $this->model_catalog_product->getProductOneVersionBySKU($config['product_sku']);
                } else {
                    // product has multiple versions
                    $product_versions = $this->model_catalog_product->getProductMultiVersionByVersionSKUES($config['product_version_skus'], true);
                    $product_version_ids = array_map(function ($item) {
                        return $item['product_version_id'];
                    }, $product_versions);
                    $configs[$key]['product_version_ids'] = $product_version_ids;

                    if (!empty($product_versions[0]['product_id'])) {
                        $product['product_id'] = $product_versions[0]['product_id'];
                    }
                }
                // update product sku using product id
                if (!empty($product['product_id'])) {
                    $configs[$key]['product_id'] = $product['product_id'];
                }
            }
        }

        return $configs;
    }

    /**
     * @param $configs
     * @return array
     */
    private function modifyCategoryConfig($configs) {
        $this->load->model('catalog/category');

        foreach ($configs as $key => $config) {
            if (0 == $config['all_category']) {
                // add category name
                $category = $this->model_catalog_category->getCategoryByName(['name' => $config['category_name']]);
                if (!empty($category['category_id'])) {
                    $configs[$key]['category_id'] = $category['category_id'];
                }
            }
        }

        return $configs;
    }

    /**
     * @param $configs
     * @return array
     */
    private function modifyManufacturerConfig($configs) {
        $this->load->model('catalog/manufacturer');

        foreach ($configs as $key => $config) {
            if (0 == $config['all_manufacturer']) {
                // add manufacturer name
                $manufacturer = $this->model_catalog_manufacturer->getManufacturerByName(['name' => $config['manufacturer_name']]);
                if (!empty($manufacturer['manufacturer_id'])) {
                    $configs[$key]['manufacturer_id'] = $manufacturer['manufacturer_id'];
                }
            }
        }

        return $configs;
    }

    /**
     * @param $configs
     * @param $discount_type
     * @param bool $need_encode
     * @return array|false|mixed|string
     */
    private function modifyConfig($configs, $discount_type, $need_encode = true) {
        $configs = json_decode($configs, true);
        switch ($discount_type) {
            case 2:
                $configs = $this->modifyProductConfig($configs);
                break;
            case 3:
                $configs = $this->modifyCategoryConfig($configs);
                break;
            case 4:
                $configs = $this->modifyManufacturerConfig($configs);
                break;
        }
        return $need_encode ? json_encode($configs) : $configs;
    }
}