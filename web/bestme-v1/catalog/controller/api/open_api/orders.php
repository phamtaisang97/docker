<?php

class ControllerApiOpenApiOrders extends Controller
{
    use Open_Api;

    const PERMISSION = "product";

    /**
     * get order list
     *
     * @return bool
     */
    public function index()
    {
        if ($this->request->server['REQUEST_METHOD'] !== 'GET') {
            $error = [
                'code' => $this->STATUS_CODE['method_not_allowed'],
                'message' => 'Bad request. Use GET only!'
            ];

            $this->responseJson($error, $this->STATUS_CODE['method_not_allowed']);
            return false;
        }

        $json = [
            'code' => $this->STATUS_CODE['forbidden'],
            'message' => 'Not permission'
        ];

        if (!$this->validate(__FUNCTION__)) {
            $this->responseJson($json, $this->STATUS_CODE['forbidden']);
            return false;
        }

        /* do getting data */
        $this->load->language('api/chatbot_novaon');

        // check parameter
        $data_filter = [];
        if (isset($this->request->get['page']) && !empty($this->request->get['page'])) {
            $data_filter = [
                'start' => ((int)$this->request->get['page'] - 1) * ((int)isset($this->request->get['limit']) ? $this->request->get['limit'] : 15),
                'limit' => isset($this->request->get['limit']) ? $this->request->get['limit'] : 15
            ];
        }

        $data_filter['order_status'] = isset($this->request->get['order_status']) ? $this->request->get['order_status'] : '';
        $data_filter['order_payment'] = isset($this->request->get['order_payment']) ? $this->request->get['order_payment'] : '';
        $data_filter['order_total_type'] = isset($this->request->get['order_total_type']) ? $this->request->get['order_total_type'] : '';
        $data_filter['order_code'] = isset($this->request->get['order_code']) ? $this->request->get['order_code'] : '';
        $data_filter['order_ids'] = isset($this->request->get['order_ids']) ? $this->request->get['order_ids'] : '';

        $data_filter['order_campaign'] = isset($this->request->get['order_campaign']) ? $this->request->get['order_campaign'] : '';
        $data_filter['order_channel'] = isset($this->request->get['order_channel']) ? $this->request->get['order_channel'] : '';
        $data_filter['order_product'] = isset($this->request->get['order_product']) ? $this->request->get['order_product'] : '';

        $data_filter['order_customer_ids'] = isset($this->request->get['order_customer_ids']) ? $this->request->get['order_customer_ids'] : '';
        $data_filter['order_promotion_code'] = isset($this->request->get['order_promotion_code']) ? $this->request->get['order_promotion_code'] : '';

        $data_filter['order_subscriber_ids'] = isset($this->request->get['order_subscriber_ids']) ? $this->request->get['order_subscriber_ids'] : '';

        $this->load->model('api/chatbox_novaon_v2');
        $orders = $this->model_api_chatbox_novaon_v2->getOrdersListData($data_filter);

        $this->responseJson($orders);

        return true;
    }

    /**
     * get order detail by id
     *
     * @return bool
     */
    public function show() {
        if ($this->request->server['REQUEST_METHOD'] !== 'GET') {
            $error = [
                'code' => $this->STATUS_CODE['method_not_allowed'],
                'message' => 'Bad request. Use GET only!'
            ];

            $this->responseJson($error, $this->STATUS_CODE['method_not_allowed']);
            return false;
        }

        $json = [
            'code' => $this->STATUS_CODE['forbidden'],
            'message' => 'Not permission'
        ];

        if (!$this->validate(__FUNCTION__)) {
            $this->responseJson($json, $this->STATUS_CODE['forbidden']);
            return false;
        }

        $this->load->language('api/chatbot_novaon');

        // check parameter
        $order_id = isset($this->request->get['id']) ? $this->request->get['id'] : '';
        if (empty($order_id)) {
            $json = [
                'code' => $this->STATUS_CODE['bad_request'],
                'message' => 'Missing order id'
            ];

            $this->responseJson($json, $this->STATUS_CODE['bad_request']);
            return false;
        }

        // do getting order detail
        $data_filter = [];
        // $data_filter['customer_ref_id'] = isset($this->request->get['customer_ref_id']) ? $this->request->get['customer_ref_id'] : '';
        $data_filter['order_ids'] = $order_id;

        $this->load->model('api/chatbox_novaon_v2');
        $json = $this->model_api_chatbox_novaon_v2->getOrdersListData($data_filter);
        $order_detail = [];
        if (isset($json['records']) && is_array($json['records']) && count($json['records']) > 0) {
            $order_detail = $json['records'][0];
        }

        $this->responseJson($order_detail);
        return true;
    }
}