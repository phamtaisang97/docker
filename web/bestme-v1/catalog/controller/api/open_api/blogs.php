<?php

class ControllerApiOpenApiBlogs extends Controller
{
    use Open_Api;

    const PERMISSION = "blog";

    const SCOPE_READ = "read";
    const SCOPE_MODIFY = "modify";

    public function index()
    {
        $json = [
            'message' => 'Not permission'
        ];

        if (!$this->validate("index")) {
            $this->responseJson($json, 400);
            return false;
        }
        try {
            /* do getting data */
            $this->load->model('blog/blog');
            $json = $this->model_blog_blog->getAllBlogs($this->request->get);
            $this->responseJson($json);
            return true;
        } catch (\Exception $exception) {
            $this->responseJson(['message' => $exception->getMessage()], 500);
            return false;
        }
    }

    public function create()
    {
        $json = [];
        $this->responseJson($json);
        return;
    }

    /**
     * @return bool
     */
    public function store()
    {
        try {
            if ($this->request->server['REQUEST_METHOD'] !== 'POST') {
                $json = [
                    'message' => 'Bad request. Use POST only!'
                ];
                $this->responseJson($json, 400);
                return false;
            }

            $json = [
                'message' => 'Not permission'
            ];

            if (!$this->validate("store")) {
                $this->responseJson($json, 400);
                return false;
            }

            $this->request->post = json_decode(file_get_contents('php://input'), true);

            $validate_result = $this->validateBlogInputData();
            if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
                $error = is_array($validate_result) ? $validate_result : [];
                $this->responseJson($error);
                return false;
            }
            $blog = $this->refactorBlogInputData();

            $this->model_blog_blog->addBlog($blog);
            $json = [
                'message' => 'Create blog success!',
            ];

            $this->responseJson($json);
            return true;
        } catch (Exception $e) {
            $this->responseJson(['message' => $e->getMessage()], 500);
            return false;
        }
    }

    public function show()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'GET')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use GET only!'
            ];

            $this->log->write('resp: ' . 'Bad request. Use GET only!');

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("show")) {
            $this->responseJson($json);
            return;
        }

        // check if has blog_id
        if (!isset($this->request->get['blog_id']) || empty($this->request->get['blog_id'])) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Missing "blog_id"!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $blog_id = $this->request->get['blog_id'];
        $this->load->model('blog/blog');
        $result = $this->model_blog_blog->getBlog($blog_id);
        if (!$result) {
            $error = [
                'code' => 506,
                'message' => 'blog (with blog_id) does not existed'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    /**
     * @return bool
     */
    public function update()
    {
        try {
            if ($this->request->server['REQUEST_METHOD'] !== 'PUT') {
                $error = [
                    'message' => 'Bad request. Use PUT only!'
                ];
                $this->responseJson($error, 400);
                return false;
            }

            $json = [
                'message' => 'Not permission'
            ];

            if (!$this->validate("update")) {
                $this->responseJson($json, 400);
                return false;
            }

            $this->request->post = json_decode(file_get_contents('php://input'), true);

            $validate_result = $this->validateBlogInputData(true);
            if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
                $error = is_array($validate_result) ? $validate_result : [];
                $this->responseJson($error);
                return false;
            }

            $this->load->model('blog/blog');
            $data = $this->refactorBlogInputData();
            $this->model_blog_blog->editBlog($this->request->post['blog_id'], $data);
            $json = [
                'code' => 200,
                'message' => 'Edit blog success !',
            ];
            $this->responseJson($json);
            return true;
        } catch (\Exception $exception) {
            $this->responseJson(['message' => $exception->getMessage()], 500);
            return false;
        }
    }

    public function delete()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'DELETE')) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Use DELETE only!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("delete")) {
            $this->responseJson($json);
            return;
        }
        $this->request->post = json_decode(file_get_contents('php://input'), true);
        // check if has blog_id
        if (!isset($this->request->post['blog_id']) || empty($this->request->post['blog_id'])) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Missing "category_id"!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }
        $blog_id = $this->request->post['blog_id'];
        // check exist blog_id in database
        $this->load->model('blog/blog');
        $exist = $this->model_blog_blog->getBlog($blog_id);
        if (!$exist) {
            $error = [
                'code' => 506,
                'message' => 'blog (with blog_id) does not existed'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }
        $this->model_blog_blog->deleteBlog($blog_id);
        $json = [
            'code' => 200,
            'message' => 'Delete blog success !',
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function deleteBlog() {
        if (($this->request->server['REQUEST_METHOD'] !== 'DELETE')) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Use DELETE only!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("delete")) {
            $this->responseJson($json);
            return;
        }
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        // check if has blog_title
        if (!isset($this->request->post['title']) || empty($this->request->post['title'])) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Missing "title"!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }
        $title = $this->request->post['title'];
        // check exist blog_id in database
        $this->load->model('blog/blog');
        $exist = $this->model_blog_blog->getBlogByTitle($title);
        if (!$exist) {
            $error = [
                'code' => 506,
                'message' => 'blog (with title) does not existed'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }
        $this->model_blog_blog->deleteBlog($exist['blog_id']);
        $json = [
            'code' => 200,
            'message' => 'Delete blog success !',
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * @return bool
     */
    public function sync()
    {
        try {
            if ($this->request->server['REQUEST_METHOD'] !== 'POST') {
                $json = [
                    'message' => 'Bad request. Use POST only!'
                ];
                $this->responseJson($json, 400);
                return false;
            }

            $json = [
                'message' => 'Not permission'
            ];

            if (!$this->validate("sync", self::SCOPE_MODIFY)) {
                $this->responseJson($json, 400);
                return false;
            }

            $this->request->post = json_decode(file_get_contents('php://input'), true);

            $validate_result = $this->validateBlogInputData();
            if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
                $error = is_array($validate_result) ? $validate_result : [];
                $this->responseJson($error);
                return false;
            }

            $data = $this->refactorBlogInputData();

            // check if blog is exists
            $blog = $this->model_blog_blog->getBlogByTitle(empty($data['old_title']) ? $data['title'] : $data['old_title']);

            if (empty($blog)) {
                $this->model_blog_blog->addBlog($data);
            } else {
                $this->model_blog_blog->editBlog($blog['blog_id'], $data);
            }

            $json = [
                'message' => 'Sync blog success!',
            ];
            $this->responseJson($json);
            return true;
        } catch (Exception $e) {
            $this->responseJson(['message' => $e->getMessage()], 500);
            return false;
        }
    }

    /**
     * @param null $exist
     * @return array|void
     */
    private function validateBlogInputData($is_update = false)
    {
        $error_code = 400;

        if ($is_update) {
            if (empty($this->request->post['blog_id'])) {
                return [
                    'code' => $error_code,
                    'message' => 'missing blog id'
                ];
            }

            $blog_id = $this->request->post['blog_id'];
            $this->load->model('blog/blog');
            $blog = $this->model_blog_blog->getBlog($blog_id);
            if (empty($blog)) {
                return [
                    'code' => $error_code,
                    'message' => 'invalid blog id'
                ];
            }
        }

        $title = isset($this->request->post['title']) ? $this->request->post['title'] : '';
        if (!$title) {
            return [
                'code' => $error_code,
                'message' => 'missing or invalid name blog !'
            ];
        }

        if (empty($this->request->post['blog_category'])) {
            return [
                'code' => $error_code,
                'message' => 'missing or invalid blog category!'
            ];
        }

        // validate author
        if (empty($this->request->post['author'])) {
            return [
                'code' => $error_code,
                'message' => 'missing or invalid author!'
            ];
        } else {
            $author = $this->request->post['author'];
            $this->load->model('user/user');
            $author = is_numeric($author) ?  $this->model_user_user->getUser($this->request->post['author']) : $this->model_user_user->getUserByEmail($this->request->post['author']);
            if (empty($author['user_id'])) {
                // get author info from input
                $this->request->post['source_author_name'] = $this->request->post['author_name'];
                $this->request->post['author'] = 0;
            } else {
                // get author info from user saved in database
                $this->request->post['source_author_name'] = null;
                $this->request->post['author'] = $author['user_id'];
            }
        }

        return [
            'code' => 200
        ];
    }

    /**
     * @return array
     */
    private function refactorBlogInputData()
    {
        try {
            $blog = [];

            // init default values
            $blog['title'] = trim(isset($this->request->post['title'])) ? trim($this->request->post['title']) : '';
            $blog['old_title'] = trim(isset($this->request->post['old_title'])) ? trim($this->request->post['old_title']) : '';
            $blog['content'] = isset($this->request->post['content']) ? $this->request->post['content'] : '';
            $blog['short_content'] = isset($this->request->post['short_content']) ? $this->request->post['short_content'] : '';
            $blog['status'] = isset($this->request->post['status']) ? $this->request->post['status'] : 0;
            $blog['image'] = isset($this->request->post['image']) ? $this->request->post['image'] : '';
            $blog['alt'] = isset($this->request->post['alt']) ? $this->request->post['alt'] : '';
            $blog['seo_name'] = trim(isset($this->request->post['seo_name'])) ? trim($this->request->post['seo_name']) : '';
            $blog['body'] = isset($this->request->post['body']) ? $this->request->post['body'] : '';
            $blog['alias'] = isset($this->request->post['alias']) ? $this->request->post['alias'] : '';
            $blog['seo_keywords'] = isset($this->request->post['seo_keywords']) ? $this->request->post['seo_keywords'] : '';
            $blog['author'] = isset($this->request->post['author']) ? $this->request->post['author'] : '';
            $blog['blog_category'] = isset($this->request->post['blog_category']) ? $this->request->post['blog_category'] : '';
            $blog['blog_category']['old_title'] = isset($this->request->post['blog_category']['old_title']) ? $this->request->post['blog_category']['old_title'] : '';
            $blog['tag_list'] = isset($this->request->post['tag_list']) ? $this->request->post['tag_list'] : '';
            $blog['type'] = isset($this->request->post['type']) ? $this->request->post['type'] : 'image';
            $blog['video_url'] = isset($this->request->post['video_url']) ? $this->request->post['video_url'] : '';
            $blog['source'] = isset($this->request->post['source']) ? $this->request->post['source'] : null;
            $blog['source_author_name'] = isset($this->request->post['source_author_name']) ? $this->request->post['source_author_name'] : null;

            /* convert data values */
            $this->load->model('blog/blog');

            // category
            if (!empty($blog['blog_category'])) {
                $blog_category = $this->model_blog_blog->getBlogCategoryByTitle(empty($blog['blog_category']['old_title']) ? $blog['blog_category']['title'] : $blog['blog_category']['old_title']);
                $blog['blog_category']['source'] = $blog['source'];
                if (empty($blog_category['blog_category_id'])) {
                    // create
                    $blog['blog_category'] = $this->model_blog_blog->createBlogCategory($blog['blog_category']);
                } else {
                    // update
                    $blog['blog_category']['blog_category_id'] = $blog_category['blog_category_id'];
                    $this->model_blog_blog->editBlogCategory($blog['blog_category']);
                    $blog['blog_category'] = $blog_category['blog_category_id'];
                }
            }

            // tag
            $tags_list = [];
            if (is_string($blog['tag_list'])) {
                $tags_list = explode(',', $blog['tag_list']);
            } elseif (is_array($blog['tag_list'])) {
                $tags_list = $blog['tag_list'];
            }
            $this->load->model('catalog/tag');
            foreach ($tags_list as $key => $tag) {
                if (!is_numeric($tag)) {
                    $tag_record = $this->model_catalog_tag->getTagByValue($tag);
                    if (!empty($tag_record['tag_id'])) {
                        $tags_list[$key] = $tag_record['tag_id'];
                    }
                }
            }
            $blog['tag_list'] = $tags_list;

            return $blog;
        } catch (Exception $exception) {
            return [];
        }
    }
}