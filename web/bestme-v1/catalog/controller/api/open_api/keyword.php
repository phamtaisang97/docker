<?php

class ControllerApiOpenApiKeyword extends Controller
{
    use Open_Api;

    const PERMISSION = "blog";

    public function index()
    {
        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("index")) {
            $this->responseJson($json);
            return;
        }
        /* do getting data */
        $this->load->model('online_store/keyword_sync');
        $json = $this->model_online_store_keyword_sync->getkeywords($this->request->get);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * @return bool
     */
    public function store()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->log->write('createPolicy(): resp: ' . 'Bad request. Use POST only!');

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("store")) {
            $this->responseJson($json);
            return;
        }

        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('createKeyword(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        $this->load->model('online_store/keyword_sync');

        $validate_result = $this->validateKeywordPostData();
        if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
            $error = is_array($validate_result) ? $validate_result : [];
            $this->responseJson($error);
            return;
        }

        // pass data post
        $data['keyword'] = isset($this->request->post['keyword']) ? $this->request->post['keyword'] : '';

        $this->model_online_store_keyword_sync->createKeyword($data);

        $json = [
            'code' => 200,
            'message' => 'Create keyword success !',
        ];
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
    
    public function delete()
    {
        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("index")) {
            $this->responseJson($json);
            return;
        }
        $this->load->model('online_store/keyword_sync');
        $this->model_online_store_keyword_sync->deleteAllKeyword();
        $json = [
            'code' => 200,
            'message' => 'delete all keyword success !',
        ];
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function validateKeywordPostData()
    {
        $this->load->model('online_store/keyword_sync');

        $name = isset($this->request->post['keyword']) ? $this->request->post['keyword'] : '';

        if (!$name) {
            return [
                'code' => 101,
                'message' => 'missing or invalid keyword'
            ];
        }

        if ($this->model_online_store_keyword_sync->checkKeywordExist($this->request->post['keyword']) != false) {
            return [
                'code' => 125,
                'message' => 'error keyword exist'
            ];
        }

        return [
            'code' => 200
        ];
    }
}