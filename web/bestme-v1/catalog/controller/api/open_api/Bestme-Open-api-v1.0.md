# Open integration

### Document for open api v1.0

I. Summary

- product management:
    - create product
    - edit product
    - delete product
    - get products list
    - get product detail

- order management:
    - get orders list
    - get order detail

- category management:
    - get categories list
    - get category detail
    - create category
    - edit category
    - delete category

- collection management:
    - get collections list
    - get collection detail
    - create collection
    - edit collection
    - delete collection

- manufacturer management:
    - get manufacturers list
    - get manufacturer detail

- customers management:
    - get customers list
    - create customer
    - edit customer
    - get customer detail
    - delete customer
    
- customer_group management:
    - get customer_group list
    - get customer_group detail
    - create customer_group
    - edit customer_group
    - delete customer_group
    
-  discounts management:
    - get list discount
    - get discount detail
    
- blog management:
    - get blog list
    - get blog detail
    - create blog
    - edit blog
    - delete blog

II. Detail

**\* Notice:**
- On header 
    - "Accept" : "application/json"
    - "Bestme-Api-AccessToken" : "IVJm6LqgJg7Vfb2dGwCYuTiJIcbpG3YeHI6GpR28u2CWCri2c57bxY4k6ZE1"
    
1. get products list

    - method: GET

    - url:
      /api/open_api/products/index?page=1&limit=15&searchKey=ao%20nam&searchField=name&except_product_ids=12,23_1,23_3&is_all=0

    - request:

        - where:

            - page: page number. If absent or empty value ('') => not use, then param "limit" will be not used too

            - limit: the maximum records will be returned for that request. If absent or empty value ('') => default 15

            - searchKey: the search value, escaped. e.g "ao nam" => "ao+nam"

            - searchField: the field to search, supported: "name|status"
              
            - quantity: (ONLY FOR VER 1.1), quantity search field, format is {operator_text}_{quantity_value}. eg lt_2, eq_3, gt_4
              
            - category: (ONLY FOR VER 1.1), category search field
              
            - manufacturer: (ONLY FOR VER 1.1), manufacturer search field

            - status: (ONLY FOR VER 1.1), filter getting all products activeStatus = 1|0

            - product_ids: the product ids (and may be product version ids) to be included, multiple values split
              by comma (,). e.g 12,13_1,13_3,... where:

                - 12: product id
                - 13_1, 13_3: product id 13 and it's version id 1, 3

            - except_product_ids: the product ids (and may be product version ids) to be excepted, multiple values split
              by comma (,). e.g 12,13_1,13_3,... where:

                - 12: product id
                - 13_1, 13_3: product id 13 and it's version id 1, 3

            - is_all: filter getting all products with 1=out-of-stock or 0=in-stock, default empty or 0 for in-stock
              only

    - response:

        - type: application/json

        - value: json for products list. example format:

          ```
          {
               "total": "62",
               "page": "1",
               "records": [
                   {
                       "id": "1107",
                       "name": "sp co ban",
                       "original_price": "50000.0000",
                       "sale_price": "34000.0000",
                       "image": "http://127.0.0.1:8000/images/x2/apple_cinema_30-500x500_MCTR3aQ.jpg",
                       "url": "http://x2.bestme.test/sp-co-ban",
                       "quantity": -1,
                       "sku": "",
                       "in_stock": 1,
                       "sale_on_out_of_stock": "1",
                       "version_quantity": 0,
                       "version": [],
                       "nameCategory": "",
                       "manufacturer": "",
                       "activeStatus": "1",
                       "user_name": "Nguyễn  Văn Admin",
                       "product_attributes": []
                   },
                   {
                       "id": "671",
                       "name": "Dép sandal xoắn cổ chân",
                       "original_price": "390000.0000",
                       "sale_price": "230000.0000",
                       "image": "/catalog/view/theme/default/image/fashion_shoeszone/products/98/thumb.jpg",
                       "url": "http://x2.bestme.test/dep-sandal-xoan-co-chan",
                       "quantity": 1,
                       "sku": "SKU215xzxz",
                       "in_stock": 1,
                       "sale_on_out_of_stock": "0",
                       "version_quantity": 0,
                       "version": [],
                       "nameCategory": "NHÓM SẢN PHẨM FIGHTING CORONA - PHÒNG DỊCH CORONA",
                       "manufacturer": "",
                       "activeStatus": "1",
                       "user_name": "Nguyễn  Văn Admin",
                       "product_attributes": []
                   },
                   {
                       "id": "1104",
                       "name": "Bàn phím B",
                       "original_price": "0.0000",
                       "sale_price": "0.0000",
                       "image": "http://127.0.0.1:8000/images/x2/slideshow1_ExmkVZI.png",
                       "url": "http://x2.bestme.test/ban-phim-b",
                       "quantity": 147,
                       "sku": "KB1131",
                       "in_stock": 1,
                       "sale_on_out_of_stock": "0",
                       "version_quantity": "6",
                       "version": [
                           {
                               "version": "Đen • S",
                               "price": 135000,
                               "compare_price": 150000,
                               "sku": "",
                               "barcode": "",
                               "quantity": "30",
                               "status": "1",
                               "product_version_id": "1022",
                               "image": "http://127.0.0.1:8000/images/x2/car_parts_auto.png",
                               "image_alt": "",
                               "version_for_store": "_en_S_ce9afa6814f0e963cffdc7d8c9f936f4"
                           },
                           {
                               "version": "Đen • L",
                               "price": 135000,
                               "compare_price": 150000,
                               "sku": "",
                               "barcode": "",
                               "quantity": "30",
                               "status": "1",
                               "product_version_id": "1024",
                               "image": "",
                               "image_alt": "",
                               "version_for_store": "_en_L_12cedc5afb2762fa31505a495640be21"
                           },
                           {
                               "version": "Đỏ • M",
                               "price": 135000,
                               "compare_price": 150000,
                               "sku": "",
                               "barcode": "",
                               "quantity": "28",
                               "status": "1",
                               "product_version_id": "1026",
                               "image": "http://127.0.0.1:8000/images/x2/apple_cinema_30-500x500_MCTR3aQ.jpg",
                               "image_alt": "",
                               "version_for_store": "_M_bd2e9f7fa0fc81b8c9f39b48a73ed864"
                           },
                           {
                               "version": "Đỏ • S",
                               "price": 135000,
                               "compare_price": 150000,
                               "sku": "",
                               "barcode": "",
                               "quantity": "29",
                               "status": "1",
                               "product_version_id": "1025",
                               "image": "",
                               "image_alt": "",
                               "version_for_store": "_S_f98d15273a2634ce63ffdf71a644bc79"
                           },
                           {
                               "version": "Đỏ • L",
                               "price": 135000,
                               "compare_price": 150000,
                               "sku": "",
                               "barcode": "",
                               "quantity": "30",
                               "status": "1",
                               "product_version_id": "1027",
                               "image": "",
                               "image_alt": "",
                               "version_for_store": "_L_20e9bcd5c0f382f1cfd1c354b9aad42d"
                           },
                           {
                               "version": "Đen • M",
                               "price": 135000,
                               "compare_price": 150000,
                               "sku": "",
                               "barcode": "",
                               "quantity": "30",
                               "status": "1",
                               "product_version_id": "1023",
                               "image": "http://127.0.0.1:8000/images/x2/canon_eos_5d_1-500x500.jpg",
                               "image_alt": "",
                               "version_for_store": "_en_M_e4b633957ea43ad3f63d69c9893398d2"
                           }
                       ],
                       "nameCategory": "Đồng hồ,Áo nữ,Áo thun,Áo cộc tay",
                       "manufacturer": "Zorka",
                       "activeStatus": "1",
                       "user_name": "Nguyễn  Văn Admin",
                       "product_attributes": [
                          {
                              "attribute_id": "808",
                              "name": "Màu sắc",
                              "detail": [
                                  "Đen",
                                  "Đỏ"
                              ]
                          },
                          {
                              "attribute_id": "809",
                              "name": "Kích cỡ",
                              "detail": [
                                  "S",
                                  "M",
                                  "L"
                              ]
                          }
                      ]
                   }
               ]
          }
          ```

    - where:

        - total: total products (and match the search if provided)

        - page: current page. If empty ('') => no page used, return all products in records

        - records: contains products list (with summary info). If count of records < limit: reach the end of products
          list
          
          where:

            - quantity of product in default store

            - default_store: setting in product form at Bestme admin


1. get product detail

    - method: GET

    - url: /api/open_api/products/show?id=1

    - request:

        - where:
            - id: the product id to get detail

    - response:

        - type: application/json

        - value: json for products list. example format:
            - if get one version product:

              ```
              {
                  "id": "1968",
                  "name": "Son dac chung loc phat",
                  "original_price": "1000000.0000",
                  "sale_price": "900000.0000",
                  "image": null,
                  "url": "http://x2.bestme.test/son-dac-chung-loc-phat",
                  "quantity": "4885",
                  "sku": "",
                  "weight": "100.00000000",
                  "in_stock": 1,
                  "sale_on_out_of_stock": "1",
                  "version_quantity": 0,
                  "version": [],
                  "nameCategory": "",
                  "manufacturer": "",
                  "activeStatus": "1",
                  "user_name": "Nguyễn  Văn Admin"
              }
              ```
    
            - if get multi version product:
                ```
                {
                    "id": "1970",
                    "name": "san pham 1109 pb",
                    "original_price": "0.0000",
                    "sale_price": "0.0000",
                    "image": "http://127.0.0.1:8000/images/x2/apple_cinema_30-500x500_MCTR3aQ.jpg",
                    "url": "http://x2.bestme.test/san-pham-1109-pb",
                    "quantity": null,
                    "sku": "spnpb 1109",
                    "weight": "1000.00000000",
                    "in_stock": 1,
                    "sale_on_out_of_stock": "1",
                    "version_quantity": "4",
                    "version": [
                        {
                            "version": "D • thap",
                            "price": 49000,
                            "compare_price": 50000,
                            "sku": "SPNPB1 1109",
                            "barcode": "WTIOW",
                            "quantity": "0",
                            "status": "1",
                            "product_version_id": "1824",
                            "image": "",
                            "image_alt": "",
                            "version_for_store": "D_thap_67b16f2fa71005d8f6fc3e2e10cbfe39"
                        },
                        {
                            "version": "X • thap",
                            "price": 49000,
                            "compare_price": 50000,
                            "sku": "SPNPB2 1109",
                            "barcode": "WTIOW",
                            "quantity": "0",
                            "status": "1",
                            "product_version_id": "1825",
                            "image": "",
                            "image_alt": "",
                            "version_for_store": "X_thap_329280c5a66419c9c5c6a962fb4948b1"
                        },
                        {
                            "version": "D • ca",
                            "price": 49000,
                            "compare_price": 50000,
                            "sku": "SPNPB3 1109",
                            "barcode": "WTIOW",
                            "quantity": "0",
                            "status": "1",
                            "product_version_id": "1826",
                            "image": "",
                            "image_alt": "",
                            "version_for_store": "D_ca_183d3e7a78301f397cf929dc777d5ac8"
                        },
                        {
                            "version": "X • ca",
                            "price": 49000,
                            "compare_price": 50000,
                            "sku": "SPNPB4 1109",
                            "barcode": "WTIOW",
                            "quantity": "0",
                            "status": "1",
                            "product_version_id": "1827",
                            "image": "",
                            "image_alt": "",
                            "version_for_store": "X_ca_64f8ecee9863f4c8c5667caa47624bbf"
                        }
                    ],
                    "nameCategory": "lsp a,lsp b",
                    "manufacturer": "32",
                    "activeStatus": "1",
                    "user_name": "Nguyễn  Văn Admin",
                    "product_attributes": [
                        {
                            "attribute_id": "1033",
                            "name": "màu",
                            "detail": [
                                "X",
                                "D"
                            ]
                        },
                        {
                            "attribute_id": "1034",
                            "name": "cao",
                            "detail": [
                                "thap",
                                "ca"
                            ]
                        }
                    ]
                }
                ```


1. create product

    - method: POST

    - url: /api/open_api/products/store

    - see edit product bellow: remove product_id in body data

1. edit product

    - method: POST

    - url: /api/open_api/products/update

    - body data:

        - if single product:

        ```
          {
            "name": "san pham 1 pb 1150",
            "description": "&lt;p&gt;mo ta&lt;\/p&gt;",
            "summary": "&lt;p&gt;mo ta ngan&lt;\/p&gt;",
            "product_version": "0",
            "price": "50,000",
            "promotion_price": "36,999",
            "weight": "200",
            "sku": "sp1pb 1150",
            "barcode": "",
            "stores_default_id": "0",
            "sale_on_out_of_stock": "1",
            "common_compare_price": "",
            "common_price": "",
            "common_weight": "0",
            "common_sku": "",
            "common_barcode": "",
            "stores_default_id_mul": "0",
            "common_cost_price": "0",
            "common_sale_on_out_of_stock": "1",
            "attribute_name": [""],
            "stores":
            {
                "0": ["0"],
                "1000": ["2"]
            },
            "original_inventory":
            {
                "0": ["4"],
                "1000": ["2"]
            },
            "cost_price":
            {
                "0": ["5,000"],
                "1000": ["4,000"]
            },
            "seo_title": "",
            "seo_desciption": "",
            "alias": "",
            "meta_keyword": "",
            "images": ["http:\/\/127.0.0.1:8000\/images\/x2\/apple_cinema_30-500x500_MCTR3aQ.jpg"],
            "image-alts": [""],
            "status": "1",
            "category": ["cat 224", "cat 223"],
            "manufacturer": "manuface f",
            "collection_list": ["collection 42"],
            "tag_list": ["tag 46"],
            "channel": "0",
            "created_user_id": "4"
        }
      ```

        - if multi versions product:

      ```
      {
        "name": "san pham 1109 pb",
        "description": "<p>mo ta<\/p>",
        "summary": "<p>mo ta ngan<\/p>",
        "product_version": "1",
        "price": "",
        "promotion_price": "",
        "weight": "1,000",
        "sku": "spnpb 1109",
        "barcode": "SP1pb",
        "stores_default_id": "0",
        "sale_on_out_of_stock": "1",
        "common_compare_price": "50,000",
        "common_price": "49,000",
        "common_weight": "1,000",
        "common_sku": "SPNPB 1109",
        "common_barcode": "WTIOW",
        "stores_default_id_mul": "0",
        "common_cost_price": "50,000",
        "common_sale_on_out_of_stock": "1",
        "attribute_name": [
            "m\u00e0u",
            "cao"
        ],
        "attribute_values": [
            [
                "X",
                "D"
            ],
            [
                "thap",
                "ca"
            ]
        ],
        "product_display": [
            "D \u2022 thap",
            "X \u2022 thap",
            "D \u2022 ca",
            "X \u2022 ca"
        ],
        "product_version_names": [
            "D \u2022 thap",
            "X \u2022 thap",
            "D \u2022 ca",
            "X \u2022 ca"
        ],
        "product_prices": [
            "50,000",
            "50,000",
            "50,000",
            "50,000"
        ],
        "product_promotion_prices": [
            "49,000",
            "49,000",
            "49,000",
            "49,000"
        ],
        "product_quantities": [
            "0",
            "0",
            "0",
            "0"
        ],
        "product_skus": [
            "SPNPB1 1109",
            "SPNPB2 1109",
            "SPNPB3 1109",
            "SPNPB4 1109"
        ],
        "product_barcodes": [
            "WTIOW",
            "WTIOW",
            "WTIOW",
            "WTIOW"
        ],
        "stores":
        [
            [
                "0",
                "2"
            ],
            [
                "0"
            ],
            [
                "0"
            ],
            [
                "0"
            ]
        ],
        "original_inventory":
        [
            [
                "10",
                "20"
            ],
            [
                "0"
            ],
            [
                "0"
            ],
            [
                "0"
            ]
        ],
        "cost_price":
        [
            [
                "50,000",
                "45,000"
            ],
            [
                "50,000"
            ],
            [
                "50,000"
            ],
            [
                "50,000"
            ]
        ],
        "seo_title": "",
        "seo_description": "",
        "alias": "",
        "meta_keyword": "",
        "images": [
            "http:\/\/127.0.0.1:8000\/images\/x2\/apple_cinema_30-500x500_MCTR3aQ.jpg"
        ],
        "image-alts": [
            ""
        ],
        "product_version_images": [
            "",
            "",
            "",
            ""
        ],
        "product_version_image_alts": [
            "",
            "",
            "",
            ""
        ],
        "status": "1",
        "category": [
            "lsp a",
            "lsp b"
        ],
        "manufacturer": "32",
        "collection_list": [
            "bst 1",
            "bst 2"
        ],
        "tag_list": [
            "tag 1",
            "tag n"
        ],
        "channel": "0",
        "created_user_id": "4"
      }
      ```
      
        - where (* is required):

            - product_id*: (required on editing ONLY) product id. Let empty for creating product, or existing product id
              for editing
            - name*: product name, max length 255
            - description*: product description, max length 30.000, should be encoded html. E.g "&lt;p&gt;test social 1
              - desc&lt;\/p&gt;" known as "test-social 1 - desc"
            - summary*: product description, max length 500, should be encoded html. E.g "&lt;p&gt;test social 1 -
              desc&lt;\/p&gt;" known as "test-social 1 - desc"
            - product_version*: =0 if single product, =1 if multi versions
            - price*: original price (vnd), max 99,999,999,999
            - promotion_price*: sale price (vnd), max 99,999,999,999
            - weight*: weight (g), max 9,999,999
            - sku: SKU
            - barcode: barcode
            - stores_default_id*: default store, used for decrease product quantity when bought from website
            - sale_on_out_of_stock*: allow sale on out of stock or not, =0: not allow, =1: allow

            - attribute_name*: (MULTI VERSIONS ONLY) array of attribute names. each name max length 30
            - attribute_values*: (MULTI VERSIONS ONLY) array of attribute names. each value max length 30, total max 100
              values
            - product_version_names*: (MULTI VERSIONS ONLY) array of version names. create from attribute_values
            - product_display*: (MULTI VERSIONS ONLY) array of versions have display attribute is true.
            - product_version_ids*: (MULTI VERSIONS ONLY) array of version id, keep value in same order with attribute
              name and value
            - product_prices*: (MULTI VERSIONS ONLY) array of original price (vnd), max 99,999,999,999, keep value in
              same order with attribute name and value
            - product_promotion_prices*: (MULTI VERSIONS ONLY) array of sale price (vnd), max 99,999,999,999, keep value
              in same order with attribute name and value
            - product_quantities*: (MULTI VERSIONS ONLY) array of quantity, max length 9,999,999, keep value in same
              order with attribute name and value
            - product_skus*: (MULTI VERSIONS ONLY) array of sku, keep value in same order with attribute name and value
            - product_barcodes*: (MULTI VERSIONS ONLY) array of barcode, keep value in same order with attribute name
              and value
              
            - common_compare_price*: product original price
            - common_price*: product promotion price
            - common_weight*: product weight
            - common_sku*: product sku
            - common_cost_price*: product cost price
              
            - stores*: array of store, keep value in same order with attribute name
              and value
            - original_inventory*: array of stores inventory number, max 9,999,999
            - cost_price*: array of store cost price, max 9,999,999
            - seo_title: seo title
            - seo_description: seo description
            - alias: seo alias
            - meta_keyword: seo meta keywords, string separated by comma
            - images*: array of image url, first is main image, each url max length 255
            - image-alts*: array of image alt, associated to images above, each alt max length 255
            - product_version_images*: array of product version image url, each url max length 255
            - product_version_image_alts*: array of product version image alt, associated to product version images above, each alt max length 255
            - status*: product status, =0: disabled, =1: enabled
            - category*: array of category names, CASE INSENSITIVE. For each name, auto create category if name not
              existing
            - manufacturer*: manufacturer names, CASE INSENSITIVE. Auto create manufacturer if name not existing
            - collection_list*: array of collection titles, CASE INSENSITIVE. For each title, auto create collection if title not
              existing
            - tag_list*: array of tag, CASE INSENSITIVE. For each tag, auto create tag if tag not
              existing
            - channel*: channel sale product. =0: website, =1: POS, =2: both website + POS
            - created_user_id*: id of current logged in user

    - response:

        - type: application/json

        - value: json for create/edit order result. example format:

          ```
              {
                  "code": 0,
                  "message": "product is created successfully",
                  "product_id": '13265'
              }
          ```

          if edit:

          ```
              {
                  "code": 0,
                  "message": "product is edited successfully",
              }
          ```

        - where:

            - response "code":

                - 200: product is created successfully

                - 1: credential (api key) is verified
    
                - 400: bad request

                - 500: internal server error
        - Notice:

            - ... collection, tag ... will be added next version

1. delete product

    - method: DELETE

    - url: /api/open_api/products/delete

    - request:

        - header: "content-type": "application/json"

    - body data:

      ```body
      {
          "data": {
              "product_id": 1246
          }
      }
      ```

        - where:

            - required: all keys in above sample data

    - response:

        - type: application/json

        - value: json for delete product result. example format:

          ```
              {
                  "code": 0,
                  "message": "product is deleted successfully",
              }
          ```

        - where:

            - response "code":

                - 0: product is deleted successfully

                - 506: product (with product_id) does not existed

                - 201: unknown error

                - 400: token invalid
      
1. get orders list

    - method: GET

    - url: /api/open_api/orders/index?page=1&limit=15&order_ids=&order_status=6&order_payment=0&order_total_type=lt_1000000%2C+gt_300000&order_code=&order_product=&order_customer_ids=60

    - request:

        - where:

            - page: page number. If absent or empty value ('') => not use, then param "limit" will be not used too

            - limit: the maximum records will be returned for that request. If absent or empty value ('') => default 15

            - order_ids: the orders ids to be returned, multiple values split by comma (,). e.g 213,216,...
            
            - order_status: the orders status to be returned, multiple values split by comma (,). e.g 6,7,8,...
            
            - order_payment: the orders payment_status to be returned, multiple values split by comma (,). e.g 0,1,2
                - 0: Unpaid
                
                - 1: Already paid
                
                - 2: Refunded
            
            - order_total_type: filter order total
            
            - order_code: enter code
            
            - order_product: enter product name
            
            - order_customer_ids: the customer_ids to be returned, multiple values split by comma (,). e.g 1,2,3,...
            
    - response:

        - type: application/json

        - value: json. empty if not found. order by "order_id" DESC. example format:

          ```
          {
              "total": 120,
              "page": 3,
              "total_revenue": 820000,
              "records": [
                  {
                      "customer": {
                          "name": "Nguyen Thuy Linh",
                          "phone_number": "0987654321",
                          "email": "nguyenthuylinh@gmail.com",
                          "delivery_addr": "So 1 Ngo 2 Duy Tan",
                          "full_delivery_addr": "So 1 Ngo 2 Duy Tan, Phuong Dich Vong, Quan Cau Giay, Ha Noi",
                          "ward": "30280",
                          "district": "883",
                          "city": "89",
                          "note": "Goi hang can than va du so luong nhe shop. Thanks!",
                          "customer_id": "30",
                          "subscriber_id": "dftcfb57fgvi24545",
                          "customer_group_id": "1"
                      },
                      "status": 6,
                      "customer_ref_id": "1234",
                      "order_id": 213,
                      "order_code": "#00000000015",
                      "tags": ["tag1,tag2"],
                      "total": 280000,
                      "date_added": "2020-02-29 15:22:09",
                      "date_modified": "2020-03-05 08:39:04",
                      "products": [
                          {
                              "id": 1,
                              "name": "Ao phong nam H2T",
                              "description": "Ao phong nam H2T chat lieu dep",
                              "short_description": "Ao phong nam H2T chat lieu dep",
                              "original_price": 200000,
                              "sale_price": 195000,
                              "image": "https://novaon-cloud.vn/img/1/12345678-1.png",
                              "url": "https://myshop.com/ao-phong-nam-h2t",
                              "quantity": 2,
                              "inventory": 10,
                              "discount": "2000.0000"
                          },
                          {
                              "id": 2,
                              "name": "Ao phong nam H2T",
                              "description": "Ao phong nam H2T chat lieu dep",
                              "short_description": "Ao phong nam H2T chat lieu dep",
                              "original_price": 200000,
                              "sale_price": 195000,
                              "image": "https://novaon-cloud.vn/img/1/12345678-1.png",
                              "url": "https://myshop.com/ao-phong-nam-h2t",
                              "quantity": 3,
                              "inventory": 30,
                              "discount": "2000.0000"
                          },
                          ...
                      ],
                      "payment_trans": "-2",
                      "payment_trans_custom_name": "Shop tự vận chuyển",
                      "payment_trans_custom_fee": 25000,
                      "payment_status": 1,
                      "delivery_fee": 25000,
                      "changable_order_statuses": [
                          {
                              "order_status_id": 9,
                              "order_status_description": "Hoàn thành"
                          },
                          {
                              "order_status_id": 10,
                              "order_status_description": "Đã huỷ"
                          }
                      ],
                      "from_district_id_ghn": "1312412",
                      "payment_method": "198535319739A",
                      "user_id": "1",
                      "order_source": "chatbot",
                      "transports": {
                          "customer_id": "60",
                          "firstname": "Linh",
                          "lastname": "Nguyen Thuy",
                          "fullname": "Nguyen Thuy Linh 333",
                          "email": "nguyenthuylinh@gmail.com",
                          "telephone": "0987654322",
                          "transport_order_code": "GAUNRHGX",
                          "transport_name": "Ghn",
                          "transport_service_name": "Đi bộ",
                          "transport_display_name": "GHN",
                          "transport_collection_amount": "485000.0000",
                          "transport_order_active": true,
                          "transport_order_detail": {
                              "PickWarehouseName": "Phố Duy Tân, Dịch Vọng Hậu, Cầu Giấy, Hà Nội, Việt Nam",
                              "OrderCode": "GAUNRHGX",
                              "CurrentStatus": "cancel",
                              "CurrentStatusLanguage": "Đơn hàng bị hủy",
                              "Cancelable": 0,
                              "CollectionAmount": "485000.0000"
                          }
                      },
                      "text_pick_address": "Số 6, Ngõ 82 dịch Vọng Hậu",
                      "campaign_id": "8s41vdft78wert1",
                      "channel_id": "1909543419365215"
                  },
                  ...
              ]
          }
          ```

            - where:

                - total: total orders (and match the search if provided)

                - page: current page. If empty ('') => no page used, return all orders in records

                - total_revenue: total revenue of orders due to current filter conditions (regardless pagination)

                - records: contains orders list (with summary info). If count of records < limit: reach the end of
                  orders list

                - status: 6=draft, 7=processing, 8=delivering, 9=complete, 10=cancel

                - payment_trans: delivery code
                    - -2: custom fee, then 2 need additional 2 keys: payment_trans_custom_name (string),
                      payment_trans_custom_fee (int)
                    - -3: free (fee = 0)
                    - not -2 and -3: delivery id (got from delivery list api). e.g: -1, 1, 2, 3, ...

                - payment_status:
                    - 1: paid
                    - 0: not yet paid

                - delivery_fee: delivery fee due to payment_trans above
                
                - changable_order_statuses: changable order status

1. get order detail

    - method: GET

    - url: /api/open_api/orders?id=123

    - response:

        - type: application/json

        - value: json. empty if not found. example format:

          ```
          {
              "customer": {
                 "name": "Nguyen Thuy Linh",
                 "phone_number": "0987654321",
                 "email": "nguyenthuylinh@gmail.com",
                 "delivery_addr": "So 1 Ngo 2 Duy Tan",
                 "full_delivery_addr": "So 1 Ngo 2 Duy Tan, Phuong Dich Vong, Quan Cau Giay, Ha Noi",
                 "ward": "30280",
                 "district": "883",
                 "city": "89",
                 "note": "Goi hang can than va du so luong nhe shop. Thanks!",
                 "customer_id": "30",
                 "subscriber_id": "dftcfb57fgvi24545",
                 "customer_group_id": "1"
              },
              "status": 6,
              "customer_ref_id": "",
              "order_id": 213,
              "order_code": "#00000000213",
              "tags": ["tag1,tag2"],
              "total": 280000,
              "date_added": "2020-02-29 15:22:09",
              "date_modified": "2020-03-05 08:39:04",
              "products": [
                  {
                      "id": 1,
                      "name": "Ao phong nam H2T",
                      "description": "Ao phong nam H2T chat lieu dep",
                      "short_description": "Ao phong nam H2T chat lieu dep",
                      "original_price": 200000,
                      "sale_price": 195000,
                      "image": "https://novaon-cloud.vn/img/1/12345678-1.png",
                      "url": "https://myshop.com/ao-phong-nam-h2t",
                      "quantity": 2,
                      "inventory": 10,
                      "discount": "2000.0000"
                  },
                  {
                      "id": 2,
                      "name": "Ao phong nam H2T",
                      "description": "Ao phong nam H2T chat lieu dep",
                      "short_description": "Ao phong nam H2T chat lieu dep",
                      "original_price": 200000,
                      "sale_price": 195000,
                      "image": "https://novaon-cloud.vn/img/1/12345678-1.png",
                      "url": "https://myshop.com/ao-phong-nam-h2t",
                      "quantity": 3,
                      "quantity": 30,
                      "discount": "2000.0000"
                  },
                  ...
              ],
              "payment_trans": "-3",
              "payment_trans_custom_name": "Shop tự vận chuyển",
              "payment_trans_custom_fee": 25000,
              "payment_status": 1,
              "delivery_fee": 25000,
              "changable_order_statuses": [
                  {
                      "order_status_id": 9,
                      "order_status_description": "Hoàn thành"
                  },
                  {
                      "order_status_id": 10,
                      "order_status_description": "Đã huỷ"
                  }
              ],
              "from_district_id_ghn": "1312412",
              "payment_method": "198535319739A",
              "user_id": "1",
              "order_source": "chatbot",
              "transports": {
                  "customer_id": "60",
                  "firstname": "Linh",
                  "lastname": "Nguyen Thuy",
                  "fullname": "Nguyen Thuy Linh 333",
                  "email": "nguyenthuylinh@gmail.com",
                  "telephone": "0987654322",
                  "transport_order_code": "GAUNRHGX",
                  "transport_name": "Ghn",
                  "transport_service_name": "Đi bộ",
                  "transport_display_name": "GHN",
                  "transport_collection_amount": "485000.0000",
                  "transport_order_active": true,
                  "transport_order_detail": {
                      "PickWarehouseName": "Phố Duy Tân, Dịch Vọng Hậu, Cầu Giấy, Hà Nội, Việt Nam",
                      "OrderCode": "GAUNRHGX",
                      "CurrentStatus": "cancel",
                      "CurrentStatusLanguage": "Đơn hàng bị hủy",
                      "Cancelable": 0,
                      "CollectionAmount": "485000.0000"
                  }
              },
              "text_pick_address": "Số 6, Ngõ 82 dịch Vọng Hậu",
              "campaign_id": "8s41vdft78wert1",
              "channel_id": "1909543419365215"
          }
          ```

19. get categories list

    - method: GET
    
    - url: /api/open_api/categories/index?page=1&limit=3&filter_name=Phụ kiện
    
    - request:
    
        - where:
    
            - page: page number. If absent or empty value ('') => not use, then param "limit" will be not used too
    
            - limit: the maximum records will be returned for that request. If absent or empty value ('') => default 15
    
            - filter_name: enter category name
    
    - response:
    
        - type: application/json
    
        - value: json. empty if not found. order by "category_id" DESC. example format:
    
          ```
          {
              "total": "190",
              "page": "1",
              "records": [
                  {
                      "id": "270",
                      "parent_id": "0",
                      "text": "lsp a",
                      "name": "lsp a",
                      "description": "lsp a",
                      "hasChildren": false,
                      "sub_ids": [],
                      "children": [],
                      "image": null,
                      "filter_name": "YES"
                  },
                  {
                      "id": "271",
                      "parent_id": "0",
                      "text": "lsp b",
                      "name": "lsp b",
                      "description": "lsp b",
                      "hasChildren": false,
                      "sub_ids": [],
                      "children": [],
                      "image": null,
                      "filter_name": "YES"
                  },
                  {
                      "id": "268",
                      "parent_id": "0",
                      "text": "cat 224",
                      "name": "cat 224",
                      "description": "cat 224",
                      "hasChildren": false,
                      "sub_ids": [],
                      "children": [],
                      "image": null,
                      "filter_name": "YES"
                  },
                  {
                      "id": "269",
                      "parent_id": "0",
                      "text": "cat 223",
                      "name": "cat 223",
                      "description": "cat 223",
                      "hasChildren": false,
                      "sub_ids": [],
                      "children": [],
                      "image": null,
                      "filter_name": "YES"
                  },
                  {
                      "id": "267",
                      "parent_id": "0",
                      "text": "Đồng hồ thông minh",
                      "name": "Đồng hồ thông minh",
                      "description": "Đồng hồ thông minh",
                      "hasChildren": false,
                      "sub_ids": [],
                      "children": [],
                      "image": null,
                      "filter_name": "YES"
                  }
              ]
          }
          ```
    
            - where:
    
                - total: total categories (and match the search if provided)
    
                - page: current page. If empty ('') => no page used, return all categories in records
    
                - records: contains categories list (with summary info). If count of records < limit: reach the end of
                  categories list
                  
1. get category detail

    - method: GET
        
        - url: /api/open_api/categories/show?category_id=103
        
        - request:
        
            - where:
        
                - category_id: enter category id
        
        - response:
        
            - type: application/json
        
            - value: json. empty if not found. example format:
        
              ```
                {
                  "id": "103",
                  "parent_id": "0",
                  "text": "Gia Vị Và Phụ Liệu",
                  "name": "Gia Vị Và Phụ Liệu",
                  "description": "Gia Vị Và Phụ Liệu",
                  "hasChildren": true,
                  "image": ""
                  "children": [
                      {
                          "id": "106",
                          "parent_id": "103",
                          "text": "Gia Vị Khô",
                          "name": "Gia Vị Khô",
                          "description": "Gia Vị Khô",
                          "hasChildren": false,
                          "children": [],
                          "image": "",
                          "filter_name": "NO"
                      },
                      {
                          "id": "107",
                          "parent_id": "103",
                          "text": "Giấm",
                          "name": "Giấm",
                          "description": "Giấm",
                          "hasChildren": false,
                          "children": [],
                          "image": "",
                          "filter_name": "NO"
                      },
                      {
                          "id": "109",
                          "parent_id": "103",
                          "text": "Nước Tương",
                          "name": "Nước Tương",
                          "description": "Nước Tương",
                          "hasChildren": false,
                          "children": [],
                          "image": "",
                          "filter_name": "NO"
                      },
                      {
                          "id": "105",
                          "parent_id": "103",
                          "text": "Dầu Thực Vật",
                          "name": "Dầu Thực Vật",
                          "description": "Dầu Thực Vật",
                          "hasChildren": false,
                          "children": [],
                          "image": "",
                          "filter_name": "NO"
                      },
                      {
                          "id": "104",
                          "parent_id": "103",
                          "text": "Các Loại Sốt",
                          "name": "Các Loại Sốt",
                          "description": "Các Loại Sốt",
                          "hasChildren": false,
                          "children": [],
                          "image": "",
                          "filter_name": "NO"
                      }
                  ]
                }
              ```

1. create category
    - method: POST
        
    - url : /api/open_api/categories/store
        
    - see edit category bellow: remove category_id in body data
    
1. edit category
    
    - method: POST
    
    - url : /api/open_api/categories/update
    
    - body data: 
        ```
        {
            "category_id": 266,
            "status": 1,
            "parent_id": 0,
            "name": "sangpt",
            "category_description": "Mo ta quan thun",
            "image": "1.jpg",
            "sub_categories": [
                "oke 2",
                "viet Nam",
                "Laptop",
                "sangpro"
            ]
        }
        ```
    - response: 
        - if create
         ```
         {
             "code": 200,
             "message": "Create category success !"
         }
         ```
        - if edit 
        ```
        {
            "code": 200,
            "message": "Edit category success !"
        }
        ```
        - or more error

1. delete category

    - method: DELETE

    - url : /api/open_api/categories/delete

    - body data:
       ```
       {
           "category_id": 266,
       }
       ```

    - response:
      ```
      {
          "code": 200,
          "message": "Delete category success !"
      }
      ```
    - or more error

21. get customers list

    - method: GET

    - url: http://x2.bestme.test/api/open_api/customers/index?page=1&limit=100&filter_search=&phone=&phone_operator=&order=&order_operator=&order_value=&order_latest=&order_latest_operator=&order_latest_value=&amount=&amount_operator=&amount_value=&source=&source_value=

    - request:

        - where:

            - page: page number. If absent or empty value ('') => not use, then param "limit" will be not used too

            - limit: the maximum records will be returned for that request. If absent or empty value ('') => default 15

            - filter_search: the search value, escaped. e.g "pham tai sang" => "pham+tai+sang"
            
            - filter phone eg: [&phone=1&phone_operator=NAT] => Filter a list of unauthenticated customers
                
                - phone: not be empty 
                
                - phone_operator: "AT" or "NAT" ( AT is authenticated, NAT is not authenticated ) 
                
            - filter order eg: [&order=1&order_operator=LT&order_value=10] => Filter the list of customers with orders less than 10
                
                - order: not be empty
                
                - order_operator: "LT", "GT", "EQ" (LT is get <, GT is get >, BG equals)
                
                - order_value: number
                
            - filter order latest eg: [&order_latest=1&order_latest_operator=IS&order_latest_value=2020-11-26+10%3A29%3A07] => Filter the list of customers with latest order is 2020-11-26 10:29:07
            
                - order_latest: not be empty
                
                - order_latest_operator: "AT", "BF", "EQ" (AT is after, BF is before, EQ)
                
                - order_latest_value: date value 
                
            - filter payment amount eg: [&amount=1&amount_operator=LT&amount_value=122000000] => Filter a list of customers with paid amounts less than 122.000.000đ
                
                - amount: not be empty
                
                - amount_operator: "LT", "GT", "EQ" (LT is get <, GT is get >, BG equals)
                
                - amount_value: value payment amount
                
            - filter customer source eg: [&source=1&source_value=2] => Filter a list of customers with source is Facebook
            
                - source: not be empty
                
                - source_value: "1" = NovaonX Social, "2" = Facebook, "3" = Bestme
                
            - filter subscriber id eg: [&subscriber_id=93ohir] => Filter a list of customers with subscriber_id is '93ohir'

    - response:

        - type: application/json

        - value: json. empty if not found. order by "customer_id DESC". example format:

          ```
          {
              "total": 18,
              "page": 3,
              "records": [
                 {
                     "customer_id": "11",
                     "customer_name": "Nguyen Thuy Linh",
                     "telephone": "0987654036",
                     "customer_source": "NovaonX Social",
                     "is_authenticated": "Đã xác thực",
                     "count_order": "0",
                     "order_total": "0",
                     "order_lastest": null,
                     "subscriber_id": null,
                     "addresses": [
                        {
                            "customer_id": "24",
                            "firstname": "A",
                            "lastname": "nguyen Van",
                            "address": "So 1 Ngo 2 Duy Tan 364",
                            "city": "89",
                            "district": "886",
                            "wards": "30373",
                            "phone": "09446324536",
                            "default_address": true
                        },
                        {
                            "customer_id": "24",
                            "firstname": "sangpttest",
                            "lastname": "sangpttest",
                            "address": "HN",
                            "city": "89",
                            "district": "884",
                            "wards": "30316",
                            "phone": "0655996323",
                            "default_address": false
                        },
                        {
                            "customer_id": "24",
                            "firstname": "sangpttest22222",
                            "lastname": "sangpttest22222",
                            "address": "HN ghahahaha",
                            "city": "24",
                            "district": "218",
                            "wards": "07459",
                            "phone": "0655996323",
                            "default_address": false
                        }
                     ]
                 },
                 {
                     "customer_id": "4",
                     "customer_name": "pham tai sang",
                     "telephone": "09312645443",
                     "customer_source": "NovaonX Social",
                     "is_authenticated": "Đã xác thực",
                     "count_order": "3",
                     "order_total": "112,130,900",
                     "order_lastest": "2020-11-19 11:27:55",
                     "subscriber_id": null,
                     "addresses": [
                         {
                            "customer_id": "4",
                            "firstname": "sangpttest22222",
                            "lastname": "sangpttest22222",
                            "address": "HN ghahahaha",
                            "city": "24",
                            "district": "218",
                            "wards": "07459",
                            "phone": "0655996323",
                            "default_address": true
                          }
                     ]
                 },
                  ...
              ]
          }
          ```

            - where:

                - is_authenticated : "1" = phone number authenticated , "0" = is not authenticated
                
                - customer_source : "1" = NovaonX Social, "2" = Facebook, "3" = Bestme	
                
                - total: total categories (and match the search if provided)

                - page: current page. If empty ('') => no page used, return all categories in records

                - records: contains customers list (with summary info). If count of records < limit: reach the end of
                  customers list

22. create customer

     - method: POST
    
     - url: /api/open_api/customers/update
     
     - see edit customer bellow: remove customer_id in body data customer
     
1. edit customer

    - method: POST

    - url: /api/open_api/customers/update

    - body data:

      ```
      {
          "data": {
                      "customer": {
                          "customer_id": "2",
                          "name": "Nguyen Thuy Linh",
                          "phone_number": "0987654036",
                          "customer_source": "1",
                          "is_authenticated": 1,
                          "email": "nguyenthuylinh363@gmail.com",
                          "delivery_addr": "So 1 Ngo 2 Duy Tan 364",
                          "ward": "30373",
                          "district": "886",
                          "city": "89",
                          "note": "Goi hang can than va du so luong nhe shop. Thanks! 364",
                          "subscriber_id": "134xyz"
                      }
                  }
      }
      ```

        - where (* is required):

            - customer_id* : (required on editing ONLY) customer id. Let empty for creating customer
            - name* : not be empty
            - phone_number* : unique
            - email* : unique
            - is_authenticated : default 1 , "1" = authenticated , "0" = not authenticated
            - customer_source : default 1 , "1" = NovaonX Social, "2" = Facebook, "3" = Bestme
            - subscriber_id : empty

    - response:

        - type: application/json

        - value: json for create/edit order result. example format:

          ```
              {
                   "code": 200,
                   "message": "Success"
              }
          ```

        - where:

            - response "code":

                - 200: customer is created successfully

                - 101: missing or invalid name
                - 125: error phone exist
                - 125: error email exist

                - 201: unknown error

                - 400: token invalid
                
1.  get customer detail
    
    - method: GET
    
    - url: /api/open_api/customers/show?customer_id=60
    
    - param: 
        - customer_id: enter customer_id
    
    - response: 
        ```
        {
            "customers": [
                {
                    "customer_id": "60",
                    "customer_name": "LinhNguyen Thuy",
                    "telephone": "0987654322",
                    "customer_source": "",
                    "is_authenticated": "Chưa xác thực",
                    "order_total": "",
                    "addresses": [
                        {
                            "customer_id": "60",
                            "firstname": "Linh",
                            "lastname": "Nguyen Thuy",
                            "address": "So 1 Ngo 2 Duy Tan",
                            "city": "89",
                            "district": "883",
                            "wards": "30280",
                            "delivery_addr": "Phường Mỹ Bình Thành phố Long Xuyên Tỉnh An Giang",
                            "phone": "0987654322",
                            "default_address": true
                        }
                    ]
                }
            ]
        }
        ```

1. delete customer
    
    - method: DELETE
    
    - url: /api/open_api/customers/delete
    
    - body data:
        ```
        {
            "customer_id": 62
        }
        ```
    - response:
        ```
        {
            "code": 200,
            "message": "Delete customer success !"
        }
        ```

1. get collections list

    - method: GET
    
    - url : /api/open_api/collections/index?page=1&name=Điều hòa
    
    - params :
        - page : enter number page
        - name : enter name collection
    
    - response :
        ```
        {
            "total": "6",
            "page": "1",
            "records": [
                {
                    "collection_id": "48",
                    "title": "Điều hòa Midea",
                    "sort_order": "0",
                    "status": "1",
                    "product_type_sort": null,
                    "type": "0",
                    "user_create_id": "1",
                    "image": "",
                    "description": "",
                    "meta_title": "",
                    "meta_description": "",
                    "alias": ""
                },
                {
                    "collection_id": "51",
                    "title": "Điều hòa Daikin",
                    "sort_order": "0",
                    "status": "1",
                    "product_type_sort": null,
                    "type": "0",
                    "user_create_id": "1",
                    "image": "",
                    "description": "",
                    "meta_title": "",
                    "meta_description": "",
                    "alias": ""
                },
                {
                    "collection_id": "52",
                    "title": "Điều hòa Gree",
                    "sort_order": "0",
                    "status": "1",
                    "product_type_sort": null,
                    "type": "0",
                    "user_create_id": "1",
                    "image": "",
                    "description": "",
                    "meta_title": "",
                    "meta_description": "",
                    "alias": ""
                },
                {
                    "collection_id": "57",
                    "title": "Điều hòa Panasonic",
                    "sort_order": "0",
                    "status": "1",
                    "product_type_sort": null,
                    "type": "0",
                    "user_create_id": "1",
                    "image": "",
                    "description": "",
                    "meta_title": "",
                    "meta_description": "",
                    "alias": ""
                },
                {
                    "collection_id": "58",
                    "title": "Điều hòa Funiki",
                    "sort_order": "0",
                    "status": "1",
                    "product_type_sort": null,
                    "type": "0",
                    "user_create_id": "1",
                    "image": "",
                    "description": "",
                    "meta_title": "",
                    "meta_description": "",
                    "alias": ""
                },
                {
                    "collection_id": "60",
                    "title": "Điều hòa LG",
                    "sort_order": "0",
                    "status": "1",
                    "product_type_sort": null,
                    "type": "0",
                    "user_create_id": "1",
                    "image": "",
                    "description": "",
                    "meta_title": "",
                    "meta_description": "",
                    "alias": ""
                }
            ]
        }
        ```
1. get collection detail
    
    - method : GET
    
    - url: /api/open_api/collections/show?collection_id=6
    
    - params:
        - collection_id: not empty, enter collection_id
        
    - response:
        ```
        {
            "collection_id": "6",
            "title": "Sản phẩm mới",
            "sort_order": "0",
            "status": "1",
            "product_type_sort": "",
            "type": "0",
            "user_create_id": "1",
            "image": "https://cdn.bestme.asia/images/phongtranhhoatay/tranh-phong-khach0.jpg",
            "description": "&lt;p&gt;nhũng sản phẩm mới nhất c&amp;oacute; mặt tại cửa h&amp;agrave;ng&lt;/p&gt;",
            "meta_title": "",
            "meta_description": "",
            "alias": ""
        }
        ```
1. create collection
    
    - method: POST
    
    - url: /api/open_api/collections/store
    
    - see edit collection bellow: remove collection_id in body data

1. edit collection 
    
    - method: POST
    
    - url: /api/open_api/collections/update
    
    - body data:
        ```
        {
            "data" : {
                "menu" : [],
                "collection_id": 66,
                "title" : "CẬU KHÁ BẢNH ĐI THẬT RỒI, ÔNG GIÁO Ạ hihi",
                "description" : "",
                "status" : "1",
                "image" : "",
                "meta_title" : "",
                "meta_keyword" : "",
                "alias" : "",
                "type_select" : "1",
                "collection_list" : ["6","5","4"],
                "cat_list" : ""
            }
        }
        ```
        
    - where: 
        - type_select: "0" or "1"
            - 0: is PRODUCTS INTO THE COLLECTION
            - 1: is COLLECTION INTO THE COLLECTION
            
        - collection_list: collection_list depends on type_select
            - type_select:
                - 0: array product_id
                - 1: array collection_id
                
        - cat_list: Sort list
                - "" : is handmade
                - "az" : By name: A-Z
                - "za" : By name: Z-A
                - "hightolow" : By price: High to low
                - "lowtohigh" : By price: Low to high
                - "newtoold" : By creation date: New to old words
                - "oldtonew" : By creation date: Old to new words
                
    - response:
        - if create
        ```
        {
            "code": 0,
            "message": "COLLECTION_CREATED",
            "collection_id": 68
        }
        ```
        - if update
        ```
        {
            "code": 0,
            "message": "COLLECTION_UPDATED"
        }
        ```
        - and more error code
        
1. delete collection
    
    - method: DELETE
    
    - url : /api/open_api/collections/delete
    
    - body data: 
        ```
        {
            "data": {
                "collection_id": 66
            }
        }
        ```
    - response:
        ```
        {
            "code": 0,
            "message": "Collection deleted successfully"
        }
        ```
        - and more json error

1. get list discount
    
    - method: GET
    
    - url : /api/open_api/discounts/index?page=1&name=&discount_type_id=
    
    - params :
        - page : enter page
        - name: enter name or code
        - discount_type_id: separated by commas, e.g 1,2,3
            - 1 : Discount by total order value
            - 2 : Discount by each product
            - 3 : Discount by product type
            - 4 : Discount by supplier
            
    - response
        ```
        {
            "total": "3",
            "page": "1",
            "records": [
                {
                    "discount_id": "1",
                    "name": "Km đơn hàng",
                    "code": "KM000001",
                    "status": "Đang hoạt động",
                    "usage_limit": "0",
                    "times_used": "9",
                    "start_at": "2021-04-07 09:57:00",
                    "end_at": "2021-06-30 16:23:00"
                },
                {
                    "discount_id": "3",
                    "name": "Km cho san pham",
                    "code": "KM000003",
                    "status": "Hết hạn",
                    "usage_limit": "0",
                    "times_used": "0",
                    "start_at": "2021-04-22 13:41:00",
                    "end_at": "2021-04-22 13:41:00"
                },
                {
                    "discount_id": "2",
                    "name": "Km cho sản phẩm",
                    "code": "KM000002",
                    "status": "Hết hạn",
                    "usage_limit": "0",
                    "times_used": "0",
                    "start_at": "2021-04-12 08:56:00",
                    "end_at": "2021-04-13 09:06:00"
                }
            ]
        }
        ```
1. get discount detail
    
    - method: GET
    
    - url: /api/open_api/discounts/show?discount_id=1
    
    - param:
        - discount_id: enter id discount
     
    - response:
        ```
        {
            "discount_id": "1",
            "name": "Km đơn hàng",
            "code": "KM000001",
            "status": "Đang hoạt động",
            "usage_limit": "0",
            "times_used": "9",
            "start_at": "2021-04-07 09:57:00",
            "end_at": "2021-06-30 16:23:00"
        }
        ```
        - and more error json
        
1. get customer_group list
    
    - method: GET
    
    - url: /api/open_api/customer_group/index?page=1&filter_name=
    
    - param:
        - page: enter page
        - filter_name: name group customer
    
    - response: 
        ```
        {
            "total": "4",
            "page": 1,
            "records": [
                {
                    "customer_group_id": "5",
                    "approval": "1",
                    "sort_order": "1",
                    "customer_group_code": "NKH000005",
                    "user_create_id": null,
                    "language_id": "2",
                    "name": "bad boy",
                    "description": ""
                },
                {
                    "customer_group_id": "4",
                    "approval": "1",
                    "sort_order": "1",
                    "customer_group_code": "NKH000004",
                    "user_create_id": null,
                    "language_id": "2",
                    "name": "good",
                    "description": ""
                },
                {
                    "customer_group_id": "3",
                    "approval": "0",
                    "sort_order": "0",
                    "customer_group_code": "NKH000003",
                    "user_create_id": null,
                    "language_id": "2",
                    "name": "xấu trai",
                    "description": ""
                },
                {
                    "customer_group_id": "2",
                    "approval": "0",
                    "sort_order": "0",
                    "customer_group_code": "NKH000002",
                    "user_create_id": null,
                    "language_id": "2",
                    "name": "đẹp trai",
                    "description": ""
                }
            ]
        }
        ```
        
1. get customer_group detail
    
    - method: GET
        
    - url: /api/open_api/customer_group/show?customer_group_id=2
    
    - param:
        - customer_group_id: enter customer_group_id
    
    - response:
        ```
        {
            "customer_group_id": "2",
            "approval": "0",
            "sort_order": "0",
            "customer_group_code": "NKH000002",
            "user_create_id": null,
            "language_id": "2",
            "name": "đẹp trai",
            "description": ""
        }
        ```
1. create customer_group

    - method: POST
    
    - url: /api/open_api/customer_group/store
    
    - see edit customer_group bellow: remove customer_group_id in body data
      
1. edit customer_group
    
    - method: POST
    
    - url: /api/open_api/customer_group/update
    
    - body data:
        ```
        {
            "customer_group_id": 7,
            "name_group": "sangpt",
            "description": "description test !!"
        }
        ```
        
    - response:
        - if create
        ```
        {
            "code": 200,
            "message": "Create customer group success !"
        }
        ```
        
        - if edit
        ```
        {
            "code": 200,
            "message": "Edit customer group success !"
        }
        ```
        
        - and more json error
        
1. delete customer_group
    
    - method: DELETE
    
    - url: /api/open_api/customer_group/delete?customer_group_id=1
    
    - body data
        ```
        {
            "customer_group_id": "4"
        }
        ```
    - response:
        ```
        {
            "code": 200,
            "message": "Delete customer success !"
        }
        ```
                
1. get blog list
    
    - method: GET
    
    - url: /api/open_api/blog/index?page=1&filter_name=&filter_status=
    
    - param:
        - page: enter page
        - filter_name: name blog
        - filter_status: 0 is hidden, 1 is display
    
    - response:
        ```
        {
            "total": "20",
            "page": 1,
            "records": [
                {
                    "blog_id": "5",
                    "author": "1",
                    "status": "1",
                    "date_publish": "2021-01-23 10:50:13",
                    "date_added": "2021-01-23 10:50:13",
                    "date_modified": "2021-01-23 10:54:16",
                    "title": "Các món khai vị nổi tiếng  trong ẩm thực Châu Âu",
                    "content": "&lt;p&gt;Khai vị là những món ăn được sử dụng trước hoặc ngoài những món ăn chính. Chúng rất đa dạng, từ các loại bánh cho tới những món thịt nguội, các loại rau củ, các đồ ăn nhẹ hay pho mát… Trong văn hóa ẩm thực châu Âu, món khai vị rất được chú trọng, chính điều này đã tạo nên nét đặc trưng của nền ẩm thực đặc sắc này.&lt;/p&gt;\r\n\r\n&lt;p&gt;Đối với những bữa tiệc thì &lt;em&gt;&lt;strong&gt;món khai vị&lt;/strong&gt;&lt;/em&gt; có một vai trò quan trọng, nó giúp kích thích vị giác của thực khách, giúp họ có cảm giác ngon miệng hơn khi thưởng thức những món ăn chính. Một vài món khai vị nổi tiếng trong ẩm thực phương Tây có thể kể đến như soup À L’oignon của Pháp, Bruschetta của Ý hay soup Solyanka của Nga.&lt;/p&gt;\r\n\r\n&lt;h2&gt;&lt;strong&gt;1. Soup À L’oignon&lt;/strong&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;&lt;img alt=&quot;món khai vị trong ẩm thực âu&quot; data-ll-status=&quot;loaded&quot; height=&quot;420&quot; sizes=&quot;(max-width: 640px) 100vw, 640px&quot; src=&quot;https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au.jpg&quot; srcset=&quot;https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au.jpg 640w, https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-300x197.jpg 300w, https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-600x394.jpg 600w&quot; width=&quot;640&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;À L’oignon là món soup được ví như nữ hoàng của những món khai vị trong ẩm thực châu Âu. Bề ngoài của nó có lớp bánh mì phủ lên trên nên trông nó khá giống với những chiếc bánh pudding mặn. Theo nhiều người, món soup này có nguồn gốc từ những người La Mã, sau đó nó được du nhập vào nền ẩm thực Pháp từ thế kỷ 18. Món ăn là sự hòa quyện giữa những nguyên liệu phổ biến như thịt bò, hành tây, phô mai và bánh mì.&lt;/p&gt;\r\n\r\n&lt;h2&gt;&lt;strong&gt;2. Soup Solyanka&lt;/strong&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;&lt;img alt=&quot;món khai vị trong ẩm thực phương Tây&quot; data-ll-status=&quot;loaded&quot; height=&quot;427&quot; sizes=&quot;(max-width: 640px) 100vw, 640px&quot; src=&quot;https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-1-1024x683.jpg&quot; srcset=&quot;https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-1-1024x683.jpg 1024w, https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-1-300x200.jpg 300w, https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-1-768x512.jpg 768w, https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-1-600x400.jpg 600w, https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-1.jpg 1300w&quot; width=&quot;640&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Một trong những món khai vị nổi tiếng trong ẩm thực của Nga đó chính là món soup Solyanka. Món ăn sẽ mang lại vị chua nhẹ của dưa chuột muối cùng với đó là vị thơm của nguyệt quế và vị ngọt của những miếng thịt. Bánh mì đen là sự lựa chọn tuyệt vời để ăn cùng với loại soup này.&lt;/p&gt;\r\n\r\n&lt;p&gt;Để giúp hương vị món ăn thêm phần hấp dẫn, nhiều người sẽ cho thêm 1-2 miếng chanh vào trước khi thưởng thức. Nếu như có dịp đến với đất nước này thì bạn có thể thưởng thức chúng ở những bữa tiệc của người dân nơi đây.&lt;/p&gt;\r\n\r\n&lt;h2&gt;&lt;strong&gt;3. Soup Bouillabaise&lt;/strong&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;&lt;img alt=&quot;món khai vị&quot; data-ll-status=&quot;loaded&quot; height=&quot;462&quot; sizes=&quot;(max-width: 616px) 100vw, 616px&quot; src=&quot;https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-2.jpeg&quot; srcset=&quot;https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-2.jpeg 616w, https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-2-300x225.jpeg 300w, https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-2-600x450.jpeg 600w&quot; width=&quot;616&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Đây là một món khai vị nổi tiếng của ẩm thực Pháp. Để tạo nên một món soup Bouillabaise, người ta sẽ cần đến những nguyên liệu như cá, tôm, vẹm xanh cùng với đó là chanh vàng, cam, nghệ tây và thì là. Tại Pháp, món khai vị này sẽ được dùng chung với một chút bánh mì.&lt;/p&gt;\r\n\r\n&lt;p&gt;Món ăn mang hương vị đặc trưng của biển nên không chỉ riêng tại Pháp mà nó còn được nhiều người trên thế giới yêu thích. Ngoài hương vị hấp dẫn thì món soup này còn mang trong mình một ý nghĩa sâu sắc, đó là cầu mong giàu có và sung túc.&lt;/p&gt;\r\n\r\n&lt;h2&gt;&lt;strong&gt;4. Bruschetta của Ý&lt;/strong&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;&lt;img alt=&quot;những món khai vị trong ẩm thực phương Tây&quot; data-ll-status=&quot;loaded&quot; height=&quot;420&quot; sizes=&quot;(max-width: 640px) 100vw, 640px&quot; src=&quot;https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-3.jpg&quot; srcset=&quot;https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-3.jpg 640w, https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-3-300x197.jpg 300w, https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-3-600x394.jpg 600w&quot; width=&quot;640&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Nhắc đến ẩm thực Ý ta không thể không nhắc đến món khai vị Bruschetta này. Xuất hiện được một thời gian khá lâu. Ngày nay, món khai vị này đã du nhập đến nhiều nơi trên thế giới và chinh phục được cả những thực khách khó tính nhất. Để có thể cảm nhận hết được những hương vị tuyệt vời mà món ăn này mang lại, một ly rượu vang đi cùng sẽ là một sự lựa chọn tuyệt vời.&lt;/p&gt;",
                    "short_content": "&lt;p&gt;Khai vị là những món ăn được sử dụng trước hoặc ngoài những món ăn chính. Chúng rất đa dạng, từ các loại bánh cho tới những món thịt nguội, các loại rau củ, các đồ ăn nhẹ hay pho mát… Trong văn hóa ẩm thực châu Âu, món khai vị rất được chú trọng, chính điều này đã tạo nên nét đặc trưng của nền ẩm thực đặc sắc này.&lt;/p&gt;",
                    "image": "https://cdn.bestme.asia/images/onsteak/thiet-ke-khong-ten-2.png",
                    "meta_description": "",
                    "alias": "cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-1",
                    "blog_category_id": "1",
                    "blog_category_status": "0",
                    "blog_category_title": "Mặc định (Uncategorized)",
                    "firstname": "Admin",
                    "lastname": "Hà Thế",
                    "tags": []
                },
                {
                    "blog_id": "6",
                    "author": "1",
                    "status": "1",
                    "date_publish": "2021-01-23 10:50:47",
                    "date_added": "2021-01-23 10:50:47",
                    "date_modified": "2021-01-23 10:54:07",
                    "title": "Các món khai vị nổi tiếng  trong ẩm thực Châu Âu",
                    "content": "&lt;p&gt;Khai vị là những món ăn được sử dụng trước hoặc ngoài những món ăn chính. Chúng rất đa dạng, từ các loại bánh cho tới những món thịt nguội, các loại rau củ, các đồ ăn nhẹ hay pho mát… Trong văn hóa ẩm thực châu Âu, món khai vị rất được chú trọng, chính điều này đã tạo nên nét đặc trưng của nền ẩm thực đặc sắc này.&lt;/p&gt;\r\n\r\n&lt;p&gt;Đối với những bữa tiệc thì &lt;em&gt;&lt;strong&gt;món khai vị&lt;/strong&gt;&lt;/em&gt; có một vai trò quan trọng, nó giúp kích thích vị giác của thực khách, giúp họ có cảm giác ngon miệng hơn khi thưởng thức những món ăn chính. Một vài món khai vị nổi tiếng trong ẩm thực phương Tây có thể kể đến như soup À L’oignon của Pháp, Bruschetta của Ý hay soup Solyanka của Nga.&lt;/p&gt;\r\n\r\n&lt;h2&gt;&lt;strong&gt;1. Soup À L’oignon&lt;/strong&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;&lt;img alt=&quot;món khai vị trong ẩm thực âu&quot; data-ll-status=&quot;loaded&quot; height=&quot;420&quot; sizes=&quot;(max-width: 640px) 100vw, 640px&quot; src=&quot;https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au.jpg&quot; srcset=&quot;https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au.jpg 640w, https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-300x197.jpg 300w, https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-600x394.jpg 600w&quot; width=&quot;640&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;À L’oignon là món soup được ví như nữ hoàng của những món khai vị trong ẩm thực châu Âu. Bề ngoài của nó có lớp bánh mì phủ lên trên nên trông nó khá giống với những chiếc bánh pudding mặn. Theo nhiều người, món soup này có nguồn gốc từ những người La Mã, sau đó nó được du nhập vào nền ẩm thực Pháp từ thế kỷ 18. Món ăn là sự hòa quyện giữa những nguyên liệu phổ biến như thịt bò, hành tây, phô mai và bánh mì.&lt;/p&gt;\r\n\r\n&lt;h2&gt;&lt;strong&gt;2. Soup Solyanka&lt;/strong&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;&lt;img alt=&quot;món khai vị trong ẩm thực phương Tây&quot; data-ll-status=&quot;loaded&quot; height=&quot;427&quot; sizes=&quot;(max-width: 640px) 100vw, 640px&quot; src=&quot;https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-1-1024x683.jpg&quot; srcset=&quot;https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-1-1024x683.jpg 1024w, https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-1-300x200.jpg 300w, https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-1-768x512.jpg 768w, https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-1-600x400.jpg 600w, https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-1.jpg 1300w&quot; width=&quot;640&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Một trong những món khai vị nổi tiếng trong ẩm thực của Nga đó chính là món soup Solyanka. Món ăn sẽ mang lại vị chua nhẹ của dưa chuột muối cùng với đó là vị thơm của nguyệt quế và vị ngọt của những miếng thịt. Bánh mì đen là sự lựa chọn tuyệt vời để ăn cùng với loại soup này.&lt;/p&gt;\r\n\r\n&lt;p&gt;Để giúp hương vị món ăn thêm phần hấp dẫn, nhiều người sẽ cho thêm 1-2 miếng chanh vào trước khi thưởng thức. Nếu như có dịp đến với đất nước này thì bạn có thể thưởng thức chúng ở những bữa tiệc của người dân nơi đây.&lt;/p&gt;\r\n\r\n&lt;h2&gt;&lt;strong&gt;3. Soup Bouillabaise&lt;/strong&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;&lt;img alt=&quot;món khai vị&quot; data-ll-status=&quot;loaded&quot; height=&quot;462&quot; sizes=&quot;(max-width: 616px) 100vw, 616px&quot; src=&quot;https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-2.jpeg&quot; srcset=&quot;https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-2.jpeg 616w, https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-2-300x225.jpeg 300w, https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-2-600x450.jpeg 600w&quot; width=&quot;616&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Đây là một món khai vị nổi tiếng của ẩm thực Pháp. Để tạo nên một món soup Bouillabaise, người ta sẽ cần đến những nguyên liệu như cá, tôm, vẹm xanh cùng với đó là chanh vàng, cam, nghệ tây và thì là. Tại Pháp, món khai vị này sẽ được dùng chung với một chút bánh mì.&lt;/p&gt;\r\n\r\n&lt;p&gt;Món ăn mang hương vị đặc trưng của biển nên không chỉ riêng tại Pháp mà nó còn được nhiều người trên thế giới yêu thích. Ngoài hương vị hấp dẫn thì món soup này còn mang trong mình một ý nghĩa sâu sắc, đó là cầu mong giàu có và sung túc.&lt;/p&gt;\r\n\r\n&lt;h2&gt;&lt;strong&gt;4. Bruschetta của Ý&lt;/strong&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;&lt;img alt=&quot;những món khai vị trong ẩm thực phương Tây&quot; data-ll-status=&quot;loaded&quot; height=&quot;420&quot; sizes=&quot;(max-width: 640px) 100vw, 640px&quot; src=&quot;https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-3.jpg&quot; srcset=&quot;https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-3.jpg 640w, https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-3-300x197.jpg 300w, https://atlasgarden.vn/wp-content/uploads/2020/10/cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-3-600x394.jpg 600w&quot; width=&quot;640&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Nhắc đến ẩm thực Ý ta không thể không nhắc đến món khai vị Bruschetta này. Xuất hiện được một thời gian khá lâu. Ngày nay, món khai vị này đã du nhập đến nhiều nơi trên thế giới và chinh phục được cả những thực khách khó tính nhất. Để có thể cảm nhận hết được những hương vị tuyệt vời mà món ăn này mang lại, một ly rượu vang đi cùng sẽ là một sự lựa chọn tuyệt vời.&lt;/p&gt;",
                    "short_content": "&lt;p&gt;Khai vị là những món ăn được sử dụng trước hoặc ngoài những món ăn chính. Chúng rất đa dạng, từ các loại bánh cho tới những món thịt nguội, các loại rau củ, các đồ ăn nhẹ hay pho mát… Trong văn hóa ẩm thực châu Âu, món khai vị rất được chú trọng, chính điều này đã tạo nên nét đặc trưng của nền ẩm thực đặc sắc này.&lt;/p&gt;",
                    "image": "https://cdn.bestme.asia/images/onsteak/thiet-ke-khong-ten-4.png",
                    "meta_description": "",
                    "alias": "cac-mon-khai-vi-noi-tieng-trong-am-thuc-chau-au-2",
                    "blog_category_id": "1",
                    "blog_category_status": "0",
                    "blog_category_title": "Mặc định (Uncategorized)",
                    "firstname": "Admin",
                    "lastname": "Hà Thế",
                    "tags": []
                },
                ...
            ]
        }
        ```
        
1. get blog detail

    - method: GET
        
    - url: /api/open_api/blog/show?blog_id=1
    
    - param:
        - blog_id: enter blog_id
    
    - response:
        ```
        {
            "blog_id": "1",
            "author": "1",
            "status": "1",
            "demo": "0",
            "date_publish": "2020-04-22 00:15:47",
            "date_added": "2020-04-22 00:15:47",
            "date_modified": "2020-04-23 18:29:29",
            "language_id": "2",
            "title": "Về Siêu thị Lan Chi",
            "content": "&lt;p style=&quot;margin-left:24px; text-indent:.25in; margin-bottom:11px&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;line-height:107%&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;background:white&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Arial&amp;quot;,&amp;quot;sans-serif&amp;quot;&quot;&gt;&lt;span style=&quot;color:#1c1e21&quot;&gt;Là một trong những nhà phân phối chính thức của SUNHOUSE, Công ty Lan Chi là đơn vị tiêu biểu trong việc phân phối các mặt hàng tiêu dùng thiết yếu với giá cả tốt nhất trên toàn địa bàn địa phương. Khởi đầu là một doanh nghiệp nhỏ từ năm 2007, Lan Chi là công ty phân phối các mặt hàng tiêu dung thiết yếu phục vụ nhu cầu của người tiêu dùng chủ yếu ở ngoại thành Hà Nội.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:24px; text-indent:.25in; margin-bottom:11px&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;line-height:107%&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;background:white&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Arial&amp;quot;,&amp;quot;sans-serif&amp;quot;&quot;&gt;&lt;span style=&quot;color:#1c1e21&quot;&gt;Trải qua 13 năm xây dựng và phát triển, Công ty Lan Chi đã luôn nỗ lực mở rộng mạng lưới thị trường , xây dựng hệ thống phân phối và hệ thống Siêu Thị bán lẻ LanChi Mart trên nhiều tỉnh, thành phố , với mục đích mang đến cho người dân 1 địa điểm mua sắm GIÁ RẺ NHẤT, và chất lượng dịch vụ đảm bảo nhất. Không những thế, Công ty Lan Chi đã tạo công ăn việc làm ổn định cho hơn 1000 lao động, góp phần &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin-left:24px; text-indent:.25in; margin-bottom:11px&quot;&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;line-height:107%&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;background:white&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Arial&amp;quot;,&amp;quot;sans-serif&amp;quot;&quot;&gt;&lt;span style=&quot;color:#1c1e21&quot;&gt;Được đánh giá là một đơn vị đi đầu trong việc thực hiện bình ổn giá các mặt hàng tiêu dùng thiết yếu, Công ty Lan Chi đã không ngừng phát triển và từng bước trưởng thành. Trên nền tảng những thành tích đã đạt được trong 13 năm, Lan Chi đặt mục tiêu vươn xa hơn, mở rộng thị trường và khẳng định tên tuổi của mình trên thị trường nội địa và quốc tế trong tương lai.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt; &lt;/p&gt;",
            "short_content": "&lt;p&gt;&lt;span style=&quot;font-size:11pt&quot;&gt;&lt;span style=&quot;line-height:107%&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;span style=&quot;background:white&quot;&gt;&lt;span style=&quot;font-family:&amp;quot;Arial&amp;quot;,&amp;quot;sans-serif&amp;quot;&quot;&gt;&lt;span style=&quot;color:#1c1e21&quot;&gt;Là một trong những nhà phân phối chính thức của SUNHOUSE, Công ty Lan Chi là đơn vị tiêu biểu trong việc phân phối các mặt hàng tiêu dùng thiết yếu với giá cả tốt nhất trên toàn địa bàn địa phương. Khởi đầu là một doanh nghiệp nhỏ từ năm 2007, Lan Chi là công ty phân phối các mặt hàng tiêu dung thiết yếu phục vụ nhu cầu của người tiêu dùng chủ yếu ở ngoại thành Hà Nội.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;",
            "image": "https://res.cloudinary.com/novaonx2/image/upload/v1587697789/19444/1.png",
            "meta_title": "Giới thiệu Về Siêu thị Lan Chi",
            "meta_description": "Là một trong những nhà phân phối chính thức của SUNHOUSE, Công ty Lan Chi là đơn vị tiêu biểu trong việc phân phối các mặt hàng tiêu dùng thiết yếu với giá cả tốt nhất trên toàn địa bàn địa phương. Khởi đầu là một doanh nghiệp nhỏ từ năm 2007, Lan Chi là công ty phân phối các mặt hàng tiêu dung thiết yếu phục vụ nhu cầ",
            "seo_keywords": null,
            "alias": "gioi-thieu",
            "alt": null,
            "type": "image",
            "video_url": null,
            "blog_category_title": "Mặc định (Uncategorized)",
            "firstname": "Admin",
            "lastname": "Hà Thế",
            "tag_id": null,
            "value": null,
            "sort_order": null,
            "blog_categories": [
                {
                    "blog_id": "1",
                    "blog_category_id": "2",
                    "language_id": "2",
                    "title": "Danh mục 1",
                    "meta_title": "",
                    "meta_description": "",
                    "alias": "danh-muc-1",
                    "status": "1",
                    "date_added": "2019-11-12 11:18:26",
                    "date_modified": "2019-11-12 11:18:26"
                }
            ]
        }
        ```
        
1. create blog
    
    - method: POST
    
    - url: /api/open_api/blog/store
    
    - see edit blog bellow: remove blog_id in body data
     
1. edit blog 
    
    - method: POST
    
    - url: /api/open_api/blog/update
    
    - body data: 
        ```
        {
          "blog_id": "24",
          "title": "oke",
          "content": "<p>noi xxxxx</p>",
          "short_content": "<p>aseqaweqaweqwe</p>",
          "status": "1",
          "image": "1.jpg",
          "alt": "avc",
          "seo_name": "oke nhe",
          "body": "body nhe",
          "alias": "combo-sua-aptamil-pro-1-11",
          "seo_keywords": "xxxxxx",
          "author": "1",
          "blog_category": "1",
          "tag_list": [],
          "type": "image",
          "video_url": "https://vn.yahoo.com/"
        }
        ```
        
    - response:
        - if create
        ```
        {
            "code": 200,
            "message": "Create blog success !"
        }
        ```
        
        - if edit
        ```
        {
            "code": 200,
            "message": "Edit blog success !"
        }
        ```
        
        - and more json error
        
1. delete blog 
    
    - method: DELETE
    
    - url: /api/open_api/blog/delete
    
    - body 
        ```
        {
            "blog_id": "27"
        }
        ```
    - response:
        ```
        {
            "code": 200,
            "message": "Delete blog success !"
        }
        ```

1. get manufacturers list
    
    - method: GET
    
    - url: /api/open_api/manufacturers/index?page=1&limit=15&manufacturer_ids=
    
    - param: 
        - page: enter page
        - limit: limit of page
        - manufacturer_ids: null is all or e.g 107,106,... is multi manufacturer
    
    - response:
        ```
        {
            "total": "71",
            "page": "1",
            "records": [
                {
                    "manufacturer_id": "107",
                    "name": "32"
                },
                {
                    "manufacturer_id": "106",
                    "name": "manuface f"
                },
                ...
            ]
        }
        ```

1. get manufacturer detail

    - method: GET
    
    - url: /api/open_api/manufacturers/show?id=107
    
    - param: 
        - id: enter manufacturer_id
    
    - response:
        ```
        {
            "manufacturer_id": "107",
            "name": "32",
            "status": "1"
        }
        ```   