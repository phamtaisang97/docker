<?php

class ControllerApiOpenApiSetting extends Controller
{
    use Open_Api;

    const PERMISSION = "setting";

    public function delete() {
        if (($this->request->server['REQUEST_METHOD'] !== 'DELETE')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use DELETE only!'
            ];

            $this->log->write('open api setting delete(): resp: ' . 'Bad request. Use DELETE only!');
            $this->responseJson($error);
            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validateAccessToken()) {
            $this->responseJson($json);
            return;
        }

        // get post data from php://input
//        $this->request->post = json_decode(file_get_contents('php://input'), true);
        try {
            $this->load->model('setting/setting');
            $this->model_setting_setting->editSettingValue('config', 'config_shop_disabled', 1);

            $json = [
                'code' => 200,
                'message' => 'Website disable successfully'
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->responseJson($json);

    }

}