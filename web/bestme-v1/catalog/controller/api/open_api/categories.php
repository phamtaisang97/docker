<?php

class ControllerApiOpenApiCategories extends Controller
{
    use Open_Api;
    const PERMISSION = "category";
    // api/open_api/category group
    public function index()
    {
        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("index")) {
            $this->responseJson($json);
            return;
        }
        /* do getting data */
        $this->load->model('catalog/category');
        $json = $this->model_catalog_category->getCategoriesForClassifyss($this->request->get);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function create()
    {
        $json = [];
        $this->responseJson($json);
        return;
    }

    public function store()
    {
        $this->load->model('catalog/category');
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->log->write('createCategory(): resp: ' . 'Bad request. Use POST only!');

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("store")) {
            $this->responseJson($json);
            return;
        }
        $error = [];
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('createCategory(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        $validate_result = $this->validateCategoryPostData();
        if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
            $error = is_array($validate_result) ? $validate_result : [];
            $this->responseJson($error);
            return;
        }

        // pass data post
        $data['name'] = isset($this->request->post['name']) ? $this->request->post['name'] : '';
        $data['category_description'] = isset($this->request->post['category_description']) ? $this->request->post['category_description'] : '';
        $data['parent_id'] = isset($this->request->post['parent_id']) ? $this->request->post['parent_id'] : '';
        $data['status'] = isset($this->request->post['status']) ? $this->request->post['status'] : '';
        $data['image'] = isset($this->request->post['image']) ? $this->request->post['image'] : '';
        $data['sub_categories'] = isset($this->request->post['sub_categories']) ? $this->request->post['sub_categories'] : '';

        // create category

        /*
         * $data sample:
         *
         * [
         *    "parent_id" => 0,
         *    "status" => 1,
         *    "category_description" => $name,
         *    "name" => $name,
         *    "image" => '',
         *    "sub_categories" => [
         *        "cat-1",
         *        "cat-2"
         *    ],
         * ];
         */
        $category = [
            'name' => $data['name'],
            'category_description' => $data['category_description'],
            'parent_id' => $data['parent_id'],
            'status' => $data['status'],
            'image' => $data['image'],
            'sub_categories' => $data['sub_categories'],
        ];

        $this->model_catalog_category->addCategory($category);

        $json = [
            'code' => 200,
            'message' => 'Create category success !',
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function show()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'GET')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use GET only!'
            ];

            $this->log->write('resp: ' . 'Bad request. Use GET only!');

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("show")) {
            $this->responseJson($json);
            return;
        }

        // check if has category_id
        if (!isset($this->request->get['category_id']) || empty($this->request->get['category_id'])) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Missing "category_id"!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $category_id = $this->request->get['category_id'];
        $this->load->model('catalog/category');
        $result = $this->model_catalog_category->getCategoryCustom($category_id);
        if (!$result) {
            $error = [
                'code' => 506,
                'message' => 'category (with category_id) does not existed'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($result));
    }

    public function update()
    {
        $this->load->model('catalog/category');
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->log->write('updateCategory(): resp: ' . 'Bad request. Use POST only!');

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("store")) {
            $this->responseJson($json);
            return;
        }
        $this->request->post = json_decode(file_get_contents('php://input'), true);
        // check if has category_id
        if (!isset($this->request->post['category_id']) || empty($this->request->post['category_id'])) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Missing "category_id"!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $category_id = $this->request->post['category_id'];

        $exist = $this->model_catalog_category->getCategory($category_id);
        if (!$exist) {
            $error = [
                'code' => 506,
                'message' => 'category (with category_id) does not existed'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $validate_result = $this->validateCategoryPostData($exist);
        if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
            $error = is_array($validate_result) ? $validate_result : [];
            $this->responseJson($error);
            return;
        }

        // pass data post
        $data['name'] = isset($this->request->post['name']) ? $this->request->post['name'] : '';
        $data['old_name'] = isset($this->request->post['old_name']) ? $this->request->post['old_name'] : '';
        $data['category_description'] = isset($this->request->post['category_description']) ? $this->request->post['category_description'] : '';
        $data['parent_id'] = isset($this->request->post['parent_id']) ? $this->request->post['parent_id'] : '';
        $data['status'] = isset($this->request->post['status']) ? $this->request->post['status'] : '';
        $data['image'] = isset($this->request->post['image']) ? $this->request->post['image'] : '';
        $data['sub_categories'] = isset($this->request->post['sub_categories']) ? $this->request->post['sub_categories'] : '';

        // create category

        /*
         * $data sample:
         *
         * [
         *    "parent_id" => 0,
         *    "status" => 1,
         *    "category_description" => $name,
         *    "name" => $name,
         *    "image" => '',
         *    "sub_categories" => [
         *        "cat-1",
         *        "cat-2"
         *    ],
         * ];
         */
        $categoryData = [
            'name' => $data['name'],
            'old_name' => $data['old_name'],
            'category_description' => $data['category_description'],
            'parent_id' => $data['parent_id'],
            'status' => $data['status'],
            'image' => $data['image'],
            'sub_categories' => $data['sub_categories'],
        ];

        $this->model_catalog_category->editCategory($category_id, $categoryData);

        $json = [
            'code' => 200,
            'message' => 'Edit category success !',
        ];
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function delete()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'DELETE')) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Use DELETE only!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("delete")) {
            $this->responseJson($json);
            return;
        }
        $this->request->post = json_decode(file_get_contents('php://input'), true);
        // check if has category_id
        if (!isset($this->request->post['category_id']) || empty($this->request->post['category_id'])) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Missing "category_id"!'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }
        $category_id = $this->request->post['category_id'];
        // check exist category_id in database
        $this->load->model('catalog/category');
        $exist = $this->model_catalog_category->getCategory($category_id);
        if (!$exist) {
            $error = [
                'code' => 506,
                'message' => 'category (with category_id) does not existed'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }
        $this->model_catalog_category->deleteCategory($category_id);
        $json = [
            'code' => 200,
            'message' => 'Delete category success !',
        ];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function sync()
    {
        $this->load->model('catalog/category');
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->log->write('updateCategory(): resp: ' . 'Bad request. Use POST only!');

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($error));

            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("sync", "modify")) {
            $this->responseJson($json);
            return;
        }
        try {
            $this->request->post = json_decode(file_get_contents('php://input'), true);
            /*build data to category*/
            /*
             * [
             *  'name' => '123',
             *  'old_name' => '123 5',
             *  'source' => 'omihub',
             * ]
             * */
            $data_to_save = $this->request->post['data'];
            $this->load->model('catalog/category');
            $source = isset($data_to_save['source']) ? $data_to_save['source'] : '';
            $old_name = isset($data_to_save['old_name']) ? $data_to_save['old_name'] : '';

            if (!empty($old_name)) {
                $category_id = $this->model_catalog_category->getCategoryIdByName($data_to_save['old_name']);
            } else {
                $category_id = $this->model_catalog_category->getCategoryIdByName($data_to_save['name']);
            }

            if (isset($category_id) && !empty($category_id)) {
                $category_id = $this->model_catalog_category->updateCategoryFast($category_id, $data_to_save['name'], $source);
            } else {
                $category_id = $this->model_catalog_category->addCategoryFast($data_to_save['name'], $source);
            }

            $json = [
                'code' => 200,
                'message' => 'Sync success!',
                'category_id' => $category_id
            ];
        } catch (Exception $exception) {
            $json = [
                'code' => 201,
                'message' => 'unknown error: ' . $exception->getMessage()
            ];
        }

        $this->responseJson($json);
        return;
    }

    public function deleteCategory()
    {
        try {
            if (($this->request->server['REQUEST_METHOD'] !== 'DELETE')) {
                $error = [
                    'code' => 101,
                    'message' => 'Bad request. Use DELETE only!'
                ];

                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($error));

                return;
            }

            $json = [
                'code' => 201,
                'message' => 'Not permission'
            ];

            if (!$this->validate("delete")) {
                $this->responseJson($json);
                return;
            }
            $this->request->post = json_decode(file_get_contents('php://input'), true);
            // check if has name
            if (!isset($this->request->post['data']['name']) || empty($this->request->post['data']['name'])) {
                $error = [
                    'code' => 101,
                    'message' => 'Bad request. Missing "name"!'
                ];

                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($error));

                return;
            }
            $category_name = $this->request->post['data']['name'];
            // check exist category_id in database
            $this->load->model('catalog/category');
            $category_id = $this->model_catalog_category->getCategoryIdByName($category_name);
            if (!$category_id) {
                $error = [
                    'code' => 506,
                    'message' => 'category (with category_name) does not existed'
                ];

                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($error));

                return;
            }

            $this->model_catalog_category->deleteCategory($category_id);
            $json = [
                'code' => 200,
                'message' => 'Delete category success !',
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        } catch (Exception $exception) {
            $json = [
                'code' => 0,
                'message' => $exception->getMessage(),
            ];
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    /**
     * @param null $exist
     * @return array|void
     */
    public function validateCategoryPostData($exist = null)
    {
        $this->load->model('catalog/category');

        $category_id = isset($this->request->post['category_id']) ? $this->request->post['category_id'] : '';

        $name = isset($this->request->post['name']) ? $this->request->post['name'] : '';

        if (!$name) {
            return [
                'code' => 101,
                'message' => 'missing or invalid name category !'
            ];
        }

        $sub_category_names = isset($this->request->post['sub_categories']) && is_array($this->request->post['sub_categories']) ? $this->request->post['sub_categories'] : [];
        $existing_sub_categories = $this->model_catalog_category->getCategoriesByNames($sub_category_names);



        if (isset($category_id) && !empty($exist)) {
            //validate edit
            if ($this->request->post['name'] != $exist['name']) {
                if ($this->model_catalog_category->checkNameCategoryExist($this->request->post['name'], $category_id) != false) {
                    return [
                        'code' => 125,
                        'message' => 'error name category exist !'
                    ];
                }
            }

        } else {
            //validate create
            foreach ($existing_sub_categories as $existing_sub_category) {
                $parent_id = $this->model_catalog_category->getCategory($existing_sub_category['category_id'])['parent_id'];
                if ($parent_id > 0) {
                    return [
                        'code' => 125,
                        'message' => 'A subcategory already exists that is associated with the parent category !'
                    ];
                }
            }
            if ($this->request->post['name'] != $exist['name']) {
                if ($this->model_catalog_category->checkNameCategoryExist($this->request->post['name'], $category_id) != false) {
                    return [
                        'code' => 125,
                        'message' => 'error name category exist !'
                    ];
                }
            }
        }

        return [
            'code' => 200
        ];
    }
}