<?php

class ControllerApiOpenApiCollections extends Controller
{
    use Open_Api;

    const PERMISSION = "collection";

    // api/open_api/collections
    public function index()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'GET')) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Use GET only!'
            ];

            $this->responseJson($error);
            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("index")) {
            $this->responseJson($json);
            return;
        }

        $data_filter = [];
        $query_params = $this->request->get;

        if (isset($query_params['page']) && !empty($query_params['page'])) {
            $data_filter = [
                'start' => ((int)$query_params['page'] - 1) * ((int)isset($query_params['limit']) ? $query_params['limit'] : 15),
                'limit' => isset($query_params['limit']) ? $query_params['limit'] : 15
            ];
        }

        if (isset($query_params['name'])) {
            $data_filter = array_merge($data_filter, [
                'filter_name' => $query_params['name']
            ]);
        }

        if (isset($query_params['status'])) {
            $data_filter = array_merge($data_filter, [
                'filter_status' => $query_params['status']
            ]);
        }

        /* do getting data */
        try {
            $this->load->model('catalog/collection');
            $json = $this->model_catalog_collection->getListDataCollections($data_filter);

            if (empty($json)) {
                $json = [
                    'code' => 201,
                    'message' => 'No data'
                ];
            }

        } catch (Exception $e) {
            $json = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->responseJson($json);

        return;
    }

    public function create()
    {
        return;
    }

    public function store()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->responseJson($error);
            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("store")) {
            $this->responseJson($json);
            return;
        }

        // get post data from php://input
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('[open api] collection store(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {}

        $this->load->model('catalog/collection');
        $validate_result = $this->validateCollectionPostData();
        if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
            $error = is_array($validate_result) ? $validate_result : [];
            $this->responseJson($error);
            return;
        }

        $count = $this->model_catalog_collection->check_collection('', $this->request->post['data']['title']);
        if ($count) {
            $json =  [
                'code' => 400,
                'message' => 'Title already exists'
            ];

            $this->responseJson($json);
            return;
        }

        $data_post = $this->mapData($this->request->post['data']);

        try {
            $collection_id = $this->model_catalog_collection->addCollection($data_post);
            $json = [
                'code' => 0,
                'message' => 'COLLECTION_CREATED',
                'collection_id' => $collection_id
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->responseJson($json);
        return;
    }

    public function show()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'GET')) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Use GET only!'
            ];

            $this->responseJson($error);
            return;
        }


        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("show")) {
            $this->responseJson($json);
            return;
        }

        $data_filter = [];
        $query_params = $this->request->get;

        if (!isset($query_params['collection_id'])) {
            $json = [
                'code' => 201,
                'message' => 'Missing param collection_id'
            ];

            $this->responseJson($json);
            return;
        }

        if (isset($query_params['collection_id'])) {
            $data_filter = array_merge($data_filter, [
                'filter_collection_id' => $query_params['collection_id']
            ]);
        }

        try {
            /* do getting data */
            $this->load->model('catalog/collection');
            $json = $this->model_catalog_collection->getCollection($data_filter);

            if (empty($json)) {
                $json = [
                    'code' => 201,
                    'message' => 'Collection does not exist'
                ];
            }
        } catch (Exception $e) {
            $json = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->responseJson($json);
        return;
    }

    public function update()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->responseJson($error);
            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("update")) {
            $this->responseJson($json);
            return;
        }

        // get post data from php://input
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('[open api] collection update(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {}

        $this->load->model('catalog/collection');
        $validate_result = $this->validateCollectionPostData();
        if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
            $error = is_array($validate_result) ? $validate_result : [];
            $this->responseJson($error);
            return;
        }

        $collection_id = isset($this->request->post['data']['collection_id']) ? $this->request->post['data']['collection_id'] : '';
        if(empty($collection_id)) {
            $json =  [
                'code' => 400,
                'message' => 'Collection_id not empty'
            ];

            $this->responseJson($json);
            return;
        }

        $count = $this->model_catalog_collection->check_collection($collection_id, $this->request->post['data']['title']);
        if ($count) {
            $json =  [
                'code' => 400,
                'message' => 'Title already exists'
            ];

            $this->responseJson($json);
            return;
        }

        $data_post = $this->mapData($this->request->post['data']);

        try {
            $this->model_catalog_collection->editCollection($data_post['collection_id'], $data_post);
            $json = [
                'code' => 0,
                'message' => 'COLLECTION_UPDATED'
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->responseJson($json);
        return;
    }

    public function delete()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'DELETE')) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Use DELETE only!'
            ];

            $this->responseJson($error);
            return;
        }


        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("delete")) {
            $this->responseJson($json);
            return;
        }

        $this->request->post = json_decode(file_get_contents('php://input'), true);

        // check if has product_id
        if (!isset($this->request->post['data']['collection_id']) || empty($this->request->post['data']['collection_id'])) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Missing "collection_id"!'
            ];

            $this->responseJson($error);
            return;
        }

        try {
            $collection_id = $this->request->post['data']['collection_id'];
            $this->load->model('catalog/collection');
            $this->model_catalog_collection->deleteCollection($collection_id);

            $json = [
                'code' => 0,
                'message' => 'Collection deleted successfully'
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->responseJson($json);
        return;
    }

    private function validateCollectionPostData()
    {
        $bad_request_code = 400;

        $data_data = isset($this->request->post['data']) ? $this->request->post['data']: [];
        if (empty($data_data)) {
            return [
                'code' => $bad_request_code,
                'message' => 'Data empty'
            ];
        }

        $title = isset($this->request->post['data']['title']) ? $this->request->post['data']['title'] : '';
        if(empty($title)) {
            return [
                'code' => $bad_request_code,
                'message' => 'Title not empty'
            ];
        }

        $type_select = isset($this->request->post['data']['type_select']) ? $this->request->post['data']['type_select'] : '';
        if ($type_select == '') {
            return [
                'code' => $bad_request_code,
                'message' => 'Type collection not empty'
            ];
        }

        if (!in_array($type_select, [0,1])) {
            return [
                'code' => $bad_request_code,
                'message' => 'Type collection incorrect'
            ];
        }

        // type_select : 1: is collection collection, 0: is collection product
        $c_list = isset($this->request->post['data']['collection_list']) ? $this->request->post['data']['collection_list'] : [];
        if($type_select) {
            if (empty($c_list)) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'Choose at least one collection'
                ];
            } else {
                if(!$this->collectionListIds($c_list)) {
                    return [
                        'code' => $bad_request_code,
                        'message' => 'List collection is not number'
                    ];
                }

                if (count($c_list) > $this->model_catalog_collection->getCollectionByIds($c_list)) {
                    return [
                        'code' => $bad_request_code,
                        'message' => 'Collection list has invalid element'
                    ];
                }
            }
        } else {
            if (empty($c_list)) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'Choose at least one product'
                ];
            } else {
                if(!$this->collectionListIds($c_list)) {
                    return [
                        'code' => $bad_request_code,
                        'message' => 'List product is not number'
                    ];
                }

                if (count($c_list) > $this->model_catalog_collection->getProductByIds($c_list)) {
                    return [
                        'code' => $bad_request_code,
                        'message' => 'Product list has invalid element'
                    ];
                }
            }
        }

        return [
            'code' => 200
        ];
    }

    /**
     * @param array $data_post
     * @return array
     */
    private function mapData($data_post)
    {
        $data = [];

        if (empty($data_post)) {
            return $data;
        }

        $data['menu'] = [];
        $data['collection_id'] = isset($data_post['collection_id']) ? $data_post['collection_id'] : '';
        $data['title'] = trim(isset($data_post['title'])) ? trim($data_post['title']) : '';
        $data['description'] = isset($data_post['description']) ? $data_post['description'] : '';
        $data['type_select'] = isset($data_post['type_select']) ? $data_post['type_select'] : '';
        $data['collection_list'] = isset($data_post['collection_list']) ? $data_post['collection_list'] : [];
        $data['image'] = isset($data_post['image']) ? $data_post['image'] : '';
        $data['status'] = isset($data_post['status']) ? $data_post['status'] : 0;

        $data['seo_name'] = trim(isset($data_post['seo_name'])) ? trim($data_post['seo_name']) : '';
        $data['body'] = isset($data_post['seo_description']) ? $data_post['seo_description'] : '';
        $data['alias'] = isset($data_post['alias']) ? $data_post['alias'] : '';
        $data['cat_list'] = isset($data_post['cat_list']) ? $data_post['cat_list'] : '';

        return $data;
    }

    private function collectionListIds($c_lists)
    {
        $is_num = true;
        foreach ($c_lists as $element) {
            if (!is_numeric($element)) {
                $is_num = false;
                break;
            }
        }

        return $is_num;
    }
}