<?php

class ControllerApiOpenApiProducts extends Controller
{
    use Open_Api;

    const PERMISSION = "product";
    const SCOPE_MODIFY = "modify";

    // api/open_api/products
    public function index()
    {
        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("index")) {
            $this->responseJson($json);
            return;
        }

        /* do getting data */
        $this->load->model('api/chatbox_novaon_v2');
        $json = $this->model_api_chatbox_novaon_v2->getProductsListData_v1_1($this->request->get);
        $this->responseJson($json);

        return;
    }

    public function create()
    {
        $json = [];
        $this->responseJson($json);
        return;
    }

    public function store()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->log->write('open api products store(): resp: ' . 'Bad request. Use POST only!');
            $this->responseJson($error);
            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("store")) {
            $this->responseJson($json);
            return;
        }

        // get post data from php://input
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('[chatbot_novaon_v2.0] product(): post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        $validate_result = $this->validateProductPostData();
        if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
            $error = is_array($validate_result) ? $validate_result : [];
            $this->responseJson($error);
            return;
        }

        try {
            /*build data to add product*/
            $data_to_save = $this->request->post['data'];

            $data_to_save = $this->beForeSave($data_to_save);

            /*add product*/
            $this->load->model('catalog/product');
            $product_id = $this->model_catalog_product->addProduct($data_to_save);
            $error = [
                'code' => 0,
                'message' => 'PRODUCT_CREATED',
                'product_id' => $product_id
            ];
        } catch (Exception $e) {
            $error = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->log->write('createProduct(): final resp: ' . json_encode($error));

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($error));

        return;
    }

    public function show()
    {
        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("show")) {
            $this->responseJson($json);
            return;
        }

        $query_params = $this->request->get;
        if (isset($query_params['id'])) {
            $data_filter = [
                'id' => $query_params['id'],
                'product_version_id' => isset($query_params['product_version_id']) ? $query_params['product_version_id'] : '',
            ];
        } else {
            $data_filter = [];
        }

        if (empty($data_filter)) {
            $json = [
                'code' => 201,
                'message' => 'Missing param id'
            ];

            $this->responseJson($json);
            return;
        }

        /* do getting data */
        $this->load->model('api/chatbox_novaon_v2');
        $json = $this->model_api_chatbox_novaon_v2->getProductsListData_v1_1($data_filter);
        $this->responseJson($json);

        return;
    }

    public function update()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'POST')) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Use POST only!'
            ];

            $this->responseJson($error);
            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("update")) {
            $this->responseJson($json);
            return;
        }

        $error = array();

        // get post data from php://input
        $this->request->post = json_decode(file_get_contents('php://input'), true);

        try {
            $this->log->write('[open api] update product : post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        // check if has product_id
        if (!isset($this->request->post['data']['product_id']) || empty($this->request->post['data']['product_id'])) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Missing "product_id"!'
            ];

            $this->responseJson($error);
            return;
        }

        $validate_result = $this->validateProductPostData();
        if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
            $error = is_array($validate_result) ? $validate_result : [];
            $this->responseJson($error);
            return;
        }

        $product_id = $this->request->post['data']['product_id'];
        // check exist product_id in database
        $this->load->model('catalog/product');
        $exist = $this->model_catalog_product->getProduct($product_id);
        if (!$exist) {
            $error = [
                'code' => 506,
                'message' => 'product (with product_id) does not existed'
            ];

            $this->responseJson($error);
            return;
        }

        try {
            $data_to_save = $this->request->post['data'];
            $data_to_save['seo_title'] = '';
            $data_to_save['seo_desciption'] = '';

            $data_to_save = $this->beForeSave($data_to_save);

            $product_id = $this->model_catalog_product->editProduct($product_id, $data_to_save);
            $error = [
                'code' => 0,
                'message' => 'product edited successfully'
            ];
        } catch (Exception $e) {
            $error = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->log->write('createProduct(): final resp: ' . json_encode($error));

        $this->responseJson($error);
        return;
    }

    public function delete()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'DELETE')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use DELETE only!'
            ];

            $this->log->write('open api products delete(): resp: ' . 'Bad request. Use DELETE only!');
            $this->responseJson($error);
            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("delete")) {
            $this->responseJson($json);
            return;
        }

        // get post data from php://input
        $this->request->post = json_decode(file_get_contents('php://input'), true);
        try {
            $this->log->write('[open api] delete product : post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        // check if has product_id
        if (!isset($this->request->post['data']['product_id']) || empty($this->request->post['data']['product_id'])) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Missing "product_id"!'
            ];

            $this->responseJson($error);
            return;
        }

        try {
            $product_id = $this->request->post['data']['product_id'];
            $this->load->model('catalog/product');
            $this->model_catalog_product->deleteProduct($product_id, true);

            $json = [
                'code' => 0,
                'message' => 'product deleted successfully'
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->responseJson($json);
        return;
    }

    public function sync()
    {
        if ($this->request->server['REQUEST_METHOD'] !== 'POST') {
            $json = [
                'message' => 'Bad request. Use POST only!'
            ];
            $this->responseJson($json, 400);
            return false;
        }

        $json = [
            'message' => 'Not permission'
        ];

        if (!$this->validate("sync", self::SCOPE_MODIFY)) {
            $this->responseJson($json, 401);
            return false;
        }
        try {
            // get post data from php://input
            $this->request->post = json_decode(file_get_contents('php://input'), true);

            $validate_result = $this->validateSyncProduct();
            if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
                $error = is_array($validate_result) ? $validate_result : [];
                $this->responseJson($error);
                return;
            }

            /*build data to add product*/
            $data_to_save = $this->request->post['data'];

            $data_to_save = $this->beForeSave($data_to_save);

            /*add product*/
            $this->load->model('catalog/product');

            if ($data_to_save['product_version'] == 1) {
                foreach ($data_to_save['product_skus'] as $product_sku) {
                    $productId = $this->model_catalog_product->searchProductAndVersionBySkuForSync($product_sku);
                    if ($productId) {
                        break;
                    }
                }
            } elseif ($data_to_save['product_version'] == 0) {
                $productId = $this->model_catalog_product->searchProductAndVersionBySkuForSync($data_to_save['sku']);
            } else {
                $productId = 0;
            }

             if (empty($productId)) {
                 $productId = $this->model_catalog_product->searchProductByName($data_to_save['name']);
             }

            if ($productId) {
                $product_id = $productId;
                $data_to_save['product_version_ids'] = $this->model_catalog_product->getProductVersionIds($product_id);
                $data_to_save['product_version_ids_api'] = true;
                $this->model_catalog_product->editProduct($productId, $data_to_save);
            } else {
                $product_id = $this->model_catalog_product->addProduct($data_to_save);
            }

            $json = [
                'code' => 200,
                'message' => 'Sync success!',
                'product_id' => $product_id
            ];


        } catch (Exception $exception) {
            $json = [
                'code' => 201,
                'message' => 'unknown error: ' . $exception->getMessage()
            ];
        }

        $this->responseJson($json);
        return;
    }

    public function deleteProduct()
    {
        if (($this->request->server['REQUEST_METHOD'] !== 'DELETE')) {
            $error = [
                'code' => 201,
                'message' => 'Bad request. Use DELETE only!'
            ];

            $this->log->write('open api products deleteProduct(): resp: ' . 'Bad request. Use DELETE only!');
            $this->responseJson($error);
            return;
        }

        $json = [
            'code' => 201,
            'message' => 'Not permission'
        ];

        if (!$this->validate("delete")) {
            $this->responseJson($json);
            return;
        }

        // get post data from php://input
        $this->request->post = json_decode(file_get_contents('php://input'), true);
        try {
            $this->log->write('[open api] deleteProduct product : post data = ' . json_encode($this->request->post, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
        }

        // check if has product_id
        if (!isset($this->request->post['data']['product_sku']) || empty($this->request->post['data']['product_sku'])) {
            $error = [
                'code' => 101,
                'message' => 'Bad request. Missing "product_sku"!'
            ];

            $this->responseJson($error);
            return;
        }

        try {
            $this->load->model('catalog/product');
            $product_sku = $this->request->post['data']['product_sku'];
            $product_id = $this->model_catalog_product->getProductIdBySku($product_sku);

            if (empty($product_id)) {
                $json = [
                    'code' => 404,
                    'message' => 'Not found product'
                ];
            } else {
                $this->model_catalog_product->deleteProduct($product_id, true);

                $json = [
                    'code' => 200,
                    'product_id' => $product_id,
                    'message' => 'product deleted successfully'
                ];
            }
        } catch (Exception $e) {
            $json = [
                'code' => 201,
                'message' => 'unknown error: ' . $e->getMessage()
            ];
        }

        $this->responseJson($json);
        return;
    }

    private function validateSyncProduct()
    {
        $bad_request_code = 400;
        $this->load->model('catalog/product');

        $product_version = isset($this->request->post['data']['product_version']) ? $this->request->post['data']['product_version'] : '';
        if (!isset($product_version)) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing or invalid product_version'
            ];
        }

        if ($product_version == 1) {
            $product_skus = isset($this->request->post['data']['product_skus']) ? $this->request->post['data']['product_skus'] : [];
            foreach ($product_skus as $product_sku) {
                $product_id = $this->model_catalog_product->searchProductAndVersionBySkuForSync($product_sku);
                if ($product_id) {
                    break;
                }
            }
        } elseif ($product_version == 0) {
            $sku = isset($this->request->post['data']['sku']) ? $this->request->post['data']['sku'] : '';
            if (!$sku) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid sku'
                ];
            }

            $product_id = $this->model_catalog_product->searchProductAndVersionBySkuForSync($sku);
        } else {
            $product_id = 0;
        }

        if (empty($product_id)) {
            $product_id = $this->model_catalog_product->searchProductByName($this->request->post['data']['name']) ? $this->model_catalog_product->searchProductByName($this->request->post['data']['name']) : 0;
        }

        $name = isset($this->request->post['data']['name']) ? $this->request->post['data']['name'] : '';
        if (!$name) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing or invalid name'
            ];
        } else {
            if ($this->model_catalog_product->checkExistingProductByName($this->request->post['data']['name'], $product_id) != false) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'error name exist'
                ];
            }
        }

         if ($product_version == '1') {
            $product_skus = isset($this->request->post['data']['product_skus']) ? $this->request->post['data']['product_skus'] : '';
            if (!$product_skus || !is_array($product_skus)) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid product_skus'
                ];
            } else {
                foreach ($product_skus as $product_sku) {
                    if (!empty($product_sku) && $this->model_catalog_product->searchProductAndVersionBySKU($product_sku, $product_id) == false) {
                        return [
                            'code' => $bad_request_code,
                            'message' => 'error sku exist'
                        ];
                    }
                }
            }

            if (!isset($this->request->post['data']['common_weight'])) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing common_weight'
                ];
            }
        } elseif ($product_version == 0) {
            $sku = isset($this->request->post['data']['sku']) ? $this->request->post['data']['sku'] : '';
            if (!$sku) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid sku'
                ];
            } else {
                if ($this->model_catalog_product->searchProductAndVersionBySKU($this->request->post['data']['sku'], $product_id) == false) {
                    return [
                        'code' => $bad_request_code,
                        'message' => 'error sku exist'
                    ];
                }
            }

            if (!isset($this->request->post['data']['weight'])) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing weight'
                ];
            }
        }

        return [
            'code' => 200
        ];
    }

    private function validateProductPostData()
    {
        $bad_request_code = 400;
        $this->load->model('catalog/product');

        $product_id = isset($this->request->post['data']['product_id']) ? $this->request->post['data']['product_id'] : '';

        $name = isset($this->request->post['data']['name']) ? $this->request->post['data']['name'] : '';
        if (!$name) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing or invalid name'
            ];
        } else {
            if ($this->model_catalog_product->checkExistingProductByName($this->request->post['data']['name'], $product_id) != false) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'error name exist'
                ];
            }
        }

//        $description = isset($this->request->post['data']['description']) ? $this->request->post['data']['description'] : '';
//        if (!$description) {
//            return [
//                'code' => $bad_request_code,
//                'message' => 'missing or invalid description'
//            ];
//        }

//        $summary = isset($this->request->post['data']['summary']) ? $this->request->post['data']['summary'] : '';
//        if (!$summary) {
//            return [
//                'code' => $bad_request_code,
//                'message' => 'missing or invalid summary'
//            ];
//        }

        if (!isset($this->request->post['data']['price'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing price'
            ];
        }

        if (!isset($this->request->post['data']['promotion_price'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing promotion_price'
            ];
        }

        if (!isset($this->request->post['data']['weight'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing weight'
            ];
        }

        if (!isset($this->request->post['data']['sku'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing sku'
            ];
        }

        if (!isset($this->request->post['data']['barcode'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing barcode'
            ];
        }

        if (!isset($this->request->post['data']['common_compare_price'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing common_compare_price'
            ];
        }

        if (!isset($this->request->post['data']['common_price'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing common_price'
            ];
        }

        if (!isset($this->request->post['data']['common_weight'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing common_weight'
            ];
        }

        if (!isset($this->request->post['data']['common_sku'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing common_sku'
            ];
        }

//        if (!isset($this->request->post['data']['common_barcode'])) {
//            return [
//                'code' => $bad_request_code,
//                'message' => 'missing common_barcode'
//            ];
//        }

        if (!isset($this->request->post['data']['common_cost_price'])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing common_cost_price'
            ];
        }

        $product_version = isset($this->request->post['data']['product_version']) ? $this->request->post['data']['product_version'] : '';
        if (!isset($product_version)) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing or invalid product_version'
            ];
        } elseif ($product_version == '1') {
            $common_compare_price = isset($this->request->post['data']['common_compare_price']) ? $this->request->post['data']['common_compare_price'] : '';
            if (!$common_compare_price || floatval($common_compare_price) > 99999999999) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid common_compare_price , max 99,999,999,999'
                ];
            }

            $common_price = isset($this->request->post['data']['common_price']) ? $this->request->post['data']['common_price'] : '';
            if (!$common_price || floatval($common_price) > 99999999999) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid common_price , max 99,999,999,999'
                ];
            }

            $attribute_name = isset($this->request->post['data']['attribute_name']) ? $this->request->post['data']['attribute_name'] : '';
            if (!$attribute_name || !is_array($attribute_name)) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid attribute_name'
                ];
            }

            foreach ($attribute_name as $att) {
                if (utf8_strlen($att) > 30) {
                    return [
                        'code' => $bad_request_code,
                        'message' => 'invalid attribute_name. each name max length 30 '
                    ];
                }
            }

            $attribute_values = isset($this->request->post['data']['attribute_values']) ? $this->request->post['data']['attribute_values'] : '';
            if (!$attribute_values || !is_array($attribute_values) || count($attribute_values) > 100) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid attribute_values or total max 100 values'
                ];
            }

            foreach ($attribute_values as $attrs) {
                foreach ($attrs as $attr) {
                    if (utf8_strlen($attr) > 30) {
                        return [
                            'code'    => $bad_request_code,
                            'message' => 'invalid attribute_name. each value max length 30 '
                        ];
                    }
                }
            }

            $product_prices = isset($this->request->post['data']['product_prices']) ? $this->request->post['data']['product_prices'] : '';
            if (!$product_prices || !is_array($product_prices)) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid product_prices'
                ];
            }

            foreach ($product_prices as $price) {
                if (floatval($price) > 99999999999) {
                    return [
                        'code' => $bad_request_code,
                        'message' => 'missing or invalid product_prices max 99,999,999,999'
                    ];
                }
            }

            $product_promotion_prices = isset($this->request->post['data']['product_promotion_prices']) ? $this->request->post['data']['product_promotion_prices'] : '';
            if (!$product_promotion_prices || !is_array($product_promotion_prices)) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid product_promotion_prices'
                ];
            }

            foreach ($product_promotion_prices as $price) {
                if (floatval($price) > 99999999999) {
                    return [
                        'code' => $bad_request_code,
                        'message' => 'missing or invalid product_promotion_prices max 99,999,999,999'
                    ];
                }
            }

            $product_quantities = isset($this->request->post['data']['product_quantities']) ? $this->request->post['data']['product_quantities'] : '';
            if (!$product_quantities || !is_array($product_quantities)) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid product_quantities'
                ];
            }

            foreach ($product_quantities as $quantity) {
                if (floatval($quantity) > 9999999) {
                    return [
                        'code' => $bad_request_code,
                        'message' => 'missing or invalid product_quantities max 9,999,999'
                    ];
                }
            }

            $product_skus = isset($this->request->post['data']['product_skus']) ? $this->request->post['data']['product_skus'] : '';
            if (!$product_skus || !is_array($product_skus)) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid product_skus'
                ];
            } else {
                foreach ($product_skus as $product_sku) {
                    if (!empty($product_sku) && $this->model_catalog_product->searchProductAndVersionBySKU($product_sku, $product_id) == false) {
                        return [
                            'code' => $bad_request_code,
                            'message' => 'error sku exist'
                        ];
                    }
                }
            }

            /*$product_barcodes = isset($this->request->post['data']['product_barcodes']) ? $this->request->post['data']['product_barcodes'] : '';
            if (!$product_barcodes || !is_array($product_barcodes)) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid product_barcodes'
                ];
            }*/

            $common_weight = isset($this->request->post['data']['common_weight']) ? $this->request->post['data']['common_weight'] : '';
            if (!$common_weight || floatval($common_weight) > 9999999) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid common_weight, max 9,999,999'
                ];
            }

            $common_sku = isset($this->request->post['data']['common_sku']) ? $this->request->post['data']['common_sku'] : '';
            if (!$common_sku) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid common_sku'
                ];
            }

//            $common_barcode = isset($this->request->post['data']['common_barcode']) ? $this->request->post['data']['common_barcode'] : '';
//            if (!$common_barcode) {
//                return [
//                    'code' => $bad_request_code,
//                    'message' => 'missing or invalid common_barcode'
//                ];
//            }
        } elseif (0 == $product_version) {
            $price = isset($this->request->post['data']['price']) ? $this->request->post['data']['price'] : '';
            if (!$price || floatval($price) > 99999999999) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid price , max 99,999,999,999'
                ];
            }

            $promotion_price = isset($this->request->post['data']['promotion_price']) ? $this->request->post['data']['promotion_price'] : '';
            if (!$promotion_price || floatval($promotion_price) > 99999999999) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid promotion_price, max 99,999,999,999'
                ];
            }

            if (!isset($this->request->post['data']['stores_default_id'])) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing stores_default_id'
                ];
            }

            $original_inventory = isset($this->request->post['data']['original_inventory']) ? $this->request->post['data']['original_inventory'] : '';
            if (!$original_inventory || floatval($original_inventory) > 9999999) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid original_inventory'
                ];
            }

            $weight = isset($this->request->post['data']['weight']) ? $this->request->post['data']['weight'] : '';
            if (!$weight || floatval($weight) > 9999999) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid weight, max 9,999,999'
                ];
            }

            $sku = isset($this->request->post['data']['sku']) ? $this->request->post['data']['sku'] : '';
            if (!$sku) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid sku'
                ];
            } else {
                if ($this->model_catalog_product->searchProductAndVersionBySKU($this->request->post['data']['sku'], $product_id) == false) {
                    return [
                        'code' => $bad_request_code,
                        'message' => 'error sku exist'
                    ];
                }
            }
        }

        $sale_on_out_of_stock = isset($this->request->post['data']['sale_on_out_of_stock']) ? $this->request->post['data']['sale_on_out_of_stock'] : '';
        if (is_null($sale_on_out_of_stock) || $sale_on_out_of_stock === '' || !in_array($sale_on_out_of_stock, [0, 1])) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing or invalid sale_on_out_of_stock'
            ];
        }


        $images = isset($this->request->post['data']['images']) ? $this->request->post['data']['images'] : '';
        if (!$images || !is_array($images)) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing or invalid images'
            ];
        }

        foreach ($images as $img) {
            if (utf8_strlen($img) > 255) {
                return [
                    'code' => $bad_request_code,
                    'message' => 'missing or invalid images, each url max length 255'
                ];
            }
        }

        $image_alts = isset($this->request->post['data']['image-alts']) ? $this->request->post['data']['image-alts'] : '';
        if (!$image_alts || !is_array($image_alts)) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing or invalid image-alts'
            ];
        }

        $status = isset($this->request->post['data']['status']) ? $this->request->post['data']['status'] : '';
        if (!$status) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing or invalid status'
            ];
        }

        $channel = isset($this->request->post['data']['channel']) ? $this->request->post['data']['channel'] : '';
        if (!isset($channel)) {
            return [
                'code' => $bad_request_code,
                'message' => 'missing or invalid channel'
            ];
        }

        $categories = isset($this->request->post['data']['category']) ? $this->request->post['data']['category'] : '';
        if (!empty($categories) && !is_array($categories)) {
            return [
                'code' => $bad_request_code,
                'message' => 'invalid categories (expected: array)'
            ];
        }

        return [
            'code' => 200
        ];
    }

    private function beForeSave($data_to_save)
    {
        // category
        $data_to_save['category'] = [];
        $category_names = isset($this->request->post['data']['category']) ? $this->request->post['data']['category'] : [];
        $this->load->model('catalog/category');
        $categories = $this->model_catalog_category->getCategoriesByNames($category_names);
        $category_name_to_id = [];
        foreach ($categories as $cat) {
            if (!isset($cat['category_id']) || !isset($cat['name'])) {
                continue;
            }

            $category_name_to_id[mb_strtoupper($cat['name'])] = $cat['category_id'];
        }

        foreach ($category_names as $category_name) {
            if (array_key_exists(mb_strtoupper($category_name), $category_name_to_id)) {
                $data_to_save['category'][] = $category_name_to_id[mb_strtoupper($category_name)];
            } else {
                $data_to_save['category'][] = "add_new_{$category_name}";
            }
        }

        // manufacturer
        $data_to_save['manufacturer'] = '';
        $manufacturer_name = isset($this->request->post['data']['manufacturer']) ? $this->request->post['data']['manufacturer'] : '';
        $this->load->model('catalog/manufacturer');
        $manufacturer = $this->model_catalog_manufacturer->getManufacturerByName($filter = ['name' => $manufacturer_name]);
        if (isset($manufacturer['manufacturer_id'])) {
            $data_to_save['manufacturer'] = $manufacturer['manufacturer_id'];
        } else {
            $data_to_save['manufacturer'] = "add_new_{$manufacturer_name}";
        }

        // collection
        $data_to_save['collection_list'] = [];
        $collection_titles = isset($this->request->post['data']['collection_list']) ? $this->request->post['data']['collection_list'] : [];
        $this->load->model('catalog/collection');
        $collections = $this->model_catalog_collection->getCollectionsByTitles($collection_titles);
        $collection_title_to_id = [];
        foreach ($collections as $collection) {
            if (!isset($collection['collection_id']) || !isset($collection['title'])) {
                continue;
            }

            $collection_title_to_id[mb_strtoupper($collection['title'])] = $collection['collection_id'];
        }

        foreach ($collection_titles as $collection_title) {
            if (array_key_exists(mb_strtoupper($collection_title), $collection_title_to_id)) {
                $data_to_save['collection_list'][] = $collection_title_to_id[mb_strtoupper($collection_title)];
            } else {
                $data_to_save['collection_list'][] = "add_new_{$collection_title}";
            }
        }

        // tag
        $data_to_save['tag_list'] = [];
        $tag_values = isset($this->request->post['data']['tag_list']) ? $this->request->post['data']['tag_list'] : [];
        $this->load->model('catalog/tag');
        $tags = $this->model_catalog_tag->getTagsByValues($tag_values);
        $tag_value_to_id = [];
        foreach ($tags as $tag) {
            if (!isset($tag['tag_id']) || !isset($tag['value'])) {
                continue;
            }

            $tag_value_to_id[mb_strtoupper($tag['value'])] = $tag['tag_id'];
        }

        foreach ($tag_values as $tag_value) {
            if (array_key_exists(mb_strtoupper($tag_value), $tag_value_to_id)) {
                $data_to_save['tag_list'][] = $tag_value_to_id[mb_strtoupper($tag_value)];
            } else {
                $data_to_save['tag_list'][] = "add_new_{$tag_value}";
            }
        }

        return $data_to_save;
    }
}