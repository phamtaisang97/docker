<?php

class ControllerApiOpenApiBlogCategories extends Controller
{
    use Open_Api;

    const PERMISSION = "blog";
    const SCOPE_MODIFY = "modify";

    /**
     * @return bool
     */
    public function sync()
    {
        try {
            if ($this->request->server['REQUEST_METHOD'] !== 'POST') {
                $json = [
                    'message' => 'Bad request. Use POST only!'
                ];
                $this->responseJson($json, $this->STATUS_CODE['method_not_allowed']);
                return false;
            }

            $json = [
                'message' => 'Not permission'
            ];

            if (!$this->validate(__FUNCTION__, self::SCOPE_MODIFY)) {
                $this->responseJson($json, $this->STATUS_CODE['bad_request']);
                return false;
            }

            $this->request->post = json_decode(file_get_contents('php://input'), true);

            $validate_result = $this->validateInputData();
            if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
                $error = is_array($validate_result) ? $validate_result : [];
                $this->responseJson($error);
                return false;
            }

            $this->load->model('blog/blog');
            $data = $this->refactorInputData();

            // check if blog category is exists
            $blog_category = $this->model_blog_blog->getBlogCategoryByTitle($data['title']);
            if (empty($blog_category['blog_category_id'])) {
                // create
                $this->model_blog_blog->createBlogCategory($data);
            } else {
                // update
                $data['blog_category_id'] = $blog_category['blog_category_id'];
                $this->model_blog_blog->editBlogCategory($data);
            }

            $json = [
                'message' => 'Sync blog category success!',
            ];
            $this->responseJson($json);
            return true;
        } catch (Exception $e) {
            $this->responseJson(['message' => $e->getMessage()], $this->STATUS_CODE['internal_server_error']);
            return false;
        }
    }

    /**
     * @return bool
     */
    public function delete()
    {
        try {
            if ($this->request->server['REQUEST_METHOD'] !== 'DELETE') {
                $json = [
                    'message' => 'Bad request. Use DELETE only!'
                ];
                $this->responseJson($json, $this->STATUS_CODE['method_not_allowed']);
                return false;
            }

            $json = [
                'message' => 'Not permission'
            ];

            if (!$this->validate(__FUNCTION__, self::SCOPE_MODIFY)) {
                $this->responseJson($json, $this->STATUS_CODE['bad_request']);
                return false;
            }

            $this->request->post = json_decode(file_get_contents('php://input'), true);

            $validate_result = $this->validateDeleteInputData();
            if (!(isset($validate_result['code']) && 200 == $validate_result['code'])) {
                $error = is_array($validate_result) ? $validate_result : [];
                $this->responseJson($error);
                return false;
            }

            $this->load->model('blog/blog');
            $data = $this->request->post;

            // check if blog category is exists
            $blog_category = $this->model_blog_blog->getBlogCategoryByTitle($data['title']);
            if (empty($blog_category['blog_category_id'])) {
                $error = [
                    'code' => 506,
                    'message' => 'blog (with title) does not existed'
                ];

                $this->responseJson($error);
                return false;
            } else {
                // update
                $this->model_blog_blog->deleteCategory($blog_category['blog_category_id']);
            }

            $json = [
                'message' => 'Delete blog category success!',
            ];
            $this->responseJson($json);
            return true;
        } catch (Exception $e) {
            $this->responseJson(['message' => $e->getMessage()], $this->STATUS_CODE['internal_server_error']);
            return false;
        }
    }

    /**
     * @return array|int[]
     */
    private function validateInputData()
    {
        if (empty($this->request->post['title'])) {
            return [
                'code' => $this->STATUS_CODE['bad_request'],
                'message' => 'missing or invalid blog category title!'
            ];
        }

        if (!isset($this->request->post['meta_title'])) {
            return [
                'code' => $this->STATUS_CODE['bad_request'],
                'message' => 'missing blog category meta title!'
            ];
        }

        if (!isset($this->request->post['meta_description'])) {
            return [
                'code' => $this->STATUS_CODE['bad_request'],
                'message' => 'missing blog category meta description!'
            ];
        }
        
        if (empty($this->request->post['alias'])) {
            return [
                'code' => $this->STATUS_CODE['bad_request'],
                'message' => 'missing or invalid blog category alias!'
            ];
        }

        if (!isset($this->request->post['source'])) {
            return [
                'code' => $this->STATUS_CODE['bad_request'],
                'message' => 'missing or invalid blog category source!'
            ];
        }

        return [
            'code' => 200
        ];
    }

    /**
     * @return array|int[]
     */
    private function validateDeleteInputData()
    {
        if (empty($this->request->post['title'])) {
            return [
                'code' => $this->STATUS_CODE['bad_request'],
                'message' => 'missing or invalid blog category title!'
            ];
        }

        return [
            'code' => 200
        ];
    }

    /**
     * @return array
     */
    private function refactorInputData()
    {
        try {
            $blog_category = [];

            // init default values
            $blog_category['title'] = trim(isset($this->request->post['title'])) ? trim($this->request->post['title']) : '';
            $blog_category['meta_title'] = isset($this->request->post['meta_title']) ? $this->request->post['meta_title'] : '';
            $blog_category['meta_description'] = isset($this->request->post['meta_description']) ? $this->request->post['meta_description'] : '';
            $blog_category['alias'] = isset($this->request->post['alias']) ? $this->request->post['alias'] : '';
            $blog_category['source'] = isset($this->request->post['source']) ? $this->request->post['source'] : '';

            return $blog_category;
        } catch (Exception $exception) {
            return [];
        }
    }
}