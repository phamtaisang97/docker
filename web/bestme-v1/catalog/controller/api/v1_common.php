<?php

/**
 * base url: /api/v1_product/xxx
 * Class ControllerApiV1Product
 */
class Controllerapiv1common extends Controller
{
    private $error = array();

    public function logo()
    {
        $this->load->model('extension/module/theme_builder_config');
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;
        try {
            $header_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HEADER);
            $header_config = json_decode($header_config, true);
            $api_logo = isset($header_config['logo']['url']) ? cloudinary_change_http_to_https($header_config['logo']['url']) : '';

            if ($api_logo) {
                $json = [
                    'logo' => $api_logo,
                ];
            } else {
                $json = [
                    'code' => 204,
                    'message' => 'No Content',
                ];
            }
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function notify_bar()
    {
        $this->load->model('extension/module/theme_builder_config');
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;
        try {
            $header_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HEADER);
            $header_config = json_decode($header_config, true);
            $content = $header_config['notify-bar']['content'];
            $url = $header_config['notify-bar']['url'];

            if ($content) {
                $json = [
                    'content' => $content,
                    'url' => $url
                ];
            } else {
                $json = [
                    'code' => 204,
                    'message' => 'No Content',
                ];
            }
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function header_menu()
    {
        $this->load->model('extension/module/theme_builder_config');
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;
        try {
            if ($this->request->server['HTTPS']) {
                $server = $this->config->get('config_ssl');
            } else {
                $server = $this->config->get('config_url');
            }
            $menu_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HEADER);
            $menu_config = json_decode($menu_config, true);

            $mainMenuId = isset($menu_config['menu']['display-list']['id']) ? $menu_config['menu']['display-list']['id'] : -1;
            $this->load->model('custom/menu');
            $mainMenu = $this->model_custom_menu->getMenuById($mainMenuId);

            if ($mainMenu) {
                $result = [];
                foreach ($mainMenu as $key => $vl) {
                    $result[] = [
                        "text" => $vl['name'],
                        "url" => $server . $vl['url'],
                        "items" => $this->getMenuByItems($vl['menu_id'])
                    ];
                }

                $json = [
                    'result' => $result
                ];
            } else {
                $json = [
                    'code' => 204,
                    'message' => 'No Content',
                ];
            }
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function getMenuByItems($items)
    {
        $this->load->model('custom/menu');
        $mainMenu = $this->model_custom_menu->getMenuById($items);
        $records = [];
        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }
        if (!empty($mainMenu)) {
            foreach ($mainMenu as $key => $vl) {
                $records[] = [
                    "text" => $vl['name'],
                    "url" => $server . $vl['url'],
                    "items" => $this->getMenuByItems($vl['menu_id'])
                ];
            }
            return $records;
        }
        return '';
    }

    public function slide_show()
    {
        $this->load->model('extension/module/theme_builder_config');
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;
        $slides_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_SLIDESHOW);
        $slides_config = json_decode($slides_config, true);
        try {
            if ($slides_config) {
                $result = [
                    "visible" => $slides_config['visible'],
                    "transition_time" => $slides_config['setting']['transition-time'],
                    "silder" => $this->getImageSlideShow()
                ];

                $json = [
                    'result' => $result
                ];
            } else {
                $json = [
                    'code' => 204,
                    'message' => 'No Content',
                ];
            }
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function getImageSlideShow()
    {
        $this->load->model('extension/module/theme_builder_config');
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;
        $slides_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_SLIDESHOW);
        $slides_config = json_decode($slides_config, true);
        $slides_config['display'] = isset($slides_config['display']) && is_array($slides_config['display']) ? $slides_config['display'] : [];

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        $result = [];
        foreach ($slides_config['display'] as $key => $slide) {
            if (isset($slide['image-url'])) {
                if (empty($slide['image-url'])) {
                    $image_url = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
                } else {
                    $image_url = cloudinary_change_http_to_https($slide['image-url']);
                }
                $result[] = [
                    "image-url" => $image_url,
                    "url" => $server . (isset($slide['url']) ? $slide['url'] : ''),
                    "description" => $slide['description']
                ];
            }
        }

        return $result;
    }

    public function contact()
    {
        $this->load->model('extension/module/theme_builder_config');
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;
        $footer_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_FOOTER);
        $footer_config = json_decode($footer_config, true);
        try {
            if ($footer_config) {
                $result = [
                    "address" => $footer_config['contact']['address'],
                    "phone" => $footer_config['contact']['phone-number'],
                    "email" => $footer_config['contact']['email']
                ];

                $json = [
                    'result' => $result
                ];
            } else {
                $json = [
                    'code' => 204,
                    'message' => 'No Content',
                ];
            }
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function built_in_pages()
    {
        $page = array(
            'trang-chu' => 'Trang chủ',
            'san-pham' => 'Sản phẩm',
            'lien-he' => 'Liên hệ',
            'tai-khoan' => 'Tài khoản',
            'dang-nhap' => 'Đăng nhập',
            'dang-ky' => 'Đăng ký',
            'dang-xuat' => 'Đăng xuất',
            'cai-dat' => 'Cài đặt',
            'gio-hang' => 'Giỏ hàng',
            'thanh-toan' => 'Thanh toán',
        );

        try {
            if ($this->request->server['HTTPS']) {
                $server = $this->config->get('config_ssl');
            } else {
                $server = $this->config->get('config_url');
            }

            $result = [];
            foreach ($page as $key_vi => $vl_vi) {
                $result[] = [
                    "title" => $vl_vi,
                    "url" => $server . $key_vi
                ];
            }
            $json = [
                'result' => $result
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function content_pages()
    {
        $this->load->model('online_store/contents');

        try {
            $content = $this->model_online_store_contents->getContents();
            if ($content) {
                $result = [];
                foreach ($content as $key => $vl) {
                    $result[] = [
                        "page_id" => $vl['page_id'],
                        "title" => $vl['title'],
                        "description" => $vl['description'],
                        "created_at" => $vl['date_added'],
                        "url" => $this->url->link('contents/page', 'page_id=' . $vl['page_id'])
                    ];
                }

                $json = [
                    'result' => $result
                ];
            } else {
                $json = [
                    'code' => 204,
                    'message' => 'No Content',
                ];
            }
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function address_map()
    {
        $this->load->model('extension/module/theme_builder_config');
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;
        $contact_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CONTACT_SECTIONS);  //       CONFIG_KEY_SECTION_CONTACT_SECTIONS
        $contact_config = json_decode($contact_config, true);
        try {
            if ($contact_config['section']['map']['address']) {
                $result = [
                    "address" => (!empty($contact_config['section']['map']['address']) ? $contact_config['section']['map']['address'] : '')
                ];

                $json = [
                    'result' => $result
                ];
            } else {
                $json = [
                    'code' => 204,
                    'message' => 'No Content',
                ];
            }
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function home_banner()
    {
        $this->load->model('extension/module/theme_builder_config');
        $banners = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_BANNER);
        $banners = json_decode($banners, true);

        try {
            if ($banners['visible'] == 1) {
                $result = [];
                $data = [];
                if (isset($banners['display']) && is_array($banners['display'])) {
                    // named banner
                    list($data['banner_highlight'], $data['banner_left'], $data['banner_middle']) = $banners['display'];

                    // more banners
                    if (count($banners['display'] > 3)) {
                        $data = array_merge($data, array_slice($banners['display'], 3));
                    }

                    $result = $data;
                }

                $json = [
                    'result' => $result
                ];
            } else {
                $json = [
                    'code' => 204,
                    'message' => 'No Content',
                ];
            }
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function partners()
    {
        $this->load->model('extension/module/theme_builder_config');
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;
        $partners_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_PARTNER);
        $partners_config = json_decode($partners_config, true);
        try {
            if ($partners_config['visible'] == 1) {
                $partners_config['display'] = isset($partners_config['display']) && is_array($partners_config['display']) ? $partners_config['display'] : [];
                foreach ($partners_config['display'] as $key => $vl) {
                    $result[] = [
                        "image-url" => $vl['image-url'],
                        "url" => $vl['url'],
                        "description" => $vl['description'],
                        "visible" => $vl['visible']
                    ];
                }

                $json = [
                    'result' => $result
                ];
            } else {
                $json = [
                    'code' => 204,
                    'message' => 'No Content',
                ];
            }
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }


    public function provinces()
    {
        $this->load->model('localisation/vietnam_administrative');
        $provinces = $this->model_localisation_vietnam_administrative->getProvinces();
        if ($provinces) {
            $json = [
                'result' => $provinces
            ];
        } else {
            $json = [
                'code' => 204,
                'message' => 'No Content',
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function districts()
    {
        $this->load->model('localisation/vietnam_administrative');
        $provinceCode = isset($this->request->get['province_code']) ? $this->request->get['province_code'] : '';
        $districts = $this->model_localisation_vietnam_administrative->getDistrictsByProvinceCode($provinceCode);
        if ($districts) {
            $json = [
                'result' => $districts
            ];
        } else {
            $json = [
                'code' => 204,
                'message' => 'No Content',
            ];
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function wards()
    {
        $this->load->model('localisation/vietnam_administrative');
        $districtCode = isset($this->request->get['district_code']) ? $this->request->get['district_code'] : '';
        $wards = $this->model_localisation_vietnam_administrative->getWardsByDistrictCode($districtCode);
        if ($wards) {
            $json = [
                'result' => $wards
            ];
        } else {
            $json = [
                'code' => 204,
                'message' => 'No Content',
            ];
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getTransport()
    {
        $this->load->model('setting/setting');
        $delivery_methods = $this->model_setting_setting->getSettingValue('delivery_methods');
        $delivery_methods = json_decode($delivery_methods, true);

        try {
            $delivery_methods = is_array($delivery_methods) ? $delivery_methods : [];
            if ($delivery_methods) {
                foreach ($delivery_methods as $key => $vl) {
                    $result[] = [
                        "id" => $vl['id'],
                        "name" => $vl['name'],
                        "status" => $vl['status']
                    ];
                }

                $json = [
                    'result' => $result
                ];
            } else {
                $json = [
                    'code' => 204,
                    'message' => 'No Content',
                ];
            }
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function transportFee()
    {
        $this->load->model('localisation/vietnam_administrative');
        $this->load->model('sale/order');
        $this->load->model('setting/setting');
        $this->load->language('sale/order');

        try {
            if (!isset($this->request->get['transport']) && empty($this->request->get['transport'])) {
                $json = [
                    'code' => 204,
                    'message' => 'No Content',
                ];
            } elseif (!isset($this->request->get['provinces']) && empty($this->request->get['provinces'])) {
                $json = [
                    'code' => 204,
                    'message' => 'No Content',
                ];
            } elseif (!isset($this->request->get['total_weight']) && empty($this->request->get['total_weight'])) {
                $json = [
                    'code' => 204,
                    'message' => 'No Content',
                ];
            } elseif (!isset($this->request->get['total_money']) && empty($this->request->get['total_money'])) {
                $json = [
                    'code' => 204,
                    'message' => 'No Content',
                ];
            } else {
                $transport = $this->request->get['transport'];
                $total_weight = $this->request->get['total_weight'];
                $total_money = $this->request->get['total_money'];
                $cityArr = $this->model_localisation_vietnam_administrative->getProvinceByCode($this->request->get['provinces']);
                $city = '';
                if (isset($cityArr['name'])) {
                    $city = $cityArr['name'];
                }

                $delivery_methods = $this->model_setting_setting->getSettingValue('delivery_methods');
                $delivery_methods = json_decode($delivery_methods, true);

                $delivery_methods = is_array($delivery_methods) ? $delivery_methods : [];
                $value_fee_cod = 0;
                $fee_price = 0;
                $name_transport = '';

                foreach ($delivery_methods as $key => $vl) {
                    $default_fee_region = isset($vl['fee_region']) ? $vl['fee_region'] : 0;

                    if ($transport == $vl['id']) {
                        $name_transport = $vl['name'];
                        $found_region = false;
                        foreach ($vl['fee_regions'] as $value) {
                            if (in_array(trim($city), $value['provinces'])) {
                                foreach ($value['weight_steps'] as $vl_weight) {
                                    if (convertWeight($vl_weight['from']) <= $total_weight && convertWeight($vl_weight['to']) >= $total_weight) {
                                        $fee_price = str_replace(',', '', $vl_weight['price']); // Gia thuoc tinh thanh
                                        $found_region = true;
                                        break;
                                    } else {
                                        $fee_price = str_replace(',', '', $default_fee_region);  // Gia mac dinh khi khong thuoc tinh thanh
                                    }
                                }
                            } else {
                                $fee_price = str_replace(',', '', $default_fee_region);  // Gia mac dinh khi khong thuoc tinh thanh
                            }

                            if ($found_region) {
                                break;
                            }
                        }
                        foreach ($vl['fee_cod'] as $key_cod => $fee_cod) {
                            $arr_fee_status = ($vl['fee_cod'][$key_cod]);
                            if ($arr_fee_status['status'] == 1) {
                                $arr_fee_status = str_replace(',', '', $arr_fee_status['value']);
                                if ($key_cod === 'dynamic') {
                                    if ($total_money != 0) {
                                        $value_fee_cod = ($arr_fee_status / 100) * $total_money;
                                    } else {
                                        $value_fee_cod = 0;
                                    }
                                } elseif ($key_cod === 'fixed') {
                                    $value_fee_cod = $arr_fee_status;
                                }
                            }
                        }
                    }
                }

                $json['fee'] = [
                    'name' => $name_transport,
                    'fee' => $fee_price + $value_fee_cod
                ];
            }

        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function custom_contents()
    {
        $data = [];
        $this->load->model('extension/module/theme_builder_config');
        $section_section_configs = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_SECTIONS);
        $section_section_configs = json_decode($section_section_configs, true);
        if (isset($section_section_configs['section']['content_customize']['visible']) && $section_section_configs['section']['content_customize']['visible'] != 1) {
            $data['display_customizes'] = [];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($data));

            return;
        }

        $content_customize_configs = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CONTENT_CUSTOMIZE);
        $content_customize_config = json_decode($content_customize_configs, true);

        $data['display_customizes'] = isset($content_customize_config['display']) ? $content_customize_config['display'] : [];

        foreach ($data['display_customizes'] as &$customize) {
            if (!$customize['content']['icon']) {
                $customize['content']['icon'] = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
            }
        }
        unset($customize);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($data));
    }

    /**
     * get blogs list
     */
    public function blogs()
    {
        try {
            $data_filter = [
                'start' => null,
                'limit' => null,
                'sort' => $this->getValueFromRequest('get', 'sort'),
                'order' => $this->getValueFromRequest('get', 'order'),
            ];

            /* get query params */
            if (isset($this->request->get['page'])) {
                $page = $this->request->get['page'];

                if (isset($this->request->get['limit'])) {
                    $limit = $this->request->get['limit'];
                } else {
                    $limit = null;
                }

                $data_filter['start'] = ($page - 1) * (int)$limit;
                $data_filter['limit'] = $limit;
            }

            /* get blogs */
            $blogs = $this->getBlogsList($data_filter);
            if ($blogs) {
                $json = [
                    'result' => $blogs
                ];
            } else {
                $json = [
                    'code' => 204,
                    'message' => 'No Content',
                ];
            }
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * get blog detail
     */
    public function blog_detail()
    {
        if (!isset($this->request->get['blog_id'])) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));

            return;
        }

        try {
            $data_filter = [
                'blog_id' => $this->request->get['blog_id'],
                'start' => 0,
                'limit' => 1
            ];

            /* get blogs */
            $blogs = $this->getBlogsList($data_filter);
            if (is_array($blogs) && count($blogs) > 0) {
                $json = [
                    'result' => $blogs[0]
                ];
            } else {
                $json = [
                    'code' => 204,
                    'message' => 'No Content',
                ];
            }
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function blog_first()
    {
        try {
            $data_filter = [
                'start' => 0,
                'limit' => 1,
                'sort' => 'b.blog_id',
                'order' => 'ASC',
            ];

            /* get blogs */
            $blogs = $this->getBlogsList($data_filter);
            if (is_array($blogs) && count($blogs) > 0) {
                $json = [
                    'result' => $blogs[0]
                ];
            } else {
                $json = [
                    'code' => 204,
                    'message' => 'No Content',
                ];
            }
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * header config
     */
    public function header()
    {
        try {
            $data = [];

            /* get header config */
            $this->load->model('extension/module/theme_builder_config');
            /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
            $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;

            $header_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HEADER);
            $header_config = json_decode($header_config, true);

            /* display notify bar */
            $data['notify_bar']['visible'] = $header_config['notify-bar']['visible'];
            $data['notify_bar']['content'] = $header_config['notify-bar']['content'];
            $data['notify_bar']['url'] = $header_config['notify-bar']['url'];

            /* display logo */
            $data['logo'] = isset($header_config['logo']['url']) ? cloudinary_change_http_to_https($header_config['logo']['url']) : 'image/no_image.png';
            $data['logo_height'] = isset($header_config['logo']['height']) ? $header_config['logo']['height'] : 50;

            $this->load->model('custom/common');

            /* display main menu */
            $menu_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HEADER);
            $menu_config = json_decode($menu_config, true);

            $mainMenuId = isset($menu_config['menu']['display-list']['id']) ? $menu_config['menu']['display-list']['id'] : -1;
            $this->load->model('custom/menu');
            $mainMenu = $this->model_custom_menu->getMenuById($mainMenuId);

            // default if empty
            $mainMenu = !empty($mainMenu) ? $mainMenu : [
                [
                    'text' => 'Trang chủ',
                    'url' => $this->url->link('common/home', '', true)
                ],
            ];
            $data['main_menu'] = $mainMenu;

            /* categories on search area */
            $list_product_categories = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_LIST_PRODUCT);
            $list_product_categories = json_decode($list_product_categories, true);
            $data['all_categories_search'] = $list_product_categories;

            // Active menu item
            $arr_mainMenu = array();
            foreach ($mainMenu as $vl_mainMenu) {
                $arr_mainMenu[] = $vl_mainMenu['url'];
            }

            $url_referer = $_SERVER['QUERY_STRING'];
            $input = strtok($url_referer, '&');
            $input = $input_suffix = str_replace("_route_=", "", $input);
            $input = $this->url->link('common/home', '', true) . $input; // $input = $this->url->link('common/home', '', true) = (example) http://x2.bestme.test/
            $result_active_menu = '';
            foreach ($arr_mainMenu as $item) {
                if (!empty($item) && is_string($item) && (strpos($input, $item) === 0 || strpos($input_suffix, $item) === 0)) { // input_suffix support for menu type url
                    $result_active_menu = $item;
                    //break; // NOT break here in case page /san-pham with filter ...
                }
            }
            $data['result_active_menu'] = $result_active_menu != '' ? $result_active_menu : $this->url->link('common/home', '', true);

            $json = [
                'result' => $data
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * footer config
     */
    public function footer()
    {
        try {
            $this->load->model('tool/image');
            $this->load->model('catalog/collection');
            $this->load->model('extension/module/theme_builder_config');
            $this->load->model('custom/menu');

            $data = [
                'visible_contact' => 1,
                'visible_contact_title' => '',
                'contact' => [],
                'visible_collection' => 1,
                'visible_collection_title' => '',
                'collections' => [],
                'visible_quick_links' => 1,
                'visible_quick_links_title' => '',
                'quick_links' => [],
                'visible_subscribe' => 1,
                'visible_subscribe_title' => '',
                'visible_social_network' => 1,
                'visible_subscribe_social_network' => 1,
                'social_config' => [],
            ];

            /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
            $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;

            /* get footer config */
            $footer_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_FOOTER);
            $footer_config = json_decode($footer_config, true);

            // contact
            $data['visible_contact'] = $footer_config['contact']['visible'];
            $data['visible_contact_title'] = $footer_config['contact']['title'];
            if (isset($data['visible_contact']) && $data['visible_contact'] == 1) {
                $data['contact'] = array(
                    'title' => $footer_config['contact']['title'],
                    'address' => $footer_config['contact']['address'],
                    'phone' => $footer_config['contact']['phone-number'],
                    'email' => $footer_config['contact']['email'],
                    'visible' => $footer_config['contact']['visible'],
                );
            }

            // collection
            $data['visible_collection'] = $footer_config['collection']['visible'];
            $data['visible_collection_title'] = $footer_config['collection']['title'];
            if (isset($data['visible_collection']) && $data['visible_collection'] == 1) {
                $childMenu = isset($footer_config['collection']['menu_id']) ? $this->model_custom_menu->getMenuById($footer_config['collection']['menu_id']) : [];

                foreach ($childMenu as $child_menu) {
                    $data['collections'][] = array(
                        'name' => $child_menu['text'],
                        'url_link' => $child_menu['url'],
                    );
                }
            }

            // quick links
            $data['visible_quick_links'] = $footer_config['quick-links']['visible'];
            $data['visible_quick_links_title'] = $footer_config['quick-links']['title'];
            if (isset($data['visible_quick_links']) && $data['visible_quick_links'] == 1) {
                $childMenu = $this->model_custom_menu->getMenuById($footer_config['quick-links']['menu_id']);
                foreach ($childMenu as $child_menu) {
                    $data['quick_links'][] = array(
                        'name' => $child_menu['text'],
                        'url_link' => $child_menu['url'],
                    );
                }
            }

            // subscribe
            $data['visible_subscribe'] = $footer_config['subscribe']['visible'];
            $data['visible_subscribe_title'] = $footer_config['subscribe']['title'];
            $data['visible_social_network'] = $footer_config['subscribe']['social_network'];
            $data['visible_subscribe_social_network'] = $footer_config['subscribe']['social_network'];

            if (isset($data['visible_subscribe_social_network']) && $data['visible_subscribe_social_network'] == 1) {
                $social_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_SOCIAL);
                $social_display_in_footer = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_FOOTER);
                $data['social_config'] = json_decode($social_config, true);
                $data['social_display'] = json_decode($social_display_in_footer, true);
                $data['social_display'] = isset($data['social_display']['subscribe']) ? $data['social_display']['subscribe'] : [];
                foreach ($data['social_config'] as $social) {
                    $social_name_in_footer = $social['name'] . '_visible';
                    // important: check !isset for $social_name_in_footer because social visible may be not existing
                    // this for working without migration old social config in footer
                    if (!isset($data['social_display'][$social_name_in_footer]) || $data['social_display'][$social_name_in_footer] == 1) {
                        $data['social_configs'][] = array(
                            'name' => (!empty($social['name']) ? $social['text'] : ''),
                            'url' => (!empty($social['name']) ? $social['url'] : ''),
                        );
                    }
                }
            }

            $json = [
                'result' => $data
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * social
     */
    public function social()
    {
        try {
            $this->load->model('extension/module/theme_builder_config');

            $data = [
                //'visible_social_network' => 1,
                //'visible_subscribe_social_network' => 1,
                'social_display' => [],
                'social_configs' => [],
            ];

            /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
            $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;

            /* get footer config */
            $footer_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_FOOTER);
            $footer_config = json_decode($footer_config, true);

            // subscribe
            //$data['visible_social_network'] = $footer_config['subscribe']['social_network'];
            $visible_subscribe_social_network = $footer_config['subscribe']['social_network'];
            //$data['visible_subscribe_social_network'] = $visible_subscribe_social_network;

            if (isset($visible_subscribe_social_network) && $visible_subscribe_social_network == 1) {
                $social_config = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_SOCIAL);
                $social_config = json_decode($social_config, true);

                $social_display_in_footer = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_FOOTER);
                $social_display = json_decode($social_display_in_footer, true);

                $data['social_display'] = isset($social_display['subscribe']) ? $social_display['subscribe'] : [];
                unset($data['social_display']['title'], $data['social_display']['social_network']);

                foreach ($social_config as $social) {
                    $social_name_in_footer = $social['name'] . '_visible';
                    // important: check !isset for $social_name_in_footer because social visible may be not existing
                    // this for working without migration old social config in footer
                    $visible = (!isset($data['social_display'][$social_name_in_footer]) || $data['social_display'][$social_name_in_footer] == 1);
                    $data['social_configs'][] = [
                        'name' => (!empty($social['name']) ? $social['text'] : ''),
                        'url' => (!empty($social['name']) ? $social['url'] : ''),
                        'visible' => $visible ? '1' : '0',
                    ];
                }
            }

            $json = [
                'result' => $data
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * favicon
     */
    public function favicon()
    {
        try {
            $data = [];

            /* get favicon config */
            $this->load->model('extension/module/theme_builder_config');
            /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
            $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;
            $favicon_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_FAVICON);
            $favicon_config = json_decode($favicon_config, true);

            $data['favicon'] = isset($favicon_config['image']['url']) ? cloudinary_change_http_to_https($favicon_config['image']['url']) : '';

            $json = [
                'result' => $data
            ];
        } catch (Exception $e) {
            $json = [
                'code' => 404,
                'message' => 'Bad Request'
            ];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * api login
     */
    public function login() {
        $json = [
            'code' => 200,
            'status' => true,
            'message' => 'Success'
        ];

        $this->load->model('account/customer');
        if ($this->validateLogin()) {
            // Unset guest
            unset($this->session->data['guest']);

            // Default Shipping Address
            $this->load->model('account/address');

            if ($this->config->get('config_tax_customer') == 'payment') {
                $this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
            }

            if ($this->config->get('config_tax_customer') == 'shipping') {
                $this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
            }
            //pass session after login
            $key_for_current_order = "current_order_". $this->session->getId() . "_" . 0;
            if (isset($this->session->data[$key_for_current_order])) {
                $data_current_order_none_user = $this->session->data[$key_for_current_order];
                unset($this->session->data[$key_for_current_order]);
                $key_for_current_order = "current_order_". $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());
                if (!isset($this->session->data[$key_for_current_order])) {
                    $this->session->data[$key_for_current_order] = [];
                }
                $this->session->data[$key_for_current_order] = $this->mergeCartInfo(array_merge($this->session->data[$key_for_current_order], $data_current_order_none_user));
            }

            // Wishlist
            if (isset($this->session->data['wishlist']) && is_array($this->session->data['wishlist'])) {
                $this->load->model('account/wishlist');

                foreach ($this->session->data['wishlist'] as $key => $product_id) {
                    $this->model_account_wishlist->addWishlist($product_id);

                    unset($this->session->data['wishlist'][$key]);
                }
            }
            // save password
            $data_post = $this->request->post;
            if (isset($data_post['remember_password'])) {
                setcookie ("email",$data_post['email'],time()+ 3600);
                setcookie ("password",$data_post['password'],time()+ 3600);
            }
        } else {
            $json = [
                'code' => 400,
                'status' => false,
                'message' => $this->error['warning']
            ];
        }

        $this->responseJson($json);
    }

    /**
     * api register
     */
    public function register() {
        $json = [
            'code' => 200,
            'status' => true,
            'message' => 'Success'
        ];

        $this->load->model('account/customer');
        if ($this->validateRegister()) {
            $this->model_account_customer->addCustomer($this->request->post);

            // Clear any previous login attempts for unregistered accounts.
            $this->model_account_customer->deleteLoginAttempts(trim($this->request->post['email']));

            $this->customer->login(trim($this->request->post['email']), $this->request->post['password']);

            unset($this->session->data['guest']);

            //pass session after login
            $key_for_current_order = "current_order_". $this->session->getId() . "_" . 0;
            if (isset($this->session->data[$key_for_current_order])) {
                $data_current_order_none_user = $this->session->data[$key_for_current_order];
                unset($this->session->data[$key_for_current_order]);
                $key_for_current_order = "current_order_". $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());
                if (!isset($this->session->data[$key_for_current_order])) {
                    $this->session->data[$key_for_current_order] = [];
                }
                $this->session->data[$key_for_current_order] = array_merge($this->session->data[$key_for_current_order], $data_current_order_none_user);
            }

            $json['redirect_url'] = $this->url->link('account/success');
        } else {
            $json = [
                'code' => 400,
                'status' => false,
                'message' => $this->error['warning']
            ];
        }

        $this->responseJson($json);
    }

    /**
     * api forgot password
     */
    public function forgotPassword() {
        $json = [
            'code' => 200,
            'status' => true,
            'message' => 'Success'
        ];

        $this->load->model('account/customer');
        $this->load->language('account/forgotten');
        if ($this->validateForgotPassword()) {
            $this->model_account_customer->editCode($this->request->post['email'], token(40), HTTPS_SERVER);

            $json['success'] = $this->language->get('text_success');
            $json['redirect_url'] = $this->url->link('account/forgotten/success', 'email=' . $this->request->post['email'], true);
        } else {
            $json = [
                'code' => 400,
                'status' => false,
                'message' => $this->error['warning']
            ];
        }

        $this->responseJson($json);
    }

    /* == private functions == */
    private function validateLogin()
    {
        $this->load->language('account/login');
        // Check how many login attempts have been made.
        $login_info = $this->model_account_customer->getLoginAttempts($this->request->post['email']);

        if ($login_info && ($login_info['total'] >= $this->config->get('config_login_attempts')) && strtotime('-1 hour') < strtotime($login_info['date_modified'])) {
            $this->error['warning'] = $this->language->get('error_attempts');
        }

        // Check if customer has been approved.
        $customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

        if ($customer_info && !$customer_info['status']) {
            $this->error['warning'] = $this->language->get('error_approved');
        }

        if (!$this->error) {
            if (!$this->customer->login($this->request->post['email'], $this->request->post['password'])) {
                $this->error['warning'] = $this->language->get('error_login');

                $this->model_account_customer->addLoginAttempt($this->request->post['email']);
            } else {
                $this->model_account_customer->deleteLoginAttempts($this->request->post['email']);
            }
        }

        return !$this->error;
    }

    private function mergeCartInfo($cart_data)
    {
        $merged_cart_data = [];
        if (!is_array($cart_data)) {
            return $merged_cart_data;
        }

        foreach ($cart_data as $cart_item) {
            if (!isset($cart_item['product_id']) || !isset($cart_item['quantity'])) {
                continue;
            }

            $product_version_id = isset($cart_item['product_version']['product_version_id']) ? $cart_item['product_version']['product_version_id'] : '';
            $product_and_version = sprintf('%s-%s', $cart_item['product_id'], $product_version_id);
            if (!array_key_exists($product_and_version, $merged_cart_data)) {
                $merged_cart_data[$product_and_version] = $cart_item;
                continue;
            }

            // merge quantity of same product id
            $merged_cart_data[$product_and_version]['quantity'] += $cart_item['quantity'];
        }
        return array_values($merged_cart_data);
    }

    private function validateRegister()
    {
        $this->load->language('account/register');
        if (!isset($this->request->post['fullname'])) {
            if (!isset($this->request->post['firstname']) || (utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
                $this->error['firstname'] = $this->language->get('error_firstname');
            }

            if (!isset($this->request->post['lastname']) || (utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
                $this->error['lastname'] = $this->language->get('error_lastname');
            }
        } else {
            if ((utf8_strlen(trim($this->request->post['fullname'])) < 1) || (utf8_strlen(trim($this->request->post['fullname'])) > 64)) {
                $this->error['fullname'] = $this->language->get('error_fullname');
            }
        }

        if(isset($this->request->post['email_register'])){
            if (!isset($this->request->post['email_register']) || (utf8_strlen(trim($this->request->post['email_register'])) > 96) || !filter_var(trim($this->request->post['email_register']), FILTER_VALIDATE_EMAIL)) {
                $this->error['email_register'] = $this->language->get('error_email');
            }
            if ($this->model_account_customer->getTotalCustomersByEmail(trim($this->request->post['email_register']))) {
                $this->error['warning'] = $this->language->get('error_exists');
            }
        }
        if(isset($this->request->post['email'])) {
            if (!isset($this->request->post['email']) || (utf8_strlen(trim($this->request->post['email'])) > 96) || !filter_var(trim($this->request->post['email']), FILTER_VALIDATE_EMAIL)) {
                $this->error['email'] = $this->language->get('error_email');
            }
            if ($this->model_account_customer->getTotalCustomersByEmail(trim($this->request->post['email']))) {
                $this->error['warning'] = $this->language->get('error_exists');
            }
        }

        // Customer Group
        if (isset($this->request->post['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->post['customer_group_id'], $this->config->get('config_customer_group_display'))) {
            $customer_group_id = $this->request->post['customer_group_id'];
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        // Custom field validation
        $this->load->model('account/custom_field');

        $custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

        foreach ($custom_fields as $custom_field) {
            if ($custom_field['location'] == 'account') {
                if ($custom_field['required'] && empty($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']])) {
                    $this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
                } elseif (($custom_field['type'] == 'text') && !empty($custom_field['validation']) && !filter_var($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']], FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => $custom_field['validation'])))) {
                    $this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
                }
            }
        }

        if (!isset($this->request->post['password']) || (utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) < 4) || (utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) > 40)) {
            $this->error['password'] = $this->language->get('error_password');
        }

        // confirm password
        if (isset($this->request->post['confirm_password']) && $this->request->post['confirm_password'] != $this->request->post['password']) {
            $this->error['confirm_password'] = 'Mật khẩu và mật khẩu nhập lại không giống nhau.';
        }

        //
        if(isset($this->request->post['policies']) && $this->request->post['policies'] != 1) {
            $this->error['policies'] = 'Bạn chưa đồng ý với điền khoản.';
        }

        // Captcha
        if ($this->config->get('captcha_' . $this->config->get('config_captcha') . '_status') && in_array('register', (array)$this->config->get('config_captcha_page'))) {
            $captcha = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha') . '/validate');

            if ($captcha) {
                $this->error['captcha'] = $captcha;
            }
        }

        // Agree to terms
        $this->request->post['agree'] = true;   // temp: default = true
        if ($this->config->get('config_account_id')) {
            $this->load->model('catalog/information');

            $information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

            if ($information_info && !isset($this->request->post['agree'])) {
                $this->error['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
            }
        }

        return !$this->error;
    }

    private function validateForgotPassword() {
        if (!isset($this->request->post['email'])) {
            $this->error['warning'] = $this->language->get('error_email');
        } elseif (!$this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
            $this->error['warning'] = $this->language->get('error_email');
        }

        // Check if customer has been approved.
        $customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

        if ($customer_info && !$customer_info['status']) {
            $this->error['warning'] = $this->language->get('error_approved');
        }

        return !$this->error;
    }

    /**
     * @param array $data_filter
     * @return array
     */
    private function getBlogsList(array $data_filter)
    {
        $this->load->model('blog/blog');
        $this->load->model('tool/image');

        /* get blogs */
        $blogs = $this->model_blog_blog->getBlogList($data_filter);

        /* build result */
        $result = [];
        if (!$blogs) {
            return [];
        }

        foreach ($blogs as $blog) {
            // correct some data
            if (!$blog['image']) {
                $blog['image'] = $this->model_tool_image->resize(
                    'placeholder.png',
                    $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'),
                    $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height')
                );
            }

            $blog['link'] = $this->url->link("blog/blog/detail", 'blog_id=' . $blog['blog_id']);
            $blog['blog_categories'] = $this->convertCategory($this->model_blog_blog->getCategoriesByBlog($blog['blog_id']));
            $blog['blog_tags'] = $this->convertTags($this->model_blog_blog->getTagsByBlog($blog['blog_id']));

            // add to list
            $result[] = [
                'blog_id' => $blog['blog_id'],
                'author' => $blog['author'],
                'date_added' => $blog['date_added'],
                'date_modified' => $blog['date_modified'],
                'title' => $blog['title'],
                'content' => $blog['content'],
                'short_content' => $blog['short_content'],
                'image' => $blog['image'],
                'meta_title' => $blog['meta_title'],
                'meta_description' => $blog['meta_description'],
                'alias' => $blog['alias'],
                'blog_category_title' => $blog['blog_category_title'],
                'blog_category_id' => $blog['blog_category_id'],
                'firstname' => $blog['firstname'],
                'lastname' => $blog['lastname'],
                'link' => $blog['link'],
                'blog_categories' => $blog['blog_categories'],
                'blog_tags' => $blog['blog_tags'],
            ];
        }

        return $result;
    }

    /**
     * @param $categories
     * @return mixed
     */
    private function convertCategory($categories)
    {
        foreach ($categories as &$category) {
            $category['link'] = $this->url->link("blog/blog", 'blog_category_id=' . $category['blog_category_id']);
        }
        unset($category);
        return $categories;
    }

    /**
     * @param $tags
     * @return mixed
     */
    private function convertTags($tags)
    {
        foreach ($tags as &$tag) {
            $tag['link'] = $this->url->link("blog/blog", 'tag_id=' . $tag['tag_id']);
        }
        unset($tag);
        return $tags;
    }
}