<?php

class ControllerApiTransport extends Controller
{
    /**
     * handle s2sCallback
     */
    public function s2sCallback()
    {
        $return_data = [
            'code' => '400',
            'message' => 'shop client error'
        ];

        try {
            // Get transport method and call paymentS2SCallback return order_info
            $webhook_data = $this->request->post;

            // check if order exists and manual update status = 0
            $order_code = getValueByKey($webhook_data, 'bestme_order_code');
            $this->load->model('sale/order');
            $order = $this->model_sale_order->getOrderByOrderCode($order_code);

            if (isset($order['manual_update_status']) && 0 == $order['manual_update_status']) {
                // get order status from webhook delivery order status
                $transport_app_code = getValueByKey($webhook_data, 'transport_method');
                $transport_app = new Transport($transport_app_code, [], $this);
                if ($transport_app->getAdaptor() instanceof \Transport\Abstract_Transport) {
                    $parsed_data = $transport_app->parseWebhookResponse($webhook_data);

                    if (!empty($parsed_data) && !is_null($parsed_data['order_status_id'])) {
                        // check if transport order exists
                        $transport_order_code_exists = $this->model_sale_order->checkOrderExistsByTransportOrderCode($parsed_data['transport_order_code']);
                        if ($transport_order_code_exists) {
                            // Update order follow order_info
                            $this->model_sale_order->updateOrderTransportStatus($parsed_data['transport_order_code'], $parsed_data['order_status_id'], $parsed_data['transport_status']);

                            // update previous order status id
                            if ($parsed_data['order_status_id'] != $order['previous_order_status_id']) {
                                $this->model_sale_order->updatePreviousOrderStatusId($order['order_id'], $order['order_status_id']);
                            }

                            $return_data = [
                                'code' => '200',
                                'message' => 'success'
                            ];
                        } else {
                            $return_data['message'] = 'could not find transport order code';
                        }
                    } else {
                        $return_data['message'] = 'wrong webhook data format';
                    }
                } else {
                    $return_data['message'] = 'not support transport method ' . $webhook_data['transport_method'];
                }
            } else {
                $return_data['message'] = 'manual_update_status is not exist or equal to 1';
            }
        } catch (Exception $e) {
            $return_data['message'] = $e->getMessage();
        }

        http_response_code($return_data['code']);
        $this->responseJson($return_data);
    }
}