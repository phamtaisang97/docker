<?php

class ControllerPreviewBlog extends Controller
{
    public function index()
    {
        $this->document->addScript('catalog/view/javascript/preview.js');
        return $this->load->controller('blog/blog');
    }
}