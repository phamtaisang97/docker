<?php

class ControllerPreviewShop extends Controller
{
    public function index()
    {
        $this->document->addScript('catalog/view/javascript/preview.js');
        return $this->load->controller('common/shop');
    }
}