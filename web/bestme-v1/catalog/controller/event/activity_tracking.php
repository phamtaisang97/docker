<?php

class ControllerEventActivityTracking extends Controller
{
    use Redis_Util;

    /*
     * run when customer create an pos order
     * lister: catalog/model/sale/order/addOrderCommon/after
     */
    public function trackingOrderComplete($trigger, $data)
    {
        if ($trigger == 'sale/order/addOrderCommon' && isset($data[0]) && $data[0]['order_status_id'] == ModelSaleOrder::ORDER_STATUS_ID_COMPLETED) {
            $this->saveToRedis('count_order_complete');
        }
    }

    /*
     * run when customer create an order
     * lister: catalog/model/sale/order/addOrderCommon/after
     */
    public function trackingCountOrder()
    {
        $this->saveToRedis('count_order');
    }

    /**
     * @param $key
     * @param $data
     */
    private function pushDataToRedis($key, $data)
    {
        try {
            $redis = $this->redisConnect();

            $redis->lPush($key, json_encode($data));
            $this->redisClose($redis);
        } catch (Exception $e) {
            $this->redisClose($redis);
        }
    }

    /**
     * @param $code
     * @param string $data
     */
    private function saveToRedis($code, $data = '')
    {
        $this->load->model('setting/setting');
        $result = [
            'key'       => $code,
            'data'      => $data,
            'shop_name' => $this->config->get('shop_name'),
            'domain'    => $_SERVER['HTTP_HOST'],
            'time'      => date("Y-m-d H:i:s"),
            'packet_paid' => $this->config->get('config_packet_paid')
        ];
        $this->pushDataToRedis('INDICATOR_TRACKING', $result);
    }

    // catalog/model/sale/order/addOrderVoucher/after
    public function afterAddOrderVoucher(&$route, &$args, &$output)
    {
        if (empty($output)) {
            return;
        }

        if (!is_array($args) || count($args) == 0) {
            return;
        }

        $this->load->model('sale/order');
        $this->model_sale_order->updateLimitTimesCoupon($args);
    }
}