<?php

/**
 * Class ControllerEventCustomerWelcome
 * @see https://github.com/opencart/opencart/wiki/Events-System
 */
class ControllerEventCustomerWelcome extends Controller
{
    public function post_customer_login(&$route, &$data, &$output)
    {
        $this->log->write('post_customer_login args: ');
        $this->log->write($route);
        $this->log->write($data);
        $this->log->write($output);

        if (!is_array($data) || empty($data)) {
            return;
        }

        $customer_id = $data[0];
        $this->log->write("customer id: $customer_id");

        $this->log->write('on post_customer_login() [due to event customer_welcome]');

        /* create module config for this module if not has */
        $this->load->model('extension/module/customer_welcome');

        $existingConfig = $this->model_extension_module_customer_welcome->getConfig($customer_id);

        $this->log->write('existingConfig: ');
        $this->log->write($existingConfig);

        if (!is_array($existingConfig) || empty($existingConfig)) {
            $this->log->write('add new Config: ');

            $existingConfig = [
                'is_shown_welcome_message' => 0
            ];

            $this->model_extension_module_customer_welcome->addConfig($customer_id, $existingConfig);

            return;
        }

        /* update module config for this module if has */
        $existingConfig = [
            'is_shown_welcome_message' => 0
        ];

        $this->model_extension_module_customer_welcome->editConfig($customer_id, $existingConfig);
    }
}