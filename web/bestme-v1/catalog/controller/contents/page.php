<?php

class ControllerContentsPage extends Controller
{
    use Theme_Config_Util;

    public function index()
    {
        // $this->document->setTitle($this->config->get('config_meta_title'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->setKeywords($this->config->get('config_meta_keyword'));
        $this->load->model('tool/image');
        $this->load->model('online_store/contents');
        $this->load->language('common/page');

        $data = [];

        $data['theme_directory'] = '/catalog/view/theme/' . $this->getCurrentThemeDir();

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $content_id = $this->request->get['page_id'];
        $content = $this->model_online_store_contents->getContent($this->request->get['page_id']);

        $data['breadcrumbs'][] = array(
            'text' => $content['title'],
            'href' => $this->url->link('contents/page', '&page_id=' . $content_id),
            'active' => true,
        );

        if (isset($this->request->get['page_id']) && $this->request->get['page_id'] != null) {
            $content = $this->model_online_store_contents->getContent($this->request->get['page_id']);
            if (isset($content['status']) && $content['status'] == '1') {
                $data['content'] = $content;
            } else {
                $data['content']['title'] = 'Not found!';
            }

            // set out page title as title of page content
            $title = isset($data['content']['title']) ? $data['content']['title'] : '';
            $this->document->setTitle($title);
        } else {
            $this->response->redirect($this->url->link('common/home', '', true));
        }

        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('contents/content_detail', $data));
    }

    public function preview()
    {
        $this->document->setTitle($this->config->get('config_meta_title'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->setKeywords($this->config->get('config_meta_keyword'));
        $this->load->model('tool/image');

        $data = [];

        $data['theme_directory'] = '/catalog/view/theme/' . $this->getCurrentThemeDir();

        $data['content']['title'] = isset($this->request->post['title']) ? $this->request->post['title'] : '';
        $data['content']['description'] = isset($this->request->post['description']) ? $this->request->post['description'] : '';

        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('contents/content_detail', $data));
    }
}
