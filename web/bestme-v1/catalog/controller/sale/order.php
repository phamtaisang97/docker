<?php

class ControllerSaleOrder extends Controller
{
    use AUTO_CREATE_RECEIPT_VOUCHER_UTIL_TRAIT;
    use Order_Util_Trait;
    use Product_Util_Trait;
    use RabbitMq_Trait;

    const PAYMENT_STATUS_NOT_PAID = 0;
    const PAYMENT_STATUS_PAID = 1;
    const PAYMENT_STATUS_PROCESSING = 3;
    const PAYMENT_STATUS_FAIL = 4;


    /* status update order */
    const ORDER_UPDATE_PAYMENT_FALSE = 0;
    const ORDER_UPDATE_PAYMENT_TRUE = 1;
    const ORDER_NO_UPDATE = 2;

    public function addCustom()
    {
        $key_for_current_order = "current_order_" . $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            //$this->load->model('localisation/vietnam_administrative');
            $this->load->model('sale/order');
            $data = $this->request->post;
            if (empty($data['customer_address'])) {
                //save new address
                $new_address['firstname'] = isset($data['fullname']) ? extract_name($data['fullname'])[0] : $data['firstname'];
                $new_address['lastname'] = isset($data['fullname']) ? extract_name($data['fullname'])[1] : $data['lastname'];
                $new_address['address'] = $data['address'];
                $new_address['phone'] = $data['phone'];
                $new_address['city_delivery'] = $data['city_delivery'];
                $new_address['district_delivery'] = $data['district_delivery'];
                $new_address['ward_delivery'] = $data['ward_delivery'];
                $this->load->model('account/address');
                $this->load->model('customer/customer');
                $this->load->model('account/customer');
                $this->load->model('sale/order');
//                $data_check_customer = $this->model_customer_customer->getCustomerByTelephone($new_address['phone']);
//                if (!empty($data_check_customer) && $this->customer->getId() != NULL && !$this->model_account_address->getAddresses()) {
//                    $this->session->data['error_phone'] = "Số điện thoại đã tồn tại!";
//                    $this->session->data['old_last_name'] = $new_address['lastname'];
//                    $this->session->data['old_first_name'] = $new_address['firstname'];
//                    $this->session->data['old_address'] = $new_address['address'];
//                    $this->session->data['old_phone'] = $new_address['phone'];
//                    $this->session->data['old_city_delivery'] = $new_address['city_delivery'];
//                    $this->session->data['old_district_delivery'] = $new_address['district_delivery'];
//                    $this->session->data['old_ward_delivery'] = $new_address['ward_delivery'];
//                    $this->response->redirect($this->url->link('checkout/order_preview', '', ''));
//                }
                $current_address = $this->model_account_address->getCountAddessAccount();
                if ($current_address == 0) {
                    $new_address['default'] = true;
                }
            }

            /* get fields and remove */
            $firstname = isset($data['fullname']) ? extract_name($data['fullname'])[0] : $data['firstname'];
            $lastname = isset($data['fullname']) ? extract_name($data['fullname'])[1] : $data['lastname'];
            $fullname = $lastname . ' ' . $firstname;
            if (!$this->customer->getEmail()) {
                $customer_info = $this->model_account_customer->getCustomerByTelephone($data['phone']);
                if (isset($customer_info['email']) && $customer_info['email'] != '') {
                    $email = $customer_info['email'];
                } else {
                    $email = '';
                }
            } else {
                if (isset($data['email']) && $data['email'] != '') {
                    $email = $data['email'];
                } else {
                    $email = $this->customer->getEmail();
                }
            }

            $phone = $data['phone'];
            $firstname_delivery = isset($data['fullname']) ? extract_name($data['fullname'])[0] : $data['firstname'];
            $lastname_delivery = isset($data['fullname']) ? extract_name($data['fullname'])[1] : $data['lastname'];
            $phone_delivery = $data['phone'];
            $city_delivery = ''; //$this->model_localisation_vietnam_administrative->getProvinceByCode($data['city_delivery'])['name'];
            $district_delivery = ''; //$this->model_localisation_vietnam_administrative->getDistrictByCode($data['district_delivery'])['name'];
            $ward_delivery = ''; //$this->model_localisation_vietnam_administrative->getWardByCode($data['ward_delivery'])['name'];
            $address_delivery = $data['address'];
            $payment_method = $data['payment_method'];

            unset($data['fullname']);
            unset($data['firstname']);
            unset($data['lastname']);
            unset($data['email']);
            unset($data['phone']);
            unset($data['firstname_delivery']);
            unset($data['lastname_delivery']);
            unset($data['phone_delivery']);
            unset($data['address_delivery']);
            unset($data['payment_method']);

            /* map fields to form. The following Form structure is got from admin/model/sale/order */
            $data['order_id'] = 0;
            $data['store_id'] = 0;
            $data['store_url'] = $this->request->server['HTTPS'] ? HTTPS_SERVER : HTTP_SERVER;

            $data['customer'] = '';
            $data['customer_id'] = $this->customer->getId();
            if ($data['customer_id'] == null && !$this->validatePhone(['telephone' => $phone])) {
                $data['customer_id'] = $this->getCustomerByTelephone($phone_delivery);
            }
            $data['customer_group_id'] = $this->config->get('config_customer_group_id');
            $data['firstname'] = $firstname;
            $data['lastname'] = $lastname;
            $data['fullname'] = $fullname;
            $data['email'] = $email;
            $data['telephone'] = $phone;
            $data['customer_custom_field'] = array();
            $data['order_payment'] = (!isset($data['payment_method']) || $data['payment_method'] == 0) ? 2 : 1;  //  1 : Đã thanh toán, 2 : Thanh toán sau (COD)
            $data['order_transfer'] = ModelSaleOrder::ORDER_STATUS_ID_PROCESSING; //  1 : Chờ vận chuyển, 2 : Đang vận chuyển, 3 : Đang hoàn đơn
            $data['order_note'] = isset($data['order_note']) ? $data['order_note'] : '';

            $data['addresses'] = array();

            $data['payment_firstname'] = $firstname;
            $data['payment_lastname'] = $lastname;
            $data['payment_company'] = '';
            $data['payment_address_1'] = $address_delivery;
            $data['payment_address_2'] = '';//sprintf('%s, %s, %s, %s', $address_delivery, $ward_delivery, $district_delivery, $city_delivery);
            $data['payment_city'] = $city_delivery;
            $data['payment_postcode'] = '';
            $data['payment_country_id'] = '';
            $data['payment_zone_id'] = '';
            $data['payment_method'] = isset($payment_method) ? $payment_method : '';
            $data['payment_custom_field'] = array();
            $data['payment_code'] = '';

            if ($this->isPaymentMethodNeedRedirect($payment_method)) {
                // payment method: vn pay
                $data['payment_status'] = self::PAYMENT_STATUS_PROCESSING; // đơn hàng đang trong quá trình thanh toán
            } else {
                $data['payment_status'] = self::PAYMENT_STATUS_NOT_PAID;
            }

            $data['shipping_firstname'] = $firstname_delivery;
            $data['shipping_lastname'] = $lastname_delivery;
            $data['shipping_company'] = '';
            $data['shipping_address_1'] = $address_delivery;
            $data['shipping_address_2'] = ''; //sprintf('%s, %s, %s, %s', $address_delivery, $ward_delivery, $district_delivery, $city_delivery);
            $data['shipping_city'] = $city_delivery;
            $data['shipping_postcode'] = '';
            $data['shipping_country_id'] = '';
            $data['shipping_zone_id'] = '';
            $data['shipping_custom_field'] = array();
            $data['shipping_method'] = '';
            $data['shipping_method_value'] = isset($data['payment_trans']) ? $data['payment_trans'] : '';
            $data['shipping_code'] = '';
            $data['shipping_ward_code'] = $data['ward_delivery'];
            $data['shipping_district_code'] = $data['district_delivery'];
            $data['shipping_province_code'] = $data['city_delivery'];

            $data['order_products'] = array();
            $data['order_vouchers'] = array();
            $data['order_totals'] = array();

            if ($this->isPaymentMethodNeedRedirect($payment_method)) {
                $data['order_status_id'] = ModelSaleOrder::ORDER_STATUS_ID_DRAFT;
                $data['order_status'] = ModelSaleOrder::ORDER_STATUS_ID_DRAFT;  /////Đang xử lý; @@
            } else {
                $data['order_status_id'] = ModelSaleOrder::ORDER_STATUS_ID_PROCESSING;
                $data['order_status'] = ModelSaleOrder::ORDER_STATUS_ID_PROCESSING;  /////Đang xử lý; @@
            }
            $data['currency_code'] = $this->config->get('config_currency');

            $data['coupon'] = '';
            $data['voucher'] = '';
            $data['reward'] = '';

            $data['from_domain'] = $_SERVER['SERVER_NAME'];
            //get shipping method
            $this->load->model('setting/setting');
            $delivery_methods = $this->model_setting_setting->getSettingValue('delivery_methods');
            $delivery_methods = json_decode($delivery_methods, true);
            $delivery_methods = is_array($delivery_methods) ? $delivery_methods : [];

            foreach ($delivery_methods as $k => $delivery_method) {
                if ($delivery_method['status'] != '1') {
                    continue;
                }
                if ($delivery_method['id'] == $data['shipping_method_value']) {
                    $data['shipping_method'] = $delivery_method['name'];
                }
            }

            // create new in order_product table
            $products = $this->session->data[$key_for_current_order];

            // check nếu không có order
            if (!$products) {
                $this->response->redirect($this->url->link('checkout/my_orders', '', true));
            }

            $this->load->model('catalog/product');
            $this->load->model('discount/discount');
            $total_amount = 0;
            $total_into_money = 0;
            $total_weight = 0;
            $discount_ids = [];

            $totalQuantityAddOnProduct = 0;
            $totalIntoMoneyAddOnProduct = 0;
            $totalWeightAddOnProduct = 0;

            foreach ($products as $key_order => &$product) {
                if (!isset($product['product_id']) || (int)$product['quantity'] == 0) {
                    continue;
                }

                $product_detail = $this->model_catalog_product->getProduct($product['product_id']);

                $product_version_id = isset($product['product_version']['product_version_id']) ? $product['product_version']['product_version_id'] : 0;
                $inStock = $this->model_catalog_product->isStock($product['product_id'], $product_version_id);
                if (!$inStock) {
                    if (isset($this->request->post['isset_cart'])) {
                        $this->response->redirect($this->url->link('common/home', 'show_cart=true', ''));
                    } else {
                        $this->response->redirect($this->url->link('checkout/order_preview', '', ''));
                    }
                }

                $product_version_server = [];
                if ($product_version_id) {
                    $product_version_server = $this->model_catalog_product->getProductVersionById($product_detail['product_id'], $product['product_version']['product_version_id']);
                }

                if (!$product_detail ||
                    ($product_detail['multi_versions'] == 1 && count($product_version_server) == 0)) {
                    if (isset($this->session->data[$key_for_current_order])) {
                        unset($this->session->data[$key_for_current_order]);
                    }
                    $this->response->redirect($this->url->link('checkout/my_orders', '', true));
                }

                // support for discount
                $product['product_version_id'] = empty($product_version_id) ? '' : $product_version_id;
                $product['default_store_id'] = $product_detail['default_store_id'];
                $product['name'] = $product_detail['name'];
                $product['model'] = $product_detail['model'];
                $product['price'] = $product_detail['price'];

                $change_data_session_product_version = false;
                $error_text_discount = '';
                $discount_id = null;

                if (isset($product['attribute']) && $product['attribute']) {
                    if (empty($product_version_server) || !isset($product_version_server['price']) ||
                        !isset($product_version_server['compare_price'])) {
                        $error_text_discount = "Phiên bản {$product['product_name_add_cart']} đã bị thay đổi, vui lòng chọn lại!";
                    }

                    if ((int)$product['product_version']['price'] == 0) {
                        if (!empty($product_version_server['discount_id'])) {
                            $product['product_version']['price'] = 0;
                        } else {
                            $product['product_version']['price'] = $product_version_server['compare_price'];
                        }
                    }

                    if ((int)$product['product_version']['price'] == 0) {
                        if (!empty($product_version_server['discount_id'])) {
                            $product['product_version']['price'] = 0;
                        } else {
                            $product['product_version']['price'] = $product_version_server['compare_price'];
                        }
                    }

                    if (!empty($product_version_server['discount_id'])) {
                        if ((int)$product['product_version']['price'] != (int)$product_version_server['price']) {
                            $error_text_discount = "Phiên bản {$product['product_name_add_cart']} {$product_version_server['version']} đã bị thay đổi, vui lòng tải lại!";
                            $product['product_version']['price'] = $product_version_server['price'];
                            $change_data_session_product_version = true;
                        }
                    } else {
                        if ((int)$product['product_version']['price'] != (int)$product_version_server['compare_price']) {
                            $error_text_discount = "Phiên bản {$product['product_name_add_cart']} {$product_version_server['version']} đã bị thay đổi, vui lòng tải lại!";
                            $product['product_version']['price'] = $product_version_server['compare_price'];
                            $change_data_session_product_version = true;
                        }
                    }

                    $discount_id = $product_version_server['discount_id'];
                    $total = (float)$product['product_version']['price'] * (int)$product['quantity'];
                    $product['price_before_discount'] = (float)$product['product_version']['compare_price'];
                } else {
                    if ((int)$product_detail['price'] == 0) {
                        if (!empty($product_detail['discount_id'])) {
                            $product_detail['price'] = 0;
                        } else {
                            $product_detail['price'] = $product_detail['compare_price_master'];
                        }
                    }

                    $discount_id = $product_detail['discount_id'];
                    $total = (float)$product_detail['price'] * (int)$product['quantity'];
                    $product['price_before_discount'] = (float)$product_detail['compare_price_master'];
                }

                if ($change_data_session_product_version) {
                    $this->session->data[$key_for_current_order][$key_order] = $product;
                }

                // Add-on Deal
                $this->load->model('discount/add_on_deal');
                $deal_data = $this->model_discount_add_on_deal->getAddOnDealByProductId($product['product_id'], $product_version_id);

                $add_on_product_ids = [];
                $add_on_products = [];
                if (!empty($deal_data)) {
                    if (!empty($product['add_on_products'])) {
                        foreach ($product['add_on_products'] as $add_on_product) {
                            $add_on_product_ids[] = $add_on_product['id'];
                        }
                    }

                    if (!empty($add_on_product_ids)) {
                        $add_on_products = $this->dataAddOnProducts($add_on_product_ids);
                    }

                    foreach ($add_on_products as $add_on_product) {
                        $totalQuantityAddOnProduct += 1;
                        $totalWeightAddOnProduct += (float)$add_on_product['weight'];
                        $totalIntoMoneyAddOnProduct += (float)$add_on_product['sale_price'];
                    }
                } else {
                    unset($this->session->data[$key_for_current_order][$key_order]['add_on_products']);
                }
                // End add-on deal

                $total_amount += $product['quantity'];
                $total_into_money += $total; //use total_into_money after discount
                $total_weight = $total_amount * $product_detail['weight'];

                // check discount expire
                if (!empty($discount_id)) {
                    if (!in_array($discount_id, $discount_ids)) {
                        $discount_ids[] = $discount_id;
                    }

                    $result = $this->model_discount_discount->checkDiscountValidById($discount_id);
                    if (empty($result)) {
                        $error_text_discount = "Chương trình khuyến mãi dành cho sản phẩm đã tạm ngưng hoặc quá hạn!";
                    }
                }
            }
            unset($product);

            // has add-on product
            $total_amount = $total_amount + $totalQuantityAddOnProduct;
            $total_into_money = $total_into_money + $totalIntoMoneyAddOnProduct;
            $total_weight = $total_weight + $totalWeightAddOnProduct;

            // merge add-on product in products of order
            $order_products = [];
            $order_add_on_products = [];
            foreach ($products as $product) {
                $order_products[] = $product;

                if (!empty($product['add_on_products'])) {
                    foreach ($product['add_on_products'] as $add_on_product) {
                        $addOnProduct = [
                            "main_product_id" => $product['product_id'],
                            "main_product_version_id" => !empty($order_product['product_version']['product_version_id']) ? $order_product['product_version']['product_version_id'] : 0,
                            "product_id" => $add_on_product['product_id'],
                            "quantity" => 1,
                            "model" => '',
                            "is_add_on_product" => true,
                            "name" => $add_on_product['product_name'],
                        ];

                        if (!empty($add_on_product['product_version_id'])) {
                            $addOnProduct["product_version"] = [
                                "product_version_id" => $add_on_product['product_version_id'],
                                "product_id" => $add_on_product['product_id'],
                                "version" => $add_on_product['version'],
                                "compare_price" => $add_on_product['original_price'],
                                "price" => $add_on_product['sale_price'],
                            ];
                        } else {
                            $addOnProduct['price'] = $add_on_product['sale_price'];
                        }

                        $order_add_on_products[] = $addOnProduct;

                        $key_order_product = $this->checkOrderProductExisted($order_products, $addOnProduct);
                        if (is_null($key_order_product)) {
                            $order_products[] = $addOnProduct;
                        } else {
                            $order_products[$key_order_product]['quantity'] += $addOnProduct['quantity'];
                        }
                    }
                }
            }

            if (!empty($order_products)) {
                $products = $order_products;
            }
            // end merge

            $order_discount_value =  0;
            $data['order-discount'] = $order_discount_value;
            $total_into_money -= $order_discount_value;

            $shipping_fee = $this->getTransportFee($data['shipping_method_value'], $data['city_delivery'], $total_weight, $total_into_money);
            $data['shipping_fee'] = str_replace(',', '', (isset($shipping_fee['fee_order']['fee']) ? $shipping_fee['fee_order']['fee'] : '0'));

            //
            $coupon_code = isset($data['voucher_code']) ? $data['voucher_code'] : '';
            $voucher_code_discount_value = isset($data['voucher_code_discount_value']) ? $data['voucher_code_discount_value'] : 0;

            $coupon_info = [];
            $discountVoucher = 0;
            if (!empty($coupon_code)) {
                $coupon_info = $this->model_sale_order->getCouponInfoByCode($coupon_code);

                $discountVoucher = $this->calcDiscountVoucher(($data['shipping_fee'] + $total_into_money), $coupon_info);
            }

            if ($voucher_code_discount_value != $discountVoucher) {
                $this->session->data['voucher_value_error'] = "Xin lỗi! Đã có lỗi xảy ra trong quá trình sử dụng voucher. 
                Liên hệ hotline {$this->config->get('config_hotline')} để được tư vấn.";
                $this->response->redirect($this->url->link('checkout/order_preview', '', ''));

                return;
            }

            $data['total'] = $data['shipping_fee'] + $total_into_money - (int)$discountVoucher;

            if ($data['total'] < 0) {
                $data['total'] = 0;
                $discountVoucher = $data['shipping_fee'] + $total_into_money;
            }

            /* validate */
            $validate_customer = $this->validateCustomer($data);
            if ($validate_customer) {
                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($validate_customer));

                return;
            }

            if (!empty($error_text_discount)) {
                $this->session->data['price_error'] = $error_text_discount;
                $this->response->redirect($this->url->link('checkout/order_preview', '', ''));
                return;
            }

            if (!empty($order_add_on_products)) {
                foreach ($order_add_on_products as $order_add_on_product) {
                    $p_id = $order_add_on_product['product_id'];
                    $pv_id = 0;
                    if (!empty($order_add_on_product['product_version']['product_version_id'])) {
                        $pv_id = $order_add_on_product['product_version']['product_version_id'];
                    }

                    if (empty($p_id)) {
                        $this->session->data['order_add_on_product'] = "Không tồn tại sản phẩm tặng kèm!";
                        $this->response->redirect($this->url->link('checkout/order_preview', '', ''));
                        return;
                    }

                    $dealId = $this->model_sale_order->getAddOnDealId($p_id, $pv_id);
                    if (empty($dealId)) {
                        $this->session->data['order_add_on_product'] = "Chương trình mua kèm đã bị tạm dừng hoặc hết hạn!";
                        $this->response->redirect($this->url->link('checkout/order_preview', '', ''));
                        return;
                    }
                }
            }

            $order_id = $this->model_sale_order->addOrderCommon($data, $data['customer_id']);
            if (!$order_id) {
                $this->response->redirect($this->url->link('checkout/my_orders', '', true));
            }

            if (!empty($coupon_info)) {
                $this->model_sale_order->addOrderVoucher($order_id, $coupon_info, $discountVoucher);
            }

            if (!empty($discount_ids)) {
                $this->model_discount_discount->addOrderDiscount($order_id, $discount_ids);
            }

            // confix Prefix, suffix in order_code
            $this->load->model('setting/setting');

            $config_id_prefix = $this->model_setting_setting->getSettingByKey('config_id_prefix');
            $config_id_prefix = isset($config_id_prefix['value']) ? $config_id_prefix['value'] : '';

            $config_id_suffix = $this->model_setting_setting->getSettingByKey('config_id_suffix');
            $config_id_suffix = isset($config_id_suffix['value']) ? $config_id_suffix['value'] : '';

            $max_order_id = 99999999999;
            $value_order = $max_order_id - ($max_order_id - $order_id);
            $strlen_value = strlen($value_order);
            $number_for = 11 - $strlen_value;
            $number_zero = '0';
            $result = '';

            for ($i = 1; $i <= $number_for; $i++) {
                $result .= $number_zero;
            }
            $order_code = $config_id_prefix . $result . $order_id . $config_id_suffix;
            // TODO: ... move to modal
            $this->db->query("UPDATE " . DB_PREFIX . "order SET order_code = '" . $this->db->escape($order_code) . "', source = 'shop' WHERE order_id = " . (int)$order_id);

            $this->model_sale_order->addOrderProduct($products, $order_id);
            $this->model_sale_order->addOrderAddOnProduct($order_add_on_products, $order_id);
            $this->model_sale_order->addReport($order_id);

            /* clean cart (current order) */
            if (isset($this->session->data[$key_for_current_order]) && !$this->isPaymentMethodNeedRedirect($payment_method)) {
                unset($this->session->data[$key_for_current_order]);
            }

            if (!$this->isPaymentMethodNeedRedirect($payment_method)) {
                $data = $this->model_sale_order->getOrder($order_id);
                $data['shipping_address'] = $this->getShippingAddress($data);
                $data['products'] = $this->getProductsByOrderId($order_id);
                $data['payment_method_name'] = $this->getPaymentMethodName($payment_method);
                $data['shop_name'] = $this->model_setting_setting->getSettingValue('shop_name') . SHOP_NAME_SUFFIX;

                // get config name display setting
                $config_name = $this->model_setting_setting->getSettingValue('config_name');
                $config_name_display = $this->model_setting_setting->getSettingValue('config_name_display');
                $data['config_name_display'] = is_null($config_name_display) ? $config_name : $config_name_display;

                $data['email_admin'] = $this->model_setting_setting->getSettingValue('config_email_admin');
                $data['shipping_fee'] = number_format(str_replace(',', '', $data['shipping_fee']));
                $data['total'] = number_format($data['total']);
                $data['order_total'] = number_format($total_into_money ? $total_into_money : 0);
                $this->sendEmailOnOrderSuccess($data);
                // origin url: index.php?route=checkout/order_preview/orderSuccess&order_code={order_code}
                $order_success_url = 'thank-you/' . urlencode($order_code);
                $this->response->redirect($order_success_url);
            } else {
                $this->redirectPaymentWebsite($order_id, $data['order_note'], $data['total'], $payment_method, $order_code);
            }
        }
    }

    public function calcDiscountVoucher($total_into_money, $coupon_info)
    {
        $discount = 0;

        $this->load->model('sale/order');
        $into_money = $total_into_money;

        $key_for_current_order = "current_order_" . $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());
        // create new in order_product table
        $products = $this->session->data[$key_for_current_order];

        // check nếu không có order
        if (!$products) {
            $this->response->redirect($this->url->link('checkout/my_orders', '', true));
        }

        if (empty($coupon_info) || empty($coupon_info['apply_for'])) {
            return $discount;
        }

        if ($coupon_info['apply_for'] == ModelSaleOrder::VOUCHER_APPLY_FOR_VALUE_ORDER) {
            if ((float)$into_money <= (float)$coupon_info['order_value_from']) {
                return $discount;
            }
        } elseif ($coupon_info['apply_for'] == ModelSaleOrder::VOUCHER_APPLY_FOR_PRODUCT) {
            $checked = $this->model_sale_order->checkCouponProduct($coupon_info['coupon_id'], $products);

            if ($checked) {
                return $discount;
            }
        }

        $total = (float)$into_money;
        switch ($coupon_info['apply_for']) {
            case ModelSaleOrder::VOUCHER_APPLY_FOR_PRODUCT:
                $discount = $this->calcDiscountCouponProduct($coupon_info, $total, $products);
                break;
            default:
                if ($coupon_info['type'] == 2) {
                    $discount = $total * (float)$coupon_info['discount'] / 100;
                } else {
                    $discount = (float)$coupon_info['discount'];
                }
                break;
        }

        return $discount;
    }

    /**
     * handle Server callback from CMS system. e.g: https://bestme.vn/api/vnpay_ipn?payment_method=paym_ons_vnpay
     *
     * additional params such as &vnp_tnxRef=abc&... will be append to that url due to payment method!
     *
     * TODO: rename this function for all payment methods, e.g paymentS2SCallback()
     */
    public function vnpayIPN()
    {
        $this->log->write('VNPay ipn IP: ' . $this->getClientIp());
        $this->log->write($_GET);

        $this->load->model('sale/order');

        // Temp hardcoded. TODO: got from url query params...
        //$payment_app_code = isset($this->request->get['payment_method']) ? $this->request->get['payment_method'] : '';
        $payment_app_code = ModelSaleOrder::VNP_APP_CODE;

        $paym_app_instance = Online_Payment::getOnlinePaymentInstance($payment_app_code, $this->registry, $data = ['somekey' => 'somevalue']);
        if (!method_exists($paym_app_instance, 'handleS2SCallback')) {
            return null;
        }

        // Could not use $this->request->get because of some data got encoded...
        // Temp use $_GET instead
        // TODO: find the best way to use $this->request->get...
        $returnData = $paym_app_instance->handleS2SCallback($_GET);

        // đơn nháp.
        // TODO: very IMPORTANT: do not check vnp_TxnRef. Got this from controller/extension/appstore/paym_ons_vnpay.php instead...
        $order_code_id = (int)$_GET['vnp_TxnRef'];
        // TODO: very IMPORTANT: do not check $returnData['RspCode']. Return common code instead...
        $need_send_mail = false;
        switch ($returnData['status']) {
            case self::ORDER_NO_UPDATE:
                break;

            case self::ORDER_UPDATE_PAYMENT_TRUE:
                $this->model_sale_order->updateOrderPaymentStatus($order_code_id, self::PAYMENT_STATUS_PAID, ModelSaleOrder::ORDER_STATUS_ID_PROCESSING, $_GET['vnp_TransactionNo']);
                $need_send_mail = true;
                $this->autoCreateReceiptVoucher($order_code_id);
                $this->model_sale_order->addReport($order_code_id);
                break;

            default:
                $this->model_sale_order->updateOrderPaymentStatus($order_code_id, self::PAYMENT_STATUS_FAIL, ModelSaleOrder::ORDER_STATUS_ID_PROCESSING, $_GET['vnp_TransactionNo']);
                $need_send_mail = true;
        }

        if ($need_send_mail) {
            // send mail
            try {
                $data = $this->model_sale_order->getOrder($order_code_id);

                // only send mail if order existing
                if (isset($data['order_id'])) {
                    $data['shipping_address'] = $this->getShippingAddress($data);
                    $data['products'] = $this->getProductsByOrderId($order_code_id);
                    $data['payment_method_name'] = $this->getPaymentMethodName($payment_app_code);
                    $data['shop_name'] = $this->model_setting_setting->getSettingValue('shop_name') . SHOP_NAME_SUFFIX;

                    // get config name display setting
                    $config_name = $this->model_setting_setting->getSettingValue('config_name');
                    $config_name_display = $this->model_setting_setting->getSettingValue('config_name_display');
                    $data['config_name_display'] = is_null($config_name_display) ? $config_name : $config_name_display;

                    $data['email_admin'] = $this->model_setting_setting->getSettingValue('config_email_admin');
                    $data['order_total'] = number_format($data['total'] - $data['shipping_fee']);
                    $data['shipping_fee'] = number_format(str_replace(',', '', $data['shipping_fee']));
                    $data['total'] = number_format($data['total']);
                    $this->sendEmailOnOrderSuccess($data);
                }
            } catch (Exception $e) {
                // do something...
            }
        }

        // response
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($returnData['resp_to_server']));
    }

    private function autoCreateReceiptVoucher($order_id)
    {
        $this->load->model('sale/order');
        $data = $this->model_sale_order->getOrder($order_id);
        $dataToSave = [
            'payment_status' => 1,
            'payment_method' => 2,
            'total_pay' => $data['total']
        ];

        $this->autoUpdateFromOrder($order_id, $dataToSave);
    }

    public function validateCustomer($data)
    {
        $json = array();

//        // validate firstname, lastname, phone, email
//        if ($errorFirstname = $this->validateFirstname($data)) {
//            $json['error']['firstname'] = $errorFirstname;
//        }
//
//        if ($errorLastname = $this->validateLastname($data)) {
//            $json['error']['lastname'] = $errorLastname;
//        }

//        if ($errorEmail = $this->validateEmail($data)) {
//            $json['error']['email'] = $errorEmail;
//        }

        if ($errorPhone = $this->validatePhone($data)) {
            $json['error']['telephone'] = $errorPhone;
        }

        return $json;
    }

    public function validateFirstname($data)
    {
        // buyer
        if ((utf8_strlen(trim($data['firstname'])) < 1) || (utf8_strlen(trim($data['firstname'])) > 32)) {
            $result = $this->language->get('error_firstname');

            return $result;
        }

        // receiver
        if ((utf8_strlen(trim($data['shipping_firstname'])) < 1) || (utf8_strlen(trim($data['shipping_firstname'])) > 32)) {
            $result = $this->language->get('error_shipping_firstname');

            return $result;
        }

        return null;
    }

    public function validateLastname($data)
    {
        // buyer
        if ((utf8_strlen(trim($data['lastname'])) < 1) || (utf8_strlen(trim($data['lastname'])) > 32)) {
            $result = $this->language->get('error_lastname');

            return $result;
        }

        // receiver
        if ((utf8_strlen(trim($data['shipping_lastname'])) < 1) || (utf8_strlen(trim($data['shipping_lastname'])) > 32)) {
            $result = $this->language->get('error_shipping_lastname');

            return $result;
        }

        return null;
    }

    public function validateEmail($data)
    {
        if ((utf8_strlen($data['email']) > 96) || (!filter_var($data['email'], FILTER_VALIDATE_EMAIL))) {
            $result = $this->language->get('error_email');

            return $result;
        }

        return null;
    }

    public function validatePhone($data)
    {
        if ((utf8_strlen($data['telephone']) < 3) || (utf8_strlen($data['telephone']) > 32)) {
            $result = $this->language->get('error_telephone');

            return $result;
        }

        return null;
    }

    /* TODO: remove when stable... */
    /*private function getTransportFee($transport = '', $city = '', $mass = '', $total_order = '')
    {

        $this->load->model('localisation/vietnam_administrative');
        $transport = $transport;
        $cityArr = $this->model_localisation_vietnam_administrative->getProvinceByCode($city);
        $city = '';
        if (isset($cityArr['name'])) {
            $city = $cityArr['name'];
        }

        $this->load->model('sale/order');
        $this->load->model('setting/setting');
        $this->load->language('sale/order');
        $delivery_methods = $this->model_setting_setting->getSettingValue('delivery_methods');
        $delivery_methods = json_decode($delivery_methods, true);

        $delivery_methods = is_array($delivery_methods) ? $delivery_methods : [];
        $value_fee_cod = 0;
        $fee_price = 0;
        $name_transport = '';

        foreach ($delivery_methods as $key => $vl) {
            $default_fee_region = isset($vl['fee_region']) ? $vl['fee_region'] : 0;

            if ($transport == $vl['id']) {
                $name_transport = $vl['name'];
                $found_region = false;
                foreach ($vl['fee_regions'] as $value) {
                    if (in_array(trim($city), $value['provinces'])) {
                        foreach ($value['weight_steps'] as $vl_weight) {
                            if (convertWeight($vl_weight['from']) <= $mass && convertWeight($vl_weight['to']) >= $mass) {
                                $fee_price = str_replace(',', '', $vl_weight['price']); // Gia thuoc tinh thanh
                                $found_region = true;
                                break;
                            } else {
                                $fee_price = str_replace(',', '', $default_fee_region);  // Gia mac dinh khi khong thuoc tinh thanh
                            }
                        }
                    } else {
                        $fee_price = str_replace(',', '', $default_fee_region);  // Gia mac dinh khi khong thuoc tinh thanh
                    }

                    if ($found_region) {
                        break;
                    }
                }
                foreach ($vl['fee_cod'] as $key_cod => $fee_cod) {
                    $arr_fee_status = ($vl['fee_cod'][$key_cod]);
                    if ($arr_fee_status['status'] == 1) {
                        $arr_fee_status = str_replace(',', '', $arr_fee_status['value']);
                        if ($key_cod === 'dynamic') {
                            if ($total_order != 0) {
                                $value_fee_cod = ($arr_fee_status / 100) * $total_order;
                            } else {
                                $value_fee_cod = 0;
                            }
                        } elseif ($key_cod === 'fixed') {
                            $value_fee_cod = $arr_fee_status;
                        }
                    }
                }
            }
        }

        $json['fee_order'] = [
            'name' => $name_transport,
            'fee' => $fee_price + $value_fee_cod,
            'error' => '',
        ];
        return $json['fee_order'];
    }*/

    // TODO REMOVE
    public function getMaxQuantityProduct($product, $order)
    {
        $this->load->model('catalog/product');
        if ($product['multi_versions'] == 1) {
            $product_version = $this->model_catalog_product->getProductByVersionId($product['product_id'], $order['product_version']['product_version_id']);
            if ($product_version && $product['status'] == 1 && $product_version['deleted'] != 1 && $product_version['status'] == 1 && ($product_version['quantity'] > 0 || $product['sale_on_out_stock'] == 1)) {
                return true;
            } else {
                return false;
            }
        }
        if ($product['multi_versions'] == 0 && $product['deleted'] != 1 && $product['status'] == 1 && ($product['product_master_quantity'] > 0 || $product['sale_on_out_stock'] == 1)) {
            return true;
        }
        return false;
    }

    public function getCouponByCode()
    {
        $json = [
            'code' => 404,
            'message' => 'Không tìm thấy mã',
            'type' => 'value',
            'discount' => 0,
        ];

        $this->load->model('sale/order');
        $code = $this->request->get['coupon_code'];
        $into_money = $this->request->get['into_money'];

        $key_for_current_order = "current_order_" . $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());
        // create new in order_product table
        $products = $this->session->data[$key_for_current_order];

        // check nếu không có order
        if (!$products) {
            $this->response->redirect($this->url->link('checkout/my_orders', '', true));
        }

        if (empty($code)) {
            $json['code'] = 403;
            $json['message'] = 'Missing coupon code';
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }

        $result = $this->model_sale_order->getCouponInfoByCode($code);

        if (empty($result)) {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }

        if (!empty($result)) {
            if (empty($result['apply_for'])) {
                $json['code'] = 403;
                $json['message'] = 'Mã không hợp lệ';
                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json));
                return;
            }

            $allowWithDiscount = $this->validCouponWithDiscount($result, $products);
            if ($allowWithDiscount) {
                $json['code'] = 403;
                $json['message'] = 'Mã không sử dụng cùng với sản phẩm đang trong chương trình khuyến mại!';
                $this->response->addHeader('Content-Type: application/json');
                $this->response->setOutput(json_encode($json));
                return;
            }

            if ($result['apply_for'] == ModelSaleOrder::VOUCHER_APPLY_FOR_VALUE_ORDER) {
                if ((float)$into_money <= (float)$result['order_value_from']) {
                    $order_value_from = (float)$result['order_value_from'];
                    $json['code'] = 403;
                    $json['message'] = "Mã áp dụng cho đơn có giá trị từ {$order_value_from}.";
                    $this->response->addHeader('Content-Type: application/json');
                    $this->response->setOutput(json_encode($json));
                    return;
                }
            } elseif ($result['apply_for'] == ModelSaleOrder::VOUCHER_APPLY_FOR_PRODUCT) {
                $checked = $this->model_sale_order->checkCouponProduct($result['coupon_id'], $products);
                if ($checked) {
                    $json['code'] = 403;
                    $json['message'] = 'Không tồn tại sản phẩm phù hợp.';
                    $this->response->addHeader('Content-Type: application/json');
                    $this->response->setOutput(json_encode($json));
                    return;
                }
            }
        }

        if (!empty($result)) {
            $total = (float)$into_money;
            switch ($result['apply_for']) {
                case ModelSaleOrder::VOUCHER_APPLY_FOR_PRODUCT:
                    $json['discount'] = $this->calcDiscountCouponProduct($result, $total, $products);
                    break;
                default:
                    if ($result['type'] == 2) {
                        $json['discount'] = $total * (float)$result['discount'] / 100;
                    } else {
                        $json['discount'] = (float)$result['discount'];
                    }
                    break;
            }

            $json['code'] = 200;
            $json['message'] = "Success";
            $json['type'] = isset($result['type']) ? $result['type'] : 'value';
            //$json['discount'] = isset($result['discount']) ? (float)$result['discount'] : 0;
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function validCouponWithDiscount($coupon, $products)
    {
        if (empty($coupon) || empty($products)) {
            return false;
        }

        $allowWithDiscount = false;
        foreach ($products as $product) {
            // find product
            $product_id = $product['product_id'];
            $this->load->model('catalog/product');
            $productData = $this->model_catalog_product->getProduct($product_id);
            $discount_id = null;

            if (empty($productData)) {
                continue;
            }

            if (isset($product['attribute']) && $product['attribute']) {
                $productVersionId = $product['product_version']['product_version_id'] ?? '';
                if (empty($productVersionId)) {
                    continue;
                }

                $productVersionData = $this->model_catalog_product->getProductVersionById($product_id, $productVersionId);
                if (empty($productVersionData)) {
                    continue;
                }

                $discount_id = $productVersionData['discount_id'];
            } else {
                $discount_id = $productData['discount_id'];
            }

            if ($coupon['allow_with_discount'] == 0 && !empty($discount_id)) {
                $allowWithDiscount = true;
            }
        }

        return $allowWithDiscount;
    }

    private function calcDiscountCouponProduct($coupon, $total, $products)
    {
        $couponProducts = $this->model_sale_order->checkCouponProduct($coupon['coupon_id'], $products, true);
        $discount = 0;
        $totalAmount = $total;
        $compareValue = (float)$coupon['applicable_type_value'];

        // map product
        foreach ($products as &$product) {
            if (!empty($product['product_version']) && !empty($product['product_version']['product_version_id'])) {
                $product['product_version_id'] = $product['product_version']['product_version_id'];
            } else {
                $product['product_version_id'] = 0;
            }
        }

        $calc = $this->calcTotalProductCoupon($couponProducts, $products);

        if ($coupon['applicable_type'] == ModelSaleOrder::VOUCHER_APPLICABLE_TYPE_QUANTITY_PRODUCT) {
            $discountTem = $this->calcDiscountCoupon($coupon, $calc['totalValueProduct']);
            $pass = $calc['totalQuantityProduct'] >= $compareValue;
            if ($coupon['how_to_apply'] == ModelSaleOrder::VOUCHER_PRODUCT_ONE_ON_ORDER) {
                return $pass ? $discountTem : 0;
            } elseif ($coupon['how_to_apply'] == ModelSaleOrder::VOUCHER_PRODUCT_QUANTITY_ON_ORDER) {
                return $pass ? $discountTem * $calc['totalQuantityProduct'] : 0;
            }
        } elseif ($coupon['applicable_type'] == ModelSaleOrder::VOUCHER_APPLICABLE_TYPE_TOTAL_ORDER) {
            $discountTem = $this->calcDiscountCoupon($coupon, $calc['totalValueProduct']);
            $pass = $calc['totalValueProduct'] >= $compareValue;
            if ($coupon['how_to_apply'] == ModelSaleOrder::VOUCHER_PRODUCT_ONE_ON_ORDER) {
                return $pass ? $discountTem : 0;
            } elseif ($coupon['how_to_apply'] == ModelSaleOrder::VOUCHER_PRODUCT_QUANTITY_ON_ORDER) {
                return $pass ? $discountTem * $calc['totalQuantityProduct'] : 0;
            }
        }

        return $discount;
    }

    private function calcTotalProductCoupon($couponProducts, $products)
    {
        $totalValueProduct = 0;
        $totalQuantityProduct = 0;

        foreach ($products as $product) {
            foreach ($couponProducts as $couponProduct) {
                if ($couponProduct['product_id'] == $product['product_id'] &&
                    $couponProduct['product_version_id'] == $product['product_version_id']) {
                    // find product
                    $product_id = $product['product_id'];
                    $this->load->model('catalog/product');
                    $productData = $this->model_catalog_product->getProduct($product_id);

                    if (isset($product['attribute']) && $product['attribute']) {
                        if ((int)$product['product_version']['price'] == 0){
                            if (!isset($product['product_version']['compare_price'])){
                                continue;
                            }
                            $product['product_version']['price'] = (float)$product['product_version']['compare_price'];
                        }
                        $price = (float)$product['product_version']['price'];
                    } else {
                        if ((int)$productData['price'] == 0) $productData['price'] = $productData['compare_price_master'];
                        $price = (float)$productData['price'];
                    }

                    $totalQuantityProduct += (float)$product['quantity'];
                    $totalValueProduct += (float)$price;
                }
            }
        }

        return [
            'totalValueProduct' => $totalValueProduct,
            'totalQuantityProduct' => $totalQuantityProduct
        ];
    }

    private function calcDiscountCoupon($coupon, $totalOrderAmount)
    {
        $discount = 0;

        if (isset($coupon['type']) && $coupon['type'] == ModelSaleOrder::VOUCHER_TYPE_AMOUNT) {
            $discount = (float)$coupon['discount'];
        } elseif (isset($coupon['type']) && $coupon['type'] == ModelSaleOrder::VOUCHER_TYPE_PERCENT) {
            $discount = $totalOrderAmount * (float)$coupon['discount'] / 100;
        }

        return $discount;
    }

    private function getCustomerByTelephone($telephone)
    {
        $this->load->model('account/customer');
        $customer = $this->model_account_customer->getCustomerByTelephone($telephone);
        if (is_array($customer) && isset($customer['customer_id'])) {
            return $customer['customer_id'];
        }

        return $this->createNewCustomer();
    }

    private function createNewCustomer()
    {
        $this->load->model('account/customer');
        $this->load->model('account/address');
        $this->load->model('report/customer');

        $customer = [
            'firstname' => isset($this->request->post['fullname']) ? extract_name($this->request->post['fullname'])[0] : $this->request->post['firstname'],
            'lastname' => isset($this->request->post['fullname']) ? extract_name($this->request->post['fullname'])[1] : $this->request->post['lastname'],
            'email' => isset($this->request->post['email']) ? $this->request->post['email'] : '',
            'telephone' => $this->request->post['phone'],
            'password' => token(9)
        ];

        $customer_id = $this->model_account_customer->addCustomer($customer);
        $this->model_report_customer->createCustomerReport($customer_id);
        $address_data = array(
            'customer_id' => $customer_id,
            'firstname' => isset($this->request->post['fullname']) ? extract_name($this->request->post['fullname'])[0] : $this->request->post['firstname'],
            'lastname' => isset($this->request->post['fullname']) ? extract_name($this->request->post['fullname'])[1] : $this->request->post['lastname'],
            'company' => '',
            'city' => $this->request->post['city_delivery'],
            'district' => $this->request->post['district_delivery'],
            'wards' => $this->request->post['ward_delivery'],
            'phone' => $this->request->post['phone'],
            'postcode' => '',
            'address' => $this->request->post['address'],
            'default' => true
        );

        $this->model_account_address->addAddressNew($customer_id, $address_data);

        return $customer_id;
    }

    //send email when order successfully
    private function sendEmailOnOrderSuccess($data)
    {
        $this->load->language('mail/order_add');
        // HTML Mail
        $data['title'] = sprintf($this->language->get('text_subject'), $data['shop_name'], $data['order_code'], $data['fullname']);
        $data['title_to_customer'] = sprintf($this->language->get('text_subject_to_customer'), $data['order_code']);

        // send email to admin
        if (isset($data['email_admin']) && $data['email_admin']) {
            $this->sendEmail($data, 'mail/order_add', $data['email_admin'], $data['title']);
        }

        // send email to customer
        if(isset($data['email']) && $data['email']) {
            $this->sendEmail($data, 'mail/order_add_to_customer', $data['email'], $data['title_to_customer'], $data['title_to_customer']);
        }

        // send email to staff
        $product_ids = array_map(function ($prod) {
            return isset($prod['product_id']) ? $prod['product_id'] : null;
        }, $data['products']);

        $mail_to_staff = $this->getStaffEmailsByProductIds($product_ids);

        if (isset($mail_to_staff) && !empty($mail_to_staff)) {
            foreach ($mail_to_staff as $key => $mail_staff) {
                // todo: for split order, send products of user_create_id for staff_id
//                $product_for_staff = [];
//                foreach ($data['products'] as $product) {
//                    if (isset($product['user_create_id']) && $product['user_create_id'] == $key) {
//                        $product_for_staff[] = $product;
//                    }
//                }
//                $data['products'] = $product_for_staff;
                $this->sendEmail($data, 'mail/order_add', $mail_staff, $data['title']);
            }
        }
    }

    /**
     * @param $data
     * @param $view
     * @param $send_to
     * @param $subject
     */
    private function sendEmail($data, $view, $send_to, $subject) {
        //// format date_added from default format in db (Y-m-d H:i:d) to d-m-Y H:i:s
        $date_added = $data['date_added'];
        if (is_string($date_added)) {
            try {
                $date_added = (date_create($date_added));
            } catch (Exception $e) {
            }
        }
        $data['date_added'] = ($date_added instanceof Datetime) ? $date_added->format("d-m-Y H:i:s") : $date_added;

        $data['text_greeting'] = sprintf($this->language->get('text_greeting'), $data['config_name_display'], $data['fullname'], $data['date_added']);
        $data['text_greeting_to_customer'] = sprintf($this->language->get('text_greeting_to_customer'), $data['fullname'], $data['order_code']);
        $data['text_link'] = $this->language->get('text_link');
        //$data['link'] = BESTME_SERVER.'admin/index.php?route=sale/order/detail&user_token=nVso40YxMATxSNR3nnCI0jQJowtQumRO&order_id='.$data['order_id'];
        $data['text_order_code'] = sprintf($this->language->get('text_order_code'), $data['order_code']);
        $data['text_footer'] = sprintf($this->language->get('text_footer'), BESTME_SERVER);

        $mail = new Mail($this->config->get('config_mail_engine'));
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = MAIL_SMTP_HOST_NAME;
        $mail->smtp_username = MAIL_SMTP_USERNAME;
        $mail->smtp_password = html_entity_decode(MAIL_SMTP_PASSWORD, ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = MAIL_SMTP_PORT;
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($send_to);
        $mail->setFrom($this->config->get('config_mail_smtp_username'));
        $mail->setSender(html_entity_decode($this->config->get('config_mail_smtp_username'), ENT_QUOTES, 'UTF-8'));
        $mail->setSubject($subject);
        $mail->setHtml($this->load->view($view, $data));
        try {
            $mail->send();
        } catch (Exception $e) {
            $this->log->write('on order successfully created sends mail got error: ' . $e->getMessage());
        }
    }

    // get products by order id
    private function getProductsByOrderId($order_id)
    {
        $this->load->model('account/order');
        $this->load->model('tool/upload');
        $this->load->model('tool/image');
        $products = $this->model_account_order->getOrderProducts($order_id);
        // Products
        $data['products'] = array();
        foreach ($products as $product) {
            $option_data = array();

            $options = $this->model_account_order->getOrderOptions($order_id, $product['order_product_id']);

            foreach ($options as $option) {
                if ($option['type'] != 'file') {
                    $value = $option['value'];
                } else {
                    $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                    if ($upload_info) {
                        $value = $upload_info['name'];
                    } else {
                        $value = '';
                    }
                }

                $option_data[] = array(
                    'name' => $option['name'],
                    'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
                );
            }

            $product_info = $this->model_catalog_product->getProduct($product['product_id']);
            $product_info['version'] = '';
            if ($product['product_version_id']) {
                $product_info_version = $this->model_catalog_product->getProductByVersionId($product['product_id'], $product['product_version_id']);
                $product_info['version'] = isset($product_info_version['version']) ? $product_info_version['version'] : '';
                $product_info['sku'] = isset($product_info_version['sku']) ? $product_info_version['sku'] : '';
            }

            if ($product_info) {
                $reorder = $this->url->link('account/order/reorder', 'order_id=' . $order_id . '&order_product_id=' . $product['order_product_id'], true);
            } else {
                $reorder = '';
            }

            if ($product_info['image']) {
                $image = $this->model_tool_image->resize($product_info['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
            }

            $data['products'][] = array(
                'product_id' => $product['product_id'],
                'name' => $this->model_catalog_product->getProductName($product['product_id']),
                'user_create_id' => $this->model_catalog_product->getProductUserCreateId($product['product_id']),
                'url' => $this->url->link('product/product', 'product_id=' . $product['product_id']),
                'image' => $image,
                'model' => $product['model'],
                'sku' => $product_info['sku'],
                'version' => empty($product_info['version']) ? $product_info['version'] : str_replace(',', ' - ', $product_info['version']),
                'option' => $option_data,
                'quantity' => $product['quantity'],
                'price' => number_format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0)),
                'total' => number_format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0)),
                'reorder' => $reorder
            );
        }
        return $data['products'];
    }

    // get shipping adress
    private function getShippingAddress($data)
    {
        $this->load->model('localisation/vietnam_administrative');
        $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($data['shipping_province_code']);
        $data['shipping_province_name'] = $province['name'];
        $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($data['shipping_district_code']);
        $data['shipping_district_name'] = !empty($district['name']) ? $district['name'] : '';
        $ward = $this->model_localisation_vietnam_administrative->getWardByCode($data['shipping_ward_code']);
        $data['shipping_ward_name'] = !empty($ward['name']) ? $ward['name'] : '';

        $addrArr = array();
        if (trim($data['shipping_address_1']) != '') {
            $addrArr[] = $data['shipping_address_1'];
        }
        if (!empty($ward['name']) && trim($ward['name']) != '') {
            $addrArr[] = $ward['name'];
        }
        if (!empty($district['name']) && trim($district['name']) != '') {
            $addrArr[] = $district['name'];
        }
        if (trim($province['name']) != '') {
            $addrArr[] = $province['name'];
        }

        return $data['full_shipping_address'] = implode(', ', $addrArr);
    }

    // get payment method name by code
    private function getPaymentMethodName($code)
    {
        $payment_methods = $this->model_setting_setting->getSettingValue('payment_methods');
        $payment_methods = json_decode($payment_methods, true);
        $payment_methods = is_array($payment_methods) ? $payment_methods : [];
        $data['current_payment_method_name'] = '';
        foreach ($payment_methods as $k => $payment_method) {
            if ($payment_method['status'] != '1') {
                continue;
            }

            if (isset($code) && $payment_method['payment_id'] == $code) {
                $data['current_payment_method_name'] = $payment_method['name'];
            }
        }

        return $data['current_payment_method_name'];
    }

    private function redirectPaymentWebsite($order_id, $order_info, $total_amount, $payment_app_code, $order_code)
    {
        $data = [
            'order_id' => $order_id,
            'order_info' => $order_info,
            'total_amount' => $total_amount,
            'order_code' => $order_code
        ];

        $paym_app_instance = Online_Payment::getOnlinePaymentInstance($payment_app_code, $this->registry, $data);
        if (!method_exists($paym_app_instance, 'createPaymentRedirectUrl')) {
            return null;
        }

        $redirect_url = $paym_app_instance->createPaymentRedirectUrl($data);

        $this->response->redirect($redirect_url);
    }

    private function isPaymentMethodNeedRedirect($payment_code)
    {
        $payment_app_code = $payment_code;
        $paym_app_instance = Online_Payment::getOnlinePaymentInstance($payment_app_code, $this->registry, $data = []);

        return method_exists($paym_app_instance, 'createPaymentRedirectUrl');
    }

    private function getStaffEmailsByProductIds(array $product_ids)
    {
        if (empty($product_ids)) {
            return [];
        }

        $this->load->model('sale/order');
        $staff_ids = $this->model_sale_order->getStaffIdsByProductIds($product_ids);

        if (empty($staff_ids)) {
            return [];
        }

        $email_staff = $this->model_sale_order->getStaffEmailsByStaffId($staff_ids);

        return !empty($email_staff) ? $email_staff : [];
    }

    /**
     * @return mixed|string
     */
    private function getClientIp() {
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        elseif (isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        elseif (isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        elseif (isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        elseif (isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    private function getFullAddOnProduct($deal_id)
    {
        $this->load->model('tool/image');

        $products = [];
        $results = $this->model_discount_add_on_deal->getFullAddOnProductById($deal_id);

        foreach ($results as $item) {
            $product_id = !empty($item['product_id']) ? $item['product_id'] : 0;
            $product_version_id = !empty($item['product_version_id']) ? $item['product_version_id'] : 0;

            if (empty($product_id)) {
                continue;
            }

            $p_v_id = $product_id;
            if ($product_version_id) {
                $p_v_id = $product_id . '-' . $product_version_id;
            }

            $version_name = implode(' • ', explode(',', $item['version']));
            $version_name = $version_name != '' ? $version_name : '';

            if ($item['image'] != '') {
                $image = $this->model_tool_image->resize($item['image'], 100, 100);
            } else {
                $image = $this->model_tool_image->resize('no_image.png', 100, 100);
            }

            $sale_price = (float)$item['discount_price'];
            $original_price = empty($item['multi_versions']) ? (float)$item['p_compare_price'] : (float)$item['pv_compare_price'];
            $sku = empty($item['multi_versions']) ? $item['p_sku'] : $item['pv_sku'];

            $percent = round(($original_price - $sale_price) / $original_price * 100);

            if ($sale_price == 0) {
                $percent = 100;
            }

            $products[] = [
                "id" => $p_v_id,
                "product_id" => $item['product_id'],
                "product_version_id" => $item['product_version_id'],
                "version" => $version_name,
                "product_name" => $item['name'],
                "image" => $image,
                "sale_price" => $sale_price,
                "original_price" => $original_price,
                "sale_price_format" => $this->currency->formatCustom($this->tax->calculate($sale_price, $item['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
                "original_price_format" => $this->currency->formatCustom($this->tax->calculate($original_price, $item['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
                "quantity" => (float)$item['pts_quantity'],
                "sku" => $sku,
                "percent_reduce" => $percent,
                "checked" => false
            ];
        }

        return $products;
    }

    private function checkOrderProductExisted($order_products, $cloneAddOnProduct)
    {
        foreach ($order_products as $key => $order_product) {
            if (($order_product['product_id'] == $cloneAddOnProduct['product_id']) &&
                !empty($order_product['product_version']) &&
                !empty($cloneAddOnProduct['product_version']) &&
                ($order_product['product_version']['product_version_id'] == $cloneAddOnProduct['product_version']['product_version_id']) &&
                ((float)$order_product['product_version']['price'] == (float)$cloneAddOnProduct['product_version']['price'])
            ) {
                return $key;
            } elseif (empty($order_product['product_version']) &&
                empty($cloneAddOnProduct['product_version']) &&
                ($order_product['product_id'] == $cloneAddOnProduct['product_id']) &&
                (((float)$order_product['price_before_discount'] == (float)$cloneAddOnProduct['price']) ||
                    ((float)$order_product['price'] == (float)$cloneAddOnProduct['price']))
            ) {
                return $key;
            }
        }

        return null;
    }
}