<?php
// catalog/controller/api/custom.php
class ControllerExtensionNovaonNovaon extends Controller
{
    public $per_page = 10;
    public $page = 1;
    public $filter = '';
    public $filter_data = [];
    public function products()
    {
        $this->Credential();
        $this->load->config('novaon/Products');
        $novaon_Products = new novaon_Products;

        /* get models */
        $this->load->model('extension/novaon/Products');
        $this->load->model('extension/novaon/product');
        $this->load->model('extension/novaon/category');
        $this->load->model('extension/novaon/image');
        $this->load->model('tool/image');

        $products    = $this->model_extension_novaon_Products->getAPIProducts($this->filter_data);
        foreach ($products as $pro) {
            $pro = $this->model_extension_novaon_Products->getAPIProduct($pro['product_id'], $pro['pv1_product_version_id']);
            if ($pro) {
                $novaon_Product = new novaon_Product;
                $sale = 0; $date_on_sale_from = ''; $date_on_sale_to = '';
                $date_start = ''; $date_end = '';

                $product_specials = $this->model_extension_novaon_Products->getProductSpecials($pro['product_id']);
                foreach ($product_specials as $product_special) {
                    if (($product_special['date_start'] == '0000-00-00' || strtotime($product_special['date_start']) < time()) && ($product_special['date_end'] == '0000-00-00' || strtotime($product_special['date_end']) > time())) {
                        $special = $product_special['price'];
                        $date_start = $product_special['date_start'];
                        $date_end = $product_special['date_end'];
                        break;
                    }
                }
                if(isset($special)){
                    $sale  = number_format($special, 2, '.', '') . ' '.$this->session->data['currency'];
                    $date_on_sale_from = $date_start;
                    $date_on_sale_to = $date_end;
                }

                $_additional_image_link = array();
                $images          = $this->model_extension_novaon_product->getProductImages($pro['product_id']);
                if ($images) {
                    foreach ($images as $image) {
                        $_additional_image_link[] = $this->model_tool_image->resize(
                            $image['image'],
                            $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'),
                            $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height')
                        );
                    }
                }

                $novaon_Product->setInt('id', $pro['product_id']);
                $novaon_Product->set('name', $pro['name']);
                $novaon_Product->set('link', $this->url->link('product/product&product_id=' . $pro['product_id']));
                $novaon_Product->set('image_Link',
                    $this->model_tool_image->resize(
                        $pro['image'],
                        $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'),
                        $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height')
                    )
                );
                $novaon_Product->set('additional_image_link', implode(",", $_additional_image_link));
                $novaon_Product->set('short_description', $pro['description']);
                $novaon_Product->set('status', ($pro['status'] == 1?'publish':'pending'));
                $novaon_Product->set('sku', $pro['sku']);
                $novaon_Product->set('manufacturer', $pro['manufacturer']);
                $novaon_Product->set('upc', $pro['upc']);
                $novaon_Product->set('gtin', (isset($pro['gtin'])?$pro['gtin']:''));
                $novaon_Product->setInt('parent_id', (isset($pro['parent_id'])?$pro['parent_id']:0));
                if ($pro['price'] == 0) {
                    $novaon_Product->set('sale_price', number_format($pro['compare_price'], 2, '.', '') . ' '.$this->session->data['currency']);
                    $novaon_Product->set('price', number_format($pro['compare_price'], 2, '.', '') . ' '.$this->session->data['currency']);
                } else {
                    $novaon_Product->set('sale_price', number_format($pro['price'], 2, '.', '') . ' '.$this->session->data['currency']);
                    $novaon_Product->set('price', number_format($pro['compare_price'], 2, '.', '') . ' '.$this->session->data['currency']);
                }
                $novaon_Product->setDate('date_created', $pro['date_added']);
                $novaon_Product->setDate('date_modified', $pro['date_modified']);
                $novaon_Product->setDate('date_on_sale_from', $date_on_sale_from);
                $novaon_Product->setDate('date_on_sale_to', $date_on_sale_to);
                $novaon_Product->setInt('stock_quantity', $pro['quantity']);
                $novaon_Product->set('in_stock', (($pro['quantity'] > 0 || $pro['sale_on_out_of_stock'] == 1 ) ? "true" : "false"));
                $novaon_Product->set('weight', (isset($pro['weight'])?$pro['weight']:''));
                $novaon_Product->setDate('availability_date', $pro['date_available']);
                //$novaon_Product->set('description', $pro['description']);
                $novaon_Product->setInt('language_id', (isset($pro['language_id'])?$pro['language_id']:($this->config->get('config_language_id'))));
                //$novaon_Product->setDate('availability_date', $pro['date_available']);
                $dimensions = new novaon_dimensions(['length'=>$pro['length'],'width'=>$pro['width'],'height'=>$pro['height']]);
                $novaon_Product->setObject('dimensions', $dimensions);

                /* push ProductAttributes */
                $ProductAttributes   = $this->model_extension_novaon_product->getProductAttributes($pro['product_id']);
                foreach ($ProductAttributes as $ProductAttribute) {
                    foreach ($ProductAttribute['attribute'] as $att) {
                        $aoptions = $this->model_extension_novaon_product->getProductOptions($pro['product_id']);
                        $attribute_options = [];
                        foreach($aoptions as $v){
                            $novaon_ProductAttributes = new novaon_attributes([
                                'id'=>$v['product_option_id'],
                                'name'=>$v['name'],
                                'position'=>0,
                                'options'=>$v['product_option_value'],
                                'visible'=> intval(isset($v['visible'])?$v['visible']:0),
                                'language_id'=> intval(isset($v['language_id'])?$v['language_id']:($this->config->get('config_language_id'))),
                            ]);
                            array_push($attribute_options, $novaon_ProductAttributes);
                        }
                        $novaon_Product->setArray('attributes', $attribute_options);
                    }
                }


                /* push ProductCategories */
                $categories = $this->model_extension_novaon_product->getCategories($pro['product_id']);
                foreach ($categories as $category) {
                    if (!empty($category['category_id'])) {
                        $ctt = $this->model_extension_novaon_category->getCategory($category['category_id']);
                        $novaon_Product->pushArray('categories', [
                            'id'=>$ctt['category_id'],
                            'name'=>$ctt['name'],
                            'language_id'=>intval(isset($ctt['language_id'])?$ctt['language_id']:($this->config->get('config_language_id')))
                        ]);
                    }
                }

                /* push meta_data */
                $meta_data_trash = $this->model_extension_novaon_product->getProductAttributeforMetadata($pro['product_id'], $pro['version']);
                foreach($meta_data_trash as $v){
                    if(is_array($v) && count($v))
                        $novaon_Product->pushArray('meta_data', $v);
                }
                /* push into global products */
                if (!empty($pro['product_id'])) {
                    $novaon_Products->push($novaon_Product);
                }
            }
        }
        if(isset($_REQUEST['debug']) && $_REQUEST['debug'] == '1'){
            print_r($novaon_Products);
            die();
        }		//print_r($novaon_Products);

        $this->response->setOutput(json_encode($novaon_Products));
    }

    public function categories()
    {
        $this->Credential();
        $this->load->config('novaon/Products');
        $novaon_Categories = new novaon_LCategories;

        $this->load->model('extension/novaon/Products');
        $this->load->model('extension/novaon/category');
        $categories = $this->model_extension_novaon_Products->getallCategory($this->filter_data);
        foreach ($categories as $category) {
            if (!empty($category['category_id'])) {
                $ctt                   = $this->model_extension_novaon_category->getCategory($category['category_id']);

                $ctgr = new novaon_LCategory($ctt);
                $novaon_Categories->push($ctgr);
            }
        }
        if(isset($_REQUEST['debug']) && $_REQUEST['debug'] == '1'){
            print_r($novaon_Categories);
            die();
        }

        $this->response->setOutput(json_encode($novaon_Categories));
    }

    public function languages()
    {
        $this->Credential();
        $this->load->config('novaon/Products');
        //$novaon_LLanguages = new novaon_LLanguages;
        $novaon_LLanguages = new novaon_LLanguages;
        $novaon_LLanguage = '';
        $this->load->model('extension/novaon/language');
        $languages   = $this->model_extension_novaon_language->getLanguages($this->filter_data);
        foreach ($languages as $language) {
            $novaon_LLanguage = new novaon_LLanguage(
                [
                    'language_id' => $language['language_id'],
                    'name' => $language['name'],
                    'code' => $language['code'],
                    'status' => $language['status']
                ]);
            $novaon_LLanguages->push($novaon_LLanguage);
        }
        if(isset($_REQUEST['debug']) && $_REQUEST['debug'] == '1'){
            print_r($novaon_LLanguages);
            die();
        }
        $this->response->setOutput(json_encode($novaon_LLanguages));
    }

    public function attributes()
    {
        $this->Credential();
        // $this->load->config('novaon/Products');
        // $novaon_LAttributes = new novaon_LAttributes;
        // $novaon_LAttribute = '';

        // $this->load->model('extension/novaon/attributes');
        // $Attributes  = $this->model_extension_novaon_attributes->getAttributes($this->filter_data);
        // foreach ($Attributes as $attribute) {
        // print_r($attribute);
        // $novaon_LAttribute = new novaon_LAttribute(
        // [
        // 'id' => $attribute['attribute_id'],
        // 'name' => $attribute['name'],
        // 'language_id' => $attribute['language_id']
        // ]);
        // $novaon_LAttributes->push($novaon_LAttribute);
        // }
        // if(isset($_REQUEST['debug']) && $_REQUEST['debug'] == '1'){
        // print_r($novaon_LAttributes);
        // die();
        // }
        //$this->response->setOutput(json_encode($novaon_LAttributes));

        $this->load->config('novaon/Products');
        $this->load->model('extension/novaon/attributes');
        $aoptions = $this->model_extension_novaon_attributes->getAttributeFromOptions($this->filter_data);		$novaon_LAttributes = new novaon_LAttributes;
        foreach($aoptions as $v){
            $novaon_LAttributes->push(new novaon_LAttribute([										'id'=>$v['id'],										'name'=>$v['name'],										'language_id'=> intval(isset($v['language_id'])?$v['language_id']:($this->config->get('config_language_id'))),									]));
        }
        $this->response->setOutput(json_encode($novaon_LAttributes));
    }

    public function productattributes()
    {
        $this->Credential();
        $this->load->config('novaon/Products');
        $novaon_LProductAttributes = new novaon_LProductAttributes;
        $novaon_LProductAttribute = '';

        $this->load->model('extension/novaon/product');
        $ProductAttributes         = $this->model_extension_novaon_product->getProductAttributes($this->filter);
        //print_r($ProductAttributes);
        foreach ($ProductAttributes as $ProductAttribute) {
            foreach ($ProductAttribute['attribute'] as $att) {
                $novaon_LProductAttribute = new novaon_LProductAttribute(
                    [
                        'attribute_id' => $att['attribute_id'],
                        'name' => $att['name'],
                        'description' => $att['text'],
                        'language_id' => intval($this->config->get('config_language_id'))
                    ]);
                $novaon_LProductAttribute->push($novaon_LProductAttribute);
            }
        }
        if(isset($_REQUEST['debug']) && $_REQUEST['debug'] == '1'){
            print_r($novaon_LProductAttributes);
            die();
        }
        $this->response->setOutput(json_encode($novaon_LProductAttributes));
    }

    public function Credential()
    {
        $this->response->addHeader('Content-Type: application/json');
        $rs = ['success'=>false, 'msg'=>'Consumer_key or Consumer_secret is missing!'];
        $this->load->model('extension/novaon/Credentials');
        //print_R($this);
        $credentials = $this->model_extension_novaon_Credentials->getCredentials();
        if (isset($this->request->get['filter'])) {
            $this->filter = $this->request->get['filter'];
        }
        if(isset($this->request->get['per_page'])) {
            $this->per_page = abs($this->request->get['per_page']);
        }
        if(isset($this->request->get['page'])) {
            $this->page = abs($this->request->get['page']);
        }
        $this->filter_data = array(
            'filter_filter' => '',
            'start' => ($this->page - 1) * $this->per_page,
            'limit' => $this->per_page
        );

        $user = (isset($_SERVER['PHP_AUTH_USER'])?$_SERVER['PHP_AUTH_USER']:(isset($_REQUEST['consumer_key'])?$_REQUEST['consumer_key']:''));
        $pass = (isset($_SERVER['PHP_AUTH_PW'])?$_SERVER['PHP_AUTH_PW']:(isset($_REQUEST['consumer_secret'])?$_REQUEST['consumer_secret']:''));
        if (!isset($credentials[0]) || $credentials[0]['consumer_key'] != $user || $credentials[0]['consumer_secret'] != $pass) {
            header($_SERVER['SERVER_PROTOCOL'] . ' HTTP/1.1 400 Bad Request', true, 400);
            die(json_encode($rs));
        }
    }
}