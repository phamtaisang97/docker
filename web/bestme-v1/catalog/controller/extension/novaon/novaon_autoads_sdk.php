<?php

class ControllerExtensionNovaonNovaonAutoAdsSDK extends Controller
{
    const SDK_TEMPLATE_FILE = 'NovaonAutoAdsSDK.js';

    public function index()
    {
        // get sdk template content
        $template_file_path = DIR_APPLICATION . 'controller/extension/novaon/' . self::SDK_TEMPLATE_FILE;
        $template_content = file_get_contents($template_file_path);

        // replace something
        $output = $template_content;

        // output virtual file
        $this->response->addHeader('Content-Type: application/javascript; charset=utf-8');
        $this->response->setOutput($output);
    }
}