<?php

class ControllerExtensionModuleFlashSale extends Controller
{
    public function index()
    {
        if (isset($this->request->get['module_id'])) {
            $this->load->model('setting/module');
            $module_id = $this->request->get['module_id'];
            $data = $this->model_setting_module->getModule($module_id);
            $data['link_flash_sale'] = array();
            $data['link_flash_sale'] = array(
                'text' => 'Trang Flash Sale',
                'href' => $this->url->link('extension/module/flash_sale/detail', 'user_token=' . $this->session->data['user_token'] . '&module_id=' . $this->request->get['module_id'], true)
            );
            $this->response->setOutput($this->load->view('extension/module/flash_sale', $data));
        }
    }
    public  function detail(){
        $this->load->model('extension/module/flash_sale');
        $this->document->setTitle("Flash sale");
        $flashProduct = $this->model_extension_module_flash_sale->getProductSales();
        $products = array();

        // Get Products to set flash sale
        $this->config->load('bestme_config_db');
        $db_config = $this->config->get('bestme_db');
        $cf_product = $db_config['PRODUCT'];
        $table = $cf_product['NAME'];
        $fields = [
            $cf_product['FIELDS']['PRODUCT'],
            $cf_product['FIELDS']['PRICE'],
            $cf_product['FIELDS']['IMAGE']
        ];

        $this->load->model('api/app_api');
        foreach($flashProduct as $flash){
            $product_id = $flash['product_id'];
            $conditions = [
                $cf_product['FILTER']['PRODUCT'] => $product_id
            ];
            $product = $this->model_api_app_api->getData($table,$fields,$conditions);
            $product['href'] = $this->url->link('product/product', 'product_id=' . $product_id);
            $products[] = $product;
        }
        $data['products'] = $products;
        /* base layout */
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('extension/module/flash_sale_detail', $data));
    }
}