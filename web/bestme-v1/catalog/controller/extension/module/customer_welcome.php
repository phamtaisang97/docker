<?php

class ControllerExtensionModuleCustomerWelcome extends Controller
{
    public function index()
    {
        /*
         * Customer Details:
         *
         * from session data:
         *     - $this->session->data['customer']['customer_id'];
         *     - $this->session->data['customer']['customer_group_id'];
         *     - $this->session->data['customer']['firstname'];
         *     - $this->session->data['customer']['lastname'];
         *     - $this->session->data['customer']['email'];
         *     - $this->session->data['customer']['telephone'];
         *     - ...
         *
         * from current customer (see  /system/library/customer.php):
         *     - $this->customer->isLogged()
         *     - $this->customer->getId()
         *     - $this->customer->getFirstName()
         *     - $this->customer->getLastName()
         *     - $this->customer->getEmail()
         *     - $this->customer->getTelephone()
         *     - ...
         */

        // not work:
        // $this->log->write('[session data] customer info: ' . json_encode($this->session->data['customer'], JSON_PRETTY_PRINT));

        $customerInfo = [
            'id' => $this->customer->getId(),
            'first_name' => $this->customer->getFirstName(),
            'last_name' => $this->customer->getLastName(),
            'email' => $this->customer->getEmail()
        ];
        $this->log->write('[session data] customer info: ' . json_encode($customerInfo, JSON_PRETTY_PRINT));

        // if (!isset($this->session->data['customer']['email'])) {
        if (empty($this->customer->getEmail())) {
            return;
        }

        /* get existing module setting */
        $this->load->model('extension/module/customer_welcome');

        $customer_id = $this->customer->getEmail();
        $existingConfig = $this->model_extension_module_customer_welcome->getConfig($customer_id);

        // check if already show welcome message
        if (!isset($existingConfig['is_shown_welcome_message']) || 1 == $existingConfig['is_shown_welcome_message']) {
            return;
        }

        /* update settings for this module */
        $existingConfig['is_shown_welcome_message'] = 1;

        $this->model_extension_module_customer_welcome->editConfig($customer_id, $existingConfig);

        /* show welcome message */
        $this->load->language('extension/module/customer_welcome');

        $data = array();

        $data['modal_title'] = $this->language->get('modal_title');
        $data['modal_close'] = $this->language->get('modal_close');

        $data['message'] = sprintf($this->language->get('modal_message'), $this->customer->getFirstName());

        return $this->load->view('extension/module/customer_welcome', $data);
    }
}