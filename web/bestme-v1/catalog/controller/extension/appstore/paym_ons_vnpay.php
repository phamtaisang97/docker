<?php

class ControllerExtensionAppstorePaymOnsVNPay extends ControllerExtensionAppstorePaymAbstract
{
    const VNP_APP_CODE = 'paym_ons_vnpay';

    /* error code ReturnURL */
    const VNP_TXNREF_SUCCESS = '00';
    const VNP_ORDER_EXIST_ERROR = '01';
    const VNP_MERCHANT_ERROR = '02';
    const VNP_DATA_ERROR = '03';
    const VNP_WEBSITE_LOCK_ERROR = '04';

    const VNP_PASSWORD_ERROR = '05';
    const VNP_OTP_ERROR = '06';
    const VNP_FAKE_PAYMENT_ERROR = '07';
    const VNP_SERVER_MAINTENANCE_ERROR = '08';
    const VNP_ACCOUNT_NOT_INTERNET_BANKING_ERROR = '09';
    const VNP_AUTH_INFO_ACCOUNT_ERROR = '10';
    const VNP_TIMEOUT_ERROR = '11';
    const VNP_ACCOUNT_LOCKED_ERROR = '12';
    const VNP_TXNREF_ERROR = '20';
    const VNP_CHECKSUM_ERROR = '21';
    const VNP_CANCEL_PAYMENT_ERROR = '24';
    const VNP_ACCOUNT_IS_NOT_ENOUGH_ERROR = '51';
    const VNP_ACCOUNT_EXCEEDING_LIMIT_ERROR = '65';
    const VNP_BANK_MAINTENANCE_ERROR = '75';
    const VNP_OTHER_ERROR = '99';

    /* error code what? */
    const VNP_EXCEPTION_ERROR = '22';
    const VNP_AMOUNT_ERROR = '23';

    /* error code IPN_URL */
    const IPN_CONFIRM_SUCCESS = '00';
    const IPN_ORDER_NOT_FOUND_ERROR = '01';
    const IPN_ORDER_ALREADY_CONFIRMED_ERROR = '02';
    const IPN_IP_INVALID = '03';
    const IPN_TOTAL_ERROR = '04';
    const IPN_INVALID_SIGNATURE_ERROR = '97';

    /* status update order */
    const ORDER_UPDATE_PAYMENT_FALSE = 0;
    const ORDER_UPDATE_PAYMENT_TRUE = 1;
    const ORDER_NO_UPDATE = 2;

    /* bestme order payment status */
    const PAYMENT_STATUS_NOT_PAID = 0;
    const PAYMENT_STATUS_PAID = 1;
    const PAYMENT_STATUS_PROCESSING = 3;
    const PAYMENT_STATUS_FAIL = 4;

    private static $VNP_RETURN_URL_ERROR_MAP = [
        self::VNP_TXNREF_SUCCESS => 'Thanh toán thành công (code: ' . self::VNP_TXNREF_SUCCESS . ')',
        self::VNP_ORDER_EXIST_ERROR => 'Thanh toán lỗi "Giao dịch đã tồn tại" (code: ' . self::VNP_ORDER_EXIST_ERROR . ')',
        self::VNP_MERCHANT_ERROR => 'Thanh toán lỗi "Merchant không hợp lệ (kiểm tra lại vnp_TmnCode)" (code: ' . self::VNP_MERCHANT_ERROR . ')',
        self::VNP_DATA_ERROR => 'Thanh toán lỗi "Dữ liệu gửi sang không đúng định dạng" (code: ' . self::VNP_DATA_ERROR . ')',
        self::VNP_WEBSITE_LOCK_ERROR => 'Thanh toán lỗi "Khởi tạo GD không thành công do Website đang bị tạm khóa" (code: ' . self::VNP_WEBSITE_LOCK_ERROR . ')',

        self::VNP_PASSWORD_ERROR => 'Thanh toán lỗi "Giao dịch không thành công do: Quý khách nhập sai mật khẩu thanh toán quá số lần quy định. Xin quý khách vui lòng thực hiện lại giao dịch" (code: ' . self::VNP_PASSWORD_ERROR . ')',
        self::VNP_OTP_ERROR => 'Thanh toán lỗi "Giao dịch không thành công do Quý khách nhập sai mật khẩu xác thực giao dịch (OTP). Xin quý khách vui lòng thực hiện lại giao dịch" (code: ' . self::VNP_OTP_ERROR . ')',
        self::VNP_FAKE_PAYMENT_ERROR => 'Thanh toán lỗi "Trừ tiền thành công. Giao dịch bị nghi ngờ (liên quan tới lừa đảo, giao dịch bất thường). Đối với giao dịch này cần merchant xác nhận thông qua merchant admin: Từ chối/Đồng ý giao dịch" (code: ' . self::VNP_FAKE_PAYMENT_ERROR . ')',
        self::VNP_SERVER_MAINTENANCE_ERROR => 'Thanh toán lỗi "Giao dịch không thành công do: Hệ thống Ngân hàng đang bảo trì. Xin quý khách tạm thời không thực hiện giao dịch bằng thẻ/tài khoản của Ngân hàng này" (code: ' . self::VNP_SERVER_MAINTENANCE_ERROR . ')',
        self::VNP_ACCOUNT_NOT_INTERNET_BANKING_ERROR => 'Thanh toán lỗi "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng chưa đăng ký dịch vụ InternetBanking tại ngân hàng" (code: ' . self::VNP_ACCOUNT_NOT_INTERNET_BANKING_ERROR . ')',
        self::VNP_AUTH_INFO_ACCOUNT_ERROR => 'Thanh toán lỗi "Giao dịch không thành công do: Khách hàng xác thực thông tin thẻ/tài khoản không đúng quá 3 lần" (code: ' . self::VNP_AUTH_INFO_ACCOUNT_ERROR . ')',
        self::VNP_TIMEOUT_ERROR => 'Thanh toán lỗi "Giao dịch không thành công do: Đã hết hạn chờ thanh toán. Xin quý khách vui lòng thực hiện lại giao dịch" (code: ' . self::VNP_TIMEOUT_ERROR . ')',
        self::VNP_ACCOUNT_LOCKED_ERROR => 'Thanh toán lỗi "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng bị khóa" (code: ' . self::VNP_ACCOUNT_LOCKED_ERROR . ')',

        self::VNP_TXNREF_ERROR => 'Thanh toán lỗi "Order không tồn tại. Xin quý khách vui lòng thực hiện lại giao dịch" (code: ' . self::VNP_TXNREF_ERROR . ')',
        self::VNP_CHECKSUM_ERROR => 'Thanh toán lỗi "" (code: ' . self::VNP_CHECKSUM_ERROR . ')',

        self::VNP_CANCEL_PAYMENT_ERROR => 'Thanh toán lỗi "Giao dịch không thành công do: Khách hàng hủy giao dịch" (code: ' . self::VNP_CANCEL_PAYMENT_ERROR . ')',
        self::VNP_ACCOUNT_IS_NOT_ENOUGH_ERROR => 'Thanh toán lỗi "Giao dịch không thành công do: Tài khoản của quý khách không đủ số dư để thực hiện giao dịch" (code: ' . self::VNP_ACCOUNT_IS_NOT_ENOUGH_ERROR . ')',
        self::VNP_ACCOUNT_EXCEEDING_LIMIT_ERROR => 'Thanh toán lỗi "Giao dịch không thành công do: Tài khoản của Quý khách đã vượt quá hạn mức giao dịch trong ngày" (code: ' . self::VNP_ACCOUNT_EXCEEDING_LIMIT_ERROR . ')',
        self::VNP_BANK_MAINTENANCE_ERROR => 'Thanh toán lỗi "Ngân hàng thanh toán đang bảo trì" (code: ' . self::VNP_BANK_MAINTENANCE_ERROR . ')',

        self::VNP_AMOUNT_ERROR => 'Thanh toán lỗi "Tổng tiền không đúng" (code: ' . self::VNP_AMOUNT_ERROR . ')',
        self::VNP_EXCEPTION_ERROR => 'Thanh toán lỗi "Lỗi hệ thống" (code: ' . self::VNP_EXCEPTION_ERROR . ')',
        self::VNP_OTHER_ERROR => 'Thanh toán lỗi "Lỗi hệ thống" (code: ' . self::VNP_OTHER_ERROR . ')',
    ];

    private static $VNP_IPN_ERROR_MAP = [
        self::IPN_CONFIRM_SUCCESS => 'Confirm Success',
        self::IPN_ORDER_NOT_FOUND_ERROR => 'Order not found',
        self::IPN_ORDER_ALREADY_CONFIRMED_ERROR => 'Order already confirmed',
        self::IPN_IP_INVALID => '',
        self::IPN_TOTAL_ERROR => 'Invalid amount',
        self::IPN_INVALID_SIGNATURE_ERROR => 'Invalid signature',
        self::VNP_CANCEL_PAYMENT_ERROR => 'Cancel payment',
        self::VNP_OTHER_ERROR => 'Unknown error'
    ];

    /**
     * @inheritDoc
     *
     * $data format such as:
     * [
     *     'order_id' => $order_id,
     *     'order_info' => $order_info,
     *     'total_amount' => $total_amount,
     * ];
     */
    public function createPaymentRedirectUrl(array $data)
    {
        $order_id = $data['order_id'];
        $vnp_OrderInfo = $data['order_info'];
        $vnp_Amount = $data['total_amount'];
        $order_code = $data['order_code'];

        $this->load->model('extension/appstore/payment_vnpay');
        $this->load->model('setting/setting');

        $accountPayment = $this->model_extension_appstore_payment_vnpay->getAccountPayment();

        $vnp_TmnCode = $accountPayment->row['vnp_web_code'];
        $vnp_HashSecret = $accountPayment->row['vnp_hash_secret'];

        $vnp_Url = VNPAY_PAYMENT_PAGE_URL;
        // order_id: Mã đơn hàng. Trong thực tế Merchant cần insert đơn hàng vào DB và gửi mã này sang VNPAY

        $vnp_TxnRef = $order_id;

        // noi dung thanh toan=
        $vnp_Locale = 'vn';
        $vnp_IpAddr = $_SERVER['REMOTE_ADDR'];
        // origin url: index.php?route=checkout/order_preview/orderSuccess&payment_app_code={self::VNP_APP_CODE}
        $vnp_Returnurl = HTTPS_SERVER . 'thank-you/' . urlencode($order_code) . '?payment_app_code=' . urlencode(self::VNP_APP_CODE);

        $inputData = [
            "vnp_Version" => "2.1.0",
            "vnp_TmnCode" => $vnp_TmnCode,
            "vnp_Amount" => $vnp_Amount * 100,
            "vnp_Command" => "pay",
            "vnp_CreateDate" => date('YmdHis'),
            "vnp_CurrCode" => "VND",
            "vnp_IpAddr" => $vnp_IpAddr,
            "vnp_Locale" => $vnp_Locale,
            "vnp_OrderInfo" => $this->model_setting_setting->getShopName(),
            "vnp_ReturnUrl" => $vnp_Returnurl,
            "vnp_TxnRef" => $vnp_TxnRef,
        ];

        if (isset($vnp_BankCode) && $vnp_BankCode != "") {
            $inputData['vnp_BankCode'] = $vnp_BankCode;
        }

        ksort($inputData);

        $query = "";
        $i = 0;
        $hashdata = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashdata .= '&' . urlencode($key) . "=" . urlencode($value);
            } else {
                $hashdata .= urlencode($key) . "=" . urlencode($value);
                $i = 1;
            }
            $query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vnp_Url = $vnp_Url . "?" . $query;
        if (isset($vnp_HashSecret)) {
            $vnpSecureHash = hash_hmac('sha512', $hashdata, $vnp_HashSecret);
            $vnp_Url .= 'vnp_SecureHash=' . $vnpSecureHash;
        }

        // log vnpay url
        $this->log->write('vnpay payment url: ' . $vnp_Url);

        return $vnp_Url;
    }

    /**
     * @inheritDoc
     */
    public function getPaymentErrorMessage($app_code)
    {
        return array_key_exists($app_code, self::$VNP_RETURN_URL_ERROR_MAP) ? self::$VNP_RETURN_URL_ERROR_MAP[$app_code] : "Lỗi hệ thống (Không xác định, code: {$app_code}";
    }

    /**
     * @inheritDoc
     */
    public function handleRedirect($data)
    {
        // log data
        $this->log->write('vnpay return url data: ');
        $this->log->write($data);

        $key_for_current_order = "current_order_" . $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());
        $vnp_ResponseCode = $this->request->get['vnp_ResponseCode'];
        $this->load->model('extension/appstore/payment_vnpay');

        // VERY IMPORTANT: TODO: use api... @Huudt
        $this->load->model('sale/order');
        $this->load->model('catalog/product');
        $this->load->model('setting/setting');

        $accountPayment = $this->model_extension_appstore_payment_vnpay->getAccountPayment();
        $vnp_TmnCode = $accountPayment->row['vnp_web_code'];
        $vnp_HashSecret = $accountPayment->row['vnp_hash_secret'];

        // checkSum
        $inputData = [];

        foreach ($data as $key => $value) {
            if (substr($key, 0, 4) == "vnp_") {
                $inputData[$key] = $value;
            }
        }

        $vnp_SecureHash = $inputData['vnp_SecureHash'];
        unset($inputData['vnp_SecureHashType']);
        unset($inputData['vnp_SecureHash']);
        ksort($inputData);
        $i = 0;
        $hashData = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashData = $hashData . '&' . urlencode($key) . "=" . urlencode($value);
            } else {
                $hashData = $hashData . urlencode($key) . "=" . urlencode($value);
                $i = 1;
            }
        }

        $secureHash = hash_hmac('sha512', $hashData, $vnp_HashSecret);
        $orderId = $inputData['vnp_TxnRef'];

        try {
            // Check Orderid
            // Kiểm tra checksum của dữ liệu
            if ($secureHash != $vnp_SecureHash) {
                return [
                    'error' => true,
                    'message' => $this->getPaymentErrorMessage(self::VNP_CHECKSUM_ERROR)
                ];
            }

             /*Lấy thông tin đơn hàng lưu trong Database và kiểm tra trạng thái của đơn hàng, mã đơn hàng là: $orderId
             Việc kiểm tra trạng thái của đơn hàng giúp hệ thống không xử lý trùng lặp, xử lý nhiều lần một giao dịch
             Giả sử: $order = mysqli_fetch_assoc($result);*/

            /*get order*/
            $order = $this->model_sale_order->getOrderByIdAndPaymentMethod($orderId, self::VNP_APP_CODE);
            $domain = $inputData['vnp_OrderInfo'];
            $shop_name = $this->model_setting_setting->getShopName();

            if (!isset($order['order_id']) || $domain != $shop_name) {
                return [
                    'error' => true,
                    'message' => $this->getPaymentErrorMessage(self::VNP_TXNREF_ERROR)
                ];
            }

            /*tổng tiền không đúng*/
            if ((int)$order['total'] * 100 != (int)$data['vnp_Amount']) {
                return [
                    'error' => true,
                    'message' => $this->getPaymentErrorMessage(self::VNP_AMOUNT_ERROR)
                ];
            }

            switch ($vnp_ResponseCode) {
                case self::VNP_TXNREF_SUCCESS:
                    // delete session
                    if (isset($this->session->data[$key_for_current_order])) {
                        unset($this->session->data[$key_for_current_order]);
                    }

                     /*move ipn process
                     update number Transaction for order*/
                    $this->model_sale_order->updateOrderPaymentCode($order['order_id'], $inputData['vnp_TransactionNo']);

                    /* Success */
                    return [
                        'error' => false,
                        'message' => $orderId,
                        'order_code' => $order['order_code']
                    ];

                case self::VNP_ORDER_EXIST_ERROR:
                case self::VNP_MERCHANT_ERROR:
                case self::VNP_DATA_ERROR:
                case self::VNP_WEBSITE_LOCK_ERROR:
                case self::VNP_PASSWORD_ERROR:
                case self::VNP_OTP_ERROR:
                case self::VNP_FAKE_PAYMENT_ERROR:
                case self::VNP_SERVER_MAINTENANCE_ERROR:
                case self::VNP_ACCOUNT_NOT_INTERNET_BANKING_ERROR:
                case self::VNP_AUTH_INFO_ACCOUNT_ERROR:
                case self::VNP_TIMEOUT_ERROR:
                case self::VNP_ACCOUNT_LOCKED_ERROR:
                case self::VNP_CANCEL_PAYMENT_ERROR:
                case self::VNP_ACCOUNT_IS_NOT_ENOUGH_ERROR:
                case self::VNP_ACCOUNT_EXCEEDING_LIMIT_ERROR:
                case self::VNP_BANK_MAINTENANCE_ERROR:
                case self::VNP_OTHER_ERROR:
                    return [
                        'error' => true,
                        'message' => $this->getPaymentErrorMessage($vnp_ResponseCode)
                    ];

                default:
                    return [
                        'error' => true,
                        'message' => $this->getPaymentErrorMessage('-99')
                    ];
            }
        } catch (Exception $e) {
            // Exception
            return [
                'error' => true,
                'message' => $this->getPaymentErrorMessage('-99')
            ];
        }
    }

    /**
     * @inheritDoc
     */
    public function handleS2SCallback($data)
    {
        // info account payment
        $this->load->model('extension/appstore/payment_vnpay');
        $accountPayment = $this->model_extension_appstore_payment_vnpay->getAccountPayment();
        $vnp_HashSecret = $accountPayment->row['vnp_hash_secret'];

        $this->load->model('sale/order');
        $this->load->model('catalog/product');
        $vnp_ResponseCode = $this->request->get['vnp_ResponseCode'];

        // checkSum
        $inputData = [];
        $data = $_REQUEST;
        foreach ($data as $key => $value) {
            if (substr($key, 0, 4) == "vnp_") {
                $inputData[$key] = $value;
            }
        }

        $vnp_SecureHash = $inputData['vnp_SecureHash'];
        unset($inputData['vnp_SecureHashType']);
        unset($inputData['vnp_SecureHash']);
        ksort($inputData);
        $i = 0;
        $hashData = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashData = $hashData . '&' . urlencode($key) . "=" . urlencode($value);
            } else {
                $hashData = $hashData . urlencode($key) . "=" . urlencode($value);
                $i = 1;
            }
        }
        $secureHash = hash_hmac('sha512', $hashData, $vnp_HashSecret);
        $order_code_id = $inputData['vnp_TxnRef'];

        try {
            /* Kiểm tra checksum của dữ liệu */
            if ($secureHash != $vnp_SecureHash) {
                $err_code = self::IPN_INVALID_SIGNATURE_ERROR;

                return [
                    'status' => self::ORDER_NO_UPDATE,
                    'message' => 'Success',
                    'resp_to_server' => [
                        'RspCode' => $err_code,
                        'Message' => self::$VNP_IPN_ERROR_MAP[$err_code]
                    ]
                ];
            }

            // Lấy thông tin đơn hàng lưu trong Database và kiểm tra trạng thái của đơn hàng, mã đơn hàng là: $orderId
            // Việc kiểm tra trạng thái của đơn hàng giúp hệ thống không xử lý trùng lặp, xử lý nhiều lần một giao dịch
            // Giả sử: $order = mysqli_fetch_assoc($result);
            // get order by order id and payment method to avoid update order not via online payment
            $order = $this->model_sale_order->getOrderByIdAndPaymentMethod($order_code_id, self::VNP_APP_CODE);
            /* Check Order */
            if (!isset($order['order_id'])) {
                $err_code = self::IPN_ORDER_NOT_FOUND_ERROR;

                return [
                    'status' => self::ORDER_NO_UPDATE,
                    'message' => 'IPN_ORDER_NOT_FOUND',
                    'resp_to_server' => [
                        'RspCode' => $err_code,
                        'Message' => self::$VNP_IPN_ERROR_MAP[$err_code]
                    ]
                ];
            }

            /* validate order amount */
            if ($order['total'] * 100 != $inputData['vnp_Amount']) {
                $err_code = self::IPN_TOTAL_ERROR;

                return [
                    'status' => self::ORDER_NO_UPDATE,
                    'message' => 'IPN_TOTAL',
                    'resp_to_server' => [
                        'RspCode' => $err_code,
                        'Message' => self::$VNP_IPN_ERROR_MAP[$err_code]
                    ]
                ];
            }

            /*
             * only return code IPN_ORDER_ALREADY_CONFIRMED_ERROR if payment is paid
             * else, return associated error code
             */
            if ($order["payment_status"] == self::PAYMENT_STATUS_PAID) {
                $err_code = self::IPN_ORDER_ALREADY_CONFIRMED_ERROR;

                return [
                    'status' => self::ORDER_NO_UPDATE,
                    'message' => 'IPN_ORDER_ALREADY_CONFIRMED',
                    'resp_to_server' => [
                        'RspCode' => $err_code,
                        'Message' => self::$VNP_IPN_ERROR_MAP[$err_code]
                    ]
                ];
            }

            /* case payment success => return success */
            if ($vnp_ResponseCode == self::VNP_TXNREF_SUCCESS && $inputData['vnp_TransactionStatus'] == self::VNP_TXNREF_SUCCESS) {
                /* success */
                $err_code = self::VNP_TXNREF_SUCCESS;

                return [
                    'status' => self::ORDER_UPDATE_PAYMENT_TRUE,
                    'message' => 'VNP_TXNREF_SUCCESS',
                    'resp_to_server' => [
                        'RspCode' => $err_code,
                        'Message' => self::$VNP_IPN_ERROR_MAP[$err_code]
                    ]
                ];
            }

            /* case cancel payment => return success */
            if ($vnp_ResponseCode == self::VNP_CANCEL_PAYMENT_ERROR) {
                /* success */
                $err_code = self::VNP_TXNREF_SUCCESS;

                return [
                    'status' => self::ORDER_UPDATE_PAYMENT_FALSE,
                    'message' => 'VNP_CANCEL_PAYMENT',
                    'resp_to_server' => [
                        'RspCode' => $err_code,
                        'Message' => self::$VNP_IPN_ERROR_MAP[$err_code]
                    ]
                ];
            }

            /* on order not yet paid, and got other error => return associated error code */
            $err_code = self::VNP_TXNREF_SUCCESS;

            return [
                'status' => self::ORDER_UPDATE_PAYMENT_FALSE,
                'message' => 'Error',
                'resp_to_server' => [
                    'RspCode' => $err_code,
                    'Message' => self::$VNP_IPN_ERROR_MAP[$vnp_ResponseCode]
                ]
            ];

        } catch (Exception $e) {
            // Exception
            $err_code = self::VNP_OTHER_ERROR;

            return [
                'status' => self::ORDER_UPDATE_PAYMENT_FALSE,
                'message' => 'VNP_OTHER_ERROR',
                'resp_to_server' => [
                    'RspCode' => $err_code,
                    'Message' => self::$VNP_IPN_ERROR_MAP[$err_code]
                ]
            ];
        }
    }
}