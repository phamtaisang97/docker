<?php


class ControllerExtensionAppstoreTransOnsGhtk extends ControllerExtensionAppstoreTransAbstract
{
    use Transport_Util_Trait;

    const APP_CODE = 'trans_ons_ghtk';

    const SHIPPING_TRANSPORT_STATUES = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12];
    const COMPLETE_TRANSPORT_STATUES = [11];
    const CANCELED_TRANSPORT_STATUES = [-1, 13, 20, 21];

    const TRANSPORT_STATUSES = [
        'ORDER_STATUS_ID_COMPLETED' => self::COMPLETE_TRANSPORT_STATUES,
        'ORDER_STATUS_ID_CANCELLED' => self::CANCELED_TRANSPORT_STATUES,
        'ORDER_STATUS_ID_DELIVERING' => self::SHIPPING_TRANSPORT_STATUES
    ];

    public static $GHTK_SHIPPING_SERVICES_ORDER_URL = 'https://services.giaohangtietkiem.vn/services/shipment/fee';
    public static $GHTK_GET_HUBS_URL = 'https://services.giaohangtietkiem.vn/services/shipment/list_pick_add';
    public static $GHTK_CREATE_ORDER_URL = 'https://services.giaohangtietkiem.vn/services/shipment/order';
    public static $GHTK_CANCEL_ORDER_URL = 'https://services.giaohangtietkiem.vn/services/shipment/cancel/';

    public static $GHTK_STATUS_ORDER_CANCEL = -1;

    /**
     * @inheritDoc
     */
    public function parseWebhookResponse(array $data)
    {
        return [
            'order_status_id' => $this->getOrderStatus(self::TRANSPORT_STATUSES, $data['status_id']),
            'transport_order_code' => $data['label_id'],
            'transport_status' => $data['status_id']
        ];
    }

    /**
     * @inheritdoc
     */
    public function getTransportStatusMessage($status_code)
    {
        $this->registry->load->language('extension/appstore/trans_ons_ghtk');
        return $this->registry->language->get('ghtk_code_' . $status_code);
    }

    /**
     * @inheritDoc
     */
    public function getListService(array $data)
    {
        $this->registry->load->language('extension/appstore/trans_ons_ghtk');

        $toProvince = $this->getProvinceNameByBestmeProvinceCode($data['transport_shipping_province']);
        $toDistrictID = $this->getDistrictNameByBestmeDistrictCode($data['transport_shipping_district']);

        $pick_address_id = $this->registry->config->get('config_' . self::APP_CODE . '_hub');

        /* shipment */
        $data = array(
            "pick_address_id" => (int)$pick_address_id,
            "province" => $toProvince, // dia chi nhan hang
            "district" => $toDistrictID, // dia chi nhan hang
            "address" => $data['transport_shipping_full_address'],
            "weight" => (int)$data['total_weight'],
            "value" => (int)$data['total_pay'],
            "transport" => '',
            'return_json' => !empty($data['return_json'])
        );

        if ($data['return_json']) {
            $result = [];
            /* transport: fly */
            $data['transport'] = 'fly';
            $item_services = $this->getItemService($data);
            if (!empty($item_services)) {
                $result[] = $item_services;
            }

            /* transport: road */
            $data['transport'] = 'road';
            $item_services = $this->getItemService($data);
            if (!empty($item_services)) {
                $result[] = $item_services;
            }

            return [
                'shipping_unit_name' => $this->getDisplayName(),
                'methods' => $result
            ];
        } else {
            $listItem = '';

            /* transport: fly */
            $data['transport'] = 'fly';
            $listItem .= $this->getItemService($data);

            /* transport: road */
            $data['transport'] = 'road';
            $listItem .= $this->getItemService($data);

            /* end shipment */
            $html = '';
            $html .= '<h5 class="entry-content-title">' . $this->getDisplayName() . '</h5><ul>';
            $html .= $listItem;
            $html .= "</ul>";

            return $html;
        }
    }

    /**
     * @inheritDoc
     */
    public function getDisplayName()
    {
        $this->registry->load->language('extension/appstore/trans_ons_ghtk');
        return $this->registry->language->get('display_name');
    }

    /**
     * @inheritdoc
     */
    public function createOrderDelivery(array $data)
    {
        $this->registry->load->language('sale/order');
        $this->registry->load->language('extension/appstore/trans_ons_ghtk');

        $FromHubID = $this->registry->config->get('config_' . self::APP_CODE . '_hub');

        $data['pick_address_id'] = '';
        $data['pick_tel'] = '';
        $data['address'] = '';
        $data['pick_name'] = '';
        $response = $this->getPickingWarehouse($data, false);
        if ($response->success) {
            $configuredPickingWarehouse = isset($response->data) && is_array($response->data) ? $response->data : [];
            foreach ($configuredPickingWarehouse as $hub) {
                if ($hub->PickAddressId == $FromHubID) {
                    $pick_address = explode(",", $hub->Address);
                    $count_pick_addresses = count($pick_address);
                    $data['pick_address_id'] = $hub->PickAddressId;
                    $data['pick_tel'] = $hub->PickTel;
                    $data['address'] = $hub->Address;
                    $data['pick_name'] = $hub->PickName;
                    $data['pick_province'] = isset($pick_address[$count_pick_addresses - 1]) ? $pick_address[$count_pick_addresses - 1] : '';
                    $data['pick_district'] = isset($pick_address[$count_pick_addresses - 2]) ? $pick_address[$count_pick_addresses - 2] : '';
                    $data['pick_ward'] = isset($pick_address[$count_pick_addresses - 3]) ? $pick_address[$count_pick_addresses - 3] : '';
                }
            }
        }

        /*thong tin khach nhan hang*/
        $province = $this->getProvinceNameByBestmeProvinceCode($data['transport_shipping_province']);
        $district = $this->getDistrictNameByBestmeDistrictCode($data['transport_shipping_district']);
        $ward = $this->getWardNameByBestmeWardCode($data['transport_shipping_wards']);

        if (empty($data['transport_shipping_district']) || empty($data['transport_shipping_wards']) || empty($data['transport_shipping_province'])) {
            return array(
                "status" => false,
                "msg" => $this->registry->language->get('ghtk_transport_error_address')
            );
        }

        $products = [
            [
                'name' => 'Bestme',
                'weight' => (int)$data['total_weight'] / 1000, // unit: g => kg
                'quantity' => 1,
                'product_code' => 'Bestme_product_code'
            ]
        ];

        // add shop_name to order_id
        $this->registry->load->model('setting/setting');
        $shop_name = $this->registry->model_setting_setting->getShopName();
        $bestme_order_code = $shop_name . '.' . $data['shipping_bill']; // notice: use order_code instead of order_id

        $orderData = [
            /*thong tin noi lay hang*/
            "id" => $bestme_order_code,
            // could not use FromHubId to call api. Temp use pick_address instead. TODO: use id...
            "pick_address" => $data['address'],
            "pick_province" => $data['pick_province'],
            "pick_district" => $data['pick_district'],
            "pick_ward" => $data['pick_ward'],
            "pick_name" => $data['pick_name'],
            "pick_tel" => $data['pick_tel'],

            /*thong tin nguoi nhan hang*/
            "tel" => $data['transport_shipping_phone'],
            "name" => $data['transport_shipping_fullname'],
            "address" => $data['shipping_address'],
            "province" => $province,
            "district" => $district,
            "ward" => $ward,
            "hamlet" => "Khác",

            "is_freeship" => "1",
            "pick_money" => (int)$data['collection_amount'],
            "note" => $data['transport_note'],
            "transport" => $data['service_name'],
            "value" => (int)($data['total_pay'] - $data['shipping_fee'])
        ];

        $order = json_encode([
            "products" => $products,
            "order" => $orderData
        ]);

        $create_order_url = defined('GHTK_CREATE_ORDER_URL') ? GHTK_CREATE_ORDER_URL : self::$GHTK_CREATE_ORDER_URL;
        $curl = curl_init();

        $curlopt_httpheader = [
            "Content-Type: application/json",
            "Token: " . $this->getTokenGHTKInternal(),
            "Content-Length: " . strlen($order),
        ];

        // add Bestme-GHTK token refer if defined
        if (defined('TRANSPORT_GHTK_TOKEN') && !empty(TRANSPORT_GHTK_TOKEN)) {
            $curlopt_httpheader[] = "X-Refer-Token: " . TRANSPORT_GHTK_TOKEN;
        }

        curl_setopt_array($curl, [
            CURLOPT_URL => $create_order_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $order,
            CURLOPT_HTTPHEADER => $curlopt_httpheader,
        ]);

        $response = json_decode(curl_exec($curl));
        curl_close($curl);

        if ($response->success) {
            $order_code = $response->order->label;
            $this->registry->load->model('sale/order');
            $data['transport_order_code'] = $order_code;
            $this->registry->model_sale_order->updateTransportDeliveryApi($data['order_id'], $order_code, $data['transport_method'], $data['service_name'], ucfirst($data['service_name']), '', $response->order->status_id, $data['address']);

            return [
                'status' => true,
                'order_code' => $order_code,
                'transport_status' => $response->order->status_id,
                'delivery_address' => $data['address']
            ];
        }

        // TODO: json?...
        return [
            "status" => false,
            "msg" => $response->message
        ];
    }

    /**
     * @inheritdoc
     */
    public function cancelOrder(array $data)
    {
        $cancel_order_url = defined('GHTK_CANCEL_ORDER_URL') ? GHTK_CANCEL_ORDER_URL : self::$GHTK_CANCEL_ORDER_URL;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $cancel_order_url . $data['transport_order_code'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => array(
                "Token: " . $this->getTokenGHTKInternal(),
            ),
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);


        if ($response->success) {
            $this->registry->load->model('sale/order');
            $this->registry->model_sale_order->updateStatusDeliveryApi($data['data_order_id'], self::$GHTK_STATUS_ORDER_CANCEL);

            return [
                'status' => true,
            ];
        }

        $this->registry->load->language('common/delivery_api');

        return [
            "status" => false,
            "msg" => $this->registry->language->get('token_error')
        ];
    }

    /**
     * @inheritdoc
     */
    public function isCancelableOrder($order_status_code)
    {
        return in_array($order_status_code, self::SHIPPING_TRANSPORT_STATUES) || in_array($order_status_code, self::COMPLETE_TRANSPORT_STATUES);
    }

    private function getItemService($data)
    {
        $get_shipping_service_url = defined('GHTK_SHIPPING_SERVICES_ORDER_URL') ? GHTK_SHIPPING_SERVICES_ORDER_URL : self::$GHTK_SHIPPING_SERVICES_ORDER_URL;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $get_shipping_service_url . '?' . http_build_query($data),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => array(
                "Token: " . $this->getTokenGHTKInternal(),
            ),
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);
        /* end shipment */
        if ($data['return_json']) {
            if ($response->success) {
                return [
                    'service_name' => $data['transport'],
                    'delivery_method' => self::APP_CODE,
                    'service_id' => $response->fee->cost_id,
                    'method_name' => $this->getDisplayName() . ' - ' . $data['transport'],
                    'method_fee' => number_format($response->fee->fee, 0, '', ',') . 'đ'
                ];
            } else {
                return [];
            }
        } else {
            $html = '';
            if ($response->success) {
                $html .= '<li class="select-shipping" data-service-name="' . $data['transport'] . '" data-delivery-method="' . self::APP_CODE . '" data-service_id="' . $response->fee->cost_id . '">' . $this->getDisplayName() . ' - ' . $data['transport'] . ' <span class="shipping-price">' . number_format($response->fee->fee, 0, '', ',') . 'đ' . '</span></li>';
            }

            return $html;
        }
    }

    /**
     * getProvinceNameByBestmeProvinceCode
     *
     * @param $bestme_province_code
     * @return false|string|string[]|null
     */
    private function getProvinceNameByBestmeProvinceCode($bestme_province_code)
    {
        // TODO: run offline then add to map file instead off online running...

        /* get bestme provice name */
        $vna = new \Vietnam_Administrative();
        $bestme_province = $vna->getProvinceByCode($bestme_province_code);
        $bestme_province_name = mb_strtolower($bestme_province['name']);

        return $bestme_province_name;
    }

    /**
     * getDistrictNameByBestmeDistrictCode
     *
     * @param int|string $bestme_district_code
     * @param int|string $province_code
     * @return int|string|void|null
     */
    private function getDistrictNameByBestmeDistrictCode($bestme_district_code)
    {
        // TODO: run offline then add to map file instead off online running...

        /* get bestme provice name */
        $vna = new \Vietnam_Administrative();
        $bestme_district = $vna->getDistrictByCode($bestme_district_code);
        $bestme_district_name = mb_strtolower($bestme_district['name']);
        return $bestme_district_name;
    }

    /**
     * @param $bestmeWardCode
     * @param $districtCode
     * @param $token
     * @return mixed|null
     */
    private function getWardNameByBestmeWardCode($bestmeWardCode)
    {
        // TODO: run offline then add to map file instead off online running...

        /* get bestme provice name */
        $vna = new \Vietnam_Administrative();
        $bestme_ward = $vna->getWardByCode($bestmeWardCode);
        $bestme_ward_name = mb_strtolower($bestme_ward['name']);

        return $bestme_ward_name;
    }

    private function getTokenGHTKInternal() {
        $this->registry->load->model('setting/setting');
        $token_ghtk = $this->registry->model_setting_setting->getSettingValue('config_' . self::APP_CODE . '_token');

        return $token_ghtk;
    }

    private function getPickingWarehouse(array $data, $type_html = false)
    {
        $pick_warehouse_url = defined('GHTK_GET_HUBS_URL') ? GHTK_GET_HUBS_URL : self::$GHTK_GET_HUBS_URL;
        $curl = curl_init($pick_warehouse_url);

        curl_setopt_array($curl, array(
            CURLOPT_URL => $pick_warehouse_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => array(
                "Token: " . $this->getTokenGHTKInternal(),
            ),
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);

        $result = new \stdClass();

        $result->code = $response->success ? 1 : 0;
        $result->success = $response->success;
        $result->msg = $response->message;
        $result->data = [];

        if ($response->success == 200) {
            foreach ($response->data as $shop) {
                $hub = new \stdClass();
                $hub->PickAddressId = $shop->pick_address_id;
                $hub->PickTel = $shop->pick_tel;
                $hub->Address = $shop->address;
                $hub->PickName = $shop->pick_name;

                $result->data[] = $hub;
            }
        }

        return $result;
    }
}