<?php

abstract class ControllerExtensionAppstorePaymAbstract extends Controller
{
    private $data;

    public function __construct($registry, $data) {
        parent::__construct($registry);

        $this->data = $data;
    }

    /**
     * @param array $data
     * @return string
     */
    public abstract function createPaymentRedirectUrl(array $data);

    /**
     * @param mixed $app_code
     * @return string
     */
    public abstract function getPaymentErrorMessage($app_code);

    /**
     * handle redirect from web
     *
     * @param array $data
     * @return array format as:
     * [
     *     'error' => true | false,
     *     'message' => <error message if 'error' true> | <the order Id got from callback query params if 'error' is false>
     * ];
     */
    public abstract function handleRedirect($data);

    /**
     * handle Server-to-server callback
     *
     * @param array $data
     * @return array format as:
     * [
     *     'error' => true | false,
     *     'message' => <error message - mixed data>
     * ];
     */
    public abstract function handleS2SCallback($data);
}