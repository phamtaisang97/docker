<?php

abstract class ControllerExtensionAppstoreTransAbstract extends Controller
{
    /**
     * parse webhook response data
     * @param array $data
     */
    public abstract function parseWebhookResponse(array $data);
}