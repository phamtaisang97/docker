<?php
/**
 * Created by PhpStorm.
 * User: Microsoft Windows
 * Date: 29/05/2020
 * Time: 1:16 PM
 */

class ControllerExtensionAppstoreMarOnsLivechatx9 extends Controller
{
    public function getCode()
    {
        $this->load->model('extension/appstore/mar_ons_livechatx9');
        $code = $this->model_extension_appstore_mar_ons_livechatx9->getMarOnsLivechatx9Code();

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($code));
    }
}