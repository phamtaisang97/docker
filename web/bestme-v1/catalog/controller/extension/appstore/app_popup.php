<?php

use App_Api\Product_Field;
use App_Api\Query_App_Builder;
use App_Api\Table_Name;
use App_Api\App_Setting;

class ControllerExtensionAppstoreAppPopup extends Controller
{
    public function index()
    {
        if (isset($this->request->get['module_id'])) {
            $module_id = $this->request->get['module_id'];
            $setting = new App_Setting($this->registry);
            $data = $setting->getModule($module_id);
            $this->response->setOutput($this->load->view('extension/appstore/app_popup', $data));
        }
    }
}