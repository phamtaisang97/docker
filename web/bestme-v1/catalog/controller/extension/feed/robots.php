<?php

class ControllerExtensionFeedRobots extends Controller
{
    const FILE_SITE_MAP = 'robots.txt';

    public function index()
    {
        $robots_content = $this->config->get('online_store_config_robots');

        if ($robots_content) {
            $output = html_entity_decode($robots_content);

            // TODO: avoid wrong html syntax...

            $this->response->addHeader('Content-Type: text/plain');
            $this->response->setOutput($output);
        }
    }
}
