<?php

class ControllerExtensionFeedGoogleSitemap extends Controller
{
    public function index()
    {
        $auto_site_map = $this->config->get('online_store_config_site_map_type');

        if ($auto_site_map == 'manual') {
            $site_map_content = $this->config->get('online_store_config_sitemap');
            $output = html_entity_decode($site_map_content);
            $output = str_replace('&', '&amp;', $output);

            $this->response->addHeader('Content-Type: application/xml');
            $this->response->setOutput($output);
        } else {
            $this->load->model('setting/setting');

            $output = '<?xml version="1.0" encoding="UTF-8"?>';
            $output .= '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

            // get first-level categories
            $output .= $this->getCategoriesSitemapDetailXml();

            // get first-level blog categories
            $output .= $this->getBlogCategoriesSitemapDetailXml();

            $types = ['categories', 'collections', 'blog_categories'];
            foreach ($types as $type) {
                $output .= $this->getSitemapDetailXml($type);
            }

            $output .= '</sitemapindex>';

            $this->response->addHeader('Content-Type: application/xml');
            $this->response->setOutput($output);
        }
    }

    private function getSitemapDetailXml($type) {
        $https_server_url = HTTPS_SERVER;
        return "<sitemap>
                  <loc>{$https_server_url}sitemap/{$type}.xml</loc>
                  <lastmod>{$this->getLastModDate($type)}</lastmod>
            </sitemap>";
    }

    /**
     * Get first-level blog categories
     *
     * @return string
     */
    private function getBlogCategoriesSitemapDetailXml() {
        $this->load->model('blog/category');
        $this->load->model('seo/seo_url');

        $output = '';
        $blog_categories = $this->model_blog_category->getBlogCategoryByParent(0);
        $https_server_url = HTTPS_SERVER;
        $filters = [
            'limit' => 1
        ];

        foreach ($blog_categories as $blog_category) {
            $filters['filter_query'] = 'blog_category_id=' . $blog_category['blog_category_id'];
            $seo_url = $this->model_seo_seo_url->getSeoUrls($filters);
            if ($seo_url) {
                $output .= "<sitemap>
                      <loc>{$https_server_url}sitemap/{$seo_url['keyword']}.xml</loc>
                      <lastmod>{$this->getLastModDate($seo_url['keyword'])}</lastmod>
                </sitemap>";
            }
        }

        return $output;
    }

    /**
     * Get first-level categories
     *
     * @return string
     */
    private function getCategoriesSitemapDetailXml() {
        $this->load->model('catalog/category');
        $this->load->model('seo/seo_url');

        $output = '';
        $categories = $this->model_catalog_category->getCategories();
        $https_server_url = HTTPS_SERVER;
        $filters = [
            'limit' => 1
        ];

        foreach ($categories as $category) {
            $filters['filter_query'] = 'category_id=' . $category['category_id'];
            $seo_url = $this->model_seo_seo_url->getSeoUrls($filters);
            if ($seo_url) {
                $output .= "<sitemap>
                      <loc>{$https_server_url}sitemap/{$seo_url['keyword']}.xml</loc>
                      <lastmod>{$this->getLastModDate($seo_url['keyword'])}</lastmod>
                </sitemap>";
            }
        }

        return $output;
    }

    /**
     * @param $type
     * @return false|string
     */
    private function getLastModDate($type) {
        $lastmod = $this->config->get("sitemap_{$type}_lastmod");
        if (!$lastmod) {
            $lastmod = date('Y-m-d');
            $this->model_setting_setting->editSettingValue('config', "sitemap_{$type}_lastmod", $lastmod);
        }

        return $lastmod;
    }

    /**
     * @return void
     */
    private function outputSitemapDetail($xml_string, $options = [])
    {
        $urlset_image_option = empty($options['image']) ? '' : 'xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"';

        $output = '<?xml version="1.0" encoding="UTF-8"?>';
        $output .= '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"
         xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" ' . $urlset_image_option . '>';

        $output .= $xml_string;

        $output .= '</urlset>';

        $this->response->addHeader('Content-Type: application/xml');
        $this->response->setOutput($output);
    }

    public function categories()
    {
        $xml_string = $this->getCategories(0);

        $this->outputSitemapDetail($xml_string);
    }

    public function manufacturers()
    {
//        $xml_string = $this->getManufacturers();
//
//        $this->outputSitemapDetail($xml_string);
    }

    public function collections()
    {
        $xml_string = $this->getCollections();

        $this->outputSitemapDetail($xml_string);
    }

    public function blogCategories()
    {
        $xml_string = $this->getBlogCategories(0);

        $this->outputSitemapDetail($xml_string);
    }

    public function sitemapDetail()
    {
        $table = $this->request->get['table'];
        $id = $this->request->get['id'];
        $options = [];

        switch ($table) {
            case 'blog_category':
                $xml_string = $this->getBlogs($id);
                $options = ['image' => true];
                break;
            case 'category':
                $xml_string = $this->getProducts($id);
                $options = ['image' => true];
                break;
            default:
                $xml_string = '';
                break;
        }

        $this->outputSitemapDetail($xml_string, $options);
    }

    /**
     * @param $parent_id
     * @param string $current_path
     * @return string
     */
    private function getCategories($parent_id)
    {
        $this->load->model('catalog/category');

        $output = '';

        $results = $this->model_catalog_category->getCategories($parent_id);

        foreach ($results as $result) {
            $category_url = $this->url->link('common/shop', 'path=' . $result['category_id']);
            $output .= '<url>';
            $output .= '  <loc>' . $category_url . '</loc>';
            $output .= '  <changefreq>weekly</changefreq>';
            $output .= '  <priority>0.7</priority>';
            $output .= '</url>';

            $category_url = str_replace('/danh-muc/', '/collections/', $category_url);
            $output .= '<url>';
            $output .= '  <loc>' . $category_url . '</loc>';
            $output .= '  <changefreq>weekly</changefreq>';
            $output .= '  <priority>0.7</priority>';
            $output .= '</url>';

            $output .= $this->getCategories($result['category_id']);
        }

        return $output;
    }

    /**
     * @param $parent_id
     * @param string $current_path
     * @return string
     */
    private function getBlogCategories($parent_id)
    {
        $this->load->model('blog/category');

        $output = '';

        $blog_categories = $this->model_blog_category->getBlogCategoryByParent($parent_id);

        foreach ($blog_categories as $blog_category) {
            $output .= '<url>';
            $output .= '  <loc>' . $this->url->link('blog/blog', 'blog_category_id=' . $blog_category['blog_category_id']) . '</loc>';
            $output .= '  <changefreq>weekly</changefreq>';
            $output .= '  <priority>0.7</priority>';
            $output .= '</url>';

            $output .= $this->getBlogCategories($blog_category['blog_category_id']);
        }

        return $output;
    }

    private function getProducts($parent_category_id)
    {
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');
        $output = '';

        $categories_ids = $this->model_catalog_category->getFullChildCategoryIds($parent_category_id);
        // add parent id to $categories_ids
        $categories_ids[] = $parent_category_id;

        $products = $this->model_catalog_product->getProductsCustomOptimize(['categories_ids' => $categories_ids]);

        foreach ($products as $product) {
            if ($product['image']) {
                $product_url = $this->url->link('product/product', 'product_id=' . $product['product_id']);

                $output .= '<url>';
                $output .= '  <loc>' . $product_url . '</loc>';
                $output .= '  <changefreq>weekly</changefreq>';
                $output .= '  <lastmod>' . date('Y-m-d\TH:i:sP', strtotime($product['date_modified'])) . '</lastmod>';
                $output .= '  <priority>1.0</priority>';
                $output .= '  <image:image>';
                $output .= '  <image:loc>' . $product['image'] . '</image:loc>';
                $output .= '  <image:caption>' . $product['name'] . '</image:caption>';
                $output .= '  <image:title>' . $product['name'] . '</image:title>';
                $output .= '  </image:image>';
                $output .= '</url>';

                $product_url = str_replace('/san-pham/', '/products/', $product_url);
                $output .= '<url>';
                $output .= '  <loc>' . $product_url . '</loc>';
                $output .= '  <changefreq>weekly</changefreq>';
                $output .= '  <lastmod>' . date('Y-m-d\TH:i:sP', strtotime($product['date_modified'])) . '</lastmod>';
                $output .= '  <priority>1.0</priority>';
                $output .= '  <image:image>';
                $output .= '  <image:loc>' . $product['image'] . '</image:loc>';
                $output .= '  <image:caption>' . $product['name'] . '</image:caption>';
                $output .= '  <image:title>' . $product['name'] . '</image:title>';
                $output .= '  </image:image>';
                $output .= '</url>';
            }
        }

        return $output;
    }

    private function getManufacturers()
    {
        $this->load->model('catalog/manufacturer');
        $output = '';

        $manufacturers = $this->model_catalog_manufacturer->getManufacturers([], false);

        foreach ($manufacturers as $manufacturer) {
            $manufacturer_url = $this->url->link('common/shop', 'manufacture=' . $manufacturer['manufacturer_id']);
            $output .= '<url>';
            $output .= '  <loc>' . $manufacturer_url . '</loc>';
            $output .= '  <changefreq>monthly</changefreq>';
            $output .= '  <priority>0.7</priority>';
            $output .= '</url>';

            $manufacturer_url = str_replace('/danh-muc/', '/collections/', $manufacturer_url);
            $output .= '<url>';
            $output .= '  <loc>' . $manufacturer_url . '</loc>';
            $output .= '  <changefreq>monthly</changefreq>';
            $output .= '  <priority>0.7</priority>';
            $output .= '</url>';
        }

        return $output;
    }

    private function getCollections()
    {
        $this->load->model('catalog/collection');
        $output = '';

        $collections = $this->model_catalog_collection->getAllcollection(['status' => 'product']);

        foreach ($collections as $collection) {
            if (!is_array($collection)) {
                continue;
            }

            $collection_url = $this->url->link('common/shop', 'collection=' . $collection['collection_id']);
            $output .= '<url>';
            $output .= '  <loc>' . $collection_url . '</loc>';
            $output .= '  <changefreq>weekly</changefreq>';
            $output .= '  <priority>0.7</priority>';
            $output .= '</url>';

            $collection_url = str_replace('/danh-muc/', '/collections/', $collection_url);
            $output .= '<url>';
            $output .= '  <loc>' . $collection_url . '</loc>';
            $output .= '  <changefreq>weekly</changefreq>';
            $output .= '  <priority>0.7</priority>';
            $output .= '</url>';
        }

        return $output;
    }

    /**
     * @param $parent_category_id
     * @param string $current_path
     * @return string
     */
    private function getBlogs($parent_category_id)
    {
        $this->load->model('blog/category');
        $this->load->model('blog/blog');

        $output = '';

        $categories_ids = $this->model_blog_category->getFullChildCategoryIds($parent_category_id);
        // add parent id to $categories_ids
        $categories_ids[] = $parent_category_id;

        // get blogs
        $blog_filters = ['categories_ids' => $categories_ids];
        $blogs = $this->model_blog_blog->getBlogList($blog_filters);

        foreach ($blogs as $blog) {
            $blog_title = htmlspecialchars($blog['title']);
            $output .= '<url>';
            $output .= '  <loc>' . $this->url->link('blog/blog/detail', 'blog_id=' . $blog['blog_id']) . '</loc>';
            $output .= '  <changefreq>weekly</changefreq>';
            $output .= '  <priority>0.7</priority>';
            $output .= '  <image:image>';
            $output .= '  <image:loc>' . $blog['image'] . '</image:loc>';
            $output .= '  <image:caption>' . $blog_title . '</image:caption>';
            $output .= '  <image:title>' . $blog_title . '</image:title>';
            $output .= '  </image:image>';
            $output .= '</url>';
        }

        return $output;
    }
}
