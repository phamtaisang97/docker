<?php

class ControllerExtensionFeedGoogleShoppingProduct extends Controller
{
    public function index()
    {
        $output = '<?xml version="1.0" encoding="UTF-8"?>';
        $output .= '<feed xmlns="http://www.w3.org/2005/Atom" xmlns:g="http://base.google.com/ns/1.0" xmlns:c="http://base.google.com/cns/1.0">';

        $this->load->model('catalog/product');

        $filter_data = array(
            'show_gg_product_feed' => 1,
        );
        $products = $this->model_catalog_product->getProductsCustomOptimize($filter_data);
        $google_product_category = htmlspecialchars('Business & Industrial > Retail');

        foreach ($products as $product) {
            $output .= '<item>';
            $output .= '<g:id>' . $product['sku'] . '</g:id>';
            $output .= '<title>' . htmlspecialchars($product['name']) . '</title>';
            $output .= '<link>' . $this->url->link('product/product', 'product_id=' . $product['product_id']) . '</link>';

            // get product category
            $categories = array_reverse($this->model_catalog_product->getCategoriesAndParentsForBradcrumd($product['product_id']));
            $categories_text = '';
            $first_category_key = array_key_first($categories);
            foreach ($categories as $key => $category) {
                if ($key != $first_category_key) {
                    $categories_text .= ' &gt; ';
                }
                $categories_text .= $category['name'];
            }
            $output .= '<g:product_type>' . htmlspecialchars($categories_text) . '</g:product_type>';

            $output .= '<g:brand>' . html_entity_decode($product['manufacturer'], ENT_QUOTES, 'UTF-8') . '</g:brand>';
            $output .= '<g:condition>new</g:condition>';

            if ($product['image']) {
                $output .= '  <g:image_link>' . htmlspecialchars($product['image']) . '</g:image_link>';
            } else {
                $output .= '  <g:image_link></g:image_link>';
            }

            $currencies = array(
                'USD',
                'EUR',
                'GBP'
            );
            if (in_array($this->session->data['currency'], $currencies)) {
                $currency_code = $this->session->data['currency'];
                $currency_value = $this->currency->getValue($this->session->data['currency']);
            } else {
                $currency_code = 'USD';
                $currency_value = $this->currency->getValue('USD');
            }
            if ((float)$product['special']) {
                $output .= '  <g:price>' . $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id']), $currency_code, $currency_value, false) . '</g:price>';
            } else {
                $old_price = '';

                if ('1' == $product['multi_versions']) {
                    if (0 != (int)$product['price_version_check_null']) {
                        $price = $product['price_2'];
                        $old_price = $product['compare_price_2'];
                    } else {
                        $price = $product['min_compare_price_version'];
                    }
                } else {
                    if (0 == $product['price_master']) {
                        $price = $product['compare_price_master'];
                    } else {
                        $price = $product['price_master'];
                        $old_price = $product['compare_price_master'];
                    }
                }

                if (empty($old_price)) {
                    $output .= '  <g:price>' . $this->formatPrice($price) . ' VND' . '</g:price>';
                } else {
                    $output .= '  <g:price>' . $this->formatPrice($old_price) . ' VND' . '</g:price>';
                    $output .= '  <g:sale_price>' . $this->formatPrice($price) . ' VND' . '</g:sale_price>';
                }
            }

            // description
            $description_tabs = $this->model_catalog_product->getProductDescriptionTab($product['product_id']);
            $product_description = empty($description_tabs[0]) ? $product['description'] : $description_tabs[0]['description'];
            $product_description = htmlspecialchars(strip_tags(html_entity_decode(html_entity_decode($product_description, ENT_QUOTES, 'UTF-8'), ENT_QUOTES, 'UTF-8')));
            $output .= '<description>' . $product_description . '</description>';

            if ($product['upc']) {
                $output .= '  <g:upc>' . $product['upc'] . '</g:upc>';
            }

            if ($product['ean']) {
                $output .= '  <g:ean>' . $product['ean'] . '</g:ean>';
            }

            $output .= '  <g:availability>in stock</g:availability>';

            if ($product['mpn']) {
                $output .= '  <g:mpn>' . htmlspecialchars($product['mpn']) . '</g:mpn>';
            } else {
                $output .= '  <g:identifier_exists>FALSE</g:identifier_exists>';
            }

            $output .= '  <g:shipping><g:country>VI</g:country><g:price>0 VND</g:price></g:shipping>';
            $output .= '  <g:google_product_category>' . $google_product_category . '</g:google_product_category>';
            $output .= '</item>';
        }

        $output .= '</feed>';

        $this->response->addHeader('Content-Type: application/xml');
        $this->response->setOutput($output);
    }

    private function formatPrice($price) {
        return number_format($price, 0, '.', '');
    }
}
