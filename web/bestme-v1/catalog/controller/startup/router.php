<?php
class ControllerStartupRouter extends Controller {
    use  Setting_Util_Trait;

	public function index() {
		// Route
		if (isset($this->request->get['route']) && $this->request->get['route'] != 'startup/router') {
			$route = $this->request->get['route'];
		} else {
			$route = $this->config->get('action_default');
		}

        $timezone = ini_get('date.timezone') ? ini_get('date.timezone') : date_default_timezone_get();
        $now = new DateTime('now', new DateTimeZone($timezone));

        /* check if shop disabled */
        if (!is_null($this->config->get('config_shop_disabled')) && $this->config->get('config_shop_disabled') == '1'){
            $this->request->get['route'] = 'common/out_of_trial';
            $route = $this->request->get['route'];
        }
        /* check if trial config enabled */
        if (!is_null($this->config->get('config_packet_trial_start')) && !is_null($this->config->get('config_packet_trial_period'))) {
            $data['config_packet_paid'] = $this->config->get('config_packet_paid');
            $data['config_packet_trial_start'] = is_null($this->config->get('config_packet_trial_start')) ? $now->format('Y-m-d') : $this->config->get('config_packet_trial_start');
            $data['config_packet_trial_period'] = is_null($this->config->get('config_packet_trial_period')) ? 0 : $this->config->get('config_packet_trial_period');
            $date_trial_start = DateTime::createFromFormat('Y-m-d', $data['config_packet_trial_start']);
            if ($date_trial_start instanceof DateTime) {
                $interval = $now->diff($date_trial_start)->format("%a");
                $trialDate = $data['config_packet_trial_period'] - $interval;
                if ($data['config_packet_paid'] != 1 && $trialDate < 1) {
                    $this->request->get['route'] = 'common/out_of_trial';
                    $route = $this->request->get['route'];
                }
            }
        }
        /* check if shop expired auto closed */
        if (!is_null($this->config->get('config_packet_paid_end')) ) {
            $date_end =  $this->config->get('config_packet_paid_end');
            $today = date("Y-m-d");
            $interval = strtotime($date_end) - strtotime($today);
            if($interval < 0) {
                $this->request->get['route'] = 'common/out_of_trial';
                $route = $this->request->get['route'];
            }

        }
		
		// Sanitize the call
		$route = preg_replace('/[^a-zA-Z0-9_\/]/', '', (string)$route);

        /* check if not enabled module website */
        $EXCEPT_PERMISSION_ROUTES = [
            'api/pos_v1',
            'api/chatbot_novaon',
            'api/chatbot_novaon_v2',
            'api/v1_common',
            'api/v1_products',
            'api/open_api_v2',
        ];
        $route_elm = explode('/', $route);
        if ($route_elm && count($route_elm) > 2) {
            $route_elm = array_slice($route_elm, 0, 2);
        }

        if (!in_array(implode('/', $route_elm), $EXCEPT_PERMISSION_ROUTES) && !$this->isEnableModule(self::$MODULE_WEBSITE)) {
            $this->request->get['route'] = 'common/out_of_trial';
            $route = $this->request->get['route'];
        }
		
		// Trigger the pre events
		$result = $this->event->trigger('controller/' . $route . '/before', array(&$route, &$data));
		
		if (!is_null($result)) {
			return $result;
		}
		
		// We dont want to use the loader class as it would make an controller callable.
		$action = new Action($route);
		
		// Any output needs to be another Action object.
		$output = $action->execute($this->registry); 
		
		// Trigger the post events
		$result = $this->event->trigger('controller/' . $route . '/after', array(&$route, &$data, &$output));
		
		if (!is_null($result)) {
			return $result;
		}
		
		return $output;
	}
}
