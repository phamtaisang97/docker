<?php

class ControllerStartupSeoUrl extends Controller
{
    // define const for chatbot novaon route
    // such as: /api/chatbot_novaon/xxx
    const CHATBOT_NOVAON_API_PART_1 = 'api';
    const CHATBOT_NOVAON_API_PART_2 = 'chatbot_novaon';
    const CHATBOT_NOVAON_API_PART_3 = [
        'products',
        'orders',
        'verify',
        'provinces',
        'districts',
        'wards'
    ];

    // define const for chatbot novaon_v2 route
    // such as: /api/chatbot_novaon_2/xxx
    const CHATBOT_NOVAON_V2_API_PART_1 = 'api';
    const CHATBOT_NOVAON_V2_API_PART_2 = 'chatbot_novaon_v2';
    const CHATBOT_NOVAON_V2_API_PART_3 = [
        'products',
        'orders',
        'verify',
        'provinces',
        'districts',
        'wards',
        'deliveries',
        'delivery_fee',
        'staffs',
        'categories',
        'manufacturers',
        'customers',
        'payment_methods',
        'shipping_services',
        'delivery_order',
        'user_groups',
        'dashboard'
    ];

    // define const for open api route
    // such as: /api/open_api/xxx
    const OPEN_API_PART_1 = 'api';
    const OPEN_API_PART_2 = 'open_api';
    const OPEN_API_PART_3 = [
        'products',
        'collections',
        'discounts',
        'orders',
        'verify',
        'provinces',
        'districts',
        'wards',
        'deliveries',
        'delivery_fee',
        'staffs',
        'categories',
        'manufacturers',
        'customers',
        'blogs',
        'blog_categories',
        'policy',
        'keyword',
        'customer_group',
        'payment_methods',
        'shipping_services',
        'delivery_order',
        'user_groups',
        'dashboard',
        'setting'
    ];

    const OPEN_API_PART_4 = [
        'index',
        'create',
        'store',
        'show',
        'update',
        'delete',
        'orders',
        'sync',
        'deleteProduct',
        'deleteBlog',
        'deleteCategory'
    ];

    // define const for api (for theme development) route
    // such as: /api/v1_product/xxx
    const API_V1_PART_1 = 'api';
    const API_V1_PART_2 = [
        'v1_common',
        'v1_products',
    ];
    const API_V1_PART_3 = [
        'products',
        'product_detail',
        'product_version',
        'logo',
        'notify_bar',
        'header_menu',
        'categories',
        'slide_show',
        'contact',
        'built_in_pages',
        'home_banner',
        'product_banner',
        'content_pages',
        'partners',
        'address_map',
        'provinces',
        'districts',
        'getTransport',
        'wards',
        'transportFee',
        'suppliers',
        'collections',
        'custom_contents',
        'products_block_list',
        'best_sale_products',
        'hot_products',
        'new_products',
        'best_view_products',
        'blogs',
        'blog_detail',
        'blog_first',
        'header',
        'footer',
        'social',
        'favicon',
    ];

    // define const for pos api route
    // such as: /api/pos_v1/xxx
    const API_POS_V1_PART_1 = 'api';
    const API_POS_V1_PART_2 = [
        'pos_v1',
    ];
    const API_POS_V1_PART_3 = [
        'products',
        'categories',
        'collections',
        'suppliers',
        'manufacturers', // alias of suppliers
        'stores',
        'staffs',
        'provinces',
        'districts',
        'wards',
        'customers',
        'customer_groups',
        'settings',
        'discounts',
        'orders',
        'reports',
        'isPOSEnabled'
    ];

    /* supported seo url */
    const SUPPORTED_ROUTES_FOR_SEO_URL = [
        //'common/home', // replace by "/" instead on /trang-chu as in DB!
        'common/shop',
        "blog/blog",
        'contact/contact',
        'checkout/profile',
        'account/login',
        'account/register',
        'checkout/setting',
        'account/logout',
        'checkout/my_orders',
        'checkout/order_preview'
    ];

    /*
     * support query param in seo url: use format -p<encoded param name>-v<url encoded param value>
     * e.g give the original link is ?route=common/shop&collection=12&is_menu=1
     * then:
     * - ?route=common/shop- => /san-pham
     * - &collection=12 => /a-l-i-a-s-c-o-l-l-e-c-t-i-o-n-12
     * - &is_menu=1 => -p150m3nu001-v1+2
     * where:
     * - is_menu is encoded to 150m3nu001
     * - 1+2 is urlencoded from "1 2"
     *
     * NOTICE: the mapping value FORMAT must be [0-9a-z] with LENGTH must be 10 for regex parsing!
     */
    protected static $SUPPORTED_P = [
        'is_menu' => '150m3nu001',
    ];

    // define const for open_api_v1 route
    // such as: /api/open_api_v1/xxx
    const OPEN_API_V1_PART_1 = 'api';
    const OPEN_API_V1_PART_2 = 'open_api_v1';
    const OPEN_API_V1_PART_3 = [
        'products',
        'orders',
        'verify',
    ];

    // define const for open_api_v2 route
    // such as: /api/open_api_v2/xxx
    const OPEN_API_V2_PART_1 = 'api';
    const OPEN_API_V2_PART_2 = 'open_api_v2';
    const OPEN_API_V2_PART_3 = [
        'products',
        'orders',
        'verify',
        'provinces',
        'districts',
        'wards',
    ];

    const SITEMAP_KEY = 'sitemap';

    const PRODUCT_URL_KEY = 'san-pham';

    public function index()
    {
        // Add rewrite to url class
        if ($this->config->get('config_seo_url')) {
            $this->url->addRewrite($this);
        }

        // Decode URL
        $custom_params = [];

        if (isset($this->request->get['_route_'])) {
            $parts = explode('/', $this->request->get['_route_']);

            // remove any empty arrays from trailing
            if (utf8_strlen(end($parts)) == 0) {
                array_pop($parts);
            }

            /**
             * validate case blog-category-seo-url
             * /blogs/parent-category-url/blog-url
             */
            $tmp_blog_category_id = null;

            $last_parts_key = array_key_last($parts);
            foreach ($parts as $part_key => $part) {
                // extract custom query param
                if (preg_match('/(-p[0-9a-z]{10}+-v[^-]+)/', $part, $matches) === 1) {
                    if (isset($matches)) {
                        $matches = is_array($matches) ? $matches : [$matches];
                        foreach ($matches as $m) {
                            // remove from part to reset part to original
                            $part = str_replace($m, '', $part);

                            // extract param key-value
                            $m = str_replace('-p', '', $m);
                            $m = mb_split('-v', $m);
                            if (count($m) < 2) {
                                continue;
                            }

                            $custom_params[$m[0]] = $m[1];
                        }
                    }
                }

                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url 
                                           WHERE keyword = '" . $this->db->escape($part) . "' 
                                            AND store_id = '" . (int)$this->config->get('config_store_id') . "'
                                            AND language_id = '" . (int)$this->config->get('config_language_id') . "'");
                if ($query->num_rows) {
                    /**
                     * cases:
                     * /{PRODUCT_URL_KEY}/:parent-category-url/:category-url/:product-url
                     * /:product-url
                     * /collections/:parent-category-url/:category-url
                     * /blogs/:parent-category-url/:category-url
                     * /blogs/:parent-category-url/:category-url/:blog-url
                     * /:blog-url
                     * /:page-content-url
                     * => redirect to 404 page
                     */
                    if (($last_parts_key == $part_key) && (
                            ((count($parts) == 1) && in_array($query->row['table_name'], ['product', 'blog', 'category', 'blog_category', 'page_content']))
                            ||
                            ((count($parts) > 2 && in_array($query->row['table_name'], ['product', 'category', 'blog_category']))
                            || (count($parts) != 3 && $query->row['table_name'] == 'blog'))
                        )
                    ) {
                        $this->request->get['route'] = 'error/not_found';
                        break;
                    }

                    $url = explode('=', $query->row['query']);

                    if ($url[0] == 'product_id') {
                        $this->request->get['product_id'] = $url[1];
                    }

                    if ($url[0] == 'blog') {
                        $this->request->get['blog_id'] = $url[1];
                    }

                    if ($url[0] == 'page_id') {
                        $this->request->get['page_id'] = $url[1];
                    }

                    if ($url[0] == 'category_id') {
                        $tmp_blog_category_id = $url[1];
                        if (!isset($this->request->get['path'])) {
                            $this->request->get['path'] = $url[1];
                        } else {
                            $this->request->get['path'] .= '_' . $url[1];
                        }
                    }

                    if ($url[0] == 'collection') {
                        if (!isset($this->request->get['collection'])) {
                            $this->request->get['collection'] = $url[1];
                        } else {
                            $this->request->get['collection'] .= '_' . $url[1];
                        }
                    }

                    // new to formal collection query param
                    if ($url[0] == 'collection_id') {
                        if (!isset($this->request->get['collection'])) {
                            $this->request->get['collection'] = $url[1];
                        } else {
                            $this->request->get['collection'] .= '_' . $url[1];
                        }
                    }

                    if ($url[0] == 'blog_category_id') {
                        $tmp_blog_category_id = $url[1];
                        if (!isset($this->request->get['blog_category_id'])) {
                            $this->request->get['blog_category_id'] = $url[1];
                        } else {
                            $this->request->get['blog_category_id'] .= '_' . $url[1];
                        }
                    }

                    if ($url[0] == 'manufacturer_id') {
						$this->request->get['manufacture'] = $url[1];
					}

                    if ($url[0] == 'information_id') {
                        $this->request->get['information_id'] = $url[1];
                    }

					if ($query->row['query'] && $url[0] != 'blog_category_id' && $url[0] != 'information_id' && $url[0] != 'manufacturer_id' && $url[0] != 'category_id' && $url[0] != 'collection' && $url[0] != 'page_id') {
                        if ($url[0] == 'product_id') {
                            $this->request->get['route'] = 'product/product';
                        } elseif ($url[0] == 'blog') {
                            // check if blog-category-url wrong
                            $this->load->model('blog/blog');
                            $blog_categories = $this->model_blog_blog->getCategoriesAndParents($url[1]);
                            $blog_categories = array_filter($blog_categories, function($v, $k) use ($tmp_blog_category_id) {
                                return '0' == $v['parent_id'] && $v['id'] == $tmp_blog_category_id;
                            }, ARRAY_FILTER_USE_BOTH);
                            if (empty($blog_categories)) {
                                $this->request->get['route'] = 'error/not_found';
                                break;
                            }

                            $this->request->get['route'] = 'blog/blog/detail';
                        } else {
                            $this->request->get['route'] = $query->row['query'];
                        }
					}
				} else {
                    // for integrating autoads - google_shopping
                    if ($parts[0] == 'api' && $parts[1] == 'novaon' && $parts[2] == "v1") {
                        $this->request->get['route'] = 'extension/novaon/' . $parts[1] . '/' . $parts[3];
                        //} elseif ($parts[0] == 'api' && $parts[1] == 'v1' && $parts[2] == "products") {
                    } elseif ($parts[0] == self::CHATBOT_NOVAON_API_PART_1 && $parts[1] == self::CHATBOT_NOVAON_API_PART_2 && in_array($parts[2], self::CHATBOT_NOVAON_API_PART_3)) {
                        $this->request->get['route'] = 'api/chatbot_novaon/' . $parts[2];
                    } elseif ($parts[0] == self::CHATBOT_NOVAON_V2_API_PART_1 && $parts[1] == self::CHATBOT_NOVAON_V2_API_PART_2 && in_array($parts[2], self::CHATBOT_NOVAON_V2_API_PART_3)) {
                        $this->request->get['route'] = 'api/chatbot_novaon_v2/' . $parts[2];
                    } elseif ($parts[0] == self::OPEN_API_PART_1 && $parts[1] == self::OPEN_API_PART_2 && in_array($parts[2], self::OPEN_API_PART_3) && !empty($parts[3]) && in_array($parts[3], self::OPEN_API_PART_4)) {
                        $this->request->get['route'] = 'api/'.self::OPEN_API_PART_2 .'/' . $parts[2] .'/' . $parts[3];
                    } elseif ($parts[0] == self::API_V1_PART_1 && in_array($parts[1], self::API_V1_PART_2) && in_array($parts[2], self::API_V1_PART_3)) {
                        $this->request->get['route'] = 'api/' . $parts[1] . '/' . $parts[2];
                    } elseif ($parts[0] == self::API_POS_V1_PART_1 && in_array($parts[1], self::API_POS_V1_PART_2) && in_array($parts[2], self::API_POS_V1_PART_3)) {
                        $this->request->get['route'] = 'api/' . $parts[1] . '/' . $parts[2];
                    } elseif ($parts[0] == 'api' && $parts[1] == 'transport_viettel_post' && $parts[2] == "webhook") {
                        $this->request->get['route'] = 'api/transport_viettel_post/' . $parts[2];
                    } elseif ($parts[0] == self::OPEN_API_V1_PART_1 && $parts[1] == self::OPEN_API_V1_PART_2 && in_array($parts[2], self::OPEN_API_V1_PART_3)) {
                        $this->request->get['route'] = 'api/open_api_v1/' . $parts[2];
                    } elseif ($parts[0] == self::OPEN_API_V2_PART_1 && $parts[1] == self::OPEN_API_V2_PART_2 && in_array($parts[2], self::OPEN_API_V2_PART_3)) {
                        $this->request->get['route'] = 'api/open_api_v2/' . $parts[2];
                    } elseif (self::SITEMAP_KEY == $parts[0] && 2 == count($parts)) {
                        // eg: /sitemap/danh-muc-mac-dinh-uncategory.xml
                        $part2_arr = explode('.', $parts[1]);
                        if (!empty($part2_arr) && 2 == count($part2_arr) && 'xml' == $part2_arr[1]) {
                            // check if 'danh-muc-mac-dinh-uncategory' is a keyword
                            $this->load->model('seo/seo_url');
                            $seo_url_filters = [
                                'limit' => 1,
                                'filter_keyword' => $part2_arr[0]
                            ];
                            $part_1_seo_url = $this->model_seo_seo_url->getSeoUrls($seo_url_filters);
                            if ($part_1_seo_url) {
                                // check if query contain table entity id
                                $query_arr = explode('=', $part_1_seo_url['query']);
                                if (!empty($query_arr) && 2 == count($query_arr) && is_numeric($query_arr[1])) {
                                    $this->request->get['route'] = 'extension/feed/google_sitemap/sitemapDetail';
                                    $this->request->get['table'] = $part_1_seo_url['table_name'];
                                    $this->request->get['id'] = $query_arr[1];
                                } else {
                                    $this->request->get['route'] = 'error/not_found';
                                }
                            } else {
                                $this->request->get['route'] = 'error/not_found';
                            }
                        }
                    } elseif ('blogs' == $parts[0] && 'all' == $parts[1] && 'all' == $part) {
                        // case blogs page: blogs/all => not handle, already handle in previous route
                        continue;
                    } elseif ('pages' == $parts[0]) {
                        // case page content page: pages/:page-content-seo-url => not handle, will handle in next route
                        continue;
                    } else {
                        $this->request->get['route'] = 'error/not_found';
                    }

                    // original:
                    // $this->request->get['route'] = 'error/not_found';

                     /*
                      * case product detail: /{PRODUCT_URL_KEY}/:product-seo-url
                      * current $part = '{PRODUCT_URL_KEY}' => not break to handle next $part = :product-seo-url
                      *
                      * case blog detail: /blogs/:blog-category-seo-url/:blog-seo-url
                      * current $part = 'blogs' => not break to handle next $part = :blog-seo-url
                      *
                      * case page content: /pages/:page-content-seo-url
                      * current $part = 'pages' => not break to handle next $part = :page-content-seo-url
                      */
                     if (self::PRODUCT_URL_KEY != $part && 'blogs' != $part && 'pages' != $part) {
                        break;
                     }
                }
            }

            if ($parts[0] == 'api' && $parts[1] == 'novaon' && $parts[2] == "v1") {
                $this->request->get['route'] = 'extension/novaon/' . $parts[1] . '/' . $parts[3];
            }

            if (!isset($this->request->get['route'])) {
                if (isset($this->request->get['product_id'])) {
                    $this->request->get['route'] = 'product/product';
                } elseif (isset($this->request->get['path'])) {
                    $this->request->get['route'] = 'product/category';
                } elseif (isset($this->request->get['manufacturer_id'])) {
                    $this->request->get['route'] = 'product/manufacturer/info';
                } elseif (isset($this->request->get['information_id'])) {
                    $this->request->get['route'] = 'information/information';
                } elseif (isset($this->request->get['page_id'])) {
                    $this->request->get['route'] = 'contents/page';
                } elseif (isset($this->request->get['blog_id'])) {
                    $this->request->get['route'] = 'blog/blog/detail';
                }
            }

            // append custom params
            foreach ($custom_params as $p_key => $p_value) {
                $found_p_key = array_search($p_key, self::$SUPPORTED_P);
                if (!$found_p_key) {
                    continue;
                }

                $this->request->get[$found_p_key] = $p_value;
            }
        } elseif (isset($this->request->get['route'])) {
            // If isset seo url go to not found product, blog, category, collection, ...
            $params = $this->request->get;
            unset($params['route']);
            foreach ($params as $key => $param) {
                if ($key == 'product_id') {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product 
                                               WHERE `product_id` = '". (int)$param . "'");
                    if (!$query->num_rows) {
                        $this->request->get['route'] = 'error/not_found';
                        break;
                    }
                }

                if ($key == 'blog') {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "blog 
                                               WHERE `blog_id` = '". (int)$param . "'");
                    if (!$query->num_rows) {
                        $this->request->get['route'] = 'error/not_found';
                        break;
                    }
                }

                // path is known as category
                if ($key == 'path') {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category 
                                               WHERE `category_id` = '". (int)$param . "'");
                    if (!$query->num_rows) {
                        $this->request->get['route'] = 'error/not_found';
                        break;
                    }
                }

                if ($key == 'collection' || $key == 'collection_id') {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "collection 
                                               WHERE `collection_id` = '". (int)$param . "'");
                    if (!$query->num_rows) {
                        $this->request->get['route'] = 'error/not_found';
                        break;
                    }
                }

                if ($key == 'blog_category_id') {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "blog_category 
                                               WHERE `blog_category_id` = '". (int)$param . "'");
                    if (!$query->num_rows) {
                        $this->request->get['route'] = 'error/not_found';
                        break;
                    }
                }

                if ($key == 'manufacturer_id') {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer 
                                               WHERE `manufacturer_id` = '". (int)$param . "'");
                    if (!$query->num_rows) {
                        $this->request->get['route'] = 'error/not_found';
                        break;
                    }
                }
            }
        }
    }

    public function rewrite($link)
    {
        $url_info = parse_url(str_replace('&amp;', '&', $link));

        $url = '';

        $data = array();

        parse_str($url_info['query'], $data);

        foreach ($data as $key => $value) {
            if (isset($data['route'])) {
                if (($data['route'] == 'product/product' && $key == 'product_id') || (($data['route'] == 'product/manufacturer/info' || $data['route'] == 'product/product') && $key == 'manufacturer_id') || ($data['route'] == 'information/information' && $key == 'information_id') || ($data['route'] == 'contents/page' && $key == 'page_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url 
                                               WHERE `query` = '" . $this->db->escape($key . '=' . (int)$value) . "' 
                                                 AND store_id = '" . (int)$this->config->get('config_store_id') . "' 
                                                 AND language_id = '" . (int)$this->config->get('config_language_id') . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $route_prepend = '';
                        if ($data['route'] == 'product/product' && $key == 'product_id') {
                            /**
                             * case product detail: prepend '/{PRODUCT_URL_KEY}'
                             */
                            $route_prepend = '/' . self::PRODUCT_URL_KEY;
                        } elseif ($data['route'] == 'contents/page' && $key == 'page_id' && $query->row['table_name'] == 'page_content') {
                            /**
                             * case page content: prepend '/pages'
                             */
                            $route_prepend = '/pages';
                        }
                        $url .= $route_prepend . '/' . $query->row['keyword'];

						unset($data[$key]);
					}
                } else if ($value == 'common/home') {
                    $url .= '/'; // instead of "/trang-chu" as in DB!
                } else if (in_array($value, self::SUPPORTED_ROUTES_FOR_SEO_URL)) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url 
                                               WHERE `query` = '" . $this->db->escape($value) . "' 
                                                 AND store_id = '" . (int)$this->config->get('config_store_id') . "' 
                                                 AND language_id = '" . (int)$this->config->get('config_language_id') . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        // handle case blog page: blogs/all
                        if ('blog/blog' == $value && empty($data['blog_category_id']) && empty($data['blog_id'])) {
                            $url .= '/' . $query->row['keyword'] . '/all';
                        } else {
                            $url .= '/' . $query->row['keyword'];
                        }
					}
				} elseif ($key == 'path' && !empty($value)) {
					$categories = explode('_', $value);
					foreach ($categories as $category) {
						$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url 
						                           WHERE `query` = 'category_id=" . (int)$category . "' 
						                             AND store_id = '" . (int)$this->config->get('config_store_id') . "' 
						                             AND language_id = '" . (int)$this->config->get('config_language_id') . "'");

						if ($query->num_rows && $query->row['keyword']) {
							$url .= '/' . $query->row['keyword'];
						} else {
							$url = '';
							break;
						}
					}
					unset($data[$key]);
				} else if ($key == 'collection' && !empty($value)) {
                    $collections = explode('_', $value);

                    foreach ($collections as $collection) {
                        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url 
                                                   WHERE `query` = 'collection=" . (int)$collection . "' 
                                                     AND store_id = '" . (int)$this->config->get('config_store_id') . "' 
                                                     AND language_id = '" . (int)$this->config->get('config_language_id') . "'");

                        if ($query->num_rows && $query->row['keyword']) {
                            $url .= '/' . $query->row['keyword'];
                        } else {
                            $url = '';
                            break;
                        }
                    }
                    unset($data[$key]);
                }  else if ($key == 'manufacture' && !empty($value)) {
                    $manufactures = explode('_', $value);
                    foreach ($manufactures as $manufacture) {
                        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url 
                                                   WHERE `query` = 'manufacturer_id=" . (int)$manufacture . "' 
                                                     AND store_id = '" . (int)$this->config->get('config_store_id') . "' 
                                                     AND language_id = '" . (int)$this->config->get('config_language_id') . "'");
                        if ($query->num_rows && $query->row['keyword']) {
                            $url .= '/' . $query->row['keyword'];
                        } else {
                            $url = '';
                            break;
                        }
                    }
                    unset($data[$key]);
                } else if ($data['route'] == 'blog/blog/detail' && $key == 'blog_id' && !empty($value)) {
                    $blog_list = explode('_', $value);
                    foreach ($blog_list as $blog) {
                        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url 
                                                   WHERE `query` = 'blog=" . (int)$blog . "' 
                                                     AND store_id = '" . (int)$this->config->get('config_store_id') . "' 
                                                     AND language_id = '" . (int)$this->config->get('config_language_id') . "'");
                        if ($query->num_rows && $query->row['keyword']) {
                            // get first-level blog category
                            $this->load->model('blog/blog');
                            $categories = $this->model_blog_blog->getCategoriesAndParents($blog);
                            $categories = array_filter($categories, function($v, $k) {
                                return '0' == $v['parent_id'];
                            }, ARRAY_FILTER_USE_BOTH);
                            $category_seo_url = empty($categories[0]) ? '' : $categories[0]['alias'] . '/';

                            $url .= '/blogs/' . $category_seo_url . $query->row['keyword'];
                        } else {
                            $url = '';
                            break;
                        }
                    }
                    unset($data[$key]);
                } else if ($key == 'blog_category_id' && !empty($value)) {
                    $categories = explode('_', $value);
                    foreach ($categories as $category) {
                        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url 
                                                   WHERE `query` = 'blog_category_id=" . (int)$category . "' 
                                                     AND store_id = '" . (int)$this->config->get('config_store_id') . "' 
                                                     AND language_id = '" . (int)$this->config->get('config_language_id') . "'");
                        if ($query->num_rows && $query->row['keyword']) {
                            $url .= '/' . $query->row['keyword'];
                        } else {
                            $url = '';
                            break;
                        }
                    }
                    unset($data[$key]);
                } else if (array_key_exists($key, self::$SUPPORTED_P) && !empty($value)) {
                    $p_name = self::$SUPPORTED_P[$key];
                    $p_value = urlencode($value);
                    $url .= "-p{$p_name}-v{$p_value}";
                    unset($data[$key]);
                }
			}
		}

        if ($url) {
            unset($data['route']);

            $query = '';

            if ($data) {
                foreach ($data as $key => $value) {
                    $query .= '&' . rawurlencode((string)$key) . '=' . rawurlencode((is_array($value) ? http_build_query($value) : (string)$value));
                }

                if ($query) {
                    $query = '?' . str_replace('&', '&amp;', trim($query, '&'));
                }
            }

            return $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') . str_replace('/index.php', '', $url_info['path']) . $url . $query;
        } else {
            return $link;
        }
    }
}
