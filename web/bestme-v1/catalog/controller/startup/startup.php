<?php
class ControllerStartupStartup extends Controller {
    const AVOID_REDIRECT_API_URL = true;

    // see controller/startup/seo_url.php
    private static $API_PART_1_CHECK = [
        'api'
    ];

    // see controller/startup/seo_url.php
    private static $API_PART_2_CHECK = [
        // chatbot
        'chatbot_novaon',
        'chatbot_novaon_v2',
        // theme api - skipped
        //'v1_common',
        //'v1_products',
        // pos
        'pos_v1',
        // TODO: gg shopping?...
    ];

	public function index() {
		// Store
		if ($this->request->server['HTTPS']) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "store WHERE REPLACE(`ssl`, 'www.', '') = '" . $this->db->escape('https://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/') . "'");
		} else {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "store WHERE REPLACE(`url`, 'www.', '') = '" . $this->db->escape('http://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/') . "'");
		}

		if (isset($this->request->get['store_id'])) {
			$this->config->set('config_store_id', (int)$this->request->get['store_id']);
		} else if ($query->num_rows) {
			$this->config->set('config_store_id', $query->row['store_id']);
		} else {
			$this->config->set('config_store_id', 0);
		}

		if (!$query->num_rows) {
			$this->config->set('config_url', HTTP_SERVER);
			$this->config->set('config_ssl', HTTPS_SERVER);
		}

		// Settings
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "setting` WHERE store_id = '0' OR store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY store_id ASC");

		foreach ($query->rows as $result) {
			if (!$result['serialized']) {
				$this->config->set($result['key'], $result['value']);
			} else {
				$this->config->set($result['key'], json_decode($result['value'], true));
			}
		}

		// Theme
		$this->config->set('template_cache', $this->config->get('developer_theme'));

        /* Url: support redirect all traffics to main domain configured by user! */
        // Check if is preview url in admin theme builder config
        // 20200823 - notice: skip if route=preview/home
        $url_query = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
        $is_preview_url = !empty($url_query) &&
            (
                strpos($url_query, 'route=preview/home') !== false ||
                strpos($url_query, 'route=preview/shop') !== false ||
                strpos($url_query, 'route=preview/product') !== false ||
                strpos($url_query, 'route=preview/contact') !== false
            );

        // try again with internal links (call from ajax)
        if (!$is_preview_url) {
            if (isset($_SERVER['HTTP_REFERER'])) {
                $url_query = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY);
                $is_preview_url = !empty($url_query) &&
                    (
                        strpos($url_query, 'route=preview/home') !== false ||
                        strpos($url_query, 'route=preview/shop') !== false ||
                        strpos($url_query, 'route=preview/product') !== false ||
                        strpos($url_query, 'route=preview/contact') !== false
                    );
            }
        }

        // avoid redirecting api url
        $is_need_skipped_redirect_api_url = false;
        if (self::AVOID_REDIRECT_API_URL) {
            $is_need_skipped_redirect_api_url = $this->isNeedSkippedRedirectApiUrl();
        }

        // do redirect
        $skip_redirect = $is_preview_url || $is_need_skipped_redirect_api_url;
        if (!$skip_redirect) {
            $this->load->model('setting/setting');
            $config_domains = $this->model_setting_setting->getSettingValue('config_domains');
            $config_domains = json_decode($config_domains, true);
        } else {
            $config_domains = false;
            //$this->log->write("[Auto redirect domain] skipped redirect. Info: is_preview_url={$is_preview_url}, is_need_skipped_redirect_api_url={$is_need_skipped_redirect_api_url}");
        }

        if (is_array($config_domains) &&
            isset($config_domains['redirect_to_main_domain']) &&
            $config_domains['redirect_to_main_domain'] == true &&
            isset($config_domains['main_domain'])
        ) {
//            $this->log->write('[Auto redirect domain] 1. Enabled config redirect_to_main_domain, main_domain=' . $config_domains['main_domain']);

            /* check if current url is not same configured redirect domain or not */
            $actual_host = "$_SERVER[HTTP_HOST]";
//            $this->log->write('[Auto redirect domain] 2. actual_host=' . $actual_host);
            if (strpos($actual_host, $config_domains['main_domain']) === false) {
                // $this->request->server['HTTPS']: not work, always got "http". TODO: why?...
                // $scheme = $this->request->server['HTTPS'] ? "https" : "http";
                // temp force https. TODO: better way?...
                $scheme = "https";

                $redirect_to = "$scheme://" . $config_domains['main_domain'] . "$_SERVER[REQUEST_URI]";

                $this->log->write('[Auto redirect domain] 3. now redirecting to: ' . $redirect_to);
//                $this->log->write('[Auto redirect domain] 4. header: HTTP/1.1 301 Moved Permanently');
//                $this->log->write('[Auto redirect domain] 5. header: "Location: "' . $redirect_to);

                // do redirect
                ob_start();
                // Permanent 301 redirection
                header("HTTP/1.1 301 Moved Permanently");
                header("Location: " . $redirect_to);
                ob_end_flush();

                exit();
            }
        }

        $this->registry->set('url', new Url($this->config->get('config_url'), $this->config->get('config_ssl')));

		// Language
		$code = '';

		$this->load->model('localisation/language');

		$languages = $this->model_localisation_language->getLanguages();

		if (isset($this->session->data['language'])) {
			$code = $this->session->data['language'];
		}

		if (isset($this->request->cookie['language']) && !array_key_exists($code, $languages)) {
			$code = $this->request->cookie['language'];
		}

		// Language Detection
		if (!empty($this->request->server['HTTP_ACCEPT_LANGUAGE']) && !array_key_exists($code, $languages)) {
			$detect = '';

			$browser_languages = explode(',', $this->request->server['HTTP_ACCEPT_LANGUAGE']);

			// Try using local to detect the language
			foreach ($browser_languages as $browser_language) {
				foreach ($languages as $key => $value) {
					if ($value['status']) {
						$locale = explode(',', $value['locale']);

						if (in_array($browser_language, $locale)) {
							$detect = $key;
							break 2;
						}
					}
				}
			}

			if (!$detect) {
				// Try using language folder to detect the language
				foreach ($browser_languages as $browser_language) {
					if (array_key_exists(strtolower($browser_language), $languages)) {
						$detect = strtolower($browser_language);

						break;
					}
				}
			}

			$code = $detect ? $detect : '';
		}

		if (!array_key_exists($code, $languages)) {
			$code = $this->config->get('config_language');
		}

		if (!isset($this->session->data['language']) || $this->session->data['language'] != $code) {
			$this->session->data['language'] = $code;
		}

		if (!isset($this->request->cookie['language']) || $this->request->cookie['language'] != $code) {
			setcookie('language', $code, time() + 60 * 60 * 24 * 30, '/', $this->request->server['HTTP_HOST']);
		}

		// Overwrite the default language object
		$language = new Language($code);
		$language->load($code);

		$this->registry->set('language', $language);

		// Set the config language_id
        if (array_key_exists($code, $languages)) {
		    $this->config->set('config_language_id', $languages[$code]['language_id']);
        }

        /* hard code. Current not support multi lang for data. TODO: if need multi language, MUST design db and data correctly, also product design MUST be clear... */
        $this->config->set('config_language_id', '2');
        $this->session->data['language']= 'vi-vn';
		// Customer
		$customer = new Cart\Customer($this->registry);
		$this->registry->set('customer', $customer);

		// Customer Group
		if (isset($this->session->data['customer']) && isset($this->session->data['customer']['customer_group_id'])) {
			// For API calls
			$this->config->set('config_customer_group_id', $this->session->data['customer']['customer_group_id']);
		} elseif ($this->customer->isLogged()) {
			// Logged in customers
			$this->config->set('config_customer_group_id', $this->customer->getGroupId());
		} elseif (isset($this->session->data['guest']) && isset($this->session->data['guest']['customer_group_id'])) {
			$this->config->set('config_customer_group_id', $this->session->data['guest']['customer_group_id']);
		}

		// Tracking Code
		if (isset($this->request->get['tracking'])) {
			setcookie('tracking', $this->request->get['tracking'], time() + 3600 * 24 * 1000, '/');

			$this->db->query("UPDATE `" . DB_PREFIX . "marketing` SET clicks = (clicks + 1) WHERE code = '" . $this->db->escape($this->request->get['tracking']) . "'");
		}

		// Currency
		$code = '';

		$this->load->model('localisation/currency');

		$currencies = $this->model_localisation_currency->getCurrencies();

		if (isset($this->session->data['currency'])) {
			$code = $this->session->data['currency'];
		}

		if (isset($this->request->cookie['currency']) && !array_key_exists($code, $currencies)) {
			$code = $this->request->cookie['currency'];
		}

		if (!array_key_exists($code, $currencies)) {
			$code = $this->config->get('config_currency');
		}

		if (!isset($this->session->data['currency']) || $this->session->data['currency'] != $code) {
			$this->session->data['currency'] = $code;
		}

		if (!isset($this->request->cookie['currency']) || $this->request->cookie['currency'] != $code) {
			setcookie('currency', $code, time() + 60 * 60 * 24 * 30, '/', $this->request->server['HTTP_HOST']);
		}

		$this->registry->set('currency', new Cart\Currency($this->registry));

		// Tax
		$this->registry->set('tax', new Cart\Tax($this->registry));

		if (isset($this->session->data['shipping_address']) && isset($this->session->data['shipping_address']['country_id'])) {
			$this->tax->setShippingAddress($this->session->data['shipping_address']['country_id'], $this->session->data['shipping_address']['zone_id']);
		} elseif ($this->config->get('config_tax_default') == 'shipping') {
			$this->tax->setShippingAddress($this->config->get('config_country_id'), $this->config->get('config_zone_id'));
		}

		if (isset($this->session->data['payment_address'])) {
			$this->tax->setPaymentAddress($this->session->data['payment_address']['country_id'], $this->session->data['payment_address']['zone_id']);
		} elseif ($this->config->get('config_tax_default') == 'payment') {
			$this->tax->setPaymentAddress($this->config->get('config_country_id'), $this->config->get('config_zone_id'));
		}

		$this->tax->setStoreAddress($this->config->get('config_country_id'), $this->config->get('config_zone_id'));

		// Weight
		$this->registry->set('weight', new Cart\Weight($this->registry));

		// Length
		$this->registry->set('length', new Cart\Length($this->registry));

		// Cart
		$this->registry->set('cart', new Cart\Cart($this->registry));

		// Encryption
		$this->registry->set('encryption', new Encryption($this->config->get('config_encryption')));

		// OpenBay Pro
		$this->registry->set('openbay', new Openbay($this->registry));

        // migrate
        $migration = new Migration($this->db, $this->log);
        $migration->migrate();

        // Hàm expire app
        $expiration = new Expiration($this->db, $this->log);
        $expiration->expiration("app");

        // delete demo data after active
        $this->deleteDemoData();
	}

    private function deleteDemoData()
    {
        try {
            $config_packet_paid = $this->config->get('config_packet_paid');
            $config_use_demo_data = $this->config->get('config_use_demo_data');
            if ($config_packet_paid && $config_use_demo_data) {
                $this->load->model('demo_data/demo_data');
                $this->model_demo_data_demo_data->removeAllDataDemo();
                $this->db->query("UPDATE `" . DB_PREFIX . "setting` SET `value` = 0 WHERE `key` = 'config_use_demo_data'");
                $this->db->query("INSERT INTO `" . DB_PREFIX . "setting` (`store_id`, `code`, `key`, `value`, `serialized`) 
                                                SELECT '0', 'config', 'config_show_alert_demo_data_deleted', 1, 0 FROM DUAL 
                                                    WHERE NOT EXISTS (SELECT 1 FROM `" . DB_PREFIX . "setting` WHERE `key` = 'config_show_alert_demo_data_deleted')");
            }
        } catch (Exception $e) {
        }
    }

    private function isNeedSkippedRedirectApiUrl()
    {
        if (!isset($this->request->get['_route_']) || empty($this->request->get['_route_'])) {
            return false;
        }

        $parts = explode('/', $this->request->get['_route_']);

        // remove any empty arrays from trailing
        if (utf8_strlen(end($parts)) == 0) {
            array_pop($parts);
        }

        return true &&
            isset($parts[0]) &&
            in_array($parts[0], self::$API_PART_1_CHECK) &&
            isset($parts[1]) &&
            in_array($parts[1], self::$API_PART_2_CHECK);
    }
}
