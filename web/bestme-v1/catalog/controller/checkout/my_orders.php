<?php

class ControllerCheckoutMyOrders extends Controller
{
    use Theme_Config_Util;
    use Product_Util_Trait;

    const LIST_THEMES_SUPPORT_PRODUCT_VERSION_IMAGE = ['food_grocerie', 'tech_sun_electro'];

    public function index()
    {
        $this->load->language('checkout/my_orders');
        $this->load->language('checkout/checkout_nav');
        $this->load->model('tool/image');

        $this->document->setTitle($this->language->get('heading_title'));

        $data = [];

        $data['theme_directory'] = '/catalog/view/theme/' . $this->getCurrentThemeDir();

        $data['breadcrumbs'][] = array(
            'text' => 'Trang chủ',
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => 'Giỏ hàng',
            'href' => '',
            'active' => true,
        );

        $key_for_current_order = "current_order_". $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());
        /* nav links */
        $data['href_profile'] = $this->url->link('checkout/profile', '', true);
        $data['href_my_orders'] = $this->url->link('checkout/my_orders', '', true);
        $data['href_history'] = $this->url->link('checkout/history', '', true);
        $data['href_login'] = $this->url->link('account/login', 'redirect=checkout', true);
        $data['selected_page'] = 'my_orders'; // or 'profile' or 'history'
        $data['delete_all_orders'] = $this->url->link('checkout/my_orders/deleteAll', '', true);
        /* if request is POST from submit form */

        $google_analytics_code = $this->config->get('online_store_config_google_code');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            // add product to current order
            if (!isset($this->session->data[$key_for_current_order])) {
                $this->session->data[$key_for_current_order] = [];
            }

            $order = $this->request->post;
            $order['ordered_at'] = (new DateTime())->format('H:i d/m/Y');

            //update product version
            if (isset($order['product_id']) && isset($order['attribute'])) {
                $this->load->model('catalog/product');
                $version = [];
                if (is_array($order['attribute'])) {
                    foreach ($order['attribute'] as $key => $attribute) {
                        $version[] = $attribute;
                    }
                }
                $product_version = $this->model_catalog_product->getProductByVersionValue($order['product_id'], $version);
                $order['product_version'] = $product_version;
            }

            $current_order = isset($this->session->data[$key_for_current_order]) ? $this->session->data[$key_for_current_order] : [];
            $flag = true;
            foreach ($current_order as $key => $product) {
                if (!isset($product['product_id']) || $product['product_id'] != $this->request->post['product_id']) {
                    continue;
                }

                if (!isset($order['product_version']) ||
                    !isset($order['product_version']['price']) ||
                    $order['product_version']['product_version_id'] == $product['product_version']['product_version_id']
                ) {
                    $flag = false;

                    // update quantity
                    $total_add_quantity = getValueByKey($product, 'quantity', 0) + getValueByKey($order, 'quantity', 0);
                    if ($total_add_quantity > 9999) {
                        $total_add_quantity = 9999;
                    }

                    $this->session->data[$key_for_current_order][$key]['quantity'] = $total_add_quantity;

                    // change price version in current time add cart
                    if (isset($order['product_version']['price']) && isset($product['product_version']['price'])) {
                        $price_version_current = getValueByKey($product['product_version'], 'price', 0);
                        $price_version_new = getValueByKey($order['product_version'], 'price', 0);
                        $price_version = isset($price_version_new) ? $price_version_new : $price_version_current;
                        $this->session->data[$key_for_current_order][$key]['product_version']['price'] = $price_version;
                    }
                }
            }

            if ($flag) {
                $this->session->data[$key_for_current_order][] = $order;
            }

            /* google analytic 4 add_to_cart event */
            if (!empty($google_analytics_code)) {
                $this->load->model('catalog/product');
                $product = $this->model_catalog_product->getProduct($order['product_id']);
                $ga4_items = [
                    'item_id' => $product['sku'],
                    'item_name' => $product['name'],
                    'currency' => "VND",
                    'index' => 0,
                    'item_brand' => $product['manufacturer'],
                    'quantity' => 1
                ];

                foreach ($product['categories'] as $category_key => $category) {
                    $ga4_items['item_category' . (0 == $category_key ? '' : $category_key)] = $category['name'];
                }

                if (!empty($order['product_version'])) {
                    $ga4_items['item_variant'] = $order['product_version']['version'];
                }

                $product_price = $this->calculateProductPrice($product);
                $ga4_items['price'] = (float)$product_price['price'];

                // save to session
                $this->session->data['ga4_add_to_cart'] = [
                    'event' => 'add_to_cart',
                    'items' => [$ga4_items]
                ];
            }

            /*
             * IMPORTANT: need unset "$this->session->data[$key_for_current_order]" on submit order successfully!
             * DO IT in "catalog/controller/sale/order"
             */

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('checkout/my_orders', '', true));
        }

        /* orders */
        $this->load->model('catalog/product');
        $current_order = isset($this->session->data[$key_for_current_order]) ? $this->session->data[$key_for_current_order] : [];
        $products = [];
        $total_amount = 0;
        $total_into_money = 0;
        $current_theme = $this->config->get('config_theme');
        $ga4_view_cart_items = [];
        $ga4_view_cart_item_index = 0;

        $totalQuantityAddOnProduct = 0;
        $totalIntoMoneyAddOnProduct = 0;
        $totalQuantityProductDisplay = 0;

        foreach ($current_order as $key_order => $c_order) {
            $is_stock = true;
            if (!isset($c_order['product_id']) || !isset($c_order['quantity'])) {
                continue;
            }

            // find product
            $product_id = $c_order['product_id'];
            $product = $this->model_catalog_product->getProduct($product_id, false, false);

            $product_quantity_max = $this->getMaxQuantityProduct($product, $c_order);

            $data['is_product_existing'] = true;
            if (empty($product) || !is_array($product)) {
                $data['is_product_existing'] = false;
            }

            // product version and image
            $product_version_id = isset($c_order['product_version']['product_version_id']) ? $c_order['product_version']['product_version_id'] : 0;
            $product_image = $product['image'];
            if ($product_version_id) {
                $product_version = $this->model_catalog_product->getProductVersion($product_version_id);
                if (empty($product_version)) {
                    $this->session->data['error'] = 'Phiên bản sản phẩm đã bị thay đổi, vui lòng chọn lại';
                } else {
                    if (in_array($current_theme, self::LIST_THEMES_SUPPORT_PRODUCT_VERSION_IMAGE)) {
                        $product_image = $product_version['image'];
                    }
                }
            }

            if ($product_image) {
                $image = $this->model_tool_image->resize($product_image, $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'));
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'));
            }

            // format
            $change_data_session_product_version = false;
            if (isset($c_order['attribute']) && $c_order['attribute'] && isset($c_order['product_version']['price'])) {
                $product_version_id = isset($c_order['product_version']['product_version_id']) ? $c_order['product_version']['product_version_id'] : 0;
                $product_version_new = [];

                if (!empty($product_version_id)) {
                    $product_version_new = $this->model_catalog_product->getProductVersionById($product_id, $product_version_id);
                }

                if (empty($product_version_new) || !isset($product_version_new['price']) || !isset($product_version_new['compare_price'])) {
                    $this->session->data['error'] = 'Phiên bản sản phẩm đã bị thay đổi, vui lòng chọn lại';
                }

                if ((int)$c_order['product_version']['price'] != (int)$product_version_new['price']) {
                    $c_order['product_version']['price'] = $product_version_new['price'];
                    $change_data_session_product_version = true;
                }

                if ((int)$c_order['product_version']['price'] == 0 && !empty($product_version_new['discount_id'])) {
                    $c_order['product_version']['price'] = 0;
                } elseif ((int)$c_order['product_version']['price'] == 0) {
                    $c_order['product_version']['price'] = $product_version_new['compare_price'];
                }

                $total = (float)$c_order['product_version']['price'] * (int)$c_order['quantity'];
                $price = (float)$c_order['product_version']['price'];
                $old_price = (float)$c_order['product_version']['compare_price'];
            } else {
                if ((int)$product['price'] == 0 && !empty($product['discount_id'])) {
                    $product['price'] = 0;
                } elseif ((int)$product['price'] == 0) {
                    $product['price'] = $product['compare_price_master'];
                }

                $total = (float)$product['price'] * (int)$c_order['quantity'];
                $price = (float)$product['price'];
                $old_price = (float)$product['compare_price_master'];
            }

            if ($change_data_session_product_version) {
                $this->session->data[$key_for_current_order][$key_order] = $c_order;
            }

            if (!$product_quantity_max) {
                $is_stock = false;
                $this->session->data['error'] = 'Vui lòng bỏ bớt sản phẩm hết hàng ra khỏi giỏ hàng của bạn';
            }

            if($product['multi_versions'] == 1 && !isset($c_order['product_version']['product_version_id'])){
                $is_stock = false;
                $this->session->data['error'] = 'Sản phẩm đã bị thay đổi, vui lòng chọn lại';
            }

            $totalQuantityProductDisplay += 1;
            // count add-on product
            $this->load->model('discount/add_on_deal');
            $deal_data = $this->model_discount_add_on_deal->getAddOnDealByProductId($product_id, $product_version_id);

            $add_on_product_ids = [];
            $add_on_products = [];
            if (!empty($deal_data)) {
                if (!empty($c_order['add_on_products'])) {
                    foreach ($c_order['add_on_products'] as $add_on_product) {
                        $add_on_product_ids[] = $add_on_product['id'];
                    }
                }

                if (!empty($add_on_product_ids)) {
                    $add_on_products = $this->dataAddOnProducts($add_on_product_ids);
                }

                foreach ($add_on_products as $add_on_product) {
                    $totalQuantityAddOnProduct += 1;
                    $totalIntoMoneyAddOnProduct += (float)$add_on_product['sale_price'];
                    $totalQuantityProductDisplay += 1;
                }
            } else {
                $this->session->data[$key_for_current_order][$key_order]['add_on_products'] = [];
            }
            // and add-on product

            //Get product name
            $product_name = isset($product['name']) ? $product['name'] : $c_order['product_name_add_cart'];
            $products[] = [
                'id' => $product_id,
                //Handle tooltip
                'fullname' => strlen($product_name) <= 30 ? '' : $product_name,
                //Handle product name
                'name' => strlen($product_name) <= 30 ? $product_name : mb_substr($product_name, 0, 31, 'UTF-8') . ' ...',
                'ordered_at' => isset($c_order['ordered_at']) ? $c_order['ordered_at'] : (new DateTime())->format('H:i d/m/Y'),
                'image_url' => $this->changeUrl($image, true, 'product_cart'),
                'url' => $this->url->link('product/product', 'product_id=' . $product_id, true),
                'amount' => $c_order['quantity'],
                'total' => $total,
                'attribute' => isset($c_order['attribute']) ? implode(' / ', $c_order['attribute']) : '',
                'price' => $price,
                'old_price_formart' => $this->currency->format($this->tax->calculate($old_price, $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
                'price_formart' => $this->currency->format($this->tax->calculate($price, $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
                'total_formart' => $this->currency->format($this->tax->calculate($total, $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
                'product_version_id' => $product_version_id,
                'attributes' => isset($c_order['attribute']) ? $c_order['attribute'] : [],
                'product_quantity_max' => 9999,
                'in_stock' => $is_stock,
                'add_on_products' => $add_on_products,
            ];

            // for overall order
            $total_amount += $c_order['quantity'];
            $total_into_money += $total;

            /* google analytic 4 view_cart event */
            $ga4_view_cart_item = [
                'item_id' => $product['sku'],
                'item_name' => $product['name'],
                'currency' => "VND",
                'index' => $ga4_view_cart_item_index++,
                'item_brand' => $product['manufacturer'],
                'quantity' => $c_order['quantity']
            ];

            foreach ($product['categories'] as $category_key => $category) {
                $ga4_view_cart_item['item_category' . (0 == $category_key ? '' : $category_key)] = $category['name'];
            }

            if (!empty($c_order['product_version'])) {
                $ga4_view_cart_item['item_variant'] = $c_order['product_version']['version'];
            }

            $ga4_view_cart_item['price'] = $price;
            $ga4_view_cart_items[] = $ga4_view_cart_item;
        }

        $all_amount = $total_amount + $totalQuantityAddOnProduct;
        $all_into_money = $total_into_money + $totalIntoMoneyAddOnProduct;

        $data['order'] = [
            'products' => $products,
            'id' => null,
            'amount' => $all_amount,
            'count_product' => $totalQuantityProductDisplay,
            'into_money' => $all_into_money,
            'into_money_formart' => $this->currency->format($this->tax->calculate($all_into_money, 0, $this->config->get('config_tax')), $this->session->data['currency']),
        ];

        /* google analytic 4 add_to_cart event: get data from session (for case buy now) */
        if (!empty($this->session->data['ga4_add_to_cart'])) {
            $data['ga4_add_to_cart'] = json_encode($this->session->data['ga4_add_to_cart'], JSON_HEX_APOS);
            $this->session->data['ga4_add_to_cart'] = null;
        }

        /* google analytic 4 view_cart event */
        if (!empty($google_analytics_code)) {
            // save to session
            $data['ga4_view_cart'] = [
                'event' => 'view_cart',
                'ecommerce' => [
                    'currency' => 'VND',
                    'value' => $total_into_money,
                    'items' => $ga4_view_cart_items
                ]
            ];
            $data['ga4_view_cart'] = json_encode($data['ga4_view_cart'], JSON_HEX_APOS);
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            $this->session->data['success'] = '';
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->session->data['error'])) {
            $data['error'] = $this->session->data['error'];
            $this->session->data['error'] = '';
            unset($this->session->data['error']);
        } else {
            $data['error'] = '';
        }

        $data['order_products'] = $products;

        /* button go to order preview */
        $data['href_order_preview'] = $this->url->link('checkout/order_preview', '', true);
        $data['href_common_shop'] = $this->url->link('common/shop', '', true);

        /* get currency symbol */
        $data['currency'] = $this->currency->getSymbolRight($this->session->data['currency']);
        if (empty($data['currency'])) {
            $data['currency'] = $this->currency->getSymbolLeft($this->session->data['currency']);
        }
        $data['currency'] = 'đ';
        //get banner product list
        $this->load->model('extension/module/theme_builder_config');
        $data['banners'] =  $this->getBanner()['banners_visible'];

        $data['customer_is_login'] = $this->customer->isLogged() ? true : false;
        /* nav base layout */
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        $data['is_on_mobile'] = $this->isMobile();
        $this->response->setOutput($this->load->view('checkout/my_orders', $data));
    }

    public function deleteAll()
    {
            $key_for_current_order = "current_order_" . $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());
            $this->load->language('checkout/my_orders');
                $current_order = isset($this->session->data[$key_for_current_order]) ? $this->session->data[$key_for_current_order] : [];
                $current_order = [];
                $this->session->data[$key_for_current_order] = [];
            $this->response->redirect($this->url->link('checkout/my_orders'));
    }
    private function getBanner()
    {
        $data = array();
        $banners = $this->model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_CATEGORY_BANNER);
        $banners = json_decode($banners, true);
        if ($banners['display'][0]['visible'] == 1) {
            $data['banners_visible'] = array(
                'url_banner' => $banners['display'][0]['url'],
                'url_image' => $banners['display'][0]['image-url'],
                'visible_banner' => $banners['display'][0]['visible'],
            );
        }
        if(!$data) {
            $data['banners_visible'] = array(
                'visible_banner' => 0,
            );
        }
        return $data;
    }

    public function addCart()
    {
        $key_for_current_order = "current_order_". $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());

        $json = array();
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            // add product to current order
            if (!isset($this->session->data[$key_for_current_order])) {
                $this->session->data[$key_for_current_order] = [];
            }

            $order = $this->request->post;
            $order['ordered_at'] = (new DateTime())->format('H:i d/m/Y');

            $this->load->model('catalog/product');
            //update product version
            if (isset($order['attribute'])) {
                $version = [];
                foreach ($order['attribute'] as $key => $attribute) {
                    $version[] = $attribute;
                }
                $product_version = $this->model_catalog_product->getProductByVersionValue($order['product_id'], $version);
                $order['product_version'] = $product_version;
            }

            $current_order = isset($this->session->data[$key_for_current_order]) ? $this->session->data[$key_for_current_order] : [];
            $flag = true;

            foreach ($current_order as $key => $product) {
                if (!isset($product['product_id']) || $product['product_id'] != $this->request->post['product_id']) {
                    continue;
                }

                if (!isset($order['product_version']) ||
                    !isset($order['product_version']['price']) ||
                    (
                        isset($order['product_version']['product_version_id']) &&
                        isset($product['product_version']['product_version_id']) &&
                        $order['product_version']['product_version_id'] == $product['product_version']['product_version_id']
                    )
                ) {
                    $total_add_quantity = getValueByKey($product, 'quantity', 0) + getValueByKey($order, 'quantity', 0);
                    if ($total_add_quantity > 9999) {
                        $total_add_quantity = 9999;
                    }

                    // change price version in current time add cart
                    if (isset($order['product_version']['price']) && isset($product['product_version']['price'])) {
                        $price_version_current = getValueByKey($product['product_version'], 'price', 0);
                        $price_version_new = getValueByKey($order['product_version'], 'price', 0);
                        $price_version = isset($price_version_new) ? $price_version_new : $price_version_current;
                        $this->session->data[$key_for_current_order][$key]['product_version']['price'] = $price_version;
                    }

                    $this->session->data[$key_for_current_order][$key]['quantity'] = $total_add_quantity;

                    if (!empty($order['chooseAddOnProduct'])) {
                        $add_on_products = $this->dataAddOnProducts($order['chooseAddOnProduct']);
                        $this->session->data[$key_for_current_order][$key]['add_on_products'] = $add_on_products;
                        $this->session->data[$key_for_current_order][$key]['chooseAddOnProduct'] = $order['chooseAddOnProduct'];
                    } else {
                        unset($this->session->data[$key_for_current_order][$key]['add_on_products']);
                        unset($this->session->data[$key_for_current_order][$key]['chooseAddOnProduct']);
                    }
                    $flag = false;
                }
            }

            if ($flag) $this->session->data[$key_for_current_order][] = $order;

            $current_order = isset($this->session->data[$key_for_current_order]) ? $this->session->data[$key_for_current_order] : [];
            $total_into_money = 0;
            $total_product_cart = 0;

            $totalQuantityAddOnProduct = 0;
            $totalIntoMoneyAddOnProduct = 0;

            foreach ($current_order as $key_order => $c_order) {
                if (!isset($c_order['product_id']) || !isset($c_order['quantity'])) {
                    continue;
                }

                // find product
                $product_id = $c_order['product_id'];
                $product = $this->model_catalog_product->getProduct($product_id);

                if (isset($c_order['attribute']) && $c_order['attribute']) {
                    if ((int)$c_order['product_version']['price'] == 0){
                        if (!isset($c_order['product_version']['compare_price'])){
                            continue;
                        }

                        if (!empty($c_order['product_version']['discount_id'])) {
                            $c_order['product_version']['price'] = 0;
                        } else {
                            $c_order['product_version']['price'] = (float)$c_order['product_version']['compare_price'];
                        }
                    }

                    $price = (float)$c_order['product_version']['price'];
                } else {
                    if ((int)$product['price'] == 0 && !empty($product['discount_id'])) {
                        $product['price'] = 0;
                    } elseif ((int)$product['price'] == 0) {
                        $product['price'] = $product['compare_price_master'];
                    }

                    $price = (float)$product['price'];
                }

                $data['is_product_existing'] = true;
                if (empty($product) || !is_array($product)) {
                    $data['is_product_existing'] = false;
                }

                // add-on products
                $c_order['add_on_products'] = [];
                if (!empty($c_order['chooseAddOnProduct'])) {
                    $c_order['add_on_products'] = $this->dataAddOnProducts($c_order['chooseAddOnProduct']);
                }

                if (!empty($c_order['add_on_products'])) {
                    foreach ($c_order['add_on_products'] as $add_on_product) {
                        $totalQuantityAddOnProduct += 1;
                        $totalIntoMoneyAddOnProduct += (float)$add_on_product['sale_price'];
                    }

                    $this->session->data[$key_for_current_order][$key_order]['add_on_products'] = $c_order['add_on_products'];
                }
                // end add-on products

                $total = $price * $c_order['quantity'];
                $total_into_money += $total;
                $total_product_cart += $c_order['quantity'];
            }
            $total_product_cart = $total_product_cart + $totalQuantityAddOnProduct;
            $total_into_money = $total_into_money + $totalIntoMoneyAddOnProduct;

            $json['total_product_cart'] = $total_product_cart > 99 ? '99+' : $total_product_cart;
            $json['success'] = true;
            $json['total_into_money'] = $this->currency->format($this->tax->calculate($total_into_money, 0, $this->config->get('config_tax')), $this->session->data['currency']);

            /* google analytic 4 add_to_cart event */
            $google_analytics_code = $this->config->get('online_store_config_google_code');
            if (empty($google_analytics_code)) {
                $json['ga4_add_to_cart'] = null;
            } else {
                $product = $this->model_catalog_product->getProduct($order['product_id']);
                $ga4_items = [
                    'item_id' => $product['sku'],
                    'item_name' => $product['name'],
                    'currency' => "VND",
                    'index' => 0,
                    'item_brand' => $product['manufacturer'],
                    'quantity' => (int)$order['quantity']
                ];

                foreach ($product['categories'] as $category_key => $category) {
                    $ga4_items['item_category' . (0 == $category_key ? '' : $category_key)] = $category['name'];
                }

                if (!empty($order['product_version'])) {
                    $ga4_items['item_variant'] = $order['product_version']['version'];
                }

                $product_price = $this->calculateProductPrice($product);
                $ga4_items['price'] = (float)$product_price['price'];

                $json['ga4_add_to_cart'] = [
                    'event' => 'add_to_cart',
                    'items' => [$ga4_items]
                ];
            }
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    protected function validateForm()
    {
        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

    public function remove_order_products()
    {
        $key_for_current_order = "current_order_". $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());
        $this->load->language('checkout/my_orders');

        $json = [
            'status' => false,
            'message' => $this->language->get('text_remove_fail'),
        ];
        if (isset($this->request->post['product_id'])) {
            $current_order = isset($this->session->data[$key_for_current_order]) ? $this->session->data[$key_for_current_order] : [];
            foreach ($current_order as $k => $product) {
                if ($product['product_id'] == $this->request->post['product_id']) {
                    if (empty(getValueByKey($this->request->post, 'product_version_id')) ||
                        (
                            isset($this->request->post['product_version_id']) &&
                            isset($product['product_version']['product_version_id']) &&
                            $this->request->post['product_version_id'] == $product['product_version']['product_version_id']
                        )
                    ) {
                        unset($current_order[$k]);
                        if (!empty($product['add_on_products'])) {
                            unset($current_order[$k]['add_on_products']);
                        }
                    }
                }
            }
            $this->session->data[$key_for_current_order] = $current_order;

            $json = [
                'status' => true,
                'message' => $this->language->get('text_remove_success'),
            ];
            $this->response->setOutput(json_encode($json));
        }

        $this->response->setOutput(json_encode($json));
    }

    public function removeAddOnProductInOrder()
    {
        $json = [
            'status' => false,
            'message' => $this->language->get('text_remove_fail'),
        ];

        $product_main_id = isset($this->request->post['product_main_id']) ? $this->request->post['product_main_id'] : 0;
        $product_id = isset($this->request->post['product_id']) ? $this->request->post['product_id'] : 0;
        $product_version_id = isset($this->request->post['product_version_id']) ? $this->request->post['product_version_id'] : 0;

        if (empty($product_main_id) || empty($product_id)) {
            $this->response->setOutput(json_encode($json));
            return;
        }

        $key_for_current_order = "current_order_". $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());
        $this->load->language('checkout/my_orders');

        $current_order = isset($this->session->data[$key_for_current_order]) ? $this->session->data[$key_for_current_order] : [];
        foreach ($current_order as $keyP => $product) {
            if ($product['product_id'] != $product_main_id || empty($product['add_on_products'])) {
                continue;
            }

            foreach ($product['add_on_products'] as $keyAoP => $add_on_product) {
                if ($add_on_product['product_id'] == $product_id &&
                    $add_on_product['product_version_id'] == $product_version_id) {
                    unset($current_order[$keyP]['add_on_products'][$keyAoP]);
                }
            }
        }

        $this->session->data[$key_for_current_order] = $current_order;

        $json = [
            'status' => true,
            'message' => $this->language->get('text_remove_success'),
        ];

        $this->response->setOutput(json_encode($json));
    }

    public function update_order_products()
    {
        $key_for_current_order = "current_order_". $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());
        $this->load->language('checkout/my_orders');

        $json = [
            'status' => false,
            'message' => $this->language->get('text_remove_fail'),
        ];

        if (isset($this->request->post['product_id']) && isset($this->request->post['quantity'])) {
            $current_order = isset($this->session->data[$key_for_current_order]) ? $this->session->data[$key_for_current_order] : [];

            foreach ($current_order as &$product) {
                if (!array_key_exists('product_id', $product)) {
                    continue;
                }

                if ($product['product_id'] == $this->request->post['product_id']) {
                    $quantity = (int)$this->request->post['quantity'] >= 0 ? (int)$this->request->post['quantity'] : 0;
                    if(empty($this->request->post['product_version_id']) ||
                        (isset($product['product_version']['product_version_id']) && $this->request->post['product_version_id'] == $product['product_version']['product_version_id'])) {
                        $product['quantity'] = $quantity;
                    }
                }
            }
            unset($product);
            $this->session->data[$key_for_current_order] = $current_order;

            $currentOrderInfoData = $this->getCurrentOrderInfo();

            $json = [
                'status' => true,
                'message' => $this->language->get('text_remove_success'),
                'data' => $currentOrderInfoData
            ];
            $this->response->setOutput(json_encode($json));
        }

        $this->response->setOutput(json_encode($json));
    }

    public function getCurrentOrder()
    {
        $key_for_current_order = "current_order_". $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());
        $json = array();
        $current_order = isset($this->session->data[$key_for_current_order]) ? $this->session->data[$key_for_current_order] : [];
        $total_into_money = 0;
        $total_product_cart = 0;
        foreach ($current_order as $c_order) {
            if (!isset($c_order['product_id']) || !isset($c_order['quantity'])) {
                continue;
            }

            // find product
            $product_id = $c_order['product_id'];
            $this->load->model('catalog/product');
            $product = $this->model_catalog_product->getProduct($product_id);

            if (isset($c_order['attribute']) && $c_order['attribute']) {
                if (isset($c_order['product_version']['price']) && (int)$c_order['product_version']['price'] == 0) {
                    $c_order['product_version']['price'] = isset($c_order['product_version']['compare_price']) ? $c_order['product_version']['compare_price'] : 0;
                }

                $price = isset($c_order['product_version']['price']) ? (float)$c_order['product_version']['price'] : 0;
            } else {
                if (isset($product['price']) && (int)$product['price'] == 0) {
                    $product['price'] = isset($product['compare_price_master']) ? $product['compare_price_master'] : 0;
                }

                $price = isset($product['price']) ? (float)$product['price'] : 0;
            }

            $data['is_product_existing'] = true;
            if (empty($product) || !is_array($product)) {
                $data['is_product_existing'] = false;
            }

            $total = $price * $c_order['quantity'];
            $total_into_money += $total;
            $total_product_cart += $c_order['quantity'];
        }
        $json['total_product_cart'] = $total_product_cart  > 99 ? '99+' : $total_product_cart;
        $json['success'] = true;
        $json['total_into_money'] = $this->currency->format($this->tax->calculate($total_into_money, 0, $this->config->get('config_tax')), $this->session->data['currency']);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }

    public function getMaxQuantityProduct($product, $order)
    {
        $this->load->model('catalog/product');
        if ($product['multi_versions'] == 1) {
            if (!isset($order['product_version']['product_version_id'])){
                return false;
            }

            $product_is_stock = $this->model_catalog_product->isStock($product['product_id'], $order['product_version']['product_version_id']);
            $product_version = $this->model_catalog_product->getProductByVersionId($product['product_id'], $order['product_version']['product_version_id']);
            if ($product_version && $product['status'] == 1 &&
                $product_version['deleted'] != 1 &&
                $product_version['status'] == 1 &&
                ($product_is_stock || $product['sale_on_out_stock'] == 1)
            ) {
                return true;
            } else {
                return false;
            }
        }

        $product_is_stock_single = $this->model_catalog_product->isStock($product['product_id'], null);
        if ($product['multi_versions'] == 0 &&
            $product['deleted'] != 1 &&
            $product['status'] == 1 &&
            ($product_is_stock_single || $product['sale_on_out_stock'] == 1)
        ) {
            return true;
        }

        return false;
    }

    public function ajaxGetOrderInfo ()
    {
        $currentOrderInfoData = $this->getCurrentOrderInfo();

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($currentOrderInfoData));
    }

    /* TODO: use getCurrentOrder()?... */
    private function getCurrentOrderInfo ()
    {
        $this->load->language('checkout/my_orders');
        $this->load->language('checkout/checkout_nav');
        $this->load->model('tool/image');


        $data = [];

        $key_for_current_order = "current_order_". $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());
        /* nav links */
        $data['href_profile'] = $this->url->link('checkout/profile', '', true);
        $data['href_my_orders'] = $this->url->link('checkout/my_orders', '', true);
        $data['href_history'] = $this->url->link('checkout/history', '', true);
        $data['href_login'] = $this->url->link('account/login', 'redirect=checkout', true);
        $data['selected_page'] = 'my_orders'; // or 'profile' or 'history'

        /* orders */
        $this->load->model('catalog/product');
        $current_order = isset($this->session->data[$key_for_current_order]) ? $this->session->data[$key_for_current_order] : [];
        $products = [];
        $total_amount = 0;
        $total_into_money = 0;
        $current_theme = $this->config->get('config_theme');

        foreach ($current_order as $c_order) {
            $is_stock = true;
            if (!isset($c_order['product_id']) || !isset($c_order['quantity'])) {
                continue;
            }

            // find product
            $product_id = $c_order['product_id'];
            $product = $this->model_catalog_product->getProduct($product_id, false);

            $product_quantity_max = $this->getMaxQuantityProduct($product, $c_order);

            $data['is_product_existing'] = true;
            if (empty($product) || !is_array($product)) {
                $data['is_product_existing'] = false;
            }

            // product version and image
            $product_version_id = isset($c_order['product_version']['product_version_id']) ? $c_order['product_version']['product_version_id'] : '';
            $product_image = $product['image'];
            if ($product_version_id) {
                $product_version = $this->model_catalog_product->getProductVersion($product_version_id);
                if (empty($product_version)) {
                    $this->session->data['error'] = 'Phiên bản sản phẩm đã bị thay đổi, vui lòng chọn lại';
                } else {
                    if (in_array($current_theme, self::LIST_THEMES_SUPPORT_PRODUCT_VERSION_IMAGE)) {
                        $product_image = $product_version['image'];
                    }
                }
            }

            if ($product_image) {
                $image = $this->model_tool_image->resize($product_image, $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'));
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'));
            }

            // format
            if (isset($c_order['attribute']) && $c_order['attribute'] && isset($c_order['product_version']['price'])) {
                if ((int)$c_order['product_version']['price'] == 0) {
                    $c_order['product_version']['price'] = $c_order['product_version']['compare_price'];
                }
                $price_compare = (float)$c_order['product_version']['compare_price'];
                $total = (float)$c_order['product_version']['price'] * (int)$c_order['quantity'];
                $price = (float)$c_order['product_version']['price'];
            } else {
                if ((int)$product['price'] == 0) {
                    $product['price'] = $product['compare_price_master'];
                }
                $price_compare = (float)$product['compare_price_master'];
                $total = (float)$product['price'] * (int)$c_order['quantity'];
                $price = (float)$product['price'];
            }

            if (!$product_quantity_max) {
                $is_stock = false;
                $this->session->data['error'] = 'Vui lòng bỏ bớt sản phẩm hết hàng ra khỏi giỏ hàng của bạn';
            }
            //Get product name
            $product_name = isset($product['name']) ? $product['name'] : $c_order['product_name_add_cart'];
            $products[] = [
                'id' => $product_id,
                // old: 'fullname' => isset($product['name']) ? $product['name'] : $c_order['product_name_add_cart'],
                // old: 'name' => isset($product['name']) ? substr($product['name'], 0, 31).'...' : substr($c_order['product_name_add_cart'], 0, 31).'...',
                //Handle tooltip
                'fullname' => strlen($product_name) <= 30 ? '' : $product_name,
                //Handle product name
                'name' => strlen($product_name) <= 30 ? $product_name : mb_substr($product_name, 0, 31, 'UTF-8') . ' ...',
                'ordered_at' => isset($c_order['ordered_at']) ? $c_order['ordered_at'] : (new DateTime())->format('H:i d/m/Y'),
                'image_url' => $this->changeUrl($image, true, 'product_cart_header'),
                'url' => $this->url->link('product/product', 'product_id=' . $product_id, true),
                'amount' => $c_order['quantity'],
                'total' => $total,
                'attribute' => isset($c_order['attribute']) ? implode(' / ', $c_order['attribute']) : '',
                'price' => $price,
                'price_compare' => $price_compare,
                'price_compare_formart' => $this->currency->format($this->tax->calculate($price_compare, $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
                'price_formart' => $this->currency->format($this->tax->calculate($price, $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
                'total_formart' => $this->currency->format($this->tax->calculate($total, $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
                'product_version_id' => $product_version_id,
                'attributes' => isset($c_order['attribute']) ? $c_order['attribute'] : [],
                'product_quantity_max' => 9999,
                'in_stock' => $is_stock,
                'manufacturer' => isset($product['manufacturer']) ? $product['manufacturer'] : 'Thương hiệu',
            ];

            // for overall order
            $total_amount += $c_order['quantity'];
            $total_into_money += $total;
        }

        $data['order'] = [
            'products' => $products,
            'id' => null,
            'amount' => $total_amount,
            'into_money' => $total_into_money,
            'into_money_formart' => $this->currency->format($this->tax->calculate($total_into_money, 0, $this->config->get('config_tax')), $this->session->data['currency']),
        ];

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            $this->session->data['success'] = '';
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->session->data['error'])) {
            $data['error'] = $this->session->data['error'];
            unset($this->session->data['error']);
        }

        $data['order_products'] = $products;

        $data['customer_is_login']  = $this->customer->isLogged() ? true : false;

        /* button go to order preview */
        $data['href_order_preview'] = $this->url->link('checkout/order_preview', '', true);
        $data['href_order_preview_not_cart'] = $this->url->link('checkout/order_preview', 'isset_cart=false', true);
        $data['href_common_shop'] = $this->url->link('common/shop', '', true);

        return $data;
    }


}
