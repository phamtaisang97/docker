<?php

class ControllerCheckoutOrderPreview extends Controller
{
    use Theme_Config_Util;
    use Order_Util_Trait;
    use Device_Util;
    use Product_Util_Trait;

    const LIST_THEMES_SUPPORT_PRODUCT_VERSION_IMAGE = ['food_grocerie', 'tech_sun_electro'];
    const APP_CODE_PAYMENT_VNPAY = 'paym_ons_vnpay';
    public function index()
    {
        $key_for_current_order = "current_order_" . $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());
        // remove is login
//        if (!$this->customer->isLogged()) {
//            $this->session->data['redirect'] = $this->url->link('checkout/order_preview', '', true);
//
//            $this->response->redirect($this->url->link('account/login', '', true));
//        }
        $this->load->language('checkout/order_preview');
        $this->load->language('checkout/checkout_nav');

        $this->document->setTitle($this->config->get('heading_title'));

        $data = [];

        $data['payment_error'] = isset($this->request->get['payment_error']) ? $this->request->get['payment_error'] : null;
        //$data['payment_error'] = 'PAY MENT error test';

        $data['theme_directory'] = '/catalog/view/theme/' . $this->getCurrentThemeDir();

        $this->load->model('extension/module/theme_builder_config');
        /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;

        /* get favicon config */
        $favicon_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_FAVICON);
        $favicon_config = json_decode($favicon_config, true);

        $data['is_on_mobile'] = $this->isMobile() ? 1 : 0;

        $data['favicon'] = isset($favicon_config['image']['url']) ? cloudinary_change_http_to_https($favicon_config['image']['url']) : '';

        // customer info
        $data['customer_info']['firstname'] = $this->customer->getFirstName();
        $data['customer_info']['lastname'] = $this->customer->getLastName();
        $data['customer_info']['email'] = $this->customer->getEmail();
        $data['customer_info']['phone'] = $this->customer->getTelephone();
        $address_id = $this->customer->getAddressId();
        $data['customer_info']['address_id'] = $address_id;
        $customer_id = $this->customer->getId();
        $this->load->model('account/address');
        $data['customer_info']['address'] = $this->model_account_address->getAddressesCustom($customer_id);
        $data['customers_info_json'] = json_encode($data['customer_info']['address']);
        $data['customer_info']['address_default'] = $this->model_account_address->getAddress($address_id);
        if (!$data['customer_info']['address_default']) {
            $data['customer_info']['address_default']['lastname'] = $this->customer->getLastName();
            $data['customer_info']['address_default']['firstname'] = $this->customer->getFirstName();
            $data['customer_info']['address_default']['fullname'] = combine_name($this->customer->getFirstName(), $this->customer->getLastName());
            $data['customer_info']['address_default']['phone'] = $this->customer->getTelephone();
        }

        // keep old info in case checkout cart with deleted product
        if (isset($this->session->data['old_last_name'])) {
            $data['customer_info']['address_default']['lastname'] = $this->session->data['old_last_name'];
            unset($this->session->data['old_last_name']);
        }

        if (isset($this->session->data['old_first_name'])) {
            $data['customer_info']['address_default']['firstname'] = $this->session->data['old_first_name'];
            unset($this->session->data['old_first_name']);
        }

        if (isset($this->session->data['old_phone'])) {
            $data['customer_info']['address_default']['phone'] = $this->session->data['old_phone'];
            unset($this->session->data['old_phone']);
        }

        if (!isset($data['customer_info']['address_default']['address']) && isset($this->session->data['old_address'])) {
            $data['customer_info']['address_default']['address'] = $this->session->data['old_address'];
            unset($this->session->data['old_address']);
        }

        if (!isset($data['customer_info']['address_default']['city']) && isset($this->session->data['old_city_delivery'])) {
            $data['customer_info']['address_default']['city'] = $this->session->data['old_city_delivery'];
            unset($this->session->data['old_city_delivery']);
        }

        if (!isset($data['customer_info']['address_default']['district']) && isset($this->session->data['old_district_delivery'])) {
            $data['customer_info']['address_default']['district'] = $this->session->data['old_district_delivery'];
            unset($this->session->data['old_district_delivery']);
        }

        if (!isset($data['customer_info']['address_default']['wards']) && isset($this->session->data['old_ward_delivery'])) {
            $data['customer_info']['address_default']['wards'] = $this->session->data['old_ward_delivery'];
            unset($this->session->data['old_ward_delivery']);
        }

        if (!isset($data['customer_info']['address_default']['fullname'])) {
            $data['customer_info']['address_default']['fullname'] = combine_name($data['customer_info']['address_default']['firstname'], $data['customer_info']['address_default']['lastname']);
        }

        if (isset($this->session->data['voucher_value_error'])) {
            $data['voucher_value_error'] = $this->session->data['voucher_value_error'];
            unset($this->session->data['voucher_value_error']);
        }

        if (isset($this->session->data['price_error'])) {
            $data['price_error'] = $this->session->data['price_error'];
            unset($this->session->data['price_error']);
        }

        if (isset($this->session->data['order_add_on_product'])) {
            $data['order_add_on_product'] = $this->session->data['order_add_on_product'];
            unset($this->session->data['order_add_on_product']);
        }

        /* nav links */
        $data['href_profile'] = $this->url->link('checkout/profile', '', true);
        $data['href_my_orders'] = $this->url->link('checkout/my_orders', '', true);
        $data['href_history'] = $this->url->link('checkout/history', '', true);
        $data['selected_page'] = 'my_orders'; // or 'profile' or 'history'

        /* if request is POST from submit form */
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            // add product to current order
            if (!isset($this->session->data[$key_for_current_order])) {
                $this->session->data[$key_for_current_order] = [];
            }

            $order = $this->request->post;
            $order['ordered_at'] = (new DateTime())->format('H:i d/m/Y');
            $this->session->data[$key_for_current_order][] = $order;

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('checkout/my_orders', '', true));
        }

        /* orders */
        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        $current_order = isset($this->session->data[$key_for_current_order]) ? $this->session->data[$key_for_current_order] : [];
        //check current order
        if (!$current_order) {
            $this->response->redirect($this->url->link('checkout/my_orders', '', true));
        }
        $products = [];
        $total_amount = 0;
        $total_into_money = 0;
        $total_weight = 0;

        $totalQuantityProductDisplay = 0;
        $totalQuantityAddOnProduct = 0;
        $totalIntoMoneyAddOnProduct = 0;
        $totalWeightAddOnProduct = 0;

        $current_theme = $this->config->get('config_theme');
        $total_price_savings = 0;
        $product_price_savings = 0;
        $totalOriginalPriceAddOnProduct = 0;
        $ga4_begin_checkout_items = [];
        $ga4_begin_checkout_index = 0;
        foreach ($current_order as $key_order => $c_order) {
            $is_stock = true;

            if (!isset($c_order['product_id']) || !isset($c_order['quantity'])) {
                continue;
            }

            // find product
            $product_id = $c_order['product_id'];
            $product = $this->model_catalog_product->getProduct($product_id, false);
            $product_quantity_max = $this->getMaxQuantityProduct($product, $c_order);

            $data['is_product_existing'] = true;
            if (empty($product) || !is_array($product)) {
                $data['is_product_existing'] = false;
            }

            // product version and image
            $product_version_id = isset($c_order['product_version']['product_version_id']) ? $c_order['product_version']['product_version_id'] : 0;
            $product_image = $product['image'];
            if ($product_version_id) {
                $product_version = $this->model_catalog_product->getProductVersion($product_version_id);
                if (empty($product_version)) {
                    $this->session->data['error'] = 'Phiên bản sản phẩm đã bị thay đổi, vui lòng chọn lại';
                } else {
                    if (in_array($current_theme, self::LIST_THEMES_SUPPORT_PRODUCT_VERSION_IMAGE)) {
                        $product_image = $product_version['image'];
                    }
                }
            }

            if ($product_image) {
                $image = $this->model_tool_image->resize($product_image, $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
            }

            // format
            $change_data_session_product_version = false;
            if (isset($c_order['attribute']) && $c_order['attribute'] && isset($c_order['product_version']['price'])) {
                $product_version_id = isset($c_order['product_version']['product_version_id']) ? $c_order['product_version']['product_version_id'] : 0;
                $product_version_new = [];

                if (!empty($product_version_id)) {
                    $product_version_new = $this->model_catalog_product->getProductVersionById($product_id, $product_version_id);
                }

                if (empty($product_version_new) || !isset($product_version_new['price']) || !isset($product_version_new['compare_price'])) {
                    $this->session->data['error'] = 'Phiên bản sản phẩm đã bị thay đổi, vui lòng chọn lại';
                }

                if ((int)$c_order['product_version']['price'] != (int)$product_version_new['price']) {
                    $c_order['product_version']['price'] = $product_version_new['price'];
                    $change_data_session_product_version = true;
                }

                if ((int)$c_order['product_version']['price'] == 0 && !empty($product_version_new['discount_id'])) {
                    $c_order['product_version']['price'] = 0;
                } elseif ((int)$c_order['product_version']['price'] == 0) {
                    $c_order['product_version']['price'] = $product_version_new['compare_price'];
                }

                $total = (float)$c_order['product_version']['price'] * (int)$c_order['quantity'];
                $price = (float)$c_order['product_version']['price'];
                $price_old = (float)$c_order['product_version']['compare_price'] * (int)$c_order['quantity'];
                $price_before_discount = (float)$c_order['product_version']['compare_price'];
                $product_price_savings += $price_old - $total;
            } else {
                if ((int)$product['price'] == 0 && !empty($product['discount_id'])) {
                    $product['price'] = 0;
                } elseif ((int)$product['price'] == 0) {
                    $product['price'] = $product['compare_price_master'];
                }

                $total = (float)$product['price'] * (int)$c_order['quantity'];
                $price = (float)$product['price'];
                $price_old = (float)$product['compare_price_master'] * (int)$c_order['quantity'];
                $price_before_discount = (float)$product['compare_price_master'];
                $product_price_savings += $price_old - $total;
            }

            // set session item product of order if price change
            if ($change_data_session_product_version) {
                $this->session->data[$key_for_current_order][$key_order] = $c_order;
            }

            if (!$product_quantity_max) {
                $is_stock = false;
                $this->session->data['error'] = 'Vui lòng bỏ bớt sản phẩm hết hàng ra khỏi giỏ hàng của bạn';
                if (isset($this->request->get['isset_cart'])) {
                    $this->response->redirect($this->url->link('common/home', 'show_cart=true', ''));
                } else {
                    $this->response->redirect($this->url->link('checkout/my_orders', '', ''));
                }
            }

            $totalQuantityProductDisplay += 1;
            // count add-on product
            $this->load->model('discount/add_on_deal');
            $deal_data = $this->model_discount_add_on_deal->getAddOnDealByProductId($product_id, $product_version_id);

            $add_on_product_ids = [];
            $add_on_products = [];
            if (!empty($deal_data)) {
                if (!empty($c_order['add_on_products'])) {
                    foreach ($c_order['add_on_products'] as $add_on_product) {
                        $add_on_product_ids[] = $add_on_product['id'];
                    }
                }

                if (!empty($add_on_product_ids)) {
                    $add_on_products = $this->dataAddOnProducts($add_on_product_ids);
                }

                foreach ($add_on_products as $add_on_product) {
                    $totalQuantityAddOnProduct += 1;
                    $totalIntoMoneyAddOnProduct += (float)$add_on_product['sale_price'];
                    $totalOriginalPriceAddOnProduct += (float)$add_on_product['original_price'] - (float)$add_on_product['sale_price'];
                    $totalQuantityProductDisplay += 1;
                }
            } else {
                $this->session->data[$key_for_current_order][$key_order]['add_on_products'] = [];
            }
            // and add-on product

            $products[] = [
                'id' => $product_id,
                'fullname' => $product['name'],
                'name' => substr($product['name'], 0, 34),
                'ordered_at' => isset($c_order['ordered_at']) ? $c_order['ordered_at'] : (new DateTime())->format('H:i d/m/Y'),
                'image_url' => $this->changeUrl($image, true, 'product_cart_header'),
                'url' => $this->url->link('product/product', 'product_id=' . $product_id, true),
                'amount_view' => $c_order['quantity'] > 99 ? '99+' : $c_order['quantity'],
                'amount' => $c_order['quantity'],
                'total' => $total,
                'attribute' => isset($c_order['attribute']) ? implode(' / ', $c_order['attribute']) : '',
                'price' => $price,
                'price_formart' => $this->currency->format($this->tax->calculate($price, $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
                'total_formart' => $this->currency->format($this->tax->calculate($total, $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
                'product_version_id' => $product_version_id,
                'product_quantity_max' => 9999,
                'in_stock' => $is_stock,
                'manufacturer_id' => $product['manufacturer_id'],
                'list_categories' => $product['categories'],
                'manufacturer' => $product['manufacturer'],
                'price_before_discount' => $price_before_discount,
                'price_old' => $this->currency->format($this->tax->calculate($price_old, $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
                'tax_class_id' => $product['tax_class_id'],
                'add_on_products' => $add_on_products,
            ];

            // for overall order
            $total_amount += $c_order['quantity'];
            //$total_into_money += $total; // move to discount
            $total_weight = ($total_amount * $product['weight']);

            /* google analytic 4 begin_checkout event */
            $ga4_begin_checkout_item = [
                'item_id' => $product['sku'],
                'item_name' => $product['name'],
                'currency' => "VND",
                'index' => $ga4_begin_checkout_index++,
                'item_brand' => $product['manufacturer'],
                'quantity' => $c_order['quantity']
            ];

            foreach ($product['categories'] as $category_key => $category) {
                $ga4_begin_checkout_item['item_category' . (0 == $category_key ? '' : $category_key)] = $category['name'];
            }

            if (!empty($c_order['product_version'])) {
                $ga4_begin_checkout_item['item_variant'] = $c_order['product_version']['version'];
            }

            $ga4_begin_checkout_item['price'] = $price;
            $ga4_begin_checkout_items[] = $ga4_begin_checkout_item;
        }

        /* google analytic 4 begin_checkout event */
        $google_analytics_code = $this->config->get('online_store_config_google_code');
        if (!empty($google_analytics_code)) {
            // save to session
            $data['ga4_begin_checkout'] = [
                'event' => 'begin_checkout',
                'ecommerce' => [
                    'items' => $ga4_begin_checkout_items
                ]
            ];
            $data['ga4_begin_checkout'] = json_encode($data['ga4_begin_checkout'], JSON_HEX_APOS);
        }

        // merge lại dữ liệu vào $products
        $this->load->model('discount/discount');
        foreach ($products as &$product) {
            $price = $product['price_before_discount'] = $product['price'];
            $price = $price < 0 ? 0 : $price;
            $total_into_money += $price * $product['amount'];
            $product['price_after_discount'] = $price * $product['amount'];
            $product['price_after_discount_format'] = $this->currency->format($product['price_after_discount'], $this->session->data['currency']);
            $product['discounts'] = [];
        }
        unset($product);

        // calculate discount value by total order value
        $this->load->model('discount/discount');
        $orderDiscount = [];
        $order_discount_info = [];
        $order_discount_value = isset($order_discount_info['value']) ? $order_discount_info['value'] : 0;
        $data['order_discount_value'] = $order_discount_value;
        $data['order_discount_value_format'] = $product['price_after_discount_format'] = $this->currency->format($order_discount_value, $this->session->data['currency']);;
        if ($orderDiscount && is_array($order_discount_info)) {
            $data['order_discount']['name'] = isset($orderDiscount['name']) ? $orderDiscount['name'] : '';
            $data['order_discount']['code'] = isset($orderDiscount['code']) ? $orderDiscount['code'] : '';
            $data['order_discount']['content'] = '';
        }
        // end calculate discount value by total order value

        $total_weight = $total_weight + $totalWeightAddOnProduct;
        //deliver method
        $transport_fee = 0;
        $this->load->model('setting/setting');
        $delivery_methods = $this->model_setting_setting->getSettingValue('delivery_methods');
        $delivery_methods = json_decode($delivery_methods, true);
        if (is_array($delivery_methods) && !empty($delivery_methods)) {
            $flag_key = 0;
            foreach ($delivery_methods as $k => $delivery_method) {
                if ($delivery_method['status'] != '1') {
                    continue;
                }
                $data['delivery_methods'][$flag_key]['id'] = $delivery_method['id'];
                $data['delivery_methods'][$flag_key]['name'] = $delivery_method['name'];
                $delivery_fee = isset($data['customer_info']['address_default']['city']) ? $this->getTransportFee($data['delivery_methods'][$flag_key]['id'], $data['customer_info']['address_default']['city'], $total_weight, $total_into_money) : '';
                $data['delivery_methods'][$flag_key]['fee'] = isset($delivery_fee['fee']) ? $delivery_fee['fee'] : 0;
                $data['delivery_methods'][$flag_key]['fee_format'] = $this->currency->format($this->tax->calculate($data['delivery_methods'][$flag_key]['fee'], 0, $this->config->get('config_tax')), $this->session->data['currency']);
                // default method first
                if ($flag_key == 0) {
                    $transport_fee = $data['delivery_methods'][$flag_key]['fee'];
                }
                $flag_key += 1;
            }
        }
        // payment method
        $payment_methods = $this->model_setting_setting->getSettingValue('payment_methods');
        $payment_methods = json_decode($payment_methods, true);
        $payment_methods = is_array($payment_methods) ? $payment_methods : [];
        $payment_methods = array_values($payment_methods);
        // Check VNPay app enabled
        if($this->checkVnPayEnabled('paym_ons_vnpay')){
            $payment_vn_pay = array(
                'payment_id' =>'paym_ons_vnpay',
                'name' => 'Thanh toán trực tuyến qua VNPay',
                'guide' => 'Đơn hàng sẽ được chuyển khi bạn hoàn thành thanh toán',
                'status' => 1
            );
            array_push($payment_methods,$payment_vn_pay);
        }
        foreach ($payment_methods as $k => $payment_method) {
            if ($payment_method['status'] != '1') {
                continue;
            }
            $data['payment_methods'][$k]['payment_id'] = $payment_method['payment_id'];
            $data['payment_methods'][$k]['name'] = $payment_method['name'];
            $data['payment_methods'][$k]['guide'] = $payment_method['guide'];
        }

        $total_amount = $total_amount + $totalQuantityAddOnProduct;
        $total_into_money = $total_into_money + $totalIntoMoneyAddOnProduct;
        $total_price_savings = ($product_price_savings + $totalOriginalPriceAddOnProduct);
        $data['order'] = [
            'products' => $products,
            'id' => null,
            'amount' => $total_amount,
            'into_money' => $total_into_money,
            'into_money_format' => $this->currency->format($this->tax->calculate($total_into_money, 0, $this->config->get('config_tax')), $this->session->data['currency']),
            'total_price_savings' => $this->currency->format($this->tax->calculate($total_price_savings, 0, $this->config->get('config_tax')), $this->session->data['currency']),
            'transport_fee' => $transport_fee,
            'transport_fee_format' => $this->currency->format($this->tax->calculate($transport_fee, 0, $this->config->get('config_tax')), $this->session->data['currency']),
            'total_money' => $this->currency->format($this->tax->calculate($total_into_money + $transport_fee, 0, $this->config->get('config_tax')), $this->session->data['currency']),
            'total_money_after_discount' => $this->currency->format($this->tax->calculate($total_into_money - $order_discount_value + $transport_fee, 0, $this->config->get('config_tax')), $this->session->data['currency']),
        ];

        $data['order_summary'] = sprintf($this->language->get('order_summary_template'), $data['order']['amount']);

        /* get header config */
        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }
        $this->load->model('extension/module/theme_builder_config');
        /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;
        $header_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_SECTION_HEADER);
        $header_config = json_decode($header_config, true);
        /* display logo */
        $data['logo'] = isset($header_config['logo']['url']) ? cloudinary_change_http_to_https($header_config['logo']['url']) : 'image/no_image.png';
        /* get default localisation */
        $this->load->model('localisation/vietnam_administrative');
        $data['province'] = isset($data['customer_info']['address_default']['city']) ? $this->model_localisation_vietnam_administrative->getProvinceByCode($data['customer_info']['address_default']['city']) : '';
        $data['district'] = isset($data['customer_info']['address_default']['district']) ? $this->model_localisation_vietnam_administrative->getDistrictByCode($data['customer_info']['address_default']['district']) : '';
        $data['wards'] = isset($data['customer_info']['address_default']['wards']) ? $this->model_localisation_vietnam_administrative->getWardByCode($data['customer_info']['address_default']['wards']) : '';

        /* edit order link */
        $data['href_edit_order'] = $this->url->link('checkout/my_orders', '', true);

        /* get currency symbol */
        $data['currency'] = $this->currency->getSymbolRight($this->session->data['currency']);
        if (empty($data['currency'])) {
            $data['currency'] = $this->currency->getSymbolLeft($this->session->data['currency']);
        }
        $data['currency'] = 'đ';

        /* tool getting province, district, ward */
        $data['href_get_province'] = $this->url->link('tool/vietnam_administrative/getProvinces', '', true);
        $data['href_get_district'] = $this->url->link('tool/vietnam_administrative/getDistrictsByProvinceCode', '', true);
        $data['href_get_ward'] = $this->url->link('tool/vietnam_administrative/getWardsByDistrictCode', '', true);

        //error for page
        if (isset($this->session->data['error'])) {
            $data['error'] = $this->session->data['error'];
            unset($this->session->data['error']);
        }
        if (isset($this->session->data['error_phone'])) {
            $data['error_phone'] = $this->session->data['error_phone'];
            unset($this->session->data['error_phone']);
        }

        /* action submit */
//        $data['action'] = $this->url->link('sale/order/addCustom', '', true);
        $data['action'] = $this->url->link('sale/order/addCustom', '', true);
        $data['customer_is_login'] = $this->customer->isLogged() ? true : false;
        // data header
        /* GTM code */
        $data['gtm_code_header'] = $this->config->get('online_store_config_gtm_code_header');
        $data['gtm_code_header'] = empty($data['gtm_code_header']) ? null : html_entity_decode($data['gtm_code_header']);
        $data['gtm_code_body'] = $this->config->get('online_store_config_gtm_code_body');
        $data['gtm_code_body'] = empty($data['gtm_code_body']) ? null : html_entity_decode($data['gtm_code_body']);

        /* Fb sdk code */
        $data['fbsdk_code_body'] = $this->config->get('online_store_config_fbsdk_code_body');
        $data['fbsdk_code_body'] = empty($data['fbsdk_code_body']) ? null : html_entity_decode($data['fbsdk_code_body']);

        /* GG Analytics code */
        $data['google_analytics_code'] = $this->config->get('online_store_config_google_code');
        $data['google_analytics_code'] = empty($data['google_analytics_code']) ? null : html_entity_decode($data['google_analytics_code']);

        /* Facebook Pixel ID */
        $data['facebook_pixel'] = $this->config->get('online_store_config_facebook_pixel');
        $data['facebook_pixel'] = empty($data['facebook_pixel']) ? null : html_entity_decode($data['facebook_pixel']);

        $data['facebook_pixel_order'] = $this->config->get('online_store_config_facebook_pixel_order');
        $data['facebook_pixel'] .= (!empty($data['facebook_pixel_order'])) ? html_entity_decode($data['facebook_pixel_order']) : '';

        /* Maxlead code */
        $data['maxlead_code'] = $this->config->get('online_store_config_maxlead_code');
        $data['maxlead_code'] = empty($data['maxlead_code']) ? null : html_entity_decode($data['maxlead_code']);

        /* nav base layout */
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        $data['home_link'] = $this->url->link('common/home', '', true);

        $this->response->setOutput($this->load->view('checkout/order_preview', $data));
    }

    protected function checkVnPayEnabled($app_code) {
        // Kiểm tra xem app đã cài đặt chưa
        $this->load->model('appstore/my_app');
        $active = $this->model_appstore_my_app->checkAppActive($app_code);
        if(!$active){
            return false;
        }
        // Kiểm tra xem module đã bật chưa
        $module_active = $this->model_appstore_my_app->checkModuleActive($app_code);
        if(!$module_active){
            return false;
        }
        // Kiểm tra xem có kết nối chưa
        $this->load->model('extension/appstore/paym_ons_vnpay');
        $connect = $this->model_extension_appstore_paym_ons_vnpay->checkConnect();
        if(!$connect){
            return false;
        }
        return true;
    }
    protected function validateForm()
    {
        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

    /* TODO: remove when stable... */
    /*private function getTransportFee($transport = '', $city = '', $mass = '', $total_order = '')
    {

        $this->load->model('localisation/vietnam_administrative');
        $transport = $transport;
        $cityArr = $this->model_localisation_vietnam_administrative->getProvinceByCode($city);
        $city = '';
        if (isset($cityArr['name'])) {
            $city = $cityArr['name'];
        }

        $this->load->model('sale/order');
        $this->load->model('setting/setting');
        $this->load->language('sale/order');
        $delivery_methods = $this->model_setting_setting->getSettingValue('delivery_methods');
        $delivery_methods = json_decode($delivery_methods, true);

        $delivery_methods = is_array($delivery_methods) ? $delivery_methods : [];
        $value_fee_cod = 0;
        $fee_price = 0;
        $name_transport = '';
        foreach ($delivery_methods as $key => $vl) {
            $default_fee_region = isset($vl['fee_region']) ? $vl['fee_region'] : 0;
            if (isset($vl['fee_regions'])) {
                if ($transport == $vl['id']) {
                    $name_transport = $vl['name'];
                    $found_region = false;
                    foreach ($vl['fee_regions'] as $value) {
                        if (in_array(trim($city), $value['provinces'])) {
                            foreach ($value['weight_steps'] as $vl_weight) {
                                if (convertWeight($vl_weight['from']) <= $mass && convertWeight($vl_weight['to']) >= $mass) {
                                    $fee_price = str_replace(',', '', $vl_weight['price']); // Gia thuoc tinh thanh
                                    $found_region = true;
                                    break;
                                } else {
                                    $fee_price = str_replace(',', '', $default_fee_region);  // Gia mac dinh khi khong thuoc tinh thanh
                                }
                            }
                            break;
                        } else {
                            $fee_price = str_replace(',', '', $default_fee_region);  // Gia mac dinh khi khong thuoc tinh thanh
                        }
                        if ($found_region) {
                            break;
                        }
                    }
                    foreach ($vl['fee_cod'] as $key_cod => $fee_cod) {
                        $arr_fee_status = ($vl['fee_cod'][$key_cod]);
                        if ($arr_fee_status['status'] == 1) {
                            $arr_fee_status = str_replace(',', '', $arr_fee_status['value']);
                            if ($key_cod === 'dynamic') {
                                if ($total_order != 0) {
                                    $value_fee_cod = ($arr_fee_status / 100) * $total_order;
                                } else {
                                    $value_fee_cod = 0;
                                }
                            } elseif ($key_cod === 'fixed') {
                                $value_fee_cod = $arr_fee_status;
                            }
                        }
                    }
                }
            }
        }

        $json['fee_order'] = [
            'name' => $name_transport,
            'fee' => $fee_price + $value_fee_cod,
            'error' => '',
        ];
        return $json['fee_order'];
    }*/

    public function getJsonOrderInfo()
    {
        $data_post = $this->request->post;
        $key_for_current_order = "current_order_" . $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());
        //data address
        $this->load->model('account/address');
        $data_post['address_id'] = isset($data_post['address_id']) ? $data_post['address_id'] : '';
        $data_post['address_city'] = isset($data_post['address_city']) ? $data_post['address_city'] : '';
        $data_post['into_money'] = isset($data_post['into_money']) ? (float)$data_post['into_money'] : 0;
        $order_discount_value = isset($data_post['order_discount_value']) ? (float)$data_post['order_discount_value'] : 0;
        $customer_address = $this->model_account_address->getAddress($data_post['address_id']);

        $order_coupon_code = isset($data_post['coupon_code']) ? $data_post['coupon_code'] : '';

        /* orders */
        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        $current_order = $this->session->data[$key_for_current_order];
        $products = [];
        $total_amount = 0;
        $total_into_money = 0;
        $total_weight = 0;

        $totalQuantityProductDisplay = 0;
        $totalQuantityAddOnProduct = 0;
        $totalIntoMoneyAddOnProduct = 0;
        $totalWeightAddOnProduct = 0;

        foreach ($current_order as $key_order => $c_order) {
            if (!isset($c_order['product_id']) || !isset($c_order['quantity'])) {
                continue;
            }

            // find product
            $product_id = $c_order['product_id'];
            $product = $this->model_catalog_product->getProduct($product_id);
            $data['is_product_existing'] = true;
            if (empty($product) || !is_array($product)) {
                $data['is_product_existing'] = false;
            }

            $product_version_id = isset($c_order['product_version']['product_version_id']) ? $c_order['product_version']['product_version_id'] : 0;
            // format
            if (isset($c_order['attribute']) && $c_order['attribute']) {
                if ((int)$c_order['product_version']['price'] == 0) $c_order['product_version']['price'] = $c_order['product_version']['compare_price'];
                $total = (float)$c_order['product_version']['price'] * (int)$c_order['quantity'];
            } else {
                if ((int)$product['price'] == 0) $product['price'] = $product['compare_price_master'];
                $total = (float)$product['price'] * (int)$c_order['quantity'];
            }

            $totalQuantityProductDisplay += 1;
            // count add-on product
            $this->load->model('discount/add_on_deal');
            $deal_data = $this->model_discount_add_on_deal->getAddOnDealByProductId($product_id, $product_version_id);

            $add_on_product_ids = [];
            $add_on_products = [];
            if (!empty($deal_data)) {
                if (!empty($c_order['add_on_products'])) {
                    foreach ($c_order['add_on_products'] as $add_on_product) {
                        $add_on_product_ids[] = $add_on_product['id'];
                    }
                }


                if (!empty($add_on_product_ids)) {
                    $add_on_products = $this->dataAddOnProducts($add_on_product_ids);
                }

                foreach ($add_on_products as $add_on_product) {
                    $totalQuantityAddOnProduct += 1;
                    $totalIntoMoneyAddOnProduct += (float)$add_on_product['sale_price'];
                    $totalQuantityProductDisplay += 1;
                }
            } else {
                $this->session->data[$key_for_current_order][$key_order]['add_on_products'] = [];
            }
            // and add-on product

            // for overall order
            $total_amount += $c_order['quantity'];
            $total_into_money += $total;
            $total_weight = ($total_amount * $product['weight']);
        }
        $total_into_money = $data_post['into_money']; // tổng này được truyền lên sau khi trừ khuyễn mãi, giá tính bên trên chưa được tính khuyến mãi
        //deliver method
        $transport_fee = 0;
        $this->load->model('setting/setting');
        $delivery_methods = $this->model_setting_setting->getSettingValue('delivery_methods');
        $delivery_methods = json_decode($delivery_methods, true);

        $this->load->model('localisation/vietnam_administrative');
        $city_code = $customer_address['city'] ?? $data_post['address_city'];
        $data['province'] = array(
            'name' => isset($this->model_localisation_vietnam_administrative->getProvinceByCode($city_code)['name']) ?
                $this->model_localisation_vietnam_administrative->getProvinceByCode($city_code)['name'] : '',
            'id' => $city_code
        );

        if (!empty($customer_address['district'])) {
            $data['district'] = array(
                'name' => $this->model_localisation_vietnam_administrative->getDistrictByCode($customer_address['district'])['name'],
                'name_with_type' => $this->model_localisation_vietnam_administrative->getDistrictByCode($customer_address['district'])['name_with_type'],
                'id' => $customer_address['district']
            );
        }
        if (!empty($customer_address['wards'])) {
            $data['wards'] = array(
                'name' => $this->model_localisation_vietnam_administrative->getWardByCode($customer_address['wards'])['name'],
                'name_with_type' => $this->model_localisation_vietnam_administrative->getWardByCode($customer_address['wards'])['name_with_type'],
                'id' => $customer_address['wards']
            );
        }
        $data['customer'] = $customer_address;

        $cityArr = $this->model_localisation_vietnam_administrative->getProvinceByCode($data_post['address_city']);
        $city = '';
        if (isset($cityArr['name'])) {
            $city = $cityArr['name'];
        }

        /* get satisfied delivery methods due to order info (weight, city) */
        $satisfied_delivery_methods = $this->getSatisfiedDeliveryMethods(isset($customer_address['city']) ? $customer_address['city'] : $data_post['address_city'], $total_weight, $total_into_money);

        // filter by transport_id if has
        $transport_id = isset($data_post['transport']) ? $data_post['transport'] : null;
        if ($transport_id != null) {
            $satisfied_delivery_methods = array_filter($satisfied_delivery_methods, function ($satisfied_delivery_method) use ($transport_id) {
                return isset($satisfied_delivery_method['id']) && $satisfied_delivery_method['id'] == $transport_id;
            });
        }

        // coupon code
        $coupon_discount = 0;
        if (!empty($order_coupon_code)) {
            $coupon_discount = $this->getCouponByCode($total_into_money, $order_coupon_code);
        }

        $check = $total_into_money - $order_discount_value - $coupon_discount + (str_replace(',', '', $transport_fee));
        foreach ($satisfied_delivery_methods as &$satisfied_delivery_method) {
            // unknown why? TODO: check and remove if not use...
            $data['default'] = 0;

            // format fee
            $satisfied_delivery_method['fee_format'] = $this->currency->format($this->tax->calculate($satisfied_delivery_method['fee'], 0, $this->config->get('config_tax')), $this->session->data['currency']);

            $transport_fee = $satisfied_delivery_method['fee'];

            $total_amount = $total_amount + $totalQuantityProductDisplay;
            $total_into_money = $total_into_money + $totalIntoMoneyAddOnProduct;
            $total_weight = $total_weight + $totalWeightAddOnProduct;

            $data['order'][] = [
                'products' => $products,
                'amount' => $total_amount,
                'into_money' => $total_into_money,
                'coupon_discount' => $coupon_discount,
                'into_money_format' => $this->currency->format($this->tax->calculate($total_into_money, 0, $this->config->get('config_tax')), $this->session->data['currency']),
                'transport_fee' => $transport_fee,
                'transport_fee_format' => $this->currency->format($this->tax->calculate((str_replace(',', '', $transport_fee)), 0, $this->config->get('config_tax')), $this->session->data['currency']),
                'total_money' => $this->currency->format($this->tax->calculate($total_into_money + (str_replace(',', '', $transport_fee)), 0, $this->config->get('config_tax')), $this->session->data['currency']),
                'price_total_after_discount_format' => $check > 0 ? $this->currency->format($total_into_money - $order_discount_value - $coupon_discount + (str_replace(',', '', $transport_fee)), $this->session->data['currency']) : $this->currency->format(0 , $this->session->data['currency']),
                'price_total_after_discount_un_format' =>  $check > 0 ? $total_into_money - $order_discount_value - $coupon_discount + (str_replace(',', '', $transport_fee)) : 0,
                'total_has_transport' => $check > 0 ? $total_into_money - $order_discount_value - $coupon_discount + (str_replace(',', '', $transport_fee)) : 0,
            ];
        }
        unset($satisfied_delivery_method);

        /* build delivery method html */
        $html_method = "";
        $count_method = count($satisfied_delivery_methods);
        if ($count_method == 0 && isset($delivery_methods[0]) && is_array($delivery_methods[0])) {
            $satisfied_delivery_methods[] = $delivery_methods[0];
            $html_method = $this->delivery_methods_default(0, $delivery_methods[0], $data_post, $products, $total_amount, $customer_address, $total_weight, $total_into_money, $delivery_methods, $order_discount_value, $order_coupon_code);
        } else {
            $current_delivery_methods_idx = 0;
            foreach ($satisfied_delivery_methods as $satisfied_delivery_method) {
                if ($count_method == 1) {
                    $html_method = '<div class="box-radio delivery_methods">';
                } elseif ($count_method > 1) {
                    $html_method .= '<div class="box-radio delivery_methods">';
                }

                $html_method .= '    <label class="custom-check">';
                $html_method .= '        <input ' . ($current_delivery_methods_idx == 0 ? 'checked' : '') . ' class="input payment_trans_' . $satisfied_delivery_method['name'] . '" type="radio" data-price="' . $transport_fee . '"  value="' . $satisfied_delivery_method['id'] . '"  name="payment_trans">';
                $html_method .= '        <span>' . $satisfied_delivery_method['name'] . '</span>';
                $html_method .= '        <span class="checkmark"></span>';
                $html_method .= '    </label>';
                $html_method .= '    <span class="custom-payment payment_trans_fee_' . $satisfied_delivery_method['id'] . '">' . $transport_fee . '</span>';
                $html_method .= '</div>';

                $current_delivery_methods_idx++;
            }
        }

        $data['delivery_methods'] = $satisfied_delivery_methods;
        $data['deliveryMethod'] = $html_method;

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($data));
    }

    function delivery_methods_default($km, $vlm, $data_post, $products, $total_amount, $customer_address, $total_weight, $total_into_money, $delivery_methods, $order_discount_value, $order_coupon_code = '')
    {
        $data['default'] = 1;
        $delivery_methods = is_array($delivery_methods) ? $delivery_methods : [];
        $data['delivery_methods'][$km]['id'] = $vlm['id'];
        $data['delivery_methods'][$km]['name'] = $vlm['name'];
        $data['delivery_methods'][$km]['fee'] = str_replace(',', '', $delivery_methods[0]['fee_region']);
        $data['delivery_methods'][$km]['fee_format'] = $delivery_methods[0]['fee_region'];
        // default method first
        $transport_fee = $delivery_methods[0]['fee_region'];

        $coupon_discount = 0;
        if (!empty($order_coupon_code)) {
            $coupon_discount = $this->getCouponByCode($total_into_money, $order_coupon_code);
        }

        $check = $total_into_money - $order_discount_value - $coupon_discount + (str_replace(',', '', $transport_fee));
        $data['order'] = [
            'products' => $products,
            'amount' => $total_amount,
            'into_money' => $total_into_money,
            'coupon_discount' => $coupon_discount,
            'into_money_format' => $this->currency->format($this->tax->calculate($total_into_money, 0, $this->config->get('config_tax')), $this->session->data['currency']),
            'transport_fee' => $transport_fee,
            'transport_fee_format' => $this->currency->format($this->tax->calculate((str_replace(',', '', $transport_fee)), 0, $this->config->get('config_tax')), $this->session->data['currency']),
            'total_money' => $this->currency->format($this->tax->calculate($total_into_money + (str_replace(',', '', $transport_fee)), 0, $this->config->get('config_tax')), $this->session->data['currency']),
            'price_total_after_discount_format' => $check > 0 ? $this->currency->format($total_into_money - $order_discount_value - $coupon_discount + (str_replace(',', '', $transport_fee)), $this->session->data['currency']) : $this->currency->format(0 , $this->session->data['currency']),
            'price_total_after_discount_un_format' =>  $check > 0 ? $total_into_money - $order_discount_value - $coupon_discount + (str_replace(',', '', $transport_fee)) : 0,
            'total_has_transport' =>$check > 0 ? $total_into_money - $order_discount_value - $coupon_discount + (str_replace(',', '', $transport_fee)) : 0,
        ];

        $html_method = "";
        $html_method .= '<div class="box-radio delivery_methods">';
        $html_method .= '    <label class="custom-check">';
        $html_method .= '        <input checked class="input payment_trans_' . $delivery_methods[0]['name'] . '" type="radio" data-price="' . $delivery_methods[0]['fee_region'] . '" value="' . $delivery_methods[0]['id'] . '"  name="payment_trans">';
        $html_method .= '        <span>' . $delivery_methods[0]['name'] . '</span>';
        $html_method .= '        <span class="checkmark"></span>';
        $html_method .= '    </label>';
        $html_method .= '    <span class="custom-payment payment_trans_fee_' . $delivery_methods[0]['id'] . '">' . $this->currency->format($this->tax->calculate((str_replace(',', '', $delivery_methods[0]['fee_region'])), 0, $this->config->get('config_tax')), $this->session->data['currency']) . '</span>';
        $html_method .= '</div>';

        return array($data, $html_method);
    }

    public function orderSuccess()
    {
        $data = [];

        /* handle online payment redirect url */
        $payment_app_code = empty($this->request->get['payment_app_code']) ? (empty($_GET['payment_app_code']) ? null : $_GET['payment_app_code']) : $this->request->get['payment_app_code'];
        if ($payment_app_code != null) {
            $handle_error = $this->handleOnlinePaymentCallback($payment_app_code);
            if (is_array($handle_error) &&
                array_key_exists('error', $handle_error) && $handle_error['error'] &&
                array_key_exists('message', $handle_error)
            ) {
                $this->response->redirect($this->url->link('checkout/order_preview', '&payment_error=' . $handle_error['message'], true));
            }

            $data['order_code'] = isset($handle_error['order_code']) ? $handle_error['order_code'] : '';
        } else {
            $data['order_code'] = isset($this->request->get['order_code']) ? $this->request->get['order_code'] : '';
        }

        $data['theme_directory'] = 'catalog/view/theme/' . $this->getCurrentThemeDir();
        $data['url_shop'] = $this->url->link('common/shop');
        $data['url_home'] = $this->url->link('common/home');

        $this->load->model('checkout/order');
        $this->load->model('account/order');
        $order_info = $this->model_checkout_order->getOrderByCode($data['order_code']);
        if ($order_info) {
            // google analytics 4
            $google_analytics_code = $this->config->get('online_store_config_google_code');
            if (empty($google_analytics_code)) {
                $data['ga4_make_a_purchase'] = null;
            } else {
                $ga4_items = [];
                $this->load->model('sale/order');
                $this->load->model('catalog/product');
                $order_products = $this->model_sale_order->getOrderProducts($order_info['order_id']);
                $currency = 'VND';
                foreach ($order_products as $index => $order_product) {
                    $product = $this->model_catalog_product->getProduct($order_product['product_id']);
                    $ga4_item = [
                        'item_id' => $product['sku'],
                        'item_name' => $product['name'],
                        'currency' => $currency,
                        'index' => $index,
                        'item_brand' => $product['manufacturer']
                    ];

                    foreach ($product['categories'] as $category_key => $category) {
                        $ga4_item['item_category' . (0 == $category_key ? '' : $category_key)] = $category['name'];
                    }

                    if ($order_product['product_version_id']) {
                        $product_version = $this->model_catalog_product->getProductByVersionId($order_product['product_id'], $order_product['product_version_id']);
                        if ($product_version) {
                            $ga4_item['item_variant'] = $product_version['version'];
                        }
                    }

                    $product_price = $this->calculateProductPrice($product);
                    $ga4_item['price'] = (float)$product_price['price'];
                    $ga4_item['quantity'] = (int)$order_product['quantity'];

                    $ga4_items[] = $ga4_item;
                }

                $data['ga4_make_a_purchase'] = [
                    'event' => 'purchase',
                    'ecommerce' => [
                        'transaction_id' => $data['order_code'],
                        'value' => (float)$order_info['total'] ?: 0,
                        'shipping' => (float)$order_info['shipping_fee'],
                        'currency' => $currency,
                        'items' => $ga4_items
                    ]
                ];

                $data['ga4_make_a_purchase'] = json_encode($data['ga4_make_a_purchase'], JSON_HEX_APOS);
            }

            $title = "Bestme - Đơn hàng ".$data['order_code'];
            $this->document->setTitle($title);

            if ($order_info['invoice_no']) {
                $data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
            } else {
                $data['invoice_no'] = '';
            }

            $data['order_total_real'] = $this->currency->format((float)($order_info['total'] - $order_info['shipping_fee']) ? ($order_info['total'] - $order_info['shipping_fee']) : 0, $this->session->data['currency']);
            $data['order_shipping_fee'] = $this->currency->format((float)$order_info['shipping_fee'] ? $order_info['shipping_fee'] : 0, $this->session->data['currency']);
            $data['order_total'] = $this->currency->format((float)$order_info['total'] ? $order_info['total'] : 0, $this->session->data['currency']);
            $data['order_code'] = $order_info['order_code'];

            $data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));
            $data['order_status_name'] = $this->model_account_order->getOrderStatusName($order_info['order_status_id']);

            if ($order_info['payment_address_format']) {
                $format = $order_info['payment_address_format'];
            } else {
                $format = 'Họ tên: {lastname} {firstname} ' . "\n" . 'Địa chỉ: {address_2}' . "\n" . 'Điện thoại: {telephone}';
            }

            $find = array(
                '{firstname}',
                '{lastname}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{postcode}',
                '{zone}',
                '{zone_code}',
                '{country}',
                '{telephone}'
            );

            $replace = array(
                'firstname' => $order_info['payment_firstname'],
                'lastname' => $order_info['payment_lastname'],
                'company' => $order_info['payment_company'],
                'address_1' => $order_info['payment_address_1'],
                'address_2' => $order_info['address_shipping_total'], // payment address = shipping address
                'city' => $order_info['payment_city'],
                'postcode' => $order_info['payment_postcode'],
                'zone' => $order_info['payment_zone'],
                'zone_code' => $order_info['payment_zone_code'],
                'country' => $order_info['payment_country'],
                'telephone' => $order_info['telephone']
            );

            $data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
            $data['payment_status'] = $order_info['payment_status'];

            // get payment status text
            if ($order_info['payment_status'] == 0) {
                $data['payment_status_text'] = $this->language->get('text_payment_status_0');
            } else if ($order_info['payment_status'] == 4) {
                $data['payment_status_text'] = $this->language->get('text_payment_status_4');
            } else { // 1: success, 3: confirming (waiting callback from payment system)
                if ($order_info['order_status_id'] == 10) {
                    $data['payment_status_text'] = $this->language->get('text_payment_status_2');
                } else {
                    $data['payment_status_text'] = $this->language->get('text_payment_status_1');
                }
            }

            /*get payment method name*/
            // check if via payment app
            if (strpos($order_info['payment_method'], 'paym_') === 0) {
                if ($order_info['payment_method'] == self::APP_CODE_PAYMENT_VNPAY) {
                    $data['payment_method'] = $this->language->get('text_vnpay_payment');
                }
            } else {
                // else: via payment setting
                $this->load->model('setting/setting');
                $payment_methods = $this->model_setting_setting->getSettingValue('payment_methods');
                $payment_methods = json_decode($payment_methods, true);
                $payment_methods = is_array($payment_methods) ? $payment_methods : [];
                $data['payment_method'] = '';
                array_filter($payment_methods, function ($value) use ($order_info, &$data) {
                    if ($value['payment_id'] == $order_info['payment_method']) {
                        $data['payment_method'] = $value['name'];
                    }
                });
            }

            if ($order_info['shipping_address_format']) {
                $format = $order_info['shipping_address_format'];
            } else {
                //$format = '{lastname} {firstname}' . "\n" . '{company}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}' . "\n" . 'Số điện thoại: {telephone}';
                $format = 'Họ tên: {lastname} {firstname} ' . "\n" . 'Địa chỉ: {address_2}' . "\n" . 'Điện thoại: {telephone}';
            }

            $find = array(
                '{firstname}',
                '{lastname}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{postcode}',
                '{zone}',
                '{zone_code}',
                '{country}',
                '{telephone}'
            );

            $replace = array(
                'firstname' => $order_info['shipping_firstname'],
                'lastname' => $order_info['shipping_lastname'],
                'company' => $order_info['shipping_company'],
                'address_1' => $order_info['shipping_address_1'],
                'address_2' => $order_info['address_shipping_total'],
                'city' => $order_info['shipping_city'],
                'postcode' => $order_info['shipping_postcode'],
                'zone' => $order_info['shipping_zone'],
                'zone_code' => $order_info['shipping_zone_code'],
                'country' => $order_info['shipping_country'],
                'telephone' => $order_info['telephone']
            );

            $data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            // get order info for special theme use specific design(ie: tech_sun_electro, food_grocerie, ...)
            $data['order_info'] = array(
                'shipping_firstname' => $order_info['shipping_firstname'],
                'shipping_lastname' => $order_info['shipping_lastname'],
                'address_shipping_total' => $order_info['address_shipping_total'],
                'telephone' => $order_info['telephone']
            );

            $data['shipping_method'] = $order_info['shipping_method'];

            $this->load->model('catalog/product');
            $this->load->model('tool/upload');
            $this->load->model('tool/image');

            // Totals
            $data['totals'] = array();

            $totals = $this->model_account_order->getOrderTotals($order_info['order_id']);

            foreach ($totals as $total) {
                $data['totals'][] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
                );
            }

            $data['comment'] = nl2br($order_info['comment']);

            // History
            $data['histories'] = array();

            $results = $this->model_account_order->getOrderHistories($order_info['order_id']);

            foreach ($results as $result) {
                $data['histories'][] = array(
                    'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                    'status' => $result['status'],
                    'comment' => $result['notify'] ? nl2br($result['comment']) : ''
                );
            }

            $data['continue'] = $this->url->link('account/order', '', true);

            $data['order_success_text'] = nl2br($this->config->get('config_order_success_text'));

            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');
            $this->response->setOutput($this->load->view('checkout/order_success', $data));
        } else {
            $title = "Bestme - Không tìm thấy đơn hàng";
            $this->document->setTitle($title);
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');
            $this->response->setOutput($this->load->view('checkout/order_success', $data));
        }
    }

    public function getMaxQuantityProduct($product, $order)
    {
        $this->load->model('catalog/product');
        if ($product['multi_versions'] == 1) {
            $product_is_stock = $this->model_catalog_product->isStock($product['product_id'], $order['product_version']['product_version_id']);
            $product_version = $this->model_catalog_product->getProductByVersionId($product['product_id'], $order['product_version']['product_version_id']);
            if ($product_version && $product['status'] == 1 && $product_version['deleted'] != 1 && $product_version['status'] == 1 && ($product_is_stock || $product['sale_on_out_stock'] == 1)) {
                return true;
            } else {
                return false;
            }
        }
        $product_is_stock_single = $this->model_catalog_product->isStock($product['product_id'], 0);
        if ($product['multi_versions'] == 0 && $product['deleted'] != 1 && $product['status'] == 1 && ($product_is_stock_single || $product['sale_on_out_stock'] == 1)) {
            return true;
        }
        return false;
    }

    private function handleOnlinePaymentCallback($payment_app_code)
    {
        $paym_app_instance = Online_Payment::getOnlinePaymentInstance($payment_app_code, $this->registry, $data = ['somekey' => 'somevalue']);
        if (!method_exists($paym_app_instance, 'handleRedirect')) {
            return null;
        }

        // Could not use $this->request->get because of some data got encoded...
        // Temp use $_GET instead
        // TODO: find the best way to use $this->request->get...
        return $paym_app_instance->handleRedirect($_GET);
    }

    private function calVoucherDiscount($total_into_money, $coupon_code)
    {
        $coupon_discount = 0;

        $this->load->model('sale/order');
        $coupon_info = $this->model_sale_order->getCouponInfoByCode($coupon_code);
        if (!empty($coupon_info)) {
            if ($coupon_info['type'] == 1) {
                $coupon_discount = (float)$coupon_info['discount'];
            } elseif ($coupon_info['type'] == 2) {
                $coupon_discount = (float)$total_into_money  * (float)$coupon_info['discount'] / 100;
            }
        }

        return $coupon_discount;
    }

    public function getCouponByCode($total_into_money, $coupon_code)
    {
        $discount = 0;

        $this->load->model('sale/order');
        $code = $coupon_code;
        $into_money = $total_into_money;

        $key_for_current_order = "current_order_" . $this->session->getId() . "_" . ($this->customer->getId() == NULL ? 0 : $this->customer->getId());
        // create new in order_product table
        $products = $this->session->data[$key_for_current_order];

        // check nếu không có order
        if (!$products) {
            $this->response->redirect($this->url->link('checkout/my_orders', '', true));
        }

        if (empty($code)) {
            return $discount;
        }

        $result = $this->model_sale_order->getCouponInfoByCode($code);

        if (empty($result)) {
            return $discount;
        }

        $allowWithDiscount = $this->validCouponWithDiscount($result, $products);
        if ($allowWithDiscount) {
            return $discount;
        }

        if (!empty($result)) {
            if (empty($result['apply_for'])) {
                return $discount;
            }

            if ($result['apply_for'] == ModelSaleOrder::VOUCHER_APPLY_FOR_VALUE_ORDER) {
                if ((float)$into_money <= (float)$result['order_value_from']) {
                    return $discount;
                }
            } elseif ($result['apply_for'] == ModelSaleOrder::VOUCHER_APPLY_FOR_PRODUCT) {
                $checked = $this->model_sale_order->checkCouponProduct($result['coupon_id'], $products);

                if ($checked) {
                    return $discount;
                }
            }
        }

        if (!empty($result)) {
            $total = (float)$into_money;
            switch ($result['apply_for']) {
                case ModelSaleOrder::VOUCHER_APPLY_FOR_PRODUCT:
                    $discount = $this->calcDiscountCouponProduct($result, $total, $products);
                    break;
                default:
                    if ($result['type'] == 2) {
                        $discount = $total * (float)$result['discount'] / 100;
                    } else {
                        $discount = (float)$result['discount'];
                    }
                    break;
            }
        }

        return $discount;
    }

    private function validCouponWithDiscount($coupon, $products)
    {
        if (empty($coupon) || empty($products)) {
            return false;
        }

        $allowWithDiscount = false;
        foreach ($products as $product) {
            // find product
            $product_id = $product['product_id'];
            $this->load->model('catalog/product');
            $productData = $this->model_catalog_product->getProduct($product_id);
            $discount_id = null;

            if (empty($productData)) {
                continue;
            }

            if (isset($product['attribute']) && $product['attribute']) {
                $productVersionId = $product['product_version']['product_version_id'] ?? '';
                if (empty($productVersionId)) {
                    continue;
                }

                $productVersionData = $this->model_catalog_product->getProductVersionById($product_id, $productVersionId);
                if (empty($productVersionData)) {
                    continue;
                }

                $discount_id = $productVersionData['discount_id'];
            } else {
                $discount_id = $productData['discount_id'];
            }

            if ($coupon['allow_with_discount'] == 0 && !empty($discount_id)) {
                $allowWithDiscount = true;
            }
        }

        return $allowWithDiscount;
    }

    private function calcDiscountCouponProduct($coupon, $total, $products)
    {
        $couponProducts = $this->model_sale_order->checkCouponProduct($coupon['coupon_id'], $products, true);
        $discount = 0;
        $totalAmount = $total;
        $compareValue = (float)$coupon['applicable_type_value'];

        // map product
        foreach ($products as &$product) {
            if (!empty($product['product_version']) && !empty($product['product_version']['product_version_id'])) {
                $product['product_version_id'] = $product['product_version']['product_version_id'];
            } else {
                $product['product_version_id'] = 0;
            }
        }

        $calc = $this->calcTotalProductCoupon($couponProducts, $products);

        if ($coupon['applicable_type'] == ModelSaleOrder::VOUCHER_APPLICABLE_TYPE_QUANTITY_PRODUCT) {
            $discountTem = $this->calcDiscountCoupon($coupon, $calc['totalValueProduct']);
            $pass = $calc['totalQuantityProduct'] >= $compareValue;
            if ($coupon['how_to_apply'] == ModelSaleOrder::VOUCHER_PRODUCT_ONE_ON_ORDER) {
                return $pass ? $discountTem : 0;
            } elseif ($coupon['how_to_apply'] == ModelSaleOrder::VOUCHER_PRODUCT_QUANTITY_ON_ORDER) {
                return $pass ? $discountTem * $calc['totalQuantityProduct'] : 0;
            }
        } elseif ($coupon['applicable_type'] == ModelSaleOrder::VOUCHER_APPLICABLE_TYPE_TOTAL_ORDER) {
            $discountTem = $this->calcDiscountCoupon($coupon, $calc['totalValueProduct']);
            $pass = $calc['totalValueProduct'] >= $compareValue;
            if ($coupon['how_to_apply'] == ModelSaleOrder::VOUCHER_PRODUCT_ONE_ON_ORDER) {
                return $pass ? $discountTem : 0;
            } elseif ($coupon['how_to_apply'] == ModelSaleOrder::VOUCHER_PRODUCT_QUANTITY_ON_ORDER) {
                return $pass ? $discountTem * $calc['totalQuantityProduct'] : 0;
            }
        }

        return $discount;
    }

    private function calcTotalProductCoupon($couponProducts, $products)
    {
        $totalValueProduct = 0;
        $totalQuantityProduct = 0;

        foreach ($products as $product) {
            foreach ($couponProducts as $couponProduct) {
                if ($couponProduct['product_id'] == $product['product_id'] &&
                    $couponProduct['product_version_id'] == $product['product_version_id']) {
                    // find product
                    $product_id = $product['product_id'];
                    $this->load->model('catalog/product');
                    $productData = $this->model_catalog_product->getProduct($product_id);

                    if (isset($product['attribute']) && $product['attribute']) {
                        if ((int)$product['product_version']['price'] == 0){
                            if (!isset($product['product_version']['compare_price'])){
                                continue;
                            }
                            $product['product_version']['price'] = (float)$product['product_version']['compare_price'];
                        }
                        $price = (float)$product['product_version']['price'];
                    } else {
                        if ((int)$productData['price'] == 0) $productData['price'] = $productData['compare_price_master'];
                        $price = (float)$productData['price'];
                    }

                    $totalQuantityProduct += (float)$product['quantity'];
                    $totalValueProduct += (float)$price;
                }
            }
        }

        return [
            'totalValueProduct' => $totalValueProduct,
            'totalQuantityProduct' => $totalQuantityProduct
        ];
    }

    private function calcDiscountCoupon($coupon, $totalOrderAmount)
    {
        $discount = 0;

        if (isset($coupon['type']) && $coupon['type'] == ModelSaleOrder::VOUCHER_TYPE_AMOUNT) {
            $discount = (float)$coupon['discount'];
        } elseif (isset($coupon['type']) && $coupon['type'] == ModelSaleOrder::VOUCHER_TYPE_PERCENT) {
            $discount = $totalOrderAmount * (float)$coupon['discount'] / 100;
        }

        return $discount;
    }
}
