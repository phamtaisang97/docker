<?php

class ControllerCheckoutSetting extends Controller
{
    use Theme_Config_Util;
    use Device_Util;

    public function index()
    {
        $this->load->language('checkout/setting');
        $this->document->setTitle($this->language->get('setting_address'));
        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('checkout/profile', '', true);

            $this->response->redirect($this->url->link('account/login', '', true));
        }

        $this->load->language('checkout/profile');
        $this->load->language('checkout/checkout_nav');
        $this->load->model('account/address');
        $this->load->model('account/customer');

        $data = [];

        $data['theme_directory'] = '/catalog/view/theme/' . $this->getCurrentThemeDir();
        $data['is_on_mobile'] = $this->isMobile() ? 1 : 0;
        /* nav links */
        $data['href_home'] = $this->url->link('common/home', '', true);
        $data['href_profile'] = $this->url->link('checkout/profile', '', true);
        $data['href_my_orders'] = $this->url->link('checkout/my_orders', '', true);
        $data['href_history'] = $this->url->link('checkout/history', '', true);
        $data['href_setting_account'] = $this->url->link('checkout/setting', '', true);
        $data['selected_page'] = 'profile'; // 'profile' or 'my_orders' or 'history'

        /* account info */

        $address_id = $this->customer->getAddressId();
        $address = $this->model_account_address->getAddress($address_id);
        $account_check = $this->model_account_address->getCountAddessAccount();
        $customer_address = $this->model_account_address->getAddessAccount();
        $info_customer = $this->model_account_customer->getCustomer($this->customer->getId());

        $this->load->model('localisation/vietnam_administrative');
        $data['provincess'] = $this->model_localisation_vietnam_administrative->getProvinces();
        //$data['districts'] = $this->model_localisation_vietnam_administrative->getDistrictsByProvinceCode();
        //$data['wards'] = $this->model_localisation_vietnam_administrative->getWardsByDistrictCode();

        $data['max_address'] = (is_array($customer_address) && count($customer_address) >= 10) ? false : true;

        foreach ($customer_address as $vl_address) {
            $data['provinces'] = $this->model_localisation_vietnam_administrative->getProvinceByCode($vl_address['city']);
            $data['districts'] = $this->model_localisation_vietnam_administrative->getDistrictByCode($vl_address['district']);
            $data['wards'] = $this->model_localisation_vietnam_administrative->getWardByCode($vl_address['wards']);

            $data['provinces_edit'] = $this->model_localisation_vietnam_administrative->getProvinceByCode($vl_address['city']);
            $data['districts_edit'] = $this->model_localisation_vietnam_administrative->getDistrictsByProvinceCode($vl_address['city']);
            $data['wards_edit'] = $this->model_localisation_vietnam_administrative->getWardsByDistrictCode($vl_address['district']);
            $address_info = (isset($vl_address['address']) ? $vl_address['address'] : '');
            $address_id = $vl_address['address_id'];
            $data['account_infos'][] = array(
                'districts_edits_info' => $data['districts_edit'],
                'ward_edits_info' => $data['wards_edit'],
                'address_id' => $address_id,
                'firstname' => $vl_address['firstname'],
                'lastname' => $vl_address['lastname'],
                'address' => $vl_address['address'],
                'phone_number' => $vl_address['phone'],
                'default' => $info_customer['address_id'],
                'is_default_addr' => $address_id == $info_customer['address_id'] ? 1 : 0,
                'districts' => (int)$vl_address['district'],
                'wards' => $vl_address['wards'],
                'province' => (!empty($data['provinces']['code']) ? (int)$data['provinces']['code'] : ''),
                'district_info' => (!empty($data['districts']['code']) ? (int)$data['districts']['code'] : ''),
                'ward' => (!empty($data['wards']['code']) ? (int)$data['wards']['code'] : ''),
                'full_address' => $address_info . (!empty($data['wards']['name']) ? ', ' . $data['wards']['name'] : '') . (!empty($data['districts']['name']) ? ', ' . $data['districts']['name'] : '') . (!empty($data['provinces']['name']) ? ', ' . $data['provinces']['name'] : ''),
            );

        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }
        $limit = 10;

        /* orders */
        $this->load->model('sale/order');
        $filter_data = array(
            'order' => 'DESC'
        );
        $order_histories = $this->model_sale_order->getOrdersByCustomerId($this->customer->getId(), $filter_data);
        foreach ($order_histories as $order) {
            $data['orders'][] = array(
                'order_code' => $order['order_code'],
                'date_added' => $order['date_added'],
                'total' => $order['total'],
                'payment_method' => $order['payment_method'],
                'order_status' => $order['name'],
            );
        }


        $total_order_histories = count($order_histories);
        $order_histories = array_slice($order_histories, ((int)$page - 1) * $limit, $limit, true);


        $pagination = new CustomPaginate();
        $pagination->total = $total_order_histories;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->url = $this->url->link('checkout/history', '&page={page}', true);

        $data['pagination'] = $pagination->render();


        /* action submit */
        $data['action'] = $this->url->link('checkout/profile', '', true);
        $data['href_save'] = $this->url->link('checkout/setting/savecustomer', '', true);
        $data['href_delete'] = $this->url->link('checkout/setting/deleteaddress', '', true);
        /* tool getting province, district, ward */
        $data['href_get_province'] = $this->url->link('tool/vietnam_administrative/getProvinces', '', true);
        $data['href_get_district'] = $this->url->link('tool/vietnam_administrative/getDistrictsByProvinceCode', '', true);
        $data['href_get_ward'] = $this->url->link('tool/vietnam_administrative/getWardsByDistrictCode', '', true);
        /* nav base layout */
        $data['theme_directory'] = '/catalog/view/theme/' . $this->getCurrentThemeDir();
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('checkout/setting', $data));
    }

    public function deleteaddress()
    {
        $this->load->model('account/address');
        $this->model_account_address->deleteAddress($this->request->post['id'], $this->customer->getId());
    }

    public function savecustomer()
    {
        $this->load->model('account/address');
        $this->load->model('account/customer');
        $this->load->language('checkout/setting');
        $action = $this->url->link('checkout/setting', '', true);
        if (isset($this->request->post['edit_hide']) && $this->request->post['edit_hide'] == 1) {
            if ($this->request->post['phone'] == '') {
                echo json_encode(array('error_code' => 1, 'message' => $this->language->get('error_phone')));
                exit();
            }

            if (!is_numeric($this->request->post['phone'])) {
                echo json_encode(array('error_code' => 2, 'message' => $this->language->get('error_phone_2')));
                exit();
            }

            if (strlen($this->request->post['phone']) > 15) {
                echo json_encode(array('error_code' => 3, 'message' => $this->language->get('error_phone_3')));
                exit();
            }

            $pattern_phone = "/^0[0-9-+]+$|^84[0-9-+]+$|^\+84[0-9-+]+$/"; ///
            if (!preg_match($pattern_phone, $this->request->post['phone'])) {
                echo json_encode(array('error_code' => 4, 'message' => $this->language->get('error_phone_2')));
                exit();
            }

            $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/';
            if (preg_match($pattern, $this->request->post['fullname'])) {
                echo json_encode(array('error_code' => 8, 'message' => $this->language->get('error_fullname_special_character')));
                exit();
            }
            if (strlen($this->request->post['fullname']) > 20) {
                echo json_encode(array('error_code' => 5, 'message' => $this->language->get('error_fullname')));
                exit();
            }

            if ($this->request->post['address'] == '') {
                echo json_encode(array('error_code' => 7, 'message' => $this->language->get('error_address')));
                exit();
            }

            if (strlen($this->request->post['address']) > 50) {
                echo json_encode(array('error_code' => 10, 'message' => "Địa chỉ tối đa 50 ký tự"));
                exit();
            }

            if (!isset($this->request->post['city_delivery']) || empty($this->request->post['city_delivery'])) {
                echo json_encode(array('error_code' => 9, 'message' => 'Bạn phải nhập tỉnh thành'));
                exit();
            }
            $info = $this->request->post;
            $info['firstname'] = extract_name($this->request->post['fullname'])[0];
            $info['lastname'] = extract_name($this->request->post['fullname'])[1];
            if (array_key_exists('fullname', $info)) {
                unset($info['fullname']);
            }
            $this->model_account_address->editAddressCustomer($this->request->post['address_id'], $info);
        } else {

            if ($this->request->post['phone'] == '') {
                echo json_encode(array('error_code' => 1, 'message' => $this->language->get('error_phone')));
                exit();
            }

            if (!is_numeric($this->request->post['phone'])) {
                echo json_encode(array('error_code' => 2, 'message' => $this->language->get('error_phone_2')));
                exit();
            }

            if (strlen($this->request->post['phone']) > 15) {
                echo json_encode(array('error_code' => 3, 'message' => $this->language->get('error_phone_3')));
                exit();
            }
            $pattern_phone = "/^0[0-9-+]+$|^84[0-9-+]+$|^\+84[0-9-+]+$/"; ///
            if (!preg_match($pattern_phone, $this->request->post['phone'])) {
                echo json_encode(array('error_code' => 4, 'message' => $this->language->get('error_phone_2')));
                exit();
            }

            $get_info_customer = $this->model_account_customer->getCustomer($this->customer->getId());
            if ($get_info_customer['telephone'] == null) {
                $checkPhone = $this->model_account_address->getCountPhoneAccount($this->request->post['phone']);
                if ($checkPhone > 0) {
                    echo json_encode(array('error_code' => 11, 'message' => $this->language->get('error_phone_exist')));
                    exit();
                }
            }

            $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/';
            if (preg_match($pattern, $this->request->post['fullname'])) {
                echo json_encode(array('error_code' => 8, 'message' => $this->language->get('error_fullname_special_character')));
                exit();
            }
            if (strlen(trim($this->request->post['fullname'])) < 1 || strlen(trim($this->request->post['fullname'])) > 40) {
                echo json_encode(array('error_code' => 5, 'message' => $this->language->get('error_fullname')));
                exit();
            }

            if ($this->request->post['address'] == '') {
                echo json_encode(array('error_code' => 7, 'message' => $this->language->get('error_address')));
                exit();
            }

            if (strlen($this->request->post['address']) > 50) {
                echo json_encode(array('error_code' => 10, 'message' => "Địa chỉ tối đa 50 ký tự"));
                exit();
            }

            if (!isset($this->request->post['city_delivery']) || empty($this->request->post['city_delivery'])) {
                echo json_encode(array('error_code' => 9, 'message' => 'Bạn phải nhập tỉnh thành'));
                exit();
            }
            $info = $this->request->post;
            $info['firstname'] = extract_name($this->request->post['fullname'])[0];
            $info['lastname'] = extract_name($this->request->post['fullname'])[1];
            if (array_key_exists('fullname', $info)) {
                unset($info['fullname']);
            }
//            var_dump($info);die();

            if ($this->model_account_address->getTotalAddresses() >= 10) {
                echo json_encode(array('error_code' => 99, 'message' => 'Sổ địa chỉ không được vượt quá 10 địa chỉ', 'href' => $action));
                exit();
            }

            $this->model_account_address->addAddressCustomer($this->customer->getId(), $info);
        }
        echo json_encode(array('error_code' => 0, 'message' => $this->language->get('error_ajax_add'), 'href' => $action));
        exit();
    }

    protected function validateForm()
    {
        return true;

        if ((utf8_strlen($this->request->post['firstname']) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
            $this->error['firstname'] = $this->language->get('error_firstname');
        }

        if ((utf8_strlen($this->request->post['lastname']) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
            $this->error['lastname'] = $this->language->get('error_lastname');
        }

        if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
            $this->error['email'] = $this->language->get('error_email');
        }

        if ((utf8_strlen($this->request->post['telephone']) != 10)) {
            $this->error['telephone'] = $this->language->get('error_telephone');
        }

        $customer_info = $this->model_customer_customer->getCustomerByTelephone($this->request->post['telephone']);

        if (!isset($this->request->get['customer_id'])) {
            if ($customer_info) {
                $this->error['warning'] = $this->language->get('error_phone_exists');
            }
        } else {
            if ($customer_info && ($this->request->get['customer_id'] != $customer_info['customer_id'])) {
                $this->error['warning'] = $this->language->get('error_phone_exists');
            }
        }

        if (isset($this->request->post['addr'])) {
            foreach ($this->request->post['addr'] as $key => $value) {
                if ((utf8_strlen($value['firstname']) < 1) || (utf8_strlen($value['firstname']) > 32)) {
                    $this->error['addr'][$key]['firstname'] = $this->language->get('error_firstname');
                }

                if ((utf8_strlen($value['lastname']) < 1) || (utf8_strlen($value['lastname']) > 32)) {
                    $this->error['addr'][$key]['lastname'] = $this->language->get('error_lastname');
                }

                if ((utf8_strlen($value['address']) < 3) || (utf8_strlen($value['address']) > 128)) {
                    $this->error['addr'][$key]['address'] = $this->language->get('error_address_1');
                }

                if ((utf8_strlen($value['city']) > 128)) {
                    $this->error['addr'][$key]['city'] = $this->language->get('error_city');
                }

                if ((utf8_strlen($value['district']) > 128)) {
                    $this->error['addr'][$key]['district'] = $this->language->get('error_district');
                }

                if ((utf8_strlen($value['phone']) != 10)) {
                    $this->error['addr'][$key]['phone'] = $this->language->get('error_telephone');
                }
            }
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }
}
