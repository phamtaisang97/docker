<?php

class ControllerCheckoutProfile extends Controller
{
    use Device_Util;
    use Theme_Config_Util;

    public function index()
    {
        $this->document->setTitle($this->language->get('account_info_title'));
        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('checkout/profile', '', true);

            $this->response->redirect($this->url->link('account/login', '', true));
        }
        $this->load->language('checkout/profile');
        $this->load->language('checkout/checkout_nav');
        $this->load->model('account/address');
        $this->load->model('account/customer');

        $this->document->setTitle($this->language->get('account_info_title'));

        $data = [];

        $data['theme_directory'] = '/catalog/view/theme/' . $this->getCurrentThemeDir();

        /* nav links */
        $data['href_home'] = $this->url->link('common/home', '', true);
        $data['href_profile'] = $this->url->link('checkout/profile', '', true);
        $data['href_my_orders'] = $this->url->link('checkout/my_orders', '', true);
        $data['href_history'] = $this->url->link('checkout/history', '', true);
        $data['href_setting_account'] = $this->url->link('checkout/setting', '', true);
        $data['selected_page'] = 'profile'; // 'profile' or 'my_orders' or 'history'

        /* if edit profile */
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->load->model('account/customer');
            $this->model_account_customer->editCustomer($this->request->post['customer_id'], $this->request->post);
            $address_data = array(
                'customer_id' => $this->request->post['customer_id'],
                'firstname' => $this->request->post['firstname'],
                'lastname' => $this->request->post['lastname'],
                'company' => $this->request->post['company'],
                'city' => $this->request->post['city'],
                'district' => $this->request->post['district'],
                'postcode' => $this->request->post['postcode'],
                'address' => $this->request->post['address'],
                'default' => true

            );
            if ($this->request->post['address_id'] != 0 || $this->request->post['address_id'] != null) {
                $this->model_account_address->editAddressNew($this->request->post['address_id'], $address_data);
            } else {
                $this->model_account_address->addAddressNew($this->request->post['customer_id'], $address_data);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('checkout/profile', '', true));
        }

        /* account info */

        $address_id = $this->customer->getAddressId();
        $address = $this->model_account_address->getAddress($address_id);
        $account_check = $this->model_account_address->getCountAddessAccount();
        if ($account_check > 0) {
            $customer_address = $this->model_account_address->getAddessAccount();
            $this->load->model('localisation/vietnam_administrative');
            $province = $this->model_localisation_vietnam_administrative->getProvinceByCode($customer_address[0]['city']);
            $district = $this->model_localisation_vietnam_administrative->getDistrictByCode($customer_address[0]['district']);
            $ward = $this->model_localisation_vietnam_administrative->getWardByCode($customer_address[0]['wards']);
            $address_info = (isset($customer_address[0]['address']) ? $customer_address[0]['address'] : '');
            $firstname = (isset($customer_address[0]['firstname']) ? $customer_address[0]['firstname'] : '');
            $lastname = (isset($customer_address[0]['lastname']) ? $customer_address[0]['lastname'] : '');
            $phone = (isset($customer_address[0]['phone']) ? $customer_address[0]['phone'] : '');
        } else {
            $province = '';
            $district = '';
            $ward = '';
            $address_info = '';
            $phone = '';
        }

        $data['account_info'] = [
            'info' => $this->customer->getEmail(),
            'email' => $this->customer->getEmail(),
            'id' => $this->customer->getId(),
            'firstname' => $this->customer->getFirstName(),
            'lastname' => $this->customer->getLastName(),
            'address' => !empty($address['address_1']) ? $address['address_1'] : '',
            'phone_number' => $phone,
            'company' => '',
            'country' => !empty($address['country']) ? $address['country'] : '',
            'address_id' => $address_id,
            'address_default_fullname' => (!empty($address['lastname']) ? $address['lastname'] : '') . ' ' . (!empty($address['firstname']) ? $address['firstname'] : ''),
            'address_default_phone' => !empty($address['phone']) ? $address['phone'] : '',
            'full_address' => $address_info . (!empty($ward['name']) ? ', ' . $ward['name'] : '') . (!empty($district['name']) ? ', ' . $district['name'] : '') . (!empty($province['name']) ? ', ' . $province['name'] : ''),
        ];
        $data['address'] = $address;

        /* get default addr */
        // TODO: other way, reuse code not duplicate with catalog/controller/checkout/setting.php line 47...
        $customer_address = $this->model_account_address->getAddessAccount();
        $info_customer = $this->model_account_customer->getCustomer($this->customer->getId());
        $data['default_delivery_address'] = [];
        foreach ($customer_address as $vl_address) {
            if ($address['address_id'] != $vl_address['address_id']) {
                continue;
            }

            $data['provinces'] = $this->model_localisation_vietnam_administrative->getProvinceByCode($vl_address['city']);
            $data['districts'] = $this->model_localisation_vietnam_administrative->getDistrictByCode($vl_address['district']);
            $data['wards'] = $this->model_localisation_vietnam_administrative->getWardByCode($vl_address['wards']);

            $data['provinces_edit'] = $this->model_localisation_vietnam_administrative->getProvinceByCode($vl_address['city']);
            $data['districts_edit'] = $this->model_localisation_vietnam_administrative->getDistrictsByProvinceCode($vl_address['city']);
            $data['wards_edit'] = $this->model_localisation_vietnam_administrative->getWardsByDistrictCode($vl_address['district']);
            $address_info = (isset($vl_address['address']) ? $vl_address['address'] : '');
            $address_id = $vl_address['address_id'];
            $data['default_delivery_address'] = [
                'districts_edits_info' => $data['districts_edit'],
                'ward_edits_info' => $data['wards_edit'],
                'address_id' => $address_id,
                'firstname' => $vl_address['firstname'],
                'lastname' => $vl_address['lastname'],
                'address' => $vl_address['address'],
                'phone_number' => $vl_address['phone'],
                'default' => $info_customer['address_id'],
                'is_default_addr' => $address_id == $info_customer['address_id'] ? 1 : 0,
                'districts' => (int)$vl_address['district'],
                'wards' => $vl_address['wards'],
                'province' => (!empty($data['provinces']['code']) ? (int)$data['provinces']['code'] : ''),
                'district_info' => (!empty($data['districts']['code']) ? (int)$data['districts']['code'] : ''),
                'ward' => (!empty($data['wards']['code']) ? (int)$data['wards']['code'] : ''),
                'full_address' => $address_info . (!empty($data['wards']['name']) ? ', ' . $data['wards']['name'] : '') . (!empty($data['districts']['name']) ? ', ' . $data['districts']['name'] : '') . (!empty($data['provinces']['name']) ? ', ' . $data['provinces']['name'] : ''),
            ];
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }
        $limit = 10;

        /* orders */
        $this->load->model('sale/order');
        $filter_data = array(
            'order' => 'DESC'
        );
        $order_histories = $this->model_sale_order->getOrdersByCustomerId($this->customer->getId(), $filter_data);
        foreach ($order_histories as $order) {
            if ($order['payment_status'] == 0) {
                $payment_status = $this->language->get('text_payment_status_0');
            } else if ($order['payment_status'] == 4) {
                $payment_status = $this->language->get('text_payment_status_4');
            } else { // 1: success, 3: confirming (waiting callback from payment system)
                if ($order['order_status_id'] == 10) {
                    $payment_status = $this->language->get('text_payment_status_2');
                } else {
                    $payment_status = $this->language->get('text_payment_status_1');
                }
            }
            $order['payment_status'] == 1 ? $this->language->get('text_payment_status_1') : $this->language->get('text_payment_status_0');
            $transport_order_status = '';
            if (!empty($order['transport_order_code'])) {
                $transport = new Transport($order['transport_name'], $data, $this);
                $is_validate_transport = $transport->getAdaptor() instanceof \Transport\Abstract_Transport;
                $transport_order_status = $is_validate_transport ? $transport->getAdaptor()->getTransportStatusMessage($order['transport_status']) : '';
            }
            $data['orders'][] = array(
                'order_info_url' => $this->url->link('account/order/info', 'order_id=' . $order['order_id'], true),
                'order_code' => $order['order_code'],
                'date_added' => $order['date_added'],
                'total' => $order['total'],
                'payment_method' => $order['payment_method'],
                'order_status' => $order['name'],
                'payment_status' => $payment_status,
                'transport_order_status' => $transport_order_status
            );
        }

        $total_order_histories = count($order_histories);
        $order_histories = array_slice($order_histories, ((int)$page - 1) * $limit, $limit, true);


        $pagination = new CustomPaginate();
        $pagination->total = $total_order_histories;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->url = $this->url->link('checkout/history', '&page={page}', true);

        $data['pagination'] = $pagination->render();


        /* action submit */
        $data['action'] = $this->url->link('checkout/profile', '', true);

        /* nav base layout */
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        $data['is_on_mobile'] = $this->isMobile() ? 1 : 0;
        /* currency */
        $data['currency'] = $this->session->data['currency'];

        // placeholder image
        $data['placeholder_image_url'] = $this->session->data['placeholder_image_url'];

        $this->response->setOutput($this->load->view('checkout/profile', $data));
    }

    protected function validateForm()
    {
        return true;

        if ((utf8_strlen($this->request->post['firstname']) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
            $this->error['firstname'] = $this->language->get('error_firstname');
        }

        if ((utf8_strlen($this->request->post['lastname']) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
            $this->error['lastname'] = $this->language->get('error_lastname');
        }

        if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
            $this->error['email'] = $this->language->get('error_email');
        }

        if ((utf8_strlen($this->request->post['telephone']) != 10)) {
            $this->error['telephone'] = $this->language->get('error_telephone');
        }

        $customer_info = $this->model_customer_customer->getCustomerByTelephone($this->request->post['telephone']);

        if (!isset($this->request->get['customer_id'])) {
            if ($customer_info) {
                $this->error['warning'] = $this->language->get('error_phone_exists');
            }
        } else {
            if ($customer_info && ($this->request->get['customer_id'] != $customer_info['customer_id'])) {
                $this->error['warning'] = $this->language->get('error_phone_exists');
            }
        }

        if (isset($this->request->post['addr'])) {
            foreach ($this->request->post['addr'] as $key => $value) {
                if ((utf8_strlen($value['firstname']) < 1) || (utf8_strlen($value['firstname']) > 32)) {
                    $this->error['addr'][$key]['firstname'] = $this->language->get('error_firstname');
                }

                if ((utf8_strlen($value['lastname']) < 1) || (utf8_strlen($value['lastname']) > 32)) {
                    $this->error['addr'][$key]['lastname'] = $this->language->get('error_lastname');
                }

                if ((utf8_strlen($value['address']) < 3) || (utf8_strlen($value['address']) > 128)) {
                    $this->error['addr'][$key]['address'] = $this->language->get('error_address_1');
                }

                if ((utf8_strlen($value['city']) > 128)) {
                    $this->error['addr'][$key]['city'] = $this->language->get('error_city');
                }

                if ((utf8_strlen($value['district']) > 128)) {
                    $this->error['addr'][$key]['district'] = $this->language->get('error_district');
                }

                if ((utf8_strlen($value['phone']) != 10)) {
                    $this->error['addr'][$key]['phone'] = $this->language->get('error_telephone');
                }
            }
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }
}
