<?php

class ControllerCheckoutHistory extends Controller
{
    use Theme_Config_Util;

    public function index()
    {
        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('checkout/history', '', true);

            $this->response->redirect($this->url->link('account/login', '', true));
        }
        $this->load->language('checkout/history');
        $this->load->language('checkout/checkout_nav');

        $this->document->setTitle($this->config->get('heading_title'));

        $data = [];

        $data['theme_directory'] = '/catalog/view/theme/' . $this->getCurrentThemeDir();

        /* nav links */
        $data['href_profile'] = $this->url->link('checkout/profile', '', true);
        $data['href_my_orders'] = $this->url->link('checkout/my_orders', '', true);
        $data['href_history'] = $this->url->link('checkout/history', '', true);
        $data['selected_page'] = 'history'; // 'profile' or 'my_orders' or 'history'

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }
        $limit = 10;

        /* orders */
        $this->load->model('sale/order');
        $filter_data = array(
            'order' => 'DESC'
        );
        $order_histories = $this->model_sale_order->getOrdersByCustomerId($this->customer->getId(), $filter_data);
        $total_order_histories = count($order_histories);
        $order_histories = array_slice($order_histories, ((int)$page - 1) * $limit, $limit, true);
        $this->load->model('localisation/order_status');
        $order_statuses_map = $this->model_sale_order->getListStatus();

        $this->load->model('customer/customer');

        $data['histories'] = array_map(function ($history) use ($order_statuses_map) {
            $name = $this->model_sale_order->getNameForOrder($history['order_id']);
            //$order_status = $order_statuses_map[$history['order_status_id']];
            $order_status = $order_statuses_map[$history['order_status']];

            return [
                'name' => substr($name, 0, 28),
                'customer' => $history['fullname'],
                'address' => $history['shipping_address_2'],
                'value' => $history['total'],
                'payment_status' => $order_status,
            ];
        }, $order_histories);

        /* get currency symbol */
        $data['currency'] = $this->currency->getSymbolRight($this->session->data['currency']);
        if (empty($data['currency'])) {
            $data['currency'] = $this->currency->getSymbolLeft($this->session->data['currency']);
        }
        $data['currency'] = 'đ';

        $pagination = new CustomPaginate();
        $pagination->total = $total_order_histories;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->url = $this->url->link('checkout/history', '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $pagination = new CustomPaginate();
        $pagination->total = $total_order_histories;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->url = $this->url->link('checkout/history', '&page={page}', true);

        $data['pagination'] = $pagination->render();

        /* nav base layout */
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('checkout/history', $data));
    }
}
