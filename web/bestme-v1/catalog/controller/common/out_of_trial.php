<?php

class Controllercommonoutoftrial extends Controller
{
    use Theme_Config_Util;

    public function index()
    {
        $data = [];

        $data['theme_directory'] = '/catalog/view/theme/' . $this->getCurrentThemeDir();

        $this->load->language('common/out_of_trial');
        $this->document->setTitle($this->language->get('text_title'));

        $data['title'] = $this->document->getTitle();

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        $this->load->model('extension/module/theme_builder_config');
        /** @var ModelExtensionModuleThemeBuilderConfig $model_extension_module_theme_builder_config */
        $model_extension_module_theme_builder_config = $this->model_extension_module_theme_builder_config;

        /* get favicon config */
        $favicon_config = $model_extension_module_theme_builder_config->getConfigByKeyForStoreId($store_id = 0, ModelExtensionModuleThemeBuilderConfig::CONFIG_KEY_THEME_FAVICON);
        $favicon_config = json_decode($favicon_config, true);

        $data['favicon'] = isset($favicon_config['image']['url']) ? $favicon_config['image']['url'] : '';

        $data['base'] = $server;
        $data['description'] = $this->document->getDescription();
        $data['keywords'] = $this->document->getKeywords();
        $data['links'] = $this->document->getLinks();
        $data['styles'] = $this->document->getStyles();
        $data['scripts'] = $this->document->getScripts('header');
        $data['lang'] = $this->language->get('code');
        $data['direction'] = $this->language->get('direction');
        $data['hot_line'] = defined('CONFIG_HOT_LINE') ? CONFIG_HOT_LINE : '';

        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('common/out_of_trial', $data));
    }

}